

   MEMBER('sbj04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ04003.INC'),ONCE        !Local module procedure declarations
                     END


AskWaybillNumber PROCEDURE                            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LocalWaybillNo       LONG
skipWaybillProc      BYTE(0)
window               WINDOW('Enter Waybill Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Enter Waybill Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Enter Waybill Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,,COLOR:Black)
                           ENTRY(@s10),AT(292,206,96,10),USE(LocalWaybillNo),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020502'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AskWaybillNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:WAYBILLS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','AskWaybillNumber')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:WAYBILLS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AskWaybillNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
      way:WaybillNumber = LocalWaybillNo
      If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
          !Found
          ! Inserting (DBH 20/10/2006) # 8381 - Reprint the retail waybill
          If way:WayBillType = 10
              glo:Select1 = way:WaybillNumber
              WaybillRetail
              glo:Select1 = ''
          Else ! If way:WayBillType = 10
          ! End (DBH 20/10/2006) #8381
              Case way:WaybillID
              Of 100 Orof 101
                  ReprintExchangeLoanWaybill(LocalWaybillNo)
          ! Inserting (DBH 06/09/2006) # 7995 - Allow for sundry waybill
              Of 300
                  ViewSundryWaybill(way:RecordNumber)
          ! End (DBH 06/09/2006) #7995
              Of 400 ! #12151 Print waybill for return orders (Bryan: 07/09/2011)
                  WayBillDespatch(way:FromAccount,'TRA',way:ToACcount,'TRA',way:WaybillNumber,way:Courier)
              Else
                  If GetTempPathA(255,TempFilePath)
                      If Sub(TempFilePath,-1,1) = '\'
                          glo:File_Name = Clip(TempFilePath) & 'WAYBILLS' & Clock() & '.TMP'
                      Else !If Sub(TempFilePath,-1,1) = '\'
                          glo:File_Name = Clip(TempFilePath) & '\WAYBILLS' & Clock() & '.TMP'
                      End !If Sub(TempFilePath,-1,1) = '\'
                  End
                  ReprintWayBills(LocalWaybillNo)
                  Remove(glo:File_Name)
              End ! Case way:WaybillID
          End ! If way:WayBillType = 10
      Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
          !Error
      End ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020502'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020502'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020502'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReprintWayBills PROCEDURE (func:WayBillNumber)        !Generated from procedure template - Window

save_way_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
save_addtmp_id       USHORT,AUTO
save_waj_id          USHORT,AUTO
tmp:ToAccount        STRING(30)
tmp:AccountType      STRING(3)
tmp:WayBillNumber    STRING(30)
tmp:SubSubAccount    STRING(30)
tmp:AccountNumber    STRING(30)
tmp:WaybillType      BYTE(0)
tmp:ToType           STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW6::View:Browse    VIEW(ADDSEARCH)
                       PROJECT(addtmp:JobNumber)
                       PROJECT(addtmp:FinalIMEI)
                       PROJECT(addtmp:Surname)
                       PROJECT(addtmp:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
addtmp:JobNumber       LIKE(addtmp:JobNumber)         !List box control field - type derived from field
addtmp:FinalIMEI       LIKE(addtmp:FinalIMEI)         !List box control field - type derived from field
addtmp:Surname         LIKE(addtmp:Surname)           !List box control field - type derived from field
addtmp:RecordNumber    LIKE(addtmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Reprint Waybill'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Reprint Waybill'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('By Job Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Waybill Number: '),AT(168,98),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(236,98),USE(tmp:WayBillNumber),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(168,114,344,212),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('32L(2)|M~Job Number~@s8@120L(2)|M~I.M.E.I. No.~@s30@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020515'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReprintWayBills')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ADDSEARCH.Open
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:WAYBILLJ.Open
  Relate:WEBJOB.Open
  Access:WAYBILLS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  !New Way
  
  !    Prog.ProgressSetup(50)
  
      !New Way
      NewWay# = 0
      Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
      way:WayBillNumber = func:WayBillNumber
      If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Found
  
          If glo:WebJob
              If way:FromAccount <> Clarionet:Global.Param2
                  Case Missive('You did not produce the selected waybill.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Post(Event:CloseWindow)
              End !If way:FromAccount <> Clarionet:Global.Param2
          Else !If glo:WebJob
              If way:FromAccount <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                  Case Missive('You did not produce the selected waybill.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Post(Event:CloseWindow)
              End !If way:FromAccount <> GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !If glo:WebJob
  
          Save_waj_ID = Access:WAYBILLJ.SaveFile()
          Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
          waj:WayBillNumber = way:WayBillNumber
          Set(waj:JobNumberKey,waj:JobNumberKey)
          Loop
              If Access:WAYBILLJ.NEXT()
                 Break
              End !If
  
              If waj:WayBillNumber <> way:WayBillNumber      |
                  Then Break.  ! End If
  
  !            If Prog.InsideLoop()
  !                Break
  !            End ! If Prog.InsideLoop()
  
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = waj:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  
              If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  addtmp:JobNumber    = waj:JobNumber
                  ! -------------------------------------------------------------------
                  ! VP115 / VP120  21st NOV 02 - must display correct IMEI number
  
                  ! Any changes to the IMEI number display requires replicating in the
                  ! WayBillDespatch procedure (sbd01app)
  
                  case waj:WayBillNumber
                      of job:Exchange_Consignment_Number
                          ! Exchange waybill
                          Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                          xch:Ref_Number  = job:Exchange_Unit_Number
                          if Access:EXCHANGE.Fetch(xch:Ref_Number_Key) and job:Exchange_Unit_Number <> 0
                              addtmp:FinalIMEI = waj:IMEINumber
                          else
                              ! Display exchange unit IMEI number
                              addtmp:FinalIMEI = xch:ESN
                          end
                      else
                          addtmp:FinalIMEI = waj:IMEINumber
                  end
                  ! -------------------------------------------------------------------
                  !Change by Neil for testing more than anyhting!
                  !IF job:Exchange_Unit_Number > 0
                  !  Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                   ! xch:Ref_Number  = job:Exchange_Unit_Number
                  !  If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                  !    !Found
                  !    addtmp:FinalIMEI    = xch:ESN
                  !  ELSE
                  !    addtmp:FinalIMEI    = job:ESN
                  !  END
                  !ELSE
                  !  addtmp:FinalIMEI    = job:ESN
                 ! END
  
                  !addtmp:FinalIMEI = waj:IMEINumber
                  addtmp:Surname      = job:Model_Number
                  If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Successful
                  Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Failed
                      Access:ADDSEARCH.CancelAutoInc()
                  End !If Access:ADDSEARCH.TryInsert() = Level:Benign
              End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
              NewWay# = 1
          End !Loop
  
          Access:WAYBILLJ.RestoreFile(Save_waj_ID)
      Else!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
  
  !    Prog.ProgressFinish()
  
  If NewWay# = 0
      glo:Select20 = ''
      tmp:WayBillType = 0
      If glo:WebJob
  
          Prog.ProgressSetup(50)
  
          FoundJobs# = 0
          Save_job_ID = Access:JOBS.SaveFile()
          Access:JOBS.ClearKey(job:InConsignKey)
          job:Incoming_Consignment_Number = func:WayBillNumber
          Set(job:InConsignKey,job:InConsignKey)
          Loop
              If Access:JOBS.NEXT()
                 Break
              End !If
              If job:Incoming_Consignment_Number <> func:WayBillNumber      |
                  Then Break.  ! End If
  
              If Prog.InsideLoop()
                  Break
              End ! If Prog.InsideLoop()
  
              !Is this job for this account?
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Found
                  If wob:HeadAccountNumber <> Clarionet:Global.Param2
                      Cycle
                  End !If wob:HeadAccountNumber = Clarionet:Global.Param2
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
  
              FoundJobs# = 1
              glo:Select20 = 'WAYREPRINTJTOARC'
              tmp:WayBillType = 1 !RRC To ARC
              If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  addtmp:JobNumber    = job:Ref_Number
                  addtmp:FinalIMEI    = job:ESN
                  addtmp:Surname      = job:Model_Number
                  If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Successful
  
                  Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Failed
                      Access:ADDSEARCH.CancelAutoInc()
                  End !If Access:ADDSEARCH.TryInsert() = Level:Benign
              End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
  
          End !Loop
          Access:JOBS.RestoreFile(Save_job_ID)
  
          If FoundJobs# = 0
  
              Save_wob_ID = Access:WEBJOB.SaveFile()
              Access:WEBJOB.ClearKey(wob:JobWayBillNoKey)
              wob:JobWayBillNumber = func:WayBillNumber
              Set(wob:JobWayBillNoKey,wob:JobWayBillNoKey)
              Loop
                  If Access:WEBJOB.NEXT()
                     Break
                  End !If
                  If wob:JobWayBillNumber <> func:WayBillNumber      |
                      Then Break.  ! End If
  
                  If Prog.InsideLoop()
                      Break
                  End ! If Prog.InsideLoop()
  
                  !Is this job for this account?
                  If wob:HeadAccountNumber <> Clarionet:Global.Param2
                      Cycle
                  End !If wob:HeadAccountNumber = Clarionet:Global.Param2
  
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      FoundJobs# = 1
                      glo:Select20 = 'WAYREPRINTJTOCUST'
  
                      tmp:WayBillType = 2 ! RRC To Cust
  
                      If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                          addtmp:JobNumber    = job:Ref_Number
                          addtmp:FinalIMEI    = job:ESN
                          addtmp:Surname      = job:Model_Number
                          If Access:ADDSEARCH.TryInsert() = Level:Benign
                              !Insert Successful
  
                          Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                              !Insert Failed
                              Access:ADDSEARCH.CancelAutoInc()
                          End !If Access:ADDSEARCH.TryInsert() = Level:Benign
                      End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              End !Loop
              Access:WEBJOB.RestoreFile(Save_wob_ID)
  
              Save_wob_ID = Access:WEBJOB.SaveFile()
              Access:WEBJOB.ClearKey(wob:ExcWayBillNoKey)
              wob:ExcWayBillNumber = func:WayBillNumber
              Set(wob:ExcWayBillNoKey,wob:ExcWayBillNoKey)
              Loop
                  If Access:WEBJOB.NEXT()
                     Break
                  End !If
                  If wob:ExcWayBillNumber <> func:WayBillNumber      |
                      Then Break.  ! End If
                  If Prog.InsideLoop()
                      Break
                  End ! If Prog.InsideLoop()
  
                  FoundJobs# = 1
                  glo:Select20 = 'WAYREPRINTE'
  
                  tmp:WayBillType = 3 !Exchange RRC TO Cust
  
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
  
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number  = job:Exchange_Unit_Number
                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Found
                          If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                              addtmp:JobNumber    = job:Ref_Number
                              addtmp:FinalIMEI    = xch:ESN
                              addtmp:Surname      = xch:Model_Number
                              If Access:ADDSEARCH.TryInsert() = Level:Benign
                                  !Insert Successful
  
                              Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                                  !Insert Failed
                                  Access:ADDSEARCH.CancelAutoInc()
                              End !If Access:ADDSEARCH.TryInsert() = Level:Benign
                          End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                      Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Error
                      End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  
                   Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              End !Loop
              Access:WEBJOB.RestoreFile(Save_wob_ID)
          End !If FoundJob# = 0
  
          Prog.ProgressFinish()
  
  
          If FoundJobs# = 0
              Case Missive('Cannot find any jobs attached to the selected waybill number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              ?OK{prop:Disable} = 1
          End !If FoundJobs# = 0
  
      Else !glo:WebJob
          Prog.ProgressSetup(50)
  
          FoundJobs# = 0
          Save_job_ID = Access:JOBS.SaveFile()
          Access:JOBS.ClearKey(job:ConsignmentNoKey)
          job:Consignment_Number = func:WayBillNumber
          Set(job:ConsignmentNoKey,job:ConsignmentNoKey)
          Loop
              If Access:JOBS.NEXT()
                 Break
              End !If
              If job:Consignment_Number <> func:WayBillNumber      |
                  Then Break.  ! End If
  
              If Prog.InsideLoop()
              End !If Prog.InsideLoop()
              FoundJobs# = 1
  
  
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Found
                  If jobe:WebJob
                      tmp:WayBillType = 4 !ARC TO RRC
                      glo:Select20 = 'WAYREPRINTJTORRC'
                  Else !If jobe:WebJob
                      tmp:WayBillType = 5 !ARC TO Customer
                      glo:Select20 = 'WAYREPRINTJTOCUST'
                  End !If jobe:WebJob
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
              If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  addtmp:JobNumber    = job:Ref_Number
                  addtmp:FinalIMEI    = job:ESN
                  addtmp:Surname      = job:Model_Number
                  If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Successful
  
                  Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Failed
                      Access:ADDSEARCH.CancelAutoInc()
                  End !If Access:ADDSEARCH.TryInsert() = Level:Benign
              End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
          End !Loop
          Access:JOBS.RestoreFile(Save_job_ID)
  
          Save_wob_ID = Access:WEBJOB.SaveFile()
          Access:WEBJOB.ClearKey(wob:ExcWayBillNoKey)
          wob:ExcWayBillNumber = func:WayBillNumber
          Set(wob:ExcWayBillNoKey,wob:ExcWayBillNoKey)
          Loop
              If Access:WEBJOB.NEXT()
                 Break
              End !If
              If wob:ExcWayBillNumber <> func:WayBillNumber      |
                  Then Break.  ! End If
              If Prog.InsideLoop()
              End !If Prog.InsideLoop()
              FoundJobs# = 1
              glo:Select20 = 'WAYREPRINTE'
  
              tmp:WayBillType = 6 !ARC Exchange to RRC
  
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = wob:RefNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
  
                  Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                  xch:Ref_Number  = job:Exchange_Unit_Number
                  If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                      !Found
                      If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                          addtmp:JobNumber    = job:Ref_Number
                          addtmp:FinalIMEI    = xch:ESN
                          addtmp:Surname      = xch:Model_Number
                          If Access:ADDSEARCH.TryInsert() = Level:Benign
                              !Insert Successful
  
                          Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                              !Insert Failed
                              Access:ADDSEARCH.CancelAutoInc()
                          End !If Access:ADDSEARCH.TryInsert() = Level:Benign
                      End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  
               Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          End !Loop
          Access:WEBJOB.RestoreFile(Save_wob_ID)
          Prog.ProgressFinish()
  
          If FoundJobs# = 0
              Case Missive('Cannot find any jobs attached to the selected waybill number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              ?OK{prop:Disable} = 1
          End !If FoundJobs# = 0
  
      End !glo:WebJob
  End !NewWay# = 0
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:ADDSEARCH,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:WayBillNumber   = func:wayBillNumber
  ! Save Window Name
   AddToLog('Window','Open','ReprintWayBills')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,addtmp:JobNumberKey)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,addtmp:JobNumber,1,BRW6)
  BRW6.AddField(addtmp:JobNumber,BRW6.Q.addtmp:JobNumber)
  BRW6.AddField(addtmp:FinalIMEI,BRW6.Q.addtmp:FinalIMEI)
  BRW6.AddField(addtmp:Surname,BRW6.Q.addtmp:Surname)
  BRW6.AddField(addtmp:RecordNumber,BRW6.Q.addtmp:RecordNumber)
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ADDSEARCH.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:WAYBILLJ.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReprintWayBills')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020515'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020515'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020515'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      glo:Select21 = 'REPRINT'
      If glo:WebJob
      
          Access:TRADEACC.Clearkey(tra:Account_Number_key)
          tra:Account_Number  = Clarionet:Global.Param2
          Access:TRADEACC.Fetch(tra:Account_Number_key)
      
          free(glo:q_jobnumber)
          clear(glo:q_jobnumber)
      
          Save_addtmp_ID = Access:ADDSEARCH.SaveFile()
          Set(addtmp:JobNumberKey)
          Loop
              If Access:ADDSEARCH.NEXT()
                 Break
              End !If
              GLO:Job_Number_Pointer = addtmp:JobNumber
              Add(glo:Q_JobNumber)
          End !Loop
          Access:ADDSEARCH.RestoreFile(Save_addtmp_ID)
      
          if Records(glo:q_JobNumber)
              If Records(glo:Q_JobNumber) = 1
                  Case tmp:WayBillType
                      Of 1 !RRC to ARC
                          tmp:ToAccount   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
                          tmp:ToType      = 'TRA'
                      Of 2 !RRC to Customer
                          tmp:ToAccount   = ''
                          tmp:ToType      = 'CUS'
                      Of 3 !RRC Exchange to Customer
                          tmp:ToAccount   = ''
                          tmp:ToType      = 'CUS'
                  End !Case tmp:WayBillType
              Else !If Records(glo:Q_JobNumber) = 1
                  !Lets try and work out who the hell this job is supposed to be despatched to
                  Case tmp:WayBillType
                      Of 1 !RRC to ARC
                          tmp:ToAccount   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
                          tmp:ToType      = 'TRA'
                      Else
                          tmp:SubSubAccount = ''
                          tmp:AccountNumber = ''
                          tmp:ToAccount = ''
                          Loop x# = 1 To Records(glo:Q_JobNumber)
                              Get(glo:Q_JobNumber,x#)
                              Access:JOBS.Clearkey(job:Ref_Number_Key)
                              job:Ref_Number  = GLO:Job_Number_Pointer
                              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  !Found
                                  tmp:ToAccount = job:Account_Number
      
                                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                  jobe:RefNumber  = job:Ref_Number
                                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Found
                                      If tmp:SubSubAccount = ''
                                          If jobe:Sub_Sub_Account <> ''
                                              tmp:SubSubAccount = jobe:Sub_Sub_Account
                                          End !If jobe:SubSubAcount <> ''
                                      Else !If tmp:SubSubAccount = ''
                                          If tmp:SubSubAccount = jobe:Sub_Sub_Account
                                              tmp:ToAccount = jobe:Sub_Sub_Account
                                              Break
                                          End !If tmp:SubSubAccount = jobe:SubSubAccount
                                      End !If tmp:SubSubAccount = ''
                                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  If tmp:AccountNumber = ''
                                      tmp:AccountNumber = job:Account_Number
                                  Else
                                      If tmp:AccountNumber = job:Account_Number
                                          tmp:ToAccount = job:Account_Number
                                          Break
                                      End !If tmp:AccountNumber = job:AccountNumber
                                  End !If tmp:AccountNumber = ''
                              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          End !Loop x# = 1 To Records(glo:Q_JobNumber)
                          tmp:ToType      = 'SUB'
                  End !Case tmp:WayBillType
              End !If Records(glo:Q_JobNumber) = 1
      
              If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                  WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                                 tmp:ToAccount,tmp:ToType,|
                                 func:WayBillNumber,tra:Courier_Outgoing)
              Else
                  WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                                 tmp:ToAccount,tmp:ToType,|
                                 func:WayBillNumber,GETINI('DESPATCH','OUTGOINGCOURIER','',CLIP(PATH()) & '\SB2KDEF.INI'))
              End !If LocalOverWrite <> 'Y'
              free(glo:q_jobnumber)
          end
      Else !glo:WebJob
          Access:TRADEACC.Clearkey(tra:Account_Number_key)
          tra:Account_Number  = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
          Access:TRADEACC.Fetch(tra:Account_Number_key)
      
          free(glo:q_jobnumber)
          clear(glo:q_jobnumber)
      
          tmp:ToAccount = ''
          tmp:AccountType = ''
      
          Save_addtmp_ID = Access:ADDSEARCH.SaveFile()
          Set(addtmp:JobNumberKey)
          Loop
              If Access:ADDSEARCH.NEXT()
                 Break
              End !If
              If tmp:ToAccount = ''
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = addtmp:JobNumber
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      If jobe:WebJob
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = addtmp:JobNumber
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
                              tmp:ToAccount = wob:HeadAccountNumber
                              tmp:AccountType = 'TRA'
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      Else !If jobe:WebJob
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = addtmp:JobNumber
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
                              tmp:ToAccount = job:Account_Number
                              tmp:AccountType = 'SUB'
                          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      End !If jobe:WebJob
      
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              End !If tmp:ToAccount = ''
              GLO:Job_Number_Pointer = addtmp:JobNumber
              Add(glo:Q_JobNumber)
          End !Loop
          Access:ADDSEARCH.RestoreFile(Save_addtmp_ID)
      
          !make sure we get the overriden courier if it is set
          if Records(glo:q_JobNumber)
              If GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI') <> 'Y'
                  WayBillDespatch(Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                                    tmp:ToAccount,tmp:AccountType,|
                                     func:WayBillNumber,tra:Courier_Outgoing)
              Else
                  WayBillDespatch(Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                                    tmp:ToAccount,tmp:AccountType,|
                                     func:WayBillNumber,GETINI('DESPATCH','OUTGOINGCOURIER','',CLIP(PATH()) & '\SB2KDEF.INI'))
               End !If LocalOverWrite <> 'Y'
              free(glo:q_jobnumber)
          end
      
      End !glo:WebJob
      
      glo:Select20 = ''
      glo:Select21 = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
ReprintWayBillFatClient PROCEDURE (WaybillNumber)     !Generated from procedure template - Window

LocalWayBillNumber   LONG
LocalCourier         STRING(30)
LocalOverwrite       STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW8::View:Browse    VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Consignment_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !List box control field - type derived from field
job_ali:ESN            LIKE(job_ali:ESN)              !List box control field - type derived from field
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !List box control field - type derived from field
job_ali:Consignment_Number LIKE(job_ali:Consignment_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Reprint Waybills'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Reprint Waybill'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       STRING(@s8),AT(232,102),USE(LocalWayBillNumber),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(168,114,344,210),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48L(2)|FM~Job Number~@s8@80L(2)|F~I.M.E.I. Number~@s20@120L(2)|F~Model Number~@s' &|
   '30@'),FROM(Queue:Browse)
                       STRING('Waybill Number:'),AT(168,102),USE(?StrWaybillNumber),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Jobs On Waybill'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(380,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020514'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReprintWayBillFatClient')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:JOBS_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  LocalWaybillNumber = WayBillNumber
  Access:JOBS_ALIAS.CLEARKEY(job_ali:ConsignmentNoKey)
  job_ali:Consignment_Number = LocalWayBillNumber
  SET(job_ali:ConsignmentNoKey,job_ali:ConsignmentNoKey)
  IF Access:JOBS_ALIAS.NEXT() THEN
     flag# = 0
  ELSE
      IF job_ali:Consignment_Number <> LocalWayBillNumber THEN
         flag# = 0
      ELSE
          flag# = 1
      END !IF
  END !IF
  IF flag# = 0 THEN
     DISABLE(?Close)
  END !IF
  ! Save Window Name
   AddToLog('Window','Open','ReprintWayBillFatClient')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,job_ali:ConsignmentNoKey)
  BRW8.AddRange(job_ali:Consignment_Number,LocalWayBillNumber)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,job_ali:Consignment_Number,1,BRW8)
  BRW8.AddField(job_ali:Ref_Number,BRW8.Q.job_ali:Ref_Number)
  BRW8.AddField(job_ali:ESN,BRW8.Q.job_ali:ESN)
  BRW8.AddField(job_ali:Model_Number,BRW8.Q.job_ali:Model_Number)
  BRW8.AddField(job_ali:Consignment_Number,BRW8.Q.job_ali:Consignment_Number)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReprintWayBillFatClient')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      Access:TRADEACC.Clearkey(tra:Account_Number_key)
      tra:Account_Number  = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
      Access:TRADEACC.Fetch(tra:Account_Number_key)
      
      free(glo:q_jobnumber)
      clear(glo:q_jobnumber)
      
      
      Access:JOBS.ClearKey(job:InConsignKey)
      job:Incoming_Consignment_Number = LocalWayBillNumber
      set(job:InConsignKey, job:InConsignKey)
      loop until Access:JOBS.Next()
        if job:Incoming_Consignment_Number <> LocalWayBillNumber then break.
        glo:q_JobNumber.GLO:Job_Number_Pointer  = job:Ref_Number
        Add(glo:q_JobNumber)
      end
      
      !make sure we get the overriden courier if it is set
      LocalOverWrite = GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI')
      LocalCourier = GETINI('DESPATCH','OUTGOINGCOURIER','',CLIP(PATH()) & '\SB2KDEF.INI')
      if Records(glo:q_JobNumber)
          If LocalOverWrite <> 'Y'
             WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                             Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                             LocalWayBillNumber,tra:Courier_Outgoing)
          Else
              WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                             Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                             LocalWayBillNumber,LocalCourier)
          End !If LocalOverWrite <> 'Y'
          free(glo:q_jobnumber)
      end
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020514'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020514'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020514'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

ChangePassword PROCEDURE (windowTitle)                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:NewPassword      STRING(30)
tmp:RetypePassword   STRING(30)
tmp:ReturnPassword   STRING(30)
window               WINDOW('Change Password'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Change Password'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Password Details'),USE(?Tab1)
                           PROMPT('Enter Password'),AT(240,187),USE(?tmp:NewPassword:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,187,124,10),USE(tmp:NewPassword),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,PASSWORD
                           PROMPT('Re-Type Password'),AT(240,215),USE(?tmp:RetypePassword:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,215,124,10),USE(tmp:RetypePassword),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,PASSWORD
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnPassword)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020504'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ChangePassword')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Change the window title
  if windowTitle = ''
      ?WindowTitle{Prop:Text} = 'Enter New Password'
  else
      ?WindowTItle{Prop:Text} = clip(windowTitle)
  end
  ! Save Window Name
   AddToLog('Window','Open','ChangePassword')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','ChangePassword')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      if clip(tmp:NewPassword) <> clip(tmp:RetypePassword)
          Case Missive('The entered passwords do not match.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          select(?tmp:RetypePassword)
          cycle
      end
      
      tmp:ReturnPassword = tmp:NewPassword
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020504'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020504'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020504'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BastionInterface     PROCEDURE  (IMEINumber, extRef, pResult) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Variable declarations

WinDirectoryName            cstring(260)
fileName1                   string(255)
fileName2                   string(255)

randomName                  string(10)
readLine                    string(5000)
resDetail                   string(255), dim(20)

grpResult group, type
grp_TimeStamp               string(255)
grp_IMEI_Number             string(255)
grp_T1_Supplier_MSG         string(255)
grp_T1_Pass                 byte
grp_T2_Activity_MSG         string(255)
grp_T2_Pass                 byte
grp_T3_Black_Greylist_MSG   string(255)
grp_T3_Pass                 byte
grp_T4_Usage_MSG            string(255)
grp_T4_Pass                 byte
grp_T5_Swapout_MSG          string(255)
grp_T5_Pass                 byte
grp_Final_MSG               string(255)
grp_Final_Pass              byte
grp_Act_Date                string(255)
          end

grpBastionResult            &grpResult            ! Reference to the above group

changePassword              byte
newPassword                 string(30)

! File declarations

INFILE FILE,DRIVER('ASCII'),PRE(INF),NAME(glo:file_name5),CREATE
INFILERec   RECORD
TextLine        STRING(5000)
            END
    END

! Window declarations

winWait WINDOW('Bastion Warranty Checking'),AT(,,188,52),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
       PANEL,AT(4,4,180,44),USE(?panelDisplay),FILL(COLOR:Silver)
       STRING('Please Wait Validating IMEI Number ...'),AT(12,20),USE(?strDisplay),TRN,FONT('Tahoma',10,COLOR:Black,FONT:bold,CHARSET:ANSI)
     END
  CODE
!   BastionInterface(string IMEINumber, long pResult),byte

    if pResult = 0 then return 1.

    grpBastionResult &= (pResult)

    !if getini('WESettings','Username','')   ='' then error#=1.
    !if getini('WESettings','Password','')   ='' then error#=1.
    !tmp:Bastion = GETINI('BASTION','EnableChecking',,CLIP(PATH())&'\SB2KDEF.INI')

    if GetWindowsDirectory(WinDirectoryName,size(WinDirectoryName))
    end

    ! Create a unique filename
    loop
        randomName = random(10000, 99999)
        fileName1 = clip(randomName) & 'BIS.CSV'
        if ~exists(clip(WinDirectoryName) & '\' & fileName1) then break.
    end

    ! Setup the reply filename
    fileName2 = clip(randomName) & 'BIR.CSV'
    if exists(clip(WinDirectoryName) & '\' & fileName2) then remove(clip(WinDirectoryName) & '\' & fileName2).

    ! copy IMEI number, login, external ref to the send file
    Relate:USERS.Open()

    Access:USERS.ClearKey(use:password_key)
    use:password = glo:password
    Access:USERS.Fetch(use:password_key)

    Relate:USERS.Close()

    LinePrint('"' & clip(IMEINumber) & '","' & clip(use:user_code) & '","' & clip(extRef) & '"', clip(WinDirectoryName) & '\' & fileName1)

    do CallSoapClient

    ! Has password expired ?
    changePassword = getini('SoapClient', 'ChangePassword')
    if changePassword = true
        ! Display window prompting for new password
        newPassword = ChangePassword('Enter New Bastion Password')
        putini('SoapClient', 'NewPassword', newPassword)
        if exists(clip(WinDirectoryName) & '\' & clip(fileName2)) then remove(clip(WinDirectoryName) & '\' & clip(fileName2)).
        do CallSoapClient
        changePassword = getini('SoapClient', 'ChangePassword')
        if changePassword = true
            ! Password change failed
            Case Missive('Bastion warranty checking.'&|
              '<13,10>'&|
              '<13,10>Password change failed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        else
            ! Password changed successfully
            Case Missive('Bastion warranty checking.'&|
              '<13,10>'&|
              '<13,10>Password successfully changed.','ServiceBase 3g',|
                           'midea.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            putini('SoapClient', 'Password', newPassword)
            if exists(clip(WinDirectoryName) & '\' & clip(fileName2)) then remove(clip(WinDirectoryName) & '\' & clip(fileName2)).
            do CallSoapClient
        end
    end

    ! check for the reply file
    ! message(clip(WinDirectoryName) & '\' & fileName2)
    if exists(clip(WinDirectoryName) & '\' & fileName2)
        glo:file_name5 = clip(WinDirectoryName) & '\' & fileName2
        open(infile)
        set(infile, 0)

        next(infile)
        readLine = inf:TextLine
        do ReadRow

        close(infile)

        grpBastionResult.grp_TimeStamp = resDetail[1]
        grpBastionResult.grp_IMEI_Number = resDetail[2]

        grpBastionResult.grp_T1_Supplier_MSG = resDetail[3]
        if upper(resDetail[4]) = 'TRUE'
            grpBastionResult.grp_T1_Pass = 1
        else
            grpBastionResult.grp_T1_Pass = 0
        end

        grpBastionResult.grp_T2_Activity_MSG = resDetail[5]
        if upper(resDetail[6]) = 'TRUE'
            grpBastionResult.grp_T2_Pass = 1
        else
            grpBastionResult.grp_T2_Pass = 0
        end

        grpBastionResult.grp_T3_Black_Greylist_MSG = resDetail[7]
        if upper(resDetail[8]) = 'TRUE'
            grpBastionResult.grp_T3_Pass = 1
        else
            grpBastionResult.grp_T3_Pass = 0
        end

        grpBastionResult.grp_T4_Usage_MSG = resDetail[9]
        if upper(resDetail[10]) = 'TRUE'
            grpBastionResult.grp_T4_Pass = 1
        else
            grpBastionResult.grp_T4_Pass = 0
        end

        grpBastionResult.grp_T5_Swapout_MSG = resDetail[11]
        if upper(resDetail[12]) = 'TRUE'
            grpBastionResult.grp_T5_Pass = 1
        else
            grpBastionResult.grp_T5_Pass = 0
        end

        grpBastionResult.grp_Final_MSG = resDetail[13]
        if upper(resDetail[14]) = 'TRUE'
            grpBastionResult.grp_Final_Pass = 1
        else
            grpBastionResult.grp_Final_Pass = 0
        end

        grpBastionResult.grp_Act_Date = resDetail[15]

        ! Format activation date field (current format '0001-01-01T00:00:00.0000000+02:00')
        grpBastionResult.grp_Act_Date = date(grpBastionResult.grp_Act_Date[6:7],grpBastionResult.grp_Act_Date[9:10],grpBastionResult.grp_Act_Date[1:4])

    else    ! Error has occurred
        Case Missive('Unable to connect to web service.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        do RemoveFiles
        return 1
    end

    do RemoveFiles

    return 0

ReadRow routine
    data
tmp:found       long
tmp:columnCount long(1)
    code

    loop
        tmp:found = instring(',"', readLine, 1, 1)
        if tmp:found = 0
            resDetail[tmp:columnCount] = readLine[2:len(clip(readLine))-1]
            break
        end
        resDetail[tmp:columnCount] = readLine[2:(tmp:found-2)]
        readLine = readLine[(tmp:found+1):len(clip(readLine))]
        tmp:columnCount += 1
        if tmp:columnCount > 20 then break.
    end

RemoveFiles routine
    ! Remove existing files
    if exists(clip(WinDirectoryName) & '\' & clip(fileName1)) then remove(clip(WinDirectoryName) & '\' & clip(fileName1)).
    if exists(clip(WinDirectoryName) & '\' & clip(fileName2)) then remove(clip(WinDirectoryName) & '\' & clip(fileName2)).

CallSoapClient routine
    ! show the window
    if Glo:WebJob
        ClarioNET:OpenPushWindow(winWait)
    else
        open(winWait)
        display()
    end

    run('SOAPC.EXE ' & clip(fileName1) & ' ' & clip(fileName2), 1)

    ! close the window
    if Glo:WebJob
        ClarioNET:ClosePushWindow(winWait)
    else
        close(winWait)
    end
WarrantyClaimStatusReport PROCEDURE                   !Generated from procedure template - Window

tmp:StartDate        DATE
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
tmp:EndDate          DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Warranty Claim Status Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Claim Status Report'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Start Date'),AT(248,198),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(324,198,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR
                           PROMPT('End Date'),AT(248,214),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(324,214,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('d'),TIP('d'),UPR
                         END
                       END
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020518'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WarrantyClaimStatusReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:StartDate = Deformat('1/1/2000',@d6)
  tmp:EndDate     = Today()
  SELF.AddItem(?Cancel,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','WarrantyClaimStatusReport')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','WarrantyClaimStatusReport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020518'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020518'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020518'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      Case Missive('Click ''OK'' to begin.','ServiceBase 3g',|
                     'mexclam.jpg','\Cancel|/OK')
          Of 2 ! OK Button
      
              If GetTempPathA(255,TempFilePath)
                  If Sub(TempFilePath,-1,1) = '\'
                      glo:File_Name = Clip(TempFilePath) & 'Warranty Claim Status Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                  Else !If Sub(TempFilePath,-1,1) = '\'
                      glo:File_Name = Clip(TempFilePath) & '\Warranty Claim Status Report ' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
                  End !If Sub(TempFilePath,-1,1) = '\'
      
                  Remove(glo:File_Name)
                  Access:EXPGEN.Open()
                  Access:EXPGEN.UseFile()
      
                  !Count Records
                  Setcursor(Cursor:Wait)
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:DateCompletedKey)
                  job:Date_Completed = tmp:StartDate
                  Set(job:DateCompletedKey,job:DateCompletedKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Date_Completed > tmp:EndDate       |
                          Then Break.  ! End If
                      Yield()
                      Count# += 1
                      If Count# >= 1000
                          Count# = Records(JOBS)
                          Break
                      End !If Count# = 1000
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
                  Setcursor()
      
                  Prog.ProgressSetup(Count#)
      
                  JobsFound# = 0
      
                  Clear(gen:Record)
                  gen:Line1   = '"SB Job No","RRC Branch Code","RRC Job No","Warranty Charge Type","Warranty Repair Type",'&|
                                  '"Exch/Hand Fee","Labour","Parts","Total","Claim Status","Repair Location","Make","Model","Engineer",'&|
                                   '"Date Booked","Date Completed","Date Approved"'
                  Access:EXPGEN.Insert()
      
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:DateCompletedKey)
                  job:Date_Completed = tmp:StartDate
                  Set(job:DateCompletedKey,job:DateCompletedKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
      
                      End !If
                      If job:Date_Completed > tmp:EndDate       |
                          Then Break.  ! End If
                      Yield()
                      If Prog.InsideLoop()
                          Break
                      End ! If Prog.InsideLoop()
      
                      Display()
      
                      If job:Warranty_Job <> 'YES'
                          Cycle
                      End !If job:Warranty_Job <> 'YES'
      
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      Access:WEBJOB.Clearkey(wob:RefNumberKey)
                      wob:RefNumber   = job:Ref_Number
                      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                          !Found
                          If wob:HeadAccountNumber <> Clarionet:Global.Param2
                              Cycle
                          End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
                      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = wob:HeadAccountNumber
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
      
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
                      Clear(gen:Record)
      
                      !Export
                      gen:Line1   = '"' & Clip(job:Ref_Number) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(tra:BranchIdentification) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(wob:JobNumber) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(job:Warranty_Charge_Type) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(job:Repair_Type_Warranty) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(Format(jobe:ExchangeRate,@n14.2)) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(Format(jobe:RRCWLabourCost,@n14.2)) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(Format(jobe:RRCWPartsCost,@n14.2)) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(Format(jobe:RRCWLabourCost + jobe:RRCWPartsCost,@n14.2)) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(jobe:WarrantyClaimStatus) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(job:Location) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(job:Manufacturer) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(job:Model_Number) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(job:Engineer) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(Format(job:Date_Booked,@d06)) & '","'
                      gen:Line1   = Clip(gen:Line1) & Clip(Format(job:Date_Completed,@d06)) & '","'
      
                      Found# = 0
                      Save_aud_ID = Access:AUDIT.SaveFile()
                      Access:AUDIT.ClearKey(aud:Action_Key)
                      aud:Ref_Number = job:Ref_Number
                      aud:Action     = 'WARRANTY CLAIM APPROVED'
                      Set(aud:Action_Key,aud:Action_Key)
                      Loop
                          If Access:AUDIT.NEXT()
                             Break
                          End !If
                          If aud:Ref_Number <> job:Ref_Number      |
                          Or aud:Action     <> 'WARRANTY CLAIM APPROVED'      |
                              Then Break.  ! End If
                          Found# = 1
                          gen:Line1   = Clip(gen:Line1) & Clip(Format(aud:Date,@d06)) & '"'
                      End !Loop
                      Access:AUDIT.RestoreFile(Save_aud_ID)
                      If Found# = 0
                          gen:Line1   = Clip(gen:Line1) & '"'
                      End !If Found# = 0
      
                      JobsFound# += 1
                      Access:EXPGEN.Insert()
      
      
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
      
                  Prog.ProgressFinish()
      
                  Access:EXPGEN.Close()
      
                  If JobsFound#
                      SendFileToClient(glo:File_Name)
                      Case Missive('"Warranty Claim Status Report" created','ServiceBase 3g',|
                                     'midea.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      !Remove(glo:File_name)
                  Else !If JobsFound#
                      Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End !If JobsFound#
      
                  Remove(glo:File_name)
              End
          Of 1 ! Cancel Button
      End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 50
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Select_Locations PROCEDURE                            !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Location                      LIKE(glo:Location)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Tag              STRING(1)
BRW7::View:Browse    VIEW(LOCINTER)
                       PROJECT(loi:Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Select Locations'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(162,65,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       STRING('Counting the jobs on a tab can take a long time'),AT(240,138),USE(?String1),TRN,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       STRING('do not continue unless you are certain.'),AT(260,150),USE(?String2),TRN,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(232,162,220,124),USE(?List),IMM,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('12L(2)|MI@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                       PANEL,AT(166,70,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Select Location'),AT(170,72),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:000000'),AT(468,72),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(166,86,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(166,134,354,196),USE(?Panel2),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       GROUP('Top Tip'),AT(170,89,344,36),USE(?GroupTip),BOXED,TRN,HIDE
                       END
                       STRING('Tag the locations you want to include'),AT(270,290),USE(?String3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(245,300),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(309,300),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(373,300),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       BUTTON('&Rev tags'),AT(324,222,16,12),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(348,222,15,15),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(384,334),USE(?CountButton),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                       BUTTON,AT(452,334),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PANEL,AT(166,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW7.UpdateBuffer
   LocationQueue.Location = loi:Location
   GET(LocationQueue,LocationQueue.Location)
  IF ERRORCODE()
     LocationQueue.Location = loi:Location
     ADD(LocationQueue,LocationQueue.Location)
    tmp:Tag = '*'
  ELSE
    DELETE(LocationQueue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:tag='*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(LocationQueue)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     LocationQueue.Location = loi:Location
     ADD(LocationQueue,LocationQueue.Location)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(LocationQueue)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(LocationQueue)
    GET(LocationQueue,QR#)
    DASBRW::8:QUEUE = LocationQueue
    ADD(DASBRW::8:QUEUE)
  END
  FREE(LocationQueue)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Location = loi:Location
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Location)
    IF ERRORCODE()
       LocationQueue.Location = loi:Location
       ADD(LocationQueue,LocationQueue.Location)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020739'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Locations')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCINTER.Open
  SELF.FilesOpened = True
  !no priming to do
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:LOCINTER,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Select_Locations')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:Tag,BRW7.Q.tmp:Tag)
  BRW7.AddField(loi:Location,BRW7.Q.loi:Location)
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(LocationQueue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCINTER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Select_Locations')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CountButton
      !loop x# =1 to records(LocationQueue)
      !    get(locationQueue,x#)
      !    message(lq:location)
      !END !Loop
    OF ?CancelButton
      Clear(LocationQueue)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020739'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020739'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020739'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?CountButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     LocationQueue.Location = loi:Location
     GET(LocationQueue,LocationQueue.Location)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag='*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     LocationQueue.Location = loi:Location
     GET(LocationQueue,LocationQueue.Location)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

InsertBatchNumber PROCEDURE (f:Manufacturer)          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:BatchNumber      LONG
tmp:Manufacturer     STRING(30)
tmp:DateType         BYTE(0)
tmp:StartDate        DATE
tmp:EndDate          DATE
window               WINDOW('Reconciling Claim Payments'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Manufacturer'),AT(227,148),USE(?tmp:Manufacturer:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(299,148,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Manufacturer'),TIP('Manufacturer'),REQ,UPR
                       BUTTON,AT(427,144),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       GROUP,AT(347,196,116,38),USE(?Group:DateRange)
                         PROMPT('Start Date'),AT(347,196),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(399,196,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),UPR
                         PROMPT('End Date'),AT(347,224),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(399,224,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       END
                       GROUP,AT(347,284,117,18),USE(?Group:DateTo),HIDE
                         PROMPT('End Date'),AT(347,287),USE(?tmp:EndDate:Prompt:2),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(399,287,64,10),USE(tmp:EndDate,,?tmp:EndDate:2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       END
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Select Accepted Date Range'),AT(287,172),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       OPTION,AT(180,198,60,106),USE(tmp:DateType),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Jobs Between Date Range'),AT(188,209),USE(?tmp:DateType:Radio1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('Jobs Before Date'),AT(187,287),USE(?tmp:DateType:Radio2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Accepted Date Range'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020507'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertBatchNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EDIBATCH.Open
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  tmp:StartDate = Deformat('1/' & Month(Today()) & Year(Today()),@d06)
  tmp:EndDate = Today()
  
  tmp:Manufacturer = f:Manufacturer
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','InsertBatchNumber')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EDIBATCH.Close
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','InsertBatchNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseManufact
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:DateType
      Case tmp:DateType
      Of 0
          ?Group:DateRange{prop:Hide} = 0
          ?Group:DateTo{prop:Hide} = 1
      Of 1
          ?Group:DateRange{prop:Hide} = 1
          ?Group:DateTo{prop:Hide} = 0
      End ! Case tmp:DateType
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020507'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020507'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020507'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      If tmp:Manufacturer = ''
          Select(?tmp:Manufacturer)
          Cycle
      End ! If tmp:Manufacturer = ''
      If tmp:DateType = 1
          tmp:StartDate = Deformat('1/1/2000',@d06)
      End ! If tmp:DateType = 1
      If tmp:StartDate = '' Or tmp:EndDate = ''
          Select(?tmp:EndDate)
          Cycle
      End ! If tmp:StartDate = '' Or tmp:EndDate = ''
      
      If tmp:StartDate > tmp:EndDate
          Cycle
      End ! If tmp:StartDate > tmp:EndDate
      
      ReconcileWebBatch(tmp:Manufacturer,tmp:StartDate,tmp:EndDate)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReconcileWebBatch PROCEDURE (func:Manufacturer,f:StartDate,f:EndDate) !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_wob_id          USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_jobswarr_id     USHORT,AUTO
tmp:Claims           LONG
tmp:Labour           REAL
tmp:Parts            REAL
tmp:Total            REAL
tmp:Tag              STRING(1)
tmp:Manufacturer     STRING(30)
tmp:BatchNumber      LONG
tmp:EDI              STRING(3)
tmp:ClaimValue       REAL
tmp:JobNumber        STRING(20)
tmp:MarkedBlue       REAL
tmp:MarkedGreen      REAL
tmp:MarkedRed        REAL
tmp:UnderPaid        REAL
tmp:TradeId          STRING(30)
tmp:TotalLabour      REAL
tmp:TotalParts       REAL
tmp:LabourVAT        REAL
tmp:PartsVAT         REAL
tmp:CountJobs        LONG
tmp:InvoiceNumber    LONG
tmp:FilterByDate     BYTE(0)
tmp:LimitMonth       STRING(20)
YearQueue            QUEUE,PRE(year)
TheYear              STRING(4)
                     END
tmp:LimitYear        STRING(4)
tmp:BrowseStartDate  DATE
tmp:BrowseEndDate    DATE
tmp:AcceptedStartDate DATE
tmp:AcceptedEndDate  DATE
LocaljobNo           STRING(8)
locAuditNotes        STRING(255)
BRW6::View:Browse    VIEW(JOBSWARR)
                       PROJECT(jow:DateAccepted)
                       PROJECT(jow:RRCDateReconciled)
                       PROJECT(jow:RecordNumber)
                       PROJECT(jow:BranchID)
                       PROJECT(jow:RepairedAt)
                       PROJECT(jow:RRCStatus)
                       PROJECT(jow:Manufacturer)
                       PROJECT(jow:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_NormalFG       LONG                           !Normal forground color
tmp:Tag_NormalBG       LONG                           !Normal background color
tmp:Tag_SelectedFG     LONG                           !Selected forground color
tmp:Tag_SelectedBG     LONG                           !Selected background color
tmp:Tag_Icon           LONG                           !Entry's icon ID
wob:RefNumber          LIKE(wob:RefNumber)            !List box control field - type derived from field
wob:RefNumber_NormalFG LONG                           !Normal forground color
wob:RefNumber_NormalBG LONG                           !Normal background color
wob:RefNumber_SelectedFG LONG                         !Selected forground color
wob:RefNumber_SelectedBG LONG                         !Selected background color
tmp:JobNumber          LIKE(tmp:JobNumber)            !List box control field - type derived from local data
tmp:JobNumber_NormalFG LONG                           !Normal forground color
tmp:JobNumber_NormalBG LONG                           !Normal background color
tmp:JobNumber_SelectedFG LONG                         !Selected forground color
tmp:JobNumber_SelectedBG LONG                         !Selected background color
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Model_Number_NormalFG LONG                        !Normal forground color
job:Model_Number_NormalBG LONG                        !Normal background color
job:Model_Number_SelectedFG LONG                      !Selected forground color
job:Model_Number_SelectedBG LONG                      !Selected background color
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Warranty_Charge_Type_NormalFG LONG                !Normal forground color
job:Warranty_Charge_Type_NormalBG LONG                !Normal background color
job:Warranty_Charge_Type_SelectedFG LONG              !Selected forground color
job:Warranty_Charge_Type_SelectedBG LONG              !Selected background color
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
job:Repair_Type_Warranty_NormalFG LONG                !Normal forground color
job:Repair_Type_Warranty_NormalBG LONG                !Normal background color
job:Repair_Type_Warranty_SelectedFG LONG              !Selected forground color
job:Repair_Type_Warranty_SelectedBG LONG              !Selected background color
tmp:ClaimValue         LIKE(tmp:ClaimValue)           !List box control field - type derived from local data
tmp:ClaimValue_NormalFG LONG                          !Normal forground color
tmp:ClaimValue_NormalBG LONG                          !Normal background color
tmp:ClaimValue_SelectedFG LONG                        !Selected forground color
tmp:ClaimValue_SelectedBG LONG                        !Selected background color
wob:ReconciledMarker   LIKE(wob:ReconciledMarker)     !List box control field - type derived from field
wob:ReconciledMarker_NormalFG LONG                    !Normal forground color
wob:ReconciledMarker_NormalBG LONG                    !Normal background color
wob:ReconciledMarker_SelectedFG LONG                  !Selected forground color
wob:ReconciledMarker_SelectedBG LONG                  !Selected background color
jow:DateAccepted       LIKE(jow:DateAccepted)         !List box control field - type derived from field
jow:DateAccepted_NormalFG LONG                        !Normal forground color
jow:DateAccepted_NormalBG LONG                        !Normal background color
jow:DateAccepted_SelectedFG LONG                      !Selected forground color
jow:DateAccepted_SelectedBG LONG                      !Selected background color
jow:RRCDateReconciled  LIKE(jow:RRCDateReconciled)    !List box control field - type derived from field
jow:RRCDateReconciled_NormalFG LONG                   !Normal forground color
jow:RRCDateReconciled_NormalBG LONG                   !Normal background color
jow:RRCDateReconciled_SelectedFG LONG                 !Selected forground color
jow:RRCDateReconciled_SelectedBG LONG                 !Selected background color
wob:RRCWInvoiceNumber  LIKE(wob:RRCWInvoiceNumber)    !List box control field - type derived from field
wob:RRCWInvoiceNumber_NormalFG LONG                   !Normal forground color
wob:RRCWInvoiceNumber_NormalBG LONG                   !Normal background color
wob:RRCWInvoiceNumber_SelectedFG LONG                 !Selected forground color
wob:RRCWInvoiceNumber_SelectedBG LONG                 !Selected background color
jow:RecordNumber       LIKE(jow:RecordNumber)         !Primary key field - type derived from field
jow:BranchID           LIKE(jow:BranchID)             !Browse key field - type derived from field
jow:RepairedAt         LIKE(jow:RepairedAt)           !Browse key field - type derived from field
jow:RRCStatus          LIKE(jow:RRCStatus)            !Browse key field - type derived from field
jow:Manufacturer       LIKE(jow:Manufacturer)         !Browse key field - type derived from field
jow:RefNumber          LIKE(jow:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::jow:BranchID         LIKE(jow:BranchID)
HK9::jow:Manufacturer     LIKE(jow:Manufacturer)
HK9::jow:RRCStatus        LIKE(jow:RRCStatus)
HK9::jow:RepairedAt       LIKE(jow:RepairedAt)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Warranty Batch Processing'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Warranty Batch Processing'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(344,384),USE(?buttonImportReconciled),TRN,FLAT,ICON('imprecp.jpg')
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('closep.jpg')
                       LIST,AT(8,64,536,274),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)F*I@s1@0L(2)|FM*~Job No~@s8@61L(2)|FM*~Job No~@s20@84L(2)|M*~Model Number~' &|
   '@s30@110L(2)|M*~Charge Type~@s30@108L(2)|M*~Repair Type~@s30@48R(2)|M*~Claim Val' &|
   'ue~@n14.2@0L(2)*~Marker~@n1@52R(2)|M*~Accepted~@d6@60L(2)|M*~Date Reconciled~@d6' &|
   '@56L(2)|M*~Invoice No~@s8@'),FROM(Queue:Browse)
                       BUTTON,AT(480,384),USE(?Button:ViewJob),TRN,FLAT,LEFT,ICON('viewjobp.jpg')
                       SHEET,AT(4,28,544,352),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Awaiting Processing'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(8,49,78,10),USE(LocaljobNo),FONT(,,0101H,,CHARSET:ANSI),COLOR(COLOR:White),OVR
                           BUTTON,AT(88,44),USE(?SearchButton),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(412,384),USE(?OK),TRN,FLAT,LEFT,ICON('invoicep.jpg')
                           STRING('Claims:'),AT(556,57),USE(?String1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(620,57),USE(tmp:Claims),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           STRING('Labour:'),AT(556,76),USE(?String2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(620,76,48,10),USE(tmp:Labour),SKIP,TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           PROMPT('Parts:'),AT(556,100),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(620,100,48,10),USE(tmp:Parts),SKIP,TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           PROMPT('Total:'),AT(556,124),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(620,124,48,10),USE(tmp:Total),SKIP,TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),READONLY
                           BUTTON('&Rev tags'),AT(134,137,2,2),USE(?DASREVTAG),HIDE
                           GROUP('Mark Job'),AT(236,340,92,32),USE(?Group1),BOXED,TRN,FONT(,7,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(296,351),USE(?MarkRed),TRN,FLAT,ICON('markredp.jpg')
                             BUTTON,AT(240,351),USE(?MarkGreen),TRN,FLAT,ICON('markgrnp.jpg')
                             BUTTON,AT(268,351),USE(?MarkBlue),TRN,FLAT,ICON('markblup.jpg')
                           END
                           PROMPT('Marked Blue'),AT(556,156),USE(?tmp:MarkedBlue:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(620,156,48,10),USE(tmp:MarkedBlue),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Marked Blue'),TIP('Marked Blue'),UPR,READONLY
                           BUTTON('sho&W tags'),AT(138,169,2,2),USE(?DASSHOWTAG),HIDE
                           PROMPT('Marked Green'),AT(556,169),USE(?tmp:MarkedGreen:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(620,169,48,10),USE(tmp:MarkedGreen),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Marked Green'),TIP('Marked Green'),UPR,READONLY
                           PROMPT('Marked Red'),AT(556,180),USE(?tmp:MarkedRed:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(620,180,48,10),USE(tmp:MarkedRed),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Marked Red'),TIP('Marked Red'),UPR,READONLY
                           PROMPT('Under Paid'),AT(556,217),USE(?tmp:UnderPaid:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(620,217,48,10),USE(tmp:UnderPaid),SKIP,TRN,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Under Paid'),TIP('Under Paid'),UPR,READONLY
                           BUTTON,AT(580,236),USE(?RefresBatchValue),TRN,FLAT,LEFT,ICON('refbatp.jpg')
                           BUTTON,AT(12,346),USE(?TagGreen),TRN,FLAT,LEFT,ICON('taggrnp.jpg')
                           BUTTON,AT(80,346),USE(?TagBlue),TRN,FLAT,LEFT,ICON('tagbluep.jpg')
                           BUTTON,AT(148,346),USE(?tagRed),TRN,FLAT,LEFT,ICON('tagredp.jpg')
                           BUTTON,AT(348,344),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(416,344),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(484,344),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Reconciled'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(160,44,128,18),USE(?Group:Date)
                             PROMPT('Month'),AT(160,44),USE(?Prompt9),FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Year'),AT(240,44),USE(?Prompt9:2),FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             LIST,AT(160,52,76,10),USE(tmp:LimitMonth),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('80L(2)|M@s20@'),DROP(12),FROM('01 January|02 February|03 March|04 April|05 May|06 June|07 July|08 August|09 September|10 October|11 November|12 December')
                             LIST,AT(240,52,48,10),USE(tmp:LimitYear),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('16L(2)|M@s4@'),DROP(10),FROM(YearQueue)
                           END
                           ENTRY(@s8),AT(8,52,68,10),USE(jow:RefNumber,,?jow:RefNumber:2),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number')
                           BUTTON,AT(412,384),USE(?ReprintInvoice),TRN,FLAT,LEFT,ICON('rprninvp.jpg')
                           CHECK('Filter By Date'),AT(86,52),USE(tmp:FilterByDate),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Filter By Date'),TIP('Filter By Date'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(552,28,124,352),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Value Of Jobs'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                     END

!static webjob window
webjobwindow WINDOW,AT(,,165,42),FONT('Tahoma',8,,),COLOR(COLOR:White),CENTER,IMM,GRAY,DOUBLE,AUTO
       STRING('Running Report'),AT(12,8,144,16),USE(?Str1),TRN,CENTER,FONT(,18,COLOR:Red,FONT:bold)
       STRING('Please Wait'),AT(56,28),USE(?XString2),TRN,FONT('Tahoma',10,COLOR:Navy,FONT:regular+FONT:italic,CHARSET:ANSI)
     END
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:SkipCount          Byte(0)
Prog:SkipNumber         Long()
Prog:PercentText        String(100)
Prog:UserString         String(100)
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING(@s100),AT(0,3,161,10),USE(Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(0,32,161,10),USE(Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),HIDE,LEFT,ICON('pcancel.ico')
     END
Prog2        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog2:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog2.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog2.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog2.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog2:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog2:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog2.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog2.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog2.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 2 And tmp:FilterByDate = 0
BRW6::Sort2:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 2 And tmp:FilterByDate = 1
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
locImportFile           Cstring(255),STATIC
ImportFile File,Driver('BASIC'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record              Record
JobNumber                    String(30)
                    End
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   glo:Queue.Pointer = jow:RefNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = jow:RefNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  Queue:Browse.tmp:Tag_NormalFG = -1
  Queue:Browse.tmp:Tag_NormalBG = -1
  Queue:Browse.tmp:Tag_SelectedFG = -1
  Queue:Browse.tmp:Tag_SelectedBG = -1
  IF (tmp:tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = jow:RefNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = jow:RefNumber
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = jow:RefNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Prog:SkipCount = 0
    Prog:PercentText = '< 1% Completed'
    Prog:UserString = 'Working...'
    Prog:SkipNumber = 0
    If glo:WebJob
        Clarionet:OpenPushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Open(Prog:ProgressWindow)
        ?Prog:Cancel{prop:Hide} = 0
    End ! If glo:WebJob
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            Prog:PercentText = Format(Prog:PercentProgress,@n3) & '% Completed'
        Else
            Prog:PercentText = ''
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer

    If glo:WebJob
        If Prog:SkipCount < Prog:SkipNumber
            Prog:SkipCount += 1
            Exit
        Else
            Prog:SkipCount = 0
        End ! If Prog:SkipCount < 100
    End ! If glo:WebJob

    If glo:WebJob
        Clarionet:UpdatePushWindow(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    Prog:PercentText = 'Finished.'
    If glo:WebJob
        Clarionet:ClosePushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Close(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()
ImportReconciled            Routine
Data
locCreatedInvoice            Byte(0)
locInvoiceNumber            Long()

locCountJobs                Long()
locTotalParts               Real()
locTotalLabour              Real()
Code
    If ClarioNET:GetFilesFromClient(1,'TEST',1) = 1
        get(FileListQueue,1)
        locImportFile = flq:FileName
        if (NOT Exists(locImportFile))
            Exit
        end
    Else
        Exit
    end

    Open(ImportFile)
    If (Error())
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Unable to open import file.'&|
            '|' & Clip(Error()) & ' : ' & Clip(FileError()) & '','ServiceBase',|
                       'mstop.jpg','/&OK')
        Of 1 ! &OK Button
        End!Case Message
        exit
    end

    recCount# = 0
    set(ImportFile,0)
    Loop
        Next(ImportFile)
        If (Error())
            Break
        end
        recCount# += 1
    end

    Prog2.ProgressSetup(recCount#)

    count# = 0

    set(ImportFile,0)
    loop
        next(ImportFile)
        if (Error())
            Break
        end

        If (Prog2.InsideLoop())
            Break
        end

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = impfil:JobNumber
        If (Access:JOBS.TryFetch(job:Ref_Number_Key))
            Cycle
        End

        If (JobInUse(job:Ref_Number,1))
            Cycle
        End

        Access:JOBSWARR.Clearkey(jow:RefNumberKey)
        jow:RefNumber = job:Ref_Number
        If (Access:JOBSWARR.TryFetcH(jow:RefNumberKey))
            Cycle
        End

        If (jow:BranchID <> tmp:TradeID)
            Cycle
        End
        If (jow:RepairedAt <> 'RRC')
            Cycle
        End
        If (jow:RRCStatus <> 'APP')
            Cycle
        End
        If (jow:Manufacturer <> tmp:Manufacturer)
            Cycle
        end

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            Cycle
        End

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
            Cycle
        End

        IF (SentToHub(job:Ref_Number))
            ! #13614 Hub Repair Date is not reliable, so use this routine instead (DBH: 12/10/2015)
            CYCLE
        END ! IF

!        if (jobe:HubRepairDate <> '')
!            Cycle
!        End

        if (locCreatedInvoice = 0)
            ! First Job, so create an invoice record for all the jobs
            If (Access:INVOICE.PrimeRecord() = Level:Benign)
                locInvoiceNumber = inv:Invoice_Number
                If (Access:INVOICE.TryInsert())
                    Break
                End !If (Access:INVOICE.TryInsert())
                locCreatedInvoice = 1
            End ! If (Access:INVOICE.PrimeRecord() = Level:Benign)
        End !if (locCreateInvoice = 0)

        jobe:InvRRCWLabourCost = jobe:RRCWLabourCost
        jobe:InvRRCWPartsCost   = jobe:RRCWPartsCost
        jobe:InvRRCWSubTotal    = jobe:RRCWLabourCost + jobe:RRCWPartsCost
        locTotalLabour     += jobe:RRCWLabourCost
        locTotalParts      += jobe:RRCWPartsCost
        Access:JOBSE.Update()

        wob:EDI = 'PAY'
        wob:RRCWInvoiceNumber = locInvoiceNumber
        wob:ReconciledMarker = 0

        If Access:WEBJOB.TryUpdate() = Level:Benign
            locCountJobs += 1

            jow:RRCStatus = 'PAY'
            jow:RRCDateReconciled = Today()
            Access:JOBSWARR.TryUpdate()
            GetStatus(910,0,'JOB') !Despatch Paid
            ! Insert --- Save the job (DBH: 31/03/2009) #10757
            Access:JOBS.TryUpdate()
            ! end --- (DBH: 31/03/2009) #10757

            locAuditNotes         = 'BATCH NO: ' & Clip(job:EDI_Batch_Number) & |
                                '<13,10>WARRANTY CLAIM PAYMENT ACKNOWLEDGED<13,10>EXCH/HANDLING: '
            !Found
            If job:Exchange_Unit_Number <> ''
                locAuditNotes   = Clip(locAuditNotes) & Format(jobe:ExchangeRate,@n10.2)
            Else !If job:Exchange_Unit_Number <> ''
                locAuditNotes   = Clip(locAuditNotes) & Format(jobe:HandlingFee,@n10.2)
            End !If job:Exchange_Unit_Number <> ''

            locAuditNotes  = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(jobe:RRCWLabourCost,@n10.2) & |
                            '<13,10>PARTS: ' & Format(jobe:RRCWPartsCost,@n10.2)

            IF (AddToAudit(wob:refnumber,'JOB','WARRANTY CLAIM MARKED PAID (RRC) INV NO: ' & locInvoiceNumber,locAuditNotes))
            END ! IF

        End !If Access:WEBJOB.TryUpdate() = Level:Benign
    End

    Prog2.ProgressFinish()

    Close(ImportFile)
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Missive('File Imported.'&|
        '|Record(s) Updated: ' & Clip(locCountJobs) & '','ServiceBase',|
                   'midea.jpg','/&OK')
    Of 1 ! &OK Button
    End!Case Message

    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = locInvoiceNumber
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        If locCountJobs = 0
            Relate:INVOICE.Delete(0)
        Else !If tmp:TotalJobs = 0
            inv:Date_Created        = Today()
            inv:account_number     = Clarionet:Global.Param2
            inv:accounttype        = 'MAI'
            inv:total              = locTotalLabour + locTotalParts
            inv:vat_rate_labour    = tmp:LabourVAT
            inv:vat_rate_parts     = tmp:PartsVAT
            inv:vat_number         = def:vat_number
            inv:batch_number       = 0
            inv:manufacturer       = func:Manufacturer
            inv:total_claimed      = locTotalLabour + locTotalParts
            inv:labour_paid        = locTotalLabour
            inv:parts_paid         = locTotalParts
            inv:reconciled_date    = Today()
            inv:invoice_type       = 'WRC'
            inv:Jobs_Count          = locCountJobs
            Access:INVOICE.Update()

            RRCWarrantyInvoice(locInvoiceNumber)
        End !If tmp:TotalJobs = 0
        !Found
    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    Post(Event:CloseWindow)

RefreshBatchValue       Routine
    tmp:Labour = 0
    tmp:Parts = 0
    tmp:Total = 0
    tmp:MarkedBlue = 0
    tmp:MarkedGreen = 0
    tmp:MarkedRed = 0
    tmp:Claims = 0

    Count# = 0
    Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
    Access:JOBSWARR.Clearkey(jow:RepairedRRCAccManKey)
    jow:BranchID = tmp:TradeID
    jow:RepairedAT = 'RRC'
    jow:RRCStatus = 'APP'
    jow:Manufacturer = func:Manufacturer
    jow:DateAccepted = tmp:AcceptedStartDate
    Set(jow:RepairedRRCAccManKey,jow:RepairedRRCAccManKey)
    Loop ! Begin Loop
        If Access:JOBSWARR.Next()
            Break
        End ! If Access:JOBSWARR.Next()
        If jow:BranchID <> tmp:TradeID Or |
            jow:RepairedAT <> 'RRC' Or |
            jow:RRCStatus <> 'APP' Or |
            jow:Manufacturer <> func:Manufacturer
            Break
        End ! jow:Manufacturer = func:Manufacturer
        If jow:DateAccepted > tmp:AcceptedEndDate
            Break
        End ! If jow:DateAccepted > tmp:AcceptedEndDate

        Count# += 1
    End ! Loop
    Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)


    Do Prog:ProgressSetup
    Prog:SkipNumber = 100
    Prog:TotalRecords = Count#
    Prog:ShowPercentage = 1 !Show Percentage Figure
    Prog:UserString = 'Searching for records...'

    Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
    Access:JOBSWARR.Clearkey(jow:RepairedRRCAccManKey)
    jow:BranchID = tmp:TradeID
    jow:RepairedAt = 'RRC'
    jow:RRCStatus = 'APP'
    jow:Manufacturer = func:Manufacturer
    jow:DateAccepted = tmp:ACceptedStartDate
    Set(jow:RepairedRRCAccManKey,jow:RepairedRRCAccManKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                !Inside Loop
                If Access:JOBSWARR.Next()
                    Prog:Exit = 1
                    Break
                End ! If Access:JOBSWARR.Next()
                If jow:BranchID <> tmp:TradeID
                    Prog:Exit = 1
                    Break
                End ! If jow:BranchID <> tmp:TradeID
                If jow:Repairedat <> 'RRC'
                    Prog:Exit = 1
                    Break
                End ! If jow:Repairat <> 'RRC'
                If jow:RRCStatus <> 'APP'
                    Prog:Exit = 1
                    Break
                End ! If jow:RRCStatus <> 'APP'
                If jow:Manufacturer <> func:Manufacturer
                    Prog:exit = 1
                    Break
                End ! If jow:Manufacturer <> func:Manufacturer
                If jow:DateAccepted > tmp:AcceptedEndDate
                    Prog:Exit = 1
                    Break
                End ! If jow:DateAccepted > tmp:AcceptedEndDate

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = jow:RefNumber
                If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                tmp:Claims += 1

                !Found
                tmp:Labour += jobe:RRCWLabourCost
                tmp:Parts   += jobe:RRCWPartsCost
                tmp:Total += jobe:RRCWLabourCost + jobe:RRCWPartsCost

                Case wob:ReconciledMarker
                    Of 1
                        tmp:MarkedGreen  += jobe:RRCWLabourCost+ jobe:RRCWPartsCost
                    Of 2
                        tmp:MarkedBlue += jobe:RRCWLabourCost+ jobe:RRCWPartsCost
                    Of 3
                        tmp:MarkedRed   += jobe:RRCWLabourCost+ jobe:RRCWPartsCost
                End !Case wob:ReconciledMarker

                Do Prog:UpdateScreen
                Prog:RecordCount += 1
                ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                End ! Loop 25 Times
            Of Event:CloseWindow
    !            Prog:Exit = 1
    !            Prog:Cancelled = 1
    !            Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)

    tmp:UnderPaid = tmp:Total - tmp:MarkedBlue - tmp:MarkedGreen - tmp:MarkedRed
RefreshDate     ROutine
    If tmp:FilterByDate
        Case tmp:LimitMonth
        Of '01 January'
            tmp:BrowseStartDate = Date(1,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(1,31,tmp:LimitYear)
        Of '02 February'
            tmp:BrowseStartDate = Date(2,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(2,28,tmp:LimitYear)
        Of '03 March'
            tmp:BrowseStartDate = Date(3,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(3,31,tmp:LimitYear)
        Of '04 April'
            tmp:BrowseStartDate = Date(4,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(4,30,tmp:LimitYear)
        Of '05 May'
            tmp:BrowseStartDate = Date(5,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(5,31,tmp:LimitYear)
        Of '06 June'
            tmp:BrowseStartDate = Date(6,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(6,30,tmp:LimitYear)
        Of '07 July'
            tmp:BrowseStartDate = Date(7,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(7,31,tmp:LimitYear)
        Of '08 August'
            tmp:BrowseStartDate = Date(8,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(8,31,tmp:LimitYear)
        Of '09 September'
            tmp:BrowseStartDate = Date(9,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(9,30,tmp:LimitYear)
        Of '10 October'
            tmp:BrowseStartDate = Date(10,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(10,31,tmp:LimitYear)
        Of '11 November'
            tmp:BrowseStartDate = Date(11,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(11,30,tmp:LimitYear)
        Of '12 December'
            tmp:BrowseStartDate = Date(12,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(12,31,tmp:LimitYear)
        End ! Case tmp:LimitMonth
    Else
        tmp:BrowseStartDate = Date(1,1,2000)
        tmp:BrowseEndDate = Today()
    End ! If tmp:LimitByDate
    Brw6.ResetSort(1)
TagGreen    Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = jow:RefNumber
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
        If wob:ReconciledMarker = 1
            glo:Queue.Pointer = jow:RefNumber
            ADD(glo:Queue,glo:Queue.Pointer)
        End !If jobe:PendingClaimColour = 1
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)

  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))

TagBlue    Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = jow:RefNumber
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
        If wob:ReconciledMarker = 2
            glo:Queue.Pointer = jow:RefNumber
            ADD(glo:Queue,glo:Queue.Pointer)
        End !If jobe:PendingClaimColour = 1
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
  END
  SETCURSOR
  brw6.ResetSort(1)
  SELECT(?List,CHOICE(?List))

TagRed    Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  brw6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(brw6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = jow:RefNumber
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
        If wob:ReconciledMarker = 3
            glo:Queue.Pointer = jow:RefNumber
            ADD(glo:Queue,glo:Queue.Pointer)
        End !If jobe:PendingClaimColour = 1
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
  END
  SETCURSOR
  brw6.ResetSort(1)
  SELECT(?List,CHOICE(?List))

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020513'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReconcileWebBatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EDIBATCH.Open
  Relate:INVOICE_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  tmp:Manufacturer    = func:Manufacturer
  tmp:AcceptedStartDate = f:StartDate
  tmp:AcceptedEndDate = f:EndDate
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = Clarionet:Global.Param2
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:TradeID = tra:BranchIdentification
      Access:VATCODE.ClearKey(vat:Vat_code_Key)
      vat:VAT_Code = tra:Labour_VAT_Code
      If Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign
          !Found
          tmp:LabourVat   = vat:Vat_Rate
      Else!If Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign
  
      Access:VATCODE.ClearKey(vat:Vat_code_Key)
      vat:VAT_Code = tra:Parts_VAT_Code
      If Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign
          !Found
          tmp:PartsVat   = vat:Vat_Rate
      Else!If Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:VATCODE.TryFetch(vat:Vat_code_Key) = Level:Benign
  
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  Case Month(Today())
  Of 1
      tmp:LimitMonth = '01 January'
  Of 2
      tmp:LimitMonth = '02 February'
  Of 3
      tmp:LimitMonth = '03 March'
  Of 4
      tmp:LimitMonth = '04 April'
  Of 5
      tmp:LimitMonth = '05 May'
  Of 6
      tmp:LimitMonth = '06 June'
  Of 7
      tmp:LimitMonth = '07 July'
  Of 8
      tmp:LimitMonth = '08 August'
  Of 9
      tmp:LimitMonth = '09 September'
  Of 10
      tmp:LimitMonth = '10 October'
  Of 11
      tmp:LimitMonth = '11 November'
  Of 12
      tmp:LimitMonth = '12 December'
  End ! Case Month(Today())
  Loop x# = 2000 To Year(Today())
      year:TheYear = x#
      Add(YearQueue)
  End ! Loop x# = 2000 To Year(Today())
  tmp:LimitYear = Year(Today())
  
  tmp:BrowseStartDate = Date(1,1,2000)
  tmp:BrowseEndDate = Today()
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:JOBSWARR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','ReconcileWebBatch')
  ?List{prop:vcr} = TRUE
  ?tmp:LimitMonth{prop:vcr} = TRUE
  ?tmp:LimitYear{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:LimitMonth{prop:vcr} = False
  ?tmp:LimitYear{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Do RefreshBatchValue
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,jow:RRCStatusManKey)
  BRW6.AddRange(jow:Manufacturer)
  BRW6.AddLocator(BRW6::Sort1:Locator)
  BRW6::Sort1:Locator.Init(?jow:RefNumber:2,jow:RefNumber,1,BRW6)
  BRW6.AddSortOrder(,jow:RRCReconciledManKey)
  BRW6.AddRange(jow:RRCDateReconciled,tmp:BrowseStartDate,tmp:BrowseEndDate)
  BRW6.AddLocator(BRW6::Sort2:Locator)
  BRW6::Sort2:Locator.Init(,jow:RRCDateReconciled,1,BRW6)
  BRW6.AddSortOrder(,jow:RepairedRRCAccManKey)
  BRW6.AddRange(jow:DateAccepted,tmp:AcceptedStartDate,tmp:AcceptedEndDate)
  BIND('tmp:Tag',tmp:Tag)
  BIND('tmp:JobNumber',tmp:JobNumber)
  BIND('tmp:ClaimValue',tmp:ClaimValue)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:Tag,BRW6.Q.tmp:Tag)
  BRW6.AddField(wob:RefNumber,BRW6.Q.wob:RefNumber)
  BRW6.AddField(tmp:JobNumber,BRW6.Q.tmp:JobNumber)
  BRW6.AddField(job:Model_Number,BRW6.Q.job:Model_Number)
  BRW6.AddField(job:Warranty_Charge_Type,BRW6.Q.job:Warranty_Charge_Type)
  BRW6.AddField(job:Repair_Type_Warranty,BRW6.Q.job:Repair_Type_Warranty)
  BRW6.AddField(tmp:ClaimValue,BRW6.Q.tmp:ClaimValue)
  BRW6.AddField(wob:ReconciledMarker,BRW6.Q.wob:ReconciledMarker)
  BRW6.AddField(jow:DateAccepted,BRW6.Q.jow:DateAccepted)
  BRW6.AddField(jow:RRCDateReconciled,BRW6.Q.jow:RRCDateReconciled)
  BRW6.AddField(wob:RRCWInvoiceNumber,BRW6.Q.wob:RRCWInvoiceNumber)
  BRW6.AddField(jow:RecordNumber,BRW6.Q.jow:RecordNumber)
  BRW6.AddField(jow:BranchID,BRW6.Q.jow:BranchID)
  BRW6.AddField(jow:RepairedAt,BRW6.Q.jow:RepairedAt)
  BRW6.AddField(jow:RRCStatus,BRW6.Q.jow:RRCStatus)
  BRW6.AddField(jow:Manufacturer,BRW6.Q.jow:Manufacturer)
  BRW6.AddField(jow:RefNumber,BRW6.Q.jow:RefNumber)
  IF ?tmp:FilterByDate{Prop:Checked} = True
    HIDE(?jow:RefNumber:2)
    ENABLE(?Group:Date)
  END
  IF ?tmp:FilterByDate{Prop:Checked} = False
    UNHIDE(?jow:RefNumber:2)
    DISABLE(?Group:Date)
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Awaiting Processing'
    ?Tab3{PROP:TEXT} = 'Reconciled'
    ?List{PROP:FORMAT} ='11L(2)F*I@s1@#1#0L(2)|FM*~Job No~@s8@#7#61L(2)|FM*~Job No~@s20@#12#84L(2)|M*~Model Number~@s30@#17#110L(2)|M*~Charge Type~@s30@#22#108L(2)|M*~Repair Type~@s30@#27#48R(2)|M*~Claim Value~@n14.2@#32#52R(2)|M*~Accepted~@d6@#42#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EDIBATCH.Close
    Relate:INVOICE_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReconcileWebBatch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?SearchButton
      BRW6.ResetSort(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020513'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020513'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020513'&'0')
      ***
    OF ?buttonImportReconciled
      ThisWindow.Update
      Do ImportReconciled
    OF ?Button:ViewJob
      ThisWindow.Update
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw6.q.wob:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      !reset the search
      LocaljobNo = 0
      BRW6.ResetSort(1)
      
      
    OF ?OK
      ThisWindow.Update
          If ~Records(glo:Queue)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You have not tagged any jobs.','ServiceBase 3g',|
                             'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
              Cycle
          End ! If ~Records(glo:Queue)
      
          Case Missive('Are you sure you want to invoice the tagged jobs?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  tmp:CountJobs = 0
                  !Create Invoice First
                  If Access:Invoice.PrimeRecord() = Level:Benign
                      tmp:InvoiceNumber   = inv:Invoice_Number
                      If Access:Invoice.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:Invoice.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:Invoice.CancelAutoInc()
                      End !If Access:Invoice.TryInsert() = Level:Benign
                  End !If Access:Invoice.PrimeRecord() = Level:Benign
      
                  Loop x# = 1 To Records(glo:Queue)
                      Get(glo:Queue,x#)
      
                      Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                      jow:RefNumber = glo:Pointer
                      If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                          !Found
      
                          Access:WEBJOB.ClearKey(wob:RefNumberKey)
                          wob:RefNumber = jow:RefNumber
                          If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                              !Found
                          Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                              !Error
                              Cycle
                          End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = wob:RefNumber
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
      
                              ! Insert --- Need to update jobs, so check it isn't in use (DBH: 31/03/2009) #10757
                              if (JobInUse(job:Ref_Number,1))
                                  cycle
                              end ! if (JobInUse(job:Ref_Number,1))
                              ! end --- (DBH: 31/03/2009) #10757
      
                              Access:JOBSE.Clearkey(jobe:RefNumberKey)
                              jobe:RefNumber  = job:Ref_Number
                              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  !Found
                                  IF (SentToHub(job:Ref_Number))
                                      ! #13614 Hub Repair Date is not reliable. Use the proper routine. (DBH: 12/10/2015)
                                      CYCLE
                                  END ! IF
!                                  If jobe:HubRepairDate <> ''
!                                      Cycle
!                                  End !If jobe:HubRepairDate <> ''
      
                                  jobe:InvRRCWLabourCost = jobe:RRCWLabourCost
                                  jobe:InvRRCWPartsCost   = jobe:RRCWPartsCost
                                  jobe:InvRRCWSubTotal    = jobe:RRCWLabourCost + jobe:RRCWPartsCost
                                  tmp:CountJobs += 1
                                  tmp:TotalLabour     += jobe:RRCWLabourCost
                                  tmp:TotalParts      += jobe:RRCWPartsCost
                                  Access:JOBSE.Update()
                              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  !Error
                              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                              wob:EDI = 'PAY'
                              wob:RRCWInvoiceNumber = tmp:InvoiceNumber
                              wob:ReconciledMarker = 0
      
                              If Access:WEBJOB.TryUpdate() = Level:Benign
                                  jow:RRCStatus = 'PAY'
                                  jow:RRCDateReconciled = Today()
                                  Access:JOBSWARR.TryUpdate()
                                  GetStatus(910,0,'JOB') !Despatch Paid
                                  ! Insert --- Save the job (DBH: 31/03/2009) #10757
                                  Access:JOBS.TryUpdate()
                                  ! end --- (DBH: 31/03/2009) #10757
      
                                  locAuditNotes         = 'BATCH NO: ' & Clip(job:EDI_Batch_Number) & |
                                                      '<13,10>WARRANTY CLAIM PAYMENT ACKNOWLEDGED<13,10>EXCH/HANDLING: '
                                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                  jobe:RefNumber  = wob:RefNumber
                                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Found
                                      If job:Exchange_Unit_Number <> ''
                                          locAuditNotes   = Clip(locAuditNotes) & Format(jobe:ExchangeRate,@n10.2)
                                      Else !If job:Exchange_Unit_Number <> ''
                                          locAuditNotes   = Clip(locAuditNotes) & Format(jobe:HandlingFee,@n10.2)
                                      End !If job:Exchange_Unit_Number <> ''
                                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                                  locAuditNotes  = Clip(locAuditNotes) & '<13,10>LABOUR: ' & Format(jobe:RRCWLabourCost,@n10.2) & |
                                                  '<13,10>PARTS: ' & Format(jobe:RRCWPartsCost,@n10.2)
      
                                  IF (AddToAudit(wob:refnumber,'JOB','WARRANTY CLAIM MARKED PAID (RRC) INV NO: ' & tmp:InvoiceNumber,locAuditNotes))
                                  END ! IF
      
                              End !If Access:WEBJOB.TryUpdate() = Level:Benign
      
                          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                      Else !If Access:WEBJOB.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:WEBJOB.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                  End !Loop x# = 1 To Records(glo:Queue)
      
                  Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                  inv:Invoice_Number  = tmp:InvoiceNumber
                  If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                      If tmp:CountJobs = 0
                          Relate:INVOICE.Delete(0)
                      Else !If tmp:TotalJobs = 0
                          inv:Date_Created        = Today()
                          inv:account_number     = Clarionet:Global.Param2
                          inv:accounttype        = 'MAI'
                          inv:total              = tmp:TotalLabour + tmp:TotalParts
                          inv:vat_rate_labour    = tmp:LabourVAT
                          inv:vat_rate_parts     = tmp:PartsVAT
                          inv:vat_number         = def:vat_number
                          inv:batch_number       = 0
                          inv:manufacturer       = func:Manufacturer
                          inv:total_claimed      = tmp:TotalLabour + tmp:TotalParts
                          inv:labour_paid        = tmp:totalLabour
                          inv:parts_paid         = tmp:totalParts
                          inv:reconciled_date    = Today()
                          inv:invoice_type       = 'WRC'
      ! Changing (DBH 30/06/2008) # 9792 - Just show the jobs counted. There is no "total on batch"anymore
      !                    inv:jobs_count         = tmp:Claims
      ! to (DBH 30/06/2008) # 9792
                          inv:Jobs_Count          = tmp:CountJobs
      ! End (DBH 30/06/2008) #9792
                          Access:INVOICE.Update()
      
                          RRCWarrantyInvoice(tmp:InvoiceNumber)
                      End !If tmp:TotalJobs = 0
                      !Found
                  Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                      !Error
                  End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
      
      
      
                  Post(Event:CloseWindow)
              Of 1 ! No Button
          End ! Case Missive
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?MarkRed
      ThisWindow.Update
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = brw6.q.wob:RefNumber
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Found
          If wob:ReconciledMarker = 3
              wob:ReconciledMarker = 0
          Else !If wob:ReconciledMarker
              wob:ReconciledMarker = 3
          End !If wob:ReconciledMarker
          Access:WEBJOB.Update()
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Error
      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      BRW6.ResetSort(1)
    OF ?MarkGreen
      ThisWindow.Update
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = brw6.q.wob:RefNumber
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Found
          If wob:ReconciledMarker = 1
              wob:ReconciledMarker = 0
          Else !If wob:ReconciledMarker
              wob:ReconciledMarker = 1
          End !If wob:ReconciledMarker
          Access:WEBJOB.Update()
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Error
      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      BRW6.ResetSort(1)
    OF ?MarkBlue
      ThisWindow.Update
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = brw6.q.wob:RefNumber
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Found
          If wob:ReconciledMarker = 2
              wob:ReconciledMarker = 0
          Else !If wob:ReconciledMarker
              wob:ReconciledMarker = 2
          End !If wob:ReconciledMarker
          Access:WEBJOB.Update()
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          !Error
      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      BRW6.ResetSort(1)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?RefresBatchValue
      ThisWindow.Update
      Do RefreshBatchValue
      BRW6.ResetSort(1)
    OF ?TagGreen
      ThisWindow.Update
      Do TagGreen
    OF ?TagBlue
      ThisWindow.Update
      Do TagBlue
    OF ?tagRed
      ThisWindow.Update
      Do TagRed
      
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:LimitMonth
      Do RefreshDate
    OF ?tmp:LimitYear
      Do RefreshDate
    OF ?ReprintInvoice
      ThisWindow.Update
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Do you want to reprint the invoice containing the SELECTED job, or print all invoices created on the same DATE as the selected job?','ServiceBase 3g',|
                     'mquest.jpg','&Cancel|\&Date|/&Selected')
          Of 3 ! &Selected Button
              RRCWarrantyInvoice(brw6.q.wob:RRCWInvoiceNumber)
          Of 2 ! &Date Button
              Access:INVOICE_ALIAS.ClearKey(inv_ali:Invoice_Number_Key)
              inv_ali:Invoice_Number = brw6.q.wob:RRCWInvoiceNumber
              If Access:INVOICE_ALIAS.TryFetch(inv_ali:Invoice_Number_Key) = Level:Benign
                  !Found
                  InvoiceDate# = inv_ali:Reconciled_Date
                  Access:INVOICE_ALIAS.Clearkey(inv_ali:AccountReconciledKey)
                  inv_ali:Account_Number = Clarionet:Global.Param2
                  inv_ali:Reconciled_Date = InvoiceDate#
                  Set(inv_ali:AccountReconciledKey,inv_ali:AccountReconciledKey)
                  Loop ! Begin Loop
                      If Access:INVOICE_ALIAS.Next()
                          Break
                      End ! If Access:INVOICE.Next()
                      If inv_ali:Account_Number <> Clarionet:Global.Param2
                          Break
                      End ! If inv:Account_Number <> Clarionet:Global.Param2
                      If inv_ali:Reconciled_Date <> InvoiceDate#
                          Break
                      End ! If inv:ReconciledDate <> InvoiceDate#
                      If inv_ali:Invoice_Type <> 'WRC'
                          Cycle
                      End ! If inv:Invoice_Type <> 'WRC'
                      RRCWarrantyInvoice(inv_ali:Invoice_Number)
                  End ! Loop
              Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                  !Error
              End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
      
          Of 1 ! &Cancel Button
      End!Case Message
      
    OF ?tmp:FilterByDate
      IF ?tmp:FilterByDate{Prop:Checked} = True
        HIDE(?jow:RefNumber:2)
        ENABLE(?Group:Date)
      END
      IF ?tmp:FilterByDate{Prop:Checked} = False
        UNHIDE(?jow:RefNumber:2)
        DISABLE(?Group:Date)
      END
      ThisWindow.Reset
      Do RefreshDate
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End !KeyCode() = MouseLeft2
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Sheet1
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet1)
        OF 1
          ?List{PROP:FORMAT} ='11L(2)F*I@s1@#1#0L(2)|FM*~Job No~@s8@#7#61L(2)|FM*~Job No~@s20@#12#84L(2)|M*~Model Number~@s30@#17#110L(2)|M*~Charge Type~@s30@#22#108L(2)|M*~Repair Type~@s30@#27#48R(2)|M*~Claim Value~@n14.2@#32#52R(2)|M*~Accepted~@d6@#42#'
          ?Tab1{PROP:TEXT} = 'Awaiting Processing'
        OF 2
          ?List{PROP:FORMAT} ='11L(2)F*I@s1@#1#0L(2)|FM*~Job No~@s8@#7#61L(2)|FM*~Job No~@s20@#12#84L(2)|M*~Model Number~@s30@#17#110L(2)|M*~Charge Type~@s30@#22#108L(2)|M*~Repair Type~@s30@#27#56L(2)|M*~Invoice No~@s8@#52#60L(2)|M*~Date Reconciled~@d6@#47#52R(2)|M*~Accepted~@d6@#42#'
          ?Tab3{PROP:TEXT} = 'Reconciled'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:LimitMonth{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:LimitMonth{prop:Use} = DBHControl{prop:Use}
        IF ?tmp:LimitYear{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:LimitYear{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW6.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 2 And tmp:FilterByDate = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:TradeID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'RRC'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = 'PAY'
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = func:Manufacturer
  ELSIF Choice(?Sheet1) = 2 And tmp:FilterByDate = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:TradeID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'RRC'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = 'PAY'
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = func:Manufacturer
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:TradeId
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'RRC'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = 'APP'
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW6.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 2 And tmp:FilterByDate = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet1) = 2 And tmp:FilterByDate = 1
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jow:RefNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = jow:RefNumber
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = jow:RefNumber
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = jow:RefNumber
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  tmp:JobNumber   = Clip(wob:RefNumber) & '-' & Clip(tmp:TradeID) & Clip(wob:JobNumber)
  tmp:ClaimValue  = jobe:RRCWLabourCost + jobe:RRCWPartsCost
  PARENT.SetQueueRecord
  SELF.Q.tmp:Tag_NormalFG = -1
  SELF.Q.tmp:Tag_NormalBG = -1
  SELF.Q.tmp:Tag_SelectedFG = -1
  SELF.Q.tmp:Tag_SelectedBG = -1
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END
  IF (wob:ReconciledMarker = 1)
    SELF.Q.wob:RefNumber_NormalFG = 32768
    SELF.Q.wob:RefNumber_NormalBG = 16777215
    SELF.Q.wob:RefNumber_SelectedFG = 16777215
    SELF.Q.wob:RefNumber_SelectedBG = 32768
  ELSE
    SELF.Q.wob:RefNumber_NormalFG = -1
    SELF.Q.wob:RefNumber_NormalBG = -1
    SELF.Q.wob:RefNumber_SelectedFG = -1
    SELF.Q.wob:RefNumber_SelectedBG = -1
  END
  SELF.Q.tmp:JobNumber_NormalFG = -1
  SELF.Q.tmp:JobNumber_NormalBG = -1
  SELF.Q.tmp:JobNumber_SelectedFG = -1
  SELF.Q.tmp:JobNumber_SelectedBG = -1
  SELF.Q.job:Model_Number_NormalFG = -1
  SELF.Q.job:Model_Number_NormalBG = -1
  SELF.Q.job:Model_Number_SelectedFG = -1
  SELF.Q.job:Model_Number_SelectedBG = -1
  SELF.Q.job:Warranty_Charge_Type_NormalFG = -1
  SELF.Q.job:Warranty_Charge_Type_NormalBG = -1
  SELF.Q.job:Warranty_Charge_Type_SelectedFG = -1
  SELF.Q.job:Warranty_Charge_Type_SelectedBG = -1
  SELF.Q.job:Repair_Type_Warranty_NormalFG = -1
  SELF.Q.job:Repair_Type_Warranty_NormalBG = -1
  SELF.Q.job:Repair_Type_Warranty_SelectedFG = -1
  SELF.Q.job:Repair_Type_Warranty_SelectedBG = -1
  SELF.Q.tmp:ClaimValue_NormalFG = -1
  SELF.Q.tmp:ClaimValue_NormalBG = -1
  SELF.Q.tmp:ClaimValue_SelectedFG = -1
  SELF.Q.tmp:ClaimValue_SelectedBG = -1
  SELF.Q.wob:ReconciledMarker_NormalFG = -1
  SELF.Q.wob:ReconciledMarker_NormalBG = -1
  SELF.Q.wob:ReconciledMarker_SelectedFG = -1
  SELF.Q.wob:ReconciledMarker_SelectedBG = -1
  SELF.Q.jow:DateAccepted_NormalFG = -1
  SELF.Q.jow:DateAccepted_NormalBG = -1
  SELF.Q.jow:DateAccepted_SelectedFG = -1
  SELF.Q.jow:DateAccepted_SelectedBG = -1
  SELF.Q.jow:RRCDateReconciled_NormalFG = -1
  SELF.Q.jow:RRCDateReconciled_NormalBG = -1
  SELF.Q.jow:RRCDateReconciled_SelectedFG = -1
  SELF.Q.jow:RRCDateReconciled_SelectedBG = -1
  SELF.Q.wob:RRCWInvoiceNumber_NormalFG = -1
  SELF.Q.wob:RRCWInvoiceNumber_NormalBG = -1
  SELF.Q.wob:RRCWInvoiceNumber_SelectedFG = -1
  SELF.Q.wob:RRCWInvoiceNumber_SelectedBG = -1
   
   
   IF (wob:ReconciledMarker = 1)
     SELF.Q.tmp:Tag_NormalFG = 32768
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.tmp:Tag_NormalFG = 16711680
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.tmp:Tag_NormalFG = 255
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Tag_NormalFG = 0
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.wob:RefNumber_NormalFG = 32768
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.wob:RefNumber_NormalFG = 16711680
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.wob:RefNumber_NormalFG = 255
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 255
   ELSE
     SELF.Q.wob:RefNumber_NormalFG = 0
     SELF.Q.wob:RefNumber_NormalBG = 16777215
     SELF.Q.wob:RefNumber_SelectedFG = 16777215
     SELF.Q.wob:RefNumber_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.tmp:JobNumber_NormalFG = 32768
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.tmp:JobNumber_NormalFG = 16711680
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.tmp:JobNumber_NormalFG = 255
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 255
   ELSE
     SELF.Q.tmp:JobNumber_NormalFG = 0
     SELF.Q.tmp:JobNumber_NormalBG = 16777215
     SELF.Q.tmp:JobNumber_SelectedFG = 16777215
     SELF.Q.tmp:JobNumber_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.job:Model_Number_NormalFG = 32768
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.job:Model_Number_NormalFG = 16711680
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.job:Model_Number_NormalFG = 255
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Model_Number_NormalFG = 0
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 32768
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 16711680
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 255
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 255
   ELSE
     SELF.Q.job:Warranty_Charge_Type_NormalFG = 0
     SELF.Q.job:Warranty_Charge_Type_NormalBG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Warranty_Charge_Type_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 32768
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 16711680
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 255
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 255
   ELSE
     SELF.Q.job:Repair_Type_Warranty_NormalFG = 0
     SELF.Q.job:Repair_Type_Warranty_NormalBG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_Warranty_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.tmp:ClaimValue_NormalFG = 32768
     SELF.Q.tmp:ClaimValue_NormalBG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedFG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.tmp:ClaimValue_NormalFG = 16711680
     SELF.Q.tmp:ClaimValue_NormalBG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedFG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.tmp:ClaimValue_NormalFG = 255
     SELF.Q.tmp:ClaimValue_NormalBG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedFG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ClaimValue_NormalFG = 0
     SELF.Q.tmp:ClaimValue_NormalBG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedFG = 16777215
     SELF.Q.tmp:ClaimValue_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.wob:ReconciledMarker_NormalFG = 32768
     SELF.Q.wob:ReconciledMarker_NormalBG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedFG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.wob:ReconciledMarker_NormalFG = 16711680
     SELF.Q.wob:ReconciledMarker_NormalBG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedFG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.wob:ReconciledMarker_NormalFG = 255
     SELF.Q.wob:ReconciledMarker_NormalBG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedFG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedBG = 255
   ELSE
     SELF.Q.wob:ReconciledMarker_NormalFG = 0
     SELF.Q.wob:ReconciledMarker_NormalBG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedFG = 16777215
     SELF.Q.wob:ReconciledMarker_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.jow:DateAccepted_NormalFG = 32768
     SELF.Q.jow:DateAccepted_NormalBG = 16777215
     SELF.Q.jow:DateAccepted_SelectedFG = 16777215
     SELF.Q.jow:DateAccepted_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.jow:DateAccepted_NormalFG = 16711680
     SELF.Q.jow:DateAccepted_NormalBG = 16777215
     SELF.Q.jow:DateAccepted_SelectedFG = 16777215
     SELF.Q.jow:DateAccepted_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.jow:DateAccepted_NormalFG = 255
     SELF.Q.jow:DateAccepted_NormalBG = 16777215
     SELF.Q.jow:DateAccepted_SelectedFG = 16777215
     SELF.Q.jow:DateAccepted_SelectedBG = 255
   ELSE
     SELF.Q.jow:DateAccepted_NormalFG = 0
     SELF.Q.jow:DateAccepted_NormalBG = 16777215
     SELF.Q.jow:DateAccepted_SelectedFG = 16777215
     SELF.Q.jow:DateAccepted_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.jow:RRCDateReconciled_NormalFG = 32768
     SELF.Q.jow:RRCDateReconciled_NormalBG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedFG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.jow:RRCDateReconciled_NormalFG = 16711680
     SELF.Q.jow:RRCDateReconciled_NormalBG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedFG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.jow:RRCDateReconciled_NormalFG = 255
     SELF.Q.jow:RRCDateReconciled_NormalBG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedFG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedBG = 255
   ELSE
     SELF.Q.jow:RRCDateReconciled_NormalFG = 0
     SELF.Q.jow:RRCDateReconciled_NormalBG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedFG = 16777215
     SELF.Q.jow:RRCDateReconciled_SelectedBG = 0
   END
   IF (wob:ReconciledMarker = 1)
     SELF.Q.wob:RRCWInvoiceNumber_NormalFG = 32768
     SELF.Q.wob:RRCWInvoiceNumber_NormalBG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedFG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedBG = 32768
   ELSIF(wob:ReconciledMarker = 2)
     SELF.Q.wob:RRCWInvoiceNumber_NormalFG = 16711680
     SELF.Q.wob:RRCWInvoiceNumber_NormalBG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedFG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedBG = 16711680
   ELSIF(wob:ReconciledMarker = 3)
     SELF.Q.wob:RRCWInvoiceNumber_NormalFG = 255
     SELF.Q.wob:RRCWInvoiceNumber_NormalBG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedFG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedBG = 255
   ELSE
     SELF.Q.wob:RRCWInvoiceNumber_NormalFG = 0
     SELF.Q.wob:RRCWInvoiceNumber_NormalBG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedFG = 16777215
     SELF.Q.wob:RRCWInvoiceNumber_SelectedBG = 0
   END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  !see if we can select the record number manually   - added by Paul 19/03/2010  log no 11307
  If Choice(?Sheet1) = 1 then
      !ok we are on the first tab
      If LocaljobNo <> 0 then
          If LocalJobNo <> jow:RefNumber then
              return record:filtered
          End
      End
  End
  ReturnValue = PARENT.ValidateRecord()
!  Access:JOBS.Clearkey(job:Ref_Number_Key)
!  job:Ref_Number  = wob:RefNumber
!  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!      !Found
!      If job:EDI_Batch_Number <> func:BatchNumber
!          Return Record:Filtered
!      End !If job:EDI_Batch_Number <> tmp:BatchNumber
!
!      Access:JOBSE.Clearkey(jobe:RefNumberKey)
!      jobe:RefNumber  = job:Ref_Number
!      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!          !Found
!          if jobe:HubRepairDate <> ''
!              Return Record:Filtered
!          End !if jobe:HubRepairDate <> ''
!
!      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!          !Error
!      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!
!  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!      !Error
!  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jow:RefNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

Prog2.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog2.ProgressSetup(func:Records)
Prog2.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog2.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog2:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog2:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog2.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog2.ResetProgress      Procedure(Long func:Records)
CODE

    Prog2.recordsToProcess = func:Records
    Prog2.recordsprocessed = 0
    Prog2.percentProgress = 0
    Prog2.progressThermometer = 0
    Prog2.CNprogressThermometer = 0
    Prog2.skipRecords = 0
    Prog2.userText = ''
    Prog2.CNuserText = ''
    Prog2.percentText = '0% Completed'
    Prog2.CNpercentText = Prog2.percentText


Prog2.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog2.InsideLoop(func:String))
Prog2.InsideLoop     Procedure(<String func:String>)
CODE

    Prog2.SkipRecords += 1
    If Prog2.SkipRecords < 50
        Prog2.RecordsProcessed += 1
        Return 0
    Else
        Prog2.SkipRecords = 0
    End
    if (func:String <> '')
        Prog2.UserText = Clip(func:String)
        Prog2.CNUserText = Prog2.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog2.NextRecord()
        ClarioNet:UpdatePushWindow(Prog2:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog2.NextRecord()
        if (Prog2.CancelLoop())
            return 1
        end ! if (Prog2.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog2.ProgressText        Procedure(String    func:String)
CODE

    Prog2.UserText = Clip(func:String)
    Prog2.CNUserText = Prog2.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog2:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog2.Kill     Procedure()
    CODE
        Prog2.ProgressFinish()
Prog2.ProgressFinish     Procedure()
CODE

    Prog2.ProgressThermometer = 100
    Prog2.CNProgressThermometer = 100
    Prog2.PercentText = '100% Completed'
    Prog2.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog2:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog2:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog2:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog2.NextRecord      Procedure()
CODE
    Yield()
    Prog2.RecordsProcessed += 1
    !If Prog2.percentprogress < 100
        Prog2.percentprogress = (Prog2.recordsprocessed / Prog2.recordstoprocess)*100
        If Prog2.percentprogress > 100 or Prog2.percentProgress < 0
            Prog2.percentprogress = 0
        End
        If Prog2.percentprogress <> Prog2.ProgressThermometer then
            Prog2.ProgressThermometer = Prog2.percentprogress
            Prog2.PercentText = format(Prog2:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog2.CNPercentText = Prog2.PercentText
    Prog2.CNProgressThermometer = Prog2.ProgressThermometer
    Display()

Prog2.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog2:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Browse_Wobs PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:failed           BYTE
LocalTradeAcc        STRING(30)
Display_Address      STRING(200)
JobCount             LONG,DIM(10)
TotaljobCount        LONG
tmp:Company_Name     STRING(30)
tmp:WarManufacturer  STRING(30)
Tmp:UseWarMan        BYTE
tmp:Type             STRING(1)
tmp:GlobalIMEINumber STRING(20)
tmp:Current_status   STRING(33)
tmp:Current_StatusBase STRING(30)
tmp:Loan_status      STRING(30)
tmp:Exchange_Status  STRING(30)
tmp:StatusDate       DATE
tmp:ExchangeStatusDate DATE
tmp:LoanStatusDate   DATE
tmp:wob_jobNumber    STRING(20)
tmp:tradeID          STRING(2)
Save_Aus_id          USHORT
save_wob_id          USHORT,AUTO
Save_JAC_ID          USHORT
Save_LAC_ID          USHORT
Save_job_id          USHORT
Save_mulj_ID         USHORT
Save_job_ali_id      USHORT
Save_Jot_Id          USHORT
Save_xch_id          USHORT
tmp:printDespatchNote BYTE
tmp:firstrecord      BYTE
tmp:batchnumber      LONG
tmp:BatchCreated     BYTE
tag_temp             STRING(1)
Select_Courier_temp  STRING(3)
Select_Trade_Account_temp STRING(3)
Courier_Temp         STRING(30)
Account_Number2_temp STRING(30)
tmp:EDI              STRING('NO {1}')
tmp:Engineers_Notes  STRING(255)
tmp:Courier          STRING(30)
tmp:SearchStatus     STRING(30)
tmp:status           STRING(30)
tmp:Location         STRING(30)
tmp:BrowseFlag       BYTE(0)
temp:Current_Status  STRING(30)
temp:Loan_Status     STRING(30)
temp:Exchange_Status STRING(30)
LocalWayBillNumber   LONG
tmp:Address          STRING(500)
address:CompanyName  STRING(30)
address:AddressLine1 STRING(30)
address:AddressLine2 STRING(30)
address:AddressLine3 STRING(30)
address:Postcode     STRING(30)
address:TelephoneNumber STRING(30)
address:FaxNumber    STRING(30)
tmp:ExchangeUnitDetails STRING(60)
tmp:LoanUnitDetails  STRING(60)
tmp:DateCompleted    DATE
LocationCountString  STRING(255)
tmp:JobStatusDays    STRING(30)
tmp:LoaStatusDays    STRING(30)
tmp:ExcStatusDays    STRING(30)
tmp:JobDays          STRING(30)
locAuditNotes        STRING(255)
QuickWindow          WINDOW('ServiceBase 2000 Webmaster'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbwall1.jpg'),CENTERED,GRAY,DOUBLE
                       MENUBAR
                         MENU('File'),USE(?File)
                           ITEM('Re-Login'),USE(?FileReLogin)
                           ITEM('Exit'),USE(?FileExit)
                         END
                         MENU('System Administration'),USE(?SystemAdmin)
                           ITEM('Trade Accounts'),USE(?SystemAdminTradeAccounts)
                           ITEM('Users'),USE(?SystemAdminUsers)
                           ITEM('WIP Report Statuses'),USE(?WIPReportStatuses)
                         END
                         MENU('Browse'),USE(?Browse)
                           ITEM('Engineers Performance'),USE(?BrowseEngineersPerformance)
                           ITEM('Exchange Units'),USE(?BrowseExchangeUnits)
                           ITEM('Loan Units'),USE(?BrowseLoanUnits)
                           ITEM('Bouncers'),USE(?BrowseBouncers)
                         END
                         MENU('Reports'),USE(?SystemAdminReports)
                           MENU('Exchange Reports'),USE(?ReportsExchangeReports)
                             ITEM('Available Exchange Units'),USE(?ReportsItem52)
                             ITEM('Exchange Device Sold Report'),USE(?ReportsExchangeReportsExchangeDeviceSoldReport)
                             ITEM('Exchange Phone Report'),USE(?ReportsExchangeReportsExchangePhoneReport)
                             ITEM('Exchange Stock Valuation'),USE(?ReportsExchangeReportsExchangeStock)
                             ITEM('Exchange Units Returned To Main Store'),USE(?ReportsExchangeReportsExchangeUnitsReturnedToMa)
                           END
                           MENU('Loan Reports'),USE(?ReportsLoanReports)
                             ITEM('Loan Device Sold Report'),USE(?ReportsLoanDeviceSoldReport)
                             ITEM('Loan Units Returned To Main Store'),USE(?ReportsLoanReportsLoanUnitsReturnedToMainSTore)
                             ITEM('Loan Phone Report'),USE(?ReportsLoanPhoneReport)
                           END
                           ITEM('Chargeable Income Report'),USE(?ReportsIncomeReport)
                           ITEM('Warranty Income Report'),USE(?ReportsWarrantyIncomeReport)
                           ITEM('Credit Report'),USE(?ReportsCreditReport)
                           ITEM('Handling && Exchange Fee Report'),USE(?ReportsHandlingExchangeFeeReport)
                           ITEM('Status Report'),USE(?ReportsStatusReport)
                           ITEM('Warranty Claim Status Report'),USE(?ReportsWarrantyClaimStatusReport),HIDE
                           ITEM('Insurance Claim Report'),USE(?ReportsInsuranceClaimReport)
                           ITEM('Warranty Parts Sent to ARC Report'),USE(?ReportsWarrantyPartsSenttoARCReport)
                           ITEM('Stock Value Report'),USE(?ReportsStockValueReport)
                           ITEM('SMS Response Report'),USE(?ReportsSMSResponseReport)
                         END
                         MENU('Rapid Procedures'),USE(?RapidProcedures)
                           ITEM('Single New Job Booking'),USE(?RapidProceduresNewJobBooking)
                           ITEM('Multiple Job Booking'),USE(?RapidProceduresMultipleJobBooking)
                           ITEM('Engineer Update'),USE(?RapidProceduresEngineerUpdate)
                           MENU('Allocations'),USE(?RapidProceduresAllocations)
                             ITEM('Job Update'),USE(?RapidProceduresAllocationsJobUpate)
                             ITEM('Exchange Update'),USE(?RapidProceduresAllocationsExchangeUpdate)
                           END
                           MENU('Handover Confirmation'),USE(?RapidProceduresHandoverConfirmation2)
                             ITEM('Job Handover Confirmation'),USE(?RapidProceduresHandOverConfirmation)
                             ITEM('Exchange Handover Confirmation'),USE(?RapidProceduresExchangeHandOverConfirmation)
                             ITEM,SEPARATOR
                             ITEM('Reprint Handover Confirmation Report'),USE(?RapidProceduresHandoverConfirmationReprintHando)
                           END
                           MENU('QA'),USE(?RapidProceduresQA2)
                             ITEM('Electronic QA'),USE(?RapidProceduresQAElectronicQA),DISABLE
                             ITEM('Manual QA'),USE(?RapidProceduresQAManualQA)
                           END
                           MENU('Waybill Procedure'),USE(?WaybillProcedure)
                             ITEM('Waybill Generation'),USE(?RapidProceduresWaybillGeneration)
                             ITEM('Waybill Confirmation'),USE(?RapidProceduresWaybillConfirmation)
                             ITEM,SEPARATOR
                             ITEM('Despatch Sundry Items'),USE(?RapidProceduresWaybillProcedureDespatchSundryIt)
                           END
                           ITEM('Exchange Audit'),USE(?RapidProceduresExchangeAudit),HIDE
                           ITEM('Loan Audit'),USE(?RapidProceduresLoanAudit),HIDE
                           ITEM('W.I.P. Audit'),USE(?RapidProceduresWIPAudit),HIDE
                           ITEM('Search Accessories'),USE(?RapidProceduresSearchAccessories)
                         END
                         MENU('Stock Control'),USE(?StockControl)
                           ITEM('Adjusted Web Order Receipts'),USE(?StockControlAdjustedWebOrderReceipts)
                           ITEM('Browse Retail Sales'),USE(?StockControlBrowseRetailSales)
                           ITEM('Browse Stock'),USE(?StockControlBrowseStock)
                           ITEM('Minimum Stock Level'),USE(?StockControlMinimumStockLevel)
                           ITEM('Outstanding Stock Orders'),USE(?StockControlOutstandingStockOrders)
                           ITEM('Pending Orders'),USE(?StockControlPendingOrders)
                           ITEM('Stock Receive Procedure'),USE(?StockControlStockReceiveProcedure)
                           ITEM('Exchange Allocation'),USE(?StockControlExchangeAllocation)
                           ITEM('Return Orders'),USE(?StockControlReturnOrders)
                           ITEM('Stock Allocation'),USE(?StockControlStockAllocation)
                           ITEM('Stock Audit'),USE(?StockControlStockAudit)
                           ITEM('Exchange Audit'),USE(?StockControlExchangeAudit)
                           ITEM('WIP Audit'),USE(?StockControlWIPAudit)
                         END
                         ITEM('Despatch'),USE(?Despatch),HIDE
                         ITEM('Warranty Claims'),USE(?WarrantyClaims)
                       END
                       STRING('SRN: 0000000'),AT(612,4),USE(?SRNNumber),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                       IMAGE('logomain.jpg'),AT(218,0),USE(?Image1),HIDE,CENTERED
                       BUTTON,AT(628,17),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpw.jpg')
                       BUTTON,AT(620,320),USE(?Close),TRN,FLAT,LEFT,ICON('exitverw.jpg')
                       BUTTON,AT(69,322),USE(?Button:CreateNewJob),TRN,FLAT,ICON('newjobw.jpg')
                       BUTTON,AT(160,322),USE(?Button:JobSearch),TRN,FLAT,ICON('jobserw.jpg')
                       BUTTON,AT(251,322),USE(?Button:SingleIMEISearch),TRN,FLAT,ICON('sinimeiw.jpg')
                       BUTTON,AT(342,322),USE(?Button:GlobalIMEISearch),TRN,FLAT,ICON('gloimeiw.jpg')
                       BUTTON,AT(433,322),USE(?Button:IMEIValidation),TRN,FLAT,ICON('validw.jpg')
                       BUTTON,AT(528,322),USE(?Button:Menu),TRN,FLAT,ICON('menuw.jpg')
                       GROUP('Rapid Procedures'),AT(63,312,548,44),USE(?Group:RapidProcedures),BOXED,FONT(,,0101010H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       END
                       IMAGE('passlong.jpg'),AT(73,368),USE(?Image2),CENTERED
                       BUTTON,AT(433,368),USE(?JobProgress),TRN,FLAT,FONT(,,,FONT:bold),ICON('jobprogw.jpg')
                       BUTTON,AT(528,368),USE(?StockControl:2),TRN,FLAT,FONT(,,,FONT:bold),ICON('stoconw.jpg')
                       GROUP('Standard Procedures'),AT(316,361,296,44),USE(?Group:StandardProcedures),BOXED,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         BUTTON,AT(342,368),USE(?ButtonBrowseVPortal),FLAT,ICON('RecVPJ.jpg')
                       END
                       PANEL,AT(81,374,128,14),USE(?Panel1),FILL(COLOR:White),BEVEL(-1,-1)
                       PROMPT('Location'),AT(85,376,120,12),USE(?Location:Prompt),CENTER,FONT(,10,010101H,FONT:bold),COLOR(COLOR:White)
                       STRING('User:'),AT(85,398,120,),USE(?User:Prompt),CENTER,FONT(,10,010101H,FONT:bold),COLOR(COLOR:White)
                       STRING('Current Version:'),AT(85,410,120,),USE(?Version:Prompt),CENTER,FONT(,7,010101H,FONT:bold),COLOR(COLOR:White)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    Map
LocalValidateAccountNumber      Procedure(),byte
LocalValidateIMEI               Procedure(),byte
LocalValidateAccessories        Procedure(),byte
    End
TempFilePath         CSTRING(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

ShowUserDetails     Routine
    ! Make a routine incase the user relog in - TrkBs: 6572 (DBH: 19-10-2005)
    ?Location:Prompt{prop:text} = Clip(glo:Location)

    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !Found
        ?User:Prompt{prop:Text} = 'User: ' & Clip(Capitalize(use:Forename)) & ' ' & Clip(Capitalize(use:surname))
    Else ! If Access:USERS.Tryfetch(use:PasswordKey) = Level:Benign
        !Error
    End ! If Access:USERS.Tryfetch(use:PasswordKey) = Level:Benign
Security_Check      Routine

    if securitycheck('RE-LOGIN') = level:benign
        enable(?FileReLogin)
    else
        disable(?FileReLogin)
    end

!!! - USER DEFAULTS - !!!
    if securitycheck('USERS - BROWSE') = level:benign
        enable(?systemAdminUsers)
    else!if securitycheck('USERS - BROWSE') = level:benign
        disable(?systemAdminUsers)
    end!if securitycheck('USERS - BROWSE') = level:benign

!!! - TRADE ACCOUNT DEFAULTS - !!!

    if securitycheck('LOAN UNITS - BROWSE') = level:benign
        enable(?BrowseLoanUnits)
    else!if securitycheck('LOAN UNITS - BROWSE') = level:benign
        disable(?BrowseLoanUnits)
    end!if securitycheck('LOAN UNITS - BROWSE') = level:benign

    if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign
        enable(?BrowseExchangeUnits)
    else!if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign
        disable(?BrowseExchangeUnits)
    end!if securitycheck('EXCHANGE UNITS - BROWSE') = level:benign

    IF (SecurityCheck('WIP AUDIT STATUSES') = Level:Benign)
        ! #13432 Add link to WIP Excluded Statuses (DBH: 14/01/2015)
        ENABLE(?WIPReportStatuses)
    ELSE  !IF
        DISABLE(?WIPReportStatuses)
    END ! IF


!!! - RAPID UPDATE - !!!
    If (SecurityCheck('JOBS - INSERT'))
        ?RapidProceduresNewJobBooking{prop:Disable} = 1
        ?RapidProceduresMultipleJobBooking{prop:Disable} = 1
    Else
        ?RapidProceduresNewJobBooking{prop:Disable} = 0
        ?RapidProceduresMultipleJobBooking{prop:Disable} = 0
    End

    If (SecurityCheck('RAPID - ENGINEER UPDATE'))
        ?RapidProceduresEngineerUpdate{prop:Disable} = 1
    Else
        ?RapidProceduresEngineerUpdate{prop:Disable} = 0
    End


    If SecurityCheck('RAPID - WAYBILL GENERATION') = level:benign
        Enable(?RapidProceduresWaybillGeneration)
    Else
        Disable(?RapidProceduresWaybillGeneration)
    End

    If SecurityCheck('RAPID - WAYBILL CONFIRMATION') = level:benign
        Enable(?RapidProceduresWaybillConfirmation)
    Else
        Disable(?RapidProceduresWaybillConfirmation)
    End

    If SecurityCheck('RAPID - EXCHANGE AUDIT') = level:benign
        Enable(?RapidProceduresExchangeAudit)
        Enable(?StockControlExchangeAudit)
    Else
        Disable(?RapidProceduresExchangeAudit)
        Disable(?StockControlExchangeAudit)
    End

    If (SecurityCheck('RAPID - LOAN AUDIT'))
        ?RapidProceduresLoanAudit{prop:Disable} = 1
    else
        ?RapidProceduresLoanAudit{prop:Disable} = 0
    end ! If (SecurityCheck('RAPID - LOAN AUDIT'))

    If (SecurityCheck('RAPID - WIP AUDIT'))
        ?RapidProceduresWIPAudit{prop:Disable} = 1
        ?StockControlWIPAudit{prop:Disable} = 1
    else
        ?RapidProceduresWIPAudit{prop:Disable} = 0
        ?StockControlWIPAudit{prop:Disable} = 0
    end ! If (SecurityCheck('RAPID - LOAN AUDIT'))

    If SecurityCheck('BROWSE RETURN ORDERS') = level:benign
        Enable(?StockControlReturnOrders)
    Else
        Disable(?StockControlReturnOrders)
    End

    If SecurityCheck('RAPID - STOCK RECEIVE') = level:benign
        Enable(?StockControlStockReceiveProcedure)
    Else
        Disable(?StockControlStockReceiveProcedure)
    End

    If SecurityCheck('RAPID - STOCK ALLOCATION') = level:benign
        Enable(?StockControlStockAllocation)
    Else
        Disable(?StockControlStockAllocation)
    End

    If SecurityCheck('RAPID - STOCK AUDIT') = level:benign
        Enable(?StockControlStockAudit)
    Else
        Disable(?StockControlStockAudit)
    End

    If SecurityCheck('RAPID ENG - ALLOCATE JOB') = level:benign
        Enable(?RapidProceduresAllocationsJobUpate)
    Else
        Disable(?RapidProceduresAllocationsJobUpate)
    End

    If (SecurityCheck('RAPID - QA PROCEDURE'))
        ?RapidProceduresQAElectronicQA{prop:Disable} = 1
        ?RapidProceduresQAManualQA{prop:Disable} = 1
    Else
        ?RapidProceduresQAElectronicQA{prop:Disable} = 0
        ?RapidProceduresQAManualQA{prop:Disable} = 0
    End
    If (SecurityCheck('RAPID - JOB ALLOCATION'))
        ?RapidProceduresAllocationsJobUpate{prop:Disable} = 1
    Else
        ?RapidProceduresAllocationsJobUpate{prop:Disable} = 0
    End
    If (SecurityCheck('RAPID - EXCHANGE ALLOCATION'))
        ?RapidProceduresAllocationsExchangeUpdate{prop:Disable} = 1
    Else
        ?RapidProceduresAllocationsExchangeUpdate{prop:Disable} = 0
    End
    If (SecurityCheck('RAPID - HANDOVER CONFIRMATION'))
        ?RapidProceduresHandoverConfirmation2{prop:Disable} = 1
    Else
        ?RapidProceduresHandoverConfirmation2{prop:Disable} = 0
    End
    If (SecurityCheck('RAPID - CREATE SUNDRY WAYBILL'))
        ?RapidProceduresWaybillProcedureDespatchSundryIt{prop:Disable} = 1
    Else
        ?RapidProceduresWaybillProcedureDespatchSundryIt{prop:Disable} = 0
    End


!!! - DESPATCH - !!!
    if securitycheck('INDIVIDUAL DESPATCH') = level:benign
        enable(?Despatch)
    else
        disable(?Despatch)
    end

!!! - REPORTS - !!!

    if securitycheck('INCOME REPORT') = level:benign
        enable(?ReportsIncomeReport)
    else!if securitycheck('INCOME REPORT') = level:benign
        disable(?ReportsIncomeReport)
    end!if securitycheck('INCOME REPORT') = level:benign

    if securitycheck('REPAIR PERFORMANCE REPORT') = level:benign
        enable(?BrowseEngineersPerformance)
    else!if securitycheck('REPAIR ACTIVITY REPORT') = level:benign
        disable(?BrowseEngineersPerformance)
    end!if securitycheck('REPAIR ACTIVITY REPORT') = level:benign

!!! - STOCK CONTROL - !!!

    if securitycheck('MINIMUM STOCK LEVEL ROUTINE') = level:benign
        enable(?StockControlMinimumStockLevel)
    else
        disable(?StockControlMinimumStockLevel)
    end

    if securitycheck('OUTSTANDING STOCK ORDERS') = level:benign
        enable(?StockControlOutstandingStockOrders)
    else
        disable(?StockControlOutstandingStockOrders)
    end

!!! - JOBS - !!!

Create_Invoice      Routine
    Case Missive('Are you sure you want to print a single invoice?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
            !fetch the subtrader account - expected by single_invoice
            access:subtracc.clearkey(sub:Account_Number_Key)
            sub:Account_Number = wob:SubAcountNumber
            access:subtracc.fetch(sub:account_number_key)

            If CanInvoiceBePrinted(wob:refNumber,1) = Level:Benign
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = wob:refNumber
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found

                    if job:Invoice_Number = 0 ! Create invoice
                        If CreateInvoice() = Level:Benign
                            glo:Select1  = job:Invoice_Number
                            !Vodacom_Delivery_Note_Web('','')
                            VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')  ! #11881 New Single Invoice (Bryan: 16/03/2011)
                            glo:Select1  = job:Ref_Number
                        End !If CreateInvoice() = Level:Benign
                    else ! Reprint invoice
                        glo:Select1  = job:Invoice_Number
                        !Vodacom_Delivery_Note_Web('','')
                        VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')  ! #11881 New Single Invoice (Bryan: 16/03/2011)
                        glo:Select1  = job:Ref_Number
                    end
                Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            End !If CanInvoiceBePrinted(glo:Select1) = Level:Benign
        Of 1 ! No Button
    End ! Case Missive


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020586'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Wobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SRNNumber
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EDIBATCH.Open
  Relate:EXCHANGE.Open
  Relate:LOGGED.Open
  Relate:MULDESPJ.Open
  Relate:MULDESP_ALIAS.Open
  Relate:STATUS.Open
  Relate:TRADEAC2.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:JOBS_ALIAS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:LOAN.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATLOG.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  LocalTradeAcc = Glo:Select1
  access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = LocalTradeAcc
  access:tradeacc.fetch(tra:Account_Number_Key)
  tmp:tradeID = tra:BranchIdentification
  
  Do ShowUserDetails
  
  set(defaults)
  access:defaults.next()
  
  Select_Courier_temp = 'ALL'
  Select_Trade_Account_temp = 'ALL'
  
  do Security_Check
  
  If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') <> 1
      ?Image1{prop:Hide} = 0
  Else ! If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') = 1
      If GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI') <> ''
          ?Image1{prop:Text} = Clip(GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI'))
          ?Image1{prop:Hide} = 0
      End ! If GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI') <> ''
  End ! If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !Version Number
  Access:DEFAULTV.Open()
  Access:DEFAULTV.UseFile()
  Set(DEFAULTV)
  Access:DEFAULTV.Next()
  ?Version:Prompt{prop:Text} = 'Version: ' & Clip(defv:VersionNumber)
  Access:DEFAULTV.Close()
  ! Save Window Name
   AddToLog('Window','Open','Browse_Wobs')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  if glo:webJob
      ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
      !Now log out
      access:users.clearkey(use:password_key)
      use:password = glo:password
      if access:users.fetch(use:password_key) = Level:Benign
          access:logged.clearkey(log:user_code_key)
          log:user_code = use:user_code
          log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
          if access:logged.fetch(log:user_code_key) = Level:Benign
              delete(logged)
          end !if
      end !if
  end
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EDIBATCH.Close
    Relate:EXCHANGE.Close
    Relate:LOGGED.Close
    Relate:MULDESPJ.Close
    Relate:MULDESP_ALIAS.Close
    Relate:STATUS.Close
    Relate:TRADEAC2.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Wobs')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?FileReLogin
      !log out the previous user
          access:users.clearkey(use:password_key)
          use:password = glo:password
          if access:users.fetch(use:password_key) = Level:Benign
              access:logged.clearkey(log:user_code_key)
              log:user_code = use:user_code
              log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
              if access:logged.fetch(log:user_code_key) = Level:Benign
                  delete(logged)
              end !if
          end !if
      !Log in the new
          glo:select1 = 'N'
          Insert_Password
          If glo:select1 = 'Y'
              post(event:closewindow)
          Else
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              if use:Location <> glo:location then
                   message('Logon Details Incorrect')
                   post(event:closewindow)
              END
              ! Refresh User name on the screen - TrkBs: 6572 (DBH: 19-10-2005)
              Do ShowUserDetails
              do Security_Check
          End
    OF ?FileExit
      post(event:closeWindow)
    OF ?SystemAdminTradeAccounts
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineTradeAccs= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?BrowseBouncers
        ! #13410 Force SBOnline if "Bouncers" turned on in trade defaults. (DBH: 31/10/2014)
        Access:TRADEAC2.ClearKey(tra2:KeyAccountNumber)
        tra2:Account_Number = Clarionet:Global.Param2
        IF (Access:TRADEAC2.TryFetch(tra2:KeyAccountNumber) = Level:Benign)
          IF (TRA2:SBOnlineBouncers = 2)
              ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
              CYCLE
          END ! IF
        END ! IF
    OF ?ReportsItem52
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsExchangeReportsExchangeDeviceSoldReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
    OF ?ReportsExchangeReportsExchangePhoneReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsExchangeReportsExchangeStock
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsExchangeReportsExchangeUnitsReturnedToMa
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsLoanDeviceSoldReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsLoanReportsLoanUnitsReturnedToMainSTore
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsLoanPhoneReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsIncomeReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsWarrantyIncomeReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsCreditReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsHandlingExchangeFeeReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsStatusReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsWarrantyClaimStatusReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsInsuranceClaimReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsWarrantyPartsSenttoARCReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsStockValueReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?ReportsSMSResponseReport
              !TB13153 - force to use SBOnline if set up - JC - reports included 13/06/14
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineReports= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
    OF ?RapidProceduresMultipleJobBooking
      !both security check and account settings added - J - 03/07/14
      if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      
      !TB13323 - force to use SBOnline if set up - JC 03/07/14
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      TRA:Account_Number = Clarionet:Global.Param2
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          If tra:UseSBOnline= 2 then
              ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
              Cycle
          End ! tra:UseSBOnline
      End ! If Access:TRADEACC.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      clear(wob:record)
    OF ?RapidProceduresEngineerUpdate
      if (SecurityCheck('RAPID - ENGINEER UPDATE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      
      
      !TB13153 - force to use SBOnline if set up - JC 18/09/2013
      Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
      TRA2:Account_Number = Clarionet:Global.Param2
      If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
          If TRA2:SBOnlineEngineer= 2 then
              ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
              Cycle
          End ! 
      End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      If GetTempPathA(255,TempFilePath)
          If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & 'RAPENG' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & '\RAPENG' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
    OF ?RapidProceduresAllocationsJobUpate
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineQAEtc= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineQAEtc
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?RapidProceduresAllocationsExchangeUpdate
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineQAEtc= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineQAEtc
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?RapidProceduresHandOverConfirmation
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineQAEtc= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineQAEtc
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?RapidProceduresExchangeHandOverConfirmation
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineQAEtc= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineQAEtc
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?RapidProceduresWaybillGeneration
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineWaybills = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?RapidProceduresWaybillConfirmation
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineWaybills = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      If GetTempPathA(255,TempFilePath)
          If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & 'WAYBILLCONF' & Clock() & '.TMP'
          Else !If Sub(TempFilePath,-1,1) = '\'
              glo:FileName = Clip(TempFilePath) & '\WAYBILLCONF' & Clock() & '.TMP'
          End !If Sub(TempFilePath,-1,1) = '\'
      End
    OF ?RapidProceduresWaybillProcedureDespatchSundryIt
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineWaybills= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineQAEtc
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?StockControlMinimumStockLevel
      !tmp:UseScheduledMinStock = GETINI('STOCK','UseScheduledMinStock',,CLIP(PATH()) & '\SB2KDEF.INI')
      IF GETINI('STOCK','UseScheduledMinStock',,CLIP(PATH()) & '\SB2KDEF.INI') = FALSE
        glo:File_Name   = 'S' & Format(CLock(),@n07) & '.TMP'
      ELSE
        glo:File_Name = GETINI('STOCK','ScheduledFileName',,CLIP(PATH()) & '\SB2KDEF.INI')
      END
      BrowseMinimumStock
    OF ?StockControlStockAllocation
      !set up a temporary name for rapidstock
      !    If GetTempPathA(255,TempFilePath)
      !        If Sub(TempFilePath,-1,1) = '\'
      !    glo:FileName = Clip(TempFilePath) & 'RAPIDSTOCK' & Clock() & '.TMP'
      !        Else !If Sub(TempFilePath,-1,1) = '\'
      !            glo:FileName = Clip(TempFilePath) & '\RAPIDSTOCK' & Clock() & '.TMP'
      !        End !If Sub(TempFilePath,-1,1) = '\'
      !    End
      glo:FileName    = 'RAPSTO' & Format(Today(),@d11) & Clip(Clock()) & '.TMP'
    OF ?WarrantyClaims
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineWarrClaims = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    OF ?Close
      !Case Missive('Are you sure you want to close ServiceBase?','ServiceBase 3g',|
      !             'mquest.jpg','\No|/Yes')
      !  Of 2 ! Yes Button
      !  Of 1 ! No Button
      !      CYCLE
      !End ! Case Missive
    OF ?Button:JobSearch
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
    OF ?Button:SingleIMEISearch
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
    OF ?StockControl:2
         !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineStock= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineDespatch
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?SystemAdminTradeAccounts
      ThisWindow.Update
      Browse_Subtraders
      ThisWindow.Reset
    OF ?SystemAdminUsers
      ThisWindow.Update
      Browse_users
      ThisWindow.Reset
    OF ?WIPReportStatuses
      ThisWindow.Update
      WIPExcludedStatuses
      ThisWindow.Reset
    OF ?BrowseEngineersPerformance
      ThisWindow.Update
      Engineer_Status_screen
      ThisWindow.Reset
    OF ?BrowseExchangeUnits
      ThisWindow.Update
      Browse_Exchange('')
      ThisWindow.Reset
    OF ?BrowseLoanUnits
      ThisWindow.Update
      Browse_loan('')
      ThisWindow.Reset
    OF ?BrowseBouncers
      ThisWindow.Update
      Browse_Bouncers
      ThisWindow.Reset
    OF ?ReportsItem52
      ThisWindow.Update
      AvailableExchangeUnitsReport
      ThisWindow.Reset
    OF ?ReportsExchangeReportsExchangeDeviceSoldReport
      ThisWindow.Update
      ExchangeLoanDeviceSoldReport(0)
      ThisWindow.Reset
    OF ?ReportsExchangeReportsExchangePhoneReport
      ThisWindow.Update
      ExchangePhoneReport
      ThisWindow.Reset
    OF ?ReportsExchangeReportsExchangeStock
      ThisWindow.Update
      ExchangeStockValuationReport
      ThisWindow.Reset
    OF ?ReportsExchangeReportsExchangeUnitsReturnedToMa
      ThisWindow.Update
      ExchangeLoanUnitsReturnedToMainStore(0)
      ThisWindow.Reset
    OF ?ReportsLoanDeviceSoldReport
      ThisWindow.Update
      ExchangeLoanDeviceSoldReport(1)
      ThisWindow.Reset
    OF ?ReportsLoanReportsLoanUnitsReturnedToMainSTore
      ThisWindow.Update
      ExchangeLoanUnitsReturnedToMainStore(1)
      ThisWindow.Reset
    OF ?ReportsLoanPhoneReport
      ThisWindow.Update
      LoanUnitReportCriteria
      ThisWindow.Reset
    OF ?ReportsIncomeReport
      ThisWindow.Update
      JobIncomeReportCriteria
      ThisWindow.Reset
    OF ?ReportsWarrantyIncomeReport
      ThisWindow.Update
      WarrantyIncomeReportCriteria
      ThisWindow.Reset
    OF ?ReportsCreditReport
      ThisWindow.Update
      CreditReportCriteria
      ThisWindow.Reset
    OF ?ReportsHandlingExchangeFeeReport
      ThisWindow.Update
      HandlingExchangeReportCriteria
      ThisWindow.Reset
    OF ?ReportsStatusReport
      ThisWindow.Update
      Status_Report_Criteria_Web
      ThisWindow.Reset
    OF ?ReportsWarrantyClaimStatusReport
      ThisWindow.Update
      WarrantyClaimStatusReport
      ThisWindow.Reset
    OF ?ReportsInsuranceClaimReport
      ThisWindow.Update
      AuthorityNumberExportCriteria
      ThisWindow.Reset
    OF ?ReportsWarrantyPartsSenttoARCReport
      ThisWindow.Update
      RFExchangeReportCriteria
      ThisWindow.Reset
    OF ?ReportsStockValueReport
      ThisWindow.Update
      Stock_Value_Detailed_Criteria
      ThisWindow.Reset
    OF ?ReportsSMSResponseReport
      ThisWindow.Update
      SMS_Response_Report
      ThisWindow.Reset
    OF ?RapidProceduresNewJobBooking
      ThisWindow.Update
      !code for security check and ccount settings added 03/07/14 - J
      if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      
      !TB13323 - force to use SBOnline if set up - JC 03/07/14
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      TRA:Account_Number = Clarionet:Global.Param2
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          If tra:UseSBOnline= 2 then
              ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
              Cycle
          End ! tra:UseSBOnline
      End ! If Access:TRADEACC.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      glo:insert_global = 'YES'
      clear(wob:record)
      Update_Jobs
      !Brw1.resetfromview
      !Brw1.resetsort(1)
    OF ?RapidProceduresMultipleJobBooking
      ThisWindow.Update
      Multiple_Job_Booking
      ThisWindow.Reset
    OF ?RapidProceduresEngineerUpdate
      ThisWindow.Update
      Update_engineer_job
      ThisWindow.Reset
      Remove(glo:FileName)
    OF ?RapidProceduresAllocationsJobUpate
      ThisWindow.Update
      Update_Jobs_Status
      ThisWindow.Reset
    OF ?RapidProceduresAllocationsExchangeUpdate
      ThisWindow.Update
      UpdateJobExchangeStatus
      ThisWindow.Reset
    OF ?RapidProceduresHandOverConfirmation
      ThisWindow.Update
      HandOverConfirmation(0)
      ThisWindow.Reset
    OF ?RapidProceduresExchangeHandOverConfirmation
      ThisWindow.Update
      HandOverConfirmation(1)
      ThisWindow.Reset
    OF ?RapidProceduresHandoverConfirmationReprintHando
      ThisWindow.Update
      HandOverReportHistory
      ThisWindow.Reset
    OF ?RapidProceduresQAElectronicQA
      ThisWindow.Update
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineQAEtc = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      glo:Select40 = 'REPEAT'
      Loop Until glo:Select40 <> 'REPEAT'
          Update_QA('PRE')
      End !Until glo:Select40 <> 'REPEAT'
      !Brw1.resetsort(1)
    OF ?RapidProceduresQAManualQA
      ThisWindow.Update
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineQAEtc = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      glo:Select40 = 'REPEAT'
      Loop Until glo:Select40 <> 'REPEAT'
          Update_QA('PRI')
      End !Until glo:Select40 <> 'REPEAT'
      !Brw1.resetsort(1)
    OF ?RapidProceduresWaybillGeneration
      ThisWindow.Update
      BuildList
      ThisWindow.Reset
    OF ?RapidProceduresWaybillConfirmation
      ThisWindow.Update
      WaybillConfirmation
      ThisWindow.Reset
      Remove(glo:FileName)
    OF ?RapidProceduresWaybillProcedureDespatchSundryIt
      ThisWindow.Update
      CreateSundryWaybill
      ThisWindow.Reset
    OF ?RapidProceduresExchangeAudit
      ThisWindow.Update
      BrowseEXCAudit
      ThisWindow.Reset
    OF ?RapidProceduresLoanAudit
      ThisWindow.Update
      BrowseLoanAudit
      ThisWindow.Reset
    OF ?RapidProceduresWIPAudit
      ThisWindow.Update
      BrowseWIPAudit
      ThisWindow.Reset
    OF ?RapidProceduresSearchAccessories
      ThisWindow.Update
      AccessoryJobMatch
      ThisWindow.Reset
    OF ?StockControlAdjustedWebOrderReceipts
      ThisWindow.Update
      AdjustedWebOrderReceipts
      ThisWindow.Reset
    OF ?StockControlBrowseRetailSales
      ThisWindow.Update
      Browse_All_Retail_Sales
      ThisWindow.Reset
    OF ?StockControlBrowseStock
      ThisWindow.Update
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineStock = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
          Browse_Stock
    OF ?StockControlOutstandingStockOrders
      ThisWindow.Update
      BrowseOutstandingStockOrders
      ThisWindow.Reset
    OF ?StockControlPendingOrders
      ThisWindow.Update
      BrowsePendingWebOrders
      ThisWindow.Reset
    OF ?StockControlStockReceiveProcedure
      ThisWindow.Update
      ReceiveStock
      ThisWindow.Reset
    OF ?StockControlExchangeAllocation
      ThisWindow.Update
          RapidExchangeAllocation
    OF ?StockControlReturnOrders
      ThisWindow.Update
      BrowseReturnOrders
      ThisWindow.Reset
    OF ?StockControlStockAllocation
      ThisWindow.Update
      RSA_MainBrowse
      ThisWindow.Reset
      Remove(glo:FileName)
    OF ?StockControlStockAudit
      ThisWindow.Update
      BrowseAudit
      ThisWindow.Reset
    OF ?StockControlExchangeAudit
      ThisWindow.Update
      BrowseEXCAudit
      ThisWindow.Reset
    OF ?StockControlWIPAudit
      ThisWindow.Update
      BrowseWIPAudit
      ThisWindow.Reset
    OF ?Despatch
      ThisWindow.Update
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineDespatch = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      !check if we can despatch this job
      
      !fetch the defaults
      set(defaults)
      access:defaults.next()
      
      !fetch the job
      access:jobs.clearkey(job:Ref_Number_Key)
      job:Ref_Number = wob:refNumber
      access:jobs.fetch(job:ref_number_key)
      
      !now check
      error"=''
      IF  job:date_completed = '' THEN ERROR" = 'JOB NOT COMPLETED'.
      IF  job:despatched = 'YES'THEN ERROR" = 'JOB ALREADY DESPATCHED'.
      IF  DEF:QA_REQUIRED = 'YES'
          IF  job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES' THEN ERROR" = 'JOB HAS NOT PASSED QA'.
      END
      If error" <> ''
            Case Missive('The selected job cannot be despatched.'&|
              '<13,10>'&|
              '<13,10>' & Error" & '.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
      ELSE
          !Look up the despatch type
          access:jobse.clearkey(jobe:RefNumberKey)
          jobe:RefNumber = job:ref_number
          access:jobse.fetch(jobe:RefNumberKey)
          !glo:select1 = wob:refNumber
          !glo:select2 = 'JOB'
          RemoteDespatch(job:ref_number,jobe:despatchtype)
      END !If error"<>''
      
      
    OF ?WarrantyClaims
      ThisWindow.Update
      RRCWarrantyProcess
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020586'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020586'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020586'&'0')
      ***
    OF ?Button:CreateNewJob
      ThisWindow.Update
    if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('You do not have access to this option.','ServiceBase',|
                       'mstop.jpg','/&OK')
        Of 1 ! &OK Button
        End!Case Message
        Cycle
    End
! Inserting (DBH 20/02/2008) # 9770 - Force the user to use SB Online
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = Clarionet:Global.Param2
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:UseSBOnline = 2          !TB13153 UsesSBOnline now has a forceing element (2) as well as a use (1) - JC 18/09/2013
            ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
            Cycle
        End ! If tra:UseSBOnline
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    CreateNewJob
! End (DBH 20/02/2008) #9770

    OF ?Button:JobSearch
      ThisWindow.Update
      JobSearchWindow
      ThisWindow.Reset
    OF ?Button:SingleIMEISearch
      ThisWindow.Update
      SingleIMEISearchWIndow
      ThisWindow.Reset
    OF ?Button:GlobalIMEISearch
      ThisWindow.Update
      GlobalIMEISearchWIndow
      ThisWindow.Reset
    OF ?Button:IMEIValidation
      ThisWindow.Update
      ClarioNET:CallClientPRocedure('OPENURL',Clip(GETINI('URL','IMEIValidation',,Clip(Path()) & '\SB2KDEF.INI')))
    OF ?Button:Menu
      ThisWindow.Update
      Execute POPUP('Single New Job Booking|Multiple Job Booking|Engineer Update|Allocations{{Job Update|Exchange Update}|' & |
                      'Handover Confirmation{{Job Handover Confirmation|Exchange Handover Confirmation|-|Reprint Handover Confirmation Report}|' & |
                      'QA{{Electronic QA|Manual QA}|Waybill{{Waybill Generation|Waybill Confirmation|Waybill Procedure|Despatch Sundry Items}|' & |
                      'Exchange Audit|Loan Audit|WIP Audit|Search Accessories')
          Begin
      
              if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              TRA:Account_Number = Clarionet:Global.Param2
              If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  If tra:UseSBOnline= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! tra:UseSBOnline
              End ! If Access:TRADEACC.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
              CreateNewJob
          End
          Begin
              if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
      
              !TB13323 - force to use SBOnline if set up - JC 03/07/14
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              TRA:Account_Number = Clarionet:Global.Param2
              If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  If tra:UseSBOnline= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! tra:UseSBOnline
              End ! If Access:TRADEACC.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
              Multiple_Job_Booking
          End
          Begin ! Engineer Udate
              if (SecurityCheck('RAPID - ENGINEER UPDATE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
      
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineEngineer= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! 
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
              If GetTempPathA(255,TempFilePath)
              If Sub(Clip(TempFilePath),-1,1) = '\'
                  glo:FileName = Clip(TempFilePath) & 'RAPENG' & Clock() & '.TMP'
              Else !If Sub(Clip(TempFilePath),-1,1) = '\'
                  glo:FileName = Clip(TempFilePath) & '\RAPENG' & Clock() & '.TMP'
              End !If Sub(Clip(TempFilePath),-1,1) = '\'
              End
              Update_Engineer_Job
              Remove(glo:FileName)
          End
          Begin ! Job Allocation
              if (SecurityCheck('RAPID - JOB ALLOCATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineQAEtc= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
              Update_Jobs_Status
          End
          Begin ! Exchange Allocation
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineQAEtc= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - EXCHANGE ALLOCATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              UpdateJobExchangeStatus
          End
          Begin ! Handover Confirmation
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineQAEtc= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - HANDOVER CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              HandOverConfirmation(0)
          End
          Begin ! Handover Confirmation
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineQAEtc= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - HANDOVER CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              HandOverConfirmation(1)
          End
          Begin
              if (SecurityCheck('RAPID - HANDOVER CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              HandOverReportHistory
          End
          Begin
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineQAEtc= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - QA PROCEDURE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              glo:Select40 = 'REPEAT'
              Loop Until glo:Select40 <> 'REPEAT'
                  Update_QA('PRE')
              End
          End ! Being
          Begin
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineQAEtc= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineQAEtc
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - QA PROCEDURE'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              glo:Select40 = 'REPEAT'
              Loop Until glo:Select40 <> 'REPEAT'
                  Update_QA('PRI')
              End
          End ! Being
          Begin ! Waybill Generation
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineWaybills= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - WAYBILL GENERATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
      
              BuildList
          End
          Begin ! Waybill Confirmation
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineWaybills= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - WAYBILL CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              If GetTempPathA(255,TempFilePath)
                  If Sub(TempFilePath,-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & 'WAYBILLCONF' & Clock() & '.TMP'
                  Else !If Sub(TempFilePath,-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & '\WAYBILLCONF' & Clock() & '.TMP'
                  End !If Sub(TempFilePath,-1,1) = '\'
              End
              WaybillConfirmation
              Remove(glo:FileName)
          End
          Begin ! Waybill Procedure
      !        !TB13153 - force to use SBOnline if set up - JC 18/09/2013
      !        Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
      !        TRA2:Account_Number = Clarionet:Global.Param2
      !        If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      !            If TRA2:SBOnlineWaybills= 2 then
      !                ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
      !                Cycle
      !            End ! TRA2:SBOnlineDespatch
      !        End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - WAYBILL CONFIRMATION'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
      !        If GetTempPathA(255,TempFilePath)
      !            If Sub(TempFilePath,-1,1) = '\'
      !                glo:FileName = Clip(TempFilePath) & 'WAYBILLCONF' & Clock() & '.TMP'
      !            Else !If Sub(TempFilePath,-1,1) = '\'
      !                glo:FileName = Clip(TempFilePath) & '\WAYBILLCONF' & Clock() & '.TMP'
      !            End !If Sub(TempFilePath,-1,1) = '\'
      !        End
              BROWSEWAYBCONF 
      !        Remove(glo:FileName)
          End !Waybill Procedure
      
          Begin
              !TB13153 - force to use SBOnline if set up - JC 18/09/2013
              Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
              TRA2:Account_Number = Clarionet:Global.Param2
              If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
                  If TRA2:SBOnlineWaybills= 2 then
                      ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                      Cycle
                  End ! TRA2:SBOnlineDespatch
              End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              if (SecurityCheck('RAPID - CREATE SUNDRY WAYBILL'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              CreateSundryWaybill
          End
          Begin ! Exchange Audit
              if (SecurityCheck('RAPID - EXCHANGE AUDIT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseEXCAudit
          End
          Begin ! Loan Audit
              if (SecurityCheck('RAPID - LOAN AUDIT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseLoanAudit
          End
          Begin ! WIP Audit
              if (SecurityCheck('RAPID - WIP AUDIT'))  ! #11682 Check access. (Bryan: 07/09/2010)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('You do not have access to this option.','ServiceBase',|
                                 'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
                  Cycle
              End
              BrowseWIPAudit
          End
          AccessoryJobMatch
      End ! 'Exchange Audit|Loan Audit|WIP Audit|Search Accessories')
    OF ?JobProgress
      ThisWindow.Update
      BrowseWebJobs
      ThisWindow.Reset
    OF ?StockControl:2
      ThisWindow.Update
      Browse_Stock
      ThisWindow.Reset
    OF ?ButtonBrowseVPortal
      ThisWindow.Update
      BrowsePreJob
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

LocalValidateAccountNumber      Procedure()

    Code
    message('In local validate account number')
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case Missive('The trade account attached to job number ' & Clip(job:Ref_Number) & ' is only used for "Batch Despatch".','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign
LocalValidateIMEI    Procedure()

local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign

        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
          '<13,10>'&|
          '<13,10>I.M.E.I. Number mismatch.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        GetStatus(850,1,job:Despatch_Type)

        IF (AddToAudit(job:ref_number,'JOB','FAILED DESPATCH VALIDATION','I.M.E.I. NUMBER MISMATCH'))
        END ! IF


        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            If AccessoryCheck('JOB')
                If AccessoryMismatch(job:Ref_Number,'JOB')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'JOB')
            End !If AccessoryCheck('JOB')
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)
            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

        IF (AddToAudit(job:Ref_Number,'JOB','FAILED DESPATCH VALIDATION',locAuditNotes))
        END ! IF

        Access:JOBS.Update()
        Return Level:Fatal

    End !If Return Level:Fatal

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
