

   MEMBER('sbj04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBJ04004.INC'),ONCE        !Local module procedure declarations
                     END


BrowseWebJobs PROCEDURE                               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer20                     LIKE(glo:Pointer20)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
SMSErrorFlag         BYTE
Count                LONG
tmp:failed           BYTE
LocalTradeAcc        STRING(30)
Display_Address      STRING(200)
JobCount             LONG,DIM(10)
TotaljobCount        LONG
tmp:Company_Name     STRING(30)
tmp:WarManufacturer  STRING(30)
Tmp:UseWarMan        BYTE
tmp:Type             STRING(1)
tmp:GlobalIMEINumber STRING(20)
tmp:Current_status   STRING(33)
tmp:Current_StatusBase STRING(30)
tmp:Loan_status      STRING(30)
tmp:Exchange_Status  STRING(30)
tmp:StatusDate       DATE
tmp:ExchangeStatusDate DATE
tmp:LoanStatusDate   DATE
tmp:wob_jobNumber    STRING(20)
tmp:tradeID          STRING(2)
Save_Aus_id          USHORT
save_wob_id          USHORT,AUTO
Save_JAC_ID          USHORT
Save_LAC_ID          USHORT
Save_job_id          USHORT
Save_mulj_ID         USHORT
Save_job_ali_id      USHORT
Save_Jot_Id          USHORT
Save_xch_id          USHORT
tmp:printDespatchNote BYTE
tmp:firstrecord      BYTE
tmp:batchnumber      LONG
tmp:BatchCreated     BYTE
tag_temp             STRING(1)
Select_Courier_temp  STRING(3)
Select_Trade_Account_temp STRING(3)
Courier_Temp         STRING(30)
Account_Number2_temp STRING(30)
tmp:EDI              STRING('NO {1}')
tmp:Engineers_Notes  STRING(255)
tmp:Courier          STRING(30)
tmp:SearchStatus     STRING(30)
tmp:status           STRING(30)
tmp:Location         STRING(30)
tmp:BrowseFlag       BYTE(0)
temp:Current_Status  STRING(30)
temp:Loan_Status     STRING(30)
temp:Exchange_Status STRING(30)
LocalWayBillNumber   LONG
tmp:Address          STRING(500)
address:CompanyName  STRING(30)
address:AddressLine1 STRING(30)
address:AddressLine2 STRING(30)
address:AddressLine3 STRING(30)
address:Postcode     STRING(30)
address:TelephoneNumber STRING(30)
address:FaxNumber    STRING(30)
tmp:ExchangeUnitDetails STRING(60)
tmp:ExchangeUnitDetails2 STRING(60)
tmp:LoanUnitDetails  STRING(60)
tmp:LoanUnitDetails2 STRING(60)
tmp:DateCompleted    DATE
LocationCountString  STRING(255)
tmp:JobStatusDays    STRING(30)
tmp:LoaStatusDays    STRING(30)
tmp:ExcStatusDays    STRING(30)
tmp:JobDays          STRING(30)
tmp:ModelNumber      STRING(30)
tmp:SubAccountNumber STRING(30)
tmp:Cost             REAL
LocalTag             STRING(1)
TempTestValue        BYTE
locAuditNotes        STRING(255)
filename3            STRING(255),STATIC
filename4            STRING(255)
BRW1::View:Browse    VIEW(WEBJOB)
                       PROJECT(wob:SubAcountNumber)
                       PROJECT(wob:IMEINumber)
                       PROJECT(wob:ModelNumber)
                       PROJECT(wob:MobileNumber)
                       PROJECT(wob:Current_Status)
                       PROJECT(wob:Exchange_Status)
                       PROJECT(wob:Loan_Status)
                       PROJECT(wob:OrderNumber)
                       PROJECT(wob:HeadAccountNumber)
                       PROJECT(wob:RecordNumber)
                       PROJECT(wob:JobNumber)
                       PROJECT(wob:RefNumber)
                       PROJECT(wob:Manufacturer)
                       PROJECT(wob:EDI)
                       PROJECT(wob:ReadyToDespatch)
                       PROJECT(wob:DespatchCourier)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse
tmp:wob_jobNumber      LIKE(tmp:wob_jobNumber)        !List box control field - type derived from local data
tmp:wob_jobNumber_NormalFG LONG                       !Normal forground color
tmp:wob_jobNumber_NormalBG LONG                       !Normal background color
tmp:wob_jobNumber_SelectedFG LONG                     !Selected forground color
tmp:wob_jobNumber_SelectedBG LONG                     !Selected background color
wob:SubAcountNumber    LIKE(wob:SubAcountNumber)      !List box control field - type derived from field
wob:SubAcountNumber_NormalFG LONG                     !Normal forground color
wob:SubAcountNumber_NormalBG LONG                     !Normal background color
wob:SubAcountNumber_SelectedFG LONG                   !Selected forground color
wob:SubAcountNumber_SelectedBG LONG                   !Selected background color
tmp:Company_Name       LIKE(tmp:Company_Name)         !List box control field - type derived from local data
tmp:Company_Name_NormalFG LONG                        !Normal forground color
tmp:Company_Name_NormalBG LONG                        !Normal background color
tmp:Company_Name_SelectedFG LONG                      !Selected forground color
tmp:Company_Name_SelectedBG LONG                      !Selected background color
wob:IMEINumber         LIKE(wob:IMEINumber)           !List box control field - type derived from field
wob:IMEINumber_NormalFG LONG                          !Normal forground color
wob:IMEINumber_NormalBG LONG                          !Normal background color
wob:IMEINumber_SelectedFG LONG                        !Selected forground color
wob:IMEINumber_SelectedBG LONG                        !Selected background color
tmp:Current_status     LIKE(tmp:Current_status)       !List box control field - type derived from local data
tmp:Current_status_NormalFG LONG                      !Normal forground color
tmp:Current_status_NormalBG LONG                      !Normal background color
tmp:Current_status_SelectedFG LONG                    !Selected forground color
tmp:Current_status_SelectedBG LONG                    !Selected background color
tmp:Location           LIKE(tmp:Location)             !List box control field - type derived from local data
tmp:Location_NormalFG  LONG                           !Normal forground color
tmp:Location_NormalBG  LONG                           !Normal background color
tmp:Location_SelectedFG LONG                          !Selected forground color
tmp:Location_SelectedBG LONG                          !Selected background color
tmp:Type               LIKE(tmp:Type)                 !List box control field - type derived from local data
tmp:Type_NormalFG      LONG                           !Normal forground color
tmp:Type_NormalBG      LONG                           !Normal background color
tmp:Type_SelectedFG    LONG                           !Selected forground color
tmp:Type_SelectedBG    LONG                           !Selected background color
wob:ModelNumber        LIKE(wob:ModelNumber)          !List box control field - type derived from field
wob:ModelNumber_NormalFG LONG                         !Normal forground color
wob:ModelNumber_NormalBG LONG                         !Normal background color
wob:ModelNumber_SelectedFG LONG                       !Selected forground color
wob:ModelNumber_SelectedBG LONG                       !Selected background color
wob:MobileNumber       LIKE(wob:MobileNumber)         !List box control field - type derived from field
wob:MobileNumber_NormalFG LONG                        !Normal forground color
wob:MobileNumber_NormalBG LONG                        !Normal background color
wob:MobileNumber_SelectedFG LONG                      !Selected forground color
wob:MobileNumber_SelectedBG LONG                      !Selected background color
wob:Current_Status     LIKE(wob:Current_Status)       !List box control field - type derived from field
wob:Current_Status_NormalFG LONG                      !Normal forground color
wob:Current_Status_NormalBG LONG                      !Normal background color
wob:Current_Status_SelectedFG LONG                    !Selected forground color
wob:Current_Status_SelectedBG LONG                    !Selected background color
wob:Exchange_Status    LIKE(wob:Exchange_Status)      !List box control field - type derived from field
wob:Exchange_Status_NormalFG LONG                     !Normal forground color
wob:Exchange_Status_NormalBG LONG                     !Normal background color
wob:Exchange_Status_SelectedFG LONG                   !Selected forground color
wob:Exchange_Status_SelectedBG LONG                   !Selected background color
wob:Loan_Status        LIKE(wob:Loan_Status)          !List box control field - type derived from field
wob:Loan_Status_NormalFG LONG                         !Normal forground color
wob:Loan_Status_NormalBG LONG                         !Normal background color
wob:Loan_Status_SelectedFG LONG                       !Selected forground color
wob:Loan_Status_SelectedBG LONG                       !Selected background color
wob:OrderNumber        LIKE(wob:OrderNumber)          !List box control field - type derived from field
wob:OrderNumber_NormalFG LONG                         !Normal forground color
wob:OrderNumber_NormalBG LONG                         !Normal background color
wob:OrderNumber_SelectedFG LONG                       !Selected forground color
wob:OrderNumber_SelectedBG LONG                       !Selected background color
tmp:Courier            LIKE(tmp:Courier)              !List box control field - type derived from local data
tmp:Courier_NormalFG   LONG                           !Normal forground color
tmp:Courier_NormalBG   LONG                           !Normal background color
tmp:Courier_SelectedFG LONG                           !Selected forground color
tmp:Courier_SelectedBG LONG                           !Selected background color
tmp:BrowseFlag         LIKE(tmp:BrowseFlag)           !List box control field - type derived from local data
wob:HeadAccountNumber  LIKE(wob:HeadAccountNumber)    !List box control field - type derived from field
wob:HeadAccountNumber_NormalFG LONG                   !Normal forground color
wob:HeadAccountNumber_NormalBG LONG                   !Normal background color
wob:HeadAccountNumber_SelectedFG LONG                 !Selected forground color
wob:HeadAccountNumber_SelectedBG LONG                 !Selected background color
wob:RecordNumber       LIKE(wob:RecordNumber)         !List box control field - type derived from field
wob:RecordNumber_NormalFG LONG                        !Normal forground color
wob:RecordNumber_NormalBG LONG                        !Normal background color
wob:RecordNumber_SelectedFG LONG                      !Selected forground color
wob:RecordNumber_SelectedBG LONG                      !Selected background color
wob:JobNumber          LIKE(wob:JobNumber)            !List box control field - type derived from field
wob:JobNumber_NormalFG LONG                           !Normal forground color
wob:JobNumber_NormalBG LONG                           !Normal background color
wob:JobNumber_SelectedFG LONG                         !Selected forground color
wob:JobNumber_SelectedBG LONG                         !Selected background color
wob:RefNumber          LIKE(wob:RefNumber)            !List box control field - type derived from field
wob:RefNumber_NormalFG LONG                           !Normal forground color
wob:RefNumber_NormalBG LONG                           !Normal background color
wob:RefNumber_SelectedFG LONG                         !Selected forground color
wob:RefNumber_SelectedBG LONG                         !Selected background color
wob:Manufacturer       LIKE(wob:Manufacturer)         !List box control field - type derived from field
wob:Manufacturer_NormalFG LONG                        !Normal forground color
wob:Manufacturer_NormalBG LONG                        !Normal background color
wob:Manufacturer_SelectedFG LONG                      !Selected forground color
wob:Manufacturer_SelectedBG LONG                      !Selected background color
job_ali:Exchange_Unit_Number LIKE(job_ali:Exchange_Unit_Number) !List box control field - type derived from field
job_ali:Exchange_Unit_Number_NormalFG LONG            !Normal forground color
job_ali:Exchange_Unit_Number_NormalBG LONG            !Normal background color
job_ali:Exchange_Unit_Number_SelectedFG LONG          !Selected forground color
job_ali:Exchange_Unit_Number_SelectedBG LONG          !Selected background color
jobe:DespatchType      LIKE(jobe:DespatchType)        !List box control field - type derived from field
jobe:DespatchType_NormalFG LONG                       !Normal forground color
jobe:DespatchType_NormalBG LONG                       !Normal background color
jobe:DespatchType_SelectedFG LONG                     !Selected forground color
jobe:DespatchType_SelectedBG LONG                     !Selected background color
job_ali:Loan_Unit_Number LIKE(job_ali:Loan_Unit_Number) !List box control field - type derived from field
job_ali:Loan_Unit_Number_NormalFG LONG                !Normal forground color
job_ali:Loan_Unit_Number_NormalBG LONG                !Normal background color
job_ali:Loan_Unit_Number_SelectedFG LONG              !Selected forground color
job_ali:Loan_Unit_Number_SelectedBG LONG              !Selected background color
job_ali:Current_Status LIKE(job_ali:Current_Status)   !List box control field - type derived from field
job_ali:Current_Status_NormalFG LONG                  !Normal forground color
job_ali:Current_Status_NormalBG LONG                  !Normal background color
job_ali:Current_Status_SelectedFG LONG                !Selected forground color
job_ali:Current_Status_SelectedBG LONG                !Selected background color
job_ali:EDI_Batch_Number LIKE(job_ali:EDI_Batch_Number) !List box control field - type derived from field
job_ali:EDI_Batch_Number_NormalFG LONG                !Normal forground color
job_ali:EDI_Batch_Number_NormalBG LONG                !Normal background color
job_ali:EDI_Batch_Number_SelectedFG LONG              !Selected forground color
job_ali:EDI_Batch_Number_SelectedBG LONG              !Selected background color
tmp:Cost               LIKE(tmp:Cost)                 !List box control field - type derived from local data
tmp:Cost_NormalFG      LONG                           !Normal forground color
tmp:Cost_NormalBG      LONG                           !Normal background color
tmp:Cost_SelectedFG    LONG                           !Selected forground color
tmp:Cost_SelectedBG    LONG                           !Selected background color
wob:EDI                LIKE(wob:EDI)                  !List box control field - type derived from field
wob:EDI_NormalFG       LONG                           !Normal forground color
wob:EDI_NormalBG       LONG                           !Normal background color
wob:EDI_SelectedFG     LONG                           !Selected forground color
wob:EDI_SelectedBG     LONG                           !Selected background color
job_ali:date_booked    LIKE(job_ali:date_booked)      !List box control field - type derived from field
job_ali:date_booked_NormalFG LONG                     !Normal forground color
job_ali:date_booked_NormalBG LONG                     !Normal background color
job_ali:date_booked_SelectedFG LONG                   !Selected forground color
job_ali:date_booked_SelectedBG LONG                   !Selected background color
job_ali:time_booked    LIKE(job_ali:time_booked)      !List box control field - type derived from field
job_ali:time_booked_NormalFG LONG                     !Normal forground color
job_ali:time_booked_NormalBG LONG                     !Normal background color
job_ali:time_booked_SelectedFG LONG                   !Selected forground color
job_ali:time_booked_SelectedBG LONG                   !Selected background color
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tmp:LoanStatusDate     LIKE(tmp:LoanStatusDate)       !Browse hot field - type derived from local data
tmp:ExchangeStatusDate LIKE(tmp:ExchangeStatusDate)   !Browse hot field - type derived from local data
tmp:StatusDate         LIKE(tmp:StatusDate)           !Browse hot field - type derived from local data
tmp:Current_StatusBase LIKE(tmp:Current_StatusBase)   !Browse hot field - type derived from local data
address:CompanyName    LIKE(address:CompanyName)      !Browse hot field - type derived from local data
address:AddressLine1   LIKE(address:AddressLine1)     !Browse hot field - type derived from local data
address:AddressLine2   LIKE(address:AddressLine2)     !Browse hot field - type derived from local data
address:AddressLine3   LIKE(address:AddressLine3)     !Browse hot field - type derived from local data
address:Postcode       LIKE(address:Postcode)         !Browse hot field - type derived from local data
address:TelephoneNumber LIKE(address:TelephoneNumber) !Browse hot field - type derived from local data
address:FaxNumber      LIKE(address:FaxNumber)        !Browse hot field - type derived from local data
tmp:ExchangeUnitDetails LIKE(tmp:ExchangeUnitDetails) !Browse hot field - type derived from local data
tmp:LoanUnitDetails    LIKE(tmp:LoanUnitDetails)      !Browse hot field - type derived from local data
tmp:JobDays            LIKE(tmp:JobDays)              !Browse hot field - type derived from local data
tmp:ExcStatusDays      LIKE(tmp:ExcStatusDays)        !Browse hot field - type derived from local data
tmp:LoaStatusDays      LIKE(tmp:LoaStatusDays)        !Browse hot field - type derived from local data
tmp:JobStatusDays      LIKE(tmp:JobStatusDays)        !Browse hot field - type derived from local data
tmp:ExchangeUnitDetails2 LIKE(tmp:ExchangeUnitDetails2) !Browse hot field - type derived from local data
tmp:LoanUnitDetails2   LIKE(tmp:LoanUnitDetails2)     !Browse hot field - type derived from local data
tmp:Loan_status        LIKE(tmp:Loan_status)          !Browse hot field - type derived from local data
tmp:Exchange_Status    LIKE(tmp:Exchange_Status)      !Browse hot field - type derived from local data
wob:ReadyToDespatch    LIKE(wob:ReadyToDespatch)      !Browse key field - type derived from field
wob:DespatchCourier    LIKE(wob:DespatchCourier)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK14::wob:Current_Status  LIKE(wob:Current_Status)
HK14::wob:DespatchCourier LIKE(wob:DespatchCourier)
HK14::wob:EDI             LIKE(wob:EDI)
HK14::wob:Exchange_Status LIKE(wob:Exchange_Status)
HK14::wob:HeadAccountNumber LIKE(wob:HeadAccountNumber)
HK14::wob:Loan_Status     LIKE(wob:Loan_Status)
HK14::wob:Manufacturer    LIKE(wob:Manufacturer)
HK14::wob:ModelNumber     LIKE(wob:ModelNumber)
HK14::wob:ReadyToDespatch LIKE(wob:ReadyToDespatch)
HK14::wob:SubAcountNumber LIKE(wob:SubAcountNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('ServiceBase 2000 Webmaster'),AT(0,0,678,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(221,350),USE(?CustomerServiceButton),TRN,FLAT,ICON('custserp.jpg')
                       PROMPT('Browse The Job File '),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(8,112,488,234),USE(?Browse),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Browsing Records'),FORMAT('70L(2)|M*~Job No~@s20@49L(2)|M*~Account~@s30@100L(2)|M*~Customer Name~@s30@70L(2' &|
   ')|M*~I.M.E.I.~@s30@110L(2)|M*~Current Status~@s33@71L(2)|M*~Location~@s30@10L(2)' &|
   '|M*~T~@s1@60L(2)|M*~Model~@s30@71L(2)|M*~Mobile Number~@s30@110L(2)|M*~Current S' &|
   'tatus~@s30@110L(2)|M*~Exchange Status~@s30@110L(2)|M*~Loan Status~@s30@120L(2)|M' &|
   '*~Order Number~@s30@100L(2)|M*~Courier~@s30@4L(2)|M~Browse Flag~@n1@0L(6)|M*@s30' &|
   '@0L(2)|M*@s8@0L(2)|M*~Job~@s8@0L(8)|M*~Ref~@s8@0L(2)|M*@s30@32R(2)|M*~Exchange U' &|
   'nit Number~L@s8@22L(2)|M*~Type~@s3@32R(2)|M*~Loan Unit Number~L@s8@120L(2)|M*~Cu' &|
   'rrent Status~@s30@35R(2)|M*~Batch No~@n_6@56R(2)|M*~Cost~@n-14.2@18L(2)|M*~EDI~R' &|
   '@s3@48L(2)|M*~Date Booked~R@d6b@48L(2)|M*~Time Booked~R@t1@10L(2)|MI~X~@s1@'),FROM(Queue:Browse:1)
                       BUTTON,AT(608,80),USE(?PrintButton),TRN,FLAT,LEFT,ICON('prnroutp.jpg')
                       BUTTON,AT(150,350),USE(?ViewBookingDetails),TRN,FLAT,LEFT,ICON('boodetp.jpg')
                       BUTTON,AT(296,350),USE(?ViewBookingDetails:2),TRN,FLAT,LEFT,ICON('engdetp.jpg')
                       BUTTON,AT(8,350),USE(?SingleJobButton),TRN,FLAT,LEFT,ICON('sinjobp.jpg')
                       BUTTON,AT(79,350),USE(?MultJobButton),TRN,FLAT,LEFT,ICON('muljobp.jpg')
                       BUTTON,AT(364,384),USE(?HTMLButton),TRN,FLAT,HIDE,LEFT,MSG('http://217.34.121.178:100/PUBLIC/NewVoda/WM30ENQ.EXE.0'),KEY(AltShiftMouseCenter2),ICON('htmlp.jpg')
                       BUTTON,AT(436,384),USE(?Button31),TRN,FLAT,LEFT,ICON('keyp.jpg')
                       STRING(@s30),AT(176,386),USE(glo:Location),FONT('Tahoma',8,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Time Booked:'),AT(8,398),USE(?Prompt25),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@d6b),AT(72,386),USE(job_ali:date_booked),TRN,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       GROUP('Status Details'),AT(512,218,160,158),USE(?StatusDetails:Group),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Job Status'),AT(520,230),USE(?Prompt7),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@d6b),AT(620,230),USE(tmp:StatusDate),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(520,238,144,10),USE(tmp:Current_StatusBase),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         STRING('Job Days:'),AT(520,250),USE(?String7),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s5),AT(556,250),USE(tmp:JobDays),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         STRING('Status Days:'),AT(596,250),USE(?String7:2),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s5),AT(640,250),USE(tmp:JobStatusDays),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         PROMPT('Exchange Status'),AT(520,272),USE(?Prompt8),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@d6b),AT(620,272),USE(tmp:ExchangeStatusDate),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(520,280,144,10),USE(tmp:Exchange_Status),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         STRING('Status Days:'),AT(520,292),USE(?String7:3),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s5),AT(564,292),USE(tmp:ExcStatusDays),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         STRING(@s60),AT(520,302,144,),USE(tmp:ExchangeUnitDetails),FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                         STRING(@s60),AT(520,310,144,),USE(tmp:ExchangeUnitDetails2),FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                         PROMPT('Loan Status'),AT(520,324),USE(?Prompt9),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@d6b),AT(620,324),USE(tmp:LoanStatusDate),FONT(,7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(520,332,144,10),USE(tmp:Loan_status),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         STRING('Status Days:'),AT(520,344),USE(?String7:4),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s5),AT(564,344),USE(tmp:LoaStatusDays),FONT(,7,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         STRING(@s60),AT(520,352,144,),USE(tmp:LoanUnitDetails),FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                         STRING(@s60),AT(520,360,144,),USE(tmp:LoanUnitDetails2),FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                       END
                       BUTTON,AT(522,384),USE(?CountJobsButton),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                       SHEET,AT(4,28,672,352),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Job Number'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(432,350),USE(?RefreshViewJobs),TRN,FLAT,ICON('refviewp.jpg')
                           PROMPT('Job Number'),AT(8,84),USE(?wob:JobNumber:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(8,92,64,10),USE(wob:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                         END
                         TAB('By Statuses'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(8,46,300,60),USE(?StatusSheet),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                             TAB('By Job Status'),USE(?TabJobStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                               ENTRY(@s30),AT(12,72,124,10),USE(temp:Current_Status),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                               BUTTON,AT(140,68),USE(?CallLookup),TRN,FLAT,ICON('lookup2p.jpg')
                               PROMPT('Current Status'),AT(12,64),USE(?Prompt11),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                             END
                             TAB('By Exchange Status'),USE(?TabExchangeStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                               ENTRY(@s30),AT(12,72,124,10),USE(temp:Exchange_Status),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                               BUTTON,AT(140,68),USE(?CallLookup:2),TRN,FLAT,ICON('lookup2p.jpg')
                               PROMPT('Exchange Status'),AT(12,64),USE(?Prompt12),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                             END
                             TAB('By Loan Status'),USE(?TabLoanStatus),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                               ENTRY(@s30),AT(12,72,124,10),USE(temp:Loan_Status),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                               BUTTON,AT(140,68),USE(?CallLookup:3),TRN,FLAT,ICON('lookup2p.jpg')
                               PROMPT('Loan Status'),AT(12,64),USE(?Prompt13),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                             END
                           END
                           PROMPT('Job Number'),AT(12,84),USE(?wob:RefNumber:Prompt),TRN,FONT(,7,,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(12,92,64,10),USE(wob:RefNumber,,?wob:RefNumber:4),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Link to JOBS RefNumber'),TIP('Link to JOBS RefNumber'),UPR
                           BUTTON,AT(522,156),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(522,129),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(608,156),USE(?ButtonSendSMS),FLAT,ICON('smstagjp.jpg')
                           BUTTON('&Rev tags'),AT(616,158,1,1),USE(?DASREVTAG),HIDE
                           BUTTON,AT(522,183),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON,AT(608,183),USE(?ButtonTagFromCSV),TRN,FLAT,ICON('tagjobcp.jpg')
                           BUTTON('sho&W tags'),AT(616,172,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(436,350),USE(?Button27),TRN,FLAT,LEFT,ICON('refviewp.jpg')
                         END
                         TAB('By Order Number'),USE(?Tab:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Order Number'),AT(8,84),USE(?wob:OrderNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,92,124,10),USE(wob:OrderNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Order Number'),TIP('Order Number'),UPR
                         END
                         TAB('By Mobile Number'),USE(?Tab:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Mobile Number'),AT(8,84),USE(?wob:MobileNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,92,124,10),USE(wob:MobileNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Mobile Number'),TIP('Mobile Number'),UPR
                         END
                         TAB('By Model Number'),USE(?Tab:7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Model Number'),AT(8,68),USE(?wob:ModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,76,124,10),USE(tmp:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Model Number'),TIP('Model Number'),UPR
                           BUTTON,AT(136,72),USE(?CallLookup:5),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Job Number'),AT(8,88),USE(?wob:RefNumber:Prompt:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(8,98,64,10),USE(wob:RefNumber,,?wob:RefNumber:5),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Link to JOBS RefNumber'),TIP('Link to JOBS RefNumber'),UPR
                         END
                         TAB('By I.M.E.I.'),USE(?Tab:8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Global IMEI Number Search'),AT(128,84),USE(?Glo:imeiNumberPrompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(8,84),USE(?wob:IMEINumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(128,92,124,10),USE(tmp:GlobalIMEINumber),FONT(,,,FONT:bold),COLOR(COLOR:White)
                           ENTRY(@s30),AT(8,92,100,10),USE(wob:IMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR
                         END
                         TAB('By Account'),USE(?Tab:9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account'),AT(8,66),USE(?wob:Account:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,74,124,10),USE(tmp:SubAccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                           BUTTON,AT(136,70),USE(?CallLookup:6),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Job Number'),AT(8,90),USE(?wob:RefNumber:Prompt:3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(8,98,64,10),USE(wob:RefNumber,,?wob:RefNumber:6),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Link to JOBS RefNumber'),TIP('Link to JOBS RefNumber'),UPR
                         END
                         TAB('Warranty Claim'),USE(?Tab:10),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(432,350),USE(?RefreshViewButton),TRN,FLAT,LEFT,ICON('refviewp.jpg')
                           OPTION,AT(4,42,188,40),USE(tmp:EDI)
                             RADIO('Rejected Claims'),AT(88,58),USE(?tmp:EDI:Radio1),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('EXC')
                             RADIO('Paid Claims'),AT(88,46),USE(?tmp:EDI:Radio4),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('PAY')
                             RADIO('Accepted Rejected Claims'),AT(88,70),USE(?tmp:EDI:Radio4:2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('AAJ')
                             RADIO('Pending Claims'),AT(8,46),USE(?tmp:EDI:Radio2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                             RADIO('Approved Claims'),AT(8,70),USE(?tmp:EDI:Radio5),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('APP')
                             RADIO('In Process Claims'),AT(8,58),USE(?tmp:EDI:Radio3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                           END
                           CHECK('Select Manufacturer'),AT(104,82),USE(Tmp:UseWarMan),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           ENTRY(@s30),AT(104,94,124,10),USE(tmp:WarManufacturer),HIDE,FONT(,,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(232,90),USE(?CallLookup:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           GROUP('Warranty'),AT(512,106,160,68),USE(?Warranty:Group),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(522,114,64,27),USE(?ValuateClaims),TRN,FLAT,LEFT,ICON('valclamp.jpg')
                             BUTTON,AT(608,114),USE(?ProcessRejectionButton),TRN,FLAT,HIDE,LEFT,ICON('prorejp.jpg')
                             BUTTON,AT(608,144),USE(?ClaimPaid),TRN,FLAT,HIDE,LEFT,ICON('clampayp.jpg')
                           END
                           ENTRY(@s8),AT(8,94,72,10),USE(wob:RefNumber,,?wob:RefNumber:2),FONT(,,,FONT:bold),COLOR(COLOR:White),MSG('Link to JOBS RefNumber')
                           PROMPT('Job Number'),AT(8,86),USE(?Prompt17),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('EDI Batch Number:'),AT(200,50),USE(?Prompt19),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n6),AT(276,50),USE(job_ali:EDI_Batch_Number),TRN,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                         TAB('Despatch'),USE(?Tab:11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Select Courier'),AT(8,41,252,24),USE(Select_Courier_temp),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(12,49),USE(?Select_Courier_temp:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Individual'),AT(44,49),USE(?Select_Courier_temp:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                           END
                           ENTRY(@s30),AT(104,49,124,10),USE(Courier_Temp),HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(522,50),USE(?CancelJobButton),TRN,FLAT,HIDE,LEFT,ICON('canjobp.jpg')
                           BUTTON,AT(232,46),USE(?CourierLookupButton),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           OPTION('Select Trade Account'),AT(8,65,252,24),USE(Select_Trade_Account_temp),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(12,73),USE(?Select_Trade_Account_temp:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Individual'),AT(44,73),USE(?Select_Trade_Account_temp:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                           END
                           ENTRY(@s30),AT(104,73,124,10),USE(Account_Number2_temp),HIDE,FONT(,,,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(232,70),USE(?TradeLookupButton),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s8),AT(8,92,64,10),USE(wob:RefNumber,,?wob:RefNumber:3),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Link to JOBS RefNumber'),TIP('Link to JOBS RefNumber'),UPR
                           PROMPT('Job Number'),AT(76,93),USE(?Prompt20),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Despatch'),AT(513,106,159,68),USE(?Despatch:Group),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(522,116),USE(?IndivDespatchButton),TRN,FLAT,LEFT,ICON('inddesp.jpg')
                             BUTTON,AT(608,116),USE(?MultDespatchButton),TRN,FLAT,LEFT,ICON('muldesp.jpg')
                             BUTTON,AT(608,144),USE(?ChangeCourierButton),TRN,FLAT,LEFT,ICON('chancoup.jpg')
                           END
                         END
                       END
                       ENTRY(@s30),AT(320,54,124,8),USE(address:CompanyName),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Company Name'),TIP('Company Name'),UPR,READONLY
                       PANEL,AT(316,50,180,58),USE(?AddressPanel),FILL(09A6A7CH),BEVEL(-1,-1)
                       PROMPT('Address:'),AT(316,42),USE(?Address),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(320,62,124,8),USE(address:AddressLine1),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Address'),TIP('Address'),UPR,READONLY
                       ENTRY(@s30),AT(320,70,124,8),USE(address:AddressLine2),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Address Line 3'),TIP('Address Line 3'),UPR,READONLY
                       ENTRY(@s30),AT(320,78,124,8),USE(address:AddressLine3),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('AddressLine3'),TIP('AddressLine3'),UPR,READONLY
                       ENTRY(@s30),AT(320,86,124,8),USE(address:Postcode),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Postcode'),TIP('Postcode'),UPR,READONLY
                       PROMPT('Phone/Fax:'),AT(320,96),USE(?TelephoneNo),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(368,96,60,8),USE(address:TelephoneNumber),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Telephone Number'),TIP('Telephone Number'),UPR,READONLY
                       ENTRY(@s30),AT(432,96,60,8),USE(address:FaxNumber),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Fax Number'),TIP('Fax Number'),UPR,READONLY
                       BUTTON,AT(522,80),USE(?payment_details),TRN,FLAT,LEFT,ICON('paydetp.jpg')
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       STRING(@t1),AT(72,398),USE(job_ali:time_booked),TRN,FONT(,10,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       PROMPT('Date Booked:'),AT(8,386),USE(?Prompt24),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(608,50),USE(?ChangeStatusButton),TRN,FLAT,LEFT,ICON('chastap.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 1
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 2
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 3
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 6
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 7
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 1
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 0
BRW1::Sort8:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'ALL' And Upper(Select_Courier_Temp) = 'ALL'
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'IND' And Upper(Select_Courier_Temp) = 'ALL'
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'ALL' And Upper(Select_Courier_Temp) = 'IND'
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'IND' And Upper(Select_Courier_Temp) = 'IND'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort2:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 4
BRW1::Sort3:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 5
BRW1::Sort4:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 6
BRW1::Sort5:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 7
BRW1::Sort6:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 1
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    Map
LocalValidateIMEI               Procedure(),byte
LocalValidateAccessories        Procedure(),byte
    End
TempFilePath         CSTRING(255)

ImportFile      FILE,DRIVER('BASIC'),PRE(IMP),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
JobNumber       long        !Jobnumber   
Spare1          String(30)  !spare
Spare2          String(30)  !spare
Spare3          String(30)  !spare
                    End     !record
                End         !file definition

!Save Entry Fields Incase Of Lookup
look:temp:Current_Status                Like(temp:Current_Status)
look:temp:Exchange_Status                Like(temp:Exchange_Status)
look:temp:Loan_Status                Like(temp:Loan_Status)
look:tmp:ModelNumber                Like(tmp:ModelNumber)
look:tmp:SubAccountNumber                Like(tmp:SubAccountNumber)
look:Courier_Temp                Like(Courier_Temp)
look:Account_Number2_temp                Like(Account_Number2_temp)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse))
  BRW1.UpdateBuffer
   glo:Queue20.Pointer20 = wob:RecordNumber
   GET(glo:Queue20,glo:Queue20.Pointer20)
  IF ERRORCODE()
     glo:Queue20.Pointer20 = wob:RecordNumber
     ADD(glo:Queue20,glo:Queue20.Pointer20)
    LocalTag = '*'
  ELSE
    DELETE(glo:Queue20)
    LocalTag = ''
  END
    Queue:Browse:1.LocalTag = LocalTag
  IF (localTag = '*')
    Queue:Browse:1.LocalTag_Icon = 2
  ELSE
    Queue:Browse:1.LocalTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse,CHOICE(?Browse))
DASBRW::6:DASTAGALL Routine
  ?Browse{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue20)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue20.Pointer20 = wob:RecordNumber
     ADD(glo:Queue20,glo:Queue20.Pointer20)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse,CHOICE(?Browse))
DASBRW::6:DASUNTAGALL Routine
  ?Browse{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue20)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse,CHOICE(?Browse))
DASBRW::6:DASREVTAGALL Routine
  ?Browse{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue20)
    GET(glo:Queue20,QR#)
    DASBRW::6:QUEUE = glo:Queue20
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue20)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer20 = wob:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer20)
    IF ERRORCODE()
       glo:Queue20.Pointer20 = wob:RecordNumber
       ADD(glo:Queue20,glo:Queue20.Pointer20)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse,CHOICE(?Browse))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse,CHOICE(?Browse))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
TagFromCsv      Routine

    If ClarioNET:GetFilesFromClient(1,'TEST',1) = 1
        get(FileListQueue,1)
        filename3 = flq:FileName
        if (NOT Exists(filename3))
            Exit
        end
    Else
        Exit
    end

    !first pass to count the number of entries
    Open(ImportFile)
    If (Error())
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Unable to open import file.'&|
            '|' & Clip(Error()) & ' : ' & Clip(FileError()) & '','ServiceBase',|
                       'mstop.jpg','/OK')
        Of 1 ! &OK Button
        End!Case Message
        exit
    end

    recCount# = 0
    set(ImportFile,0)
    Loop
        Next(ImportFile)
        If (Error())
            Break
        end
        recCount# += 1
    end

    Prog.ProgressSetup(recCount#)

    count# = 0

    !exception file name
    filename4 = 'SMS_'&format(today(),@D12)&'_'&format(clock(),@t5)&'.csv'

    !defaults
    Count = 0

    !ImportFile - importing data
    set(ImportFile,0)
    Loop
        next(ImportFile)
        if error() then break.

        !message('Got import file job number ='&clip(imp:JobNumber))
!       imp:JobNumber defines the job - job:Ref_Number
!       does this exist?       "Job not on servicebase"
        Access:jobs.clearkey(job:Ref_Number_Key)
        job:Ref_Number = imp:JobNumber
        if access:Jobs.fetch(job:Ref_Number_Key)
            !error

             Lineprint(clip(imp:jobNumber)&',Job not on ServiceBase',filename4)
             Count += 1
        ELSE
            Access:Webjob.clearkey(wob:RefNumberKey)
            wob:RefNumber = imp:jobNumber
            if access:Webjob.fetch(wob:RefNumberKey)
                !No webjob
                Lineprint(clip(imp:jobNumber)&',Corrupt Job/Web Record',filename4)
                Count += 1
            ELSE
        !       despatched special case (901,902,905,810) "Job despatched"
                if job:Current_Status[1] = '9' !or job:location = tmp:DespatchLocation then
                    Lineprint(clip(imp:jobNumber)&',Job despatched',filename4)
                    Count += 1
                ELSE
            !       Correct stauts         "Job not in correct status"
                    Case choice(?StatusSheet)
                        of 1    !job status
                            if job:Current_Status <>  temp:Current_Status
                                Lineprint(clip(imp:jobNumber)&',Job not on selected status '&clip(temp:Current_Status),filename4)
                                Count += 1
                            ELSE
                                glo:Queue20.Pointer20 = wob:RecordNumber
                                Add(Glo:Queue20)
                            END
                        of 2    !exchange status
                            if job:Exchange_Status <> temp:Exchange_Status
                                Lineprint(clip(imp:jobNumber)&',Exchange not on selected status '&clip(temp:Exchange_Status),filename4)
                                Count += 1
                            ELSE
                                glo:Queue20.Pointer20 = wob:RecordNumber
                                Add(Glo:Queue20)
                            END
                        of 3    !loan status
                            if job:Loan_Status <>  temp:Loan_Status
                                Lineprint(clip(imp:jobNumber)&',Loan not on selected status '&clip(temp:Loan_Status),filename4)
                                Count += 1
                            ELSE
                                glo:Queue20.Pointer20 = wob:RecordNumber
                                Add(Glo:Queue20)
                            END
                    END !Case
                END !if job despatched
            END !if wob fetch worked
        END !if jobs fetch worked

    END !loop through import file


    Prog.ProgressFinish()
    Close(ImportFile)

    if count > 0 then

        miss# = missive('Some jobs could not be tagged. An exception file has been created.','ServiceBase 3g','mexclam.jpg','OK')

        !send to client
        flq:Filename = filename4
        add(FileListQueue)
        Return" = ClarioNET:SendFilesToClient(1,0)

        remove(filename4)
        filename4 = ''
    END

    Case Missive('File Imported: '&clip(filename3)&|
        '|Record(s) tagged: ' & Clip(recCount# - count) & '','ServiceBase',|
                   'midea.jpg','/OK')
    Of 1 ! &OK Button
    End!Case Message

    remove(ImportFile)

    brw1.ResetSort(1)

    exit
TabChanging     Routine
!70     L             (2)           |      M        *        I     ~I.M.E.I.~  @s30@ #16#
!lengh  left/right    (indent)             resise   colour  icon    title   picture  number

    CASE CHOICE(?CurrentTab)
      OF 1

        ?Browse{PROP:FORMAT} ='70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s33@#21#71L(2)|M*~Location~@s30@#26#10L(2)|M*~T~@s1@#31#60L(2)|M*~Model~@s30@#36#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:1{PROP:TEXT} = 'By Job Number'

      OF 2

          !notes to self
          !"current status is showing no 21 - the tmp:current status field and that displays OK
          !Loan and exchange status are using the correct fields and not displaying OK for some values of status
          !what is happening???
          !Wob:CurrentStatus is in the list, but is not displayed, and is "HOT"
          !LETS DO THE SAME FOR EXCHANGE AND LOAN!!
          !THE colouring is wrong - it is showing what the next line should be not this one ... that may need sorting
          !tmp:Current_status is 33 characters long to allow "J " to be added

          !move taging first - was between location and account
          ?Browse{PROP:FORMAT} ='10L(2)|MI~X~@s1@#142#'&|       !was 146 seems to have changed!
                                '70L(2)|M*~Job No~@s20@#1#'&|
                                '90L(2)|M*~Order Number~@s30@#61#'&|
                                '10L(2)|M*~T~@s1@#31#'&|
                                '100L(2)|M*~Customer Name~@s30@#11#'&|
                                '60L(2)|M*~Model~@s30@#36#'&|
                                '70L(2)|M*~I.M.E.I.~@s30@#16#'&|
                                '71L(2)|M*~Location~@s30@#26#'&|
                                '49L(2)|M*~Account~@s30@#6#'&|
                                '71L(2)|M*~Mobile Number~@s30@#41#'&|
                                '110L(2)|M*~Current Status~@s33@#21#'&|
                                '100L(2)|M*~Courier~@s30@#66#'&|
                                '0L(2)|M~Browse Flag~@n1@#71#/'       !&|

                                !these were immediately after the current status #21#
                                !'110L(2)|M*~Exchange Status~@s30@#51#'&|
                                !'110L(2)|M*~Loan Status~@s30@#56#'&|              / n0

                                !these were removed as not needed
                                !'10L(6)|M*@s3@#76#'&|
                                !'10L(2)|M*@s8@#81#'&|
                                !'10L(2)|M*~Job~@s8@#86#'&|
                                !'10L(8)|M*~Ref~@s8@#91#'&|
                                !'10L(2)|M*@s30@#96#/'
                                !32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'

!          ?Tab:2{PROP:TEXT} = 'By Statuses'
!
!        Case Choice(?StatusSheet)
!            Of 2            !
!              ?Browse{PROP:FORMAT} ='10L(2)|MI~X~@s1@#146#'&|
!                                    '70L(2)|M*~Job No~@s20@#1#'&|
!                                    '90L(2)|M*~Order Number~@s30@#61#'&|
!                                    '10L(2)|M*~T~@s1@#31#1'&|
!                                    '00L(2)|M*~Customer Name~@s30@#11#'&|
!                                    '60L(2)|M*~Model~@s30@#36#'&|
!                                    '70L(2)|M*~I.M.E.I.~@s30@#16#'&|
!                                    '71L(2)|M*~Location~@s30@#26#'&|
!                                    '49L(2)|M*~Account~@s30@#6#'&|
!                                    '110L(2)|M*~Current Status~@s30@#21#'&|
!                                    '71L(2)|M*~Mobile Number~@s30@#41#'&|
!                                    '110L(2)|M*~Current Status~@s30@#46#'&|
!                                    '110L(2)|M*~Loan Status~@s30@#56#'&|
!                                    '100L(2)|M*~Courier~@s30@#66#'&|
!                                    '4L(2)|M*~Browse Flag~@n1@#71#'&|
!                                    '0L(6)|M*@s3@#76#'&|
!                                    '0L(2)|M*@s8@#81#'&|
!                                    '0L(2)|M*~Job~@s8@#86#'&|
!                                    '0L(8)|M*~Ref~@s8@#91#'&|
!                                    '0L(2)|M*@s30@#96#/'
!                                    !32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
!              ?TabExchangeStatus{PROP:TEXT} = 'By Exchange Status'
!
!            Of 3
!
!              ?Browse{PROP:FORMAT} ='10L(2)|MI~X~@s1@#146#'&|
!                                    '70L(2)|M*~Job No~@s20@#1#'&|
!                                    '90L(2)|M*~Order Number~@s30@#61#'&|
!                                    '10L(2)|M*~T~@s1@#31#'&|
!                                    '100L(2)|M*~Customer Name~@s30@#11#'&|
!                                    '60L(2)|M*~Model~@s30@#36#'&|
!                                    '70L(2)|M*~I.M.E.I.~@s30@#16#'&|
!                                    '71L(2)|M*~Location~@s30@#26#'&|
!                                    '49L(2)|M*~Account~@s30@#6#'&|
!                                    '110L(2)|M*~Current Status~@s30@#21#'&|
!                                    '71L(2)|M*~Mobile Number~@s30@#41#'&|
!                                    '110L(2)|M*~Current Status~@s30@#46#'&|
!                                    '110L(2)|M*~Exchange Status~@s30@#51#'&|
!                                    '120L(2)|M*~Order Number~@s30@#61#'&|
!                                    '100L(2)|M*~Courier~@s30@#66#'&|
!                                    '4L(2)|M*~Browse Flag~@n1@#71#'&|
!                                    '0L(6)|M*@s30@#76#'&|
!                                    '0L(2)|M*@s8@#81#'&|
!                                    '0L(2)|M*~Job~@s8@#86#'&|
!                                    '0L(8)|M*~Ref~@s8@#91#'&|
!                                    '0L(2)|M*@s30@#96#/'
!                                    !32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
!              ?TabLoanStatus{PROP:TEXT} = 'By Loan Status'
!
!        End ! Case Choice(?StatusSheet)

      OF 3
        ?Browse{PROP:FORMAT} ='120L(2)|M*~Order Number~@s30@#61#70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s30@#21#71L(2)|M*~Location~@s30@#26#10L(2)|M*~T~@s1@#31#60L(2)|M*~Model~@s30@#36#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#' !32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:5{PROP:TEXT} = 'By Order Number'
      OF 4
        ?Browse{PROP:FORMAT} ='71L(2)|M*~Mobile Number~@s30@#41#70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s30@#21#60L(2)|M*~Model~@s30@#36#71L(2)|M*~Location~@s30@#26#10L(2)|M*~T~@s1@#31#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:6{PROP:TEXT} = 'By Mobile Number'
      OF 5
        ?Browse{PROP:FORMAT} ='70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s30@#21#71L(2)|M*~Location~@s30@#26#10L(2)|M*~T~@s1@#31#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:7{PROP:TEXT} = 'By Model Number'
      OF 6
        ?Browse{PROP:FORMAT} ='70L(2)|M*~I.M.E.I.~@s30@#16#60L(2)|M*~Model~@s30@#36#70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#110L(2)|M*~Current Status~@s30@#21#71L(2)|M*~Location~@s30@#26#10L(2)|M*~T~@s1@#31#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:8{PROP:TEXT} = 'By I.M.E.I.'
      OF 7
        ?Browse{PROP:FORMAT} ='70L(2)|M*~Job No~@s20@#1#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s30@#21#10L(2)|M*~T~@s1@#31#71L(2)|M*~Location~@s30@#26#60L(2)|M*~Model~@s30@#36#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:9{PROP:TEXT} = 'By Account'
      OF 8
        ?Browse{PROP:FORMAT} ='70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s30@#21#71L(2)|M*~Location~@s30@#26#10L(2)|M*~T~@s1@#31#60L(2)|M*~Model~@s30@#36#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#100L(2)|M*~Courier~@s30@#66#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:10{PROP:TEXT} = 'Warranty Claim'
      OF 9
        ?Browse{PROP:FORMAT} ='70L(2)|M*~Job No~@s20@#1#49L(2)|M*~Account~@s30@#6#100L(2)|M*~Customer Name~@s30@#11#70L(2)|M*~I.M.E.I.~@s30@#16#110L(2)|M*~Current Status~@s30@#21#100L(2)|M*~Courier~@s30@#66#10L(2)|M*~T~@s1@#31#71L(2)|M*~Location~@s30@#26#60L(2)|M*~Model~@s30@#36#71L(2)|M*~Mobile Number~@s30@#41#110L(2)|M*~Current Status~@s30@#46#110L(2)|M*~Exchange Status~@s30@#51#110L(2)|M*~Loan Status~@s30@#56#120L(2)|M*~Order Number~@s30@#61#4L(2)|M*~Browse Flag~@n1@#71#0L(6)|M*@s30@#76#0L(2)|M*@s8@#81#0L(2)|M*~Job~@s8@#86#0L(8)|M*~Ref~@s8@#91#0L(2)|M*@s30@#96#32R(2)|M*~Exchange Unit Number~L@s8@#101#22L(2)|M*~Type~@s3@#106#32R(2)|M*~Loan Unit Number~L@s8@#111#120L(2)|M*~Current Status~@s30@#116#35R(2)|M*~Batch No~@n_6@#121#12L(2)|M*~EDI~R@s3@#131#'
        ?Tab:11{PROP:TEXT} = 'Despatch'
    END
RefreshStatus    routine

    !Warn about time
    Case Missive('This procedure will take a long time to complete and once started cannot be interrupted.'&|
      '<13,10>'&|
      '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button

            Prog.ProgressSetup(Records(WEBJOB))

            save_job_id = access:webjob.savefile()

            temp# = 0
            access:webjob.clearkey(wob:HeadRefNumberKey)
            wob:HeadAccountNumber = LocalTradeAcc
            wob:RefNumber = 0
            set(wob:HeadRefNumberKey,wob:HeadRefNumberKey)
            loop
                if access:webjob.next() then break.
                if wob:HeadAccountNumber <> LocalTradeAcc then break.

                If Prog.InsideLoop()
                    Break
                End ! If Prog.InsideLoop()

                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_Number  = wob:RefNumber
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign


                !Now to fix the status dates
                Save_aus_ID = Access:AUDSTATS.SaveFile()
                Access:AUDSTATS.ClearKey(aus:StatusTimeKey)
                aus:RefNumber   = wob:RefNumber
                aus:Type        = 'JOB'
                aus:DateChanged = Today()
                aus:TimeChanged = clock()
                Set(aus:StatusTimeKey,aus:StatusTimeKey)
                Loop
                    If Access:AUDSTATS.PREVIOUS()
                       Break
                    End !If
                    If aus:RefNumber   <> wob:RefNumber      |
                    Or aus:Type        <> 'JOB'      |
                        Then Break.  ! End If
                    wob:Current_Status_Date  = aus:DateChanged
                    wob:current_Status       = aus:NewStatus
                    If job:Current_Status <> aus:NewStatus
                        job:Current_Status = aus:NewStatus
                        job:PreviousStatus = aus:OldStatus
                        job:StatusUser     = aus:UserCode
                    End !If job:Current_Status <> aus:NewStatus

                    Break
                End !Loop
                Access:AUDSTATS.RestoreFile(Save_aus_ID)


                Save_aus_ID = Access:AUDSTATS.SaveFile()
                Access:AUDSTATS.ClearKey(aus:StatusTimeKey)
                aus:RefNumber   = wob:RefNumber
                aus:Type        = 'LOA'
                aus:DateChanged = Today()
                aus:TimeChanged = clock()
                Set(aus:StatusTimeKey,aus:StatusTimeKey)
                Loop
                    If Access:AUDSTATS.PREVIOUS()
                       Break
                    End !If
                    If aus:RefNumber   <> wob:RefNumber      |
                    Or aus:Type        <> 'LOA'      |
                        Then Break.  ! End If
                    wob:Loan_Status_Date  =  aus:DateChanged
                    wob:loan_Status = aus:NewStatus
                    If job:Loan_Status <> aus:NewStatus
                        job:Loan_Status = aus:NewStatus
                    End !If job:Current_Status <> aus:NewStatus
                    Break
                End !Loop
                Access:AUDSTATS.RestoreFile(Save_aus_ID)


                Save_aus_ID = Access:AUDSTATS.SaveFile()
                Access:AUDSTATS.ClearKey(aus:StatusTimeKey)
                aus:RefNumber   = wob:RefNumber
                aus:Type        = 'EXC'
                aus:DateChanged = Today()
                aus:TimeChanged = clock()
                Set(aus:StatusTimeKey,aus:StatusTimeKey)
                Loop
                    If Access:AUDSTATS.PREVIOUS()
                       Break
                    End !If
                    If aus:RefNumber   <> wob:RefNumber      |
                    Or aus:Type        <> 'EXC'      |
                        Then Break.  ! End If
                    wob:Exchange_Status_Date  =  aus:DateChanged
                    wob:Exchange_Status = aus:NewStatus
                    If job:Exchange_Status <> aus:NewStatus
                        job:Exchange_Status = aus:NewStatus
                    End !If job:Current_Status <> aus:NewStatus
                    Break
                End !Loop
                Access:AUDSTATS.RestoreFile(Save_aus_ID)
                temp# += 1
                access:webjob.update()
                Access:JOBS.Update()
            END!loop through Wobs
            Prog.ProgressFinish()

            Case Missive('' & Clip(temp#) & ' jobs updated.','ServiceBase 3g',|
                           'midea.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            access:webjob.restorefile(save_job_id)
            BRW1.ResetSort(1)

        Of 1 ! No Button
    End ! Case Missive
Security_Check      Routine

    if securitycheck('AUTO CHANGE COURIER') = level:benign
        enable(?ChangeCourierButton)
    else!if securitycheck('AUTO CHANGE COURIER') = level:benign
        disable(?ChangeCourierButton)
    end!if securitycheck('AUTO CHANGE COURIER') = level:benign


!!! - RAPID UPDATE - !!!


!!! - DESPATCH - !!!
    if securitycheck('MULTIPLE DESPATCH') = level:benign
        enable(?MultDespatchButton)
    else!if securitycheck('RAPID - STATUS CHANGE') = level:benign
        disable(?MultDespatchButton)
    end

    if securitycheck('INDIVIDUAL DESPATCH') = level:benign
        enable(?IndivDespatchButton)
    else
        disable(?IndivDespatchButton)
    end

!!! - JOBS - !!!
    if securitycheck('PAYMENT DETAILS') = level:benign
        enable(?payment_details)
    else
        disable(?payment_details)
    end

Create_Invoice        Routine
Data
local:NewInvoice        Byte(0)
Code

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = wob:SubAcountNumber
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        If CanInvoiceBePrinted(wob:RefNumber,1) = Level:Benign
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = wob:RefNumber
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
                If job:Invoice_Number = 0
                    local:NewInvoice = 1
                Else ! If job:Invoice_Number = 0
                    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                    inv:Invoice_Number = job:Invoice_Number
                    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                        !Found
                        If inv:RRCInvoiceDate = 0
                            local:NewInvoice = 1
                        End ! If inv:RRCInvoiceDate = 0
                    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                End ! If job:Invoice_Number = 0

                If local:NewInvoice = 1
                    Case Missive('Are you sure you want to create an Invoice for the selected job?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            If CreateInvoice() <> Level:Benign
                                Exit
                            End ! If CreateInvoice() = Level:Benign
                        Of 1 ! No Button
                            Exit
                    End ! Case Missive
                End ! If local:NewInvoice = 1

                FoundCredit# = 0
                Access:JOBSINV.Clearkey(jov:TypeRecordKey)
                jov:RefNumber = job:Ref_Number
                jov:Type = 'I'
                Set(jov:TypeRecordKey,jov:TypeRecordKey)
                Loop ! Begin Loop
                    If Access:JOBSINV.Next()
                        Break
                    End ! If Access:JOBSINV.Next()
                    If jov:RefNumber <> job:Ref_Number
                        Break
                    End ! If jov:RefNumber <> job:Ref_Number
                    If jov:Type <> 'I'
                        Break
                    End ! If jov:Type <> 'I'
                    FoundCredit# = 1
                    Break
                End ! Loop

                If FoundCredit#
                    ! Invoice has been credited, bring up window to select invoice (DBH: 02/08/2007)
                    SelectInvoiceCreditNote(wob:RefNumber,'I')
                Else ! If FoundCredit#
                    glo:Select1 = job:Invoice_Number
                    VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','') ! ! #11881 New Single Invoice (Bryan: 16/03/2011)
!                    If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!                        Vodacom_Delivery_Note_web('','')
!                    Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!                        Single_Invoice('','')
!                    End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
                    glo:Select1 = ''
                End ! If FoundCredit#
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        Else ! If CanInvoiceBePrinted(wob:RefNumber,1) = Level:Benign
            Exit
        End ! If CanInvoiceBePrinted(wob:RefNumber,1) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
        !Error
        Exit
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020501'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseWebJobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CustomerServiceButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EDIBATCH.Open
  Relate:EXCHANGE.Open
  Relate:LOGGED.Open
  Relate:MULDESPJ.Open
  Relate:MULDESP_ALIAS.Open
  Relate:STATUS.Open
  Relate:TRADEAC2.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WAYBCONF.Open
  Relate:WEBJOB.Open
  Access:JOBS_ALIAS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  Access:LOAN.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:AUDSTATS.UseFile
  Access:LOCATLOG.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:JOBSE2.UseFile
  Access:JOBSWARR.UseFile
  Access:JOBSINV.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:WEBJOB,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  LocalTradeAcc = Clarionet:Global.Param2
  access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = LocalTradeAcc
  access:tradeacc.fetch(tra:Account_Number_Key)
  tmp:tradeID = tra:BranchIdentification
  set(defaults)
  access:defaults.next()
  
  Select_Courier_temp = 'ALL'
  Select_Trade_Account_temp = 'ALL'
  
  do Security_Check
  
  !if ClarioNET:Global.Param5[len(clip(ClarioNET:Global.param5))] <> '/' then
  !    !we have the wrong param5 - must be the wrong connect program
  !    hide(?HTMLButton)
  !ELSE
      ?HTMLButton{prop:msg}= GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI')
  !END
  
  Do TabChanging
  ! Save Window Name
   AddToLog('Window','Open','BrowseWebJobs')
  ?Browse{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?temp:Current_Status{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?temp:Current_Status{Prop:Tip}
  END
  IF ?temp:Current_Status{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?temp:Current_Status{Prop:Msg}
  END
  IF ?temp:Exchange_Status{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?temp:Exchange_Status{Prop:Tip}
  END
  IF ?temp:Exchange_Status{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?temp:Exchange_Status{Prop:Msg}
  END
  IF ?temp:Loan_Status{Prop:Tip} AND ~?CallLookup:3{Prop:Tip}
     ?CallLookup:3{Prop:Tip} = 'Select ' & ?temp:Loan_Status{Prop:Tip}
  END
  IF ?temp:Loan_Status{Prop:Msg} AND ~?CallLookup:3{Prop:Msg}
     ?CallLookup:3{Prop:Msg} = 'Select ' & ?temp:Loan_Status{Prop:Msg}
  END
  IF ?tmp:WarManufacturer{Prop:Tip} AND ~?CallLookup:4{Prop:Tip}
     ?CallLookup:4{Prop:Tip} = 'Select ' & ?tmp:WarManufacturer{Prop:Tip}
  END
  IF ?tmp:WarManufacturer{Prop:Msg} AND ~?CallLookup:4{Prop:Msg}
     ?CallLookup:4{Prop:Msg} = 'Select ' & ?tmp:WarManufacturer{Prop:Msg}
  END
  IF ?tmp:ModelNumber{Prop:Tip} AND ~?CallLookup:5{Prop:Tip}
     ?CallLookup:5{Prop:Tip} = 'Select ' & ?tmp:ModelNumber{Prop:Tip}
  END
  IF ?tmp:ModelNumber{Prop:Msg} AND ~?CallLookup:5{Prop:Msg}
     ?CallLookup:5{Prop:Msg} = 'Select ' & ?tmp:ModelNumber{Prop:Msg}
  END
  IF ?tmp:SubAccountNumber{Prop:Tip} AND ~?CallLookup:6{Prop:Tip}
     ?CallLookup:6{Prop:Tip} = 'Select ' & ?tmp:SubAccountNumber{Prop:Tip}
  END
  IF ?tmp:SubAccountNumber{Prop:Msg} AND ~?CallLookup:6{Prop:Msg}
     ?CallLookup:6{Prop:Msg} = 'Select ' & ?tmp:SubAccountNumber{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,wob:HeadCurrentStatusKey)
  BRW1.AddRange(wob:Current_Status)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?wob:RefNumber:4,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadExchangeStatus)
  BRW1.AddRange(wob:Exchange_Status)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?wob:RefNumber:4,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadLoanStatusKey)
  BRW1.AddRange(wob:Loan_Status)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?wob:RefNumber:4,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadOrderNumberKey)
  BRW1.AddRange(wob:HeadAccountNumber)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?wob:OrderNumber,wob:OrderNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadMobileNumberKey)
  BRW1.AddRange(wob:HeadAccountNumber)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?wob:MobileNumber,wob:MobileNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadModelNumberKey)
  BRW1.AddRange(wob:ModelNumber)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?wob:RefNumber:5,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadIMEINumberKey)
  BRW1.AddRange(wob:HeadAccountNumber)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?wob:IMEINumber,wob:IMEINumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadSubKey)
  BRW1.AddRange(wob:SubAcountNumber)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?wob:RefNumber:6,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:EDIKey)
  BRW1.AddRange(wob:Manufacturer)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?wob:RefNumber:2,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:EDIRefNumberKey)
  BRW1.AddRange(wob:EDI)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?wob:RefNumber:2,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:DespatchKey)
  BRW1.AddRange(wob:ReadyToDespatch)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?wob:RefNumber:3,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:DespatchSubKey)
  BRW1.AddRange(wob:SubAcountNumber)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?wob:RefNumber:3,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:DespatchCourierKey)
  BRW1.AddRange(wob:DespatchCourier)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?wob:RefNumber:3,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:DespatchSubCourierKey)
  BRW1.AddRange(wob:DespatchCourier)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?wob:RefNumber:3,wob:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,wob:HeadRefNumberKey)
  BRW1.AddRange(wob:HeadAccountNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?wob:RefNumber,wob:RefNumber,1,BRW1)
  BIND('tmp:wob_jobNumber',tmp:wob_jobNumber)
  BIND('tmp:Company_Name',tmp:Company_Name)
  BIND('tmp:Current_status',tmp:Current_status)
  BIND('tmp:Location',tmp:Location)
  BIND('tmp:Type',tmp:Type)
  BIND('tmp:Courier',tmp:Courier)
  BIND('tmp:BrowseFlag',tmp:BrowseFlag)
  BIND('tmp:Cost',tmp:Cost)
  BIND('LocalTag',LocalTag)
  BIND('tmp:LoanStatusDate',tmp:LoanStatusDate)
  BIND('tmp:ExchangeStatusDate',tmp:ExchangeStatusDate)
  BIND('tmp:StatusDate',tmp:StatusDate)
  BIND('tmp:Current_StatusBase',tmp:Current_StatusBase)
  BIND('address:CompanyName',address:CompanyName)
  BIND('address:AddressLine1',address:AddressLine1)
  BIND('address:AddressLine2',address:AddressLine2)
  BIND('address:AddressLine3',address:AddressLine3)
  BIND('address:Postcode',address:Postcode)
  BIND('address:TelephoneNumber',address:TelephoneNumber)
  BIND('address:FaxNumber',address:FaxNumber)
  BIND('tmp:ExchangeUnitDetails',tmp:ExchangeUnitDetails)
  BIND('tmp:LoanUnitDetails',tmp:LoanUnitDetails)
  BIND('tmp:JobDays',tmp:JobDays)
  BIND('tmp:ExcStatusDays',tmp:ExcStatusDays)
  BIND('tmp:LoaStatusDays',tmp:LoaStatusDays)
  BIND('tmp:JobStatusDays',tmp:JobStatusDays)
  BIND('tmp:ExchangeUnitDetails2',tmp:ExchangeUnitDetails2)
  BIND('tmp:LoanUnitDetails2',tmp:LoanUnitDetails2)
  BIND('tmp:Loan_status',tmp:Loan_status)
  BIND('tmp:Exchange_Status',tmp:Exchange_Status)
  BIND('tmp:EDI',tmp:EDI)
  BIND('tmp:Address',tmp:Address)
  ?Browse{PROP:IconList,1} = '~notick1.ico'
  ?Browse{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tmp:wob_jobNumber,BRW1.Q.tmp:wob_jobNumber)
  BRW1.AddField(wob:SubAcountNumber,BRW1.Q.wob:SubAcountNumber)
  BRW1.AddField(tmp:Company_Name,BRW1.Q.tmp:Company_Name)
  BRW1.AddField(wob:IMEINumber,BRW1.Q.wob:IMEINumber)
  BRW1.AddField(tmp:Current_status,BRW1.Q.tmp:Current_status)
  BRW1.AddField(tmp:Location,BRW1.Q.tmp:Location)
  BRW1.AddField(tmp:Type,BRW1.Q.tmp:Type)
  BRW1.AddField(wob:ModelNumber,BRW1.Q.wob:ModelNumber)
  BRW1.AddField(wob:MobileNumber,BRW1.Q.wob:MobileNumber)
  BRW1.AddField(wob:Current_Status,BRW1.Q.wob:Current_Status)
  BRW1.AddField(wob:Exchange_Status,BRW1.Q.wob:Exchange_Status)
  BRW1.AddField(wob:Loan_Status,BRW1.Q.wob:Loan_Status)
  BRW1.AddField(wob:OrderNumber,BRW1.Q.wob:OrderNumber)
  BRW1.AddField(tmp:Courier,BRW1.Q.tmp:Courier)
  BRW1.AddField(tmp:BrowseFlag,BRW1.Q.tmp:BrowseFlag)
  BRW1.AddField(wob:HeadAccountNumber,BRW1.Q.wob:HeadAccountNumber)
  BRW1.AddField(wob:RecordNumber,BRW1.Q.wob:RecordNumber)
  BRW1.AddField(wob:JobNumber,BRW1.Q.wob:JobNumber)
  BRW1.AddField(wob:RefNumber,BRW1.Q.wob:RefNumber)
  BRW1.AddField(wob:Manufacturer,BRW1.Q.wob:Manufacturer)
  BRW1.AddField(job_ali:Exchange_Unit_Number,BRW1.Q.job_ali:Exchange_Unit_Number)
  BRW1.AddField(jobe:DespatchType,BRW1.Q.jobe:DespatchType)
  BRW1.AddField(job_ali:Loan_Unit_Number,BRW1.Q.job_ali:Loan_Unit_Number)
  BRW1.AddField(job_ali:Current_Status,BRW1.Q.job_ali:Current_Status)
  BRW1.AddField(job_ali:EDI_Batch_Number,BRW1.Q.job_ali:EDI_Batch_Number)
  BRW1.AddField(tmp:Cost,BRW1.Q.tmp:Cost)
  BRW1.AddField(wob:EDI,BRW1.Q.wob:EDI)
  BRW1.AddField(job_ali:date_booked,BRW1.Q.job_ali:date_booked)
  BRW1.AddField(job_ali:time_booked,BRW1.Q.job_ali:time_booked)
  BRW1.AddField(LocalTag,BRW1.Q.LocalTag)
  BRW1.AddField(tmp:LoanStatusDate,BRW1.Q.tmp:LoanStatusDate)
  BRW1.AddField(tmp:ExchangeStatusDate,BRW1.Q.tmp:ExchangeStatusDate)
  BRW1.AddField(tmp:StatusDate,BRW1.Q.tmp:StatusDate)
  BRW1.AddField(tmp:Current_StatusBase,BRW1.Q.tmp:Current_StatusBase)
  BRW1.AddField(address:CompanyName,BRW1.Q.address:CompanyName)
  BRW1.AddField(address:AddressLine1,BRW1.Q.address:AddressLine1)
  BRW1.AddField(address:AddressLine2,BRW1.Q.address:AddressLine2)
  BRW1.AddField(address:AddressLine3,BRW1.Q.address:AddressLine3)
  BRW1.AddField(address:Postcode,BRW1.Q.address:Postcode)
  BRW1.AddField(address:TelephoneNumber,BRW1.Q.address:TelephoneNumber)
  BRW1.AddField(address:FaxNumber,BRW1.Q.address:FaxNumber)
  BRW1.AddField(tmp:ExchangeUnitDetails,BRW1.Q.tmp:ExchangeUnitDetails)
  BRW1.AddField(tmp:LoanUnitDetails,BRW1.Q.tmp:LoanUnitDetails)
  BRW1.AddField(tmp:JobDays,BRW1.Q.tmp:JobDays)
  BRW1.AddField(tmp:ExcStatusDays,BRW1.Q.tmp:ExcStatusDays)
  BRW1.AddField(tmp:LoaStatusDays,BRW1.Q.tmp:LoaStatusDays)
  BRW1.AddField(tmp:JobStatusDays,BRW1.Q.tmp:JobStatusDays)
  BRW1.AddField(tmp:ExchangeUnitDetails2,BRW1.Q.tmp:ExchangeUnitDetails2)
  BRW1.AddField(tmp:LoanUnitDetails2,BRW1.Q.tmp:LoanUnitDetails2)
  BRW1.AddField(tmp:Loan_status,BRW1.Q.tmp:Loan_status)
  BRW1.AddField(tmp:Exchange_Status,BRW1.Q.tmp:Exchange_Status)
  BRW1.AddField(wob:ReadyToDespatch,BRW1.Q.wob:ReadyToDespatch)
  BRW1.AddField(wob:DespatchCourier,BRW1.Q.wob:DespatchCourier)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  if glo:webJob
      ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
      !Now log out
      access:users.clearkey(use:password_key)
      use:password = glo:password
      if access:users.fetch(use:password_key) = Level:Benign
          access:logged.clearkey(log:user_code_key)
          log:user_code = use:user_code
          log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
          if access:logged.fetch(log:user_code_key) = Level:Benign
              delete(logged)
          end !if logged.fetch
      end !if users.fetch
  end !if glo:webJob
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EDIBATCH.Close
    Relate:EXCHANGE.Close
    Relate:LOGGED.Close
    Relate:MULDESPJ.Close
    Relate:MULDESP_ALIAS.Close
    Relate:STATUS.Close
    Relate:TRADEAC2.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WAYBCONF.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseWebJobs')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      PickJobStatus
      PickExchangeStatus
      PickLoanStatus
      BrowseMODELNUM
      Browse_Subtraders
      PickCouriers
      PickWebAccounts
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?PrintButton
      GLO:SELECT1=''
    OF ?MultJobButton
          if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You do not have access to this option.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              Cycle
          End
      
          !TB13323 - force to use SBOnline if set up - JC 03/07/14
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          TRA:Account_Number = Clarionet:Global.Param2
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              If tra:UseSBOnline= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! tra:UseSBOnline
          End ! If Access:TRADEACC.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      clear(wob:record)
    OF ?ButtonSendSMS
      !Send SMS to tagged jobs
      if records(glo:Queue20) = 0 then
          miss# = missive('You must tag some jobs for this to work','ServiceBase 3g','mstop.jpg','OK' )
          cycle
      END
      
      
      SMSErrorFlag = 0
      
      Loop count = 1 to records(glo:Queue20)
      
          get(Glo:Queue20,count)
      
      
          Access:Webjob.clearkey(wob:RecordNumberKey)
          wob:RecordNumber =  glo:Queue20.Pointer20
          if access:Webjob.fetch(wob:RecordNumberKey)
              !errir
          END
      
      
          Access:jobs.clearkey(job:Ref_Number_Key)
          job:Ref_Number = wob:RefNumber
          if access:jobs.fetch(job:Ref_Number_Key)
      
              !message('Job not found by ref number '&clip(glo:Queue20.Pointer20))
      
          ELSE
              
              Case choice(?StatusSheet)
                  of 1    !job status
                      SendSMSText('J','N','N')  !,job:Location,AutomaticSend,SpecialType)
                  of 2    !exchange status
                      !message('Doing the send smstext E N N from within browse jobs')
                      SendSMSText('E','N','N')  !,job:Location,AutomaticSend,SpecialType)
                  of 3    !loan status
                      SendSMSText('L','N','N')  !,job:Location,AutomaticSend,SpecialType)
              END !Case
      
              !did it work?
              if glo:ErrorText[1:5] = 'ERROR' then
      
                  miss# = missive('Unable to send SMS for job '&clip(job:Ref_number)&' as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'ServiceBase 3g','mstop.jpg','OK' )
                  SMSErrorFlag = true
      
              ELSE
                  !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
                  !miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'ServiceBase 3g','mwarn.jpg','OK' )
      
                  if choice(?StatusSheet) = 1 and job:Current_Status = '510 ESTIMATE READY' then
                      !Job:Current_Status = '520 ESTIMATE SENT
                      Access:Status.clearkey(sts:Status_Key)
                      sts:Status = '520 ESTIMATE SENT'
                      if access:Status.fetch(sts:Status_Key)
                          !message('Cannot find 520 estimate ready')
                      ELSE
                          SetTheJobStatus(sts:Ref_Number,'JOB')
                          
                          IF (Access:JOBS.TryUpdate() = Level:Benign)
                              UpdateTheJobStatusHistory()
                              If AddToAudit(job:Ref_Number,'JOB','AUTOMATED STATUS CHANGE TO: 520 ESTIMATE SENT','PREVIOUS STATUS: 510 ESTIMATE READY<13,10>NEW STATUS: 520 ESTIMATE SENT ' &|
                                      '<13,10,13,10>REASON:<13,10>AUTOMATED SMS SENT')
      
                              End ! if addtoaudit
                          END !if jobs.update
                          Access:Webjob.tryupdate()
                      END !if status found
                  END !if this is job and estimate ready
              END !if error shown
          END !if jobs record fetched
      END !loop through tagges jobs
      
      if SMSErrorFlag = true then
          miss# = missive('SMS messaging complete - some errors (already reported) occured during this process','ServiceBase 3g','mstop.jpg','OK' )
      ELSE
          miss# = missive('SMS Message(s) Submitted','ServiceBase 3g','mstop.jpg','OK' )
      END
      
      if choice(?StatusSheet) = 1 and temp:Current_Status = '510 ESTIMATE READY' then
          !this will have changed statuses
          BRW1.resetsort(1)
      END !if this was a job on estimate ready
      
    OF ?Button27
      do RefreshStatus
    OF ?tmp:GlobalIMEINumber
      !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
      IF LEN(CLIP(tmp:GlobalIMEINumber)) = 18
        !Ericsson IMEI!
        tmp:GlobalIMEINumber = SUB(tmp:GlobalIMEINumber,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      
      If GetTempPathA(255,TempFilePath)
          If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & 'GLOBIMEI' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & '\GLOBIMEI' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
      
      Remove(glo:File_Name)
      Access:ADDSEARCH.Open()
      Access:ADDSEARCH.UseFile()
      
      ! Inserting (DBH 22/11/2006) # 8405 - Call IMEI Search Routine
      BuildIMEISearch(tmp:GlobalIMEInumber)
      ! End (DBH 22/11/2006) #8405
      
      Access:ADDSEARCH.Close()
      
      IMEIHistorySearch(tmp:GlobalIMEINumber)
      !IMEISearch(tmp:GlobalIMEINumber)
      
      Remove(glo:File_Name)
      
    OF ?RefreshViewButton
      Case Missive('This procedure may take a long time to complete, and once started cannot be interrupted.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Cycle
      End ! Case Missive
      
      Prog.ProgressSetup(Records(WEBJOB))
      
      temp# = 0
      access:webjob.clearkey(wob:HeadRefNumberKey)
      wob:HeadAccountNumber = LocalTradeAcc
      wob:RefNumber = 0
      set(wob:HeadRefNumberKey,wob:HeadRefNumberKey)
      loop
          if access:webjob.next() then break.
          if wob:HeadAccountNumber <> LocalTradeAcc then break.
      
          If Prog.InsideLoop()
              Break
          End ! If Prog.InsideLoop()
      
          access:jobs.clearkey(job:Ref_Number_Key)
          job:ref_number = wob:refNumber
          if access:jobs.fetch(job:Ref_Number_Key)
              !error - ignore this one!
          ELSE
              !If wob:EDI = 'PAY' then the job has been marked as paid.
              !too late then!
      
              !Has the batch been approved?
              !If so, mark the job as being approved, unless it's been paid, or rejectec
              Access:EDIBATCH.ClearKey(ebt:Batch_Number_Key)
              ebt:Manufacturer = job:Manufacturer
              ebt:Batch_Number = job:EDI_Batch_Number
              If Access:EDIBATCH.TryFetch(ebt:Batch_Number_Key) = Level:Benign
                  !Found
                  If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                      wob:EDI = 'APP'
                      Access:WEBJOB.Update()
                  End !If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP
              Else!If Access:EDIBATCH.TryFetch(ebt:Batch_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:EDIBATCH.TryFetch(ebt:Batch_Number_Key) = Level:Benign
      
              If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                  if wob:EDI <> job:edi
                      wob:EDI = job:edi
                      access:webjob.update()
                      temp# += 1
                  END !if edi mismatch
              End !If wob:EDI <> 'PAY'
      
          END
      
      END
      
      Prog.ProgressFinish()
      
      Case Missive('' & Clip(temp#) & ' jobs updated.','ServiceBase 3g',|
                     'midea.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
    OF ?tmp:EDI
      case tmp:EDI
          of 'EXC'
              unhide(?ProcessRejectionButton)
          ELSE
              hide(?ProcessRejectionButton)
      END !CASE
      
      If tmp:EDI = 'APP'
          ?ClaimPaid{prop:Hide} = 0
      Else !tmp:EDI = 'PAY'
          ?ClaimPaid{prop:Hide} = 1
      End !tmp:EDI = 'PAY'
      
      !thiswindow.update
      !display
      brw1.resetsort(1)
    OF ?tmp:WarManufacturer
      BRW1.ResetSort(1)
    OF ?Select_Courier_temp
      Case Select_courier_temp
          of 'IND'
              ?Courier_Temp{prop:hide}=false
              ?CourierLookupButton{prop:hide}=false
          of 'ALL'
              ?Courier_temp{prop:hide}=true
              ?CourierLookupButton{prop:hide}=true
      END !Case
      
      BRW1.ResetSort(1)
    OF ?Select_Trade_Account_temp
      case Select_trade_account_temp
          of 'IND'
              ?Account_Number2_temp{prop:hide}=false
              ?TradeLookupButton{prop:hide}=false
          of 'ALL'
              ?Account_Number2_temp{prop:hide}=true
              ?TradeLookupButton{prop:hide}=true
      END
      
      BRW1.ResetSort(1)
    OF ?TradeLookupButton
      glo:Select1   = ''
      If glo:WebJob = 1
          glo:Select1   = Clarionet:Global.Param2
      Else !If glo:WebJob = 1
          If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
              glo:Select1 = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          End !If GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI') <> ''
      End !If glo:WebJob = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CustomerServiceButton
      ThisWindow.Update
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.wob:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          x# = Choice(?CurrentTab)
          Request# = GlobalRequest
          GlobalRequest = ChangeRecord
          UpdateJobs
          GlobalRequest = Request#
          Brw1.ResetSort(1)
          ! Fudge to make sure that it returns to the same tab if left from. (Poxy Clarionet) - TrkBs: 6636 (DBH: 02-11-2005)
          Select(?CurrentTab,x#)
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !brw1.resetsort(1)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020501'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020501'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020501'&'0')
      ***
    OF ?PrintButton
      ThisWindow.Update
      ChoosePrintRoutine
      ThisWindow.Reset
      CASE CLIP(GLO:SELECT1)
          OF 'CANCEL'
              !do nothing
      
          OF 'CUSTOMER'
              !print cutomer receipt
              !Print Receipt
              GLO:Select1 = wob:refnumber
              Job_receipt
          OF 'JOBCARD'
              !print job card
              glo:select1 = wob:refnumber
              Job_card
          OF 'ESTIMATE'
              !print estimate
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number = wob:refnumber
              !message('JOB '&job:ref_number)
              if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:estimate <> 'YES'
                      Case Missive('The selected job has not been marked as an estimate.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If job:estimate <> 'YES'
                      glo:select1 = wob:refNumber
                      Estimate('')
                      glo:select1 = ''
                  End!If job:estimate <> 'YES'
              end
      
          OF 'INVOICE'
              !print invoice
              Do Create_Invoice
          OF 'CREDIT'
              !print credit note
              FoundCredit# = 0
              Access:JOBSINV.Clearkey(jov:TypeRecordKey)
              jov:RefNumber = wob:RefNumber
              jov:Type = 'C'
              Set(jov:TypeRecordKey,jov:TypeRecordKey)
              Loop ! Begin Loop
                  If Access:JOBSINV.Next()
                      Break
                  End ! If Access:JOBSINV.Next()
                  If jov:RefNumber <> wob:RefNumber
                      Break
                  End ! If jov:RefNumber <> wob:RefNumber
                  If jov:Type <> 'C'
                      Break
                  End ! If jov:Type <> 'C'
                  FoundCredit# = 1
                  Break
              End ! Loop
              If FoundCredit#
                  SelectInvoiceCreditNote(wob:RefNumber,'C')
              Else ! If FoundCredit#
                  Case Missive('The selected job has not been credited.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              End ! If FoundCredit#
      
              !message('You do not have authority to print a credit note')
          OF 'DESPATCH'
              !print despatch note
              glo:select1 = wob:refnumber
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = wob:RefNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  If job:Loan_Unit_Number <> 0 or job:Warranty_Job <> 'YES'
                      despatch_Note
                  Else !If job:Loan_Unit_Number <> 0
                      Warranty_Delivery_Note_Web
                  End !If job:Loan_Unit_Number <> 0
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
          OF 'WAYBILL'
      
              AskWayBillNumber
      
          OF 'BASTION'
              glo:select1 = wob:refnumber
              BastionWarrantyCheckReport()
      
          Of 'LOANCOLL'
              glo:Select1 = wob:RefNumber
              LoanCollectionNote
      
          Of 'OBFVAL'
              OBFValidationReport(wob:RefNumber)
      end !CASE
      
      brw1.resetsort(1)
    OF ?ViewBookingDetails
      ThisWindow.Update
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.wob:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      brw1.resetsort(1)
      
    OF ?ViewBookingDetails:2
      ThisWindow.Update
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.wob:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Request# = GlobalRequest
          GlobalRequest = ChangeRecord
      
          !j NEW BIT - didn't work
          !Save_aus_ID = Access:jobs_ALIAS.SaveFile()
        
          Update_Jobs_Rapid
      
          GlobalRequest = Request#
          !J new bit
          !Access:JOBS_ALIAS.RestoreFile(Save_aus_ID)
      
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      brw1.resetsort(1)
    OF ?SingleJobButton
      ThisWindow.Update
! Changing (DBH 30/11/2006) # 8540 - Call routine to create new job
!    Insert_Global = 'YES'
!    Clear(wob:Record)
!    Clear(job:Record)
!
!    Update_Jobs
!    ! Inserting (DBH 14/07/2006) # 7975 - What reports are required to be printed?
!    If GlobalResponse = 1
!        ! Job has booked correctly (DBH: 14/07/2006)
!        Loop x# = 1 To Records(glo:Queue)
!            Get(glo:Queue,x#)
!            Case glo:Pointer
!            Of 'THERMAL LABEL'
!                glo:Select1 = job:Ref_Number
!                If job:Bouncer = 'B'
!                    Thermal_Labels('SE')
!                Else ! If job:Bouncer = 'B'
!                    If ExchangeAccount(job:Account_Number)
!                        Thermal_Labels('ER')
!                    Else ! If ExchangeAccount(job:Account_Number)
!                        Thermal_Labels('')
!                    End ! If ExchangeAccount(job:Account_Number)
!                End ! If job:Bouncer = 'B'
!                glo:Select1 = ''
!            Of 'JOB CARD'
!                glo:Select1 = job:Ref_Number
!                Job_Card
!                glo:Select1 = ''
!            Of 'RETAINED ACCESSORIES LABEL'
!                glo:Select1 = job:Ref_Number
!                Job_Retained_Accessories_Label
!                glo:Select1 = ''
!            Of 'RECEIPT'
!                glo:Select1 = job:Ref_Number
!                Job_Receipt
!                glo:Select1 = ''
!            End ! Case glo:Pointer
!        End ! Loop x# = 1 To Records(glo:Queue)
!
!    End ! If GlobalReponse = 1
! to (DBH 30/11/2006) # 8540
! Inserting (DBH 20/02/2008) # 9770 - Force the user to use SB Online
    if (SecurityCheck('JOBS - INSERT'))  ! #11682 Check access. (Bryan: 07/09/2010)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('You do not have access to this option.','ServiceBase',|
                       'mstop.jpg','/&OK')
        Of 1 ! &OK Button
        End!Case Message
        Cycle
    End

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = Clarionet:Global.Param2
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        If tra:UseSBOnline = 2          !TB13153 UsesSBOnline now has a forceing element (2) as well as a use (1) - JC 18/09/2013
            ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
            Cycle
        End ! If tra:UseSBOnline
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
! End (DBH 20/02/2008) #9770

    CreateNewJob()
! End (DBH 30/11/2006) #8540

    Brw1.ResetSort(1)
    OF ?MultJobButton
      ThisWindow.Update
      Multiple_Job_Booking
      ThisWindow.Reset
      brw1.resetsort(1)
    OF ?Button31
      ThisWindow.Update
      ShowKey
      ThisWindow.Reset
    OF ?CountJobsButton
      ThisWindow.Update
      !count all jobs - all code and button added 28/11/02 - ref L330
      !complicated and changed L340 - 29/11/02 - now counts by locations
      
      !set up the queue of locations to match
      Select_locations
      
      !! General notes to programmers.
      !! We search by webjob to count the search.
      !! LocalTradeAcc is set up to hold the head trader and is used as a limit on this browse
      
      if records(LocationQueue) then
      
          !blank used variables
          LocationCountString = ''
          clear(JobCount)
          yldcnt# = 0
          TotaljobCount = 0
      
          !Save positions in wob and job
          save_wob_id = access:webjob.savefile()
          Save_job_id = access:jobs.savefile()
      
          !now start sorting them out
          access:webjob.clearkey(wob:HeadJobNumberKey)
          wob:HeadAccountNumber = LocalTradeAcc
          set(wob:HeadJobNumberKey,wob:HeadJobNumberKey)
      
          loop
             if access:webjob.next()
                 break
              end !if no more webjobs
      
              if wob:HeadAccountNumber <> LocalTradeAcc
                  break
              END !if no more this account
      
              !Check if I should give a bit
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
      
              !and now we sort out the exclusions for each tab
              Case(choice(?CurrentTab))
                  of 1
                      !no filtering - count them all
                  of 2
                    Case Choice(?StatusSheet)
                        Of 1
                          !check for status matching
                          if wob:current_Status <> temp:current_Status then cycle.
      
                        Of 2
                          !check for exchange status
                          if wob:Exchange_Status <> temp:Exchange_Status then cycle.
      
                        Of 3
                          !check for loan status
                          if wob:Loan_Status <> temp:Loan_Status then cycle.
                    End ! Case Choice(?StatusSheet)
                  of 8
                      !check matching edi
                      if wob:EDI <> tmp:EDI then cycle.
                      !check for manufacturer
                      if Tmp:UseWarMan and wob:Manufacturer <> tmp:WarManufacturer then cycle.
                      !as on window - do not count if a hubrepair
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = wob:RefNumber
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) then cycle.
                      !if jobe:HubRepair then cycle.
                      if jobe:HubRepairDate <> '' then cycle.
                  of 9
                      !First check this is ready for despatch
                      access:jobse.clearkey(jobe:RefNumberKey)
                      jobe:RefNumber = wob:RefNumber
                      if access:jobse.fetch(jobe:RefNumberKey) then cycle.
                      if jobe:despatched <> 'REA' then cycle.
      
                      !now check the other possibilities
                      Case Select_Courier_Temp
                          Of 'ALL'
                              Case Select_Trade_Account_temp
                                  Of 'ALL'
                                      !no more to check
                                  Of 'IND'
                                      if wob:SubAcountNumber <> account_number2_temp then cycle.
                              End!Case Select_Trade_Account_temp
      
                          Of 'IND'  !Case Select_Courier_Temp
                              !We now need to check job:current_courier as well as jobe:despatched !
                              Access:jobs.clearkey(job:Ref_Number_Key)
                              job:Ref_Number = wob:RefNumber
                              access:jobs.fetch(job:ref_number_key)
      
                              Case Select_Trade_Account_temp
                                  Of 'ALL'
                                       if job:Courier <> Courier_Temp then  cycle.
                                  Of 'IND'
                                       if wob:SubAcountNumber <> account_number2_temp then cycle.
                                       if job:Courier <> Courier_Temp then cycle.
                              End!Case Select_Trade_Account_temp
                      End!Case Select_Courier_Temp
              END !Case current tab
      
              !find the location from the jobs file
              Access:jobs.clearkey(job:Ref_Number_Key)
              job:Ref_Number = wob:RefNumber
              access:jobs.fetch(job:ref_number_key)
      
              !add the count to the relevant count variable
              loop x#=1 to records(locationQueue) + 1
                  get(locationQueue,x#)
                  if LQ:Location = Job:Location then break.
              END! loop through LocationQueue
      
              jobCount[x#] +=1
              !Note that all non matching entries go into job count[N+1} - which is ignored by messaging
      
          end !loop
      
          !now sort out the message to be shown
          Loop x# = 1 to records(locationQueue)
              get(locationQueue,x#)
              LocationCountString = clip(LocationCountString)&'|'&clip(jobCount[x#])&' jobs found '&LQ:Location
              TotaljobCount += JobCount[x#]
          END !Loop through queue
      
          Case Missive('Jobs counted in the following locations:'&|
            '<13,10>' & Clip(LocationCountString) & '.'&|
            '<13,10>Total Jobs Counted: ' & Clip(TotalJobCount) & '.','ServiceBase 3g',|
                         'midea.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          !Tidy up now finished
          access:webjob.restorefile(save_wob_id)
          access:jobs.restorefile(save_job_id)
      
      END !if records(LocationQueue)
      
      
    OF ?RefreshViewJobs
      ThisWindow.Update
      !Routine to fill in missing fields in webjobs - 3967 (DBH: 25-03-2004)
      Case Missive('This procedure may take a long time to complete, and once started cannot be interrupted.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Prog.ProgressSetup(Records(WEBJOB))
      
              save_job_id = access:webjob.savefile()
      
              temp# = 0
              access:webjob.clearkey(wob:HeadRefNumberKey)
              wob:HeadAccountNumber = LocalTradeAcc
              wob:RefNumber = 0
              set(wob:HeadRefNumberKey,wob:HeadRefNumberKey)
              loop
                  if access:webjob.next() then break.
                  if wob:HeadAccountNumber <> LocalTradeAcc then break.
      
                  If Prog.InsideLoop()
                      Break
                  End ! If Prog.InsideLoop()
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      If wob:MobileNumber <> job:Mobile_Number
                          wob:MobileNumber = job:Mobile_Number
                          temp# += 1
                          Access:WEBJOB.Update()
                      End !If wob:MobileNumber <> job:Mobile_Number
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              END!loop through Wobs
              Prog.ProgressFinish()
      
              Case Missive('' & Clip(temp#) & ' jobs updated.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              access:webjob.restorefile(save_job_id)
              BRW1.ResetSort(1)
          Of 1 ! No Button
      End ! Case Missive
    OF ?temp:Current_Status
      BRW1.ResetSort(1)
      IF temp:Current_Status OR ?temp:Current_Status{Prop:Req}
        sts:Status = temp:Current_Status
        !Save Lookup Field Incase Of error
        look:temp:Current_Status        = temp:Current_Status
        IF Access:STATUS.TryFetch(sts:Status_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            temp:Current_Status = sts:Status
          ELSE
            !Restore Lookup On Error
            temp:Current_Status = look:temp:Current_Status
            SELECT(?temp:Current_Status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup
      ThisWindow.Update
      sts:Status = temp:Current_Status
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          temp:Current_Status = sts:Status
          Select(?+1)
      ELSE
          Select(?temp:Current_Status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?temp:Current_Status)
    OF ?temp:Exchange_Status
      BRW1.ResetSort(1)
      IF temp:Exchange_Status OR ?temp:Exchange_Status{Prop:Req}
        sts:Status = temp:Exchange_Status
        !Save Lookup Field Incase Of error
        look:temp:Exchange_Status        = temp:Exchange_Status
        IF Access:STATUS.TryFetch(sts:Status_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            temp:Exchange_Status = sts:Status
          ELSE
            !Restore Lookup On Error
            temp:Exchange_Status = look:temp:Exchange_Status
            SELECT(?temp:Exchange_Status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup:2
      ThisWindow.Update
      sts:Status = temp:Exchange_Status
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          temp:Exchange_Status = sts:Status
          Select(?+1)
      ELSE
          Select(?temp:Exchange_Status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?temp:Exchange_Status)
    OF ?temp:Loan_Status
      BRW1.ResetSort(1)
      IF temp:Loan_Status OR ?temp:Loan_Status{Prop:Req}
        sts:Status = temp:Loan_Status
        !Save Lookup Field Incase Of error
        look:temp:Loan_Status        = temp:Loan_Status
        IF Access:STATUS.TryFetch(sts:Status_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            temp:Loan_Status = sts:Status
          ELSE
            !Restore Lookup On Error
            temp:Loan_Status = look:temp:Loan_Status
            SELECT(?temp:Loan_Status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup:3
      ThisWindow.Update
      sts:Status = temp:Loan_Status
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          temp:Loan_Status = sts:Status
          Select(?+1)
      ELSE
          Select(?temp:Loan_Status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?temp:Loan_Status)
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonTagFromCSV
      ThisWindow.Update
      Do TagFromCsv
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:ModelNumber
      BRW1.ResetSort(1)
      IF tmp:ModelNumber OR ?tmp:ModelNumber{Prop:Req}
        mod:Model_Number = tmp:ModelNumber
        !Save Lookup Field Incase Of error
        look:tmp:ModelNumber        = tmp:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tmp:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            tmp:ModelNumber = look:tmp:ModelNumber
            SELECT(?tmp:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup:5
      ThisWindow.Update
      mod:Model_Number = tmp:ModelNumber
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tmp:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ModelNumber)
    OF ?tmp:SubAccountNumber
      BRW1.ResetSort(1)
      IF tmp:SubAccountNumber OR ?tmp:SubAccountNumber{Prop:Req}
        sub:Account_Number = tmp:SubAccountNumber
        wob:HeadAccountNumber = LocalTradeAcc
        !Save Lookup Field Incase Of error
        look:tmp:SubAccountNumber        = tmp:SubAccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Main_Account_Key)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            tmp:SubAccountNumber = sub:Account_Number
          ELSE
            CLEAR(wob:HeadAccountNumber)
            !Restore Lookup On Error
            tmp:SubAccountNumber = look:tmp:SubAccountNumber
            SELECT(?tmp:SubAccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup:6
      ThisWindow.Update
      sub:Account_Number = tmp:SubAccountNumber
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          tmp:SubAccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:SubAccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SubAccountNumber)
    OF ?tmp:EDI
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Tmp:UseWarMan
      if tmp:useWarMan = 1 then
          unhide(?tmp:WarManufacturer)
      !    unhide(?wob:RefNumber:2)
          unhide(?CallLookup:4)
      !    unhide(?Prompt17)
      Else
          hide(?tmp:WarManufacturer)
          hide(?wob:RefNumber:2)
          hide(?CallLookup:4)
          hide(?Prompt17)
          !reset display
          tmp:WarManufacturer = ''
          BRW1.ResetSort(1)
      END
      display(?wob:RefNumber:2)
      display(?tmp:WarManufacturer)
      display(?CallLookup:4)
      display(?Prompt17)
    OF ?tmp:WarManufacturer
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup:4
      ThisWindow.Update
      man:Manufacturer = tmp:WarManufacturer
      GlobalRequest = SelectRecord
      Browse_Manufacturers
      SELF.Response = GlobalResponse
      IF SELF.Response = RequestCompleted
        tmp:WarManufacturer = man:Manufacturer
      END
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:WarManufacturer)
    OF ?ValuateClaims
      ThisWindow.Update
      Case Missive('This will valuate the pending claims. This may take a long time depending on the amount of claims.'&|
        '<13,10>'&|
        '<13,10>Are  you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
      
          !---before routine
      
              Prog.ProgressSetup(Records(WEBJOB))
      
              claim_value$ = 0
      
          !Count records
      
              free(glo:Queue7)
      
              count_records# = 0
              Save_wob_ID = Access:WEBJOB.SaveFile()
              Access:WEBJOB.ClearKey(wob:HeadEDIKey)
              wob:HeadAccountNumber = Clarionet:Global.Param2
              wob:EDI               = tmp:EDI
              Set(wob:HeadEDIKey,wob:HeadEDIKey)
              Loop
                  If Access:WEBJOB.NEXT()
                     Break
                  End !If
                  If wob:HeadAccountNumber <> Clarionet:Global.Param2      |
                  Or wob:EDI               <> tmp:EDI      |
                      Then Break.  ! End If
      
                  If SentToHub(wob:RefNumber)
                      Cycle
                  End !If SentToHub(wob:RefNumber)
      
                  glo:Queue7.GLO:Pointer7 = wob:RefNumber
                  add(glo:Queue7)
                  count_records# += 1
              End !Loop
              Access:WEBJOB.RestoreFile(Save_wob_ID)
      
              if records(glo:Queue7)
                  loop a# = 1 to records(glo:Queue7)                  ! Update the EDI field
                      get(glo:Queue7,a#)
                      if error() then break.
                      Access:JOBS.ClearKey(job:Ref_Number_Key)
                      job:Ref_Number = glo:Queue7.GLO:Pointer7
                      if not Access:JOBS.Fetch(job:Ref_Number_Key)
                          EDI" = job:EDI
                          job:EDI = PendingJob(job:Manufacturer)
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = job:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
                              If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                                  wob:EDI = job:EDI
                                  Access:WEBJOB.Update()
                              End !If wob:EDI <> 'PAY'
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                          if EDI" <> job:EDI
                              if job:EDI <> 'NO' then count_records# -= 1.
                              Access:JOBS.Update()
                          end
                      end
                  end
              end
      
              free(glo:Queue7)
      
              Brw1.ResetSort(1)
      
              Access:WEBJOB.ClearKey(wob:HeadEDIKey)
              wob:HeadAccountNumber = Clarionet:Global.Param2
              wob:EDI               = tmp:EDI
              Set(wob:HeadEDIKey,wob:HeadEDIKey)
              Loop
                  If Access:WEBJOB.NEXT()
                     Break
                  End !If
                  If wob:HeadAccountNumber <> Clarionet:Global.Param2      |
                  Or wob:EDI               <> tmp:EDI      |
                      Then Break.  ! End If
      
                  If Prog.InsideLoop()
                      Break
                  End ! If Prog.InsideLoop()
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  If SentToHub(wob:RefNumber)
                      Cycle
                  End !If SentToHub(wob:RefNumber)
      
                  !Code added 02/12/02 - to check for manufacturer
                  if Tmp:UseWarMan and tmp:WarManufacturer <> job:Manufacturer then cycle.
      
                  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                  man:Manufacturer  = job:Manufacturer
                  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                    !Found
      
                  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                    !Error
                  End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                  Save_wob_ID = Access:WEBJOB.SaveFile()
                  !Do not reprice jobs - L945 (DBH: 04-09-2003)
                  !JobPricingRoutine
                  Access:WEBJOB.RestoreFile(Save_wob_ID)
      
                  access:jobs.update()
                  Access:JOBSE.Update()
      
                  count# += 1
                  Claim_Value$ += Round(jobe:RRCWLabourCost,.02) + Round(jobe:RRCWPartsCost,.02)
      
              End !Loop
      
              Prog.ProgressFinish()
      
              Case Missive('Number of claims: ' & Count# & '.'&|
                '<13,10>'&|
                '<13,10>Value of claims: ' & Format(Claim_Value$,@n14.2) & '.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
      Select(?Browse)
    OF ?ProcessRejectionButton
      ThisWindow.Update
      EDI# = 0
      Case Missive('Do you wish to RESUBMIT the claim, or acknowledge the REJECTion?','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|Reject|Resubmit')
          Of 3 ! Resubmit Button
      
              EDI# = 1
          Of 2 ! Reject Button
              EDI# = 2
          Of 1 ! Cancel Button
      End ! Case Missive
      
      If EDI#
          Loop   !makes edi_reason compulsory here
              Glo:EDI_Reason = ''
              Get_EDI_Reason   !writes to glo:EDI_Reason string 60
              if Clip(glo:edi_reason) = '' then
                  Case Missive('If you do not give a reason, this process cannot be completed.'&|
                    '<13,10>'&|
                    '<13,10>Do you want to RETURN and enter a reason, or CANCEL the process?','ServiceBase 3g',|
                                 'mquest.jpg','\Cancel|/Return')
                      Of 2 ! Return Button
                      Of 1 ! Cancel Button
                          break
                  End ! Case Missive
              ELSE
                  break
              END !If glo:edi_reason is blank
          End !loop
      
          If Clip(glo:edi_reason) <> ''
              Pending# = False
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = brw1.q.wob:RefNumber
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Found
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      !If job has been rejected via the OBF/2nd Year process, then
                      !jobe:OBFProcessed = 3. In this case do NOT change job:edi
                      !the job should not be visible in the normal warranty browses
      
                      Case EDI#
                          Of 1
                              If jobe:OBFProcessed = 3
                                  jobe:OBFProcessed = 0
                              Else !If jobe:OBFProcessed = 3
                                  job:EDI = 'NO'
                                  ! Inserting (DBH 10/04/2006) #7252 - The unit is being returned to the pending table
                                  Pending# = True
                                  ! End (DBH 10/04/2006) #7252
                                  job:EDI_Batch_Number = ''
      
                                  ! Inserting (DBH 28/02/2007) # 8707 - Update the pending warranty screen
                                  Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                                  jow:RefNumber = job:Ref_Number
                                  If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                      !Found
                                      jow:Status = 'NO'
                                      jow:Submitted += 1
                                      Access:JOBSWARR.Update()
                                  Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                      !Error
                                  End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                  ! End (DBH 28/02/2007) #8707
      
                              End !If jobe:OBFProcessed = 3
      
                              wob:EDI = 'NO'
      ! Changing (DBH 28/02/2007) # 8707 - Status should be "resubmitted"
      !                        jobe:WarrantyClaimStatus = 'PENDING'
      ! to (DBH 28/02/2007) # 8707
                              jobe:WarrantyCLaimStatus = 'RESUBMITTED'
      ! End (DBH 28/02/2007) #8707
                              jobe:WarrantyStatusDate = Today()
      
                          Of 2
                              If jobe:OBFProcessed = 3
                                  jobe:OBFProcessed = 4
                              Else !If jobe:OBFProcessed = 3
                                  job:EDI = 'REJ'
                                  ! Inserting (DBH 28/02/2007) # 8707 - Update the pending warranty screen
                                  Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                                  jow:RefNumber = job:Ref_Number
                                  If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                      !Found
                                      jow:Status = 'REJ'
                                      Access:JOBSWARR.Update()
                                  Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                      !Error
                                  End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                  ! End (DBH 28/02/2007) #8707
      
                              End !If jobe:OBFProcessed = 3
      
                              wob:EDI = 'AAJ'
                              jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                              jobe:WarrantyStatusDate = Today()
                      End !Case EDI#
                      ! Changing (DBH 10/04/2006) #7252 - Change order to make sure the job is updated first.
                      ! Access:WEBJOB.Update()
                      !                 Access:JOBSE.Update()
                      !
                      !                 If access:jobs.update() = Level:Benign
                      ! to (DBH 10/04/2006) #7252
                      If Access:JOBS.Update() = Level:Benign
                          Access:WEBJOB.Update()
                          Access:JOBSE.Update()
                          ! Inserting (DBH 10/04/2006) #7252 - Update the "in pending" date
                          If Pending# And job:EDI = 'NO'
                              Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                              jobe2:RefNumber = job:Ref_Number
                              If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Found
                                  jobe2:InPendingDate = Today()
                                  Access:JOBSE2.TryUpdate()
                              Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Error
                                  If Access:JOBSE2.PrimeRecord() = Level:Benign
                                      jobe2:RefNumber = job:Ref_Number
                                      jobe2:InPendingDate = Today()
                                      If Access:JOBSE2.TryInsert() = Level:Benign
                                          ! Insert Successful
      
                                      Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                                          ! Insert Failed
                                          Access:JOBSE2.CancelAutoInc()
                                      End ! If Access:JOBSE2.TryInsert() = Level:Benign
                                  End !If Access:JOBSE2.PrimeRecord() = Level:Benign
                              End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                          End ! If Pending# And job:EDI = 'NO'
                          ! End (DBH 10/04/2006) #7252
      
                      ! End (DBH 10/04/2006) #7252
                          Case EDI#
                              Of 1
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
                              Of 2
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTION ACKNOWLEDGED','REASON: ' & Clip(Glo:EDI_Reason))
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
                          End !Case EDI#
      
                      End !If access:jobs.update() = Level:Benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
      
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          END !If glo:edi reason is not blank
      End !EDI#
      BRW1.ResetSort(1)
    OF ?ClaimPaid
      ThisWindow.Update
      InsertBatchNumber('')
      BRW1.ResetSort(1)
    OF ?Courier_Temp
      IF Courier_Temp OR ?Courier_Temp{Prop:Req}
        cou:Courier = Courier_Temp
        !Save Lookup Field Incase Of error
        look:Courier_Temp        = Courier_Temp
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            Courier_Temp = cou:Courier
          ELSE
            !Restore Lookup On Error
            Courier_Temp = look:Courier_Temp
            SELECT(?Courier_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CancelJobButton
      ThisWindow.Update
      CancelWob(wob:refNumber)
      ThisWindow.Reset
    OF ?CourierLookupButton
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickCouriers
      ThisWindow.Reset
      if GlobalResponse = RequestCompleted
          Courier_Temp = cou:Courier
          display(?Courier_Temp)
          BRW1.ResetSort(1)
      end
    OF ?Account_Number2_temp
      IF Account_Number2_temp OR ?Account_Number2_temp{Prop:Req}
        sub:Account_Number = Account_Number2_temp
        !Save Lookup Field Incase Of error
        look:Account_Number2_temp        = Account_Number2_temp
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(8,SelectRecord) = RequestCompleted
            Account_Number2_temp = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            Account_Number2_temp = look:Account_Number2_temp
            SELECT(?Account_Number2_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?TradeLookupButton
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickWebAccounts
      ThisWindow.Reset
      if globalresponse = requestcompleted
          Account_Number2_temp = sub:Account_Number
          display(?Account_Number2_temp)
          BRW1.ResetSort(1)
      end
    OF ?IndivDespatchButton
      ThisWindow.Update
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineDespatch = 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! If TRA2:SBOnlineStock
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      !check if we can despatch this job
      
      
      
      
      !fetch the defaults
      set(defaults)
      access:defaults.next()
      
      BRW1.UpdateBuffer()
      
      !fetch the job
      
      Error# = 0
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.wob:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If JobInUse(job:Ref_Number,1)
      
          Else !JobInUse(job:Ref_Number,1)
              If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And job:Despatch_type = 'JOB' AND job:Who_Booked <> 'WEB' ! #12259 Allow to despatch if VCP job. (Bryan: 26/08/2011)
                  If job:Loan_Unit_Number <> 0
                      Case Missive('Cannot despatch. '&|
                        '<13,10>'&|
                        '<13,10>The loan unit attached to job ' & Clip(job:Ref_Number) & ' has not been returned.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End !If job:Loan_Unit_Number <> 0
              End !If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
              ! Inserting (DBH 06/02/2008) # 9613 - Check if the delivery address is outside the region
              If Error# = 0
                  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                  sub:Account_Number = job:Account_Number
                  If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                      Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                      jobe2:RefNumber = job:Ref_Number
                      If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                          If sub:UseCustDespAdd = 'YES'
                              If HubOutOfRegion(Clarionet:Global.Param2,jobe2:HubCustomer) = 1
                                  If ReleasedForDespatch(job:Ref_Number) = 0
                                      Error# = 1
                                  End ! If ReleasedForDespatch(job:Ref_Number) = 0
                              End ! If HubOutOfRegion(Clarionet:Global.Param2,jobe2:HubCustomer) = 1
                          Else ! If sub:UseCustDespAdd = 'YES'
                              If HubOutOfRegion(Clarionet:Global.Param2,jobe2:HubDelivery) = 1
                                  If ReleasedForDespatch(job:Ref_Number) = 0
                                      Error# = 1
                                  End ! If ReleasedForDespatch(job:Ref_Number) = 0
                              End ! If HubOutOfRegion(Clarionet:Global.Param2,jobe2:HubCustomer) = 1
                          End ! If sub:UseCustDespAdd = 'YES'
      
                      End ! If Access:JOBSE.TryFetch(jobe2:RefNumberKey) = Level:Benign
                  End ! If Access:SUBTRACC.Clearkey(sub:Account_Number_Key) = Level:Benign
                  If Error# = 1
                      Beep(Beep:SystemHand);  Yield()
                      Case Missive('Cannot Despatch!'&|
                          '|'&|
                          '|The delivery address it outside your region.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End ! If Error# = 1
              End ! If Error# = 0
              ! End (DBH 06/02/2008) #9613
      
              If Error# = 0
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      If IsJobAlreadyInBatch(job:Ref_Number,Clarionet:Global.Param2)
                          Case Missive('Cannot despatch job number ' & Clip(job:Ref_NUmber) & '.'&|
                            '<13,10>'&|
                            '<13,10>This job is part of a multiple despatch batch that has not been despatched.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
      
                      End !If IsJobAlreadyInBatch(job:RefNumber,Clarionet:Global.Param2)
      
                      If Error# = 0
                          RemoteDespatch(job:Ref_Number,jobe:DespatchType)
                      End !If Error# = 0
      
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              End !If Error# = 0
          End !Else !JobInUse(job:Ref_Number,1)
      
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      BRW1.ResetSort(1)
    OF ?MultDespatchButton
      ThisWindow.Update
          !TB13153 - force to use SBOnline if set up - JC 18/09/2013
          Access:TRADEAC2.Clearkey(TRA2:KeyAccountNumber)
          TRA2:Account_Number = Clarionet:Global.Param2
          If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
              If TRA2:SBOnlineDespatch= 2 then
                  ClarioNET:CallClientProcedure('OPENURL', GETINI('SBONLINE','URL',,Clip(Path()) & '\SB2KDEF.INI'))
                  Cycle
              End ! TRA2:SBOnlineDespatch
          End ! If Access:TRADEAC2.TryFetch(TRA2:KeyAccountNumber) = Level:Benign
      
      
      check_access('MULTIPLE DESPATCH',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      else!if x" = false
      
      !Has something been tagged already.
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
      
          MultipleBatchDespatch
      end!if x" = f
      
      brw1.resetsort(1)
      BRW1.ResetSort(1)
    OF ?ChangeCourierButton
      ThisWindow.Update
      check_access('CHANGE COURIER',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      else!if x" = false
              saverequest#      = globalrequest
              globalresponse    = requestcancelled
              globalrequest     = selectrecord
              pickcouriers
              if globalresponse = requestcompleted
                  Case Missive('This will change the courier of the selected job to ' & Clip(cou:Courier) & '.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          access:jobs.clearkey(job:ref_number_key)
                          job:ref_number  = brw1.q.wob:RefNumber
                          If access:jobs.fetch(job:ref_number_key) = Level:Benign
                              Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                              mulj:JobNumber = job:Ref_Number
                              If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                  !Found
                                  Case Missive('Error! Job number ' & Clip(job:Ref_Number) & ' is part of a multiple batch. '&|
                                    '<13,10>You cannot change it''s courier until you remove it from it''s batch.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                  jobe:RefNumber  = job:Ref_Number
                                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Found
                                      Case jobe:despatchtype
                                          Of 'JOB'
                                              job:courier = cou:courier
                                              job:current_courier = job:courier
                                              !tmp:Courier = wob:DespatchCourier
                                          Of 'EXC'
                                              job:exchange_courier = cou:courier
                                              job:current_courier = job:exchange_courier
                                          Of 'LOA'
                                              job:loan_courier = cou:courier
                                              job:current_courier = job:loan_courier
                                      End!Case job:despatch_type
                                      access:jobs.update()
      
                                      !Update the webjob courier to match as well
                                      !TB12667 - JC - 2/07/12
                                      Access:WebJob.clearkey(wob:RefNumberKey)
                                      wob:RefNumber = job:Ref_number
                                      if access:WebJob.fetch(wob:RefNumberKey) = level:benign
                                          wob:DespatchCourier = cou:courier
                                          Access:Webjob.update()
                                      END !if webjob fetched
      
                                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                              End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                          End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Of 1 ! No Button
                  End ! Case Missive
      
              end
      
      
      end!if x" = false
      brw1.resetsort(1)
    OF ?payment_details
      ThisWindow.Update
      If SecurityCheck('PAYMENT TYPES')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('PAYMENT DETAILS')
          Thiswindow.reset(1)
      
          Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
          job_ali:Ref_Number  = brw1.q.wob:RefNumber
          If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
              !Found
              continue# = 1
              if job_ali:Loan_Unit_Number = 0
                  If job_ali:Warranty_Job = 'YES' and job_ali:Chargeable_Job <> 'YES'
                      continue# = 0
                      Case Missive('This job is marked as a warranty only job.'&|
                        '<13,10>'&|
                        '<13,10>It can only be marked as paid by reconciling the claim.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!If job_ali:Warranty_Job = 'YES' and job_ali:Chargeable_Job <> 'YES'
              end
      
              If job_ali:Date_Completed = '' And continue# = 1
                  Case Missive('This job has not been completed.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to add/amend payments?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          continue# = 0
                  End ! Case Missive
              End!If job_ali:Date_Completed = '' And continue# = 1
      
              If continue# = 1
                  !glo:Select1 = brw1.q.wob:RefNumber
                  Access:JOBS.ClearKey(job:Ref_Number_Key)
                  job:Ref_Number = brw1.q.wob:RefNumber
                  if not Access:JOBS.Fetch(job:Ref_Number_Key)
      
                      glo:Select1 = job:Ref_Number
      
                      Browse_Payments()
      
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = glo:Select1
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Found
                          ChkPaid()
                      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      glo:Select1 = ''
                  end
      
              End!If continue# = 1
          Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      
      End!If SecurityCheck('PAYMENT DETAILS')
      brw1.resetsort(1)
    OF ?ChangeStatusButton
      ThisWindow.Update
      check_access('CHANGE STATUS',x")
      If x" = False
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If x" = False
          Thiswindow.reset
          error# = 0
          clear(sts:record)
          access:status.clearkey(sts:status_key)
          sts:status = job_ali:current_status
          if access:status.fetch(sts:status_key) = Level:Benign
              If sts:ref_number = 799
                  check_access('JOBS - UNCANCEL',x")
                  If x" = False
                      Case Missive('This job has been cancelled and you do not have access to un-cancel it.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  Else!If x" = False
                      Case Missive('This job has been cancelled. If you proceed it will be un-cancelled. '&|
                        '<13,10>'&|
                        '<13,10>Are you sure?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                              job_ali:cancelled = 'NO'
                              access:jobs_alias.update()
                          Of 1 ! No Button
                              error# = 1
                      End ! Case Missive
                  End!If x" = False
              End!If sts:ref_number = 799
          End!if access:status.fetch(sts:status_key) = Level:Benign
          If error# = 0
              Change_Status(Wob:refnumber)
          End!If error# = 0
      End!If x" = False
      
      brw1.resetsort(1)
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?CurrentTab
      Do TabChanging
    OF ?StatusSheet
      omit('***bryan***')
      Have skipped the TinTools Reformat because
      it is confused by the sheet on sheet configuration.
      But I'm am keeping it, because I can cut and paste
      it's code and readjust it quicker than if I tried to
      reformat the browse by hand.
        ***bryan***
      Do TabChanging
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?tmp:WarManufacturer
      man:Manufacturer = tmp:WarManufacturer
      IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
        IF SELF.Run(1,SelectRecord) = RequestCompleted
          tmp:WarManufacturer = man:Manufacturer
        END
      END
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

LocalValidateIMEI    Procedure()

local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign

        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
          '<13,10>'&|
          '<13,10>There is an I.M.E.I. number mismatch.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        GetStatus(850,1,job:Despatch_Type)

        IF (AddToAudit(job:ref_number,'JOB','FAILED DESPATCH VALIDATION','I.M.E.I. NUMBER MISMATCH'))
        END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            If AccessoryCheck('JOB')
                If AccessoryMismatch(job:Ref_Number,'JOB')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'JOB')
            End !If AccessoryCheck('JOB')
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)
            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            IF (AddToAudit(job:Ref_Number,'JOB','FAILED DESPATCH VALIDATION',locAuditNotes))
            END ! IF

        Access:JOBS.Update()
        Return Level:Fatal

    End !If Return Level:Fatal

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = temp:Current_Status
  ELSIF CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = temp:Exchange_Status
  ELSIF CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = temp:Loan_Status
  ELSIF CHOICE(?CurrentTab) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
  ELSIF CHOICE(?CurrentTab) = 4
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
  ELSIF CHOICE(?CurrentTab) = 5
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:ModelNumber
  ELSIF CHOICE(?CurrentTab) = 6
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
  ELSIF CHOICE(?CurrentTab) = 7
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:SubAccountNumber
  ELSIF CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:EDI
  ELSIF CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:EDI
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:WarManufacturer
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'ALL' And Upper(Select_Courier_Temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'IND' And Upper(Select_Courier_Temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Account_Number2_temp
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'ALL' And Upper(Select_Courier_Temp) = 'IND'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Courier_Temp
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'IND' And Upper(Select_Courier_Temp) = 'IND'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Account_Number2_temp
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Courier_Temp
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = LocalTradeAcc
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 2 And Choice(?StatusSheet) = 3
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(7,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(8,Force)
  ELSIF CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 1
    RETURN SELF.SetSort(9,Force)
  ELSIF CHOICE(?CurrentTab) = 8 And Tmp:UseWarMan = 0
    RETURN SELF.SetSort(10,Force)
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'ALL' And Upper(Select_Courier_Temp) = 'ALL'
    RETURN SELF.SetSort(11,Force)
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'IND' And Upper(Select_Courier_Temp) = 'ALL'
    RETURN SELF.SetSort(12,Force)
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'ALL' And Upper(Select_Courier_Temp) = 'IND'
    RETURN SELF.SetSort(13,Force)
  ELSIF CHOICE(?CurrentTab) = 9 AND Upper(Select_Trade_Account_temp) = 'IND' And Upper(Select_Courier_Temp) = 'IND'
    RETURN SELF.SetSort(14,Force)
  ELSE
    RETURN SELF.SetSort(15,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = wob:RecordNumber
     GET(glo:Queue20,glo:Queue20.Pointer20)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  !!First bit moved
  !
  !!tmp:BrowseFlag = 5
  !If tmp:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  !    If Sub(tmp:Current_StatusBase,1,3) = Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
  !        tmp:BrowseFlag = 2
  !    Else
  !        tmp:BrowseFlag = 0
  !    End !If job:Current_Status = GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI')
  !
  !Else !tmp:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  !    If tmp:Location = GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')
  !        tmp:BrowseFlag = 3
  !    Else
  !        If tmp:Location = GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  !           tmp:BrowseFlag = 4
  !        Else
  !            tmp:BrowseFlag = 1
  !        End
  !    End
  !End !tmp:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  !
  !! DBH #10544 - Show a Purple Line if the job has been booked as a Liquid Repair
  !Access:JOBSE.clearkey(jobe:RefNumberKey)
  !jobe:RefNumber =job_ali:Ref_Number
  !if (access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
  !    if (jobe:Booking48HourOption = 4)
  !        tmp:BrowseFlag = 5
  !    end
  !end
  !
  !IF tmp:failed = FALSE then
  !
  !        Case def:browse_option
  !            Of 'INV'
  !                address:CompanyName     = job_ali:Company_Name
  !                address:AddressLine1    = job_ali:Address_Line1
  !                address:AddressLine2    = job_ali:Address_Line2
  !                address:AddressLine3    = job_ali:Address_Line3
  !                address:Postcode        = job_ali:Postcode
  !                address:TelephoneNumber = job_ali:Telephone_Number
  !                address:FaxNumber       = job_ali:Fax_Number
  !
  !            Of 'COL'
  !                address:CompanyName     = job_ali:Company_Name_Collection
  !                address:AddressLine1    = job_ali:Address_Line1_Collection
  !                address:AddressLine2    = job_ali:Address_Line2_Collection
  !                address:AddressLine3    = job_ali:Address_Line3_Collection
  !                address:Postcode        = job_ali:Postcode_Collection
  !                address:TelephoneNumber = job_ali:Telephone_Collection
  !                address:FaxNumber       = ''
  !            Of 'DEL'
  !                address:CompanyName     = job_ali:Company_Name_Delivery
  !                address:AddressLine1    = job_ali:Address_Line1_Delivery
  !                address:AddressLine2    = job_ali:Address_Line2_Delivery
  !                address:AddressLine3    = job_ali:Address_Line3_Delivery
  !                address:Postcode        = job_ali:Postcode_Delivery
  !                address:TelephoneNumber = job_ali:Telephone_Delivery
  !                address:FaxNumber       = ''
  !            Else
  !                address:CompanyName     = job_ali:Company_Name
  !                address:AddressLine1    = job_ali:Address_Line1
  !                address:AddressLine2    = job_ali:Address_Line2
  !                address:AddressLine3    = job_ali:Address_Line3
  !                address:Postcode        = job_ali:Postcode
  !                address:TelephoneNumber = job_ali:Telephone_Number
  !                address:FaxNumber       = job_ali:Fax_Number
  !        End!Case def:browse_option
  !
  !    tmp:ExchangeUnitDetails = ''
  !    tmp:ExchangeUnitDetails2 = ''
  !    If job_ali:Exchange_Unit_Number <> 0
  !        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
  !        xch:Ref_Number  = job_ali:Exchange_Unit_Number
  !        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  !            !Found
  !            tmp:ExchangeUnitDetails = 'Exchange Issued: ' & Clip(xch:ESN)
  !            tmp:ExchangeUnitDetails2 = 'Model No: ' & Clip(xch:Model_Number)
  !        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  !            !Error
  !        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  !    Else !job_ali:Exchange_Unit_Number <> 0
  !    End !job_ali:Exchange_Unit_Number <> 0
  !
  !    tmp:LoanUnitDetails = ''
  !    tmp:LoanUnitDetails2 = ''
  !    If job_ali:Loan_Unit_Number <> 0
  !        Access:LOAN.Clearkey(loa:Ref_Number_Key)
  !        loa:Ref_Number  = job_ali:Loan_Unit_Number
  !        If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
  !            !Found
  !            tmp:LoanUnitDetails = 'Loan Issued: ' & Clip(loa:ESN)
  !            tmp:LoanUnitDetails2 = 'Model No: ' & Clip(loa:Model_Number)
  !        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  !            !Error
  !        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  !    Else !job_ali:Exchange_Unit_Number <> 0
  !    End !job_ali:Exchange_Unit_Number <> 0
  !
  !END
  !
  !!Second bit moved
  !!Status Days
  !Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  !tra:Account_Number  = wob:HeadAccountNumber
  !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    !Found
  !
  !Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !    !Error
  !End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  !Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
  !job_ali:Ref_Number  = wob:RefNumber
  !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  !    !Found
  !
  !Else ! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  !    !Error
  !End !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  !
  !Sat# = tra:IncludeSaturday
  !Sun# = tra:IncludeSunday
  !
  !tmp:StatusDate = ''
  !Save_aus_ID = Access:AUDSTATS.SaveFile()
  !Access:AUDSTATS.ClearKey(aus:DateChangedKey)                                                                                    
  !aus:RefNumber   = wob:RefNumber
  !aus:Type        = 'JOB'
  !aus:DateChanged = Today()
  !Set(aus:DateChangedKey,aus:DateChangedKey)
  !Loop
  !    If Access:AUDSTATS.PREVIOUS()
  !       Break
  !    End !If
  !    If aus:RefNumber   <> wob:RefNumber      |
  !    Or aus:Type        <> 'JOB'      |
  !        Then Break.  ! End If
  !    tmp:StatusDate  =  aus:DateChanged
  !
  !    Break
  !End !Loop
  !Access:AUDSTATS.RestoreFile(Save_aus_ID)
  !
  !tmp:LoanStatusDate = ''
  !Save_aus_ID = Access:AUDSTATS.SaveFile()
  !Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  !aus:RefNumber   = wob:RefNumber
  !aus:Type        = 'LOA'
  !aus:DateChanged = Today()
  !Set(aus:DateChangedKey,aus:DateChangedKey)
  !Loop
  !    If Access:AUDSTATS.PREVIOUS()
  !       Break
  !    End !If
  !    If aus:RefNumber   <> wob:RefNumber      |
  !    Or aus:Type        <> 'LOA'      |
  !        Then Break.  ! End If
  !    tmp:LoanStatusDate  =  aus:DateChanged
  !    Break
  !End !Loop
  !Access:AUDSTATS.RestoreFile(Save_aus_ID)
  !
  !tmp:ExchangeStatusDate = ''
  !Save_aus_ID = Access:AUDSTATS.SaveFile()
  !Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  !aus:RefNumber   = wob:RefNumber
  !aus:Type        = 'EXC'
  !aus:DateChanged = Today()
  !Set(aus:DateChangedKey,aus:DateChangedKey)
  !Loop
  !    If Access:AUDSTATS.PREVIOUS()
  !       Break
  !    End !If
  !    If aus:RefNumber   <> wob:RefNumber      |
  !    Or aus:Type        <> 'EXC'      |
  !        Then Break.  ! End If
  !    tmp:ExchangeStatusDate  =  aus:DateChanged
  !    Break
  !End !Loop
  !Access:AUDSTATS.RestoreFile(Save_aus_ID)
  !
  !tmp:JobStatusDays = GetDays(Sat#,Sun#,tmp:StatusDate)
  !tmp:ExcStatusDays = GetDays(Sat#,Sun#,tmp:ExchangeStatusDate)
  !tmp:LoaStatusDays = GetDays(Sat#,Sun#,tmp:LoanStatusDate)
  !
  !If job_ali:Date_Completed <> ''
  !    tmp:JobDays = GetDaysToDate(Sat#,Sun#,job_ali:Date_Booked,job_ali:date_Completed)
  !Else !job_ali:Date_Completed <> ''
  !    tmp:JobDays = GetDays(Sat#,Sun#,job_ali:Date_Booked)
  !End !job_ali:Date_Completed <> ''
  !
  !If jobe:WebJob
  !    If wob:DateJobDespatched <> ''
  !        tmp:JobStatusDays = 'N/A'
  !    End !If wob:DateJobDespatched <> ''
  !Else
  !    if job_ali:Date_Despatched <> ''
  !        tmp:JobStatusDays = 'N/A'
  !    End !if job_ali:Date_Despatched <> ''
  !End !jobe:WebJob
  !
  !If job_ali:Exchange_Despatched <> '' Or job_ali:Exchange_Unit_Number = 0
  !    tmp:ExcStatusDays = 'N/A'
  !End !job_ali:Exchange_Despatched <> ''
  !
  !If job_ali:Loan_Despatched <> '' Or job_ali:Loan_unit_Number = 0
  !    tmp:LoaStatusDays = 'N/A'
  !End !job_ali:Loan_Despatched <> ''
  !
  PARENT.SetQueueRecord
  IF (tmp:BrowseFlag = 1)
    SELF.Q.tmp:wob_jobNumber_NormalFG = 255
    SELF.Q.tmp:wob_jobNumber_NormalBG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedFG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.tmp:wob_jobNumber_NormalFG = 32768
    SELF.Q.tmp:wob_jobNumber_NormalBG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedFG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.tmp:wob_jobNumber_NormalFG = 16711680
    SELF.Q.tmp:wob_jobNumber_NormalBG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedFG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.tmp:wob_jobNumber_NormalFG = 7961087
    SELF.Q.tmp:wob_jobNumber_NormalBG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedFG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.tmp:wob_jobNumber_NormalFG = 8388736
    SELF.Q.tmp:wob_jobNumber_NormalBG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedFG = 16777215
    SELF.Q.tmp:wob_jobNumber_SelectedBG = 8388736
  ELSE
    SELF.Q.tmp:wob_jobNumber_NormalFG = -1
    SELF.Q.tmp:wob_jobNumber_NormalBG = -1
    SELF.Q.tmp:wob_jobNumber_SelectedFG = -1
    SELF.Q.tmp:wob_jobNumber_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:SubAcountNumber_NormalFG = 255
    SELF.Q.wob:SubAcountNumber_NormalBG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedFG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:SubAcountNumber_NormalFG = 32768
    SELF.Q.wob:SubAcountNumber_NormalBG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedFG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:SubAcountNumber_NormalFG = 16711680
    SELF.Q.wob:SubAcountNumber_NormalBG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedFG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:SubAcountNumber_NormalFG = 7961087
    SELF.Q.wob:SubAcountNumber_NormalBG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedFG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:SubAcountNumber_NormalFG = 8388736
    SELF.Q.wob:SubAcountNumber_NormalBG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedFG = 16777215
    SELF.Q.wob:SubAcountNumber_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:SubAcountNumber_NormalFG = -1
    SELF.Q.wob:SubAcountNumber_NormalBG = -1
    SELF.Q.wob:SubAcountNumber_SelectedFG = -1
    SELF.Q.wob:SubAcountNumber_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.tmp:Company_Name_NormalFG = 255
    SELF.Q.tmp:Company_Name_NormalBG = 16777215
    SELF.Q.tmp:Company_Name_SelectedFG = 16777215
    SELF.Q.tmp:Company_Name_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.tmp:Company_Name_NormalFG = 32768
    SELF.Q.tmp:Company_Name_NormalBG = 16777215
    SELF.Q.tmp:Company_Name_SelectedFG = 16777215
    SELF.Q.tmp:Company_Name_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.tmp:Company_Name_NormalFG = 16711680
    SELF.Q.tmp:Company_Name_NormalBG = 16777215
    SELF.Q.tmp:Company_Name_SelectedFG = 16777215
    SELF.Q.tmp:Company_Name_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.tmp:Company_Name_NormalFG = 7961087
    SELF.Q.tmp:Company_Name_NormalBG = 16777215
    SELF.Q.tmp:Company_Name_SelectedFG = 16777215
    SELF.Q.tmp:Company_Name_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.tmp:Company_Name_NormalFG = 8388736
    SELF.Q.tmp:Company_Name_NormalBG = 16777215
    SELF.Q.tmp:Company_Name_SelectedFG = 16777215
    SELF.Q.tmp:Company_Name_SelectedBG = 8388736
  ELSE
    SELF.Q.tmp:Company_Name_NormalFG = -1
    SELF.Q.tmp:Company_Name_NormalBG = -1
    SELF.Q.tmp:Company_Name_SelectedFG = -1
    SELF.Q.tmp:Company_Name_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:IMEINumber_NormalFG = 255
    SELF.Q.wob:IMEINumber_NormalBG = 16777215
    SELF.Q.wob:IMEINumber_SelectedFG = 16777215
    SELF.Q.wob:IMEINumber_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:IMEINumber_NormalFG = 32768
    SELF.Q.wob:IMEINumber_NormalBG = 16777215
    SELF.Q.wob:IMEINumber_SelectedFG = 16777215
    SELF.Q.wob:IMEINumber_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:IMEINumber_NormalFG = 16711680
    SELF.Q.wob:IMEINumber_NormalBG = 16777215
    SELF.Q.wob:IMEINumber_SelectedFG = 16777215
    SELF.Q.wob:IMEINumber_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:IMEINumber_NormalFG = 7961087
    SELF.Q.wob:IMEINumber_NormalBG = 16777215
    SELF.Q.wob:IMEINumber_SelectedFG = 16777215
    SELF.Q.wob:IMEINumber_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:IMEINumber_NormalFG = 8388736
    SELF.Q.wob:IMEINumber_NormalBG = 16777215
    SELF.Q.wob:IMEINumber_SelectedFG = 16777215
    SELF.Q.wob:IMEINumber_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:IMEINumber_NormalFG = -1
    SELF.Q.wob:IMEINumber_NormalBG = -1
    SELF.Q.wob:IMEINumber_SelectedFG = -1
    SELF.Q.wob:IMEINumber_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.tmp:Current_status_NormalFG = 255
    SELF.Q.tmp:Current_status_NormalBG = 16777215
    SELF.Q.tmp:Current_status_SelectedFG = 16777215
    SELF.Q.tmp:Current_status_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.tmp:Current_status_NormalFG = 32768
    SELF.Q.tmp:Current_status_NormalBG = 16777215
    SELF.Q.tmp:Current_status_SelectedFG = 16777215
    SELF.Q.tmp:Current_status_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.tmp:Current_status_NormalFG = 16711680
    SELF.Q.tmp:Current_status_NormalBG = 16777215
    SELF.Q.tmp:Current_status_SelectedFG = 16777215
    SELF.Q.tmp:Current_status_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.tmp:Current_status_NormalFG = 7961087
    SELF.Q.tmp:Current_status_NormalBG = 16777215
    SELF.Q.tmp:Current_status_SelectedFG = 16777215
    SELF.Q.tmp:Current_status_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.tmp:Current_status_NormalFG = 8388736
    SELF.Q.tmp:Current_status_NormalBG = 16777215
    SELF.Q.tmp:Current_status_SelectedFG = 16777215
    SELF.Q.tmp:Current_status_SelectedBG = 8388736
  ELSE
    SELF.Q.tmp:Current_status_NormalFG = -1
    SELF.Q.tmp:Current_status_NormalBG = -1
    SELF.Q.tmp:Current_status_SelectedFG = -1
    SELF.Q.tmp:Current_status_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.tmp:Location_NormalFG = 255
    SELF.Q.tmp:Location_NormalBG = 16777215
    SELF.Q.tmp:Location_SelectedFG = 16777215
    SELF.Q.tmp:Location_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.tmp:Location_NormalFG = 32768
    SELF.Q.tmp:Location_NormalBG = 16777215
    SELF.Q.tmp:Location_SelectedFG = 16777215
    SELF.Q.tmp:Location_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.tmp:Location_NormalFG = 16711680
    SELF.Q.tmp:Location_NormalBG = 16777215
    SELF.Q.tmp:Location_SelectedFG = 16777215
    SELF.Q.tmp:Location_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.tmp:Location_NormalFG = 7961087
    SELF.Q.tmp:Location_NormalBG = 16777215
    SELF.Q.tmp:Location_SelectedFG = 16777215
    SELF.Q.tmp:Location_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.tmp:Location_NormalFG = 8388736
    SELF.Q.tmp:Location_NormalBG = 16777215
    SELF.Q.tmp:Location_SelectedFG = 16777215
    SELF.Q.tmp:Location_SelectedBG = 8388736
  ELSE
    SELF.Q.tmp:Location_NormalFG = -1
    SELF.Q.tmp:Location_NormalBG = -1
    SELF.Q.tmp:Location_SelectedFG = -1
    SELF.Q.tmp:Location_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.tmp:Type_NormalFG = 255
    SELF.Q.tmp:Type_NormalBG = 16777215
    SELF.Q.tmp:Type_SelectedFG = 16777215
    SELF.Q.tmp:Type_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.tmp:Type_NormalFG = 32768
    SELF.Q.tmp:Type_NormalBG = 16777215
    SELF.Q.tmp:Type_SelectedFG = 16777215
    SELF.Q.tmp:Type_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.tmp:Type_NormalFG = 16711680
    SELF.Q.tmp:Type_NormalBG = 16777215
    SELF.Q.tmp:Type_SelectedFG = 16777215
    SELF.Q.tmp:Type_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.tmp:Type_NormalFG = 7961087
    SELF.Q.tmp:Type_NormalBG = 16777215
    SELF.Q.tmp:Type_SelectedFG = 16777215
    SELF.Q.tmp:Type_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.tmp:Type_NormalFG = 8388736
    SELF.Q.tmp:Type_NormalBG = 16777215
    SELF.Q.tmp:Type_SelectedFG = 16777215
    SELF.Q.tmp:Type_SelectedBG = 8388736
  ELSE
    SELF.Q.tmp:Type_NormalFG = -1
    SELF.Q.tmp:Type_NormalBG = -1
    SELF.Q.tmp:Type_SelectedFG = -1
    SELF.Q.tmp:Type_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:ModelNumber_NormalFG = 255
    SELF.Q.wob:ModelNumber_NormalBG = 16777215
    SELF.Q.wob:ModelNumber_SelectedFG = 16777215
    SELF.Q.wob:ModelNumber_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:ModelNumber_NormalFG = 32768
    SELF.Q.wob:ModelNumber_NormalBG = 16777215
    SELF.Q.wob:ModelNumber_SelectedFG = 16777215
    SELF.Q.wob:ModelNumber_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:ModelNumber_NormalFG = 16711680
    SELF.Q.wob:ModelNumber_NormalBG = 16777215
    SELF.Q.wob:ModelNumber_SelectedFG = 16777215
    SELF.Q.wob:ModelNumber_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:ModelNumber_NormalFG = 7961087
    SELF.Q.wob:ModelNumber_NormalBG = 16777215
    SELF.Q.wob:ModelNumber_SelectedFG = 16777215
    SELF.Q.wob:ModelNumber_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:ModelNumber_NormalFG = 8388736
    SELF.Q.wob:ModelNumber_NormalBG = 16777215
    SELF.Q.wob:ModelNumber_SelectedFG = 16777215
    SELF.Q.wob:ModelNumber_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:ModelNumber_NormalFG = -1
    SELF.Q.wob:ModelNumber_NormalBG = -1
    SELF.Q.wob:ModelNumber_SelectedFG = -1
    SELF.Q.wob:ModelNumber_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:MobileNumber_NormalFG = 255
    SELF.Q.wob:MobileNumber_NormalBG = 16777215
    SELF.Q.wob:MobileNumber_SelectedFG = 16777215
    SELF.Q.wob:MobileNumber_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:MobileNumber_NormalFG = 32768
    SELF.Q.wob:MobileNumber_NormalBG = 16777215
    SELF.Q.wob:MobileNumber_SelectedFG = 16777215
    SELF.Q.wob:MobileNumber_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:MobileNumber_NormalFG = 16711680
    SELF.Q.wob:MobileNumber_NormalBG = 16777215
    SELF.Q.wob:MobileNumber_SelectedFG = 16777215
    SELF.Q.wob:MobileNumber_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:MobileNumber_NormalFG = 7961087
    SELF.Q.wob:MobileNumber_NormalBG = 16777215
    SELF.Q.wob:MobileNumber_SelectedFG = 16777215
    SELF.Q.wob:MobileNumber_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:MobileNumber_NormalFG = 8388736
    SELF.Q.wob:MobileNumber_NormalBG = 16777215
    SELF.Q.wob:MobileNumber_SelectedFG = 16777215
    SELF.Q.wob:MobileNumber_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:MobileNumber_NormalFG = -1
    SELF.Q.wob:MobileNumber_NormalBG = -1
    SELF.Q.wob:MobileNumber_SelectedFG = -1
    SELF.Q.wob:MobileNumber_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:Current_Status_NormalFG = 255
    SELF.Q.wob:Current_Status_NormalBG = 16777215
    SELF.Q.wob:Current_Status_SelectedFG = 16777215
    SELF.Q.wob:Current_Status_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:Current_Status_NormalFG = 32768
    SELF.Q.wob:Current_Status_NormalBG = 16777215
    SELF.Q.wob:Current_Status_SelectedFG = 16777215
    SELF.Q.wob:Current_Status_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:Current_Status_NormalFG = 16711680
    SELF.Q.wob:Current_Status_NormalBG = 16777215
    SELF.Q.wob:Current_Status_SelectedFG = 16777215
    SELF.Q.wob:Current_Status_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:Current_Status_NormalFG = 7961087
    SELF.Q.wob:Current_Status_NormalBG = 16777215
    SELF.Q.wob:Current_Status_SelectedFG = 16777215
    SELF.Q.wob:Current_Status_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:Current_Status_NormalFG = 8388736
    SELF.Q.wob:Current_Status_NormalBG = 16777215
    SELF.Q.wob:Current_Status_SelectedFG = 16777215
    SELF.Q.wob:Current_Status_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:Current_Status_NormalFG = -1
    SELF.Q.wob:Current_Status_NormalBG = -1
    SELF.Q.wob:Current_Status_SelectedFG = -1
    SELF.Q.wob:Current_Status_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:Exchange_Status_NormalFG = 255
    SELF.Q.wob:Exchange_Status_NormalBG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedFG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:Exchange_Status_NormalFG = 32768
    SELF.Q.wob:Exchange_Status_NormalBG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedFG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:Exchange_Status_NormalFG = 16711680
    SELF.Q.wob:Exchange_Status_NormalBG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedFG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:Exchange_Status_NormalFG = 7961087
    SELF.Q.wob:Exchange_Status_NormalBG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedFG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:Exchange_Status_NormalFG = 8388736
    SELF.Q.wob:Exchange_Status_NormalBG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedFG = 16777215
    SELF.Q.wob:Exchange_Status_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:Exchange_Status_NormalFG = -1
    SELF.Q.wob:Exchange_Status_NormalBG = -1
    SELF.Q.wob:Exchange_Status_SelectedFG = -1
    SELF.Q.wob:Exchange_Status_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:Loan_Status_NormalFG = 255
    SELF.Q.wob:Loan_Status_NormalBG = 16777215
    SELF.Q.wob:Loan_Status_SelectedFG = 16777215
    SELF.Q.wob:Loan_Status_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:Loan_Status_NormalFG = 32768
    SELF.Q.wob:Loan_Status_NormalBG = 16777215
    SELF.Q.wob:Loan_Status_SelectedFG = 16777215
    SELF.Q.wob:Loan_Status_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:Loan_Status_NormalFG = 16711680
    SELF.Q.wob:Loan_Status_NormalBG = 16777215
    SELF.Q.wob:Loan_Status_SelectedFG = 16777215
    SELF.Q.wob:Loan_Status_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:Loan_Status_NormalFG = 7961087
    SELF.Q.wob:Loan_Status_NormalBG = 16777215
    SELF.Q.wob:Loan_Status_SelectedFG = 16777215
    SELF.Q.wob:Loan_Status_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:Loan_Status_NormalFG = 8388736
    SELF.Q.wob:Loan_Status_NormalBG = 16777215
    SELF.Q.wob:Loan_Status_SelectedFG = 0
    SELF.Q.wob:Loan_Status_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:Loan_Status_NormalFG = -1
    SELF.Q.wob:Loan_Status_NormalBG = -1
    SELF.Q.wob:Loan_Status_SelectedFG = -1
    SELF.Q.wob:Loan_Status_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.wob:OrderNumber_NormalFG = 255
    SELF.Q.wob:OrderNumber_NormalBG = 16777215
    SELF.Q.wob:OrderNumber_SelectedFG = 16777215
    SELF.Q.wob:OrderNumber_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.wob:OrderNumber_NormalFG = 32768
    SELF.Q.wob:OrderNumber_NormalBG = 16777215
    SELF.Q.wob:OrderNumber_SelectedFG = 16777215
    SELF.Q.wob:OrderNumber_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.wob:OrderNumber_NormalFG = 16711680
    SELF.Q.wob:OrderNumber_NormalBG = 16777215
    SELF.Q.wob:OrderNumber_SelectedFG = 16777215
    SELF.Q.wob:OrderNumber_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.wob:OrderNumber_NormalFG = 7961087
    SELF.Q.wob:OrderNumber_NormalBG = 16777215
    SELF.Q.wob:OrderNumber_SelectedFG = 16777215
    SELF.Q.wob:OrderNumber_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.wob:OrderNumber_NormalFG = 8388736
    SELF.Q.wob:OrderNumber_NormalBG = 16777215
    SELF.Q.wob:OrderNumber_SelectedFG = 16777215
    SELF.Q.wob:OrderNumber_SelectedBG = 8388736
  ELSE
    SELF.Q.wob:OrderNumber_NormalFG = -1
    SELF.Q.wob:OrderNumber_NormalBG = -1
    SELF.Q.wob:OrderNumber_SelectedFG = -1
    SELF.Q.wob:OrderNumber_SelectedBG = -1
  END
  IF (tmp:BrowseFlag = 1)
    SELF.Q.tmp:Courier_NormalFG = 255
    SELF.Q.tmp:Courier_NormalBG = 16777215
    SELF.Q.tmp:Courier_SelectedFG = 16777215
    SELF.Q.tmp:Courier_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.tmp:Courier_NormalFG = 32768
    SELF.Q.tmp:Courier_NormalBG = 16777215
    SELF.Q.tmp:Courier_SelectedFG = 16777215
    SELF.Q.tmp:Courier_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.tmp:Courier_NormalFG = 16711680
    SELF.Q.tmp:Courier_NormalBG = 16777215
    SELF.Q.tmp:Courier_SelectedFG = 16777215
    SELF.Q.tmp:Courier_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.tmp:Courier_NormalFG = 7961087
    SELF.Q.tmp:Courier_NormalBG = 16777215
    SELF.Q.tmp:Courier_SelectedFG = 16777215
    SELF.Q.tmp:Courier_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.tmp:Courier_NormalFG = 8388736
    SELF.Q.tmp:Courier_NormalBG = 16777215
    SELF.Q.tmp:Courier_SelectedFG = 16777215
    SELF.Q.tmp:Courier_SelectedBG = 8388736
  ELSE
    SELF.Q.tmp:Courier_NormalFG = -1
    SELF.Q.tmp:Courier_NormalBG = -1
    SELF.Q.tmp:Courier_SelectedFG = -1
    SELF.Q.tmp:Courier_SelectedBG = -1
  END
  SELF.Q.wob:HeadAccountNumber_NormalFG = -1
  SELF.Q.wob:HeadAccountNumber_NormalBG = -1
  SELF.Q.wob:HeadAccountNumber_SelectedFG = -1
  SELF.Q.wob:HeadAccountNumber_SelectedBG = -1
  SELF.Q.wob:RecordNumber_NormalFG = -1
  SELF.Q.wob:RecordNumber_NormalBG = -1
  SELF.Q.wob:RecordNumber_SelectedFG = -1
  SELF.Q.wob:RecordNumber_SelectedBG = -1
  SELF.Q.wob:JobNumber_NormalFG = -1
  SELF.Q.wob:JobNumber_NormalBG = -1
  SELF.Q.wob:JobNumber_SelectedFG = -1
  SELF.Q.wob:JobNumber_SelectedBG = -1
  SELF.Q.wob:RefNumber_NormalFG = -1
  SELF.Q.wob:RefNumber_NormalBG = -1
  SELF.Q.wob:RefNumber_SelectedFG = -1
  SELF.Q.wob:RefNumber_SelectedBG = -1
  SELF.Q.wob:Manufacturer_NormalFG = -1
  SELF.Q.wob:Manufacturer_NormalBG = -1
  SELF.Q.wob:Manufacturer_SelectedFG = -1
  SELF.Q.wob:Manufacturer_SelectedBG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_NormalFG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_NormalBG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_SelectedFG = -1
  SELF.Q.job_ali:Exchange_Unit_Number_SelectedBG = -1
  IF (tmp:BrowseFlag = 1)
    SELF.Q.jobe:DespatchType_NormalFG = 255
    SELF.Q.jobe:DespatchType_NormalBG = 16777215
    SELF.Q.jobe:DespatchType_SelectedFG = 16777215
    SELF.Q.jobe:DespatchType_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.jobe:DespatchType_NormalFG = 32768
    SELF.Q.jobe:DespatchType_NormalBG = 16777215
    SELF.Q.jobe:DespatchType_SelectedFG = 16777215
    SELF.Q.jobe:DespatchType_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.jobe:DespatchType_NormalFG = 16711680
    SELF.Q.jobe:DespatchType_NormalBG = 16777215
    SELF.Q.jobe:DespatchType_SelectedFG = 16777215
    SELF.Q.jobe:DespatchType_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.jobe:DespatchType_NormalFG = 7961087
    SELF.Q.jobe:DespatchType_NormalBG = 16777215
    SELF.Q.jobe:DespatchType_SelectedFG = 16777215
    SELF.Q.jobe:DespatchType_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.jobe:DespatchType_NormalFG = 8388736
    SELF.Q.jobe:DespatchType_NormalBG = 16777215
    SELF.Q.jobe:DespatchType_SelectedFG = 16777215
    SELF.Q.jobe:DespatchType_SelectedBG = 8388736
  ELSE
    SELF.Q.jobe:DespatchType_NormalFG = -1
    SELF.Q.jobe:DespatchType_NormalBG = -1
    SELF.Q.jobe:DespatchType_SelectedFG = -1
    SELF.Q.jobe:DespatchType_SelectedBG = -1
  END
  SELF.Q.job_ali:Loan_Unit_Number_NormalFG = -1
  SELF.Q.job_ali:Loan_Unit_Number_NormalBG = -1
  SELF.Q.job_ali:Loan_Unit_Number_SelectedFG = -1
  SELF.Q.job_ali:Loan_Unit_Number_SelectedBG = -1
  SELF.Q.job_ali:Current_Status_NormalFG = -1
  SELF.Q.job_ali:Current_Status_NormalBG = -1
  SELF.Q.job_ali:Current_Status_SelectedFG = -1
  SELF.Q.job_ali:Current_Status_SelectedBG = -1
  IF (tmp:BrowseFlag = 1)
    SELF.Q.job_ali:EDI_Batch_Number_NormalFG = 255
    SELF.Q.job_ali:EDI_Batch_Number_NormalBG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedFG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedBG = 255
  ELSIF (tmp:BrowseFlag = 2)
    SELF.Q.job_ali:EDI_Batch_Number_NormalFG = 32768
    SELF.Q.job_ali:EDI_Batch_Number_NormalBG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedFG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedBG = 32768
  ELSIF (tmp:BrowseFlag = 3)
    SELF.Q.job_ali:EDI_Batch_Number_NormalFG = 16711680
    SELF.Q.job_ali:EDI_Batch_Number_NormalBG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedFG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedBG = 16711680
  ELSIF (tmp:BrowseFlag = 4)
    SELF.Q.job_ali:EDI_Batch_Number_NormalFG = 7961087
    SELF.Q.job_ali:EDI_Batch_Number_NormalBG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedFG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedBG = 7961087
  ELSIF (tmp:BrowseFlag = 5)
    SELF.Q.job_ali:EDI_Batch_Number_NormalFG = 8388736
    SELF.Q.job_ali:EDI_Batch_Number_NormalBG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedFG = 16777215
    SELF.Q.job_ali:EDI_Batch_Number_SelectedBG = 8388736
  ELSE
    SELF.Q.job_ali:EDI_Batch_Number_NormalFG = -1
    SELF.Q.job_ali:EDI_Batch_Number_NormalBG = -1
    SELF.Q.job_ali:EDI_Batch_Number_SelectedFG = -1
    SELF.Q.job_ali:EDI_Batch_Number_SelectedBG = -1
  END
  SELF.Q.tmp:Cost_NormalFG = -1
  SELF.Q.tmp:Cost_NormalBG = -1
  SELF.Q.tmp:Cost_SelectedFG = -1
  SELF.Q.tmp:Cost_SelectedBG = -1
  SELF.Q.wob:EDI_NormalFG = -1
  SELF.Q.wob:EDI_NormalBG = -1
  SELF.Q.wob:EDI_SelectedFG = -1
  SELF.Q.wob:EDI_SelectedBG = -1
  SELF.Q.job_ali:date_booked_NormalFG = -1
  SELF.Q.job_ali:date_booked_NormalBG = -1
  SELF.Q.job_ali:date_booked_SelectedFG = -1
  SELF.Q.job_ali:date_booked_SelectedBG = -1
  SELF.Q.job_ali:time_booked_NormalFG = -1
  SELF.Q.job_ali:time_booked_NormalBG = -1
  SELF.Q.job_ali:time_booked_SelectedFG = -1
  SELF.Q.job_ali:time_booked_SelectedBG = -1
  IF (localTag = '*')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  tmp:failed = FALSE
  
  access:jobs_alias.clearkey(job_ali:Ref_number_Key)
  job_ali:ref_number = wob:refnumber
  if access:jobs_alias.fetch(job_ali:Ref_Number_key)
  
      tmp:Company_Name = 'BOOKING ABORTED'
      tmp:failed = TRUE
      tmp:Courier = 'N/A'
      tmp:datecompleted = ''
  
      ! Changing (DBH 11/23/2005) #6599 - Now want the location to show "Despatched" for aborted jobs
      ! tmp:Location          = 'N/A'
      ! to (DBH 11/23/2005) #6599
      tmp:Location          = 'DESPATCHED'
      ! End (DBH 11/23/2005) #6599
  
  !    if job_ali:Exchange_Unit_Number = 0 then
  !        !exchange unit attached
  !        tmp:Current_status    = 'N/A'
  !    ELSE
  !        tmp:Current_status    = 'N/A'
  !    END
      ! Show the wob status because this should be 799 JOB CANCELLED - TrkBs: 6599 (DBH: 11-11-2005)
      tmp:Current_Status = wob:Current_Status
      tmp:Current_StatusBase = wob:Current_Status
      tmp:Loan_status        = 'N/A'
      tmp:Exchange_Status    = 'N/A'
  
  else
  
      tmp:DateCompleted   = job_ali:Date_Completed
  
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job_ali:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Filter out despatched jobs for despatch tab
  ! Changing (DBH 30/06/2008) # 9792 - No warranty tab
  !    if choice(?CurrentTab)= 9 then  !On despatch tab
  ! to (DBH 30/06/2008) # 9792
      if choice(?CurrentTab)= 9 then  !On despatch tab
  ! End (DBH 30/06/2008) #9792
          tmp:Courier = wob:DespatchCourier
  !        If jobe:Despatched <> 'REA'  ! AND Job_ali:DESPATCHED <> 'REA'
  !            Return Record:Filtered
  !        End
  !        Case jobe:DespatchType
  !            Of 'JOB'
  !                tmp:Courier           = job_ali:Courier
  !            Of 'EXC'
  !                tmp:Courier           = job_ali:Exchange_Courier
  !            Of 'LOA'
  !                tmp:Courier           = job_ali:Loan_Courier
  !            Else
  !                tmp:Courier           = job_ali:Courier
  !        End !Case jobe:Despatched
  !
  !        if Select_Courier_temp = 'IND'
  !            if Courier_Temp <> tmp:Courier then return(record:filtered).
  !        end
      END !If on despatch tab
  
      !Filter out Hub repair job on Warranty tab
  ! Deleting (DBH 30/06/2008) # 9792 - No warranty tab
  !    if choice(?currenttab) = 8 then !on warranty tab
  !        if jobe:HubRepairDate <> '' then return(record:filtered).
  !        if Tmp:UseWarMan = 1 then
  !            if wob:Manufacturer <> tmp:WarManufacturer then return(record:filtered).
  !        END
  !    END
  ! End (DBH 30/06/2008) #9792
  
      !for all tabs ...
      if job_ali:Warranty_Job ='YES' then
          if job_ali:Chargeable_Job = 'YES' then
              tmp:type = 'S'
          ELSE
              tmp:type = 'W'
          END !if charegeable job
      ELSE
          if job_ali:Chargeable_Job = 'YES'
              tmp:type = 'C'
          ELSE
              tmp:type = 'X'
          END !if chargeable job
      END !if warranty job
  
      tmp:Company_Name      = job_ali:Company_Name
  
      tmp:Location          = job_ali:Location
      if job_ali:Exchange_Unit_Number = 0 then
          !exchange unit attached
          tmp:Current_status    = 'J '& WOB:Current_status
      ELSE
          tmp:Current_status    = 'E '& WOB:Exchange_status
      END
      tmp:Current_StatusBase = WOB:Current_status
      tmp:Loan_status        = wob:Loan_Status
      tmp:Exchange_Status    = wob:Exchange_Status
  
      ! If the ESN is still IN PROGRESS a day later, then we can call that an aborted job - TrkBs: 6599 (DBH: 11-11-2005)
      If wob:IMEINumber = '* IN PROGRESS * ' And job:Date_Booked < Today()
          tmp:Company_Name = 'BOOKING ABORTED'
          ! Inserting (DBH 11/23/2005) #6599 - Show the location as "Despatched" for aborted jobs
          tmp:Location = 'DESPATCHED'
          ! End (DBH 11/23/2005) #6599
      End !If wob:ESN = '* IN PROGRESS * ' And job:Date_Booked < Today()
      ! Inserting (DBH 11/23/2005) #6599 - Show the location as "Despatched" for cancelled, aborted jobs
      If job:Current_Status = '799 JOB CANCELLED'
          tmp:Location = 'DESPATCHED'
      End ! If job:Current_Status = '799 JOB CANCELLED'
      ! End (DBH 11/23/2005) #6599
  END !If access job_alias
  
  access:jobnotes.clearkey(jbn:RefNumberKey)
  jbn:RefNumber = wob:refnumber
  if access:jobnotes.fetch(jbn:RefNumberKey)
      tmp:Engineers_Notes = ''
  ELSE
      tmp:Engineers_notes = jbn:Engineers_Notes
  END
  
  tmp:wob_jobNumber = wob:RefNumber & '-'&tmp:tradeID& wob:JobNumber
  
  ! Inserting (DBH 12/10/2006) # 8120 - Show the job's Chargeable Cost in the browse
  IF job_ali:Chargeable_Job = 'YES'
      tmp:Cost = job_ali:Courier_Cost + jobe:RRCCPartsCost + jobe:RRCCLabourCost + |
                  (job_ali:Courier_Cost * (VatRate(job_ali:Account_Number,'L') / 100)) + |
                  (jobe:RRCCPartsCOst * (VatRate(job_ali:Account_Number,'P') / 100)) + |
                  (jobe:RRCCLabourCOst * (VatRate(job_ali:Account_Number,'L') / 100))
  Else ! IF job:Chargeable_Job = 'YES'
      tmp:Cost = 0
  End ! IF job:Chargeable_Job = 'YES'
  ! End (DBH 12/10/2006) #8120
  !first bit moved to here get exchange and loan and copy address
  
  IF tmp:failed = FALSE then
  
      Case def:browse_option
          Of 'INV'
              address:CompanyName     = job_ali:Company_Name
              address:AddressLine1    = job_ali:Address_Line1
              address:AddressLine2    = job_ali:Address_Line2
              address:AddressLine3    = job_ali:Address_Line3
              address:Postcode        = job_ali:Postcode
              address:TelephoneNumber = job_ali:Telephone_Number
              address:FaxNumber       = job_ali:Fax_Number
  
          Of 'COL'
              address:CompanyName     = job_ali:Company_Name_Collection
              address:AddressLine1    = job_ali:Address_Line1_Collection
              address:AddressLine2    = job_ali:Address_Line2_Collection
              address:AddressLine3    = job_ali:Address_Line3_Collection
              address:Postcode        = job_ali:Postcode_Collection
              address:TelephoneNumber = job_ali:Telephone_Collection
              address:FaxNumber       = ''
          Of 'DEL'
              address:CompanyName     = job_ali:Company_Name_Delivery
              address:AddressLine1    = job_ali:Address_Line1_Delivery
              address:AddressLine2    = job_ali:Address_Line2_Delivery
              address:AddressLine3    = job_ali:Address_Line3_Delivery
              address:Postcode        = job_ali:Postcode_Delivery
              address:TelephoneNumber = job_ali:Telephone_Delivery
              address:FaxNumber       = ''
          Else
              address:CompanyName     = job_ali:Company_Name
              address:AddressLine1    = job_ali:Address_Line1
              address:AddressLine2    = job_ali:Address_Line2
              address:AddressLine3    = job_ali:Address_Line3
              address:Postcode        = job_ali:Postcode
              address:TelephoneNumber = job_ali:Telephone_Number
              address:FaxNumber       = job_ali:Fax_Number
      End!Case def:browse_option
  
      tmp:ExchangeUnitDetails = ''
      tmp:ExchangeUnitDetails2 = ''
      If job_ali:Exchange_Unit_Number <> 0
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = job_ali:Exchange_Unit_Number
          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Found
              tmp:ExchangeUnitDetails = 'Exchange Issued: ' & Clip(xch:ESN)
              tmp:ExchangeUnitDetails2 = 'Model No: ' & Clip(xch:Model_Number)
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      End !job_ali:Exchange_Unit_Number <> 0
  
      tmp:LoanUnitDetails = ''
      tmp:LoanUnitDetails2 = ''
      If job_ali:Loan_Unit_Number <> 0
          Access:LOAN.Clearkey(loa:Ref_Number_Key)
          loa:Ref_Number  = job_ali:Loan_Unit_Number
          If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
              !Found
              tmp:LoanUnitDetails = 'Loan Issued: ' & Clip(loa:ESN)
              tmp:LoanUnitDetails2 = 'Model No: ' & Clip(loa:Model_Number)
          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      End !job_ali:Exchange_Unit_Number <> 0
  
  END
  !Second bit moved  here - status days, date  etc
  !Status Days
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = wob:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number  = wob:RefNumber
  If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
  
  Else ! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
  
  Sat# = tra:IncludeSaturday
  Sun# = tra:IncludeSunday
  
  tmp:StatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)                                                                                    
  aus:RefNumber   = wob:RefNumber
  aus:Type        = 'JOB'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> wob:RefNumber      |
      Or aus:Type        <> 'JOB'      |
          Then Break.  ! End If
      tmp:StatusDate  =  aus:DateChanged
  
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:LoanStatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = wob:RefNumber
  aus:Type        = 'LOA'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> wob:RefNumber      |
      Or aus:Type        <> 'LOA'      |
          Then Break.  ! End If
      tmp:LoanStatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:ExchangeStatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = wob:RefNumber
  aus:Type        = 'EXC'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> wob:RefNumber      |
      Or aus:Type        <> 'EXC'      |
          Then Break.  ! End If
      tmp:ExchangeStatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:JobStatusDays = GetDays(Sat#,Sun#,tmp:StatusDate)
  tmp:ExcStatusDays = GetDays(Sat#,Sun#,tmp:ExchangeStatusDate)
  tmp:LoaStatusDays = GetDays(Sat#,Sun#,tmp:LoanStatusDate)
  
  If job_ali:Date_Completed <> ''
      tmp:JobDays = GetDaysToDate(Sat#,Sun#,job_ali:Date_Booked,job_ali:date_Completed)
  Else !job_ali:Date_Completed <> ''
      tmp:JobDays = GetDays(Sat#,Sun#,job_ali:Date_Booked)
  End !job_ali:Date_Completed <> ''
  
  If jobe:WebJob
      If wob:DateJobDespatched <> ''
          tmp:JobStatusDays = 'N/A'
      End !If wob:DateJobDespatched <> ''
  Else
      if job_ali:Date_Despatched <> ''
          tmp:JobStatusDays = 'N/A'
      End !if job_ali:Date_Despatched <> ''
  End !jobe:WebJob
  
  If job_ali:Exchange_Despatched <> '' Or job_ali:Exchange_Unit_Number = 0
      tmp:ExcStatusDays = 'N/A'
  End !job_ali:Exchange_Despatched <> ''
  
  If job_ali:Loan_Despatched <> '' Or job_ali:Loan_unit_Number = 0
      tmp:LoaStatusDays = 'N/A'
  End !job_ali:Loan_Despatched <> ''
  !colouring moved to last place 23/04/13 JC TB 12983
  !thinking - if moved here then will colour all bits and nothing will be changed after ?
  
      !TB12983 - added 16/09/13 in an attempt to get the display working. The display problem occurs on tab2 - seems to be fixed if we turn off the colouring
      if CHOICE(?CurrentTab) = 2 then
  
          tmp:BrowseFlag = 0
  
      ELSE
  
          If tmp:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  
              If Sub(tmp:Current_StatusBase,1,3) = Sub(GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                  tmp:BrowseFlag = 2
              Else
                  tmp:BrowseFlag = 0
              End !If job:Current_Status = GETINI('RRC','StatusSendToARC',,CLIP(PATH())&'\SB2KDEF.INI')
          Else !tmp:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  
              If tmp:Location = GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')
                  tmp:BrowseFlag = 3
              Else
                  If tmp:Location = GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')
                     tmp:BrowseFlag = 4
                  Else
                      tmp:BrowseFlag = 1
                  End
              End
          End !tmp:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  
          ! DBH #10544 - Show a Purple Line if the job has been booked as a Liquid Repair
          Access:JOBSE.clearkey(jobe:RefNumberKey)
          jobe:RefNumber =job_ali:Ref_Number
          if (access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
              if (jobe:Booking48HourOption = 4)
                  tmp:BrowseFlag = 5
              end
          end
  
  
      END !if we are not on tab2
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = wob:RecordNumber
     GET(glo:Queue20,glo:Queue20.Pointer20)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 100
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
CourierDefaults PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LocalOverWrite       STRING(1)
LocalCourier         STRING(30)
window               WINDOW('Courier Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Courier Defaults'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,92),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           CHECK('Overwrite Outgoing Courier'),AT(248,199),USE(LocalOverWrite),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           GROUP,AT(248,207,184,24),USE(?Group1)
                             PROMPT('Courier'),AT(248,215),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(288,215,116,10),USE(LocalCourier),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                             BUTTON,AT(404,212),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           END
                         END
                       END
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(300,258),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:LocalCourier                Like(LocalCourier)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020523'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CourierDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  LocalOverwrite = GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI')
  LocalCourier = GETINI('DESPATCH','OUTGOINGCOURIER','',CLIP(PATH()) & '\SB2KDEF.INI')
  ! Save Window Name
   AddToLog('Window','Open','CourierDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?LocalCourier{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?LocalCourier{Prop:Tip}
  END
  IF ?LocalCourier{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?LocalCourier{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CourierDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickCouriers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?LocalOverWrite
      IF LocalOverWrite = 'Y' THEN
         UNHIDE(?Group1)
      ELSE
          HIDE(?Group1)
      END !IF
    OF ?Close
      IF LocalOverwrite = 'Y' AND LocalCourier = '' THEN
         SELECT(?LocalCourier)
         CYCLE
      END !IF
      PUTINIext('DESPATCH','OVERWRITECOURIER',LocalOverWrite,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIext('DESPATCH','OUTGOINGCOURIER',LocalCourier,CLIP(PATH()) & '\SB2KDEF.INI')
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020523'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020523'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020523'&'0')
      ***
    OF ?LocalCourier
      IF LocalCourier OR ?LocalCourier{Prop:Req}
        cou:Courier = LocalCourier
        !Save Lookup Field Incase Of error
        look:LocalCourier        = LocalCourier
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            LocalCourier = cou:Courier
          ELSE
            !Restore Lookup On Error
            LocalCourier = look:LocalCourier
            SELECT(?LocalCourier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      cou:Courier = LocalCourier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          LocalCourier = cou:Courier
          Select(?+1)
      ELSE
          Select(?LocalCourier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?LocalCourier)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
WaybillConfirmation PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Accessory_Pointer             LIKE(glo:Accessory_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_job_id          USHORT,AUTO
Local                CLASS
UnProcessed          Procedure(),Byte
                     END
save_waj_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_wayjob_id       USHORT,AUTO
save_wob_id          USHORT,AUTO
save_jobsobf_id      USHORT,AUTO
save_joe_id          USHORT,AUTO
LocalOverWrite       STRING(1)
LocalCourier         STRING(30)
tmp:Location         STRING(30)
tmp:False            BYTE(0)
tmp:True             BYTE(1)
tmp:Present          STRING(3)
tmp:Damaged          STRING(3)
tmp:BlankColumn      STRING(30)
tmp:Scanned_Job_No   LONG
tmp:Scanned_IMEI_No  STRING(20)
tmp:Engineers_Notes  STRING(255)
Unit_Details_Group   GROUP,PRE()
tmp:Job_No           LONG
tmp:Model_No         STRING(30)
tmp:IMEI             STRING(20)
tmp:Job_Type         STRING(30)
tmp:Charge_Type      STRING(30)
tmp:DOP              DATE
                     END
tmp:Initial_Transit_Type STRING(30)
tmp:Accessories_Model_No STRING(30)
Damaged_Accessory_Queue QUEUE,PRE()
Accessory_Name       STRING(30)
                     END
tmp:RecipientType    STRING(30)
tmp:From             STRING(60)
tmp:Subject          STRING(30)
tmp:Txt_Body         STRING(255)
tmp:EmailMessage     STRING(1024)
tmp:CCList           STRING(500)
tmp:SenderEmailAddress STRING(255)
tmp:TradeAccountFilter STRING(30)
tmp:BrowseFlag       BYTE(0)
tmp:CurrentWayBill   LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Exchanged        BYTE(0)
tmp:Blank            STRING(30)
tmp:EngineerOption   STRING(50)
tmp:WaybillNumber    STRING(30)
tmp:WaybillType      STRING(30)
BRW3::View:Browse    VIEW(WAYBILLS)
                       PROJECT(way:WayBillNumber)
                       PROJECT(way:RecordNumber)
                       PROJECT(way:WayBillType)
                       PROJECT(way:Received)
                       PROJECT(way:AccountNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
way:WayBillNumber      LIKE(way:WayBillNumber)        !List box control field - type derived from field
tmp:WaybillType        LIKE(tmp:WaybillType)          !List box control field - type derived from local data
way:RecordNumber       LIKE(way:RecordNumber)         !List box control field - type derived from field
way:WayBillType        LIKE(way:WayBillType)          !Browse key field - type derived from field
way:Received           LIKE(way:Received)             !Browse key field - type derived from field
way:AccountNumber      LIKE(way:AccountNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ACCESSOR)
                       PROJECT(acr:Accessory)
                       PROJECT(acr:Model_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:Present            LIKE(tmp:Present)              !List box control field - type derived from local data
tmp:Present_Icon       LONG                           !Entry's icon ID
tmp:Damaged            LIKE(tmp:Damaged)              !List box control field - type derived from local data
tmp:Damaged_Icon       LONG                           !Entry's icon ID
acr:Accessory          LIKE(acr:Accessory)            !List box control field - type derived from field
tmp:BlankColumn        LIKE(tmp:BlankColumn)          !List box control field - type derived from local data
acr:Model_Number       LIKE(acr:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(WAYJOBS)
                       PROJECT(wayjob:JobNumber)
                       PROJECT(wayjob:IMEINumber)
                       PROJECT(wayjob:ModelNumber)
                       PROJECT(wayjob:DateBooked)
                       PROJECT(wayjob:JobType)
                       PROJECT(wayjob:RecordNumber)
                       PROJECT(wayjob:WayBillNumber)
                       PROJECT(wayjob:Processed)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wayjob:JobNumber       LIKE(wayjob:JobNumber)         !List box control field - type derived from field
wayjob:IMEINumber      LIKE(wayjob:IMEINumber)        !List box control field - type derived from field
wayjob:ModelNumber     LIKE(wayjob:ModelNumber)       !List box control field - type derived from field
wayjob:DateBooked      LIKE(wayjob:DateBooked)        !List box control field - type derived from field
tmp:Exchanged          LIKE(tmp:Exchanged)            !List box control field - type derived from local data
tmp:Exchanged_Icon     LONG                           !Entry's icon ID
tmp:Blank              LIKE(tmp:Blank)                !List box control field - type derived from local data
wayjob:JobType         LIKE(wayjob:JobType)           !List box control field - type derived from field
wayjob:RecordNumber    LIKE(wayjob:RecordNumber)      !Primary key field - type derived from field
wayjob:WayBillNumber   LIKE(wayjob:WayBillNumber)     !Browse key field - type derived from field
wayjob:Processed       LIKE(wayjob:Processed)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW18::View:Browse   VIEW(WAYSUND)
                       PROJECT(was:Description)
                       PROJECT(was:Quantity)
                       PROJECT(was:RecordNumber)
                       PROJECT(was:WAYBILLSRecordNumber)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
was:Description        LIKE(was:Description)          !List box control field - type derived from field
was:Quantity           LIKE(was:Quantity)             !List box control field - type derived from field
was:RecordNumber       LIKE(was:RecordNumber)         !Primary key field - type derived from field
was:WAYBILLSRecordNumber LIKE(was:WAYBILLSRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::way:AccountNumber    LIKE(way:AccountNumber)
HK9::way:Received         LIKE(way:Received)
HK9::way:WayBillType      LIKE(way:WayBillType)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK14::wayjob:Processed    LIKE(wayjob:Processed)
HK14::wayjob:WayBillNumber LIKE(wayjob:WayBillNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Waybill Confirmation'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Options'),USE(?Options)
                           ITEM('Initial Transit Type'),USE(?OptionsInitialTransitType)
                           ITEM('E-Mail Defaults'),USE(?OptionsEMailDefaults)
                           ITEM('Courier Defaults'),USE(?OptionsCourierDefaults)
                         END
                       END
                       PANEL,AT(64,39,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Waybill Confirmation'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,55,144,230),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Awaiting Receipt'),USE(?Tab1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(68,256),USE(?ClearWaybill),TRN,FLAT,ICON('clearp.jpg')
                         END
                         TAB('Received'),USE(?Tab2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       ENTRY(@s30),AT(68,84,124,10),USE(way:WayBillNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Waybill Number'),TIP('Waybill Number'),UPR
                       SHEET,AT(456,55,160,232),USE(?Sheet:Accessories),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Accessories'),USE(?Tab5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Accessories'),AT(460,58),USE(?String10),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(460,87,152,168),USE(?List:3),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)|FI~P~@s1@Q''Present''11L(2)|FI~D~@s1@Q''Damaged''130L(2)|F~Accessory~@s30@120' &|
   'L(2)|F@s30@'),FROM(Queue:Browse:2)
                         END
                         TAB('Tab 14'),USE(?Tab14),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       LIST,AT(68,97,136,158),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('47L(2)|M~Waybill No~@n_8@120L(2)|M~Type~@s30@0L(2)|M~Record Number~@s8@'),FROM(Queue:Browse)
                       SHEET,AT(212,288,240,74),USE(?Sheet:UnitDetails),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Unit Details'),USE(?Unit_Details_Tab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Unit Details'),AT(216,291),USE(?String9),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(220,301),USE(?JobNumber:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n8b),AT(268,301),USE(tmp:Job_No),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Model'),AT(220,313),USE(?Model:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(276,313),USE(tmp:Model_No),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('IMEI'),AT(220,325),USE(?IMEI:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s20),AT(276,325),USE(tmp:IMEI),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Type'),AT(220,337),USE(?JobType:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(276,337),USE(tmp:Job_Type),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Charge Type'),AT(220,345),USE(?ChargeType:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(276,345),USE(tmp:Charge_Type),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('D.O.P'),AT(356,301),USE(?DOP:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@d06b),AT(384,301),USE(tmp:DOP),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Tab 13'),USE(?Tab13),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(456,288,160,74),USE(?Sheet6),COLOR(0D6E7EFH),SPREAD
                         TAB('Notes'),USE(?Engineers_Notes_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(460,304,148,54),USE(tmp:Engineers_Notes),SKIP,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                         END
                       END
                       SHEET,AT(64,288,144,74),USE(?Sheet:Scan),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Scan_Tab)
                           PROMPT('Job Number'),AT(72,294),USE(?tmp:Scanned_Job_No:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(72,306,128,10),USE(tmp:Scanned_Job_No),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('I.M.E.I. Number'),AT(72,322),USE(?tmp:Scanned_IMEI_No:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(72,334,128,10),USE(tmp:Scanned_IMEI_No),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Tab 12'),USE(?Tab12)
                         END
                       END
                       BUTTON('&Rev tags'),AT(468,401,56,16),USE(?DASREVTAG),DISABLE,HIDE
                       BUTTON,AT(68,366),USE(?ReprintWaybillButton),TRN,FLAT,LEFT,ICON('prnwayp.jpg')
                       STRING(@s50),AT(144,370,165,16),USE(tmp:EngineerOption),FONT(,14,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       BUTTON('&Untag All'),AT(524,405,54,14),USE(?DASUNTAGALL),DISABLE,HIDE,LEFT
                       BUTTON('Tag &All'),AT(572,403,48,14),USE(?DASTAGAll),DISABLE,HIDE,LEFT
                       BUTTON('&Tag'),AT(420,403,44,14),USE(?DASTAG),DISABLE,HIDE,LEFT
                       BUTTON,AT(384,366),USE(?Next),DISABLE,TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(548,366),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON('sho&W tags'),AT(616,391,56,16),USE(?DASSHOWTAG),DISABLE,HIDE
                       SHEET,AT(212,39,240,248),USE(?Sheet:Items)
                         TAB('Tab 9'),USE(?Tab:Job)
                           PROMPT('Jobs On Waybill'),AT(216,72,204,12),USE(?WayBillText),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           SHEET,AT(212,54,240,231),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                             TAB('Un-processed Jobs'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               BUTTON,AT(216,256),USE(?NotReceipt),DISABLE,TRN,FLAT,ICON('notrecp.jpg')
                               BUTTON,AT(384,256),USE(?Complete),DISABLE,TRN,FLAT,LEFT,ICON('compp.jpg')
                             END
                             TAB('Processed Jobs'),USE(?ProcessedTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                           END
                           ENTRY(@s8),AT(216,84,64,10),USE(wayjob:JobNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                           LIST,AT(216,96,232,159),USE(?List:2),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('44L(2)|M~Job No~@s10@72L(2)|M~I.M.E.I. Number~@s18@50L(2)|M~Model No~@s30@40L(2)' &|
   '|M~Booked~@d6@11L(2)I~E~@n1@120L(2)@s30@E(0FFFFFFH,0FFFFFFH,0FFFFFFH,0FFFFFFH)4L' &|
   '(2)~Type~@s1@E(0FFFFFFH,0FFFFFFH,0FFFFFFH,0FFFFFFH)'),FROM(Queue:Browse:1)
                         END
                         TAB('Tab 10'),USE(?Tab:Sundry)
                           SHEET,AT(212,55,240,230),USE(?Sheet8),SPREAD
                             TAB('Items On Waybill'),USE(?Tab11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               LIST,AT(216,84,232,171),USE(?List:4),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('132L(2)|M~Description~@s30@32L(2)|M~Quantity~@s30@'),FROM(Queue:Browse:3)
                               BUTTON,AT(380,256),USE(?Button:ReceiveWaybill),TRN,FLAT,ICON('recwayp.jpg')
                             END
                           END
                         END
                       END
                       BUTTON,AT(144,256),USE(?ShowJobsOnWayBill),TRN,FLAT,RIGHT,ICON('showwayp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW3::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet1) = 1 and glo:WebJob = 0
BRW3::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet1) = 2 and glo:WebJob = 0
BRW3::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet1) = 1 and glo:WebJob = 1
BRW3::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 2 and glo:WebJob = 1
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW4                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW4::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet2) = 2
BRW18                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW18::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW6.UpdateBuffer
   glo:Q_Accessory.Accessory_Pointer = acr:Accessory
   GET(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
  IF ERRORCODE()
     glo:Q_Accessory.Accessory_Pointer = acr:Accessory
     ADD(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
    tmp:Present = 'Y'
  ELSE
    DELETE(glo:Q_Accessory)
    tmp:Present = ''
  END
    Queue:Browse:2.tmp:Present = tmp:Present
  IF (tmp:Present = 'Y')
    Queue:Browse:2.tmp:Present_Icon = 1
  ELSE
    Queue:Browse:2.tmp:Present_Icon = 0
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Q_Accessory)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Q_Accessory.Accessory_Pointer = acr:Accessory
     ADD(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Q_Accessory)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Q_Accessory)
    GET(glo:Q_Accessory,QR#)
    DASBRW::7:QUEUE = glo:Q_Accessory
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Q_Accessory)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Accessory_Pointer = acr:Accessory
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Accessory_Pointer)
    IF ERRORCODE()
       glo:Q_Accessory.Accessory_Pointer = acr:Accessory
       ADD(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
CheckForStickyNote      Routine

    !TB12540 - Show contact history if there is a stick note
    !need to find one that has not been cancelled, and is valid for this request
    Access:conthist.clearkey(cht:KeyRefSticky)
    cht:Ref_Number = job:ref_number
    cht:SN_StickyNote = 'Y'
    Set(cht:KeyRefSticky,cht:KeyRefSticky)      
    Loop
        if access:Conthist.next() then break.
        IF cht:Ref_Number <> job:ref_number then break.
        if cht:SN_StickyNote <> 'Y' then break.
        if cht:SN_Completed <> 'Y' and cht:SN_WaybillConf = 'Y' then
            glo:select12 = job:ref_number
            Browse_Contact_History
            BREAK
        END
    END

    EXIT
Check_Accessories Routine
    ! Update accessories and audit trail
    data
tmp:Notes   string(255)
tmp:RRC     string(255)
tmp:ARC     string(255)
    code

    Access:Accessor.ClearKey(acr:Accesory_Key)
    acr:Model_Number = tmp:Accessories_Model_No
    set(acr:Accesory_Key,acr:Accesory_Key)
    loop until Access:Accessor.Next()       ! Loop through the accessories attached to this model number
        if acr:Model_Number <> tmp:Accessories_Model_No then break.

        Access:JobAcc.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = tmp:Job_No
        jac:Accessory  = acr:Accessory
        if not Access:JobAcc.Fetch(jac:Ref_Number_Key)                          ! Is this accessory attached to this job ?
            !Only count accessories that were "sent to ARC" - 4285 (DBH: 26-05-2004)
            Error# = False
            If glo:WebJob = True

            Else !If glo:WebJob = True
                If jac:Attached <> True
                    Error# = True
                End !If jac:Attached <> True
            End !If glo:WebJob = True

            If Error# = False
                tmp:RRC = clip(tmp:RRC) & clip(acr:Accessory)
                if jac:Damaged
                    tmp:RRC = clip(tmp:RRC) & ' (DAMAGED)'
                end
                tmp:RRC = clip(tmp:RRC) & '<13,10>'
            End !If Error# = False
        end

        glo:Q_Accessory.GLO:Accessory_Pointer = acr:Accessory                   ! Is accessory in DASTAG queue
        get(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
        if not errorcode()

            ! Update accessory record
            Access:JobAcc.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = tmp:Job_No
            jac:Accessory  = acr:Accessory
            if Access:JobAcc.Fetch(jac:Ref_Number_Key)                          ! Accessory does not exist on job
                jac:Ref_Number = tmp:Job_No                                     ! Insert accessory record if not found
                jac:Accessory  = acr:Accessory
                !A new accessory has been added, check to see
                !if it has been attached at the ARC - 4285 (DBH: 26-05-2004)
                If glo:WebJob = True
                    jac:Attached = False
                Else !If glo:WebJob = True
                    jac:Attached = True
                End !If glo:WebJob = True
                Access:JobAcc.TryInsert()
            end

            tmp:ARC = clip(tmp:ARC) & clip(glo:Q_Accessory.GLO:Accessory_Pointer)

            jac:Damaged = False
            !Mark the accessories at the ARC as "sent to ARC" - 4285 (DBH: 26-05-2004)
            If glo:WebJob = True
                jac:Attached = False
            Else !If glo:WebJob = True
                jac:Attached = True
            End !If glo:WebJob = True
            Damaged_Accessory_Queue.Accessory_Name = acr:Accessory
            get(Damaged_Accessory_Queue,Damaged_Accessory_Queue.Accessory_Name) ! Is this accessory damaged ?
            if not errorcode()
                tmp:ARC = clip(tmp:ARC) & ' (DAMAGED)'
                jac:Damaged = True
            end

            tmp:ARC = clip(tmp:ARC) & '<13,10>'

            Access:JobAcc.TryUpdate()
        end

    end
    if tmp:ARC <> tmp:RRC                                                       ! Are the accessories at the ARC, RRC the same ?
        !Incase an accessory has not been received. Make sure that the "sent to ARC" accessories match the tagged list - 4285 (DBH: 10-06-2004)
        If ~glo:WebJob
            Save_jac_ID = Access:JOBACC.SaveFile()
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = tmp:Job_No
            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
            Loop
                If Access:JOBACC.NEXT()
                   Break
                End !If
                If jac:Ref_Number <> tmp:Job_No      |
                    Then Break.  ! End If
                glo:Accessory_Pointer = jac:Accessory
                Get(glo:Q_Accessory,glo:Accessory_Pointer)
                IF Error()
                    jac:Attached = False
                Else !IF Error()
                    jac:Attached = True
                End !IF Error()
                Access:JOBACC.Update()
            End !Loop
            Access:JOBACC.RestoreFile(Save_jac_ID)
        End !If ~glo:WebJob

        tmp:Notes = 'ACCESSORIES SENT BY RRC:' & '<13,10>' & tmp:RRC
        tmp:Notes = clip(tmp:Notes) & 'ACCESSORIES RECEIVED AT ARC:' & '<13,10>' & tmp:ARC
    end

    if tmp:Notes <> ''

        IF (AddToAudit(tmp:Job_No,'JOB','ACCESSORIES MISMATCH',tmp:Notes))
        END ! IF

        !There has been an accessory mismatch. Change the status to "query" - 4285 (DBH: 27-05-2004)
        If glo:WebJob
            GetStatus(Sub(GETINI('RRC','StatusRRCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB')
        Else !If glo:WebJob
            GetStatus(Sub(GETINI('RRC','StatusARCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB')
        End !If glo:WebJob
        Access:JOBS.Update()

    end
Display_Accessories Routine
    ! Load the DASTAG queue with the default tagged accessories

    free(glo:Q_Accessory)                   ! Free the tag queue
    free(Damaged_Accessory_Queue)

    if tmp:Accessories_Model_No = ''        ! Is filter valid ?
        BRW6.ResetSort(1)
        exit
    end

    Access:Accessor.ClearKey(acr:Accesory_Key)
    acr:Model_Number = tmp:Accessories_Model_No
    set(acr:Accesory_Key,acr:Accesory_Key)
    loop until Access:Accessor.Next()       ! Loop through the accessories attached to this model number
        if acr:Model_Number <> tmp:Accessories_Model_No then break.
        Access:JobAcc.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = tmp:Job_No
        jac:Accessory  = acr:Accessory
        if not Access:JobAcc.Fetch(jac:Ref_Number_Key)                          ! Is this accessory attached to this job ?
            !Only show accessories that were attached at, or sent to the ARC - 4285 (DBH: 26-05-2004)
            If jac:Attached = True
                glo:Q_Accessory.GLO:Accessory_Pointer = acr:Accessory               ! Add accessory to DASTAG queue
                get(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
                if errorcode()
                    add(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
                end
                if jac:Damaged                                                      ! Is this accessory damaged ?
                    Damaged_Accessory_Queue.Accessory_Name = acr:Accessory
                    get(Damaged_Accessory_Queue,Damaged_Accessory_Queue.Accessory_Name)
                    if errorcode()
                        add(Damaged_Accessory_Queue,Damaged_Accessory_Queue.Accessory_Name)
                    end
                end
            End !If jac:Attached = True
        end
    end

    BRW6.ResetSort(1)
Damaged_Accessory Routine
    ! Mark an accessory as damaged
    get(Queue:Browse:2,choice(?List:3))
    if not errorcode()
        Damaged_Accessory_Queue.Accessory_Name = Queue:Browse:2.acr:Accessory
        get(Damaged_Accessory_Queue,Damaged_Accessory_Queue.Accessory_Name)
        if errorcode()
            Damaged_Accessory_Queue.Accessory_Name = Queue:Browse:2.acr:Accessory
            add(Damaged_Accessory_Queue,Damaged_Accessory_Queue.Accessory_Name)
            tmp:Damaged = 'YES'
        else
            delete(Damaged_Accessory_Queue)
            tmp:Damaged = 'NO'
        end
        Queue:Browse:2.tmp:Damaged = tmp:Damaged
        if (tmp:Damaged = 'YES')
            Queue:Browse:2.tmp:Damaged_Icon = 2
        else
            Queue:Browse:2.tmp:Damaged_Icon = 0
        end
        put(Queue:Browse:2)
        select(?List:3,choice(?List:3))
    end

Email_RRC Routine
    ! Email missing jobs

    Relate:TradeAcc.Open()
    Relate:SubTracc.Open()
    Relate:SUBEMAIL.Open()
    Relate:TRAEMAIL.Open()

    ! Does the trade account want an email sent?
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = way:AccountNumber
    if not Access:SUBTRACC.Fetch(sub:Account_Number_Key)

        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        if not Access:TRADEACC.Tryfetch(tra:Account_Number_Key)
            if tra:Use_Sub_Accounts = 'YES'
                do EmailText

                if sub:EmailRecipientList

                    ! Need to email Recipients
                    tmp:CCList = ''
                    tmp:SenderEmailAddress = ''

                    Access:SUBEMAIL.ClearKey(sue:RecipientTypeKey)
                    sue:RefNumber     = sub:RecordNumber
                    sue:RecipientType = tmp:RecipientType
                    set(sue:RecipientTypeKey,sue:RecipientTypeKey)
                    loop until Access:SUBEMAIL.Next()
                        if sue:RefNumber <> sub:RecordNumber then break.
                        if sue:RecipientType <> tmp:RecipientType then break.
                        if tmp:CCList = ''
                            if tmp:SenderEmailAddress = ''
                                tmp:SenderEmailAddress = Clip(sue:ContactName) & '<' & Clip(sue:EmailAddress) & '>'
                            else
                                tmp:CCList = Clip(sue:ContactName) & '<' & Clip(sue:EmailAddress) & '>'
                            end
                        else !If tmp:CCList = ''
                            tmp:CCList = Clip(tmp:CCList) & ',' & Clip(sue:ContactName) & '<' & Clip(sue:EmailAddress) & '>'
                        end !If tmp:CCList = ''
                    end !Loop

                    SendEmail(tmp:SenderEmailAddress, tmp:CCList,tmp:Subject, |
                              '', '', '', tmp:EmailMessage)

                end !If sub:EmailRecipientList

            else !If tra:Use_Sub_Accounts
                do EmailText

                if tra:EmailRecipientList

                    ! Need to email Recipients
                    tmp:CCList = ''
                    tmp:SenderEmailAddress = ''

                    Access:TRAEMAIL.ClearKey(tre:RecipientKey)
                    tre:RefNumber     = tra:RecordNumber
                    tre:RecipientType = tmp:RecipientType
                    set(tre:RecipientKey,tre:RecipientKey)
                    loop until Access:TRAEMAIL.Next()
                        if tre:RefNumber <> tra:RecordNumber then break.
                        if tre:RecipientType <> tmp:RecipientType then break.
                        if tmp:CCList = ''
                            if tmp:SenderEmailAddress = ''
                                tmp:SenderEmailAddress = Clip(tre:ContactName) & '<' & Clip(tre:EmailAddress) & '>'
                            else
                                tmp:CCList = Clip(tre:ContactName) & '<' & Clip(tre:EmailAddress) & '>'
                            end
                        else !If tmp:CCList = ''
                            tmp:CCList = Clip(tmp:CCList) & ',' & Clip(tre:ContactName) & '<' & Clip(tre:EmailAddress) & '>'
                        end !If tmp:CCList = ''
                    end ! Loop

                    SendEmail(tmp:SenderEmailAddress, tmp:CCList,tmp:Subject, |
                              '', '', '', tmp:EmailMessage)

                end !If tra:EmailRecipientList

            end
        end
    end

    Relate:TradeAcc.Close()
    Relate:SubTracc.Close()
    Relate:SUBEMAIL.Close()
    Relate:TRAEMAIL.Close()
EmailText Routine
    tmp:EmailMessage = Clip(tmp:Txt_Body) & '<13,10>'

    Save_job_ID = Access:JOBS.SaveFile()
    Access:JOBS.ClearKey(job:InConsignKey)
    job:Incoming_Consignment_Number = way:WayBillNumber
    Set(job:InConsignKey,job:InConsignKey)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:Incoming_Consignment_Number <> way:WayBillNumber      |
            Then Break.  ! End If
        If job:Current_Status <> GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')
            Cycle
        End !If job:Current_Status <> GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')

        tmp:EmailMessage = Clip(tmp:EmailMessage) & '------------------------------------------------------' & '<13,10>'
        tmp:EmailMessage = Clip(tmp:EmailMessage) & 'Your Job Reference: ' & job:Ref_Number & '<13,10>'
        tmp:EmailMessage = Clip(tmp:EmailMessage) & 'Manufacturer: ' & job:Manufacturer & '<13,10>'
        tmp:EmailMessage = Clip(tmp:EmailMessage) & 'Model Number: ' & job:Model_Number & '<13,10>'
        tmp:EmailMessage = Clip(tmp:EmailMessage) & 'IMEI Number: ' & job:ESN & '<13,10>'



    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)

Login      Routine
!    glo:select1 = 'N'
!    Insert_Password
!    If glo:select1 = 'Y'
!        Halt
!    Else
!        access:users.open
!        access:users.usefile
!        access:users.clearkey(use:password_key)
!        use:password =glo:password
!        access:users.fetch(use:password_key)
!        set(defaults)
!        access:defaults.next()
!        0{prop:text} = 'ServiceBase 2000 Cellular Progress System. Version: ' & Clip(def:version_number) & '       Current User: ' &|
!                        Clip(use:forename) & ' ' & Clip(use:surname)
!!        If glo:password <> 'JOB:ENTER'
!!            Do Security_Check
!!        End
!        access:users.close
!    End
Process_Scan Routine
    ! Process the scanned in entries
    data
FoundJob    byte
    code
    clear(Unit_Details_Group)
    tmp:Accessories_Model_No = ''
    tmp:Engineers_Notes = ''

    if (tmp:Scanned_Job_No = 0) or (tmp:Scanned_IMEI_No = '')
        Case Missive('Please enter an I.M.E.I. and a Job number.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        do Display_Accessories                                              ! Display accessories for this job
        display()
        select(?tmp:Scanned_Job_No)
        exit
    end

    Case glo:WebJob
        Of 0
            Access:Jobs.ClearKey(job:InConsignKey)
            job:Incoming_Consignment_Number = way:WayBillNumber
            set(job:InConsignKey,job:InConsignKey)
            loop until Access:Jobs.Next()                                           ! Does job exist in the browse ?
                if job:Incoming_Consignment_Number <> way:WayBillNumber then break.
                if job:Ref_Number <> tmp:Scanned_Job_No then cycle.
                if clip(job:ESN) <> clip(tmp:Scanned_IMEI_No) then cycle.
                tmp:Model_No = job:Model_Number
                tmp:IMEI     = job:ESN

                FoundJob = True
                break
            end

        Of 1
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:ConsignmentNoKey)
            job:Consignment_Number = way:WayBillNumber
            Set(job:ConsignmentNoKey,job:ConsignmentNoKey)
            Loop
                If Access:JOBS.NEXT()
                   Break
                End !If
                If job:Consignment_Number <> way:WayBillNumber      |
                    Then Break.  ! End If
                If job:Ref_Number <> tmp:Scanned_Job_No
                    Cycle
                End !If job:Ref_Number <> tmp:Scanned_Job_No
                If job:ESN <> tmp:Scanned_IMEI_No
                    Cycle
                End !If job:ESN <> tmp:Scanned_IMEI_No
                tmp:Model_No = job:Model_Number
                tmp:IMEI     = job:ESN

                FoundJob    = True
                Break
            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)

            !Maybe it's an exchange unit...
            If FoundJob <> True
                Save_wob_ID = Access:WEBJOB.SaveFile()
                Access:WEBJOB.ClearKey(wob:ExcWayBillNoKey)
                wob:ExcWayBillNumber = way:WayBillNumber
                Set(wob:ExcWayBillNoKey,wob:ExcWayBillNoKey)
                Loop
                    If Access:WEBJOB.NEXT()
                       Break
                    End !If
                    If wob:ExcWayBillNumber <> way:WayBillNumber      |
                        Then Break.  ! End If
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = wob:RefNumber
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Found
                        If job:Ref_Number <> tmp:Scanned_Job_No
                            Cycle
                        End !If job:Ref_Number <> tmp:Scanned_Job_No

                        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                        xch:Ref_Number  = job:Exchange_Unit_Number
                        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                            !Found
                            If xch:ESN <> tmp:Scanned_IMEI_No
                                Cycle
                            End !If xch:ESN <> tmp:Scanned_IMEI_No
                        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                            !Error
                            Cycle
                        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    tmp:Model_No = xch:Model_Number
                    tmp:IMEI     = xch:ESN

                    FoundJob = True
                    Break
                End !Loop
                Access:WEBJOB.RestoreFile(Save_wob_ID)
            End !If FoundJob <> True
    End !Case glo:WebJob

    if FoundJob = False
        Case Missive('Error! No matching job found.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        do Display_Accessories                                              ! Display accessories for this job
        display()
        select(?tmp:Scanned_Job_No)
        exit
    end
    tmp:Job_No = job:ref_number

    !Show the Engineer Option - 4285 (DBH: 11-06-2004)
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
        Case jobe:Engineer48HourOption
            Of 1
                tmp:EngineerOption = '48 Hour Exch'
            Of 2
                tmp:EngineerOption = 'ARC Repair'
            Of 3
                tmp:EngineerOption = '7 Day TAT'
            Of 4
                tmp:EngineerOption = 'Standard Repair'
            Else
                tmp:EngineerOption = ''
        End !Case jobe:Engineer48HourOption
    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    if (job:Warranty_Job = 'YES') and (job:Chargeable_Job = 'YES')
        tmp:Job_Type = 'WARRANTY/CHARGEABLE'
        tmp:Charge_Type = clip(job:Warranty_Charge_Type) & '/' & clip(job:Charge_Type)
    else !if (job:Warranty_Job = 'YES') and (job:Chargeable_Job = 'YES')
        if job:Warranty_Job = 'YES'
            tmp:Job_Type = 'WARRANTY'
            tmp:Charge_Type = job:Warranty_Charge_Type
        end
        if job:Chargeable_Job = 'YES'
            tmp:Job_Type = 'CHARGEABLE'
            tmp:Charge_Type = job:Charge_Type
        end
    end !if (job:Warranty_Job = 'YES') and (job:Chargeable_Job = 'YES')

    tmp:DOP = job:DOP

    Access:JobNotes.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    if not Access:JobNotes.Fetch(jbn:RefNumberKey)          ! Fetch engineers notes for this job
        tmp:Engineers_Notes = jbn:Engineers_Notes
    end

    tmp:Accessories_Model_No = job:Model_Number
    do Display_Accessories                                  ! Display accessories for this job

    ?Complete{prop:Disable} = 1
    ?Next{prop:Disable} = 0
    ?NotReceipt{prop:Disable} = False
    Select(?Next)

    display()
Reset_Display Routine
    Select(?Sheet:Items,1)
    Select(?Sheet:Scan,1)
    Select(?Sheet:UnitDetails,1)
    Select(?Sheet:Accessories,1)
    ! Reset the screen for the next job entry
    tmp:Scanned_Job_No = 0
    tmp:Scanned_IMEI_No = ''

    clear(Unit_Details_Group)
    tmp:Accessories_Model_No = ''
    tmp:Engineers_Notes = ''

    do Display_Accessories                                              ! Display accessories for this job
    display()
    select(?tmp:Scanned_Job_No)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020520'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WaybillConfirmation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Finish,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESSOR.Open
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:EXCHANGE.Open
  Relate:LOGGED.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:WAYBCONF.Open
  Relate:WAYBILLJ.Open
  Relate:WAYJOBS.Open
  Relate:WEBJOB.Open
  Access:JOBACC.UseFile
  Access:JOBNOTES.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:CONTHIST.UseFile
  Access:USERS.UseFile
  Access:AUDIT.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:LOCATLOG.UseFile
  Access:WAYBILLS.UseFile
  Access:JOBSENG.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  if glo:WebJob
      tmp:TradeAccountFilter = Clarionet:Global.Param2
  end
  ! --------------------------------------- Bound Fields --------------------------------------- !
  BIND('tmp:False',tmp:False)
  BIND('tmp:True',tmp:True)
  BIND('glo:webJob',glo:webJob)
  BIND('tmp:TradeAccountFilter',tmp:TradeAccountFilter)
  ! --------------------------------------- Bound Fields --------------------------------------- !
  ! --------------------------------------- Bound Fields --------------------------------------- !
  BIND('tmp:CurrentWayBill',tmp:CurrentWayBill)
  ! --------------------------------------- Bound Fields --------------------------------------- !
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:WAYBILLS,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:ACCESSOR,SELF)
  BRW4.Init(?List:2,Queue:Browse:1.ViewPosition,BRW4::View:Browse,Queue:Browse:1,Relate:WAYJOBS,SELF)
  BRW18.Init(?List:4,Queue:Browse:3.ViewPosition,BRW18::View:Browse,Queue:Browse:3,Relate:WAYSUND,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','WaybillConfirmation')
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !Only show "Not Receipt" button at ARC - 4285 (DBH: 14-06-2004)
  If glo:WebJob
      ?NotReceipt{prop:Hide} = True
  End !glo:WebJob
  
  tmp:Initial_Transit_Type = Clip(GETINI('INITIAL','TransitType',,CLIP(PATH())&'\CELRAPWC.INI'))
  
  if tmp:Initial_Transit_Type = ''
      Case Missive('Please set-up the initial transit type before processing jobs.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
  end
  
  Access:TranType.ClearKey(trt:Transit_Type_Key)
  trt:Transit_Type = tmp:Initial_Transit_Type
  Access:TranType.Fetch(trt:Transit_Type_Key)
  
  tmp:RecipientType = Clip(GETINI('EMAIL','Recipient',,CLIP(PATH())&'\CELRAPWC.INI'))
  tmp:From = Clip(GETINI('EMAIL','From',,CLIP(PATH())&'\CELRAPWC.INI'))
  tmp:Subject = Clip(GETINI('EMAIL','Subject',,CLIP(PATH())&'\CELRAPWC.INI'))
  
  glo:file_name = CLIP(PATH()) & '\MAILTXT.DEF'
  If Relate:ExpGen.Open() = Level:Benign
      set(ExpGen,0)
      loop until Access:ExpGen.Next()
          tmp:Txt_Body = clip(tmp:Txt_Body) & clip(gen:Line1) & '<13,10>'
      end
      Relate:ExpGen.Close()
  End ! Relate:ExpGen.Open() = Level:Benign
  
  
  ?Sheet:Items{PROP:Wizard} = True
  ?Sheet:Items{PROP:NoSheet} = True
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,way:TypeRecNumberKey)
  BRW3.AddRange(way:Received)
  BRW3.AddLocator(BRW3::Sort3:Locator)
  BRW3::Sort3:Locator.Init(?way:WayBillNumber,way:WayBillNumber,1,BRW3)
  BRW3.AddSortOrder(,way:TypeRecNumberKey)
  BRW3.AddRange(way:Received)
  BRW3.AddLocator(BRW3::Sort4:Locator)
  BRW3::Sort4:Locator.Init(?way:WayBillNumber,way:WayBillNumber,1,BRW3)
  BRW3.AddSortOrder(,way:TypeAccountRecNumberKey)
  BRW3.AddRange(way:Received)
  BRW3.AddLocator(BRW3::Sort2:Locator)
  BRW3::Sort2:Locator.Init(?way:WayBillNumber,way:WayBillNumber,1,BRW3)
  BRW3.AddSortOrder(,way:TypeAccountRecNumberKey)
  BRW3.AddRange(way:Received)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?way:WayBillNumber,way:WayBillNumber,1,BRW3)
  BRW3.AddSortOrder(,way:TypeAccountRecNumberKey)
  BRW3.AddRange(way:Received)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(?way:WayBillNumber,way:WayBillNumber,1,BRW3)
  BIND('tmp:WaybillType',tmp:WaybillType)
  BRW3.AddField(way:WayBillNumber,BRW3.Q.way:WayBillNumber)
  BRW3.AddField(tmp:WaybillType,BRW3.Q.tmp:WaybillType)
  BRW3.AddField(way:RecordNumber,BRW3.Q.way:RecordNumber)
  BRW3.AddField(way:WayBillType,BRW3.Q.way:WayBillType)
  BRW3.AddField(way:Received,BRW3.Q.way:Received)
  BRW3.AddField(way:AccountNumber,BRW3.Q.way:AccountNumber)
  BRW6.Q &= Queue:Browse:2
  BRW6.AddSortOrder(,acr:Accesory_Key)
  BRW6.AddRange(acr:Model_Number,tmp:Accessories_Model_No)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,acr:Accessory,1,BRW6)
  BIND('tmp:Present',tmp:Present)
  BIND('tmp:Damaged',tmp:Damaged)
  BIND('tmp:BlankColumn',tmp:BlankColumn)
  BIND('tmp:Accessories_Model_No',tmp:Accessories_Model_No)
  ?List:3{PROP:IconList,1} = '~bluetick.ico'
  ?List:3{PROP:IconList,2} = '~redcros.ico'
  BRW6.AddField(tmp:Present,BRW6.Q.tmp:Present)
  BRW6.AddField(tmp:Damaged,BRW6.Q.tmp:Damaged)
  BRW6.AddField(acr:Accessory,BRW6.Q.acr:Accessory)
  BRW6.AddField(tmp:BlankColumn,BRW6.Q.tmp:BlankColumn)
  BRW6.AddField(acr:Model_Number,BRW6.Q.acr:Model_Number)
  BRW4.Q &= Queue:Browse:1
  BRW4.AddSortOrder(,wayjob:JobNumberKey)
  BRW4.AddRange(wayjob:Processed)
  BRW4.AddLocator(BRW4::Sort1:Locator)
  BRW4::Sort1:Locator.Init(?wayjob:JobNumber,wayjob:JobNumber,1,BRW4)
  BRW4.AddSortOrder(,wayjob:JobNumberKey)
  BRW4.AddRange(wayjob:Processed)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?wayjob:JobNumber,wayjob:JobNumber,1,BRW4)
  BIND('tmp:Exchanged',tmp:Exchanged)
  BIND('tmp:Blank',tmp:Blank)
  BIND('tmp:CurrentWayBill',tmp:CurrentWayBill)
  ?List:2{PROP:IconList,1} = '~bluetick.ico'
  BRW4.AddField(wayjob:JobNumber,BRW4.Q.wayjob:JobNumber)
  BRW4.AddField(wayjob:IMEINumber,BRW4.Q.wayjob:IMEINumber)
  BRW4.AddField(wayjob:ModelNumber,BRW4.Q.wayjob:ModelNumber)
  BRW4.AddField(wayjob:DateBooked,BRW4.Q.wayjob:DateBooked)
  BRW4.AddField(tmp:Exchanged,BRW4.Q.tmp:Exchanged)
  BRW4.AddField(tmp:Blank,BRW4.Q.tmp:Blank)
  BRW4.AddField(wayjob:JobType,BRW4.Q.wayjob:JobType)
  BRW4.AddField(wayjob:RecordNumber,BRW4.Q.wayjob:RecordNumber)
  BRW4.AddField(wayjob:WayBillNumber,BRW4.Q.wayjob:WayBillNumber)
  BRW4.AddField(wayjob:Processed,BRW4.Q.wayjob:Processed)
  BRW18.Q &= Queue:Browse:3
  BRW18.AddSortOrder(,was:EnteredKey)
  BRW18.AddRange(was:WAYBILLSRecordNumber,way:RecordNumber)
  BRW18.AddLocator(BRW18::Sort0:Locator)
  BRW18::Sort0:Locator.Init(,was:RecordNumber,1,BRW18)
  BRW18.AddField(was:Description,BRW18.Q.was:Description)
  BRW18.AddField(was:Quantity,BRW18.Q.was:Quantity)
  BRW18.AddField(was:RecordNumber,BRW18.Q.was:RecordNumber)
  BRW18.AddField(was:WAYBILLSRecordNumber,BRW18.Q.was:WAYBILLSRecordNumber)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  BRW18.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW18.AskProcedure = 0
      CLEAR(BRW18.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  do DASBRW::7:DASUNTAGALL
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:EXCHANGE.Close
    Relate:LOGGED.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:WAYBCONF.Close
    Relate:WAYBILLJ.Close
    Relate:WAYJOBS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','WaybillConfirmation')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Next
      !If on second (received) tab, check if the user has access
      !to receive jobs on a competed waybill - 3998 (DBH: 08-03-2004)
      If Choice(?Sheet1) = 2
          If SecurityCheck('RECOMPLETE RECEIVED WAYBILL')
              Case Missive('You do not have a sufficient access to receive a job on a completed waybill.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If SecurityCheck('RECOMPLETE RECEIVED WAYBILL')
      End !Choice(?Sheet1) = 2
      If glo:WebJob
          !Mark to appear in the RRC Web Despatch Table
          !Change the job's location back to the RRC Location
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = tmp:Scanned_Job_No
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
              IF (JobInUse(job:Ref_Number,1))
                  ! #13520 Do not update job if it is in use. (DBH: 02/06/2015)
                  CYCLE
              END ! IF
      
              do CheckForStickyNote
              !Found
              JobUpdated# = 0
              !Are we despatching the job, or the exchange?
              If job:Consignment_Number <> tmp:CurrentWayBill
                  !Double check that it IS the exchange
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber   = job:Ref_Number
                  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Found
                      If wob:ExcWayBillNumber = tmp:CurrentWayBill
                          !Mark the Exchange Unit to be despatched
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber  = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
                              jobe:DespatchType   = 'EXC'
                              !jobe:Despatched     = 'REA'  ! This is now set when the unit is QA'ed
      
                              Access:JOBSE.Update()
                              !Change made by Neil!
                              GetStatus(Sub(GETINI('RRC','ExchangeStatusReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),1,'EXC')                  ! Changes status / creates audit entry
      
                              !TB13233 - J - 11/12/14 - add location details to Exchange units - this is now at RRC
                              UpdateExchLocation(job:Ref_number,Use:user_code,'AT RRC')
                              !END TB13223
      
                              job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
      
                              !Reset the Courier back to the Trade Account default.. as per booking
                              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                              sub:Account_Number  = job:Account_Number
                              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                  !Found
                                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                  tra:Account_Number  = sub:Main_Account_Number
                                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                      !Found
                                      If tra:Use_Sub_Accounts = 'YES'
                                          job:Courier = sub:Courier_Outgoing
                                      Else !If tra:Use_Sub_Accounts = 'YES'
                                          job:Courier = tra:Courier_Outgoing
                                      End !If tra:Use_Sub_Accounts = 'YES'
                                  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                      !Error
                                  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
                              Access:JOBS.Update()
                              JobUpdated# = 1
                              Do Reset_Display
                          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      End !If wob:ExcWayBillNumber = tmp:CurrentWayBill
                  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
              Else !If job:Consignment_Number = tmp:CurrentWayBill
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      jobe:DespatchType   = 'JOB'
                      !jobe:Despatched     = 'REA' ! This is now set when the unit is QA'ed
                      jobe:HubRepair      = 0
      
                      Access:JOBSE.Update()
                      !Change made by Neil!
                      tmp:Location    = Clip(GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                      LocationChange(tmp:Location)
                      stanum# = Sub(GETINI('RRC','StatusReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                      GetStatus(stanum#,1,'JOB')                  ! Changes status / creates audit entry
      
                      job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
                      !Reset the Courier back to the Trade Account default.. as per booking
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = job:Account_Number
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Found
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = sub:Main_Account_Number
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Found
                              If tra:Use_Sub_Accounts = 'YES'
                                  job:Courier = sub:Courier_Outgoing
                              Else !If tra:Use_Sub_Accounts = 'YES'
                                  job:Courier = tra:Courier_Outgoing
                              End !If tra:Use_Sub_Accounts = 'YES'
                          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Error
                          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
                      Access:JOBS.Update()
                      JobUpdated# = 1
      
                      !Reset the engineer
                      Access:WEBJOB.Clearkey(wob:RefNumberKey)
                      wob:RefNumber   = job:Ref_Number
                      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                          !Found
                          IF wob:RRCEngineer <> '' And wob:RRCEngineer <> job:Engineer
                              job:Engineer = wob:RRCEngineer
                              Access:JOBS.Update()
      
                              IF (AddToAudit(job:ref_number,'JOB','ENGINEER ALLOCATED: ' & CLip(job:engineer),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)))
                              END ! IF
      
      
                              If Access:JOBSENG.PrimeRecord() = Level:Benign
                                  joe:JobNumber     = job:Ref_Number
                                  joe:UserCode      = job:Engineer
                                  joe:DateAllocated = Today()
                                  joe:TimeAllocated = Clock()
                                  joe:AllocatedBy   = use:User_Code
                                  access:users.clearkey(use:User_Code_Key)
                                  use:User_Code   = job:Engineer
                                  access:users.fetch(use:User_Code_Key)
                                  joe:Status = 'ENGINEER QA'
                                  joe:StatusDate  = Today()
                                  joe:StatusTime  = Clock()
      
                                  joe:EngSkillLevel = use:SkillLevel
                                  joe:JobSkillLevel = jobe:SkillLevel
      
                                  If Access:JOBSENG.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:JOBSENG.TryInsert() = Level:Benign
                              End !If Access:JOBSENG.PrimeRecord() = Level:Benign
                          End !IF wob:RRCEngineer <> ''
                      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                      ! Inserting (DBH 06/03/2007) # 8703 - Check if the selected job has had accessories retained by the RRC
                      Access:JOBACC.Clearkey(jac:Ref_Number_Key)
                      jac:Ref_Number = job:Ref_Number
                      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                      Loop ! Begin Loop
                          If Access:JOBACC.Next()
                              Break
                          End ! If Access:JOBACC.Next()
                          If jac:Ref_Number <> job:Ref_Number
                              Break
                          End ! If jac:Ref_Number <> job:Ref_Number
                          If jac:Attached = 0
                              Case Missive('The selected job has accessories that have been retained by the RRC.','ServiceBase 3g',|
                                             'mexclam.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Break
                          End ! If jac:Attached = ''
                      End ! Loop
                      ! End (DBH 06/03/2007) #8703
      
                      Do Reset_Display
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
              End !If job:Consignment_Number = tmp:CurrentWayBill
      
              ! Inserting (DBH 06/03/2007) # 8703 - Don't update the waybill entry if the job hasn't been updated correctly
              If JobUpdated# = 1
              ! End (DBH 06/03/2007) #8703
      
                  !Mark job in list, as processed
                  Access:WAYJOBS.ClearKey(wayjob:JobNumberKey)
                  wayjob:WayBillNumber = tmp:CurrentWayBill
                  wayjob:Processed     = 0
                  wayjob:JobNumber     = job:Ref_Number
                  If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
                      !Found
                      wayjob:Processed = 1
                      Access:WAYJOBS.Update()
                  Else!If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
      
                  If Local.UnProcessed() = 0
                      Case Missive('This waybill is now complete.','ServiceBase 3g',|
                                     'mexclam.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Post(Event:Accepted,?Complete)
                  End !If Local.UnProcessedJobs() = 0
              End ! If JobUpdated# = 1
      
      
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      Else !glo:WebJob
      
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = tmp:Scanned_Job_No
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Found
              IF (JobInUse(job:Ref_Number,1))
                  ! #13520 Do not update job if it is in use. (DBH: 02/06/2015)
                  CYCLE
              END ! IF
      
              do CheckForStickyNote
              !Reget job number.. incase it's disappeared from view
      
              !Mark job in list, as processed
              Access:WAYJOBS.ClearKey(wayjob:JobNumberKey)
              wayjob:WayBillNumber = tmp:CurrentWayBill
              wayjob:Processed     = 0
              wayjob:JobNumber     = job:Ref_Number
              If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
      
              end ! If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
      
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber    = job:Ref_Number
              if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                  ! Found
              else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
      
              ! Insert --- Check if a new job is required. Allow the user to cancel (DBH: 17/04/2009) #10473
              continue# = 1
              if (wayjob:JobType = 'J')
                  if (job:Exchange_Unit_Number <> 0)
                      if (jobe:Engineer48HourOption = 1)
                          Beep(Beep:SystemExclamation)  ;  Yield()
                          Case Missive('This is a faulty exchange unit from an RRC.'&|
                              '|'&|
                              '|Depending on the manufacturer/model, you may need to book a new job to repair this unit.'&|
                              '|Are you sure you want to continue?','ServiceBase',|
                                         'mexclam.jpg','\&No|/&Yes')
                              Of 2 ! &Yes Button
                              Of 1 ! &No Button
                                  continue# = 0
                          End!Case Message
                      else ! if (jobe:Engineer48HourOption = 1)
                          Beep(Beep:SystemExclamation)  ;  Yield()
                          Case Missive('This is a faulty exchange unit from an RRC.','ServiceBase',|
                                         'mexclam.jpg','/&OK')
                              Of 1 ! &OK Button
                          End!Case Message
                      end ! if (jobe:Engineer48HourOption = 1)
                  end !if (job:Exchange_Unit_Number <> 0)
              end !if (wayjob:JobType = 'J')
              ! end --- (DBH: 17/04/2009) #10473
      
              if (continue# = 1)
                  Access:TranType.ClearKey(trt:Transit_Type_Key)
                  trt:Transit_Type = tmp:Initial_Transit_Type
                  if not Access:TranType.Fetch(trt:Transit_Type_Key)
                      job:Transit_Type = trt:Transit_Type                             ! Update transit type
                      if trt:InternalLocation <> ''                                   ! Update location
                          job:Location = trt:InternalLocation
                      end
                      LocationChange(Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                      stanum# = Sub(GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                      GetStatus(stanum#,1,'JOB')                  ! Changes status / creates audit entry
                      !Autochange the courier?
                      If GETINI('DESPATCH','OVERWRITECOURIER',,CLIP(PATH())&'\SB2KDEF.INI')
                          job:Courier = GETINI('DESPATCH','OUTGOINGCOURIER',,CLIP(PATH())&'\SB2KDEF.INI')
                      End !If GETINI('DESPATCH','OVERWRITECOURIER',,CLIP(PATH())&'\SB2KDEF.INI')
      
                      job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
      
                      If Access:Jobs.TryUpdate() = Level:Benign
                          ! Inserting (DBH 14/03/2007) # 8718 - Job has now arrived at the ARC.. is it an obf?
                          If jobe:OBFValidated = 1
                              Relate:JOBSOBF.Open()
                              Access:JOBSOBF.ClearKey(jof:RefNumberKey)
                              jof:RefNumber = job:Ref_Number
                              If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                                  !Found
                                  jof:Status = 1
                                  Access:JOBSOBF.Update()
                              Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                                  !Error
                              End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                              Relate:JOBSOBF.Close
                          End ! If jobe:OBFValidated = 1
                          ! End (DBH 14/03/2007) #8718
                      End ! If Access:Jobs.TryUpdate() = Level:Benign
      
      
      
                      do Check_Accessories                                            ! Create audit trail entry - accessories
                  end
      
                  if trt:workshop_label = 'YES'                                       ! Print labels
                      set(defaults)
                      Access:Defaults.Next()
                      if def:use_job_label = 'YES'
                          glo:select1 = job:Ref_Number
                          if job:bouncer = 'B'
                              Thermal_Labels('SE')
                          else!If job:bouncer = 'B'
                              if ExchangeAccount(job:Account_Number)
                                  Thermal_Labels('ER')
                              else !If ExchangeAccount(job:Account_Number)
                                  Thermal_Labels('')
                              end !If ExchangeAccount(job:Account_Number)
                          end!If job:bouncer = 'B'
                          glo:select1 = ''
                          if trt:exchange_unit = 'YES'
                              glo:select1  = job:Ref_Number
                              job_retained_accessories_label
                              glo:select1  = ''
                          end
                      end!If print_label_temp = 'YES'
                  end
      
                  if trt:job_card = 'YES'                                             ! Print job card
                      glo:select1  = job:Ref_Number
                      Job_Card
                      glo:select1  = ''
                  end!If trt:job_card = 'YES'
      
                  !Found
                  wayjob:Processed = 1
                  Access:WAYJOBS.Update()
      
                  do Reset_Display                                                    ! Reset screen for next job
      
                  !check for OBF
                  access:jobse.clearkey(jobe:RefNumberKey)
                  jobe:RefNumber = job:ref_number
                  if access:jobse.fetch(jobe:refNumberKey)
                      !error
                  ELSE
                      if jobe:OBFvalidated = true then
                          Case Missive('Out of box failure.'&|
                            '<13,10>'&|
                            '<13,10>Pass to OBF Department.','ServiceBase 3g',|
                                         'mexclam.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          GetStatus('202',1,'JOB')  !change status and update audit entry
      
                          job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
      
                          Access:JOBS.Update()
                      End !If OBF validated
                  END !If access:jobse.fetch
                  If wayjob:JobType = 'J'
                      If job:Exchange_Unit_Number <> 0
                          if (jobe:Engineer48HourOption = 1)
                              ! Insert --- Check if the manufacturer, or model are not excluded (DBH: 05/05/2009) #10795
                              if (excludeModelFromRebooking(job:Manufacturer,job:Model_Number) = 0)
                              ! end --- (DBH: 05/05/2009) #10795
                                  ! 48 Hour, now complete the job and book a new one (DBH: 10/03/2009) #10473
                                  CompleteAndRebookJob(job:Ref_Number,'MAIN STORE','48HR')
                              end ! if (excludeModelFromRebooking(job:Model_Number,job:Manufacturer) = 0)
                          end ! if (jobe:Engineer48Option = 1)
                      End !If job:Exchange_Unit_Number <> 0
                  End !wayjob:JobType = 'JOB'
      
                  If Local.UnProcessed() = 0
                      Case Missive('This waybill is now complete.','ServiceBase 3g',|
                                     'mexclam.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Post(Event:Accepted,?Complete)
                  End !If Local.UnProcessedJobs() = 0
              end ! if (continue# = 1)
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      End !glo:WebJob
      
      BRW4.ResetSort(1)
      ?Complete{prop:Disable} = 0
      ?NotReceipt{prop:Disable} = True
      ?Next{prop:Disable} = 1
      tmp:EngineerOption = ''
      !Select(?tmp:Scanned_IMEI_No)
      !TB13276 - J - 22/04/14 - After all actions select the job number field
      Select(?tmp:Scanned_Job_No)
      
    OF ?Finish
      if glo:webJob
          ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
          !Now log out
          access:users.clearkey(use:password_key)
          use:password = glo:password
          if access:users.fetch(use:password_key) = Level:Benign
              access:logged.clearkey(log:user_code_key)
              log:user_code = use:user_code
              log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
              if access:logged.fetch(log:user_code_key) = Level:Benign
                  delete(logged)
              end !if
          end !if
      end
    OF ?Complete
      Error# = 0
      If Local.UnProcessed() <> 0
          If glo:WebJob
              Case Missive('Are you sure you want to complete this waybill?'&|
                '<13,10>'&|
                '<13,10>Any un-processed jobs will be marked as "Query".','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      Error# = 1
              End ! Case Missive
          Else !If glo:WebJob
              Case Missive('Are you sure you want to complete this waybill?'&|
                '<13,10>'&|
                '<13,10>Any un-processed jobs will be marked as "Query" and an e-mail will be send to the RRC listing these jobs.','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      Error# = 1
              End ! Case Missive
          End !If glo:WebJob
      End !Local.UnProcessed() <> 0
      
      If Error# = 0
      
          BRW3.UpdateBuffer()
      
          Temp_WayBill" = way:WayBillNumber
      
          Access:WayBills.ClearKey(way:WayBillNumberKey)
          way:WayBillNumber = brw3.q.way:WayBillNumber
          if not Access:WayBills.Fetch(way:WayBillNumberKey)
              Error# = 0
              !Only allow to re-complete a received waybill, depending on access level - 3909 (DBH: 20-02-2004)
              If way:Received = True
                  If SecurityCheck('RECOMPLETE RECEIVED WAYBILL')
                      Case Missive('You do not have sufficient access to re-complete a received waybill.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Error# = 1
                  End !If SecurityCheck('RECOMPLETE RECEIVED WAYBILL')
              Else !If way:Received = True
                  way:Received = True                                     ! Update waybill as received
              End !If way:Received = True
              If Error# = 0
                  Access:WayBills.TryUpdate()
                  If ~glo:WebJob
                      do Email_RRC
                      !Ok here we are
                      Save_job_ID = Access:JOBS.SaveFile()
                      Access:JOBS.ClearKey(job:InConsignKey)
                      job:Incoming_Consignment_Number = way:WayBillNumber
                      Set(job:InConsignKey,job:InConsignKey)
                      Loop
                          If Access:JOBS.NEXT()
                             Break
                          End !If
                          If job:Incoming_Consignment_Number <> way:WayBillNumber      |
                              Then Break.  ! End If
      
                          If job:Current_Status = GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')
                              Cycle
                          End !If job:Current_Status <> GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')
                          If job:Location = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                              Cycle
                          End !If job:Location = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
      
                          !Has this job been at the ARC location at all? - 4142 (DBH: 20-04-2004)
                          Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                          lot:RefNumber   = job:Ref_Number
                          lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                          If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                              !Found
                              Cycle
                          Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                              !Error
                          End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
      
                          stanum# = Sub(GETINI('RRC','StatusARCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                          GetStatus(stanum#,0,'JOB')
      
                          job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
      
                          Access:JOBS.Update()
      
                          IF (AddToAudit(job:Ref_Number,'JOB','WAYBILL CONFIRMATION:RECEIVED AT ARC',''))
                          END ! IF
      
                      End !Loop
                      Access:JOBS.RestoreFile(Save_job_ID)
      
                  Else
                      Save_job_ID = Access:JOBS.SaveFile()
                      Access:JOBS.ClearKey(job:ConsignmentNoKey)
                      job:Consignment_Number = way:WayBillNumber
                      Set(job:ConsignmentNoKey,job:ConsignmentNoKey)
                      Loop
                          If Access:JOBS.NEXT()
                             Break
                          End !If
                          If job:Consignment_Number <> way:WayBillNumber      |
                              Then Break.  ! End If
                          If job:Current_Status = GETINI('RRC','StatusReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI')
                              Cycle
                          End !If job:Current_Status <> GETINI('RRC','StatusReceivedAtARC',,CLIP(PATH())&'\SB2KDEF.INI')
      
                          If job:Location = Clip(GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                              Cycle
                          End !If job:Location = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
      
                          !Has this unit been despatched, i.e. it has been received at the RRC - 4142 (DBH: 20-04-2004)
                          Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                          lot:RefNumber   = job:Ref_Number
                          lot:NewLocation = Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI'))
                          If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                              !Found
                              Cycle
                          Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                              !Error
                          End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
      
                          stanum# = Sub(GETINI('RRC','StatusRRCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                          GetStatus(stanum#,0,'JOB')
      
                          job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
      
                          Access:JOBS.Update()
                      End !Loop
                      Access:JOBS.RestoreFile(Save_job_ID)
      
                      Save_wob_ID = Access:WEBJOB.SaveFile()
                      Access:WEBJOB.ClearKey(wob:ExcWayBillNoKey)
                      wob:ExcWayBillNumber = way:WayBillNumber
                      Set(wob:ExcWayBillNoKey,wob:ExcWayBillNoKey)
                      Loop
                          If Access:WEBJOB.NEXT()
                             Break
                          End !If
                          If wob:ExcWayBillNumber <> way:WayBillNumber      |
                              Then Break.  ! End If
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = wob:RefNumber
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
      
                          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          If job:Exchange_Status = GETINI('RRC','ExchangeStatusReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI')
                              Cycle
                          End !If job:Exchange_Status = GETINI('RRC','StatusExchangeReceivedAtRRC',,CLIP(PATH())&'\SB2KDEF.INI')
                          stanum# = Sub(GETINI('RRC','StatusRRCReceivedQuery',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                          GetStatus(stanum#,0,'EXC')
      
                          job:Workshop = 'YES'    ! L256 - Added to allow the unit to be QA'ed.
      
                          Access:JOBS.Update()
      
                      End !Loop
                      Access:WEBJOB.RestoreFile(Save_wob_ID)
                  End !If ~glo:WebJob
              End !If Error# = 0
              BRW3.ResetSort(1)                                       ! Refresh list
              BRW4.ResetSort(1)
      
              ?Sheet:Scan{prop:Disable} = 1
              ?Complete{prop:Disable} = 1
              ?Next{prop:Disable} = 1
          end
      
      End !Error# = 0
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OptionsInitialTransitType
      ThisWindow.Update
      TransitTypeWindow
      ThisWindow.Reset
    OF ?OptionsEMailDefaults
      ThisWindow.Update
      WayBillEmailDefaults
      ThisWindow.Reset
    OF ?OptionsCourierDefaults
      ThisWindow.Update
      CourierDefaults
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020520'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020520'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020520'&'0')
      ***
    OF ?ClearWaybill
      ThisWindow.Update
      !Move spurious entry to Received Tab - 3909 (DBH: 20-02-2004)
      If SecurityCheck('CLEAR WAYBILL')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('CLEAR WAYBILL')
          Case Missive('Are you sure you want to mark the selected waybill as received?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
                  way:WayBillNumber = brw3.q.way:WayBillNumber
                  If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                      !Found
                      way:Received = True
                      Access:WAYBILLS.Update()
      
                      !TB13370 - J - 15/04/15 - adding a waybillconf record
                      Access:WaybConf.clearkey(WAC:KeyWaybillNo)
                      WAC:WaybillNo        = way:WayBillNumber
                      if access:Waybconf.fetch(WAC:KeyWaybillNo) = level:Benign
                          !found
                          WAC:ReceiveDate = today()
                          WAC:ReceiveTime = clock()
                          WAC:Received    = 1
                          Access:Waybconf.update()
                      END
      
                  Else !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                      !Error
                  End !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
      
              Of 1 ! No Button
          End ! Case Missive
      End !SecurityCheck('CLEAR WAYBILL')
      Brw3.ResetSort(1)
      !Select(?tmp:Scanned_IMEI_No)
      !TB13276 - J - 22/04/14 - After completing select the job number field
      if Way:Received then
        Select(?tmp:Scanned_Job_No)
      ELSE
        Select(?tmp:Scanned_IMEI_No)
      END
      
    OF ?tmp:Scanned_Job_No
      ?tmp:Scanned_IMEI_No{Prop:Touched} = True       ! Force event:accepted on IMEI control
    OF ?tmp:Scanned_IMEI_No
      if 0{Prop:AcceptAll} = False
          do Process_Scan
      end
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ReprintWaybillButton
      ThisWindow.Update
      AskWaybillNumber
      ThisWindow.Reset
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?NotReceipt
      ThisWindow.Update
      !Create Waybill Rejection and return job to RRC - 4285 (DBH: 14-06-2004)
      If SecurityCheck('WAYBILL NOT RECEIPT')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('WAYBILL NOT RECEIPT')
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = tmp:Job_No
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Found
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Found
      
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Found
      
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
          IF JobInUse(job:Ref_Number,1)
              Cycle
          End !IF IsJobInUse(job:Ref_Number)
      
          Case Missive('Are you sure you want to return this unit to the RRC?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  tmp:WayBillNumber = NextWayBillNumber()
                  If tmp:WayBillNumber = 0
                      Case Missive('Error! Unable to get next Waybill number.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  End !If tmp:WayBillNumber = 0
      
                  !Remove job from waybill  (DBH: 14-06-2004)
                  Access:WAYBILLS.Clearkey(way:WayBillNumberKEy)
                  way:WayBillNumber   = brw3.q.way:WayBillNumber
                  If Access:WAYBILLS.Tryfetch(way:WayBillNumberKEy) = Level:Benign
                      !Found
                      Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
                      waj:WayBillNumber = way:WayBillNumber
                      waj:JobNumber     = job:Ref_Number
                      If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                          !Found
                          Delete(WAYBILLJ)
                      Else !If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                          !Error
                          Cycle
                      End !If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                  Else ! If Access:WAYBILLS.Tryfetch(way:WayBillNumberKEy) = Level:Benign
                      !Error
                  End !If Access:WAYBILLS.Tryfetch(way:WayBillNumberKEy) = Level:Benign
      
                  GetStatus(899,1,'JOB') !Failed Delivery
                  !Clear cons number, otherwise the job will still be picked up and marked as query  (DBH: 14-06-2004)
                  job:Incoming_Consignment_Number = ''
                  !Put back to RRC  (DBH: 14-06-2004)
                  LocationChange(GETINI('RRC','RRCLocation',,CLIP(Path()) & '\SB2KDEF.INI'))
                  !Untick Hub Repair  (DBH: 14-06-2004)
                  jobe:HubRepair = False
                  Access:JOBSE.Update()
                  Access:JOBS.Update()
      
                  IF (AddToAudit(job:ref_number,'JOB','WAYBILL REJECTION','WAYBILL REJECTION NUMBER:' & tmp:WaybillNumber))
                  END ! IF
      
                  Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                  way:WaybillNumber   = tmp:WaybillNumber
                  If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                      !Found
                      way:AccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
                      way:WayBillType = 7 !Waybill Rejection
                      way:WayBillID = 13 !WwayBill Rejection
                      way:FromAccount = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
                      way:ToAccount = wob:HeadAccountNumber
                      If Access:WayBills.Update() = Level:Benign
                          If Access:WAYBILLJ.PrimeRecord() = Level:Benign
                              waj:WaybillNumber   = way:WaybillNumber
                              waj:JobNumber       = job:Ref_Number
                              waj:IMEINumber      = job:ESN
                              waj:OrderNumber     = job:Order_Number
                              waj:JobType         = 'JOB'
                              If Access:WAYBILLJ.TryInsert() = Level:Benign
                                  !Insert Successful
                                  WaybillDespatch(way:AccountNUmber,'TRA',way:ToAccount,'TRA',tmp:WayBillNumber,'')
                              Else !If Access:waybillj.TryInsert() = Level:Benign
                                  !Insert Failed
                                  Access:WAYBILLJ.CancelAutoInc()
                              End !If Access:waybillj.TryInsert() = Level:Benign
                          End !If Access:waybillj.PrimeRecord() = Level:Benign
                      End !If Access:WayBills.Update() = Level:Benign
                  Else ! If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                      !Error
                  End !If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
      
                  !Remove job from list  (DBH: 14-06-2004)
                  Access:WAYJOBS.ClearKey(wayjob:JobNumberKey)
                  wayjob:WayBillNumber = tmp:CurrentWayBill
                  wayjob:Processed     = 0
                  wayjob:JobNumber     = job:Ref_Number
                  If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
                      !Found
                      Delete(WAYJOBS)
                  Else!If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:WAYJOBS.TryFetch(wayjob:JobNumberKey) = Level:Benign
      
                  If Local.UnProcessed() = 0
                      Case Missive('This waybill is now complete.','ServiceBase 3g',|
                                     'mexclam.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Post(Event:Accepted,?Complete)
                  End !If Local.UnProcessedJobs() = 0
      
              Of 1 ! No Button
          End ! Case Missive
          BRW4.ResetSort(1)
          ?Complete{prop:Disable} = 0
          ?NotReceipt{prop:Disable} = True
          ?Next{prop:Disable} = 1
          !Select(?tmp:Scanned_IMEI_No)
          !TB13276 - J - 22/04/14 - After completing select the job number field
          if Way:Received then
            Select(?tmp:Scanned_Job_No)
          ELSE
            Select(?tmp:Scanned_IMEI_No)
          END
      
      End !SecurityCheck('WAYBILL NOT RECEIPT')
      
      
      
    OF ?Button:ReceiveWaybill
      ThisWindow.Update
      Case Missive('Are you sure you want to mark the selected waybill as "Received"?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Access:WAYBILLS.ClearKey(way:RecordNumberKey)
              way:RecordNumber = brw3.q.way:RecordNumber
              If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
                  !Found
                  If way:Received = True
                      Case Missive('Warning!'&|
                        '|This waybill has already been received. '&|
                        '|Are you sure you want to receive it again?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                          Of 1 ! No Button
                              Do Reset_Display
                              Select(?tmp:Scanned_Job_No)
                              Cycle
                      End ! Case Missive
                  End ! If way:Received = True
                  way:Received = True
                  If Access:WAYBILLS.Update() = Level:Benign
                      If Access:WAYAUDIT.PrimeRecord() = Level:Benign
                          waa:WAYBILLSRecordNumber = way:RecordNumber
                          Access:USERS.ClearKey(use:Password_Key)
                          use:Password     = glo:Password
                          If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                              !Found
                          Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                              !Error
                          End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                          waa:UserCode = use:User_Code
                          waa:TheDate = Today()
                          waa:TheTime = Clock()
                          waa:Action = 'WAYBILL RECEIVED'
                          If Access:WAYAUDIT.TryInsert() = Level:Benign
                              !Insert
                              brw3.ResetSort(1)
                              Do Reset_Display
                              !Select(?tmp:Scanned_Job_No)
                          Else ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                              Access:WAYAUDIT.CancelAutoInc()
                          End ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                      End ! If Access.WAYAUDIT.PrimeRecord() = Level:Benign
                  End ! If Access:WAYBILLS.Update() = Level:Benign
      
              Else ! If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
                  !Error
              End ! If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
          Of 1 ! No Button
      End ! Case Missive
      
            !TB13276 - J - 22/04/14 - After completing select the job number field
            if Way:Received then
              Select(?tmp:Scanned_Job_No)
            ELSE
              Select(?tmp:Scanned_IMEI_No)
            END
    OF ?ShowJobsOnWayBill
      ThisWindow.Update
      !Show jobs on selected waybill
      !New Job
      If brw3.q.tmp:WaybillTYpe = 'Sundry'
          tmp:CurrentWayBill = brw3.q.way:WayBillNumber
          Select(?Sheet:Items,2)
          Select(?Sheet:Scan,2)
          Select(?Sheet:UnitDetails,2)
          Select(?Sheet:Accessories,2)
          brw18.ResetSort(1)
          Access:WAYBILLS.ClearKey(way:RecordNumberKey)
          way:RecordNumber = brw3.q.way:RecordNumber
          If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
              !Found
              tmp:Engineers_Notes = way:UserNotes
              Display()
          Else ! If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
              !Error
          End ! If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
      Else ! If brw3.q.tmp:WaybillTYpe = 'Sundry'
          Select(?Sheet:Items,1)
          Select(?Sheet:Scan,1)
          Select(?Sheet:UnitDetails,1)
          Select(?Sheet:Accessories,1)
      
      
          NewWay# = 0
      
          tmp:CurrentWayBill = brw3.q.way:WayBillNumber
          Setcursor(Cursor:Wait)
          !Clear List Fist
          Set(wayjob:RecordNumberKey)
          Loop
              If Access:WAYJOBS.NEXT()
                 Break
              End !If
              Delete(WAYJOBS)
          End !Loop
      
          UnProcessed# = 0
          Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
          way:WayBillNumber = tmp:CurrentWayBill
          If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
              !Found
              Save_waj_ID = Access:WAYBILLJ.SaveFile()
              Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
              waj:WayBillNumber = way:WayBillNumber
              Set(waj:JobNumberKey,waj:JobNumberKey)
              Loop
                  If Access:WAYBILLJ.NEXT()
                     Break
                  End !If
                  If waj:WayBillNumber <> way:WayBillNumber      |
                      Then Break.  ! End If
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = waj:JobNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
      
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  If Access:WAYJOBS.PrimeRecord() = Level:Benign
                      wayjob:JobNumber        = waj:JobNumber
                      wayjob:IMEINumber       = waj:IMEINumber
      
                      wayjob:DateBooked       = job:Date_Booked
                      Case waj:JobType
                          Of 'JOB'
                              wayjob:JobType = 'J'
                              wayjob:ModelNumber      = job:Model_Number
                              If glo:WebJob
                                  If job:Location = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
                                      wayjob:Processed = 0
                                      UnProcessed# = 1
                                  Else !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                                      wayjob:Processed = 1
                                  End !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                              Else !If glo:WebJob
                                  If job:Location = Clip(GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI'))
                                      wayjob:Processed = 0
                                      UnProcessed# = 1
                                  Else !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                                      wayjob:Processed = 1
                                  End !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                              End !If glo:WebJob
      
                          Of 'EXC'
                              Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                              xch:Ref_Number  = job:Exchange_Unit_Number
                              If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                  !Found
      
                              Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              wayjob:JobType = 'E'
                              wayjob:ExchangeUnitNumber = job:Exchange_Unit_Number
                              wayjob:ModelNumber      = xch:Model_Number
      
                              If Sub(job:Exchange_Status,1,3) = Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                                  wayjob:Processed = 0
                                  UnProcessed# = 1
                              Else !If Sub(job:Exchange_Status,1,3) = Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                                  wayjob:Processed = 1
                              End !If Sub(job:Exchange_Status,1,3) = Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                      End !Case waj:JobType
                      wayjob:WayBillNumber      = tmp:CurrentWayBill
                      If Access:WAYJOBS.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:WAYJOBS.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:WAYJOBS.CancelAutoInc()
                      End !If Access:WAYJOBS.TryInsert() = Level:Benign
                  End !If Access:WAYJOBS.PrimeRecord() = Level:Benign
                  NewWay# = 1
              End !Loop
              Access:WAYBILLJ.RestoreFile(Save_waj_ID)
              ?WayBillText{prop:Text} = 'Jobs On Waybill: ' & tmp:CurrentWayBill
              ?Sheet:Scan{prop:Disable} = 0
              If Choice(?Sheet1) <> 2
                  ?Complete{prop:Disable} = 0
              End !If Choice(?Sheet1) <> 2
          Else!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
      
          If NewWay# = 0
              If glo:WebJob
                  !Check for jobs that match Consignment Number = Waybill Number
      
                  tmp:CurrentWayBill = brw3.q.way:WayBillNumber
      
      
                  !look for jobs to despatch
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:ConsignmentNoKey)
                  job:Consignment_Number = tmp:CurrentWayBill
                  Set(job:ConsignmentNoKey,job:ConsignmentNoKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Consignment_Number <> tmp:CurrentWayBill      |
                          Then Break.  ! End If
      
                      If Access:WAYJOBS.PrimeRecord() = Level:Benign
                          wayjob:JobNumber        = job:Ref_Number
                          wayjob:IMEINumber       = job:ESN
                          wayjob:ModelNumber      = job:Model_Number
                          wayjob:DateBooked       = job:Date_Booked
                          wayjob:JobType          = 'J'
                          wayjob:ExchangeUnitNumber = 0
                          wayjob:WayBillNumber      = tmp:CurrentWayBill
                          If job:Location = Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI'))
                              wayjob:Processed = 0
                              UnProcessed# = 1
      
                          Else !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                              wayjob:Processed = 1
                          End !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                          If Access:WAYJOBS.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:WAYJOBS.TryInsert() = Level:Benign
                              !Insert Failed
                              Access:WAYJOBS.CancelAutoInc()
                          End !If Access:WAYJOBS.TryInsert() = Level:Benign
                      End !If Access:WAYJOBS.PrimeRecord() = Level:Benign
      
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
      
                  Save_wob_ID = Access:WEBJOB.SaveFile()
                  Access:WEBJOB.ClearKey(wob:ExcWayBillNoKey)
                  wob:ExcWayBillNumber = tmp:CurrentWayBill
                  Set(wob:ExcWayBillNoKey,wob:ExcWayBillNoKey)
                  Loop
                      If Access:WEBJOB.NEXT()
                         Break
                      End !If
                      If wob:ExcWayBillNumber <> tmp:CurrentWayBill      |
                          Then Break.  ! End If
      
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = wob:RefNumber
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Found
                          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                          xch:Ref_Number  = job:Exchange_Unit_Number
                          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              !Found
                              If Access:WAYJOBS.PrimeRecord() = Level:Benign
                                  wayjob:JobNumber        = job:Ref_Number
                                  wayjob:IMEINumber       = xch:ESN
                                  wayjob:ModelNumber      = xch:Model_Number
                                  wayjob:DateBooked       = job:Date_Booked
                                  wayjob:JobType          = 'E'
                                  wayjob:ExchangeUnitNumber = 0
                                  wayjob:WayBillNumber      = tmp:CurrentWayBill
                                  If Sub(job:Exchange_Status,1,3) = Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                                      wayjob:Processed = 0
                                      UnProcessed# = 1
      
                                  Else !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                                      wayjob:Processed = 1
                                  End !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                                  If Access:WAYJOBS.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:WAYJOBS.TryInsert() = Level:Benign
                                      !Insert Failed
                                      Access:WAYJOBS.CancelAutoInc()
                                  End !If Access:WAYJOBS.TryInsert() = Level:Benign
                              End !If Access:WAYJOBS.PrimeRecord() = Level:Benign
      
                          Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Error
                      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  End !Loop
                  Access:WEBJOB.RestoreFile(Save_wob_ID)
      
                  ?WayBillText{prop:Text} = 'Jobs On Waybill: ' & tmp:CurrentWayBill
                  ?Sheet:Scan{prop:Disable} = 0
                  ?Complete{prop:Disable} = 0
      
                  Setcursor()
      
              Else !glo:WebJob
                  !Check for jobs that match Incoming Consignment Number = Waybill Number
      
                  tmp:CurrentWayBill = brw3.q.way:WayBillNumber
      
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:InConsignKey)
                  job:Incoming_Consignment_Number = tmp:CurrentWayBill
                  Set(job:InConsignKey,job:InConsignKey)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Incoming_Consignment_Number <> tmp:CurrentWayBill      |
                          Then Break.  ! End If
                      If Access:WAYJOBS.PrimeRecord() = Level:Benign
                          wayjob:JobNumber        = job:Ref_Number
                          wayjob:IMEINumber       = job:ESN
                          wayjob:ModelNumber      = job:Model_Number
                          wayjob:DateBooked       = job:Date_Booked
                          wayjob:JobType          = 'J'
                          wayjob:ExchangeUnitNumber = 0
                          wayjob:WayBillNumber      = tmp:CurrentWayBill
                          If job:Location = Clip(GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI'))
                              wayjob:Processed = 0
                              UnProcessed# = 1
      
                          Else !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                              wayjob:Processed = 1
                          End !If job:Location = Clip(GETINI('RRC','InTransitARC',,CLIP(PATH())&'\SB2KDEF.INI'))
                          If Access:WAYJOBS.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:WAYJOBS.TryInsert() = Level:Benign
                              !Insert Failed
                              Access:WAYJOBS.CancelAutoInc()
                          End !If Access:WAYJOBS.TryInsert() = Level:Benign
                      End !If Access:WAYJOBS.PrimeRecord() = Level:Benign
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
                  Setcursor()
                  ?WayBillText{prop:Text} = 'Jobs On Waybill: ' & tmp:CurrentWayBill
                  ?Sheet:Scan{prop:Disable} = 0
                  If Choice(?Sheet1) <> 2
                      ?Complete{prop:Disable} = 0
                  End !If Choice(?Sheet1) <> 2
                  Display()
              End !glo:WebJob
          End!If NewWay# = 0
          Select(?tmp:Scanned_Job_No)
          Brw4.ResetSort(1)
      End ! If brw3.q.tmp:WaybillTYpe = 'Sundry'
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    If brw3.q.way:WayBillNumber <> tmp:CurrentWayBill
        ?Sheet:Scan{prop:Disable} = 1
        ?Complete{prop:Disable} = 1
        ?Next{prop:Disable} = 1
        Do Reset_Display
    End !brw3.q.way:WayBillNumber <> tmp:CurrentWayBill
    
  OF ?Sheet2
    BRW4.ResetSort(1)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List:3
      if keycode() = MouseLeft and (?List:3{PROPLIST:MouseDownRow} > 0)
          case ?List:3{PROPLIST:MouseDownField}
              of 2
                  do Damaged_Accessory
                  ?List:3{PROPLIST:MouseDownField} = 3
                  cycle
          end
      end
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.UnProcessed       Procedure()
Code
    Save_wayjob_ID = Access:WAYJOBS.SaveFile()
    Access:WAYJOBS.ClearKey(wayjob:JobNumberKey)
    wayjob:WayBillNumber = brw3.q.way:WayBillNumber
    wayjob:Processed     = 0
    Set(wayjob:JobNumberKey,wayjob:JobNumberKey)
    Loop
        If Access:WAYJOBS.NEXT()
           Break
        End !If
        If wayjob:WayBillNumber <> brw3.q.way:WayBillNumber      |
        Or wayjob:Processed     <> 0      |
            Then Break.  ! End If
        Return 1
        Break
    End !Loop
    Access:WAYJOBS.RestoreFile(Save_wayjob_ID)
    Return 0
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 1 and glo:WebJob = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = glo:WebJob
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:False
  ELSIF Choice(?Sheet1) = 2 and glo:WebJob = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = glo:WebJob
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:True
  ELSIF Choice(?Sheet1) = 1 and glo:WebJob = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = glo:WebJob
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:TradeAccountFilter
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:False
  ELSIF Choice(?Sheet1) = 2 and glo:WebJob = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = glo:WebJob
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:TradeAccountFilter
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:True
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = glo:WebJob
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:TradeAccountFilter
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:False
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 1 and glo:WebJob = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet1) = 2 and glo:WebJob = 0
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet1) = 1 and glo:WebJob = 1
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?Sheet1) = 2 and glo:WebJob = 1
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  IF (way:WaybillID = 300)
    tmp:WaybillType = 'Sundry'
  ELSE
    tmp:WaybillType = 'Job'
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:WaybillType = tmp:WaybillType            !Assign formula result to display queue


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Q_Accessory.Accessory_Pointer = acr:Accessory
     GET(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
    IF ERRORCODE()
      tmp:Present = ''
    ELSE
      tmp:Present = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  tmp:Damaged = 'NO'
  
  Damaged_Accessory_Queue.Accessory_Name = acr:Accessory
  get(Damaged_Accessory_Queue,Damaged_Accessory_Queue.Accessory_Name)
  if not errorcode()
      tmp:Damaged = 'YES'
  end
  
  !Access:JobAcc.ClearKey(jac:Ref_Number_Key)
  !jac:Ref_Number = tmp:Job_No
  !jac:Accessory  = acr:Accessory
  !If Access:JobAcc.TryFetch(jac:Ref_Number_Key) = Level:Benign
  !    !Found
  !    If jac:Damaged
  !        tmp:Damaged = 'YES'
  !    End !If jac:Damaged
  !Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
  !    !Error
  !    !Assert(0,'<13,10>Fetch Error<13,10>')
  !End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
  PARENT.SetQueueRecord
  IF (tmp:Present = 'Y')
    SELF.Q.tmp:Present_Icon = 1
  ELSE
    SELF.Q.tmp:Present_Icon = 0
  END
  IF (tmp:Damaged = 'YES')
    SELF.Q.tmp:Damaged_Icon = 2
  ELSE
    SELF.Q.tmp:Damaged_Icon = 0
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Q_Accessory.Accessory_Pointer = acr:Accessory
     GET(glo:Q_Accessory,glo:Q_Accessory.Accessory_Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW4.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet2) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:CurrentWayBill
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:CurrentWayBill
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW4.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  tmp:Exchanged = 0
  If wayjob:JobType = 'J'
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = wayjob:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If job:Exchange_Unit_Number <> 0
              tmp:Exchanged = 1
          End !If job:Exchange_Unit_Number <> 0
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  End !wayjob:JobType = 'JOB'
  PARENT.SetQueueRecord
  IF (tmp:Exchanged = 1)
    SELF.Q.tmp:Exchanged_Icon = 1
  ELSE
    SELF.Q.tmp:Exchanged_Icon = 0
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW18.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

WayBillEmailDefaults PROCEDURE                        !Generated from procedure template - Window

tmp:RecipientType    STRING(30)
tmp:From             STRING(60)
tmp:Subject          STRING(30)
tmp:Txt_Body         STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('E-Mail Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Email Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('E-Mail Details'),USE(?EMail_Tab)
                           PROMPT('Recipient Type'),AT(183,118),USE(?tmp:RecipientType:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(249,118,124,10),USE(tmp:RecipientType),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(377,116),USE(?Transit_Types_Lookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('From'),AT(183,134),USE(?tmp:From:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(249,134,124,10),USE(tmp:From),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Subject'),AT(183,150),USE(?tmp:Subject:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(249,150,124,10),USE(tmp:Subject),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Text Body'),AT(183,168),USE(?tmp:Txt_Body:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(249,168,247,135),USE(tmp:Txt_Body),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:RecipientType                Like(tmp:RecipientType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020521'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WayBillEmailDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:RECIPTYP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:RecipientType = Clip(GETINI('EMAIL','Recipient',,CLIP(PATH())&'\CELRAPWC.INI'))
  tmp:From = Clip(GETINI('EMAIL','From',,CLIP(PATH())&'\CELRAPWC.INI'))
  tmp:Subject = Clip(GETINI('EMAIL','Subject',,CLIP(PATH())&'\CELRAPWC.INI'))
  
  glo:file_name = CLIP(PATH()) & '\MAILTXT.DEF'
  Relate:ExpGen.Open()
  set(ExpGen,0)
  loop until Access:ExpGen.Next()
      tmp:Txt_Body = clip(tmp:Txt_Body) & clip(gen:Line1) & '<13,10>'
  end
  Relate:ExpGen.Close()
  ! Save Window Name
   AddToLog('Window','Open','WayBillEmailDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','WayBillEmailDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickRecipientTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      PUTINI('EMAIL','Recipient',tmp:RecipientType,CLIP(PATH()) & '\CELRAPWC.INI')
      PUTINI('EMAIL','From',tmp:From,CLIP(PATH()) & '\CELRAPWC.INI')
      PUTINI('EMAIL','Subject',tmp:Subject,CLIP(PATH()) & '\CELRAPWC.INI')
      
      glo:file_name = CLIP(PATH()) & '\MAILTXT.DEF'
      remove(glo:file_name)
      Relate:ExpGen.Open()
      set(ExpGen,0)
      gen:Line1 = tmp:Txt_Body
      Access:ExpGen.TryInsert()
      Relate:ExpGen.Close()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020521'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020521'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020521'&'0')
      ***
    OF ?tmp:RecipientType
      IF tmp:RecipientType OR ?tmp:RecipientType{Prop:Req}
        rec:RecipientType = tmp:RecipientType
        !Save Lookup Field Incase Of error
        look:tmp:RecipientType        = tmp:RecipientType
        IF Access:RECIPTYP.TryFetch(rec:RecipientTypeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:RecipientType = rec:RecipientType
          ELSE
            !Restore Lookup On Error
            tmp:RecipientType = look:tmp:RecipientType
            SELECT(?tmp:RecipientType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Transit_Types_Lookup
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickRecipientTypes
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
              tmp:RecipientType = rec:RecipientType
          of requestcancelled
              tmp:RecipientType = ''
      end
      
      display(?tmp:RecipientType)
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
TransitTypeWindow PROCEDURE                           !Generated from procedure template - Window

tmp:Initial_Transit_Type STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Initial Transit Type'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Initial Transit Type'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Transit Type'),USE(?Transit_Type_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(252,207,148,10),USE(tmp:Initial_Transit_Type),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(404,201),USE(?Transit_Types_Lookup),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020519'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TransitTypeWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:TRANTYPE.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:Initial_Transit_Type = Clip(GETINI('INITIAL','TransitType',,CLIP(PATH())&'\CELRAPWC.INI'))
  ! Save Window Name
   AddToLog('Window','Open','TransitTypeWindow')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRANTYPE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','TransitTypeWindow')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      PUTINI('INITIAL','TransitType',tmp:Initial_Transit_Type,CLIP(PATH()) & '\CELRAPWC.INI')
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020519'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020519'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020519'&'0')
      ***
    OF ?Transit_Types_Lookup
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickTransitTypes
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
              tmp:Initial_Transit_Type = trt:Transit_Type
          of requestcancelled
              tmp:Initial_Transit_Type = ''
      end
      
      display(?tmp:Initial_Transit_Type)
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BuildList            PROCEDURE                        ! Declare Procedure
save_job_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
TempFilePath         CSTRING(255)
  CODE
   Relate:USERS.Open
   Relate:JOBS.Open
   Relate:JOBSE.Open
   Relate:DEFAULTV.Open
   Relate:WEBJOB.Open
!    If GetTempPathA(255,TempFilePath)
!        If Sub(TempFilePath,-1,1) = '\'
!            glo:File_Name = Clip(TempFilePath) & 'WAYGEN' & Clock() & '.TMP'
!        Else !If Sub(TempFilePath,-1,1) = '\'
!            glo:File_Name = Clip(TempFilePath) & '\WAYGEN' & Clock() & '.TMP'
!        End !If Sub(TempFilePath,-1,1) = '\'
!    End
!
!    !Should be able to use the existing
!    !global address search for this file.
!    !Saves creating another temp file in the dictionary
!
!!    If Command('WebAcc')
!!        Clarionet:Global.Param2 = Command('WebAcc')
!!    End !If Command('WebJob')
!
!    Count# = 0
!    Save_job_ID = Access:JOBS.SaveFile()
!    Access:JOBS.ClearKey(job:InConsignKey)
!    job:Incoming_Consignment_Number = ''
!    Set(job:InConsignKey,job:InConsignKey)
!    Loop
!        If Access:JOBS.NEXT()
!           Break
!        End !If
!        If job:Incoming_Consignment_Number <> ''      |
!            Then Break.  ! End If
!        Count# += 1
!        If Count# > 1000
!            Count# = Records(JOBS)
!            Break
!        End !If Count# > 1000
!    End !Loop
!
!    Access:JOBS.RestoreFile(Save_job_ID)
!
!    Access:ADDSEARCH.Open()
!    Access:ADDSEARCH.UseFile()
!
!    Prog.ProgressSetup(Count#)
!    Prog.ProgressText('Searching...')
!    !May be quicker to use the "Location Key",
!    !but it will depend how often the Inconsignment number is blank
!
!    Save_job_ID = Access:JOBS.SaveFile()
!    Access:JOBS.ClearKey(job:InConsignKey)
!    job:Incoming_Consignment_Number = ''
!    Set(job:InConsignKey,job:InConsignKey)
!    Loop
!        If Access:JOBS.NEXT()
!           Break
!        End !If
!        If job:Incoming_Consignment_Number <> ''      |
!            Then Break.  ! End If
!
!        If Prog.InsideLoop()
!            Break
!        End !If Prog.InsideLoop()
!
!        !Make sure that Generic jobs are picked up
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)
!        wob:RefNumber = job:Ref_Number
!        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!            !Found
!            If wob:HeadAccountNumber <> Clarionet:Global.Param2
!                Cycle
!            End !If wob:HeadAccountNumber <> Clarionet:Global.Param2
!        Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!
!        If job:Location <> GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
!            Cycle
!        End !If job:Location <> 'REMOTE REPAIR CENTRE'
!
!        Access:JOBSE.Clearkey(jobe:RefNumberKey)
!        jobe:RefNumber  = job:Ref_Number
!        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!            !Found
!            If ~jobe:HubRepair
!                Cycle
!            End !If ~jobe:HubRepair
!        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!            !Error
!        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!        If Access:ADDSEARCH.PrimeRecord() = Level:Benign
!            addtmp:JobNumber    = job:Ref_Number
!            addtmp:AddressLine1 = job:Manufacturer
!            addtmp:Postcode     = job:Model_Number
!            !addtmp:IncomingIMEI = job:Date_Booked
!            addtmp:IncomingIMEI = job:ESN
!            addtmp:ExchangedIMEI    = job:Current_Status
!            If Access:ADDSEARCH.TryInsert() = Level:Benign
!                !Insert Successful
!            Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
!                !Insert Failed
!            End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
!        End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
!
!    End !Loop
!    Access:JOBS.RestoreFile(Save_job_ID)
!
!    Prog.ProgressFinish()
!    Access:ADDSEARCH.Close()
!
    MainBrowse()
   Relate:USERS.Close
   Relate:JOBS.Close
   Relate:JOBSE.Close
   Relate:DEFAULTV.Close
   Relate:WEBJOB.Close
CreateSundryWaybill PROCEDURE                         !Generated from procedure template - Window

tmp:Destination      STRING(30)
tmp:ModeOfTransport  STRING(30)
tmp:SecurityPackNumber STRING(30)
tmp:UserNotes        STRING(255)
tmp:WaybillNumber    LONG
save_waysund_temp_id USHORT,AUTO
OtherAddressGroup    GROUP,PRE()
tmp:OtherAccountNumber STRING(30)
tmp:OtherCompanyName STRING(30)
tmp:OtherAddress1    STRING(30)
tmp:OtherAddress2    STRING(30)
tmp:OtherAddress3    STRING(30)
tmp:OtherPostcode    STRING(30)
tmp:OtherTelephoneNo STRING(30)
tmp:OtherContactName STRING(60)
tmp:OtherEmailAddress STRING(255)
tmp:OtherHub         STRING(30)
                     END
tmp:SendToOtherAddress BYTE(0)
BRW9::View:Browse    VIEW(WAYSUND_TEMP)
                       PROJECT(wastmp:Description)
                       PROJECT(wastmp:Quantity)
                       PROJECT(wastmp:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
wastmp:Description     LIKE(wastmp:Description)       !List box control field - type derived from field
wastmp:Quantity        LIKE(wastmp:Quantity)          !List box control field - type derived from field
wastmp:RecordNumber    LIKE(wastmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       GROUP('Destination Address Details'),AT(344,58,268,148),USE(?Group:DestinationAddress),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Account Number'),AT(352,68),USE(?tmp:OtherAccountNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,68,124,10),USE(tmp:OtherAccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Account Number'),TIP('Account Number'),UPR,SINGLE
                         PROMPT('Company Name'),AT(352,82),USE(?tmp:OtherCompanyName:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,82,124,10),USE(tmp:OtherCompanyName),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Company Name'),TIP('Company Name'),REQ,UPR,SINGLE
                         PROMPT('Address'),AT(352,95),USE(?tmp:OtherAddress1:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,95,124,10),USE(tmp:OtherAddress1),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Address 1'),TIP('Address 1'),REQ,UPR,SINGLE
                         TEXT,AT(448,108,124,10),USE(tmp:OtherAddress2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Address 2'),TIP('Address 2'),UPR,SINGLE
                         PROMPT('Suburb'),AT(352,119),USE(?tmp:OtherAddress1:Prompt:2),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,119,124,10),USE(tmp:OtherAddress3),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Address 3'),TIP('Address 3'),UPR,SINGLE
                         BUTTON,AT(576,116),USE(?Button:LookupSuburb),TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('Postcode'),AT(352,132),USE(?tmp:OtherPostcode:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,132,64,10),USE(tmp:OtherPostcode),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Postcode'),TIP('Postcode'),UPR,SINGLE,READONLY
                         PROMPT('Hub'),AT(352,143),USE(?tmp:OtherHub:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,143,64,10),USE(tmp:OtherHub),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Hub'),TIP('Hub'),REQ,UPR,SINGLE,READONLY
                         PROMPT('Telephone Number'),AT(352,158),USE(?tmp:OtherTelephoneNo:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,158,124,10),USE(tmp:OtherTelephoneNo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Telephone Number'),TIP('Telephone Number'),REQ,UPR,SINGLE
                         PROMPT('Contact Name'),AT(352,172),USE(?tmp:OtherContactName:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,172,124,10),USE(tmp:OtherContactName),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Contact Name'),TIP('Contact Name'),REQ,UPR,SINGLE
                         PROMPT('Email Address'),AT(352,186),USE(?tmp:OtherEmailAddress:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(448,186,124,10),USE(tmp:OtherEmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Email Address'),TIP('Email Address'),SINGLE
                       END
                       PANEL,AT(64,54,552,310),USE(?Panel5),FILL(09A6A7CH)
                       GROUP('Waybill Details'),AT(68,58,272,148),USE(?Group2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         CHECK('Send To Other Address'),AT(180,68),USE(tmp:SendToOtherAddress),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Send To Other Address'),TIP('Send To Other Address'),VALUE('1','0')
                         PROMPT('Destination'),AT(72,86),USE(?tmp:Destination:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(180,86,124,10),USE(tmp:Destination),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Destination'),TIP('Destination'),REQ,UPR
                         BUTTON,AT(308,82),USE(?Lookup:Destination),TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('Mode Of Transport'),AT(72,110),USE(?tmp:ModeOfTransport:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(180,110,124,10),USE(tmp:ModeOfTransport),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Mode Of Transport'),TIP('Mode Of Transport'),REQ,UPR
                         BUTTON,AT(308,106),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('Security Pack Number'),AT(72,132),USE(?tmp:SecurityPackNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(180,132,124,10),USE(tmp:SecurityPackNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Security Pack Number'),TIP('Security Pack Number'),REQ,UPR
                       END
                       GROUP('User Notes'),AT(344,254,268,108),USE(?Group4),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(352,264,252,92),USE(tmp:UserNotes),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       END
                       GROUP('Waybill Items'),AT(68,256,272,106),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         LIST,AT(72,270,192,88),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('114L(2)|M~Description~@s30@32L(2)|M~Quantity~@s30@'),FROM(Queue:Browse)
                         BUTTON,AT(268,270),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                         BUTTON,AT(268,300),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                         BUTTON,AT(268,332),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Create Sundry Waybill'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(480,366),USE(?Button:GenerateWayBill),TRN,FLAT,ICON('genwayp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
tmp:TempFilePath    CString(255)
!Save Entry Fields Incase Of Lookup
look:tmp:Destination                Like(tmp:Destination)
look:tmp:ModeOfTransport                Like(tmp:ModeOfTransport)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020661'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CreateSundryWaybill')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  glo:FileName = 'SUNWAY' & Clock() & '.TMP'
  If GetTempPathA(255,tmp:TempFilePath)
      If Sub(tmp:TempFilePath,-1,1) = '\'
          glo:FileName = Clip(tmp:TempFilePath) & Clip(glo:FileName)
      Else !If Sub(TempFilePath,-1,1) = '\'
          glo:FileName = Clip(tmp:TempFilePath) & '\' & Clip(glo:FileName)
      End !If Sub(tmp:TempFilePath,-1,1) = '\'
  End
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COURIER.Open
  Relate:WAYAUDIT.Open
  Relate:WAYBCONF.Open
  Relate:WAYSUND_TEMP.Open
  Access:TRADEACC.UseFile
  Access:WAYBILLS.UseFile
  Access:WAYSUND.UseFile
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:WAYSUND_TEMP,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','CreateSundryWaybill')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Destination{Prop:Tip} AND ~?Lookup:Destination{Prop:Tip}
     ?Lookup:Destination{Prop:Tip} = 'Select ' & ?tmp:Destination{Prop:Tip}
  END
  IF ?tmp:Destination{Prop:Msg} AND ~?Lookup:Destination{Prop:Msg}
     ?Lookup:Destination{Prop:Msg} = 'Select ' & ?tmp:Destination{Prop:Msg}
  END
  IF ?tmp:ModeOfTransport{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:ModeOfTransport{Prop:Tip}
  END
  IF ?tmp:ModeOfTransport{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:ModeOfTransport{Prop:Msg}
  END
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,wastmp:RecordNumberKey)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,wastmp:RecordNumber,1,BRW9)
  BRW9.AddField(wastmp:Description,BRW9.Q.wastmp:Description)
  BRW9.AddField(wastmp:Quantity,BRW9.Q.wastmp:Quantity)
  BRW9.AddField(wastmp:RecordNumber,BRW9.Q.wastmp:RecordNumber)
  IF ?tmp:SendToOtherAddress{Prop:Checked} = True
    tmp:Destination = 'OTHER'
    OtherAddressGroup = ''
    DISABLE(?tmp:Destination)
    DISABLE(?Lookup:Destination)
    ENABLE(?Group:DestinationAddress)
  END
  IF ?tmp:SendToOtherAddress{Prop:Checked} = False
    tmp:Destination = ''
    OtherAddressGroup = ''
    DISABLE(?Group:DestinationAddress)
    ENABLE(?tmp:Destination)
    ENABLE(?Lookup:Destination)
  END
  BRW9.AskProcedure = 3
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:WAYAUDIT.Close
    Relate:WAYBCONF.Close
    Relate:WAYSUND_TEMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CreateSundryWaybill')
  GlobalErrors.SetProcedureName
  Remove(Glo:FileName)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickRRCAccounts
      PickCouriers
      UpdateSundryWaybillItem
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:OtherAddress3
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = tmp:OtherAddress3
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              tmp:OtherPostcode = sur:Postcode
              tmp:OtherHub = sur:Hub
              ?tmp:OtherPostcode{prop:Skip} = True
              ?tmp:OtherPostcode{prop:ReadOnly} = True
              ?tmp:OtherHub{prop:Skip} = True
              ?tmp:OtherHub{prop:ReadOnly} = True
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Case Missive('Cannot find the select Suburb. '&|
                '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the postcode?','ServiceBase 3g',|
                             'mexclam.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      tmp:OtherAddress3 = ''
                      tmp:OtherPostcode = ''
                      tmp:OtherHub = ''
                      ?tmp:OtherPostcode{prop:Skip} = True
                      ?tmp:OtherPostcode{prop:ReadOnly} = True
                      ?tmp:OtherHub{prop:Skip} = True
                      ?tmp:OtherHub{prop:ReadOnly} = True
                      Bryan.CompFieldColour()
                      Display()
                      Select(?tmp:OtherAddress3)
                  Of 2 ! Pick Button
                      tmp:OtherAddress3 = ''
                      tmp:OtherPostcode = ''
                      tmp:OtherHub = ''
                      Post(Event:Accepted,?Button:LookupSuburb)
                  Of 1 ! Continue Button
                      ?tmp:OtherPostcode{prop:Skip} = False
                      ?tmp:OtherPostcode{prop:ReadOnly} = False
                      ?tmp:OtherHub{prop:Skip} = False
                      ?tmp:OtherHub{prop:ReadOnly} = False
                      Bryan.CompFieldColour()
                      Select(?tmp:OtherPostcode)
              End ! Case Missive
          End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
    OF ?Button:LookupSuburb
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tmp:OtherAddress3 = sur:Suburb
              tmp:OtherPostcode = sur:Postcode
              tmp:OtherHub = sur:Hub
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      ?tmp:OtherPostcode{prop:Skip} = True
      ?tmp:OtherPostcode{prop:ReadOnly} = True
      ?tmp:OtherHub{prop:Skip} = True
      ?tmp:OtherHub{prop:ReadOnly} = True
      Bryan.CompFieldColour()
      Display()
    OF ?tmp:SendToOtherAddress
      IF ?tmp:SendToOtherAddress{Prop:Checked} = True
        tmp:Destination = 'OTHER'
        OtherAddressGroup = ''
        DISABLE(?tmp:Destination)
        DISABLE(?Lookup:Destination)
        ENABLE(?Group:DestinationAddress)
      END
      IF ?tmp:SendToOtherAddress{Prop:Checked} = False
        tmp:Destination = ''
        OtherAddressGroup = ''
        DISABLE(?Group:DestinationAddress)
        ENABLE(?tmp:Destination)
        ENABLE(?Lookup:Destination)
      END
      ThisWindow.Reset
    OF ?tmp:Destination
      IF tmp:Destination OR ?tmp:Destination{Prop:Req}
        tra:Account_Number = tmp:Destination
        !Save Lookup Field Incase Of error
        look:tmp:Destination        = tmp:Destination
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Destination = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:Destination = look:tmp:Destination
            SELECT(?tmp:Destination)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = tmp:Destination
      If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:OtherAccountNumber  = tra:Account_Number
          tmp:OtherCompanyName    = tra:Company_Name
          tmp:OtherAddress1       = tra:Address_Line1
          tmp:OtherAddress2       = tra:Address_Line2
          tmp:OtherAddress3       = tra:Address_Line3
          tmp:OtherPostcode       = tra:Postcode
          tmp:OtherContactName    = tra:Contact_Name
          tmp:OtherEmailAddress   = tra:EmailAddress
          tmp:OtherHub            = tra:Hub
          Display()
      Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    OF ?Lookup:Destination
      ThisWindow.Update
      tra:Account_Number = tmp:Destination
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Destination = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:Destination)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Destination)
    OF ?tmp:ModeOfTransport
      IF tmp:ModeOfTransport OR ?tmp:ModeOfTransport{Prop:Req}
        cou:Courier = tmp:ModeOfTransport
        !Save Lookup Field Incase Of error
        look:tmp:ModeOfTransport        = tmp:ModeOfTransport
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:ModeOfTransport = cou:Courier
          ELSE
            !Restore Lookup On Error
            tmp:ModeOfTransport = look:tmp:ModeOfTransport
            SELECT(?tmp:ModeOfTransport)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      cou:Courier = tmp:ModeOfTransport
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:ModeOfTransport = cou:Courier
          Select(?+1)
      ELSE
          Select(?tmp:ModeOfTransport)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ModeOfTransport)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020661'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020661'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020661'&'0')
      ***
    OF ?Button:GenerateWayBill
      ThisWindow.Update
    If tmp:Destination = ''
        Case Missive('You must select a destination.', 'ServiceBase 3g', |
                     'mstop.jpg', '/OK')
        Of 1 ! OK Button
        End ! Case Missive
        Select(?tmp:Destination)
        Cycle
    End ! If tmp:Destination = ''
    If Clip(tmp:ModeOfTransport) = ''
        Case Missive('You must select a Mode Of Transport.', 'ServiceBase 3g', |
                     'mstop.jpg', '/OK')
        Of 1 ! OK Button
        End ! Case Missive
        Select(?tmp:ModeOfTransport)
        Cycle
    End ! If Clip(tmp:ModeOfTransport) = ''
    If Clip(tmp:SecurityPackNumber) = ''
        Case Missive('You must select a Security Pack Number.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Select(?tmp:SecurityPackNumber)
        Cycle
    End ! If Clip(tmp:SecurityPackNumber) = ''

    If tmp:SendToOtherAddress
        If CLip(tmp:OtherCompanyName) = ''
            Case Missive('You must enter a Company Name.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Select(?tmp:OtherCompanyName)
            Cycle
        End ! If CLip(tmp:OtherCompanyName) = ''
        If Clip(tmp:OtherAddress1) = ''
            Case Missive('You must enter an Address.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Select(?tmp:OtherAddress1)
            Cycle
        End ! If Clip(tmp:OtherAddress1) = ''
        If Clip(tmp:OtherContactName) = ''
            Case Missive('You must enter a Contact Name.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Select(?tmp:OtherContactName)
            Cycle
        End ! If Clip(tmp:OtherContactName) = ''
        If Clip(tmp:OtherTelephoneNo) = ''
            Case Missive('You must enter a Telephone Number.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Select(?tmp:OtherTelephoneNo)
            CYcle
        End ! If Clip(tmp:OtherTelephoneNo) = ''
        If Clip(tmp:OtherHub) = ''
            Beep(Beep:SystemHand);  Yield()
            Case Missive('You must enter a Hub.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Select(?tmp:OtherHub)
            Cycle
        End ! If Clip(tmp:Hub) = ''
    End ! If tmp:SendToOtherAddress

    !TB13370 - J - 21/04/15 - includes request to ensure there are some items on the waybill
    !so this bit of code is not working.
    !Hmm... when I compiled it I get the error with the full stop at the end, so this is working!
    If Records(Queue:Browse) = 0
!        Case Missive('You have not attached any items to the waybill.' &|
!                     '|Are you sure you want to create a waybill with no items attached?', 'ServiceBase 3g', |
!                     'mquest.jpg', '\No|/Yes')
!        Of 2 ! Yes Button
!        Of 1 ! No Button
!            Cycle
!        End ! Case Missive
        !TB13370 - change the error message and don't give them an option to send an empty waybill
        Miss# = Missive('No Waybill Items have been entered.','ServiceBase 3g','Mstop.jpg','OK')
        cycle

    End ! If Records(Queue:Browse) = 0

    !repeating check for items on waybill
    If Records(WaySund_Temp) = 0
        Miss# = Missive('No Waybill Items have been entered','ServiceBase 3g','Mstop.jpg','OK')
        cycle
    END

    Case Missive('Are you sure you want to create a waybill for ' & Clip(tmp:OtherCompanyName) & '?', 'ServiceBase 3g', |
                 'mquest.jpg', '\No|/Yes')
    Of 2 ! Yes Button

        tmp:WaybillNumber = NextWayBillNumber()
        If tmp:WayBillNumber > 0
            Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
            way:WaybillNumber = tmp:WayBillNumber
            If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
                ! Found
                If glo:WebJob
    !                way:WayBillType = 0 ! From RRC
                    way:FromAccount        =  Clarionet:Global.Param2
                Else ! If glo:WebJob
    !                way:WaybillType = 1 ! From ARC

! Changing (DBH 23/04/2007) # 8939 - Use default head account
!                      way:FromAccount        = 'AA20'
! to (DBH 23/04/2007) # 8939
                    way:FromAccount = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
! End (DBH 23/04/2007) #8939
                End ! If glo:WebJob
                ! Inserting (DBH 16/10/2006) # 7995 - Waybilltype determines where the item is going
                If tmp:Destination = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
                    way:WayBillType = 0
                Else ! If tmp:Destination = 'AA20'
                    way:WayBillType = 1
                End ! If tmp:Destination = 'AA20'
                ! End (DBH 16/10/2006) #7995
                way:AccountNumber      = tmp:Destination
                way:WayBillID          = 300

                way:ToAccount          = tmp:Destination
                way:SecurityPackNumber = tmp:SecurityPackNumber
                way:UserNotes          = Upper(tmp:UserNotes)
                way:Courier            = tmp:ModeOfTransport
                way:TheDate            = Today()
                way:TheTime            = CLock()
                If tmp:SendToOtherAddress
                    way:WayBillType        = 2
                    ! Item is being sent to a customer, will not appear in any confirmation screens (DBH: 16/10/2006)
                    way:OtherCompanyName   = tmp:OtherCompanyName
                    way:OtherAccountNumber = tmp:OtherAccountNumber
                    way:OtherAddress1      = tmp:OtherAddress1
                    way:OtherAddress2      = tmp:OtherAddress2
                    way:OtherAddress3      = tmp:OtherAddress3
                    way:OtherPostcode      = tmp:OtherPostcode
                    way:OtherContactName   = tmp:OtherContactName
                    way:OtherTelephoneNO   = tmp:OtherTelephoneNo
                    way:OtherEmailAddress  = tmp:OtherEmailAddress
                    ! Inserting (DBH 30/01/2008) # 9613 - Add Hub
                    way:OtherHub           = tmp:OtherHub
                    ! End (DBH 30/01/2008) #9613
                End ! If tmp:SendToOtherAddress
                If Access:WAYBILLS.Update() = Level:Benign
                    Save_WAYSUND_TEMP_ID = Access:WAYSUND_TEMP.SaveFile()
                    Access:WAYSUND_TEMP.Clearkey(wastmp:RecordNumberKey)
                    wastmp:RecordNumber = 1
                    Set(wastmp:RecordNumberKey, wastmp:RecordNumberKey)
                    Loop ! Begin Loop
                        If Access:WAYSUND_TEMP.Next()
                            Break
                        End ! If Access:WAYSUND_TEMP.Next()
                        If Access:WAYSUND.PrimeRecord() = Level:Benign
                            was:WAYBILLSRecordNumber = way:RecordNumber
                            was:Description          = wastmp:Description
                            was:Quantity             = wastmp:Quantity
                            If Access:WAYSUND.TryInsert() = Level:Benign
                            ! Insert
                            Else ! If Access:WAYSUND.TryInsert() = Level:Benign
                                Access:WAYSUND.CancelAutoInc()
                            End ! If Access:WAYSUND.TryInsert() = Level:Benign
                        End ! If Access.WAYSUND.PrimeRecord() = Level:Benign
                    End ! Loop
                    Access:WAYSUND_TEMP.RestoreFile(Save_WAYSUND_TEMP_ID)

                    If Access:WAYAUDIT.PrimeRecord() = Level:Benign
                        waa:WAYBILLSRecordNumber = way:RecordNumber
                        Access:USERS.ClearKey(use:Password_Key)
                        use:Password = glo:Password
                        If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                        ! Found
                        Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                        ! Error
                        End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                        waa:UserCode = use:User_Code
                        waa:Action   = 'WAYBILL CREATED'
                        waa:TheDate  = Today()
                        waa:TheTime  = Clock()
                        If Access:WAYAUDIT.TryInsert() = Level:Benign
                                ! Insert
                                ! Waybill created (DBH: 04/09/2006)
                            WayBillDespatch(way:FromAccount, 'TRA', way:ToAccount, 'TRA', way:WaybillNumber, way:Courier)
                            Post(Event:CloseWindow)
                        Else ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                            Access:WAYAUDIT.CancelAutoInc()
                        End ! If Access:WAYAUDIT.TryInsert() = Level:Benign
                    End ! If Access.WAYAUDIT.PrimeRecord() = Level:Benign
                    !TB13370- Waybconf record would be made in NextWayBillNumber but things have changed
                    Access:Waybconf.clearkey(WAC:KeyWaybillNo)
                    WAC:WaybillNo = way:WayBillNumber
                    If access:Waybconf.fetch(WAC:KeyWaybillNo)
                        Access:WAYBCONF.primerecord()
                        WAC:WaybillNo        = way:WayBillNumber
                        WAC:GenerateDate     = way:TheDate            !yes this is a duplicate, but it is needed for a key
                        WAC:GenerateTime     = way:TheTime            !yes this is a duplicate, but it is needed for a key
                        WAC:ConfirmationSent = false
                        WAC:ConfirmResentQty = 0
                    END
                    WAC:AccountNumber    = way:AccountNumber
                    Access:WAYBCONF.update()

                    !End TB13370 - J - 21/04/15
                End ! If Access:WAYBILLS.Update() = Level:Benign
            Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
            ! Error
            End ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
        End ! If tmp:WayBillNumber > 0
    Of 1 ! No Button
    End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateSundryWaybillItem PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::wastmp:Record LIKE(wastmp:RECORD),STATIC
QuickWindow          WINDOW('Update the WAYSUND_TEMP File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Description'),AT(248,198),USE(?wastmp:Description:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(304,198,124,10),USE(wastmp:Description),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Description'),TIP('Description'),REQ,UPR
                       PROMPT('Quantity'),AT(248,216),USE(?wastmp:Quantity:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(304,216,124,10),USE(wastmp:Quantity),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Quantity'),TIP('Quantity'),UPR
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Sundry Item'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(292,258),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020662'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSundryWaybillItem')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(wastmp:Record,History::wastmp:Record)
  SELF.AddHistoryField(?wastmp:Description,2)
  SELF.AddHistoryField(?wastmp:Quantity,3)
  SELF.AddUpdateFile(Access:WAYSUND_TEMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:WAYSUND_TEMP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WAYSUND_TEMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateSundryWaybillItem')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:WAYSUND_TEMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSundryWaybillItem')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020662'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020662'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020662'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults




MainBrowse PROCEDURE                                  !Generated from procedure template - Window

local                CLASS
ValidateAccessories  Procedure(),Byte
                     END
save_jac_id          USHORT,AUTO
save_way_id          USHORT,AUTO
tmp:tag              STRING(1)
tmp:WayBillNumber    STRING(30)
tmp:Job_No           LONG
tmp:Model_No         STRING(30)
tmp:IMEI_No          STRING(20)
tmp:Scanned_Job_No   LONG
tmp:Scanned_IMEI_No  STRING(20)
Processed_Q          QUEUE,PRE()
PQ_Job_No            LONG
PQ_Model_No          STRING(30)
PQ_IMEI_No           STRING(20)
que:SecurityPackNumber STRING(30)
                     END
tmp:Accessory_Valid_Flag BYTE(0)
tmp:SecurityPackNumber STRING(30)
Comments             STRING(255)
LocalCourier         STRING(30)
LocalOverWrite       STRING(1)
tmp:AccountNoFilter  STRING(30)
tmp:Exchanged1       BYTE(0)
tmp:Blank            STRING(30)
tmp:Exchanged2       BYTE(0)
tmp:Blank2           STRING(30)
tmp:PrintWaybill     BYTE(0)
NR48eCounter         BYTE
OBFCounter           BYTE
AWType               STRING(3)
PType                STRING(3)
WaybillType          STRING(3)
ProcessAuto          STRING(1)
locAuditNotes        STRING(255)
locAction            STRING(80)
BRW2::View:Browse    VIEW(WAYBAWT)
                       PROJECT(wya:JobNumber)
                       PROJECT(wya:ModelNumber)
                       PROJECT(wya:IMEINumber)
                       PROJECT(wya:WAYBAWTID)
                       PROJECT(wya:AccountNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List
wya:JobNumber          LIKE(wya:JobNumber)            !List box control field - type derived from field
wya:JobNumber_NormalFG LONG                           !Normal forground color
wya:JobNumber_NormalBG LONG                           !Normal background color
wya:JobNumber_SelectedFG LONG                         !Selected forground color
wya:JobNumber_SelectedBG LONG                         !Selected background color
wya:ModelNumber        LIKE(wya:ModelNumber)          !List box control field - type derived from field
wya:ModelNumber_NormalFG LONG                         !Normal forground color
wya:ModelNumber_NormalBG LONG                         !Normal background color
wya:ModelNumber_SelectedFG LONG                       !Selected forground color
wya:ModelNumber_SelectedBG LONG                       !Selected background color
wya:IMEINumber         LIKE(wya:IMEINumber)           !List box control field - type derived from field
wya:IMEINumber_NormalFG LONG                          !Normal forground color
wya:IMEINumber_NormalBG LONG                          !Normal background color
wya:IMEINumber_SelectedFG LONG                        !Selected forground color
wya:IMEINumber_SelectedBG LONG                        !Selected background color
AWType                 LIKE(AWType)                   !List box control field - type derived from local data
AWType_NormalFG        LONG                           !Normal forground color
AWType_NormalBG        LONG                           !Normal background color
AWType_SelectedFG      LONG                           !Selected forground color
AWType_SelectedBG      LONG                           !Selected background color
tmp:Exchanged1         LIKE(tmp:Exchanged1)           !List box control field - type derived from local data
tmp:Exchanged1_Icon    LONG                           !Entry's icon ID
tmp:Blank              LIKE(tmp:Blank)                !List box control field - type derived from local data
wya:WAYBAWTID          LIKE(wya:WAYBAWTID)            !Primary key field - type derived from field
wya:AccountNumber      LIKE(wya:AccountNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(WAYBPRO)
                       PROJECT(wyp:JobNumber)
                       PROJECT(wyp:ModelNumber)
                       PROJECT(wyp:IMEINumber)
                       PROJECT(wyp:WAYBPROID)
                       PROJECT(wyp:AccountNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wyp:JobNumber          LIKE(wyp:JobNumber)            !List box control field - type derived from field
wyp:JobNumber_NormalFG LONG                           !Normal forground color
wyp:JobNumber_NormalBG LONG                           !Normal background color
wyp:JobNumber_SelectedFG LONG                         !Selected forground color
wyp:JobNumber_SelectedBG LONG                         !Selected background color
wyp:ModelNumber        LIKE(wyp:ModelNumber)          !List box control field - type derived from field
wyp:ModelNumber_NormalFG LONG                         !Normal forground color
wyp:ModelNumber_NormalBG LONG                         !Normal background color
wyp:ModelNumber_SelectedFG LONG                       !Selected forground color
wyp:ModelNumber_SelectedBG LONG                       !Selected background color
wyp:IMEINumber         LIKE(wyp:IMEINumber)           !List box control field - type derived from field
wyp:IMEINumber_NormalFG LONG                          !Normal forground color
wyp:IMEINumber_NormalBG LONG                          !Normal background color
wyp:IMEINumber_SelectedFG LONG                        !Selected forground color
wyp:IMEINumber_SelectedBG LONG                        !Selected background color
PType                  LIKE(PType)                    !List box control field - type derived from local data
PType_NormalFG         LONG                           !Normal forground color
PType_NormalBG         LONG                           !Normal background color
PType_SelectedFG       LONG                           !Selected forground color
PType_SelectedBG       LONG                           !Selected background color
tmp:Exchanged2         LIKE(tmp:Exchanged2)           !List box control field - type derived from local data
tmp:Exchanged2_Icon    LONG                           !Entry's icon ID
tmp:Blank2             LIKE(tmp:Blank2)               !List box control field - type derived from local data
wyp:WAYBPROID          LIKE(wyp:WAYBPROID)            !Primary key field - type derived from field
wyp:AccountNumber      LIKE(wyp:AccountNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask2          DECIMAL(10,0,0)                  !Xplore
XploreMask12          DECIMAL(10,0,48)                !Xplore
XploreTitle2         STRING(' ')                      !Xplore
xpInitialTab2        SHORT                            !Xplore
window               WINDOW('Waybill Generation'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Waybill Generation'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Waybill Generation'),USE(?Tab1)
                           STRING('Please Process Up To 10 Jobs Per Waybill'),AT(64,78,552,10),USE(?String4),CENTER,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI)
                           STRING('Awaiting Processing'),AT(76,96),USE(?String1),TRN,FONT('Tahoma',12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Processed'),AT(380,96),USE(?String2),TRN,FONT('Tahoma',12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(74,110,240,148),USE(?List),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('36L(2)|M*~Job No~@s8@80L(2)|M*~Model No~@s30@80L(2)|M*~IMEI No~@s20@25L(2)|*~Typ' &|
   'e~@s3@0L(2)I~E~@n1@0L(2)@s30@E(0FFFFFFH,0FFFFFFH,0FFFFFFH,0FFFFFFH)'),FROM(Queue:Browse:2)
                           LIST,AT(380,110,226,176),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('36L(2)|M*~Job No~@s8@68L(2)|M*~Model No~@s30@80L(2)|M*~IMEI No~@s20@12L(2)|M*~Ty' &|
   'pe~@s3@0L(2)I~E~@n1@0L(2)@s30@E(0FFFFFFH,0FFFFFFH,0FFFFFFH,0FFFFFFH)'),FROM(Queue:Browse:1)
                           STRING('Processed'),AT(319,140,56,10),USE(?String5),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING('NR / 48E'),AT(319,161,56,10),USE(?String6),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING(@n02),AT(319,172,56,10),USE(NR48eCounter),CENTER,FONT(,,COLOR:White,,CHARSET:ANSI)
                           STRING('OBF'),AT(319,201,56,10),USE(?String7),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING(@n02),AT(319,214,56,10),USE(OBFCounter),CENTER,FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(76,262),USE(?tmp:Scanned_Job_No:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(192,262,124,10),USE(tmp:Scanned_Job_No),LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('I.M.E.I. Number'),AT(76,278),USE(?tmp:Scanned_IMEI_No:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(192,278,124,10),USE(tmp:Scanned_IMEI_No),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Security Pack Number'),AT(76,296),USE(?tmp:SecurityPackNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(192,294,124,10),USE(tmp:SecurityPackNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Security Pack Number'),TIP('Security Pack Number'),UPR
                           BUTTON,AT(251,308),USE(?ProcessJob),TRN,FLAT,LEFT,ICON('projobp.jpg')
                           BUTTON,AT(74,308),USE(?RemoveAwaitingJob),TRN,FLAT,LEFT,ICON('remjobp.jpg')
                           BUTTON,AT(380,308),USE(?RemoveProcessedJob),TRN,FLAT,LEFT,ICON('remjobp.jpg')
                           BUTTON,AT(460,308),USE(?GenerateOBFWaybill),TRN,FLAT,ICON('GOBFWBP.jpg')
                           BUTTON,AT(544,308),USE(?Generate48Waybill),TRN,FLAT,LEFT,ICON('GNR48WBP.jpg')
                           PROMPT('Comments'),AT(380,292),USE(?Comments:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(482,292,124,10),USE(Comments),LEFT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Comments'),TIP('Comments'),UPR
                         END
                       END
                       BUTTON,AT(480,366),USE(?ReprintWaybillButton),TRN,FLAT,LEFT,ICON('prnwayp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:2                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore2              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore2Step1         StepLongClass   !LONG            !Xplore: Column displaying wya:JobNumber
Xplore2Locator1      StepLocatorClass                 !Xplore: Column displaying wya:JobNumber
Xplore2Step2         StepStringClass !STRING          !Xplore: Column displaying wya:ModelNumber
Xplore2Locator2      StepLocatorClass                 !Xplore: Column displaying wya:ModelNumber
Xplore2Step3         StepStringClass !STRING          !Xplore: Column displaying wya:IMEINumber
Xplore2Locator3      StepLocatorClass                 !Xplore: Column displaying wya:IMEINumber
Xplore2Step4         StepCustomClass !                !Xplore: Column displaying AWType
Xplore2Locator4      StepLocatorClass                 !Xplore: Column displaying AWType
Xplore2Step5         StepCustomClass !                !Xplore: Column displaying tmp:Exchanged1
Xplore2Locator5      StepLocatorClass                 !Xplore: Column displaying tmp:Exchanged1
Xplore2Step6         StepCustomClass !                !Xplore: Column displaying tmp:Blank
Xplore2Locator6      StepLocatorClass                 !Xplore: Column displaying tmp:Blank
Xplore2Step7         StepLongClass   !LONG            !Xplore: Column displaying wya:WAYBAWTID
Xplore2Locator7      StepLocatorClass                 !Xplore: Column displaying wya:WAYBAWTID
Xplore2Step8         StepStringClass !STRING          !Xplore: Column displaying wya:AccountNumber
Xplore2Locator8      StepLocatorClass                 !Xplore: Column displaying wya:AccountNumber

  CODE
  GlobalResponse = ThisWindow.Run()

ValidateAccessories Routine
    data
local:ErrorCode                   Byte
Save_jac_ID                       UShort
Save_lac_ID                       UShort
    code

    tmp:Accessory_Valid_Flag = 0

    If job:Exchange_Unit_Number <> 0
        Exit
    End !If job:Exchange_Unit_Number <> 0

    local:ErrorCode  = 0
    If AccessoryCheck('JOB')
        If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 1
        Else!If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 2
        End !If AccessoryMismatch(job:Ref_Number,'JOB')
    End !If AccessoryCheck('JOB')


        Case local:Errorcode
            Of 0 Orof 2
                locAuditNotes         = 'ACCESSORIES:<13,10>'
            Of 1
                locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
        End !Case local:Errorcode

        !Add the list of accessories to the Audit Trail
        If job:Despatch_Type <> 'LOAN'
            Save_jac_ID = Access:JOBACC.SaveFile()
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
            Loop
                If Access:JOBACC.NEXT()
                   Break
                End !If
                If jac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
            End !Loop
            Access:JOBACC.RestoreFile(Save_jac_ID)
        Else !If job:Despatch_Type <> 'LOAN'
            Save_lac_ID = Access:LOANACC.SaveFile()
            Access:LOANACC.ClearKey(lac:Ref_Number_Key)
            lac:Ref_Number = job:Ref_Number
            Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
            Loop
                If Access:LOANACC.NEXT()
                   Break
                End !If
                If lac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
            End !Loop
            Access:LOANACC.RestoreFile(Save_lac_ID)
        End !If job:Despatch_Type <> 'LOAN'

        Case local:Errorcode
            Of 1
                !If Error show the Accessories that were actually tagged
                locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
                Clear(glo:Queue)
                Loop x# = 1 To Records(glo:Queue)
                    Get(glo:Queue,x#)
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
                End !Loop x# = 1 To Records(glo:Queue).
        End!Case local:Errorcode

        Case local:ErrorCode
            Of 0
                locAction        = 'WAYBILL GENERATION: PASSED ACCESSORIES'
            Of 1
                locAction        = 'WAYBILL GENERATION: FAILED ACCESSORIES'
            Of 2
                locAction        = 'WAYBILL GENERATION: PASSED ACCESSORIES (FORCED)'
        End !Case local:ErrorCode

        IF (AddToAudit(job:ref_number,'JOB',locAction,locAuditNotes))
        END ! IF


    tmp:Accessory_Valid_Flag  = local:ErrorCode
LocalProcessedCounters            Routine

    OBFCounter = 0
    NR48eCounter = 0

    Access:WayBPro.clearkey(wyp:AccountJobNumberKey)
    wyp:AccountNumber = tmp:AccountNoFilter
    Set(wyp:AccountJobNumberKey,wyp:AccountJobNumberKey)
    loop
        If Access:WayBPro.next() then
            break
        End
        If wyp:AccountNumber <> tmp:AccountNoFilter then
            break
        End
        !now check whic counter to add this jb to

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = wyp:JobNumber
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !added by Paul 20/04/2010 - log no 10546
            !check the job type
            !open the jobse record
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:Jobse.fetch(jobe:RefNumberKey) = level:benign then
                If jobe:OBFvalidated = 1 then
                    OBFCounter += 1
                Else
                    NR48eCounter += 1
                End
            End

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End !If

    End

    Display()
ProcessWaybill          ROUTINE

    !Before beginning, make sure there are jobs to despatch - L911 (DBH: 07-08-2003)
    Clear(glo:q_JobNumber)
    Free(glo:q_JobNumber)

    Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
    wyp:AccountNumber = tmp:AccountNoFilter
    Set(wyp:AccountJobNumberKey,wyp:AccountJobNumberKey)
    Loop
        If Access:WAYBPRO.NEXT()
           Break
        End !If
        If wyp:AccountNumber <> tmp:AccountNoFilter      |
            Then Break.  ! End If

        !Get the job, just to check
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = wyp:JobNumber
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
            ! Inserting (DBH 06/04/2006) #6609 - Don't process a job if it's in use
            If JobInUse(job:Ref_Number,0)
                Case Missive('Job Number ' & job:Ref_Number & ' cannot be processed. It is currently in use on another machine.'&|
                  '|Please try again later.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Cycle
            End ! If JobInUse(job:Ref_Number,0)
            ! End (DBH 06/04/2006) #6609
            !changes by Paul 20/04/2010 - log no 10546
            Access:JOBSE.clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.Fetch(jobe:RefNumberKey) = level:notify then
                !error
            End
            !decide which job types we need to add to the waybill
            If WaybillType = 'OBF' then
                !only Add OBF Job types
                If jobe:OBFvalidated = 1 then
                    !this is an OBF job - so add it to the queue
                    GLO:Job_Number_Pointer  = job:Ref_Number
                    Add(glo:q_JobNumber)
                End
            Else
                !Add the Normal Type Jobs
                If jobe:OBFvalidated = 0 then
                    !this is not an OBF
                    GLO:Job_Number_Pointer  = job:Ref_Number
                    Add(glo:q_JobNumber)
                End
            End

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
    End !Loop


    If Records(glo:q_JobNumber) = 0
        !There are no jobs in the list -  (DBH: 07-08-2003)
        Case Missive('There are no jobs ready to be despatched.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else !Records(glo:q_JobNumber) = 0
        !There are jobs, lets start despatching -  (DBH: 07-08-2003)
        LocalOverWrite = GETINI('DESPATCH','OVERWRITECOURIER','N',CLIP(PATH()) & '\SB2KDEF.INI')
        LocalCourier = GETINI('DESPATCH','OUTGOINGCOURIER','',CLIP(PATH()) & '\SB2KDEF.INI')

        !changes made by Paul 29/04/2010 - log no 10546
        Continue# = 0
        If ProcessAuto <> 'Y' then
            Case Missive('Are you sure you want to generate a waybill for the processed jobs?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    Continue# = 1
                Of 1
                    Continue# = 0
            End
        Else
            Continue# = 1
        End

        If Continue# = 1 then

            tmp:WayBillNumber   = 0
            !Only do clever things with Waybills if the courier
            !is set to use them - L911 (DBH: 07-08-2003)
            tmp:PrintWaybill    = 0

            If LocalOverWrite <> 'Y'
               Access:TRADEACC.Clearkey(tra:Account_Number_key)
               tra:Account_Number  = Clarionet:Global.Param2
               If Access:TRADEACC.Tryfetch(tra:Account_Number_key) = Level:Benign
                  !Found
                  !Get the courier from the trade account,
                  !from that, if autoincrement cons number is set,
                  !use that as the Waybill number. Otherwise use the old way
                  If tra:Courier_OutGoing
                     Access:COURIER.Clearkey(cou:Courier_Key)
                     cou:Courier = tra:Courier_Outgoing
                     If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                        !Found
                        !If courier is set to print waybills, then pick up the
                        !next number from the waybill file - L911 (DBH: 07-08-2003)
                        If cou:PrintWaybill
                            tmp:Printwaybill = 1
                            !Get next waybill number
                            tmp:WaybillNumber = NextWaybillNumber()
                            !Do not continue if Waybill error - 3432 (DBH: 27-10-2003)
                            If tmp:WaybillNumber = 0
                                Exit
                                !Cycle
                            End !If tmp:WaybillNumber = 0
                        Else !If cou:PrintWaybill
                            If cou:AutoConsignmentNo
                                cou:LastConsignmentNo += 1
                                Access:COURIER.Update()
                                tmp:WayBillNumber   = cou:LastConsignmentNo
                            End !If cou:AutoConsignmentNo
                        End !If cou:PrintWaybill
                     Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                        !Error
                     End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                  End !If cou:Courier_OutGoing
               Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_key) = Level:Benign
                   !Error
               End !If Access:TRADEACC.Tryfetch(tra:Account_Number_key) = Level:Benign
            Else
                Access:COURIER.CLEARKEY(cou:Courier_Key)
                cou:Courier = LocalCourier
                If Access:Courier.Tryfetch(cou:Courier_Key) = Level:Benign
                   !Found
                    If cou:PrintWaybill
                        tmp:PrintWaybill = 1
                        tmp:WaybillNumber   = NextWaybillNumber()
                        !Do not continue if Waybill Error - 3432 (DBH: 27-10-2003)
                        If tmp:WayBillNumber = 0
                            Exit
                            !Cycle
                        End !If tmp:WayBillNumber = 0
                    Else !If cou:PrintWaybill
                        If cou:AutoConsignmentNo
                           cou:LastConsignmentNo += 1
                           Access:COURIER.Update()
                           tmp:WayBillNumber = cou:LastConsignmentNo
                        End !if cou:AutoConsignmentNo
                    End !If cou:PrintWaybill
                End !if Access:Courier.TryFetch(cou:Courier_Key) = Level:Benign
            End !if LocalOverwrite <> 'Y'

            If tmp:WayBillNumber = 0
                Case Missive('Error! You must have an outgoing courier assigned to your account, and this courier must be set-up to produce waybills.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            Else !If tmp:WayBillNumber = 0
                If tmp:PrintWaybill
                    Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                    way:WaybillNumber   = tmp:WaybillNumber
                    If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                        !Found
                        way:AccountNumber   = Clarionet:Global.Param2
                        way:WayBillNumber   = tmp:WayBillNumber
                        way:WaybillID       = 1
                        way:FromAccount     = Clarionet:Global.Param2
                        way:ToAccount       = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                        If Access:WAYBILLS.Update() = Level:Benign
                            !Insert Successful
                            Loop jobs# = 1 To Records(glo:Q_JobNumber)
                                Get(glo:Q_JobNumber,jobs#)
                                Access:JOBS.Clearkey(job:Ref_Number_Key)
                                job:Ref_Number  = glo:Job_Number_Pointer
                                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                    !Found
                                    ! Inserting (DBH 06/04/2006) #6609 - Double check that the job is not in use
                                    If JobInUse(job:Ref_Number,1)
                                        Cycle
                                    End ! If JobInUse(job:Ref_Number,1)
                                    ! End (DBH 06/04/2006) #6609

                                    LocationChange(Clip(GETINI('RRC','InTransit',,CLIP(PATH())&'\SB2KDEF.INI')))

                                    stanum# = Sub(GETINI('RRC','StatusDespatchedToARC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)

                                    GetStatus(stanum#,1,'JOB')

                                    job:Incoming_Consignment_Number = tmp:WayBillNumber

                                    !Reget waybill record
                                    Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
                                    wyp:AccountNumber = tmp:AccountNoFilter
                                    wyp:JobNumber     = job:Ref_Number
                                    If Access:WAYBPRO.TryFetch(wyp:AccountJobNumberKey) = Level:Benign
                                        !Found
                                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                        jobe:RefNumber  = job:Ref_Number
                                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                            !Found
                                            jobe:InSecurityPackNo = wyp:SecurityPackNumber

                                            Access:JOBSE.Update()
                                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                            !Error
                                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                                        If Access:JOBS.TryUpdate() = Level:Benign

                                            AddToConsignmentHistory(job:Ref_Number,'RRC','ARC','RAM',tmp:WayBillNumber,'JOB')

                                            !Save job engineer
                                            Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                            wob:RefNumber   = job:Ref_Number
                                            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                !Found
                                                wob:RRCEngineer = job:Engineer
                                                Access:WEBJOB.Update()
                                            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                !Error
                                            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                                            !Remove engineer from job
                                            job:Engineer = ''
                                            Access:JOBS.TryUpdate()

                                            !Add the waybill jobs list
                                            If Access:WAYBILLJ.PrimeRecord() = Level:Benign
                                                waj:WayBillNumber      = tmp:WayBillNumber
                                                waj:JobNumber          = job:Ref_Number
                                                waj:IMEINumber         = wyp:IMEINumber ! Scanned IMEI
                                                waj:OrderNumber        = job:Order_Number
                                                waj:SecurityPackNumber = wyp:SecurityPackNumber
                                                waj:JobType            = 'JOB'
                                                !If Access:WAYBILLJ.TryInsert() = Level:Benign
                                                If Access:WAYBILLJ.TryUpdate() = Level:Benign
                                                    !Insert Successful

                                                Else !If Access:WAYBILLJ.TryInsert() = Level:Benign
                                                    !Insert Failed
                                                    Access:WAYBILLJ.CancelAutoInc()
                                                End !If Access:WAYBILLJ.TryInsert() = Level:Benign
                                            End !If Access:WAYBILLJ.PrimeRecord() = Level:Benign

                                            !TB13370 - add staging table for waybill confirmation with courier
                                            !J - 16/03/15
                                            Access:WAYBCONF.primerecord()
                                            WAC:WaybillNo        = way:WayBillNumber
                                            WAC:AccountNumber    = way:AccountNumber
                                            WAC:GenerateDate     = way:TheDate            !yes this is a duplicate, but it is needed for a key
                                            WAC:GenerateTime     = way:TheTime            !yes this is a duplicate, but it is needed for a key
                                            WAC:ConfirmationSent = false
                                            WAC:ConfirmResentQty = 0
                                            Access:WAYBCONF.update()
                                            !end TB13370

                                            IF (AddToAudit(job:ref_number,'JOB','WAYBILL GENERATED: ' & tmp:WayBillNumber,'SECURITY PACK NO: ' & wyp:SecurityPackNumber))
                                            END ! IF


                                        End !If Access:JOBS.TryUpdate() = Level:Benign
                                        Relate:WAYBPRO.Delete(0)
                                    Else!If Access:WAYBPRO.TryFetch(wyp:AccountJobNumberKey) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End!If Access:WAYBPRO.TryFetch(wyp:AccountJobNumberKey) = Level:Benign

                                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                    !Error
                                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            End !Loop jobs# = 1 To Records(glo:Q_JobNumber)

                            glo:ErrorText = Comments

                            If LocalOverWrite <> 'Y' THEN
                               WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                                               Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                                               tmp:WayBillNumber,tra:Courier_Outgoing)
                            Else
                                WayBillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                                               Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')),'TRA',|
                                               tmp:WayBillNumber,LocalCourier)
                            End !if LocalOverWrite <> 'Y'
                            glo:ErrorText = ''

                            Case Missive('Waybill ' & Clip(tmp:WaybillNumber) & ' created.','ServiceBase 3g',|
                                           'midea.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive

                        End !If Access:WAYBILLS.Update() = Level:Benign
                    Else ! If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                        !Error
                    End !If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                End !If tmp:PrintWaybill

            End !If tmp:WayBillNumber = 0
        End ! If Continue
    End !Records(glo:q_JobNumber) = 0
    BRW7.ResetSort(1)
    display()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore2.GetBbSize('MainBrowse','?List')             !Xplore
  BRW2.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020522'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MainBrowse')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:LOANACC.Open
  Relate:LOGGED.Open
  Relate:STARECIP.Open
  Relate:WAYBCONF.Open
  Relate:WAYBILLJ.Open
  Relate:WAYBPRO.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:SUBEMAIL.UseFile
  Access:TRAEMAIL.UseFile
  Access:WAYBILLS.UseFile
  Access:JOBACC.UseFile
  Access:COURIER.UseFile
  Access:CONTHIST.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:WAYBAWT,SELF)
  BRW7.Init(?List:2,Queue:Browse:1.ViewPosition,BRW7::View:Browse,Queue:Browse:1,Relate:WAYBPRO,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:AccountNoFilter = Clarionet:Global.Param2
  
  !added by Paul 20/04/2010 - log no 10546
  !check to see if there are already processed records and if so - add them to the count
  
  Do LocalProcessedCounters
  ! Save Window Name
   AddToLog('Window','Open','MainBrowse')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore2.Init(ThisWindow,BRW2,Queue:Browse:2,window,?List,'sbj04app.INI','>Header',0,BRW2.ViewOrder,Xplore2.RestoreHeader,BRW2.SequenceNbr,XploreMask2,XploreMask12,XploreTitle2,BRW2.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW2.Q &= Queue:Browse:2
  BRW2.AddSortOrder(,wya:AccountJobNumberKey)
  BRW2.AddRange(wya:AccountNumber,tmp:AccountNoFilter)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,wya:JobNumber,1,BRW2)
  BIND('AWType',AWType)
  BIND('tmp:Exchanged1',tmp:Exchanged1)
  BIND('tmp:Blank',tmp:Blank)
  ?List{PROP:IconList,1} = '~bluetick.ico'
  BRW2.AddField(wya:JobNumber,BRW2.Q.wya:JobNumber)
  BRW2.AddField(wya:ModelNumber,BRW2.Q.wya:ModelNumber)
  BRW2.AddField(wya:IMEINumber,BRW2.Q.wya:IMEINumber)
  BRW2.AddField(AWType,BRW2.Q.AWType)
  BRW2.AddField(tmp:Exchanged1,BRW2.Q.tmp:Exchanged1)
  BRW2.AddField(tmp:Blank,BRW2.Q.tmp:Blank)
  BRW2.AddField(wya:WAYBAWTID,BRW2.Q.wya:WAYBAWTID)
  BRW2.AddField(wya:AccountNumber,BRW2.Q.wya:AccountNumber)
  BRW7.Q &= Queue:Browse:1
  BRW7.AddSortOrder(,wyp:AccountJobNumberKey)
  BRW7.AddRange(wyp:AccountNumber,tmp:AccountNoFilter)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,wyp:JobNumber,1,BRW7)
  BIND('PType',PType)
  BIND('tmp:Exchanged2',tmp:Exchanged2)
  BIND('tmp:Blank2',tmp:Blank2)
  ?List:2{PROP:IconList,1} = '~bluetick.ico'
  BRW7.AddField(wyp:JobNumber,BRW7.Q.wyp:JobNumber)
  BRW7.AddField(wyp:ModelNumber,BRW7.Q.wyp:ModelNumber)
  BRW7.AddField(wyp:IMEINumber,BRW7.Q.wyp:IMEINumber)
  BRW7.AddField(PType,BRW7.Q.PType)
  BRW7.AddField(tmp:Exchanged2,BRW7.Q.tmp:Exchanged2)
  BRW7.AddField(tmp:Blank2,BRW7.Q.tmp:Blank2)
  BRW7.AddField(wyp:WAYBPROID,BRW7.Q.wyp:WAYBPROID)
  BRW7.AddField(wyp:AccountNumber,BRW7.Q.wyp:AccountNumber)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:LOANACC.Close
    Relate:LOGGED.Close
    Relate:STARECIP.Close
    Relate:WAYBCONF.Close
    Relate:WAYBILLJ.Close
    Relate:WAYBPRO.Close
    Relate:WEBJOB.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore2.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore2.sq.Col = Xplore2.CurrentCol
    GET(Xplore2.sq,Xplore2.sq.Col)
    IF Xplore2.ListType = 1 AND BRW2.ViewOrder = False !Xplore
      BRW2.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore2.PutInix('MainBrowse','?List',BRW2.SequenceNbr,Xplore2.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','MainBrowse')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore2.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore2.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore2.AddField(wya:JobNumber,BRW2.Q.wya:JobNumber)
  Xplore2.AddField(wya:ModelNumber,BRW2.Q.wya:ModelNumber)
  Xplore2.AddField(wya:IMEINumber,BRW2.Q.wya:IMEINumber)
  Xplore2.AddField(wya:WAYBAWTID,BRW2.Q.wya:WAYBAWTID)
  Xplore2.AddField(wya:AccountNumber,BRW2.Q.wya:AccountNumber)
  BRW2.FileOrderNbr = BRW2.AddSortOrder(,wya:AccountJobNumberKey) !Xplore Sort Order for File Sequence
  BRW2.AddRange(wya:AccountNumber,tmp:AccountNoFilter) !Xplore
  BRW2.SetOrder('')                                   !Xplore
  BRW2.ViewOrder = True                               !Xplore
  Xplore2.AddAllColumnSortOrders(1)                   !Xplore
  BRW2.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?GenerateOBFWaybill
      !Check security access
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = InsertEngineerPassword()
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'GENERATE OBF WAYBILL'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              WaybillType = 'OBF'
      
      
              glo:Notes_Global = 'OBF'
              ProcessAuto = ''
              Do ProcessWaybill
      
              WaybillType = ''
              glo:Notes_Global = ''
      
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
              Case Missive('You do not have sufficient access to Generate this waybill.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    OF ?Generate48Waybill
      !Check security access
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = InsertEngineerPassword()
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          Access:ACCAREAS.Clearkey(acc:Access_Level_Key)
          acc:User_Level  = use:User_Level
          acc:Access_Area = 'GENERATE NR/48E WAYBILL'
          If Access:ACCAREAS.Tryfetch(acc:Access_Level_Key) = Level:Benign
              !Found
              WaybillType = '48E'
      
              glo:Notes_Global = '48E'
              ProcessAuto = ''
              Do ProcessWaybill
      
              WaybillType = ''
              glo:Notes_Global = ''
      
          Else ! If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
              !Error
              Case Missive('You do not have sufficient access to Generate this waybill.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End !If Access:ACCAREAS.Tryfetch(Access_Level_Key) = Level:Benign
      
      End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    OF ?Close
      if glo:webJob
          ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
          !Now log out
          access:users.clearkey(use:password_key)
          use:password = glo:password
          if access:users.fetch(use:password_key) = Level:Benign
              access:logged.clearkey(log:user_code_key)
              log:user_code = use:user_code
              log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
              if access:logged.fetch(log:user_code_key) = Level:Benign
                  delete(logged)
              end !if
          end !if
      end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020522'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020522'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020522'&'0')
      ***
    OF ?ProcessJob
      ThisWindow.Update
      ! Process Job
      
      Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)
      wya:AccountNumber = tmp:AccountNoFilter
      wya:JobNumber = tmp:Scanned_Job_No
      if not Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
          if tmp:Scanned_IMEI_No = wya:IMEINumber
      
              Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
              wyp:AccountNumber = tmp:AccountNoFilter
              wyp:JobNumber = wya:IMEINumber
              if Access:WAYBPRO.Fetch(wyp:AccountJobNumberKey)
      
                  Access:Jobs.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wya:JobNumber
                  If not Access:Jobs.Fetch(job:Ref_Number_Key)
                      If local.ValidateAccessories() = True
                          if not Access:WAYBPRO.PrimeRecord()
                              !added by Paul 20/04/2010 - log no 10546
                              Access:jobse.clearkey(jobe:RefNumberKey)
                              jobe:RefNumber = job:Ref_Number
                              If access:jobse.fetch(jobe:RefNumberKey) = level:notify then
                                  !error
                              End
                              If jobe:OBFvalidated = 1 then
                                  OBFCounter += 1
                              Else
                                  NR48eCounter += 1
                              End
                              wyp:AccountNumber = tmp:AccountNoFilter
                              wyp:JobNumber = wya:JobNumber
                              wyp:ModelNumber = wya:ModelNumber
                              wyp:IMEINumber = wya:IMEINumber
                              wyp:SecurityPackNumber = tmp:SecurityPackNumber
      
                              if Access:WAYBPRO.Update()
                                  Access:WAYBPRO.CancelAutoInc()
                              else
                                  Relate:WAYBAWT.Delete(0)
                                  BRW2.ResetSort(1)
                                  BRW7.ResetSort(1)
                              end
                          end
      
                      End !If local.ValidateAccessories = False
      !                do ValidateAccessories
      !                case tmp:Accessory_Valid_Flag
      !                    Of 0 orof 2
      !                        !'WAYBILL GENERATION: PASSED ACCESSORIES'
      !                        !'WAYBILL GENERATION: PASSED ACCESSORIES (FORCED)'
      !                    Of 1
      !                        !'WAYBILL GENERATION: FAILED ACCESSORIES'
      !                end
                  end
      
                  tmp:Scanned_Job_No = 0
                  tmp:Scanned_IMEI_No = ''
                  tmp:SecurityPackNumber = ''
              else
                  Case Missive('This job has already been processed.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              end
      
          else
              Case Missive('No job can be found with the selected Job and I.M.E.I. numbers.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          end
      else
          Case Missive('No job can be found with the selected Job and I.M.E.I. numbers.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      end
      display()
      ?tmp:Scanned_Job_No{prop:Touched} = 1
      ?tmp:Scanned_IMEI_No{Prop:Touched} = 1
      select(?tmp:Scanned_Job_No)
      
      !Check to see if a waybill needs to be auto generated
      If OBFCounter = 10 then
          Case Missive('10 OBF jobs Processed. Press OK to Create Waybill.','ServiceBase 3g',|
                         'MExclam.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      
          WaybillType = 'OBF'
      
      
          glo:Notes_Global = 'OBF'
          ProcessAuto = 'Y'
          Do ProcessWaybill
      
          WaybillType = ''
          glo:Notes_Global = ''
          ProcessAuto = ''
      
          Do LocalProcessedCounters
      End
      
      If NR48eCounter = 10 then
          Case Missive('10 NR/48E jobs Processed. Press OK to Create Waybill.','ServiceBase 3g',|
                         'MExclam.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      
          WaybillType = '48E'
      
          glo:Notes_Global = '48E'
          ProcessAuto = 'Y'
          Do ProcessWaybill
      
          WaybillType = ''
          glo:Notes_Global = ''
          ProcessAuto = ''
      
          Do localProcessedcounters
      End
      BRW2.ResetSort(1)
    OF ?RemoveAwaitingJob
      ThisWindow.Update
      Case Missive('Are you sure you want to remove the selected job from waybill processing?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = brw2.q.wya:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      jobe:HubRepair = 0
                      !Blank the Hub Repair Date/Time, so SB doesn't
                      !think the job has gone to the HUB - 3944 (DBH: 25-02-2004)
                      jobe:HubRepairDate = 0
                      jobe:HubRepairTime = 0
                      Access:JOBSE.Update()
                      Change_Status(job:Ref_Number)
                      Access:JOBS.Update()
      
                      Access:WAYBAWT.Clearkey(wya:JobNumberKey)
                      wya:JobNumber   = job:Ref_Number
                      If Access:WAYBAWT.Tryfetch(wya:JobNumberKey) = Level:Benign
                          !Found
                          Delete(WAYBAWT)
                      Else ! If Access:WAYBAWT.Tryfetch(wya:JobNumberKey) = Level:Benign
                          !Error
                      End !If Access:WAYBAWT.Tryfetch(wya:JobNumberKey) = Level:Benign
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
                  Beep(Beep:SystemExclamation);  Yield()
                  Case Missive('Error! Unable to find the job details.'&|
                      '|'&|
                      '|Delete entry anyway?','ServiceBase 3g',|
                                 'mexclam.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          brw2.UpdateViewRecord()
                          If wya:JobNumber > 0
                              Delete(WAYBAWT)
                              brw2.ResetSort(1)
                          End ! If wya:JobNumber > 0
                      Of 1 ! No Button
                  End ! Case Missive
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          Of 1 ! No Button
      End ! Case Missive
      BRW2.ResetSort(1)
    OF ?RemoveProcessedJob
      ThisWindow.Update
      Case Missive('Are you sure you want to remove the selected job from waybill processing?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = brw7.q.wyp:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      jobe:HubRepair = 0
                      Access:JOBSE.Update()
                      Change_Status(job:Ref_Number)
                      Access:JOBS.Update()
      
                      Access:WAYBPRO.ClearKey(wyp:AccountJobNumberKey)
                      wyp:AccountNumber = brw7.q.wyp:AccountNumber
                      wyp:JobNumber     = brw7.q.wyp:JobNumber
                      If Access:WAYBPRO.TryFetch(wyp:AccountJobNumberKey) = Level:Benign
                          !Found
                          Delete(WAYBPRO)
                          Do LocalProcessedCounters
                      Else!If Access:WAYBPRO.TryFetch(wyp:AccountJobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:WAYBPRO.TryFetch(wyp:AccountJobNumberKey) = Level:Benign
      
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          Of 1 ! No Button
      End ! Case Missive
      BRW7.ResetSort(1)
    OF ?ReprintWaybillButton
      ThisWindow.Update
      AskWaybillNumber
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore2.IgnoreEvent = True                       !Xplore
     Xplore2.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      !Case KeyCode()
      !    Of F10Key
      !        Post(Event:Accepted,?GenerateWaybill)
      !End !KeyCode()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 2)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore2.GetColumnInfo()                         !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.ValidateAccessories      Procedure()
local:Mismatch      Byte()
BookedInQueue       Queue,Pre(bookin)
Accessory               String(30)
                    End
BookedOutQueue      Queue,Pre(bookout)
Accessory               String(30)
                    End

    Code
    !Validate accessories against those booked in - 4285 (DBH: 26-05-2004)
    If job:Exchange_Unit_Number <> 0
        Return True
    End !If job:Exchange_Unit_Number <> 0

    glo:Select12 = job:Ref_Number
! Changing (DBH 05/03/2007) # 8703 - Cancel the process if the user cancels the validate screen
!    ValidateBookedAccessories
! to (DBH 05/03/2007) # 8703
    If ValidateBookedAccessories()
        Return False
    End ! If ValidateBookedAccessores()
! End (DBH 05/03/2007) #8703
    glo:Select12 = ''

    !Compare the booked in, with the tagged accessories - 4285 (DBH: 25-05-2004)
    local:Mismatch = False
    Save_jac_ID = Access:JOBACC.SaveFile()
    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
    jac:Ref_Number = job:Ref_Number
    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
    Loop
        If Access:JOBACC.NEXT()
           Break
        End !If
        If jac:Ref_Number <> job:Ref_Number      |
            Then Break.  ! End If

        glo:Pointer = jac:Accessory
        Get(glo:Queue,glo:Pointer)
        If Error()
            jac:Attached = False
            local:Mismatch = True
        Else ! !If Error()
            jac:Attached = True
        End !If Error()

        Access:JOBACC.Update()
    End !Loop
    Access:JOBACC.RestoreFile(Save_jac_ID)

    If local:Mismatch = True
! Deleting (DBH 05/03/2007) # 8703 - Move to the validation screen
!        Case Missive('There is a mismatch between the selected accessories and the accessories booked in with the unit.','ServiceBase 3g',|
!                       'mstop.jpg','/OK')
!            Of 1 ! OK Button
!        End ! Case Missive
! End (DBH 05/03/2007) #8703
! Changing (DBH 05/03/2007) # 8703 - Call new screen to display sent/retained accessories
!        !Is the mismatch ok? - 4285 (DBH: 26-05-2004)
!        If AccessoryMismatch(job:Ref_Number,'JOB')
!            !No, not ok. Stop the process - 4285 (DBH: 26-05-2004)
!            Return False
!        End !AccessoryMismatch(job:Ref_Number,'JOB')
! to (DBH 05/03/2007) # 8703
        If WaybillGenerationAccessoryMismatch(job:Ref_Number)
            ! Cancelled (DBH: 05/03/2007)
            Return False
        End ! If WaybillGenerationAccessoryMismatch(job:Ref_Number)
! End (DBH 05/03/2007) #8703
    End !If local:Mismatch = True

        Free(BookedInQueue)
        Free(BookedOutQueue)
        Save_jac_ID = Access:JOBACC.SaveFile()
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
        Loop
            If Access:JOBACC.NEXT()
               Break
            End !If
            If jac:Ref_Number <> job:Ref_Number     |
                Then Break.  ! End If
! Inserting (DBH 05/03/2007) # 8703 - Show the retained accessories in the audit trail
            If jac:Attached = False
! End (DBH 05/03/2007) #8703
                bookin:Accessory = jac:Accessory
                Add(BookedInQueue)
            End ! If jac:Attached = False
            If jac:Attached = True
                bookout:Accessory = jac:Accessory
                Add(BookedOutQueue)
            End !If jac:Attached = True
        End !Loop
        Access:JOBACC.RestoreFile(Save_jac_ID)

        If Records(BookedInQueue)
! Changing (DBH 05/03/2007) # 8703 - Show the retained accessories
!            aud:Notes = 'ACCESSORIES BOOKED IN:'
! to (DBH 05/03/2007) # 8703
            locAuditNotes = 'ACCESSORIES RETAINED:'
! End (DBH 05/03/2007) #8703
            Loop x# = 1 To Records(BookedInQueue)
                Get(BookedInQueue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(bookin:Accessory)
            End !Loop x# = 1 To Records(BookedInQueue)

            locAuditNotes = Clip(locAuditNotes) & '<13,10><13,10>ACCESSORIES SENT TO ARC:'

            Loop x# = 1 To Records(BookedOutQueue)
                Get(BookedOutQueue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(bookout:Accessory)
            End !Loop x# = 1 To Records(BookedOutQueue)
        End !If Records(BookedInQueue)

        locAction          = 'WAYBILL GENERATION ACCESSORY VALIDATION'
        If local:Mismatch = True
            locAction      = Clip(locAction) & ' (MISMATCH)'
        End !If local:Mismatch = True

        IF (AddToAudit(job:Ref_Number,'JOB',locAction,locAuditNotes))
        END ! IF


    Return True
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW2.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,2,'GetFreeElementName'
  IF BRW2.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW2.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW2.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,2,'GetFreeElementPosition'
  IF BRW2.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW2.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,2
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore2.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore2.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore2.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW2.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore2.sq)                    !Xplore
    Xplore2.SetupOrder(BRW2.SortOrderNbr)             !Xplore
    GET(Xplore2.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW2.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW2.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW2.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
  tmp:Exchanged1 = 0
  Access:JOBS.Clearkey(job:Ref_Number_Key)
  job:Ref_Number  = wya:JobNumber
  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Found
      If job:Exchange_Unit_NUmber <> 0
          tmp:Exchanged1 = 1
      End !If job:Exchange_Unit_NUmber <> 0
      !added by Paul 20/04/2010 - log no 10546
      !check the job type
      !open the jobse record
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber = job:Ref_Number
      If Access:Jobse.fetch(jobe:RefNumberKey) = level:benign then
          If jobe:OBFvalidated = 1 then
              AWType = 'O'
          Else
              If jobe:Engineer48HourOption = 1 then
                  AWType = '48E'
              Else
                  AWType = 'NR'
              End
          End
      End
  
  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  PARENT.SetQueueRecord
  IF (AWType = 'O')
    SELF.Q.wya:JobNumber_NormalFG = 255
    SELF.Q.wya:JobNumber_NormalBG = 16777215
    SELF.Q.wya:JobNumber_SelectedFG = 16777215
    SELF.Q.wya:JobNumber_SelectedBG = 255
  ELSIF (AWType = '48E')
    SELF.Q.wya:JobNumber_NormalFG = 32768
    SELF.Q.wya:JobNumber_NormalBG = 16777215
    SELF.Q.wya:JobNumber_SelectedFG = 16777215
    SELF.Q.wya:JobNumber_SelectedBG = 32768
  ELSE
    SELF.Q.wya:JobNumber_NormalFG = 65793
    SELF.Q.wya:JobNumber_NormalBG = 16777215
    SELF.Q.wya:JobNumber_SelectedFG = 16777215
    SELF.Q.wya:JobNumber_SelectedBG = 65793
  END
  IF (AWType = 'O')
    SELF.Q.wya:ModelNumber_NormalFG = 255
    SELF.Q.wya:ModelNumber_NormalBG = 16777215
    SELF.Q.wya:ModelNumber_SelectedFG = 16777215
    SELF.Q.wya:ModelNumber_SelectedBG = 255
  ELSIF (AWType = '48E')
    SELF.Q.wya:ModelNumber_NormalFG = 32768
    SELF.Q.wya:ModelNumber_NormalBG = 16777215
    SELF.Q.wya:ModelNumber_SelectedFG = 16777215
    SELF.Q.wya:ModelNumber_SelectedBG = 32768
  ELSE
    SELF.Q.wya:ModelNumber_NormalFG = 65793
    SELF.Q.wya:ModelNumber_NormalBG = 16777215
    SELF.Q.wya:ModelNumber_SelectedFG = 16777215
    SELF.Q.wya:ModelNumber_SelectedBG = 65793
  END
  IF (AWType = 'O')
    SELF.Q.wya:IMEINumber_NormalFG = 255
    SELF.Q.wya:IMEINumber_NormalBG = 16777215
    SELF.Q.wya:IMEINumber_SelectedFG = 16777215
    SELF.Q.wya:IMEINumber_SelectedBG = 255
  ELSIF (AWType = '48E')
    SELF.Q.wya:IMEINumber_NormalFG = 32768
    SELF.Q.wya:IMEINumber_NormalBG = 16777215
    SELF.Q.wya:IMEINumber_SelectedFG = 16777215
    SELF.Q.wya:IMEINumber_SelectedBG = 32768
  ELSE
    SELF.Q.wya:IMEINumber_NormalFG = 65793
    SELF.Q.wya:IMEINumber_NormalBG = 16777215
    SELF.Q.wya:IMEINumber_SelectedFG = 16777215
    SELF.Q.wya:IMEINumber_SelectedBG = 65793
  END
  IF (AWType = 'O')
    SELF.Q.AWType_NormalFG = 255
    SELF.Q.AWType_NormalBG = 16777215
    SELF.Q.AWType_SelectedFG = 16777215
    SELF.Q.AWType_SelectedBG = 255
  ELSIF (AWType = '48E')
    SELF.Q.AWType_NormalFG = 32768
    SELF.Q.AWType_NormalBG = 16777215
    SELF.Q.AWType_SelectedFG = 16777215
    SELF.Q.AWType_SelectedBG = 32768
  ELSE
    SELF.Q.AWType_NormalFG = 65793
    SELF.Q.AWType_NormalBG = 16777215
    SELF.Q.AWType_SelectedFG = 16777215
    SELF.Q.AWType_SelectedBG = 65793
  END
  IF (tmp:Exchanged1 = 1)
    SELF.Q.tmp:Exchanged1_Icon = 1
  ELSE
    SELF.Q.tmp:Exchanged1_Icon = 0
  END


BRW2.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,2,'TakeEvent'
  Xplore2.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore2.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore2.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore2.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore2.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore2.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW2.FileSeqOn = False                         !Xplore
       Xplore2.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW2.SavePosition()                           !Xplore
       Xplore2.HandleMyEvents()                       !Xplore
       !BRW2.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW2.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore2.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.SetQueueRecord PROCEDURE

  CODE
  tmp:Exchanged2 = 0
  Access:JOBS.Clearkey(job:Ref_Number_Key)
  job:Ref_Number  = wyp:JobNumber
  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Found
      If job:Exchange_Unit_NUmber <> 0
          tmp:Exchanged2 = 1
      End !If job:Exchange_Unit_NUmber <> 0
      !added by Paul 20/04/2010 - log no 10546
      !check the job type
      !open the jobse record
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber = job:Ref_Number
      If Access:Jobse.fetch(jobe:RefNumberKey) = level:benign then
          If jobe:OBFvalidated = 1 then
              PType = 'O'
          Else
              If jobe:Engineer48HourOption = 1 then
                  PType = '48E'
              Else
                  PType = 'NR'
              End
          End
      End
  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  PARENT.SetQueueRecord
  IF (PType = 'O')
    SELF.Q.wyp:JobNumber_NormalFG = 255
    SELF.Q.wyp:JobNumber_NormalBG = 16777215
    SELF.Q.wyp:JobNumber_SelectedFG = 16777215
    SELF.Q.wyp:JobNumber_SelectedBG = 255
  ELSIF (PType = '48E')
    SELF.Q.wyp:JobNumber_NormalFG = 32768
    SELF.Q.wyp:JobNumber_NormalBG = 16777215
    SELF.Q.wyp:JobNumber_SelectedFG = 16777215
    SELF.Q.wyp:JobNumber_SelectedBG = 32768
  ELSE
    SELF.Q.wyp:JobNumber_NormalFG = 65793
    SELF.Q.wyp:JobNumber_NormalBG = 16777215
    SELF.Q.wyp:JobNumber_SelectedFG = 16777215
    SELF.Q.wyp:JobNumber_SelectedBG = 65793
  END
  IF (PType = 'O')
    SELF.Q.wyp:ModelNumber_NormalFG = 255
    SELF.Q.wyp:ModelNumber_NormalBG = 16777215
    SELF.Q.wyp:ModelNumber_SelectedFG = 16777215
    SELF.Q.wyp:ModelNumber_SelectedBG = 255
  ELSIF (PType = '48E')
    SELF.Q.wyp:ModelNumber_NormalFG = 32768
    SELF.Q.wyp:ModelNumber_NormalBG = 16777215
    SELF.Q.wyp:ModelNumber_SelectedFG = 16777215
    SELF.Q.wyp:ModelNumber_SelectedBG = 32768
  ELSE
    SELF.Q.wyp:ModelNumber_NormalFG = 65793
    SELF.Q.wyp:ModelNumber_NormalBG = 16777215
    SELF.Q.wyp:ModelNumber_SelectedFG = 16777215
    SELF.Q.wyp:ModelNumber_SelectedBG = 65793
  END
  IF (PType = 'O')
    SELF.Q.wyp:IMEINumber_NormalFG = 255
    SELF.Q.wyp:IMEINumber_NormalBG = 16777215
    SELF.Q.wyp:IMEINumber_SelectedFG = 16777215
    SELF.Q.wyp:IMEINumber_SelectedBG = 255
  ELSIF (PType = '48E')
    SELF.Q.wyp:IMEINumber_NormalFG = 32768
    SELF.Q.wyp:IMEINumber_NormalBG = 16777215
    SELF.Q.wyp:IMEINumber_SelectedFG = 16777215
    SELF.Q.wyp:IMEINumber_SelectedBG = 32768
  ELSE
    SELF.Q.wyp:IMEINumber_NormalFG = 65793
    SELF.Q.wyp:IMEINumber_NormalBG = 16777215
    SELF.Q.wyp:IMEINumber_SelectedFG = 16777215
    SELF.Q.wyp:IMEINumber_SelectedBG = 65793
  END
  IF (PType = 'O')
    SELF.Q.PType_NormalFG = 255
    SELF.Q.PType_NormalBG = 16777215
    SELF.Q.PType_SelectedFG = 16777215
    SELF.Q.PType_SelectedBG = 255
  ELSIF (PType = '48E')
    SELF.Q.PType_NormalFG = 32768
    SELF.Q.PType_NormalBG = 16777215
    SELF.Q.PType_SelectedFG = 16777215
    SELF.Q.PType_SelectedBG = 32768
  ELSE
    SELF.Q.PType_NormalFG = 65793
    SELF.Q.PType_NormalBG = 16777215
    SELF.Q.PType_SelectedFG = 16777215
    SELF.Q.PType_SelectedBG = 65793
  END
  IF (tmp:Exchanged2 = 1)
    SELF.Q.tmp:Exchanged2_Icon = 1
  ELSE
    SELF.Q.tmp:Exchanged2_Icon = 0
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW2.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW2.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW2.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW2.ResetPairsQ PROCEDURE()
  CODE
Xplore2.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore2.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore2.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW2.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !wya:JobNumber
  OF 6 !wya:ModelNumber
  OF 11 !wya:IMEINumber
  OF 16 !AWType
  OF 21 !tmp:Exchanged1
  OF 23 !tmp:Blank
  OF 24 !wya:WAYBAWTID
  OF 25 !wya:AccountNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore2.SetNewOrderFields PROCEDURE()
  CODE
  BRW2.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore2.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore2Step8,wya:AccountJobNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,wya:AccountJobNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore2Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore2Locator1)
      Xplore2Locator1.Init(,wya:JobNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore2Locator2)
      Xplore2Locator2.Init(,wya:ModelNumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore2Locator3)
      Xplore2Locator3.Init(,wya:IMEINumber,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step4.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore2Locator4)
      Xplore2Locator4.Init(,AWType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step5.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore2Locator5)
      Xplore2Locator5.Init(,tmp:Exchanged1,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step6.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore2Locator6)
      Xplore2Locator6.Init(,tmp:Blank,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore2Locator7)
      Xplore2Locator7.Init(,wya:WAYBAWTID,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore2Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore2Locator8)
      Xplore2Locator8.Init(,wya:AccountNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(wya:AccountNumber,tmp:AccountNoFilter)
  RETURN
!================================================================================
Xplore2.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore2.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW2.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(WAYBAWT)
  END
  RETURN TotalRecords
!================================================================================
Xplore2.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('wya:JobNumber')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('wya:ModelNumber')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model No')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model No')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('wya:IMEINumber')             !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('IMEI No')                    !Header
                PSTRING('@s20')                       !Picture
                PSTRING('IMEI No')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('AWType')                     !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('AWType')                     !Header
                PSTRING('@S20')                       !Picture
                PSTRING('AWType')                     !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('tmp:Exchanged1')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Exchanged1')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Exchanged1')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('tmp:Blank')                  !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Blank')                  !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Blank')                  !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('wya:WAYBAWTID')              !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('WAYBAWT ID')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('WAYBAWT ID')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('wya:AccountNumber')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Header Account Number')      !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Header Account Number')      !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(8)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
ViewSundryWaybill PROCEDURE (f:WaybillRecordNumber)   !Generated from procedure template - Window

tmp:UserName         STRING(30)
tmp:WaybillRecordNumber LONG
BRW9::View:Browse    VIEW(WAYSUND)
                       PROJECT(was:Description)
                       PROJECT(was:Quantity)
                       PROJECT(was:RecordNumber)
                       PROJECT(was:WAYBILLSRecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
was:Description        LIKE(was:Description)          !List box control field - type derived from field
was:Quantity           LIKE(was:Quantity)             !List box control field - type derived from field
was:RecordNumber       LIKE(was:RecordNumber)         !Primary key field - type derived from field
was:WAYBILLSRecordNumber LIKE(was:WAYBILLSRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(WAYAUDIT)
                       PROJECT(waa:TheDate)
                       PROJECT(waa:TheTime)
                       PROJECT(waa:Action)
                       PROJECT(waa:RecordNumber)
                       PROJECT(waa:WAYBILLSRecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
waa:TheDate            LIKE(waa:TheDate)              !List box control field - type derived from field
waa:TheTime            LIKE(waa:TheTime)              !List box control field - type derived from field
tmp:UserName           LIKE(tmp:UserName)             !List box control field - type derived from local data
waa:Action             LIKE(waa:Action)               !List box control field - type derived from field
waa:RecordNumber       LIKE(waa:RecordNumber)         !Primary key field - type derived from field
waa:WAYBILLSRecordNumber LIKE(waa:WAYBILLSRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       STRING('Waybill:'),AT(168,86),USE(?String2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s8),AT(200,86),USE(way:WayBillNumber),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('Waybill Items'),AT(168,98),USE(?String4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('User Notes'),AT(360,98),USE(?String4:2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(168,110,184,72),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('106L(2)|M~Description~@s30@32L(2)|M~Quantity~@s30@'),FROM(Queue:Browse)
                       TEXT,AT(360,110,152,72),USE(way:UserNotes),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('User Notes')
                       STRING('Audit Trail'),AT(168,188),USE(?String5),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(168,200,344,126),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('47R(2)|M~Date~@d6@33L(2)|M~Time~R@t1b@120L(2)|M~User Name~@s60@240L(2)|M~Action~' &|
   '@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Sundry Waybill Details'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(376,332),USE(?Button:PrintWaybill),TRN,FLAT,ICON('prnwayp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020663'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ViewSundryWaybill')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:USERS.Open
  Relate:WAYAUDIT.Open
  Access:WAYBILLS.UseFile
  SELF.FilesOpened = True
  tmp:WaybillRecordNumber = f:WaybillRecordNumber
  Access:WAYBILLS.ClearKey(way:RecordNumberKey)
  way:RecordNumber = f:WaybillRecordNumber
  If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
      !Found
  Else ! If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
      !Error
  End ! If Access:WAYBILLS.TryFetch(way:RecordNumberKey) = Level:Benign
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:WAYSUND,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:WAYAUDIT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','ViewSundryWaybill')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,was:EnteredKey)
  BRW9.AddRange(was:WAYBILLSRecordNumber,tmp:WaybillRecordNumber)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,was:RecordNumber,1,BRW9)
  BRW9.AddField(was:Description,BRW9.Q.was:Description)
  BRW9.AddField(was:Quantity,BRW9.Q.was:Quantity)
  BRW9.AddField(was:RecordNumber,BRW9.Q.was:RecordNumber)
  BRW9.AddField(was:WAYBILLSRecordNumber,BRW9.Q.was:WAYBILLSRecordNumber)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,waa:DateKey)
  BRW10.AddRange(waa:WAYBILLSRecordNumber,tmp:WaybillRecordNumber)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,waa:TheDate,1,BRW10)
  BIND('tmp:UserName',tmp:UserName)
  BRW10.AddField(waa:TheDate,BRW10.Q.waa:TheDate)
  BRW10.AddField(waa:TheTime,BRW10.Q.waa:TheTime)
  BRW10.AddField(tmp:UserName,BRW10.Q.tmp:UserName)
  BRW10.AddField(waa:Action,BRW10.Q.waa:Action)
  BRW10.AddField(waa:RecordNumber,BRW10.Q.waa:RecordNumber)
  BRW10.AddField(waa:WAYBILLSRecordNumber,BRW10.Q.waa:WAYBILLSRecordNumber)
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:USERS.Close
    Relate:WAYAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ViewSundryWaybill')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020663'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020663'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020663'&'0')
      ***
    OF ?Button:PrintWaybill
      ThisWindow.Update
      WayBillDespatch(way:FromAccount,'TRA',way:ToACcount,'TRA',way:WaybillNumber,way:Courier)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  Access:USERS.ClearKey(use:User_Code_Key)
  use:User_Code = waa:UserCode
  If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
      !Error
      tmp:UserName = '** UNKNOWN USER: ' & Clip(waa:UserCode) & ' **'
  End ! If Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign
  BRW10::RecordStatus=ReturnValue
  RETURN ReturnValue

