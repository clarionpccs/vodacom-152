    SECTION('Declarations')
RepQClass           CLASS,TYPE
RecordNumber            LONG
CriteriaFileName        CSTRING(255)
FullCriteriaFilename    CSTRING(255)
FinalFolder             CSTRING(255)
FinalFilename           CSTRING(255) ! Name of Final Filename
FinalFullPath           CSTRING(255) ! Destination of Final Report (inc name)
TotalRecords            LONG
CurrentRecord           LONG
SBOReport               LONG
Usercode                STRING(3)
AccountNumber           STRING(30)
Construct               PROCEDURE()
Destruct                PROCEDURE()
Init                    PROCEDURE(STRING pCommandLine),LONG
Failed                  PROCEDURE()
FinishReport            PROCEDURE(STRING pExportFilename, STRING pReportFilename),LONG
SetReportName           PROCEDURE(STRING pReportName)
UpdateProgress          PROCEDURE(),LONG
UpdateStatus            PROCEDURE(<STRING pStatus>, REAL pPct=0, LONG pFinished=0, <STRING pReportFilename>)
                    END !  CLASS   

    SECTION('Procedures')
RepQClass.Construct      PROCEDURE()
    CODE
        SELF.SBOReport = FALSE
        SELF.TotalRecords = 0
        SELF.CurrentRecord = 0
RepQClass.Destruct  PROCEDURE()
    CODE
        REMOVE(SELF.FullCriteriaFilename)
        
RepQClass.Init      PROCEDURE(STRING pCommandLine)
RetValue                LONG(Level:Benign)
posRN                   LONG
    CODE
        SELF.SBOReport = FALSE
        LOOP 1 TIMES
            posRN = INSTRING('SBO',pCommandLine,1,1)
            IF (posRN = 0)
                RetValue = Level:Fatal
                BREAK
            ELSE
                SELF.RecordNumber = CLIP(SUB(pCommandLine,posRN + 3,10))
                SELF.SBOReport = TRUE
            END ! IF
            
            Relate:REPORTQ.Open()
            
            Access:REPORTQ.ClearKey(rpq:RecordNumberKey)
            rpq:RecordNumber = SELF.RecordNumber
            IF (Access:REPORTQ.TryFetch(rpq:RecordNumberKey))
                RetValue = Level:Fatal
            ELSE ! IF
                SELF.CriteriaFilename = rpq:CriteriaXMLFile
                SELF.FullCriteriaFilename = PATH() & '\temp\' & CLIP(rpq:CriteriaXMLFile)
                SELF.Usercode = rpq:Usercode
                SELF.AccountNumber = rpq:AccountNumber
            END ! IF
            
            SELF.FinalFolder = PATH() & '\downloads\'
            
            IF (~EXISTS(SELF.FinalFolder))
                rtn# = MkDir(SELF.FinalFolder)
            END ! IF            
            
            SELF.FinalFolder = PATH() & '\downloads\' & CLIP(rpq:AccountNumber) & '\'
            
            IF (~EXISTS(SELF.FinalFolder))
                rtn# = MkDir(SELF.FinalFolder)
            END ! IF
            
            Relate:REPORTQ.Close()
        END! BREAK LOOP
        
        RETURN RetValue
   
RepQClass.FinishReport   PROCEDURE(STRING pExportFilename, STRING pReportFilename)!,LONG !Pass full path to report. Move to final position
RetValue                        LONG(Level:Benign)
    CODE
        LOOP 1 TIMES
            SELF.SetReportName(pExportFilename)
            ! REMOVE Any Existing Report
            IF (EXISTS(SELF.FinalFullPath))
                REMOVE(SELF.FinalFullPath)
                IF (EXISTS(SELF.FinalFullPath))
                    ! Failed to remove
                    !STOP('Not removed: ' & SELF.FinalFullPath)
                    SELF.Failed()
                    RetValue = Level:Fatal
                    BREAK
                END ! IF
            END ! IF

            ! Move file to final folder
            RENAME(CLIP(pReportFilename),SELF.FinalFullPath)
            IF (~EXISTS(SELF.FinalFullPath))
                !STOP('Not moved: <13,10>' & CLIP(pReportFilename) & '<13,10>' & SELF.FinalFullPath)
                SELF.Failed()
                RetValue = Level:Fatal
                BREAK
            ELSE ! IF
                SELF.UpdateStatus('Finished',,1,SELF.FinalFilename)
            END ! IF
        END ! BREAK LOOP
        
        RETURN RetValue
        
RepQClass.SetReportName     PROCEDURE(STRING pReportFilename) ! Input filename of temp folder. Set the final full path
i                               LONG
lenName                         LONG
posExt                          LONG
newName                         STRING(1000)
    CODE
        lenName = LEN(CLIP(pReportFilename))
        LOOP i = lenName TO 1 BY -1
            IF (pReportFilename[i] = '.')
                posExt = i
                BREAK
            END ! IF
        END ! LOOP
        
        ! Append user and random on the end of report name
        newName = pReportFilename[1 : i - 1] & |
            ' (' & CLIP(SELF.Usercode) & ') - ' & |
            FORMAT(TODAY(),@d012) & FORMAT(CLOCK(),@t05) & |
            pReportFilename[i : lenName]
        
        SELF.FinalFilename = CLIP(newName)
        
        SELF.FinalFullPath = SELF.FinalFolder & SELF.FinalFilename
        
RepQClass.UpdateStatus      PROCEDURE(<STRING pStatus>, REAL pPct=0, LONG pFinished=0,<STRING pReportFilename>)
    CODE
        Relate:REPORTQ.Open()
        Access:REPORTq.ClearKey(rpq:RecordNumberKey)
        rpq:RecordNumber = SELF.RecordNumber
        IF (Access:REPORTq.TryFetch(rpq:RecordNumberKey) = Level:Benign)
            IF (pStatus <> '')
                rpq:Status = pStatus
            END ! IF
            rpq:Percentage = pPct
            IF (pFinished = 1)
                rpq:FinishedDate = TODAY()
                rpq:FinishedTime = CLOCK()
                rpq:Percentage = 100
            END ! IF
            IF (pReportFilename <> '')
                rpq:FinalURL = pReportFilename
            END ! IF
            Access:REPORTQ.TryUpdate()
        ELSE ! IF
        END ! IF
        Relate:REPORTQ.Close()
        
RepQClass.UpdateProgress    PROCEDURE()!,LONG
Pct                             LONG
RetValue                        LONG(Level:Benign)
    CODE
        SELF.CurrentRecord += 1
        Pct = INT((SELF.CurrentRecord / SELF.TotalRecords) * 100)    
        IF (Pct % 10 = 0)
            SELF.UpdateStatus(,Pct)
            
            ! Does the Report still exist?
            Relate:REPORTQ.Open()
            Access:REPORTq.ClearKey(rpq:RecordNumberKey)
            rpq:RecordNumber = SELF.RecordNumber
            IF (Access:REPORTq.TryFetch(rpq:RecordNumberKey))
                RetValue = Level:Fatal
            END ! IF
            Relate:REPORTQ.Close()            
            
        END ! IF
        
        RETURN RetValue
        
RepQClass.Failed    PROCEDURE()
    CODE
        SELF.UpdateStatus('Failed',0,1)
            