    SECTION('Declarations')
!region Declarations
SBR                 CLASS
RecordsToProcess        LONG,AUTO
RecordsProcessed        LONG,AUTO
PercentProgress         BYTE,AUTO
CancelPressed           BYTE,AUTO
StartDate               DATE(),AUTO
StartTime               TIME(),AUTO
ProgressThermometer     BYTE(),AUTO
CNTProgressThermometer  BYTE(),AUTO
UserString              STRING(100)
CNTUserString           STRING(100)
SkipRecords             LONG(),AUTO
SkipCounter             LONG()
HideProgress            LONG()
Init                    PROCEDURE(LONG pHideProgress=0)
UpdateProgressText      PROCEDURE(<STRING fText>,<BYTE fSkipClarionet>)
UpdateProgressWindow    PROCEDURE(<BYTE fNoCount>),BYTE,PROC
OpenProgressWindow      PROCEDURE(<LONG fSkipRecords>,<LONG fRecords>)
EndProgress             PROCEDURE(<STRING fOpenFolder>)
AddToExport             PROCEDURE(<STRING fText>,<BYTE fStart>,<BYTE fEnd>)
LoadTempData            PROCEDURE(oiExcel fExcel,STRING fTempFileName,STRING fEndPoint),STRING
FinishFormat            PROCEDURE(oiExcel fExcel,STRING fLastColumn,LONG fLastRow,<STRING fFreezeColumn>)
CopyFinalFile           PROCEDURE(STRING fTempFile,STRING fFinalFile),BYTE
ClearClipboard          PROCEDURE(LONG fTimeOut=5)
Kill                    PROCEDURE()

                    END
SBR:StatusQueue         QUEUE()
SBR:StatusMessage           STRING(100)
                    END
SBR:SavePath        CSTRING(255)
SBR:ExportFilePath  CSTRING(255),STATIC
SBR:ExportFile      FILE,DRIVER('ASCII'),PRE(sbrexp),NAME(SBR:ExportFilePath),CREATE,BINDABLE,THREAD
                        RECORD
ExpLine                     STRING(1000)
                        END
                    END

    COMPILE('**CNT**',ClarionetUsed = 1)
SBR:CNTReportProgressWindow WINDOW('Working...'),AT(,,307,46),CENTER,GRAY,IMM, |
                                FONT('Tahoma',8,,FONT:Regular),DOUBLE
                                PROGRESS,AT(3,20,301,15),USE(SBR.CNTProgressThermometer),RANGE(0,100)
                                STRING(@s100),AT(3,5,301,10),USE(SBR.CNTUserString),CENTER
                            END
    **CNT**
SBR:ReportProgressWindow    WINDOW('Progress...'),AT(0,0,680,428),GRAY,IMM, |
                                ICON('Cellular3g.ico'),FONT('Tahoma',8,,FONT:Regular,CHARSET:ANSI), |
                                TIMER(1),COLOR(00D6EAEFh),WALLPAPER('sbback.jpg'),Tiled,DOUBLE
                                PANEL,AT(164,68,352,12),USE(?SBR:ProgPanel),FILL(009A6A7Ch)
                                LIST,AT(208,88,268,206),USE(?SBR:List),FONT(,,00010101h,,CHARSET:ANSI), |
                                    FORMAT('20L(2)|M'),COLOR(COLOR:White,00010101h,COLOR:White),VSCROLL, |
                                    GRID(COLOR:WHITE),FROM(SBR:StatusQueue)
                                PANEL,AT(164,84,352,246),USE(?SBR:ProgPanel2),FILL(009A6A7Ch)
                                PROGRESS,AT(206,314,268,12),USE(SBR.ProgressThermometer),RANGE(0,100)
                                STRING(''),AT(259,300,161,10),USE(SBR.UserString),FONT('Tahoma',8, |
                                    COLOR:White,FONT:Bold),TRN,COLOR(009A6A7Ch),CENTER
                                PANEL,AT(164,332,352,28),USE(?SBR:ProgPanel3),FILL(009A6A7Ch)
                                PROMPT('Report Progress'),AT(168,70),USE(?SBRWindowTitle),FONT('Tahoma', |
                                    8,COLOR:White,FONT:Bold,CHARSET:ANSI),TRN
                                BUTTON,AT(444,332),USE(?SBR:ProgressCancel),ICON('cancelp.jpg'),FLAT,LEFT,TRN
                                BUTTON,AT(376,332),USE(?SBR:Button:OpenReportFolder),ICON('openrepp.jpg'),HIDE, |
                                    FLAT,TRN
                                BUTTON,AT(444,332),USE(?SBR:Finish),ICON('finishp.jpg'),HIDE,FLAT,TRN
                            END
!endregion
!region Procedures
    SECTION('Procedures')
SBR.Init            PROCEDURE(LONG pHideProgress=0)
    CODE
        REMOVE(SBR:ExportFile)
        CREATE(SBR:ExportFile)
        OPEN(SBR:ExportFile)
        
        SELF.HideProgress = pHideProgress


SBR.OpenProgressWindow      PROCEDURE(<LONG fSkipRecords>,<LONG fRecords>)
    CODE
        IF (fRecords = 0)
            SBR.RecordsToProcess = 100
        ELSE
            SBR.RecordsToProcess = fRecords
        END

        IF (fSkipRecords > 0)
            SBR.SkipRecords = fSkipRecords
        ELSE
            SBR.SkipRecords = 0
        END

        SBR.RecordsProcessed = 0
        SBR.ProgressThermometer = 0
        SBR.PercentProgress = 0
        SBR.StartDate = TODAY()
        SBR.StartTime = CLOCK()
        SBR.CancelPressed = 0
        COMPILE('**CNT**',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            Clarionet:OpenPushWindow(SBR:CNTReportProgressWindow)
            RETURN
        END
        **CNT**
        IF (SELF.HideProgress = 0)
            OPEN(SBR:ReportProgressWindow)
        END ! IF
        
SBR.Kill            PROCEDURE()
    CODE
        CLOSE(SBR:ExportFile)

SBR.UpdateProgressText      PROCEDURE(<STRING fText>,<BYTE fSkipClarionet>)
locText                         STRING(100)
    CODE
        IF (SELF.HideProgress = 1)
            RETURN
        END ! IF
        IF (fText <> '')
            locText = fText
        END
        COMPILE('**CNT**',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            IF (fSkipClarionet = 1)
                RETURN
            END
            SBR.CNTUserString = CLIP(locText)
            Clarionet:UpdatePushWindow(SBR:CNTReportProgressWindow)
            RETURN
        END
        **CNT**
        SBR:StatusQueue.SBR:StatusMessage = CLIP(locText)
        ADD(SBR:StatusQueue)
        SELECT(?SBR:List,RECORDS(SBR:StatusQueue))
        DISPLAY()

SBR.UpdateProgressWindow    PROCEDURE(<BYTE fNoCount>)
    CODE
        IF (SELF.HideProgress = 1)
            RETURN 0
        END ! IF
        SELF.SkipCounter += 1
        IF (SELF.SkipRecords > 0)
            IF (SELF.SkipCounter > SELF.SkipRecords)
                SELF.SkipCounter = 0
            ELSE
                RETURN 0
            END
        END
        IF (fNoCount <> 1)
           ! Update window, but dont increase counter

            SBR.RecordsProcessed += 1
            IF (SBR.PercentProgress < 100)
                SBR.PercentProgress = (SBR.RecordsProcessed / SBR.RecordsToProcess) * 100
                IF (SBR.PercentProgress => 100)
                    SBR.RecordsProcessed = 0
                    SBR.PercentProgress = 0
                    SBR.ProgressThermometer = 0
                END
                IF (SBR.PercentProgress <> SBR.ProgressThermometer)
                    SBR.ProgressThermometer = SBR.PercentProgress
                END
            END
        END

        DISPLAY()
        COMPILE('**CNT**',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            SBR.CNTProgressThermometer = SBR.ProgressThermometer
            Clarionet:UpdatePushWindow(SBR:CNTReportProgressWindow)
            RETURN 0
        END
        **CNT**

        cancel# = 0
        SBR.CancelPressed = 0
        accept
            Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?SBR:ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
            End!Case Event()
        End!accept
        If cancel# = 1
            Beep(Beep:SystemQuestion)  ;  Yield()
            Case Message('Do you want to finish off building the excel document with the data you have compile so far, or just quit now?','ServiceBase',|
                Icon:Question,'&Finish Report|&Quit Now|&Cancel',3)
            Of 1 ! &Finish Report Button
                SBR.CancelPressed = 1
                RETURN 2
            Of 2 ! &Quit Now Button
                SBR.CancelPressed = 2
                RETURN 1
            Of 3 ! &Cancel Button
            End!Case Message
        End!If cancel# = 1
        RETURN 0

SBR.EndProgress     PROCEDURE(<STRING fOpenFolder>)
    CODE
        IF (SELF.HideProgress = 1)
            RETURN
        END ! IF
        COMPILE('**CNT**',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            Clarionet:ClosePushWindow(SBR:CNTReportProgressWindow)
            RETURN
        END
        **CNT**
        SBR.ProgressThermometer = 100
        SBR.UserString = 'Finished...'
        DISPLAY()
        ?SBR:ProgressCancel{PROP:Hide} = 1
        ?SBR:Finish{prop:Hide} = 0
        IF (fOpenFolder <> '')
            ?SBR:Button:OpenReportFolder{PROP:Hide} = 0
        END

        ACCEPT
            CASE FIELD()
            OF ?SBR:Finish
                CASE EVENT()
                OF EVENT:Accepted
                    BREAK
                END
            OF ?SBR:Button:OpenReportFolder
                CASE EVENT()
                OF EVENT:Accepted
                    run('explorer.exe ' & fOpenFolder)
                END
            END
        END

        CLOSE(SBR:ReportProgressWindow)
        
SBR.AddToExport     PROCEDURE(<STRING fText>,<BYTE fStart>,<BYTE fEnd>)
locText                 STRING(1000)
    CODE
        IF fText <> ''
            locText = fText
        END

        IF (fStart = 1 AND fEnd = 1)
            sbrexp:ExpLine = '"' & CLIP(locText) & '"'
            ADD(SBR:ExportFile)
        ELSIF (fStart = 1)
            sbrexp:ExpLine = '"' & CLIP(locText)
        ELSIF (fEnd = 1)
            sbrexp:ExpLine = CLIP(sbrexp:ExpLine) & '","' & CLIP(locText) & '"'
            ADD(SBR:ExportFile)
        ELSIF (fStart = 0 AND fEnd = 0)
            sbrexp:ExpLine = CLIP(sbrexp:ExpLine) & '","' & CLIP(locText)
        END

SBR.FinishFormat    PROCEDURE(oiExcel fExcel,STRING fLastColumn,LONG fLastRow,<STRING fFreezeColumn>)
kLastSquare             CSTRING(20)
    CODE
        kLastSquare = fLastColumn & fLastRow
        fExcel.SetCellFontName('Tahoma','A1',kLastSquare)
        fExcel.SetCellFontSize(8,'A1',kLastSquare)
        fExcel.SelectCells('B2')
        IF (fFreezeColumn <> '')
            fExcel.SelectCells(fFreezeColumn)
            fExcel.FreezePanes()
        END
        fExcel.SetColumnWidth('A',fLastColumn)


SBR.LoadTempData    PROCEDURE(oiExcel fExcel,STRING fTempFileName,STRING fEndPoint)
locClipBoard            ANY
    CODE
        SELF.ClearClipboard()
        fExcel.OpenWorkBook(fTempFileName)
        fExcel.Copy('A1',fEndPoint)
        locClipBoard = CLIPBOARD()
        
        SELF.ClearClipboard()
        fExcel.CloseWorkBook(2)

        RETURN locClipboard

SBR.CopyFinalFile   PROCEDURE(STRING fTempFile,STRING fFinalFile)
rtnValue                BYTE(0)
    CODE
        REMOVE(CLIP(fFinalFile))
        LOOP UNTIL (NOT EXISTS(fFinalFile))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot save the file. Check that the file is not already open, and try again.'&|
                '|'&|
                '|' & Clip(fFinalFile) & '','ServiceBase',|
                Icon:Hand,'&Try Again|&Cancel',2)
            Of 1 ! &Try Again Button
                REMOVE(CLIP(fFinalFile))
            Of 2 ! &Cancel Button
                rtnValue = Level:Fatal
                BREAK
            End!Case Message
        END
        IF (rtnValue)
            RETURN rtnValue
        END
        RENAME(CLIP(fTempFile),CLIP(fFinalFIle))
        RETURN rtnValue

SBR.ClearClipboard  PROCEDURE(LONG fTimeOut=4)
locTimeOut  LONG()
locCurrentTime  TIME()
    CODE
        SETCURSOR(CURSOR:WAIT)
        locCurrentTime = CLOCK()
        SETCLIPBOARD('blank')
        LOOP
            IF (CLOCK() < locCurrentTime + (fTimeOut * 100))
                CYCLE
            END
            IF (CLIPBOARD() = 'blank')
                BREAK
            END

        END
        SETCURSOR()

!endregion


