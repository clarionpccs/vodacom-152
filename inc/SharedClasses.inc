    ! My Custom Classes
    SECTION('Declarations')
! Region Declarations
GenericExportFileName       CSTRING(255),STATIC
GenericExportFile   File,DRIVER('ASCII'),PRE(genexpfil),Name(GenericExportFileName),CREATE,BINDABLE,THREAD
RECORD                  RECORD
ExportLine                  STRING(8000)
                        END
                    END
ExcelReportingStatusQueue   QUEUE(),PRE(qsm)
StatusMessage                   STRING(100)
                            END

    COMPILE('***CNT***',ClarionetUsed = 1)
ERPProgressThermometerCNT           LONG,AUTO
ERPUserStringCNT                    CSTRING(100),AUTO
ExcelReportingProgressWindowCNT     WINDOW('Working...'),AT(,,307,46),CENTER,GRAY,IMM, |
                                        FONT('Tahoma',8,,FONT:Regular),DOUBLE
                                        PROGRESS,AT(3,20,301,15),USE(ERPProgressThermometerCNT),RANGE(0,100)
                                        STRING(@s100),AT(3,5,301,10),USE(ERPUserStringCNT),CENTER
                                    END
! ***CNT***
ExcelReportingProgressWindow        WINDOW('Progress...'),AT(0,0,680,428),GRAY,IMM, |
                                        ICON('Cellular3g.ico'),FONT('Tahoma',8,,FONT:Regular,CHARSET:ANSI), |
                                        TIMER(1),COLOR(00D6EAEFh),WALLPAPER('sbback.jpg'),Tiled,DOUBLE
                                        PANEL,AT(164,68,352,12),USE(?ERPProgPanel),FILL(009A6A7Ch)
                                        LIST,AT(208,88,268,206),USE(?ERPList),FONT(,,00010101h,,CHARSET:ANSI), |
                                            FORMAT('20L(2)|M'),COLOR(COLOR:White,00010101h,COLOR:White),VSCROLL, |
                                            GRID(COLOR:WHITE),FROM(ExcelReportingStatusQueue)
                                        PANEL,AT(164,84,352,246),USE(?ERPProgPanel2),FILL(009A6A7Ch)
                                        PROGRESS,AT(206,314,268,12),USE(?ERPProgressThermometer),RANGE(0,100)
                                        STRING(''),AT(259,300,161,10),USE(?ERPUserString),FONT('Tahoma',8, |
                                            COLOR:White,FONT:Bold),TRN,COLOR(009A6A7Ch),CENTER
                                        PANEL,AT(164,332,352,28),USE(?ERPProgPanel3),FILL(009A6A7Ch)
                                        PROMPT('Report Progress'),AT(168,70),USE(?ERPWindowTitle),FONT('Tahoma', |
                                            8,COLOR:White,FONT:Bold,CHARSET:ANSI),TRN
                                        BUTTON,AT(444,332),USE(?ERPbtnProgressCancel),ICON('cancelp.jpg'),FLAT,LEFT,TRN
                                        BUTTON,AT(376,332),USE(?ERPbtnOpenReportFolder),ICON('openrepp.jpg'),HIDE, |
                                            FLAT,TRN
                                        BUTTON,AT(444,332),USE(?ERPbtnReportingFinish),ICON('finishp.jpg'),HIDE,FLAT,TRN
                                    END
MyExportClass       CLASS,TYPE
Delimeter               STRING(1),PRIVATE
UseQuotes               BYTE(0),PRIVATE
OpenDataFile            PROCEDURE(STRING fFilename,<STRING fDel>,BYTE = 0),BYTE
AddField                PROCEDURE(<STRING fField>,<BYTE fFirstField>,<BYTE fLastField>,BYTE=1)
CloseDataFile           PROCEDURE()
RecordsToProcess        LONG,PRIVATE
SkipRecords             LONG,PRIVATE
RecordsProcessed        LONG,PRIVATE
ProgressThermometer     LONG,PRIVATE
PercentProgress         LONG,PRIVATE
StartDate               DATE
StartTime               TIME
CancelPressed           BYTE
SkipCount               LONG,PRIVATE
HideProgress            LONG    
OpenProgressWindow      PROCEDURE(LONG = 0,LONG = 100,LONG pHideProgress = 0)
UpdateProgressText      PROCEDURE(<STRING fText>,<BYTE fSkipClarionet>,LONG pNoLineFeed=0)
UpdateProgressWindow    PROCEDURE(<BYTE fNoCount>),BYTE
FinishProgress          PROCEDURE(<STRING fOpenFolder>)
FinishFormat            PROCEDURE(oiExcel fExcel,STRING fLastColumn,LONG fLastRow,<STRING fFreezeColumn>)
ClearClipboard          PROCEDURE(LONG fTimeOut=4)
CopyDataFileToClipboard PROCEDURE(oiExcel fExcel,STRING fEndPoint,STRING fTempFileName,BYTE = 0),ANY
CopyFinalFile           PROCEDURE(STRING fTempFile,STRING fFinalFile),BYTE,PROC
                    END
! endregion
    SECTION('Procedures')
! region Procedures
!!! <summary>
!!! Procedure to initalize an ASCII export
!!! </summary>
!!! <param name="fFilename">Name of the export file</param>
!!! <param name="fDel">String(@s1) containing the field delimiter</param>
!!! <remarks>
!!! This procedure will remove the existing, and create a new file. It will return a value if an error occurs.
!!! </remarks>
myExportClass.OpenDataFile  PROCEDURE(STRING fFilename,<STRING fDel>,BYTE fQuotes)
    CODE
        REMOVE(fFilename)
        IF (EXISTS(fFilename))
            RETURN Level:Fatal
        END

        GenericExportFilename = fFilename
        IF (fDel = '')
            SELF.Delimeter = ','
        ELSE
            SELF.Delimeter = fDel
        END

        SELF.UseQuotes = fQuotes

        CREATE(GenericExportFile)
        OPEN(GenericExportFile)
        IF (ERROR())
            RETURN Level:Fatal
        END
        RETURN Level:Benign

!!! <summary>
!!! Procedure to close the open ASCII file.
!!! </summary>
myExportClass.CloseDataFile  PROCEDURE()
    CODE
        CLOSE(GenericExportFile)

!!! <summary>
!!! Procedure to populate an export line
!!! </summary>
!!! <param name="fField">String to popuplate</param>
!!! <param name="fFirstField">(Optional) To signify the first record</param>
!!! <param name="fLastField">(Optional) To signify the last record and ADD to the file</param>
!!! <param name="fMultiple">(Optional) Add the same field a number of times</param>
myExportClass.AddField      PROCEDURE(<STRING fField>,<BYTE fFirstField>,<BYTE fLastField>,BYTE fMultiple)
locField    CSTRING(100)
    CODE
        IF (fField <> '')
            locField = fField
        ELSE
            locField = ''
        END
        IF (fFirstField = 1 AND fLastField = 0)
            Clear(genexpfil:Record)
            IF (SELF.UseQuotes)
                genexpfil:ExportLine = '"' & CLIP(locField)
            ELSE
                genexpfil:ExportLine = locField
            END
        ELSIF (fLastField = 1 AND fFirstField = 0)
            IF (SELF.UseQuotes)
                genexpfil:ExportLine = CLIP(genexpfil:ExportLine) & '"' & CLIP(SELF.Delimeter) & '"' & CLIP(locField) & '"'
            ELSE
                genexpfil:ExportLine = CLIP(genexpfil:ExportLine) & SELF.Delimeter & locField
            END
            ADD(GenericExportFile)
        ELSIF (fFirstField = 1 AND fLastField = 1)
            Clear(genexpfil:Record)
            IF (SELF.UseQuotes)
                genexpfil:ExportLine = '"' & CLIP(locField) & '"'
            ELSE
                genexpfil:ExportLine = locField
            END
            ADD(GenericExportFile)
        ELSE
            LOOP mm# = 1 TO fMultiple
                IF (SELF.UseQuotes)
                    genexpfil:ExportLine = CLIP(genexpfil:ExportLine) & '"' & CLIP(SELF.Delimeter) & '"' & locField
                ELSE
                    genexpfil:ExportLine = CLIP(genexpfil:ExportLine) & SELF.Delimeter & locField
                END
            END
        END
!!! <summary>
!!! Open A ServiceBase Report Progress Window
!!! </summary>
!!! <param name="fSkipRecords">(Optional) Don't update the screen for every record</param>
!!! <param name="fRecords">(Optional) Specify the total number of records for an accurate progress bar</param>
MyExportClass.OpenProgressWindow    PROCEDURE(LONG fSkipRecords,LONG fRecords,LONG pHideProgress)
retValue                                BYTE(),AUTO
    CODE
    
        SELF.HideProgress = pHideProgress
        
        IF (SELF.HideProgress = 1)
            RETURN
        END !  IF
        FREE(ExcelReportingStatusQueue)

    ! Initialise Progress Variables
        SELF.RecordsToProcess = fRecords
        SELF.SkipRecords = fSkipRecords
        SELF.RecordsProcessed = 0
        SELF.ProgressThermometer = 0
        SELF.StartDate = TODAY()
        SELF.StartTime = CLOCK()
        SELF.CancelPressed = 0
        

        COMPILE('***CNT***',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            Clarionet:OpenPushWindow(ExcelReportingProgressWindowCNT)
            RETURN
        END
        ! ***CNT***

!!! <summary>
!!! Update the Progress Queue Of A ServiceBase Excel Report
!!! </summary>
!!! <param name="fText">The text to display</param>
!!! <param name="fSkipClarionet">(Optional) Do not display the text if running via Clarionet</param>
MyExportClass.UpdateProgressText    PROCEDURE(<STRING fText>,<BYTE fSkipClarionet>,LONG pNoLineFeed=0)
locText                                 STRING(100)
    CODE
        IF (SELF.HideProgress = 1)
            RETURN
        END ! IF
        IF (fText <> '')
            locText = fText
        END

        COMPILE('***CNT***',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            IF (fSkipClarionet = 1)
                RETURN
            END
            ERPUserStringCNT = CLIP(locText)
            Clarionet:UpdatePushWindow(ExcelReportingProgressWindowCNT)
            RETURN
        END

        ! ***CNT***
        IF (pNoLineFeed = 1 AND |
            LEN(CLIP(ExcelReportingStatusQueue) & CLIP(locText)) < 100)
            ! Post on one line, unless it's longer than the width
            GET(ExcelReportingStatusQueue,RECORDS(ExcelReportingStatusQueue))
            ExcelReportingStatusQueue.StatusMessage = CLIP(ExcelReportingStatusQueue) & CLIP(locText)
            PUT(ExcelReportingStatusQueue)
        ELSE
            ExcelReportingStatusQueue.StatusMessage = CLIP(locText)
            ADD(ExcelReportingStatusQueue)
        END ! IF
        SELECT(?ERPList,RECORDS(ExcelReportingStatusQueue))
        DISPLAY()
        RETURN
!!! <summary>
!!! Update the progress bar of a ServiceBase Excel Report. Check for cancel being pressed
!!! </summary>
!!! <param name="fNoCount">(Optional) Do not increase the program bar, just check for cancel being pressed</param>
MyExportClass.UpdateProgressWindow  PROCEDURE(<BYTE fNoCount>)
cancelled                               BYTE,AUTO
    CODE
        IF (SELF.HidePRogress = 1)
            RETURN 0
        END!  IF
        SELF.SkipCount += 1
        IF (SELF.SkipRecords > 0)
            IF (SELF.SkipCount > SELF.SkipRecords)
                SELF.SkipCount = 0
            ELSE
                RETURN 0
            END
        END

        IF (fNoCount <> 1)
           ! Update window, but dont increase counter

            SELF.RecordsProcessed += 1
            IF (SELF.PercentProgress < 100)
                SELF.PercentProgress = (SELF.RecordsProcessed / SELF.RecordsToProcess) * 100
                IF (SELF.PercentProgress => 100)
                    SELF.RecordsProcessed = 0
                    SELF.PercentProgress = 0
                    SELF.ProgressThermometer = 0
                END
                IF (SELF.PercentProgress <> SELF.ProgressThermometer)
                    SELF.ProgressThermometer = SELF.PercentProgress
                END
            END
        END

        COMPILE('***CNT***',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            ERPProgressThermometerCNT = SELF.ProgressThermometer
            Clarionet:UpdatePushWindow(ExcelReportingProgressWindowCNT)
            RETURN 0
        END
        ! ***CNT***

        ?ERPProgressThermometer{PROP:Progress} = SELF.ProgressThermometer

        cancelled = 0
        SELF.CancelPressed = 0
        ACCEPT
            CASE EVENT()
            OF EVENT:Timer
                BREAK
            OF EVENT:CloseWindow
                cancelled = 1
                BREAK
            OF EVENT:Accepted
                YIELD()
                IF (FIELD() = ?ERPbtnProgressCancel)
                    cancelled = 1
                    BREAK
                END
            END
        END

        IF (cancelled)
            Beep(Beep:SystemQuestion)  ;  Yield()
            Case Message('Do you want to finish off building the excel document with the data you have compile so far, or just quit now?','ServiceBase',|
                Icon:Question,'&Finish Report|&Quit Now|&Cancel',3)
            Of 1 ! &Finish Report Button
                SELF.CancelPressed = 1
                RETURN 2
            Of 2 ! &Quit Now Button
                SELF.CancelPressed = 2
                RETURN 1
            Of 3 ! &Cancel Button
            End!Case Message
        END

        RETURN 0

!!! <summary>
!!! ServiceBase Excel Report: Format the excel document
!!! </summary>
!!! <param name="fExcel">Office Inside Excel Classname</param>
!!! <param name="fLastColumn">The Last Column Of The Report</param>
!!! <param name="fLastRow">The Last Row Of The Report</param>
!!! <param name="fFreezeColumn">(Optional) Set Cell To Place A Freeze Panes</param>
MyExportClass.FinishFormat  PROCEDURE(oiExcel fExcel,STRING fLastColumn,LONG fLastRow,<STRING fFreezeColumn>)
lastSquare                      CSTRING(20),AUTO
    CODE
        IF (SELF.HideProgress = 1)
            RETURN
        END ! IF    
        IF fExcel &= NULL
            STOP('Attempting To Use Excel Object')
            RETURN
        END ! IF NOT p_Web &= NULL

        lastSquare = fLastColumn & fLastRow
        fExcel.SetCellFontName('Tahoma','A1',lastSquare)
        fExcel.SetCellFontSize(8,'A1',lastSquare)
        fExcel.SelectCells('B2')
        IF (fFreezeColumn <> '')
            fExcel.SelectCells(fFreezeColumn)
            fExcel.FreezePanes()
        END
        fExcel.SetColumnWidth('A',fLastColumn)

!!! <summary>
!!! ServiceBase Excel Report: Copy the data from a csv file into an Excel File
!!! </summary>
!!! <param name="fExcel">Office Inside Excel Classname</param>
!!! <param name="fEndPoint">The Last Column/Row Of The Report</param>
!!! <param name="fTempFileName">The csv filename</param>
!!! <param name="fDebug">Stop Before Closing Excel (only useful is ShowExcel is turned on)</param>
MyExportClass.CopyDataFileToClipboard       PROCEDURE(oiExcel fExcel,STRING fEndPoint,STRING fTempFileName,BYTE fDebug)
tempClipBoard                                   ANY
tempFilename                                    STRING(255)

    CODE
      
        IF fExcel &= NULL
            STOP('Attempting To Use Excel Object')
            RETURN ''
        END ! IF NOT p_Web &= NULL
        !SELF.ClearClipboard()

        tempFilename = fTempFileName

        fExcel.OpenWorkBook(tempFilename)
        fExcel.Copy('A1',fEndPoint)

        tempClipBoard = CLIPBOARD()
        !SELF.ClearClipboard()
        fExcel.Copy('A1')
        IF (fDebug = 1)
            Message('Waiting....')
        END
        fExcel.CloseWorkbook(2)
        RETURN tempClipBoard

MyExportClass.ClearClipboard        PROCEDURE(LONG fTimeOut=10)
currentTime                             TIME(),AUTO
    CODE
        SETCURSOR(CURSOR:Wait)
        currentTime = CLOCK()
        SETCLIPBOARD('')
        LOOP
            IF (CLOCK() > currentTime + (fTimeOut * 100))
                BREAK
            END
            IF (LEN(CLIPBOARD()) < 1)
                BREAK
            END
        END
        SETCURSOR()


MyExportClass.CopyFinalFile PROCEDURE(STRING fTempFile,STRING fFinalFile)
rtnValue                        BYTE(0)
    CODE
        REMOVE(CLIP(fFinalFile))
        LOOP UNTIL (NOT EXISTS(fFinalFile))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot save the file. Check that the file is not already open, and try again.'&|
                '|'&|
                '|' & Clip(fFinalFile) & '','ServiceBase',|
                Icon:Hand,'&Try Again|&Cancel',2)
            Of 1 ! &Try Again Button
                REMOVE(CLIP(fFinalFile))
            Of 2 ! &Cancel Button
                rtnValue = Level:Fatal
                BREAK
            End!Case Message
        END
        IF (rtnValue)
            RETURN rtnValue
        END
        RENAME(CLIP(fTempFile),CLIP(fFinalFIle))
        RETURN rtnValue

!!! <summary>
!!! ServiceBase Excel Report: Finish Off Progress And Show Finish And Open Buttons
!!! </summary>
!!! <param name="fOpenFolder">(Optional) If popuplated, then a Open Folder button will appear/param>
MyExportClass.FinishProgress        PROCEDURE(<STRING fOpenFolder>)
    CODE
        IF (SELF.HideProgress = 1)
            RETURN
        END ! IF    
        COMPILE('***CNT***',ClarionetUsed = 1)
        IF (ClarionetServer:Active())
            Clarionet:ClosePushWindow(ExcelReportingProgressWindowCNT)
            RETURN
        END
        ! ***CNT***

        ?ERPProgressThermometer{PROP:Progress} = 100
        ?ERPUserString{PROP:Text} = 'Finished...'
        DISPLAY()
        ?ERPbtnProgressCancel{PROP:Hide} = 1
        ?ERPbtnReportingFinish{PROP:Hide} = 0
        IF (fOpenFolder <> '')
            ?ERPbtnOpenReportFolder{PROP:Hide} = 0
        END

        ACCEPT
            CASE FIELD()
            OF ?ERPbtnReportingFinish
                CASE EVENT()
                OF EVENT:Accepted
                    BREAK
                END
            OF ?ERPbtnOpenReportFolder
                CASE EVENT()
                OF EVENT:Accepted
                    RUN('EXPLORER.EXE ' & CLIP(fOpenFolder))
                END
            END
        END

        CLOSE(ExcelReportingProgressWindow)
! endregion
