

   MEMBER('celrapeo.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


OracleDefaults PROCEDURE                              !Generated from procedure template - Window

tmp:ExportPath       STRING(255)
window               WINDOW('Oracle Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Oracle Defaults'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Export Path'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(248,204,152,10),USE(tmp:ExportPath),FONT(,,,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(404,199),USE(?LookupExportPath),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup5          SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020568'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('OracleDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  tmp:ExportPath = GETINI('ORACLE','ExportPath',,CLIP(PATH())&'\SB2KDEF.INI')
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','OracleDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup5.Init
  FileLookup5.Flags=BOR(FileLookup5.Flags,FILE:LongName)
  FileLookup5.Flags=BOR(FileLookup5.Flags,FILE:Directory)
  FileLookup5.SetMask('All Files','*.*')
  FileLookup5.WindowTitle='Export Path'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','OracleDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020568'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020568'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020568'&'0')
      ***
    OF ?LookupExportPath
      ThisWindow.Update
      tmp:ExportPath = Upper(FileLookup5.Ask(1)  )
      DISPLAY
    OF ?OK
      ThisWindow.Update
      PUTINI('ORACLE','ExportPath',tmp:ExportPath,CLIP(PATH()) & '\SB2KDEF.INI')
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Main PROCEDURE                                        !Generated from procedure template - Window

LocalRequest         LONG
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AppFrame             WINDOW('Oracle Export - ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       MENUBAR
                         ITEM('Defaults'),USE(?Defaults)
                       END
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,1),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,163,192,92),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Oracle Exports'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(368,258),USE(?Button6),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(360,197),USE(?WaybillGeneration:2),TRN,FLAT,LEFT,ICON('spaexpp.jpg')
                       BUTTON,AT(256,197),USE(?WaybillGeneration),TRN,FLAT,LEFT,ICON('finexpp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.open
        access:users.usefile
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
        access:users.close
    End
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020563'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, AppFrame, 1)  !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  !Look for a redirection file
  !set path and update procedure
  g_path = GETINI('global','datapath','xx7fail8zz', clip(path()) & '\celweb.ini')
  !message('g_path is ' & clip(g_path))
  if clip(g_path)='xx7fail8zz' then
      !not using an ini file - let it go onto own checks
  ELSE
      setpath (clip(g_path))
  end
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:LOGGED.Open
  Relate:TRADEACC.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  if clip(ClarioNET:Global.Param4) = 'NO' then
      !Makeover off:
      !ThisMakeOver.ColorDepth=0
  End
  OPEN(AppFrame)
  SELF.Opened=True
      ! Close splash (DBH: 24-05-2005)
      PUTINI('STARTING','Run',True,)
  ! Save Window Name
   AddToLog('Window','Open','Main')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  error# = 1
  if GLO:webJob then
      !glo:Password was set on enty to the passed variable - LogonUserPassword
      access:users.clearkey(use:password_key)
      use:password = glo:Password
      if access:users.fetch(use:password_key) = level:benign then error#=0.
      If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() then error#=1.
      
      !check the traders details
      a#=instring(',',ClarioNET:Global.Param2,1,1)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = clip(ClarioNET:Global.Param2[1:a#-1])
      if not access:tradeacc.fetch(tra:account_number_key)
          If Clip(tra:password)<> ClarioNET:Global.Param2[a#+1:len(clip(ClarioNET:Global.Param2))]
              halt(0,'Incorrect Trade details')
          END
          !glo:Default_Site_Location = tra:SiteLocation
      end
      !Send back the version number to the client
      set(defaultv)
      if access:defaultv.next()
          !error  - this is handled elsewhere ignored here
      ELSE
          !pass current version number back to client
          ClarioNET:CallClientProcedure('VERSIONSAVE',defv:VersionNumber)
      END
  
  ELSE
      Loop x# = 1 To Len(Clip(Command()))
          If Sub(Command(),x#,1) = '%'
              glo:Password = Clip(Sub(Command(),x#+1,30))
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Found
                  error# = 0
                  If use:RenewPassword <> 0 
                      If use:PasswordLastChanged = ''
                          use:PasswordLastChanged = Today()
                          Access:USERS.Update()
                      Else !If use:PasswordLastChanged = ''
                          If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() 
                              error#=1
                          End
                      End
                  End !If use:RenewPassword <> 0
              Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              
              Break
          End!If Sub(Command(),x#,1) = '%'
      End!Loop x# = 1 To Len(Comman())
  END !if GLO:WebJob
  
  If error# = 1
      Do Login
  End
  
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Do log_out
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:LOGGED.Close
    Relate:TRADEACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Main')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button6
      if glo:webJob
          ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
          !Now log out
          access:users.clearkey(use:password_key)
          use:password = glo:password
          if access:users.fetch(use:password_key) = Level:Benign
              access:logged.clearkey(log:user_code_key)
              log:user_code = use:user_code
              log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
              if access:logged.fetch(log:user_code_key) = Level:Benign
                  delete(logged)
              end !if
          end !if
      end
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Defaults
      ThisWindow.Update
      OracleDefaults
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020563'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020563'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020563'&'0')
      ***
    OF ?Button6
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?WaybillGeneration:2
      ThisWindow.Update
      BrowseSparesFile
      ThisWindow.Reset
    OF ?WaybillGeneration
      ThisWindow.Update
      BrowseOracleFile
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Do Log_out
      Do Login
    OF EVENT:OpenWindow
         AppFrame{Prop:Active}=1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

CreateExport PROCEDURE                                !Generated from procedure template - Window

save_inv_id          USHORT,AUTO
Local                CLASS
SentToHub            Procedure(),Byte
ExportJobs           Procedure(String func:Type,String func:Status)
                     END
save_job_id          USHORT,AUTO
save_jobe_id         USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_man_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
savepath             STRING(255)
pos                  STRING(255)
tmp:InvoiceDate      DATE
tmp:OracleExport     STRING(255),STATIC
tmp:ChaAverageTotal  REAL
tmp:ChaPurchaseTotal REAL
tmp:WarOutWarrantyTotal REAL
tmp:WarSaleTotal     REAL
tmp:InvoiceType      STRING(1)
tmp:ExportPath       STRING(255)
tmp:WarCostAdjustment REAL
tmp:OracleNumber     LONG
tmp:ChaARCAverageTotal REAL
tmp:WarAverageTotal  REAL
tmp:WarRRCAverageTotal REAL
TrailerGroup         GROUP,PRE(trail)
Count                LONG
HandlingFee          REAL
HandlingFeeVAT       REAL
RRCCLabour           REAL
RRCCLabourVAT        REAL
RRCCPartsCost        REAL
RRCCPartsSale        REAL
RRCCPartsSaleVAT     REAL
RRCCTotal            REAL
ARCCLabour           REAL
ARCCLabourVAT        REAL
ARCCPartsCost        REAL
ARCCPartsSale        REAL
ARCCPartsSaleVAT     REAL
ARCCTotal            REAL
RRCWLabour           REAL
RRCWLabourVAT        REAL
RRCWPartsCost        REAL
RRCWPartsSale        REAL
RRCWPartsSaleVAT     REAL
ARCWLabour           REAL
ARCWLabourVAT        REAL
ARCWPartsCost        REAL
ARCWPartsSale        REAL
ARCWPartsSaleVAT     REAL
ARCWManufactLabour   REAL
ARCWManufactLabourVAT REAL
ARCWManufactParts    REAL
ARCWManufactPartsVAT REAL
ARC3rdParty          REAL
ARC3rdPartyVAT       REAL
                     END
tmp:Total            REAL,DIM(40)
tmp:AccountNumber    STRING(30)
tmp:HeadAccount      STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:WarARCPurchaseTotal REAL
tmp:ARCLocation      STRING(30)
AppFrame             WINDOW('Oracle Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Oracle Export'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(225,153),USE(?tmp:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(301,153,124,10),USE(tmp:AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Account Number'),TIP('Account Number'),REQ,UPR
                           BUTTON,AT(429,148),USE(?LookupAccountNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Start Date'),AT(225,175),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(301,175,64,10),USE(tmp:StartDate),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),UPR,READONLY
                           BUTTON,AT(369,169),USE(?LookupStartDate),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(225,193),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(301,193,64,10),USE(tmp:EndDate),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('End Date'),TIP('End Date'),UPR,READONLY
                           BUTTON,AT(369,191),USE(?LookupEndDate),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('An Oracle Export File will now be created in the following folder:'),AT(223,217),USE(?Prompt1),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s255),AT(194,231,292,12),USE(tmp:OracleExport),CENTER,FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(376,332),USE(?OK),TRN,FLAT,LEFT,ICON('creexpp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
OracleExport    File,Driver('ASCII'),Pre(orcexp),Name(tmp:OracleExport),Create,Bindable,Thread
Record              Record
Line1               String(4000)
                    End
                End

!OracleExport    File,Driver('BASIC', '/FIELDDELIMITER = 1,124'),Pre(orcexp),Name(tmp:OracleExport),Create,Bindable,Thread
!Record                  Record
!JobNumber               String(30)
!BatchNumber             String(30)
!BatchReferenceNumber    String(30)
!SiteBranchCode          String(30)
!WebJobNumber            String(30)
!GenericAccount          String(30)
!HeadAccountNumber       String(30)
!SubAccountNumber        String(30)
!SubSubAccountNumber     String(30)
!TransactionType         String(30)
!InvoiceTo               String(30)
!CustomerOrderNumber     String(30)
!DateCompleted           String(30)
!InvoiceNumber           String(30)
!InvoiceDate             String(30)
!CreditNoteNumber        String(30)
!CreditNoteDate          String(30)
!ChargeableJob           String(30)
!ChaChargeType           String(30)
!ChaRepairType           String(30)
!WarrantyJob             String(30)
!WarChargeType           String(30)
!WarRepairType           String(30)
!Manufacturer            String(30)
!WarrantyClaimStatus     String(30)
!WarrantyClaimStatusDate String(30)
!BookingOrigin           String(30)
!Location                String(30)
!!RRC Costs
!RRCHandlingFee          String(30)
!RRCHandlingFeeVAT       String(30)
!RRCExchangeFee          String(30)
!RRCExchangeFeeVAT       String(30)
!RRCChaLabourCost        String(30)
!RRCChaLabourCostVAT     String(30)
!RRCChaPartsCost         String(30)
!RRCChaPartsSaleCost     String(30)
!RRCChaPartsSaleVAT      String(30)
!RRCChaTotal             String(30)
!RRCChaLostLoanCharge    String(30)
!RRCChaLostLoanChargeVAT String(30)
!!==
!!ARC Costs
!ARCChaLabourCost        String(30)
!ARCChaLabourCostVAT     String(30)
!ARCChaPartsCost         String(30)
!ARCChaPartsSaleCost     String(30)
!ARCChaPartsSaleVAT      String(30)
!ARCChaTotal             String(30)
!!==
!!RRC Costs
!RRCWarLabourCost        String(30)
!RRCWarLabourCostVat     String(30)
!RRCWarPartsCost         String(30)
!RRCWarPartsSaleCost     String(30)
!RRCWarPartsSaleVAT      String(30)
!!==
!!ARC Costs
!ARCWarExchangeFee       String(30)
!ARCWarExchangeFeeVAT    String(30)
!ARCWarLabourCost        String(30)
!ARCWarLabourCostVAT     String(30)
!ARCWarPartsCost         String(30)
!ARCWarPartsSaleCost     String(30)
!ARCWarPartsSaleVAT      String(30)
!ARCManufacturerPaid     String(30)
!ARCManufacturerPaidVAT  String(30)
!ARCManufacturerParts    String(30)
!ARCManufacturerPartsVAT String(30)
!ARCChaLostLoanCharge    String(30)
!ARCChaLostLoanChargeVAT String(30)
!WarrantyClaimDate       String(30)
!ARCThirdPartyCost       String(30)
!ARCThirdPartyCostVAT    String(30)
!ARCThirdPartyInvoiceNo  String(30)
!ThirdPartySite          String(30)
!                        End
!                    End
OracleHeadExport    File,Driver('BASIC', '/FIELDDELIMITER = 1,124'),Pre(orchexp),Name(tmp:OracleExport),Create,Bindable,Thread
Record                  Record
JobNumber               String(30) !1
BatchNumber             String(30)
BatchReferenceNumber    String(30)
SiteBranchCode          String(30)
WebJobNumber            String(30)
GenericAccount          String(30)
HeadAccountNumber       String(30)
SubAccountNumber        String(30)
SubSubAccountNumber     String(30)
TransactionType         String(30) !10
InvoiceTo               String(30)
CustomerOrderNumber     String(30)
DateCompleted           String(30)
InvoiceNumber           String(30)
InvoiceDate             String(30)
CreditNoteNumber        String(30)
CreditNoteDate          String(30)
ChargeableJob           String(30)
ChaChargeType           String(30)
ChaRepairType           String(30) !20
WarrantyJob             String(30)
WarChargeType           String(30)
WarRepairType           String(30)
Manufacturer            String(30)
WarrantyClaimStatus     String(30)
WarrantyClaimStatusDate String(30)
BookingOrigin           String(30)
Location                String(30)
!RRC Costs
RRCHandlingFee          String(30)
RRCHandlingFeeVAT       String(30) !30
RRCChaLabourCost        String(30)
RRCChaLabourCostVAT     String(30)
RRCChaPartsCost         String(30)
RRCChaPartsSaleCost     String(30)
RRCChaPartsSaleVAT      String(30)
RRCChaTotal             String(30)
!==
!ARC Costs
ARCChaLabourCost        String(30)
ARCChaLabourCostVAT     String(30)
ARCChaPartsCost         String(30)
ARCChaPartsSaleCost     String(30) !40
ARCChaPartsSaleVAT      String(30)
ARCChaTotal             String(30)
ARCChaLostLoanCharge    String(30)
ARCChaLostLoanChargeVAT String(30)
RRCExchangeFee          String(30)
RRCExchangeFeeVAT       String(30)
!==
!RRC Costs
RRCWarLabourCost        String(30)
RRCWarLabourCostVat     String(30)
RRCWarPartsCost         String(30)
RRCWarPartsSaleCost     String(30) !50
RRCWarPartsSaleVAT      String(30)
!==
!ARC Costs
ARCWarLabourCost        String(30)
ARCWarLabourCostVAT     String(30)
ARCWarPartsCost         String(30)
ARCWarPartsSaleCost     String(30)
ARCWarPartsSaleVAT      String(30)
ARCManufacturerPaid     String(30)
ARCManufacturerPaidVAT  String(30)
ARCManufacturerParts    String(30)
ARCManufacturerPartsVAT String(30) !60
WarrantyClaimDate       String(30)
ARCThirdPartyCost       String(30)
ARCThirdPartyCostVAT    String(30)
ARCThirdPartyInvoiceNo  String(30)
ThirdPartySite          String(30)
RefurbLabourCost        String(30)
RefurbCostVAT           String(30)
RefurbPartsCost         String(30)
RefurbPartsSaleCost     String(30)
RefurbPartsSaleVAT      String(30) !70
                        End
                    End

OracleRRCExport    File,Driver('BASIC', '/FIELDDELIMITER = 1,124'),Pre(orcrexp),Name(tmp:OracleExport),Create,Bindable,Thread
Record                  Record
JobNumber               String(30) !1
BatchNumber             String(30)
BatchReferenceNumber    String(30)
SiteBranchCode          String(30)
WebJobNumber            String(30)
GenericAccount          String(30)
HeadAccountNumber       String(30)
SubAccountNumber        String(30)
SubSubAccountNumber     String(30)
TransactionType         String(30) !10
InvoiceTo               String(30)
CustomerOrderNumber     String(30)
DateCompleted           String(30)
InvoiceNumber           String(30)
InvoiceDate             String(30)
CreditNoteNumber        String(30)
CreditNoteDate          String(30)
ChargeableJob           String(30)
ChaChargeType           String(30)
ChaRepairType           String(30) !20
WarrantyJob             String(30)
WarChargeType           String(30)
WarRepairType           String(30)
Manufacturer            String(30)
WarrantyClaimStatus     String(30)
WarrantyClaimStatusDate String(30)
BookingOrigin           String(30)
Location                String(30)
!RRC Costs
RRCHandlingFee          String(30)
RRCHandlingFeeVAT       String(30) !30
RRCChaLabourCost        String(30)
RRCChaLabourCostVAT     String(30)
RRCChaPartsCost         String(30)
RRCChaPartsSaleCost     String(30)
RRCChaPartsSaleVAT      String(30)
RRCChaTotal             String(30)
RRCChaTotalVAT          String(30)
RRCLostLoanCharge       String(30)
RRCLostLoanChargeVAT    String(30)
LoanLoanToRRCCharge     String(30) !40
LoanLoanToRRCChargeVAT  String(30)
ManufacturerExchangeFee String(30)
ManufacturerExchangeFeeVAT String(30)
RRCExchangeFee          String(30)
RRCExchangeFeeVAT       String(30)
!==
!RRC Costs
RRCWarLabourCost        String(30)
RRCWarLabourCostVat     String(30)
RRCWarPartsCost         String(30)
RRCWarPartsSaleCost     String(30) 
RRCWarPartsSaleVAT      String(30) !50
RRCWarTotal             String(30)
RRCWarTotalVAT          String(30)
!==
!ARC Costs
WarrantyClaimDate       String(30)
ARCThirdPartyCost       String(30)
ARCThirdPartyCostVAT    String(30)
ARCThirdPartyMarkup     String(30)
ARCThirdPartyMarkupVAT  String(30)
ARCThirdPartyTotal      String(30)
ARCThirdPartyTotalVAT   String(30)
ARCThirdPartyInvoiceNo  String(30) !60
ThirdPartySite          String(30)
                        End
                    End

!Save Entry Fields Incase Of Lookup
look:tmp:AccountNumber                Like(tmp:AccountNumber)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

HeaderLine      Routine
    Clear(orcexp:Record)
    orcexp:Line1            = '"HEADER","' & Clip(tmp:OracleNumber) & '","' & Format(today(),@d06) & '"'
!    orcexp:JobNumber        = 'HEADER'
!    orcexp:SiteBranchCode   = tmp:OracleNumber
!    orcexp:WebJobNumber     = Format(Today(),@d06)
    Add(OracleExport)
FooterLine      Routine
    Clear(orcexp:Record)
    orcexp:Line1    = '"TRAILER","' & Clip(trail:Count)
    If tmp:HeadAccount = 'HEAD'
        Loop x# = 3 To 28
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End
        Loop x# = 1 To 32
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(Round(tmp:Total[x#],.02),@n14.2))
        End !Loop x# = 1 To 32
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(Round(tmp:Total[33],.02),@n14.2))
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(Round(tmp:Total[34],.02),@n14.2))
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        Loop x# = 35 to 39
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(Round(tmp:Total[x#],.02),@n14.2))
        End !Loop x# = 35 to 39
    Else !If tmp:HeadAccount = 'HEAD'
        Loop x# = 3 To 28
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End
        Loop x# = 1 To 24
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(Round(tmp:Total[x#],.02),@n14.2))
        End !Loop x# = 1 To 32
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        Loop x# = 25 to 30
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(Round(tmp:Total[x#],.02),@n14.2))
        End !Loop x# = 35 to 39

    End !If tmp:HeadAccount = 'HEAD'
    Add(OracleExport)
!    Clear(orcexp:Record)
!    orcexp:JobNumber                = 'TRAILER'
!    orcexp:SiteBranchCode           = trail:Count
!    !-=-                            =
!    orcexp:RRCHandlingFee           = trail:HandlingFee
!    orcexp:RRCHandlingFeeVAT        = trail:HandlingFeeVAT
!    orcexp:RRCChaLabourCost         = trail:RRCCLabour
!    orcexp:RRCChaLabourCostVAT      = trail:RRCCLabourVAT
!    orcexp:RRCChaPartsCost          = trail:RRCCPartsCost
!    orcexp:RRCChaPartsSaleCost      = trail:RRCCPartsSale
!    orcexp:RRCChaPartsSaleVAT       = trail:RRCCPartsSaleVAT
!    orcexp:RRCChaTotal              = trail:RRCCTotal
!    !==
!    !ARC Costs
!    orcexp:ARCChaLabourCost         = trail:ARCCLabour
!    orcexp:ARCChaLabourCostVAT      = trail:ARCCLabourVAT
!    orcexp:ARCChaPartsCost          = trail:ARCCPartsCost
!    orcexp:ARCChaPartsSaleCost      = trail:ARCCPartsSale
!    orcexp:ARCChaPartsSaleVAT       = trail:ARCCPartsSaleVAT
!    orcexp:ARCChaTotal              = trail:ARCCTotal
!
!    !==
!    !RRC Costs
!    orcexp:RRCWarLabourCost         = trail:RRCWLabour
!    orcexp:RRCWarLabourCostVat      = trail:RRCWLabourVAT
!    orcexp:RRCWarPartsCost          = trail:RRCWPartsCost
!    orcexp:RRCWarPartsSaleCost      = trail:RRCWPartsSale
!    orcexp:RRCWarPartsSaleVAT       = trail:RRCWPartsSaleVAT
!    !==                             
!    !ARC Costs
!    orcexp:ARCWarLabourCost         = trail:ARCWLabour
!    orcexp:ARCWarLabourCostVAT      = trail:ARCWLabourVAT
!    orcexp:ARCWarPartsCost          = trail:ARCWPartsCost
!    orcexp:ARCWarPartsSaleCost      = trail:ARCWPartsSale
!    orcexp:ARCWarPartsSaleVAT       = trail:ARCWPartsSaleVAT
!    orcexp:ARCManufacturerPaid      = trail:ARCWManufactLabour
!    orcexp:ARCManufacturerPaidVAT   = trail:ARCWManufactLabourVAT
!    orcexp:ARCManufacturerParts     = trail:ARCWManufactParts
!    orcexp:ARCManufacturerPartsVAT  = trail:ARCWManufactPartsVAT
!    orcexp:ARCThirdPartyCost        = trail:ARC3rdParty
!    orcexp:ARCThirdPartyCostVAT     = trail:ARC3rdPartyVAT
!    Add(OracleExport)
TitleLine       Routine
    If tmp:HeadAccount = 'HEAD'

        Clear(orcexp:Record)
        orcexp:Line1    = '"ServiceBase Job No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase EDI Batch Submission No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Manufacturers Batch Ref No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Site Branch Code'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Site Specific Job No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Generic Account'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Head Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Sub Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Sub Sub Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Transaction Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Invoice To'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Customer Order No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Job Completed Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Invoice No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Invoice Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Credit Note No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Credit Note Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Job'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Job Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Job Repair Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Job'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Job Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Job Repair Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Manufacturer'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Status'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Status Change Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Booking Origin'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Repair Location'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Handling Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Handling Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Chargeable Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT on Franchise Chargeable Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Chargeable Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Chargeable Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Chargeable Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Amount Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Chargeable Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On ARC Chargable Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Chargeable Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Chargeable Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On ARC Chargeable Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Amount Paid To ARC'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Lost Loan Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On ARC Lost Loan Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Manufacturer Exchange Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Exchange Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Warranty Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Warranty Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Warranty Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Warranty Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Warranty Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Warranty Claim Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On ARC Warranty Claim Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Warranty Claim Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Warranty Claim Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Warranty Labour Paid By Manufacturer'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On ARC Warranty Labour Paid By Manufacturer'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Warranty Parts Paid By Manufacturer'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On ARC Warranty Parts Paid By Manufacturer'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Date Paid'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC Charged By 3rd Part (net)'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT Charged By 3rd Party'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Invoice Number'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Third Party Repairer Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Refurbishment Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Refurbishment Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Refurbishment Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Refurbishment Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Refurbishment Parts Sale"'
        Add(OracleExport)

    Else !If tmp:HeadAccount = 'HEAD'
        Clear(orcexp:Record)
        orcexp:Line1    = '"ServiceBase Job No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Batch Submission No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Manufacturers Batch Ref No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Site Branch Code'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Site Specific Job No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Generic Account'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Head Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Sub Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Sub Sub Account No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Transaction Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Invoice To'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Customer Order No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Job Completed Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Invoice No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Invoice Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Credit Note No'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ServiceBase Credit Note Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Job'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Job Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Job Repair Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Job'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Job Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Job Repair Type'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Manufacturer'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Status'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Status Change Date'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Booking Origin'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Repair Location'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Handling Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Handling Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Chargeable Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT on Franchise Chargeable Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Chargeable Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Chargeable Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Chargeable Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Chargeable Amount Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Lost Loan Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Lost Loan Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Lost Loan Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Lost Loan Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Manufacturer Exchange Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Exchange Fee'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Exchange Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Exchange Fee Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Warranty Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Warranty Labour'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Warranty Parts Cost'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Franchise Warranty Parts Sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Franchise Warranty Parts sale'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Amount Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT Warranty Amount Paid To Franchise'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Warranty Claim Date Paid'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'ARC / 3rd Party Repairs Charged'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT Charged on ARC / 3rd Party Repairs'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Mark Up ARC / 3rd Party Repair'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT On Mark Up'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Amount Paid To Franchise On ARC / 3rd Party Repair'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'VAT Paid On ARC / 3rd Party Repair'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Invoice Number'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & 'Third Party Repairer Account Number"'
        Add(OracleExport)
    End !If tmp:HeadAccount = 'HEAD'
ExportJobs      Routine  !NOT USED!
!    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!    sub:Account_Number  = job:Account_Number
!    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!        !Found
!        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!        tra:Account_Number  = sub:Main_Account_Number
!        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!            !Found
!
!        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!            !Error
!            Exit
!        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!        !Error
!        Exit
!    End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!
!
!    Access:JOBSE.Clearkey(jobe:RefNumberKey)
!    jobe:RefNumber  = job:Ref_Number
!    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!        !Found
!
!    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!        !Error
!    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!
!    !Get the parts costs
!    tmp:ChaAverageTotal = 0
!    tmp:ChaPurchaseTotal = 0
!    tmp:ChaARCAverageTotal = 0
!    Save_par_ID = Access:PARTS.SaveFile()
!    Access:PARTS.ClearKey(par:Part_Number_Key)
!    par:Ref_Number  = job:Ref_Number
!    Set(par:Part_Number_Key,par:Part_Number_Key)
!    Loop
!        If Access:PARTS.NEXT()
!           Break
!        End !If
!        If par:Ref_Number  <> job:Ref_Number      |
!            Then Break.  ! End If
!        tmp:ChaAverageTotal          += Round(par:RRCAveragePurchaseCost,.01)
!        tmp:ChaPurchaseTotal         += Round(par:Purchase_Cost,.01)
!        tmp:ChaARCAverageTotal       += Round(par:AveragePurchaseCost,.01)
!    End !Loop
!    Access:PARTS.RestoreFile(Save_par_ID)
!
!    tmp:WarOutWarrantyTotal = 0
!    tmp:WarSaleTotal    = 0
!    tmp:WarCostAdjustment = 0
!    tmp:WarAverageTotal = 0
!    tmp:WarRRCAverageTotal = 0
!    Save_wpr_ID = Access:WARPARTS.SaveFile()
!    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
!    wpr:Ref_Number  = job:Ref_Number
!    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!    Loop
!        If Access:WARPARTS.NEXT()
!           Break
!        End !If
!        If wpr:Ref_Number  <> job:Ref_Number      |
!            Then Break.  ! End If
!        tmp:WarOutWarrantyTotal += Round(wpr:RRCSaleCost,.01)
!        tmp:WarSaleTotal        += Round(wpr:Sale_Cost,.01)
!        tmp:WarCostAdjustment   += Round(wpr:CostAdjustment,.01)
!        tmp:WarAverageTotal     += Round(wpr:AveragePurchaseCost,.01)
!        tmp:WarRRCAverageTotal  += Round(wpr:RRCAveragePurchaseCost,.01)
!    End !Loop
!    Access:WARPARTS.RestoreFile(Save_wpr_ID)
!
!    Clear(orcexp:Record)
!    orcexp:JobNumber                = job:Ref_Number
!    orcexp:SiteBranchCode           = tra:BranchIdentification
!    orcexp:WebJobNumber             = wob:JobNumber
!
!    If sub:Generic_Account
!        orcexp:GenericAccount       = 'YES'
!    Else !If sub:GenericAccount
!        orcexp:GenericAccount       = 'NO'
!    End !If sub:GenericAccount
!    
!    orcexp:HeadAccountNumber        = tra:Account_Number
!    orcexp:SubAccountNumber         = job:Account_Number
!    orcexp:SubSubAccountNumber      = jobe:Sub_Sub_Account
!    orcexp:CustomerOrderNumber      = job:Order_Number
!    orcexp:DateCompleted            = Format(job:Date_Completed,@d06)
!    orcexp:InvoiceNumber            = inv:Invoice_Number
!    orcexp:InvoiceDate              = Format(inv:Date_Created,@d06)
!    orcexp:CreditNoteNumber         = ''
!    orcexp:CreditNotedate           = ''
!    orcexp:ChargeableJob            = 'NO'
!    orcexp:WarrantyJob              = 'NO'
!
!    If tmp:InvoiceType = 'C'
!        If job:Chargeable_Job = 'YES'
!            orcexp:ChargeableJob            = 'YES'
!            orcexp:ChaChargeType            = job:Charge_Type
!            orcexp:ChaRepairType            = job:Repair_Type
!        Else
!            orcexp:ChargeableJob            = 'NO'
!        End !If job:Chargeable_Job
!    End !If tmp:InvoiceType = 'C'
!
!    If tmp:InvoiceType = 'W' or tmp:InvoiceType = 'X'
!        If job:Warranty_job = 'YES'
!            orcexp:WarrantyJob              = 'YES'
!            orcexp:WarChargeType            = job:Warranty_Charge_Type
!            orcexp:WarRepairType            = job:Repair_Type_Warranty
!        Else!If job:Warranty_job
!            orcexp:WarrantyJob              = 'NO'
!        End !If job:Warranty_job
!    End !If tmp:InvoiceType = 'W'
!
!
!    orcexp:Manufacturer             = job:Manufacturer
!    orcexp:WarrantyClaimStatus      = ''
!    orcexp:WarrantyClaimStatusDate  = ''
!
!    If job:Invoice_Number_Warranty <> 0
!        orcexp:BatchNumber          = inv:Batch_Number
!    Else !If job:Invoice_Number_Warranty <> 0
!        orcexp:BatchNumber          = job:EDI_Batch_Number
!        EDIPricing()
!        Access:JOBS.Update()
!    End !If job:Invoice_Number_Warranty <> 0
!    
!    orcexp:BatchReferenceNumber     = inv:Claim_Reference
!    orcexp:BookingOrigin            = tra:BranchIdentification
!    If job:Third_Party_Site <> ''
!        orcexp:Location             = '3rd Party'
!    Else !If job:Third_Party_Site <> ''
!        If jobe:HubRepair
!            orcexp:Location         = 'ARC'
!        Else !If jobe:HubRepair
!            orcexp:Location         = 'RRC'
!        End !If jobe:HubRepair
!    End !If job:Third_Party_Site <> ''
!    
!    orcexp:RRCHandlingFee           = Format(jobe:InvoiceHandlingFee,@n14.2)
!    
!    orcexp:RRCHandlingFeeVAT        = Format((jobe:InvoiceHandlingFee * inv:Vat_Rate_Labour / 100),@n14.2)
!
!    !So the costs will show zero, if not filled in. Pointless? You betcha!
!    orcexp:RRCChaLabourCost         = 0
!    orcexp:RRCChaLabourCostVAT      = 0 
!    orcexp:RRCChaPartsCost          = 0 
!    orcexp:RRCChaPartsSaleCost      = 0
!    orcexp:RRCChaPartsSaleVAT       = 0
!    orcexp:RRCChaTotal              = 0
!    orcexp:RRCWarLabourCost         = 0
!    orcexp:RRCWarLabourCostVat      = 0
!    orcexp:RRCWarPartsCost          = 0
!    orcexp:RRCWarPartsSaleCost      = 0
!    orcexp:RRCWarPartsSaleVAT       = 0
!    orcexp:ARCChaLabourCost         = 0
!    orcexp:ARCChaLabourCostVAT      = 0
!    orcexp:ARCChaPartsCost          = 0
!    orcexp:ARCChaPartsSaleCost      = 0
!    orcexp:ARCChaPartsSaleVAT       = 0
!    orcexp:ARCChaTotal              = 0
!    orcexp:ARCWarLabourCost         = 0
!    orcexp:ARCWarLabourCostVAT      = 0
!    orcexp:ARCWarPartsCost          = 0
!    orcexp:ARCWarPartsSaleCost      = 0
!    orcexp:ARCWarPartsSaleVAT       = 0
!    orcexp:ARCManufacturerPaid      = 0
!    orcexp:ARCManufacturerPaidVAT   = 0
!    orcexp:ARCManufacturerParts     = 0
!    orcexp:ARCManufacturerPartsVAT  = 0
!    orcexp:ARCThirdPartyCost        = 0
!    orcexp:ARCThirdPartyCostVAT     = 0
!
!    !RRC Job Originally
!    If jobe:WebJob
!        If job:Chargeable_Job = 'YES' and tmp:InvoiceType = 'C'
!            orcexp:RRCChaLabourCost         = Format(jobe:InvRRCCLabourCost,@n14.2)
!            orcexp:RRCChaLabourCostVAT      = Format(jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100,@n14.2)
!            orcexp:RRCChaPartsCost          = Format(tmp:ChaAverageTotal,@n14.2)
!            orcexp:RRCChaPartsSaleCost      = Format(jobe:InvRRCCPartsCost,@n14.2)
!            orcexp:RRCChaPartsSaleVAT       = Format(jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100,@n14.2)
!            orcexp:RRCChaTotal              = Format(orcexp:RRCChaLabourCost + orcexp:RRCChaLabourCostVAT + |
!                                                orcexp:RRCChaPartsSaleCost + orcexp:RRCChaPartsSaleVAT,@n14.2)
!        End !If job:Chargeable_Job = 'YES'
!
!        If job:Warranty_job = 'YES' And (tmp:InvoiceType = 'W' or tmp:InvoiceType = 'X')
!            If job:Invoice_Number_Warranty <> 0
!                orcexp:RRCWarLabourCost         = Format(jobe:InvRRCWLabourCost,@n14.2)
!                orcexp:RRCWarLabourCostVat      = Format(jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100,@n14.2)
!                orcexp:RRCWarPartsCost          = Format(tmp:WarRRCAverageTotal,@n14.2)
!                orcexp:RRCWarPartsSaleCost      = Format(tmp:WarOutWarrantyTotal,@n14.2)
!                orcexp:RRCWarPartsSaleVAT       = Format(tmp:WarOutWarrantyTotal * inv:Vat_Rate_Parts / 100,@n14.2)
!
!            Else !If job:Invoice_Number_Warranty <> 0
!                orcexp:RRCWarLabourCost         = Format(jobe:RRCWLabourCost,@n14.2)
!                orcexp:RRCWarLabourCostVat      = Format(jobe:RRCWLabourCost * VatRate(job:Account_Number,'L')/ 100,@n14.2)
!                orcexp:RRCWarPartsCost          = Format(tmp:WarRRCAverageTotal,@n14.2)
!                orcexp:RRCWarPartsSaleCost      = Format(tmp:WarOutWarrantyTotal,@n14.2)
!                orcexp:RRCWarPartsSaleVAT       = Format(tmp:WarOutWarrantyTotal * VatRate(job:Account_Number,'P') / 100,@n14.2)
!
!            End !If job:Invoice_Number_Warranty <> 0
!        End !If job:Warranty_job = 'YES'
!    End !If jobe:WebJob
!    !==                             
!    !ARC Costs
!
!    !Is this a hub job? Or has it been sent to the hub and come back?
!    !Check to see in the location history file there is an entry
!    !for AT ARC.
!
!    SentToHub# = 0
!    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
!    lot:RefNumber   = job:Ref_Number
!    lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
!    If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
!        !Found
!        SentToHub# = 1
!    Else!If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End!If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
!
!    If jobe:HubRepair Or SentToHub#
!        If job:Chargeable_Job = 'YES' And tmp:InvoiceType = 'C'
!            orcexp:ARCChaLabourCost         = Format(job:Invoice_Labour_Cost,@n14.2)
!            orcexp:ARCChaLabourCostVAT      = Format(job:Invoice_Labour_Cost * inv:Vat_rate_Labour/100,@n14.2)
!            orcexp:ARCChaPartsCost          = Format(tmp:ChaARCAverageTotal,@n14.2)
!            orcexp:ARCChaPartsSaleCost      = Format(job:Invoice_Parts_Cost,@n14.2)
!            orcexp:ARCChaPartsSaleVAT       = Format(job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100,@n14.2)
!            orcexp:ARCChaTotal              = Format(orcexp:ARCChaLabourCost + orcexp:ARCChaLabourCostVAT +|
!                                            orcexp:ARCChaPartsSaleCost + orcexp:ARCChaPartsSaleVAT,@n14.2)
!        End !If job:Chargeable_Job = 'YES'
!        If job:Warranty_Job = 'YES' And (tmp:InvoiceType = 'W' or tmp:InvoiceType = 'X')
!            If job:Invoice_Number_Warranty <> 0
!                orcexp:ARCWarLabourCost         = Format(job:WInvoice_Labour_Cost,@n14.2)
!                orcexp:ARCWarLabourCostVAT      = Format(job:Winvoice_Labour_Cost * inv:Vat_Rate_Labour/100,@n14.2)
!                orcexp:ARCWarPartsCost          = Format(tmp:WarAverageTotal,@n14.2)
!                orcexp:ARCWarPartsSaleCost      = Format(tmp:WarSaleTotal,@n14.2)
!                orcexp:ARCWarPartsSaleVAT       = Format(tmp:WarSaleTotal * inv:Vat_Rate_Parts/100,@n14.2)
!                orcexp:ARCManufacturerPaid      = Format(jobe:LabourAdjustment,@n14.2)
!                orcexp:ARCManufacturerPaidVAT   = Format(jobe:LabourAdjustment * inv:Vat_Rate_Labour/100,@n14.2)
!                orcexp:ARCManufacturerParts     = Format(tmp:WarCostAdjustment,@n14.2)
!                orcexp:ARCManufacturerPartsVAT  = Format(tmp:WarCostAdjustment * inv:Vat_Rate_Parts/100,@n14.2)
!                orcexp:WarrantyClaimDate        = Format(inv:Date_Created,@d06)
!            Else !If job:Invoice_Number_Warranty <> 0
!                orcexp:ARCWarLabourCost         = Format(job:Labour_Cost_Warranty,@n14.2)
!                orcexp:ARCWarLabourCostVAT      = Format(job:Labour_Cost_Warranty * VatRate(job:Account_Number,'L')/100,@n14.2)
!                orcexp:ARCWarPartsCost          = Format(tmp:WarAverageTotal,@n14.2)
!                orcexp:ARCWarPartsSaleCost      = Format(tmp:WarSaleTotal,@n14.2)
!                orcexp:ARCWarPartsSaleVAT       = Format(tmp:WarSaleTotal * VatRate(job:Account_Number,'P')/100,@n14.2)
!                orcexp:ARCManufacturerPaid      = ''
!                orcexp:ARCManufacturerPaidVAT   = ''
!                orcexp:ARCManufacturerParts     = ''
!                orcexp:ARCManufacturerPartsVAT  = ''
!                orcexp:WarrantyClaimDate        = ''
!            End !If job:Invoice_Number_Warranty <> 0
!        End !If job:Warranty_Job = 'YES'
!    End !If jobe:HubRepair
!    !==                     
!    !RRC Costs                      
!    !==
!    !ARC Costs
!    orcexp:ARCThirdPartyCost        = Format(jobe:ARC3rdPartyCost,@n14.2)
!    orcexp:ARCThirdPartyCostVAT     = Format(jobe:ARC3rdPartyCost * inv:Vat_Rate_Labour/100,@n14.2)
!    orcexp:ARCThirdPartyInvoiceNo   = 0
!    orcexp:ThirdPartySite           = job:Third_Party_Site
!    
!    Add(OracleExport)
!
!    trail:Count += 1
!    trail:HandlingFee               += orcexp:RRCHandlingFee
!    trail:HandlingFeeVAT            += orcexp:RRCHandlingFeeVAT
!    trail:RRCCLabour                += orcexp:RRCChaLabourCost
!    trail:RRCCLabourVAT             += orcexp:RRCChaLabourCostVAT
!    trail:RRCCPartsCost             += orcexp:RRCChaPartsCost
!    trail:RRCCPartsSale             += orcexp:RRCChaPartsSaleCost
!    trail:RRCCPartsSaleVAT          += orcexp:RRCChaPartsSaleVAT
!    trail:RRCCTotal                 += orcexp:RRCChaTotal 
!    trail:ARCCLabour                += orcexp:ARCChaLabourCost
!    trail:ARCCLabourVAT             += orcexp:ARCChaLabourCostVAT
!    trail:ARCCPartsCost             += orcexp:ARCChaPartsCost
!    trail:ARCCPartsSale             += orcexp:ARCChaPartsSaleCost
!    trail:ARCCPartsSaleVAT          += orcexp:ARCChaPartsSaleVAT
!    trail:ARCCTotal                 += orcexp:ARCChaTotal
!    trail:RRCWLabour                += orcexp:RRCWarLabourCost
!    trail:RRCWLabourVAT             += orcexp:RRCWarLabourCostVat
!    trail:RRCWPartsCost             += orcexp:RRCWarPartsCost
!    trail:RRCWPartsSale             += orcexp:RRCWarPartsSaleCost
!    trail:RRCWPartsSaleVAT          += orcexp:RRCWarPartsSaleVAT
!    trail:ARCWLabour                += orcexp:ARCWarLabourCost
!    trail:ARCWLabourVAT             += orcexp:ARCWarLabourCostVAT
!    trail:ARCWPartsCost             += orcexp:ARCWarPartsCost
!    trail:ARCWPartsSale             += orcexp:ARCWarPartsSaleCost
!    trail:ARCWPartsSaleVAT          += orcexp:ARCWarPartsSaleVAT
!    trail:ARCWManufactLabour        += orcexp:ARCManufacturerPaid
!    trail:ARCWManufactLabourVAT     += orcexp:ARCManufacturerPaidVAT
!    trail:ARCWManufactParts         += orcexp:ARCManufacturerParts
!    trail:ARCWManufactPartsVAT      += orcexp:ARCManufacturerPartsVAT
!    trail:ARC3rdParty               += orcexp:ARCThirdPartyCost
!    trail:ARC3rdPartyVAT            += orcexp:ARCThirdPartyCostVAT



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020566'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, AppFrame, 1)  !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CreateExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:INVOICE_ALIAS.Open
  Relate:ORACLECN.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  Access:ORACLEEX.UseFile
  Access:USERS.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:LOCATLOG.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  tmp:OracleExport = Upper(GETINI('ORACLE','ExportPath',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:InvoiceDate = Today()
  tmp:StartDate   = Date(Month(Today()),1,Year(today()))
  tmp:EndDate     = Today()
  
  !Get ARC Site Location
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation  = tra:SiteLocation
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  OPEN(AppFrame)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','CreateExport')
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:AccountNumber{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?tmp:AccountNumber{Prop:Tip}
  END
  IF ?tmp:AccountNumber{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?tmp:AccountNumber{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:INVOICE_ALIAS.Close
    Relate:ORACLECN.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CreateExport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickHeadAccounts
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020566'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020566'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020566'&'0')
      ***
    OF ?tmp:AccountNumber
      IF tmp:AccountNumber OR ?tmp:AccountNumber{Prop:Req}
        tra:Account_Number = tmp:AccountNumber
        !Save Lookup Field Incase Of error
        look:tmp:AccountNumber        = tmp:AccountNumber
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:AccountNumber = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:AccountNumber = look:tmp:AccountNumber
            SELECT(?tmp:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      tra:Account_Number = tmp:AccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:AccountNumber = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:AccountNumber)
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      !Get the Oracle Number
      !Do we have an export path?
      Error# = 0
      Clear(tmp:Total)
      tmp:OracleExport = Clip(GETINI('ORACLE','ExportPath',,CLIP(PATH())&'\SB2KDEF.INI'))
      If tmp:OracleExport = ''
          Case Missive('You have not set-up a default export path.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
      End !tmp:OracleExport = ''
      IF tmp:AccountNumber = '' And Error# = 0
          Case Missive('You must select an Account Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:AccountNumber)
          Error# = 1
      End !tmp:AccountNumber = '' And Error# = 0
      
      If tmp:StartDate = '' Or tmp:EndDate = '' And Error# = 0
          Case Missive('You must select a date range.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
      End !tmp:StartDate = '' Or tmp:EndDate = '' And Error# = 0
      
      If Error# = 0
          tmp:HeadAccount = tmp:AccountNumber
          If tmp:AccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
              !Is this being run for the Head ACcount?
              tmp:HeadAccount = 'HEAD'
          End !If tmp:AccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
      
          tmp:OracleNumber = 0
          If Access:ORACLEEX.PrimeRecord() = Level:Benign
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Found
                  ora:UserCode    = use:User_Code
              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Error
              End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              If Access:ORACLEEX.TryInsert() = Level:Benign
                  !Insert Successful
                  tmp:OracleNumber = ora:RecordNumber
              Else !If Access:ORACLEEX.TryInsert() = Level:Benign
                  !Insert Failed
                  Access:ORACLEEX.CancelAutoInc()
              End !If Access:ORACLEEX.TryInsert() = Level:Benign
          End !If Access:ORACLEEX.PrimeRecord() = Level:Benign
      
          If tmp:OracleNumber <> 0
      
              savepath = path()
              If Sub(Clip(tmp:OracleExport),-1,1) <> '\'
                  tmp:OracleExport = Clip(tmp:OracleExport) & '\'
              End !Sub(tmp:ExportPath,-1,1) = '\'
              tmp:OracleExport = Clip(tmp:OracleExport) & 'sb_job' & Clip(tmp:HeadAccount) & '_' & Format(Day(Today()),@n02) & '_' & Format(Month(Today()),@n02) & '_' & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.csv'
      
              Create(OracleExport)
              Open(OracleExport)
      
              !Quick record count
              Count# = 0
              Save_inv_ID = Access:INVOICE.SaveFile()
              Access:INVOICE.ClearKey(inv:Date_Created_Key)
              inv:Date_Created = tmp:StartDate
              Set(inv:Date_Created_Key,inv:Date_Created_Key)
              Loop
                  If Access:INVOICE.NEXT()
                     Break
                  End !If
                  If inv:Date_Created > tmp:EndDate      |
                      Then Break.  ! End If
      
                  If inv:Invoice_Type = 'RET'
                      !Ignore Retail Invoices
                      Cycle
                  End !If inv:Invoice_Type = 'RET'
      
                  Count# += 1
                  If Count# = 1000
                      Count# = Records(INVOICE)
                      Break
                  End !If Count# = 1000
      
              End !Loop
              Access:INVOICE.RestoreFile(Save_inv_ID)
      
              Save_jobe_ID = Access:JOBSE.SaveFile()
              Access:JOBSE.ClearKey(jobe:WarrStatusDateKey)
              jobe:WarrantyStatusDate = tmp:StartDate
              Set(jobe:WarrStatusDateKey,jobe:WarrStatusDateKey)
              Loop
                  If Access:JOBSE.NEXT()
                     Break
                  End !If
                  If jobe:WarrantyStatusDate > tmp:EndDate       |
                      Then Break.  ! End If
                  Count# += 1
                  If Count# > 2000
                      Count# += Records(JOBSE)
                      Break
                  End !If Count# > 2000
              End !Loop
              Access:JOBSE.RestoreFile(Save_jobe_ID)
      
      
              Prog.ProgressSetup(Count#)
              Do TitleLine
              Do HeaderLine
      
              Counting# = 0
              !First all invoiced jobs within the date range
              Save_inv_ID = Access:INVOICE.SaveFile()
              Access:INVOICE.ClearKey(inv:Date_Created_Key)
              inv:Date_Created = tmp:StartDate
              Set(inv:Date_Created_Key,inv:Date_Created_Key)
              Loop
                  If Access:INVOICE.NEXT()
                     Break
                  End !If
                  If inv:Date_Created > tmp:EndDate       |
                      Then Break.  ! End If
      
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()
      
                  If inv:Invoice_Type = 'RET'
                      !Ignore Retail Invoices
                      Cycle
                  End !If inv:Invoice_Type = 'RET'
      
                  If inv:Invoice_Type = 'WAR'
                      tmp:InvoiceType = 'W'
                      !Warranty Invoice
                      Save_job_ID = Access:JOBS.SaveFile()
                      Access:JOBS.ClearKey(job:WarInvoiceNoKey)
                      job:Invoice_Number_Warranty = inv:Invoice_Number
                      Set(job:WarInvoiceNoKey,job:WarInvoiceNoKey)
                      Loop
                          If Access:JOBS.NEXT()
                             Break
                          End !If
                          If job:Invoice_Number_Warranty <> inv:Invoice_Number      |
                              Then Break.  ! End If
      
                          !Has this job already been done
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = job:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
      !                        If wob:OracleExportNumber <> 0
      !                            Cycle
      !                        End !If wob:OracleExportNumber <> 0
      
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber  = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
                              
                          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          If tmp:HeadAccount = 'HEAD'
      !                        !IF running for the Head ACcount, then only
      !                        !pick up jobs that have been to the hub
      !                        If ~SentToHub(job:Ref_Number)
      !                            Cycle
      !                        End !If ~SentToHub(job:Ref_Number)
                          Else !If tmp:HeadAccount = 'HEAD'
                              !Otherwise only pick up jobs that were booked by the
                              !account. But only jobs that have NOT been sent to the hub
                              If wob:HeadAccountNumber <> tmp:HeadAccount
                                  Cycle
                              End !If wob:HeadAccount <> tmp:HeadACcount
                              If SentToHub(job:Ref_Number)
                                  Cycle
                              End !If SentToHub(job:Ref_Number)
      
                          End !If tmp:HeadAccount = 'HEAD'
      
                          !Is this an RRC job, and is so,
                          !was it sent to the ARC?
      
                          If jobe:WebJob
                              !Booked at RRC?
                              If SentToHub(job:Ref_Number)
                                  !But sent to ARC
                                  Local.ExportJobs('WIARC','RECONCILED')
                              Else !If SentToHub(job:Ref_Number)
                                  Local.ExportJobs('WIRRC','RECONCILED')
                              End !If SentToHub(job:Ref_Number)
                          Else !If jobe:WebJob
                              Local.ExportJobs('WIARC','RECONCILED')
                          End !If jobe:WebJob
      
      
                          !Reget.. just in case
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = job:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
                              wob:OracleExportNumber = tmp:OracleNumber
                              Access:WEBJOB.TryUpdate()
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                      End !Loop
                      Access:JOBS.RestoreFile(Save_job_ID)
                  Else !If inv:Invoice_Type = 'WAR'
                      !Chargeable Invoice
                      tmp:InvoiceType = 'C'
                      Save_job_ID = Access:JOBS.SaveFile()
                      Access:JOBS.ClearKey(job:InvoiceNumberKey)
                      job:Invoice_Number = inv:Invoice_Number
                      Set(job:InvoiceNumberKey,job:InvoiceNumberKey)
                      Loop
                          If Access:JOBS.NEXT()
                             Break
                          End !If
                          If job:Invoice_Number <> inv:Invoice_Number      |
                              Then Break.  ! End If
      
                          !Has this job already been done
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = job:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
      !                        If wob:OracleExportNumber <> 0
      !                            Cycle
      !                        End !If wob:OracleExportNumber <> 0
      
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                          !Is this an RRC job, and is so,
                          !was it sent to the ARC?
      
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber  = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
      
                          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          If tmp:HeadAccount = 'HEAD'
      !                        !IF running for the Head ACcount, then only
      !                        !pick up jobs that have been to the hub
      !                        If ~SentToHub(job:Ref_Number)
      !                            Cycle
      !                        End !If ~SentToHub(job:Ref_Number)
                          Else !If tmp:HeadAccount = 'HEAD'
                              !Otherwise only pick up jobs that were booked by the
                              !account. But only jobs that have NOT been sent to the hub
                              If wob:HeadAccountNumber <> tmp:HeadAccount
                                  Cycle
                              End !If wob:HeadAccount <> tmp:HeadACcount
                              If SentToHub(job:Ref_Number)
                                  Cycle
                              End !If SentToHub(job:Ref_Number)
      
                          End !If tmp:HeadAccount = 'HEAD'
      
                          If jobe:WebJob
                              !Booked at RRC?
                              If SentToHub(job:Ref_Number)
                                  !But sent to ARC
                                  !Has the invoice been created by the RRC yet?
                                  If inv:ExportedRRCOracle
                                      Local.ExportJobs('CIRRC','')
                                  End !If inv:ExportedRRCOracle
                                  If inv:ExportedARCOracle
                                      Local.ExportJobs('CIARC','')
                                  End !If inv:ExportedARCOracle
                              Else !If SentToHub(job:Ref_Number)
                                  If inv:ExportedRRCOracle
                                      Local.ExportJobs('CIRRC','')
                                  End !If inv:ExportedRRCOracle
                              End !If SentToHub(job:Ref_Number)
                          Else !If jobe:WebJob
                              If inv:ExportedARCOracle
                                  Local.ExportJobs('CIARC','')
                              End !If inv:ExportedARCOracle
                          End !If jobe:WebJob
      
                          !Reget.. just in case
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber   = job:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Found
      !                        wob:OracleExportNumber = tmp:OracleNumber
      !                        Access:WEBJOB.TryUpdate()
                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
      
                      End !Loop
                      Access:JOBS.RestoreFile(Save_job_ID)
                  End !If inv:Invoice_Type = 'WAR'
      !            pos = Position(inv:OracleNumberKey)
      !            inv:ExportedOracleDate = Today()
      !            inv:ExportedARCOracle   = 1
      !            inv:OracleNumber        = tmp:OracleNumber
      !            Access:INVOICE.TryUpdate()
      !            Reset(inv:OracleNumberKey,pos)
      
              End !Loop
              Access:INVOICE.RestoreFile(Save_inv_ID)
      
              !Now only pick up warranty jobs that have changed their status
              !within the date range
              Save_jobe_ID = Access:JOBSE.SaveFile()
              Access:JOBSE.ClearKey(jobe:WarrStatusDateKey)
              jobe:WarrantyStatusDate = tmp:StartDate
              Set(jobe:WarrStatusDateKey,jobe:WarrStatusDateKey)
              Loop
                  If Access:JOBSE.NEXT()
                     Break
                  End !If
                  If jobe:WarrantyStatusDate > tmp:EndDate       |
                      Then Break.  ! End If
      
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = jobe:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
      
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                      Cycle
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  !Get the invoice file
                  If job:Invoice_Number <> 0
                      Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                      inv:Invoice_Number  = job:Invoice_Number
                      If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                          !Found
                      Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                          !Error
                      End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                  End !If job:Invoice_Number <> 0
      
                  If job:Warranty_Job <> 'YES'
                      Cycle
                  End !If job:Warranty_Job <> 'YES'
      
                  If job:EDI = 'XXX'
                      Cycle
                  End !If job:EDI = 'XXX'
      
                  !Found
                  If job:Date_Completed = ''
                      Cycle
                  End !If job:Date_Completed = ''
                  !Doube check that is isn't invoiced
                  If job:Invoice_Number_Warranty <> 0
                      Cycle
                  End !If job:Invoice_Number_Warranty <> 0
                  If job:Warranty_Job <> 'YES'
                      Cycle
                  End !If job:Warranty_Job <> 'YES'
      
                  tmp:InvoiceType = 'X'
      
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber   = job:Ref_Number
                  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Found
      
                  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Error
                      Cycle
                  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                  If tmp:HeadAccount <> 'HEAD'
                      !Otherwise only pick up jobs that were booked by the
                      !account. But only jobs that have NOT been sent to the hub
                      If wob:HeadAccountNumber <> tmp:HeadAccount
                          Cycle
                      End !If wob:HeadAccount <> tmp:HeadACcount
                      If SentToHub(job:Ref_Number)
                          Cycle
                      End !If SentToHub(job:Ref_Number)
      
                  End !If tmp:HeadAccount = 'HEAD'
      
                  !Ok Write a line for the current status
                  If jobe:WebJob
                      !Booked at RRC?
                      If SentToHub(job:Ref_Number)
                          !But sent to ARC
                          Local.ExportJobs('ARC',Clip(jobe:WarrantyClaimStatus))
                      Else !If SentToHub(job:Ref_Number)
                          Local.ExportJobs('RRC',Clip(jobe:WarrantyClaimStatus))
                      End !If SentToHub(job:Ref_Number)
                  Else !If jobe:WebJob
                      Local.ExportJobs('ARC',Clip(jobe:WarrantyClaimStatus))
                  End !If jobe:WebJob
      
                  !Then go through Audit Trail and find any other previous status types
                  Save_aud_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.ClearKey(aud:Ref_Number_Key)
                  aud:Ref_Number = job:Ref_Number
                  aud:Date       = Today()
                  Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
                  Loop
                      If Access:AUDIT.NEXT()
                         Break
                      End !If
                      If aud:Ref_Number <> job:Ref_Number      |
                          Then Break.  ! End If
      !                Message('Job Number: ' & job:Ref_Number & '|Action: ' & aud:Action)
      
                      !Don't include a line for the current status
                      If Instring(jobe:WarrantyClaimStatus,aud:Action,1,1)
                          Cycle
                      End !If Instring(jobe:WarrantyClaimStatus,aud:Action,1,1)
      
                      Case aud:Action
                          Of 'WARRANTY CLAIM RESUBMITTED'
                              Status" = 'RESUBMITTED'
                              If jobe:WebJob
                                  !Booked at RRC?
                                  If SentToHub(job:Ref_Number)
                                      !But sent to ARC
                                      Local.ExportJobs('ARC',Clip(Status"))
                                  Else !If SentToHub(job:Ref_Number)
                                      Local.ExportJobs('RRC',Clip(Status"))
                                  End !If SentToHub(job:Ref_Number)
                              Else !If jobe:WebJob
                                  Local.ExportJobs('ARC',Clip(Status"))
                              End !If jobe:WebJob
      
                          Of 'WARRANTY CLAIM REJECTED'
                              Status" = 'REJECTED'
                              If jobe:WebJob
                                  !Booked at RRC?
                                  If SentToHub(job:Ref_Number)
                                      !But sent to ARC
                                      Local.ExportJobs('ARC',Clip(Status"))
                                  Else !If SentToHub(job:Ref_Number)
                                      Local.ExportJobs('RRC',Clip(Status"))
                                  End !If SentToHub(job:Ref_Number)
                              Else !If jobe:WebJob
                                  Local.ExportJobs('ARC',Clip(Status"))
                              End !If jobe:WebJob
      
                          Of 'WARRANTY CLAIM FINAL REJECTION'
                              Status" = 'FINAL REJECTION'
                              If jobe:WebJob
                                  !Booked at RRC?
                                  If SentToHub(job:Ref_Number)
                                      !But sent to ARC
                                      Local.ExportJobs('ARC',Clip(Status"))
                                  Else !If SentToHub(job:Ref_Number)
                                      Local.ExportJobs('RRC',Clip(Status"))
                                  End !If SentToHub(job:Ref_Number)
                              Else !If jobe:WebJob
                                  Local.ExportJobs('ARC',Clip(Status"))
                              End !If jobe:WebJob
      
                          Of 'WARRANTY CLAIM RECONCILED'
                              Status" = 'RECONCILED'
                              If jobe:WebJob
                                  !Booked at RRC?
                                  If SentToHub(job:Ref_Number)
                                      !But sent to ARC
                                      Local.ExportJobs('ARC',Clip(Status"))
                                  Else !If SentToHub(job:Ref_Number)
                                      Local.ExportJobs('RRC',Clip(Status"))
                                  End !If SentToHub(job:Ref_Number)
                              Else !If jobe:WebJob
                                  Local.ExportJobs('ARC',Clip(Status"))
                              End !If jobe:WebJob
      
                      End !Case aud:Action.
      
      
                      If Instring('EDI FILE CREATED',aud:Action,1,1)
                          !Don't count this if the current status is submitted
                          If jobe:WarrantyClaimStatus = 'SUBMITTED'
                              Cycle
                          End !If jobe:WarrantyClaimStatus = 'SUBMITTED'
                          Status" = 'SUBMITTED'
                          If jobe:WebJob
                              !Booked at RRC?
                              If SentToHub(job:Ref_Number)
                                  !But sent to ARC
                                  Local.ExportJobs('ARC',Clip(Status"))
                              Else !If SentToHub(job:Ref_Number)
                                  Local.ExportJobs('RRC',Clip(Status"))
                              End !If SentToHub(job:Ref_Number)
                          Else !If jobe:WebJob
                              Local.ExportJobs('ARC',Clip(Status"))
                          End !If jobe:WebJob
                      End !If Instring('EDI FILE CREATED',aud:Action,1,1)
      
                  End !Loop
                  Access:AUDIT.RestoreFile(Save_aud_ID)
      
      
              End !Loop
              Access:JOBSE.RestoreFile(Save_jobe_ID)
      
      
              Prog.ProgressFinish()
              Do FooterLine
      
              Close(OracleExport)
      
              If trail:Count = 0
                  Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                                 'mstop.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
                  Remove(OracleExport)
                  Access:ORACLEEX.Clearkey(ora:RecordNumberKey)
                  ora:RecordNumber    = tmp:OracleNumber
                  If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                      !Found
                      Delete(ORACLEEX)
                  Else ! If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
              Else !If trail:Count = 0
                  Case Missive('Export Completed.'&|
                    '<13,10>File Created:'&|
                    '<13,10>'&|
                    '<13,10>' & Clip(tmp:OracleExport) & '.','ServiceBase 3g',|
                                 'mexclam.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
              End !If trail:Count = 0
      
          End !tmp:OracleNumer <> 0
      End !Error# = 0
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.SentToHub     Procedure()
Code
    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
    lot:RefNumber   = job:Ref_Number
    lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
        !Found
        Return Level:Fatal
    Else!If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign

    Return Level:Benign
Local.ExportJobs      Procedure(String  func:Type,String func:Status)
local:ARCInvoiced       Byte(0)
local:RRCInvoiced       Byte(0)
Code
!New Way
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found

        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            Return
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        Return
    End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
        Return
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

    !Is job invoiced?
    If job:Chargeable_Job = 'YES'
        If job:Invoice_Number <> 0
            Access:INVOICE_ALIAS.ClearKey(inv_ali:Invoice_Number_Key)
            inv_ali:Invoice_Number = job:Invoice_Number
            If Access:INVOICE_ALIAS.TryFetch(inv_ali:Invoice_Number_Key) = Level:Benign
                !Found
                If inv:ARCInvoiceDate <> ''
                    local:ARCInvoiced = 1
                End !If inv:ARCInvoiceDate <> ''
                If inv:RRCInvoiceDate <> ''
                    local:RRCInvoiced = 1
                End !If inv:RRCInvoiceDate <> ''
            Else !If Access:INVOICE_ALIAS.TryFetch(inv_ali:Invoice_Number_Key) = Level:Benign
                !Error
            End !If Access:INVOICE_ALIAS.TryFetch(inv_ali:Invoice_Number_Key) = Level:Benign
        End !If job:Invoice_Number <> 0
    End !If job:Chargeable_Job = 'YES'


    !Get the parts costs
    tmp:ChaAverageTotal = 0
    tmp:ChaPurchaseTotal = 0
    tmp:ChaARCAverageTotal = 0
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:ChaAverageTotal          += Round(par:RRCAveragePurchaseCost * par:Quantity,.01)
        tmp:ChaPurchaseTotal         += Round(par:Purchase_Cost * par:Quantity,.01)
        tmp:ChaARCAverageTotal       += Round(par:AveragePurchaseCost * par:Quantity,.01)
    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)

    tmp:WarOutWarrantyTotal = 0
    tmp:WarSaleTotal    = 0
    tmp:WarCostAdjustment = 0
    tmp:WarAverageTotal = 0
    tmp:WarRRCAverageTotal = 0
    tmp:WarARCPurchaseTotal = 0
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        tmp:WarOutWarrantyTotal += Round(wpr:RRCSaleCost * wpr:Quantity,.01)
        tmp:WarSaleTotal        += Round(wpr:Sale_Cost * wpr:Quantity,.01)
        tmp:WarCostAdjustment   += Round(wpr:CostAdjustment * wpr:Quantity,.01)
        tmp:WarAverageTotal     += Round(wpr:AveragePurchaseCost * wpr:Quantity,.01)
        tmp:WarRRCAverageTotal  += Round(wpr:RRCAveragePurchaseCost * wpr:Quantity,.01)
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = wpr:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:Location = tmp:ARCLocation
                tmp:WarARCPurchaseTotal    += Round(wpr:AveragePurchaseCost * wpr:Quantity,.01)
            End !If sto:Location = tmp:ARCLocation
        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

    !_____________________________________________________________________


    Clear(orcexp:Record)
    !Job Number
    orcexp:Line1    = '"' & Clip(job:Ref_Number)
    !Batch Reference Number
    If job:Invoice_Number_Warranty <> 0
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(inv:Batch_Number,@n_14.2b))
    Else !If job:Invoice_Number_Warranty <> 0
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:EDI_Batch_Number,@n_14.2b))
        EDIPricing()
        Access:JOBS.Update()
    End !If job:Invoice_Number_Warranty <> 0
    !Batch Number
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(inv:Claim_Reference)

    !Site Branch Code
    Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
        !Found
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(tra_ali:BranchIdentification)
    Else ! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
        !Error
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
    End !If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
    
    !Site Job Number
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(wob:JobNumber)
    !Generic Account
    If sub:Generic_Account
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('YES')
    Else !If sub:GenericAccount
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('NO')
    End !If sub:GenericAccount
    !Head Account
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(tra:Account_Number)
    !Sub Account Number
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Account_Number)
    !Sub Sub Account Number
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(jobe:Sub_Sub_Account)
    !Transaction Type
    !Invoice To
    Case func:Type
        Of 'WIARC' Orof 'WIRRC' 
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('RECONCILED')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(man:Trade_Account)
        Of 'CIARC'
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('ARC INVOICE CREATED')
            If jobe:WebJob
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(wob:HeadAccountNumber)
            Else !If jobe:WebJob
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Account_Number)
            End !If jobe:WebJob
        Of 'CIRRC'
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('RRC INVOICE CREATED')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Account_Number)
        Else
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Clip(func:Status))
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(man:Trade_Account)
    End !Case func:Type
    !Customer Order Number
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Order_Number)
    !Job Completed Date
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Date_Completed,@d06))
    !ServiceBase Invoice Number
    !ServiceBase Invoice Date
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(inv:Invoice_Number) & ' - ' & Clip(tra_ali:BranchIdentification)
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(inv:Date_Created,@d06b))
    !ServiceBase Credit Note Number
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
    !ServiceBase Credit Note Date
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
    !Chargeable Job
    !Chargeable Job Type
    !Chargeable Repair Type
    If job:Chargeable_Job = 'YES'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('YES')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Charge_Type)
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Repair_Type)
    Else
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('NO')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
    End !If job:Chargeable_Job
    !Warranty Job
    !Warranty Job Type
    !Warranty Repair Type
    If job:Warranty_job = 'YES'
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('YES')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Warranty_Charge_Type)
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Repair_Type_Warranty)
    Else!If job:Warranty_job
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('NO')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
    End !If job:Warranty_job
    !Manufacturer
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Manufacturer)
    !Warranty Claim Status
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(jobe:WarrantyClaimStatus)
    !Warranty Claim Status Change Date
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:WarrantyStatusDate,@d06b))
    !Booking Origin
    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(tra_ali:BranchIdentification)
    !Repair Location
    If job:Third_Party_Site <> ''
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('3rd Party')
    Else !If job:Third_Party_Site <> ''
        If SentToHub(job:Ref_Number)
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('ARC')
        Else !If jobe:HubRepair
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('RRC')
        End !If jobe:HubRepair
    End !If job:Third_Party_Site <> ''
    If (job:Chargeable_Job = 'YES' And job:Invoice_Number <> 0) Or |
        (job:Warranty_Job = 'YES' And job:INvoice_Number_Warranty <> 0)
        If SentToHub(job:Ref_Number) And jobe:WebJob = 1
            !Handling Fee Paid To Franchise
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvoiceHandlingFee,@n_14.2))
            tmp:Total[1]    += jobe:InvoiceHandlingFee
            !Handling Fee Paid To Franchise VAT
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format((jobe:InvoiceHandlingFee * inv:Vat_Rate_Labour / 100),@n_14.2))
            tmp:Total[2]    +=jobe:InvoiceHandlingFee * inv:Vat_Rate_Labour / 100
        Else
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End !If SentToHub(job:Ref_Number) And jobe:WebJob = 1
    Else !job:Warranty_Job = 'YES' And job:INvoice_Number_Warranty <> 0)
        If SentToHub(job:Ref_Number) And jobe:WebJob = 1
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:HandlingFee,@n_14.2))
            tmp:Total[1]    += jobe:HandlingFee

            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:HandlingFee * VatRate(job:Account_Number,'L')/100,@n_14.2))
            tmp:Total[2]    += jobe:HandlingFee * VatRate(job:Account_Number,'L') / 100
        Else !If SentToHub(job:Ref_Number) And jobe:WebJob = 1
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End !If SentToHub(job:Ref_Number) And jobe:WebJob = 1
    End !job:Warranty_Job = 'YES' And job:INvoice_Number_Warranty <> 0)

    !RRC Chargeable Labour
        !Only show chargeable costs for chargeable jobs - L871 (DBH: 11-07-2003)
    If jobe:WebJob And job:Chargeable_Job = 'YES'
        If local:RRCInvoiced
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCCLabourCost,@n_14.2))
            tmp:Total[3]    += jobe:InvRRCCLabourCost
            !RRC Chargeable Labour VAT
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100,@n_14.2))
            tmp:Total[4]    += jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100
            !RRC Chargeable Parts Cost
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:ChaAverageTotal,@n_14.2))
            tmp:Total[5]    += tmp:ChaAverageTotal
            !RRC Chargeable Sale Cost
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCCPartsCost,@n_14.2))
            tmp:Total[6]    += jobe:InvRRCCPartsCost
            !RRC Chargeable Parts VAT
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100,@n_14.2))
            tmp:Total[7]    += jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100
            !RRC Chargeable Total
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100),@n_14.2))
            tmp:Total[8]    += jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100)
        Else !If local:RRCInvoiced
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCCLabourCost,@n_14.2))
            tmp:Total[3]    += jobe:InvRRCCLabourCost
            !RRC Chargeable Labour VAT
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCCLabourCost * VatRate(job:Account_Number,'L') / 100,@n_14.2))
            tmp:Total[4]    += jobe:RRCCLabourCost * VatRate(job:Account_Number,'P') / 100
            !RRC Chargeable Parts Cost
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:ChaAverageTotal,@n_14.2))
            tmp:Total[5]    += tmp:ChaAverageTotal
            !RRC Chargeable Sale Cost
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCCPartsCost,@n_14.2))
            tmp:Total[6]    += jobe:RRCCPartsCost
            !RRC Chargeable Parts VAT
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCCPartsCost * VatRate(job:Account_Number,'P') / 100,@n_14.2))
            tmp:Total[7]    += jobe:RRCCPartsCost * VatRate(job:Account_Number,'P') / 100
            !RRC Chargeable Total
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCCLabourCost + jobe:RRCCPartsCost + (jobe:RRCCLabourCost * VatRate(job:Account_Number,'L') / 100) + (jobe:RRCCPartsCost * VatRate(job:Account_Number,'P') / 100),@n_14.2))
            tmp:Total[8]    += jobe:RRCCLabourCost + jobe:RRCCPartsCost + (jobe:RRCCLabourCost * VatRate(job:Account_Number,'L') / 100) + (jobe:RRCCPartsCost * VatRate(job:Account_Number,'P') / 100)
        End !If local:RRCInvoiced
    Else !If jobe:WebJob
        Loop x# = 1 To 6
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')    
        End !Loop x# = 1 To 6
    End !If jobe:WebJob
    !_____________________________________________________________________

    !_____________________________________________________________________


    If tmp:HeadAccount = 'HEAD'
        !Only show ARC costs IF the job has been
        !sent to ARC, or is an ARC job - L871 (DBH: 11-07-2003)
        If job:Chargeable_Job = 'YES' And SentToHub(job:Ref_Number)
            If local:ARCInvoiced
                !ARC Chargeable Labour
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Labour_Cost,@n_14.2))
                tmp:Total[9]    += job:Invoice_Labour_Cost
                !ARC Chargeable Labour VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Labour_Cost * inv:Vat_rate_Labour/100,@n_14.2))
                tmp:Total[10]   += job:Invoice_Labour_Cost * inv:Vat_rate_Labour/100
                !ARC Chargeable Parts Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:ChaARCAverageTotal,@n_14.2))
                tmp:Total[11]   += tmp:ChaARCAverageTotal
                !ARC Chargeable Sale Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Parts_Cost,@n_14.2))
                tmp:Total[12]   += job:Invoice_Parts_Cost
                !ARC Chargeable Parts Cost VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100,@n_14.2))
                tmp:Total[13]   += job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100
                !ARC Chargeable Total
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Labour_Cost + job:Invoice_Labour_Cost * inv:Vat_rate_Labour/100 +|
                                                job:Invoice_Parts_Cost + job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100,@n_14.2))
                tmp:Total[14]   += job:Invoice_Labour_Cost + job:Invoice_Labour_Cost * inv:Vat_rate_Labour/100 +|
                                                job:Invoice_Parts_Cost + job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100
                !ARC Lost Loan Fee
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Courier_Cost,@n_14.2))
                tmp:Total[15]   += job:Invoice_Courier_Cost
                !ARC Lost Loan Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100))
                tmp:Total[16]   += job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100

            Else !If local:ARCInvoiced
                !ARC Chargeable Labour
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Labour_Cost,@n_14.2))
                tmp:Total[9]    += job:Labour_Cost
                !ARC Chargeable Labour VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Labour_Cost * VatRate(job:Account_Number,'L')/100,@n_14.2))
                tmp:Total[10]   += job:Invoice_Labour_Cost * VatRate(job:Account_Number,'L')/100
                !ARC Chargeable Parts Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:ChaARCAverageTotal,@n_14.2))
                tmp:Total[11]   += tmp:ChaARCAverageTotal
                !ARC Chargeable Sale Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Parts_Cost,@n_14.2))
                tmp:Total[12]   += job:Parts_Cost
                !ARC Chargeable Parts Cost VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Parts_Cost * VatRate(job:Account_Number,'P')/100,@n_14.2))
                tmp:Total[13]   += job:Invoice_Parts_Cost * VatRate(job:Account_Number,'P')/100
                !ARC Chargeable Total
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Labour_Cost + job:Labour_Cost * VatRate(job:Account_Number,'L')/100 +|
                                                job:Parts_Cost + job:Parts_Cost * VatRate(job:Account_Number,'P')/100,@n_14.2))
                tmp:Total[14]   += job:Labour_Cost + job:Labour_Cost * VatRate(job:Account_Number,'L')/100 +|
                                                job:Parts_Cost + job:Parts_Cost * VatRate(job:Account_Number,'P')/100
                !ARC Lost Loan Fee
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost,@n_14.2))
                tmp:Total[15]   += job:Courier_Cost
                !ARC Lost Loan Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost * VatRate(job:Account_Number,'L')/100))
                tmp:Total[16]   += job:Courier_Cost * VatRate(job:Account_Number,'L')/100
            End !If local:ARCInvoiced
        Else !If job:Chargeable_Job = 'YES'
            Loop x# = 1 To 8
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')    
            End !Loop x# = 1 To 6
        End !If job:Chargeable_Job = 'YES'
        !Manufacturer Exchange Fee
            !Show the MANUFACTURER Exchage Fee
            !which is filled in Courier_Cost_Warranty
            !But only for ARC Exchanges - L871 (DBH: 11-07-2003)
        If job:Warranty_Job = 'YES' and job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
            If job:Invoice_Number_Warranty <> 0
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:WInvoice_Courier_Cost,@n_14.2))
                tmp:Total[17]   += job:WInvoice_Courier_Cost
                !Manufacturer Exchange Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100,@n_14.2))
                tmp:Total[18]   += job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
            Else !If job:Invoice_Number_Warranty <> 0
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost_Warranty,@n_14.2))
                tmp:Total[17]   += job:Courier_Cost_Warranty
                !Manufacturer Exchange Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100,@n_14.2))
                tmp:Total[18]   += job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
            End !If job:Invoice_Number_Warranty <> 0
        Else !If job:Warranty_Job = 'YES' and job:Exchange_Unit_Number <> 0
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End !If job:Warranty_Job = 'YES' and job:Exchange_Unit_Number <> 0
        !Warranty Invoiced, but job not sent to ARC... show RRC costs as well
            !For warranty parts the sale cost = in warranty - L871 (DBH: 11-07-2003)
        If job:Warranty_Job = 'YES'
            If local.SentToHub() = Level:Benign
                If job:Invoice_Number_Warranty <> 0
                    !RRC Warranty Labour
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWLabourCost,@n_14.2))
                    tmp:Total[19]   += jobe:InvRRCWLabourCost
                    !RRC Warranty Labour VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100,@n_14.2))
                    tmp:Total[20]   += jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100
                    !RRC Warranty Parts Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarRRCAverageTotal,@n_14.2))
                    tmp:Total[21]   += tmp:WarRRCAverageTotal
                    !RRC Warranty Sale Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWPartsCost,@n_14.2))
                    tmp:Total[22]   += jobe:InvRRCWPartsCost
                    !RRC Warranty Sale Cost VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100,@n_14.2))
                    tmp:Total[23]   += jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100
                Else !If job:Invoice_Number_Warranty <> 0
                    !RRC Warranty Labour
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWLabourCost,@n_14.2))
                    tmp:Total[19]   += jobe:RRCWLabourCost
                    !RRC Warranty Labour VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWLabourCost * VatRate(job:Account_Number,'L')/ 100,@n_14.2))
                    tmp:Total[20]   += jobe:RRCWLabourCost * VatRate(job:Account_Number,'L')/ 100
                    !RRC Warranty Parts Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarRRCAverageTotal,@n_14.2))
                    tmp:Total[21]   += tmp:WarRRCAverageTotal
                    !RRC Warranty Sale Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWPartsCost,@n_14.2))
                    tmp:Total[22]   += jobe:RRCWPartsCost
                    !RRC Warranty Sale Cost VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWPartsCost * VatRate(job:Account_Number,'P') / 100,@n_14.2))
                    tmp:total[23]   += jobe:RRCWPartsCost * VatRate(job:Account_Number,'P') / 100
                End !If job:Invoice_Number_Warranty <> 0
            Else !If local:SentToHub() = Level:Benign
                Loop x# = 1 To 5
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
                End !Loop x# = 1 To 5
            End !If local:SentToHub() = Level:Benign
        Else !If job:Warranty_Job = 'YES'
            Loop x# = 1 To 5
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            End !Loop x# = 1 To 5
        End !If job:Warranty_Job = 'YES'

        !Warranty Invoiced.. show ARC costs
        If job:Warranty_Job = 'YES'
            !Show the claims costs not the ARC costs - L871 (DBH: 11-07-2003)
            !The sale cost should show the In Warranty Cost - L871 (DBH: 11-07-2003)
            If job:Invoice_Number_Warranty <> 0
                !ARC Warranty Labour
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvoiceClaimValue,@n_14.2))
                tmp:Total[24]   += jobe:InvoiceClaimValue
                !ARC Warranty Labour VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvoiceClaimValue * inv:Vat_Rate_Labour/100,@n_14.2))
                tmp:Total[25]   += jobe:InvoiceClaimValue * inv:Vat_Rate_Labour/100
                !ARC Warranty Parts Cost
                    !Show the ARC Purchase Price but
                    !the total ARC/RRC Claim Parts Total - L794 (DBH: 15-07-2003)
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarARCPurchaseTotal,@n_14.2))
                tmp:Total[26]   += tmp:WarARCPurchaseTotal
                !ARC Warranty Sale Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvClaimPartsCost,@n_14.2))
                tmp:Total[27]   += jobe:InvClaimPartsCost
                !ARC Warranty Sale Cost VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvClaimPartsCost * inv:Vat_Rate_Parts/100,@n_14.2))
                tmp:Total[28]   += jobe:InvClaimPartsCost * inv:Vat_Rate_Parts/100
                !ARC Warranty Labour Paid By Manufacturer
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:LabourAdjustment,@n_14.2))
                tmp:Total[29]   += jobe:LabourAdjustment
                !ARC Warranty Labour Paid By Manufacturer VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:LabourAdjustment * inv:Vat_Rate_Labour/100,@n_14.2))
                tmp:Total[30]   += jobe:LabourAdjustment * inv:Vat_Rate_Labour/100
                !ARC Warranty Parts Paid By Manufacturer
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarCostAdjustment,@n_14.2))
                tmp:Total[31]   += tmp:WarCostAdjustment
                !ARC Warranty Parts Paid By Manufacturer VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarCostAdjustment * inv:Vat_Rate_Parts/100,@n_14.2))
                tmp:Total[32]   += tmp:WarCostAdjustment * inv:Vat_Rate_Parts/100
                !Warranty Claim Date Paid
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(inv:Date_Created,@d06b))

            Else !If job:Invoice_Number_Warranty <> 0
                !ARC Warranty Labour
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ClaimValue,@n_14.2))
                tmp:Total[24]   += jobe:ClaimValue
                !ARC Warranty Labour VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ClaimValue * VatRate(job:Account_Number,'L')/100,@n_14.2))
                tmp:Total[25]   += jobe:ClaimValue * VatRate(job:Account_Number,'L')/100
                !ARC Warranty Parts Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarARCPurchaseTotal,@n_14.2))
                tmp:Total[26]   += tmp:WarARCPurchaseTotal
                !Arc Warranty Parts Sale Cost
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ClaimPartsCost,@n_14.2))
                tmp:Total[27]   += jobe:ClaimPartsCost
                !ARC Warranty Parts Sale Cost VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ClaimPartsCost * VatRate(job:Account_Number,'P')/100,@n_14.2))
                tmp:total[28]   += jobe:ClaimPartsCost * VatRate(job:Account_Number,'P')/100
                Loop x# = 1 To 5
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')    
                End !Loop x# = 1 To 5
            End !If job:Invoice_Number_Warranty <> 0
        Else !If job:Warranty_Job = 'YES'
            Loop x# = 1 To 10
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            End !Loop x# = 1 To 10
        End !If job:Warranty_Job = 'YES'
        
        !ARC Charged By 3rd Party                    
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ARC3rdPartyCost,@n_14.2))
        tmp:Total[33]   += jobe:ARC3rdPartyCost
        !ARC Charged By 3rd Party VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ARC3rdPartyVAT,@n_14.2))
        tmp:Total[34]   += jobe:ARC3rdPartyVAT
        !3rd Party Invoice Number
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(jobe:ARC3rdPartyInvoiceNumber)
        !3rd Party Site
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Third_Party_Site)
        !Refurb Labour
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        !Refurb VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        !Refurb Parts
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        !Refurb Parts Sale
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        !Refurb Parts VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
    !_____________________________________________________________________

    Else !If tmp:HeadAccount = 'HEAD'
    !_____________________________________________________________________

        !RRC Chargeable Total VAT
        If job:Chargeable_Job = 'YES'
            If local:RRCInvoiced
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip((jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100))
                tmp:Total[9]    += (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour / 100) + (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts / 100)
                !Lost Loan Fee
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Courier_Cost,@n_14.2))
                tmp:Total[10]   += job:Invoice_Courier_Cost
                !Lost Loan Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100))
                tmp:Total[11]   += job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100

            Else !If local:RRCInvoiced
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip((jobe:RRCCLabourCost * VatRate(job:Account_Number,'L') / 100) + (jobe:RRCCPartsCost * VatRate(job:Account_Number,'P') / 100))
                tmp:Total[9]    += (jobe:RRCCLabourCost * VatRate(job:Account_Number,'L') / 100) + (jobe:RRCCPartsCost * VatRate(job:Account_Number,'P') / 100)
                !Lost Loan Fee
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost,@n_14.2))
                tmp:Total[10]   += job:Courier_Cost
                !Lost Loan Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost * VatRate(job:Account_Number,'L')/100))
                tmp:Total[11]   += job:Courier_Cost * VatRate(job:Account_Number,'L')/100

            End !If local:RRCInvoiced
        Else !If job:Chargeble_Job = 'YES'
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End !If job:Chargeble_Job = 'YES'

        !Lost Loan Fee
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        tmp:Total[12]   += 0
        !Lost Loan Fee VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        tmp:Total[13]   += 0
        !Manufacturer Exchange Fee
            !Replicate change made to ARC - L871 (DBH: 11-07-2003)
        If job:Warranty_Job = 'YES' and job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
            If job:Invoice_Number_Warranty <> 0
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:WInvoice_Courier_Cost,@n_14.2))
                tmp:Total[14]   += job:WInvoice_Courier_Cost
                !Manufacturer Exchange Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100,@n_14.2))
                tmp:Total[15]   += job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
            Else !If job:Invoice_Number_Warranty <> 0
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost_Warranty,@n_14.2))
                tmp:Total[14]   += job:Courier_Cost_Warranty
                !Manufacturer Exchange Fee VAT
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100,@n_14.2))
                tmp:Total[15]   += job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
            End !If job:Invoice_Number_Warranty <> 0
        Else !If job:Warranty_Job = 'YES' and job:Exchange_Unit_Number <> 0
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        End !If job:Warranty_Job = 'YES' and job:Exchange_Unit_Number <> 0
        !Manufacturer Exchange Fee
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        tmp:Total[16]   += 0
        !Manufacturer Exchange Fee VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        tmp:Total[17]   += 0
        !Warranty Invoiced, but job not sent to ARC... show RRC costs as well
        If job:Warranty_Job = 'YES'
            !Replicate changes made to ARC version - L871 (DBH: 11-07-2003)
            If local.SentToHub() = Level:Benign
                If job:Invoice_Number_Warranty <> 0
                    !RRC Warranty Labour
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWLabourCost,@n_14.2))
                    tmp:Total[18]   += jobe:InvRRCWLabourCost
                    !RRC Warranty Labour VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100,@n_14.2))
                    tmp:Total[19]   += jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour / 100
                    !RRC Warranty Parts Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarRRCAverageTotal,@n_14.2))
                    tmp:Total[20]   += tmp:WarRRCAverageTotal
                    !RRC Warranty Sale Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWPartsCost,@n_14.2))
                    tmp:Total[21]   += jobe:InvRRCWPartsCost
                    !RRC Warranty Sale Cost VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100,@n_14.2))
                    tmp:Total[22]   += jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts / 100
                Else !If job:Invoice_Number_Warranty <> 0
                    !RRC Warranty Labour
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWLabourCost,@n_14.2))
                    tmp:Total[18]   += jobe:RRCWLabourCost
                    !RRC Warranty Labour VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWLabourCost * VatRate(job:Account_Number,'L')/ 100,@n_14.2))
                    tmp:Total[19]   += jobe:RRCWLabourCost * VatRate(job:Account_Number,'L')/ 100
                    !RRC Warranty Parts Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(tmp:WarRRCAverageTotal,@n_14.2))
                    tmp:Total[20]   += tmp:WarRRCAverageTotal
                    !RRC Warranty Sale Cost
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWPartsCost,@n_14.2))
                    tmp:Total[21]   += jobe:RRCWPartsCost
                    !RRC Warranty Sale Cost VAT
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:RRCWPartsCost * VatRate(job:Account_Number,'P') / 100,@n_14.2))
                    tmp:total[22]   += jobe:RRCWPartsCost * VatRate(job:Account_Number,'P') / 100
                End !If job:Invoice_Number_Warranty <> 0
            Else !If local:SentToHub() = Level:Benign
                Loop x# = 1 To 5
                    orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
                End !Loop x# = 1 To 5
            End !If local:SentToHub() = Level:Benign
        Else !If job:Warranty_Job = 'YES'
            Loop x# = 1 To 5
                orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
            End !Loop x# = 1 To 5
        End !If job:Warranty_Job = 'YES'

        !ARC Charged By 3rd Party                    
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ARC3rdPartyCost,@n_14.2))
        tmp:Total[25]   += jobe:ARC3rdPartyCost
        !ARC Charged By 3rd Party VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ARC3rdPartyVAT,@n_14.2))
        tmp:Total[26]   += jobe:ARC3rdPartyVAT
        !ARC Third party Markup
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ARC3rdPartyMarkup,@n_14.2))
        tmp:Total[27]   += jobe:ARC3rdPartyMarkup
        !ARC Third Party markup VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        tmp:Total[28]   += 0
        !ARC Third Party Total
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(Format(jobe:ARC3rdPartyCost + jobe:ARC3rdPartyVAT,@n_14.2))
        tmp:Total[29]   += jobe:ARC3rdPartyCost + jobe:ARC3rdPartyVAT
        !ARC Third Party Total VAT
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip('')
        tmp:Total[30]   += 0
        !3rd Party Invoice Number
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(jobe:ARC3rdPartyInvoiceNumber)
        !3rd Party Site
        orcexp:Line1    = Clip(orcexp:Line1) & '","' & Clip(job:Third_Party_Site)
    End !If tmp:HeadAccount = 'HEAD'
    !_____________________________________________________________________


    orcexp:Line1 = Clip(orcexp:Line1) & '"'
    Add(OracleExport)
    If Error()
        Stop(Error())
    End !If Error()

    !_____________________________________________________________________


    !Write to control file
    !Has this entry already been included on an oracle export?
    If tmp:InvoiceType = 'X'
        Access:ORACLECN.Clearkey(orac:AuditRecordNumberKey)
        orac:AuditRecordNumber  = aud:Record_Number
        If Access:ORACLECN.Tryfetch(orac:AuditRecordNumberKey) = Level:Benign
            !Found
            orac:OracleNumber   = tmp:OracleNumber
        Else ! If Access:ORACLECN.Tryfetch(orac:AuditRecordNumberKey) = Level:Benign
            !Error
            If Access:ORACLECN.PrimeRecord() = Level:Benign
                orac:OracleNumber   = tmp:OracleNumber
                orac:AuditRecordNumber  = aud:Record_Number
                If Access:ORACLECN.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:ORACLECN.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:ORACLECN.CancelAutoInc()
                End !If Access:ORACLECN.TryInsert() = Level:Benign
            End !If Access:ORACLECN.PrimeRecord() = Level:Benign
        End !If Access:ORACLECN.Tryfetch(orac:AuditRecordNumberKey) = Level:Benign
    End !If tmp:InvoiceType = 'X'

    trail:Count += 1

    Prog.ProgressText('Found Jobs: ' & trail:Count)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
BrowseOracleFile PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:User             STRING(60)
save_inv_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
pos                  STRING(255)
BRW1::View:Browse    VIEW(ORACLEEX)
                       PROJECT(ora:RecordNumber)
                       PROJECT(ora:TheDate)
                       PROJECT(ora:TheTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ora:RecordNumber       LIKE(ora:RecordNumber)         !List box control field - type derived from field
ora:TheDate            LIKE(ora:TheDate)              !List box control field - type derived from field
ora:TheTime            LIKE(ora:TheTime)              !List box control field - type derived from field
tmp:User               LIKE(tmp:User)                 !List box control field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Finance Export Files'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Finance Export File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,344,182),USE(?Browse:1),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('56L(2)|M~File Number~@s8@51R(2)|M~Date Created~C(0)@d6@50R(2)|M~Time Created~C(0' &|
   ')@t1b@240L(2)|M~User~C(0)@s60@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By File Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(168,98,64,10),USE(ora:RecordNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Record Number'),TIP('Record Number'),UPR
                           BUTTON,AT(168,298),USE(?CreateNewExport:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(448,298),USE(?CreateNewExport),TRN,FLAT,LEFT,ICON('creexpp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020564'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseOracleFile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:INVOICE.Open
  Relate:ORACLECN.Open
  Relate:WEBJOB.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORACLEEX,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseOracleFile')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ora:RecordNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ora:RecordNumber,ora:RecordNumber,1,BRW1)
  BIND('tmp:User',tmp:User)
  BRW1.AddField(ora:RecordNumber,BRW1.Q.ora:RecordNumber)
  BRW1.AddField(ora:TheDate,BRW1.Q.ora:TheDate)
  BRW1.AddField(ora:TheTime,BRW1.Q.ora:TheTime)
  BRW1.AddField(tmp:User,BRW1.Q.tmp:User)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:ORACLECN.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseOracleFile')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020564'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020564'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020564'&'0')
      ***
    OF ?CreateNewExport:2
      ThisWindow.Update
      Case Missive('If you remove this file then all the jobs that were attached to it will be picked up on the next Oracle file.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to delete this file?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              Access:ORACLEEX.Clearkey(ora:RecordNumberKey)
              ora:RecordNumber    = brw1.q.ora:RecordNumber
              If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                  !Found
                  Relate:ORACLEEX.Delete(0)
              Else ! If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
          Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
    OF ?CreateNewExport
      ThisWindow.Update
      CreateExport
      ThisWindow.Reset
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = ora:UserCode
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:User = Clip(use:Forename) & ' ' & Clip(Use:Surname)
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  PARENT.SetQueueRecord


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
CreateSparesExport PROCEDURE                          !Generated from procedure template - Window

Stock_Group          QUEUE,PRE()
SiteLocation         STRING(30)
Stock_Decremented_Audit REAL
Stock_Decremented    REAL
Stock_Added_Audit    REAL
Stock_Added          REAL
                     END
save_inv_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_man_id          USHORT,AUTO
save_res_id          USHORT,AUTO
save_ret_id          USHORT,AUTO
savepath             STRING(255)
pos                  STRING(255)
tmp:Start_Date       DATE
tmp:End_Date         DATE
tmp:OracleExport     STRING(255),STATIC
tmp:ChaAverageTotal  REAL
tmp:ChaPurchaseTotal REAL
tmp:WarOutWarrantyTotal REAL
tmp:WarSaleTotal     REAL
tmp:InvoiceType      STRING(1)
tmp:ExportPath       STRING(255)
tmp:WarCostAdjustment REAL
tmp:OracleNumber     LONG
tmp:ChaARCAverageTotal REAL
tmp:WarAverageTotal  REAL
tmp:WarRRCAverageTotal REAL
TrailerGroup         GROUP,PRE(trail)
Count                LONG
CostTotal            REAL
SaleTotal            REAL
VatTotal             REAL
                     END
Tmp:GRN_Total_Amount REAL
AppFrame             WINDOW('Oracle Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Oracle Spares Export'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('creexpp.jpg')
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Export all Retail Invoices between the selected dates'),AT(177,154),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Create File In Folder'),AT(177,170),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(273,170,200,10),USE(tmp:ExportPath),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(477,166),USE(?LookupExportPath),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Start Date'),AT(177,190),USE(?StrStartDate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(273,190,64,10),USE(tmp:Start_Date),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Invoice Date'),TIP('Invoice Date'),REQ,UPR
                           BUTTON,AT(341,186),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           STRING('End Date'),AT(177,210),USE(?StrEndDate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(273,210,64,10),USE(tmp:End_Date),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(341,208),USE(?LookupEndDate),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup8          SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
OracleExport    File,Driver('BASIC', '/FIELDDELIMITER = 1,124'),Pre(orcexp),Name(tmp:OracleExport),Create,Bindable,Thread
Record                  Record
Transaction_Type    STRING(255)
Transaction_Number  STRING(20)
Transaction_Date    STRING(11)
Franchise_Number    STRING(20)
AccountNumber       String(30)
AccountName         String(30)
CostOfSaleAmount    String(30)
Total_Invoice_Amnt  STRING(20)
Tax_Code            STRING(20)
Tax_Amount          STRING(20)
                    End
                 End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

HeaderLine      Routine
    Clear(orcexp:Record)
    orcexp:Transaction_Type     = 'HEADER'
    orcexp:Transaction_Number   = tmp:OracleNumber
    orcexp:Transaction_Date     = Format(Today(),@d06)
    Add(OracleExport)
FooterLine      Routine
    Clear(orcexp:Record)
    orcexp:Transaction_Type         = 'TRAILER'
    orcexp:Transaction_Number       = trail:Count
    orcexp:Transaction_Date         = ''
    orcexp:Franchise_Number         = ''
    orcexp:AccountNumber            = ''
    orcexp:AccountName              = ''
    orcexp:CostOfSaleAmount         = trail:CostTotal
    orcexp:Total_Invoice_Amnt       = trail:SaleTotal
    orcexp:Tax_Code                 = ''
    orcexp:Tax_Amount               = trail:VATTotal
    !-=-                            =
    Add(OracleExport)
TitleLine       Routine
    Clear(orcexp:Record)
    orcexp:Transaction_Type   = 'Transaction Type'
    orcexp:Transaction_Number = 'Transaction Number'
    orcexp:Transaction_Date   = 'Transaction Date'
    orcexp:Franchise_Number   = 'Franchise Number'
    orcexp:AccountNumber      = 'Account Number'
    orcexp:AccountName        = 'Account Name'
    orcexp:CostOfSaleAmount   = 'Cost Of Sale Amount'
    orcexp:Total_Invoice_Amnt = 'Invoice Amount'
    orcexp:Tax_Code           = 'VAT Code'
    orcexp:Tax_Amount         = 'VAT Amount'
    Add(OracleExport)
ExportSpares      Routine
  CLEAR(orcexp:Record)
  orcexp:Transaction_Type = 'STOCK ADDED'
  orcexp:Franchise_Number = Stock_Group.SiteLocation
  orcexp:Total_Invoice_Amnt = Stock_Group.Stock_Added
  ADD(OracleExport)
  trail:Count+=1
  CLEAR(orcexp:Record)
  orcexp:Transaction_Type = 'STOCK DECREMENTED'
  orcexp:Franchise_Number = Stock_Group.SiteLocation
  orcexp:Total_Invoice_Amnt = Stock_Group.Stock_Decremented
  ADD(OracleExport)
  trail:Count+=1
  CLEAR(orcexp:Record)
  orcexp:Transaction_Type = 'STOCK ADDED DURING AUDIT'
  orcexp:Franchise_Number = Stock_Group.SiteLocation
  orcexp:Total_Invoice_Amnt = Stock_Group.Stock_Added_Audit
  ADD(OracleExport)
  trail:Count+=1
  CLEAR(orcexp:Record)
  orcexp:Transaction_Type = 'STOCK DECREMENTED DURING AUDIT'
  orcexp:Franchise_Number = Stock_Group.SiteLocation
  orcexp:Total_Invoice_Amnt = Stock_Group.Stock_Decremented_Audit
  ADD(OracleExport)
  trail:Count+=1
ExportRetail    ROUTINE
Data
local:PartsCost     Real
local:PartsSale     Real
Code
    CLEAR(orcexp:Record)
    orcexp:Transaction_Type = 'Invoice'

    orcexp:Transaction_Number = inv:Invoice_Number
    orcexp:Transaction_Date = FORMAT(inv:Date_Created,@D06b)

    !Find the account that is sale was made for

    orcexp:Tax_Code = 'VAT 14'
    Access:TRADEACC.ClearKey(tra:StoresAccountKey)
    tra:StoresAccount = inv:Account_Number
    If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
        !Found
        orcexp:Franchise_Number = tra:BranchIdentification
        orcexp:AccountNumber    = tra:Account_Number
        orcexp:AccountName      = tra:Company_Name
    Else!If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
        orcexp:Franchise_Number = '???'
        orcexp:AccountNumber = '???'
        orcexp:AccountName  = '???'
    End!If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign

    local:PartsCost = 0
    local:PartsSale = 0

    Save_ret_ID = Access:RETSALES.SaveFile()
    Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
    ret:Invoice_Number = inv:Invoice_Number
    Set(ret:Invoice_Number_Key,ret:Invoice_Number_Key)
    Loop
        If Access:RETSALES.NEXT()
           Break
        End !If
        If ret:Invoice_Number <> inv:Invoice_Number      |
            Then Break.  ! End If
        Save_res_ID = Access:RETSTOCK.SaveFile()
        Access:RETSTOCK.ClearKey(res:Despatched_Only_Key)
        res:Ref_Number = ret:Ref_Number
        res:Despatched = 'YES'
        Set(res:Despatched_Only_Key,res:Despatched_Only_Key)
        Loop
            If Access:RETSTOCK.NEXT()
               Break
            End !If
            If res:Ref_Number <> ret:Ref_Number      |
            Or res:Despatched <> 'YES'      |
                Then Break.  ! End If
            local:PartsCost += res:Purchase_Cost * res:Quantity
            local:PartsSale  += res:Sale_Cost * res:Quantity
        End !Loop
        Access:RETSTOCK.RestoreFile(Save_res_ID)
    End !Loop
    Access:RETSALES.RestoreFile(Save_ret_ID)

    orcexp:CostOfSaleAmount     = Format(local:PartsCost,@n_14.2)
    orcexp:Total_Invoice_Amnt   = Format(inv:Total,@n_14.2)

    orcexp:Tax_Amount = FORMAT((inv:Total/100)*inv:Vat_Rate_Retail,@n_14.2)
    ADD(OracleExport)
    trail:Count+=1
    trail:CostTotal += orcexp:CostOfSaleAmount
    trail:SaleTotal += orcexp:Total_Invoice_Amnt
    trail:VATTotal += orcexp:Tax_Amount
ExportGRN   ROUTINE
  !Get all the ordparts on the note!
  Tmp:GRN_Total_Amount = 0
  Access:OrdParts.ClearKey(orp:Order_Number_Key)
  orp:Order_Number = grn:Order_Number
  SET(orp:Order_Number_Key,orp:Order_Number_Key)
  LOOP
    IF Access:OrdParts.Next()
      BREAK
    END
    IF orp:Order_Number <> grn:Order_Number
      BREAK
    END
    IF orp:GRN_Number <> grn:Goods_Received_Number
      CYCLE
    END
    !Add up the total?
    Tmp:GRN_Total_Amount += orp:Purchase_Cost * orp:Number_Received
  END
  CLEAR(orcexp:Record)
  orcexp:Transaction_Type = 'ORDER/GRN DATA'

  orcexp:Transaction_Number = grn:Goods_Received_Number
  orcexp:Transaction_Date = FORMAT(grn:Goods_Received_Date,@D8)


  Access:Orders.ClearKey(ord:Order_Number_Key)
  ord:Order_Number = grn:Order_Number
  IF Access:Orders.Fetch(ord:Order_Number_Key)
    !Error!
    orcexp:Franchise_Number = '???'
  ELSE
    Access:Supplier.ClearKey(sup:Company_Name_Key)
    sup:Company_Name = ord:Supplier
    IF Access:Supplier.Fetch(sup:Company_Name_Key)
      !Error!
      orcexp:Franchise_Number = '???'
    ELSE
      orcexp:Franchise_Number = sup:Account_Number
    END
  END
  orcexp:Tax_Code =  ''
  orcexp:Total_Invoice_Amnt = Tmp:GRN_Total_Amount
  orcexp:Tax_Amount = ''
  ADD(OracleExport)
  trail:Count+=1

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020567'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, AppFrame, 1)  !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CreateSparesExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:GRNOTES.Open
  Relate:INVOICE.Open
  Relate:ORACLEEX.Open
  Relate:RETSALES.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  Access:STOHIST.UseFile
  Access:STOCK.UseFile
  Access:SUBTRACC.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDERS.UseFile
  Access:SUPPLIER.UseFile
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  tmp:OracleExport = Upper(GETINI('ORACLE','ExportPath',,CLIP(PATH())&'\SB2KDEF.INI'))
  tmp:Start_Date = Today()
  tmp:End_Date = Today()
  OPEN(AppFrame)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','CreateSparesExport')
  Bryan.CompFieldColour()
  ?tmp:Start_Date{Prop:Alrt,255} = MouseLeft2
  ?tmp:End_Date{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup8.Init
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:LongName)
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:Directory)
  FileLookup8.SetMask('All Files','*.*')
  FileLookup8.WindowTitle='Export Path'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:GRNOTES.Close
    Relate:INVOICE.Close
    Relate:ORACLEEX.Close
    Relate:RETSALES.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CreateSparesExport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020567'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020567'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020567'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      !Get the Oracle Number
      tmp:OracleNumber = 0
      
      If Access:ORACSPEX.PrimeRecord() = Level:Benign
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              ora1:UserCode    = use:User_Code
          Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
          End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          If Access:ORACSPEX.TryInsert() = Level:Benign
              !Insert Successful
              tmp:OracleNumber = ora1:RecordNumber
          Else !If Access:ORACLEEX.TryInsert() = Level:Benign
              !Insert Failed
              Access:ORACSPEX.CancelAutoInc()
          End !If Access:ORACLEEX.TryInsert() = Level:Benign
      End !If Access:ORACLEEX.PrimeRecord() = Level:Benign
      
      If tmp:OracleNumber <> 0
      
          savepath = path()
          If Sub(Clip(tmp:ExportPath),-1,1) = '\'
              tmp:OracleExport = Clip(tmp:ExportPath) & 'sb_spares_head_' & Format(Day(Today()),@n02) & '_' & Format(Month(Today()),@n02) & '_' & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.dat'
          Else !Sub(tmp:ExportPath,-1,1) = '\'
              tmp:OracleExport = Clip(tmp:ExportPath) & '\sb_spares_head_' & Format(Day(Today()),@n02) & '_' & Format(Month(Today()),@n02) & '_' & Format(Year(today()),@n04) & Format(Clock(),@t2) & '.dat'
          End !Sub(tmp:ExportPath,-1,1) = '\'
      
          Create(OracleExport)
          Open(OracleExport)
          If Error()
              Stop(Error())
          Else
              !Quick record count
              Prog.ProgressSetup(Records(StoHist)+RECORDS(Invoice))
              Do TitleLine
      !        Do HeaderLine
      
              Access:Invoice.ClearKey(inv:Date_Created_Key)
              inv:Date_Created = tmp:Start_Date
              SET(inv:Date_Created_Key,inv:Date_Created_Key)
              LOOP
                IF Access:Invoice.Next()
                  BREAK
                END
                IF inv:Date_Created > tmp:End_Date
                  BREAK
                END
                IF inv:Invoice_Type <> 'RET'
                  CYCLE
                END
                !Only include invoices that haven't already been put on
                !an Oracle Export
                  If inv:OracleNumber <> 0
                      Cycle
                  End !If inv:OracleNumber <> 0
      
                DO ExportRetail
      
                  inv:ExportedOracleDate = Today()
                  inv:ExportedARCOracle   = 1
                  inv:OracleNumber        = tmp:OracleNumber
                  Access:INVOICE.TryUpdate()
      
      
              END
      
      !        Access:GRNotes.ClearKey(grn:Goods_Received_Number_Key)
      !        SET(grn:Goods_Received_Number_Key,grn:Goods_Received_Number_Key)
      !        LOOP
      !          IF Access:GRNOtes.Next()
      !            BREAK
      !          END
      !          IF grn:Goods_Received_Date < tmp:Start_Date
      !            CYCLE
      !          END
      !          IF grn:Goods_Received_Date > tmp:End_Date
      !            CYCLE
      !          END
      !          DO ExportGRN
      !        END
      !
      !        !Stock History!
      !
      !        Access:StoHist.ClearKey(shi:DateKey)
      !        shi:Date = Tmp:Start_Date
      !        SET(shi:DateKey,shi:DateKey)
      !        LOOP
      !          IF Access:StoHist.Next()
      !            BREAK
      !          END
      !          IF Shi:Date > Tmp:End_Date
      !            BREAK
      !          END
      !          If Prog.InsideLoop()
      !            Break
      !          End !If Prog.InsideLoop()
      !          !Get totals!
      !          !Get Stock location!
      !          Access:Stock.ClearKey(sto:Ref_Number_Key)
      !          sto:Ref_Number = shi:Ref_Number
      !          IF Access:Stock.Fetch(sto:Ref_Number_Key)
      !            !Error!
      !            CYCLE
      !          ELSE
      !            SORT(Stock_Group,Stock_Group.SiteLocation)
      !            Stock_Group.SiteLocation = sto:Location
      !            GET(Stock_Group,Stock_Group.SiteLocation)
      !            IF ERROR()
      !              Stock_Group.SiteLocation = sto:Location
      !              Stock_Group.Stock_Decremented_Audit = 0
      !              Stock_Group.Stock_Decremented = 0
      !              Stock_Group.Stock_Added_Audit = 0
      !              Stock_Group.Stock_Added = 0
      !              ADD(Stock_Group)
      !            END
      !          END
      !          IF shi:Transaction_Type = 'ADD'
      !            IF shi:Job_Number = 0
      !              IF shi:Notes = 'STOCK ADDED'
      !                !This is an add in!
      !                IF INSTRING('STOCK AUDIT',shi:Despatch_Note_Number,1,1)
      !                  Stock_Group.Stock_Added_Audit += shi:Purchase_Cost * shi:Quantity
      !                  PUT(Stock_Group)
      !                ELSE
      !                  Stock_Group.Stock_Added += shi:Purchase_Cost * shi:Quantity
      !                  PUT(Stock_Group)
      !                END
      !              END
      !            END
      !          END
      !          IF shi:Transaction_Type = 'DEC'
      !            IF shi:Job_Number = 0
      !              IF shi:Notes = 'STOCK DECREMENTED'
      !                !This is an add in!
      !                IF INSTRING('STOCK AUDIT',shi:Despatch_Note_Number,1,1)
      !                  Stock_Group.Stock_Decremented_Audit += shi:Purchase_Cost * shi:Quantity
      !                  PUT(Stock_Group)
      !                ELSE
      !                  Stock_Group.Stock_Decremented+=shi:Purchase_Cost * shi:Quantity
      !                  PUT(Stock_Group)
      !                END
      !              END
      !            END
      !          END
      !        END
      !        LOOP x# = 1 TO RECORDS(Stock_Group)
      !          GET(Stock_Group,x#)
      !          Do ExportSpares
      !        END
             
              Prog.ProgressFinish()
              Do FooterLine
      
              Close(OracleExport)
      
              If trail:Count = 0
                  Case Missive('There are no records that match the selected criteria.','ServiceBase 3g',|
                                 'mexclam.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
                  Access:ORACLEEX.Clearkey(ora:RecordNumberKey)
                  ora:RecordNumber    = tmp:OracleNumber
                  If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                      !Found
                      Delete(ORACLEEX)
                  Else ! If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
              Else !If trail:Count = 0
                  Case Missive('Export Completed.'&|
                    '<13,10>File Created:'&|
                    '<13,10>'&|
                    '<13,10>' & Clip(tmp:OracleExport) & '.','ServiceBase 3g',|
                                 'mexclam.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
              End !If trail:Count = 0
          End !If Error()
      
      
      
      End !tmp:OracleNumer <> 0
    OF ?LookupExportPath
      ThisWindow.Update
      tmp:ExportPath = Upper(FileLookup8.Ask(1)  )
      DISPLAY
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:Start_Date = TINCALENDARStyle1(tmp:Start_Date)
          Display(?tmp:Start_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:End_Date = TINCALENDARStyle1(tmp:End_Date)
          Display(?tmp:End_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:Start_Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:End_Date
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
BrowseSparesFile PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:User             STRING(60)
save_inv_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
pos                  STRING(255)
BRW1::View:Browse    VIEW(ORACSPEX)
                       PROJECT(ora1:RecordNumber)
                       PROJECT(ora1:TheDate)
                       PROJECT(ora1:TheTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ora1:RecordNumber      LIKE(ora1:RecordNumber)        !List box control field - type derived from field
ora1:TheDate           LIKE(ora1:TheDate)             !List box control field - type derived from field
ora1:TheTime           LIKE(ora1:TheTime)             !List box control field - type derived from field
tmp:User               LIKE(tmp:User)                 !List box control field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('BrowseSpares Export Files'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Spares Export File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,344,184),USE(?Browse:1),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('56L(2)|M~File Number~@s8@51R(2)|M~Date Created~C@d6@50R(2)|M~Time Created~C(0)@t' &|
   '1b@240L(2)|M~User~C(0)@s60@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By File Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(168,98,64,10),USE(ora1:RecordNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Record Number'),TIP('Record Number'),UPR
                           BUTTON,AT(168,298),USE(?CreateNewExport:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(448,298),USE(?CreateNewExport),TRN,FLAT,LEFT,ICON('creexpp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020565'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseSparesFile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:INVOICE.Open
  Relate:ORACSPEX.Open
  Relate:WEBJOB.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORACSPEX,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseSparesFile')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ora1:RecordNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ora1:RecordNumber,ora1:RecordNumber,1,BRW1)
  BIND('tmp:User',tmp:User)
  BRW1.AddField(ora1:RecordNumber,BRW1.Q.ora1:RecordNumber)
  BRW1.AddField(ora1:TheDate,BRW1.Q.ora1:TheDate)
  BRW1.AddField(ora1:TheTime,BRW1.Q.ora1:TheTime)
  BRW1.AddField(tmp:User,BRW1.Q.tmp:User)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:ORACSPEX.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseSparesFile')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020565'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020565'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020565'&'0')
      ***
    OF ?CreateNewExport:2
      ThisWindow.Update
              Access:ORACSPEX.Clearkey(ora1:RecordNumberKey)
              ora1:RecordNumber    = brw1.q.ora1:RecordNumber
              If Access:ORACSPEX.Tryfetch(ora1:RecordNumberKey) = Level:Benign
                  !Found
                  Save_inv_ID = Access:INVOICE.SaveFile()
                  Access:INVOICE.ClearKey(inv:OracleNumberKey)
                  inv:OracleNumber   = ora1:RecordNumber
                  Set(inv:OracleNumberKey,inv:OracleNumberKey)
                  Loop
                      If Access:INVOICE.NEXT()
                         Break
                      End !If
                      If inv:OracleNumber   <> ora1:RecordNUmber      |
                          Then Break.  ! End If
                      
                      If inv:Invoice_Type = 'RET'
                          pos = Position(inv:OracleNUmberKey)
                          inv:OracleNumber = 0
                          inv:ExportedARCOracle = 0
                          inv:ExportedOracleDate= 0
                          Access:INVOICE.Update()
                          Reset(inv:OracleNumberKey,pos)
                      End !If inv:Invoice_Type = 'RET'
                  End !Loop
                  Access:INVOICE.RestoreFile(Save_inv_ID)
                  Delete(ORACSPEX)
              Else ! If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:ORACLEEX.Tryfetch(ora:RecordNumberKey) = Level:Benign
      BRW1.ResetSort(1)
    OF ?CreateNewExport
      ThisWindow.Update
      CreateSparesExport
      ThisWindow.Reset
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = ora1:UserCode
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:User = Clip(use:Forename) & ' ' & Clip(Use:Surname)
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

XFiles PROCEDURE                                      !Generated from procedure template - Window

window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','XFiles')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','XFiles')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
