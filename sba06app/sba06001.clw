

   MEMBER('sba06app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA06001.INC'),ONCE        !Local module procedure declarations
                     END


Line500_XML_OLD      PROCEDURE  (f:ID)                ! Declare Procedure
seq                  LONG
RepositoryDir        CSTRING('C:\ServiceBaseMQ\MQ_Repository<0>{224}')
save_invoice_id      USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_subtracc_id     USHORT,AUTO
tmp:STFMessage       STRING(30)
tmp:AOWMessage       STRING(30)
tmp:RIVMessage       STRING(30)
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
XmlData group,type
ServiceCode string(3)
REC         group
SB_PO_NO          string(10)
ORACLE_PO_NO      string(10)
URL               string(256)
TYPE              string(3)
            end
AOW         group,over(REC)
TRANS_DESC       string(50)
BILL_AC_NO       string(10)
SB_REPAIR_NO     string(20)
SB_INV_NO        string(10)
AMT_EXCL_VAT     string(19)
VAT_RATE         string(19)
COST             string(19)
INV_DATE         string(10)
INV_DUE_DATE     string(10)
DESPATCH_DATE    string(10)
DATE_REQUIRED    string(10)
SERVICE_CODE     string(20)
            end
RIV         group,over(REC)
TRANS_DESC       string(50)
MAIN_RETAIL_AC   String(20)
SUB_RETAIL_AC    String(20)
FRANCHISE_BRANCH String(20)
SB_INV_NO        string(10)
AMT_EXCL_VAT     string(19)
VAT_RATE         string(19)
COST             string(19)
INV_DATE         string(10)
INV_DUE_DATE     string(10)
DESPATCH_DATE    string(10)
DATE_REQUIRED    string(10)
SERVICE_CODE     string(20)
            end
STF         group,over(REC)
TRANS_DESC       string(50)
BILL_AC_NO       string(10)
CUST_ORD_NO      string(20)
SB_INV_NO        string(10)
AMT_EXCL_VAT     string(19)
VAT_RATE         string(19)
COST             string(19)
QTY              string(10)
INV_DATE         string(10)
INV_DUE_DATE     string(10)
DESPATCH_DATE    string(10)
DATE_REQUIRED    string(10)
SERVICE_CODE     string(20)
            end
POR         group,over(REC)
CURRENCY_CODE       string(3)
CURRENCY_UNIT_PRICE string(19)
DELIV_TO_REQ        string(10)
DEST_ORG_ID         string(20)
GL_DATE             string(10)
INTERFACE_SRC       string(20)
ITEM_ID             string(20)  ! Length ?????
ORG_ID              string(2)
PREPARER_ID         string(10)
QTY                 string(10)
SUPPLIER_NUMBER     string(15)
SB_PO_NUMBER        string(10)
TYPE                string(3)
            end
        end

    map
SendXML         procedure(const *XmlData aXmlData, string aRepositoryDir)
    end

XmlValues GROUP(XmlData)
           END
  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
    ! Check default to see if should create messages - TrkBs: 5110 (DBH: 20-07-2005)
    If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        Return
    End ! If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> True

    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    If CreateTestRecord_OLD(RepositoryDir) = False
        Return
    End ! If CreateTestRecord(RepositoryDir) = False

    ! Start - Prime the ServiceCode just incase the defaults haven't been set - TrkBs: 6178 (DBH: 01-08-2005)
    tmp:STFMessage = GETINI('XML','STFDescription',,Clip(Path()) & '\SB2KDEF.INI')
    If tmp:STFMessage = ''
        tmp:STFMessage = 'STF'
    End ! If tmp:STFMessage = ''

    tmp:AOWMessage = GETINI('XML','AOWDescription',,Clip(Path()) & '\SB2KDEF.INI')
    If tmp:AOWMessage = ''
        tmp:AOWMessage = 'AOW'
    End ! If tmp:STFMessage = ''

    tmp:RIVMessage = GETINI('XML','RIVDescription',,Clip(Path()) & '\SB2KDEF.INI')
    If tmp:RIVMessage = ''
        tmp:RIVMessage = 'RIV'
    End ! If tmp:RIVMessage = ''
    ! End   - Prime the ServiceCode just incase the defaults haven't been set - TrkBs: 6178 (DBH: 01-08-2005)

    ! Send XML Message

    ! Manually open Invoice, but assume all other files are open - TrkBs: 5110 (DBH: 29-06-2005)

    Relate:INVOICE.Open()
    save_INVOICE_id = Access:INVOICE.SaveFile()

    Case f:ID
    Of 'STF'
        Do CreateSTF

    Of 'AOW'
        Do CreateAOW

    Of 'RIV'
        Do CreateRIV
    End ! Case f:ID

    Access:INVOICE.RestoreFile(save_INVOICE_id)
    Relate:INVOICE.Close()
CreateSTF       Routine
    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = ret:Invoice_Number
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Error
    End ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    clear(XmlValues)
    XmlValues.ServiceCode       = 'STF'
    XmlValues.STF.TRANS_DESC    = 'Retail Sale To Franchise'
    save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = ret:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Found
        XmlValues.STF.BILL_AC_NO    = Sub(sub:Line500AccountNumber, 1, 10) ! Retail Sale Account Number
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Error
        XmlValues.STF.BILL_AC_NO    = ''
    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
    
    XmlValues.STF.CUST_ORD_NO   = Format(ret:Ref_Number, @n010) ! Retail Sale Number
    XmlValues.STF.SB_INV_NO     = 'Z' & Format(ret:Invoice_Number, @n09) ! Retail Sale Invoice Number
    XmlValues.STF.AMT_EXCL_VAT  = Round(ret:Invoice_Sub_Total, .01) ! Sale Amount (Excl Vat)
    XmlValues.STF.VAT_RATE      = Round(inv:Vat_Rate_Retail, .01)
    XmlValues.STF.COST          = Round(ret:Invoice_Sub_Total + (ret:Invoice_Sub_Total * inv:Vat_Rate_Retail / 100), .01)
    XmlValues.STF.QTY           = '1'   ! Always 1
    XmlValues.STF.INV_DATE      = Format(ret:Invoice_Date, @d06b)
    XmlValues.STF.INV_DUE_DATE  = Format(ret:Invoice_Date, @d06b)
    XmlValues.STF.DESPATCH_DATE = Format(ret:Invoice_Date, @d06b)
    XmlValues.STF.DATE_REQUIRED = Format(ret:Invoice_Date, @d06b)
    ! Use Service Code default - TrkBs: 6178 (DBH: 01-08-2005)
    XmlValues.STF.SERVICE_CODE  = Clip(tmp:STFMessage)! 'STF'   ! string(20)
    SendXml(XmlValues, RepositoryDir)
CreateAOW           Routine
    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = job:Invoice_Number
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Error
    End ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    ! Inserting (DBH 12/01/2006) #6997 - Reget the child files.. just incase
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    ! End (DBH 12/01/2006) #6997

    clear(XmlValues)
    XmlValues.ServiceCode    = 'AOW'
    XmlValues.AOW.TRANS_DESC = 'ARC Out Of Warranty Repair'

    save_TRADEACC_id = Access:TRADEACC.SaveFile()
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Found
        ! Get the Trade Account details of the booking account - TrkBs: 5110 (DBH: 13-07-2005)
        If jobe:WebJob = 1 And ~glo:WebJob
            ! For a RRC job being invoiced at the ARC, use the RRC's account number - TrkBs: 5110 (DBH: 17-08-2005)
            XmlValues.AOW.BILL_AC_NO = Sub(tra:Line500AccountNumber, 1, 10) ! Booking Account Number
        End ! If jobe:WebJob = 1

        XmlValues.AOW.SB_REPAIR_NO  = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
        If glo:WebJob
            ! This is a "company owned" RRC, use it's Branch ID. - TrkBs: 6178 (DBH: 01-08-2005)
            XmlValues.AOW.SB_INV_NO     = 'Z' & Format(Clip(inv:Invoice_Number) & Clip(tra:BranchIdentification),@n09)
        End ! If glo:WebJob
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Access:TRADEACC.RestoreFile(save_TRADEACC_id)

    save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Found
        If ~jobe:WebJob Or glo:WebJob
            ! For ARC job, or RRC invoice, use the account number from the job account - TrkBs: 5110 (DBH: 17-08-2005)
            XmlValues.AOW.BILL_AC_NO = Sub(sub:Line500AccountNumber, 1, 10) ! Booking Account Number
        End ! If ~jobe:WebJob Or glo:WebJob
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Error
    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)

    If ~glo:WebJob
        ! This is NOT a "company owned" RRC. Use the ARC's Branch ID (DBH: 01-08-2005)
        save_TRADEACC_id = Access:TRADEACC.SaveFile()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = GETINI('BOOKING', 'HeadAccount',, CLIP(PATH()) & '\SB2KDEF.INI')
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
            ! Get the Trade Account details of the ARC - TrkBs: 5110 (DBH: 13-07-2005)
            XmlValues.AOW.SB_INV_NO     = 'Z' & Format(Clip(inv:Invoice_Number) & Clip(tra:BranchIdentification),@n09)
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Error
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Access:TRADEACC.RestoreFile(save_TRADEACC_id)
    End ! If ~glo:WebJob

    If glo:WebJob
        ! This is a "company owned" RRC. Use the RRC costs - TrkBs: 6178 (DBH: 01-08-2005)
        XmlValues.AOW.AMT_EXCL_VAT     = Round(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
                                               jobe:InvRRCCLabourCost +   |
                                               jobe:InvRRCCPartsCost, .01)
        
        XmlValues.AOW.COST             = Round(XmlValues.AOW.AMT_EXCL_VAT + |
                                        (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
                                               jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
                                               jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)
    Else ! If glo:WebJob
        ! This is NOT a "company owned" RRC. Use the ARC costs - TrkBs: 6178 (DBH: 01-08-2005)
        XmlValues.AOW.AMT_EXCL_VAT  = Round(job:Invoice_Courier_Cost + | ! Invoice Sub Total
                                            job:Invoice_Labour_Cost +  |
                                            job:Invoice_Parts_Cost, .01)
        XmlValues.AOW.COST          = Round(XmlValues.AOW.AMT_EXCL_VAT + |
                                      (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! Invoice VAT Rate
                                            job:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) +    |
                                            job:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100)), .01)
    End ! If glo:WebJob
    ! As we can only use one Vat Rate, will use the Labour one - TrkBs: 5110 (DBH: 05-08-2005)
    XmlValues.AOW.VAT_RATE      = Round(inv:Vat_Rate_Labour, .01)
! Changing Code (DBH, 11/15/2005) - #6724 Not taking account of the RRC invoicing at a later date
!    XmlValues.AOW.INV_DATE      = Format(inv:Date_Created, @d06b)
!    XmlValues.AOW.INV_DUE_DATE  = Format(inv:Date_Created, @d06b)
!    XmlValues.AOW.DESPATCH_DATE = Format(inv:Date_Created, @d06b)
!    XmlValues.AOW.DATE_REQUIRED = Format(inv:Date_Created, @d06b)
!  to  (DBH, 11/15/2005) - #6724 :-
    If glo:WebJob
        ! If the RRC is creating the invoice, use the RRC invoice date (DBH: 15-11-2005)
        XmlValues.AOW.INV_DATE      = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.AOW.INV_DUE_DATE  = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.AOW.DESPATCH_DATE = Format(inv:RRCInvoiceDate, @d06b)
        XmlValues.AOW.DATE_REQUIRED = Format(inv:RRCInvoiceDate, @d06b)
    Else ! If glo:WebJob
        ! Use the ARC invoice date (DBH: 15-11-2005)
        XmlValues.AOW.INV_DATE      = Format(inv:ARCInvoiceDate, @d06b)
        XmlValues.AOW.INV_DUE_DATE  = Format(inv:ARCInvoiceDate, @d06b)
        XmlValues.AOW.DESPATCH_DATE = Format(inv:ARCInvoiceDate, @d06b)
        XmlValues.AOW.DATE_REQUIRED = Format(inv:ARCInvoiceDate, @d06b)
    End ! If glo:WebJob
! End (DBH, 11/15/2005) - #6724
    XmlValues.AOW.SERVICE_CODE  = Clip(tmp:AOWMessage)!'AOW'  ! string(20)
    SendXml(XmlValues, RepositoryDir)
CreateRIV       Routine
Data
Code
    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = job:Invoice_Number
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    ! Error
    End ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign

    ! Inserting (DBH 12/01/2006) #6997 - Reget the child files.. just incase
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    Access:WEBJOB.Clearkey(wob:RefNumberKey)
    wob:RefNumber   = job:Ref_Number
    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    ! End (DBH 12/01/2006) #6997

    If glo:WebJob
        ! Changing (DBH 11/22/2005) #6774 - RIV is only produced for Generic Account, otherwise nothing (except for company owned RRCs)
        !         save_TRADEACC_id = Access:TRADEACC.SaveFile()
        !         Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        !         tra:Account_Number  = wob:HeadAccountNumber
        !         If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !             ! Found
        !             ! Is this a company owned RRC?
        !             local:CompanyOwned = tra:CompanyOwned
        !         Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !             ! Error
        !         End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !         Access:TRADEACC.RestoreFile(save_TRADEACC_id)
        !
        !         If local:CompanyOwned = True
        !             save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
        !             Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        !             sub:Account_Number  = job:Account_Number
        !             If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !                 ! Found
        !                 If sub:Generic_Account <> True
        !                     ! This is a "company owned" RRC, repairing a non generic account. No RIV, just AOW - TrkBs: 6178 (DBH: 18-08-2005)
        !                     Do CreateAOW
        !                     Exit
        !                 End ! If sub:Generic_Account = True
        !             Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !                 ! Error
        !             End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !             Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
        !        End ! If local:CompanyOwned = True
        ! to (DBH 11/22/2005) #6774
        save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            ! Found
            If sub:Generic_Account <> True
                ! This is not a Generic Account so no message, except for "Company Owned" franchises which produce an AOW - TrkBs: 6774 (DBH: 22-11-2005)
                save_TRADEACC_id = Access:TRADEACC.SaveFile()
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    ! Found
                    IF tra:CompanyOwned = True
                        Do CreateAOW
                        Exit
                    End ! IF tra:CompanyOwned = True
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    ! Error
                End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                Access:TRADEACC.RestoreFile(save_TRADEACC_id)
                ! Not a "Company Owned" Franchise. Don't produce anything - TrkBs: 6774 (DBH: 22-11-2005)
                Exit
            End ! If sub:Generic_Account <> True
        Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            ! Error
        End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)
       ! End (DBH 11/22/2005) #6774
    End ! If glo:WebJob

    clear(XmlValues)
    XmlValues.ServiceCode    = 'RIV'
    XmlValues.RIV.TRANS_DESC = 'Retailer Invoice'

    save_SUBTRACC_id = Access:SUBTRACC.SaveFile()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Found
        save_TRADEACC_id = Access:TRADEACC.SaveFile()
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found
            XmlValues.RIV.MAIN_RETAIL_AC = tra:Line500AccountNumber
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        Access:TRADEACC.RestoreFile(save_TRADEACC_id)
    Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        ! Error
    End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Access:SUBTRACC.RestoreFile(save_SUBTRACC_id)

    XmlValues.RIV.SUB_RETAIL_AC = job:Account_Number
    XmlValues.RIV.FRANCHISE_BRANCH = wob:HeadAccountNumber

    save_TRADEACC_id = Access:TRADEACC.SaveFile()
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = wob:HeadAccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        ! Found
        ! Get the Trade Account details of the booking account - TrkBs: 5110 (DBH: 13-07-2005)
        XmlValues.RIV.SB_INV_NO     = 'Z' & Format(Clip(inv:Invoice_Number) & Clip(tra:BranchIdentification),@n09)
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Access:TRADEACC.RestoreFile(save_TRADEACC_id)

    XmlValues.RIV.AMT_EXCL_VAT     = Round(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
                                           jobe:InvRRCCLabourCost +   |
                                           jobe:InvRRCCPartsCost, .01)
    ! As we can only use one Vat Rate, will use the Labour one - TrkBs: 5110 (DBH: 05-08-2005)

    XmlValues.RIV.VAT_RATE         = Round(inv:Vat_Rate_Labour, .01)
    XmlValues.RIV.COST          = Round(XmlValues.RIV.AMT_EXCL_VAT + |
                                        (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
                                           jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
                                           jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)
! Changing Code (DBH, 11/15/2005) - #6724 Not taking account of the RRC invoicing at a later date
!    XmlValues.RIV.INV_DATE      = Format(inv:Date_Created, @d06b)
!    XmlValues.RIV.INV_DUE_DATE  = Format(inv:Date_Created, @d06b)
!    XmlValues.RIV.DESPATCH_DATE = Format(inv:Date_Created, @d06b)
!    XmlValues.RIV.DATE_REQUIRED = Format(inv:Date_Created, @d06b)
!  to  (DBH, 11/15/2005) - #6724 :-
    XmlValues.RIV.INV_DATE      = Format(inv:RRCInvoiceDate, @d06b)
    XmlValues.RIV.INV_DUE_DATE  = Format(inv:RRCInvoiceDate, @d06b)
    XmlValues.RIV.DESPATCH_DATE = Format(inv:RRCInvoiceDate, @d06b)
    XmlValues.RIV.DATE_REQUIRED = Format(inv:RRCInvoiceDate, @d06b)
! End (DBH, 11/15/2005) - #6724
    XmlValues.RIV.SERVICE_CODE  = Clip(tmp:RIVMessage)!'RIV'  ! string(20)
    SendXml(XmlValues, RepositoryDir)

SendXML         procedure(aXmlData, aRepositoryDir)

TheDate date
TheTime time
MsgId   string(24)

    code

    TheDate = today()
    TheTime = clock()
    MsgId = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
    seq += 1
    if seq > 99999 then
        seq = 1
    end

    if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
        objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
            objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
                objXmlExport.WriteTag('SRC_SYSTEM', '')
                objXmlExport.WriteTag('SRC_APPLICATION', '')
                objXmlExport.WriteTag('SERVICING_APPLICATION', '')
                objXmlExport.WriteTag('ACTION_CODE', 'UPD')
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
                objXmlExport.WriteTag('MESSAGE_ID', '')
                objXmlExport.WriteTag('USER_NAME', '')
                objXmlExport.WriteTag('TOKEN', '')
            objXmlExport.CloseTag('MESSAGE_HEADER')
            objXmlExport.OpenTag('MESSAGE_BODY')

            case aXmlData.ServiceCode
            of 'AOW'
                 objXmlExport.OpenTag('AOW_UPDATE_REQUEST', 'version="1.0"')
                    objXmlExport.WriteTag('TRANS_DESC', clip(aXmlData.AOW.TRANS_DESC))
                    objXmlExport.WriteTag('BILL_AC_NO', clip(aXmlData.AOW.BILL_AC_NO))
                    objXmlExport.WriteTag('SB_REPAIR_NO', clip(aXmlData.AOW.SB_REPAIR_NO))
                    objXmlExport.WriteTag('SB_INV_NO', clip(aXmlData.AOW.SB_INV_NO))
                    objXmlExport.WriteTag('AMT_EXCL_VAT', clip(aXmlData.AOW.AMT_EXCL_VAT))
                    objXmlExport.WriteTag('VAT_RATE', clip(aXmlData.AOW.VAT_RATE))
                    objXmlExport.WriteTag('COST', clip(aXmlData.AOW.COST))
                    objXmlExport.WriteTag('INV_DATE', clip(aXmlData.AOW.INV_DATE))
                    objXmlExport.WriteTag('INV_DUE_DATE', clip(aXmlData.AOW.INV_DUE_DATE))
                    objXmlExport.WriteTag('DESPATCH_DATE', clip(aXmlData.AOW.DESPATCH_DATE))
                    objXmlExport.WriteTag('DATE_REQUIRED', clip(aXmlData.AOW.DATE_REQUIRED))
                    objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.AOW.SERVICE_CODE))
                    objXmlExport.WriteTag('RESEND_YN', 'N')
                 objXmlExport.CloseTag('AOW_UPDATE_REQUEST')

            of 'RIV'
                 objXmlExport.OpenTag('RIV_UPDATE_REQUEST', 'version="1.0"')
                    objXmlExport.WriteTag('TRANS_DESC', clip(aXmlData.RIV.TRANS_DESC))
                    objXmlExport.WriteTag('MAIN_RETAIL_AC', clip(aXmlData.RIV.MAIN_RETAIL_AC))
                    objXmlExport.WriteTag('SUB_RETAIL_AC', clip(aXmlData.RIV.SUB_RETAIL_AC))
                    objXmlExport.WriteTag('FRANCHISE_BRANCH', clip(aXmlData.RIV.FRANCHISE_BRANCH))
                    objXmlExport.WriteTag('SB_INV_NO', clip(aXmlData.RIV.SB_INV_NO))
                    objXmlExport.WriteTag('AMT_EXCL_VAT', clip(aXmlData.RIV.AMT_EXCL_VAT))
                    objXmlExport.WriteTag('VAT_RATE', clip(aXmlData.RIV.VAT_RATE))
                    objXmlExport.WriteTag('COST', clip(aXmlData.RIV.COST))
                    objXmlExport.WriteTag('INV_DATE', clip(aXmlData.RIV.INV_DATE))
                    objXmlExport.WriteTag('INV_DUE_DATE', clip(aXmlData.RIV.INV_DUE_DATE))
                    objXmlExport.WriteTag('DESPATCH_DATE', clip(aXmlData.RIV.DESPATCH_DATE))
                    objXmlExport.WriteTag('DATE_REQUIRED', clip(aXmlData.RIV.DATE_REQUIRED))
                    objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.RIV.SERVICE_CODE))
                    objXmlExport.WriteTag('RESEND_YN', 'N')
                 objXmlExport.CloseTag('RIV_UPDATE_REQUEST')

            of 'STF'
                 objXmlExport.OpenTag('STF_UPDATE_REQUEST', 'version="1.0"')
                    objXmlExport.WriteTag('TRANS_DESC', clip(aXmlData.STF.TRANS_DESC))
                    objXmlExport.WriteTag('BILL_AC_NO', clip(aXmlData.STF.BILL_AC_NO))
                    objXmlExport.WriteTag('CUST_ORD_NO', clip(aXmlData.STF.CUST_ORD_NO))
                    objXmlExport.WriteTag('SB_INV_NO', clip(aXmlData.STF.SB_INV_NO))
                    objXmlExport.WriteTag('AMT_EXCL_VAT', clip(aXmlData.STF.AMT_EXCL_VAT))
                    objXmlExport.WriteTag('VAT_RATE', clip(aXmlData.STF.VAT_RATE))
                    objXmlExport.WriteTag('COST', clip(aXmlData.STF.COST))
                    objXmlExport.WriteTag('QTY', clip(aXmlData.STF.QTY))
                    objXmlExport.WriteTag('INV_DATE', clip(aXmlData.STF.INV_DATE))
                    objXmlExport.WriteTag('INV_DUE_DATE', clip(aXmlData.STF.INV_DUE_DATE))
                    objXmlExport.WriteTag('DESPATCH_DATE', clip(aXmlData.STF.DESPATCH_DATE))
                    objXmlExport.WriteTag('DATE_REQUIRED', clip(aXmlData.STF.DATE_REQUIRED))
                    objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.STF.SERVICE_CODE))
                    objXmlExport.WriteTag('RESEND_YN', 'N')
                 objXmlExport.CloseTag('STF_UPDATE_REQUEST')

            of 'POR'
                 objXmlExport.OpenTag('POR_UPDATE_REQUEST', 'version="1.0"')
                    objXmlExport.WriteTag('CURRENCY_CODE', clip(aXmlData.POR.CURRENCY_CODE))
                    objXmlExport.WriteTag('CURRENCY_UNIT_PRICE', clip(aXmlData.POR.CURRENCY_UNIT_PRICE))
                    objXmlExport.WriteTag('DELIV_TO_REQ', clip(aXmlData.POR.DELIV_TO_REQ))
                    objXmlExport.WriteTag('DEST_ORG_ID', clip(aXmlData.POR.DEST_ORG_ID))
                    objXmlExport.WriteTag('GL_DATE', clip(aXmlData.POR.GL_DATE))
                    objXmlExport.WriteTag('INTERFACE_SRC', clip(aXmlData.POR.INTERFACE_SRC))
                    objXmlExport.WriteTag('ITEM_ID', clip(aXmlData.POR.ITEM_ID))
                    objXmlExport.WriteTag('ORG_ID', clip(aXmlData.POR.ORG_ID))
                    objXmlExport.WriteTag('PREPARER_ID', clip(aXmlData.POR.PREPARER_ID))
                    objXmlExport.WriteTag('QTY', clip(aXmlData.POR.QTY))
                    objXmlExport.WriteTag('SUPPLIER_NUMBER', clip(aXmlData.POR.SUPPLIER_NUMBER))
                    objXmlExport.WriteTag('SB_PO_NUMBER', clip(aXmlData.POR.SB_PO_NUMBER))
                    objXmlExport.WriteTag('TYPE', clip(aXmlData.POR.TYPE))
                    objXmlExport.WriteTag('RESEND_YN', 'N')
                 objXmlExport.CloseTag('POR_UPDATE_REQUEST')

            of 'REC'
                 objXmlExport.OpenTag('REC_UPDATE_REQUEST', 'version="1.0"')
                    objXmlExport.WriteTag('SB_PO_NO', clip(aXmlData.REC.SB_PO_NO))
                    objXmlExport.WriteTag('ORACLE_PO_NO', clip(aXmlData.REC.ORACLE_PO_NO))
                    objXmlExport.WriteTag('URL', clip(aXmlData.REC.URL))
                    objXmlExport.WriteTag('TYPE', clip(aXmlData.REC.TYPE))
                    objXmlExport.WriteTag('RESEND_YN', 'N')
                 objXmlExport.CloseTag('REC_UPDATE_REQUEST')
             end

            objXmlExport.CloseTag('MESSAGE_BODY')
        objXmlExport.CloseTag('VODACOM_MESSAGE')
        objXmlExport.FClose();
    end





XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServuceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSALES.Open
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','XFiles')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSALES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','XFiles')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

CID_XML              PROCEDURE  (String f:Mobile,String f:Account,Byte f:Type) ! Declare Procedure
seq                  LONG
RepositoryDir        CSTRING('C:\ServiceBaseMQ\MQ_Repository<0>{224}')
save_invoice_id      USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_subtracc_id     USHORT,AUTO
tmp:STFMessage       STRING(30)
tmp:AOWMessage       STRING(30)
tmp:RIVMessage       STRING(30)
! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
XmlData group,type
ServiceCode string(3)
CID         group
MSISDN_OR_ICCID         string(20)
CALL_ID                 string(7)
ACCOUNT_ID              string(9)
SOURCE                  string(60)
TOPIC_CODE              string(60)
COMMENT                 string(255)
            end
        end

    map
SendXML         procedure(const *XmlData aXmlData, string aRepositoryDir)
    end

XmlValues GROUP(XmlData)
           END
  CODE
! ================================================================
! Initialise Xml Export Object
  objXmlExport.FInit(fileXmlExport)
! ================================================================
    ! Check default to see if should create messages - TrkBs: 6141 (DBH: 20-07-2005)
    If GETINI('XML','CreateCID',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        Return
    End ! If GETINI('XML','CreateLine500',,Clip(Path()) & '\SB2KDEF.INI') <> True

    ! Lookup General Default for XML repository folder - TrkBs: 5110 (DBH: 29-06-2005)
    RepositoryDir               = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')

    If CreateTestRecord_OLD(RepositoryDir) = False
        Return
    End ! If CreateTestRecord(RepositoryDir) = False

    Do CreateCID
CreateCID                  Routine
    Access:TRADEACC_ALIAS.Open()
    Access:TRADEACC_ALIAS.UseFile()
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = f:Account
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Found

    Else ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    ! Error
    End ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign

    clear(XmlValues)
    XmlValues.ServiceCode         = 'CID'
    XmlValues.CID.MSISDN_OR_ICCID = f:Mobile
    XmlValues.CID.CALL_ID         = ''
    XmlValues.CID.ACCOUNT_ID      = ''
    XmlValues.CID.SOURCE          = 'ServiceBase'
    Case f:Type
    Of 1 !Booked In
        XmlValues.CID.TOPIC_CODE      = 'Repair Booked In'
        XmlValues.CID.COMMENT         = 'Repair Booked In At ' & Clip(tra_ali:Company_Name)
    Of 2 !Despatched
        XmlValues.CID.TOPIC_CODE      = 'Repair Collected'
        XmlValues.CID.COMMENT         = 'Repair Collected From ' & Clip(tra_ali:Company_Name)
    End ! Case f:Type
    
    SendXml(XmlValues, RepositoryDir)

    Access:TRADEACC_ALIAS.Close()
SendXML         procedure(aXmlData, aRepositoryDir)

TheDate date
TheTime time
MsgId   string(24)

    code

    TheDate = today()
    TheTime = clock()
    MsgId = 'MSG' & format(TheDate, @d012) & format(TheTime, @t05) & format(TheTime % 100, @n02) & format(seq, @n05)
    seq += 1
    if seq > 99999 then
        seq = 1
    end
    if objXmlExport.FOpen(clip(aRepositoryDir) & '\' & clip(aXmlData.ServiceCode) & '\' & MsgId & '.xml', true) = level:benign then
        objXmlExport.OpenTag('VODACOM_MESSAGE', 'version="1.0"')
            objXmlExport.OpenTag('MESSAGE_HEADER', 'direction="REQ"')
                objXmlExport.WriteTag('SRC_SYSTEM', '')
                objXmlExport.WriteTag('SRC_APPLICATION', '')
                objXmlExport.WriteTag('SERVICING_APPLICATION', '')
                objXmlExport.WriteTag('ACTION_CODE', 'INS')
                objXmlExport.WriteTag('SERVICE_CODE', clip(aXmlData.ServiceCode))
                objXmlExport.WriteTag('MESSAGE_ID', '')
                objXmlExport.OpenTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('VALIDITY','84600000','unit="millisecond"')
                objXmlExport.WriteTag('ACTION','','type="DISCARD"')
                objXmlExport.WriteTag('RESPONSE_REQUIRED','Y')
                objXmlExport.CloseTag('MESSAGE_EXPIRY')
                objXmlExport.WriteTag('USER_NAME', '')
                objXmlExport.WriteTag('TOKEN', '')
            objXmlExport.CloseTag('MESSAGE_HEADER')
            objXmlExport.OpenTag('MESSAGE_BODY')

            objXmlExport.OpenTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')
                objXmlExport.WriteTag('MSISDN_OR_ICCID', clip(aXmlData.CID.MSISDN_OR_ICCID))
                objXmlExport.WriteTag('CALL_ID', clip(aXmlData.CID.CALL_ID))
                objXmlExport.WriteTag('ACCOUNT_ID', clip(aXmlData.CID.ACCOUNT_ID))
                objXmlExport.WriteTag('SOURCE', clip(aXmlData.CID.SOURCE))
                objXmlExport.WriteTag('TOPIC_CODE', clip(aXmlData.CID.TOPIC_CODE))
                objXmlExport.WriteTag('COMMENT', clip(aXmlData.CID.COMMENT))
             objXmlExport.CloseTag('CUSTOMER_INTERACTION_DETAIL_REQUEST')

            objXmlExport.CloseTag('MESSAGE_BODY')
        objXmlExport.CloseTag('VODACOM_MESSAGE')
        objXmlExport.FClose();
    end





CreateTestRecord_OLD PROCEDURE  (String f:Path)       ! Declare Procedure
tmp:TestRecord       STRING(255),STATIC
TestRecord    File,Driver('ASCII'),Pre(test),Name(tmp:TestRecord),Create,Bindable,Thread
Record                  Record
TestLine                String(1)
                        End
                    End
  CODE
    ! Start - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005)
    tmp:TestRecord = Clip(f:Path)

    If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & Random(1,9999) & Clock() & '.tmp'
    Else ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'
        tmp:TestRecord = Clip(tmp:TestRecord) & '\' & Random(1,9999) & Clock() & '.tmp'
    End ! If Sub(Clip(tmp:TestRecord),-1,1) = '\'

    Remove(TestRecord)
    Create(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Open(TestRecord)
    If Error()
        Return False
    End ! If Error()
    test:TestLine = '1'
    Add(TestRecord)
    If Error()
        Return False
    End ! If Error()
    Close(TestRecord)
    Remove(TestRecord)
    If Error()
        Return False
    End ! If Error()
    ! End   - Create a small test file in the repository folder - TrkBs: 5110 (DBH: 30-08-2005
    Return True
ResubmitMessages PROCEDURE                            !Generated from procedure template - Window

tmp:ResendType       BYTE(0)
tmp:PONumber         STRING(30)
tmp:Suffix           STRING('01')
tmp:RecordNo         LONG
tmp:EvoAll           STRING(1)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,244),USE(?Panel5),FILL(09A6A7CH)
                       OPTION('Choose Message To Re-Send'),AT(260,118,164,110),USE(tmp:ResendType),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('POR (GRV Note)'),AT(266,136),USE(?tmp:ResendType:POR1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('POR (Third Party Batch)'),AT(266,150),USE(?tmp:ResendType:POR2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                         RADIO('CID (Booking Message)'),AT(266,164),USE(?tmp:ResendType:CID1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                         RADIO('CID (Customer Collection Message)'),AT(266,178),USE(?tmp:ResendType:CID2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('4')
                         RADIO('EVO POR (GRV Note)'),AT(266,192),USE(?tmp:ResendType:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('5')
                         RADIO('EVO POR (Third Party Batch)'),AT(266,206),USE(?tmp:ResendType:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('6')
                       END
                       PROMPT('Do not enter the ''/01, /02,..'' suffix'),AT(277,236),USE(?Prompt:NoSuffix),HIDE,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('PO Number:'),AT(272,254),USE(?tmp:PONumber:Prompt),HIDE,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('SS3'),AT(348,254),USE(?Prompt:SS),TRN,HIDE,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n6),AT(368,254,42,10),USE(tmp:PONumber),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('PO Number'),TIP('PO Number'),OVR,UPR
                       PROMPT(' / '),AT(412,254),USE(?tmp:Suffix:Prompt),HIDE,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n02),AT(424,254,15,10),USE(tmp:Suffix),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Suffix'),TIP('Suffix'),UPR
                       PROMPT('Record No:'),AT(272,268),USE(?tmp:RecordNo:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@n-14),AT(368,268,42,10),USE(tmp:RecordNo),HIDE,LEFT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                       CHECK('All Unsent'),AT(424,268),USE(tmp:EvoAll),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Resubmit XML Messages'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(168,332),USE(?ResubmissionHistory),TRN,FLAT,ICON('resub2p.jpg')
                       BUTTON,AT(376,332),USE(?Button:Resubmit),TRN,FLAT,ICON('resubp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServuceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020637'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ResubmitMessages')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:GRNOTES.Open
  Relate:RESUBMIT.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:TRDBATCH.UseFile
  Access:JOBSE.UseFile
  Access:ORDPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If SecurityCheck('RESUBMIT TO ORACLE')
      ?tmp:ResendType:POR1{prop:Disable} = True
      ?tmp:ResendType:POR2{prop:Disable} = True
  End ! If SecurityCheck('RESUBMIT TO ORACLE')
  
  If SecurityCheck('RESUBMIT TO CID')
      ?tmp:ResendType:CID1{prop:Disable} = True
      ?tmp:ResendType:CID2{prop:Disable} = True
  End ! If SecurityCheck('RESUBMIT TO ORACLE')
  ! Save Window Name
   AddToLog('Window','Open','ResubmitMessages')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:GRNOTES.Close
    Relate:RESUBMIT.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ResubmitMessages')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ResendType
          ?tmp:PONumber:Prompt{prop:Hide} = True
          ?Prompt:SS{prop:Hide}           = True
          ?tmp:PONumber{prop:Hide}        = True
          ?Prompt:NoSuffix{prop:Hide}     = True
          ?tmp:Suffix{prop:Hide}          = True
          ?tmp:Suffix:Prompt{prop:Hide}   = True
          ?tmp:RecordNo{PROP:Hide}        = true
          ?tmp:RecordNo:Prompt{PROP:Hide} = true
          ?tmp:EvoAll{prop:hide}          = true
      
          Case tmp:ResendType
          Of 1 ! POR GRV
              ?tmp:PONumber:Prompt{prop:Hide} = False
              ?tmp:PONumber:Prompt{prop:Text} = 'PO Number:'
              ?Prompt:SS{prop:Hide}           = False
              ?Prompt:SS{prop:Text}           = 'SS'
              ?tmp:PONumber{prop:Hide}        = False
              ?tmp:PONumber{prop:Text}        = '@n08'
              ?Prompt:NoSuffix{prop:Hide}     = True
              ?tmp:Suffix{prop:Hide}          = False
              ?tmp:Suffix:Prompt{Prop:Hide}   = False
      
          Of 2 ! POR 3rd Party
              ?tmp:PONumber:Prompt{prop:Hide} = False
              ?tmp:PONumber:Prompt{prop:Text} = 'PO Number:'
              ?Prompt:SS{prop:Hide}           = False
              ?Prompt:SS{prop:Text}           = 'SS3'
              ?tmp:PONumber{prop:Hide}        = False
              ?tmp:PONumber{prop:Text}        = '@n07'
              ?Prompt:NoSuffix{prop:Hide}     = False
      
          Of 3 ! CID Booking
              ?tmp:PONumber:Prompt{Prop:Hide} = False
              ?tmp:PONumber:Prompt{prop:text} = 'SB Job Number'
              ?tmp:PONumber{prop:Hide}        = False
              ?tmp:PONumber{prop:Text}        = '@n08'
          Of 4 ! CID Customer Collection
              ?tmp:PONumber:Prompt{Prop:Hide} = False
              ?tmp:PONumber:Prompt{prop:text} = 'SB Job Number'
              ?tmp:PONumber{prop:Hide}        = False
              ?tmp:PONumber{prop:Text}        = '@n08'
          of 5 orof 6 !Evo bits
              ?tmp:RecordNo{PROP:Hide}        = false
              ?tmp:RecordNo:Prompt{PROP:Hide} = false
              ?tmp:EvoAll{prop:hide}          = false
          End ! Case tmp:ResendType
      
          if Tmp:ResendType < 5 then
              Select(?tmp:PONumber)
          ELSE
              Select(?tmp:RecordNo)
          END
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020637'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020637'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020637'&'0')
      ***
    OF ?ResubmissionHistory
      ThisWindow.Update
      BrowseResubmissions
      ThisWindow.Reset
    OF ?Button:Resubmit
      ThisWindow.Update
      Case tmp:ResendType
      Of 1 !GRN
          Found# = False
          Suffix# = 0
          Access:GRNOTES.ClearKey(grn:Order_Number_Key)
          grn:Order_Number        = tmp:PONumber
          Set(grn:Order_Number_Key,grn:Order_Number_Key)
          Loop
              If Access:GRNOTES.NEXT()
                 Break
              End !If
              If grn:Order_Number        <> tmp:PONumber      |
                  Then Break.  ! End If
              !Does this GRN Have Parts Attached?
              PartsAttached# = False
              FoundValue# = False
              Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
              orp:Order_Number = grn:Order_Number
              Set(orp:Part_Number_Key,orp:Part_Number_Key)
              Loop
                  If Access:ORDPARTS.NEXT()
                     Break
                  End !If
                  If orp:Order_Number <> grn:Order_Number      |
                      Then Break.  ! End If
                  If orp:GRN_Number = grn:Goods_Received_Number
                      PartsAttached# = True
                  Else
                      Cycle
                  End ! If orp:GRN_Number = grnali:Goods_Received_Number
      
                  If orp:Date_Received = ''
                      Cycle
                  End ! If orp:Date_Received = ''
      
              End !Loop
      
              If PartsAttached# = False
                  Cycle
              End ! If PartsAttached# = False
      
              Suffix# += 1
      
              If Suffix# = tmp:Suffix
                  grn:BatchRunNotPrinted = True
                  If Access:GRNOTES.TryUpdate() = Level:Benign
                      Found# = True
                      Break
                  End ! If Access:GRNOTES.TryUpdate() = Level:Benign
              End ! If Suffix# = tmp:Suffix
      
          End !Loop
      
          If Found# = True
              ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
              If Access:RESUBMIT.PrimeRecord() = Level:Benign
                  Access:USERS.Clearkey(use:Password_Key)
                  use:Password    = glo:Password
                  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                      ! Found
      
                  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                      ! Error
                  End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  reb:Usercode = use:User_Code
                  reb:MessageType = 'POR'
                  reb:Notes = 'GRN PURCHASE ORDER NO: ' & Clip(tmp:PONumber) & '/' & Clip(tmp:Suffix)
                  If Access:RESUBMIT.TryInsert() = Level:Benign
                      ! Insert Successful
                  Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                      ! Insert Failed
                      Access:RESUBMIT.CancelAutoInc()
                  End ! If Access:RESUBMIT.TryInsert() = Level:Benign
              End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
              ! End (DBH, 11/16/2005) - #6724
              Case Missive('The selected Purchase Order has been marked for resubmission.'&|
                '|Please run REPBATCH.EXE to resubmit.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Else ! If Found# = True
              Case Missive('Unable to find the selected Purchase Order.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          End ! If Found# = True
      Of 2 !Third Party
          Found# = False
          Access:TRDBATCH.ClearKey(trb:PurchaseOrderKey)
          trb:PurchaseOrderNumber = tmp:PONumber
          Set(trb:PurchaseOrderKey,trb:PurchaseOrderKey)
          Loop
              If Access:TRDBATCH.NEXT()
                 Break
              End !If
              If trb:PurchaseOrderNumber <> tmp:PONumber      |
                  Then Break.  ! End If
              trb:BatchRunNotPrinted = True
              If Access:TRDBATCH.TryUpdate() = Level:Benign
                  Found# = True
              End ! If Access:TRDBATCH.TryUpdate() = Level:Benign
          End !Loop
          If Found# = True
              ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
              If Access:RESUBMIT.PrimeRecord() = Level:Benign
                  Access:USERS.Clearkey(use:Password_Key)
                  use:Password    = glo:Password
                  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                      ! Found
      
                  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                      ! Error
                  End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  reb:Usercode = use:User_Code
                  reb:MessageType = 'POR'
                  reb:Notes = '3RD PARTY PURCHASE ORDER NO: ' & Clip(tmp:PONumber)
                  If Access:RESUBMIT.TryInsert() = Level:Benign
                      ! Insert Successful
                  Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                      ! Insert Failed
                      Access:RESUBMIT.CancelAutoInc()
                  End ! If Access:RESUBMIT.TryInsert() = Level:Benign
              End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
              ! End (DBH, 11/16/2005) - #6724
              Case Missive('The selected Third Party Receipt has been marked for resubmission.'&|
                '|Please run REPBATCH.EXE to resubmit.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Else
              Case Missive('Unable to find the selected Purchase Order.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
      
          End ! If Found# = False                       #
      Of 3 ! CID Booking
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = tmp:PONumber
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Found
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  ! Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
                      If ~jobe:VSACustomer
                          Case Missive('The selected job is NOT for a VSA Customer.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                      Else ! If ~jobe:VSACustomer
                          CID_XML(job:Mobile_Number,wob:HeadAccountNumber,1)
                          ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
                          If Access:RESUBMIT.PrimeRecord() = Level:Benign
                              Access:USERS.Clearkey(use:Password_Key)
                              use:Password    = glo:Password
                              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                  ! Found
      
                              Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                  ! Error
                              End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                              reb:Usercode = use:User_Code
                              reb:MessageType = 'CID'
                              reb:Notes = 'BOOKING. JOB NO: ' & job:Ref_Number
                              If Access:RESUBMIT.TryInsert() = Level:Benign
                                  ! Insert Successful
                              Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                                  ! Insert Failed
                                  Access:RESUBMIT.CancelAutoInc()
                              End ! If Access:RESUBMIT.TryInsert() = Level:Benign
                          End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
                          ! End (DBH, 11/16/2005) - #6724
                          Case Missive('Message resubmitted.','ServiceBase 3g',|
                                         'midea.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                      End ! If ~jobe:VSACustomer
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  ! Error
              End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Error
              Case Missive('Unable to find the selected Job Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      Of 4 ! CID Customer Collection
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = tmp:PONumber
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Found
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = job:Ref_Number
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  ! Found
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Found
                      If ~jobe:VSACustomer
                          Case Missive('The selected job is NOT for a VSA Customer.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                      Else !If ~jobe:VSACustomer
                          FoundDespatch# = False
      
                          If jobe:WebJob
                              ! RRC job. Has it been despatched from RRC (either job or exchange) (DBH: 31-08-2005)
                              Access:AUDIT.ClearKey(aud:Action_Key)
                              aud:Ref_Number = job:Ref_Number
                              aud:Action     = 'DESPATCH FROM RRC'
                              Set(aud:Action_Key,aud:Action_Key)
                              Loop
                                  If Access:AUDIT.NEXT()
                                     Break
                                  End !If
                                  If aud:Ref_Number <> job:Ref_Number      |
                                  Or aud:Action     <> 'DESPATCH FROM RRC'      |
                                      Then Break.  ! End If
                                  FoundDespatch# = True
                                  Break
                              End !Loop
      
                          Else ! If jobe:WebJob
                              ! ARC only job. Has it been despatched from ARC (either job or exchange) (DBH: 31-08-2005)
                              Access:AUDIT.ClearKey(aud:Action_Key)
                              aud:Ref_Number = job:Ref_Number
                              aud:Action     = 'DESPATCH FROM ARC'
                              Set(aud:Action_Key,aud:Action_Key)
                              Loop
                                  If Access:AUDIT.NEXT()
                                     Break
                                  End !If
                                  If aud:Ref_Number <> job:Ref_Number      |
                                  Or aud:Action     <> 'DESPATCH FROM ARC'      |
                                      Then Break.  ! End If
                                  FoundDespatch# = True
                                  Break
                              End !Loop
                          End ! If jobe:WebJob
      
                          If FoundDespatch# = True
                              CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)
                              ! Inserting Code (DBH, 11/16/2005) - #6724 Record resubmission in new "history" file
                              If Access:RESUBMIT.PrimeRecord() = Level:Benign
                                  Access:USERS.Clearkey(use:Password_Key)
                                  use:Password    = glo:Password
                                  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      ! Found
      
                                  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      ! Error
                                  End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                  reb:Usercode = use:User_Code
                                  reb:MessageType = 'CID'
                                  reb:Notes = 'DESPATCH. JOB NO: ' & job:Ref_Number
                                  If Access:RESUBMIT.TryInsert() = Level:Benign
                                      ! Insert Successful
                                  Else ! If Access:RESUBMIT.TryInsert() = Level:Benign
                                      ! Insert Failed
                                      Access:RESUBMIT.CancelAutoInc()
                                  End ! If Access:RESUBMIT.TryInsert() = Level:Benign
                              End !If Access:RESUBMIT.PrimeRecord() = Level:Benign
                              ! End (DBH, 11/16/2005) - #6724
                              Case Missive('Message resubmitted.','ServiceBase 3g',|
                                             'midea.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                          Else ! If FoundDespatch# = True
                              Case Missive('The selected job has not been despatched.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                          End ! If FoundDespatch# = True
                      End ! If ~jobe:VSACustomer
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      ! Error
                  End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  ! Error
              End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              ! Error
              Case Missive('Unable to find the selected Job Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      of 5
          if tmp:EvoAll = 'Y' then
              EVO_Export('ORDER',0,'Y')     !note all calls from this window are verbose ("Y" at end)
          ELSE
              if tmp:RecordNo > 0 then EVO_Export('ORDER',tmp:RecordNo,'Y').
          END
          !SentType = ORDER, ALL or THIRD
          !sentRecord = number of record to process (0 means all in R state)
          !Verbose = 'Y' = echo messages to the screen (else log them in a file)
      of 6
          if tmp:EvoAll = 'Y' then
              EVO_EXPORT('THIRD',0,'Y')
          ELSE
              if tmp:RecordNo > 0 then EVO_Export('THIRD',tmp:RecordNo,'Y').
          END
      End ! Case tmp:POType
      
      tmp:PONumber = 0
      tmp:RecordNo = 0
      !Select(?tmp:PONumber)
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseResubmissions PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:Username         STRING(60)
BRW1::View:Browse    VIEW(RESUBMIT)
                       PROJECT(reb:TheDate)
                       PROJECT(reb:TheTime)
                       PROJECT(reb:Usercode)
                       PROJECT(reb:MessageType)
                       PROJECT(reb:Notes)
                       PROJECT(reb:RecordNumber)
                       JOIN(USERS,'Upper(use:User_Code) = Upper(reb:Usercode)')
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
reb:TheDate            LIKE(reb:TheDate)              !List box control field - type derived from field
reb:TheTime            LIKE(reb:TheTime)              !List box control field - type derived from field
reb:Usercode           LIKE(reb:Usercode)             !List box control field - type derived from field
tmp:Username           LIKE(tmp:Username)             !List box control field - type derived from local data
reb:MessageType        LIKE(reb:MessageType)          !List box control field - type derived from field
reb:Notes              LIKE(reb:Notes)                !List box control field - type derived from field
reb:RecordNumber       LIKE(reb:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the RESUBMIT File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel5),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Browse XML Message Resubmissions'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,90,344,236),USE(?Browse:1),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('50R(2)|M~Date~@d6@26R(2)|M~Time~@t1b@0L(2)|M~Usercode~@s3@109L(2)|M~User~@s60@23' &|
   'L(2)|M~Type~@s3@80L(2)|M~Details~@s255@'),FROM(Queue:Browse:1)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServuceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020639'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseResubmissions')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RESUBMIT.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RESUBMIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseResubmissions')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,reb:TheDateKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,reb:TheDate,1,BRW1)
  BIND('tmp:Username',tmp:Username)
  BRW1.AddField(reb:TheDate,BRW1.Q.reb:TheDate)
  BRW1.AddField(reb:TheTime,BRW1.Q.reb:TheTime)
  BRW1.AddField(reb:Usercode,BRW1.Q.reb:Usercode)
  BRW1.AddField(tmp:Username,BRW1.Q.tmp:Username)
  BRW1.AddField(reb:MessageType,BRW1.Q.reb:MessageType)
  BRW1.AddField(reb:Notes,BRW1.Q.reb:Notes)
  BRW1.AddField(reb:RecordNumber,BRW1.Q.reb:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RESUBMIT.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseResubmissions')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020639'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020639'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020639'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  tmp:Username = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  PARENT.SetQueueRecord
  SELF.Q.tmp:Username = tmp:Username                  !Assign formula result to display queue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Line500_XML          PROCEDURE  (f:ID)                ! Declare Procedure
    INCLUDE('Line500XML.inc','Data')
  CODE
    INCLUDE('Line500XML.inc','Code')
