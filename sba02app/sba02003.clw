

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02003.INC'),ONCE        !Local module procedure declarations
                     END


Update_Exception_Charge PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::exc:Record  LIKE(exc:RECORD),STATIC
QuickWindow          WINDOW('Update the EXCCHRGE File'),AT(,,244,132),FONT('Arial',8,,),CENTER,IMM,HLP('Update_Exception_Charge'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?EXC:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(61,20,124,12),USE(exc:Manufacturer),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Charge Type'),AT(8,34),USE(?EXC:Charge_Type:Prompt),TRN
                           ENTRY(@s30),AT(61,34,124,12),USE(exc:Charge_Type),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Model Number'),AT(8,48),USE(?EXC:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(61,48,124,12),USE(exc:Model_Number),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Unit Type'),AT(8,62),USE(?EXC:Unit_Type:Prompt),TRN
                           ENTRY(@s30),AT(61,62,124,10),USE(exc:Unit_Type),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Repair Type'),AT(8,76),USE(?EXC:Repair_Type:Prompt),TRN
                           ENTRY(@s30),AT(61,76,124,12),USE(exc:Repair_Type),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Cost'),AT(8,90),USE(?EXC:Cost:Prompt),TRN
                           ENTRY(@n15.2),AT(61,90,64,12),USE(exc:Cost),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&OK'),AT(4,108,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(84,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Exception_Charge')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EXC:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(exc:Record,History::exc:Record)
  SELF.AddHistoryField(?exc:Manufacturer,1)
  SELF.AddHistoryField(?exc:Charge_Type,2)
  SELF.AddHistoryField(?exc:Model_Number,3)
  SELF.AddHistoryField(?exc:Unit_Type,4)
  SELF.AddHistoryField(?exc:Repair_Type,5)
  SELF.AddHistoryField(?exc:Cost,6)
  SELF.AddUpdateFile(Access:EXCCHRGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCCHRGE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EXCCHRGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Update_Exception_Charge')
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCCHRGE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Exception_Charge')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

