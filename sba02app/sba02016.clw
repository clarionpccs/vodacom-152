

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02016.INC'),ONCE        !Local module procedure declarations
                     END


UpdateINVOICE PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::inv:Record  LIKE(inv:RECORD),STATIC
QuickWindow          WINDOW('Update the INVOICE File'),AT(,,212,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateINVOICE'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,204,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Invoice Number'),AT(8,20),USE(?INV:Invoice_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<<#p),AT(80,20,44,10),USE(inv:Invoice_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           OPTION('Invoice Type'),AT(80,34,50,36),USE(inv:Invoice_Type),BOXED
                             RADIO('CHA'),AT(84,44),USE(?INV:Invoice_Type:Radio1),VALUE('CHA')
                             RADIO('WAR'),AT(84,56),USE(?INV:Invoice_Type:Radio2),VALUE('WAR')
                           END
                           PROMPT('Job Number'),AT(8,74),USE(?INV:Job_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<<#p),AT(80,74,44,10),USE(inv:Job_Number),SKIP,LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Yellow,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           PROMPT('Date Created'),AT(8,88),USE(?INV:Date_Created:Prompt),TRN
                           ENTRY(@d6b),AT(80,88,104,10),USE(inv:Date_Created),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Account Number'),AT(8,102),USE(?INV:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(80,102,64,10),USE(inv:Account_Number),SKIP,LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Yellow,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Total'),AT(8,116),USE(?INV:Total:Prompt),TRN
                           ENTRY(@N14.2B),AT(80,116,60,10),USE(inv:Total),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White)
                           PROMPT('Labour Rate'),AT(8,130),USE(?INV:Vat_Rate_Labour:Prompt),TRN
                           ENTRY(@N14.2B),AT(80,130,60,10),USE(inv:Vat_Rate_Labour),DECIMAL(14),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Parts Rate'),AT(8,144),USE(?INV:Vat_Rate_Parts:Prompt),TRN
                           ENTRY(@N14.2B),AT(80,144,60,10),USE(inv:Vat_Rate_Parts),DECIMAL(14),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Retail Sales Rate'),AT(8,20),USE(?INV:Vat_Rate_Retail:Prompt),TRN
                           ENTRY(@n14.2B),AT(80,20,60,10),USE(inv:Vat_Rate_Retail),DECIMAL(14),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('V.A.T. Number'),AT(8,34),USE(?INV:VAT_Number:Prompt),TRN
                           ENTRY(@s30),AT(80,34,124,10),USE(inv:VAT_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('V.A.T. Number'),AT(8,48),USE(?INV:Invoice_VAT_Number:Prompt),TRN
                           ENTRY(@s30),AT(80,48,124,10),USE(inv:Invoice_VAT_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Currency'),AT(8,62),USE(?INV:Currency:Prompt),TRN
                           ENTRY(@s30),AT(80,62,124,10),USE(inv:Currency),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('EDI Batch Number'),AT(8,76),USE(?INV:Batch_Number:Prompt)
                           ENTRY(@n6),AT(80,76,40,10),USE(inv:Batch_Number),UPR
                           PROMPT('Manufacturer'),AT(8,90),USE(?INV:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(80,90,124,10),USE(inv:Manufacturer),SKIP,LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Claim Reference'),AT(8,104),USE(?INV:Claim_Reference:Prompt),TRN
                           ENTRY(@s30),AT(80,104,124,10),USE(inv:Claim_Reference),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Total Claimed'),AT(8,118),USE(?INV:Total_Claimed:Prompt)
                           ENTRY(@n14.2),AT(80,118,60,10),USE(inv:Total_Claimed),DECIMAL(14),MSG('r'),TIP('r')
                           PROMPT('Labour Paid:'),AT(8,132),USE(?INV:Labour_Paid:Prompt)
                           ENTRY(@n14.2),AT(80,132,60,10),USE(inv:Labour_Paid),DECIMAL(14)
                           PROMPT('Parts Paid:'),AT(8,146),USE(?INV:Parts_Paid:Prompt)
                           ENTRY(@n14.2),AT(80,146,60,10),USE(inv:Parts_Paid),DECIMAL(14)
                         END
                         TAB('General (cont. 2)'),USE(?Tab:3)
                           PROMPT('Reconciled Date:'),AT(8,20),USE(?INV:Reconciled_Date:Prompt)
                           ENTRY(@d6b),AT(80,20,104,10),USE(inv:Reconciled_Date)
                         END
                       END
                       BUTTON('OK'),AT(65,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(114,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(163,164,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a INVOICE Record'
  OF ChangeRecord
    ActionMessage = 'Changing a INVOICE Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateINVOICE')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INV:Invoice_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(inv:Record,History::inv:Record)
  SELF.AddHistoryField(?inv:Invoice_Number,1)
  SELF.AddHistoryField(?inv:Invoice_Type,2)
  SELF.AddHistoryField(?inv:Job_Number,3)
  SELF.AddHistoryField(?inv:Date_Created,4)
  SELF.AddHistoryField(?inv:Account_Number,5)
  SELF.AddHistoryField(?inv:Total,7)
  SELF.AddHistoryField(?inv:Vat_Rate_Labour,8)
  SELF.AddHistoryField(?inv:Vat_Rate_Parts,9)
  SELF.AddHistoryField(?inv:Vat_Rate_Retail,10)
  SELF.AddHistoryField(?inv:VAT_Number,11)
  SELF.AddHistoryField(?inv:Invoice_VAT_Number,12)
  SELF.AddHistoryField(?inv:Currency,13)
  SELF.AddHistoryField(?inv:Batch_Number,14)
  SELF.AddHistoryField(?inv:Manufacturer,15)
  SELF.AddHistoryField(?inv:Claim_Reference,16)
  SELF.AddHistoryField(?inv:Total_Claimed,17)
  SELF.AddHistoryField(?inv:Labour_Paid,19)
  SELF.AddHistoryField(?inv:Parts_Paid,20)
  SELF.AddHistoryField(?inv:Reconciled_Date,21)
  SELF.AddUpdateFile(Access:INVOICE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVOICE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','UpdateINVOICE')
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateINVOICE')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

