

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02015.INC'),ONCE        !Local module procedure declarations
                     END


UpdateORDERS PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW2::View:Browse    VIEW(ORDPARTS)
                       PROJECT(orp:Order_Number)
                       PROJECT(orp:Part_Ref_Number)
                       PROJECT(orp:Quantity)
                       PROJECT(orp:Purchase_Cost)
                       PROJECT(orp:Sale_Cost)
                       PROJECT(orp:Job_Number)
                       PROJECT(orp:Date_Received)
                       PROJECT(orp:Record_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
orp:Order_Number       LIKE(orp:Order_Number)         !List box control field - type derived from field
orp:Part_Ref_Number    LIKE(orp:Part_Ref_Number)      !List box control field - type derived from field
orp:Quantity           LIKE(orp:Quantity)             !List box control field - type derived from field
orp:Purchase_Cost      LIKE(orp:Purchase_Cost)        !List box control field - type derived from field
orp:Sale_Cost          LIKE(orp:Sale_Cost)            !List box control field - type derived from field
orp:Job_Number         LIKE(orp:Job_Number)           !List box control field - type derived from field
orp:Date_Received      LIKE(orp:Date_Received)        !List box control field - type derived from field
orp:Record_Number      LIKE(orp:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::ord:Record  LIKE(ord:RECORD),STATIC
QuickWindow          WINDOW('Update the ORDERS File'),AT(,,252,188),FONT('Arial',8,,),CENTER,IMM,HLP('UpdateORDERS'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,348,180),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Order Number'),AT(8,20),USE(?ORD:Order_Number:Prompt),TRN
                           ENTRY(@n012),AT(61,20,52,10),USE(ord:Order_Number),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Order Number')
                           PROMPT('Supplier'),AT(8,34),USE(?ORD:Supplier:Prompt),TRN
                           ENTRY(@s30),AT(61,34,124,10),USE(ord:Supplier),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Supplier Name')
                           PROMPT('Date'),AT(8,48),USE(?ORD:Date:Prompt),TRN
                           ENTRY(@d6b),AT(61,48,104,10),USE(ord:Date),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Date Ordered')
                           CHECK('Printed'),AT(61,62,70,8),USE(ord:Printed),COLOR(COLOR:Silver),VALUE('YES','NO'),MSG('Order Printed')
                           CHECK('All Received'),AT(135,62,70,8),USE(ord:All_Received),COLOR(COLOR:Silver),VALUE('YES','NO'),MSG('All Parts On Order Received')
                           PROMPT('User'),AT(8,74),USE(?ORD:User:Prompt),TRN
                           ENTRY(@s2),AT(61,74,40,10),USE(ord:User),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Order Created By')
                         END
                         TAB('ORDPARTS'),USE(?Tab:2)
                           LIST,AT(8,36,340,144),USE(?Browse:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('52R(2)|M~Order Number~C(0)@n012@52R(2)|M~Ref Number~C(0)@n012@40L(2)|M~Quantity~' &|
   'L(2)@p<<<<<<<<<<<<<<#p@60D(10)|M~Purchase Cost~C(0)@n14.2@60D(18)|M~Sale Cost~C(0)@n14.' &|
   '2@68L(2)|M~Job Number Stock~L(2)@s12@80R(2)|M~Date Received~C(0)@d6b@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(360,88,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                           BUTTON('&Change'),AT(360,112,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO')
                           BUTTON('&Delete'),AT(360,136,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                         END
                       END
                       BUTTON('&OK'),AT(12,164,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(92,164,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('DELETE THIS'),AT(172,164,76,20),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW2::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting a ORDERS Record'
  OF ChangeRecord
    ActionMessage = 'Changing a ORDERS Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateORDERS')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ORD:Order_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ord:Record,History::ord:Record)
  SELF.AddHistoryField(?ord:Order_Number,1)
  SELF.AddHistoryField(?ord:Supplier,2)
  SELF.AddHistoryField(?ord:Date,3)
  SELF.AddHistoryField(?ord:Printed,4)
  SELF.AddHistoryField(?ord:All_Received,5)
  SELF.AddHistoryField(?ord:User,6)
  SELF.AddUpdateFile(Access:ORDERS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDERS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDERS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:ORDPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','UpdateORDERS')
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,orp:Order_Number_Key)
  BRW2.AddRange(orp:Order_Number,Relate:ORDPARTS,Relate:ORDERS)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,orp:Part_Ref_Number,1,BRW2)
  BRW2.AddField(orp:Order_Number,BRW2.Q.orp:Order_Number)
  BRW2.AddField(orp:Part_Ref_Number,BRW2.Q.orp:Part_Ref_Number)
  BRW2.AddField(orp:Quantity,BRW2.Q.orp:Quantity)
  BRW2.AddField(orp:Purchase_Cost,BRW2.Q.orp:Purchase_Cost)
  BRW2.AddField(orp:Sale_Cost,BRW2.Q.orp:Sale_Cost)
  BRW2.AddField(orp:Job_Number,BRW2.Q.orp:Job_Number)
  BRW2.AddField(orp:Date_Received,BRW2.Q.orp:Date_Received)
  BRW2.AddField(orp:Record_Number,BRW2.Q.orp:Record_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW2.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateORDERS')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateORDPARTS
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

