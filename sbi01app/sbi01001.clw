

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBI01001.INC'),ONCE        !Local module procedure declarations
                     END


EDI_Defaults PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::edi:Record  LIKE(edi:RECORD),STATIC
QuickWindow          WINDOW('Motorola EDI Defaults'),AT(,,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),CENTER,ICON('pc.ico'),HLP('EDI_Defaults'),WALLPAPER('sbback.jpg'),TILED,SYSTEM,GRAY,DOUBLE,IMM
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Motorola EDI Defaults'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Motorola'),USE(?Tab:1)
                           PROMPT('&Supplier Code'),AT(270,180),USE(?EDI:Supplier_Code:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(346,180,64,10),USE(edi:Supplier_Code),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('&Country Code'),AT(270,200),USE(?EDI:Country_Code:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@S3),AT(346,200,64,10),USE(edi:Country_Code),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('&Dealer ID'),AT(270,220),USE(?EDI:Dealer_ID:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(346,220,64,10),USE(edi:Dealer_ID),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Motorola EDI Defaults'
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020544'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFEDI.Open
    SET(DEFEDI)
    CASE Access:DEFEDI.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFEDI.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('EDI_Defaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(edi:Record,History::edi:Record)
  SELF.AddHistoryField(?edi:Supplier_Code,1)
  SELF.AddHistoryField(?edi:Country_Code,3)
  SELF.AddHistoryField(?edi:Dealer_ID,4)
  SELF.AddUpdateFile(Access:DEFEDI)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFEDI.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFEDI
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:None
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EDI_Defaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFEDI)
      Access:DEFEDI.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFEDI.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','EDI_Defaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    edi:Dealer_ID = 'N/A'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020544'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020544'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020544'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

EDI_Defaults_Ericsson PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ed2:Record  LIKE(ed2:RECORD),STATIC
QuickWindow          WINDOW('EDI Defaults'),AT(0,,158,67),FONT('Arial',8,,),CENTER,IMM,ICON('pc.ico'),HLP('EDI_Defaults'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,152,32),USE(?CurrentTab),SPREAD
                         TAB('Ericsson'),USE(?Tab:1)
                           PROMPT('&Country Code'),AT(8,20),USE(?EDI:Country_Code:Prompt)
                           ENTRY(@S3),AT(84,20,64,10),USE(ed2:Country_Code),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(40,44,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(96,44,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,40,152,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFEDI2.Open
    SET(DEFEDI2)
    CASE Access:DEFEDI2.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFEDI2.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('EDI_Defaults_Ericsson')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EDI:Country_Code:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ed2:Record,History::ed2:Record)
  SELF.AddHistoryField(?ed2:Country_Code,2)
  SELF.AddUpdateFile(Access:DEFEDI2)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFEDI2.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFEDI2
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:None
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EDI_Defaults_Ericsson')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFEDI2)
      Access:DEFEDI2.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFEDI2.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','EDI_Defaults_Ericsson')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    ed2:Country_Code = 'N/A'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

AlcatelExport        PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
savepath             STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! Moving Bar Window
RejectRecord         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO

Progress:Thermometer BYTE
ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER,FONT('Arial',8,,)
     END

x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(2000)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
serv_code     STRING(4)
month         STRING(3)
repair_date   STRING(9)
serial_no     STRING(17)
total_labour  STRING(14)
total_parts   STRING(14)
currency      STRING(11)
user          STRING(20)
model         String(30)
repairtype    String(30)
FaultCode     String(30)
AccountNo     String(30)

           .

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
parts1        REAL
labour        REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBS.Open
   Relate:WARPARTS.Open
   Relate:EXCHANGE.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:STATUS.Open
   Relate:AUDIT.Open
   Relate:STAHEAD.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
    savepath = path()
    filename = 'ALC' & FOrmat(f_batch_number,@n05) & '.DAT'
    if not filedialog ('Choose File',filename,'DAT Files|*.DAT|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        !failed
        setpath(savepath)
    else!if not filedialog
        !found

        YIELD()
        RecordsPerCycle = 25
        RecordsProcessed = 0
        PercentProgress = 0
        OPEN(ProgressWindow)
        Progress:Thermometer = 0
        ?Progress:PctText{Prop:Text} = '0% Completed'
        ProgressWindow{Prop:Text} = 'Alcatel EDI Export'
        ?Progress:UserString{Prop:Text}=''
        setcursor(cursor:wait)
        DISPLAY()
        OPEN(Out_File)                           ! Open the output file
        IF ERRORCODE()                           ! If error
            CREATE(Out_File)                       ! create a new file
            If Errorcode()
                Case Missive('Unable to create EDI File.'&|
                  '<13,10>'&|
                  '<13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive

                Do Exit_Proc
            End!If Errorcode() 
            OPEN(out_file)
            EMPTY(Out_file)

        ELSE
            OPEN(out_file)
            EMPTY(Out_file)
        End
        count = 1

!Count records
            count_records# = 0
            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:edi_key)
            job:manufacturer    = 'ALCATEL'
            job:edi             = 'YES'
            job:edi_batch_number = f_batch_number
            Set(job:edi_key,job:edi_key)
            Loop
                If access:jobs.next()
                    Break
                End!If access:jobs.next()
                If job:manufacturer <> 'ALCATEL'  |
                    or job:edi      <> 'YES'         |
                    or job:edi_batch_number <> f_batch_number   |
                    then Break.
                count_records#  += 1
            End!Loop
            access:jobs.restorefile(save_job_id)
            setcursor()
        
            Recordstoprocess = count_records#

            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:edi_key)
            job:manufacturer     = 'ALCATEL'
            job:edi              = 'YES'
            job:edi_batch_number = f_batch_number
            set(job:edi_key,job:edi_key)
            loop
                !omemo(jobs)
                if access:jobs.next()
                   break
                end !if
                Do GetNextRecord2
                if job:manufacturer     <> 'ALCATEL'      |
                or job:edi              <> 'YES'      |
                or job:edi_batch_number <> f_batch_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Do Export
            end !loop
            access:jobs.restorefile(save_job_id)
            setcursor()
            Case Missive('Export Completed.','ServiceBase 3g',|
                           'midea.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        Close(out_file)
        Do exit_proc

        setpath(savepath)
    end!if not filedialog
exit_proc    ROUTINE
  SETCURSOR()
   Relate:JOBS.Close
   Relate:WARPARTS.Close
   Relate:EXCHANGE.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:STATUS.Close
   Relate:AUDIT.Close
   Relate:STAHEAD.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
  RETURN

GetNextRecord2      Routine
  RecordsProcessed += 1
  RecordsThisCycle += 1
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
    IF PercentProgress <> Progress:Thermometer THEN
      Progress:Thermometer = PercentProgress
      ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & '% Completed'
      DISPLAY()
    END
  END
EndPrintRun         Routine
    Progress:Thermometer = 100
    ?Progress:PctText{Prop:Text} = '100% Completed'
    Close(ProgressWindow)
    DISPLAY()
Export        Routine
      l1:serv_code = SUB(MAN:EDI_Account_Number,1,4)&'!'
      l1:month = FORMAT(MONTH(today()),@n02)&'!'
      l1:repair_date = FORMAT(YEAR(JOB:Date_Completed),@n04)&FORMAT(MONTH(JOB:Date_Completed),@n02)&FORMAT(DAY(JOB:Date_Completed),@n02)&'!'

        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            l1:serial_no = SUB(JOB:ESN,1,15)&'!'
        Else !IMEIError# = 1
            l1:serial_no = SUB(jot:OriginalIMEI,1,15)&'!'
        End !IMEIError# = 1
      
      l1:total_labour = SUB(FORMAT(job:labour_cost_warranty,@n013`3),1,12)&'!'
      l1:total_parts      = SUB(FORMAT(job:parts_cost_warranty,@n013`3),1,12)&'!'
      l1:currency = 'STERLING!'
      l1:user = Format(SUB(JOB:Ref_Number,1,10),@s10) &'!'
      l1:model  = Format(job:model_number,@s29) & '!'
      l1:repairtype = Format(job:repair_Type_warranty,@s29) & '!'
      l1:faultcode  = Format(job:fault_code1,@s29) & '!'
      l1:AccountNo  = Format(job:account_number,@s29)
      ADD(out_file)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
XDoNotDelete PROCEDURE                                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XDoNotDelete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPGEN.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','XDoNotDelete')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPGEN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','XDoNotDelete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
FindManufacturer     PROCEDURE  (func:Manufacturer,f:Silent) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
sav:Path             STRING(255)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = func:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            If man:EDI_Path = ''
                If f:Silent = 0
                    Case Missive('You have not setup the EDI Path for this Manufacturer.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                End ! If f:Silent = 0
                Return Level:Fatal
            Else !If man:EDI_Path = ''
                sav:Path    = Path()
                SetPath(Clip(man:EDI_Path))
                If Error()
                    If f:Silent = 0
                        Case Missive('The EDI Path for this Manufacturer does not exist.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                        End ! Case Missive
                    End ! If f:Silent = 0
                    Return Level:Fatal
                End !If Error()
                SetPath(sav:Path)
            End !If man:EDI_Path = ''

            If man:EDI_Account_Number = ''
                If f:Silent = 0
                    Case Missive('An EDI Account Number has not been setup for this Manufacturer.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                End ! If f:Silent = 0
                Return Level:Fatal
            End !If man:EDI_Account_Number = ''

            If man:nokiatype = '' and func:Manufacturer = 'NOKIA'
                If f:Silent = 0
                    Case Missive('You must select who processes the Warranty Claim.'&|
                      '<13,10>'&|
                      '<13,10>This is found in Manufacturer''s defaults.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                End ! If f:Silent = 0
                Return Level:Fatal
            End!If man:nokiatype = ''

            If func:Manufacturer = 'MOTOROLA' or func:Manufacturer = 'ERICSSON'
                Set(DEFEDI)
                If Access:DEFEDI.Next()
                    If Access:DEFEDI.PrimeRecord() = Level:Benign
                        edi:Dealer_ID = 'N/A'
                        Access:DEFEDI.TryInsert()
                    End !If Access:DEFEDI.PrimeRecord() = Level:Benign
                    If f:Silent = 0
                        Case Missive('The ' & Clip(func:Manufacturer) & ' EDI Defaults have not been setup.','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If f:Silent
                    Return Level:Fatal
                Else !If Access.DEFEDI.Next()

                End !If Access.DEFEDI.Next()

            End !If func:Manufacturer = 'MOTOROLA'
            If func:Manufacturer = 'SAGEM'
                set(defedi2)
                If access:defedi2.next()
                    ed2:country_code = 'N/A'
                    access:defedi2.insert()
                    If f:Silent = 0
                        Case Missive('The SAGEM EDI Defaults have not been setup.','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If f:Silent = 0
                    ManError# = 1
                End!If access:defedi.next()
            End !If func:Manufacturer = 'SAGEM'
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            If f:Silent = 0
                Case Missive('Cannot find the Manufacturer ' & Clip(func:Manufacturer) & '.'&|
                  '<13,10>'&|
                  '<13,10>Ensure that a Manufacturer of this name has been setup.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If f:Silent = 0
            Return Level:Fatal
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

        Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CountEDIRecords      PROCEDURE  (func:Manufacturer,func:EDI,func:BatchNumber,func:ProcessBeforeDate) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_job_ali_id      USHORT,AUTO
tmp:Count            LONG
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    SetCursor(cursor:wait)
    tmp:Count = 0
    Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.ClearKey(job_ali:EDI_Key)
    job_ali:Manufacturer     = func:Manufacturer
    job_ali:EDI              = func:EDI
    If func:BatchNumber <> 0
        job_ali:EDI_Batch_Number = func:BatchNumber    
    End !If func:BatchNumber <> 0
    Set(job_ali:EDI_Key,job_ali:EDI_Key)
    Loop
        If Access:JOBS_ALIAS.NEXT()
           Break
        End !If
        If job_ali:Manufacturer     <> func:Manufacturer      |
        Or job_ali:EDI              <> func:EDI      |
            Then Break.
        If func:BatchNumber <> 0
            If job_ali:EDI_Batch_Number <> func:BatchNumber
                Break
            End !If job_ali:EDI_Batch_Number <> func:BatchNumber
        Else !If func:BatchNumber <> 0
            If job_ali:date_completed > func:ProcessBeforeDate
                Cycle
            End!if job:date_completed > tmp:ProcessBeforeDate
        End !If func:BatchNumber <> 0
                        !POP Required?
        If man:POPRequired
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job_ali:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If ~jobe:POPConfirmed
                    Cycle
                End !If ~jobe:POPReceived

            Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                Cycle
            End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            
        End !If man:POPRequired

        tmp:Count += 1
    End !Loop
    Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
    Setcursor()

    Return tmp:Count
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
EDIPricing           PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

        !Do not reprice - L945 (DBH: 03-09-2003)
        !JobPricingRoutine

        !Clear the pretty colours

        jobe:PendingClaimColour = 0
        Access:JOBSE.Update()

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
WriteOutFault        PROCEDURE  (func:Manufacturer)   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:OutFaultCode     STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Calculate_Billing(x",job:Repair_Type_Warranty,x",tmp:OutFaultCode)

    Access:MANFAULT.ClearKey(maf:MainFaultKey)
    maf:Manufacturer = func:Manufacturer
    maf:MainFault    = 1
    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Found
        Case maf:Field_Number
            Of 1
                job:Fault_Code1 = tmp:OutFaultCode
            Of 2
                job:Fault_Code2 = tmp:OutFaultCode
            Of 3
                job:Fault_Code3 = tmp:OutFaultCode
            Of 4
                job:Fault_Code4 = tmp:OutFaultCode
            Of 5
                job:Fault_Code5 = tmp:OutFaultCode
            Of 6
                job:Fault_Code6 = tmp:OutFaultCode
            Of 7
                job:Fault_Code7 = tmp:OutFaultCode
            Of 8
                job:Fault_Code8 = tmp:OutFaultCode
            Of 9
                job:Fault_Code9 = tmp:OutFaultCode
            Of 10
                job:Fault_Code10 = tmp:OutFaultCode
            Of 11
                job:Fault_Code11 = tmp:OutFaultCode
            Of 12
                job:Fault_Code12 = tmp:OutFaultCode
            
        End !Case maf:Field_Number
    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ServiceHistoryRoutine PROCEDURE  (func:Manufacturer,f_Batch_Number,func:ProcessBeforeDate) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!    If ServiceHistoryReport(func:Manufacturer,f_batch_Number,func:ProcessBeforeDate)
!        If f_batch_Number = 0
!            If Access:EDIBATCH.PrimeRecord() = Level:Benign
!                ebt:Batch_Number    = man:Batch_Number
!                ebt:Manufacturer    = func:Manufacturer
!                If Access:EDIBATCH.TryInsert() = Level:Benign
!                    !Insert Successful
!                Else !If Access:EDIBATCH.TryInsert() = Level:Benign
!                    !Insert Failed
!                End !If Access:EDIBATCH.TryInsert() = Level:Benign
!
!                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
!                man:Manufacturer = func:Manufacturer
!                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                    !Found
!                    man:Batch_Number += 1
!                    Access:MANUFACT.Update()
!                Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!            End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
!            Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.','ServiceBase 2000',|
!                           'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!        Else !If f_batch_Number = 0
!            Case MessageEx('Re-Created Batch Number ' & f_Batch_Number & '.','ServiceBase 2000',|
!                           'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!            End!Case MessageEx
!        End !If f_batch_Number = 0
!    End !If ServiceHistoryReport('MOTOROLA',f_batch_Number,tmp:ProcessBeforeDate)
!    Return
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddOracleStatus      PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Clear Oracle Export Number, and write
    !claim date
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
        If jobe:WarrantyClaimStatus = '' Or jobe:WarrantyClaimStatus = 'PENDING'
            !This job has been claimed before
            jobe:WarrantyClaimStatus = 'SUBMITTED'
        Else !If jobe:WarrantyClaimStatus <> ''
            jobe:WarrantyClaimStatus    = 'RESUBMITTED'
        End !If jobe:WarrantyClaimStatus <> ''
        jobe:WarrantyStatusDate     = Today()
        Access:JOBSE.Update()
    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
