

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01006.INC'),ONCE        !Local module procedure declarations
                     END


EDIHeader            PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Clear(gen:record)
    gen:line1    = 'UNB+UNOA:2+5013546126849+NOKIAMPSF:ZZ+' & Format(ord:Date,@d11) & ':' & Format(Clock(),@t2) & '+' & Clip(ord:Order_Number) & '+RADIOHEAD+ORDERS'''
    access:expgen.insert()

    gen:line1    = 'UNH+17+ORDERS:D:96A:UN'''
    access:expgen.insert()

    gen:line1    = 'BGM+220+' & Clip(ord:Order_Number) & '+9'''
    access:expgen.insert()

    gen:line1    = 'DTM+2:' & Format(ord:Date,@d12) & ':102'''
    access:expgen.insert()

    gen:line1    = 'NAD+BY+5013546126849::9++COMMUNICAID LTD'''
    access:expgen.insert()

    gen:line1    = 'RFF+IA:831'''
    access:expgen.insert()

    gen:line1    = 'NAD+SU+NMPFI::9'''
    access:expgen.insert()

    gen:line1    = 'NAD+DP+5013546126849::9++KINGFISHER WAY+HINCHINGBROOKE BUSINESS PARK:HUNTINGDON:CAMBS+++PE29 6FL'''
    access:expgen.insert()

    gen:line1    = 'CUX+2:EUR:9'''

    !was  = 'CUX+2:GBP:9'''

    access:expgen.insert()
!Lines 8
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
EDIBody              PROCEDURE  (func:line)           ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!added to ensure!
    SET(Defaults,0)
    Access:Defaults.Next()

    gen:line1    = 'LIN+' & CLip(func:Line) & '++' & Clip(glo:q_part_number) & ':EN'''
    access:expgen.insert()

    gen:line1    = 'IMD+F++TU::9:' & Clip(glo:q_description) & ''''
    access:expgen.insert()

    gen:line1    = 'QTY+21:' & Clip(glo:q_quantity) & ''''
    access:expgen.insert()

    gen:line1    = 'PRI+AAA:' & Clip(FORMAT((glo:q_purchase_cost*Clip(GETINI('EDI','PartsOrderEuroRate',,CLIP(PATH())&'\SB2KDEF.INI'))),@n11.2) * glo:q_quantity) & ''''
    access:expgen.insert()

    gen:line1    = 'TAX+7+VAT+++:::17.50+S'''
    access:expgen.insert()
!Lines 5
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
EDIFooter            PROCEDURE  (func:Count,func:Line) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    gen:line1    = 'UNS+S'''
    access:expgen.insert()

    gen:line1   = 'CNT+' & Clip(func:Line-1) & ':1'''
    access:expgen.insert()

    gen:line1   = 'UNT+' & Clip(func:Count) & '+17'''
    access:expgen.insert()
    gen:line1   = 'UNZ+1+' & Clip(ord:Order_Number) & ''''
    access:expgen.insert()

!Line 2
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ChangePartNumber PROCEDURE (f:OldPartNumber,f:OldDescription,f:NewPartNumber,f:NewDescription) !Generated from procedure template - Window

tmp:OriginalPartNumber STRING(30)
tmp:OriginalDescription STRING(30)
tmp:NewPartNumber    STRING(30)
tmp:NewDescription   STRING(30)
tmp:Return           BYTE(0)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       GROUP('Current Details'),AT(212,138,256,58),USE(?Group2),BOXED,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Part Number'),AT(228,151),USE(?tmp:OriginalPartNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(324,154,124,10),USE(tmp:OriginalPartNumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number'),TIP('Part Number'),UPR,READONLY
                         PROMPT('Description'),AT(228,174),USE(?tmp:OriginalDescription:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(324,174,124,10),USE(tmp:OriginalDescription),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Description'),TIP('Description'),UPR,READONLY
                       END
                       GROUP('New Details'),AT(212,218,256,58),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Part Number'),AT(228,234),USE(?tmp:NewPartNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(324,234,124,10),USE(tmp:NewPartNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number'),TIP('Part Number'),REQ,UPR
                         PROMPT('Description'),AT(228,250),USE(?tmp:NewDescription:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(324,252,124,10),USE(tmp:NewDescription),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Description'),TIP('Description'),REQ,UPR
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Change Part Number'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020696'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ChangePartNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  SELF.FilesOpened = True
  tmp:OriginalPartNumber = f:OldPartNumber
  tmp:OriginalDescription = f:OldDescription
  tmp:NewPartNumber = f:OldPartNumber
  tmp:NewDescription = f:OldDescription
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','ChangePartNumber')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ChangePartNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If tmp:NewPartNumber = '' Or tmp:NewDescription = ''
          Cycle
      End ! If tmp:NewPartNumber = '' Or tmp:NewDescription = ''
      If tmp:NewPartNumber = tmp:OriginalPartNumber And tmp:NewDescription = tmp:OriginalDescription
          Case Missive('You have not made any changes to the part number or description.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If tmp:NewPartNumber = tmp:OriginalPartNumber And tmp:NewDescription = tmp:OriginalDescription
      
      
      ! Inserting (DBH 03/03/2008) # 9820 - Check if the "New" part number already exists in stock.
      ! Inserting (DBH 13/05/2008) # 10034 - Only check if the part number is changing
      If tmp:NewPartNumber <> tmp:OriginalPartNumber
      ! End (DBH 13/05/2008) #10034
          Found# = 0
          Access:LOCATION.Clearkey(loc:Location_Key)
          loc:Location = ''
          Set(loc:Location_Key,loc:Location_Key)
          Loop
              If Access:LOCATION.Next()
                  Break
              End ! If Access:LOCATION.Next()
              If ~loc:Active
                  Cycle
              End ! If ~loc:Active
              Access:STOCK_ALIAS.Clearkey(sto_ali:Location_Key)
              sto_ali:Location = loc:Location
              sto_ali:Part_Number = tmp:NewPartNumber
              If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                  Found# = 1
                  Break
              End ! If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
          End ! Loop
      
          If Found# = 1
              Beep(Beep:SystemHand);  Yield()
              Case Missive('Error!'&|
                  '|The selected New Part Number already exists.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Found# = 1
      End ! If tmp:NewPartNumber <> tmp:OriginalPartNumber
      ! End (DBH 03/03/2008) #9820
      
      
      Case Missive('Are you sure you want to update ALL occurrences of this part?'&|
        '|(This may take a few minutes to complete)','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              f:NewPartNumber = tmp:NewPartNumber
              f:NewDescription = tmp:NewDescription
              tmp:Return = 1
          Of 1 ! No Button
              Cycle
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020696'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020696'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020696'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BrowsePartNumberHistory PROCEDURE (f:StockRefNumber)  !Generated from procedure template - Browse

tmp:UserName         STRING(60)
tmp:StockRefNumber   LONG
BRW8::View:Browse    VIEW(STOPARTS)
                       PROJECT(spt:DateChanged)
                       PROJECT(spt:TimeChanged)
                       PROJECT(spt:OldPartNumber)
                       PROJECT(spt:OldDescription)
                       PROJECT(spt:NewPartNumber)
                       PROJECT(spt:NewDescription)
                       PROJECT(spt:Notes)
                       PROJECT(spt:RecordNumber)
                       PROJECT(spt:STOCKRefNumber)
                       JOIN(USERS,'use:User_Code = spt:UserCode')
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
spt:DateChanged        LIKE(spt:DateChanged)          !List box control field - type derived from field
spt:TimeChanged        LIKE(spt:TimeChanged)          !List box control field - type derived from field
tmp:UserName           LIKE(tmp:UserName)             !List box control field - type derived from local data
spt:OldPartNumber      LIKE(spt:OldPartNumber)        !List box control field - type derived from field
spt:OldDescription     LIKE(spt:OldDescription)       !List box control field - type derived from field
spt:NewPartNumber      LIKE(spt:NewPartNumber)        !List box control field - type derived from field
spt:NewDescription     LIKE(spt:NewDescription)       !List box control field - type derived from field
spt:Notes              LIKE(spt:Notes)                !List box control field - type derived from field
spt:RecordNumber       LIKE(spt:RecordNumber)         !Primary key field - type derived from field
spt:STOCKRefNumber     LIKE(spt:STOCKRefNumber)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Browse Part Number History'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(4,28,672,352),USE(?Panel4),FILL(09A6A7CH)
                       LIST,AT(8,32,664,290),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('44R(2)|M~Date~@d6@24R(2)|M~Time~@t1b@80L(2)|M~User Name~@s60@130L(2)|M~Old Part ' &|
   'Number~@s30@130L(2)|M~Old Description~@s30@130L(2)|M~New Part Number~@s30@120L(2' &|
   ')|M~New Description~@s30@0L(2)|M~Where Changed~@s255@'),FROM(Queue:Browse)
                       PROMPT('Where Part No Changed'),AT(8,324),USE(?spt:Notes:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(100,326,184,52),USE(spt:Notes),SKIP,VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Notes'),TIP('Notes'),UPR,READONLY
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(604,386),USE(?Cancel),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020695'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowsePartNumberHistory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STOPARTS.Open
  SELF.FilesOpened = True
  tmp:StockRefNumber =f:StockRefNumber
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:STOPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowsePartNumberHistory')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,spt:DateChangedKey)
  BRW8.AddRange(spt:STOCKRefNumber,tmp:StockRefNumber)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,spt:DateChanged,1,BRW8)
  BIND('tmp:UserName',tmp:UserName)
  BRW8.AddField(spt:DateChanged,BRW8.Q.spt:DateChanged)
  BRW8.AddField(spt:TimeChanged,BRW8.Q.spt:TimeChanged)
  BRW8.AddField(tmp:UserName,BRW8.Q.tmp:UserName)
  BRW8.AddField(spt:OldPartNumber,BRW8.Q.spt:OldPartNumber)
  BRW8.AddField(spt:OldDescription,BRW8.Q.spt:OldDescription)
  BRW8.AddField(spt:NewPartNumber,BRW8.Q.spt:NewPartNumber)
  BRW8.AddField(spt:NewDescription,BRW8.Q.spt:NewDescription)
  BRW8.AddField(spt:Notes,BRW8.Q.spt:Notes)
  BRW8.AddField(spt:RecordNumber,BRW8.Q.spt:RecordNumber)
  BRW8.AddField(spt:STOCKRefNumber,BRW8.Q.spt:STOCKRefNumber)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOPARTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowsePartNumberHistory')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020695'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020695'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020695'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  tmp:UserName = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
  PARENT.SetQueueRecord
  SELF.Q.tmp:UserName = tmp:UserName                  !Assign formula result to display queue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

StockFaultCodesWindow PROCEDURE                       !Generated from procedure template - Window

tmp:PartFaultCode    STRING(30),DIM(12)
tmp:JobFaultCode     STRING(255),DIM(20)
FieldNumbersQueue    QUEUE,PRE(field)
ScreenOrder          LONG
FieldNumber          LONG
                     END
Field2NumbersQueue   QUEUE,PRE(field2)
ScreenOrder          LONG
FieldNumber          LONG
                     END
PromptNumbersQueue   QUEUE,PRE(prompt)
ScreenOrder          LONG
FieldNumber          LONG
                     END
Prompt2NumbersQueue  QUEUE,PRE(prompt2)
ScreenOrder          LONG
FieldNumber          LONG
                     END
LookupNumbersQueue   QUEUE,PRE(lookup)
ScreenOrder          LONG
FieldNumber          LONG
                     END
Lookup2NumbersQueue  QUEUE,PRE(lookup2)
ScreenOrder          LONG
FieldNumber          LONG
                     END
Lookup3NumbersQueue  QUEUE,PRE(lookup3)
ScreenOrder          LONG
FieldNumber          LONG
                     END
FaultCodeQueue       QUEUE,PRE(fault)
FaultNumber          LONG
FaultCode            STRING(30)
                     END
ActionMessage        CSTRING(40)
window               WINDOW('Stock Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Stock Fault Codes'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(4,30,672,350),USE(?Sheet1),SPREAD
                         TAB('Part Fault Codes'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(424,49),USE(?Button:FaultCode:1),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           PROMPT('Fault Code'),AT(196,51,92,16),USE(?Prompt:FaultCode:1),HIDE
                           ENTRY(@s30),AT(292,51,124,10),USE(tmp:PartFaultCode[1]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(500,75),USE(?Button:FaultCodeMultiple:2),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           PROMPT('Fault Code'),AT(196,78,92,16),USE(?Prompt:FaultCode:2),HIDE
                           ENTRY(@s30),AT(292,78,124,10),USE(tmp:PartFaultCode[2]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s30),AT(292,102,124,10),USE(tmp:PartFaultCode[3]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(424,75),USE(?Button:FaultCode:2),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           PROMPT('Fault Code'),AT(196,102,92,16),USE(?Prompt:FaultCode:3),HIDE
                           ENTRY(@s30),AT(292,126,124,10),USE(tmp:PartFaultCode[4]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(424,99),USE(?Button:FaultCode:3),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           BUTTON,AT(500,99),USE(?Button:FaultCodeMultiple:3),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(500,49),USE(?Button:FaultCodeMultiple:1),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           ENTRY(@s30),AT(292,150,124,10),USE(tmp:PartFaultCode[5]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(424,123),USE(?Button:FaultCode:4),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           PROMPT('Fault Code'),AT(196,174,92,16),USE(?Prompt:FaultCode:6),HIDE
                           PROMPT('Fault Code'),AT(196,150,92,16),USE(?Prompt:FaultCode:5),HIDE
                           PROMPT('Fault Code'),AT(196,126,92,16),USE(?Prompt:FaultCode:4),HIDE
                           ENTRY(@s30),AT(292,174,124,10),USE(tmp:PartFaultCode[6]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(424,147),USE(?Button:FaultCode:5),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           BUTTON,AT(500,123),USE(?Button:FaultCodeMultiple:4),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           PROMPT('Fault Code'),AT(196,198,92,16),USE(?Prompt:FaultCode:7),HIDE
                           ENTRY(@s30),AT(292,198,124,10),USE(tmp:PartFaultCode[7]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(500,171),USE(?Button:FaultCodeMultiple:6),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(424,171),USE(?Button:FaultCode:6),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           PROMPT('Fault Code'),AT(196,225,92,16),USE(?Prompt:FaultCode:8),HIDE
                           ENTRY(@s30),AT(292,225,124,10),USE(tmp:PartFaultCode[8]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s30),AT(292,249,124,10),USE(tmp:PartFaultCode[9]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(500,219),USE(?Button:FaultCodeMultiple:8),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(424,219),USE(?Button:FaultCode:8),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           BUTTON,AT(500,195),USE(?Button:FaultCodeMultiple:7),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(424,195),USE(?Button:FaultCode:7),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           BUTTON,AT(500,147),USE(?Button:FaultCodeMultiple:5),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           PROMPT('Fault Code'),AT(196,273,92,16),USE(?Prompt:FaultCode:10),HIDE
                           PROMPT('Fault Code'),AT(196,249,92,16),USE(?Prompt:FaultCode:9),HIDE
                           ENTRY(@s30),AT(292,273,124,10),USE(tmp:PartFaultCode[10]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(424,243),USE(?Button:FaultCode:9),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           PROMPT('Fault Code'),AT(196,297,92,16),USE(?Prompt:FaultCode:11),HIDE
                           ENTRY(@s30),AT(292,297,124,10),USE(tmp:PartFaultCode[11]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(424,291),USE(?Button:FaultCode:11),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           BUTTON,AT(500,291),USE(?Button:FaultCodeMultiple:11),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(424,267),USE(?Button:FaultCode:10),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                           BUTTON,AT(500,267),USE(?Button:FaultCodeMultiple:10),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(500,243),USE(?Button:FaultCodeMultiple:9),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           PROMPT('Fault Code'),AT(196,321,92,16),USE(?Prompt:FaultCode:12),HIDE
                           ENTRY(@s30),AT(292,321,124,10),USE(tmp:PartFaultCode[12]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(500,315),USE(?Button:FaultCodeMultiple:12),TRN,FLAT,HIDE,ICON('lookupmp.jpg')
                           BUTTON,AT(424,315),USE(?Button:FaultCode:12),TRN,FLAT,HIDE,ICON('lookupsp.jpg')
                         END
                         TAB('Job Fault Codes'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,59,96,19),USE(?Prompt:JobFaultCode:1),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(136,59,124,10),USE(tmp:JobFaultCode[1]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(264,57),USE(?Button:Lookup:1),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(384,83,96,19),USE(?Prompt:JobFaultCode:12),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(136,83,124,10),USE(tmp:JobFaultCode[2]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code'),AT(8,83,96,19),USE(?Prompt:JobFaultCode:2),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(516,83,124,10),USE(tmp:JobFaultCode[12]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(136,107,124,10),USE(tmp:JobFaultCode[3]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(264,105),USE(?Button:Lookup:3),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(516,107,124,10),USE(tmp:JobFaultCode[13]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(644,105),USE(?Button:Lookup:13),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(264,81),USE(?Button:Lookup:2),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(644,81),USE(?Button:Lookup:12),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(384,107,96,19),USE(?Prompt:JobFaultCode:13),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,107,96,19),USE(?Prompt:JobFaultCode:3),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(136,131,124,10),USE(tmp:JobFaultCode[4]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code'),AT(384,155,96,19),USE(?Prompt:JobFaultCode:15),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(384,131,96,19),USE(?Prompt:JobFaultCode:14),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,131,96,19),USE(?Prompt:JobFaultCode:4),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(516,155,124,10),USE(tmp:JobFaultCode[15]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(644,129),USE(?Button:Lookup:14),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(516,131,124,10),USE(tmp:JobFaultCode[14]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(136,155,124,10),USE(tmp:JobFaultCode[5]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(264,129),USE(?Button:Lookup:4),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(384,179,96,19),USE(?Prompt:JobFaultCode:16),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(516,179,124,10),USE(tmp:JobFaultCode[16]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(644,174),USE(?Button:Lookup:16),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(136,179,124,10),USE(tmp:JobFaultCode[6]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(264,153),USE(?Button:Lookup:5),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(644,153),USE(?Button:Lookup:15),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(384,206,96,19),USE(?Prompt:JobFaultCode:17),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(384,233,96,19),USE(?Prompt:JobFaultCode:18),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,179,96,19),USE(?Prompt:JobFaultCode:6),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,155,96,19),USE(?Prompt:JobFaultCode:5),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(136,206,124,10),USE(tmp:JobFaultCode[7]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(264,202),USE(?Button:Lookup:7),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(264,177),USE(?Button:Lookup:6),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(516,206,124,10),USE(tmp:JobFaultCode[17]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code'),AT(8,206,96,19),USE(?Prompt:JobFaultCode:7),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(644,202),USE(?Button:Lookup:17),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(136,233,124,10),USE(tmp:JobFaultCode[8]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(644,227),USE(?Button:Lookup:18),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(516,233,124,10),USE(tmp:JobFaultCode[18]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(264,227),USE(?Button:Lookup:8),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(8,233,96,19),USE(?Prompt:JobFaultCode:8),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(516,59,124,10),USE(tmp:JobFaultCode[11]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(136,258,124,10),USE(tmp:JobFaultCode[9]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           PROMPT('Fault Code'),AT(384,258,96,19),USE(?Prompt:JobFaultCode:19),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(264,254),USE(?Button:Lookup:9),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           ENTRY(@s255),AT(516,258,124,10),USE(tmp:JobFaultCode[19]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(644,254),USE(?Button:Lookup:19),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(264,281),USE(?Button:Lookup:10),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           BUTTON,AT(644,281),USE(?Button:Lookup:20),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(384,283,96,19),USE(?Prompt:JobFaultCode:20),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,283,96,19),USE(?Prompt:JobFaultCode:10),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Fault Code'),AT(8,258,96,19),USE(?Prompt:JobFaultCode:9),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(136,283,124,10),USE(tmp:JobFaultCode[10]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           ENTRY(@s255),AT(516,283,124,10),USE(tmp:JobFaultCode[20]),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Fault Code'),TIP('Fault Code'),UPR
                           BUTTON,AT(644,57),USE(?Button:Lookup:11),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Fault Code'),AT(384,59,96,19),USE(?Prompt:JobFaultCode:11),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(588,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(536,384),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
local       Class
LookupFaultCode    Procedure(long f:FieldNumber, String f:FaultCode),String
LookupButtonCode   Procedure(Long f:FieldNumber,String f:Field),String
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
BuildNumbersQueue        Routine
    Free(FieldNumbersQueue)
    Free(PromptNumbersQueue)
    Free(LookupNumbersQueue)
    Free(Lookup2NumbersQueue)
    Free(FaultCodeQueue)

!    fault:FaultNumber   = 1
!    fault:FaultCode     = stm:FaultCode1
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 2
!    fault:FaultCode     = stm:FaultCode2
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 3
!    fault:FaultCode     = stm:FaultCode3
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 4
!    fault:FaultCode     = stm:FaultCode4
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 5
!    fault:FaultCode     = stm:FaultCode5
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 6
!    fault:FaultCode     = stm:FaultCode6
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 7
!    fault:FaultCode     = stm:FaultCode7
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 8
!    fault:FaultCode     = stm:FaultCode8
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 9
!    fault:FaultCode     = stm:FaultCode9
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 10
!    fault:FaultCode     = stm:FaultCode10
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 11
!    fault:FaultCode     = stm:FaultCode11
!    Add(FaultCodeQueue)
!    fault:FaultNumber   = 12
!    fault:FaultCode     = stm:FaultCode12
!    Add(FaultCodeQueue)

    field:ScreenOrder = 1
    field:FieldNumber = ?tmp:PartFaultCode_1
    Add(FieldNumbersQueue)
    field:ScreenOrder = 2
    field:FieldNumber = ?tmp:PartFaultCode_2
    Add(FieldNumbersQueue)
    field:ScreenOrder = 3
    field:FieldNumber = ?tmp:PartFaultCode_3
    Add(FieldNumbersQueue)
    field:ScreenOrder = 4
    field:FieldNumber = ?tmp:PartFaultCode_4
    Add(FieldNumbersQueue)
    field:ScreenOrder = 5
    field:FieldNumber = ?tmp:PartFaultCode_5
    Add(FieldNumbersQueue)
    field:ScreenOrder = 6
    field:FieldNumber = ?tmp:PartFaultCode_6
    Add(FieldNumbersQueue)
    field:ScreenOrder = 7
    field:FieldNumber = ?tmp:PartFaultCode_7
    Add(FieldNumbersQueue)
    field:ScreenOrder = 8
    field:FieldNumber = ?tmp:PartFaultCode_8
    Add(FieldNumbersQueue)
    field:ScreenOrder = 9
    field:FieldNumber = ?tmp:PartFaultCode_9
    Add(FieldNumbersQueue)
    field:ScreenOrder = 10
    field:FieldNumber = ?tmp:PartFaultCode_10
    Add(FieldNumbersQueue)
    field:ScreenOrder = 11
    field:FieldNumber = ?tmp:PartFaultCode_11
    Add(FieldNumbersQueue)
    field:ScreenOrder = 12
    field:FieldNumber = ?tmp:PartFaultCode_12
    Add(FieldNumbersQueue)

    lookup:ScreenOrder = 1
    lookup:FieldNumber = ?Button:FaultCode:1
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 2
    lookup:FieldNumber = ?Button:FaultCode:2
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 3
    lookup:FieldNumber = ?Button:FaultCode:3
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 4
    lookup:FieldNumber = ?Button:FaultCode:4
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 5
    lookup:FieldNumber = ?Button:FaultCode:5
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 6
    lookup:FieldNumber = ?Button:FaultCode:6
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 7
    lookup:FieldNumber = ?Button:FaultCode:7
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 8
    lookup:FieldNumber = ?Button:FaultCode:8
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 9
    lookup:FieldNumber = ?Button:FaultCode:9
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 10
    lookup:FieldNumber = ?Button:FaultCode:10
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 11
    lookup:FieldNumber = ?Button:FaultCode:11
    Add(LookupNumbersQueue)
    lookup:ScreenOrder = 12
    lookup:FieldNumber = ?Button:FaultCode:12
    Add(LookupNumbersQueue)

    lookup2:ScreenOrder = 1
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:1
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 2
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:2
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 3
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:3
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 4
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:4
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 5
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:5
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 6
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:6
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 7
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:7
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 8
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:8
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 9
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:9
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 10
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:10
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 11
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:11
    Add(Lookup2NumbersQueue)
    lookup2:ScreenOrder = 12
    lookup2:FieldNumber = ?Button:FaultCodeMultiple:12
    Add(Lookup2NumbersQueue)

    prompt:ScreenOrder = 1
    prompt:FieldNumber = ?Prompt:FaultCode:1
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 2
    prompt:FieldNumber = ?Prompt:FaultCode:2
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 3
    prompt:FieldNumber = ?Prompt:FaultCode:3
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 4
    prompt:FieldNumber = ?Prompt:FaultCode:4
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 5
    prompt:FieldNumber = ?Prompt:FaultCode:5
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 6
    prompt:FieldNumber = ?Prompt:FaultCode:6
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 7
    prompt:FieldNumber = ?Prompt:FaultCode:7
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 8
    prompt:FieldNumber = ?Prompt:FaultCode:8
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 9
    prompt:FieldNumber = ?Prompt:FaultCode:9
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 10
    prompt:FieldNumber = ?Prompt:FaultCode:10
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 11
    prompt:FieldNumber = ?Prompt:FaultCode:11
    Add(PromptNumbersQueue)
    prompt:ScreenOrder = 12
    prompt:FieldNumber = ?Prompt:FaultCode:12
    Add(PromptNumbersQueue)

    field2:ScreenOrder = 1
    field2:FieldNumber = ?tmp:JobFaultCode_1
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 2
    field2:FieldNumber = ?tmp:JobFaultCode_2
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 3
    field2:FieldNumber = ?tmp:JobFaultCode_3
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 4
    field2:FieldNumber = ?tmp:JobFaultCode_4
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 5
    field2:FieldNumber = ?tmp:JobFaultCode_5
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 6
    field2:FieldNumber = ?tmp:JobFaultCode_6
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 7
    field2:FieldNumber = ?tmp:JobFaultCode_7
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 8
    field2:FieldNumber = ?tmp:JobFaultCode_8
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 9
    field2:FieldNumber = ?tmp:JobFaultCode_9
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 10
    field2:FieldNumber = ?tmp:JobFaultCode_10
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 11
    field2:FieldNumber = ?tmp:JobFaultCode_11
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 12
    field2:FieldNumber = ?tmp:JobFaultCode_12
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 13
    field2:FieldNumber = ?tmp:JobFaultCode_13
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 14
    field2:FieldNumber = ?tmp:JobFaultCode_14
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 15
    field2:FieldNumber = ?tmp:JobFaultCode_15
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 16
    field2:FieldNumber = ?tmp:JobFaultCode_16
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 17
    field2:FieldNumber = ?tmp:JobFaultCode_17
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 18
    field2:FieldNumber = ?tmp:JobFaultCode_18
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 19
    field2:FieldNumber = ?tmp:JobFaultCode_19
    Add(Field2NumbersQueue)
    field2:ScreenOrder = 20
    field2:FieldNumber = ?tmp:JobFaultCode_20
    Add(Field2NumbersQueue)

    lookup3:ScreenOrder = 1
    lookup3:FieldNumber = ?Button:Lookup:1
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 2
    lookup3:FieldNumber = ?Button:Lookup:2
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 3
    lookup3:FieldNumber = ?Button:Lookup:3
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 4
    lookup3:FieldNumber = ?Button:Lookup:4
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 5
    lookup3:FieldNumber = ?Button:Lookup:5
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 6
    lookup3:FieldNumber = ?Button:Lookup:6
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 7
    lookup3:FieldNumber = ?Button:Lookup:7
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 8
    lookup3:FieldNumber = ?Button:Lookup:8
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 9
    lookup3:FieldNumber = ?Button:Lookup:9
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 10
    lookup3:FieldNumber = ?Button:Lookup:10
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 11
    lookup3:FieldNumber = ?Button:Lookup:11
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 12
    lookup3:FieldNumber = ?Button:Lookup:12
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 13
    lookup3:FieldNumber = ?Button:Lookup:13
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 14
    lookup3:FieldNumber = ?Button:Lookup:14
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 15
    lookup3:FieldNumber = ?Button:Lookup:15
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 16
    lookup3:FieldNumber = ?Button:Lookup:16
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 17
    lookup3:FieldNumber = ?Button:Lookup:17
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 18
    lookup3:FieldNumber = ?Button:Lookup:18
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 19
    lookup3:FieldNumber = ?Button:Lookup:19
    Add(Lookup3NumbersQueue)
    lookup3:ScreenOrder = 20
    lookup3:FieldNumber = ?Button:Lookup:20
    Add(Lookup3NumbersQueue)


    prompt2:ScreenOrder = 1
    prompt2:FieldNumber = ?Prompt:JobFaultCode:1
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 2
    prompt2:FieldNumber = ?Prompt:JobFaultCode:2
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 3
    prompt2:FieldNumber = ?Prompt:JobFaultCode:3
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 4
    prompt2:FieldNumber = ?Prompt:JobFaultCode:4
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 5
    prompt2:FieldNumber = ?Prompt:JobFaultCode:5
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 6
    prompt2:FieldNumber = ?Prompt:JobFaultCode:6
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 7
    prompt2:FieldNumber = ?Prompt:JobFaultCode:7
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 8
    prompt2:FieldNumber = ?Prompt:JobFaultCode:8
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 9
    prompt2:FieldNumber = ?Prompt:JobFaultCode:9
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 10
    prompt2:FieldNumber = ?Prompt:JobFaultCode:10
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 11
    prompt2:FieldNumber = ?Prompt:JobFaultCode:11
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 12
    prompt2:FieldNumber = ?Prompt:JobFaultCode:12
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 13
    prompt2:FieldNumber = ?Prompt:JobFaultCode:13
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 14
    prompt2:FieldNumber = ?Prompt:JobFaultCode:14
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 15
    prompt2:FieldNumber = ?Prompt:JobFaultCode:15
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 16
    prompt2:FieldNumber = ?Prompt:JobFaultCode:16
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 17
    prompt2:FieldNumber = ?Prompt:JobFaultCode:17
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 18
    prompt2:FieldNumber = ?Prompt:JobFaultCode:18
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 19
    prompt2:FieldNumber = ?Prompt:JobFaultCode:19
    Add(Prompt2NumbersQueue)
    prompt2:ScreenOrder = 20
    prompt2:FieldNumber = ?Prompt:JobFaultCode:20
    Add(Prompt2NumbersQueue)

FaultCode            Routine
    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = stm:Manufacturer
    map:ScreenOrder = 0
    Set(map:ScreenOrderKey,map:ScreenOrderKey)
    Loop
        If Access:MANFAUPA.Next()
            Break
        End ! If Access:MANFAUPA.Next()
        If map:Manufacturer <> stm:Manufacturer
            Break
        End ! If map:Manufacturer <> stm:Manufacturer
        If map:ScreenOrder = 0
            Cycle
        End ! If map:ScreenOrder

        prompt:ScreenOrder = map:ScreenOrder
        Get(PromptNumbersQueue,prompt:ScreenOrder)
        field:ScreenOrder = map:ScreenOrder
        Get(FieldNumbersQueue,prompt:ScreenOrder)
        lookup:ScreenOrder = map:ScreenOrder
        Get(LookupNumbersQueue,lookup:ScreenOrder)
        lookup2:ScreenOrder = map:ScreenOrder
        Get(Lookup2NumbersQueue,lookup2:ScreenOrder)
        field:FieldNumber{prop:ReadOnly} = 0
        field:FieldNumber{prop:Skip} = 0

        field:FieldNumber{prop:Hide} = 0
        prompt:FieldNumber{prop:Hide} = 0
        prompt:FieldNumber{prop:Text} = map:Field_Name
        If map:Lookup = 'YES'
            Found# = 0
            ! See if any multiple entries have been added, if so disable the single field. (DBH: 26/10/2007)
            Access:STOMPFAU.Clearkey(stu:FieldKey)
            stu:RefNumber = stm:RecordNumber
            stu:FieldNumber = map:Field_Number
            Set(stu:FieldKey,stu:FieldKey)
            Loop
                If Access:STOMPFAU.Next()
                    Break
                End ! If Access:STOMPFAU.Next()
                If stu:RefNumber <> stm:RecordNumber
                    Break
                End ! If stu:RefNumber <> stm:RecordNumber
                If stu:FieldNumber <> map:Field_Number
                    Break
                End ! If stu:FieldNumber <> map:Field_Number
                Found# = 1
                Break
            End ! Loop
            If Found#
                lookup:FieldNumber{prop:Hide} = 1
                lookup2:FieldNumber{prop:Hide} = 0
                tmp:PartFaultCode[map:ScreenOrder] = '** MULTIPLE VALUES **'
                field:FieldNumber{prop:ReadOnly} = 1
                field:FieldNumber{prop:Skip} = 1
            Else ! If Access:STOMPFAU.TryFetch(stu:FieldKey) = Level:Benign
                lookup:FieldNumber{prop:Hide} = 0
                lookup2:FieldNumber{prop:Hide} = 0
                If tmp:PartFaultCode[map:ScreenOrder] = '** MULTIPLE VALUES **'
                    tmp:PartFaultCode[map:ScreenOrder] = ''
                End ! If tmp:PartFaultCode[map:ScreenOrder] = '** MULTIPLE VALUES **'
            End ! If Access:STOMPFAU.TryFetch(stu:FieldKey) = Level:Benign
        End ! If map:Lookup = 'YES'
    End ! Loop

    Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
    maf:Manufacturer = stm:Manufacturer
    maf:ScreenOrder = 0
    Set(maf:ScreenOrderKey,maf:ScreenOrderKey)
    Loop
        If Access:MANFAULT.Next()
            Break
        End ! If Access:MANFAULT.Next()
        If maf:Manufacturer <> stm:Manufacturer
            Break
        End ! If maf:Manufacturer <> job:Manufacturer
        If maf:ScreenOrder = 0
            Cycle
        End ! If maf:ScreenOrder = 0
        If maf:MainFault
            Cycle
        End ! If maf:MainFault

        prompt2:ScreenOrder = maf:ScreenOrder
        Get(Prompt2NumbersQueue,prompt2:ScreenOrder)
        field2:ScreenOrder = maf:ScreenOrder
        Get(Field2NumbersQueue,field2:ScreenOrder)
        lookup3:ScreenOrder = maf:ScreenOrder
        Get(Lookup3NumbersQueue,lookup3:ScreenOrder)

        If maf:NotAvailable
            Cycle
        End ! If maf:NotAvailable

        prompt2:FieldNumber{prop:Hide} = 0
        field2:FieldNumber{prop:Hide} = 0
        If maf:Lookup = 'YES'
            lookup3:FieldNumber{prop:Hide} = 0
        End ! If maf:Lookup = 'YES'

        prompt2:FieldNumber{prop:Text} = maf:Field_Name

        Case maf:Field_Type
        Of 'DATE'
            field2:FieldNumber{prop:Text} = Clip(maf:DateType)
        Of 'STRING'
            If maf:RestrictLength
                field2:FieldNumber{prop:Text} = '@s' & maf:LengthTo
            Else ! If maf:RestrictLength
                If maf:Field_Number = 10 Or maf:Field_Number = 11 Or maf:Field_Number = 12
                    field2:FieldNumber{prop:Text} = '@s255'
                Else ! If maf:Field_Number = 10 Or maf:Field_Number = 11 Or maf:Field_Number = 12
                    field2:FieldNumber{prop:Text} = '@s30'
                End ! If maf:Field_Number = 10 Or maf:Field_Number = 11 Or maf:Field_Number = 12
            End ! If maf:RestrictLength
        Of 'NUMBER'
            If maf:RestrictLength
                field2:FieldNumber{prop:Text} = '@n_' & maf:LengthTo
            Else ! If maf:RestrictLength
                field2:FieldNumber{prop:Text} = '@n_9'
            End ! If maf:RestrictLength

        End ! Case maf:Field_Type
    End ! Loop

    Bryan.CompFieldColour()
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020701'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StockFaultCodesWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:STOMODEL)
  Relate:MANFAULT.Open
  Access:MANFAUPA.UseFile
  Access:STOMPFAU.UseFile
  Access:STOMJFAU.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Get Values
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 0
      Set(map:ScreenOrderKey,map:ScreenOrderKey)
      Loop
          If Access:MANFAUPA.Next()
              Break
          End ! If Access:MANFAUPA.Next()
          If map:Manufacturer <> stm:Manufacturer
              Break
          End ! If map:Manufacturer <> stm:Manufacturer
          If map:ScreenOrder = 0
              Cycle
          End ! If map:ScreenOrder
  
          Case map:Field_Number
          Of 1
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode1
          Of 2
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode2
          Of 3
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode3
          Of 4
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode4
          Of 5
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode5
          Of 6
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode6
          Of 7
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode7
          Of 8
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode8
          Of 9
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode9
          Of 10
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode10
          Of 11
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode11
          Of 12
              tmp:PartFaultCode[map:ScreenOrder] = stm:FaultCode12
          End ! Case map:Field_Number
      End ! Loop
  
      Access:STOMJFAU.Clearkey(stj:FieldKey)
      stj:RefNumber = stm:RecordNumber
      If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
          ! Get the Job Fault Code child file. Create it, if it doesn't exist (DBH: 30/10/2007)
      Else ! If Access:STOMJFAU.TryFetch(stj:Field_Key) = Level:Benign
          If Access:STOMJFAU.PrimeRecord() = Level:Benign
              stj:RefNumber = stm:RecordNumber
              If Access:STOMJFAU.TryInsert() = Level:Benign
  
              Else ! If Access:STOMJFAU.TryInsert() = Level:Benign
                  Access:STOMJFAU.CancelAutoInc()
                  Stop('An Error Has Occured')
                  Post(Event:CloseWindow)
              End ! If Access:STOMJFAU.TryInsert() = Level:Benign
          End ! If Access:STOMJFAU.PrimeRecord() = Level:Benign
      End ! If Access:STOMJFAU.TryFetch(stj:Field_Key) = Level:Benign
  
      Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
      maf:Manufacturer = stm:Manufacturer
      maf:ScreenOrder = 0
      Set(maf:ScreenOrderKey,maf:ScreenOrderKey)
      Loop
          If Access:MANFAULT.Next()
              Break
          End ! If Access:MANFAULT.Next()
          If maf:Manufacturer <> stm:Manufacturer
              Break
          End ! If maf:Manufacturer <> stm:Manufacturer
          If maf:ScreenOrder = 0
              Cycle
          End ! If maf:ScreenOrder <> 0
          Case maf:Field_Number
          Of 1
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode1
          Of 2
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode2
          Of 3
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode3
          Of 4
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode4
          Of 5
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode5
          Of 6
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode6
          Of 7
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode7
          Of 8
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode8
          Of 9
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode9
          Of 10
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode10
          Of 11
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode11
          Of 12
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode12
          Of 13
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode13
          Of 14
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode14
          Of 15
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode15
          Of 16
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode16
          Of 17
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode17
          Of 18
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode18
          Of 19
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode19
          Of 20
              tmp:JobFaultCode[maf:ScreenOrder] = stj:FaultCode20
          End ! Case maf:Field_Number
      End ! Loop
  
  Do BuildNumbersQueue
  Do FaultCode
  ! Save Window Name
   AddToLog('Window','Open','StockFaultCodesWindow')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StockFaultCodesWindow')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:FaultCode:1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:1, Accepted)
      x# = 1
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:1, Accepted)
    OF ?Button:FaultCodeMultiple:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:2, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 2
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:2, Accepted)
    OF ?Button:FaultCode:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:2, Accepted)
      x# = 2
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:2, Accepted)
    OF ?Button:FaultCode:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:3, Accepted)
      x# = 3
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:3, Accepted)
    OF ?Button:FaultCodeMultiple:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:3, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 3
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:3, Accepted)
    OF ?Button:FaultCodeMultiple:1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:1, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 1
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:1, Accepted)
    OF ?Button:FaultCode:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:4, Accepted)
      x# = 4
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:4, Accepted)
    OF ?Button:FaultCode:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:5, Accepted)
      x# = 5
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:5, Accepted)
    OF ?Button:FaultCodeMultiple:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:4, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 4
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:4, Accepted)
    OF ?Button:FaultCodeMultiple:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:6, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 6
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:6, Accepted)
    OF ?Button:FaultCode:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:6, Accepted)
      x# = 6
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:6, Accepted)
    OF ?Button:FaultCodeMultiple:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:8, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 8
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:8, Accepted)
    OF ?Button:FaultCode:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:8, Accepted)
      x# = 8
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:8, Accepted)
    OF ?Button:FaultCodeMultiple:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:7, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 7
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:7, Accepted)
    OF ?Button:FaultCode:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:7, Accepted)
      x# = 7
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:7, Accepted)
    OF ?Button:FaultCodeMultiple:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:5, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 5
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:5, Accepted)
    OF ?Button:FaultCode:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:9, Accepted)
      x# = 9
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:9, Accepted)
    OF ?Button:FaultCode:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:11, Accepted)
      x# = 11
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:11, Accepted)
    OF ?Button:FaultCodeMultiple:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:11, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 11
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:11, Accepted)
    OF ?Button:FaultCode:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:10, Accepted)
      x# = 10
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:10, Accepted)
    OF ?Button:FaultCodeMultiple:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:10, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 10
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:10, Accepted)
    OF ?Button:FaultCodeMultiple:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:9, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 9
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:9, Accepted)
    OF ?Button:FaultCodeMultiple:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:12, Accepted)
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 12
      If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
          StockPartMultipleFaultCodes(map:Field_Number)
      End ! If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCodeMultiple:12, Accepted)
    OF ?Button:FaultCode:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:12, Accepted)
      x# = 12
      tmp:PartFaultCode[x#] = Local.LookupFaultCode(x#,tmp:PartFaultCode[x#])
      Do FaultCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FaultCode:12, Accepted)
    OF ?Button:Lookup:1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:1, Accepted)
      x# = 1
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:1, Accepted)
    OF ?Button:Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:3, Accepted)
      x# = 3
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:3, Accepted)
    OF ?Button:Lookup:13
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:13, Accepted)
      x# = 13
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:13, Accepted)
    OF ?Button:Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:2, Accepted)
      x# = 2
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:2, Accepted)
    OF ?Button:Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:12, Accepted)
      x# = 12
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:12, Accepted)
    OF ?Button:Lookup:14
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:14, Accepted)
      x# = 14
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:14, Accepted)
    OF ?Button:Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:4, Accepted)
      x# = 4
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:4, Accepted)
    OF ?Button:Lookup:16
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:16, Accepted)
      x# = 16
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:16, Accepted)
    OF ?Button:Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:5, Accepted)
      x# = 5
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:5, Accepted)
    OF ?Button:Lookup:15
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:15, Accepted)
      x# = 15
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:15, Accepted)
    OF ?Button:Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:7, Accepted)
      x# = 7
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:7, Accepted)
    OF ?Button:Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:6, Accepted)
      x# = 6
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:6, Accepted)
    OF ?Button:Lookup:17
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:17, Accepted)
      x# = 17
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:17, Accepted)
    OF ?Button:Lookup:18
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:18, Accepted)
      x# = 18
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:18, Accepted)
    OF ?Button:Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:8, Accepted)
      x# = 8
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:8, Accepted)
    OF ?Button:Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:9, Accepted)
      x# = 9
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:9, Accepted)
    OF ?Button:Lookup:19
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:19, Accepted)
      x# = 19
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:19, Accepted)
    OF ?Button:Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:10, Accepted)
      x# = 10
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:10, Accepted)
    OF ?Button:Lookup:20
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:20, Accepted)
      x# = 20
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:20, Accepted)
    OF ?Button:Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:11, Accepted)
      x# = 11
      tmp:JobFaultCode[x#] = local.LookupButtonCode(x#,tmp:JobFaultCode[x#])
      Do FaultCode
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Lookup:11, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020701'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020701'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020701'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! Write Values
      Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
      map:Manufacturer = stm:Manufacturer
      map:ScreenOrder = 0
      Set(map:ScreenOrderKey,map:ScreenOrderKey)
      Loop
          If Access:MANFAUPA.Next()
              Break
          End ! If Access:MANFAUPA.Next()
          If map:Manufacturer <> stm:Manufacturer
              Break
          End ! If map:Manufacturer <> stm:Manufacturer
          If map:ScreenOrder = 0
              Cycle
          End ! If map:ScreenOrder
  
          Case map:Field_Number
          Of 1
              stm:FaultCode1 = tmp:PartFaultCode[map:ScreenOrder]
          Of 2
              stm:FaultCode2 = tmp:PartFaultCode[map:ScreenOrder]
          Of 3
              stm:FaultCode3 = tmp:PartFaultCode[map:ScreenOrder]
          Of 4
              stm:FaultCode4 = tmp:PartFaultCode[map:ScreenOrder]
          Of 5
              stm:FaultCode5 = tmp:PartFaultCode[map:ScreenOrder]
          Of 6
              stm:FaultCode6 = tmp:PartFaultCode[map:ScreenOrder]
          Of 7
              stm:FaultCode7 = tmp:PartFaultCode[map:ScreenOrder]
          Of 8
              stm:FaultCode8 = tmp:PartFaultCode[map:ScreenOrder]
          Of 9
              stm:FaultCode9 = tmp:PartFaultCode[map:ScreenOrder]
          Of 10
              stm:FaultCode10 = tmp:PartFaultCode[map:ScreenOrder]
          Of 11
              stm:FaultCode11 = tmp:PartFaultCode[map:ScreenOrder]
          Of 12
              stm:FaultCode12 = tmp:PartFaultCode[map:ScreenOrder]
          End ! Case map:Field_Number
      End ! Loop
  
      Access:STOMJFAU.Clearkey(stj:FieldKey)
      stj:RefNumber = stm:RecordNumber
      If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
          Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
          maf:Manufacturer = stm:Manufacturer
          maf:ScreenOrder = 0
          Set(maf:ScreenOrderKey,maf:ScreenOrderKey)
          Loop
              If Access:MANFAULT.Next()
                  Break
              End ! If Access:MANFAULT.Next()
              If maf:Manufacturer <> stm:Manufacturer
                  Break
              End ! If maf:Manufacturer <> stm:Manufacturer
              If maf:ScreenOrder = 0
                  Cycle
              End ! If maf:ScreenOrder <> 0
              Case maf:Field_Number
              Of 1
                  stj:FaultCode1 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 2
                  stj:FaultCode2 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 3
                  stj:FaultCode3 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 4
                  stj:FaultCode4 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 5
                  stj:FaultCode5 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 6
                  stj:FaultCode6 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 7
                  stj:FaultCode7 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 8
                  stj:FaultCode8 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 9
                  stj:FaultCode9 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 10
                  stj:FaultCode10 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 11
                  stj:FaultCode11 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 12
                  stj:FaultCode12 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 13
                  stj:FaultCode13 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 14
                  stj:FaultCode14 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 15
                  stj:FaultCode15 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 16
                  stj:FaultCode16 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 17
                  stj:FaultCode17 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 18
                  stj:FaultCode18 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 19
                  stj:FaultCode19 = tmp:JobFaultCode[maf:ScreenOrder]
              Of 20
                  stj:FaultCode20 = tmp:JobFaultCode[maf:ScreenOrder]
              End ! Case maf:Field_Number
          End ! Loop
          Access:STOMJFAU.TryUpdate()
      End !If Access:STOMJFAU.TryFetch(stj:Field_Key) = Level:Benign
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
local.LookupFaultCode    Procedure(long f:FieldNumber, String f:FaultCode)
BlankQueue              Queue(DefLinkedFaultCodeQueue)
                        End
Code
    Access:MANFAUPA.Clearkey(map:ScreenOrderKey)
    map:Manufacturer = stm:Manufacturer
    map:ScreenOrder = f:FieldNumber
    If Access:MANFAUPA.TryFetch(map:ScreenOrderKey) = Level:Benign
        If map:MainFault
            Access:MANFAULT.Clearkey(maf:MainFaultKey)
            maf:Manufacturer = stm:Manufacturer
            maf:MainFault = 1
            If Access:MANFAULT.TryFetch(Maf:MainFaultKey) = Level:Benign
                Request# = GlobalRequest
                GlobalRequest = SelectRecord
                If map:UseRelatedJobCode > 0
                    BrowseJobFaultCodeLookup(stm:Manufacturer,maf:Field_Number,0,f:FieldNumber,0,BlankQueue)
                Else ! If map:UseRelatedJobCode > 0
                    BrowseJobFaultCodeLookup(stm:Manufacturer,maf:Field_NUmber,0,'',0,BlankQueue)
                End ! If map:UseRelatedJobCode > 0
                GlobalRequest = Request#
                If GlobalResponse = RequestCompleted
                    Return mfo:Field
                End ! If GlobalResponse = RequestCompleted
            End ! If Access:MANFAULT.TryFetch(Maf:MainFaultKey) = Level:Benign
        Else ! If map:MainFault
            Request# = GlobalRequest
            GlobalRequest = SelectRecord
            BrowsePartFaultCodeLookup(stm:Manufacturer,map:Field_Number,0,0,BlankQueue)
            GlobalRequest = Request#
            If GlobalResponse = RequestCompleted
                Return mfp:Field
            End ! If GlobalResponse = RequestCompleted
        End ! If map:MainFault
    End ! If Access:MANFAUPA.Tryetch(map:ScreenOrderKey) = Level:Benign

    Return f:FaultCode
local.LookupButtonCode        Procedure(Long f:FieldNumber,String f:Field)
local:JobType                 Byte(0)
BlankQueue              Queue(DefLinkedFaultCodeQueue)
                        End
Code
    Access:MANFAULT.Clearkey(maf:ScreenOrderKey)
    maf:Manufacturer    = stm:Manufacturer
    maf:ScreenOrder    = f:FieldNumber
    If Access:MANFAULT.Tryfetch(maf:ScreenOrderKey) = Level:Benign
        ! Found
        If maf:Field_Type = 'DATE'
            Return TINCALENDARStyle1(f:Field)
        Else ! If maf:Field_Type = 'DATE'
            SaveRequest# = GlobalRequest
            GlobalRequest = SelectRecord

            BrowseJobFaultCodeLookup(stm:Manufacturer,maf:Field_Number,0,'',0,BlankQueue)
            GlobalRequest = SaveRequest#

            If GlobalResponse = RequestCompleted
                Access:MANFAUEX.ClearKey(max:ModelNumberKey)
                max:MANFAULORecordNumber = mfo:RecordNumber
                max:ModelNumber          = stm:Model_Number
                If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Found
                    Case Missive('The Model Number on this job has been "Excluded" from using the selected Fault Code.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return f:Field
                Else !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign
                    !Error
                End !If Access:MANFAUEX.TryFetch(max:ModelNumberKey) = Level:Benign

                f:Field = mfo:Field
            End ! If GlobalResponse = RequestCompleted
            Return f:Field
        End ! If maf:Field_Type = 'DATE'
    Else ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
        ! Error
    End ! If Access:MANFAULT.Tryfetch(maf:Field_Number_Key) = Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
StockPartMultipleFaultCodes PROCEDURE (f:FieldNumber) !Generated from procedure template - Window

tmp:FieldNumber      LONG
BRW9::View:Browse    VIEW(STOMPFAU)
                       PROJECT(stu:Field)
                       PROJECT(stu:Description)
                       PROJECT(stu:RecordNumber)
                       PROJECT(stu:RefNumber)
                       PROJECT(stu:FieldNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
stu:Field              LIKE(stu:Field)                !List box control field - type derived from field
stu:Description        LIKE(stu:Description)          !List box control field - type derived from field
stu:RecordNumber       LIKE(stu:RecordNumber)         !Primary key field - type derived from field
stu:RefNumber          LIKE(stu:RefNumber)            !Browse key field - type derived from field
stu:FieldNumber        LIKE(stu:FieldNumber)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(MANFPALO)
                       PROJECT(mfp:Field)
                       PROJECT(mfp:Description)
                       PROJECT(mfp:RecordNumber)
                       PROJECT(mfp:Manufacturer)
                       PROJECT(mfp:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
mfp:Field              LIKE(mfp:Field)                !List box control field - type derived from field
mfp:Description        LIKE(mfp:Description)          !List box control field - type derived from field
mfp:RecordNumber       LIKE(mfp:RecordNumber)         !Primary key field - type derived from field
mfp:Manufacturer       LIKE(mfp:Manufacturer)         !Browse key field - type derived from field
mfp:Field_Number       LIKE(mfp:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::mfp:Field_Number    LIKE(mfp:Field_Number)
HK11::mfp:Manufacturer    LIKE(mfp:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK16::stu:FieldNumber     LIKE(stu:FieldNumber)
HK16::stu:RefNumber       LIKE(stu:RefNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(64,56,296,308),USE(?Sheet1),SPREAD
                         TAB('Assigned Fault Codes'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,78,220,282),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse)
                           BUTTON,AT(292,110),USE(?Button:AllocateFaultCode),TRN,FLAT,ICON('allfaup.jpg')
                           BUTTON,AT(292,332),USE(?Delete),TRN,FLAT,ICON('remfaup.jpg')
                         END
                       END
                       SHEET,AT(364,56,252,308),USE(?Sheet2),SPREAD
                         TAB('All Fault Codes'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(368,80,244,282),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('120L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Part Selected Fault Codes'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(544,366),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020702'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StockPartMultipleFaultCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFPALO.Open
  Access:STOMODEL.UseFile
  SELF.FilesOpened = True
  tmp:FieldNumber = f:FieldNumber
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:STOMPFAU,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:MANFPALO,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','StockPartMultipleFaultCodes')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,stu:FieldKey)
  BRW9.AddRange(stu:FieldNumber)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,stu:Field,1,BRW9)
  BRW9.AddField(stu:Field,BRW9.Q.stu:Field)
  BRW9.AddField(stu:Description,BRW9.Q.stu:Description)
  BRW9.AddField(stu:RecordNumber,BRW9.Q.stu:RecordNumber)
  BRW9.AddField(stu:RefNumber,BRW9.Q.stu:RefNumber)
  BRW9.AddField(stu:FieldNumber,BRW9.Q.stu:FieldNumber)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,mfp:Field_Key)
  BRW10.AddRange(mfp:Field_Number)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,mfp:Field,1,BRW10)
  BRW10.AddField(mfp:Field,BRW10.Q.mfp:Field)
  BRW10.AddField(mfp:Description,BRW10.Q.mfp:Description)
  BRW10.AddField(mfp:RecordNumber,BRW10.Q.mfp:RecordNumber)
  BRW10.AddField(mfp:Manufacturer,BRW10.Q.mfp:Manufacturer)
  BRW10.AddField(mfp:Field_Number,BRW10.Q.mfp:Field_Number)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFPALO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StockPartMultipleFaultCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateStockPartFaultCode
    ReturnValue = GlobalResponse
  END
  BRW10.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:AllocateFaultCode
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AllocateFaultCode, Accepted)
      Brw10.UpdateViewRecord()
      If Access:STOMPFAU.PrimeRecord() = Level:Benign
          stu:RefNumber = stm:RecordNumber
          stu:Manufacturer = stm:Manufacturer
          stu:FieldNumber = tmp:FieldNumber
          stu:Field = mfp:Field
          stu:Description = mfp:Description
          If Access:STOMPFAU.TryInsert() = Level:Benign
      
          Else ! If Access:STOMPFAU.TryInsert() = Level:Benign
              Access:STOMPFAU.CancelAutoInc()
          End ! If Access:STOMPFAU.TryInsert() = Level:Benign
      End ! If Access:STOMPFAU.PrimeRecord() = Level:Benign
      BRW10.ResetSort(1)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AllocateFaultCode, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020702'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020702'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020702'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?Button:AllocateFaultCode)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = stm:RecordNumber
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:FieldNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = stm:Manufacturer
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:FieldNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  Access:STOMPFAU.Clearkey(stu:FieldKey)
  stu:RefNumber = stm:RecordNumber
  stu:FieldNumber = tmp:FieldNumber
  stu:Field = mfp:Field
  If Access:STOMPFAU.TryFetch(stu:FieldKey) = Level:Benign
      Return Record:Filtered
  End ! If Access:STOMPFAU.TryFetch(stu:FieldKey) = Level:Benign
  BRW10::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ValidateRecord, (),BYTE)
  RETURN ReturnValue

UpdateStockPartFaultCode PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::stu:Record  LIKE(stu:RECORD),STATIC
QuickWindow          WINDOW('Update the STOMPFAU File'),AT(,,260,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateStockPartFaultCode'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,252,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?stu:RecordNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(128,20,40,10),USE(stu:RecordNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Link to STOMODEL RecordNumber'),AT(8,34),USE(?stu:RefNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(128,34,40,10),USE(stu:RefNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Manufacturer'),AT(8,48),USE(?stu:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(128,48,124,10),USE(stu:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                           PROMPT('Field Number'),AT(8,62),USE(?stu:FieldNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(128,62,40,10),USE(stu:FieldNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Field Number'),TIP('Field Number'),UPR
                           PROMPT('Field'),AT(8,76),USE(?stu:Field:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(128,76,124,10),USE(stu:Field),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Field'),TIP('Field'),UPR
                         END
                       END
                       BUTTON('OK'),AT(162,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(211,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(211,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateStockPartFaultCode')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?stu:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stu:Record,History::stu:Record)
  SELF.AddHistoryField(?stu:RecordNumber,1)
  SELF.AddHistoryField(?stu:RefNumber,2)
  SELF.AddHistoryField(?stu:Manufacturer,3)
  SELF.AddHistoryField(?stu:FieldNumber,4)
  SELF.AddHistoryField(?stu:Field,5)
  SELF.AddUpdateFile(Access:STOMPFAU)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STOMPFAU.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOMPFAU
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateStockPartFaultCode')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOMPFAU.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateStockPartFaultCode')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


OldPartNumberSearch PROCEDURE (f:Location,f:PartNumber) !Generated from procedure template - Browse

BRW8::View:Browse    VIEW(ADDSEARCH)
                       PROJECT(addtmp:AddressLine1)
                       PROJECT(addtmp:JobNumber)
                       PROJECT(addtmp:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
addtmp:AddressLine1    LIKE(addtmp:AddressLine1)      !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
addtmp:JobNumber       LIKE(addtmp:JobNumber)         !List box control field - type derived from field
addtmp:RecordNumber    LIKE(addtmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Old Part Number Search'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Part Number'),USE(?Tab1)
                           PROMPT('"Old" Part Number:'),AT(168,98),USE(?Prompt:OldPartNumber),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,111,124,10),USE(addtmp:AddressLine1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Address Line 1')
                           LIST,AT(168,124,344,202),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~"New" Part Number~@s30@120L(2)|M~Description~@s30@0L(2)|M~Job Number~@' &|
   's8@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Old Part Number Search'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(168,332),USE(?Button:AddStock),TRN,FLAT,ICON('addstokp.jpg')
                       BUTTON,AT(236,332),USE(?Button:DepleteStock),TRN,FLAT,ICON('depstokp.jpg')
                       BUTTON,AT(308,332),USE(?Button:OrderStock),TRN,FLAT,ICON('ordstop.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
tmp:TempFilePath    CString(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020724'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('OldPartNumberSearch')
  If GetTempPathA(255,tmp:TempFilePath)
      If Sub(tmp:TempFilePath,-1,1) <> '\'
          tmp:TempFilePath = Clip(tmp:TempFilePath) & '\'
      End !If Sub(tmp:TempFilePath,-1,1) <> '\'
  End
  glo:File_Name = Clip(tmp:TempFilePath) & 'oldpartsearch.tmp'
  Remove(glo:File_Name)
  
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ADDSEARCH.Open
  Relate:STOCK.Open
  Relate:USERS_ALIAS.Open
  Access:STOPARTS.UseFile
  SELF.FilesOpened = True
  Access:STOPARTS.Clearkey(spt:LocationOldPartKey)
  spt:Location = f:Location
  spt:OldPartNumber = f:PartNumber
  Set(spt:LocationOldPartKey,spt:LocationOldPartKey)
  Loop ! Begin Loop
      If Access:STOPARTS.Next()
          Break
      End ! If Access:STOPARTS.Next()
      If spt:Location <> f:Location
          Break
      End ! If spt:Location <> f:Location
      If spt:OldPartNumber <> f:PartNumber
          Break
      End ! If spt:OldPartNumber <> f:PartNumber
      Access:STOCK.ClearKey(sto:Ref_Number_Key)
      sto:Ref_Number = spt:STOCKRefNumber
      If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
          !Found
      Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          Cycle
      End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
  
      If Access:ADDSEARCH.PrimeRecord() = Level:Benign
          addtmp:AddressLine1 = sto:Part_Number
          addtmp:JobNumber = spt:STOCKRefNumber
          If Access:ADDSEARCH.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:ADDSEARCH.TryInsert() = Level:Benign
              Access:ADDSEARCH.CancelAutoInc()
          End ! If Access:ADDSEARCH.TryInsert() = Level:Benign
      End ! If Access.ADDSEARCH.PrimeRecord() = Level:Benign
  End ! Loop
  Access:STOCK.ClearKey(sto:Location_Key)
  sto:Part_Number = f:PartNumber
  sto:Location = f:Location
  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      !Found
      If Access:ADDSEARCH.PrimeRecord() = Level:Benign
          addtmp:AddressLine1 = sto:Part_Number
          addtmp:JobNumber = sto:Ref_Number
          If Access:ADDSEARCH.TryInsert() = Level:Benign
              !Insert
          Else ! If Access:ADDSEARCH.TryInsert() = Level:Benign
              Access:ADDSEARCH.CancelAutoInc()
          End ! If Access:ADDSEARCH.TryInsert() = Level:Benign
      End ! If Access.ADDSEARCH.PrimeRecord() = Level:Benign
  Else ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
      !Error
  End ! If Access:STOCK.TryFetch(sto:Part_Number_Key) = Level:Benign
  
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:ADDSEARCH,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','OldPartNumberSearch')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ?Prompt:OldPartNumber{prop:Text} = '"Old" Part Number" ' & f:PartNumber
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,addtmp:AddressLine1Key)
  BRW1.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?addtmp:AddressLine1,addtmp:AddressLine1,1,BRW1)
  BRW1.AddField(addtmp:AddressLine1,BRW1.Q.addtmp:AddressLine1)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(addtmp:JobNumber,BRW1.Q.addtmp:JobNumber)
  BRW1.AddField(addtmp:RecordNumber,BRW1.Q.addtmp:RecordNumber)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:ADDSEARCH.Close
    Relate:STOCK.Close
    Relate:USERS_ALIAS.Close
  Remove(glo:File_Name)
  END
  ! Save Window Name
   AddToLog('Window','Close','OldPartNumberSearch')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020724'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020724'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020724'&'0')
      ***
    OF ?Button:AddStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AddStock, Accepted)
      If SecurityCheck('STOCK - ADD STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          brw1.UpdateViewRecord()
      
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = addtmp:JobNumber
          If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              If sto:Sundry_Item = 'YES'
                  Case Missive('Cannot Add Stock.'&|
                    '<13,10>'&|
                    '<13,10>This part has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End ! If sto:Sundry_Item = 'YES'
          Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              Cycle
          End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
      
          Add_Stock(sto:Ref_Number)
      end!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AddStock, Accepted)
    OF ?Button:DepleteStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:DepleteStock, Accepted)
      If SecurityCheck('STOCK - DEPLETE STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Brw1.UpdateViewRecord()
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = addtmp:JobNumber
          If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              If sto:Sundry_Item = 'YES'
                  Case Missive('Cannot Deplete Stock.'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End ! If sto:Sundry_Item = 'YES'
          Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              Cycle
          End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
          Deplete_Stock(sto:Ref_Number,0)
      End!!if x" = false
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:DepleteStock, Accepted)
    OF ?Button:OrderStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:OrderStock, Accepted)
      If SecurityCheck('STOCK - ORDER STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          brw1.UpdateViewRecord()
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = addtmp:JobNumber
          If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              If sto:sundry_item = 'YES' and sto:ExchangeUnit <> 'YES'    ! L232 - allow exchange unit ordering
                  Case Missive('Cannot Order.'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              Else
                  If sto:Suspend
                      Case Missive('This part cannot be ordered. It has been suspended.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Cycle
                  End !If sto:Suspend
              End!If sto:sundry_item = 'YES'
          Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              Cycle
          End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
      
          glo:select1 = sto:Ref_Number
          glo:Select2 = ''
          If f:Location = 'MAIN STORE' And sto:Part_Number = 'EXCH'
              SaveRequest#      = GlobalRequest
              GlobalResponse    = RequestCancelled
              GlobalRequest     = SelectRecord
              Browse_Model_Numbers
              If Globalresponse = RequestCompleted
                  glo:Select2 = Clip(mod:Manufacturer) & ' ' & Clip(mod:Model_Number)
              Else
                  Error# = 1
              End
              GlobalRequest     = SaveRequest#
          End !If Location_Temp = 'MAIN STORE'
          If Error# = 0
              Stock_Order
          End !If Error# = 0
          glo:select1 = ''
          glo:Select2 = ''
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:OrderStock, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())
  Access:STOCK.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = addtmp:JobNumber
  If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
      !Error
      Clear(Sto:Record)
  End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

BrowseSuspendedStock PROCEDURE (fLocation)            !Generated from procedure template - Window

CurrentTab           STRING(80)
locLocation          STRING(30)
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Sale_Cost)
                       PROJECT(sto:Retail_Cost)
                       PROJECT(sto:AccessoryCost)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Location)
                       PROJECT(sto:Suspend)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Retail_Cost        LIKE(sto:Retail_Cost)          !List box control field - type derived from field
sto:AccessoryCost      LIKE(sto:AccessoryCost)        !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::sto:Location        LIKE(sto:Location)
HK12::sto:Suspend         LIKE(sto:Suspend)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse The Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('View Suspended Parts'),AT(168,68),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:000000'),AT(468,68),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,66,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       SHEET,AT(164,80,352,252),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,100,124,10),USE(sto:Part_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(168,302),USE(?buttonRefreshSuspendedStock),TRN,FLAT,HIDE,ICON('refsusp.jpg')
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,100,124,10),USE(sto:Description),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       LIST,AT(168,114,344,184),USE(?Browse:1),IMM,HVSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Part Number~@s30@80L(2)|M~Description~@s30@80L(2)|M~Supplier~@s30@60D(1' &|
   '0)|M~Purchase Cost~C(0)@n14.2@60D(18)|M~Sale Cost~C(0)@n14.2@60D(14)|M~Retail Co' &|
   'st~C(0)@n14.2@60D(12)|M~Accessory Cost~C(0)@n14.2@'),FROM(Queue:Browse:1)
                       PANEL,AT(164,334,352,26),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,334),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(164,334),USE(?buttonAddStock),TRN,FLAT,HIDE,ICON('addstokp.jpg')
                       BUTTON,AT(372,302),USE(?buttonStockHistory),TRN,FLAT,ICON('stohistp.jpg')
                       BUTTON,AT(444,302),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020743'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseSuspendedStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  SELF.FilesOpened = True
  locLocation = fLocation
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      If (locLocation = MainStoreLocation())
          ?buttonAddStock{prop:Hide} = 0
          ?buttonRefreshSuspendedSTock{prop:Hide} = 0
      End
  ! Save Window Name
   AddToLog('Window','Open','BrowseSuspendedStock')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sto:LocDescSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sto:LocPartSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  BRW1.AddField(sto:AccessoryCost,BRW1.Q.sto:AccessoryCost)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Suspend,BRW1.Q.sto:Suspend)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseSuspendedStock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSTOCK
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020743'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020743'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020743'&'0')
      ***
    OF ?buttonRefreshSuspendedStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonRefreshSuspendedStock, Accepted)
      If (SecurityCheck('SUSPEND PARTS'))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      Beep(Beep:SystemExclamation)  ;  Yield()
      Case Missive('This will ensure that any parts suspended at Main Store will be suspended in all other locations (if they have no stock).','ServiceBase',|
                     'mexclam.jpg','\&Cancel|/&Continue')
      Of 2 ! &Continue Button
          count# = 0
          Access:STOCK.Clearkey(sto:LocPartSuspendKey)
          sto:Location = fLocation
          sto:Suspend = 1
          sto:Part_Number = ''
          Set(sto:LocPartSuspendKey,sto:LocPartSuspendKey)
          Loop Until Access:STOCK.Next()
              if (sto:Location <> fLocation)
                  Break
              end
              if (sto:Suspend <> 1)
                  Break
              end
              count# += 1
          End
      
          Prog.ProgressSetup(count#)
          Access:STOCK.Clearkey(sto:LocPartSuspendKey)
          sto:Location = fLocation
          sto:Suspend = 1
          sto:Part_Number = ''
          Set(sto:LocPartSuspendKey,sto:LocPartSuspendKey)
          Loop Until Access:STOCK.Next()
              if (sto:Location <> fLocation)
                  Break
              end
              if (sto:Suspend <> 1)
                  Break
              end
              if (Prog.InsideLoop())
                  Break
              end
      
              Access:LOCATION.Clearkey(loc:RecordNumberKey)
              loc:RecordNumber = 0
              Set(loc:RecordNumberKey,loc:RecordNumberKey)
              Loop Until Access:LOCATION.Next()
                  if (loc:Location = MainStoreLocation())
                      cycle
                  end
                  if (loc:Active = 0)
                      Cycle
                  end
      
                  Access:STOCK_ALIAS.Clearkey(sto_ali:Location_Key)
                  sto_ali:Location = loc:Location
                  sto_ali:Part_Number = sto:Part_Number
                  If (Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign)
                      if (sto_ali:Quantity_Stock > 0)
                          Cycle
                      end
                      if (sto_ali:Suspend)
                          Cycle
                      end
                      If (StockAliasInUse(0))
                          Beep(Beep:SystemHand)  ;  Yield()
                          Case Missive('Part: ' & Clip(sto_ali:Part_Number) & ', Location: ' & Clip(sto_ali:Location) & ' was not updated. Item is in use.','ServiceBase',|
                                         'mstop.jpg','/&OK')
                          Of 1 ! &OK Button
                          End!Case Message
                          Cycle
                      End
                      sto_ali:Suspend = 1
                      If (Access:STOCK_ALIAS.TryUpdate() = Level:Benign)
                          If (AddToStockHistory(sto_ali:Ref_Number, |
                                                  'ADD', |
                                                  '', |
                                                  0, |
                                                  0, |
                                                  0, |
                                                  sto_ali:Purchase_Cost, |
                                                  sto_ali:Sale_Cost, |
                                                  sto_ali:Retail_Cost, |
                                                  'PART SUSPENDED', |
                                                  'AUTOMATED ROUTINE: MAIN STORE PART SUSPENDED'))
                          End
                      End
                  End
              End
      
      
          End
          Prog.ProgressFinish()
          Beep(Beep:SystemAsterisk)  ;  Yield()
          Case Missive('Finished.','ServiceBase',|
                         'midea.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
      Of 1 ! &Cancel Button
      End!Case Message
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonRefreshSuspendedStock, Accepted)
    OF ?buttonAddStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonAddStock, Accepted)
      If SecurityCheck('STOCK - ADD STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          error# = 0
      
          stockRefNumber# = brw1.q.sto:Ref_Number
      
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = stockRefNumber#
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Add Stock.'&|
                    '<13,10>'&|
                    '<13,10>This part has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Add_Stock(brw1.q.sto:ref_number)
      
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = stockRefNumber#
              If (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                  If (sto:Quantity_Stock > 0 And sto:Suspend)
                      ! If stock has been added. Unsuspend other parts.
                      sto:Suspend = 0
                      If (Access:STOCK.TryUpdate() = Level:Benign)
                          If (AddToStockHistory(sto:Ref_Number, |
                                                  'ADD', |
                                                  '', |
                                                  0, |
                                                  0, |
                                                  0, |
                                                  sto:Purchase_Cost, |
                                                  sto:Sale_Cost, |
                                                  sto:Retail_Cost, |
                                                  'PART UN-SUSPENDED', |
                                                  'STOCK ADDED'))
                          End
      
                          Prog.ProgressSetup(Records(LOCATION))
      
                          Access:LOCATION.Clearkey(loc:RecordNumberKey)
                          loc:RecordNumber = 0
                          Set(loc:RecordNumberKey,loc:RecordNumberKey)
                          Loop Until Access:LOCATION.Next()
                              if (loc:Location = MainStoreLocation())
                                  cycle
                              end
                              if (Prog.InsideLoop())
                                  Break
                              end !if (Prog.InsideLoop())
                              if (loc:Active = 0)
                                  Cycle
                              end
      
                              Access:STOCK_ALIAS.Clearkey(sto_ali:Location_Key)
                              sto_ali:Location = loc:Location
                              sto_ali:Part_Number = sto:Part_Number
                              If (Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign)
                                  if (sto_ali:Suspend = 1)
                                      If (StockAliasInUse(0))
                                          Beep(Beep:SystemHand)  ;  Yield()
                                          Case Missive('Part: ' & Clip(sto_ali:Part_Number) & ', Location: ' & Clip(sto_ali:Location) & ' was not updated. Item is in use.','ServiceBase',|
                                                         'mstop.jpg','/&OK')
                                          Of 1 ! &OK Button
                                          End!Case Message
                                          Cycle
                                      End
                                      sto_ali:Suspend = 0
                                      If (Access:STOCK_ALIAS.TryUpdate() = Level:Benign)
                                          If (AddToStockHistory(sto_ali:Ref_Number, |
                                                                  'ADD', |
                                                                  '', |
                                                                  0, |
                                                                  0, |
                                                                  0, |
                                                                  sto_ali:Purchase_Cost, |
                                                                  sto_ali:Sale_Cost, |
                                                                  sto_ali:Retail_Cost, |
                                                                  'PART UN-SUSPENDED', |
                                                                  'MAIN STORE STOCK ADDED'))
                                          End
                                      End
                                  End
                              End
                          End
      
                          Prog.ProgressFinish()
      
                      End
                  End !If (sto:Quantity_STock > 0 And sto:Suspend)
              End
          End!If error# = 0
      end!if x" = false
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonAddStock, Accepted)
    OF ?buttonStockHistory
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonStockHistory, Accepted)
      If SecurityCheck('STOCK - STOCK HISTORY')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          glo:select1 = brw1.q.STO:Ref_Number
          Browse_Stock_History
          glo:select1 = ''
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonStockHistory, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = locLocation
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
      if (sto:Suspend = 0)
          Return Record:Filtered
      end
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
