

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBB01002.INC'),ONCE        !Local module procedure declarations
                     END


BrowseCurrencies PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(CURRENCY)
                       PROJECT(cur:CurrencyCode)
                       PROJECT(cur:Description)
                       PROJECT(cur:DailyRate)
                       PROJECT(cur:DivideMultiply)
                       PROJECT(cur:LastUpdateDate)
                       PROJECT(cur:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
cur:CurrencyCode       LIKE(cur:CurrencyCode)         !List box control field - type derived from field
cur:CurrencyCode_NormalFG LONG                        !Normal forground color
cur:CurrencyCode_NormalBG LONG                        !Normal background color
cur:CurrencyCode_SelectedFG LONG                      !Selected forground color
cur:CurrencyCode_SelectedBG LONG                      !Selected background color
cur:Description        LIKE(cur:Description)          !List box control field - type derived from field
cur:Description_NormalFG LONG                         !Normal forground color
cur:Description_NormalBG LONG                         !Normal background color
cur:Description_SelectedFG LONG                       !Selected forground color
cur:Description_SelectedBG LONG                       !Selected background color
cur:DailyRate          LIKE(cur:DailyRate)            !List box control field - type derived from field
cur:DailyRate_NormalFG LONG                           !Normal forground color
cur:DailyRate_NormalBG LONG                           !Normal background color
cur:DailyRate_SelectedFG LONG                         !Selected forground color
cur:DailyRate_SelectedBG LONG                         !Selected background color
cur:DivideMultiply     LIKE(cur:DivideMultiply)       !List box control field - type derived from field
cur:DivideMultiply_NormalFG LONG                      !Normal forground color
cur:DivideMultiply_NormalBG LONG                      !Normal background color
cur:DivideMultiply_SelectedFG LONG                    !Selected forground color
cur:DivideMultiply_SelectedBG LONG                    !Selected background color
cur:LastUpdateDate     LIKE(cur:LastUpdateDate)       !List box control field - type derived from field
cur:LastUpdateDate_NormalFG LONG                      !Normal forground color
cur:LastUpdateDate_NormalBG LONG                      !Normal background color
cur:LastUpdateDate_SelectedFG LONG                    !Selected forground color
cur:LastUpdateDate_SelectedBG LONG                    !Selected background color
cur:RecordNumber       LIKE(cur:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the CURRENCY File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Currency Exchange Rate File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,100,344,196),USE(?Browse:1),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('57L(2)|M*~Currency Code~@s30@102L(2)|M*~Description~@s30@49R(2)|M*~Daily Rate~@n' &|
   '-14.4@59C|M*~Divide / Multiply~@s1@79R(2)|M*~Last Updated~C(0)@d6@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD
                         TAB('By Currency Code'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,87,124,10),USE(cur:CurrencyCode),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),UPR
                           PANEL,AT(396,86,11,10),USE(?Panel5),FILL(COLOR:Red)
                           PROMPT('- Rate Not Updated Today'),AT(412,86),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(168,298),USE(?Insert:3),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(236,298),USE(?Change:3),TRN,FLAT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(304,298),USE(?Delete:3),TRN,FLAT,ICON('deletep.jpg')
                           BUTTON,AT(444,298),USE(?Select),TRN,FLAT,ICON('selectp.jpg')
                           BUTTON,AT(444,298),USE(?UpdateCurrency),TRN,FLAT,ICON('updatep.jpg')
                         END
                       END
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020630'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseCurrencies')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CURRENCY.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:CURRENCY,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If ThisWindow.Request = SelectRecord
      ?UpdateCurrency{PROP:Hide} = True
  End ! ThisWindow.Request = SelectRecord
  ! Save Window Name
   AddToLog('Window','Open','BrowseCurrencies')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,cur:CurrencyCodeKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?cur:CurrencyCode,cur:CurrencyCode,1,BRW1)
  BRW1.AddField(cur:CurrencyCode,BRW1.Q.cur:CurrencyCode)
  BRW1.AddField(cur:Description,BRW1.Q.cur:Description)
  BRW1.AddField(cur:DailyRate,BRW1.Q.cur:DailyRate)
  BRW1.AddField(cur:DivideMultiply,BRW1.Q.cur:DivideMultiply)
  BRW1.AddField(cur:LastUpdateDate,BRW1.Q.cur:LastUpdateDate)
  BRW1.AddField(cur:RecordNumber,BRW1.Q.cur:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CURRENCY.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseCurrencies')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = True
  
  case request
      of insertrecord
          If SecurityCheck('CURRENCY - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
              Access:CURRENCY.CancelAutoInc()
          end!If SecurityCheck('CURRENCY - INSERT')
      of changerecord
          If SecurityCheck('CURRENCY - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end!If SecurityCheck('CURRENCY - CHANGE')
      of deleterecord
          If SecurityCheck('CURRENCY - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end!If SecurityCheck('CURRENCY - DELETE')
  end !case request
  
  If do_update# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateCurrencies
    ReturnValue = GlobalResponse
  END
  End!If do_update# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020630'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020630'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020630'&'0')
      ***
    OF ?UpdateCurrency
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateCurrency, Accepted)
      If SecurityCheck('CURRENCY - UPDATE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else ! SecurityCheck('CURRENCY - UPDATE')
          glo:Select1 = 'UPDATE CURRENCY'
          GlobalRequest = ChangeRecord
          UpdateCurrencies
          glo:Select1 = ''
          Brw1.ResetSort(1)
      End ! SecurityCheck('CURRENCY - UPDATE')
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateCurrency, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.cur:CurrencyCode_NormalFG = -1
  SELF.Q.cur:CurrencyCode_NormalBG = -1
  SELF.Q.cur:CurrencyCode_SelectedFG = -1
  SELF.Q.cur:CurrencyCode_SelectedBG = -1
  SELF.Q.cur:Description_NormalFG = -1
  SELF.Q.cur:Description_NormalBG = -1
  SELF.Q.cur:Description_SelectedFG = -1
  SELF.Q.cur:Description_SelectedBG = -1
  SELF.Q.cur:DailyRate_NormalFG = -1
  SELF.Q.cur:DailyRate_NormalBG = -1
  SELF.Q.cur:DailyRate_SelectedFG = -1
  SELF.Q.cur:DailyRate_SelectedBG = -1
  SELF.Q.cur:DivideMultiply_NormalFG = -1
  SELF.Q.cur:DivideMultiply_NormalBG = -1
  SELF.Q.cur:DivideMultiply_SelectedFG = -1
  SELF.Q.cur:DivideMultiply_SelectedBG = -1
  SELF.Q.cur:LastUpdateDate_NormalFG = -1
  SELF.Q.cur:LastUpdateDate_NormalBG = -1
  SELF.Q.cur:LastUpdateDate_SelectedFG = -1
  SELF.Q.cur:LastUpdateDate_SelectedBG = -1
   
   
   IF (cur:LastUpdateDate < Today())
     SELF.Q.cur:CurrencyCode_NormalFG = 255
     SELF.Q.cur:CurrencyCode_NormalBG = 16777215
     SELF.Q.cur:CurrencyCode_SelectedFG = 16777215
     SELF.Q.cur:CurrencyCode_SelectedBG = 255
   ELSE
     SELF.Q.cur:CurrencyCode_NormalFG = 0
     SELF.Q.cur:CurrencyCode_NormalBG = 16777215
     SELF.Q.cur:CurrencyCode_SelectedFG = 16777215
     SELF.Q.cur:CurrencyCode_SelectedBG = 10119804
   END
   IF (cur:LastUpdateDate < Today())
     SELF.Q.cur:Description_NormalFG = 255
     SELF.Q.cur:Description_NormalBG = 16777215
     SELF.Q.cur:Description_SelectedFG = 16777215
     SELF.Q.cur:Description_SelectedBG = 255
   ELSE
     SELF.Q.cur:Description_NormalFG = 0
     SELF.Q.cur:Description_NormalBG = 16777215
     SELF.Q.cur:Description_SelectedFG = 16777215
     SELF.Q.cur:Description_SelectedBG = 10119804
   END
   IF (cur:LastUpdateDate < Today())
     SELF.Q.cur:DailyRate_NormalFG = 255
     SELF.Q.cur:DailyRate_NormalBG = 16777215
     SELF.Q.cur:DailyRate_SelectedFG = 16777215
     SELF.Q.cur:DailyRate_SelectedBG = 255
   ELSE
     SELF.Q.cur:DailyRate_NormalFG = 0
     SELF.Q.cur:DailyRate_NormalBG = 16777215
     SELF.Q.cur:DailyRate_SelectedFG = 16777215
     SELF.Q.cur:DailyRate_SelectedBG = 10119804
   END
   IF (cur:LastUpdateDate < Today())
     SELF.Q.cur:DivideMultiply_NormalFG = 255
     SELF.Q.cur:DivideMultiply_NormalBG = 16777215
     SELF.Q.cur:DivideMultiply_SelectedFG = 16777215
     SELF.Q.cur:DivideMultiply_SelectedBG = 255
   ELSE
     SELF.Q.cur:DivideMultiply_NormalFG = 0
     SELF.Q.cur:DivideMultiply_NormalBG = 16777215
     SELF.Q.cur:DivideMultiply_SelectedFG = 16777215
     SELF.Q.cur:DivideMultiply_SelectedBG = 10119804
   END
   IF (cur:LastUpdateDate < Today())
     SELF.Q.cur:LastUpdateDate_NormalFG = 255
     SELF.Q.cur:LastUpdateDate_NormalBG = 16777215
     SELF.Q.cur:LastUpdateDate_SelectedFG = 16777215
     SELF.Q.cur:LastUpdateDate_SelectedBG = 255
   ELSE
     SELF.Q.cur:LastUpdateDate_NormalFG = 0
     SELF.Q.cur:LastUpdateDate_NormalBG = 16777215
     SELF.Q.cur:LastUpdateDate_SelectedFG = 16777215
     SELF.Q.cur:LastUpdateDate_SelectedBG = 10119804
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Suppliers PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:Account_Number)
                       PROJECT(sup:Telephone_Number)
                       PROJECT(sup:Address_Line1)
                       PROJECT(sup:Postcode)
                       PROJECT(sup:Fax_Number)
                       PROJECT(sup:Contact_Name)
                       PROJECT(sup:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:Account_Number     LIKE(sup:Account_Number)       !List box control field - type derived from field
sup:Telephone_Number   LIKE(sup:Telephone_Number)     !List box control field - type derived from field
sup:Address_Line1      LIKE(sup:Address_Line1)        !List box control field - type derived from field
sup:Postcode           LIKE(sup:Postcode)             !List box control field - type derived from field
sup:Fax_Number         LIKE(sup:Fax_Number)           !List box control field - type derived from field
sup:Contact_Name       LIKE(sup:Contact_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Supplier File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,112,276,182),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('130L(2)|M~Company Name~@s30@70L(2)|M~Account Number~@s15@73L(2)|M~Telephone Numb' &|
   'er~@s15@80L(2)|M~Address~@s30@44L(2)|M~Postcode~@s10@64L(2)|M~Fax Number~@s15@80' &|
   'L(2)|M~Contact Name~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,220),USE(?Rename_Button),TRN,FLAT,ICON('rensupp.jpg')
                       BUTTON,AT(448,298),USE(?Select:2),TRN,FLAT,ICON('selectp.jpg')
                       BUTTON,AT(168,298),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,298),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(308,298),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Company Name'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(sup:Company_Name),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),UPR
                         END
                         TAB('By Account Number'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(168,98,64,10),USE(sup:Account_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,164),USE(?ExchangeRatesButton),TRN,FLAT,ICON('exchratp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Supplier File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020081'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Suppliers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:SUPPLIER.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUPPLIER,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Suppliers')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sup:AccountNumberKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sup:Account_Number,sup:Account_Number,1,BRW1)
  BRW1.AddSortOrder(,sup:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sup:Company_Name,sup:Company_Name,1,BRW1)
  BRW1.AddField(sup:Company_Name,BRW1.Q.sup:Company_Name)
  BRW1.AddField(sup:Account_Number,BRW1.Q.sup:Account_Number)
  BRW1.AddField(sup:Telephone_Number,BRW1.Q.sup:Telephone_Number)
  BRW1.AddField(sup:Address_Line1,BRW1.Q.sup:Address_Line1)
  BRW1.AddField(sup:Postcode,BRW1.Q.sup:Postcode)
  BRW1.AddField(sup:Fax_Number,BRW1.Q.sup:Fax_Number)
  BRW1.AddField(sup:Contact_Name,BRW1.Q.sup:Contact_Name)
  BRW1.AddField(sup:RecordNumber,BRW1.Q.sup:RecordNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Company Name'
    ?Tab:3{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='130L(2)|M~Company Name~@s30@#1#70L(2)|M~Account Number~@s15@#2#73L(2)|M~Telephone Number~@s15@#3#80L(2)|M~Address~@s30@#4#44L(2)|M~Postcode~@s10@#5#64L(2)|M~Fax Number~@s15@#6#80L(2)|M~Contact Name~@s60@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:SUPPLIER.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Suppliers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            If SecurityCheck('SUPPLIERS - INSERT')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
                do_update# = false
            end
        of changerecord
            If SecurityCheck('SUPPLIERS - CHANGE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
                do_update# = false
            end
        of deleterecord
            If SecurityCheck('SUPPLIERS - DELETE')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSUPPLIER
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Rename_Button
      ThisWindow.Update
      Rename_Supplier_Window
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rename_Button, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rename_Button, Accepted)
    OF ?ExchangeRatesButton
      ThisWindow.Update
      BrowseCurrencies
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020081'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020081'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020081'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='130L(2)|M~Company Name~@s30@#1#70L(2)|M~Account Number~@s15@#2#73L(2)|M~Telephone Number~@s15@#3#80L(2)|M~Address~@s30@#4#44L(2)|M~Postcode~@s10@#5#64L(2)|M~Fax Number~@s15@#6#80L(2)|M~Contact Name~@s60@#7#'
          ?Tab:2{PROP:TEXT} = 'By Company Name'
        OF 2
          ?Browse:1{PROP:FORMAT} ='70L(2)|M~Account Number~@s15@#2#130L(2)|M~Company Name~@s30@#1#73L(2)|M~Telephone Number~@s15@#3#80L(2)|M~Address~@s30@#4#44L(2)|M~Postcode~@s10@#5#64L(2)|M~Fax Number~@s15@#6#80L(2)|M~Contact Name~@s60@#7#'
          ?Tab:3{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sup:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Company_Name, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Company_Name, Selected)
    OF ?sup:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Account_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Account_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Rename_Supplier_Window PROCEDURE                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Old_Supplier_Name STRING(30)
tmp:New_Supplier_Name STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Old_Supplier_Name
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:New_Supplier_Name
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Rename Supplier'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Rename Supplier'),USE(?Tab1)
                           STRING('Note : This routine will take a long time to run.'),AT(256,190),USE(?String1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Old Name'),AT(248,199),USE(?tmp:Old_Supplier_Name:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(308,199,124,10),USE(tmp:Old_Supplier_Name),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(8,124),FROM(Queue:FileDropCombo)
                           PROMPT('New Name'),AT(248,214),USE(?tmp:New_Supplier_Name:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(308,214,124,10),USE(tmp:New_Supplier_Name),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(8,124),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rename Supplier'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Rename_Suppliers Routine
    ! From the spec :-
    ! Rename supplier record
    ! Update stock template with the supplier info attached
    ! Parts on jobs with the supplier info attached
    ! Any orders (outstanding/received) with the supplier info attached
    !sto:Supplier_Accessory_Key
    data
RS:RecordsToProcess long
RS:RecordCount      long(1)
RS:Error            byte

Stock_Q             queue
StockID                 long
                    end
    code

    !---------------------------------------------------------------------------
    Access:STOCK.ClearKey(sto:Supplier_Only_Key)
    sto:Supplier = tmp:Old_Supplier_Name
    set(sto:Supplier_Only_Key,sto:Supplier_Only_Key)
    loop until Access:STOCK.Next()                          ! Calculate records to process for stock table
        if sto:Supplier <> tmp:Old_Supplier_Name then break.
        Stock_Q.StockID = sto:Ref_Number
        add(Stock_Q)
        RS:RecordsToProcess += 1
        Prog.ProgressText('Stock Records Found : ' & RS:RecordsToProcess)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    Prog.ResetProgress(RS:RecordsToProcess)

    loop
        get(Stock_Q,RS:RecordCount)
        if error() then break.
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = Stock_Q.StockID
        if not Access:STOCK.Fetch(sto:Ref_Number_Key)
            sto:Supplier = tmp:New_Supplier_Name
            Access:STOCK.TryUpdate()
        end
        RS:RecordCount += 1
        Prog.ProgressText('Processing Stock Records: ' & RS:RecordCount)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.
    !---------------------------------------------------------------------------

    ! Chargeable Parts
    free(Stock_Q)
    RS:RecordsToProcess = 0
    RS:RecordCount = 1

    Prog.ResetProgress(records(par:Supplier_Key))

    Access:PARTS.ClearKey(par:Supplier_Key)
    par:Supplier = tmp:Old_Supplier_Name
    set(par:Supplier_Key,par:Supplier_Key)
    loop until Access:PARTS.Next()                          ! Calculate records to process for chargeable parts table
        if par:Supplier <> tmp:Old_Supplier_Name then break.
        Stock_Q.StockID = par:Record_Number
        add(Stock_Q)
        RS:RecordsToProcess += 1
        Prog.ProgressText('Parts(C) Records Found : ' & RS:RecordsToProcess)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    Prog.ResetProgress(RS:RecordsToProcess)

    loop
        get(Stock_Q,RS:RecordCount)
        if error() then break.
        Access:PARTS.ClearKey(par:recordnumberkey)
        par:Record_Number = Stock_Q.StockID
        if not Access:PARTS.Fetch(par:recordnumberkey)
            par:Supplier = tmp:New_Supplier_Name
            Access:PARTS.TryUpdate()
        end
        RS:RecordCount += 1
        Prog.ProgressText('Processing Parts(C) Records: ' & RS:RecordCount)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    ! Warranty Parts
    free(Stock_Q)
    RS:RecordsToProcess = 0
    RS:RecordCount = 1

    Prog.ResetProgress(records(wpr:Supplier_Key))

    Access:WARPARTS.ClearKey(wpr:Supplier_Key)
    wpr:Supplier = tmp:Old_Supplier_Name
    set(wpr:Supplier_Key,wpr:Supplier_Key)
    loop until Access:WARPARTS.Next()                          ! Calculate records to process for warranty parts table
        if wpr:Supplier <> tmp:Old_Supplier_Name then break.
        Stock_Q.StockID = wpr:Record_Number
        add(Stock_Q)
        RS:RecordsToProcess += 1
        Prog.ProgressText('Parts(W) Records Found : ' & RS:RecordsToProcess)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    Prog.ResetProgress(RS:RecordsToProcess)

    loop
        get(Stock_Q,RS:RecordCount)
        if error() then break.
        Access:WARPARTS.ClearKey(wpr:recordnumberkey)
        wpr:Record_Number = Stock_Q.StockID
        if not Access:WARPARTS.Fetch(wpr:recordnumberkey)
            wpr:Supplier = tmp:New_Supplier_Name
            Access:WARPARTS.TryUpdate()
        end
        RS:RecordCount += 1
        Prog.ProgressText('Processing Parts(W) Records: ' & RS:RecordCount)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    ! Estimate Parts
    free(Stock_Q)
    RS:RecordsToProcess = 0
    RS:RecordCount = 1

    Prog.ResetProgress(records(epr:Supplier_Key))

    Access:ESTPARTS.ClearKey(epr:Supplier_Key)
    epr:Supplier = tmp:Old_Supplier_Name
    set(epr:Supplier_Key,epr:Supplier_Key)
    loop until Access:ESTPARTS.Next()                          ! Calculate records to process for estimate parts table
        if epr:Supplier <> tmp:Old_Supplier_Name then break.
        Stock_Q.StockID = epr:Record_Number
        add(Stock_Q)
        RS:RecordsToProcess += 1
        Prog.ProgressText('Parts(E) Records Found : ' & RS:RecordsToProcess)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    Prog.ResetProgress(RS:RecordsToProcess)

    loop
        get(Stock_Q,RS:RecordCount)
        if error() then break.
        Access:ESTPARTS.ClearKey(epr:record_number_key)
        epr:Record_Number = Stock_Q.StockID
        if not Access:ESTPARTS.Fetch(epr:record_number_key)
            epr:Supplier = tmp:New_Supplier_Name
            Access:ESTPARTS.TryUpdate()
        end
        RS:RecordCount += 1
        Prog.ProgressText('Processing Parts(E) Records: ' & RS:RecordCount)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.
    !---------------------------------------------------------------------------
    ! Orders

    free(Stock_Q)
    RS:RecordsToProcess = 0
    RS:RecordCount = 1

    Prog.ResetProgress(records(ord:Supplier_Key))

    Access:ORDERS.ClearKey(ord:Supplier_Key)
    ord:Supplier = tmp:Old_Supplier_Name
    set(ord:Supplier_Key,ord:Supplier_Key)
    loop until Access:ORDERS.Next()                          ! Calculate records to process for orders table
        if ord:Supplier <> tmp:Old_Supplier_Name then break.
        Stock_Q.StockID = ord:Order_Number
        add(Stock_Q)
        RS:RecordsToProcess += 1
        Prog.ProgressText('Order Records Found : ' & RS:RecordsToProcess)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.

    Prog.ResetProgress(RS:RecordsToProcess)

    loop
        get(Stock_Q,RS:RecordCount)
        if error() then break.
        Access:ORDERS.ClearKey(ord:Order_Number_Key)
        ord:Order_Number = Stock_Q.StockID
        if not Access:ORDERS.Fetch(ord:Order_Number_Key)
            ord:Supplier = tmp:New_Supplier_Name
            Access:ORDERS.TryUpdate()
        end
        RS:RecordCount += 1
        Prog.ProgressText('Processing Order Records: ' & RS:RecordCount)
        if Prog.InsideLoop()
            RS:Error = 1
            break
        end
    end

    if RS:Error then exit.
    !---------------------------------------------------------------------------
    Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
    sup:Company_Name = tmp:Old_Supplier_Name
    if not Access:SUPPLIER.Fetch(sup:Company_Name_Key)
        Relate:SUPPLIER.Delete(0)                           ! Delete original supplier
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020105'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Rename_Supplier_Window')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ESTPARTS.Open
  Access:STOCK.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:ORDERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Rename_Supplier_Window')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB5.Init(tmp:Old_Supplier_Name,?tmp:Old_Supplier_Name,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(sup:Company_Name_Key)
  FDCB5.AddField(sup:Company_Name,FDCB5.Q.sup:Company_Name)
  FDCB5.AddField(sup:RecordNumber,FDCB5.Q.sup:RecordNumber)
  FDCB5.AddUpdateField(sup:Company_Name,tmp:Old_Supplier_Name)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB6.Init(tmp:New_Supplier_Name,?tmp:New_Supplier_Name,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  FDCB6.AddUpdateField(sup:Company_Name,tmp:New_Supplier_Name)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESTPARTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Rename_Supplier_Window')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      ! Rename supplier
      
      if tmp:Old_Supplier_Name = ''
          select(?tmp:Old_Supplier_Name)
          cycle
      else
          Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
          sup:Company_Name = tmp:Old_Supplier_Name
          if Access:SUPPLIER.Fetch(sup:Company_Name_Key)
              select(?tmp:Old_Supplier_Name)
              cycle
          end
      end
      
      if tmp:New_Supplier_Name = ''
          select(?tmp:New_Supplier_Name)
          cycle
      else
          Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
          sup:Company_Name = tmp:New_Supplier_Name
          if Access:SUPPLIER.Fetch(sup:Company_Name_Key)
              select(?tmp:New_Supplier_Name)
              cycle
          end
      end
      
      Prog.ProgressSetup(records(STOCK))
      
      do Rename_Suppliers
      
      Prog.ProgressFinish()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
UpdateSUPPLIER PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::sup:Record  LIKE(sup:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
QuickWindow          WINDOW('Update the SUPPLIER File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       SHEET,AT(64,104,552,258),USE(?CurrentTab),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Company Name'),AT(112,120),USE(?SUP:Company_Name:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(188,120,124,10),USE(sup:Company_Name),FONT('Tahoma',8,,FONT:bold),COLOR(080FFFFH),REQ,UPR
                           PROMPT('Account Number'),AT(112,136),USE(?SUP:Account_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(188,136,64,10),USE(sup:Account_Number),FONT('Tahoma',8,,FONT:bold),COLOR(080FFFFH),REQ,UPR
                           PROMPT('Postcode'),AT(112,156),USE(?SUP:Postcode:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s10),AT(188,156,64,10),USE(sup:Postcode),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(256,150),USE(?Clear_Address),SKIP,TRN,FLAT,LEFT,ICON('clearp.jpg')
                           PROMPT('Address'),AT(112,180),USE(?SUP:Address_Line1:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(188,180,124,10),USE(sup:Address_Line1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(188,192,124,10),USE(sup:Address_Line2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(188,204,124,10),USE(sup:Address_Line3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           CHECK('Use Foreign Currency'),AT(360,200),USE(sup:UseForeignCurrency),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Tax Exempt'),AT(456,200),USE(sup:EVO_TaxExempt),VALUE('1','0')
                           PROMPT('Telephone Number'),AT(112,220),USE(?SUP:Telephone_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(188,220,64,10),USE(sup:Telephone_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Currency Type'),AT(360,216),USE(?sup:CurrencyCode:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,216,124,10),USE(sup:CurrencyCode),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),UPR
                           BUTTON,AT(584,212),USE(?LookupCurrencyType),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Fax Number'),AT(112,236),USE(?SUP:Fax_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(188,236,64,10),USE(sup:Fax_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Oracle Supplier Number'),AT(360,230),USE(?sup:MOPSupplierNumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,230,124,10),USE(sup:MOPSupplierNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('MOP Supplier Number'),TIP('MOP Supplier Number'),UPR
                           PROMPT('Email Address'),AT(112,252),USE(?sup:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(188,252,124,10),USE(sup:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Oracle Item ID'),AT(360,244),USE(?sup:MOPItemID:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(456,244,124,10),USE(sup:MOPItemID),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('MOP Item ID'),TIP('MOP Item ID'),UPR
                           PROMPT('Contact Name'),AT(112,268),USE(?SUP:Contact_Name:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(188,268,124,10),USE(sup:Contact_Name),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           CHECK('Exclude from EVO'),AT(456,280),USE(sup:EVO_Excluded),TRN,VALUE('1','0')
                           CHECK('Use Free Stock'),AT(455,260),USE(sup:UseFreeStock),VALUE('1','0')
                           PROMPT('Notes'),AT(112,284),USE(?SUP:Notes:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(188,284,124,52),USE(sup:Notes),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('EVO GL Account No:'),AT(361,292),USE(?sup:EVO_GL_Acc_No:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s10),AT(456,292,124,10),USE(sup:EVO_GL_Acc_No),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('EVO Vendor Number:'),AT(361,304),USE(?sup:EVO_Vendor_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s10),AT(456,304,124,10),USE(sup:EVO_Vendor_Number),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('EVO Profit Centre:'),AT(361,316),USE(?sup:EVO_Profit_Centre:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(456,316,124,10),USE(sup:EVO_Profit_Centre),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(548,334),USE(?Replicate_Manufacturer),TRN,FLAT,LEFT,ICON('repmanp.jpg')
                           GROUP('Stock Order'),AT(356,116,200,76),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('History Usage'),AT(360,128),USE(?sup:HistoryUsage:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n2),AT(456,128,64,10),USE(sup:HistoryUsage),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('History Usage'),TIP('History Usage'),UPR,STEP(1)
                           PROMPT('Multiply By Factor Of'),AT(360,144),USE(?sup:Factor:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(456,144,64,10),USE(sup:Factor),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Multiply By Factor Of'),TIP('Multiply By Factor Of'),UPR
                           PROMPT('Normal Supply Period'),AT(360,160),USE(?SUP:Normal_Supply_Period:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n3b),AT(456,160,64,10),USE(sup:Normal_Supply_Period),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Days'),AT(524,160),USE(?Prompt10),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(456,176,64,10),USE(sup:Minimum_Order_Value),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Minimum Order Value'),AT(360,176),USE(?SUP:Minimum_Order_Value:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Days'),AT(524,128),USE(?Prompt10:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('%'),AT(524,144),USE(?Percent),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN,HIDE
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Supplier'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:sup:CurrencyCode                Like(sup:CurrencyCode)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Supplier'
  OF ChangeRecord
    ActionMessage = 'Changing A Supplier'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020084'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSUPPLIER')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sup:Record,History::sup:Record)
  SELF.AddHistoryField(?sup:Company_Name,2)
  SELF.AddHistoryField(?sup:Account_Number,11)
  SELF.AddHistoryField(?sup:Postcode,3)
  SELF.AddHistoryField(?sup:Address_Line1,4)
  SELF.AddHistoryField(?sup:Address_Line2,5)
  SELF.AddHistoryField(?sup:Address_Line3,6)
  SELF.AddHistoryField(?sup:UseForeignCurrency,18)
  SELF.AddHistoryField(?sup:EVO_TaxExempt,25)
  SELF.AddHistoryField(?sup:Telephone_Number,7)
  SELF.AddHistoryField(?sup:CurrencyCode,19)
  SELF.AddHistoryField(?sup:Fax_Number,8)
  SELF.AddHistoryField(?sup:MOPSupplierNumber,20)
  SELF.AddHistoryField(?sup:EmailAddress,9)
  SELF.AddHistoryField(?sup:MOPItemID,21)
  SELF.AddHistoryField(?sup:Contact_Name,10)
  SELF.AddHistoryField(?sup:EVO_Excluded,27)
  SELF.AddHistoryField(?sup:UseFreeStock,22)
  SELF.AddHistoryField(?sup:Notes,17)
  SELF.AddHistoryField(?sup:EVO_GL_Acc_No,23)
  SELF.AddHistoryField(?sup:EVO_Vendor_Number,24)
  SELF.AddHistoryField(?sup:EVO_Profit_Centre,26)
  SELF.AddHistoryField(?sup:HistoryUsage,15)
  SELF.AddHistoryField(?sup:Factor,16)
  SELF.AddHistoryField(?sup:Normal_Supply_Period,13)
  SELF.AddHistoryField(?sup:Minimum_Order_Value,14)
  SELF.AddUpdateFile(Access:SUPPLIER)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CURRENCY.Open
  Relate:DEFAULTS.Open
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUPPLIER
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Only force Oracle fields for NEW records (DBH: 31-08-2005)
  If ThisWindow.Request = InsertRecord
      ?sup:MOPSupplierNumber{prop:Req} = True
      ?sup:MOPItemID{prop:Req} = True
  End ! If ThisWindow.Request = InsertRecord
  ! End   - Only force Oracle fields for NEW records (DBH: 31-08-2005)
  ! Save Window Name
   AddToLog('Window','Open','UpdateSUPPLIER')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?sup:CurrencyCode{Prop:Tip} AND ~?LookupCurrencyType{Prop:Tip}
     ?LookupCurrencyType{Prop:Tip} = 'Select ' & ?sup:CurrencyCode{Prop:Tip}
  END
  IF ?sup:CurrencyCode{Prop:Msg} AND ~?LookupCurrencyType{Prop:Msg}
     ?LookupCurrencyType{Prop:Msg} = 'Select ' & ?sup:CurrencyCode{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?sup:UseForeignCurrency{Prop:Checked} = True
    UNHIDE(?sup:CurrencyCode:Prompt)
    UNHIDE(?sup:CurrencyCode)
    UNHIDE(?LookupCurrencyType)
  END
  IF ?sup:UseForeignCurrency{Prop:Checked} = False
    HIDE(?sup:CurrencyCode:Prompt)
    HIDE(?sup:CurrencyCode)
    HIDE(?LookupCurrencyType)
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CURRENCY.Close
    Relate:DEFAULTS.Close
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSUPPLIER')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseCurrencies
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sup:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          Postcode_Routine (sup:postcode,sup:address_line1,sup:address_line2,sup:address_line3)
          Select(?sup:address_line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      Case Missive('Are you sure you want to clear the address?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              SUP:Postcode=''
              SUP:Address_Line1=''
              SUP:Address_Line2=''
              SUP:Address_Line3=''
      
              Display()
              Select(?sup:postcode)
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?sup:UseForeignCurrency
      IF ?sup:UseForeignCurrency{Prop:Checked} = True
        UNHIDE(?sup:CurrencyCode:Prompt)
        UNHIDE(?sup:CurrencyCode)
        UNHIDE(?LookupCurrencyType)
      END
      IF ?sup:UseForeignCurrency{Prop:Checked} = False
        HIDE(?sup:CurrencyCode:Prompt)
        HIDE(?sup:CurrencyCode)
        HIDE(?LookupCurrencyType)
      END
      ThisWindow.Reset
    OF ?sup:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Telephone_Number, Accepted)
      
          temp_string = Clip(left(sup:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sup:Telephone_Number    = temp_string
          Display(?sup:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Telephone_Number, Accepted)
    OF ?sup:CurrencyCode
      IF sup:CurrencyCode OR ?sup:CurrencyCode{Prop:Req}
        cur:CurrencyCode = sup:CurrencyCode
        !Save Lookup Field Incase Of error
        look:sup:CurrencyCode        = sup:CurrencyCode
        IF Access:CURRENCY.TryFetch(cur:CurrencyCodeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sup:CurrencyCode = cur:CurrencyCode
          ELSE
            !Restore Lookup On Error
            sup:CurrencyCode = look:sup:CurrencyCode
            SELECT(?sup:CurrencyCode)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupCurrencyType
      ThisWindow.Update
      cur:CurrencyCode = sup:CurrencyCode
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sup:CurrencyCode = cur:CurrencyCode
          Select(?+1)
      ELSE
          Select(?sup:CurrencyCode)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sup:CurrencyCode)
    OF ?sup:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Fax_Number, Accepted)
      
          temp_string = Clip(left(sup:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sup:Fax_Number    = temp_string
          Display(?sup:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Fax_Number, Accepted)
    OF ?Replicate_Manufacturer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Manufacturer, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      select_manufacturer
      if globalresponse = requestcompleted
          Case Missive('Are you sure you want to replicate the Manufacturer''s Details to this supplier?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  sup:company_name    = man:manufacturer
                  access:manufact.clearkey(man:manufacturer_key)
                  man:manufacturer = sup:company_name
                  if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                      sup:company_name     = man:manufacturer
                      SUP:Postcode        = man:Postcode
                      SUP:Address_Line1   = man:Address_Line1
                      SUP:Address_Line2   = man:Address_Line2
                      SUP:Address_Line3   = man:Address_Line3
                      sup:telephone_number = man:telephone_number
                      sup:fax_number       = man:fax_number
                      sup:contact_name     = man:contact_name1
                      sup:account_number   = man:account_number
                  end !if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                  Display()
              Of 1 ! No Button
          End ! Case Missive
      end
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Manufacturer, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020084'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020084'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020084'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_Stock_Model_Numbers PROCEDURE                  !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::stm:Record  LIKE(stm:RECORD),STATIC
QuickWindow          WINDOW('Update the STOMODEL File'),AT(,,411,332),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('Update_Stock_Model_Numbers'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,212,216),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Model Number'),AT(8,20),USE(?STM:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(stm:Model_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 1'),AT(8,36),USE(?stm:FaultCode1:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(stm:FaultCode1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 1'),TIP('Fault Code 1'),UPR
                           PROMPT('Fault Code 2'),AT(8,52),USE(?stm:FaultCode2:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(stm:FaultCode2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 2'),TIP('Fault Code 2'),UPR
                           PROMPT('Fault Code 3'),AT(8,68),USE(?stm:FaultCode3:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(stm:FaultCode3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 3'),TIP('Fault Code 3'),UPR
                           PROMPT('Fault Code 4'),AT(8,84),USE(?stm:FaultCode4:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(stm:FaultCode4),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 4'),TIP('Fault Code 4'),UPR
                           PROMPT('Fault Code 5'),AT(8,100),USE(?stm:FaultCode5:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,100,124,10),USE(stm:FaultCode5),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 5'),TIP('Fault Code 5'),UPR
                           PROMPT('Fault Code 6'),AT(8,116),USE(?stm:FaultCode6:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,116,124,10),USE(stm:FaultCode6),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 6'),TIP('Fault Code 6'),UPR
                           PROMPT('Fault Code 7'),AT(8,132),USE(?stm:FaultCode7:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(stm:FaultCode7),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 7'),TIP('Fault Code 7'),UPR
                           PROMPT('Fault Code 8'),AT(8,148),USE(?stm:FaultCode8:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(stm:FaultCode8),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 8'),TIP('Fault Code 8'),UPR
                           PROMPT('Fault Code 9'),AT(8,164),USE(?stm:FaultCode9:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,164,124,10),USE(stm:FaultCode9),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 9'),TIP('Fault Code 9'),UPR
                           PROMPT('Fault Code 10'),AT(11,180),USE(?stm:FaultCode10:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(63,180,124,10),USE(stm:FaultCode10),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 10'),TIP('Fault Code 10'),UPR
                           PROMPT('Fault Code 11'),AT(15,196),USE(?stm:FaultCode11:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(67,196,124,10),USE(stm:FaultCode11),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 11'),TIP('Fault Code 11'),UPR
                         END
                       END
                       BUTTON('&OK'),AT(80,284,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(160,284,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Stock_Model_Numbers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STM:Model_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stm:Record,History::stm:Record)
  SELF.AddHistoryField(?stm:Model_Number,3)
  SELF.AddHistoryField(?stm:FaultCode1,8)
  SELF.AddHistoryField(?stm:FaultCode2,9)
  SELF.AddHistoryField(?stm:FaultCode3,10)
  SELF.AddHistoryField(?stm:FaultCode4,11)
  SELF.AddHistoryField(?stm:FaultCode5,12)
  SELF.AddHistoryField(?stm:FaultCode6,13)
  SELF.AddHistoryField(?stm:FaultCode7,14)
  SELF.AddHistoryField(?stm:FaultCode8,15)
  SELF.AddHistoryField(?stm:FaultCode9,16)
  SELF.AddHistoryField(?stm:FaultCode10,17)
  SELF.AddHistoryField(?stm:FaultCode11,18)
  SELF.AddUpdateFile(Access:STOMODEL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Stock_Model_Numbers')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Stock_Model_Numbers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    stm:Manufacturer = sto:manufacturer
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Pick_Manufactuer_Model PROCEDURE                      !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::3:TAGDISPSTATUS    BYTE(0)
DASBRW::3:QUEUE           QUEUE
Model_Number_Pointer          LIKE(glo:Model_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
ExcludedMan          STRING(300)
select_temp          STRING(1)
LocalRequest         LONG
FilesOpened          BYTE
Model_Number_Tick    STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Unit_Type)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
Model_Number_Tick      LIKE(Model_Number_Tick)        !List box control field - type derived from local data
Model_Number_Tick_Icon LONG                           !Entry's icon ID
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Unit_Type          LIKE(mod:Unit_Type)            !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Model Number File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,112,272,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),COLUMN,FORMAT('11L(2)|I@s1@125L(2)|M~Model Number~@s30@120L(2)|M~Unit Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,246),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(448,272),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(448,298),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       BUTTON('&Rev tags'),AT(240,224,32,13),USE(?DASREVTAG),HIDE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Model Number File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON('sho&W tags'),AT(240,196,32,13),USE(?DASSHOWTAG),HIDE
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Model Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Unit Type'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,98,124,10),USE(mod:Unit_Type),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,160),USE(?Select),TRN,FLAT,LEFT,ICON('selectp.jpg'),STD(STD:Close)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 and GLO:Select2 <> ''
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 and GLO:Select2 = ''
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 and GLO:Select2 <> ''
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 and GLO:Select2 = ''
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::3:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
   GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  IF ERRORCODE()
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    Model_Number_Tick = '*'
  ELSE
    DELETE(GLO:Q_ModelNumber)
    Model_Number_Tick = ''
  END
    Queue:Browse:1.Model_Number_Tick = Model_Number_Tick
  IF (model_number_tick = '*')
    Queue:Browse:1.Model_Number_Tick_Icon = 1
  ELSE
    Queue:Browse:1.Model_Number_Tick_Icon = 0
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::3:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Q_ModelNumber)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::3:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_ModelNumber)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::3:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::3:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_ModelNumber)
    GET(GLO:Q_ModelNumber,QR#)
    DASBRW::3:QUEUE = GLO:Q_ModelNumber
    ADD(DASBRW::3:QUEUE)
  END
  FREE(GLO:Q_ModelNumber)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::3:QUEUE.Model_Number_Pointer = mod:Model_Number
     GET(DASBRW::3:QUEUE,DASBRW::3:QUEUE.Model_Number_Pointer)
    IF ERRORCODE()
       GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
       ADD(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::3:DASSHOWTAG Routine
   CASE DASBRW::3:TAGDISPSTATUS
   OF 0
      DASBRW::3:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::3:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::3:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020095'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Pick_Manufactuer_Model')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  !after opening files - before the window comes in
  !crete a limiting list of manufacturers to exclude
  Access:Manufact.clearkey(man:RecordNumberKey)
  set(man:RecordNumberKey,man:RecordNumberKey)
  Loop
      if access:Manufact.next() then break.
      !if man:Notes[1:8] = 'INACTIVE' then
      !TB13214 - change to using field for inactive
      if man:Inactive = 1 then
          ExcludedMan = clip(ExcludedMan)&'|'&clip(man:Manufacturer)&'|'      !pipes are there to separate manufact names - TB13168 JC 22/10/13
      END !if marked as inactive
  END !loop through manufact
  
  !TB12488 Disable Manufacturers  J 22/06/2012
  
  !this will mean I can use
  !if instring(clip(man:Manufacturer),excludedMan,1,1) then ... to exclude models and manufacts
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Pick_Manufactuer_Model')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Unit_Type_Key)
  BRW1.AddRange(mod:Manufacturer,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?mod:Unit_Type,mod:Unit_Type,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Unit_Type_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?mod:Unit_Type,mod:Manufacturer,1,BRW1)
  BRW1.AddSortOrder(,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BIND('Model_Number_Tick',Model_Number_Tick)
  BIND('GLO:Select2',GLO:Select2)
  ?Browse:1{PROP:IconList,1} = '~bluetick.ico'
  BRW1.AddField(Model_Number_Tick,BRW1.Q.Model_Number_Tick)
  BRW1.AddField(mod:Model_Number,BRW1.Q.mod:Model_Number)
  BRW1.AddField(mod:Unit_Type,BRW1.Q.mod:Unit_Type)
  BRW1.AddField(mod:Manufacturer,BRW1.Q.mod:Manufacturer)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_ModelNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Model Number'
    ?Tab2{PROP:TEXT} = 'By Unit Type'
    ?Browse:1{PROP:FORMAT} ='11L(2)|I@s1@#1#125L(2)|M~Model Number~@s30@#3#120L(2)|M~Unit Type~@s30@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Pick_Manufactuer_Model')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      DO DASBRW::3:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020095'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020095'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020095'&'0')
      ***
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If Keycode() = MouseLeft2
          Post(event:accepted,?Dastag)
      End!If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='11L(2)|I@s1@#1#125L(2)|M~Model Number~@s30@#3#120L(2)|M~Unit Type~@s30@#4#'
          ?Tab:2{PROP:TEXT} = 'By Model Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='11L(2)|I@s1@#1#120L(2)|M~Unit Type~@s30@#4#125L(2)|M~Model Number~@s30@#3#'
          ?Tab2{PROP:TEXT} = 'By Unit Type'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mod:Unit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Unit_Type, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Unit_Type, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
      DO DASBRW::3:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?DasTag)
          Of F6Key
              Post(Event:Accepted,?DasTagAll)
          Of F7Key
              Post(Event:Accepted,?DasUntagAll)
          Of F10Key
              Post(Event:Accepted,?Select)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 and GLO:Select2 <> ''
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 and GLO:Select2 = ''
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 and GLO:Select2 <> ''
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 and GLO:Select2 = ''
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    IF ERRORCODE()
      Model_Number_Tick = ''
    ELSE
      Model_Number_Tick = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (model_number_tick = '*')
    SELF.Q.Model_Number_Tick_Icon = 1
  ELSE
    SELF.Q.Model_Number_Tick_Icon = 0
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  if instring('|'&clip(mod:Manufacturer)&'|',excludedMan,1,1) then return(record:filtered).
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_ModelNumber.Model_Number_Pointer = mod:Model_Number
     GET(GLO:Q_ModelNumber,GLO:Q_ModelNumber.Model_Number_Pointer)
    EXECUTE DASBRW::3:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Replicate_Question PROCEDURE (f_type,f_pass)          !Generated from procedure template - Window

option_temp          STRING('X')
levels_temp          BYTE(0)
costs_temp           BYTE(0)
all_temp             BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('The Part''s Number, Description and Locations will now be replicated across all t' &|
   'he Seleceted Locations.'),AT(212,146,256,24),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('What other information do you wish to replicate?'),AT(212,180,256,16),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Minimum and Re-order Levels'),AT(280,214),USE(levels_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Cost Structure'),AT(280,230),USE(costs_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('All'),AT(280,246),USE(all_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Replicate Stock Item'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?Button2),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Button3),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020106'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Replicate_Question')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Replicate_Question')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?all_temp{Prop:Checked} = True
    levels_temp = 1
    costs_temp = 1
  END
  IF ?all_temp{Prop:Checked} = False
    costs_temp = 0
    levels_temp = 0
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Replicate_Question')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?all_temp
      IF ?all_temp{Prop:Checked} = True
        levels_temp = 1
        costs_temp = 1
      END
      IF ?all_temp{Prop:Checked} = False
        costs_temp = 0
        levels_temp = 0
      END
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020106'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020106'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020106'&'0')
      ***
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      f_pass = 0
      If levels_temp = 1 and costs_temp = 0
          f_pass = 1
      End!If costs_temp = 1 and level_temp = 0
      If costs_temp = 1 and levels_temp = 0
          f_pass = 2
      End!If costs_temp = 1 and levels_temp = 0
      If costs_temp = 1 and levels_temp = 1
          f_pass = 3
      End!If costs_temp = 1 and levels_temp = 1
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      f_pass = 'X'
      Post(event:Closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Pick_Other_Location PROCEDURE (f_type,f_ref_number)   !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_stm_id          USHORT,AUTO
save_stm_ali_id      USHORT,AUTO
sav:Purchase_Cost    REAL
sav:Sale_Cost        REAL
sav:Retail_Cost      REAL
sav:Minimum_Level    REAL
sav:Reorder_Level    REAL
sav:ShelfLocation    STRING(30)
sav:SecondLocation   STRING(30)
sav:AveragePurchaseCost REAL
sav:PurchaseMarkup   REAL
sav:PercentageMarkUp REAL
save_loc_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
save_sto_id          USHORT,AUTO
no_temp              STRING('NO')
tag_temp             STRING(1)
levels_temp          STRING('0')
costs_temp           BYTE(0)
all_temp             BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Main_Store)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Main_Store         LIKE(loc:Main_Store)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Location File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Replicate Main Store Stock'),AT(168,86),USE(?Title),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Tab 3'),USE(?Tab3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('General:'),AT(220,130),USE(?Prompt7),FONT(,,080FFFFH,FONT:bold)
                           PROMPT('This facility will replicate all the Main Store Stock Items into other locations'),AT(220,150,244,16),USE(?General1),FONT(,8,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Note: If the same part already exists in another location, it''s details will be ' &|
   'overwritten.'),AT(220,170,256,24),USE(?General2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Click ''Next'' To Continue...'),AT(293,230),USE(?Prompt4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('By Location'),USE(?Tab:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Tag the locations that you wish to replicate to, then click ''Next'''),AT(184,142,104,32),USE(?Prompt5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(312,123,200,133),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@80L(2)|M*~Location~@s30@'),FROM(Queue:Browse:1)
                           PROMPT('Select Locations:'),AT(184,126),USE(?Prompt8),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(316,171,50,13),USE(?DASREVTAG),HIDE
                           GROUP('Key'),AT(184,188,96,42),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(193,198,12,8),USE(?Panel2),FILL(COLOR:Green)
                             PROMPT('- Virtual Site'),AT(209,198),USE(?Prompt12),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(193,214,12,8),USE(?Panel2:2),FILL(COLOR:Gray)
                             PROMPT('- Inactive Site'),AT(209,214),USE(?Prompt12:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           BUTTON('sho&W tags'),AT(312,198,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(312,258),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(380,258),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,258),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                         TAB('Tab 4'),USE(?Tab4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Replication Details:'),AT(220,132),USE(?Prompt9),FONT(,,080FFFFH,FONT:bold)
                           PROMPT('The Stock Items General Details will be replicated. Do you wish to use the Main ' &|
   'Store''s Costs?'),AT(220,154,240,20),USE(?Prompt6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('(Note: Stock Quantity will NOT be replicated)'),AT(220,174),USE(?Prompt11),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Minimum Stock And Re-Order Levels'),AT(263,194),USE(levels_temp),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('Use Costs'),AT(263,214),USE(costs_temp),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK('All Details'),AT(263,236),USE(all_temp),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           PROMPT('Click ''Finish'' when you are ready to begin. (Warning! this may take a long time ' &|
   'to complete)'),AT(169,266),USE(?Prompt10),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Replicate Main Store Stock'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(384,332),USE(?Ok),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON,AT(232,332),USE(?VSNextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(168,332),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

Wizard2         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = loc:Location
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = loc:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = loc:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::6:QUEUE = GLO:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = loc:Location
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = loc:Location
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
ShelfLocation:Unallocated routine
    ! Set the shelf location to 'UNALLOCATED'
    sto:Shelf_Location  = 'UNALLOCATED'

    ! Create shelf location entry for this site location if it does not exist
    Access:LocShelf.ClearKey(los:Shelf_Location_Key)
    los:Site_Location = sto:Location
    los:Shelf_Location = sto:Shelf_Location             ! Unallocated
    if Access:LocShelf.Fetch(los:Shelf_Location_Key)
        los:Site_Location = sto:Location
        los:Shelf_Location  = sto:Shelf_Location
        Access:LOCSHELF.TryInsert()
    end

! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020075'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Pick_Other_Location')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Title
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMJFAU_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:STOMPFAU_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:LOCSHELF.UseFile
  Access:MANMARK.UseFile
  Access:STOMPFAU.UseFile
  Access:STOMJFAU.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCATION,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  costs_temp = 1
  
  if SecurityCheck('REPLICATE STOCK - USE COSTS')
      ?costs_temp{Prop:Disable} = true
  end
  ! Save Window Name
   AddToLog('Window','Open','Pick_Other_Location')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
    Wizard2.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Ok, |                           ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,loc:Main_Store_Key)
  BRW1.AddRange(loc:Main_Store,no_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,loc:Location,1,BRW1)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(loc:Location,BRW1.Q.loc:Location)
  BRW1.AddField(loc:RecordNumber,BRW1.Q.loc:RecordNumber)
  BRW1.AddField(loc:Main_Store,BRW1.Q.loc:Main_Store)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMJFAU_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:STOMPFAU_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Pick_Other_Location')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard2.Validate()
        DISABLE(Wizard2.NextControl())
     ELSE
        ENABLE(Wizard2.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?VSNextButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
      Case Choice(?Sheet1)
          Of 2
              If ~Records(glo:Queue)
                  Case Missive('You must select at least ONE Site Location.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End!If ~Records(glo:Queue)
      End!Case Choice(?CurrentTab)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VSNextButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020075'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020075'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020075'&'0')
      ***
    OF ?Ok
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
          If ~Records(glo:Queue)
              Case Missive('You must select at least one location.','ServiceBase 3g',|
                  'mstop.jpg','/OK')
              Of 1 ! OK Button
              End ! Case Missive
          Else!If ~Records(glo:Queue)
              Case Missive('Click ''OK'' To Begin.','ServiceBase 3g',|
                  'mexclam.jpg','\Cancel|/OK')
              Of 2 ! OK Button
                  stream(stock)
                  stream(stock_alias)
                  stream(stomodel)
                  stream(stomjfau)
                  stream(stomodel_alias)
                  stream(stomjfau_alias)
      
                  If f_type = 'ALL'
      
                      Prog.ProgressSetup(Records(STOCK))
      
                      save_sto_ali_id = access:stock_alias.savefile()                             !Loop through the stock for the main
                      access:stock_alias.clearkey(sto_ali:location_part_description_key)          !stores
                      sto_ali:location    = MainStoreLocation()
                      set(sto_ali:location_part_description_key,sto_ali:location_part_description_key)
                      loop
                          if access:stock_alias.next()
                              break
                          end !if
                          if sto_ali:location    <> MainStoreLocation()     |
                              then break.  ! end if
                          If (Prog.InsideLoop())
                              Beep(Beep:SystemHand)  ;  Yield()
                              Case Missive('You cannot cancel this process.','ServiceBase',|
                                  'mstop.jpg','/&OK')
                              Of 1 ! &OK Button
                              End!Case Message
                          End
                          Loop x# = 1 To Records(glo:Queue)                                    !Loop through the selected locations
                              Get(glo:Queue,x#)
                              access:stock.clearkey(sto:location_part_description_key)
                              sto:location    = glo:pointer                                    !Try and see if the part already exists
                              sto:part_number = sto_ali:part_number
                              sto:description = sto_ali:description
                              if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  sav:minimum_level = sto:minimum_level
                                  sav:reorder_level = sto:reorder_level
                                  sav:purchase_cost  = sto:purchase_cost
                                  sav:sale_cost      = sto:sale_cost
                                  sav:retail_cost    = sto:retail_cost
                                  sav:ShelfLocation   = sto:Shelf_Location
                                  sav:SecondLocation  = sto:Second_Location
                                  ref_number# = sto:ref_number
                                  quantity_stock# = sto:quantity_stock
                                  sto:record :=: sto_ali:record
                                  sto:location    = glo:pointer
                                  sto:part_number = sto_ali:part_number
                                  sto:description = sto_ali:description
                                  sto:ref_number  = ref_number#
                                  sto:quantity_stock = quantity_stock#
                                  sto:Shelf_Location  = sav:ShelfLocation
                                  sto:Second_Location  = sav:SecondLocation
      
                                  If sto_ali:Location = MainStoreLocation()
                                      If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                          If loc:VirtualSite
                                              sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                              sto:PurchaseMarkUp      = InWarrantyMarkup(sto:Manufacturer,loc:Location)
                                              sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp
                                              sto:Purchase_Cost       = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:PurchaseMarkup/100))
                                              sto:Sale_Cost           = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:Percentage_Mark_Up/100))
                                          End !If loc:VirtualSite
                                      End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                  End !If sto_ali:Location = MainStoreLocation()
      
                                  If costs_temp = 0
                                      sto:sale_Cost       = 0 !sav:sale_cost
                                      sto:retail_cost     = 0 !sav:retail_cost
                                      sto:purchase_cost   = 0 !sav:purchase_cost
                                      sto:AveragePurchaseCost = 0 !sav:AveragePurchaseCost
                                      sto:PurchaseMarkup  = 0 !sav:PurchaseMarkup
                                      sto:Percentage_Mark_Up = 0 !sav:PercentageMarkUp
                                  End!If costs_temp = 0
      
                                  If levels_temp = 0
                                      sto:minimum_level   = sav:minimum_level
                                      sto:reorder_level   = sav:reorder_level
                                  End!If levels_temp = 0
      
                                  Access:STOCK.Update()
      
                                  save_stm_id = access:stomodel.savefile()
                                  access:stomodel.clearkey(stm:mode_number_only_key)
                                  stm:ref_number   = sto:ref_number
                                  set(stm:mode_number_only_key,stm:mode_number_only_key)
                                  loop
                                      if access:stomodel.next()
                                          break
                                      end !if
                                      if stm:ref_number   <> sto:ref_number  |
                                          then break.  ! end if
                                      ! Inserting (DBH 31/07/2008) # 10289 - Remove job fault codes
                                      Access:STOMJFAU.ClearKey(stj:FieldKey)
                                      stj:RefNumber = stm:RecordNumber
                                      If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                                          !Found
                                          Delete(STOMJFAU)
                                      Else ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                                          !Error
                                      End ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                                      ! End (DBH 31/07/2008) #10289
                                      Delete(stomodel)
                                  end !loop
                                  access:stomodel.restorefile(save_stm_id)
      
                                  save_stm_ali_id = access:stomodel_alias.savefile()
                                  access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                  stm_ali:ref_number   = sto_ali:ref_number
                                  !stm_ali:manufacturer = sto_ali:manufacturer   ! #11687 Manufacturer may be blank. (Bryan: 08/09/2010)
                                  set(stm_ali:model_number_key,stm_ali:model_number_key)
                                  loop
                                      if access:stomodel_alias.next()
                                          break
                                      end !if
                                      if stm_ali:ref_number   <> sto_ali:ref_number
                                          Break
                                      End
                                      get(stomodel,0)
                                      if access:stomodel.primerecord() = Level:Benign
                                          RecordNumber$   = stm:RecordNumber
                                          stm:record  :=: stm_ali:record
                                          stm:RecordNumber    = RecordNumber$
                                          stm:ref_number  = sto:ref_number
                                          stm:location    = sto:location
                                          if access:stomodel.insert()
                                              access:stomodel.cancelautoinc()
                                          end
      
                                          ! Inserting (DBH 31/07/2008) # 10289 - Add related job fault codes
                                          Access:STOMJFAU_ALIAS.ClearKey(stj_ali:FieldKey)
                                          stj_ali:RefNumber = stm_ali:RecordNumber
                                          If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                              !Found
                                              If Access:STOMJFAU.PrimeRecord() = Level:Benign
                                                  Rec# = stj:RecordNumber
                                                  stj:Record :=: stj_ali:Record
                                                  stj:RecordNumber = Rec#
                                                  stj:RefNumber = stm:RecordNumber
                                                  If Access:STOMJFAU.TryInsert() = Level:Benign
                                                      !Insert
                                                  Else ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                      Access:STOMJFAU.CancelAutoInc()
                                                  End ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                              End ! If Access.STOMJFAU.PrimeRecord() = Level:Benign
                                          Else ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                              !Error
                                          End ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                          ! End (DBH 31/07/2008) #10289
      
                                          ! Remove all entries from "new" part (DBH: 31/07/2008)
                                          Access:STOMPFAU.Clearkey(stu:FieldKey)
                                          stu:RefNumber = stm:RecordNumber
                                          stu:FieldNumber = 0
                                          Set(stu:FieldKey,stu:FieldKey)
                                          Loop ! Begin Loop
                                              If Access:STOMPFAU.Next()
                                                  Break
                                              End ! If Access:STOMPFAU.Next()
                                              If stu:RefNumber <> stm:RecordNumber
                                                  Break
                                              End ! If stu:RefNumber <> stm:RecordNumber
                                              Delete(STOMPFAU)
                                          End ! Loop
      
                                          Access:STOMPFAU_ALIAS.Clearkey(stu_ali:FieldKey)
                                          stu_ali:RefNumber = stm_ali:RecordNumber
                                          stu_ali:FieldNumber = 0
                                          Set(stu_ali:FieldKey,stu_ali:FieldKey)
                                          Loop ! Begin Loop
                                              If Access:STOMPFAU_ALIAS.Next()
                                                  Break
                                              End ! If Access:STOMPFAU_ALIAS.Next()
                                              If stu_ali:RefNumber <> stm_ali:RecordNumber
                                                  Break
                                              End ! If stu_ali:RefNumber <> stm_ali:RecordNumber
      
                                              If Access:STOMPFAU.PrimeRecord() = Level:Benign
                                                  Rec# = stu:RecordNumber
                                                  stu:Record :=: stu_ali:Record
                                                  stu:RecordNumber = Rec#
                                                  stu:RefNumber = stm:RecordNumber
                                                  If Access:STOMPFAU.TryInsert() = Level:Benign
                                                      !Insert
                                                  Else ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                      Access:STOMPFAU.CancelAutoInc()
                                                  End ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                              End ! If Access.STOMPFAU.PrimeRecord() = Level:Benign
                                          End ! Loop
      
                                      end!if access:stomodel.primerecord() = Level:Benign
      
                                  end !loop
                                  access:stomodel_alias.restorefile(save_stm_ali_id)
      
      
                              Else!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  get(stock,0)                                                    !If part doesn't exist. Add the part
                                  if access:stock.primerecord() = Level:Benign                    !into the new location. Make a direct
                                      ref_number# = sto:ref_number                                !copy of the alias part, except for the
                                      sto:record :=: sto_ali:record                               !ref number and location.
                                      sto:ref_number  = ref_number#
                                      sto:location    = glo:pointer
                                      sto:quantity_stock  = 0
                                      do ShelfLocation:Unallocated
                                      sto:Second_Location = ''
      
                                      If sto_ali:Location = MainStoreLocation()
                                          If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                              If loc:VirtualSite
                                                  sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                                  sto:PurchaseMarkUp      = InWarrantyMarkup(sto:Manufacturer,loc:Location)
                                                  sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp
                                                  sto:Purchase_Cost       = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:PurchaseMarkup/100))
                                                  sto:Sale_Cost           = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:Percentage_Mark_Up/100))
                                              End !If loc:VirtualSite
                                          End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                      End !If sto_ali:Location = MainStoreLocation()
      
                                      If costs_temp = 0
                                          sto:sale_Cost       = 0
                                          sto:retail_cost     = 0
                                          sto:purchase_cost   = 0
                                          sto:AveragePurchaseCost = 0
                                          sto:PurchaseMarkup  = 0
                                          sto:Percentage_Mark_Up = 0
                                      End!If costs_temp = 0
      
                                      If levels_temp = 0
                                          sto:minimum_level = 0
                                          sto:reorder_level = 0
                                      End!If levels_temp = 0
                                      if access:stock.insert()
                                          access:stock.cancelautoinc()
                                      Else
                                          If sto_ali:manufacturer <> ''
                                              save_stm_ali_id = access:stomodel_alias.savefile()
                                              access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                              stm_ali:ref_number   = sto_ali:ref_number
                                              !stm_ali:manufacturer = sto_ali:manufacturer ! #11687 Manufacturer may be blank. (Bryan: 08/09/2010)
                                              set(stm_ali:model_number_key,stm_ali:model_number_key)
                                              loop
                                                  if access:stomodel_alias.next()
                                                      break
                                                  end !if
                                                  if stm_ali:ref_number   <> sto_ali:ref_number
                                                      Break
                                                  End
                                                  get(stomodel,0)
                                                  if access:stomodel.primerecord() = Level:Benign
                                                      RecordNumber$   = stm:RecordNumber
                                                      stm:record  :=: stm_ali:record
                                                      stm:RecordNumber    = RecordNumber$
                                                      stm:ref_number  = sto:ref_number
                                                      stm:location    = sto:location
                                                      if access:stomodel.insert()
                                                          access:stomodel.cancelautoinc()
                                                      end
                                                      ! Inserting (DBH 31/07/2008) # 10289 - Add related job fault codes
                                                      Access:STOMJFAU_ALIAS.ClearKey(stj_ali:FieldKey)
                                                      stj_ali:RefNumber = stm_ali:RecordNumber
                                                      If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                          !Found
                                                          If Access:STOMJFAU.PrimeRecord() = Level:Benign
                                                              Rec# = stj:RecordNumber
                                                              stj:Record :=: stj_ali:Record
                                                              stj:RecordNumber = Rec#
                                                              stj:RefNumber = stm:RecordNumber
                                                              If Access:STOMJFAU.TryInsert() = Level:Benign
                                                                  !Insert
                                                              Else ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                                  Access:STOMJFAU.CancelAutoInc()
                                                              End ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                          End ! If Access.STOMJFAU.PrimeRecord() = Level:Benign
                                                      Else ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                          !Error
                                                      End ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                      ! End (DBH 31/07/2008) #10289
      
                                                      ! Remove all entries from "new" part (DBH: 31/07/2008)
                                                      Access:STOMPFAU.Clearkey(stu:FieldKey)
                                                      stu:RefNumber = stm:RecordNumber
                                                      stu:FieldNumber = 0
                                                      Set(stu:FieldKey,stu:FieldKey)
                                                      Loop ! Begin Loop
                                                          If Access:STOMPFAU.Next()
                                                              Break
                                                          End ! If Access:STOMPFAU.Next()
                                                          If stu:RefNumber <> stm:RecordNumber
                                                              Break
                                                          End ! If stu:RefNumber <> stm:RecordNumber
                                                          Delete(STOMPFAU)
                                                      End ! Loop
      
                                                      Access:STOMPFAU_ALIAS.Clearkey(stu_ali:FieldKey)
                                                      stu_ali:RefNumber = stm_ali:RecordNumber
                                                      stu_ali:FieldNumber = 0
                                                      Set(stu_ali:FieldKey,stu_ali:FieldKey)
                                                      Loop ! Begin Loop
                                                          If Access:STOMPFAU_ALIAS.Next()
                                                              Break
                                                          End ! If Access:STOMPFAU_ALIAS.Next()
                                                          If stu_ali:RefNumber <> stm_ali:RecordNumber
                                                              Break
                                                          End ! If stu_ali:RefNumber <> stm_ali:RecordNumber
      
                                                          If Access:STOMPFAU.PrimeRecord() = Level:Benign
                                                              Rec# = stu:RecordNumber
                                                              stu:Record :=: stu_ali:Record
                                                              stu:RecordNumber = Rec#
                                                              stu:RefNumber = stm:RecordNumber
                                                              If Access:STOMPFAU.TryInsert() = Level:Benign
                                                                  !Insert
                                                              Else ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                                  Access:STOMPFAU.CancelAutoInc()
                                                              End ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                          End ! If Access.STOMPFAU.PrimeRecord() = Level:Benign
                                                      End ! Loop
      
                                                  end!if access:stomodel.primerecord() = Level:Benign
      
                                              end !loop
                                              access:stomodel_alias.restorefile(save_stm_ali_id)
                                          End!If sto_ali:manufacturer <> ''
                                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                              'ADD', | ! Transaction_Type
                                              '', | ! Depatch_Note_Number
                                              0, | ! Job_Number
                                              0, | ! Sales_Number
                                              0, | ! Quantity
                                              sto:Purchase_Cost, | ! Purchase_Cost
                                              sto:Sale_Cost, | ! Sale_Cost
                                              sto:Retail_Cost, | ! Retail_Cost
                                              'REPLICATED FROM LOCATION: ' & Clip(loc:Location), | ! Notes
                                              '') ! Information
                                              ! Added OK
                                          Else ! AddToStockHistory
                                              ! Error
                                          End ! AddToStockHistory
                                      end
                                  End!if access:stock.primerecord() = Level:Benign
                              End!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:Queue)
                          access:stock_alias.restorefile(save_sto_ali_id)
                      end !loop
                      access:location.restorefile(save_loc_id)
                      Prog.ProgressFinish()
                      Case Missive('Process Completed.','ServiceBase 3g',|
                          'midea.jpg','/OK')
                      Of 1 ! OK Button
                      End ! Case Missive
      
                  Else!If f_type = 'ALL'
      ! ____________________________________________________________________________________
                      Prog.ProgressSetup(Records(Glo:Queue))
                      !Get the orignal part
                      access:stock_alias.clearkey(sto_ali:ref_number_key)
                      sto_ali:ref_number  = f_ref_number
                      IF access:stock_alias.tryfetch(sto_ali:ref_number_key) = Level:Benign
                          !Loop through the selected locations
                          Loop x# = 1 To Records(glo:Queue)
                              Get(glo:Queue,x#)
                              Prog.ProgressText('Location: ' & glo:Pointer)
                              If (Prog.InsideLoop())
                                  Beep(Beep:SystemHand)  ;  Yield()
                                  Case Missive('You cannot cancel this process.','ServiceBase',|
                                      'mstop.jpg','/&OK')
                                  Of 1 ! &OK Button
                                  End!Case Message
                              End
                              If glo:pointer = sto_ali:location
                                  Cycle
                              End!If glo:pointer = sto_ali:location
                              !Try and see if the part already exists
                              access:stock.clearkey(sto:location_key)
                              sto:location    = glo:pointer
                              sto:part_number = sto_ali:part_number
                              if access:stock.tryfetch(sto:location_key) = Level:Benign
                                  !Check the location of the "New" part
                                  Access:LOCATION.Clearkey(loc:Location_Key)
                                  loc:Location    = sto:Location
                                  If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                                      !Found
                                      If ~(sto_ali:E1 And loc:Level1)
                                          If ~(sto_ali:E2 And loc:Level2)
                                              If ~(sto_ali:E3 And loc:Level3)
                                                  Cycle
                                              End !If ~(sto_ali:E3 And loc:Level3)
                                          End !If ~(sto_ali:E2 And loc:Level2)
                                      End !If ~(sto_ali:E1 And loc:Level1)
                                  Else! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
      
                                  sav:minimum_level = sto:minimum_level
                                  sav:reorder_level = sto:reorder_level
                                  sav:purchase_cost  = sto:purchase_cost
                                  sav:sale_cost      = sto:sale_cost
                                  sav:retail_cost    = sto:retail_cost
                                  sav:ShelfLocation   = sto:Shelf_Location
                                  sav:SecondLocation   = sto:Second_Location
                                  sav:AveragePurchaseCost = sto:AveragePurchaseCost
                                  sav:PurchaseMarkup  = sto:PurchaseMarkup
                                  sav:PercentageMarkUp = sto:Percentage_Mark_Up
                                  ref_number# = sto:ref_number
                                  quantity_stock# = sto:quantity_stock
                                  sto:record :=: sto_ali:record
                                  sto:location    = glo:pointer
                                  sto:part_number = sto_ali:part_number
                                  sto:description = sto_ali:description
                                  sto:ref_number  = ref_number#
                                  sto:quantity_stock = quantity_stock#
                                  sto:Shelf_Location  = sav:ShelfLocation
                                  sto:Second_Location = sav:SecondLocation
      
                                  if (sto_ali:Sundry_Item = 'YES')
                                      ! #11697 Override quantity if it's a sundry item. (Bryan: 17/09/2010)
                                      sto:Quantity_Stock = 1
                                      sto:Quantity_To_Order = 0
                                      sto:Quantity_On_Order = 0
                                  end ! if (sto_ali:Sundry_Item = 'YES')
      
                                  !Er.. Do nothing
                                  sto:sale_Cost       = sav:Sale_Cost
                                  sto:retail_cost     = sav:Retail_Cost
                                  sto:purchase_cost   = Sav:Purchase_Cost
                                  sto:AveragePurchaseCost = sav:AveragePurchaseCost
      ! Changing (DBH 17/04/2007) # 8924 - Fix
      !                            sto:PurchaseMarkup  = sav:PercentageMarkUp
      ! to (DBH 17/04/2007) # 8924
                                  sto:PurchaseMarkup = sav:PurchaseMarkup
      ! End (DBH 17/04/2007) #8924
                                  sto:Percentage_Mark_Up = sav:PercentageMarkUp
      
                                  !only update the costs, if it's from main store, if you've ticked the box
                                  !and if the shelf location is unallocated
                                  If sto_ali:Location = MainStoreLocation() and Costs_Temp = 1 And (sto:Shelf_Location = 'UNALLOCATED' or sto:Quantity_Stock = 0)
                                      If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                          If loc:VirtualSite
                                              sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                              sto:PurchaseMarkUp      = InWarrantyMarkup(sto:Manufacturer,loc:Location)
                                              sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp
                                              sto:Purchase_Cost       = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:PurchaseMarkup/100))
                                              sto:Sale_Cost           = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:Percentage_Mark_Up/100))
                                          End !If loc:VirtualSite
                                      End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                  End !If sto_ali:Location = MainStoreLocation()
      
                                  If Costs_Temp = 0
                                      !Er.. Do nothing
                                      sto:sale_Cost       = sav:Sale_Cost
                                      sto:retail_cost     = sav:Retail_Cost
                                      sto:purchase_cost   = Sav:Purchase_Cost
                                      sto:AveragePurchaseCost = sav:AveragePurchaseCost
                                      sto:PurchaseMarkup  = sav:PurchaseMarkUp
                                      sto:Percentage_Mark_Up = sav:PercentageMarkUp
                                  End !If Costs_Temp = 0
      !                            If costs_temp = 0
      !                                sto:sale_Cost       = 0 !sav:sale_cost
      !                                sto:retail_cost     = 0 !sav:retail_cost
      !                                sto:purchase_cost   = 0 !sav:purchase_cost
      !                                sto:AveragePurchaseCost = 0 !sav:AveragePurchaseCost
      !                                sto:PurchaseMarkup  = 0 !sav:PurchaseMarkup
      !                                sto:Percentage_Mark_Up = 0 !sav:PercentageMarkUp
      !                            End!If costs_temp = 0
      
                                  If levels_temp = 0
                                      sto:minimum_level   = sav:minimum_level
                                      sto:reorder_level   = sav:reorder_level
                                  End!If levels_temp = 0
                                  access:stock.update()
                                  save_stm_id = access:stomodel.savefile()
                                  access:stomodel.clearkey(stm:mode_number_only_key)
                                  stm:ref_number   = sto:ref_number
                                  set(stm:mode_number_only_key,stm:mode_number_only_key)
                                  loop
                                      if access:stomodel.next()
                                          break
                                      end !if
                                      if stm:ref_number   <> sto:ref_number  |
                                          then break.  ! end if
      
                                      ! Inserting (DBH 31/07/2008) # 10289 - Remove job fault codes
                                      Access:STOMJFAU.ClearKey(stj:FieldKey)
                                      stj:RefNumber = stm:RecordNumber
                                      If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                                          !Found
                                          Delete(STOMJFAU)
                                      Else ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                                          !Error
                                      End ! If Access:STOMJFAU.TryFetch(stj:FieldKey) = Level:Benign
                                      ! End (DBH 31/07/2008) #10289
                                      Delete(stomodel)
                                  end !loop
                                  access:stomodel.restorefile(save_stm_id)
      
                                  save_stm_ali_id = access:stomodel_alias.savefile()
                                  access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                  stm_ali:ref_number   = sto_ali:ref_number
                                  set(stm_ali:model_number_key,stm_ali:model_number_key)
                                  loop
                                      if access:stomodel_alias.next()
                                          break
                                      end !if
                                      if stm_ali:ref_number   <> sto_ali:ref_number
                                          Break
                                      End
                                      If Access:STOMODEL.PrimeRecord() = Level:Benign
                                          Rec# = stm:RecordNumber
                                          stm:Record :=: stm_ali:Record
                                          stm:RecordNumber = Rec#
                                          stm:Ref_Number = sto:Ref_Number
                                          stm:Location = sto:Location
                                          If Access:STOMODEL.TryInsert() = Level:Benign
                                              !Insert
                                              ! Inserting (DBH 31/07/2008) # 10289 - Add related job fault codes
                                              Access:STOMJFAU_ALIAS.ClearKey(stj_ali:FieldKey)
                                              stj_ali:RefNumber = stm_ali:RecordNumber
                                              If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                  !Found
                                                  If Access:STOMJFAU.PrimeRecord() = Level:Benign
                                                      Rec# = stj:RecordNumber
                                                      stj:Record :=: stj_ali:Record
                                                      stj:RecordNumber = Rec#
                                                      stj:RefNumber = stm:RecordNumber
                                                      If Access:STOMJFAU.TryInsert() = Level:Benign
                                                          !Insert
                                                      Else ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                          Access:STOMJFAU.CancelAutoInc()
                                                      End ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                  End ! If Access.STOMJFAU.PrimeRecord() = Level:Benign
                                              Else ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                  !Error
                                              End ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                              ! End (DBH 31/07/2008) #10289
      
                                              ! Remove all entries from "new" part (DBH: 31/07/2008)
                                              Access:STOMPFAU.Clearkey(stu:FieldKey)
                                              stu:RefNumber = stm:RecordNumber
                                              stu:FieldNumber = 0
                                              Set(stu:FieldKey,stu:FieldKey)
                                              Loop ! Begin Loop
                                                  If Access:STOMPFAU.Next()
                                                      Break
                                                  End ! If Access:STOMPFAU.Next()
                                                  If stu:RefNumber <> stm:RecordNumber
                                                      Break
                                                  End ! If stu:RefNumber <> stm:RecordNumber
                                                  Delete(STOMPFAU)
                                              End ! Loop
      
                                              Access:STOMPFAU_ALIAS.Clearkey(stu_ali:FieldKey)
                                              stu_ali:RefNumber = stm_ali:RecordNumber
                                              stu_ali:FieldNumber = 0
                                              Set(stu_ali:FieldKey,stu_ali:FieldKey)
                                              Loop ! Begin Loop
                                                  If Access:STOMPFAU_ALIAS.Next()
                                                      Break
                                                  End ! If Access:STOMPFAU_ALIAS.Next()
                                                  If stu_ali:RefNumber <> stm_ali:RecordNumber
                                                      Break
                                                  End ! If stu_ali:RefNumber <> stm_ali:RecordNumber
      
                                                  If Access:STOMPFAU.PrimeRecord() = Level:Benign
                                                      Rec# = stu:RecordNumber
                                                      stu:Record :=: stu_ali:Record
                                                      stu:RecordNumber = Rec#
                                                      stu:RefNumber = stm:RecordNumber
                                                      If Access:STOMPFAU.TryInsert() = Level:Benign
                                                          !Insert
                                                      Else ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                          Access:STOMPFAU.CancelAutoInc()
                                                      End ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                  End ! If Access.STOMPFAU.PrimeRecord() = Level:Benign
                                              End ! Loop
      
      
                                          Else ! If Access:STOMODEL.TryInsert() = Level:Benign
                                              Access:STOMODEL.CancelAutoInc()
                                          End ! If Access:STOMODEL.TryInsert() = Level:Benign
                                      End ! If Access.STOMODEL.PrimeRecord() = Level:Benign
      
                                  end !loop
                                  access:stomodel_alias.restorefile(save_stm_ali_id)
      
      
                              Else!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  ! Part Doesn't Exist. Insert It!
      
                                  !Check the location of the "New" part
                                  Access:LOCATION.Clearkey(loc:Location_Key)
                                  loc:Location    = glo:Pointer
                                  If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                                      !Found
                                      If ~(sto_ali:E1 And loc:Level1)
                                          If ~(sto_ali:E2 And loc:Level2)
                                              If ~(sto_ali:E3 And loc:Level3)
                                                  Cycle
                                              End !If ~(sto_ali:E3 And loc:Level3)
                                          End !If ~(sto_ali:E2 And loc:Level2)
                                      End !If ~(sto_ali:E1 And loc:Level1)
                                  Else! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:LOCATION.Tryfetch(loc:Location_Key) = Level:Benign
      
                                  get(stock,0)                                                    !If part doesn't exist. Add the part
                                  if access:stock.primerecord() = Level:Benign                    !into the new location. Make a direct
                                      ref_number# = sto:ref_number                                !copy of the alias part, except for the
                                      sto:record :=: sto_ali:record                               !ref number and location.
                                      sto:ref_number  = ref_number#
                                      sto:location    = glo:pointer
                                      sto:quantity_stock  = 0
                                      sto:Second_Location = ''
                                      do ShelfLocation:Unallocated
                                      If sto_ali:Location = MainStoreLocation()
                                          If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And Costs_temp = 1
                                              If loc:VirtualSite
                                                  sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                                  sto:PurchaseMarkUp      = InWarrantyMarkup(sto:Manufacturer,loc:Location)
                                                  sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp
                                                  sto:Purchase_Cost       = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:PurchaseMarkup/100))
                                                  sto:Sale_Cost           = sto:AveragePurchaseCost + (sto:AveragePurchaseCost * (sto:Percentage_Mark_Up/100))
                                              End !If loc:VirtualSite
                                          End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                                      End !If sto_ali:Location = MainStoreLocation()
      
      
                                      If costs_temp = 0
                                          sto:sale_Cost       = 0 !sav:sale_cost
                                          sto:retail_cost     = 0 !sav:retail_cost
                                          sto:purchase_cost   = 0 !sav:purchase_cost
                                          sto:AveragePurchaseCost = 0 !sav:AveragePurchaseCost
                                          sto:PurchaseMarkup  = 0 !sav:PurchaseMarkup
                                          sto:Percentage_Mark_Up = 0 !sav:PercentageMarkUp
                                      End!If costs_temp = 0
      
                                      If levels_temp = 0
                                          sto:minimum_level = 0
                                          sto:reorder_level = 0
                                      End!If levels_temp = 0
      
                                      if (sto_ali:Sundry_Item = 'YES')
                                          ! #11697 Override quantity if it's a sundry item. (Bryan: 17/09/2010)
                                          sto:Quantity_Stock = 1
                                          sto:Quantity_To_Order = 0
                                          sto:Quantity_On_Order = 0
                                      end ! if (sto_ali:Sundry_Item = 'YES')
      
                                      if access:stock.insert()
                                          access:stock.cancelautoinc()
                                      Else
                                          save_stm_ali_id = access:stomodel_alias.savefile()
                                          access:stomodel_alias.clearkey(stm_ali:model_number_key)
                                          stm_ali:ref_number   = sto_ali:ref_number
                                          set(stm_ali:model_number_key,stm_ali:model_number_key)
                                          loop
                                              if access:stomodel_alias.next()
                                                  break
                                              end !if
                                              if stm_ali:ref_number   <> sto_ali:ref_number
                                                  Break
                                              end
                                              get(stomodel,0)
                                              if access:stomodel.primerecord() = Level:Benign
                                                  RecordNumber$   = stm:RecordNumber
                                                  stm:record  :=: stm_ali:record
                                                  stm:RecordNumber    = RecordNumber$
                                                  stm:ref_number  = sto:ref_number
                                                  stm:location    = sto:location
                                                  if access:stomodel.insert()
                                                      access:stomodel.cancelautoinc()
                                                  end
                                                  ! Inserting (DBH 31/07/2008) # 10289 - Add related job fault codes
                                                  Access:STOMJFAU_ALIAS.ClearKey(stj_ali:FieldKey)
                                                  stj_ali:RefNumber = stm_ali:RecordNumber
                                                  If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                      !Found
                                                      If Access:STOMJFAU.PrimeRecord() = Level:Benign
                                                          Rec# = stj:RecordNumber
                                                          stj:Record :=: stj_ali:Record
                                                          stj:RecordNumber = Rec#
                                                          stj:RefNumber = stm:RecordNumber
                                                          If Access:STOMJFAU.TryInsert() = Level:Benign
                                                              !Insert
                                                          Else ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                              Access:STOMJFAU.CancelAutoInc()
                                                          End ! If Access:STOMJFAU.TryInsert() = Level:Benign
                                                      End ! If Access.STOMJFAU.PrimeRecord() = Level:Benign
                                                  Else ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                      !Error
                                                  End ! If Access:STOMJFAU_ALIAS.TryFetch(stj_ali:FieldKey) = Level:Benign
                                                  ! End (DBH 31/07/2008) #10289
      
                                                  Access:STOMPFAU_ALIAS.Clearkey(stu_ali:FieldKey)
                                                  stu_ali:RefNumber = stm_ali:RecordNumber
                                                  stu_ali:FieldNumber = 0
                                                  Set(stu_ali:FieldKey,stu_ali:FieldKey)
                                                  Loop ! Begin Loop
                                                      If Access:STOMPFAU_ALIAS.Next()
                                                          Break
                                                      End ! If Access:STOMPFAU_ALIAS.Next()
                                                      If stu_ali:RefNumber <> stm_ali:RecordNumber
                                                          Break
                                                      End ! If stu_ali:RefNumber <> stm_ali:RecordNumber
      
                                                      If Access:STOMPFAU.PrimeRecord() = Level:Benign
                                                          Rec# = stu:RecordNumber
                                                          stu:Record :=: stu_ali:Record
                                                          stu:RecordNumber = Rec#
                                                          stu:RefNumber = stm:RecordNumber
                                                          If Access:STOMPFAU.TryInsert() = Level:Benign
                                                              !Insert
                                                          Else ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                              Access:STOMPFAU.CancelAutoInc()
                                                          End ! If Access:STOMPFAU.TryInsert() = Level:Benign
                                                      End ! If Access.STOMPFAU.PrimeRecord() = Level:Benign
                                                  End ! Loop
      
                                              end!if access:stomodel.primerecord() = Level:Benign
      
                                          end !loop
                                          access:stomodel_alias.restorefile(save_stm_ali_id)
                                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                              'ADD', | ! Transaction_Type
                                              '', | ! Depatch_Note_Number
                                              0, | ! Job_Number
                                              0, | ! Sales_Number
                                              0, | ! Quantity
                                              sto:Purchase_Cost, | ! Purchase_Cost
                                              sto:Sale_Cost, | ! Sale_Cost
                                              sto:Retail_Cost, | ! Retail_Cost
                                              'REPLICATED FROM LOCATION: ' & Clip(sto_ali:Location), | ! Notes
                                              '') ! Information
                                              ! Added OK
      
                                          Else ! AddToStockHistory
                                              ! Error
                                          End ! AddToStockHistory
                                      end
                                  End!if access:stock.primerecord() = Level:Benign
                              End!if access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:Queue)
      
                      End!IF access:stock_alias.tryfetch(sto_ali:ref_number_key) = Level:Benign
      
                      Prog.ProgressFinish()
                      Case Missive('Process Completed.','ServiceBase 3g',|
                          'midea.jpg','/OK')
                      Of 1 ! OK Button
                      End ! Case Missive
                      flush(stock)
                      flush(stock_alias)
                      flush(stomodel)
                      flush(stomjfau)
                      flush(stomodel_alias)
                      flush(stomjfau_alias)
      
                      Post(Event:closewindow)
                  End!If f_type = 'ALL'
      
      
      
              Of 1 ! Cancel Button
              End ! Case Missive
      
          End!If ~Records(glo:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Ok, Accepted)
    OF ?VSNextButton
      ThisWindow.Update
         Wizard2.TakeAccepted()
    OF ?VSBackButton
      ThisWindow.Update
         Wizard2.TakeAccepted()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::6:DASUNTAGALL
      Select(?Sheet1,1)
      Case f_Type
          Of 'ALL'
          Of 'SINGLE'
              ?title{prop:text} = 'Replicate Stock Item'
              ?general1{prop:text} = 'This facility will replicate the Stock Item you have just inserted into other locations.'
      End!Case f_Type
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = loc:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END
  IF (loc:VirtualSite = 1)
    SELF.Q.loc:Location_NormalFG = 32768
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 32768
  ELSIF (loc:Active = 0)
    SELF.Q.loc:Location_NormalFG = 8421504
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 8421504
  ELSE
    SELF.Q.loc:Location_NormalFG = -1
    SELF.Q.loc:Location_NormalBG = -1
    SELF.Q.loc:Location_SelectedFG = -1
    SELF.Q.loc:Location_SelectedBG = -1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  !TB12448 second part hide franchises - JC - 12/07/12
  
      access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
      tra_ali:SiteLocation = loc:Location
      if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
          !eh?? - let it show
      ELSE
          if tra_ali:Stop_Account = 'YES' then return(record:filtered).
      END
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = loc:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Wizard2.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()


Wizard2.TakeBackEmbed PROCEDURE
   CODE

Wizard2.TakeNextEmbed PROCEDURE
   CODE

Wizard2.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 5
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Browse_Stock_By_Model PROCEDURE (func:Location)       !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:partnumber       STRING(30)
tmp:description      STRING(30)
tmp:location         STRING(30)
save_sto_id          USHORT,AUTO
save_stm_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
manufacturer_temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Description)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:RecordNumber)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Location)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Purchase_Cost)
                         PROJECT(sto:Sale_Cost)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Shelf_Location)
                         PROJECT(sto:Second_Location)
                         PROJECT(sto:Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
stm:RecordNumber       LIKE(stm:RecordNumber)         !Primary key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Browse key field - type derived from field
stm:Location           LIKE(stm:Location)             !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK18::stm:Location        LIKE(stm:Location)
HK18::stm:Model_Number    LIKE(stm:Model_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Site Location'),AT(68,58),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(124,58,124,10),USE(location_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(108,152,428,176),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('124L(2)|M~Part Number~@s30@124L(2)|M~Description~@s30@38R(2)|M~In Warr~L@n10.2@3' &|
   '8R(2)|M~Out Warr~L@n10.2@47R(2)|M~Qty In Stock~L@N8@120R(2)|M~Shelf Location~L@s' &|
   '30@120R(2)|M~Second Location~L@s30@32R(2)|M~Ref Number~L@s8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(204,332),USE(?DepleteStock),TRN,FLAT,LEFT,ICON('depstokp.jpg')
                       BUTTON,AT(476,332),USE(?TransferStock),TRN,FLAT,LEFT,ICON('stotranp.jpg')
                       BUTTON,AT(108,332),USE(?AddStock),TRN,FLAT,LEFT,ICON('addstokp.jpg')
                       BUTTON,AT(548,246),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(548,274),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(548,302),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       PROMPT('Shelf Location'),AT(352,122),USE(?STO:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(412,122,124,10),USE(sto:Shelf_Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver),UPR,READONLY
                       BUTTON,AT(548,172),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       PROMPT('2nd Location'),AT(352,138),USE(?STO:Second_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(412,138,124,10),USE(sto:Second_Location),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR,READONLY
                       ENTRY(@s30),AT(168,124,124,10),USE(model_number_temp),FONT(,,,FONT:bold),COLOR(COLOR:White),UPR
                       BUTTON,AT(296,118),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(388,332),USE(?OrderStock),TRN,FLAT,LEFT,ICON('ordstop.jpg')
                       BUTTON,AT(296,332),USE(?StockHistory),TRN,FLAT,LEFT,ICON('stohistp.jpg')
                       SHEET,AT(64,76,552,286),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,110,124,10),USE(stm:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(472,332),USE(?ReturnFaultyStock),TRN,FLAT,HIDE,ICON('retfaup.jpg')
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(108,110,124,10),USE(stm:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       PROMPT('Model Number'),AT(108,124),USE(?Prompt2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:model_number_temp                Like(model_number_temp)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020073'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Stock_By_Model')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      ?Location_Temp{prop:Disable} = 1
      ?Insert{prop:Hide} = 1
      ?Delete{prop:Hide} = 1
      ?TransferStock{prop:Hide} = 1
      ?ReturnFaultyStock{prop:Hide} = 0
  End !glo:WebJob
  ! Save Window Name
   AddToLog('Window','Open','Browse_Stock_By_Model')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?model_number_temp{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?model_number_temp{Prop:Tip}
  END
  IF ?model_number_temp{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?model_number_temp{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,stm:Location_Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STM:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1.AddSortOrder(,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  BRW1.AddField(sto:Second_Location,BRW1.Q.sto:Second_Location)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(stm:RecordNumber,BRW1.Q.stm:RecordNumber)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:Location_Key)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,location_temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#38R(2)|M~In Warr~L@n10.2@#3#38R(2)|M~Out Warr~L@n10.2@#4#47R(2)|M~Qty In Stock~L@N8@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Stock_By_Model')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    browse_models_by_man
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?location_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?location_temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?location_temp, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020073'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020073'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020073'&'0')
      ***
    OF ?DepleteStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DepleteStock, Accepted)
      If SecurityCheck('STOCK - DEPLETE STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Deplete Stock.'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Deplete_Stock(brw1.q.stm:ref_number,0)
          End!If error# = 0
      
      End!!if x" = false
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DepleteStock, Accepted)
    OF ?TransferStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TransferStock, Accepted)
      If glo:WebJob
          If SecurityCheck('STOCK - DEPLETE STOCK')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else!if x" = false
      
              Thiswindow.reset
              error# = 0
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = brw1.q.sto:ref_number
              if access:stock.fetch(sto:ref_number_key)
                  error# = 1
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:sundry_item = 'YES'
                      Case Missive('Cannot Deplete Stock.'&|
                        '<13,10>'&|
                        '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End!If sto:sundry_item = 'YES'
              end!if access:stock.fetch(sto:ref_number_key)
      
              If error# = 0
                  Deplete_Stock(brw1.q.sto:ref_number,1)
              End!If error# = 0
      
          End!!if x" = false
      Else !glo:WebJob
      
          If SecurityCheck('STOCK - STOCK TRANSFER')
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else!if x" = false
      
              Thiswindow.reset
              glo:select1 = location_temp
              glo:select2 = brw1.q.STO:Ref_Number
              Transfer_stock
              glo:select1 = ''
              glo:select2 = ''
      
          End!Else!if x" = false
      
      End !glo:WebJob
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TransferStock, Accepted)
    OF ?AddStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddStock, Accepted)
      If SecurityCheck('STOCK - ADD STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Add Stock.'&|
                    '<13,10>'&|
                    '<13,10>This part has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Add_Stock(brw1.q.stm:ref_number)
          End!If error# = 0
      
      end!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddStock, Accepted)
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      If ~glo:WebJob
          thiswindow.reset(1)
          Globalrequest   = Insertrecord
          access:stock.primerecord()
          sto:Location    = Location_Temp
          If DeleteStockPart(Location_Temp,InsertRecord,brw1.q.stm:Ref_Number)
              Updatestock
          End !If DeleteStockPart(Location_Temp,Request,brw1.q.sto:Ref_Number)
      End !glo:WebJob
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Change
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
      thiswindow.reset(1)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = brw1.q.stm:ref_number
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
          If glo:WebJob
              GlobalRequest = ViewRecord
          Else !If glo:WebJob
              Globalrequest   = ChangeRecord
          End !If glo:WebJob
          If DeleteStockPart(Location_Temp,GlobalRequest,brw1.q.stm:Ref_Number)
              Updatestock
          End !If DeleteStockPart(Location_Temp,Request,brw1.q.sto:Ref_Number)
      
      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
    OF ?Delete
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete, Accepted)
      If ~glo:WebJob
          thiswindow.reset(1)
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number  = brw1.q.stm:ref_number
          If access:stock.fetch(sto:ref_number_key) = Level:Benign
              Globalrequest   = DeleteRecord
              If DeleteStockPart(Location_Temp,GlobalRequest,brw1.q.stm:Ref_Number)
                  Updatestock
              End !If DeleteStockPart(Location_Temp,Request,brw1.q.sto:Ref_Number)
          End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
      End !glo:WebJob
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete, Accepted)
    OF ?model_number_temp
      IF model_number_temp OR ?model_number_temp{Prop:Req}
        mod:Model_Number = model_number_temp
        !Save Lookup Field Incase Of error
        look:model_number_temp        = model_number_temp
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            model_number_temp = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            model_number_temp = look:model_number_temp
            SELECT(?model_number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?CallLookup
      ThisWindow.Update
      mod:Model_Number = model_number_temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          model_number_temp = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?model_number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?model_number_temp)
    OF ?OrderStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OrderStock, Accepted)
      If SecurityCheck('STOCK - ORDER STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Order.'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              Else
                  If sto:Suspend
                      error# = 1
                      Case Missive('This part cannot be Ordered. It has been suspended.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End !If sto:Suspend
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              glo:select1 = sto:ref_number
              Stock_Order
              glo:select1 = ''
          End!If error# = 0
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OrderStock, Accepted)
    OF ?StockHistory
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockHistory, Accepted)
      If SecurityCheck('STOCK - STOCK HISTORY')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.stm:ref_number
          if access:stock.fetch(sto:ref_number_key) = Level:Benign
              glo:select1 = STO:Ref_Number
              Browse_Stock_History
              glo:select1 = ''
          End!if access:stock.fetch(sto:ref_number_key) = Level:Benign
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StockHistory, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If Keycode() = MouseLeft2
          Post(Event:accepted,?Change)
      End!If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#38R(2)|M~In Warr~L@n10.2@#3#38R(2)|M~Out Warr~L@n10.2@#4#47R(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#124L(2)|M~Part Number~@s30@#1#38R(2)|M~In Warr~L@n10.2@#3#38R(2)|M~Out Warr~L@n10.2@#4#47R(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      location_temp = func:Location
      Display()
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = model_number_temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  ELSIF Choice(?CurrentTab) = 1
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = model_number_temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  ELSE
  END
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
      if (sto:Suspend)
          Return Record:Filtered
      end
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (loc:Main_Store = 'YES')
    SELF.Q.loc:Location_NormalFG = 255
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 255
  ELSIF (loc:VirtualSite = 1)
    SELF.Q.loc:Location_NormalFG = 1024
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 1024
  ELSIF (loc:Active = 0)
    SELF.Q.loc:Location_NormalFG = 8421504
    SELF.Q.loc:Location_NormalBG = 16777215
    SELF.Q.loc:Location_SelectedFG = 16777215
    SELF.Q.loc:Location_SelectedBG = 8421504
  ELSE
    SELF.Q.loc:Location_NormalFG = -1
    SELF.Q.loc:Location_NormalBG = -1
    SELF.Q.loc:Location_SelectedFG = -1
    SELF.Q.loc:Location_SelectedBG = -1
  END


FDCB6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(6, ValidateRecord, (),BYTE)
  !TB12448 second part hide franchises - JC - 12/07/12
  
      access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
      tra_ali:SiteLocation = loc:Location
      if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
          !eh?? - let it show
      ELSE
          if tra_ali:Stop_Account = 'YES' then return(record:filtered).
      END
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(6, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Browse_Stock PROCEDURE                                !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
average_temp         LONG
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
average_text_temp    STRING(30)
tmp:LastOrdered      DATE
save_stm_ali_id      USHORT,AUTO
save_shi_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
save_orp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
tmp:partnumber       STRING(30)
tmp:description      STRING(30)
tmp:location         STRING(30)
tmp:Overwrite        BYTE(0)
tmp:Matching         BYTE(0)
tmp:DeleteStock      BYTE(0)
tmp:WrongSkill       BYTE(0)
LocalRequest         LONG
FilesOpened          BYTE
Location_Temp        STRING(30)
Manufacturer_Temp    STRING(30)
CPCSExecutePopUp     BYTE
accessory_temp       STRING('ALL')
shelf_location_temp  STRING(60)
no_temp              STRING('NO')
yes_temp             STRING('YES')
BrFilter1            STRING(300)
BrLocator1           STRING(30)
BrFilter2            STRING(300)
BrLocator2           STRING(30)
tmp:QtyOnOrder       LONG
SaveQueue            QUEUE,PRE(save)
RetailMarkUp         REAL
AveragePurchaseCost  REAL
PurchaseMarkup       REAL
DateBooked           DATE
PurchaseCost         REAL
SaleCost             REAL
RetailCost           REAL
AccessoryCost        REAL
PercentageMarkUp     REAL
QuantityStock        LONG
Shelf_Location       STRING(30)
Second_Location      STRING(30)
Minimum_Level        REAL
Reorder_Level        REAL
                     END
Parts_Q              QUEUE,PRE(ptq)
Site_Location        STRING(30)
Part_No              STRING(30)
Description          STRING(30)
Shelf_Location       STRING(30)
Skill_Level_1        STRING(1)
Skill_Level_2        STRING(1)
Skill_Level_3        STRING(1)
Quantity             REAL
                     END
tmp:PurchaseCost     REAL
tmp:SaleCost         REAL
tmp:RetailCost       REAL
tmp:ReviewShelfLocation BYTE(0)
tmp:MainStore        BYTE(0)
tmp:ExchangeModelNumber STRING(30)
tmp:OldPartNumber    STRING(30)
locFalse             BYTE(0)
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:Notes              LIKE(man:Notes)                !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Manufacturer)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Sale_Cost)
                       PROJECT(sto:Retail_Cost)
                       PROJECT(sto:AveragePurchaseCost)
                       PROJECT(sto:Location)
                       PROJECT(sto:Suspend)
                       PROJECT(sto:Accessory)
                       PROJECT(sto:Shelf_Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Manufacturer       LIKE(sto:Manufacturer)         !List box control field - type derived from field
shelf_location_temp    LIKE(shelf_location_temp)      !List box control field - type derived from local data
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Retail_Cost        LIKE(sto:Retail_Cost)          !List box control field - type derived from field
tmp:SaleCost           LIKE(tmp:SaleCost)             !List box control field - type derived from local data
tmp:PurchaseCost       LIKE(tmp:PurchaseCost)         !List box control field - type derived from local data
tmp:RetailCost         LIKE(tmp:RetailCost)           !List box control field - type derived from local data
sto:AveragePurchaseCost LIKE(sto:AveragePurchaseCost) !Browse hot field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Suspend            LIKE(sto:Suspend)              !Browse key field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse key field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::sto:Accessory       LIKE(sto:Accessory)
HK10::sto:Location        LIKE(sto:Location)
HK10::sto:Manufacturer    LIKE(sto:Manufacturer)
HK10::sto:Suspend         LIKE(sto:Suspend)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB26::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:Notes)
                       PROJECT(man:RecordNumber)
                     END
QuickWindow          WINDOW('Browse The Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Browse The Stock File'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Site Location:'),AT(8,34),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(72,30),USE(?LookupLocation),TRN,FLAT,ICON('lookupp.jpg')
                       STRING(@s30),AT(104,34),USE(Location_Temp),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       OPTION('Part Type'),AT(388,30,189,19),USE(accessory_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Non-Accessory'),AT(397,37),USE(?accessory_temp:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                         RADIO('Accessory'),AT(473,37),USE(?accessory_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                         RADIO('All'),AT(541,37),USE(?accessory_temp:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                       END
                       LIST,AT(8,98,572,266),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(HomeKey),FORMAT('86L(2)|M~Part Number~@s30@86L(2)|M~Description~@s30@86L(2)|M~Manufacturer~@s30@1' &|
   '32L(2)|M~Shelf Location~@s60@97L(2)|M~Supplier~@s30@/48R(2)|M~Qty In Stock~L@N8@' &|
   '0L(2)|M~Reference Number~@n012@/56L(2)|M~Purchase Cost~@n14.2@/56L(2)|M~Sale Cos' &|
   't~@n14.2@/56L(2)|M~Retail Cost~@n14.2@/56R(2)|M~Sale Cost~L@n14.2@/56R(2)|M~Purc' &|
   'hase Cost~L@n14.2@/56R(2)|M~Retail Cost~L@n14.2@/'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(588,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       GROUP,AT(601,196,75,103),USE(?Button_Group3)
                         BUTTON,AT(608,246),USE(?browse_model_stock),TRN,FLAT,LEFT,ICON('brmstokp.jpg')
                         BUTTON,AT(608,220),USE(?DuplicateStock),TRN,FLAT,LEFT,ICON('repstokp.jpg')
                         BUTTON,AT(608,272),USE(?Button14),TRN,FLAT,LEFT,ICON('revlocp.jpg')
                         BUTTON,AT(608,194),USE(?rebuild_model_stock),TRN,FLAT,LEFT,ICON('rebstokp.jpg')
                       END
                       BUTTON,AT(608,308),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(608,336),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(608,364),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(4,56,672,338),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By &Part Number'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,78,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('"Old" Part Number Search'),AT(264,78),USE(?tmp:OldPartNumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(380,78,124,10),USE(tmp:OldPartNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Old Part Number'),TIP('Old Part Number'),UPR
                           BUTTON,AT(512,70),USE(?Button:SearchOldPartNumber),TRN,FLAT,ICON('fullseap.jpg')
                           BUTTON,AT(512,366),USE(?ReturnStock),TRN,FLAT,HIDE,ICON('retstokp.jpg')
                         END
                         TAB('By Desc&ription'),USE(?Tab:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,80,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By S&helf Location'),USE(?Tab:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,80,124,10),USE(sto:Shelf_Location),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Su&pplier'),USE(?Tab:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,80,124,10),USE(sto:Supplier),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By &Manufacturer'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(136,80,124,10),USE(sto:Part_Number,,?STO:Part_Number:2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Part Number'),AT(264,80),USE(?Prompt2),FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           COMBO(@s30),AT(8,80,124,10),USE(Manufacturer_Temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,FORMAT('120L(2)@s30@0L(2)@s255@'),DROP(10),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       SHEET,AT(584,114,88,78),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 6'),USE(?Tab6)
                           PROMPT('Qty On Order'),AT(588,119),USE(?Prompt4),TRN,FONT(,,080FFFFH,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           STRING(@s4),AT(644,119),USE(tmp:QtyOnOrder),TRN,RIGHT,FONT(,7,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Last Order'),AT(588,130),USE(?Prompt4:2),TRN,FONT(,,080FFFFH,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           STRING(@d6b),AT(620,130),USE(tmp:LastOrdered),TRN,RIGHT,FONT(,7,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Costs'),AT(588,143),USE(?Prompt9),TRN,LEFT,FONT(,,,FONT:bold+FONT:underline)
                           STRING(@n9.2),AT(628,154),USE(tmp:PurchaseCost),TRN,RIGHT,FONT(,7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n9.2),AT(628,167),USE(tmp:SaleCost),TRN,RIGHT,FONT(,7,,FONT:bold,CHARSET:ANSI)
                           STRING(@n9.2),AT(628,178),USE(tmp:RetailCost),TRN,RIGHT,FONT(,7,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Retail Cost'),AT(588,178),USE(?tmp:RetailCost:Prompt),TRN,FONT(,,080FFFFH,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Purch Cost:'),AT(588,154),USE(?tmp:PurchaseCost:Prompt),TRN,FONT(,,080FFFFH,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Selling Price:'),AT(588,167),USE(?tmp:SaleCost:Prompt),TRN,FONT(,,080FFFFH,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         END
                       END
                       BUTTON,AT(608,396),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       GROUP,AT(0,398,576,24),USE(?Button_Group2)
                         BUTTON,AT(515,398),USE(?buttonWaybillConfirmation),TRN,FLAT,ICON('rswconfp.jpg')
                         BUTTON,AT(423,398),USE(?buttonReturnStockTracking),TRN,FLAT,ICON('retsttrp.jpg')
                         BUTTON,AT(344,366),USE(?ButtonImportOrder),FLAT,ICON('impordp.jpg')
                         BUTTON,AT(175,396),USE(?buttonSuspendParts),TRN,FLAT,HIDE,ICON('suspar2p.jpg')
                         BUTTON,AT(91,396),USE(?buttonSuspendedStock),TRN,FLAT,ICON('suspartp.jpg')
                         BUTTON,AT(7,396),USE(?Button:OutstandingStockOrders),TRN,FLAT,ICON('outstokp.jpg')
                       END
                       PANEL,AT(4,28,672,24),USE(?Panel1),FILL(09A6A7CH)
                       GROUP,AT(4,366,576,24),USE(?Button_Group)
                         BUTTON,AT(8,366),USE(?Add_Stock),TRN,FLAT,LEFT,ICON('addstokp.jpg')
                         BUTTON,AT(92,366),USE(?Deplete_Stock),TRN,FLAT,LEFT,ICON('depstokp.jpg')
                         BUTTON,AT(260,366),USE(?Order_Stock),TRN,FLAT,LEFT,ICON('ordstop.jpg')
                         BUTTON,AT(428,366),USE(?Stock_Transfer),TRN,FLAT,LEFT,ICON('stotranp.jpg')
                       END
                       BUTTON,AT(176,366),USE(?Stock_History),TRN,FLAT,LEFT,ICON('stohistp.jpg')
                     END

! moving bar window

rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

!the window itself
progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort5:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
BRW1::Sort10:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
BRW1::Sort11:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
BRW1::Sort3:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
BRW1::Sort12:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
BRW1::Sort13:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
BRW1::Sort4:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
BRW1::Sort14:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
BRW1::Sort15:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB26               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_stm_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ShowHideReplicate       Routine
    If Location_Temp <> MainStoreLocation()
        ?DuplicateStock{prop:Hide} = 1
        ?ReturnStock{prop:Hide} = 0    ! #12151 Show Return Stock Button (Bryan: 01/09/2011)
        !?buttonReturnStockTracking{prop:Hide} = 0

    Else
        ?DuplicateStock{prop:Hide} = 0
        ?ReturnStock{prop:Hide} = 1      ! #12151 Show Return Stock Button (Bryan: 01/09/2011)
        !?buttonReturnStockTracking{prop:Hide} = 1
    End !If Location_Temp <> MainStoreLocation()
AddStockHistory     Routine
    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                         'ADD', | ! Transaction_Type
                         '', | ! Depatch_Note_Number
                         0, | ! Job_Number
                         0, | ! Sales_Number
                         0, | ! Quantity
                         sto:Purchase_Cost, | ! Purchase_Cost
                         sto:Sale_Cost, | ! Sale_Cost
                         sto:Retail_Cost, | ! Retail_Cost
                         'STOCK REPLICATED FROM: ' & Clip(Location_Temp), | ! Notes
                         '') ! Information
        ! Added OK
    Else ! AddToStockHistory
        ! Error
    End ! AddToStockHistory
AddModelStock       Routine
    Save_stm_ali_ID = Access:STOMODEL_ALIAS.SaveFile()
    Access:STOMODEL_ALIAS.ClearKey(stm_ali:Mode_Number_Only_Key)
    stm_ali:Ref_Number   = sto_ali:Ref_Number
    Set(stm_ali:Mode_Number_Only_Key,stm_ali:Mode_Number_Only_Key)
    Loop
        If Access:STOMODEL_ALIAS.NEXT()
           Break
        End !If
        If stm_ali:Ref_Number   <> sto_ali:Ref_Number      |
            Then Break.  ! End If
        If Access:STOMODEL.PrimeRecord() = Level:Benign
            RecordNumber$       = stm:RecordNumber
            stm:Record         :=: stm_ali:Record
            stm:RecordNumber    = RecordNumber$
            stm:Ref_Number      = sto:Ref_Number
            stm:Location        = loc:Location
            If Access:STOMODEL.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:STOMODEL.TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:STOMODEL.TryInsert() = Level:Benign
        End !If Access:STOMODEL.PrimeRecord() = Level:Benign
    End !Loop
    Access:STOMODEL_ALIAS.RestoreFile(Save_stm_ali_ID)
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Refresh_Browse      Routine
    If (Location_Temp = MainStoreLocation())
        ?buttonSuspendParts{prop:Hide} = 0
    Else
        ?buttonSuspendParts{prop:Hide} = 1
    End !If (Location_Temp = MainStoreLocation())
!    thiswindow.reset(1)
!    Case Choice(?CurrentTab)
!        Of 5
!            If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                sto:accessory   = Upper(accessory_temp)
!                brw1.addrange(sto:manufacturer,manufacturer_temp)
!                brw1.applyrange
!            Else!If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                sto:accessory   = Upper(accessory_temp)
!                brw1.addrange(sto:manufacturer,manufacturer_temp)
!                brw1.applyrange
!            End!If accessory_temp <> 'ALL'
!        Else
!            If accessory_temp = 'ALL'
!                brw1.addrange(sto:location,location_temp)
!                brw1.applyrange
!            Else!If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                brw1.addrange(sto:accessory,accessory_temp)
!                brw1.applyrange
!            End!If accessory_temp <> 'ALL'
!    End!Case Choice(?CurrentTab)
!    thiswindow.reset(1)
!    Brw1.resetsort(1)

Costs:RecalculateMarkup Routine
    ! Recalculate costs
    sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp                         ! Get out warranty markup
    sto:PurchaseMarkUp = InWarrantyMarkup(sto:Manufacturer,sto:Location)    ! Get in warranty markup

    if sto:Percentage_Mark_Up <> 0
        if sto:Location = MainStoreLocation()
            sto:sale_cost = Markups(sto:Sale_Cost,sto:Purchase_Cost,sto:Percentage_Mark_Up)
        else
            sto:Sale_Cost = Markups(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
        end
    end

    if sto:RetailMarkup <> 0
        sto:Retail_Cost = Markups(sto:Retail_Cost,sto:Purchase_Cost,sto:RetailMarkup)
    end

    if sto:PurchaseMarkUp <> 0
        if loc:Location <> MainStoreLocation()
            sto:Purchase_Cost = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
        end
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020072'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:LOCSHELF.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
    If glo:WebJob
        !message('Setting up window')
  !      ?Location_Temp{prop:ReadOnly} = 1
  !      ?Location_Temp{prop:Skip} = 1
        ?LookupLocation{prop:Disable} = 1
        ?Prompt4{prop:Hide} = 1
        ?Prompt4:2{prop:Hide} = 1
        ?tmp:QtyOnOrder{prop:Hide} = 1
        ?tmp:LastOrdered{prop:Hide} = 1
        ?Stock_Transfer{prop:Hide} = 1
        ?Rebuild_Model_Stock{prop:Hide} = 1
        ?Insert:3{prop:Hide} = 1
        ?Delete:3{prop:Hide} = 1
        ?DuplicateStock{prop:Hide} = 1
        BRW1.InsertControl=0
        BRW1.DeleteControl=0
    End !glo:WebJob


  ! Save Window Name
   AddToLog('Window','Open','Browse_Stock')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Location_Temp{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?Location_Temp{Prop:Tip}
  END
  IF ?Location_Temp{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?Location_Temp{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sto:LocPartSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:LocDescSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:LocShelfSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sto:Shelf_Location,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Shelf_Location_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?Tab:5,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Shelf_Location_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?sto:Shelf_Location,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Supplier_Key)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?sto:Supplier,sto:Supplier,1,BRW1)
  BRW1.AddSortOrder(,sto:Supplier_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?sto:Supplier,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Supplier_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?sto:Supplier,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:LocManSuspendKey)
  BRW1.AddRange(sto:Manufacturer)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?STO:Part_Number:2,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Manufacturer_Accessory_Key)
  BRW1.AddRange(sto:Manufacturer)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?STO:Part_Number:2,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Manufacturer_Accessory_Key)
  BRW1.AddRange(sto:Manufacturer)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?STO:Part_Number:2,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:LocPartSuspendKey)
  BRW1.AddRange(sto:Suspend)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BIND('shelf_location_temp',shelf_location_temp)
  BIND('tmp:SaleCost',tmp:SaleCost)
  BIND('tmp:PurchaseCost',tmp:PurchaseCost)
  BIND('tmp:RetailCost',tmp:RetailCost)
  BIND('tmp:QtyOnOrder',tmp:QtyOnOrder)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Manufacturer,BRW1.Q.sto:Manufacturer)
  BRW1.AddField(shelf_location_temp,BRW1.Q.shelf_location_temp)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  BRW1.AddField(tmp:SaleCost,BRW1.Q.tmp:SaleCost)
  BRW1.AddField(tmp:PurchaseCost,BRW1.Q.tmp:PurchaseCost)
  BRW1.AddField(tmp:RetailCost,BRW1.Q.tmp:RetailCost)
  BRW1.AddField(sto:AveragePurchaseCost,BRW1.Q.sto:AveragePurchaseCost)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Suspend,BRW1.Q.sto:Suspend)
  BRW1.AddField(sto:Accessory,BRW1.Q.sto:Accessory)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 2
  FDCB26.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB26::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB26.Q &= Queue:FileDropCombo:1
  FDCB26.AddSortOrder(man:Manufacturer_Key)
  FDCB26.AddField(man:Manufacturer,FDCB26.Q.man:Manufacturer)
  FDCB26.AddField(man:Notes,FDCB26.Q.man:Notes)
  FDCB26.AddField(man:RecordNumber,FDCB26.Q.man:RecordNumber)
  FDCB26.AddUpdateField(man:Manufacturer,Manufacturer_Temp)
  ThisWindow.AddItem(FDCB26.WindowComponent)
  FDCB26.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By &Part Number'
    ?Tab:4{PROP:TEXT} = 'By Desc&ription'
    ?Tab:5{PROP:TEXT} = 'By S&helf Location'
    ?Tab:6{PROP:TEXT} = 'By Su&pplier'
    ?Tab5{PROP:TEXT} = 'By &Manufacturer'
    ?Browse:1{PROP:FORMAT} ='86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  If glo:WebJob
      If Request = InsertRecord
          Access:STOCK.CancelAutoInc()
          Return Level:Fatal
      End !If Request = InsertRecord
      If Request = DeleteRecord
          Return Level:Fatal
      End !If Request = DeleteRecord
      Request = ViewRecord
  End !glo:WebJob
  
  If DeleteStockPart(Location_Temp,Request,brw1.q.sto:Ref_Number)
      glo:Select1 = Location_Temp
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      UpdateSTOCK
    END
    ReturnValue = GlobalResponse
  END
      glo:Select1 = ''
  End !DeleteStockPart(Location_Temp,Request,brw1.q.sto:Ref_Number)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button14
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button14, Accepted)
      If SecurityCheck('REVIEW SHELF LOCATIONS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      end
      
      if Location_Temp = MainStoreLocation() ! If location is main store disable checkbox on message
          tmp:MainStore = 1
      else
          tmp:MainStore = 0
      end
      
      tmp:ReviewShelfLocation = ReviewShelfLocationsMessage(tmp:MainStore)
      
      ! tmp:ReviewShelfLocation = 0 - Cancel
      ! tmp:ReviewShelfLocation = 1 - Ok
      ! tmp:ReviewShelfLocation = 2 - Ok + copy purchase cost from Main Store for unallocated parts
      
      if tmp:ReviewShelfLocation <> 0
      
          Prog.ProgressSetup(RECORDS(STOCK)/RECORDS(Location))
          Prog.ProgressText('Reviewing Shelf Locations')
      
          Access:Stock.ClearKey(sto:Location_Key)
          sto:Location = Location_Temp
          SET(sto:Location_Key,sto:Location_Key)
          LOOP
              IF Access:Stock.Next()
                  BREAK
              END
              IF sto:Location <> Location_Temp
                  BREAK
              END
              IF Prog.InsideLoop()
                  !Cancel Pressed!
                  BREAK
              END
      
              !Write UNALLOCATED into any parts with no matching shelf location
              Access:LocShelf.ClearKey(los:Shelf_Location_Key)
              los:Site_Location = sto:Location
              los:Shelf_Location = sto:Shelf_Location
              IF Access:LocShelf.Fetch(los:Shelf_Location_Key)
                  !Not here!
                  sto:Shelf_Location = 'UNALLOCATED'
      
              END
      
              !Update the costs of ALL UNAALLOCATED parts
      
              If sto:Shelf_Location = 'UNALLOCATED'
                  if tmp:ReviewShelfLocation = 2                  ! Copy purchase cost
      
                      Access:LOCATION.ClearKey(loc:Location_Key)
                      loc:Location = sto:Location
                      if not Access:LOCATION.Fetch(loc:Location_Key)
                          sto:Percentage_Mark_Up  = loc:OutWarrantyMarkUp                     ! Get out warranty markup
                      end
      
                      sto:PurchaseMarkUp = InWarrantyMarkup(sto:Manufacturer,sto:Location)    ! Get in warranty markup
      
                      Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)                       ! Get trade price
                      sto_ali:Location = MainStoreLocation()
                      sto_ali:Part_Number = sto:Part_Number
                      if not Access:STOCK_ALIAS.Fetch(sto_ali:Location_Key)
                          sto:AveragePurchaseCost = sto_ali:Sale_Cost
                      end
      
                      if sto:Percentage_Mark_Up <> 0
                          sto:Sale_Cost = Markups(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
                      end
      
                      if sto:RetailMarkup <> 0
                          sto:Retail_Cost = Markups(sto:Retail_Cost,sto:Purchase_Cost,sto:RetailMarkup)
                      end
      
                      if sto:PurchaseMarkUp <> 0
                          sto:Purchase_Cost = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
                      end
      
                  end
              End !If sto:Shelf_Location = 'UNALLOCATED'
              Access:Stock.TryUpdate()
      
          END
          Prog.ProgressFinish()
      
      end
      
      !Case MessageEx('This routine will check the shelf locations on all parts in this site location. Any shelf location that does not exist for this site will be changed to Unallocated.'&|
      !  '<13,10>'&|
      !  '<13,10>Do you want to continue?','ServiceBase 2000',|
      !               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !    Of 1 ! &Yes Button
      !
      !        Prog.ProgressSetup(RECORDS(STOCK)/RECORDS(Location))
      !        Prog.ProgressText('Reviewing Shelf Locations')
      !
      !        Access:Stock.ClearKey(sto:Location_Key)
      !        sto:Location = Location_Temp
      !        SET(sto:Location_Key,sto:Location_Key)
      !        LOOP
      !          IF Access:Stock.Next()
      !            BREAK
      !          END
      !          IF sto:Location <> Location_Temp
      !            BREAK
      !          END
      !          IF Prog.InsideLoop()
      !            !Cancel Pressed!
      !            BREAK
      !          END
      !          Access:LocShelf.ClearKey(los:Shelf_Location_Key)
      !          los:Site_Location = sto:Location
      !          los:Shelf_Location = sto:Shelf_Location
      !          IF Access:LocShelf.Fetch(los:Shelf_Location_Key)
      !            !Not here!
      !            sto:Shelf_Location = 'UNALLOCATED'
      !            Access:Stock.TryUpdate()
      !          END
      !        END
      !        Prog.ProgressFinish()
      !
      !    Of 2 ! &No Button
      !End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button14, Accepted)
    OF ?Button:SearchOldPartNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:SearchOldPartNumber, Accepted)
      If tmp:OldPartNumber = ''
          Cycle
      End ! If tmp:OldPartNumber = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:SearchOldPartNumber, Accepted)
    OF ?Close
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      !reset Global Variables
      RelocatedToMainStore = false
      RelocatedSite        = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    OF ?ButtonImportOrder
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonImportOrder, Accepted)
      If SecurityCheck('STOCK - ORDER STOCK')
          miss# =  Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
      Else!if x" = false
          ImportStockFile
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonImportOrder, Accepted)
    OF ?buttonSuspendParts
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendParts, Accepted)
      If (SecurityCheck('SUSPEND PARTS'))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK')
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendParts, Accepted)
    OF ?Add_Stock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
      If SecurityCheck('STOCK - ADD STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Add Stock.'&|
                    '<13,10>'&|
                    '<13,10>This part has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Add_Stock(brw1.q.sto:ref_number)
          End!If error# = 0
      
      end!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
    OF ?Deplete_Stock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
      If SecurityCheck('STOCK - DEPLETE STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case Missive('Cannot Deplete Stock.'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Deplete_Stock(brw1.q.sto:ref_number,0)
          End!If error# = 0
      
      End!!if x" = false
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
    OF ?Order_Stock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
      If SecurityCheck('STOCK - ORDER STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
      
              If (sto:Location <> MainStoreLocation())
                  If (MainStoreSuspended(sto:Part_Number))
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Missive('Cannot order! The selected part has been suspended at the Main Store.','ServiceBase',|
                                     'mstop.jpg','/&OK')
                      Of 1 ! &OK Button
                      End!Case Message
                      Cycle
                  End ! If (MainStoreSuspended(sto:Part_Number))
              End
              If sto:sundry_item = 'YES' and sto:ExchangeUnit <> 'YES'    ! L232 - allow exchange unit ordering
                  Case Missive('Cannot Order.'&|
                    '<13,10>'&|
                    '<13,10>This item has been marked as a Sundry Item.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  error# = 1
              Else
                  If sto:Suspend
                      Case Missive('This part cannot be ordered. It has been suspended.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
                  End !If sto:Suspend
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              glo:select1 = brw1.q.sto:ref_number
              glo:Select2 = ''
              If Location_Temp = 'MAIN STORE' And sto:Part_Number = 'EXCH'
                  SaveRequest#      = GlobalRequest
                  GlobalResponse    = RequestCancelled
                  GlobalRequest     = SelectRecord
                  Browse_Model_Numbers
                  If Globalresponse = RequestCompleted
                      glo:Select2 = Clip(mod:Manufacturer) & ' ' & Clip(mod:Model_Number)
                  Else
                      Error# = 1
                  End
                  GlobalRequest     = SaveRequest#
              End !If Location_Temp = 'MAIN STORE'
              If Error# = 0
                  Stock_Order
              End !If Error# = 0
              glo:select1 = ''
              glo:Select2 = ''
          End!If error# = 0
      
      End!Else!if x" = false
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
    OF ?Stock_Transfer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
      !Return Faulty Stock code moved to new button - 4003 (DBH: 04-03-2004)
      If SecurityCheck('STOCK - STOCK TRANSFER')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          glo:select1 = location_temp
          glo:select2 = brw1.q.STO:Ref_Number
          Transfer_stock
          glo:select1 = ''
          glo:select2 = ''
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupLocation
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupLocation, Accepted)
      ! Lookup button pressed. Call select procedure if required
      loc:Location = Location_Temp                    ! Move value for lookup
      IF Access:LOCATION.TryFetch(loc:Location_Key)   ! IF record not found
         loc:Location = Location_Temp                 ! Move value for lookup
         ! Based on ?Location_Temp
         SET(loc:Location_Key,loc:Location_Key)       ! Prime order for browse
         Access:LOCATION.TryNext()
      END
      GlobalRequest = SelectRecord
      PickSiteLocations                               ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?LookupLocation)                      ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?Location_Temp,loc:Location)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?LookupLocation+1)                       ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
      BRW1.ResetSort(1)
      Do refresh_browse
      RelocatedToMainStore = false    !reset variable - from now on they are looking at what they want to see.
      unhide(?Delete:3)
      unhide(?Insert:3)
      unhide(?Button_Group)
      unhide(?Button_Group2)
      unhide(?Button_Group3)
      Do ShowHideReplicate
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupLocation, Accepted)
    OF ?Location_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
      BRW1.ResetSort(1)
      Do refresh_browse
      Do ShowHideReplicate
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
    OF ?accessory_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
      Do refresh_browse
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020072'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020072'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020072'&'0')
      ***
    OF ?browse_model_stock
      ThisWindow.Update
      Browse_Stock_By_Model(Location_Temp)
      ThisWindow.Reset
    OF ?DuplicateStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DuplicateStock, Accepted)
      tmp:Overwrite = 0
      tmp:Matching = 0
      tmp:DeleteStock = 0
      free(Parts_Q)
      Error# = 0
      CLEAR(glo:Queue20)
      FREE(glo:Queue20)
      Case Missive('Do you wish to pick the locations to replicate the stock to?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Pick_Trans_Location
          Of 1 ! No Button
      End ! Case Missive
      
      
      tmp:ReviewShelfLocation = ReviewShelfLocationsMessage(0)
      
      ! tmp:ReviewShelfLocation = 0 - Cancel
      ! tmp:ReviewShelfLocation = 1 - Ok
      ! tmp:ReviewShelfLocation = 2 - Ok + copy purchase cost from Main Store for unallocated parts + zero quantity parts
      
      if tmp:ReviewShelfLocation = 0 then cycle.
      
          Case Missive('Do you wish to replicate all parts to locations with MATCHING Access Levels, or to ALL locations regardless?','ServiceBase 3g',|
                         'mquest.jpg','\Cancel|All|Matching')
              Of 3 ! Matching Button
                  tmp:Matching = 1
      
                  Case Missive('If you continue ALL stock items in this location will be replicated to all location with MATCHING Access Levels.'&|
                    '<13,10>Do you wish to OVERWRITE existing parts (Note: Quantities, and Costs will NOT be overwritten), or do you wish to ADD NEW items?','ServiceBase 3g',|
                                 'mquest.jpg','\Cancel|Add New|Overwrite')
                      Of 3 ! Overwrite Button
                          tmp:Overwrite = 1
      
                      Of 2 ! Add New Button
      
                      Of 1 ! Cancel Button
                          Error# = 1
                  End!Case MessageEx
      
                  if Error# = 0
                      Case Missive('This routine will match levels of Stock Parts against the leves of Site Locations.'&|
                        '<13,10>Any mismatched parts will be removed from the Site Location.'&|
                        '<13,10>Please ensure that levels are set correctly before you BEGIN. '&|
                        '<13,10>This routine CANNOT be reversed.','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|/Begin')
                          Of 2 ! Begin Button
                              If SecurityCheck('STOCK - MATCH LOCATIONS')
                                  Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Error# = 1
                              Else
                                  tmp:DeleteStock = 1
                              End
                          Of 1 ! Cancel Button
                              Error# = 1
                      End ! Case Missive
                  end
      
              Of 2 ! All Button
      
                  Case Missive('If you continue ALL stock items in this location will be replicated to ALL other locations.'&|
                    '<13,10>Do you wish to OVERWRITE existing parts (Note: Quantities and costs will NOT be overwritten), or do you just wish to add NEW items?','ServiceBase 3g',|
                                 'mquest.jpg','\Cancel|New|Overwrite')
                      Of 3 ! Overwrite Button
                          tmp:Overwrite = 1
                      Of 2 ! New Button
      
                      Of 1 ! Cancel Button
      
                          Error# = 1
                  End ! Case Missive
      
              Of 1 ! Cancel Button
                  Error# = 1
          End ! Case Missive
      
      If Error# = 0
          Case Missive('Click ''OK'' to begin replicating stock items.','ServiceBase 3g',|
                         'mexclam.jpg','\Cancel|/OK')
              Of 2 ! OK Button
      
                  Count# = 0
                  Setcursor(cursor:wait)
                  Save_sto_ali_ID = Access:STOCK_ALIAS.SaveFile()
                  Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                  sto_ali:Location    = Location_Temp
                  Set(sto_ali:Location_Key,sto_ali:Location_Key)
                  Loop
                      If Access:STOCK_ALIAS.NEXT()
                         Break
                      End !If
                      If sto_ali:Location    <> Location_Temp      |
                          Then Break.  ! End If
                      Count# += 1
                      If Count#> 1000
                          Count#= Records(STOCK)
                          Break
                      End !If Count#> 1000
      
                  End !Loop
                  Access:STOCK_ALIAS.RestoreFile(Save_sto_ali_ID)
      
                  Setcursor()
                  Prog.ProgressSetup(Count#)
      
                  Count# = 0
                  Count2# = 0
      
                  Save_sto_ali_ID = Access:STOCK_ALIAS.SaveFile()
                  Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                  sto_ali:Location    = Location_Temp
                  Set(sto_ali:Location_Key,sto_ali:Location_Key)
                  Loop
                      If Access:STOCK_ALIAS.NEXT()
                         Break
                      End !If
                      If sto_ali:Location    <> Location_Temp      |
                          Then Break.  ! End If
                      If Prog.InsideLoop()
                          Break
                      End !If Prog.InsideLoop()
      
                      !Copy to all locations
                      Save_loc_ID = Access:LOCATION.SaveFile()
                      Set(loc:Location_Key)
                      Loop
                          If Access:LOCATION.NEXT()
                             Break
                          End !If
                          If loc:Location = Location_Temp
                              Cycle
                          End !If loc:Location = Location_Temp
                          SORT(glo:Queue20,glo:Queue20.GLO:Pointer20)
                          IF RECORDS(glo:Queue20)
                            glo:Queue20 = loc:Location
                            GET(glo:Queue20,glo:Queue20.GLO:Pointer20)
                            IF ERROR()
                              CYCLE
                            END
                          END
                          !Do the levels match?
                          tmp:WrongSkill = 0
      
                          If tmp:Matching
                              ! A complete change of logic - if a site location has level 3 then should receive all parts so..
                              !                            - site location has level 3 - all parts
                              !                            -                         2 - parts with 1 & 2 levels
                              !                            -                         1 - parts with 1
                              !If ~(sto_ali:E1 And loc:Level1)
                              !    If ~(sto_ali:E2 and loc:Level2)
                              !        If ~(sto_ali:E3 and loc:Level3)
                              !            Cycle
                              !        End !If ~(sto_ali:E3 and loc:Level3)
                              !    End !If ~(sto_ali:E2 and loc:Level2)
                              !End !If ~(sto_ali:E1 And loc:Level1)
                              if loc:Level3 = 0
                                  if loc:Level2 = 1
                                      if sto_ali:E3 then tmp:WrongSkill = 1.
                                  else
                                      if loc:Level1 = 1
                                          if not sto_ali:E1 then tmp:WrongSkill = 1.
                                      else
                                          cycle
                                      end
                                  end
                              end
      
                          End !If tmp:Matching
      
                          if tmp:WrongSkill = 1
                              if tmp:DeleteStock = 1
                                  !Is the part already in this location?
                                  Access:STOCK.ClearKey(sto:Location_Key)
                                  sto:Location    = loc:Location
                                  sto:Part_Number = sto_ali:Part_Number
                                  If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                      if sto:Quantity_Stock <> 0
                                          clear(Parts_Q)
                                          Parts_Q.ptq:Site_Location  = sto:Location
                                          Parts_Q.ptq:Part_No        = sto:Part_Number
                                          Parts_Q.ptq:Description    = sto:Description
                                          Parts_Q.ptq:Shelf_Location = sto:Shelf_Location
                                          if sto:E1
                                              Parts_Q.ptq:Skill_Level_1  = '1'
                                          end
                                          if sto:E2
                                              Parts_Q.ptq:Skill_Level_2  = '2'
                                          end
                                          if sto:E3
                                              Parts_Q.ptq:Skill_Level_3  = '3'
                                          end
                                          Parts_Q.ptq:Quantity       = sto:Quantity_Stock
                                          add(Parts_Q,Parts_Q.ptq:Site_Location)
                                      end
                                      Relate:STOCK.Delete(0)
                                  End
      
                              end
      
                              cycle
                          end
      
                          !Is the part already in this location?
                          Access:STOCK.ClearKey(sto:Location_Key)
                          sto:Location    = loc:Location
                          sto:Part_Number = sto_ali:Part_Number
                          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                              !Found
                              !Record in Use?
                              Pointer# = Pointer(Stock)
                              Hold(Stock,1)
                              Get(Stock,Pointer#)
                              If Errorcode() = 43
                                  Cycle
                              End !If Errorcode() = 43
                              If tmp:overwrite = 0
                                  Cycle
                              End !If tmp:overwrite = 0
                              RecordNumber$               = sto:Ref_Number
                              save:RetailMarkup           = sto:RetailMarkup
                              save:AveragePurchaseCost    = sto:AveragePurchaseCost
                              save:PurchaseMarkup         = sto:PurchaseMarkUp
                              save:DateBooked             = sto:DateBooked
                              save:PurchaseCost           = sto:Purchase_Cost
                              save:SaleCost               = sto:Sale_Cost
                              save:RetailCost             = sto:Retail_Cost
                              save:AccessoryCost          = sto:AccessoryCost
                              save:PercentageMarkUp       = sto:Percentage_Mark_Up
                              save:QuantityStock          = sto:Quantity_Stock
                              save:Shelf_Location         = sto:Shelf_Location
                              save:Second_Location        = sto:Second_Location
                              save:Minimum_Level          = sto:Minimum_Level
                              save:Reorder_Level          = sto:Reorder_Level
      
                              sto:Record :=: sto_ali:Record
      
                              sto:Ref_Number              = RecordNumber$
                              sto:RetailMarkup            = save:RetailMarkup
                              sto:AveragePurchaseCost     = save:AveragePurchaseCost
                              sto:PurchaseMarkup          = save:PurchaseMarkUp
                              sto:DateBooked              = save:DateBooked
                              sto:Purchase_Cost           = save:PurchaseCost
                              sto:Sale_Cost               = save:SaleCost
                              sto:Retail_Cost             = save:RetailCost
                              sto:AccessoryCost           = save:AccessoryCost
                              sto:Percentage_Mark_up      = save:PercentageMarkUp
                              sto:Quantity_Stock          = save:QuantityStock
                              sto:Shelf_Location          = save:Shelf_Location
                              sto:Second_Location         = save:Second_Location
                              sto:Minimum_Level           = save:Minimum_Level
                              sto:Reorder_Level           = save:Reorder_Level
                              sto:Location                = loc:Location
      
                              !Write UNALLOCATED into any parts with no matching shelf location
                              Access:LocShelf.ClearKey(los:Shelf_Location_Key)
                              los:Site_Location = sto:Location
                              los:Shelf_Location = sto:Shelf_Location
                              if Access:LocShelf.Fetch(los:Shelf_Location_Key)
                                  sto:Shelf_Location = 'UNALLOCATED'
                              end
      
                              If sto:Shelf_Location = 'UNALLOCATED' And loc:Location <> MainStoreLocation()
                                  if tmp:ReviewShelfLocation = 2
                                      sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                      do Costs:RecalculateMarkup ! Recalculate costs
                                  end
                              End !If sto:Shelf_Location = 'UNALLOCATED'
                              !if tmp:ReviewShelfLocation = 2
                              !    do Costs:RecalculateMarkup ! Recalculate costs
                              !end
      
                              if tmp:ReviewShelfLocation = 2
                                  if sto:Quantity_Stock = 0
                                      sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                      do Costs:RecalculateMarkup ! Recalculate costs
                                  end
                              end
      
                              Count2# += 1
                              Prog.ProgressText('Parts Added: ' & Count# & '  Parts Updated: ' & Count2#)
      
                              If Access:STOCK.TryUpdate() = Level:Benign
                                  !Remove existing STOMODEL, and then add new one
                                  Save_stm_ID = Access:STOMODEL.SaveFile()
                                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                                  stm:Ref_Number   = sto:Ref_Number
                                  Set(stm:Mode_Number_Only_Key,stm:Mode_Number_Only_Key)
                                  Loop
                                      If Access:STOMODEL.NEXT()
                                         Break
                                      End !If
                                      If stm:Ref_Number   <> sto:Ref_Number      |
                                          Then Break.  ! End If
                                      Delete(STOMODEL)
                                  End !Loop
                                  Access:STOMODEL.RestoreFile(Save_stm_ID)
      
                                  !Added By Neil1
                                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                                  stm:Ref_Number   = 0
                                  Set(stm:Mode_Number_Only_Key,stm:Mode_Number_Only_Key)
                                  LOOP
                                    If Access:STOMODEL.NEXT()
                                         Break
                                      End !If
                                      If stm:Ref_Number   <> 0      |
                                          Then Break.  ! End If
                                      Delete(STOMODEL)
                                  End !Loop
      
                                  Do AddModelStock
                                  !Do AddStockHistory
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                       'ADD', | ! Transaction_Type
                                                       '', | ! Depatch_Note_Number
                                                       0, | ! Job_Number
                                                       0, | ! Sales_Number
                                                       0, | ! Quantity
                                                       sto:Purchase_Cost, | ! Purchase_Cost
                                                       sto:Sale_Cost, | ! Sale_Cost
                                                       sto:Retail_Cost, | ! Retail_Cost
                                                       'STOCK REPLICATED FROM: ' & Clip(Location_Temp), | ! Notes
                                                       '', | ! Information
                                                       save:AveragePurchaseCost,|
                                                       save:PurchaseCost,|
                                                       save:SaleCost,|
                                                       save:RetailCost)
                                      ! Added OK
                                  Else ! AddToStockHistory
                                      ! Error
                                  End ! AddToStockHistory
                              End !If Access:STOCK.Update() = Level:Benign
      
                          Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                              Count# += 1
                              Prog.ProgressText('Parts Added: ' & Count# & '  Parts Updated: ' & Count2#)
                              !Add a new part, but with no stock levels
                              If Access:STOCK.PrimeRecord() = Level:Benign
                                  RecordNumber$   = sto:Ref_Number
                                  sto:Record :=: sto_ali:Record
                                  sto:Ref_Number  = RecordNumber$
                                  sto:Quantity_Stock = 0
                                  sto:Location = loc:Location
                                  sto:Shelf_Location = 'UNALLOCATED'
                                  sto:Second_Location = 'UNALLOCATED'
      
                                  If sto:Shelf_Location = 'UNALLOCATED' And loc:Location <> MainStoreLocation()
                                      if tmp:ReviewShelfLocation = 2
                                          sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                          do Costs:RecalculateMarkup ! Recalculate costs
                                      end
                                  End !If sto:Shelf_Location = 'UNALLOCATED'
      
                                  if tmp:ReviewShelfLocation = 2
                                      if sto:Quantity_Stock = 0
                                          sto:AveragePurchaseCost = sto_ali:Sale_Cost
                                          do Costs:RecalculateMarkup ! Recalculate costs
                                      end
                                  end
                                  !if tmp:ReviewShelfLocation = 2
                                  !    do Costs:RecalculateMarkup ! Recalculate costs
                                  !end
      
                                  If Access:STOCK.TryInsert() = Level:Benign
                                      Do AddModelStock
                                      !Insert Successful
                                      Do AddStockHistory
      
                                  Else !If Access:STOCK.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End!If Access:STOCK.TryInsert() = Level:Benign
                              End !If Access:STOCK.PrimeRecord() = Level:Benign
                          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      
                      End !Loop
      
                      Access:LOCATION.RestoreFile(Save_loc_ID)
                  End !Loop
                  Access:STOCK_ALIAS.RestoreFile(Save_sto_ali_ID)
                  Prog.ProgressFinish()
                  Case Missive('Stock Updated.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  if records(Parts_Q)
                      SiteLocationPartLevelMatchReport(address(Parts_Q))
                      Free(Parts_Q)
                  end
              Of 1 ! Cancel Button
          End ! Case Missive
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DuplicateStock, Accepted)
    OF ?rebuild_model_stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rebuild_model_stock, Accepted)
      If SecurityCheck('STOCK - REBUILD STOCK')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
      
          Case Missive('This is a maintenance facility.'&|
            '<13,10>It is used for when a Stock Item has been removed from Stock Control, but may still be found when selecting a part for a job. '&|
            '<13,10>Only run this routine if it is required, it will take a LONG TIME!','ServiceBase 3g',|
                         'mexclam.jpg','\Cancel|/OK')
              Of 2 ! OK Button
      
                  Prog.ProgressSetup(Records(STOMODEL))
      
                  Prog.ProgressText('Rebuilding Model Stock')
      
                  Access:STOMODEL_ALIAS.RestoreFile(Save_stm_ali_ID)
                  save_stm_id = access:stomodel.savefile()
                  set(stm:model_number_key)
                  loop
                      if access:stomodel.next()
                         break
                      end !if
      
                      If Prog.InsideLoop()
                          Break
                      End !If Prog.InsideLoop()
      
                      access:stock.clearkey(sto:ref_part_description_key)
                      sto:location    = stm:location
                      sto:ref_number  = stm:ref_number
                      sto:part_number = stm:part_number
                      sto:description = stm:description
                      if access:stock.fetch(sto:ref_part_description_key)
                         Delete(stomodel)
                         Cycle
                      end
      
                      Save_stm_ali_ID = Access:STOMODEL_ALIAS.SaveFile()
                      Access:STOMODEL_ALIAS.ClearKey(stm_ali:Model_Number_Key)
                      stm_ali:Ref_Number   = stm:Ref_Number
                      stm_ali:Manufacturer    = stm:Manufacturer
                      stm_ali:Model_Number    = stm:Model_Number
                      Set(stm_ali:Model_Number_Key,stm_ali:Model_Number_Key)
                      Loop
                          If Access:STOMODEL_ALIAS.NEXT()
                             Break
                          End !If
                          If stm_ali:Ref_Number   <> stm:Ref_Number      |
                          Or stm_ali:Manufacturer <> stm:Manufacturer      |
                          Or stm_ali:Model_Number <> stm:Model_Number      |
                              Then Break.  ! End If
                          If stm_ali:RecordNumber <> stm:RecordNumber
                             Delete(STOMODEL)
                             Cycle
                          End !If stm_ali:RecordNumber <> stm:RecordNumber
                      End !Loop
                      Access:STOMODEL_ALIAS.RestoreFile(Save_stm_ali_ID)
      
                  end !loop
                  access:stomodel.restorefile(save_stm_id)
      
                  Prog.ProgressFinish()
              Of 1 ! Cancel Button
          End ! Case Missive
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rebuild_model_stock, Accepted)
    OF ?Button:SearchOldPartNumber
      ThisWindow.Update
      OldPartNumberSearch(Location_temp,tmp:OldPartNumber)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:SearchOldPartNumber, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:SearchOldPartNumber, Accepted)
    OF ?ReturnStock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnStock, Accepted)
      ! #12151 Return Stock (Bryan: 01/09/2011)
      IF (SecurityCheck('RRC/ARC RETURN STOCK'))
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      BRW1.UpdateViewRecord()
      IF (sto:Sundry_Item = 'YES')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Cannot Return.'&|
              '|'&|
              '|This item has been marked as a Sundry Item.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END ! IF (sto:Sundry_Item = 'YES')
      
      
      ReturnStockProcess()
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReturnStock, Accepted)
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?buttonWaybillConfirmation
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonWaybillConfirmation, Accepted)
      IF (glo:WebJob = 1)
          ! Show "Faulty" version
          ReturnWaybillConfirmation(1)
      ELSE
          IF (Location_Temp = MainStoreLocation())
              ReturnWaybillConfirmation(0) ! Show Main Store Return
          ELSE
              ReturnWaybillConfirmation(1) ! Show ARC "Faulty" Return
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonWaybillConfirmation, Accepted)
    OF ?buttonReturnStockTracking
      ThisWindow.Update
      BrowseReturnOrderTracking
      ThisWindow.Reset
    OF ?buttonSuspendParts
      ThisWindow.Update
      SuspendStock(Location_Temp)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendParts, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendParts, Accepted)
    OF ?buttonSuspendedStock
      ThisWindow.Update
      BrowseSuspendedStock(Location_Temp)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendedStock, Accepted)
      brw1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonSuspendedStock, Accepted)
    OF ?Button:OutstandingStockOrders
      ThisWindow.Update
      BrowseOutstandingStockOrders
      ThisWindow.Reset
    OF ?Add_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
    OF ?Deplete_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
    OF ?Order_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
      BRW1.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
    OF ?Stock_Transfer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
    OF ?Stock_History
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_History, Accepted)
      If SecurityCheck('STOCK - STOCK HISTORY')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Thiswindow.reset
          glo:select1 = brw1.q.STO:Ref_Number
          Browse_Stock_History
          glo:select1 = ''
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_History, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
    If ~glo:WebJob
        !Qty On Order
        tmp:QtyOnOrder = ''
        tmp:LastOrdered = 0                                                          !file. But check with the order
        save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
        access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
        orp:part_ref_number = brw1.q.sto:ref_number                                                    !should hopefully speed things
        set(orp:ref_number_key,orp:ref_number_key)                                              !up.
        loop
            if access:ordparts.next()
               break
            end !if
            if orp:part_ref_number <> brw1.q.sto:ref_number      |
                then break.  ! end if
            If orp:Order_Number <> ''
                access:orders.clearkey(ord:order_number_key)
                ord:order_number = orp:order_number
                if access:orders.fetch(ord:order_number_key) = Level:Benign
                    If ord:all_received = 'NO'
                        If orp:all_received = 'NO'
                            tmp:QtyOnOrder += orp:quantity
                            If ord:date > tmp:LastOrdered
                                tmp:LastOrdered = ord:date
                            End!If ord:date > tmp:LastOrdered
                        End!If orp:all_received <> 'YES'
                    End!If ord:all_received <> 'YES'
                end!if access:orders.fetch(ord:order_number_key) = Level:Benign
            End !If orp:Order_Number <> ''
        end !loop
        access:ordparts.restorefile(save_orp_id)
        Display(?tmp:QtyOnOrder)
    End !glo:WebJob
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab:3{PROP:TEXT} = 'By &Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='86L(2)|M~Description~@s30@#2#86L(2)|M~Part Number~@s30@#1#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab:4{PROP:TEXT} = 'By Desc&ription'
        OF 3
          ?Browse:1{PROP:FORMAT} ='132L(2)|M~Shelf Location~@s60@#4#86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab:5{PROP:TEXT} = 'By S&helf Location'
        OF 4
          ?Browse:1{PROP:FORMAT} ='97L(2)|M~Supplier~@s30@/#5#86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#48R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab:6{PROP:TEXT} = 'By Su&pplier'
        OF 5
          ?Browse:1{PROP:FORMAT} ='86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#'
          ?Tab5{PROP:TEXT} = 'By &Manufacturer'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Manufacturer_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Site Location Defaults
      Set(defstock)
      access:defstock.next()
      If dst:use_site_location = 'YES'
          location_temp = dst:site_location
      Else
          location_temp = ''
      End
      
      If glo:WebJob
          If COMMAND('WebAcc')
              Clarionet:Global.Param2 = Command('WebAcc')
          End !If COMMAND('WebAcc')
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = Clarionet:Global.Param2
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              Location_temp   = tra:SiteLocation
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = LevelBenign
      End !glo:WebJob
      
      !check user and access
      access:users_alias.clearkey(use_ali:password_key)
      use_ali:Password = glo:password
      if access:users_alias.fetch(use_ali:password_key)
          !user not found?
      ELSE
          if use_ali:location <> '' then
                  Location_temp = use_ali:Location
                  if Use_ali:User_Type = 'ADMINISTRATOR' and use_ali:StockFromLocationOnly = true then
                      !stop change of location
                      ?Location_Temp{prop:ReadOnly} = 1
                      ?Location_Temp{prop:Skip} = 1
                      ?LookupLocation{prop:Disable} = 1
                  END !if location limited
         END !If location not blank
      END
      
      !reset Global Variables
      RelocatedToMainStore = false        !note this is a variable global to this dll that can be changed
      RelocatedSite        = ''           !note this is a variable global to this dll that can be changed
      RelocatedMark_Up     = 0            !note this is a variable global to this dll that can be changed
      
      if glo:RelocateStore = true then
          !already sorted that we are at AA20 and useSparesRequest box is ticked
          !remember where we were ...
          RelocatedSite = glo:RelocatedFrom                !note this is a variable global to this dll that can be changed
          RelocatedMark_Up  = glo:RelocatedMark_Up
      
          Location_Temp = MainStoreLocation()
          RelocatedToMainStore = true                     !note this is a variable global to this dll that can be changed
      
          hide(?Button_Group)
          hide(?Button_Group2)
          hide(?Button_Group3)
          hide(?Delete:3)
          hide(?Insert:3)
      
      END !if this was AA20
      
      
      brw1.resetsort(1)
      Display()
      Select(?browse:1)
      
      Do ShowHideReplicate
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Manufacturer_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Manufacturer_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = accessory_temp
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Manufacturer_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = locFalse
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  If Records(Queue:Browse:1)
      Enable(?Button_Group)
  Else
      Disable(?Button_Group)
  End
  
  
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(15,Force)
  ELSE
    RETURN SELF.SetSort(16,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  shelf_location_temp = CLIP(sto:Shelf_Location) & ' / ' & CLIP(sto:Second_Location)
  !If VirtualSite(Location_Temp)
  !    ?tmp:SaleCost{prop:hide} = false
  !    ?tmp:SaleCost:Prompt{prop:Text} = 'in Warv:'              !was 'In Warr:'
  !    ?tmp:RetailCost:Prompt{prop:Text} = 'out Warv:'           !was 'Out Warr:'
  !    tmp:PurchaseCost    = sto:AveragePurchaseCost
  !    tmp:SaleCost        = sto:Purchase_Cost
  !    tmp:RetailCost      = sto:Sale_Cost
  !    ?tmp:RetailCost{prop:Hide} = False
  !    ?tmp:RetailCost:Prompt{prop:Hide} = False
  !Else !VirtualSite(Location_Temp)
  !    if RelocatedToMainStore
  !        Access:Stock_Alias.clearkey(sto_ali:Location_Key)
  !        sto_ali:Part_Number = sto:Part_Number
  !        sto_ali:Location = 'AA20 VODACOM REPAIRS ARC'
  !        if access:Stock_Alias.fetch(sto_ali:Location_Key)
  !            !hm not in AA20 stock - what should I show
  !            tmp:PurchaseCost    = sto:AveragePurchaseCost
  !            
  !            ?tmp:SaleCost:Prompt{prop:text} = 'not found'
  !            ?tmp:RetailCost:Prompt{prop:Text} = 'not found'
  !            ?tmp:RetailCost:Prompt{prop:Hide} = true
  !            ?tmp:RetailCost{prop:Hide} = true
  !            ?tmp:SaleCost{prop:hide} = true
  !
  !        ELSE
  !            ?tmp:SaleCost{prop:hide} = false
  !            ?tmp:SaleCost:Prompt{prop:Text} =   'In Warp:'      !was 'In Warr:'
  !            ?tmp:RetailCost:Prompt{prop:Text} = 'Out Warp:'     !was 'Out Warr:'
  !            tmp:PurchaseCost    = sto_ali:AveragePurchaseCost
  !            tmp:SaleCost        = sto_ali:Purchase_Cost
  !            tmp:RetailCost      = sto_ali:Sale_Cost
  !            
  !            ?tmp:RetailCost{prop:Hide} = False
  !            ?tmp:RetailCost:Prompt{prop:Hide} = False
  !
  !        END
  !
  !    ELSE
  !        ?tmp:SaleCost{prop:hide} = false
  !        ?tmp:SaleCost:Prompt{prop:Text} = 'Selling Price:'
  !        ?tmp:RetailCost:Prompt{prop:Hide} = True
  !        tmp:PurchaseCost    = sto:Purchase_Cost
  !        tmp:SaleCost        = sto:Sale_Cost
  !        ?tmp:RetailCost{prop:Hide} = True
  !    END
  !
  !End !VirtualSite(Location_Temp)
  PARENT.SetQueueRecord
  SELF.Q.shelf_location_temp = shelf_location_temp    !Assign formula result to display queue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, TakeNewSelection, ())
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection
  If VirtualSite(Location_Temp)
      ?tmp:SaleCost{prop:hide} = false
      ?tmp:SaleCost:Prompt{prop:Text}   = 'In Warr:'
      ?tmp:RetailCost:Prompt{prop:Text} = 'Out Warr:'
      tmp:PurchaseCost    = sto:AveragePurchaseCost
      tmp:SaleCost        = sto:Purchase_Cost
      tmp:RetailCost      = sto:Sale_Cost
      ?tmp:RetailCost{prop:Hide} = False
      ?tmp:RetailCost:Prompt{prop:Hide} = False
  Else !VirtualSite(Location_Temp)
      if RelocatedToMainStore then
  
          ?tmp:SaleCost{prop:hide} = false
          ?tmp:SaleCost:Prompt{prop:Text}   = 'In Warr:'      !was 'In Warr:'
          ?tmp:RetailCost:Prompt{prop:Text} = 'Out Warr:'     !was 'Out Warr:'
          tmp:PurchaseCost    = sto:Sale_Cost
          tmp:SaleCost        = sto:Sale_Cost * (1 + InWarrantyMarkup(sto:Manufacturer,RelocatedSite)/100)
          tmp:RetailCost      = sto:Sale_Cost * (1 + RelocatedMark_Up/100)
  
          ?tmp:RetailCost{prop:Hide} = False
          ?tmp:RetailCost:Prompt{prop:Hide} = False
  
      ELSE
          
          tmp:PurchaseCost                = sto:Purchase_Cost
          tmp:SaleCost                    = sto:Sale_Cost
          ?tmp:SaleCost:Prompt{prop:Text} = 'Selling Price:'
          ?tmp:SaleCost{prop:hide}            = false
          ?tmp:RetailCost{prop:Hide}          = True
          ?tmp:RetailCost:Prompt{prop:Hide}   = True
      END
  
  End !VirtualSite(Location_Temp)
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, TakeNewSelection, ())


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB26.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(26, ValidateRecord, (),BYTE)
  !if man:Notes[1:8] = 'INACTIVE' then RETURN(RECORD:FILTERED).
  !TB12488 Disable Manufacturers  J 22/06/2012
  !TB13243 - use inactive field
  if man:Inactive then RETURN(RECORD:FILTERED).
  ReturnValue = PARENT.ValidateRecord()
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(26, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
