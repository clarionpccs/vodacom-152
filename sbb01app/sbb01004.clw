

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01004.INC'),ONCE        !Local module procedure declarations
                     END


Receive_Order PROCEDURE (LONG fRecordNumber,BYTE fType) !Generated from procedure template - Window

LocalRequest         LONG
save_stm_id          USHORT,AUTO
save_ccp_id          USHORT,AUTO
sav:partnumber       STRING(30)
sav:description      STRING(30)
sav:purchase_cost    REAL
sav:sale_cost        REAL
sav:retail_cost      REAL
sav:ShelfLocation    STRING(30)
sav:SecondLocation   STRING(30)
save_ope_id          USHORT,AUTO
save_orp_ali_id      USHORT,AUTO
save_shi_id          USHORT,AUTO
average_temp         LONG
save_sto_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
tmp:CostsChanged     BYTE(0)
save_res_id          USHORT,AUTO
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
save_cwp_id          USHORT,AUTO
sale_cost_temp       REAL
pos                  STRING(255)
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
percentage_mark_up_temp REAL
parts_record_number_Temp REAL
warparts_record_number_temp REAL
retstock_record_number_temp LONG
no_of_labels_temp    REAL
retail_cost_temp     REAL
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
Average_text_temp    STRING(8)
tmp:quantitytoorder  LONG
tmp:quantityonorder  LONG
tmp:PartNumber       STRING(30)
tmp:Description      STRING(30)
tmp:SaleCost         REAL
tmp:PurchaseCost     REAL
tmp:RetailCost       REAL
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
tmp:ForeignPurchasePrice REAL
tmp:ForeignSellingPrice REAL
tmp:Information      STRING(255)
saveAveragePurchaseCost REAL
savePurchaseCost     REAL
saveSaleCost         REAL
saveRetailCost       REAL
window               WINDOW('Amend Item On Parts Order'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend / Receive Parts Order Item'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,336,310),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Item Details'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(68,182,212,60),USE(?groupPrices),HIDE
                             PROMPT('Purchase Price'),AT(68,182),USE(?tmp:PurchaseCost:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2b),AT(216,182,64,10),USE(tmp:PurchaseCost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ
                             PROMPT('Purchase Price'),AT(68,198),USE(?tmp:ForeignPurchasePrice:Prompt),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n-14.2b),AT(216,198,64,10),USE(tmp:ForeignPurchasePrice),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Purchase Price'),TIP('Purchase Price'),REQ,UPR
                             PROMPT('Selling Price'),AT(68,216),USE(?tmp:SaleCost:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2b),AT(216,216,64,10),USE(tmp:SaleCost),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                             PROMPT('Selling Price'),AT(68,232),USE(?tmp:ForeignSellingPrice:Prompt),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n-14.2b),AT(216,232,64,10),USE(tmp:ForeignSellingPrice),SKIP,HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Selling Price'),TIP('Selling Price'),UPR,READONLY
                           END
                           PROMPT('Location'),AT(68,70),USE(?STO:Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,70,124,10),USE(sto:Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Shelf Location'),AT(68,86),USE(?STO:Shelf_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,86,96,10),USE(tmp:ShelfLocation),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(252,82),USE(?LookupShelfLocation),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('2nd Location'),AT(68,102),USE(?STO:Second_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,102,124,10),USE(tmp:SecondLocation),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           BUTTON,AT(301,112),USE(?Button:ChangePartDetails),TRN,FLAT,ICON('chpartp.jpg')
                           PROMPT('Part Number'),AT(68,118),USE(?tmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,118,124,10),USE(tmp:PartNumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,READONLY
                           PROMPT('Description'),AT(68,134),USE(?tmp:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,134,124,10),USE(tmp:Description),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,READONLY
                           PROMPT('Quantity Ordered'),AT(304,138),USE(?orp:Number_Received:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(304,150,64,10),USE(orp:Quantity),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Number Received'),TIP('Number Received'),UPR
                           PROMPT('Quantity Received'),AT(68,150),USE(?Quantity_Temp:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@p<<<<<<<#p),AT(216,150,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,RANGE(0,99999999)
                           PROMPT('Supplier Invoice No'),AT(68,166),USE(?despatch_note_temp:prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,166,124,10),USE(despatch_note_temp),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Number Of Labels'),AT(68,248),USE(?no_of_labels:Prompt),TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@s6),AT(216,248,64,10),USE(no_of_labels_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,STEP(1)
                           BUTTON,AT(68,262),USE(?PreviousDespatch),SKIP,TRN,FLAT,LEFT,ICON('prevsupp.jpg')
                         END
                       END
                       SHEET,AT(408,54,208,310),USE(?Sheet5),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Details'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity In Stock'),AT(412,70),USE(?Prompt16),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(544,70),USE(sto:Quantity_Stock),RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Quantity To Order'),AT(412,86),USE(?Prompt16:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s12),AT(524,86),USE(tmp:quantitytoorder),RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Quantity On Order'),AT(412,102),USE(?Prompt16:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s12),AT(524,102),USE(tmp:quantityonorder),RIGHT,FONT(,,,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Stock Usage'),AT(412,122),USE(?Prompt19),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 7 Days'),AT(412,134),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s12),AT(524,134),USE(days_7_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 30 Days'),AT(412,146),USE(?Prompt5:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s12),AT(524,146),USE(days_30_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('31 - 60 Days'),AT(412,158),USE(?Prompt5:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s12),AT(524,158),USE(days_60_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('61 - 90 Days'),AT(412,170),USE(?Prompt5:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s12),AT(524,170),USE(days_90_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LINE,AT(516,182,67,0),USE(?Line1),COLOR(COLOR:White)
                           PROMPT('Average Daily Use'),AT(412,186),USE(?Prompt5:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(544,186),USE(Average_text_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Comments'),AT(412,200),USE(?orp:Reason:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(412,210,172,46),USE(orp:Reason),SKIP,HIDE,VSCROLL,FONT(,,,FONT:bold),COLOR(COLOR:Silver),READONLY,MSG('Reason')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_par_id   ushort,auto
save_wpr_id   ushort,auto

local       Class
Receive         Procedure(),Byte
            End ! local       Class
!Save Entry Fields Incase Of Lookup
look:tmp:ShelfLocation                Like(tmp:ShelfLocation)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
AveragePrice        Routine
DATA
SavePurchaseCost        REAL()
CODE
    glo:ErrorText = ''
    SavePurchaseCost = sto:Purchase_Cost

    sto:Purchase_Cost   = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_Stock,sto:Purchase_Cost),@n14.2)

    If glo:ErrorText <> ''
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                             'ADD', | ! Transaction_Type
                             '', | ! Depatch_Note_Number
                             0, | ! Job_Number
                             0, | ! Sales_Number
                             0, | ! Quantity
                             sto:Purchase_Cost, | ! Purchase_Cost
                             sto:Sale_Cost, | ! Sale_Cost
                             sto:Retail_Cost, | ! Retail_Cost
                             'AVERAGE COST CALCULATION', | ! Notes
                             Clip(glo:ErrorText),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
            ! Added OK
        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    End !If glo:ErrorText <> ''
    glo:ErrorText = ''
ChangePartNumber        Routine
Data
local:NewPartNumber         Like(sto:Part_Number)
local:NewDescription        Like(sto:Description)
Code
    If ChangePartNumber(tmp:PartNumber,tmp:Description,local:NewPArtNumber,local:NewDescription)
        ChangeAllPartNumbers(tmp:PartNumber,local:NewPartNumber,local:NewDescription,0,'RECEIVE STOCK ORDER (' & Clip(orp:Order_Number) & ')')

        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = orp:Part_Ref_Number
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            orp:Part_Number = sto:Part_Number
            orp:Description = sto:Description
            Access:ORDPARTS.Update()
            tmp:PartNumber = sto:Part_Number
            tmp:Description = sto:Description
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

        Display()
    Else ! If ChangePartNumber(tmp:PartNumber,tmp:Description,local:NewPArtNumber,local:NewDescription)
        Exit
    End ! If ChangePartNumber(tmp:PartNumber                ,tmp:Description,local:NewPArtNumber,local:NewDescription)

ChangeRelated       Routine
    !Update Stock Model File
!    setcursor(cursor:wait)
!    save_stm_id = access:stomodel.savefile()
!    access:stomodel.clearkey(stm:ref_part_description)
!    stm:ref_number  = sto:ref_number
!    stm:part_number = sto:part_number
!    set(stm:ref_part_description,stm:ref_part_description)
!    loop
!        if access:stomodel.next()
!           break
!        end !if
!        if stm:ref_number  <> sto:Ref_number      |
!        or stm:part_number <> sto:part_number      |
!            then break.  ! end if
!        pos = Position(stm:ref_part_description)
!        stm:part_number = tmp:partnumber
!        stm:description = tmp:description
!        access:stomodel.update()
!        Reset(stm:ref_part_description,pos)
!    end !loop
!    access:stomodel.restorefile(save_stm_id)
!    setcursor()
!
!    !Update Common Faults
!    setcursor(cursor:wait)
!    save_ccp_id = access:commoncp.savefile()
!    clear(ccp:record, -1)
!    ccp:part_number = sto:part_number
!    set(ccp:partnumberkey,ccp:partnumberkey)
!    loop
!        next(commoncp)
!        if errorcode()                 |
!           or ccp:part_number <> sto:part_number      |
!           then break.  ! end if
!        pos = Position(ccp:PartNumberKey)
!        ccp:part_number = tmp:PartNumber
!        access:commoncp.update()
!        Reset(ccp:PartNumberKey,pos)
!    end !loop
!    access:commoncp.restorefile(save_ccp_id)
!    setcursor()
!
!    setcursor(cursor:wait)
!    save_cwp_id = access:commonwp.savefile()
!    clear(cwp:record, -1)
!    cwp:part_number = sto:part_number
!    set(cwp:partnumberkey,cwp:partnumberkey)
!    loop
!        next(commonwp)
!        if errorcode()                 |
!           or cwp:part_number <> sto:part_number      |
!           then break.  ! end if
!        pos = Position(cwp:PartNumberKey)
!        cwp:Part_Number = tmp:PartNumber
!        access:commonwp.update()
!        Reset(cwp:PartNumberKey,pos)
!    end !loop
!    access:commonwp.restorefile(save_cwp_id)
!    setcursor()
Get_Record_Numbers      Routine
    setcursor(cursor:wait)
    save_par_id = access:parts.savefile()
    access:parts.clearkey(par:order_number_key)
    par:ref_number   = orp:job_number
    par:order_number = orp:order_number
    set(par:order_number_key,par:order_number_key)
    loop
        if access:parts.next()
           break
        end !if
        if par:ref_number   <> orp:job_number      |
        or par:order_number <> orp:order_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If par:date_received = '' And par:part_number = sav:PartNumber
            parts_record_number_temp = par:record_number
            Break
        End!If par:date_received = '' And par:part_number = sav:PartNumber
    end !loop
    access:parts.restorefile(save_par_id)

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:order_number_key)
    wpr:ref_number   = orp:job_number
    wpr:order_number = orp:order_number
    set(wpr:order_number_key,wpr:order_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number   <> orp:job_number      |
        or wpr:order_number <> orp:order_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If wpr:date_received = '' And wpr:part_number = sav:PartNumber
            warparts_record_number_temp = wpr:record_number
            Break
        End!If wpr:date_received = '' And wpr:part_number = sav:PartNumber
    end !loop
    access:warparts.restorefile(save_wpr_id)

    save_res_id = access:retstock.savefile()
    access:retstock.clearkey(res:order_number_key)
    res:ref_number  = orp:job_number
    res:order_number    = orp:order_number
    Set(res:order_number_key,res:order_number_key)
    Loop
        if access:retstock.next()
            break
        End
        if res:ref_number   <> orp:job_number   |
        or res:order_number <> orp:order_number |
            then break.
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If res:date_received = '' And res:part_number = sav:PartNumber
            retstock_record_number_temp = res:record_number
            Break
        End!If wpr:date_received = '' And wpr:part_number = sav:PartNumber
    End!Loop
    access:retstock.restorefile(save_res_id)

    setcursor()
Hide_Fields     Routine
    If percentage_mark_up_temp <> ''
        ?tmp:SaleCost{prop:readonly} = 1
        ?tmp:SaleCost{prop:skip} = 1
        ?tmp:ForeignSellingPrice{PROP:ReadOnly} = 1
        ?tmp:ForeignSellingPrice{PROP:Skip} = 1
        !?tmp:SaleCost{prop:color} = color:silver
        tmp:SaleCost = tmp:PurchaseCost + (tmp:PurchaseCost * (percentage_mark_up_temp/100))
    Else
        If tmp:ForeignPurchasePrice{PROP:Hide} = False
            ?tmp:SaleCost{prop:readonly} = True
            ?tmp:SaleCost{prop:skip} = True
            ?tmp:ForeignSellingPrice{PROP:ReadOnly} = False
            ?tmp:ForeignSellingPrice{PROP:Skip} = False
        Else ! If tmp:ForeignPurchasePrice{PROP:Hide} = False
            ?tmp:SaleCost{prop:readonly} = 0
            ?tmp:SaleCost{prop:skip} = 0
            ?tmp:SaleCost{prop:color} = color:white
            ?tmp:ForeignSellingPrice{PROP:ReadOnly} = True
            ?tmp:ForeignSellingPrice{PROP:Skip} = True
        End ! If tmp:ForeignPurchasePrice{PROP:Hide} = False
    End
    Display()
Replicate       Routine
    If tmp:CostsChanged = 1
        Case Missive('Do you wish to update all occurances of this part with the amended details?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                PartsChanging(sav:PartNumber,tmp:PartNumber,sav:Description,tmp:Description,sav:Sale_Cost,tmp:SaleCost,|
                                sav:Purchase_Cost,tmp:PurchaseCost,sav:Retail_Cost,tmp:RetailCost,sav:ShelfLocation,tmp:ShelfLocation,|
                                sav:SecondLocation,tmp:SecondLocation)

            Of 1 ! No Button
        End ! Case Missive

    End!If tmp:CostsChanged = 1
Receive:Later        Routine
!    Case orp:part_type
!        Of 'JOB'
!            access:parts.clearkey(par:RecordNumberKey) !Update The Part On The Job
!            par:record_number    = parts_record_number_temp
!            If access:parts.fetch(par:RecordNumberKey) = Level:Benign
!
!                par:date_received            = date_received_temp
!                par:despatch_note_number    = despatch_note_temp
!                par:quantity                = quantity_temp
!                par:PartAllocated       = 0
!                access:parts.update()
!                get(parts,0) ! Create New Part For New Order
!                if access:parts.primerecord() = Level:Benign
!                    access:parts_alias.clearkey(par_ali:RecordNumberKey)
!                    par_ali:record_number = parts_record_number_temp
!                    if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
!                        record_number$           = par:record_number
!                        par:record              :=: par_ali:record
!                        par:record_number        = record_number$
!                        par:purchase_cost        = tmp:PurchaseCost
!                        par:sale_cost            = tmp:SaleCost
!                        par:quantity             = orp:quantity - quantity_temp
!                        par:date_received        = ''
!                        par:date_ordered         = date_received_temp
!                        par:order_number         = ord:order_number
!                        par:PartAllocated       = 1
!                        if access:parts.insert()
!                           access:parts.cancelautoinc()
!                        end
!                    end!if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
!                end!if access:parts.primerecord() = Level:Benign
!
!            End!If access:parts.fetch(par:RecordNumberKey) = Level:Benign
!
!
!        Of 'WAR'
!            access:warparts.clearkey(wpr:RecordNumberKey)
!            wpr:record_number = warparts_record_number_temp
!            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
!                wpr:date_received             = date_received_temp
!                wpr:despatch_note_number     = despatch_note_temp
!                wpr:quantity                = quantity_temp
!                wpr:PartAllocated       = 0
!                access:warparts.update()
!                get(warparts,0)
!                if access:warparts.primerecord() = Level:Benign
!                    access:warparts_alias.clearkey(war_ali:RecordNumberKey)
!                    war_ali:record_number = warparts_record_number_temp
!                    if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
!                        record_number$           = wpr:record_number
!                        wpr:record              :=: war_ali:record
!                        wpr:record_number        = record_number$
!                        wpr:purchase_cost        = tmp:PurchaseCost
!                        wpr:sale_cost            = tmp:SaleCost
!                        wpr:quantity             = orp:quantity - quantity_temp
!                        wpr:date_received        = ''
!                        wpr:date_ordered         = date_received_temp
!                        wpr:order_number         = ord:order_number
!                        wpr:PartAllocated       = 1
!                        if access:warparts.insert()
!                           access:warparts.cancelautoinc()
!                        end
!                    end!if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
!                end !if access:warparts.primerecord() = Level:Benign
!
!            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
!
!
!        Of 'STO'
!            StockPart# = 1
!            Access:STOCK.Clearkey(sto:Ref_Number_Key)
!            sto:Ref_Number  = orp:Part_Ref_Number
!            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                !Found
!
!            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!                Access:STOCK.ClearKey(sto:Location_Key)
!                sto:Location    = MainStoreLocation()
!                sto:Part_Number = orp:Part_Number
!                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    !Found
!
!                Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    !Error
!                    Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
!                                   'mstop.jpg','/OK')
!                        Of 1 ! OK Button
!                    End ! Case Missive
!                    StockPart# = 0
!                End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!            If StockPart# = 1

                IF (FType = 1)
                    sto:quantity_stock += quantity_temp
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0

                    If orp:Reason <> ''
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp, | ! Quantity
                                             tmp:PurchaseCost, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason),|
                                                    saveAveragePurchaseCost,|
                                                    savePurchaseCost,|
                                                    saveSaleCost,|
                                                    saveRetailCost)
                            ! Added OK
                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    Else ! If orp:Reason <> ''
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp, | ! Quantity
                                             tmp:PurchaseCost, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                    saveAveragePurchaseCost,|
                                                    savePurchaseCost,|
                                                    saveSaleCost,|
                                                    saveRetailCost)
                            ! Added OK
                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    End ! If orp:Reason <> ''
                    Do AveragePrice
                    If sto:Percentage_Mark_Up <> 0
                        sto:Sale_Cost   = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:Percentage_Mark_Up/100))
                    End !If sto:Percentage_Mark_Up <> 0
                    If sto:RetailMarkUp <> 0
                        sto:Retail_Cost = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:RetailMarkUp/100))
                    End !If sto:RetailMarkUp <> 0
                END
                Access:STOCK.Update()
!
!            end !if access:stock.fetch(sto:ref_number_key)
!        Of 'RET'
!            orp:allocated_to_sale   = 'NO'
!
!
!    End!Case orp:part_type
Receive:Normal      Routine
!    Case orp:part_type
!        Of 'JOB'
!            access:parts.clearkey(par:RecordNumberKey) !Update The Part On The Job
!            par:record_number    = parts_record_number_temp
!            If access:parts.fetch(par:RecordNumberKey) = Level:Benign
!                par:date_received            = date_received_temp
!                par:despatch_note_number    = despatch_note_temp
!                par:quantity                = quantity_temp
!                par:PartAllocated           = 0
!                access:parts.update()
!            End!If access:parts.fetch(par:RecordNumberKey) = Level:Benign
!
!        Of 'WAR'
!            access:warparts.clearkey(wpr:RecordNumberKey)
!            wpr:record_number = warparts_record_number_temp
!            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
!                wpr:date_received = date_received_temp
!                wpr:despatch_note_number = despatch_note_temp
!                wpr:quantity            = quantity_temp
!                wpr:PartAllocated       = 0
!                access:warparts.update()
!                quantity_to_job$ = wpr:quantity
!            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
!
!            If orp:part_ref_number <> ''        !If Stock Part.. Update Stock Details
!                access:stock.clearkey(sto:ref_number_key)
!                sto:ref_number = orp:part_ref_number
!                if access:stock.fetch(sto:ref_number_key)
!                    Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
!                                   'mstop.jpg','/OK')
!                        Of 1 ! OK Button
!                    End ! Case Missive
!                Else!if access:stock.fetch(sto:ref_number_key)
!                    If sto:quantity_stock < 0
!                        sto:quantity_stock = 0
!                    End!If sto:quantity_stock < 0
!                    access:stock.update()
!                    !No Stock History Need Because Part Updated
!                End!!if access:stock.fetch(sto:ref_number_key)
!            End!If orp:part_ref_number <> ''
!
!        Of 'STO' OrOf 'RET'
!            StockPart# = 1
!            Access:STOCK.Clearkey(sto:Ref_Number_Key)
!            sto:Ref_Number  = orp:Part_Ref_Number
!            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                !Found
!
!            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!                Access:STOCK.ClearKey(sto:Location_Key)
!                sto:Location    = MainStoreLocation()
!                sto:Part_Number = orp:Part_Number
!                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    !Found
!
!                Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    !Error
!                    Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
!                                   'mstop.jpg','/OK')
!                        Of 1 ! OK Button
!                    End ! Case Missive
!                    StockPart# = 0
!                End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!            If StockPart# = 1
                IF (FType = 1)
                    sto:quantity_stock += quantity_temp
                    sto:quantity_on_order -= quantity_temp
                    If sto:quantity_on_order < 0
                        sto:quantity_on_order = 0
                    End
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0


                    If orp:Reason <> ''
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp, | ! Quantity
                                             tmp:PurchaseCOst, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason),|
                                                    saveAveragePurchaseCost,|
                                                    savePurchaseCost,|
                                                    saveSaleCost,|
                                                    saveRetailCost)
                            ! Added OK

                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    Else ! If orp:Reason <> ''
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp, | ! Quantity
                                             tmp:PurchaseCOst, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                    saveAveragePurchaseCost,|
                                                    savePurchaseCost,|
                                                    saveSaleCost,|
                                                    saveRetailCost)
                            ! Added OK

                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    End ! If orp:Reason <> ''

                    Do AveragePrice

                    If sto:Percentage_Mark_Up <> 0
                        sto:Sale_Cost   = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:Percentage_Mark_Up/100))
                    End !If sto:Percentage_Mark_Up <> 0
                    If sto:RetailMarkUp <> 0
                        sto:Retail_Cost = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:RetailMarkUp/100))
                    End !If sto:RetailMarkUp <> 0
                END
                Access:STOCK.Update()

!            end !If Stockpart# = 1
!        Of 'RET'
!            orp:allocated_to_sale   = 'NO'

!    End!Case orp:part_type
!
Receive:NoQuantity  ROUTINE
        IF (FType = 1)
            If orp:Reason <> ''
                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                    'ADD', | ! Transaction_Type
                    Despatch_Note_Temp, | ! Depatch_Note_Number
                    0, | ! Job_Number
                    0, | ! Sales_Number
                    0, | ! Quantity
                    tmp:PurchaseCost, | ! Purchase_Cost
                    tmp:SaleCost, | ! Sale_Cost
                    tmp:RetailCost, | ! Retail_Cost
                    'ZERO QTY ORDER RECEIVED', | ! Notes
                    'ORIGINAL ORDER QTY: ' & orp:Quantity & |
                    '<13,10>ORDER NUMBER: ' & Format(orp:order_number, @n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) & |
                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason),|
                    saveAveragePurchaseCost,|
                    savePurchaseCost,|
                    saveSaleCost,|
                    saveRetailCost)
                    ! Added OK
                Else ! AddToStockHistory
                    ! Error
                End ! AddToStockHistory
            Else ! If orp:Reason <> ''
                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                    'ADD', | ! Transaction_Type
                    Despatch_Note_Temp, | ! Depatch_Note_Number
                    0, | ! Job_Number
                    0, | ! Sales_Number
                    0, | ! Quantity
                    tmp:PurchaseCost, | ! Purchase_Cost
                    tmp:SaleCost, | ! Sale_Cost
                    tmp:RetailCost, | ! Retail_Cost
                    'ZERO QTY ORDER RECEIVED', | ! Notes
                    'ORIGINAL ORDER QTY: ' & orp:Quantity & |
                    '<13,10>ORDER NUMBER: ' & Format(orp:order_number, @n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) & |
                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                    saveAveragePurchaseCost,|
                    savePurchaseCost,|
                    saveSaleCost,|
                    saveRetailCost)
                    ! Added OK
                Else ! AddToStockHistory
                    ! Error
                End ! AddToStockHistory
            End ! If orp:Reason <> ''
        END ! IF (FType = 0)
        
        orp:Number_Received = 0
        orp:DespatchNoteNumber = Despatch_Note_Temp
        Access:ORDPARTS.TryUpdate()
  
Receive:Reorder     Routine
!    Case orp:part_type
!        Of 'JOB'
!            access:parts.clearkey(par:RecordNumberKey) !Update The Part On The Job
!            par:record_number    = parts_record_number_temp
!            If access:parts.fetch(par:RecordNumberKey) = Level:Benign
!                par:date_received            = date_received_temp
!                par:despatch_note_number    = despatch_note_temp
!                par:quantity                = quantity_temp
!                par:PartAllocated       = 0
!                access:parts.update()
!                get(parts,0) ! Create New Part For New Order
!                if access:parts.primerecord() = Level:Benign
!                    access:parts_alias.clearkey(par_ali:RecordNumberKey)
!                    par_ali:record_number = parts_record_number_temp
!                    if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
!                        record_number$           = par:record_number
!                        par:record              :=: par_ali:record
!                        par:record_number        = record_number$
!                        par:purchase_cost        = tmp:PurchaseCost
!                        par:sale_cost            = tmp:SaleCost
!                        par:quantity             = orp:quantity - quantity_temp
!                        par:pending_ref_number   = ope:ref_number
!                        par:date_received        = ''
!                        par:date_ordered         = ''
!                        par:order_number         = ''
!                        par:PartAllocated       = 1
!                        if access:parts.insert()
!                           access:parts.cancelautoinc()
!                        end
!                    end!if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
!                end!if access:parts.primerecord() = Level:Benign
!
!            End!If access:parts.fetch(par:RecordNumberKey) = Level:Benign
!
!        Of 'WAR'
!            access:warparts.clearkey(wpr:RecordNumberKey)
!            wpr:record_number = warparts_record_number_temp
!            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
!                wpr:date_received             = date_received_temp
!                wpr:despatch_note_number     = despatch_note_temp
!                wpr:quantity                = quantity_temp
!                wpr:PartAllocated       = 0
!                access:warparts.update()
!                get(warparts,0)
!                if access:warparts.primerecord() = Level:Benign
!                    access:warparts_alias.clearkey(war_ali:RecordNumberKey)
!                    war_ali:record_number = warparts_record_number_temp
!                    if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
!                        record_number$           = wpr:record_number
!                        wpr:record              :=: war_ali:record
!                        wpr:record_number        = record_number$
!                        wpr:purchase_cost        = tmp:PurchaseCost
!                        wpr:sale_cost            = tmp:SaleCost
!                        wpr:quantity             = orp:quantity - quantity_temp
!                        wpr:pending_ref_number   = ope:ref_number
!                        wpr:date_received        = ''
!                        wpr:date_ordered         = ''
!                        wpr:order_number         = ''
!                        wpr:PartAllocated       = 1
!                        if access:warparts.insert()
!                           access:warparts.cancelautoinc()
!                        end
!                    end!if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
!                end !if access:warparts.primerecord() = Level:Benign
!
!            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
!
!
!         Of 'STO'
!            StockPart# = 1
!            Access:STOCK.Clearkey(sto:Ref_Number_Key)
!            sto:Ref_Number  = orp:Part_Ref_Number
!            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                !Found
!
!            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!                Access:STOCK.ClearKey(sto:Location_Key)
!                sto:Location    = MainStoreLocation()
!                sto:Part_Number = orp:Part_Number
!                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    !Found
!
!                Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    !Error
!                    Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
!                                   'mstop.jpg','/OK')
!                        Of 1 ! OK Button
!                    End ! Case Missive
!                    StockPart# = 0
!                End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!            If StockPart# = 1
             IF (FType = 1)
                If sto:quantity_on_order < 0
                    sto:quantity_on_order = 0
                End
                If sto:quantity_stock < 0
                    sto:quantity_stock = 0
                End!If sto:quantity_stock < 0

                If orp:Reason <> ''
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'ADD', | ! Transaction_Type
                                         Despatch_Note_Temp, | ! Depatch_Note_Number
                                         0, | ! Job_Number
                                         0, | ! Sales_Number
                                         Quantity_Temp, | ! Quantity
                                         tmp:PurchaseCost, | ! Purchase_Cost
                                         tmp:SaleCost, | ! Sale_Cost
                                         tmp:RetailCost, | ! Retail_Cost
                                         'STOCK ADDED FROM ORDER', | ! Notes
                                         'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                Else ! If orp:Reason <> ''
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'ADD', | ! Transaction_Type
                                         Despatch_Note_Temp, | ! Depatch_Note_Number
                                         0, | ! Job_Number
                                         0, | ! Sales_Number
                                         Quantity_Temp, | ! Quantity
                                         tmp:PurchaseCost, | ! Purchase_Cost
                                         tmp:SaleCost, | ! Sale_Cost
                                         tmp:RetailCost, | ! Retail_Cost
                                         'STOCK ADDED FROM ORDER', | ! Notes
                                         'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                End ! If orp:Reason <> ''

                Do AveragePrice

                If sto:Percentage_Mark_Up <> 0
                    sto:Sale_Cost   = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:Percentage_Mark_Up/100))
                End !If sto:Percentage_Mark_Up <> 0
                If sto:RetailMarkUp <> 0
                    sto:Retail_Cost = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:RetailMarkUp/100))
                End !If sto:RetailMarkUp <> 0
            END
            Access:STOCK.Update()
!
!            end !if access:stock.fetch(sto:ref_number_key)
!        Of 'RET'
!
!            orp:allocated_to_sale   = 'NO'
!
!    End!Case orp:part_type
ShowForeignCurrency         Routine
    ?tmp:ForeignPurchasePrice:Prompt{prop:Hide} = True
    ?tmp:ForeignPurchasePrice{prop:Hide} = True
    ?tmp:ForeignSellingPrice:Prompt{prop:Hide} = True
    ?tmp:ForeignSellingPrice{prop:Hide} = True
    ?tmp:PurchaseCost{PROP:Skip} = False
    ?tmp:PurchaseCost{PROP:ReadOnly} = False

    Access:ORDERS.ClearKey(ord:Order_Number_Key)
    ord:Order_Number = orp:Order_Number
    If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
        !Found

        If orp:OrderedCurrency <> ''
            ! Use the current details recorded at point of order - TrkBs: 6264 (DBH: 07-09-2005)

            ! cost will be entered into the foreign field - TrkBs: 5110 (DBH: 16-06-2005)
            If SecurityCheck('AMEND ORDER RECEIPT COSTS')
                ?tmp:ForeignPurchasePrice{prop:ReadOnly} = True
            Else ! If SecurityCheck('AMEND ORDER RECEIPT COSTS')
                ?tmp:ForeignPurchasePrice{prop:ReadOnly} = False
            End ! If SecurityCheck('AMEND ORDER RECEIPT COSTS')

            ?tmp:ForeignPurchasePrice:Prompt{prop:Hide} = False
            ?tmp:ForeignPurchasePrice{prop:Hide} = False
            ?tmp:PurchaseCost{PROP:Skip} = True
            ?tmp:PurchaseCost{PROP:ReadOnly} = True

            ! Found
! Changing (DBH 11/25/2005) #6789 - Use today's rate, instead of the ordered rate
!             Case orp:OrderedDivideMultiply
!             Of '*'
!                 tmp:ForeignPurchasePrice = tmp:PurchaseCost / orp:OrderedDailyRate
!             Of '/'
!                 tmp:ForeignPurchasePrice = tmp:PurchaseCost * orp:OrderedDailyRate
!             End ! Case orp:OrderedDivideMultiply
! to (DBH 11/25/2005) #6789
            CurrCode" = orp:OrderedCurrency
            GetCurrencyConversion(ord:Supplier,tmp:PurchaseCost,CurrCode",Amount$)
            tmp:ForeignPurchasePrice = Amount$
            ! End (DBH 11/25/2005) #6789

            ?tmp:ForeignPurchasePrice:Prompt{prop:Text} = 'Purchase Price (' & Clip(orp:OrderedCurrency) & ')'

            ?tmp:ForeignSellingPrice:Prompt{prop:Hide} = False
            ?tmp:ForeignSellingPrice{prop:Hide} = False
            ?tmp:ForeignSellingPrice:Prompt{prop:Text} = 'Selling Price (' & Clip(orp:OrderedCurrency) & ')'

! Changing (DBH 11/25/2005) #6789 - Use today's rate, instead of the ordered rate
!             Case orp:OrderedDivideMultiply
!             Of '*'
!                 tmp:ForeignSellingPrice = tmp:SaleCost / orp:OrderedDailyRate
!             Of '/'
!                 tmp:ForeignSellingPrice = tmp:SaleCost * orp:OrderedDailyRate
!             End ! Case orp:OrderedDivideMultiply
! to (DBH 11/25/2005) #6789
            CurrCode" = orp:OrderedCurrency
            GetCurrencyConversion(ord:Supplier,tmp:SaleCost,CurrCode",Amount$)
            tmp:ForeignSellingPrice = Amount$
            ! End (DBH 11/25/2005) #6789

        End ! If orp:OrderedCurrency <> ''

    Else !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
        !Error
    End !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign

    Bryan.CompFieldColour()
    Display()

Split_Receive       Routine
        Case orp:part_type
            Of 'JOB'
                access:parts.clearkey(par:RecordNumberKey)    !Update Job Part
                par:record_number = parts_record_number_temp
                if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                    par:date_received = date_received_temp
                    par:despatch_note_number = despatch_note_temp
                    par:quantity = orp:quantity
                    par:PartAllocated       = 0
                    access:parts.update()
                end!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                If orp:part_ref_number <> ''
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number = orp:part_ref_number
                    if access:stock.fetch(sto:ref_number_key)
                        Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!if access:stock.fetch(sto:ref_number_key)
                        sto:quantity_stock += (quantity_temp - orp:quantity)
                        sto:quantity_on_order -= orp:quantity
                        If sto:quantity_on_order < 0
                            sto:quantity_on_order = 0
                        End
                        If sto:quantity_stock < 0
                            sto:quantity_stock = 0
                        End!If sto:quantity_stock < 0
                        access:stock.update()
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp - orp:Quantity, | ! Quantity
                                             tmp:PurchaseCost, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'SPLIT ITEMS, STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                        '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                            ! Added OK

                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    end !if access:stock.fetch(sto:ref_number_key)
                Else!If orp:part_ref_number <> ''                                            !Add external part
                    glo:select1 = ''
                    glo:select2 = ''
                    glo:select3 = ''
                    Pick_Locations
                    If glo:select1 = ''
                        Case Missive('The Excess Parts will NOT be added to stock.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!If glo:select1 = ''
                        do_delete# = 1
                        get(stock,0)
                        if access:stock.primerecord() = level:benign
                            sto:part_number            = par:part_number
                            sto:description            = par:description
                            sto:supplier               = par:supplier
                            sto:purchase_cost          = par:purchase_cost
                            sto:sale_cost              = par:sale_cost
                            sto:shelf_location         = glo:select2
                            sto:manufacturer           = job:manufacturer
                            sto:location               = glo:select1
                            sto:second_location        = glo:select3
                            sto:quantity_stock         = (quantity_temp - orp:quantity)
                            access:stock.insert()
                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'ADD', | ! Transaction_Type
                                                 par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                 job:Ref_Number, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 Quantity_Temp - orp:Quantity, | ! Quantity
                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                 par:Sale_Cost, | ! Sale_Cost
                                                 par:Retail_Cost, | ! Retail_Cost
                                                 'SPLIT ITEMS, STOCK ADDED FROM ORDER', | ! Notes
                                                 'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                            '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                                ! Added OK
                            Else ! AddToStockHistory
                                ! Error
                            End ! AddToStockHistory
                        End!if access:stock.primerecord() = level:benign
                    End!If glo:select1 = ''
                End!If orp:part_ref_number <> ''


            Of 'WAR'
                access:warparts.clearkey(wpr:RecordNumberKey)
                wpr:record_number = warparts_record_number_temp
                if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                    wpr:date_received = date_received_temp
                    wpr:despatch_note_number = despatch_note_temp
                    wpr:quantity = orp:quantity
                    wpr:PartAllocated       = 0
                    access:warparts.update()
                end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                If orp:part_ref_number <> ''
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number = orp:part_ref_number
                    if access:stock.fetch(sto:ref_number_key)
                        Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!if access:stock.fetch(sto:ref_number_key)
                        sto:quantity_stock += (quantity_temp - orp:quantity)
                        sto:quantity_on_order -= orp:quantity
                        If sto:quantity_on_order < 0
                            sto:quantity_on_order = 0
                        End
                        If sto:quantity_stock < 0
                            sto:quantity_stock = 0
                        End!If sto:quantity_stock < 0
                        access:stock.update()
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp - orp:Quantity, | ! Quantity
                                             tmp:PurchaseCost, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'SPLIT ITEMS, STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                        '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                            ! Added OK
                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    end !if access:stock.fetch(sto:ref_number_key)
                Else!If orp:part_ref_number <> ''
                    glo:select1 = ''
                    glo:select2 = ''
                    glo:select3 = ''
                    Pick_Locations
                    If glo:select1 = ''
                        Case Missive('The Excess Parts will NOT be added to stock.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!If glo:select1 = ''
                        do_delete# = 1
                        get(stock,0)
                        if access:stock.primerecord() = level:benign
                            sto:part_number            = wpr:part_number
                            sto:description            = wpr:description
                            sto:supplier               = wpr:supplier
                            sto:purchase_cost          = wpr:purchase_cost
                            sto:sale_cost              = wpr:sale_cost
                            sto:shelf_location         = glo:select2
                            sto:manufacturer           = job:manufacturer
                            sto:location               = glo:select1
                            sto:second_location        = glo:select3
                            sto:quantity_stock         = (quantity_temp - orp:quantity)
                            access:stock.insert()
                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'ADD', | ! Transaction_Type
                                                 wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                 job:Ref_Number, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 Quantity_Temp - orp:Quantity, | ! Quantity
                                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                                 wpr:Sale_Cost, | ! Sale_Cost
                                                 wpr:Retail_Cost, | ! Retail_Cost
                                                 'SPLIT ITEMS, STOCK ADDED FROM ORDER', | ! Notes
                                                 'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                            '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                                ! Added OK
                            Else ! AddToStockHistory
                                ! Error
                            End ! AddToStockHistory
                        End!if access:stock.primerecord() = level:benign
                    End!If glo:select1 = ''

                End!If orp:part_ref_number <> ''

            Of 'STO'
                StockPart# = 1
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = orp:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found

                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Access:STOCK.ClearKey(sto:Location_Key)
                    sto:Location    = MainStoreLocation()
                    sto:Part_Number = orp:Part_Number
                    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        !Found

                    Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        !Error
                        Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        StockPart# = 0
                    End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                If StockPart# = 1


                    sto:quantity_stock += quantity_temp
                    sto:quantity_on_order -= orp:quantity
                    If sto:quantity_on_order < 0
                        sto:quantity_on_order = 0
                    End
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0

                    If orp:Reason <> ''
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp, | ! Quantity
                                             tmp:PurchaseCost, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                            ! Added OK
                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    Else ! If orp:Reason <> ''
                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'ADD', | ! Transaction_Type
                                             Despatch_Note_Temp, | ! Depatch_Note_Number
                                             0, | ! Job_Number
                                             0, | ! Sales_Number
                                             Quantity_Temp, | ! Quantity
                                             tmp:PurchaseCost, | ! Purchase_Cost
                                             tmp:SaleCost, | ! Sale_Cost
                                             tmp:RetailCost, | ! Retail_Cost
                                             'STOCK ADDED FROM ORDER', | ! Notes
                                             'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                            ! Added OK
                        Else ! AddToStockHistory
                            ! Error
                        End ! AddToStockHistory
                    End ! If orp:Reason <> ''

                    Do AveragePrice

                    If sto:Percentage_Mark_Up <> 0
                        sto:Sale_Cost   = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:Percentage_Mark_Up/100))
                    End !If sto:Percentage_Mark_Up <> 0
                    If sto:RetailMarkUp <> 0
                        sto:Retail_Cost = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:RetailMarkUp/100))
                    End !If sto:RetailMarkUp <> 0

                    Access:STOCK.Update()

                end !if access:stock.fetch(sto:ref_number_key)

            Of 'RET'
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = orp:part_ref_number
                if access:stock.fetch(sto:ref_number_key)
                    Case Missive('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                Else!if access:stock.fetch(sto:ref_number_key)
                    sto:quantity_stock += (quantity_temp - orp:quantity)
                    sto:quantity_on_order -= orp:quantity
                    If sto:quantity_on_order < 0
                        sto:quantity_on_order = 0
                    End
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0
                    access:stock.update()
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'ADD', | ! Transaction_Type
                                         Despatch_Note_Temp, | ! Depatch_Note_Number
                                         0, | ! Job_Number
                                         0, | ! Sales_Number
                                         Quantity_Temp - orp:Quantity, | ! Quantity
                                         tmp:PurchaseCost, | ! Purchase_Cost
                                         tmp:SaleCost, | ! Sale_Cost
                                         tmp:RetailCost, | ! Retail_Cost
                                         'SPLIT ITEMS, STOCK ADDED FROM ORDER', | ! Notes
                                         'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp),|
                                                saveAveragePurchaseCost,|
                                                savePurchaseCost,|
                                                saveSaleCost,|
                                                saveRetailCost)
                        ! Added OK
                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                end !if access:stock.fetch(sto:ref_number_key)
                orp:allocated_to_sale   = 'NO'

        End!Case orp:part_type
WorkOutForeignCurrency      Routine
    Access:ORDERS.Clearkey(ord:Order_Number_Key)
    ord:Order_Number    = orp:Order_Number
    If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
        ! Found
        GetCurrencyConversionReverse(ord:Supplier,tmp:ForeignPurchasePrice,CurrCode",Amount$)
        If CurrCode" <> ''
            tmp:PurchaseCost = Amount$
        End ! If CurrCode" <> ''e

        GetCurrencyConversionReverse(ord:Supplier,tmp:ForeignSellingPrice,CurrCode",Amount$)
        If CurrCode" <> ''
            tmp:SaleCost = Amount$
        End ! If CurrCode" <> ''
    Else ! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
        ! Error
    End ! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign

    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020091'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Receive_Order')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:COMMONCP.Open
  Relate:CURRENCY.Open
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:USERS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:RETSTOCK.UseFile
  Access:STOMODEL.UseFile
  Access:COMMONWP.UseFile
  Access:LOCSHELF.UseFile
  Access:SUPPLIER.UseFile
  SELF.FilesOpened = True
  ! Open Files
  Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
  orp:Record_Number = fRecordNumber
  IF (Access:ORDPARTS.TryFetch(orp:Record_Number_Key))
      Beep(Beep:SystemHand)  ;  Yield()
      Case Missive('An error occurred retrieving the part details.','ServiceBase',|
                     'mstop.jpg','/&OK') 
      Of 1 ! &OK Button
      End!Case Message
      Return RequestCancelled
  END
  
  
  Access:ORDERS.Clearkey(ord:Order_Number_Key)
  ord:Order_Number = orp:Order_Number
  IF (Access:ORDERS.TryFetch(ord:Order_Number_Key))
      Beep(Beep:SystemHand)  ;  Yield()
      Case Missive('An error occurred retrieving the part details.','ServiceBase',|
                     'mstop.jpg','/&OK') 
      Of 1 ! &OK Button
      End!Case Message
      Return RequestCancelled
  END
  
  Access:STOCK.Clearkey(sto:Ref_Number_Key)
  sto:Ref_Number = orp:Part_Ref_Number
  IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
      Beep(Beep:SystemHand)  ;  Yield()
      Case Missive('An error occurred retrieving the stock part details.','ServiceBase',|
                     'mstop.jpg','/&OK') 
      Of 1 ! &OK Button
      End!Case Message
      Return RequestCancelled
  END
  ! Work out stock usage figures
  StockUsage(orp:Part_Ref_Number,Days_7_Temp,Days_30_Temp,Days_60_Temp,Days_90_Temp,Average_Temp)
  
  If average_temp < 1
      average_text_temp = '< 1'
  Else!If average_temp < 1
      average_text_temp = Int(average_temp)
  End!If average_temp < 1
  tmp:quantityonorder = 0
  tmp:quantitytoorder = 0
  
  setcursor(cursor:wait)                                                                  !file. And work out the quantity
  save_ope_id = access:ordpend.savefile()                                                 !awaiting order
  access:ordpend.clearkey(ope:part_ref_number_key)
  ope:part_ref_number =  orp:Part_Ref_Number
  set(ope:part_ref_number_key,ope:part_ref_number_key)
  loop
      if access:ordpend.next()
         break
      end !if
      if ope:part_ref_number <> orp:Part_Ref_Number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      tmp:quantitytoorder += ope:quantity
  end !loop
  access:ordpend.restorefile(save_ope_id)
  setcursor()
  
  setcursor(cursor:wait)                                                                  !file. But check with the order
  save_orp_ali_id = access:ordparts_alias.savefile()                                                !file first to see if the whole
  access:ordparts_alias.clearkey(orp_ali:ref_number_key)                                            !order has been received. This
  orp_ali:part_ref_number = orp:Part_Ref_Number                                                    !should hopefully speed things
  set(orp_ali:ref_number_key,orp_ali:ref_number_key)                                              !up.
  loop
      if access:ordparts_alias.next()
         break
      end !if
      if orp_ali:part_ref_number <> orp:Part_Ref_Number      |
          then break.  ! end if
      access:orders.clearkey(ord:order_number_key)
      ord:order_number = orp_ali:order_number
      if access:orders.fetch(ord:order_number_key) = Level:Benign
          If ord:all_received <> 'YES'
              If orp_ali:all_received <> 'YES'
                  tmp:quantityonorder += orp_ali:quantity
              End!If orp:all_received <> 'YES'
          End!If ord:all_received <> 'YES'
      end!if access:orders.fetch(ord:order_number_key) = Level:Benign
  end !loop
  access:ordparts_alias.restorefile(save_orp_ali_id)
  setcursor()
  
  !Do not show costs for exchange orders -  (DBH: 10-01-2004)
  IF Sub(tmp:PartNumber,1,4) = 'EXCH' And Instring('EXCH',tmp:Description,1,1)
      ?tmp:SaleCost{prop:Hide} = 1
      ?tmp:PurchaseCost{prop:Hide} = 1
      ?tmp:SaleCost:Prompt{prop:Hide} = 1
      ?tmp:PurchaseCost:Prompt{prop:Hide} = 1
  End !Sub(ope:Part_Number,1,4) = 'EXCH' And Instring('EXCH',ope:Description,1,1)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !All Received
  If orp:all_received = 'YES'  OR (fType = 0 And orp:Date_Received <> '')
      Hide(?OKbutton)
      Disable(?despatch_note_temp)
      Disable(?despatch_note_temp:prompt)
      HIDE(?PreviousDespatch)
      ?orp:Quantity{prop:Hide} = False
      ?orp:Number_Received:Prompt{prop:Hide} = False
      !?Quantity_Temp:Prompt{prop:Text} = 'Quantity Ordered'
      ! Inserting (DBH 22/08/2006) # 7963 - Show the costs when viewing the part
      !Quantity_Temp = orp:Quantity
      Quantity_Temp = orp:Number_Received ! #12325 Show no. received to cope with zero quantity (Bryan: 07/10/2011)
      tmp:PurchaseCost = orp:Purchase_Cost
      ! End (DBH 22/08/2006) #7963
  Else
      orp:date_received = Today()
      orp:TimeReceived = CLOCK()
      Quantity_Temp = 0
      tmp:PurchaseCost = orp:Purchase_Cost
      IF (FType = 1)
          ! Inserting (DBH 23/08/2006) # 7963 - If the user doesn't have access to enter the costs, fill them them.
          If orp:OrderedCurrency <> ''
              If SecurityCheck('AMEND ORDER RECEIPT COSTS')
!                  Quantity_Temp = orp:Quantity
                  tmp:PurchaseCost = orp:Purchase_Cost
              Else ! If SecurityCheck('AMEND ORDER RECEIPT COSTS')
!                  Quantity_Temp = 0
                  tmp:PurchaseCost = 0
              End ! If SecurityCheck('AMEND ORDER RECEIPT COSTS')
          Else ! If orp:OrderedCurrency <> ''
!              Quantity_Temp = 0
              tmp:PurchaseCost = 0
          End ! If orp:OrderedCurrency <> ''
          ! End (DBH 23/08/2006) #7963

          orp:DatePriceCaptured = TODAY()
          orp:TimePriceCaptured = CLOCK()
          
          ?orp:Quantity{prop:Hide} = False
          ?orp:Number_Received:Prompt{prop:Hide} = False
          !?Quantity_Temp:Prompt{prop:Text} = 'Quantity Ordered'
          ! Inserting (DBH 22/08/2006) # 7963 - Show the costs when viewing the part
          !Quantity_Temp = orp:Quantity
          Quantity_Temp = orp:Number_Received ! #12325 Show number received, this will then cope with zero receipts. (Bryan: 07/10/2011)
          ?despatch_note_temp{prop:ReadOnly} = 1
          ?orp:Quantity{prop:ReadOnly} = 1
          ?Quantity_Temp{prop:Disable} = 1
          HIDE(?PreviousDespatch)
      END
  End
  Display()
  
  If orp:Reason <> ''
      ?orp:Reason{prop:Hide} = 0
      ?orp:Reason:Prompt{prop:Hide} = 0
  End !orp:Reason <> ''
  If orp:Part_Type = 'STO'
      ?tmp:ShelfLocation{prop:Skip} = 0
      ?tmp:ShelfLocation{prop:ReadOnly} = 0
      ?tmp:SecondLocation{prop:Skip} = 0
      ?tmp:SecondLocation{prop:ReadOnly} = 0
      ?LookupShelfLocation{prop:Hide} = 0
  Else !orp:Part_Type = 'STO'
      ?tmp:ShelfLocation{prop:Skip} = 1
      ?tmp:ShelfLocation{prop:ReadOnly} = 1
      ?tmp:SecondLocation{prop:Skip} = 1
      ?tmp:SecondLocation{prop:ReadOnly} = 1
      ?LookupShelfLocation{prop:Hide} = 1
  End !orp:Part_Type = 'STO'
  !ThisMakeOver.SetWindow(Win:FORM)
  
  ! Deleting (DBH 22/08/2006) # 7963 - Force user to enter quantity and price
  !quantity_temp       = ORP:Quantity
  !tmp:PurchaseCost  = ORP:Purchase_Cost
  ! End (DBH 22/08/2006) #7963
  date_received_temp  = orp:date_received
  
  tmp:SaleCost      = ORP:Sale_Cost
  tmp:RetailCost    = orp:retail_cost
  tmp:partnumber      = orp:part_number
  tmp:description     = orp:description
  
  despatch_note_temp  = orp:DespatchNoteNumber
  
  !Get the Part's Details from stock, if the ref number has been saved.
  !If not, use the main store location.
  saveAveragePurchaseCost = sto:AveragePurchaseCost
  savePurchaseCost = sto:Purchase_Cost
  saveSaleCost = sto:Sale_Cost
  saveRetailCost = sto:Retail_Cost
  
  tmp:ShelfLocation   = sto:Shelf_Location
  tmp:SecondLocation  = sto:Second_Location
  If sto:Percentage_Mark_Up <> ''
  !        ?tmp:SaleCost{prop:Disable} = 1
  End !If sto:Percentage_Mark_Up <> ''
  
  sav:ShelfLocation   = tmp:ShelfLocation
  sav:SecondLocation  = tmp:SecondLocation
  sav:partnumber      = tmp:PartNumber
  sav:description     = tmp:Description
  sav:purchase_cost   = tmp:PurchaseCost
  sav:retail_cost     = tmp:RetailCost
  sav:sale_cost       = tmp:SaleCost
  ! Inserting (DBH 13/08/2007) # 9117 - Only show part number if user's got access
  If SecurityCheck('STOCK - CHANGE PART DETAILS')
      ?Button:ChangePartDetails{prop:Hide} = 1
  End ! If SecurityCheck('STOCK - CHANGE PART NUMBER')
  ! End (DBH 13/08/2007) #9117
  !Labels
  set(defaults)
  access:defaults.next()
  If def:receive_stock_label = 'YES'
      Enable(?no_of_labels_temp)
      no_of_labels_temp = 1
  Else!If def:stock_label = 'YES'
      Disable(?no_of_labels_temp)
      no_of_labels_temp = 0
  End!If def:stock_label = 'YES'
  
  Case fType
  OF 0 ! Receive Part
  Of 1 ! Capture Pricies
      ?groupPrices{prop:Hide} = 0
      Do ShowForeignCurrency
  END
  ! Save Window Name
   AddToLog('Window','Open','Receive_Order')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:ShelfLocation{Prop:Tip} AND ~?LookupShelfLocation{Prop:Tip}
     ?LookupShelfLocation{Prop:Tip} = 'Select ' & ?tmp:ShelfLocation{Prop:Tip}
  END
  IF ?tmp:ShelfLocation{Prop:Msg} AND ~?LookupShelfLocation{Prop:Msg}
     ?LookupShelfLocation{Prop:Msg} = 'Select ' & ?tmp:ShelfLocation{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:COMMONCP.Close
    Relate:CURRENCY.Close
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Receive_Order')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseShelfLocations
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
! Deleting (DBH 14/08/2007) # 9117 - Tidy up code
!    If ?tmp:ForeignPurchasePrice{prop:Hide} = False
!        Qty$ = tmp:ForeignPurchasePrice
!    Else ! If ?tmp:ForeignPurchaseCost{prop:Hide} = False
!        Qty$ = tmp:PurchaseCost
!    End ! If ?tmp:ForeignPurchaseCost{prop:Hide} = False
!    Case Missive('Quantity: ' & Quantity_Temp & |
!      '|Purchase Cost: ' & Format(Qty$,@n14.2) & '.'&|
!      '||Is this correct?','ServiceBase 3g',|
!                   'mexclam.jpg','\No|/Yes')
!        Of 2 ! Yes Button
!        Of 1 ! No Button
!          Cycle
!    End ! Case Missive
!
!    ! Do not allow to received more than was ordered - TrkBs: 6264 (DBH: 07-09-2005)
!    If quantity_temp > orp:quantity
!        Case Missive('The quantity you have receipted is higher than what was ordered.' &    |
!                     '|Please re-enter the quantity you wish to receipt.', 'ServiceBase 3g', |
!                     'mstop.jpg', '/OK')
!        Of 1 ! OK Button
!        End ! Case Missive
!! Changing (DBH 22/08/2006) # 7963 - Black the quantity if wrong
!!          Quantity_Temp = orp:Quantity
!! to (DBH 22/08/2006) # 7963
!      Quantity_Temp = 0
!! End (DBH 22/08/2006) #7963
!        Select(Quantity_Temp)
!        Display()
!        Cycle
!    End ! If quantity_temp > orp:quantity
!    ! Despatch Note Number is required as the Nat Store Invoice No - TrkBs: 6254 (DBH: 07-09-2005)
!    If despatch_note_temp = ''
!        Case Missive('You must enter a Supplier Invoice Number.', 'ServiceBase 3g', |
!                     'mstop.jpg', '/OK')
!        Of 1 ! OK Button
!        End ! Case Missive
!        Select(?despatch_note_temp)
!        Cycle
!    End ! If despatch_note_temp = ''
!
!    ! Fill in the currency details - TrkBs: 5110 (DBH: 18-05-2005)
!    ! Inserting (DBH 31/01/2006) #7106 - Do not lookup the currency details if none were set at point of order
!    Access:ORDERS.ClearKey(ord:Order_Number_Key)
!    ord:Order_Number = orp:Order_Number
!    If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!
!        If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
!        ! End (DBH 31/01/2006) #7106
!            ! Found
!            Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
!            sup:Company_Name = ord:Supplier
!            If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!                ! Found
!                If sup:UseForeignCurrency
!                    Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
!                    cur:CurrencyCode = sup:CurrencyCode
!                    If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
!                        ! Found
!                        orp:ReceivedCurrency       = cur:CurrencyCode
!                        orp:ReceivedDailyRate      = cur:DailyRate
!                        orp:ReceivedDivideMultiply = cur:DivideMultiply
!                    Else ! If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
!                    ! Error
!                    End ! If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
!                End ! If sup:UseForeignCurrency
!            Else ! If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!            ! Error
!            End ! If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!        Else ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
!            orp:ReceivedCurrency       = ''
!            orp:ReceivedDailyRate      = ''
!            orp:ReceivedDivideMultiply = ''
!        End ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
!
!    Else ! If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!        ! Error
!    End ! If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!    ! End   - Fill in the currency details - TrkBs: 5110 (DBH: 18-05-2005)
!
!    If Quantity_Temp = 0
!! Deleting (DBH 22/08/2006) # 7963 - Ask for confirmation first.
!!          Case Missive('You have selected to receive ZERO items.' &|
!!                       '<13,10>' &|
!!                       '<13,10>Are you sure?', 'ServiceBase 3g', |
!!                       'mquest.jpg', '\No|/Yes')
!!          Of 2 ! Yes Button
!! End (DBH 22/08/2006) #7963
!            Case orp:Part_Type
!            Of 'STO'
!                Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                sto:Ref_Number  = orp:Part_Ref_Number
!                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    ! Found
!                    If orp:Reason <> ''
!                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
!                                           'ADD', | ! Transaction_Type
!                                           Despatch_Note_Temp, | ! Depatch_Note_Number
!                                           0, | ! Job_Number
!                                           0, | ! Sales_Number
!                                           0, | ! Quantity
!                                           tmp:PurchaseCost, | ! Purchase_Cost
!                                           tmp:SaleCost, | ! Sale_Cost
!                                           tmp:RetailCost, | ! Retail_Cost
!                                           'ZERO QTY ORDER RECEIVED', | ! Notes
!                                           'ORIGINAL ORDER QTY: ' & orp:Quantity & |
!                        '<13,10>ORDER NUMBER: ' & Format(orp:order_number, @n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) & |
!                        '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason)) ! Information
!                            ! Added OK
!                        Else ! AddToStockHistory
!                            ! Error
!                        End ! AddToStockHistory
!
!                    Else ! If orp:Reason <> ''
!                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
!                                           'ADD', | ! Transaction_Type
!                                           Despatch_Note_Temp, | ! Depatch_Note_Number
!                                           0, | ! Job_Number
!                                           0, | ! Sales_Number
!                                           0, | ! Quantity
!                                           tmp:PurchaseCost, | ! Purchase_Cost
!                                           tmp:SaleCost, | ! Sale_Cost
!                                           tmp:RetailCost, | ! Retail_Cost
!                                           'ZERO QTY ORDER RECEIVED', | ! Notes
!                                           'ORIGINAL ORDER QTY: ' & orp:Quantity & |
!                        '<13,10>ORDER NUMBER: ' & Format(orp:order_number, @n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) & |
!                        '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)) ! Information
!                            ! Added OK
!                        Else ! AddToStockHistory
!                            ! Error
!                        End ! AddToStockHistory
!
!                    End ! If orp:Reason <> ''
!
!                    orp:number_received    = 0
!                    orp:date_received      = date_received_temp
!                    orp:all_received       = 'YES'
!                    orp:DespatchNoteNumber = Despatch_Note_Temp
!
!                    access:ordparts.update()
!                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    Case Missive('Error! Unable to retrieve the Stock Part''s Details.', 'ServiceBase 3g', |
!                                 'mstop.jpg', '/OK')
!                    Of 1 ! OK Button
!                    End ! Case Missive
!                End ! Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!
!            End ! Case orp:Part_Type
!!          Of 1 ! No Button
!!          End ! Case Missive
!    Else ! tmp:Quantity = 0
!        tmp:CostsChanged    = 0
!        If sav:PartNumber <> tmp:PartNumber Or    |
!            sav:Description <> tmp:Description Or |
!            sav:Sale_Cost <> tmp:SaleCost Or      |
!            (sav:Purchase_Cost <> tmp:PurchaseCost And GETINI('STOCK', 'UseAveragePurchaseCost',, CLIP(PATH()) & '\SB2KDEF.INI') <> 1) Or |
!            sav:Retail_Cost <> tmp:RetailCost   Or    |
!            sav:ShelfLocation <> tmp:ShelfLocation Or |
!            sav:SecondLocation <> tmp:SecondLocation
!
!            Case Missive('Are you sure you want to alter the details of this item?', 'ServiceBase 3g', |
!                         'mquest.jpg', '\No|/Yes')
!            Of 2 ! Yes Button
!                tmp:CostsChanged       = 1
!                orp:purchase_cost      = tmp:PurchaseCost
!                orp:sale_cost          = tmp:SaleCost
!                orp:retail_cost        = tmp:RetailCost
!                orp:part_number        = tmp:partnumber
!                orp:description        = tmp:description
!                orp:DespatchNoteNumber = despatch_note_temp
!
!            Of 1 ! No Button
!
!                tmp:PurchaseCost   = orp:purchase_cost
!                tmp:SaleCost       = orp:sale_cost
!                tmp:RetailCost     = orp:retail_cost
!                tmp:partnumber     = orp:part_number
!                tmp:description    = orp:description
!                tmp:ShelfLocation  = sav:ShelfLocation
!                tmp:SecondLocation = sav:SecondLocation
!
!            End ! Case Missive
!        End ! If
!        Display()
!
!
!        parts_record_number_temp    = ''
!        warparts_record_number_temp = ''
!        Do Get_Record_Numbers
!
!        ! Write the Despatch Note Number!
!        orp:DespatchNoteNumber = despatch_note_temp
!
!        ! Write the changes back to the job if necessary
!        If tmp:CostsChanged = 1
!            Case orp:Part_Type
!            Of 'JOB'
!                Access:PARTS.Clearkey(par:RecordNumberKey)
!                par:Record_Number   = Parts_Record_Number_Temp
!                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
!                    ! Found
!                    par:Part_Number   = tmp:PartNumber
!                    par:Description   = tmp:Description
!                    par:Sale_Cost     = tmp:SaleCost
!                    par:Purchase_Cost = tmp:PurchaseCost
!                    par:Retail_Cost   = tmp:RetailCost
!                    Access:PARTS.Update()
!                Else! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
!                    ! Error
!                    ! Assert(0, '<13,10>Could not retrieve the associated part from the Job<13,10>')
!                End! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
!
!            Of 'WAR'
!                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
!                wpr:Record_Number   = Warparts_Record_Number_Temp
!                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
!                    ! Found
!                    wpr:Part_Number   = tmp:PartNumber
!                    wpr:Description   = tmp:Description
!                    wpr:Sale_Cost     = tmp:SaleCost
!                    wpr:Purchase_Cost = tmp:PurchaseCost
!                    wpr:Retail_Cost   = tmp:RetailCost
!                    Access:WARPARTS.Update()
!                Else! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
!                    ! Error
!                    ! Assert(0, '<13,10>Could not retrieve the associated part from the Job<13,10>')
!                End! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
!
!            Of 'STO'
!
!                StockPart# = 1
!                Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                sto:Ref_Number  = orp:Part_Ref_Number
!                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                ! Found
!
!                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                        ! Error
!                        ! Assert(0,'<13,10>Fetch Error<13,10>')
!                    Access:STOCK.ClearKey(sto:Location_Key)
!                    sto:Location    = MainStoreLocation()
!                    sto:Part_Number = orp:Part_Number
!                    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                    ! Found
!
!                    Else! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                        ! Error
!                        Case Missive('Error! Unable to retrieve the Stock Part''s Details.', 'ServiceBase 3g', |
!                                     'mstop.jpg', '/OK')
!                        Of 1 ! OK Button
!                        End ! Case Missive
!                        StockPart# = 0
!                    End! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
!                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!
!                If StockPart# = 1
!                    ! Found
!                    sto:Part_Number = tmp:PartNumber
!                    sto:Description = tmp:Description
!                    If GETINI('STOCK', 'IgnorePriceChange',, CLIP(PATH()) & '\SB2KDEF.INI') <> 1
!                        sto:Sale_Cost     = tmp:SaleCost
!                        sto:Purchase_Cost = tmp:PurchaseCost
!                        sto:Retail_Cost   = tmp:RetailCost
!                    End ! If GETINI('STOCK','IgnorePriceChange',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
!                    sto:Second_Location = tmp:SecondLocation
!                    sto:Shelf_Location  = tmp:ShelfLocation
!
!                    Access:STOCK.Update()
!
!                    tmp:information          = 'PREVIOUS DETAILS:-'
!                    If tmp:PartNumber <> sav:PartNumber
!                        tmp:Information = Clip(tmp:Information) & '<13,10>PART NUMBER: ' & CLip(sav:PartNumber)
!                    End! If func:OldPartNumber <> func:NewPartNumber
!                    If tmp:Description <> sav:Description
!                        tmp:Information = Clip(tmp:Information) & '<13,10>DESCRIPTION: ' & CLip(sav:Description)
!                    End! If func:OldDescription <> func:NewDescription
!                    If GETINI('STOCK', 'IgnorePriceChange',, CLIP(PATH()) & '\SB2KDEF.INI') <> 1
!                        If tmp:PurchaseCost <> sav:Purchase_Cost
!                            tmp:Information = Clip(tmp:Information) & '<13,10>PURCHASE COST: ' & CLip(sav:Purchase_Cost)
!                        End! If func:OldPurchaseCost <> func:NewPurchaseCost
!                        If tmp:SaleCost <> sav:Sale_Cost
!                            tmp:Information = Clip(tmp:Information) & '<13,10>TRADE PRICE: ' & CLip(sav:Sale_Cost)
!                        End! If func:OldSaleCost <> func:NewSaleCost
!                        If tmp:RetailCost <> sav:Retail_Cost
!                            tmp:Information = Clip(tmp:Information) & '<13,10>RETAIL PRICE: ' & CLip(sav:Retail_Cost)
!                        End! If func:OldRetailCost <> func:NewRetailCost
!                    End ! If GETINI('STOCK','IgnorePriceChange',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
!                    If tmp:ShelfLocation <> sav:ShelfLocation
!                        tmp:Information = Clip(tmp:Information) & '<13,10>SHELF LOCATION: ' & CLip(sav:ShelfLocation)
!                    End ! If tmp:ShelfLocatoin <> sav:ShelfLocation
!                    If tmp:SecondLocation <> sav:SecondLocation
!                        tmp:Information = Clip(tmp:Information) & '<13,10>RETAIL PRICE: ' & CLip(sav:SecondLocation)
!                    End ! If tmp:SecondLocation <> sav:SecondLocation
!
!                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
!                                       'ADD', | ! Transaction_Type
!                                       '', | ! Depatch_Note_Number
!                                       0, | ! Job_Number
!                                       0, | ! Sales_Number
!                                       0, | ! Quantity
!                                       tmp:PurchaseCost, | ! Purchase_Cost
!                                       tmp:SaleCost, | ! Sale_Cost
!                                       tmp:RetailCost, | ! Retail_Cost
!                                       'DETAILS CHANGED', | ! Notes
!                                       tmp:Information) ! Information
!                      ! Added OK
!                    Else ! AddToStockHistory
!                      ! Error
!                    End ! AddToStockHistory
!
!                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    ! Error
!!                      Assert(0, '<13,10>Could not retrieve the associated part from Stock<13,10>')
!                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!
!            End! Case orp:Part_Type
!        End! If tmp:CostsChanged = 1
!
!        If quantity_temp = orp:quantity                                                             ! If not values are changed.
!            orp:number_received = orp:quantity                                                  ! Everythng has arrived
!            orp:date_received   = date_received_temp
!            orp:all_received    = 'YES'
!            access:ordparts.update()
!            quantity_to_job$ = 0
!            Do Normal_Receive
!        ElsIf quantity_temp < orp:quantity
!            Case Missive('The quanity received is less than the quantity requested. Do you wish to:' & |
!                         '<13,10>a) IGNORE the difference and amend the original item or,' &           |
!                         '<13,10>b) RE-ORDER the remaining items or,' &                 |
!                         '<13,10>c) CONTINUE. The remaining items will arrive later?' & |
!                         '<13,10>', 'ServiceBase 3g', |
!                         'mquest.jpg', '\Cancel|Continue|Re-order|Ignore')
!            Of 4 ! Ignore Button
!                Do Normal_Receive
!
!                orp:quantity        = quantity_temp
!                orp:Number_Received = Quantity_Temp
!                orp:all_received    = 'YES'
!                access:ordparts.update()
!            Of 3 ! Re-order Button
!                get(ordpend, 0)
!                if access:ordpend.primerecord() = Level:Benign
!                    ope:part_ref_number = orp:part_ref_number
!                    ope:job_number      = orp:job_number
!                    ope:part_type       = orp:part_type
!                    ope:supplier        = ord:supplier
!                    ope:part_number     = orp:part_number
!                    ope:description     = orp:description
!                    ope:quantity        = orp:quantity - quantity_temp
!                    if access:ordpend.insert()
!                        access:ordpend.cancelautoinc()
!                    end ! if
!                end ! if access:ordpend.primerecord() = Level:Benign
!
!                Do Reorder_Receive
!
!                orp:quantity        = quantity_temp
!                orp:Number_Received = Quantity_Temp
!
!                orp:all_received = 'YES'
!                access:ordparts.update()
!            Of 2 ! Continue Button
!                ! Save the Despatch Note No on the "arrived" part of the order - TrkBs: 6444 (DBH: 03-10-2005)
!                Do arrive_later_receive
!
!                glo:select3  = 'NEW ORDER'
!                glo:select4  = orp:record_number
!                glo:select5  = quantity_temp
!                orp:quantity = orp:quantity - quantity_temp
!                glo:select6  = date_received_temp
!                ! Save the Despatch Note No on the "arrived" part - TrkBs: 6444 (DBH: 03-10-2005)
!
!                glo:Select7            = Despatch_Note_Temp
!                ! Changing (DBH 31/01/2006) #7106 - Make sure that you don't pass back spurious currency details
!                !             ! Inserting (DBH 11/28/2005) #6789 - Pass the "received" currency details to the new "Ordered" part. And the details on this pending part
!                !             ! Changing (DBH 12/01/2006) #7002 - Blank these fields.
!                !             ! orp:ReceivedCurrency       = 0
!                !             ! orp:ReceivedDailyRate      = 0
!                !             ! orp:ReceivedDivideMultiply = 0
!                !             ! to (DBH 12/01/2006) #7002
!                !             orp:ReceivedCurrency       = ''
!                !             orp:ReceivedDailyRate      = ''
!                !             orp:ReceivedDivideMultiply = ''
!                !             ! End (DBH 12/01/2006) #7002
!                !             glo:Select8 = cur:CurrencyCode
!                !             glo:Select9 = cur:DailyRate
!                !             glo:Select10 = cur:DivideMultiply
!                !             ! End (DBH 11/28/2005) #6789
!                ! to (DBH 31/01/2006) #7106
!                glo:Select8                = orp:ReceivedCurrency
!                glo:Select9                = orp:ReceivedDailyRate
!                glo:Select10               = orp:ReceivedDivideMultiply
!                orp:ReceivedCurrency       = ''
!                orp:ReceivedDailyRate      = ''
!                orp:ReceivedDivideMultiply = ''
!                ! End (DBH 31/01/2006) #7106
!                orp:date_received      = ''
!                orp:DespatchNoteNumber = ''
!                orp:Number_Received    = orp:Quantity
!                access:ordparts.update()
!            Of 1 ! Cancel Button
!                RETURN Level:Fatal
!            End ! Case Missive
!        End ! If quantity_temp = orp:quantity
!
!        ORP:Purchase_Cost = tmp:PurchaseCost
!        access:ordparts.update()
!
!        If no_of_labels_temp > 0
!            Loop x# = 1 To no_of_labels_temp
!                glo:select1  = orp:part_ref_number
!                Case def:label_printer_type
!                Of 'TEC B-440 / B-442'
!                    Stock_Label(date_received_temp, quantity_temp, orp:order_number, despatch_note_temp, tmp:SaleCost)
!                Of 'TEC B-452'
!                    Stock_Label_B452(date_received_temp, quantity_temp, orp:order_number, despatch_note_temp, tmp:SaleCost)
!                End! Case def:label_printer_type
!                glo:select1 = ''
!            End! Loop x# = 1 To no_of_labels_temp
!        End! If no_of_labels_temp > 0
!        If despatch_Note_temp <> ''
!            glo:select24 = despatch_note_temp
!        End! If despatch_Note_temp <> ''
!
!        Do Replicate
!
!
!    End ! tmp:Quantity = 0
!
!
!    ! Inserting (DBH 03/04/2007) # 8046 - Ask if the user wants to print a bin label
!    lab# = SelectLabelQuantity(Quantity_Temp)
!    If lab# > 0
!        StockBarcodeLabel(orp:Record_Number,lab#)
!    End ! If lab# > 0
!    PrintShelfLocations(tmp:ShelfLocation)
!    ! End (DBH 03/04/2007) #8046
! End (DBH 14/08/2007) #9117
      If local.Receive()
          Cycle
      End ! If local:Receive()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020091'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020091'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020091'&'0')
      ***
    OF ?tmp:PurchaseCost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PurchaseCost, Accepted)
      Do Hide_Fields
      Do WorkOutForeignCurrency
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PurchaseCost, Accepted)
    OF ?tmp:ForeignPurchasePrice
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ForeignPurchasePrice, Accepted)
      Do Hide_Fields
      Do WorkOutForeignCurrency
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ForeignPurchasePrice, Accepted)
    OF ?tmp:SaleCost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SaleCost, Accepted)
      Do WorkOutForeignCurrency
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SaleCost, Accepted)
    OF ?tmp:ForeignSellingPrice
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ForeignSellingPrice, Accepted)
      Do WorkOutForeignCurrency
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ForeignSellingPrice, Accepted)
    OF ?tmp:ShelfLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ShelfLocation, Accepted)
      If tmp:ShelfLocation{prop:Skip} = 0
      
      
      IF tmp:ShelfLocation OR ?tmp:ShelfLocation{Prop:Req}
        los:Shelf_Location = tmp:ShelfLocation
        los:Site_Location = sto:Location
        GLO:Select1 = sto:Location
        !Save Lookup Field Incase Of error
        look:tmp:ShelfLocation        = tmp:ShelfLocation
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:ShelfLocation = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:ShelfLocation = look:tmp:ShelfLocation
            SELECT(?tmp:ShelfLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      End !If tmp:ShelfLocation{prop:Skip} = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ShelfLocation, Accepted)
    OF ?LookupShelfLocation
      ThisWindow.Update
      los:Shelf_Location = tmp:ShelfLocation
      GLO:Select1 = sto:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:ShelfLocation = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?tmp:ShelfLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ShelfLocation)
    OF ?Button:ChangePartDetails
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ChangePartDetails, Accepted)
      Do ChangePartNumber
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ChangePartDetails, Accepted)
    OF ?PreviousDespatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PreviousDespatch, Accepted)
      Post(Event:AlertKey,0)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PreviousDespatch, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      If orp:all_received <> 'YES'
          If glo:select24 <> ''
              despatch_note_temp = glo:select24
          Else!If glo:select24 <> ''
              Case Missive('You have not entered a supplier invoice number previously.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End!If glo:select24 <> ''
          Display(?despatch_note_temp)
      End!If orp:all_received <> 'YES'
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
local.Receive       PROCEDURE()
locAmount               REAL()
locMissive              BYTE()
    CODE
        IF (?tmp:ForeignPurchasePrice{PROP:Hide} = 0)
            locAmount = tmp:ForeignPurchasePrice
        ELSE
            locAmount = tmp:PurchaseCost
        END
        
        IF (Quantity_Temp > orp:Quantity)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('You cannot receive more items than you ordered.','ServiceBase',|
                'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message            
            Quantity_Temp = 0
            SELECT(?Quantity_Temp)
            DISPLAY()
            RETURN 1
        END
        
        IF (Despatch_Note_Temp = '')
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('You must enter a Supplier Invoice Number.','ServiceBase',|
                'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message            
            SELECT(?Despatch_Note_Temp)
            RETURN 1
        END
        
        IF (fType = 0 AND quantity_Temp = 0)
            Beep(Beep:SystemQuestion)  ;  Yield()
            Case Missive('Are you sure you want to receive zero items?','ServiceBase',|
                           'mquest.jpg','\&No|/&Yes') 
            Of 2 ! &Yes Button
            Of 1 ! &No Button
                RETURN 1
            End!Case Message
        END

        IF (Quantity_Temp <> orp:Quantity AND Quantity_Temp <> 0)   ! #12325 Don't show message for zero returns (Bryan: 07/10/2011)
            Beep(Beep:SystemExclamation)  ;  Yield()
            locMissive =  Missive('The quantity received is less than the quantity requested. Do you wish to:'&|
                '|a) IGNORE the difference and amend the original item'&|
                '|b) RE-ORDER the remaining items'&|
                '|c) CONTINUE. The remaining items will arrive later?','ServiceBase',|
                'mexclam.jpg','\&Cancel|Continue|Re-order|Ignore') 
            IF (locMissive = 1)
                RETURN 1
            END
           
        END
        
        IF (FType = 1)
            ! Lookup the currency details, but only if set at point of order (DBH: 14/08/2007)
            Access:ORDERS.ClearKey(ord:Order_Number_Key)
            ord:Order_Number = orp:Order_Number
            If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
            !Found
                If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
                    Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
                    sup:Company_Name = ord:Supplier
                    If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                    !Found
                        If sup:UseForeignCurrency
                            Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
                            cur:CurrencyCode = sup:CurrencyCode
                            If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                            !Found
                                orp:ReceivedCurrency = cur:CurrencyCode
                                orp:ReceivedDailyRate = cur:DailyRate
                                orp:ReceivedDivideMultiply = cur:DivideMultiply
                            Else ! If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                            !Error
                            End ! If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign

                        End ! If sup:UseForeignCurrency
                    Else ! If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                    !Error
                    End ! If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign

                Else ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
                    orp:ReceivedCurrency = ''
                    orp:ReceivedDailyRate = ''
                    orp:ReceivedDivideMultiply = ''
                End ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
            Else ! If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
            !Error
            End ! If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
        END ! IF (FType = 1)            
        
        IF (Quantity_Temp = 0)
            IF (FType = 1)
            ! #11382 Check that the entered purchase cost is not over the price variant percentage (DBH: 09/08/2010)
                priceVariant$ = getini('STOCK','PartPriceVariant',0,PATH() & '\SB2KDEF.INI')
                if (priceVariant$ > 0)
                ! #11908 Allow for price variant below as well as above. (Bryan: 18/01/2011)
                    if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100))) OR |
                        tmp:PurchaseCost < (sto:Purchase_Cost - (sto:Purchase_Cost * (priceVariant$/100))))
                        If (PriceVariantCheck(sto:Part_Number,sto:Purchase_Cost,tmp:PurchaseCost) = FALSE)
                            Return 1
                        end ! If (PriceVariantCheck(sto:Purchase_Cost,tmp:PurchaseCost))
                    end ! if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100)))
                end ! if (priceVariant$ > 0)  
            ! #12006 Do no mark as received until prices are captured. (Bryan: 24/08/2011)
                orp:All_Received = 'YES'            
            END            
            DO Receive:NoQuantity
        ELSE
            If sav:ShelfLocation <> tmp:ShelfLocation Or sav:SecondLocation <> tmp:SecondLocation
                Case Missive('Are you sure you want to change the Shelf/2nd Location of this item?','ServiceBase 3g',|
                    'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                Of 1 ! No Button
                    tmp:ShelfLocation = sav:ShelfLocation
                    tmp:SecondLocation = sav:SecondLocation
                    Display()
                    Return 1
                End ! Case Missive
            End ! If sav:ShelfLocation <> tmp:ShelfLocation Or sav:SecondLocation <> tmp:SecondLocation
            orp:Purchase_Cost = tmp:PurchaseCost
            orp:Sale_Cost = tmp:SaleCost
            orp:Retail_Cost = tmp:RetailCost
            orp:Part_Number = tmp:PartNumber
            orp:Description = tmp:Description
            orp:DespatchNoteNumber = Despatch_Note_Temp

        !Found
            If StockInUse(1)
                Return 1
            End ! If StockInUse(0)    
            
            IF (FType = 1)
            ! #11382 Check that the entered purchase cost is not over the price variant percentage (DBH: 09/08/2010)
                priceVariant$ = getini('STOCK','PartPriceVariant',0,PATH() & '\SB2KDEF.INI')
                if (priceVariant$ > 0)
                ! #11908 Allow for price variant below as well as above. (Bryan: 18/01/2011)
                    if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100))) OR |
                        tmp:PurchaseCost < (sto:Purchase_Cost - (sto:Purchase_Cost * (priceVariant$/100))))
                        If (PriceVariantCheck(sto:part_Number,sto:Purchase_Cost,tmp:PurchaseCost) = FALSE)
                            Return 1
                        end ! If (PriceVariantCheck(sto:Purchase_Cost,tmp:PurchaseCost))
                    end ! if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100)))
                end ! if (priceVariant$ > 0)
                
                If GETINI('STOCK','IgnorePriceChange',,Clip(Path()) & '\SB2KDEF.INI') <> 1
                    sto:Sale_Cost = tmp:SaleCost
                    sto:Purchase_Cost = tmp:PurchaseCost
                    sto:Retail_Cost = tmp:RetailCost
                End ! If GETINI('STOCK','IgnorePriceChange',,Clip(Path()) & '\SB2KDEF.INI') <> 1                
            END ! IF (FType = 1)       
            
            sto:Second_Location = tmp:SecondLocation
            sto:Shelf_Location = tmp:ShelfLocation
            Access:STOCK.TryUpdate()
            
            IF (Quantity_Temp = orp:Quantity)
                IF (FType = 0)
                    orp:Number_Received = orp:Quantity
                END
                IF (FType = 1)
                    orp:All_Received = 'YES'
                END
                Access:ORDPARTS.TryUpdate()
                DO Receive:Normal
            ELSE
                CASE locMissive
                OF 4 ! Ignore Button
                    DO Receive:Normal
                    orp:Quantity = Quantity_Temp
                    orp:Number_Received = orp:Quantity
                    IF (FType = 1)
                        orp:All_Received = 'YES'
                    END
                    Access:ORDPARTS.TryUpdate()
                OF 3 ! Re-order Button
                    If Access:ORDPEND.PrimeRecord() = Level:Benign
                        ope:Part_Ref_Number = orp:Part_Ref_Number
                        ope:Job_Number = orp:Job_Number
                        ope:Part_Type = orp:Part_Type
                        ope:Supplier = ord:Supplier
                        ope:Part_Number = orp:Part_Number
                        ope:Description = orp:Description
                        ope:Quantity = orp:Quantity - Quantity_Temp
                        If Access:ORDPEND.TryInsert() = Level:Benign
                        !Insert
                        Else ! If Access:ORDPEND.TryInsert() = Level:Benign
                            Access:ORDPEND.CancelAutoInc()
                        End ! If Access:ORDPEND.TryInsert() = Level:Benign
                    End ! If Access.ORDPEND.PrimeRecord() = Level:Benign
                    DO Receive:Reorder
                    
                    orp:Quantity = Quantity_Temp
                    orp:Number_Received = Quantity_Temp
                    IF (fType = 1)
                        orp:All_Received = 'YES'
                    END
                    Access:ORDPARTS.TryUpdate()
                OF 2 ! Continue Button
                    DO Receive:Later
                    ! Create new line for parts coming in
                    IF (Access:ORDPARTS_ALIAS.PrimeRecord() = Level:Benign)
                        recNo# = orp_ali:Record_Number
                        orp_ali:Record :=: orp:Record
                        orp_ali:Record_Number = recNo#
                        orp_ali:Quantity = Quantity_Temp
                        orp_ali:Number_Received = orp_ali:Quantity
                        orp_ali:Date_Received = TODAY()
                        orp_ali:All_Received = 'NO'
                        orp_ali:DespatchNoteNumber = Despatch_Note_Temp
                        IF (Access:ORDPARTS_ALIAS.TryInsert())
                            Access:ORDPARTS.CancelAutoInc()
                            Beep(Beep:SystemHand)  ;  Yield()
                            Case Missive('An error occurred creating the new line for the parts to arrive later.','ServiceBase',|
                                'mstop.jpg','/&OK') 
                            Of 1 ! &OK Button
                            End!Case Message                            
                            RETURN 1
                        END
                    END
                    
                    orp:Quantity -= Quantity_Temp
                    orp:ReceivedCurrency = ''
                    orp:ReceivedDailyRate = ''
                    orp:ReceivedDivideMultiply = ''
                    orp:Date_Received = ''
                    orp:TimeReceived = ''
                    orp:DespatchNoteNumber = ''
                    orp:Number_Received = orp:Quantity
                    Access:ORDPARTS.TryUpdate()                    
                END
            END
            
            orp:Purchase_Cost = tmp:PurchaseCost
            Access:ORDPARTS.TryUpdate()
            

            IF (FType = 1)
                If no_of_labels_temp > 0
                    Loop x# = 1 To no_of_labels_temp
                        glo:select1  = orp:part_ref_number
                        Case def:label_printer_type
                        Of 'TEC B-440 / B-442'
                            Stock_Label(date_received_temp, quantity_temp, orp:order_number, despatch_note_temp, tmp:SaleCost)
                        Of 'TEC B-452'
                            Stock_Label_B452(date_received_temp, quantity_temp, orp:order_number, despatch_note_temp, tmp:SaleCost)
                        End! Case def:label_printer_type
                        glo:select1 = ''
                    End! Loop x# = 1 To no_of_labels_temp
                End! If no_of_labels_temp > 0
                If despatch_Note_temp <> ''
                    glo:select24 = despatch_note_temp
                End! If despatch_Note_temp <> ''
            END

            Do Replicate
                
        END
        
            ! Inserting (DBH 03/04/2007) # 8046 - Ask if the user wants to print a bin label
        IF (FType = 1)
            lab# = SelectLabelQuantity(Quantity_Temp)
            If lab# > 0
                StockBarcodeLabel(orp:Record_Number,lab#)
            End ! If lab# > 0
            PrintShelfLocations(tmp:ShelfLocation)
        END
    ! End (DBH 03/04/2007) #8046
        Return 0
!local.Receive            Procedure()
!local:Amount                Real()
!Code
!    If ?tmp:ForeignPurchasePrice{prop:Hide} = False
!        local:Amount = tmp:ForeignPurchasePrice
!    Else ! If ?tmp:ForeignPurchasePrice{prop:Hide} = False
!        local:Amount = tmp:PurchaseCost
!    End ! If ?tmp:ForeignPurchasePrice{prop:Hide} = False
!
!    Case fType
!    Of 0 ! Receive Part
!        Beep(Beep:SystemQuestion)  ;  Yield()
!        Case Missive('Receive Quantity: ' & Clip(quantity_temp) & ''&|
!            '|'&|
!            '|Is this correct?','ServiceBase',|
!                       'mquest.jpg','\&No|/&Yes') 
!        Of 2 ! &Yes Button
!        Of 1 ! &No Button
!            Return 1
!        End!Case Message
!    Of 1 ! Capture Price
!        Case Missive('Quantity: ' & Quantity_Temp & |
!          '|Purchase Cost: ' & Format(local:Amount,@n14.2) & '.'&|
!          '||Is this correct?','ServiceBase 3g',|
!                       'mexclam.jpg','\No|/Yes')
!            Of 2 ! Yes Button
!            Of 1 ! No Button
!              Return 1
!        End ! Case Missive
!    End
!
!    ! Do not allow to receive more than was ordered (DBH: 14/08/2007)
!    If Quantity_Temp > orp:Quantity
!        Case Missive('You cannot receive more items than you ordered.','ServiceBase 3g',|
!                       'mstop.jpg','/OK')
!            Of 1 ! OK Button
!        End ! Case Missive
!        Quantity_Temp = 0
!        Select(?Quantity_Temp)
!        Display()
!        Return 1
!    End ! If Quantity_Temp > orp:Quantity
!
!    If Despatch_Note_Temp = ''
!        ! Despatch Note Number is used as National Store Invoice Number (DBH: 14/08/2007)
!        Case Missive('You must enter a Supplier Invoice Number.','ServiceBase 3g',|
!                       'mstop.jpg','/OK')
!            Of 1 ! OK Button
!        End ! Case Missive
!        Select(?Despatch_Note_Temp)
!        Return 1
!    End ! If Despatch_Note_Temp = ''
!
!
!    IF (FType = 1)
!        ! Lookup the currency details, but only if set at point of order (DBH: 14/08/2007)
!        Access:ORDERS.ClearKey(ord:Order_Number_Key)
!        ord:Order_Number = orp:Order_Number
!        If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!            !Found
!            If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
!                Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
!                sup:Company_Name = ord:Supplier
!                If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!                    !Found
!                    If sup:UseForeignCurrency
!                        Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
!                        cur:CurrencyCode = sup:CurrencyCode
!                        If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
!                            !Found
!                            orp:ReceivedCurrency = cur:CurrencyCode
!                            orp:ReceivedDailyRate = cur:DailyRate
!                            orp:ReceivedDivideMultiply = cur:DivideMultiply
!                        Else ! If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
!                            !Error
!                        End ! If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
!
!                    End ! If sup:UseForeignCurrency
!                Else ! If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!                    !Error
!                End ! If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
!
!            Else ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
!                orp:ReceivedCurrency = ''
!                orp:ReceivedDailyRate = ''
!                orp:ReceivedDivideMultiply = ''
!            End ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
!        Else ! If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!            !Error
!        End ! If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
!    END ! IF (FType = 1)
!
!    If Quantity_Temp = 0
!
!    Else ! If Quantity_Temp = 0
!        If sav:ShelfLocation <> tmp:ShelfLocation Or sav:SecondLocation <> tmp:SecondLocation
!            Case Missive('Are you sure you want to change the Shelf/2nd Location of this item?','ServiceBase 3g',|
!                           'mquest.jpg','\No|/Yes')
!                Of 2 ! Yes Button
!                Of 1 ! No Button
!                    tmp:ShelfLocation = sav:ShelfLocation
!                    tmp:SecondLocation = sav:SecondLocation
!                    Display()
!                    Return 1
!            End ! Case Missive
!        End ! If sav:ShelfLocation <> tmp:ShelfLocation Or sav:SecondLocation <> tmp:SecondLocation
!        orp:Purchase_Cost = tmp:PurchaseCost
!        orp:Sale_Cost = tmp:SaleCost
!        orp:Retail_Cost = tmp:RetailCost
!        orp:Part_Number = tmp:PartNumber
!        orp:Description = tmp:Description
!        orp:DespatchNoteNumber = Despatch_Note_Temp
!
!        !Found
!        If StockInUse(1)
!            Return 1
!        End ! If StockInUse(0)
!
!        IF (FType = 1)
!            ! #11382 Check that the entered purchase cost is not over the price variant percentage (DBH: 09/08/2010)
!            priceVariant$ = getini('STOCK','PartPriceVariant',0,PATH() & '\SB2KDEF.INI')
!            if (priceVariant$ > 0)
!                ! #11908 Allow for price variant below as well as above. (Bryan: 18/01/2011)
!                if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100))) OR |
!                    tmp:PurchaseCost < (sto:Purchase_Cost - (sto:Purchase_Cost * (priceVariant$/100))))
!                    If (PriceVariantCheck(sto:part_Number,sto:Purchase_Cost,tmp:PurchaseCost) = FALSE)
!                        Return 1
!                    end ! If (PriceVariantCheck(sto:Purchase_Cost,tmp:PurchaseCost))
!                end ! if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100)))
!            end ! if (priceVariant$ > 0)
!        END ! IF (FType = 1)
!
!        ! #11518 Move message before writing to Stock History (DBH: 16/06/2010)
!        if (quantity_Temp <> orp:Quantity)
!            missive# = Missive('The quanity received is less than the quantity requested. Do you wish to:' & |
!                                     '<13,10>a) IGNORE the difference and amend the original item or,' &           |
!                                     '<13,10>b) RE-ORDER the remaining items or,' &                 |
!                                     '<13,10>c) CONTINUE. The remaining items will arrive later?' & |
!                                     '<13,10>', 'ServiceBase 3g', |
!                                     'mquest.jpg', '\Cancel|Continue|Re-order|Ignore')
!
!            if (missive# = 1)
!                return 1
!            end
!        end
!
!        IF (FType = 1)
!            If GETINI('STOCK','IgnorePriceChange',,Clip(Path()) & '\SB2KDEF.INI') <> 1
!                sto:Sale_Cost = tmp:SaleCost
!                sto:Purchase_Cost = tmp:PurchaseCost
!                sto:Retail_Cost = tmp:RetailCost
!            End ! If GETINI('STOCK','IgnorePriceChange',,Clip(Path()) & '\SB2KDEF.INI') <> 1
!        END ! IF (FType = 1)
!
!        sto:Second_Location = tmp:SecondLocation
!        sto:Shelf_Location = tmp:ShelfLocation
!        Access:STOCK.TryUpdate()
!        
!        If Quantity_Temp = orp:Quantity
!            orp:Number_Received = orp:Quantity
!            orp:Date_Received = Date_Received_Temp
!            orp:All_Received = 'YES'
!            Access:ORDPARTS.TryUpdate()
!            Do Normal_Receive
!        Else ! If Quantity_Temp = orp:Quantity
!            case missive#
!            Of 4 ! Ignore Button
!                Do Normal_Receive
!
!                orp:Quantity = Quantity_Temp
!                orp:Number_Received = Quantity_Temp
!                orp:All_Received = 'YES'
!                Access:ORDPARTS.TryUpdate()
!            Of 3 ! Re-order Button
!                If Access:ORDPEND.PrimeRecord() = Level:Benign
!                    ope:Part_Ref_Number = orp:Part_Ref_Number
!                    ope:Job_Number = orp:Job_Number
!                    ope:Part_Type = orp:Part_Type
!                    ope:Supplier = ord:Supplier
!                    ope:Part_Number = orp:Part_Number
!                    ope:Description = orp:Description
!                    ope:Quantity = orp:Quantity - Quantity_Temp
!                    If Access:ORDPEND.TryInsert() = Level:Benign
!                        !Insert
!                    Else ! If Access:ORDPEND.TryInsert() = Level:Benign
!                        Access:ORDPEND.CancelAutoInc()
!                    End ! If Access:ORDPEND.TryInsert() = Level:Benign
!                End ! If Access.ORDPEND.PrimeRecord() = Level:Benign
!                Do Reorder_Receive
!
!                orp:Quantity = Quantity_Temp
!                orp:Number_Received = Quantity_Temp
!                orp:All_Received = 'YES'
!                Access:ORDPARTS.TryUpdate()
!            Of 2 ! Continue Button
!                Do Arrive_Later_Receive
!                glo:Select3 = 'NEW ORDER'
!                glo:Select4 = orp:Record_Number
!                glo:Select5 = Quantity_Temp
!                orp:Quantity = orp:Quantity - Quantity_Temp
!                glo:Select6 = Date_Received_Temp
!                glo:Select7 = Despatch_Note_Temp
!                glo:Select8 = orp:ReceivedCurrency
!                glo:Select9 = orp:ReceivedDailyRate
!                glo:Select10 = orp:ReceivedDivideMultiply
!                orp:ReceivedCurrency = ''
!                orp:ReceivedDailyRate = ''
!                orp:ReceivedDivideMultiply = ''
!                orp:Date_Received = ''
!                orp:DespatchNoteNumber = ''
!                orp:Number_Received = orp:Quantity
!                Access:ORDPARTS.TryUpdate()
!            Of 1 ! Cancel Button
!                Return 1
!            End ! Case
!        End ! If Quantity_Temp = orp:Quantity
!
!        orp:Purchase_Cost = tmp:PurchaseCOst
!        Access:ORDPARTS.TryUpdate()
!
!        If no_of_labels_temp > 0
!            Loop x# = 1 To no_of_labels_temp
!                glo:select1  = orp:part_ref_number
!                Case def:label_printer_type
!                Of 'TEC B-440 / B-442'
!                    Stock_Label(date_received_temp, quantity_temp, orp:order_number, despatch_note_temp, tmp:SaleCost)
!                Of 'TEC B-452'
!                    Stock_Label_B452(date_received_temp, quantity_temp, orp:order_number, despatch_note_temp, tmp:SaleCost)
!                End! Case def:label_printer_type
!                glo:select1 = ''
!            End! Loop x# = 1 To no_of_labels_temp
!        End! If no_of_labels_temp > 0
!        If despatch_Note_temp <> ''
!            glo:select24 = despatch_note_temp
!        End! If despatch_Note_temp <> ''
!
!        Do Replicate
!
!    End ! If Quantity_Temp = 0
!
!    ! Inserting (DBH 03/04/2007) # 8046 - Ask if the user wants to print a bin label
!    lab# = SelectLabelQuantity(Quantity_Temp)
!    If lab# > 0
!        StockBarcodeLabel(orp:Record_Number,lab#)
!    End ! If lab# > 0
!    PrintShelfLocations(tmp:ShelfLocation)
!    ! End (DBH 03/04/2007) #8046
!    Return 0
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Amend_Quantity PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Amended_Amount       LONG
window               WINDOW('Amend Quantity Received'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Amend Quantity Received'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(239,126),USE(?sto:Part_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(318,126,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Description'),AT(239,150),USE(?sto:Description:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(318,150,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Date Received'),AT(239,172),USE(?orp:Date_Received:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6b),AT(318,172,64,10),USE(orp:Date_Received),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Number Received'),AT(239,194),USE(?orp:Number_Received:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(318,194,40,10),USE(orp:Number_Received),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Number Received'),TIP('Number Received'),UPR
                           STRING('Amended Amount'),AT(239,218),USE(?String1),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@n-14),AT(318,218,40,10),USE(Amended_Amount),LEFT,FONT(,,,FONT:bold),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Quantity Received'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020100'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Amend_Quantity')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sto:Part_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ORDERS.Open
  Access:STOCK.UseFile
  Access:ORDPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Amend_Quantity')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Amend_Quantity')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020100'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020100'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020100'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:ordparts.clearkey(orp:record_number_key)
      orp:record_number = glo:select2
      if access:ordparts.fetch(orp:record_number_key)
          Case Missive('Error! Cannot access the Order''s Parts File.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Return Level:Fatal
      end
      access:orders.clearkey(ord:order_number_key)
      ord:order_number = glo:select1
      if access:orders.fetch(ord:order_number_key)
          Case Missive('Error! Cannot access the Order File.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
         Return Level:Fatal
      end
      !All Received
      If orp:all_received <> 'YES'
          Hide(?OKbutton)
      Else
          orp:date_received = Today()
      End
      Display()
      
      !ThisMakeOver.SetWindow(Win:FORM)
      
      Amended_Amount       = ORP:Quantity
      
      Found# = 0
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = orp:Part_Ref_Number
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
          Found# = 1
      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Access:STOCK.ClearKey(sto:Location_Key)
          sto:Location    = MainStoreLocation()
          sto:Part_Number = orp:Part_Number
          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Found
              Found# = 1
          Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Parts_Orders PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:LabelType        BYTE(0)
save_res_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_stock_id        USHORT,AUTO
retstock_record_number_temp LONG
parts_record_number_temp REAL
warparts_record_number_temp REAL
LocalRequest         LONG
FilesOpened          BYTE
Received_Temp        STRING(20)
outstanding_temp     STRING('NO {1}')
job_number_temp      STRING(6)
type_temp            STRING(3)
tmp:tag              STRING('0')
tmp:Goods_Received_Date LONG
tmp:ExchangeUnit     BYTE(0)
tmp:StockItem        BYTE(0)
tmp:Currency         STRING(30)
tmp:BarCodeLabelNumber LONG
tmp:BarcodeLabelType BYTE(0)
tmp:OrderNumberPrefix STRING(3)
locCreditLocation    STRING(30)
locCreditReceived    STRING(3)
locFalse             BYTE
locCreditType        STRING(8)
locRTNTag            STRING(1)
Save_ordpart_number  LONG
Save_ordpart_order   LONG
Save_orpart_DNN      STRING(30)
BRW1::View:Browse    VIEW(ORDERS)
                       PROJECT(ord:Order_Number)
                       PROJECT(ord:Supplier)
                       PROJECT(ord:User)
                       PROJECT(ord:Date)
                       PROJECT(ord:Printed)
                       PROJECT(ord:All_Received)
                       PROJECT(ord:OrderedCurrency)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ord:Order_Number       LIKE(ord:Order_Number)         !List box control field - type derived from field
ord:Supplier           LIKE(ord:Supplier)             !List box control field - type derived from field
ord:User               LIKE(ord:User)                 !List box control field - type derived from field
ord:Date               LIKE(ord:Date)                 !List box control field - type derived from field
ord:Printed            LIKE(ord:Printed)              !List box control field - type derived from field
ord:All_Received       LIKE(ord:All_Received)         !List box control field - type derived from field
ord:OrderedCurrency    LIKE(ord:OrderedCurrency)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ORDPARTS)
                       PROJECT(orp:Part_Number)
                       PROJECT(orp:Description)
                       PROJECT(orp:Quantity)
                       PROJECT(orp:Number_Received)
                       PROJECT(orp:Purchase_Cost)
                       PROJECT(orp:Sale_Cost)
                       PROJECT(orp:Date_Received)
                       PROJECT(orp:TimeReceived)
                       PROJECT(orp:DatePriceCaptured)
                       PROJECT(orp:TimePriceCaptured)
                       PROJECT(orp:All_Received)
                       PROJECT(orp:Part_Ref_Number)
                       PROJECT(orp:Record_Number)
                       PROJECT(orp:DespatchNoteNumber)
                       PROJECT(orp:Order_Number)
                       JOIN(sto:Ref_Part_Description2_Key,orp:Part_Ref_Number,orp:Part_Number,orp:Description)
                         PROJECT(sto:Second_Location)
                         PROJECT(sto:Shelf_Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
orp:Part_Number        LIKE(orp:Part_Number)          !List box control field - type derived from field
orp:Part_Number_NormalFG LONG                         !Normal forground color
orp:Part_Number_NormalBG LONG                         !Normal background color
orp:Part_Number_SelectedFG LONG                       !Selected forground color
orp:Part_Number_SelectedBG LONG                       !Selected background color
orp:Description        LIKE(orp:Description)          !List box control field - type derived from field
orp:Description_NormalFG LONG                         !Normal forground color
orp:Description_NormalBG LONG                         !Normal background color
orp:Description_SelectedFG LONG                       !Selected forground color
orp:Description_SelectedBG LONG                       !Selected background color
type_temp              LIKE(type_temp)                !List box control field - type derived from local data
type_temp_NormalFG     LONG                           !Normal forground color
type_temp_NormalBG     LONG                           !Normal background color
type_temp_SelectedFG   LONG                           !Selected forground color
type_temp_SelectedBG   LONG                           !Selected background color
job_number_temp        LIKE(job_number_temp)          !List box control field - type derived from local data
job_number_temp_NormalFG LONG                         !Normal forground color
job_number_temp_NormalBG LONG                         !Normal background color
job_number_temp_SelectedFG LONG                       !Selected forground color
job_number_temp_SelectedBG LONG                       !Selected background color
orp:Quantity           LIKE(orp:Quantity)             !List box control field - type derived from field
orp:Quantity_NormalFG  LONG                           !Normal forground color
orp:Quantity_NormalBG  LONG                           !Normal background color
orp:Quantity_SelectedFG LONG                          !Selected forground color
orp:Quantity_SelectedBG LONG                          !Selected background color
orp:Number_Received    LIKE(orp:Number_Received)      !List box control field - type derived from field
orp:Number_Received_NormalFG LONG                     !Normal forground color
orp:Number_Received_NormalBG LONG                     !Normal background color
orp:Number_Received_SelectedFG LONG                   !Selected forground color
orp:Number_Received_SelectedBG LONG                   !Selected background color
tmp:Currency           LIKE(tmp:Currency)             !List box control field - type derived from local data
tmp:Currency_NormalFG  LONG                           !Normal forground color
tmp:Currency_NormalBG  LONG                           !Normal background color
tmp:Currency_SelectedFG LONG                          !Selected forground color
tmp:Currency_SelectedBG LONG                          !Selected background color
orp:Purchase_Cost      LIKE(orp:Purchase_Cost)        !List box control field - type derived from field
orp:Purchase_Cost_NormalFG LONG                       !Normal forground color
orp:Purchase_Cost_NormalBG LONG                       !Normal background color
orp:Purchase_Cost_SelectedFG LONG                     !Selected forground color
orp:Purchase_Cost_SelectedBG LONG                     !Selected background color
orp:Sale_Cost          LIKE(orp:Sale_Cost)            !List box control field - type derived from field
orp:Sale_Cost_NormalFG LONG                           !Normal forground color
orp:Sale_Cost_NormalBG LONG                           !Normal background color
orp:Sale_Cost_SelectedFG LONG                         !Selected forground color
orp:Sale_Cost_SelectedBG LONG                         !Selected background color
orp:Date_Received      LIKE(orp:Date_Received)        !List box control field - type derived from field
orp:Date_Received_NormalFG LONG                       !Normal forground color
orp:Date_Received_NormalBG LONG                       !Normal background color
orp:Date_Received_SelectedFG LONG                     !Selected forground color
orp:Date_Received_SelectedBG LONG                     !Selected background color
orp:TimeReceived       LIKE(orp:TimeReceived)         !List box control field - type derived from field
orp:TimeReceived_NormalFG LONG                        !Normal forground color
orp:TimeReceived_NormalBG LONG                        !Normal background color
orp:TimeReceived_SelectedFG LONG                      !Selected forground color
orp:TimeReceived_SelectedBG LONG                      !Selected background color
orp:DatePriceCaptured  LIKE(orp:DatePriceCaptured)    !List box control field - type derived from field
orp:DatePriceCaptured_NormalFG LONG                   !Normal forground color
orp:DatePriceCaptured_NormalBG LONG                   !Normal background color
orp:DatePriceCaptured_SelectedFG LONG                 !Selected forground color
orp:DatePriceCaptured_SelectedBG LONG                 !Selected background color
orp:TimePriceCaptured  LIKE(orp:TimePriceCaptured)    !List box control field - type derived from field
orp:TimePriceCaptured_NormalFG LONG                   !Normal forground color
orp:TimePriceCaptured_NormalBG LONG                   !Normal background color
orp:TimePriceCaptured_SelectedFG LONG                 !Selected forground color
orp:TimePriceCaptured_SelectedBG LONG                 !Selected background color
orp:All_Received       LIKE(orp:All_Received)         !List box control field - type derived from field
orp:Part_Ref_Number    LIKE(orp:Part_Ref_Number)      !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !List box control field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !List box control field - type derived from field
orp:Record_Number      LIKE(orp:Record_Number)        !List box control field - type derived from field
orp:DespatchNoteNumber LIKE(orp:DespatchNoteNumber)   !List box control field - type derived from field
orp:Order_Number       LIKE(orp:Order_Number)         !Browse key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Parts Orders File'),AT(0,0,679,428),FONT('Tahoma',8,,,CHARSET:ANSI),COLOR(0D6EAEFH),IMM,WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Browse The Parts Order File'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       OPTION('Order Type'),AT(452,42,112,20),USE(outstanding_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('All'),AT(532,50),USE(?outstanding_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                         RADIO('Outstanding'),AT(464,50),USE(?outstanding_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                       END
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(608,396),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       SHEET,AT(4,28,672,366),USE(?CurrentTab),FONT(,,,FONT:bold),COLOR(09A6A7CH),SPREAD
                         TAB,USE(?Tab:2)
                           PROMPT('Purchase Orders'),AT(8,32),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n12b),AT(51,44,64,10),USE(ord:Order_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Order Number')
                           LIST,AT(52,64,512,102),USE(?Browse:1),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('67L(2)|M~Order Number~@n~SS~010@127L(2)|M~Supplier~@s30@27L(2)|M~User~@s3@52R(2)' &|
   '|M~Date~C(0)@d6b@0L(2)|M~Printed~@s3@52L(2)|M~Received~@s3@0L(2)|M~Currency On O' &|
   'rdering~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(565,66),USE(?ButtonAuditTrail),TRN,FLAT,ICON('auditp.jpg')
                           BUTTON,AT(567,120),USE(?Delete:3),TRN,FLAT,LEFT,ICON('delordp.jpg')
                           PROMPT('By Part Number'),AT(8,178),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PANEL,AT(292,170,8,8),USE(?Panel1),FILL(COLOR:Red)
                           PROMPT(' - Items Not Received'),AT(304,170),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Shelf Location'),AT(408,174),USE(?STO:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(480,176,124,10),USE(sto:Shelf_Location),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@s30),AT(8,190,124,10),USE(orp:Part_Number),COLOR(COLOR:White),UPR
                           BUTTON('&Rev tags'),AT(160,178,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(212,176,70,13),USE(?DASSHOWTAG),HIDE
                           PANEL,AT(292,180,8,8),USE(?Panel1:2),FILL(COLOR:Green)
                           PROMPT(' - Items Not Priced'),AT(304,180),USE(?Prompt4:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('2nd Location'),AT(408,188),USE(?STO:Second_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(480,188,124,10),USE(sto:Second_Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PANEL,AT(292,190,8,8),USE(?Panel1:3),FILL(COLOR:Gray)
                           PROMPT(' - Items Received'),AT(304,190),USE(?Prompt4:3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,202,596,162),USE(?List),IMM,FONT(,,0101010H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@76L(2)|M*~Part Number~@s30@104L(2)|M*~Description~@s30@25L(2)|M*~Type' &|
   '~@s3@29R(2)|M*~Ref No~L@s6@38R(2)|M*~Quantity~@s8@32R(2)|M*~Rcvd~@s8@34C|M*~Curr' &|
   'ency~@s30@44R(2)|M*~Purch Price~@n7.2@42R(2)|M*~Selling Price~@n7.2@[48R(2)|*~Da' &|
   'te~@d6b@28R(2)|M*~Time~@t7b@]|M~Received~[48R(2)|M*~Date~@d17b@28R(2)|M*~Time~@t' &|
   '7b@]|M~Price Captured~0L(2)|M~All Rcvd~@s3@0L(12)|M~Part Ref Number~@n012@0L(12)' &|
   '|M~Second Location~@s30@0L(12)|M~Shelf Location~@s30@0D(12)|M~record number~L@n1' &|
   '2@0L(12)|M@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(608,200),USE(?Received_Button),TRN,FLAT,HIDE,ICON('recprtp.jpg')
                           BUTTON,AT(608,226),USE(?All_Recieved_Button),TRN,FLAT,HIDE,ICON('rectagpp.jpg')
                           BUTTON,AT(608,252),USE(?btnPrintGRNReceived),TRN,FLAT,ICON('prngrecp.jpg')
                           BUTTON,AT(608,286),USE(?buttonCapturePricing),TRN,FLAT,ICON('cappricp.jpg')
                           BUTTON,AT(608,314),USE(?buttonCaptureTaggedPricing),TRN,FLAT,ICON('captprip.jpg')
                           BUTTON,AT(608,340),USE(?Goods_Received_Note),TRN,FLAT,ICON('prngcapp.jpg')
                           BUTTON,AT(8,364),USE(?DASTAG),TRN,FLAT,HIDE,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(76,364),USE(?DASTAGAll),TRN,FLAT,HIDE,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(144,364),USE(?DASUNTAGALL),TRN,FLAT,HIDE,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(468,366),USE(?Button:CancelExchangeOrder),TRN,FLAT,ICON('canexchp.jpg')
                           BUTTON,AT(536,366),USE(?Delete_Part_Button),TRN,FLAT,ICON('delpordp.jpg')
                           BUTTON,AT(567,94),USE(?Reprint_Order_Button),TRN,FLAT,LEFT,ICON('rprnordp.jpg')
                         END
                       END
                       BUTTON,AT(4,398),USE(?Button14),DISABLE,TRN,FLAT,HIDE,LEFT,ICON('editqtyp.jpg')
                     END

BuildingWindow WINDOW,AT(,,124,21),FONT('Tahoma',8,,),COLOR(COLOR:Black),CENTER,GRAY,DOUBLE
       PROMPT('Building Parts List....'),AT(4,4),USE(?Prompt1),FONT(,12,COLOR:Lime,FONT:bold)
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'NO'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'ALL'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'NO'
BRW1::Sort2:StepClass StepClass                       !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'ALL'
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_orp_id   ushort,auto
save_par_id   ushort,auto
save_wpr_id   ushort,auto
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?cancel),LEFT,ICON('cancel.gif')
     END
    MAP
PrintGoodsReceivedNote          PROCEDURE(BYTE fType, DATE fDate)
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   glo:Queue.Pointer = orp:Record_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = orp:Record_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = orp:Record_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::11:QUEUE = glo:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = orp:Record_Number
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = orp:Record_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
GoodsReceivedNote          Routine
 ! #12342 Deprecated (DBH: 17/02/2012)
!    Goods_Received# = 0                                         ! Check if any parts have been received on this date
!
!    Access:ORDPARTS.ClearKey(ORP:Order_Number_key)
!    ORP:Order_Number = ORD:Order_Number
!    set(ORP:Order_Number_Key, ORP:Order_Number_Key)
!    loop until Access:ORDPARTS.Next()                           ! Loop thru parts on order - check at least one part
!        if orp:Order_Number <> ord:Order_Number then break.     ! has been received on the selected date.
!        if orp:DatePriceCaptured = tmp:Goods_Received_Date And orp:GRN_Number = 0
!            ! At least one part has been received, and is not already on a GRN - TrkBs: 5110 (DBH: 10-08-2005)
!            Goods_Received# = 1
!            break
!        end ! if
!    end ! loop
!
!    if Goods_Received# = 1
!        if not Access:GRNOTES.PrimeRecord()                     ! Create goods received note
!            grn:Order_Number        = ord:Order_Number
!            grn:Goods_Received_Date = tmp:Goods_Received_Date
!            ! Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
!            If GETINI('XML', 'CreateOracle',, Clip(Path()) & '\SB2KDEF.INI') <> 1
!                grn:BatchRunNotPrinted      = False
!            Else ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
!                grn:BatchRunNotPrinted      = True
!            End ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
!            ! End   - Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
!            if not Access:GRNOTES.TryUpdate()
!
!                ! Mark Parts!
!                FirstPart# = True
!                Access:ORDPARTS.ClearKey(ORP:Order_Number_key)
!                ORP:Order_Number = ORD:Order_Number
!                set(ORP:Order_Number_Key, ORP:Order_Number_Key)
!                loop until Access:ORDPARTS.Next()                           ! Loop thru parts on order - check at least one part
!                    if orp:Order_Number <> ord:Order_Number then break.     ! has been received on the selected date.
!                    if orp:DatePriceCaptured = tmp:Goods_Received_Date
!                        If orp:GRN_Number = 0
!                            If FirstPart# = True
!                                  ! Take the exchange rate from the first part.
!                                  ! All the parts should hopefully be the same for the same date - TrkBs: 5110 (DBH: 25-05-2005)
!                                grn:CurrencyCode   = orp:OrderedCurrency
!                                grn:DailyRate      = orp:OrderedDailyRate
!                                grn:DivideMultiply = orp:OrderedDivideMultiply
!                                Access:GRNOTES.Update()
!                                FirstPart# = False
!                            End ! FirstPart# = True
!                            orp:GRN_Number     = grn:Goods_Received_Number
!                            Access:OrdParts.Update()
!                        End ! If
!                    end ! if
!                end ! loop
!                glo:select1 = grn:Goods_Received_Number
!                glo:select2 = ''
!                glo:select3 = ''
!                Goods_Received_Note                             ! Print goods received note
!                glo:select1 = ''
!                glo:select2 = ''
!                glo:select3 = ''
!            end ! if
!        end ! if
!    ELSE
!        Beep(Beep:SystemHand)  ;  Yield()
!        Case Missive('No new items found.'&|
!            '|You must capture the pricing before you can print the GRN.','ServiceBase',|
!                       'mstop.jpg','/&OK') 
!        Of 1 ! &OK Button
!        End!Case Message
!    end ! if
Check_job_status             Routine
   access:jobs.clearkey(job:ref_number_key)
   job:ref_number  = orp:job_number
   If access:jobs.fetch(job:ref_number_key) = Level:Benign
       Case CheckParts('C')
       Of 1
           GetStatus(330, 0, 'JOB')
       Of 2
           GetStatus(335, 0, 'JOB')
       ! Start - If there is no stock in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
       Of 3
           GetStatus(340, 0, 'JOB')
       ! End   - If there is no stock in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
       OF 4
           ! All parts received - TrkBs: 4625 (DBH: 17-02-2005)
           GetStatus(345, 0, 'JOB')
       Else
           Case CheckParts('W')
           Of 1
               GetStatus(330, 0, 'JOB')
           Of 2
               GetStatus(335, 0, 'JOB')
           ! Start - If there is no stock in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
           Of 3
               GetStatus(340, 0, 'JOB')
           ! End   - If there is no stock in Main Store - TrkBs: 4625 (DBH: 18-08-2004 49)
           Of 4
               ! All parts received - TrkBs: 4625 (DBH: 17-02-2005)
               GetStatus(345, 0, 'JOB')
           Else
                        ! No parts attached - TrkBs: 3425 (DBH: 17-02-2005)
                        ! GetStatus(345,0,'JOB')
           End! Case CheckParts('W')
       End! Case CheckParts('C')
       end_date" = ''
       access:jobs.update()
   End ! If access:jobs.fetch(job:ref_number_key) = Level:Benign

Get_Record_Numbers      Routine
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:order_number_key)
        par:ref_number   = orp:job_number
        par:order_number = orp:order_number
        set(par:order_number_key,par:order_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number   <> orp:job_number      |
            or par:order_number <> orp:order_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If par:date_received = '' And par:part_number = orp:part_number
                parts_record_number_temp = par:record_number
                Break
            End!If par:date_received = '' And par:part_number = orp:part_number
        end !loop
        access:parts.restorefile(save_par_id)

        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:order_number_key)
        wpr:ref_number   = orp:job_number
        wpr:order_number = orp:order_number
        set(wpr:order_number_key,wpr:order_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number   <> orp:job_number      |
            or wpr:order_number <> orp:order_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If wpr:date_received = '' And wpr:part_number = orp:part_number
                warparts_record_number_temp = wpr:record_number
                Break
            End!If wpr:date_received = '' And wpr:part_number = orp:part_number
        end !loop
        access:warparts.restorefile(save_wpr_id)

        save_res_id = access:retstock.savefile()
        access:retstock.clearkey(res:order_number_key)
        res:ref_number  = orp:job_number
        res:order_number    = orp:order_number
        Set(res:order_number_key,res:order_number_key)
        Loop
            if access:retstock.next()
                break
            End
            if res:ref_number   <> orp:job_number   |
            or res:order_number <> orp:order_number |
                then break.
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If res:date_received = '' And res:part_number = orp:part_number
                retstock_record_number_temp = res:record_number
                Break
            End!If wpr:date_received = '' And wpr:part_number = orp:part_number
        End!Loop
        access:retstock.restorefile(save_res_id)

        setcursor()

AllReceived     Routine

    orp:record_number   = brw6.q.orp:record_number
    access:ordparts.clearkey(orp:record_number_key)
    orp:record_number = brw6.q.orp:record_number
    if access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
        Do check_job_status
    End!if access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
    found# = 0

    setcursor(cursor:wait)
    save_orp_id = access:ordparts.savefile()
    access:ordparts.clearkey(orp:order_number_key)
    orp:order_number    = brw1.q.ord:order_number
    set(orp:order_number_key,orp:order_number_key)
    loop
        if access:ordparts.next()
           break
        end !if
        if orp:order_number    <> brw1.q.ord:order_number      |
            then break.  ! end if
        If orp:all_received <> 'YES'
            found# = 1
            Break
        End
    end !loop
    access:ordparts.restorefile(save_orp_id)
    setcursor()

    if found# = 0
        Access:ORDERS.Clearkey(ord:Order_Number_Key)
        ord:Order_Number    = brw1.q.ord:Order_Number
        If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
            !Found
            ord:all_received = 'YES'
            access:orders.update()

            ! Auto print the GRV for the last parts received - TrkBs: 6301 (DBH: 31-08-2005)
!            tmp:Goods_Received_Date = today()
!            Do GoodsReceivedNote
            PrintGoodsReceivedNote(1,TODAY())  ! #12342 Call the new print procedure (DBH: 05/04/2012)
            ! End   - Auto print the GRV for the last parts received - TrkBs: 6301 (DBH: 31-08-2005)


            Case Missive('This order has been fulfilled as all parts have now been received.'&|
              '<13,10>'&|
              '<13,10>It will now be removed from the Outstanding Order List.','ServiceBase 3g',|
                           'midea.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive

        Else! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign

    End
    glo:select1 = ''
    glo:select2 = ''
    glo:select3 = ''
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                tmp:cancel = 1
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020089'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Parts_Orders')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:OrderNumberPrefix = GETINI('STOCK','OrderNumberPrefix',,Clip(Path()) & '\SB2KDEF.INI')
  If Clip(tmp:OrderNumberPrefix) = ''
      tmp:OrderNumberPrefix = 'SS'
  End ! If Clip(tmp:OrderNumberPrefix) = ''
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CURRENCY.Open
  Relate:DEFAULTS.Open
  Relate:GRNOTES.Open
  Relate:JOBS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:RTNORDER.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:PARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:JOBSTAGE.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:RETSTOCK.UseFile
  Access:SUPPLIER.UseFile
  SELF.FilesOpened = True
  !Last Despatch Number
  glo:select24 = ''
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORDERS,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:ORDPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Inserting (DBH 03/12/2007) # 9034 - Reformat the browse with prefix
  ?Browse:1{prop:Format} = '67L(2)|M~Order Number~@n~' & Clip(tmp:OrderNumberPRefix) & '~010@127L(2)|M~Supplier~@s30@27L(2)|M~User~@s3@52R(2)' &|
                 '|M~Date~C(0)@d6b@52L(2)|M~Received~@s3@0L(2)|M~Currency On ' &|
                 'Ordering~@s30@'
  ! End (DBH 03/12/2007) #9034
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Case def:SummaryOrders
      Of 0
          ?Received_Button{prop:hide} = 0
          ?All_Recieved_Button{prop:hide} = 0
          ?DASTAG{prop:hide} = 0
          ?DASTAGALL{prop:hide} = 0
          ?DASUNTAGALL{prop:hide} = 0
      Of 1
          !?ReceiveProcess{prop:hide} = 0
          brw6.ChangeControl = 0
  End!def:SummaryOrders
  
  !TB12777 - queue used
  free(glo:Queue2)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Parts_Orders')
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,ord:Received_Key)
  BRW1.AddRange(ord:All_Received,outstanding_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?ORD:Order_Number,ord:Order_Number,1,BRW1)
  BRW1.AddSortOrder(,ord:Order_Number_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?ord:Order_Number,ord:Order_Number,1,BRW1)
  BRW1.AddSortOrder(,ord:Order_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ord:Order_Number,ord:Order_Number,1,BRW1)
  BRW1.AddField(ord:Order_Number,BRW1.Q.ord:Order_Number)
  BRW1.AddField(ord:Supplier,BRW1.Q.ord:Supplier)
  BRW1.AddField(ord:User,BRW1.Q.ord:User)
  BRW1.AddField(ord:Date,BRW1.Q.ord:Date)
  BRW1.AddField(ord:Printed,BRW1.Q.ord:Printed)
  BRW1.AddField(ord:All_Received,BRW1.Q.ord:All_Received)
  BRW1.AddField(ord:OrderedCurrency,BRW1.Q.ord:OrderedCurrency)
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,orp:Part_Number_Key)
  BRW6.AddRange(orp:Order_Number,Relate:ORDPARTS,Relate:ORDERS)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?orp:Part_Number,orp:Part_Number,1,BRW6)
  BIND('tmp:tag',tmp:tag)
  BIND('type_temp',type_temp)
  BIND('job_number_temp',job_number_temp)
  BIND('tmp:Currency',tmp:Currency)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:tag,BRW6.Q.tmp:tag)
  BRW6.AddField(orp:Part_Number,BRW6.Q.orp:Part_Number)
  BRW6.AddField(orp:Description,BRW6.Q.orp:Description)
  BRW6.AddField(type_temp,BRW6.Q.type_temp)
  BRW6.AddField(job_number_temp,BRW6.Q.job_number_temp)
  BRW6.AddField(orp:Quantity,BRW6.Q.orp:Quantity)
  BRW6.AddField(orp:Number_Received,BRW6.Q.orp:Number_Received)
  BRW6.AddField(tmp:Currency,BRW6.Q.tmp:Currency)
  BRW6.AddField(orp:Purchase_Cost,BRW6.Q.orp:Purchase_Cost)
  BRW6.AddField(orp:Sale_Cost,BRW6.Q.orp:Sale_Cost)
  BRW6.AddField(orp:Date_Received,BRW6.Q.orp:Date_Received)
  BRW6.AddField(orp:TimeReceived,BRW6.Q.orp:TimeReceived)
  BRW6.AddField(orp:DatePriceCaptured,BRW6.Q.orp:DatePriceCaptured)
  BRW6.AddField(orp:TimePriceCaptured,BRW6.Q.orp:TimePriceCaptured)
  BRW6.AddField(orp:All_Received,BRW6.Q.orp:All_Received)
  BRW6.AddField(orp:Part_Ref_Number,BRW6.Q.orp:Part_Ref_Number)
  BRW6.AddField(sto:Second_Location,BRW6.Q.sto:Second_Location)
  BRW6.AddField(sto:Shelf_Location,BRW6.Q.sto:Shelf_Location)
  BRW6.AddField(orp:Record_Number,BRW6.Q.orp:Record_Number)
  BRW6.AddField(orp:DespatchNoteNumber,BRW6.Q.orp:DespatchNoteNumber)
  BRW6.AddField(orp:Order_Number,BRW6.Q.orp:Order_Number)
  BRW6.AddField(sto:Ref_Number,BRW6.Q.sto:Ref_Number)
  BRW6.AddField(sto:Part_Number,BRW6.Q.sto:Part_Number)
  BRW6.AddField(sto:Description,BRW6.Q.sto:Description)
  Case def:SummaryORders
      Of 1
          ?List{Prop:IconList,1} = ''
          ?List{Prop:IconList,2} = ''
  End !Case def:SummaryORders
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !Last Despatch Number
  Clear(glo:g_select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:CURRENCY.Close
    Relate:DEFAULTS.Close
    Relate:GRNOTES.Close
    Relate:JOBS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:RTNORDER.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Parts_Orders')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request = DeleteRecord
      setcursor(cursor:wait)
      save_orp_id = access:ordparts.savefile()
      access:ordparts.clearkey(orp:part_number_key)
      orp:order_number = ord:order_number
      set(orp:part_number_key,orp:part_number_key)
      loop
          if access:ordparts.next()
             break
          end !if
          if orp:order_number <> ord:order_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If orp:number_received <> 0
              found# = 1
              Break
          End
      end !loop
      access:ordparts.restorefile(save_orp_id)
      setcursor()
      If found# = 1
          Case Missive('Unable to delete.'&|
            '<13,10>'&|
            '<13,10>Some or all of the parts on this order have been received.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = 0
      End
  End!If request = DeleteRecord
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateORDERS
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Close
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      !TB12777 - queue used
      free(glo:Queue2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    OF ?Received_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
      !    ThisWindow.Update
      !
      !! Check       if selected part is for an exchange unit
      !    tmp:ExchangeUnit = false
      !
      !    recordNum# = orp:record_number
      !    Access:ORDPARTS.ClearKey(orp:record_number_key)
      !    orp:Record_Number = recordNum#
      !    if not Access:ORDPARTS.Fetch(orp:record_number_key)
      !        Access:STOCK.ClearKey(sto:Ref_Number_Key)
      !        sto:Ref_Number = orp:Part_Ref_Number
      !        if not Access:STOCK.Fetch(sto:Ref_Number_Key)
      !            if sto:ExchangeUnit = 'YES'
      !                tmp:ExchangeUnit = true
      !            end
      !        end
      !    end
      !
      !    if tmp:ExchangeUnit = true
      !        ! Exchange unit
      !        IF (orp:All_Received = 'YES')
      !            CYCLE
      !        END
      !        Rapid_Exchange_Unit(orp:record_number)  ! #12127 Start inserting straight away. (Bryan: 01/07/2011)
      !    else
      !    ! Standard stock item
      !        glo:select1 = ord:order_number
      !        glo:select2 = orp:record_number
      !        glo:Select3 = ''
      !
      !        Receive_Order(0)
      !    end
      !
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
    OF ?All_Recieved_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?All_Recieved_Button, Accepted)
      !Case Missive('This will mark ALL the tagged parts on this order as received.'&|
      !  '<13,10>'&|
      !  '<13,10>Are you sure?','ServiceBase 3g',|
      !               'mquest.jpg','\No|/Yes')
      !    Of 2 ! Yes Button
      !        ThisWindow.Update
      !
      !        tmp:ExchangeUnit = false
      !        tmp:StockItem = false
      !
      !        loop i# = 1 To records(glo:queue)
      !            get(glo:queue, i#)
      !            if (Error())
      !                break
      !            end
      !            Access:ORDPARTS.ClearKey(orp:record_number_key)
      !            orp:record_number = glo:queue.glo:pointer
      !            if Access:ORDPARTS.Fetch(orp:record_number_key) then cycle.
      !
      !            if (orp:All_Received = 'YES' OR orp:Date_Received <> '')   ! #12006 Don't include items already received. (Bryan: 24/08/2011)
      !                delete(glo:Queue)
      !                i# -= 1
      !                cycle
      !            end
      !
      !            Access:STOCK.ClearKey(sto:Ref_Number_Key)
      !            sto:Ref_Number = orp:Part_Ref_Number
      !            if not Access:STOCK.Fetch(sto:Ref_Number_Key)
      !                if sto:ExchangeUnit = 'YES'
      !                    tmp:ExchangeUnit = true
      !                else
      !                    tmp:StockItem = true
      !                    ! #11382 Check that the entered purchase cost is not over the price variant percentage (DBH: 09/08/2010)
      !                    priceVariant$ = getini('STOCK','PartPriceVariant',0,PATH() & '\SB2KDEF.INI')
      !                    if (priceVariant$ > 0)
      !                        if (orp:Purchase_Cost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100))))
      !                            If (PriceVariantCheck(sto:Part_Number,sto:Purchase_Cost,orp:Purchase_Cost) = FALSE)
      !                                ! Remove part from queue
      !                                Delete(glo:Queue)
      !                                i# -= 1
      !                                cycle
      !                            end ! If (PriceVariantCheck(sto:Purchase_Cost,tmp:PurchaseCost))
      !                        end ! if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100)))
      !                    end ! if (priceVariant$ > 0)
      !                end
      !            end
      !        end
      !
      !        ! #11382 Check to see if there are still parts to process (DBH: 09/08/2010)
      !        if (Records(glo:Queue) = 0)
      !            Do DASBRW::11:DASUNTAGALL
      !            cycle
      !        end
      !
      !        if tmp:ExchangeUnit = true
      !            Case Missive('This order contains exchange units. These must be received separately.','ServiceBase 3g',|
      !                           'mstop.jpg','/OK')
      !                Of 1 ! OK Button
      !            End ! Case Missive
      !            cycle
      !        end
      !
      !        if tmp:ExchangeUnit = false
      !            tmp:LabelType = 0
      !            set(DEFAULTS)
      !            Access:DEFAULTS.Next()
      !            If def:Receive_Stock_Label = 'YES'
      !                Case Missive('Label(s) will now be printed. Do you wish to print:'&|
      !                  '<13,10>'&|
      !                  '<13,10>A Label Per Line, or'&|
      !                  '<13,10>A Label Per Item?','ServiceBase 3g',|
      !                               'mquest.jpg','\No Labels|Per Item|Per Line')
      !                    Of 3 ! Per Line Button
      !                        tmp:LabelType = 1
      !                    Of 2 ! Per Item Button
      !                        tmp:LabelType = 2
      !                    Of 1 ! No Labels Button
      !                        tmp:LabelType = 0
      !                End ! Case Missive
      !            End!If def:stock_label = 'YES'
      !
      !            ! Inserting (DBH 03/04/2007) # 8046 - Ask if the user wants to print a bin label
      !            Case Missive('Barcode Label: '&|
      !              '|a) Print a label per order line.'&|
      !              '|b) Print a label per order item.'&|
      !              '|c) Specify number of labels per line.?','ServiceBase 3g',|
      !                           'mquest.jpg','\No Label|Option C|Option B|Option A')
      !                Of 4 ! Option A Button
      !                    tmp:BarcodeLabelType = 4
      !                Of 3 ! Option B Button
      !                    tmp:BarcodeLabelType = 3
      !                Of 2 ! Option C Button
      !                    tmp:BarcodeLabelNumber = SelectLabelQuantity(1)
      !                    tmp:BarcodeLabelType = 2
      !                Of 1 ! No Label Button
      !                    tmp:BarcodeLabelNumber = 0
      !            End ! Case Missive
      !            ! End (DBH 03/04/2007) #8046
      !
      !            glo:select2  = ''
      !            Receive_All_Parts
      !
      !            If glo:Select2 <> ''
      !                ! Despatch Note Number must be filled - TrkBs: 6264 (DBH: 07-09-2005)
      !                ThisWindow.Update
      !
      !                setcursor(cursor:wait)
      !
      !                recordspercycle     = 25
      !                recordsprocessed    = 0
      !                percentprogress     = 0
      !                setcursor(cursor:wait)
      !                open(progresswindow)
      !                progress:thermometer    = 0
      !                ?progress:pcttext{prop:text} = '0% Completed'
      !
      !                recordstoprocess    = records(glo:queue)
      !
      !                Loop x# = 1 To Records(glo:queue)
      !                    get(glo:queue,x#)
      !                    access:ordparts.clearkey(orp:record_number_key)
      !                    orp:record_number   = glo:pointer
      !                    If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
      !                        !Found
      !
      !                    Else! If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
      !                        !Error
      !                    End! If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
      !
      !                    Do GetNextRecord2
      !                    If orp:all_received = 'YES'
      !                        Cycle
      !                    End!If orp:all_received = 'YES'
      !
      !                    Do check_job_status
      !
      !                    ! Fill in the currency details - TrkBs: 5110 (DBH: 18-05-2005)
      !                    ! Inserting (DBH 31/01/2006) #7106 - Do not lookup currency details, if none were filled in at point of order
      !                    Access:ORDERS.ClearKey(ord:Order_Number_Key)
      !                    ord:Order_Number = orp:Order_Number
      !                    If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
      !                        !Found
      !                        If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
      !                        ! End (DBH 31/01/2006) #7106
      !
      !                            Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
      !                            sup:Company_Name = ord:Supplier
      !                            If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
      !                                !Found
      !                                If sup:UseForeignCurrency
      !                                    Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
      !                                    cur:CurrencyCode = sup:CurrencyCode
      !                                    If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
      !                                        !Found
      !                                        orp:ReceivedCurrency = cur:CurrencyCode
      !                                        orp:ReceivedDailyRate   = cur:DailyRate
      !                                        orp:ReceivedDivideMultiply = cur:DivideMultiply
      !                                    Else !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
      !                                        !Error
      !                                    End !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
      !                                End ! If sup:UseForeignCurrency
      !                            Else !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
      !                                !Error
      !                            End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
      !                        Else ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
      !                            orp:ReceivedCurrency = ''
      !                            orp:ReceivedDailyRate = ''
      !                            orp:ReceivedDivideMultiply = ''
      !                        End !If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
      !                    Else !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
      !                        !Error
      !                    End !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
      !                    ! End   - Fill in the currency details - TrkBs: 5110 (DBH: 18-05-2005)
      !
      !                    !orp:all_received = 'YES'
      !                    orp:date_received = Today()
      !                    orp:TimeReceived = CLOCK()
      !                    !J - added
      !                    orp:Number_Received = orp:Quantity
      !                    orp:DespatchNoteNumber = glo:select2
      !                    access:ordparts.update()
      !
      !                    parts_record_number_temp = ''
      !                    warparts_record_number_temp = ''
      !                    Do Get_Record_Numbers
      !
      !                    quantity_to_job$ = 0
      !                            Access:STOCK.ClearKey(sto:Ref_Number_Key)
      !                            sto:Ref_Number = orp:Part_Ref_Number
      !                            If Access:Stock.fetch(sto:Ref_Number_Key)
      !                                    !Error
      !                                    Case Missive('Error! Unable to retreive the Stock Part''s Details.','ServiceBase 3g',|
      !                                                   'mstop.jpg','/OK')
      !                                        Of 1 ! OK Button
      !                                    End ! Case Missive
      !                                    StockPart# = 0
      !                            End!If Access:Stock.fetch(sto:Ref_Number_Key)
      !                            If StockPart# = 1
      !                                sto:quantity_stock += orp:quantity
      !                                sto:quantity_on_order -= orp:quantity
      !                                If sto:quantity_on_order < 0
      !                                    sto:quantity_on_order = 0
      !                                End
      !                                If sto:quantity_stock < 0
      !                                    sto:quantity_stock = 0
      !                                End!If sto:quantity_stock < 0
      !
      !                                access:stock.update()
      !                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
      !                                                   'ADD', | ! Transaction_Type
      !                                                   glo:Select2, | ! Depatch_Note_Number
      !                                                   0, | ! Job_Number
      !                                                   0, | ! Sales_Number
      !                                                   orp:Quantity, | ! Quantity
      !                                                   sto:Purchase_Cost, | ! Purchase_Cost
      !                                                   sto:Sale_Cost, | ! Sale_Cost
      !                                                   sto:Retail_Cost, | ! Retail_Cost
      !                                                   'STOCK ADDED FROM ORDER', | ! Notes
      !                                                   'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier)) ! Information
      !                                  ! Added OK
      !
      !                                Else ! AddToStockHistory
      !                                  ! Error
      !                                End ! AddToStockHistory
      !!                                glo:ErrorText = ''
      !!                                sto:Purchase_Cost   = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_Stock,sto:Purchase_Cost),@n14.2)
      !!                                If glo:ErrorText <> ''
      !!                                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
      !!                                                       'ADD', | ! Transaction_Type
      !!                                                       '', | ! Depatch_Note_Number
      !!                                                       0, | ! Job_Number
      !!                                                       0, | ! Sales_Number
      !!                                                       0, | ! Quantity
      !!                                                       sto:Purchase_Cost, | ! Purchase_Cost
      !!                                                       sto:Sale_Cost, | ! Sale_Cost
      !!                                                       sto:Retail_Cost, | ! Retail_Cost
      !!                                                       'AVERAGE COST CALCULATION', | ! Notes
      !!                                                       Clip(glo:ErrorText)) ! Information
      !!                                      ! Added OK
      !!                                    Else ! AddToStockHistory
      !!                                      ! Error
      !!                                    End ! AddToStockHistory
      !!                                End !If glo:ErrorText <> ''
      !!                                glo:ErrorText = ''
      !                                Access:STOCK.Update()
      !                            end !if access:stock.fetch(sto:ref_number_key)
      !        !                Of 'RET'
      !        !                    orp:allocated_to_sale = 'NO'
      !        !                    access:ordparts.update()
      !
      !!                    End!Case orp:part_type
      !
      !                    Case tmp:LabelType
      !                        Of 1
      !                            glo:select1  = orp:part_ref_number
      !                            Case def:label_printer_type
      !                                Of 'TEC B-440 / B-442'
      !                                    Stock_Label(orp:Date_Received,orp:Quantity,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
      !                                Of 'TEC B-452'
      !                                    Stock_Label_B452(orp:Date_Received,orp:Quantity,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
      !                            End!Case def:label_printer_type
      !                            glo:select1 = ''
      !                        Of 2
      !                            Loop looptemp# = 1 To orp:Quantity
      !                                glo:select1  = orp:part_ref_number
      !                                Case def:label_printer_type
      !                                    Of 'TEC B-440 / B-442'
      !                                        Stock_Label(orp:Date_Received,1,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
      !                                    Of 'TEC B-452'
      !                                        Stock_Label_B452(orp:Date_Received,1,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
      !                                End!Case def:label_printer_type
      !                            End!Loop x# = 1 To no_of_labels_temp
      !                            glo:select1 = ''
      !                    End!Case tmp:LabelType
      !
      !                    ! Inserting (DBH 04/04/2007) # 8046 - Print label per option
      !                    Case tmp:BarCodeLabelType
      !                    Of 4 ! Label Per Line
      !                        StockBarcodeLabel(orp:Record_Number,1)
      !                    Of 3
      !                        StockBarcodeLabel(orp:Record_Number,orp:Quantity)
      !                    Of 2
      !                        If tmp:BarcodeLabelNumber > 0
      !                            StockBarcodeLabel(orp:Record_Number,tmp:BarcodeLabelNumber)
      !                        End ! If tmp:BarcodeLabelNumber > 0
      !
      !                    End ! Case tmp:BarCodeLabelType
      !                    ! End (DBH 04/04/2007) #8046
      !
      !                End!Loop x# = 1 To Records(glo:queue)
      !                setcursor()
      !                close(progresswindow)
      !
      !                glo:select2  = ''
      !                Do DASBRW::11:DASUNTAGALL
      !                Do AllReceived
      !            End ! If glo:Select2 = ''
      !
      !        end
      !    Of 1 ! No Button
      !End ! Case Missive
      IF (Records(glo:Queue) = 0)
          CYCLE
      END
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Do you want to receive all the Tagged Items on this order?','ServiceBase',|
          'mquest.jpg','\&No|/&Yes') 
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      
      ERROR# = 0
      LOOP ll# = 1 TO RECORDS (glo:Queue)
          GET(glo:Queue,ll#)
          IF (ERROR())
              BREAK
          END
          
          Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
          orp:Record_Number = glo:Pointer
          IF (Access:ORDPARTS.Tryfetch(orp:Record_Number_Key))
              CYCLE
          END
          
          IF (orp:All_Received = 'YES' OR orp:Date_Received <> '')
              DELETE(glo:Queue)
              ll# -= 1
              CYCLE
          END
          
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number = orp:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
              CYCLE
          END
          
          IF (StockInUse(1))
              DELETE(glo:Queue)
              ll# -= 1
              CYCLE
          END
          
          
          IF (sto:ExchangeUnit = 'YES')
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You have tagged Exchange Units Orders. These must be received individually.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              ERROR# = 1
              BREAK
          END
          
      END
      IF (ERROR# = 1)
          CYCLE
      END
      
      IF (RECORDS(glo:Queue) = 0)
          ! There is nothing left tagged
          DO DASBRW::11:DASUNTAGALL
          CYCLE
      END
      
      
      glo:select2  = ''
      Receive_All_Parts
      
      If glo:Select2 <> ''
          BRW1.UpdateViewRecord()
          
          LOOP ll# = 1 TO RECORDS(glo:Queue)
              GET(glo:Queue,ll#)
              Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
              orp:Record_Number = glo:Pointer
              IF (Access:ORDPARTS.Tryfetch(orp:Record_Number_Key))
                  CYCLE
              END
              
              IF (orp:All_Received = 'YES' OR orp:Date_Received <> '')
                  CYCLE
              END
          
              orp:Date_Received = TODAY()
              orp:TimeReceived = CLOCK()
              orp:Number_Received = orp:Quantity
              orp:DespatchNoteNumber = glo:Select2
              IF (Access:ORDPARTS.TryUpdate() = Level:Benign)
              
              END  ! IF (Access:ORDPARTS.TryUpdate() = Level:Benign)
          END ! LOOP ll# = 1 TO RECORDS(glo:Queue)
          glo:Select2 = ''
          DO DASBRW::11:DASUNTAGALL
          DO AllReceived
      END
      BRW1.ResetQueue(1)
      BRW6.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?All_Recieved_Button, Accepted)
    OF ?Reprint_Order_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
      ThisWindow.Update
      Clear(glo:q_partsorder)
      Free(glo:q_partsorder)
      glo:select1    = ord:order_number
      !Neils Bit - because someone omitted it....tum te tum
      Access:OrdParts.ClearKey(ORP:Order_Number_key)
      ORP:Order_Number = ORD:Order_Number
      SET(ORP:Order_Number_Key,ORP:Order_Number_Key)
      LOOP
        IF Access:OrdParts.Next()
          BREAK
        END
        IF ORP:Order_Number <> ORD:Order_Number
          BREAK
        END
        glo:q_order_number      = orp:order_number
        glo:q_part_number       = orp:part_number
        glo:q_description       = orp:description
        glo:q_supplier          = ord:supplier
        glo:q_purchase_cost     = orp:purchase_cost
        glo:q_sale_cost         = orp:sale_cost
      
        Sort(glo:q_partsorder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
        Get(glo:q_partsorder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
        If Error()
            glo:q_quantity          = orp:quantity
            Add(glo:q_partsorder)
        Else
            glo:q_quantity          += orp:quantity
            Put(glo:q_partsorder)
        End
      END
      !Bryans bit again
      ord:printed = 'NO'
      Access:orders.update()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020089'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020089'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020089'&'0')
      ***
    OF ?outstanding_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?outstanding_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?outstanding_temp, Accepted)
    OF ?Browse:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Accepted)
      Do DASBRW::11:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Accepted)
    OF ?ButtonAuditTrail
      ThisWindow.Update
      ViewPendAudTrail(ord:order_number)
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Received_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
      ! Check if selected part is for an exchange unit
      Brw6.UpdateViewRecord()
      IF (orp:All_Received = 'YES')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected line has been received.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      exchangeUnit# = 0
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = orp:Part_Ref_Number
      IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign)
          IF (sto:ExchangeUnit = 'YES')
              exchangeUnit# = 1
              Rapid_Exchange_Unit(orp:Record_Number) ! #12127 Start inserting straight away. (Bryan: 01/07/2011)
          END ! IF (sto:ExchangeUnit = 'YES')
      END ! IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign)
      
      IF (exchangeUnit# = 0)
          Receive_Order(orp:Record_Number,0)
      END
      
      !comented 12127 / VODA000617  version 11.6.
      !if glo:select3 = 'NEW ORDER'
      !    access:ordparts_alias.clearkey(orp_ali:record_number_key)
      !    orp_ali:record_number = glo:select4
      !    if access:ordparts_alias.fetch(orp_ali:record_number_key) = Level:Benign
      !        get(ordparts,0)
      !        if access:ordparts.primerecord() = level:benign
      !            orp:order_number    = orp_ali:order_number
      !            orp:part_ref_number = orp_ali:part_ref_number
      !            orp:quantity        = glo:select5
      !            orp:part_number     = orp_ali:part_number
      !            orp:description     = orp_ali:description
      !            orp:purchase_cost   = orp_ali:purchase_cost
      !            orp:sale_cost       = orp_ali:sale_cost
      !            orp:job_number      = orp_ali:job_number
      !            orp:number_received = orp:quantity
      !            orp:date_received   = glo:Select6
      !            orp:part_type       = orp_ali:part_type
      !            orp:all_received    = 'YES'
      !            ! Save the Despatch Note Number - TrkBs: 6444 (DBH: 03-10-2005)
      !            orp:DespatchNoteNumber = glo:Select7
      !            orp:OrderedCurrency       = orp_ali:OrderedCurrency
      !            orp:OrderedDailyRate      = orp_ali:OrderedDailyRate
      !            orp:OrderedDivideMultiply = orp_ali:OrderedDivideMultiply
      !            ! Inserting (DBH 28/11/2005) #6789 - Get the passed "received" currency details.
      !            orp:ReceivedCurrency       = glo:Select8
      !            orp:ReceivedDailyRate      = glo:Select9
      !            orp:ReceivedDivideMultiply = glo:Select10
      !            ! End (DBH 11/28/2005) #6789
      !            if access:ordparts.insert()
      !                access:ordparts.cancelautoinc()
      !            end
      !        end!if access:ordparts.primerecord() = level:benign
      !
      !        parts_record_number_temp = ''
      !        warparts_record_number_temp = ''
      !        Do Get_Record_Numbers
      !        found# = 0
      !        If orp_ali:part_type = 'JOB'
      !            access:parts.clearkey(par:RecordNumberKey)
      !            par:record_number = parts_record_number_temp
      !            if access:parts.fetch(par:RecordNumberKey) = Level:Benign
      !                par:quantity = glo:select5
      !                par:date_received = Today()
      !                access:parts.update()
      !            end!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
      !            access:parts_alias.clearkey(par_ali:RecordNumberKey)
      !            par_ali:record_number = parts_record_number_temp
      !            if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:benign
      !                Get(parts,0)
      !                If access:parts.primerecord() = Level:Benign
      !                    record_number$           = par:record_number
      !                    par:record              :=: par_ali:record
      !                    par:record_number        = record_number$
      !                    par:quantity             = orp_ali:quantity
      !                    par:date_ordered         = Today()
      !                    par:date_received        = ''
      !                    par:order_part_number    = orp_ali:record_number
      !                    access:parts.insert()
      !                End!If access:parts.primerecord() = Level:Benign
      !            end !if access:parts.fetch(par:RecordNumberKey) = Level:Benign
      !        End !If orp_ali:part_type = 'JOB'
      !        If orp_ali:part_type = 'WAR'
      !            access:warparts.clearkey(wpr:RecordNumberKey)
      !            wpr:record_number = warparts_record_number_temp
      !            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
      !                wpr:quantity = glo:select5
      !                wpr:date_received = Today()
      !                access:warparts.update()
      !            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
      !            access:warparts_alias.clearkey(war_ali:RecordNumberKey)
      !            war_ali:record_number = warparts_record_number_temp
      !            if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:Benign
      !                If access:warparts.primerecord() = Level:Benign
      !                    record_number$           = wpr:record_number
      !                    wpr:record              :=: war_ali:record
      !                    wpr:record_number        = record_number$
      !                    wpr:quantity             = orp_ali:quantity
      !                    wpr:date_ordered         = Today()
      !                    wpr:date_received        = ''
      !                    wpr:order_part_number    = orp_ali:record_number
      !                    access:warparts.insert()
      !                End!If access:warparts.primerecord() = Level:Benign
      !            end!if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:Benign
      !        End !If orp_ali:part_type = 'WAR
      !        If orp_ali:part_type = 'RET'
      !            access:retstock.clearkey(res:record_number_key)                                 !Make part on old order as received
      !            res:record_number = retstock_record_number_temp
      !            if access:retstock.tryfetch(res:record_number_key) = Level:Benign
      !                res:despatched = 'OLD'
      !                access:retstock.update()
      !            End!if access:retstock.tryfetch(res:record_number_key) = Level:Benign
      !            access:retstock_alias.clearkey(ret_ali:record_number_key)
      !            ret_ali:record_number = retstock_record_number_temp
      !            if access:retstock_alias.tryfetch(ret_ali:record_number_key) = Level:Benign     !Get the old part's alias
      !                access:retsales_alias.clearkey(res_ali:ref_number_key)
      !                res_ali:ref_number = res:ref_number
      !                if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign    !Get the old Parts' Sale Alias
      !                    get(retsales,0)
      !                    if access:retsales.primerecord() = Level:Benign
      !                        ref_number$    = ret:ref_number
      !                        ret:record    :=: res_ali:record
      !                        ret:Ref_number    = ref_number$
      !                        ret:consignment_number = ''
      !                        ret:date_despatched = ''
      !                        ret:despatched  = ''
      !                        ret:despatch_number = ''
      !                        ret:invoice_number  = ''
      !                        ret:invoice_date    = ''
      !                        ret:invoice_courier_cost    = ''
      !                        ret:invoice_parts_cost      = ''
      !                        ret:invoice_sub_total       = ''
      !                        if access:retsales.insert()                                         !Create an new sale from the old one
      !                            access:retsales.cancelautoinc()
      !                        Else!if access:retsales.insert()
      !                            parts_cost$ = 0
      !                            get(retstock,0)
      !                            if access:retstock.primerecord() = Level:Benign                 !Create a new part from the old one
      !                                record_number$    = res:record_number
      !                                res:record    :=: ret_ali:record
      !                                res:ref_number  = ret:ref_number
      !                                res:record_number    = record_number$
      !                                res:date_received   = Today()
      !                                res:quantity        = glo:select5
      !                                res:despatched  = 'YES'
      !                                parts_cost$     = res:quantity * res:item_cost
      !                                if access:retstock.insert()
      !                                    access:retstock.cancelautoinc()
      !                                end
      !                            end!if access:retstock.primerecord() = Level:Benign
      !                            get(retstock,0)
      !                            if access:retstock.primerecord() = Level:Benign                 !Create a new part from the old one
      !                                record_number$    = res:record_number
      !                                res:record    :=: ret_ali:record
      !                                res:ref_number  = ret:ref_number
      !                                res:record_number    = record_number$
      !                                res:quantity        = orp_ali:quantity
      !                                res:date_ordered        = Today()
      !                                res:despatched  = 'ORD'
      !                                res:order_part_number    = orp_ali:record_number
      !                                if access:retstock.insert()
      !                                    access:retstock.cancelautoinc()
      !                                end
      !                                access:retsales.clearkey(ret:ref_number_key)
      !                                ret:ref_number  = ref_number$
      !                                If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      !                                    ret:parts_cost  = res:quantity * res:item_cost + parts_cost$
      !                                    ret:sub_total   = ret:parts_cost    + ret:courier_cost
      !                                    access:retsales.update()
      !                                End!If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      !
      !                            end!if access:retstock.primerecord() = Level:Benign
      !                        End!if access:retsales.insert()
      !                    end!if access:retsales.primerecord() = Level:Benign
      !                End!if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign
      !            End!if access:retstock_alias.tryfetch(ret_ali:record_number_key) = Level:Benign
      !
      !        End !If orp_ali:part_type = 'WAR
      !
      !    End
      !End
      ! All received?
      Do DASBRW::11:DASUNTAGALL
      
      If brw1.q.ord:All_Received <> 'YES'
          Do AllReceived
      End !brw1.q.ord:All_Received <> 'YES'
      BRW1.ResetQueue(1)
      BRW6.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
    OF ?btnPrintGRNReceived
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnPrintGRNReceived, Accepted)
      ThisWindow.Update
      BRW1.UpdateBuffer()
      
      If ord:Order_Number <> 0
          tmp:Goods_Received_Date = today()
      
          if Goods_Received_Note_Window(tmp:Goods_Received_Date)          ! Call window prompting for received date
      
              !Do GoodsReceivedNote
              PrintGoodsReceivedNote(0,tmp:Goods_Received_Date)
      
          end ! if
      End ! ord:Order_Number <> 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnPrintGRNReceived, Accepted)
    OF ?buttonCapturePricing
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonCapturePricing, Accepted)
      ! Check if selected part is for an exchange unit
      Brw6.UpdateViewRecord()
      exchangeUnit# = 0
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = orp:Part_Ref_Number
      IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign)
          IF (sto:ExchangeUnit = 'YES')
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error! The selected line is an exchange unit order.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END ! IF (sto:ExchangeUnit = 'YES')
      END ! IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign)
      
      IF (orp:Date_Received = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Quantities must be received first before pricing can be captured.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      !TB12777 - need to check the invoice number before receiving
      !if all received then this will be viewing a completed order
      if orp:all_received <> 'YES'
          !has this one been done already in this session?
          glo:Queue2.GLO:Pointer2 =  orp:DespatchNoteNumber
          Get(glo:Queue2,glo:Queue2.GLO:Pointer2)
          if error() then
              !not found
              Case CheckDepatchNoteNumber(orp:DespatchNoteNumber,orp:Description)
                  of 'OK'
                      !OK
                      !make note we have seen this number
                      glo:Queue2.GLO:Pointer2 =  orp:DespatchNoteNumber
                      add(glo:Queue2)
      
                  of 'CANCEL'
                      !wrong and no change to make
                      cycle
                  ELSE
                      !override all existing parts to this DNN now in glo:ErrorText
                      !remember the details we are supposed to be using
                      Save_ordpart_number = Orp:Record_number
                      Save_ordPart_order  = orp:Order_Number
                      Save_orpart_DNN     = orp:DespatchNoteNumber
                      Access:ordparts.clearkey(orp:Order_Number_Key)
                      orp:Order_Number = Save_Ordpart_order
                      set(orp:Order_Number_Key,orp:record_number_key)
                      Loop
                          If access:ordparts.next() then break.
                          if orp:Order_Number <> Save_Ordpart_order then break.
                          if orp:DespatchNoteNumber = Save_orpart_DNN then
                              orp:DespatchNoteNumber = clip(Glo:ErrorText)
                              Access:Ordparts.update()
                          END
                      END !loop through ordparts on the same order
      
                      !refetch the original
                      Access:ordparts.clearkey(orp:record_number_key)
                      orp:Record_Number = Save_ordpart_number
                      Access:ordparts.fetch(orp:record_number_key)
      
                      !make note we have seen this number
                      glo:Queue2.GLO:Pointer2 =  orp:DespatchNoteNumber
                      add(glo:Queue2)
      
              END !check DespatchNoteNumber
          END !if not found in queue
      END !if all received <> YES
      
      Receive_Order(orp:Record_Number,1)
      
      ! All received?
      Do DASBRW::11:DASUNTAGALL
      
      If brw1.q.ord:All_Received <> 'YES'
          Do AllReceived
      End !brw1.q.ord:All_Received <> 'YES'
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonCapturePricing, Accepted)
    OF ?buttonCaptureTaggedPricing
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonCaptureTaggedPricing, Accepted)
      IF (Records(glo:Queue) = 0)
          CYCLE
      END
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Do you want to Capture the Price for all the Tagged Items on this order?','ServiceBase',|
          'mquest.jpg','\&No|/&Yes') 
      Of 2 ! &Yes Button
      Of 1 ! &No Button
          CYCLE
      End!Case Message
      
      
      
      ERROR# = 0
      LOOP ll# = 1 TO RECORDS (glo:Queue)
          GET(glo:Queue,ll#)
          IF (ERROR())
              BREAK
          END
          
          Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
          orp:Record_Number = glo:Pointer
          IF (Access:ORDPARTS.Tryfetch(orp:Record_Number_Key))
              CYCLE
          END
          
          IF (orp:All_Received = 'YES' OR orp:DatePriceCaptured <> '' OR orp:Date_Received = '')
              DELETE(glo:Queue)
              ll# -= 1
              CYCLE
          END
          
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number = orp:Part_Ref_Number
          IF (Access:STOCK.TryFetch(sto:Ref_Number_Key))
              CYCLE
          END
          
          IF (StockInUse(1))
              DELETE(glo:Queue)
              ll# -= 1
              CYCLE
          END
      
          
          IF (sto:ExchangeUnit = 'YES')
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You have tagged Exchange Units Orders. These must be received individually.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              ERROR# = 1
              BREAK
          ELSE
              ! #11382 Check that the entered purchase cost is not over the price variant percentage (DBH: 09/08/2010)
              priceVariant$ = getini('STOCK','PartPriceVariant',0,PATH() & '\SB2KDEF.INI')
              if (priceVariant$ > 0)
                  if (orp:Purchase_Cost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100))))
                      If (PriceVariantCheck(sto:Part_Number,sto:Purchase_Cost,orp:Purchase_Cost) = FALSE)
                                  ! Remove part from queue
                          Delete(glo:Queue)
                          i# -= 1
                          cycle
                      end ! If (PriceVariantCheck(sto:Purchase_Cost,tmp:PurchaseCost))
                  end ! if (tmp:PurchaseCost > (sto:Purchase_Cost + (sto:Purchase_Cost * (priceVariant$/100)))
              end ! if (priceVariant$ > 0)            
          END
      
          !TB12777 - need to check the invoice number before receiving
          if orp:all_received <> 'YES' then       !not all received
              !has this one been done already in this session?
              glo:Queue2.GLO:Pointer2 =  orp:DespatchNoteNumber
              Get(glo:Queue2,glo:Queue2.GLO:Pointer2)
              if error() then
                  !not found
                  Case CheckDepatchNoteNumber(orp:DespatchNoteNumber,orp:description)
                      of 'OK'
                          !OK
                          !make note we have seen this number
                          glo:Queue2.GLO:Pointer2 =  orp:DespatchNoteNumber
                          add(glo:Queue2)
                      of 'CANCEL'
                          !wrong and no change to make
                          Delete(glo:Queue)
                          i# -= 1
                          cycle
                      ELSE
                          !override all existing parts to this DNN now in glo:ErrorText
                          !remember the details we are supposed to be using
                          Save_ordpart_number = Orp:Record_number
                          Save_ordPart_order  = orp:Order_Number
                          Save_orpart_DNN     = orp:DespatchNoteNumber
                          Access:ordparts.clearkey(orp:Order_Number_Key)
                          orp:Order_Number = Save_Ordpart_order
                          set(orp:Order_Number_Key,orp:record_number_key)
                          Loop
                              If access:ordparts.next() then break.
                              if orp:Order_Number <> Save_Ordpart_order then break.
                              if orp:DespatchNoteNumber = Save_orpart_DNN then
                                  orp:DespatchNoteNumber = clip(Glo:ErrorText)
                                  Access:Ordparts.update()
                              END
                          END !loop through ordparts on the same order
      
                          !refetch the original
                          Access:ordparts.clearkey(orp:record_number_key)
                          orp:Record_Number = Save_ordpart_number
                          Access:ordparts.fetch(orp:record_number_key)
      
                          !make note we have seen this number
                          glo:Queue2.GLO:Pointer2 =  orp:DespatchNoteNumber
                          add(glo:Queue2)
      
                  END !check DespatchNoteNumber
      
              END !if used before on this session
          END !orp:all_received = 'YES'
      END
      IF (ERROR# = 1)
          CYCLE
      END
      
      IF (RECORDS(glo:Queue) = 0)
          ! There is nothing left tagged
          DO DASBRW::11:DASUNTAGALL
          CYCLE
      END
      
      BRW1.UpdateViewRecord()
      Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
      sup:Company_Name = ord:Supplier
      IF (Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign)
          IF (sup:UseForeignCurrency)
              Access:CURRENCY.ClearKey(cur:CurrencyCodeKey)
              cur:CurrencyCode = sup:CurrencyCode
              If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                      !Found
      
              Else !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
                                      !Error
              End !If Access:CURRENCY.TryFetch(cur:CurrencyCodeKey) = Level:Benign
          END
      END
      
      tmp:LabelType = 0
      set(DEFAULTS)
      Access:DEFAULTS.Next()
      If def:Receive_Stock_Label = 'YES'
          Case Missive('Label(s) will now be printed. Do you wish to print:'&|
              '<13,10>'&|
              '<13,10>A Label Per Line, or'&|
              '<13,10>A Label Per Item?','ServiceBase 3g',|
              'mquest.jpg','\No Labels|Per Item|Per Line')
          Of 3 ! Per Line Button
              tmp:LabelType = 1
          Of 2 ! Per Item Button
              tmp:LabelType = 2
          Of 1 ! No Labels Button
              tmp:LabelType = 0
          End ! Case Missive
      End!If def:stock_label = 'YES'
      
              ! Inserting (DBH 03/04/2007) # 8046 - Ask if the user wants to print a bin label
      Case Missive('Barcode Label: '&|
          '|a) Print a label per order line.'&|
          '|b) Print a label per order item.'&|
          '|c) Specify number of labels per line.?','ServiceBase 3g',|
          'mquest.jpg','\No Label|Option C|Option B|Option A')
      Of 4 ! Option A Button
          tmp:BarcodeLabelType = 4
      Of 3 ! Option B Button
          tmp:BarcodeLabelType = 3
      Of 2 ! Option C Button
          tmp:BarcodeLabelNumber = SelectLabelQuantity(1)
          tmp:BarcodeLabelType = 2
      Of 1 ! No Label Button
          tmp:BarcodeLabelNumber = 0
      End ! Case Missive
              ! End (DBH 03/04/2007) #8046
      
      LOOP ll# = 1 TO RECORDS(glo:Queue)
          GET(glo:Queue,ll#)
          Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
          orp:Record_Number = glo:Pointer
          IF (Access:ORDPARTS.Tryfetch(orp:Record_Number_Key))
              CYCLE
          END
          
          IF (orp:All_Received = 'YES' OR orp:DatePriceCaptured <> '')
              CYCLE
          END
      
          IF (CLIP(ord:OrderedCurrency) <> '' AND ord:OrderedCurrency <> '0')
              IF (sup:UseForeignCurrency)
                  orp:ReceivedCurrency = cur:CurrencyCode
                  orp:ReceivedDailyRate   = cur:DailyRate
                  orp:ReceivedDivideMultiply = cur:DivideMultiply
              END
          ELSE
              orp:ReceivedCurrency = ''
              orp:ReceivedDailyRate   = ''
              orp:ReceivedDivideMultiply = ''
          END
          orp:DatePriceCaptured = TODAY()
          orp:TimePriceCaptured = CLOCK()
          orp:All_Received = 'YES'
          IF (Access:ORDPARTS.TryUpdate() = Level:Benign)
              Access:STOCK.Clearkey(sto:Ref_Number_Key)
              sto:Ref_Number = orp:Part_Ref_Number
              IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key))
                  CYCLE
              END
              sto:Quantity_Stock += orp:Quantity
              sto:Quantity_On_Order -= orp:Quantity
              IF (sto:Quantity_On_Order < 0)
                  sto:Quantity_On_Order = 0
              END
      
              IF (Access:STOCK.TryUpdate() = Level:Benign)
                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                      'ADD', | ! Transaction_Type
                      orp:DespatchNoteNumber, | ! Depatch_Note_Number
                      0, | ! Job_Number
                      0, | ! Sales_Number
                      orp:Quantity, | ! Quantity
                      sto:Purchase_Cost, | ! Purchase_Cost
                      sto:Sale_Cost, | ! Sale_Cost
                      sto:Retail_Cost, | ! Retail_Cost
                      'STOCK ADDED FROM ORDER', | ! Notes
                      'ORDER NUMBER: ' & Format(orp:order_number,@n~SS~010) & '<13,10>SUPPLIER: ' & Clip(ord:supplier)) ! Information
      
                  END
              END
      
              Case tmp:LabelType
              Of 1
                  glo:select1  = orp:part_ref_number
                  Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      Stock_Label(orp:Date_Received,orp:Quantity,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                  Of 'TEC B-452'
                      Stock_Label_B452(orp:Date_Received,orp:Quantity,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                  End!Case def:label_printer_type
                  glo:select1 = ''
              Of 2
                  Loop looptemp# = 1 To orp:Quantity
                      glo:select1  = orp:part_ref_number
                      Case def:label_printer_type
                      Of 'TEC B-440 / B-442'
                          Stock_Label(orp:Date_Received,1,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                      Of 'TEC B-452'
                          Stock_Label_B452(orp:Date_Received,1,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                      End!Case def:label_printer_type
                  End!Loop x# = 1 To no_of_labels_temp
                  glo:select1 = ''
              End!Case tmp:LabelType
      
              ! Inserting (DBH 04/04/2007) # 8046 - Print label per option
              Case tmp:BarCodeLabelType
              Of 4 ! Label Per Line
                  StockBarcodeLabel(orp:Record_Number,1)
              Of 3
                  StockBarcodeLabel(orp:Record_Number,orp:Quantity)
              Of 2
                  If tmp:BarcodeLabelNumber > 0
                      StockBarcodeLabel(orp:Record_Number,tmp:BarcodeLabelNumber)
                  End ! If tmp:BarcodeLabelNumber > 0
      
              End ! Case tmp:BarCodeLabelType
              ! End (DBH 04/04/2007) #8046
      
              glo:ErrorText = ''
              sto:Purchase_Cost   = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_Stock,sto:Purchase_Cost),@n14.2)
              If glo:ErrorText <> ''
                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                     'ADD', | ! Transaction_Type
                                     '', | ! Depatch_Note_Number
                                     0, | ! Job_Number
                                     0, | ! Sales_Number
                                     0, | ! Quantity
                                     sto:Purchase_Cost, | ! Purchase_Cost
                                     sto:Sale_Cost, | ! Sale_Cost
                                     sto:Retail_Cost, | ! Retail_Cost
                                     'AVERAGE COST CALCULATION', | ! Notes
                                     Clip(glo:ErrorText)) ! Information
                    ! Added OK
                  Else ! AddToStockHistory
                    ! Error
                  End ! AddToStockHistory
              End !If glo:ErrorText <> ''
              glo:ErrorText = ''
              Access:STOCK.Update()
          END  ! IF (Access:ORDPARTS.TryUpdate() = Level:Benign)
      END ! LOOP ll# = 1 TO RECORDS(glo:Queue)
      glo:Select2 = ''
      Do DASBRW::11:DASUNTAGALL
      DO AllReceived
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonCaptureTaggedPricing, Accepted)
    OF ?Goods_Received_Note
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Goods_Received_Note, Accepted)
      ThisWindow.Update
      BRW1.UpdateBuffer()
      
      If ord:Order_Number <> 0
          tmp:Goods_Received_Date = today()
      
          if Goods_Received_Note_Window(tmp:Goods_Received_Date)          ! Call window prompting for received date
      
              !Do GoodsReceivedNote
              PrintGoodsReceivedNote(1,tmp:Goods_Received_Date)
      
          end ! if
      End ! ord:Order_Number <> 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Goods_Received_Note, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:CancelExchangeOrder
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CancelExchangeOrder, Accepted)
      brw6.UpdateViewRecord()
      If orp:All_Received = 'YES'
          Cycle
      End ! If orp:All_Received = 'YES'
      
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = orp:Part_Ref_Number
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          If sto:ExchangeUnit = 'YES'
              Case Missive('The selected order will be marked as received. The outstanding quantity will be cancelled.'&|
                '|Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      If SecurityCheck('CANCEL EXCHANGE ORDER')
                          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      Else ! If SecurityCheck('CANCEL EXCHANGE ORDER')
                          orp:All_Received = 'YES'
                          orp:Date_Received = Today()
                          Access:ORDPARTS.TryUpdate()
                          Brw6.ResetSort(1)
                      End ! If SecurityCheck('CANCEL EXCHANGE ORDER')
                  Of 1 ! No Button
              End ! Case Missive
          Else ! If sto:ExchangeUnit = 'YES'
              Case Missive('The selected order is not an exchange order.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End ! If sto:ExchangeUnit = 'YES'
      End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      ! Are there any more parts on the order? (DBH: 06/12/2007)
      Do AllReceived
      Brw1.ResetSort(1)
      Brw6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CancelExchangeOrder, Accepted)
    OF ?Delete_Part_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part_Button, Accepted)
      Thiswindow.reset
      error# = 0
      glo:select1 = orp:record_number
      found# = 0
      setcursor(cursor:wait)
      save_orp_id = access:ordparts.savefile()
      access:ordparts.clearkey(orp:part_number_key)
      orp:order_number = ord:order_number
      set(orp:part_number_key,orp:part_number_key)
      loop
          if access:ordparts.next()
             break
          end !if
          if orp:order_number <> ord:order_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          If orp:number_received <> ''
              found# = 1
              Break
          End!    If orp:number_received <> ''
      end !loop
      access:ordparts.restorefile(save_orp_id)
      setcursor()
      
      If found# = 1
          Case Missive('Unable to delete.'&|
            '<13,10>'&|
            '<13,10>One or more parts on this order has been received.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If found# = 1
          Case Missive('You are about to remove this part from Order Number ' & Clip(ord:Order_Number) & '.'&|
            '<13,10>Do you wish to:'&|
            '<13,10>a) Mark this part for RE-ORDERING on the next parts order or,'&|
            '<13,10>b) REMOVE this part from the order?','ServiceBase 3g',|
                         'mquest.jpg','\Cancel|Remove|Re-Order')
              Of 3 ! Re-Order Button
                  access:ordparts.clearkey(orp:record_number_key)
                  orp:record_number = glo:select1
                  if access:ordparts.fetch(orp:record_number_key) = Level:Benign
                      Case orp:part_type
                          Of 'JOB'
                              get(ordpend,0)
                              if access:ordpend.primerecord() = level:benign
                                  ope:part_ref_number = orp:part_ref_number
                                  ope:job_number      = orp:job_number
                                  ope:part_type       = 'JOB'
                                  ope:supplier        = ord:supplier
                                  ope:part_number     = orp:part_number
                                  ope:Description     = orp:description
                                  ope:quantity        = orp:quantity
                                  if access:ordpend.insert()
                                      access:ordpend.cancelautoinc()
                                  end
                              end!if access:ordpend.primerecord() = level:benign
                              access:parts.clearkey(par:order_part_key)
                              par:ref_number        = orp:job_number
                              par:order_part_number = orp:record_number
                              if access:parts.fetch(par:order_part_key) = Level:Benign
                                  par:date_ordered = ''
                                  par:order_number = ''
                                  par:exclude_from_order = 'YES'
                                  access:parts.update()
                              end!if access:parts.fetch(par:order_part_key) = Level:Benign
      
                          Of 'WAR'
                              get(ordpend,0)
                              if access:ordpend.primerecord() = level:benign
                                  ope:part_ref_number = orp:part_ref_number
                                  ope:job_number      = orp:job_number
                                  ope:part_type       = 'WAR'
                                  ope:supplier        = ord:supplier
                                  ope:part_number     = orp:part_number
                                  ope:quantity        = orp:quantity
                                  ope:description     = orp:description
                                  if access:ordpend.insert()
                                      access:ordpend.cancelautoinc()
                                  end
                              end!if access:ordpend.primerecord() = level:benign
                              access:warparts.clearkey(wpr:order_part_key)
                              wpr:ref_number        = orp:job_number
                              wpr:order_part_number = orp:record_number
                              if access:warparts.fetch(wpr:order_part_key) = Level:Benign
                                  wpr:date_ordered = ''
                                  wpr:order_number = ''
                                  wpr:exclude_from_order = 'YES'
                                  access:warparts.update()
                              end!if access:parts.fetch(par:order_part_key) = Level:Benign
      
                          Of 'STO'
                              if access:ordpend.primerecord() = level:benign
                                  ope:part_ref_number = orp:part_ref_number
                                  ope:job_number      = orp:job_number
                                  ope:part_type       = 'STO'
                                  ope:supplier        = ord:supplier
                                  ope:part_number     = orp:part_number
                                  ope:description     = orp:description
                                  ope:quantity        = orp:quantity
                                  if access:ordpend.insert()
                                      access:ordpend.cancelautoinc()
                                  end
                              end!if access:ordpend.primerecord() = level:benign
      
                          Of 'RET'
                              get(ordpend,0)
                              if access:ordpend.primerecord() = level:benign
      
                                  ope:part_ref_number = orp:part_ref_number
                                  ope:job_number  = orp:job_number
                                  ope:part_type   = 'RET'
                                  ope:supplier    = ord:supplier
                                  ope:part_number = orp:part_number
                                  ope:description   = orp:description
                                  ope:account_number  = orp:account_number
                                  ope:quantity        = orp:quantity
                                  if access:ordpend.insert()
                                      access:ordpend.cancelautoinc()
                                  end
                              end!if access:ordpend.primerecord() = level:benign
                              access:parts.clearkey(par:order_part_key)
                              par:ref_number        = orp:job_number
                              par:order_part_number = orp:record_number
                              if access:parts.fetch(par:order_part_key) = Level:Benign
                                  par:date_ordered = ''
                                  par:order_number = ''
                                  par:exclude_from_order = 'YES'
                                  access:parts.update()
                              end!if access:parts.fetch(par:order_part_key) = Level:Benign
      
                      End!Case orp:part_type
                      save_STOCK_id = Access:STOCK.SaveFile()
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number    = orp:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          ! Found
                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                             'REC', | ! Transaction_Type
                                             '', | ! Depatch_Note_Number
                                             orp:Job_Number, | ! Job_Number
                                             0, | ! Sales_Number
                                             orp:Quantity, | ! Quantity
                                             orp:Purchase_Cost, | ! Purchase_Cost
                                             orp:Sale_Cost, | ! Sale_Cost
                                             orp:Retail_Cost, | ! Retail_Cost
                                             'STOCK ITEM REMOVE FROM ORDER', | ! Notes
                                             'REMOVED FROM ORDER NUMBER: ' & Clip(ord:Order_Number)) ! Information
                            ! Added OK
                          Else ! AddToStockHistory
                            ! Error
                          End ! AddToStockHistory
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          ! Error
                      End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      Access:STOCK.RestoreFile(save_STOCK_id)
                      Delete(ordparts)
                  end!if access:ordparts.fetch(orp:order_number_key) = Level:Benign
      
              Of 2 ! Remove Button
                  access:ordparts.clearkey(orp:record_number_key)
                  orp:record_number = glo:select1
                  if access:ordparts.fetch(orp:record_number_key) = Level:Benign
                      remove# = 0
                      exclude# = 0
                      If orp:job_number <> ''
                          Case Missive('This part has been ordered for Job Number: ' & Clip(job:Ref_Number) & '.'&|
                            '<13,10>Do you wish to:'&|
                            '<13,10>a) REMOVE the part from the job or,'&|
                            '<13,10>b) EXCLUDE the part on the job from ordering?','ServiceBase 3g',|
                                         'mquest.jpg','\Cancel|Exclude|Remove')
                              Of 3 ! Remove Button
                                  Case orp:part_type
                                      Of 'JOB'
                                          access:parts.clearkey(par:order_part_key)
                                          par:ref_number        = orp:job_number
                                          par:order_part_number = orp:record_number
                                          if access:parts.fetch(par:order_part_key) = Level:Benign
                                              Delete(Parts)
                                          end !if access:parts.fetch(par:order_part_key) = Level:Benign
                                          remove# = 1
      
                                      Of 'WAR'
                                          access:warparts.clearkey(wpr:order_part_key)
                                          wpr:ref_number        = orp:job_number
                                          wpr:order_part_number = orp:record_number
                                          if access:warparts.fetch(wpr:order_part_key) = Level:Benign
                                              Delete(warparts)
                                          end !if access:parts.fetch(par:order_part_key) = Level:Benign
                                          remove# = 1
      
                                  End!Case orp:part_type
                              Of 2 ! Exclude Button
                                  Case orp:part_type
      
                                      Of 'JOB'
                                          access:parts.clearkey(par:order_part_key)
                                          par:ref_number        = orp:job_number
                                          par:order_part_number = orp:record_number
                                          if access:parts.fetch(par:order_part_key) = Level:Benign
                                              par:exclude_from_order = 'YES'
                                              par:date_ordered = Today()
                                              par:order_number = ''
                                              access:parts.update()
                                          end !if
                                          exclude# =1
      
                                      Of 'WAR'
                                          access:warparts.clearkey(wpr:order_part_key)
                                          wpr:ref_number        = orp:job_number
                                          wpr:order_part_number = orp:record_number
                                          if access:warparts.fetch(wpr:order_part_key) = Level:Benign
                                              wpr:exclude_from_order = 'YES'
                                              wpr:date_ordered = Today()
                                              wpr:order_number = ''
                                              access:warparts.update()
                                          end !if
                                          exclude# =1
      
                                  End!Case orp:part_type
                              Of 1 ! Cancel Button
                          End ! Case Missive
                      End !If orp:job_number <> ''
                      save_STOCK_id = Access:STOCK.SaveFile()
                      Access:STOCK.Clearkey(sto:Ref_Number_Key)
                      sto:Ref_Number    = orp:Part_Ref_Number
                      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          ! Found
                          If orp:Part_Ref_Number <> ''
                              If Remove# = 1
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'REC', | ! Transaction_Type
                                                     '', | ! Depatch_Note_Number
                                                     orp:Job_Number, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     orp:Quantity, | ! Quantity
                                                     orp:Purchase_Cost, | ! Purchase_Cost
                                                     orp:Sale_Cost, | ! Sale_Cost
                                                     orp:Retail_Cost, | ! Retail_Cost
                                                     'STOCK ITEM REMOVE FROM ORDER', | ! Notes
                                                     'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number) & |
                                                      '<13,10> REMOVED FROM JOB NUMBER: ' & Clip(orp:job_number)) ! Information
                                    ! Added OK
      
                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
                              End ! If Remove# = 1
                              If Exclude# = 1
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'REC', | ! Transaction_Type
                                                     '', | ! Depatch_Note_Number
                                                     orp:Job_Number, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     orp:Quantity, | ! Quantity
                                                     orp:Purchase_Cost, | ! Purchase_Cost
                                                     orp:Sale_Cost, | ! Sale_Cost
                                                     orp:Retail_Cost, | ! Retail_Cost
                                                     'STOCK ITEM REMOVE FROM ORDER', | ! Notes
                                                     'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number) & |
                                                      '<13,10> PART EXCLUDED FROM ORDER ON JOB NUMBER: ' & Clip(orp:job_number)) ! Information
                                    ! Added OK
      
                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
                              End ! If Exclude# = 1
                          Else ! If orp:Part_Ref_Number <> ''
                              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                 'REC', | ! Transaction_Type
                                                 '', | ! Depatch_Note_Number
                                                 orp:Job_Number, | ! Job_Number
                                                 0, | ! Sales_Number
                                                 orp:Quantity, | ! Quantity
                                                 orp:Purchase_Cost, | ! Purchase_Cost
                                                 orp:Sale_Cost, | ! Sale_Cost
                                                 orp:Retail_Cost, | ! Retail_Cost
                                                 'STOCK ITEM REMOVE FROM ORDER', | ! Notes
                                                 'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number)) ! Information
                                ! Added OK
      
                              Else ! AddToStockHistory
                                ! Error
                              End ! AddToStockHistory
                          End ! If orp:Part_Ref_Number <> ''
                      Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          ! Error
                      End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      Access:STOCK.RestoreFile(save_STOCK_id)
                      Delete(ordparts)
                  end!if access:ordparts.fetch(orp:record_number_key) = Level:Benign
              Of 1 ! Cancel Button
          End ! Case Missive
      End!If found# = 1
      
      !call to check if any more parts exist
      do AllReceived
      BRW6.ResetSort(1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part_Button, Accepted)
    OF ?Reprint_Order_Button
      ThisWindow.Update
      Parts_Order
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
      glo:select1 = ''
      Clear(glo:q_partsorder)
      Free(glo:q_partsorder)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
    OF ?Button14
      ThisWindow.Update
      Amend_Quantity
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If Keycode() = MouseLeft2
          Post(event:accepted,?Received_Button)
      End!If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
PrintGoodsReceivedNote          PROCEDURE(BYTE fType, DATE fDate)
locGoodsReceived     BYTE(0)
    CODE

        locGoodsReceived = 0                                         ! Check if any parts have been received on this date

        Access:ORDPARTS.ClearKey(ORP:Order_Number_key)
        ORP:Order_Number = ORD:Order_Number
        set(ORP:Order_Number_Key, ORP:Order_Number_Key)
        loop until Access:ORDPARTS.Next()                           ! Loop thru parts on order - check at least one part
            if orp:Order_Number <> ord:Order_Number then break.     ! has been received on the selected date.
            IF (fType = 1) ! Captured GRN
                if orp:DatePriceCaptured = fDate And orp:GRN_Number = 0
                    ! At least one part has been received, and is not already on a GRN - TrkBs: 5110 (DBH: 10-08-2005)
                    locGoodsReceived = 1
                    break
                end ! if
            ELSE
                ! Uncaptured GRN
                IF (orp:Date_Received = fDate AND orp:UncapturedGRNNumber = 0)
                    locGoodsReceived = 1
                    BREAK
                END ! IF (orp:DateReceived = fGoodsReceivedDate AND orp:UncapturedGRNNumber = 0)
            END
        end ! loop

        if locGoodsReceived = 1
            if not Access:GRNOTES.PrimeRecord()                     ! Create goods received note
                grn:Order_Number        = ord:Order_Number
                grn:Goods_Received_Date = fDate

                !NEW EVO defaults TB12777 - JC - 11/01/13
                if Getini('EVO','CREATE',0 ,clip(path())&'\SB2KDEF.INI') = true then
                    grn:EVO_Status = 'R'    !ready to send
                ELSE
                    grn:EVO_Status = 'X'    !Excluded (other value is S = SENT)
                END

                IF (fType = 0)
                    ! Don't want GRN to appear on Line 500
                    grn:BatchRunNotPrinted = 0
                ELSE
                    ! Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
                    If GETINI('XML', 'CreateOracle',, Clip(Path()) & '\SB2KDEF.INI') <> 1
                        grn:BatchRunNotPrinted      = False
                    Else ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
                        grn:BatchRunNotPrinted      = True
                    End ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
                END
                IF (fType = 0)
                    grn:Uncaptured = 1
                END
                ! End   - Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
                if not Access:GRNOTES.TryUpdate()

                    ! Mark Parts!
                    FirstPart# = True
                    Access:ORDPARTS.ClearKey(ORP:Order_Number_key)
                    ORP:Order_Number = ORD:Order_Number
                    set(ORP:Order_Number_Key, ORP:Order_Number_Key)
                    loop until Access:ORDPARTS.Next()                           ! Loop thru parts on order - check at least one part
                        if orp:Order_Number <> ord:Order_Number then break.     ! has been received on the selected date.
                        IF (fType = 0)
                            IF (orp:Date_Received = fDate)
                                IF (orp:UncapturedGRNNumber = 0)
                                    orp:UncapturedGRNNumber = grn:Goods_Received_Number
                                    Access:ORDPARTS.TryUpdate()
                                END
                            END
                        ELSE
                            if orp:DatePriceCaptured = fDate
                                If orp:GRN_Number = 0
                                    If FirstPart# = True
                                          ! Take the exchange rate from the first part.
                                          ! All the parts should hopefully be the same for the same date - TrkBs: 5110 (DBH: 25-05-2005)
                                        grn:CurrencyCode   = orp:OrderedCurrency
                                        grn:DailyRate      = orp:OrderedDailyRate
                                        grn:DivideMultiply = orp:OrderedDivideMultiply
                                        Access:GRNOTES.Update()
                                        FirstPart# = False
                                    End ! FirstPart# = True
                                    orp:GRN_Number     = grn:Goods_Received_Number
                                    Access:OrdParts.Update()
                                End ! If
                            end ! if
                        END
                    end ! loop
                    glo:select1 = grn:Goods_Received_Number
                    glo:select2 = ''
                    glo:select3 = ''
                    Goods_Received_Note                             ! Print goods received note
                    glo:select1 = ''
                    glo:select2 = ''
                    glo:select3 = ''
                    !NEW EVO defaults TB12777 - JC - 28/01/13
                    if Getini('EVO','CREATE',0 ,clip(path())&'\SB2KDEF.INI') = true then
                        EVO_Export('ORDER',grn:Goods_Received_Number,'N')
                    END !if using EVO
                end ! if grnotes update worked
            end ! if grnotes.primerecord worked

        ELSE
            CASE fType
            OF 0
                Case Missive('No new items found.'&|
                    '|You must receive parts before you can print the GRN.','ServiceBase',|
                               'mstop.jpg','/&OK')
                Of 1 ! &OK Button
                End!Case Message
            OF 1
                Case Missive('No new items found.'&|
                    '|You must capture the pricing before you can print the GRN.','ServiceBase',|
                               'mstop.jpg','/&OK')
                Of 1 ! &OK Button
                End!Case Message
            END
        end ! if

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'NO'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'ALL'
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
  sup:Company_Name = ord:Supplier
  If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
      !Found
  
  Else ! Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
      !Error
  End ! Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = orp:Record_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Case def:SummaryORders
      Of 1
            job_number_temp = 'Stock'
            type_temp       = 'STO'
      Else
          IF (orp:Job_Number <> '')
            job_number_temp = orp:Job_Number
          ELSE
            job_number_temp = 'Stock'
          END
          CASE (orp:Part_Type)
          OF 'JOB'
            type_temp = 'CHA'
          ELSE
            type_temp = orp:Part_Type
          END
  End !Case def:SummaryORders
  IF (orp:OrderedCurrency <> '')
    tmp:Currency = orp:OrderedCurrency
  ELSE
    tmp:Currency = 'ZAR'
  END
  ! Before Embed Point: %FormatBrowse) DESC(Legacy: Browser, Format an element of the queue) ARG(6)
  ! Show Received
  If orp:number_received = ''
      received_temp = 'NONE'
  Else
      If orp:number_received <> orp:quantity
          received_temp = orp:number_received
      Else
          received_temp = 'ALL'
      End
  End
  ! After Embed Point: %FormatBrowse) DESC(Legacy: Browser, Format an element of the queue) ARG(6)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END
  SELF.Q.orp:Part_Number_NormalFG = -1
  SELF.Q.orp:Part_Number_NormalBG = -1
  SELF.Q.orp:Part_Number_SelectedFG = -1
  SELF.Q.orp:Part_Number_SelectedBG = -1
  SELF.Q.orp:Description_NormalFG = -1
  SELF.Q.orp:Description_NormalBG = -1
  SELF.Q.orp:Description_SelectedFG = -1
  SELF.Q.orp:Description_SelectedBG = -1
  SELF.Q.type_temp_NormalFG = -1
  SELF.Q.type_temp_NormalBG = -1
  SELF.Q.type_temp_SelectedFG = -1
  SELF.Q.type_temp_SelectedBG = -1
  SELF.Q.job_number_temp_NormalFG = -1
  SELF.Q.job_number_temp_NormalBG = -1
  SELF.Q.job_number_temp_SelectedFG = -1
  SELF.Q.job_number_temp_SelectedBG = -1
  SELF.Q.orp:Quantity_NormalFG = -1
  SELF.Q.orp:Quantity_NormalBG = -1
  SELF.Q.orp:Quantity_SelectedFG = -1
  SELF.Q.orp:Quantity_SelectedBG = -1
  SELF.Q.orp:Number_Received_NormalFG = -1
  SELF.Q.orp:Number_Received_NormalBG = -1
  SELF.Q.orp:Number_Received_SelectedFG = -1
  SELF.Q.orp:Number_Received_SelectedBG = -1
  SELF.Q.tmp:Currency_NormalFG = -1
  SELF.Q.tmp:Currency_NormalBG = -1
  SELF.Q.tmp:Currency_SelectedFG = -1
  SELF.Q.tmp:Currency_SelectedBG = -1
  SELF.Q.orp:Purchase_Cost_NormalFG = -1
  SELF.Q.orp:Purchase_Cost_NormalBG = -1
  SELF.Q.orp:Purchase_Cost_SelectedFG = -1
  SELF.Q.orp:Purchase_Cost_SelectedBG = -1
  SELF.Q.orp:Sale_Cost_NormalFG = -1
  SELF.Q.orp:Sale_Cost_NormalBG = -1
  SELF.Q.orp:Sale_Cost_SelectedFG = -1
  SELF.Q.orp:Sale_Cost_SelectedBG = -1
  SELF.Q.orp:Date_Received_NormalFG = -1
  SELF.Q.orp:Date_Received_NormalBG = -1
  SELF.Q.orp:Date_Received_SelectedFG = -1
  SELF.Q.orp:Date_Received_SelectedBG = -1
  SELF.Q.orp:TimeReceived_NormalFG = -1
  SELF.Q.orp:TimeReceived_NormalBG = -1
  SELF.Q.orp:TimeReceived_SelectedFG = -1
  SELF.Q.orp:TimeReceived_SelectedBG = -1
  SELF.Q.orp:DatePriceCaptured_NormalFG = -1
  SELF.Q.orp:DatePriceCaptured_NormalBG = -1
  SELF.Q.orp:DatePriceCaptured_SelectedFG = -1
  SELF.Q.orp:DatePriceCaptured_SelectedBG = -1
  SELF.Q.orp:TimePriceCaptured_NormalFG = -1
  SELF.Q.orp:TimePriceCaptured_NormalBG = -1
  SELF.Q.orp:TimePriceCaptured_SelectedFG = -1
  SELF.Q.orp:TimePriceCaptured_SelectedBG = -1
  SELF.Q.tmp:Currency = tmp:Currency                  !Assign formula result to display queue
   
   
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Part_Number_NormalFG = 8421504
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Part_Number_NormalFG = 32768
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Part_Number_NormalFG = 255
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Description_NormalFG = 8421504
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Description_NormalFG = 32768
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Description_NormalFG = 255
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.type_temp_NormalFG = 8421504
     SELF.Q.type_temp_NormalBG = 16777215
     SELF.Q.type_temp_SelectedFG = 16777215
     SELF.Q.type_temp_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.type_temp_NormalFG = 32768
     SELF.Q.type_temp_NormalBG = 16777215
     SELF.Q.type_temp_SelectedFG = 16777215
     SELF.Q.type_temp_SelectedBG = 32768
   ELSE
     SELF.Q.type_temp_NormalFG = 255
     SELF.Q.type_temp_NormalBG = 16777215
     SELF.Q.type_temp_SelectedFG = 16777215
     SELF.Q.type_temp_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.job_number_temp_NormalFG = 8421504
     SELF.Q.job_number_temp_NormalBG = 16777215
     SELF.Q.job_number_temp_SelectedFG = 16777215
     SELF.Q.job_number_temp_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.job_number_temp_NormalFG = 32768
     SELF.Q.job_number_temp_NormalBG = 16777215
     SELF.Q.job_number_temp_SelectedFG = 16777215
     SELF.Q.job_number_temp_SelectedBG = 32768
   ELSE
     SELF.Q.job_number_temp_NormalFG = 255
     SELF.Q.job_number_temp_NormalBG = 16777215
     SELF.Q.job_number_temp_SelectedFG = 16777215
     SELF.Q.job_number_temp_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Quantity_NormalFG = 8421504
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Quantity_NormalFG = 32768
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Quantity_NormalFG = 255
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Number_Received_NormalFG = 8421504
     SELF.Q.orp:Number_Received_NormalBG = 16777215
     SELF.Q.orp:Number_Received_SelectedFG = 16777215
     SELF.Q.orp:Number_Received_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Number_Received_NormalFG = 32768
     SELF.Q.orp:Number_Received_NormalBG = 16777215
     SELF.Q.orp:Number_Received_SelectedFG = 16777215
     SELF.Q.orp:Number_Received_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Number_Received_NormalFG = 255
     SELF.Q.orp:Number_Received_NormalBG = 16777215
     SELF.Q.orp:Number_Received_SelectedFG = 16777215
     SELF.Q.orp:Number_Received_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.tmp:Currency_NormalFG = 8421504
     SELF.Q.tmp:Currency_NormalBG = 16777215
     SELF.Q.tmp:Currency_SelectedFG = 16777215
     SELF.Q.tmp:Currency_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.tmp:Currency_NormalFG = 32768
     SELF.Q.tmp:Currency_NormalBG = 16777215
     SELF.Q.tmp:Currency_SelectedFG = 16777215
     SELF.Q.tmp:Currency_SelectedBG = 32768
   ELSE
     SELF.Q.tmp:Currency_NormalFG = 255
     SELF.Q.tmp:Currency_NormalBG = 16777215
     SELF.Q.tmp:Currency_SelectedFG = 16777215
     SELF.Q.tmp:Currency_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Purchase_Cost_NormalFG = 8421504
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Purchase_Cost_NormalFG = 32768
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Purchase_Cost_NormalFG = 255
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Sale_Cost_NormalFG = 8421504
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Sale_Cost_NormalFG = 32768
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Sale_Cost_NormalFG = 255
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Date_Received_NormalFG = 8421504
     SELF.Q.orp:Date_Received_NormalBG = 16777215
     SELF.Q.orp:Date_Received_SelectedFG = 16777215
     SELF.Q.orp:Date_Received_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:Date_Received_NormalFG = 32768
     SELF.Q.orp:Date_Received_NormalBG = 16777215
     SELF.Q.orp:Date_Received_SelectedFG = 16777215
     SELF.Q.orp:Date_Received_SelectedBG = 32768
   ELSE
     SELF.Q.orp:Date_Received_NormalFG = 255
     SELF.Q.orp:Date_Received_NormalBG = 16777215
     SELF.Q.orp:Date_Received_SelectedFG = 16777215
     SELF.Q.orp:Date_Received_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:TimeReceived_NormalFG = 8421504
     SELF.Q.orp:TimeReceived_NormalBG = 16777215
     SELF.Q.orp:TimeReceived_SelectedFG = 16777215
     SELF.Q.orp:TimeReceived_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:TimeReceived_NormalFG = 32768
     SELF.Q.orp:TimeReceived_NormalBG = 16777215
     SELF.Q.orp:TimeReceived_SelectedFG = 16777215
     SELF.Q.orp:TimeReceived_SelectedBG = 32768
   ELSE
     SELF.Q.orp:TimeReceived_NormalFG = 255
     SELF.Q.orp:TimeReceived_NormalBG = 16777215
     SELF.Q.orp:TimeReceived_SelectedFG = 16777215
     SELF.Q.orp:TimeReceived_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:DatePriceCaptured_NormalFG = 8421504
     SELF.Q.orp:DatePriceCaptured_NormalBG = 16777215
     SELF.Q.orp:DatePriceCaptured_SelectedFG = 16777215
     SELF.Q.orp:DatePriceCaptured_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:DatePriceCaptured_NormalFG = 32768
     SELF.Q.orp:DatePriceCaptured_NormalBG = 16777215
     SELF.Q.orp:DatePriceCaptured_SelectedFG = 16777215
     SELF.Q.orp:DatePriceCaptured_SelectedBG = 32768
   ELSE
     SELF.Q.orp:DatePriceCaptured_NormalFG = 255
     SELF.Q.orp:DatePriceCaptured_NormalBG = 16777215
     SELF.Q.orp:DatePriceCaptured_SelectedFG = 16777215
     SELF.Q.orp:DatePriceCaptured_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:TimePriceCaptured_NormalFG = 8421504
     SELF.Q.orp:TimePriceCaptured_NormalBG = 16777215
     SELF.Q.orp:TimePriceCaptured_SelectedFG = 16777215
     SELF.Q.orp:TimePriceCaptured_SelectedBG = 8421504
   ELSIF(orp:Date_Received <> '' And orp:DatePriceCaptured = '')
     SELF.Q.orp:TimePriceCaptured_NormalFG = 32768
     SELF.Q.orp:TimePriceCaptured_NormalBG = 16777215
     SELF.Q.orp:TimePriceCaptured_SelectedFG = 16777215
     SELF.Q.orp:TimePriceCaptured_SelectedBG = 32768
   ELSE
     SELF.Q.orp:TimePriceCaptured_NormalFG = 255
     SELF.Q.orp:TimePriceCaptured_NormalBG = 16777215
     SELF.Q.orp:TimePriceCaptured_SelectedFG = 16777215
     SELF.Q.orp:TimePriceCaptured_SelectedBG = 255
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = orp:Record_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Goods_Received_Note_Window PROCEDURE (DateValue)      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Goods_Received_Date LONG
Return_Flag          BYTE
window               WINDOW('Create A Goods Received Note'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Goods Received Note Creation'),USE(?Tab1)
                           PROMPT('Order Number'),AT(248,194),USE(?ord:Order_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n~SS~010),AT(344,194,55,10),USE(ord:Order_Number),SKIP,RIGHT(2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Order Number')
                           PROMPT('Goods Received Date'),AT(248,210),USE(?tmp:Goods_Received_Date:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d06b),AT(344,210,55,10),USE(tmp:Goods_Received_Date),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                           BUTTON,AT(404,206),USE(?PopCalendar),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Goods Received Note Creation'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(Return_Flag)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020104'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Goods_Received_Note_Window')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ord:Order_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ORDERS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:Goods_Received_Date = DateValue
  
  !Bodge to use for Retail GRN Notes
  If ord:Order_Number = 0
      ?ord:Order_Number{prop:Hide} = 1
      ?ord:Order_Number:Prompt{prop:Hide} = 1
  End !ord:Order_Number = 0
  ! Inserting (DBH 03/12/2007) # 9034 - Allow to change order number prefix
  prefix" = GETINI('STOCK','OrderNumberPrefix',,Clip(Path()) & '\SB2KDEF.INI')
  If Clip(prefix") = ''
      prefix" = 'SS'
  End ! If Clip(prefix") <> ''
  ?ord:Order_Number{prop:Text} = '@n~' & Clip(prefix") & '~010'
  ! End (DBH 03/12/2007) #9034
  
  ! Save Window Name
   AddToLog('Window','Open','Goods_Received_Note_Window')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Goods_Received_Note_Window')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      if tmp:Goods_Received_Date = ''
          select(?tmp:Goods_Received_Date)
          cycle
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:Goods_Received_Date = TINCALENDARStyle1()
          Display(?tmp:Goods_Received_Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020104'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020104'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020104'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      DateValue   = tmp:Goods_Received_Date
      Return_Flag = 1
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Receive_All_Parts PROCEDURE                           !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Receiving All Parts'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Receiving All Parts'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(296,258),USE(?OKButton),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Despatch Note Number'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s40),AT(280,206,124,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020090'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Receive_All_Parts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Receive_All_Parts')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Receive_All_Parts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OKButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
      ! Despatch Note No is required - TrkBs: 6264 (DBH: 07-09-2005)
      If glo:Select2 = ''
          Case Missive('You must enter a Despatch Note Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?glo:Select2)
          Cycle
      Else ! If glo:Select2 = ''
          Post(Event:CloseWindow)
      End ! If glo:Select2 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OKButton, Accepted)
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case Missive('Are you sure you want to cancel the Order Receipt?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              glo:Select2 = ''
              Post(Event:CloseWindow)
          Of 1 ! No Button
              Select(?glo:Select2)
              Cycle
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020090'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020090'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020090'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AmendJobRequest PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
sav:Quantity         LONG
window               WINDOW('Amend Job Request'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Job Request'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN,HIDE
                       END
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,212,192,42),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Amend Job Request'),USE(?Tab1)
                           PROMPT('Location'),AT(248,222),USE(?orjtmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(308,222,124,10),USE(orjtmp:JobNumber),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Location'),TIP('Location'),UPR,READONLY
                           PROMPT('Quantity'),AT(248,238),USE(?orjtmp:Quantity:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(308,238,64,10),USE(orjtmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity'),TIP('Quantity'),UPR,RANGE(1,99999),STEP(1)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020087'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AmendJobRequest')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:ORDJOBS)
  Relate:ORDJOBS.Open
  Relate:PARTS.Open
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDJOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  sav:Quantity    = orstmp:Quantity
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','AmendJobRequest')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDJOBS.Close
    Relate:PARTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AmendJobRequest')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020087'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020087'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020087'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF orjtmp:Quantity <> sav:Quantity
      Case Missive('Are you sure you want to change the Quantity Requested?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Case orjtmp:CharWarr
                  Of 'C'
                      Access:PARTS.ClearKey(par:RequestedKey)
                      par:Requested   = True
                      par:Part_Number = orjtmp:PartNumber
                      If Access:PARTS.TryFetch(par:RequestedKey) = Level:Benign
                          par:Quantity    = orjtmp:Quantity
                          Access:PARTS.Update()
                      End!If Access:PARTS.TryFetch(par:RequestedKey) = Level:Benign
                  Of 'W'
                      Access:WARPARTS.ClearKey(par:RequestedKey)
                      wpr:Requested   = True
                      wpr:Part_Number = orjtmp:PartNumber
                      If Access:WARPARTS.TryFetch(wpr:RequestedKey) = Level:Benign
                          wpr:Quantity    = orjtmp:Quantity
                          Access:WARPARTS.Update()
                      End!If Access:PARTS.TryFetch(par:RequestedKey) = Level:Benign
              End!Case orjtmp:CharWarr
          Of 1 ! No Button
              orjtmp:Quantity = sav:Quantity
              Display(?orjtmp:Quantity)
              Cycle
      End ! Case Missive
  End!orstmp:Quantity <> sav:Quantity
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
AmendStockRequest PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
sav:Quantity         LONG
window               WINDOW('Amend Stock Request'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Stock Request'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN,HIDE
                       END
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(244,212,192,42),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Amend Stock Request'),USE(?Tab1)
                           PROMPT('Location'),AT(248,222),USE(?orstmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,222,124,10),USE(orstmp:Location),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Location'),TIP('Location'),UPR,READONLY
                           PROMPT('Quantity'),AT(248,236),USE(?orstmp:Quantity:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(308,236,64,10),USE(orstmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity'),TIP('Quantity'),UPR,RANGE(1,99999),STEP(1)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020088'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AmendStockRequest')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:ORDSTOCK)
  Relate:ORDPARTS.Open
  Relate:ORDSTOCK.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  sav:Quantity    = orstmp:Quantity
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','AmendStockRequest')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPARTS.Close
    Relate:ORDSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AmendStockRequest')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020088'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020088'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020088'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF orstmp:Quantity <> sav:Quantity
      Case Missive('Are you sure you want to change the Quantity Requested?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Access:STOCK.Clearkey(sto:Location_Key)
              sto:Location    = orstmp:Location
              sto:Part_Number = orstmp:PartNumber
              If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                  !Found
                  sto:QuantityRequested   = orstmp:Quantity
                  Access:STOCK.Update()
              End! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
  
          Of 1 ! No Button
              orstmp:Quantity = sav:Quantity
              Display(?orstmp:Quantity)
              Cycle
      End ! Case Missive
  End!orstmp:Quantity <> sav:Quantity
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdatePartsOrder PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::orp:Record  LIKE(orp:RECORD),STATIC
QuickWindow          WINDOW('UPdate Orde Parts'),AT(,,220,71),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdatePartsOrder'),GRAY,RESIZE
                       SHEET,AT(4,4,212,36),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Quantity'),AT(8,20),USE(?orp:Quantity:Prompt)
                           SPIN(@p<<<<<<<#p),AT(84,20,64,10),USE(orp:Quantity),DECIMAL(14),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Quantity')
                         END
                       END
                       PANEL,AT(4,44,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,48,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,48,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Update The Order Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdatePartsOrder')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orp:Quantity:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orp:Record,History::orp:Record)
  SELF.AddHistoryField(?orp:Quantity,4)
  SELF.AddUpdateFile(Access:ORDPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDPARTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdatePartsOrder')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPARTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdatePartsOrder')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

SelectLabelQuantity PROCEDURE (f:Quantity)            !Generated from procedure template - Window

tmp:LabelQuantity    LONG(1)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(296,258),USE(?Button:PrintLabel),TRN,FLAT,ICON('prnlabp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Print Label'),AT(248,192),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Quantity of Label(s) Per Part'),AT(248,212),USE(?tmp:LabelQuantity:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SPIN(@n8),AT(368,212,64,10),USE(tmp:LabelQuantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Label Quantity Per Part'),TIP('Label Quantity Per Part'),UPR,RANGE(1,99),STEP(1)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Print Label'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:LabelQuantity)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020683'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SelectLabelQuantity')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  tmp:LabelQuantity = f:Quantity
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','SelectLabelQuantity')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','SelectLabelQuantity')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:LabelQuantity = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:PrintLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PrintLabel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PrintLabel, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020683'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020683'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020683'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateOrderJobs PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::orjtmp:Record LIKE(orjtmp:RECORD),STATIC
QuickWindow          WINDOW('Update the ORDJOBS File'),AT(,,196,124),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOrderJobs'),GRAY,RESIZE,MDI
                       SHEET,AT(4,4,188,98),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?orjtmp:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,20,40,10),USE(orjtmp:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Order Number'),AT(8,34),USE(?orjtmp:OrderNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,34,40,10),USE(orjtmp:OrderNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Order Number'),TIP('Order Number'),UPR
                           PROMPT('Part Number'),AT(8,48),USE(?orjtmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,48,124,10),USE(orjtmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           PROMPT('Job Number'),AT(8,62),USE(?orjtmp:JobNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,62,40,10),USE(orjtmp:JobNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                           CHECK('Chargeable/Warranty'),AT(64,76,88,8),USE(orjtmp:CharWarr),MSG('Chargeable/Warranty'),TIP('Chargeable/Warranty'),VALUE('1','0')
                           PROMPT('Quantity'),AT(8,88),USE(?orjtmp:Quantity:Prompt),TRN
                           SPIN(@n8),AT(64,88,51,10),USE(orjtmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity'),TIP('Quantity'),UPR,STEP(1)
                         END
                       END
                       BUTTON('OK'),AT(98,106,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(147,106,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(147,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOrderJobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orjtmp:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orjtmp:Record,History::orjtmp:Record)
  SELF.AddHistoryField(?orjtmp:RecordNumber,1)
  SELF.AddHistoryField(?orjtmp:OrderNumber,2)
  SELF.AddHistoryField(?orjtmp:PartNumber,3)
  SELF.AddHistoryField(?orjtmp:JobNumber,4)
  SELF.AddHistoryField(?orjtmp:CharWarr,6)
  SELF.AddHistoryField(?orjtmp:Quantity,7)
  SELF.AddUpdateFile(Access:ORDJOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDJOBS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDJOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateOrderJobs')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDJOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateOrderJobs')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

