

   MEMBER('sbj03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ03002.INC'),ONCE        !Local module procedure declarations
                     END


AdjustQuantity PROCEDURE (site_loc)                   !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::res:Record  LIKE(res:RECORD),STATIC
QuickWindow          WINDOW('Adjust Quantity'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Quantity'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           ENTRY(@n8),AT(368,168,64,10),USE(res:Quantity),SKIP,RIGHT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Quantity Received'),AT(248,184),USE(?res:QuantityReceived:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(368,184,64,10),USE(res:QuantityReceived),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Quantity Received'),TIP('Quantity Received'),REQ,UPR
                           STRING('Reason'),AT(248,200),USE(?String1),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(304,200,128,52),USE(res:Amend_Reason),HIDE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Quantity Requested'),AT(248,168),USE(?res:QuantityReceived:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    GlobalErrors.Throw(Msg:InsertIllegal)
    RETURN
  OF ChangeRecord
    ActionMessage = 'Adjust Quantity'
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020493'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AdjustQuantity')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:RETSTOCK)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(res:Record,History::res:Record)
  SELF.AddHistoryField(?res:Quantity,11)
  SELF.AddHistoryField(?res:QuantityReceived,25)
  SELF.AddHistoryField(?res:Amend_Reason,28)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RETSTOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RETSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if res:Received = true then
    ?res:QuantityReceived{prop:Readonly}=true
  END
      ! Save Window Name
   AddToLog('Window','Open','AdjustQuantity')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RETSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AdjustQuantity')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?res:QuantityReceived
      IF res:QuantityReceived <> res:Quantity
        UNHIDE(?res:Amend_Reason)
        UNHIDE(?String1)
        ?res:Amend_Reason{PROP:Req} = TRUE
        !ThisMakeOver.Refresh
      END
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020493'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020493'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020493'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ?res:Amend_Reason{PROP:Hide} = FALSE
    res:Amended = TRUE
    res:Amend_Site_Loc = Site_Loc
  END
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

ReceiveStock PROCEDURE                                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_grr_id          USHORT,AUTO
tmp:SiteLocation     STRING(30)
tmp:InvoiceNumber    LONG
tmp:Tag              STRING(1)
tmp:QuantityReceive  LONG
tmp:BrowseRefNumber  LONG
tmp:Goods_Received_Date LONG
save_res_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW7::View:Browse    VIEW(STOCKRECEIVETMP)
                       PROJECT(stotmp:PartNumber)
                       PROJECT(stotmp:Description)
                       PROJECT(stotmp:ItemCost)
                       PROJECT(stotmp:Quantity)
                       PROJECT(stotmp:QuantityReceived)
                       PROJECT(stotmp:RESRecordNumber)
                       PROJECT(stotmp:ExchangeOrder)
                       PROJECT(stotmp:Received)
                       PROJECT(stotmp:RecordNumber)
                       PROJECT(stotmp:SessionID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_NormalFG       LONG                           !Normal forground color
tmp:Tag_NormalBG       LONG                           !Normal background color
tmp:Tag_SelectedFG     LONG                           !Selected forground color
tmp:Tag_SelectedBG     LONG                           !Selected background color
tmp:Tag_Icon           LONG                           !Entry's icon ID
stotmp:PartNumber      LIKE(stotmp:PartNumber)        !List box control field - type derived from field
stotmp:PartNumber_NormalFG LONG                       !Normal forground color
stotmp:PartNumber_NormalBG LONG                       !Normal background color
stotmp:PartNumber_SelectedFG LONG                     !Selected forground color
stotmp:PartNumber_SelectedBG LONG                     !Selected background color
stotmp:Description     LIKE(stotmp:Description)       !List box control field - type derived from field
stotmp:Description_NormalFG LONG                      !Normal forground color
stotmp:Description_NormalBG LONG                      !Normal background color
stotmp:Description_SelectedFG LONG                    !Selected forground color
stotmp:Description_SelectedBG LONG                    !Selected background color
stotmp:ItemCost        LIKE(stotmp:ItemCost)          !List box control field - type derived from field
stotmp:ItemCost_NormalFG LONG                         !Normal forground color
stotmp:ItemCost_NormalBG LONG                         !Normal background color
stotmp:ItemCost_SelectedFG LONG                       !Selected forground color
stotmp:ItemCost_SelectedBG LONG                       !Selected background color
stotmp:Quantity        LIKE(stotmp:Quantity)          !List box control field - type derived from field
stotmp:Quantity_NormalFG LONG                         !Normal forground color
stotmp:Quantity_NormalBG LONG                         !Normal background color
stotmp:Quantity_SelectedFG LONG                       !Selected forground color
stotmp:Quantity_SelectedBG LONG                       !Selected background color
stotmp:QuantityReceived LIKE(stotmp:QuantityReceived) !List box control field - type derived from field
stotmp:QuantityReceived_NormalFG LONG                 !Normal forground color
stotmp:QuantityReceived_NormalBG LONG                 !Normal background color
stotmp:QuantityReceived_SelectedFG LONG               !Selected forground color
stotmp:QuantityReceived_SelectedBG LONG               !Selected background color
stotmp:RESRecordNumber LIKE(stotmp:RESRecordNumber)   !List box control field - type derived from field
stotmp:RESRecordNumber_NormalFG LONG                  !Normal forground color
stotmp:RESRecordNumber_NormalBG LONG                  !Normal background color
stotmp:RESRecordNumber_SelectedFG LONG                !Selected forground color
stotmp:RESRecordNumber_SelectedBG LONG                !Selected background color
stotmp:ExchangeOrder   LIKE(stotmp:ExchangeOrder)     !List box control field - type derived from field
stotmp:ExchangeOrder_NormalFG LONG                    !Normal forground color
stotmp:ExchangeOrder_NormalBG LONG                    !Normal background color
stotmp:ExchangeOrder_SelectedFG LONG                  !Selected forground color
stotmp:ExchangeOrder_SelectedBG LONG                  !Selected background color
stotmp:Received        LIKE(stotmp:Received)          !List box control field - type derived from field
stotmp:Received_NormalFG LONG                         !Normal forground color
stotmp:Received_NormalBG LONG                         !Normal background color
stotmp:Received_SelectedFG LONG                       !Selected forground color
stotmp:Received_SelectedBG LONG                       !Selected background color
stotmp:RecordNumber    LIKE(stotmp:RecordNumber)      !Primary key field - type derived from field
stotmp:SessionID       LIKE(stotmp:SessionID)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
AppFrame             WINDOW('Stock Receive Routine'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Receive Routine'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Site Location'),AT(101,92),USE(?tmp:SiteLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(177,92,124,10),USE(tmp:SiteLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Site Location'),TIP('Site Location'),REQ,UPR
                           BUTTON,AT(305,88),USE(?LookupSiteLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('- Received'),AT(440,108),USE(?Prompt5:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PANEL,AT(424,108,12,10),USE(?Panel3:2),FILL(COLOR:Gray)
                           PANEL,AT(424,84,12,10),USE(?Panel3),FILL(COLOR:Green)
                           PROMPT('- Exchange Order'),AT(440,84),USE(?Prompt5),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PANEL,AT(424,96,12,10),USE(?Panel3:3),FILL(COLOR:Maroon)
                           PROMPT('- Loan Order'),AT(440,96),USE(?Prompt5:3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(513,124),USE(?Button11),TRN,FLAT,ICON('repgrnp.jpg')
                           PROMPT('Invoice Number'),AT(101,108),USE(?tmp:InvoiceNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n_8),AT(177,108,64,10),USE(tmp:InvoiceNumber),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Invoice Number'),TIP('Invoice Number'),UPR
                           LIST,AT(101,124,404,176),USE(?List),IMM,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)*I@n1@120L(2)|M*~Part Number~@s30@120L(2)|M*~Description~@s30@56R(2)|M*~It' &|
   'em Cost~@n10.2@36R(2)|M*~Qty~@n-14@56R(2)|M*~Qty Rcvd~@n-14@0R(2)|M*~RETR ecord ' &|
   'Number~@n-14@0R(2)|M*~Exchange Order~@n3@0R(2)|M*~Received~@n3@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(286,176,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Rev tags'),AT(202,181,50,13),USE(?DASREVTAG),HIDE
                           BUTTON,AT(101,308),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(169,308),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(237,308),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(440,308),USE(?buttonAmendQty),TRN,FLAT,ICON('amdqtyp.jpg')
                         END
                       END
                       BUTTON,AT(548,366),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON,AT(309,308),USE(?ProcessTagged),DISABLE,TRN,FLAT,LEFT,ICON('protagp.jpg')
                       BUTTON,AT(513,308),USE(?Goods_Received_Note_Button),TRN,FLAT,LEFT,ICON('prngrnp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:SiteLocation                Like(tmp:SiteLocation)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = stotmp:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = stotmp:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  Queue:Browse.tmp:Tag_NormalFG = -1
  Queue:Browse.tmp:Tag_NormalBG = -1
  Queue:Browse.tmp:Tag_SelectedFG = -1
  Queue:Browse.tmp:Tag_SelectedBG = -1
  IF (tmp:tag = '*' And stotmp:Received <> 1)
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = stotmp:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::8:QUEUE = glo:Queue
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer = stotmp:RecordNumber
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = stotmp:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
BuildList        ROUTINE

    SETCURSOR(CURSOR:WAIT)
    Access:RETSTOCK.Clearkey(res:Despatched_Key)
    res:Ref_Number = ret:Ref_Number
    res:Despatched = 'YES'
    SET(res:Despatched_Key,res:Despatched_Key)
    LOOP UNTIL Access:RETSTOCK.Next()
        IF (res:Ref_Number <> ret:Ref_Number OR |
            res:Despatched <> 'YES')
            BREAK
        END

        addNewPart# = 1
        IF (ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
            ! #12127 Group Parts If Exchange Order. Keep Received Seperate (Bryan: 05/07/2011)
            Access:STOCKRECEIVETMP.Clearkey(stotmp:ReceivedPartNumberKey)
            stotmp:Received = res:Received
            stotmp:PartNumber = res:Part_Number
            IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:ReceivedPartNumberKey) = Level:Benign)
                stotmp:Quantity += 1
                stotmp:QuantityReceived += res:QuantityReceived
                Access:STOCKRECEIVETMP.TryUpdate()
                addNewPart# = 0
            END
        END ! IF (ret:ExchangeOrder = 1)
        IF (addNewPart# = 1)

            IF (Access:STOCKRECEIVETMP.PrimeRecord() = Level:Benign)
                stotmp:PartNumber = res:Part_Number
                stotmp:Description = res:Description
                stotmp:ItemCost = res:Item_Cost
                stotmp:Quantity = res:Quantity
                stotmp:QuantityReceived = res:QuantityReceived
                stotmp:RESRecordNumber = res:Record_Number
                stotmp:Received = res:Received
                stotmp:ExchangeOrder = ret:ExchangeOrder
                stotmp:LoanOrder = ret:LoanOrder  ! #12341 Add Loan Orders (DBH: 26/01/2012)
                
                IF (Access:STOCKRECEIVETMP.TryInsert())
                    Access:STOCKRECEIVETMP.CancelAutoInc()
                END
            END
        END ! IF (addNewPart# = 1)
    END

    SETCURSOR()
    BRW1.ResetSort(1)
ClearList       ROUTINE
    SETCURSOR(CURSOR:WAIT)
    Access:STOCKRECEIVETMP.Clearkey(stotmp:RecordNumberKey)
    stotmp:RecordNumber = 1
    SET(stotmp:RecordNumberKey,stotmp:RecordNumberKey)
    LOOP UNTIL Access:STOCKRECEIVETMP.Next()
        Access:STOCKRECEIVETMP.DeleteRecord(0)
    END
    SETCURSOR()
    BRW1.ResetSort(1)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020494'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, AppFrame, 1)  !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReceiveStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Finish,RequestCancelled)
  ! Set Filename For Temp File
  glo:FileName = 'STOREC' & RANDOM(1,1000) & CLOCK() & '.TMP'
  Relate:GRNOTESR.Open
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  Relate:RETSALES.Open
  Relate:STOCKRECEIVETMP.Open
  Access:TRADEACC.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  Access:STOHIST.UseFile
  Access:SUBTRACC.UseFile
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:STOCKRECEIVETMP,SELF)
  OPEN(AppFrame)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If glo:WebJob
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = Clarionet:Global.Param2
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:SiteLocation    = tra:SiteLocation
          ?tmp:SiteLocation{prop:Disable} = 1
          ?LookupSiteLocation{prop:Hide} = 1
      Else ! If AccessTRADEACC.Tryfetch(traAccount_Number_Key) = LevelBenign
          !Error
      End !If AccessTRADEACC.Tryfetch(traAccount_Number_Key) = LevelBenign
  ELSE
      tmp:SiteLocation = use:Location
  End !glo:WebJob
      ! Save Window Name
   AddToLog('Window','Open','ReceiveStock')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:SiteLocation{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?tmp:SiteLocation{Prop:Tip}
  END
  IF ?tmp:SiteLocation{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?tmp:SiteLocation{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,stotmp:PartNumberKey)
  BRW1.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,stotmp:SessionID,1,BRW1)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tmp:Tag,BRW1.Q.tmp:Tag)
  BRW1.AddField(stotmp:PartNumber,BRW1.Q.stotmp:PartNumber)
  BRW1.AddField(stotmp:Description,BRW1.Q.stotmp:Description)
  BRW1.AddField(stotmp:ItemCost,BRW1.Q.stotmp:ItemCost)
  BRW1.AddField(stotmp:Quantity,BRW1.Q.stotmp:Quantity)
  BRW1.AddField(stotmp:QuantityReceived,BRW1.Q.stotmp:QuantityReceived)
  BRW1.AddField(stotmp:RESRecordNumber,BRW1.Q.stotmp:RESRecordNumber)
  BRW1.AddField(stotmp:ExchangeOrder,BRW1.Q.stotmp:ExchangeOrder)
  BRW1.AddField(stotmp:Received,BRW1.Q.stotmp:Received)
  BRW1.AddField(stotmp:RecordNumber,BRW1.Q.stotmp:RecordNumber)
  BRW1.AddField(stotmp:SessionID,BRW1.Q.stotmp:SessionID)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Tab 1'
    ?List{PROP:FORMAT} ='11L(2)*I@n1@#1#120L(2)|M*~Part Number~@s30@#7#120L(2)|M*~Description~@s30@#12#56R(2)|M*~Item Cost~L@n14.2@#17#37R(2)|M*~Quantity~L@s8@#22#32R(2)|M*~Qty Recvd~L@s8@#27#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:GRNOTESR.Close
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
    Relate:RETSALES.Close
    Relate:STOCKRECEIVETMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReceiveStock')
  REMOVE(glo:FileName)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSiteLocations
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Goods_Received_Note_Button
          ThisWindow.Update
          BRW1.UpdateBuffer()
      
          if tmp:InvoiceNumber <> 0
              tmp:Goods_Received_Date = today()
              if Goods_Received_Note_Window(tmp:Goods_Received_Date)          ! Call window prompting for received date
      
                  Goods_Received# = 0                                         ! Check if any parts have been received on this date
                  Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
                  ret:Invoice_Number = tmp:InvoiceNumber
                  If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                  !Found
                      Save_res_ID = Access:RETSTOCK.SaveFile()
                      Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                      res:Ref_Number  = ret:Ref_Number
                      Set(res:Part_Number_Key,res:Part_Number_Key)
                      Loop
                          If Access:RETSTOCK.NEXT()
                              Break
                          End !If
                          If res:Ref_Number  <> ret:Ref_Number      |
                              Then Break.  ! End If
                          If ~res:Received
                              Cycle
                          End !If ~ret:Received
                          If res:DateReceived = tmp:Goods_Received_Date
                              Goods_Received# = 1
                              break
                          End !If ret:DateReceived = tmp:Goods_Received_Date
                      End !Loop
                      Access:RETSTOCK.RestoreFile(Save_res_ID)
                  Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  End        !If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      
      
                  if Goods_Received# = 1
                      If Access:GRNOTESR.PrimeRecord() = Level:Benign
                          grr:Order_Number = tmp:InvoiceNumber
                          grr:Goods_Received_date = tmp:Goods_Received_Date
                          If Access:GRNOTESR.TryInsert() = Level:Benign
                          !Insert Successful
                              Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
                              ret:Invoice_Number = tmp:InvoiceNumber
                              If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      
                                  Save_res_ID = Access:RETSTOCK.SaveFile()
                                  Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                                  res:Ref_Number  = ret:Ref_Number
                                  Set(res:Part_Number_Key,res:Part_Number_Key)
                                  Loop
                                      If Access:RETSTOCK.NEXT()
                                          Break
                                      End !If
                                      If res:Ref_Number  <> ret:Ref_Number      |
                                          Then Break.  ! End If
                                      If ~res:Received
                                          Cycle
                                      End !If ~ret:Received
                                      If res:DateReceived = tmp:Goods_Received_Date
                                          If res:GRNNumber = 0
                                              res:GRNNumber = grr:Goods_Received_Number
                                              Access:RETSTOCK.Update()
                                          End !If ret:GRNNumber = 0
                                      End !If ret:DateReceived = tmp:Goods_Received_Date
                                  End !Loop
                                  Access:RETSTOCK.RestoreFile(Save_res_ID)
                                  GLO:Select1 = grr:Goods_Received_Number
                                  GLO:Select2 = tmp:InvoiceNumber
                                  GLO:Select3 = tmp:Goods_Received_Date
                                  Goods_Received_Note_Retail
                                  GLO:Select1 = ''
                                  GLO:Select2 = ''
                                  GLO:Select3 = ''
                              End !If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                          Else !If Access:GRNOTESR.TryInsert() = Level:Benign
                          !Insert Failed
                              Access:GRNOTESR.CancelAutoInc()
                          End !If Access:GRNOTESR.TryInsert() = Level:Benign
                      End !If Access:GRNOTESR.PrimeRecord() = Level:Benign
                  Else !if Goods_Received# = 1
                      Case Missive('No items were received on the selected date.','ServiceBase 3g',|
                          'mstop.jpg','/OK')
                      Of 1 ! OK Button
                      End ! Case Missive
                  End !if Goods_Received# = 1
              End !if Goods_Received_Note_Window(tmp:Goods_Received_Date)
          End !if tmp:InvoiceNumber <> 0
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020494'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020494'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020494'&'0')
      ***
    OF ?tmp:SiteLocation
      tmp:InvoiceNumber = 0
      DO ClearList
      DISPLAY()
      IF tmp:SiteLocation OR ?tmp:SiteLocation{Prop:Req}
        loc:Location = tmp:SiteLocation
        !Save Lookup Field Incase Of error
        look:tmp:SiteLocation        = tmp:SiteLocation
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:SiteLocation = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:SiteLocation = look:tmp:SiteLocation
            SELECT(?tmp:SiteLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = tmp:SiteLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:SiteLocation = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:SiteLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:SiteLocation)
    OF ?Button11
      ThisWindow.Update
      !This is a reprint - 3439 (DBH: 16-01-2004)
      Case Missive('Are you sure you want to re-print the GRN''s that have been produced for this invoice?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Save_grr_ID = Access:GRNOTESR.SaveFile()
              Access:GRNOTESR.ClearKey(grr:Order_Number_Key)
              grr:Order_Number        = tmp:InvoiceNumber
              Set(grr:Order_Number_Key,grr:Order_Number_Key)
              Loop
                  If Access:GRNOTESR.NEXT()
                     Break
                  End !If
                  If grr:Order_Number        <> tmp:InvoiceNumber      |
                      Then Break.  ! End If
                  GLO:Select1 = grr:Goods_Received_Number
                  GLO:Select2 = tmp:InvoiceNumber
                  GLO:Select3 = grr:Goods_Received_Date
                  glo:Select4 = 'REPRINT'
                  Goods_Received_Note_Retail
                  GLO:Select1 = ''
                  GLO:Select2 = ''
                  GLO:Select3 = ''
                  glo:Select4 = ''
              End !Loop
              Access:GRNOTESR.RestoreFile(Save_grr_ID)
          Of 1 ! No Button
      End ! Case Missive
    OF ?tmp:InvoiceNumber
      tmp:BrowseRefNumber = ''
      DO ClearList
      Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
      ret:Invoice_Number = tmp:InvoiceNumber
      If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
          !Found
          !Does the account number of the sale match either the account number
          !of the current user, or it's Stores Account?
      
          DoNormalCheck# = 0
          If glo:WebJob
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = Clarionet:Global.Param2
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:StoresAccount <> ''
                      If tra:StoresAccount <> ret:Account_Number
                          Error# = 1
                      End !If tra:StoresAccount <> ret:Account_Number
                  Else !If tra:Stores_Account <> ''
                      DoNormalCheck# = 1
                  End !If tra:Stores_Account <> ''
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
      
          Else !If glo:WebJob
              Access:TRADEACC.Clearkey(tra:Account_Number_Key)
              tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
              If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:StoresAccount <> ''
                      If tra:StoresAccount <> ret:Account_Number
                          Error# = 1
                      End !If tra:StoresAccount <> ret:Account_Number
                  Else !If tra:Stores_Account <> ''
                      DoNormalCheck# = 1
                  End !If tra:Stores_Account <> ''
              Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          End !If glo:WebJob
      
          If DoNormalCheck#
              !Look up Head Account!
              Access:SubTrAcc.ClearKey(sub:Account_Number_Key)
              sub:Account_Number = ret:Account_Number
              IF Access:SubTracc.Fetch(sub:Account_Number_Key)
                !Error!
              ELSE
                Access:TradeAcc.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
                  !Error!
                ELSE
                  IF tmp:SiteLocation <> tra:SiteLocation
                      Error# = 1
                  END
                END
              END
      
          End !If DoNormalCheck#
      
          If Error# = 1
              Case Missive('This invoice number is not for this site location.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?tmp:InvoiceNumber)
              ?ProcessTagged{prop:Disable} = 1
          ELSE
            tmp:BrowseRefNumber = ret:Ref_Number
            DO BuildList
            ?ProcessTagged{prop:Disable} = 0
             Post(Event:Accepted,?DasUnTagAll)
          End !If Error# = 1
      
      Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
          !Error
          Case Missive('Cannot find the selected invoice number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:InvoiceNumber)
          ?ProcessTagged{prop:Disable} = 1
      End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      BRW1.ResetSort(1)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?buttonAmendQty
      ThisWindow.Update
      BRW1.UpdateViewRecord()
      IF (stotmp:RESRecordNumber > 0)
          Access:RETSTOCK.Clearkey(res:Record_Number_Key)
          res:Record_Number = stotmp:RESRecordNumber
          IF (Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign)
              GlobalRequest = ChangeRecord
              AdjustQuantity(tmp:SiteLocation)
              IF (GlobalResponse = RequestCompleted)
                  DO ClearList
                  Do BuildList
              END
          END
      END
    OF ?ProcessTagged
      ThisWindow.Update
      IF (RECORDS(glo:Queue) = 0)
          CYCLE
      END
      
      IF (ret:ExchangeOrder = 1)
          ! #12127 Call procedure to scan/add imei numbers (Bryan: 05/07/2011)
          ReceiveExchangeLoanStock(ret:Ref_Number,0)
          DO ClearList
          Do BuildList
          CYCLE
      END
      IF (ret:LoanOrder = 1)
          ! #12127 Call procedure to scan/add imei numbers (Bryan: 05/07/2011)
          ReceiveExchangeLoanStock(ret:Ref_Number,1)
          DO ClearList
          Do BuildList
          CYCLE
      END
      
      
      Case Missive('Are you sure you want to receive the tagged items?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:queue,x#)
                  Access:STOCKRECEIVETMP.Clearkey(stotmp:RecordNumberKey)
                  stotmp:RecordNumber = glo:Pointer
                  IF (Access:STOCKRECEIVETMP.Tryfetch(stotmp:RecordNumberKey))
                      CYCLE
                  END
      
                  Access:RETSTOCK.Clearkey(res:Record_Number_Key)
                  res:Record_Number   = stotmp:RESRecordNumber
                  If Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign
                      !Found
                      If ~res:Received
                          Access:STOCK.ClearKey(sto:Location_Key)
                          sto:Location    = tmp:SiteLocation
                          sto:Part_Number = res:Part_Number
                          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock += res:QuantityReceived
                              If Access:STOCK.Update() = Level:Benign
                                  If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                     'ADD', | ! Transaction_Type
                                                     '', | ! Depatch_Note_Number
                                                     0, | ! Job_Number
                                                     0, | ! Sales_Number
                                                     res:Quantity, | ! Quantity
                                                     res:Item_Cost, | ! Purchase_Cost
                                                     res:Sale_Cost, | ! Sale_Cost
                                                     res:Retail_Cost, | ! Retail_Cost
                                                     'STOCK ADDED FROM ORDER', | ! Notes
                                                     'RAPID STOCK RECEIVED <13,10>INVOICE NO: ' & tmp:InvoiceNumber) ! Information
                                    ! Added OK
                                  Else ! AddToStockHistory
                                    ! Error
                                  End ! AddToStockHistory
                                  ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                  if (sto:Suspend)
                                      sto:Suspend = 0
                                      If (Access:STOCK.TryUpdate() = Level:Benign)
                                          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                           'ADD', | ! Transaction_Type
                                                           '', | ! Depatch_Note_Number
                                                           0, | ! Job_Number
                                                           0, | ! Sales_Number
                                                           0, | ! Quantity
                                                           res:Item_Cost, | ! Purchase_Cost
                                                           res:Sale_Cost, | ! Sale_Cost
                                                           res:Retail_Cost, | ! Retail_Cost
                                                           'PART UNSUSPENDED', | ! Notes
                                                           '') ! Information
                                          End ! AddToStockHistory
                                      End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                  end
                                  If res:QuantityReceived < res:Quantity
                                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                         'DEC', | ! Transaction_Type
                                                         '', | ! Depatch_Note_Number
                                                         0, | ! Job_Number
                                                         0, | ! Sales_Number
                                                         res:Quantity - res:QuantityReceived, | ! Quantity
                                                         res:Item_Cost, | ! Purchase_Cost
                                                         res:Sale_Cost, | ! Sale_Cost
                                                         res:Retail_Cost, | ! Retail_Cost
                                                         'RAPID STOCK RECEIVED ADJUSTMENT', | ! Notes
                                                         'INVOICE NO: ' & tmp:InvoiceNUmber) ! Information
                                        ! Added OK
                                      Else ! AddToStockHistory
                                        ! Error
                                      End ! AddToStockHistory
                                  End !If res:QuantityReceived < res:Quantity
                                  res:Received = 1
                                  res:DateReceived = Today()
                                  Access:RETSTOCK.Update()
      
                                  !Only work out average if more than 0 is received
                                  If res:QuantityReceived > 0
                                      AvPurchaseCost$ = sto:AveragePurchaseCost
                                      PurchaseCost$ = sto:Purchase_Cost
                                      SaleCost$ = sto:Sale_Cost
                                      RetailCost$ = sto:Retail_Cost
      
                                      If VirtualSite(tmp:SiteLocation)
                                          sto:AveragePurchaseCost = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_stock,sto:AveragePurchaseCost),@n14.2)
                                          sto:Purchase_Cost   = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,sto:PurchaseMarkup)
                                          sto:Sale_Cost       = Markups(sto:Sale_Cost,sto:AveragePurchaseCost,sto:Percentage_Mark_Up)
                                      Else !If VirtualSite(tmp:Location)
                                          sto:Purchase_Cost       = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_stock,sto:Purchase_Cost),@n14.2)
                                          sto:Sale_Cost       = Markups(sto:Sale_Cost,sto:Purchase_Cost,sto:Percentage_Mark_Up)
                                          sto:Retail_Cost     = Markups(sto:Retail_Cost,sto:Purchase_Cost,sto:RetailMarkUp)
                                      End !If VirtualSite(tmp:Location)
                                      !Write record of information!
                                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                         'ADD', | ! Transaction_Type
                                                         '', | ! Depatch_Note_Number
                                                         0, | ! Job_Number
                                                         0, | ! Sales_Number
                                                         0, | ! Quantity
                                                         sto:Purchase_Cost, | ! Purchase_Cost
                                                         sto:Sale_Cost, | ! Sale_Cost
                                                         sto:Retail_Cost, | ! Retail_Cost
                                                         'AVERAGE COST CALCULATION', | ! Notes
                                                         Clip(glo:ErrorText),|
                                                          AvPurchaseCost$,|
                                                           PurchaseCost$,|
                                                              SaleCost$,|
                                                                     RetailCost$)
                                        ! Added OK
                                      Else ! AddToStockHistory
                                        ! Error
                                      End ! AddToStockHistory
                                  End !If res:QuantityReceived > 0
      
                                  Access:STOCK.Update()
                              End !If Access:STOCK.Update() = Level:Benign
                          Else!If Access:STOCK.TryFetch(sto:Location_Part_Description_Key) = Level:Benign
                              !Error
                              Case Missive('Unable to find Part Number ' & Clip(res:Part_Number) & ' in your Site Location.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                          End!If Access:STOCK.TryFetch(sto:Location_Part_Description_Key) = Level:Benign
                      End !If ~res:Recevied
                  Else! If Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:RETSTOCK.Tryfetch(res:Record_Number_Key) = Level:Benign
                  Do ClearList
                  Do BuildList
              End !Loop x# = 1 To Records(glo:Queue)
          Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet1)
        OF 1
          ?List{PROP:FORMAT} ='11L(2)*I@n1@#1#120L(2)|M*~Part Number~@s30@#7#120L(2)|M*~Description~@s30@#12#56R(2)|M*~Item Cost~L@n14.2@#17#37R(2)|M*~Quantity~L@s8@#22#32R(2)|M*~Qty Recvd~L@s8@#27#'
          ?Tab1{PROP:TEXT} = 'Tab 1'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  PARENT.ResetQueue(ResetMode)
  IF (RECORDS(SELF.ListQueue) = 0 OR ret:ExchangeOrder = 1 OR ret:LoanOrder = 1)
      ?buttonAmendQty{prop:Disable} = 1
  ELSE
      ?buttonAmendQty{prop:Disable} = 0
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stotmp:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  SELF.Q.tmp:Tag_NormalFG = -1
  SELF.Q.tmp:Tag_NormalBG = -1
  SELF.Q.tmp:Tag_SelectedFG = -1
  SELF.Q.tmp:Tag_SelectedBG = -1
  IF (tmp:tag = '*' And stotmp:Received <> 1)
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END
  SELF.Q.stotmp:PartNumber_NormalFG = -1
  SELF.Q.stotmp:PartNumber_NormalBG = -1
  SELF.Q.stotmp:PartNumber_SelectedFG = -1
  SELF.Q.stotmp:PartNumber_SelectedBG = -1
  SELF.Q.stotmp:Description_NormalFG = -1
  SELF.Q.stotmp:Description_NormalBG = -1
  SELF.Q.stotmp:Description_SelectedFG = -1
  SELF.Q.stotmp:Description_SelectedBG = -1
  SELF.Q.stotmp:ItemCost_NormalFG = -1
  SELF.Q.stotmp:ItemCost_NormalBG = -1
  SELF.Q.stotmp:ItemCost_SelectedFG = -1
  SELF.Q.stotmp:ItemCost_SelectedBG = -1
  SELF.Q.stotmp:Quantity_NormalFG = -1
  SELF.Q.stotmp:Quantity_NormalBG = -1
  SELF.Q.stotmp:Quantity_SelectedFG = -1
  SELF.Q.stotmp:Quantity_SelectedBG = -1
  SELF.Q.stotmp:QuantityReceived_NormalFG = -1
  SELF.Q.stotmp:QuantityReceived_NormalBG = -1
  SELF.Q.stotmp:QuantityReceived_SelectedFG = -1
  SELF.Q.stotmp:QuantityReceived_SelectedBG = -1
  SELF.Q.stotmp:RESRecordNumber_NormalFG = -1
  SELF.Q.stotmp:RESRecordNumber_NormalBG = -1
  SELF.Q.stotmp:RESRecordNumber_SelectedFG = -1
  SELF.Q.stotmp:RESRecordNumber_SelectedBG = -1
  SELF.Q.stotmp:ExchangeOrder_NormalFG = -1
  SELF.Q.stotmp:ExchangeOrder_NormalBG = -1
  SELF.Q.stotmp:ExchangeOrder_SelectedFG = -1
  SELF.Q.stotmp:ExchangeOrder_SelectedBG = -1
  SELF.Q.stotmp:Received_NormalFG = -1
  SELF.Q.stotmp:Received_NormalBG = -1
  SELF.Q.stotmp:Received_SelectedFG = -1
  SELF.Q.stotmp:Received_SelectedBG = -1
   
   
   IF (stotmp:Received = 1)
     SELF.Q.tmp:Tag_NormalFG = 8421504
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.tmp:Tag_NormalFG = 32768
     SELF.Q.tmp:Tag_NormalBG = 16777215
     SELF.Q.tmp:Tag_SelectedFG = 16777215
     SELF.Q.tmp:Tag_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.tmp:Tag_NormalFG = 4210816
     SELF.Q.tmp:Tag_NormalBG = -1
     SELF.Q.tmp:Tag_SelectedFG = -1
     SELF.Q.tmp:Tag_SelectedBG = 4210816
   ELSE
     SELF.Q.tmp:Tag_NormalFG = -1
     SELF.Q.tmp:Tag_NormalBG = -1
     SELF.Q.tmp:Tag_SelectedFG = -1
     SELF.Q.tmp:Tag_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:PartNumber_NormalFG = 8421504
     SELF.Q.stotmp:PartNumber_NormalBG = 16777215
     SELF.Q.stotmp:PartNumber_SelectedFG = 16777215
     SELF.Q.stotmp:PartNumber_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:PartNumber_NormalFG = 32768
     SELF.Q.stotmp:PartNumber_NormalBG = 16777215
     SELF.Q.stotmp:PartNumber_SelectedFG = 16777215
     SELF.Q.stotmp:PartNumber_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:PartNumber_NormalFG = 4210816
     SELF.Q.stotmp:PartNumber_NormalBG = -1
     SELF.Q.stotmp:PartNumber_SelectedFG = -1
     SELF.Q.stotmp:PartNumber_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:PartNumber_NormalFG = -1
     SELF.Q.stotmp:PartNumber_NormalBG = -1
     SELF.Q.stotmp:PartNumber_SelectedFG = -1
     SELF.Q.stotmp:PartNumber_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:Description_NormalFG = 8421504
     SELF.Q.stotmp:Description_NormalBG = 16777215
     SELF.Q.stotmp:Description_SelectedFG = 16777215
     SELF.Q.stotmp:Description_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:Description_NormalFG = 32768
     SELF.Q.stotmp:Description_NormalBG = 16777215
     SELF.Q.stotmp:Description_SelectedFG = 16777215
     SELF.Q.stotmp:Description_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:Description_NormalFG = 4210816
     SELF.Q.stotmp:Description_NormalBG = -1
     SELF.Q.stotmp:Description_SelectedFG = -1
     SELF.Q.stotmp:Description_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:Description_NormalFG = -1
     SELF.Q.stotmp:Description_NormalBG = -1
     SELF.Q.stotmp:Description_SelectedFG = -1
     SELF.Q.stotmp:Description_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:ItemCost_NormalFG = 8421504
     SELF.Q.stotmp:ItemCost_NormalBG = 16777215
     SELF.Q.stotmp:ItemCost_SelectedFG = 16777215
     SELF.Q.stotmp:ItemCost_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:ItemCost_NormalFG = 32768
     SELF.Q.stotmp:ItemCost_NormalBG = 16777215
     SELF.Q.stotmp:ItemCost_SelectedFG = 16777215
     SELF.Q.stotmp:ItemCost_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:ItemCost_NormalFG = 4210816
     SELF.Q.stotmp:ItemCost_NormalBG = -1
     SELF.Q.stotmp:ItemCost_SelectedFG = -1
     SELF.Q.stotmp:ItemCost_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:ItemCost_NormalFG = -1
     SELF.Q.stotmp:ItemCost_NormalBG = -1
     SELF.Q.stotmp:ItemCost_SelectedFG = -1
     SELF.Q.stotmp:ItemCost_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:Quantity_NormalFG = 8421504
     SELF.Q.stotmp:Quantity_NormalBG = 16777215
     SELF.Q.stotmp:Quantity_SelectedFG = 16777215
     SELF.Q.stotmp:Quantity_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:Quantity_NormalFG = 32768
     SELF.Q.stotmp:Quantity_NormalBG = 16777215
     SELF.Q.stotmp:Quantity_SelectedFG = 16777215
     SELF.Q.stotmp:Quantity_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:Quantity_NormalFG = 4210816
     SELF.Q.stotmp:Quantity_NormalBG = -1
     SELF.Q.stotmp:Quantity_SelectedFG = -1
     SELF.Q.stotmp:Quantity_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:Quantity_NormalFG = -1
     SELF.Q.stotmp:Quantity_NormalBG = -1
     SELF.Q.stotmp:Quantity_SelectedFG = -1
     SELF.Q.stotmp:Quantity_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:QuantityReceived_NormalFG = 8421504
     SELF.Q.stotmp:QuantityReceived_NormalBG = 16777215
     SELF.Q.stotmp:QuantityReceived_SelectedFG = 16777215
     SELF.Q.stotmp:QuantityReceived_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:QuantityReceived_NormalFG = 32768
     SELF.Q.stotmp:QuantityReceived_NormalBG = 16777215
     SELF.Q.stotmp:QuantityReceived_SelectedFG = 16777215
     SELF.Q.stotmp:QuantityReceived_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:QuantityReceived_NormalFG = 4210816
     SELF.Q.stotmp:QuantityReceived_NormalBG = -1
     SELF.Q.stotmp:QuantityReceived_SelectedFG = -1
     SELF.Q.stotmp:QuantityReceived_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:QuantityReceived_NormalFG = -1
     SELF.Q.stotmp:QuantityReceived_NormalBG = -1
     SELF.Q.stotmp:QuantityReceived_SelectedFG = -1
     SELF.Q.stotmp:QuantityReceived_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:RESRecordNumber_NormalFG = 8421504
     SELF.Q.stotmp:RESRecordNumber_NormalBG = 16777215
     SELF.Q.stotmp:RESRecordNumber_SelectedFG = 16777215
     SELF.Q.stotmp:RESRecordNumber_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:RESRecordNumber_NormalFG = 32768
     SELF.Q.stotmp:RESRecordNumber_NormalBG = 16777215
     SELF.Q.stotmp:RESRecordNumber_SelectedFG = 16777215
     SELF.Q.stotmp:RESRecordNumber_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:RESRecordNumber_NormalFG = 4210816
     SELF.Q.stotmp:RESRecordNumber_NormalBG = -1
     SELF.Q.stotmp:RESRecordNumber_SelectedFG = -1
     SELF.Q.stotmp:RESRecordNumber_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:RESRecordNumber_NormalFG = -1
     SELF.Q.stotmp:RESRecordNumber_NormalBG = -1
     SELF.Q.stotmp:RESRecordNumber_SelectedFG = -1
     SELF.Q.stotmp:RESRecordNumber_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:ExchangeOrder_NormalFG = 8421504
     SELF.Q.stotmp:ExchangeOrder_NormalBG = 16777215
     SELF.Q.stotmp:ExchangeOrder_SelectedFG = 16777215
     SELF.Q.stotmp:ExchangeOrder_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:ExchangeOrder_NormalFG = 32768
     SELF.Q.stotmp:ExchangeOrder_NormalBG = 16777215
     SELF.Q.stotmp:ExchangeOrder_SelectedFG = 16777215
     SELF.Q.stotmp:ExchangeOrder_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:ExchangeOrder_NormalFG = 4210816
     SELF.Q.stotmp:ExchangeOrder_NormalBG = -1
     SELF.Q.stotmp:ExchangeOrder_SelectedFG = -1
     SELF.Q.stotmp:ExchangeOrder_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:ExchangeOrder_NormalFG = -1
     SELF.Q.stotmp:ExchangeOrder_NormalBG = -1
     SELF.Q.stotmp:ExchangeOrder_SelectedFG = -1
     SELF.Q.stotmp:ExchangeOrder_SelectedBG = -1
   END
   IF (stotmp:Received = 1)
     SELF.Q.stotmp:Received_NormalFG = 8421504
     SELF.Q.stotmp:Received_NormalBG = 16777215
     SELF.Q.stotmp:Received_SelectedFG = 16777215
     SELF.Q.stotmp:Received_SelectedBG = 8421504
   ELSIF(stotmp:ExchangeOrder = 1)
     SELF.Q.stotmp:Received_NormalFG = 32768
     SELF.Q.stotmp:Received_NormalBG = 16777215
     SELF.Q.stotmp:Received_SelectedFG = 16777215
     SELF.Q.stotmp:Received_SelectedBG = 32768
   ELSIF(stotmp:LoanOrder = 1)
     SELF.Q.stotmp:Received_NormalFG = 4210816
     SELF.Q.stotmp:Received_NormalBG = -1
     SELF.Q.stotmp:Received_SelectedFG = -1
     SELF.Q.stotmp:Received_SelectedBG = 4210816
   ELSE
     SELF.Q.stotmp:Received_NormalFG = -1
     SELF.Q.stotmp:Received_NormalBG = -1
     SELF.Q.stotmp:Received_SelectedFG = -1
     SELF.Q.stotmp:Received_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = stotmp:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

Parts_Key PROCEDURE                                   !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Parts Key'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Parts Colour Key'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,92),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           GROUP('Key'),AT(272,166,136,84),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(280,191,12,8),USE(?Panel1),FILL(COLOR:Black)
                             PROMPT('- Part Awaiting Processing'),AT(300,191),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('- Part On Order'),AT(300,175),USE(?Prompt1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(280,175,12,8),USE(?Panel1:2),FILL(COLOR:Purple)
                             PANEL,AT(280,207,12,8),USE(?Panel1:3),FILL(COLOR:Fuschia)
                             PROMPT('- Part Awaiting Picking'),AT(300,207),USE(?Prompt1:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('- Part Picked'),AT(300,223),USE(?Prompt1:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(280,239,12,8),USE(?Panel1:7),FILL(COLOR:Red)
                             PROMPT('- Part Awaiting Return'),AT(300,239),USE(?Prompt1:7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PANEL,AT(280,223,12,8),USE(?Panel1:5),FILL(COLOR:Blue)
                           END
                         END
                       END
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020484'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Parts_Key')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Parts_Key')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Parts_Key')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020484'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020484'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020484'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RSA_MainBrowse PROCEDURE                              !Generated from procedure template - Window

save_rapsto_id       USHORT,AUTO
tmp:StockRefNo       LONG
Web_Temp_Job         STRING(20)
save_epr_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_stl_id          USHORT,AUTO
Local                CLASS
Validation           Procedure(String func:Type),Byte
AddFaulty            Procedure(String func:Type)
RemovePartFromStock  Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber),Byte
FindOtherParts       Procedure(Long func:JobNumber,Long func:RecordNumber),Byte
BuildChaList         Procedure(String func:Status)
BuildWarList         Procedure(String func:Status)
BuildEstList         Procedure(String func:Status)
ChangePartStatus     Procedure(Long func:RecordNumber,String func:Status)
                     END
tmp:Count            LONG
tmp:Location         STRING(30)
tmp:Engineer         STRING(3)
tmp:FullEngineer     STRING(60)
tmp:ColourFlag       STRING(3)
tmp:Status           STRING('XXX')
tmp:Web_Temp_Job     STRING(20)
Blank_Byte           STRING(1)
Pro_Bye              STRING('PRO')
PIK_Byte             STRING('PIK')
RET_Byte             STRING('RET')
Web_String           STRING('WEB')
FoundAnyPart         BYTE
FoundOrderedPart     BYTE
tmp:StockAvailable   STRING(1)
tmp:JobNumber        LONG
RapidQueue           QUEUE,PRE(rapque)
RecordNumber         LONG
                     END
RapidJobQueue        QUEUE,PRE(jobque)
JobNumber            LONG
                     END
tmp:QuantityStock    STRING(8)
tmp:ARCLocation      STRING(30)
tmp:CurrentLocation  STRING(30)
tmp:UseRapid         BYTE(0)
tmp:ARCSiteLocation  STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ReqDate              STRING(10)
ReqTime              STRING(10)
AA20Relocated        BYTE
BRW2::View:Browse    VIEW(STOCKALL)
                       PROJECT(stl:ShelfLocation)
                       PROJECT(stl:Description)
                       PROJECT(stl:PartNumber)
                       PROJECT(stl:Quantity)
                       PROJECT(stl:RecordNumber)
                       PROJECT(stl:Location)
                       PROJECT(stl:Engineer)
                       PROJECT(stl:JobNumber)
                       PROJECT(stl:Status)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
stl:ShelfLocation      LIKE(stl:ShelfLocation)        !List box control field - type derived from field
stl:ShelfLocation_NormalFG LONG                       !Normal forground color
stl:ShelfLocation_NormalBG LONG                       !Normal background color
stl:ShelfLocation_SelectedFG LONG                     !Selected forground color
stl:ShelfLocation_SelectedBG LONG                     !Selected background color
stl:Description        LIKE(stl:Description)          !List box control field - type derived from field
stl:Description_NormalFG LONG                         !Normal forground color
stl:Description_NormalBG LONG                         !Normal background color
stl:Description_SelectedFG LONG                       !Selected forground color
stl:Description_SelectedBG LONG                       !Selected background color
stl:PartNumber         LIKE(stl:PartNumber)           !List box control field - type derived from field
stl:PartNumber_NormalFG LONG                          !Normal forground color
stl:PartNumber_NormalBG LONG                          !Normal background color
stl:PartNumber_SelectedFG LONG                        !Selected forground color
stl:PartNumber_SelectedBG LONG                        !Selected background color
tmp:FullEngineer       LIKE(tmp:FullEngineer)         !List box control field - type derived from local data
tmp:FullEngineer_NormalFG LONG                        !Normal forground color
tmp:FullEngineer_NormalBG LONG                        !Normal background color
tmp:FullEngineer_SelectedFG LONG                      !Selected forground color
tmp:FullEngineer_SelectedBG LONG                      !Selected background color
Web_Temp_Job           LIKE(Web_Temp_Job)             !List box control field - type derived from local data
Web_Temp_Job_NormalFG  LONG                           !Normal forground color
Web_Temp_Job_NormalBG  LONG                           !Normal background color
Web_Temp_Job_SelectedFG LONG                          !Selected forground color
Web_Temp_Job_SelectedBG LONG                          !Selected background color
stl:Quantity           LIKE(stl:Quantity)             !List box control field - type derived from field
stl:Quantity_NormalFG  LONG                           !Normal forground color
stl:Quantity_NormalBG  LONG                           !Normal background color
stl:Quantity_SelectedFG LONG                          !Selected forground color
stl:Quantity_SelectedBG LONG                          !Selected background color
tmp:QuantityStock      LIKE(tmp:QuantityStock)        !List box control field - type derived from local data
tmp:QuantityStock_NormalFG LONG                       !Normal forground color
tmp:QuantityStock_NormalBG LONG                       !Normal background color
tmp:QuantityStock_SelectedFG LONG                     !Selected forground color
tmp:QuantityStock_SelectedBG LONG                     !Selected background color
tmp:StockAvailable     LIKE(tmp:StockAvailable)       !List box control field - type derived from local data
tmp:StockAvailable_NormalFG LONG                      !Normal forground color
tmp:StockAvailable_NormalBG LONG                      !Normal background color
tmp:StockAvailable_SelectedFG LONG                    !Selected forground color
tmp:StockAvailable_SelectedBG LONG                    !Selected background color
tmp:StockAvailable_Icon LONG                          !Entry's icon ID
stl:RecordNumber       LIKE(stl:RecordNumber)         !List box control field - type derived from field
stl:RecordNumber_NormalFG LONG                        !Normal forground color
stl:RecordNumber_NormalBG LONG                        !Normal background color
stl:RecordNumber_SelectedFG LONG                      !Selected forground color
stl:RecordNumber_SelectedBG LONG                      !Selected background color
ReqDate                LIKE(ReqDate)                  !List box control field - type derived from local data
ReqDate_NormalFG       LONG                           !Normal forground color
ReqDate_NormalBG       LONG                           !Normal background color
ReqDate_SelectedFG     LONG                           !Selected forground color
ReqDate_SelectedBG     LONG                           !Selected background color
ReqTime                LIKE(ReqTime)                  !List box control field - type derived from local data
ReqTime_NormalFG       LONG                           !Normal forground color
ReqTime_NormalBG       LONG                           !Normal background color
ReqTime_SelectedFG     LONG                           !Selected forground color
ReqTime_SelectedBG     LONG                           !Selected background color
stl:Location           LIKE(stl:Location)             !Browse key field - type derived from field
stl:Engineer           LIKE(stl:Engineer)             !Browse key field - type derived from field
stl:JobNumber          LIKE(stl:JobNumber)            !Browse key field - type derived from field
stl:Status             LIKE(stl:Status)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK19::stl:Engineer        LIKE(stl:Engineer)
HK19::stl:Location        LIKE(stl:Location)
HK19::stl:Status          LIKE(stl:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Rapid Stock Allocation'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Utilities'),USE(?Utilities)
                           ITEM('Refresh List'),USE(?UtilitiesRefreshList)
                         END
                       END
                       PROMPT('Window Title'),AT(0,-31),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(580,13),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,9,632,18),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(640,1,36,32),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       SHEET,AT(4,31,672,354),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('By Shelf Location'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(20,63,124,10),USE(stl:ShelfLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Shelf Location'),TIP('Shelf Location'),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(20,63,124,10),USE(stl:Description),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description'),TIP('Description'),UPR
                         END
                         TAB('By Part No'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(20,63,124,10),USE(stl:PartNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number'),TIP('Part Number'),UPR
                         END
                         TAB('By Engineer'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(20,63,64,10),USE(stl:JobNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                           ENTRY(@s3),AT(116,63,44,10),USE(tmp:Engineer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Engineer'),TIP('Engineer'),UPR
                           BUTTON,AT(164,58),USE(?LookupEngineer),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Engineer'),AT(192,63),USE(?tmp:Engineer:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Awaiting Picking'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Picking In Progress'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Picked'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Awaiting Return'),USE(?Tab8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Parts On Order'),USE(?Tab9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(20,63,64,10),USE(stl:JobNumber,,?stl:JobNumber:2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                           BUTTON,AT(432,352),USE(?AutoFulFill),TRN,FLAT,LEFT,ICON('autfullp.jpg')
                           BUTTON,AT(496,352),USE(?Button10),TRN,FLAT,LEFT,ICON('fulfillp.jpg')
                         END
                       END
                       LIST,AT(20,85,624,250),USE(?List),IMM,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('100L(2)|FM*~Shelf Location~@s30@90L(2)|FM*~Description~@s30@110L(2)|FM*~Part Num' &|
   'ber~@s30@100L(2)|FM*~Engineer~@s60@85L(2)|FM*~Job Number~@s20@20L(2)|FM*~Qty~@s8' &|
   '@34R(2)|FM*~Stock~@s8@11L(2)|F*I@s1@32L(2)|F*~Record Number~@s8@40L(2)|F*~Req Da' &|
   'te~@s10@40L(2)|F*~Req Time~@s10@'),FROM(Queue:Browse)
                       GROUP('Group 1'),AT(68,347,364,24),USE(?Group1)
                         BUTTON,AT(68,352),USE(?ProcessPicking),TRN,FLAT,LEFT,ICON('propikp.jpg')
                         BUTTON,AT(212,352),USE(?AllocatePart),TRN,FLAT,LEFT,ICON('allpartp.jpg')
                         BUTTON,AT(140,352),USE(?Picked),TRN,FLAT,LEFT,ICON('pickp.jpg')
                         BUTTON,AT(284,352),USE(?AllocateExchange),TRN,FLAT,LEFT,FONT(,,,,CHARSET:ANSI),ICON('resexcp.jpg')
                         BUTTON,AT(356,352),USE(?ReturnPart),TRN,FLAT,LEFT,ICON('retpartp.jpg')
                       END
                       PANEL,AT(4,389,672,26),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(612,389),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Site Location: '),AT(8,13),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@s30),AT(68,13),USE(tmp:Location),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(612,55),USE(?ColourKey),TRN,FLAT,LEFT,ICON('keyp.jpg')
                     END

!static webjob window
webjobwindow WINDOW,AT(,,165,42),FONT('Tahoma',8,,),COLOR(COLOR:White),CENTER,IMM,GRAY,DOUBLE,AUTO
       STRING('Running Report'),AT(12,8,144,16),USE(?Str1),TRN,CENTER,FONT(,18,COLOR:Red,FONT:bold)
       STRING('Please Wait'),AT(56,28),USE(?String2),TRN,FONT('Tahoma',10,COLOR:Navy,FONT:regular+FONT:italic,CHARSET:ANSI)
     END
DefModelQueue   Queue,Type
Manufacturer        String(30)
ModelNumber         String(30)
Quantity            Long
                End

PassModelQueue  Queue(DefModelQueue)
                End

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 1
BRW2::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 2
BRW2::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 3
BRW2::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 4
BRW2::Sort5:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 5
BRW2::Sort6:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 6
BRW2::Sort7:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 7
BRW2::Sort8:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 8
BRW2::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?Sheet1) = 9
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
SkipCorruptJobs     PROCEDURE(Long fRefNumber),BYTE
    END
!Save Entry Fields Incase Of Lookup
look:tmp:Engineer                Like(tmp:Engineer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

UpdateStockAlx   Routine

    !completed just before they delete the original
    Access:StockAlx.clearkey(STLX:KeySTLRecordNumber)
    STLX:StlRecordNumber = stl:RecordNumber
    if access:StockAlx.fetch(STLX:KeySTLRecordNumber) = level:Benign
        if stl:Location = 'MAIN STORE' then
            !message('Not deleting main store - 0')
            STLX:StlRecordNumber = 0        !need to update this so it does not interfere in the browse and record number will be reused
            STLX:AllocateDate = today()
            STLX:AllocateTime = clock()
            STLX:AllocateUser = use:User_Code
            Access:StockAlx.update()

        ELSE
            !message('About to delete stockalx - 1')
            Relate:StockAlx.delete(0)   !bug fix for TB12867 - changed from delete() to delete(0) to avoid the confirmation question
        END
    END !if stockalx.fetch

    EXIT
RefreshList     Routine

    Prog.ProgressSetup(Records(PARTS) + RECORDS(WARPARTS))

    !Empty File
    Save_stl_ID = Access:STOCKALL.SaveFile()
    Access:STOCKALL.ClearKey(stl:PartNumberKey)
    stl:Location   = tmp:Location
    Set(stl:PartNumberKey,stl:PartNumberKey)
    Loop
        If Access:STOCKALL.NEXT()
           Break
        End !If
        If stl:Location   <> tmp:Location      |
            Then Break.  ! End If
        !Delete the StockALL entries - 3451 (DBH: 28-10-2003)
        !message('About to delete stockal1 - 2')
        DELETE(STOCKALL)
    End !Loop
    Access:STOCKALL.RestoreFile(Save_stl_ID)


    Stream(PARTS)
    Stream(WARPARTS)
    Stream(STOCKALL)

    !Awaiting Picking

    If tmp:UseRapid = 1
        Local.BuildChaList('')
        Local.BuildChaList('PRO')
        Local.BuildChaList('PIK')
        Local.BuildChaList('RET')
        Local.BuildChaList('WEB')
        Local.BuildChaList('RTS')
        Local.BuildChaList('REQ')

        Local.BuildWarList('')
        Local.BuildWarList('PRO')
        Local.BuildWarList('PIK')
        Local.BuildWarList('RET')
        Local.BuildWarList('WEB')
        Local.BuildWarList('RTS')
        Local.BuildWarList('REQ')

        Local.BuildEstList('')
        Local.BuildEstList('PRO')
        Local.BuildEstList('PIK')
        Local.BuildEstList('RET')
        Local.BuildEstList('WEB')
        Local.BuildEstList('RTS')
        Local.BuildEstList('REQ')
    End !If tmp:UseRapid = 1


    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:WebOrderKey)
    par:WebOrder    = 1
    Set(par:WebOrderKey,par:WebOrderKey)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:WebOrder    <> 1      |
            Then Break.  ! End If

        If Prog.InsideLoop()

        End !If Prog.InsideLoop()

        ! #11965 Ignore cancelled, or corrupt "missing" jobs. (Bryan: 03/02/2011)
        IF (SkipCorruptJobs(par:Ref_Number))
            CYCLE
        END

        !edited back in by Paul 19/04/2010 - log no 11298
        If par:PartAllocated <> 0
            Cycle
        End !If par:PartAllocated <> 0
        !edit end

        If Local.Validation('C')
            Cycle
        End !If Local.Validation('C')


        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
            !Error!
        ELSE
            If sto:Location <> tmp:Location
                Cycle
            End !If sto:Location <> tmp:Location
        END
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = par:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'WEB',job:Engineer)
        END

    End !Loop

    Access:PARTS.RestoreFile(Save_par_ID)
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:WebOrderKey)
    wpr:WebOrder    = 1
    Set(wpr:WebOrderKey,wpr:WebOrderKey)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:WebOrder    <> 1      |
            Then Break.  ! End If

        ! #11965 Ignore cancelled, or corrupt "missing" jobs. (Bryan: 03/02/2011)
        IF (SkipCorruptJobs(wpr:Ref_Number))
            CYCLE
        END

        !edited back in by Paul 19/04/2010 - log no 11298
        If wpr:PartAllocated <> 0
            Cycle
        End !If par:PartAllocated <> 0
        !edit end

        If Local.Validation('W')
            Cycle
        End !If Local.Validation('C')

        If Prog.InsideLoop()

        End !If Prog.InsideLoop()


        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
            !Error!
        ELSE
            If sto:Location <> tmp:Location
                Cycle
            End !If sto:Location <> tmp:Location

        END

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = par:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'WEB',job:Engineer)
        END

    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)
    Prog.ProgressFinish()


    Flush(PARTS)
    Flush(WARPARTS)
    Flush(STOCKALL)
Waybill:Check Routine
    ! Check if this job requires inserting into the 'waybill generation - awaiting processing' table

    if jobe:HubRepair
        if job:Incoming_Consignment_Number <> ''                    ! Job must have an empty consignment number
            do Waybill:CheckRemoveFromAwaitProcess
            exit
        end

        if job:Location <> GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
            do Waybill:CheckRemoveFromAwaitProcess
            exit
        end



        Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
        wob:RefNumber = job:Ref_Number
        if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.


        Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
        wya:AccountNumber = wob:HeadAccountNumber
        wya:JobNumber   = job:Ref_Number
        if Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
            if not Access:WAYBAWT.PrimeRecord()                     ! Insert new waybill 'awaiting processing' record
                wya:JobNumber     = job:Ref_Number
                wya:AccountNumber = wob:HeadAccountNumber
                wya:Manufacturer  = job:Manufacturer
                wya:ModelNumber   = job:Model_Number
                wya:IMEINumber    = job:ESN
                if Access:WAYBAWT.Update()
                    Access:WAYBAWT.CancelAutoInc()
                end
            end
        else

        end

    else
        do Waybill:CheckRemoveFromAwaitProcess
    end
Waybill:CheckRemoveFromAwaitProcess Routine
    ! Remove waybill 'awaiting processing' record (not actual waybill record(!))

    Access:WEBJOB.ClearKey(wob:RefNumberKey)                    ! Fetch web job
    wob:RefNumber = job:Ref_Number
    if Access:WEBJOB.TryFetch(wob:RefNumberKey) then exit.

    Access:WAYBAWT.ClearKey(wya:AccountJobNumberKey)            ! Does this record already exist ?
    wya:AccountNumber = wob:HeadAccountNumber
    wya:JobNumber = job:Ref_Number
    if not Access:WAYBAWT.Fetch(wya:AccountJobNumberKey)
        !message('About to delete WAYBAWT - 3')
        Relate:WAYBAWT.Delete(0)                                ! Delete record if it exists
    end

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020486'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RSA_MainBrowse')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHOR48.Open
  Relate:JOBEXACC.Open
  Relate:STAHEAD.Open
  Relate:STOCKALL.Open
  Relate:STOCKALX.Open
  Relate:STOFAULT.Open
  Relate:TRADEAC2.Open
  Relate:USERS_ALIAS.Open
  Relate:WAYBILLJ.Open
  Relate:WAYBPRO.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:STOCK.UseFile
  Access:ESTPARTS.UseFile
  Access:LOCATION.UseFile
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:STOCKALL,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      tmp:Location    = use:Location
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = LevelBenign
      !Error
  End !If AccessUSERS.Tryfetch(usePassword_Key) = LevelBenign
  
  tmp:UseRapid = True
  
  !Check out location!
  Access:Location.ClearKey(loc:Location_Key)
  loc:Location = tmp:Location
  IF Access:Location.Fetch(loc:Location_Key)
    !Error!
  ELSE
    IF loc:UseRapidStock = FALSE
      tmp:UseRapid = False
      !Disable - !
      HIDE(?Tab1)
      HIDE(?Tab2)
      HIDE(?Tab3)
      HIDE(?Tab4)
      HIDE(?Tab5)
      HIDE(?Tab6)
      HIDE(?Tab7)
      HIDE(?Tab8)
      Disable(?Group1)
      Select(?Tab9)
      POST(Event:NewSelection,?Sheet1)
    END
  END
  
  tmp:ARCLocation = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_NUmber    = tmp:ARCLocation
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    !Found
    tmp:ARCSiteLocation = tra:SiteLocation
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  
  If glo:WebJob
      tmp:CurrentLocation = Clarionet:Global.Param2
  Else !glo:WebJob
      tmp:CurrentLocation = tmp:ARCLocation
  End !glo:WebJob
  
  !need to allocate stock directly to AA20 from MAIN STORE if box ticked
  AA20Relocated = false
  if clip(tmp:location) = 'MAIN STORE'
      Access:tradeAc2.clearkey(TRA2:KeyAccountNumber)
      TRA2:Account_Number = 'AA20'
      if access:tradeac2.fetch(TRA2:KeyAccountNumber)
          !error
      ELSE
          if TRA2:UseSparesRequest = 'YES' then
              AA20Relocated = true
              !message('AA20 reloacted')
          END
      END
  END !if main store
      ! Save Window Name
   AddToLog('Window','Open','RSA_MainBrowse')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Engineer{Prop:Tip} AND ~?LookupEngineer{Prop:Tip}
     ?LookupEngineer{Prop:Tip} = 'Select ' & ?tmp:Engineer{Prop:Tip}
  END
  IF ?tmp:Engineer{Prop:Msg} AND ~?LookupEngineer{Prop:Msg}
     ?LookupEngineer{Prop:Msg} = 'Select ' & ?tmp:Engineer{Prop:Msg}
  END
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,stl:ShelfLocationKey)
  BRW2.AddRange(stl:Location)
  BRW2.AddLocator(BRW2::Sort4:Locator)
  BRW2::Sort4:Locator.Init(?stl:ShelfLocation,stl:ShelfLocation,1,BRW2)
  BRW2.AddSortOrder(,stl:DescriptionKey)
  BRW2.AddRange(stl:Location)
  BRW2.AddLocator(BRW2::Sort1:Locator)
  BRW2::Sort1:Locator.Init(?stl:Description,stl:Description,1,BRW2)
  BRW2.AddSortOrder(,stl:PartNumberKey)
  BRW2.AddRange(stl:Location)
  BRW2.AddLocator(BRW2::Sort2:Locator)
  BRW2::Sort2:Locator.Init(?stl:PartNumber,stl:PartNumber,1,BRW2)
  BRW2.AddSortOrder(,stl:EngineerKey)
  BRW2.AddRange(stl:Engineer)
  BRW2.AddLocator(BRW2::Sort3:Locator)
  BRW2::Sort3:Locator.Init(?stl:JobNumber,stl:JobNumber,1,BRW2)
  BRW2.AddSortOrder(,stl:StatusShelfLocationKey)
  BRW2.AddRange(stl:Status)
  BRW2.AddLocator(BRW2::Sort5:Locator)
  BRW2::Sort5:Locator.Init(,stl:ShelfLocation,1,BRW2)
  BRW2.AddSortOrder(,stl:StatusShelfLocationKey)
  BRW2.AddRange(stl:Status)
  BRW2.AddLocator(BRW2::Sort6:Locator)
  BRW2::Sort6:Locator.Init(,stl:ShelfLocation,1,BRW2)
  BRW2.AddSortOrder(,stl:StatusShelfLocationKey)
  BRW2.AddRange(stl:Status)
  BRW2.AddLocator(BRW2::Sort7:Locator)
  BRW2::Sort7:Locator.Init(,stl:ShelfLocation,1,BRW2)
  BRW2.AddSortOrder(,stl:StatusShelfLocationKey)
  BRW2.AddRange(stl:Status)
  BRW2.AddLocator(BRW2::Sort8:Locator)
  BRW2::Sort8:Locator.Init(,stl:ShelfLocation,1,BRW2)
  BRW2.AddSortOrder(,stl:StatusJobNumberKey)
  BRW2.AddRange(stl:Status)
  BRW2.AddLocator(BRW2::Sort9:Locator)
  BRW2::Sort9:Locator.Init(?stl:JobNumber:2,stl:JobNumber,1,BRW2)
  BRW2.AddSortOrder(,stl:ShelfLocationKey)
  BRW2.AddRange(stl:Location)
  BIND('tmp:FullEngineer',tmp:FullEngineer)
  BIND('Web_Temp_Job',Web_Temp_Job)
  BIND('tmp:QuantityStock',tmp:QuantityStock)
  BIND('tmp:StockAvailable',tmp:StockAvailable)
  BIND('ReqDate',ReqDate)
  BIND('ReqTime',ReqTime)
  BIND('tmp:Engineer',tmp:Engineer)
  BIND('tmp:Location',tmp:Location)
  ?List{PROP:IconList,1} = '~block_green.ico'
  ?List{PROP:IconList,2} = '~notick1.ico'
  BRW2.AddField(stl:ShelfLocation,BRW2.Q.stl:ShelfLocation)
  BRW2.AddField(stl:Description,BRW2.Q.stl:Description)
  BRW2.AddField(stl:PartNumber,BRW2.Q.stl:PartNumber)
  BRW2.AddField(tmp:FullEngineer,BRW2.Q.tmp:FullEngineer)
  BRW2.AddField(Web_Temp_Job,BRW2.Q.Web_Temp_Job)
  BRW2.AddField(stl:Quantity,BRW2.Q.stl:Quantity)
  BRW2.AddField(tmp:QuantityStock,BRW2.Q.tmp:QuantityStock)
  BRW2.AddField(tmp:StockAvailable,BRW2.Q.tmp:StockAvailable)
  BRW2.AddField(stl:RecordNumber,BRW2.Q.stl:RecordNumber)
  BRW2.AddField(ReqDate,BRW2.Q.ReqDate)
  BRW2.AddField(ReqTime,BRW2.Q.ReqTime)
  BRW2.AddField(stl:Location,BRW2.Q.stl:Location)
  BRW2.AddField(stl:Engineer,BRW2.Q.stl:Engineer)
  BRW2.AddField(stl:JobNumber,BRW2.Q.stl:JobNumber)
  BRW2.AddField(stl:Status,BRW2.Q.stl:Status)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'By Shelf Location'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Tab3{PROP:TEXT} = 'By Part No'
    ?Tab4{PROP:TEXT} = 'By Engineer'
    ?Tab5{PROP:TEXT} = 'Awaiting Picking'
    ?Tab6{PROP:TEXT} = 'Picking In Progress'
    ?Tab7{PROP:TEXT} = 'Picked'
    ?Tab8{PROP:TEXT} = 'Awaiting Return'
    ?Tab9{PROP:TEXT} = 'Parts On Order'
    ?List{PROP:FORMAT} ='100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHOR48.Close
    Relate:JOBEXACC.Close
    Relate:STAHEAD.Close
    Relate:STOCKALL.Close
    Relate:STOCKALX.Close
    Relate:STOFAULT.Close
    Relate:TRADEAC2.Close
    Relate:USERS_ALIAS.Close
    Relate:WAYBILLJ.Close
    Relate:WAYBPRO.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','RSA_MainBrowse')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickEngineers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?UtilitiesRefreshList
      ThisWindow.Update
      IF (SecurityCheck('STOCK ALLOCATION REFRESH'))    ! #12144 Add access level (Bryan: 06/06/2011)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case !message
      ELSE
      
          Case Missive('Warning! There should be no need to refresh the Stock Allocation browse unless there has been an error and you have been advised to do so by your System Supervisor.'&|
            '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
      
                  Do RefreshList
              Of 1 ! No Button
          End ! Case Missive
      END
      BRW2.ResetSort(1)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020486'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020486'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020486'&'0')
      ***
    OF ?tmp:Engineer
      IF tmp:Engineer OR ?tmp:Engineer{Prop:Req}
        use:User_Code = tmp:Engineer
        !Save Lookup Field Incase Of error
        look:tmp:Engineer        = tmp:Engineer
        IF Access:USERS.TryFetch(use:User_Code_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Engineer = use:User_Code
          ELSE
            !Restore Lookup On Error
            tmp:Engineer = look:tmp:Engineer
            SELECT(?tmp:Engineer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?LookupEngineer
      ThisWindow.Update
      BRW2.ResetSort(1)
      SELECT(?tmp:Engineer)
      use:User_Code = tmp:Engineer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Engineer = use:User_Code
          Select(?+1)
      ELSE
          Select(?tmp:Engineer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Engineer)
    OF ?AutoFulFill
      ThisWindow.Update
      Case Missive('Are you sure you want to automatically fulfil All parts on order?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
      
          Free(RapidJobQueue)
          Free(RapidQueue)
      
          Save_stl_ID = Access:STOCKALL.SaveFile()
          Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
          stl:Location  = tmp:Location
          stl:Status    = 'WEB'
          Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
          Loop
              If Access:STOCKALL.NEXT()
                 Break
              End !If
              If stl:Location  <> tmp:Location      |
              Or stl:Status    <> 'WEB'      |
                  Then Break.  ! End If
              rapque:RecordNumber = stl:RecordNumber
              Add(RapidQueue)
          End !Loop
          Access:STOCKALL.RestoreFile(Save_stl_ID)
      
          Sort(RapidQueue,rapque:RecordNumber)
      
          Loop x# = 1 To Records(RapidQueue)
              Get(RapidQueue,x#)
              Access:STOCKALL.Clearkey(stl:RecordNumberKEy)
              stl:RecordNumber  = rapque:RecordNumber
              If Access:STOCKALL.Tryfetch(stl:RecordNumberKEy) = Level:Benign
                  !Found
                  !This is from stock
                  !Has this job already been tried and failed?
                  Sort(RapidJobQueue,jobque:JobNumber)
                  jobque:JobNumber    = stl:JobNumber
                  Get(RapidJobQueue,jobque:JobNumber)
                  If ~Error()
                      Cycle
                  End !If ~Error()
      
                  tmp:StockRefNo = stl:PartRefNumber
      
                  Case stl:PartType
                      OF 'CHA'
                          Access:Parts.ClearKey(par:recordnumberkey)
                          par:Record_Number = stl:PartRecordNumber
                          IF Access:Parts.Fetch(par:recordnumberkey)
                              !Error!
                              Cycle
                          ELSE
        
            
                          END
                      OF 'WAR'
                          Access:WarParts.ClearKey(wpr:recordnumberkey)
                          wpr:Record_Number = stl:PartRecordNumber
                          IF Access:WarParts.Fetch(wpr:recordnumberkey)
                              !Error!
                              Cycle
                          ELSE
      
                          END
                  End
      
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = tmp:StockRefNo
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key)
                     !Cannot find stock entry
                     Cycle
                  END
                  If stl:Quantity > sto:Quantity_Stock
                      !Cannot fullfill request
                      Cycle
                  Else !stl:Quantity > sto:Quantity_Stock
                      !
                  End !stl:Quantity > sto:Quantity_Stock
      
                  tmp:JobNumber   = stl:JobNumber

                  If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber) = Level:Benign
                      !All parts are ok to be taken
                      Save_stl_ID = Access:STOCKALL.SaveFile()
                      Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
                      stl:Location  = tmp:Location
                      stl:Status    = 'WEB'
                      stl:JobNumber = tmp:JobNumber
                      Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
                      Loop
                          If Access:STOCKALL.NEXT()
                             Break
                          End !If
                          If stl:Location  <> tmp:Location      |
                          Or stl:Status    <> 'WEB'      |
                          Or stl:JobNumber <> tmp:JobNumber |
                              Then Break.  ! End If
                          If Local.RemovePartFromStock(stl:PartRefNumber,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber) = Level:Benign
                              do UpdateStockAlx
                              !Remove Entry from browse - 3451 (DBH: 28-10-2003)
                              !message('About to delete stockall - 4')
                              Relate:STOCKALL.Delete(0)
                          End !.RemoveFromStock(par:Part_Ref_Number,par:Record_Number,par:Quantity,'C',par:Part_Number)
                      End !Loop
                      Access:STOCKALL.RestoreFile(Save_stl_ID)
      
                  Else !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
                      !Not all parts can be fullfilled
                      !Add job number to queue, so no other parts for this job will be picked
                      Sort(RapidJobQueue,jobque:JobNumber)
                      jobque:JobNumber = tmp:JobNumber
                      Add(RapidJobQueue)
                  End !If Local.FindOtherParts(stl:JobNumber,stl:RecordNumber)
      
      
              Else!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:RAPIDSTOCK.TryFetch(stl:RecordNumberKey) = Level:Benign
      
              !Added 05/12/02 - L393 / VP109 -
              !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
              !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
      
              Access:STOCKALL.Clearkey(stl:RecordNumberKey)
              stl:recordNumber    = rapque:RecordNumber
              If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
                  !Found
                  !Reget the record to check the status bits below
              Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      
      
              FoundAnyPart = false
              FoundOrderedPart = false
      
              !first check the chargeable parts
              access:Parts.clearkey(par:Part_Number_Key)
              par:Ref_Number = stl:JobNumber
              set(par:Part_Number_Key,par:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:Parts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if par:Ref_Number <> stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if par:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              If par:order_number <> ''
                                  If par:date_received = ''
                                      FoundOrderedPart = 1
                                  Else!If par:date_received = ''
                                      !ignore this it has been received
                                  End!If par:date_received = ''
                              End
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
                 if FoundAnyPart and FoundORderedPart then break.
              
              End !loop to find an unallocated part
      
              if FoundAnyPart and FoundOrderedPart then
                  !I dont need to check any further
              ELSE
                  !Check warranty parts
                  access:warParts.clearkey(wpr:Part_Number_Key)
                  wpr:Ref_Number = stl:JobNumber
                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:WarParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if wpr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if wpr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  If wpr:order_number <> ''
                                      If wpr:date_received = ''
                                          FoundOrderedPart = 1
                                      Else!If par:date_received = ''
                                          !ignore this it has been received
                                      End!If par:date_received = ''
                                  End
      
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                      
                      if foundanypart and foundorderedPart then break.
                      
                  End !loop to find an unallocated part
      
              END !if I have what I need already
      
              if FoundAnyPart and foundorderedPart then
                  !I dont need to check this
              ELSE
                  !Check estimate parts
                  access:EstParts.clearkey(epr:Part_Number_Key)
                  epr:Ref_Number = stl:JobNumber
                  set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:EstParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if epr:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if epr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  If epr:order_number <> ''
                                      If epr:date_received = ''
                                          FoundOrderedPart = 1
                                      Else!If par:date_received = ''
                                          !ignore this it has been received
                                      End!If par:date_received = ''
                                  End
      
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                      
                      if foundanypart and foundorderedpart then break.
                      
                  End !loop to find an unallocated part
              END !if I haven't FoundAnyPart
      
      
              if FoundOrderedPart = false or FoundAnyPArt = false
                   !change job status to 330 then to 315 as needed
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = stl:JobNumber
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        Error# = 0
      
                        Pointer# = Pointer(JOBS)
                        Hold(JOBS,1)
                        Get(JOBS,Pointer#)
                        If Errorcode() = 43
                          Case Missive('This job is currently in use by another station. Unable to update status. You will have to do this manually.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                !          !Post(Event:CloseWindow)
                        ELSE !If Errorcode() = 43
                            !Found
                            if FoundOrderedPart = false
                                  getstatus(330,0,'JOB')  !Spares Requested
                            END !If no outstanding orders
                            if FoundAnyPart = false
                                  getstatus(345,0,'JOB')  !In repair
                            end !found no unallocated parts
                            Access:JOBS.TryUpdate()
                        End  !If Errorcode() = 43
                        Release(JOBS)
                   END !if tryfetch on jobs
              END !if I haven't FoundAnyPart
      
              !end of Added 05/12/02 - L393 / VP109 -
      
          End !x# = 1 To Records(RapidQueue)
      
      
      
          Of 1 ! No Button
      End ! Case Missive
      BRW2.ResetSort(1)
    OF ?Button10
      ThisWindow.Update
      Access:STOCKALL.Clearkey(stl:RecordNumberKey)
      stl:RecordNumber    = brw2.q.stl:RecordNumber
      If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Found
          Case stl:PartType
              Of 'CHA'
                  Access:PARTS.Clearkey(par:RecordNumberKey)
                  par:Record_Number   = stl:PartRecordNumber
                  If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                      !Found
                      tmp:StockRefNo  = par:Part_Ref_Number
                  Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = stl:PartRecordNumber
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                      !Found
                      tmp:StockRefNo  = wpr:Part_Ref_Number
                  Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
          End !Case stl:PartType
      Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Error
      End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      
      
      If brw2.q.stl:Quantity > brw2.q.tmp:QuantityStock
          Case Missive('There are insufficient items in stock to fulfil this request.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else
      
          If Local.RemovePartFromStock(tmp:StockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber) = Level:Benign
      
              !Added 05/12/02 - L393 / VP109 -
              !If all warranty/chargeable/estimate parts are now in stock - change job status to 330 - Parts Ordered
              !And if not using loc:UseRapidStock - if all parts allocated change job status to 310 allocated to engineer
      
              FoundAnyPart = false
              FoundOrderedPart = false
      
              !first check the chargeable parts
              access:Parts.clearkey(par:Part_Number_Key)
              par:Ref_Number = brw2.q.stl:JobNumber
              set(par:Part_Number_Key,par:Part_Number_Key)
              Loop !to find an unallocated part
                  if access:Parts.next() then
                      !leave no more jobs
                      break
                  ELSE
                      if par:Ref_Number <> brw2.q.stl:JobNumber then
                          !Leave no more matching jobs
                          break
                      ELSE
                          !found a part for this job
                          if par:PartAllocated then
                              !ignore this it was allocated
                          ELSE
                              !found an unallocated part!
                              FoundAnyPart = true
                              If par:order_number <> ''
                                  If par:date_received = ''
                                      FoundOrderedPart = 1
                                  Else!If par:date_received = ''
                                      !ignore this it has been received
                                  End!If par:date_received = ''
                              End
                          END !if partAllocated
                      END !If ref_numbers disagree
                  END !if access:jobs.next()
                  if FoundAnyPart and FoundORderedPart then break.
              End !loop to find an unallocated part
      
              if FoundAnyPart and FoundOrderedPart then
                  !I dont need to check any further
              ELSE
                  !Check warranty parts
                  access:warParts.clearkey(wpr:Part_Number_Key)
                  wpr:Ref_Number = brw2.q.stl:JobNumber
                  set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:WarParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if wpr:Ref_Number <> brw2.q.stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if wpr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  If wpr:order_number <> ''
                                      If wpr:date_received = ''
                                          FoundOrderedPart = 1
      
                                      Else!If par:date_received = ''
                                          !ignore this it has been received
                                      End!If par:date_received = ''
                                  End
      
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                      if foundanypart and foundorderedPart then break.
                  End !loop to find an unallocated part
      
              END !if I have what I need already
      
              if FoundAnyPart and foundorderedPart then
                  !I dont need to check this
              ELSE
                  !Check estimate parts
                  access:EstParts.clearkey(epr:Part_Number_Key)
                  epr:Ref_Number = brw2.q.stl:JobNumber
                  set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:EstParts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if epr:Ref_Number <> brw2.q.stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if epr:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  If epr:order_number <> ''
                                      If epr:date_received = ''
                                          FoundOrderedPart = 1
                                      Else!If par:date_received = ''
                                          !ignore this it has been received
                                      End!If par:date_received = ''
                                  End
      
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                      if foundanypart and foundorderedpart then break.
                  End !loop to find an unallocated part
              END !if I haven't FoundAnyPart
      
      
              if FoundOrderedPart = false or FoundAnyPArt = false
                   !change job status to 330 then to 315 as needed
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = brw2.q.stl:JobNumber
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        Error# = 0
      
                        Pointer# = Pointer(JOBS)
                        Hold(JOBS,1)
                        Get(JOBS,Pointer#)
                        If Errorcode() = 43
                          Case Missive('This job (' & Clip(brw2.q.stl:JobNumber) & ') is currently in use by another station. Unable to update status. '&|
                            '<13,10>You will have to do this manually.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                          Error# = 1
                !          !Post(Event:CloseWindow)
                        ELSE !If Errorcode() = 43
                            !Found
                            if FoundOrderedPart = false
                                  getstatus(330,0,'JOB')  !Spares Requested
                            END !If no outstanding orders
                            if FoundAnyPart = false
                                  getstatus(345,0,'JOB')  !In repair
                            end !found no unallocated parts
                            Access:JOBS.TryUpdate()
                        End  !If Errorcode() = 43
                        Release(JOBS)
                   END !if tryfetch on jobs
              END !if I haven't FoundAnyPart
              !end of Added 05/12/02 - L393 / VP109 -
      
              
              !update StockALX with the details before you remove the record - needed for a report
              do UpdateStockAlx
              !Remove Entry from browse - 3451 (DBH: 28-10-2003)
              !message('About to delete stockall - 5')
              Relate:STOCKALL.Delete(0)
          End !Local.RemovePartFromStock(tmp:StockRefNo,stl:PartRecordNumber,stl:Quantity,stl:PartType,stl:PartNumber)
      End !stl:Quantity < sto:Quantity_Stock
      BRW2.ResetSort(1)
    OF ?ProcessPicking
      ThisWindow.Update
      Local.ChangePartStatus(brw2.q.stl:recordNumber,'PRO')
      BRW2.ResetSort(1)
    OF ?AllocatePart
      ThisWindow.Update
      Access:STOCKALL.Clearkey(stl:RecordNumberKey)
      stl:RecordNumber    = brw2.q.stl:RecordNumber
      If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Found
          If stl:PartNumber = 'EXCH'
              Case Missive('The selected part is an "Exchange Part". Click Allocate Exchange to allocate an exchange unit.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else !If stl:PartNumber = 'EXCH'
              Error# = 0
              If stl:Status = 'WEB'
                  Case Missive('The selected part is on order. You must fulfil the request from the "Parts On Order" tab.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Error# = 1
              End !If stl:Status = 'WEB'
              If Error# = 0
      
                  Case stl:PartType
                      Of 'CHA'
                          Access:PARTS.Clearkey(par:recordnumberkey)
                          par:Record_Number   = stl:PartRecordNumber
                          If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                              !Found
                              Access:STOCK.Clearkey(sto:Ref_Number_Key)
                              sto:Ref_Number  = par:Part_Ref_Number
                              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Found
                                  If sto:ReturnFaultySpare
                                      Case Missive('The faulty part must be returned before the new part can be issued.'&|
                                        '<13,10>'&|
                                        '<13,10>Do you wish to CONFIRM receipt, or DECLINE issue?','ServiceBase 3g',|
                                                     'mquest.jpg','Decline|Confirm')
                                          Of 2 ! Confirm Button
                                              Local.AddFaulty('C')
                                          Of 1 ! Decline Button
                                              Error# = 1
                                      End ! Case Missive
                                  End !If sto:ReturnFaultySpare
                              Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              If Error# = 0
                                  par:PartAllocated = 1      !
                                  par:WebOrder = 0
                                  Access:PARTS.Update()
                              End !If Error# = 0
                          Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                              !Error
                              Error# = 1
                          End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
                      Of 'WAR'
                          Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                          wpr:Record_Number   = stl:PartRecordNumber
                          If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                              !Found
                              Access:STOCK.Clearkey(sto:Ref_Number_Key)
                              sto:Ref_Number  = wpr:Part_Ref_Number
                              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Found
                                  If sto:ReturnFaultySpare
                                      Case Missive('The faulty part must be returned before the new part can be issued.'&|
                                        '<13,10>'&|
                                        '<13,10>Do you wish to CONFIRM receipt, or DECLINE issue?','ServiceBase 3g',|
                                                     'mquest.jpg','Decline|Confirm')
                                          Of 2 ! Confirm Button
                                              Local.AddFaulty('W')
                                          Of 1 ! Decline Button
                                              Error# = 1
                                      End ! Case Missive
                                  End !If sto:ReturnFaultySpare
      
                              Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              If Error# = 0
                                  wpr:PartAllocated = 1
                                  wpr:WebOrder = 0
                                  Access:WARPARTS.Update()
                              End !If Error# = 0
      
                          Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                              !Error
                              Error# = 1
                          End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
                      Of 'EST'
                          Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                          epr:Record_Number   = stl:PartRecordNumber
                          If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                              !Found
                              Access:STOCK.Clearkey(sto:Ref_Number_Key)
                              sto:Ref_Number  = epr:Part_Ref_Number
                              If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Found
                                  If sto:ReturnFaultySpare
                                      Case Missive('The faulty part must be returned before the new part can be issued.'&|
                                        '<13,10>'&|
                                        '<13,10>Do you wish to CONFIRM receipt, or DECLINE issue?','ServiceBase 3g',|
                                                     'mquest.jpg','Decline|Confirm')
                                          Of 2 ! Confirm Button
                                              Local.AddFaulty('E')
                                          Of 1 ! Decline Button
                                              Error# = 1
                                      End ! Case Missive
                                  End !If sto:ReturnFaultySpare
      
                              Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                  !Error
                              End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              If Error# = 0
                                  epr:PartAllocated = 1
                                  Access:ESTPARTS.Update()
                              End !If Error# = 0
      
                          Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                              !Error
                              Error# = 1
                          End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
      
                  End !Case stl:PartType
                  If Error# = 0
                      do UpdateStockAlx
                      !message('About to delete stockall - 6')
                      Relate:STOCKALL.Delete(0)
                  End !If Error# = 0
      
      
                  !Added 05/12/02 - L393 / VP109 -
                  !If all warranty/chargeable/estimate parts are now allocated - change job status to 315 - In Repair
                  FoundAnyPart = false
      
                  !first check the chargeable parts
                  access:Parts.clearkey(par:Part_Number_Key)
                  par:Ref_Number = stl:JobNumber
                  set(par:Part_Number_Key,par:Part_Number_Key)
                  Loop !to find an unallocated part
                      if access:Parts.next() then
                          !leave no more jobs
                          break
                      ELSE
                          if par:Ref_Number <> stl:JobNumber then
                              !Leave no more matching jobs
                              break
                          ELSE
                              !found a part for this job
                              if par:PartAllocated then
                                  !ignore this it was allocated
                              ELSE
                                  !found an unallocated part!
                                  FoundAnyPart = true
                                  break
                              END !if partAllocated
                          END !If ref_numbers disagree
                      END !if access:jobs.next()
                  End !loop to find an unallocated part
      
                  if FoundAnyPart = false
                      !Check warranty parts
                      access:warParts.clearkey(wpr:Part_Number_Key)
                      wpr:Ref_Number = stl:JobNumber
                      set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                      Loop !to find an unallocated part
                          if access:WarParts.next() then
                              !leave no more jobs
                              break
                          ELSE
                              if wpr:Ref_Number <> stl:JobNumber then
                                  !Leave no more matching jobs
                                  break
                              ELSE
                                  !found a part for this job
                                  if wpr:PartAllocated then
                                      !ignore this it was allocated
                                  ELSE
                                      !found an unallocated part!
                                      FoundAnyPart = true
                                      break
                                  END !if partAllocated
                              END !If ref_numbers disagree
                          END !if access:jobs.next()
                      End !loop to find an unallocated part
      
                  END !if I haven't FoundAnyPart
      
                  if FoundAnyPart = false
                      !Check estimate parts
                      access:EstParts.clearkey(epr:Part_Number_Key)
                      epr:Ref_Number = stl:JobNumber
                      set(epr:Part_Number_Key,epr:Part_Number_Key)
                      Loop !to find an unallocated part
                          if access:EstParts.next() then
                              !leave no more jobs
                              break
                          ELSE
                              if epr:Ref_Number <> stl:JobNumber then
                                  !Leave no more matching jobs
                                  break
                              ELSE
                                  !found a part for this job
                                  if epr:PartAllocated then
                                      !ignore this it was allocated
                                  ELSE
                                      !found an unallocated part!
                                      FoundAnyPart = true
                                      break
                                  END !if partAllocated
                              END !If ref_numbers disagree
                          END !if access:jobs.next()
                      End !loop to find an unallocated part
                  END !if I haven't FoundAnyPart
      
                  if FoundAnyPart = false
                       !change job status to 315
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = stl:JobNumber
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            Error# = 0
      
                            Pointer# = Pointer(JOBS)
                            Hold(JOBS,1)
                            Get(JOBS,Pointer#)
                            If Errorcode() = 43
                              Case Missive('This job is currently in use by another station. Unable to update status to "315 IN REPAIR". '&|
                                '<13,10>You will have to do this manually.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Error# = 1
                    !          !Post(Event:CloseWindow)
                            ELSE !If Errorcode() = 43
                                !Found
                                getstatus(315,0,'JOB')
                                Access:JOBS.TryUpdate()
                            End  !If Errorcode() = 43
                            Release(JOBS)
                       END !if tryfetch on jobs
                  END !if I haven't FoundAnyPart
      
      
              !end of Added 05/12/02 - L393 / VP109 -
      
              End !If Error# = 0
          End !If stl:PartNumber = 'EXCH'
      Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Error
      End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      BRW2.ResetSort(1)
    OF ?Picked
      ThisWindow.Update
      Local.ChangePartStatus(brw2.q.stl:RecordNumber,'PIK')
      
      BRW2.ResetSort(1)
    OF ?AllocateExchange
      ThisWindow.Update
      glo:Preview = 'R'
      
      Access:STOCKALL.Clearkey(stl:RecordNumberKey)
      stl:RecordNumber    = brw2.q.stl:RecordNumber
      If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      
          !Found
          If stl:PartNumber <> 'EXCH'
              Case Missive('The selected part is NOT an exchange part. Are you sure you still want to allocate an Exchange Unit for this job?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      Cycle
              End ! Case Missive
          End !If stl:PartNumber <> 'EXCH'
      
          Allocate# = 0
          Access:JOBS.Clearkey(job:Ref_Number_Key)
          job:Ref_Number  = stl:JobNumber
          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
              Error# = 0
              Pointer# = Pointer(JOBS)
              Hold(JOBS,1)
              Get(JOBS,Pointer#)
              If Errorcode() = 43
                  Case Missive('This job is currently is use by another station.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                Error# = 1
      !          !Post(Event:CloseWindow)
              End !If Errorcode() = 43
      
              !Found
              if Error# = 0
                  ExchangeUnitNumber# = job:Exchange_Unit_Number
                  ViewExchangeUnit()
                  Access:JOBS.TryUpdate()
                  If job:Exchange_Unit_Number <> ExchangeUnitNumber#
                      Allocate# = 1
                      ! Inserting (DBH 26/02/2008) # 9717 - Check if an replenishment order is required
                      If ExchangeUnitNumber# = 0 And job:Exchange_Unit_Number <> 0
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber = job:Ref_Number
                          If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
                          End ! If Access:WEBJOBS.TryFetch(wob:RefNumberKey) = Level:Benign
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number = wob:HeadAccountNumber
                          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      
                          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          If UseReplenishmentProcess(job:Model_Number,wob:HeadAccountNumber) = 1
                              Order# = 1
                              Beep(Beep:SystemQuestion);  Yield()
                              Case Missive('Do you wish to order a replacement exchange unit?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                  Of 1 ! No Button
                                      Beep(Beep:SystemQuestion);  Yield()
                                      Case Missive('Can you please confirm that you do NOT want to order a Replacement Unit.'&|
                                          '|'&|
                                          '|Otherwise, click "Order" to order a replacement unit.','ServiceBase 3g',|
                                                     'mquest.jpg','/Order|\Confirm')
                                          Of 2 ! Confirm Button
                                              Order# = 0
                                          Of 1 ! Order Button
                                      End ! Case Missive
                              End ! Case Missive
                              If Order# = 1
                                  If Access:EXCHOR48.PrimeRecord() = Level:Benign
                                      ex4:Location     = tra:SiteLocation
                                      ex4:Manufacturer = job:Manufacturer
                                      ex4:ModelNUmber  = job:Model_Number
                                      ex4:Received     = 0
                                      ex4:JobNumber    = 0
                                      ex4:OrderUnitNumber = job:Exchange_Unit_Number
                                      If Access:EXCHOR48.TryInsert() = Level:Benign
                                          Clear(PassModelQueue)
                                          Free(PassModelQueue)
                                          PassModelQueue.Manufacturer = job:Manufacturer
                                          PassModelQueue.ModelNumber  = job:Model_Number
                                          PassModelQUeue.Quantity     = 1
                                          Add(PassModelQueue)
                                          ExchangeOrders(PassModelQueue,0,0)
                                          If AddToAudit(job:Ref_Number,'EXC','REPLENISHMENT EXCHANGE ORDER CREATED',|
                                                      'MANUFACTURER: ' & Clip(job:Manufacturer) & |
                                                      '<13,10>MODEL NUMBER: ' & Clip(job:Model_Number))
                                          End ! If AddToAudit(job:Ref_Number,'EXC','REPLENISHMENT EXCHANGE ORDER CREATED',|
                                      Else ! If Access:EXCHOR48.TryInsert() = Level:Benign
                                          Access:EXCHOR48.CancelAutoInc()
                                      End ! If Access:EXCHOR48.TryInsert() = Level:Benign
                                  End ! If Access:EXCHOR48.PrimeRecord() = Level:Benign
                              End ! If Order# = 1
                          End ! If UseReplenishmentProcess(job:Model_Number,wob:HeadAccountNumber) = 1
                      End ! If tmp:CurrentExchangeUnitNumber = 0 And job:Exchange_Unit_Number <> 0
                      ! End (DBH 26/02/2008) #9717
      
                  End !If job:Exchange_Unit_Number <> ExchangeUnitNumber#
      
                  Release(JOBS)
              End
      
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
          If Allocate# = 1
              Error# = 0
              Case stl:PartType
                  Of 'CHA'
                      Access:PARTS.Clearkey(par:recordnumberkey)
                      par:Record_Number   = stl:PartRecordNumber
                      If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                          !Found
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = par:Part_Ref_Number
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              If sto:ReturnFaultySpare
                                  Case Missive('The faulty part must be returned before the new part can be issued.'&|
                                    '<13,10>'&|
                                    '<13,10>Do you wish to CONFIRM receipt, or DECLINE issue?','ServiceBase 3g',|
                                                 'mquest.jpg','Decline|Confirm')
                                      Of 2 ! Confirm Button
                                          Local.AddFaulty('C')
                                      Of 1 ! Decline Button
                                          Error# = 1
                                  End ! Case Missive
                              End !If sto:ReturnFaultySpare
                          Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          If Error# = 0
                              par:PartAllocated = 1
                              Access:PARTS.Update()
                          End !If Error# = 0
                      Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                          !Error
                          Error# = 1
                      End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
                  Of 'WAR'
                      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                      wpr:Record_Number   = stl:PartRecordNumber
                      If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                          !Found
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = wpr:Part_Ref_Number
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              If sto:ReturnFaultySpare
                                  Case Missive('The faulty part must be returned before the new part can be issued.'&|
                                    '<13,10>'&|
                                    '<13,10>Do you wish to CONFIRM receipt, or DECLINE issue?','ServiceBase 3g',|
                                                 'mquest.jpg','Decline|Confirm')
                                      Of 2 ! Confirm Button
                                          Local.AddFaulty('W')
                                      Of 1 ! Decline Button
                                          Error# = 1
                                  End ! Case Missive
                              End !If sto:ReturnFaultySpare
      
                          Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          If Error# = 0
                              wpr:PartAllocated = 1
                              Access:WARPARTS.Update()
                          End !If Error# = 0
      
                      Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                          !Error
                          Error# = 1
                      End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
                  Of 'EST'
                      Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                      epr:Record_Number   = stl:PartRecordNumber
                      If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                          !Found
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = epr:Part_Ref_Number
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              If sto:ReturnFaultySpare
                                  Case Missive('The faulty part must be returned before the new part can be issued.'&|
                                    '<13,10>'&|
                                    '<13,10>Do you wish to CONFIRM receipt, or DECLINE issue?','ServiceBase 3g',|
                                                 'mquest.jpg','Decline|Confirm')
                                      Of 2 ! Confirm Button
                                          Local.AddFaulty('E')
                                      Of 1 ! Decline Button
                                          Error# = 1
                                  End ! Case Missive
                              End !If sto:ReturnFaultySpare
      
                          Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                          End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          If Error# = 0
                              epr:PartAllocated = 1
                              Access:ESTPARTS.Update()
                          End !If Error# = 0
      
                      Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                          !Error
                          Error# = 1
                      End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
              End !Case stl:PartType
              If Error# = 0
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      Do Waybill:Check
                      Access:JOBSE.Update()
                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                  !Reget entry incase it has already been deleted. -  (DBH: 24-07-2003)
                  Access:STOCKALL.Clearkey(stl:RecordNumberKey)
                  stl:RecordNumber  = brw2.q.stl:RecordNumber
                  If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
                    !Found
                      do UpdateStockAlx
                      !message('About to delete stockall - 7')
                      Relate:STOCKALL.Delete(0)
      
                  Else ! If Access:STOCKALL.Tryfetch(stl:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:STOCKALL.Tryfetch(stl:RefNumberKey) = Level:Benign
              End !If Error# = 0
          End !Allocate# = 1
      Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Error
      End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      
      glo:Preview = ''
      
      
      BRW2.ResetSort(1)
    OF ?ReturnPart
      ThisWindow.Update
      Access:STOCKALL.Clearkey(stl:RecordNumberKey)
      stl:RecordNumber    = brw2.q.stl:RecordNumber
      If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Found
          Error# = 0
          If stl:Status = 'WEB'
              Case Missive('The selected part is on order. You must fulfil the request from the "Parts On Order" tab.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Error# = 1
          End !If stl:Status = 'WEB'
          If stl:PartNumber = 'EXCH'
              Case Missive('The selected part is for an Exchange Unit. Use the "Restock/Allocate Exchange" button.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Error# = 1
          End !If stl:PartNumber = 'EXCH'
          If Error# = 0
              Case stl:PartType
                  Of 'CHA'
                      Access:PARTS.Clearkey(par:recordnumberkey)
                      par:Record_Number   = stl:PartRecordNumber
                      If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                          !Found
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = par:Ref_Number
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
                              If DeleteChargeablePart(1)
                                  !message('About to delete parts - 8')
                                  Delete(PARTS)
                              End !If DeleteChargeablePart(1)
                          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                              Error# = 1
                          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                      Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                          !Error
                          Error# = 1
                      End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
                  Of 'WAR'
                      Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                      wpr:Record_Number   = stl:PartRecordNumber
                      If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                          !Found
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = wpr:Ref_Number
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
                              If DeleteWarrantyPart(1)
                                  !message('About to delete warparts - 9')
                                  Delete(WARPARTS)
                              End !If DeleteWarrantyPart(1)
                          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                              Error# = 1
                          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                      Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                          !Error
                          Error# = 1
                      End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
                  Of 'EST'
                      Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                      epr:Record_Number   = stl:PartRecordNumber
                      If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                          !Found
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = epr:Ref_Number
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
                              If DeleteEstimatePart(1)
                                  !message('About to delete estparts - 10')
                                  Delete(ESTPARTS)
                              End !If DeleteEstimatePart(1)
                          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                              Error# = 1
                          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                          !Error
                          Error# = 1
                      End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
      
              End !Case stl:PartType
          End !If Error# = 0
      Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
          !Error
      End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
      
      BRW2.ResetSort(1)
    OF ?ColourKey
      ThisWindow.Update
      Parts_Key
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet1)
        OF 1
          ?List{PROP:FORMAT} ='100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab1{PROP:TEXT} = 'By Shelf Location'
        OF 2
          ?List{PROP:FORMAT} ='90L(2)|FM*~Description~@s30@#6#100L(2)|FM*~Shelf Location~@s30@#1#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab2{PROP:TEXT} = 'By Description'
        OF 3
          ?List{PROP:FORMAT} ='110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab3{PROP:TEXT} = 'By Part No'
        OF 4
          ?List{PROP:FORMAT} ='85L(2)|FM*~Job Number~@s20@#21#100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab4{PROP:TEXT} = 'By Engineer'
        OF 5
          ?List{PROP:FORMAT} ='100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab5{PROP:TEXT} = 'Awaiting Picking'
        OF 6
          ?List{PROP:FORMAT} ='100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab6{PROP:TEXT} = 'Picking In Progress'
        OF 7
          ?List{PROP:FORMAT} ='100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab7{PROP:TEXT} = 'Picked'
        OF 8
          ?List{PROP:FORMAT} ='100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#85L(2)|FM*~Job Number~@s20@#21#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab8{PROP:TEXT} = 'Awaiting Return'
        OF 9
          ?List{PROP:FORMAT} ='85L(2)|FM*~Job Number~@s20@#21#100L(2)|FM*~Shelf Location~@s30@#1#90L(2)|FM*~Description~@s30@#6#110L(2)|FM*~Part Number~@s30@#11#100L(2)|FM*~Engineer~@s60@#16#20L(2)|FM*~Qty~@s8@#26#34R(2)|FM*~Stock~@s8@#31#40L(2)|F*~Req Date~@s10@#47#40L(2)|F*~Req Time~@s10@#52#11L(2)|F*I@s1@#36#32L(2)|F*~Record Number~@s8@#42#'
          ?Tab9{PROP:TEXT} = 'Parts On Order'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW2.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.ChangePartStatus      Procedure(Long func:RecordNumber,String func:Status)
Code
    Access:STOCKALL.Clearkey(stl:RecordNumberKey)
    stl:RecordNumber    = func:RecordNumber
    If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
        !Found
        If stl:PartNumber = 'EXCH'
            Case Missive('The selected part in an "Exchange Part".'&|
              '<13,10>Click "Allocate Exchange" to allocate an exchange unit.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        Else !If stl:PartNumber = 'EXCH'
            Error# = 0

            If stl:Status = 'WEB'
                Case Missive('The selected part is on order.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Error# = 1
            End !If stl:Status = 'WEB'

            If Error# = 0
                Case stl:PartType
                    Of 'CHA'
                        Access:PARTS.Clearkey(par:recordnumberkey)
                        par:Record_Number   = stl:PartRecordNumber
                        If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                            !Found
                            par:Status  = func:Status
                            Access:PARTS.Update()
                        Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                            !Error
                            Error# = 1
                        End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
                    Of 'WAR'
                        Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                        wpr:Record_Number   = stl:PartRecordNumber
                        If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                            !Found
                            wpr:Status  = func:Status
                            Access:WARPARTS.Update()
                        Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                            !Error
                            Error# = 1
                        End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign
                    Of 'EST'
                        Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                        epr:Record_Number   = stl:PartRecordNumber
                        If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                            !Found
                            epr:Status = func:Status
                            Access:ESTPARTS.Update()
                        Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                            !Error
                        End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                End !Case rapsto:PartType
                If Error# = 0
                    stl:Status = func:Status
                    Access:STOCKALL.Update()
                End !If Error# = 0

            End !If Error# = 0
        End !If stl:PartNumber = 'EXCH'
    Else ! If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
        !Error
    End !If Access:STOCKALL.Tryfetch(stl:RecordNumberKey) = Level:Benign
Local.BuildChaList     Procedure(String func:Status)
Code
    Save_par_ID = Access:PARTS.SaveFile()
    Access:PARTS.ClearKey(par:AllocatedStatusKey)
    par:PartAllocated = 0
    par:Status        = Clip(func:Status)
    Set(par:AllocatedStatusKey,par:AllocatedStatusKey)
    Loop
        If Access:PARTS.NEXT()
           Break
        End !If
        If par:PartAllocated <> 0      |
        Or par:Status        <> Clip(func:Status)      |
            Then Break.  ! End If

        If Prog.InsideLoop()

        End !If Prog.InsideLoop()

        ! #11965 Ignore cancelled, or corrupt "missing" jobs. (Bryan: 03/02/2011)
        IF (SkipCorruptJobs(par:Ref_Number))
            CYCLE
        END

        If par:WebOrder = 1
            Cycle
        End !If par:WebOrder = 1

        If Local.Validation('C')
            Cycle
        End !If Local.Validation('C')



        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
            !Error!
        ELSE
            If sto:Location <> tmp:Location
                Cycle
            End !If sto:Location <> tmp:Location
        END

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = par:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,par:Status,job:Engineer)
        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:JOBS.Tryfetch(jobRef_Number_Key) = LevelBenign

    End !Loop
    Access:PARTS.RestoreFile(Save_par_ID)


Local.BuildWarList       Procedure(String func:Status)
Code
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:AllocatedStatusKey)
    wpr:PartAllocated = 0
    wpr:Status        = Clip(func:Status)
    Set(wpr:AllocatedStatusKey,wpr:AllocatedStatusKey)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:PartAllocated <> 0      |
        Or wpr:Status        <> Clip(func:Status)      |
            Then Break.  ! End If
        If Prog.InsideLoop()

        End !If Prog.InsideLoop()

        ! #11965 Ignore cancelled, or corrupt "missing" jobs. (Bryan: 03/02/2011)
        IF (SkipCorruptJobs(wpr:Ref_Number))
            CYCLE
        END

        If wpr:WebOrder = 1
            Cycle
        End !If wpr:WebOrder = 1


        If Local.Validation('W')
            Cycle
        End !If Local.Validation('C')




        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
            !Error!
        ELSE
            If sto:Location <> tmp:Location
                Cycle
            End !If sto:Location <> tmp:Location

        END


        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = wpr:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
            AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,wpr:Status,job:Engineer)
        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:JOBS.Tryfetch(jobRef_Number_Key) = LevelBenign

    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)


Local.BuildEstList      Procedure(String func:Status)
Code
    Save_epr_ID = Access:ESTPARTS.SaveFile()
    Access:ESTPARTS.ClearKey(epr:AllocatedStatusKey)
    epr:PartAllocated = 0
    epr:Status        = Clip(func:Status)
    Set(epr:AllocatedStatusKey,epr:AllocatedStatusKey)
    Loop
        If Access:ESTPARTS.NEXT()
           Break
        End !If
        If epr:PartAllocated <> 0      |
        Or epr:Status        <> Clip(func:Status)      |
            Then Break.  ! End If
                If Local.Validation('E')
            Cycle
        End !If Local.Validation('C')
        If Prog.InsideLoop()

        End !If Prog.InsideLoop()

        ! #11965 Ignore cancelled, or corrupt "missing" jobs. (Bryan: 03/02/2011)
        IF (SkipCorruptJobs(epr:Ref_Number))
            CYCLE
        END


        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = epr:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
            !Error!
        ELSE
            If sto:Location <> tmp:Location
                Cycle
            End !If sto:Location <> tmp:Location

        END

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = epr:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found
            If job:Estimate_Accepted = 'YES' Or job:Estimate_Rejected = 'YES'
                Cycle
            End !If job:Estimate_Accepted = 'YES' Or job:Estimate_Rejected = 'YES'

            AddToStockAllocation(epr:Record_Number,'EST',epr:Quantity,epr:Status,job:Engineer)

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:JOBS.Tryfetch(jobRef_Number_Key) = LevelBenign

    End !Loop
    Access:ESTPARTS.RestoreFile(Save_epr_ID)

Local.Validation    Procedure(String    func:Type)
Code
    Case func:Type
        Of 'C'
            If par:Quantity = 0
                Return Level:Fatal
            End !If par:Quantity = 0

            If par:Part_Number = 'EXCH'
                if par:status = 'RTS' then
                    !change par:status to be picked up for rapidstock:status
                    par:status = 'RET'
                ELSE
                    If par:Status <> 'REQ'
                        Return Level:Fatal
                    End !If par:Status = 'PIK'

                end !if par:status <> RTS
            End !If par:Part_Number = 'EXCH'
            If par:Part_Ref_Number = 0
                Return Level:Fatal
            End !If par:Part_Ref_Number = 0

            If par:Pending_Ref_Number <> ''
                Return Level:Fatal
            End !If par:Pending_Ref_Number <> ''

            If par:Order_Number <> '' and par:Date_Received = ''
                Return Level:Fatal
            End !If par:Order_Number <> '' and par:Date_Received = ''


            !Don't count parts if the job is still an estimate
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = par:Ref_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                If job:Estimate = 'YES'
                    If job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                        Return Level:Fatal
                    End !If job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                End !If job:Estimate = 'YES'
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


            !Count this job?
!
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = par:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
                !Cannot find the job at all
                Return Level:Fatal
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            If glo:WebJob
                !Routine run but RRC. Only include jobs that aren't at the ARC
                If jobe:HubRepair
                    Return Level:Fatal
                End !If jobe:HubRepair

                !Is this job, for the account thats running the Stock Allocation?
                If wob:HeadAccountNumber <> Clarionet:Global.Param2
                    Return Level:Fatal
                End !If wob:HeadAccountNumber <> Clarionet:Global.Param2

            Else !If glo:WebJob
                If ~jobe:HubRepair
                    Return Level:Fatal
                End !If ~jobe:HubRepair

                !Running this routine at the ARC.
                !The selected job is at the HUB (i.e. HubRepair is ticked)
            End !If glo:WebJob

            !Now let's see if we can find an entry in stock history for this part.
            !This will check if the part on the job is assigned to the correct site
            PartError# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = par:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                !Is there a stock history entry?

                Access:STOHIST.ClearKey(shi:JobNumberKey)
                shi:Ref_Number       = sto:Ref_Number
                shi:Transaction_Type = 'DEC'
                shi:Job_Number       = par:Ref_Number
                If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                    !Found
                    !Found a stock history entry saying that this part was used for this location
                Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    PartError# = 1
                End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign

            Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Cannot find the selected part in stock control
                PartError# = 1
            End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            If PartError#
                !There is an error with the part, let's try and find another part
                !with the same number in the RIGHT location
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location    = use:Location
                sto:Part_Number = par:Part_Number
                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Found the part in the user's location.
                    !Now is there a stock history to say that this part was attached to this job
                    !from this location?

                    Access:STOHIST.ClearKey(shi:JobNumberKey)
                    shi:Ref_Number       = sto:Ref_Number
                    shi:Transaction_Type = 'DEC'
                    shi:Job_Number       = par:Ref_Number
                    If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                        !Found
                        !Found a stock history entry saying that this part was used for this location
                        par:Part_Ref_Number = sto:Ref_Number
                        Access:PARTS.Update()

                    Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')

                        If glo:WebJob
                            !Cannot find this part in the RRC Locations, let's
                            !try the ARC Location
                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = tmp:ARCSiteLocation
                            sto:Part_Number = par:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Found
                                Access:STOHIST.ClearKey(shi:JobNumberKey)
                                shi:Ref_Number       = sto:Ref_Number
                                shi:Transaction_Type = 'DEC'
                                shi:Job_Number       = par:Ref_Number
                                If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                    !Found
                                    par:Part_Ref_Number = sto:Ref_Number
                                    Access:PARTS.Update()

                                Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                            Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Error
                            End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                        Else !If glo:WebJob
                            !Cannot find this part in the ARC Location, let's try the locatin
                            !of where the job was booked in.
                            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                            tra:Account_Number  = wob:HeadAccountNumber
                            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Found
                                Access:STOCK.Clearkey(sto:Location_Key)
                                sto:Location    = tra:SiteLocation
                                sto:Part_Number = par:Part_Number
                                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    !Found
                                    Access:STOHIST.ClearKey(shi:JobNumberKey)
                                    shi:Ref_Number       = sto:Ref_Number
                                    shi:Transaction_Type = 'DEC'
                                    shi:Job_Number       = par:Ref_Number
                                    If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                        !Found
                                        par:Part_Ref_Number = sto:Ref_Number
                                        Access:PARTS.Update()

                                    Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    !Error
                                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Error
                            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                        End !If glo:WebJob
                    End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign


                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Error
                    !Can't find this part at all
                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            End !If PartError#


        Of 'W'

            If wpr:Quantity = 0
                Return Level:Fatal
            End !If par:Quantity = 0

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = wpr:Ref_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found

            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


            If wpr:Part_Number = 'EXCH'
                if wpr:status = 'RTS' then
                    !change status to be picked for Rapidstock:Status
                    wpr:status = 'RET'
                ELSE
                    If wpr:Status <> 'REQ'
                        Return Level:Fatal
                    End !If par:Status = 'PIK'
                END
            End !If par:Part_Number = 'EXCH'


            If wpr:Part_Ref_Number = 0
                Return Level:Fatal
            End !If par:Part_Ref_Number = 0


            If wpr:Pending_Ref_Number <> ''
                Return Level:Fatal
            End !If par:Pending_Ref_Number <> ''


            If wpr:Order_Number <> '' and wpr:Date_Received = ''
                Return Level:Fatal
            End !If par:Order_Number <> '' and par:Date_Received = ''

            If wpr:WebOrder
                !Return Level:Fatal
            End !If par:WebOrder

            !Count this job?
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = wpr:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
                !Cannot find the job at all
                Return Level:Fatal
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign



            If glo:WebJob
                !Routine run but RRC. Only include jobs that aren't at the ARC
                If jobe:HubRepair
                    Return Level:Fatal
                End !If jobe:HubRepair

                !Is this job, for the account thats running the Stock Allocation?
                If wob:HeadAccountNumber <> Clarionet:Global.Param2
                    Return Level:Fatal
                End !If wob:HeadAccountNumber <> Clarionet:Global.Param2

            Else !If glo:WebJob
                If ~jobe:HubRepair
                    Return Level:Fatal
                End !If ~jobe:HubRepair

                !Running this routine at the ARC.
                !The selected job is at the HUB (i.e. HubRepair is ticked)
            End !If glo:WebJob



            !Now, lets get the stock part location...
            !It should be the same as the job, if not.. change it.
            PartError# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = wpr:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                !Is there a stock history entry?

                Access:STOHIST.ClearKey(shi:JobNumberKey)
                shi:Ref_Number       = sto:Ref_Number
                shi:Transaction_Type = 'DEC'
                shi:Job_Number       = wpr:Ref_Number
                If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                    !Found
                    !Found a stock history entry saying that this part was used for this location
                Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    PartError# = 1

                End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign

            Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Cannot find the selected part in stock control
                PartError# = 1
            End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


            If PartError#
                !There is an error with the part, let's try and find another part
                !with the same number in the RIGHT location
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location    = use:Location
                sto:Part_Number = wpr:Part_Number
                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Found the part in the user's location.
                    !Now is there a stock history to say that this part was attached to this job
                    !from this location?


                    Access:STOHIST.ClearKey(shi:JobNumberKey)
                    shi:Ref_Number       = sto:Ref_Number
                    shi:Transaction_Type = 'DEC'
                    shi:Job_Number       = wpr:Ref_Number
                    If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                        !Found
                        !Found a stock history entry saying that this part was used for this location
                        wpr:Part_Ref_Number = sto:Ref_Number
                        Access:WARPARTS.Update()


                    Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')

                        If glo:WebJob
                            !Cannot find this part in the RRC Locations, let's
                            !try the ARC Location
                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = tmp:ARCSiteLocation
                            sto:Part_Number = wpr:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Found
                                Access:STOHIST.ClearKey(shi:JobNumberKey)
                                shi:Ref_Number       = sto:Ref_Number
                                shi:Transaction_Type = 'DEC'
                                shi:Job_Number       = wpr:Ref_Number
                                If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                    !Found
                                    wpr:Part_Ref_Number = sto:Ref_Number
                                    Access:WARPARTS.Update()

                                Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                            Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Error
                            End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                        Else !If glo:WebJob
                            !Cannot find this part in the ARC Location, let's try the locatin
                            !of where the job was booked in.


                            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                            tra:Account_Number  = wob:HeadAccountNumber
                            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Found

                                Access:STOCK.Clearkey(sto:Location_Key)
                                sto:Location    = tra:SiteLocation
                                sto:Part_Number = wpr:Part_Number
                                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    !Found

                                    Access:STOHIST.ClearKey(shi:JobNumberKey)
                                    shi:Ref_Number       = sto:Ref_Number
                                    shi:Transaction_Type = 'DEC'
                                    shi:Job_Number       = wpr:Ref_Number
                                    If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                        !Found

                                        wpr:Part_Ref_Number = sto:Ref_Number
                                        Access:WARPARTS.Update()

                                    Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    !Error
                                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Error
                            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                        End !If glo:WebJob
                    End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign


                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Error
                    !Can't find this part at all
                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            End !If PartError#



        Of 'E'
            If epr:Quantity = 0
                Return Level:Fatal
            End !If epr:Quantity = 0

            !Don't count parts if the job is not an estimate
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = epr:Ref_Number
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                If job:Estimate <> 'YES'
                    Return Level:Fatal
                End !If job:Estimate = 'YES'
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

            If epr:Part_Ref_Number = 0
                Return Level:Fatal
            End !If par:Part_Ref_Number = 0

            If epr:Pending_Ref_Number <> ''
                Return Level:Fatal
            End !If par:Pending_Ref_Number <> ''

            If epr:Order_Number <> '' and epr:Date_Received = ''
                Return Level:Fatal
            End !If par:Order_Number <> '' and par:Date_Received = ''

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


            !Count this job?
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = epr:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
                !Cannot find the job at all
                Return Level:Fatal
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            If glo:WebJob
                !Routine run but RRC. Only include jobs that aren't at the ARC
                If jobe:HubRepair
                    Return Level:Fatal
                End !If jobe:HubRepair

                !Is this job, for the account thats running the Stock Allocation?
                If wob:HeadAccountNumber <> Clarionet:Global.Param2
                    Return Level:Fatal
                End !If wob:HeadAccountNumber <> Clarionet:Global.Param2

            Else !If glo:WebJob
                If ~jobe:HubRepair
                    Return Level:Fatal
                End !If ~jobe:HubRepair

                !Running this routine at the ARC.
                !The selected job is at the HUB (i.e. HubRepair is ticked)
            End !If glo:WebJob

            !Now, lets get the stock part location...
            !It should be the same as the job, if not.. change it.
            PartError# = 0
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = epr:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                !Is there a stock history entry?

                Access:STOHIST.ClearKey(shi:JobNumberKey)
                shi:Ref_Number       = sto:Ref_Number
                shi:Transaction_Type = 'DEC'
                shi:Job_Number       = epr:Ref_Number
                If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                    !Found
                    !Found a stock history entry saying that this part was used for this location
                Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    PartError# = 1
                End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign

            Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Cannot find the selected part in stock control
                PartError# = 1
            End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            If PartError#
                !There is an error with the part, let's try and find another part
                !with the same number in the RIGHT location
                Access:STOCK.Clearkey(sto:Location_Key)
                sto:Location    = use:Location
                sto:Part_Number = epr:Part_Number
                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Found the part in the user's location.
                    !Now is there a stock history to say that this part was attached to this job
                    !from this location?

                    Access:STOHIST.ClearKey(shi:JobNumberKey)
                    shi:Ref_Number       = sto:Ref_Number
                    shi:Transaction_Type = 'DEC'
                    shi:Job_Number       = epr:Ref_Number
                    If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                        !Found
                        !Found a stock history entry saying that this part was used for this location
                        epr:Part_Ref_Number = sto:Ref_Number
                        Access:ESTPARTS.Update()

                    Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')

                        If glo:WebJob
                            !Cannot find this part in the RRC Locations, let's
                            !try the ARC Location
                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = tmp:ARCSiteLocation
                            sto:Part_Number = epr:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Found
                                Access:STOHIST.ClearKey(shi:JobNumberKey)
                                shi:Ref_Number       = sto:Ref_Number
                                shi:Transaction_Type = 'DEC'
                                shi:Job_Number       = epr:Ref_Number
                                If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                    !Found
                                    epr:Part_Ref_Number = sto:Ref_Number
                                    Access:ESTPARTS.Update()

                                Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                            Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Error
                            End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                        Else !If glo:WebJob
                            !Cannot find this part in the ARC Location, let's try the locatin
                            !of where the job was booked in.
                            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                            tra:Account_Number  = wob:HeadAccountNumber
                            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Found
                                Access:STOCK.Clearkey(sto:Location_Key)
                                sto:Location    = tra:SiteLocation
                                sto:Part_Number = epr:Part_Number
                                If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    !Found
                                    Access:STOHIST.ClearKey(shi:JobNumberKey)
                                    shi:Ref_Number       = sto:Ref_Number
                                    shi:Transaction_Type = 'DEC'
                                    shi:Job_Number       = epr:Ref_Number
                                    If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                        !Found
                                        epr:Part_Ref_Number = sto:Ref_Number
                                        Access:ESTPARTS.Update()

                                    Else!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign
                                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    !Error
                                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                            Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Error
                            End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

                        End !If glo:WebJob
                    End!If Access:STOHIST.TryFetch(shi:JobNumberKey) = Level:Benign


                Else ! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                    !Error
                    !Can't find this part at all
                End !If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
            End !If PartError#
    End !Case func:Type
    Return Level:Benign
Local.AddFaulty       Procedure(String  func:Type)
Code
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        Case func:Type
            Of 'W'
                stf:PartNumber  = wpr:Part_Number
                stf:Description = wpr:Description
                stf:Quantity    = wpr:Quantity
                stf:PurchaseCost    = wpr:Purchase_Cost
            Of 'C'
                stf:PartNumber  = par:Part_Number
                stf:Description = par:Description
                stf:Quantity    = par:Quantity
                stf:PurchaseCost    = par:Purchase_Cost
            Of 'E'
                stf:PartNumber  = epr:Part_Number
                stf:Description = epr:Description
                stf:Quantity    = epr:Quantity
                stf:PurchaseCost    = epr:Purchase_Cost
        End !Case func:Type

        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Found

        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        stf:StoreUserCode   = use:User_Code
        stf:PartType        = 'FAU'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign
Local.RemovePartFromStock       Procedure(Long func:StockRefNumber,Long func:PartRecordNumber,Long func:Quantity,String func:PartType,String func:PartNumber)  !StockRefNumber, PartRecordNumber, Quantity, PartType, Quantity
Code

    !Message('In remove part from stock - with PartRecordNo='&clip(func:PartRecordNumber)&' and part type ='&clip(func:partType))
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = func:StockRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
        IF (StockInUse(1))
            Return Level:Fatal
        End

        If sto:Quantity_Stock < func:Quantity
            !There isn't enough in stock anymore
            Return Level:Fatal
        End !If sto:Quantity < func:Quantity

        If func:PartNumber = 'EXCH'
            Case Missive('The selected part is an "Exchange Part".'&|
              '<13,10>Click "Allocate Exchange" to allocate an exchange unit.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        Else !If stl:PartNumber = 'EXCH'
            Error# = 0
            Case func:PartType
                Of 'CHA'
                    Access:PARTS.Clearkey(par:recordnumberkey)
                    par:Record_Number   = func:PartRecordNumber
                    If Access:PARTS.Tryfetch(par:recordnumberkey) = Level:Benign
                        !Found
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = par:Ref_Number
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = par:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                            IF (StockInUse(1))
                                Return Level:Fatal
                            End

                            If sto:ReturnFaultySpare

                                Access:USERS.Clearkey(use:User_Code_Key)
                                use:User_Code = job:Engineer
                                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign         
                                  !Error
                                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                Case Missive('This faulty part must be returned before a new part can be issued:'&|
                                  '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                  '<13,10>Job: ' & Clip(par:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
                                               'mquest.jpg','Decline|Confirm')
                                    Of 2 ! Confirm Button
                                        Local.AddFaulty('C')
                                    Of 1 ! Decline Button
                                        Error# = 1
                                End ! Case Missive
                            End !If sto:ReturnFaultySpare
                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Else ! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                        !Error
                        Error# = 1
                    End !If AccessPARTS.Tryfetch(parRecord_Number_Key) = LevelBenign
                Of 'WAR'
                    Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                    wpr:Record_Number   = func:PartRecordNumber
                    If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = wpr:Ref_Number
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = wpr:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                            IF (StockInUse(1))
                                Return Level:Fatal
                            End

                            If sto:ReturnFaultySpare
                                Access:USERS.Clearkey(use:User_Code_Key)
                                use:User_Code = job:Engineer
                                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                Case Missive('This faulty part must be returned before a new part can be issued:'&|
                                  '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                  '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
                                               'mquest.jpg','Decline|Confirm')
                                    Of 2 ! Confirm Button
                                        Local.AddFaulty('W')
                                    Of 1 ! Decline Button
                                        Error# = 1
                                End ! Case Missive

                            End !If sto:ReturnFaultySpare

                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                    Else ! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                        !Error
                        Error# = 1
                    End !If AccessWARPARTS.Tryfetch(wprRecord_Number_Key) = LevelBenign



                Of 'EST'
                    Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
                    epr:Record_Number   = func:PartRecordNumber
                    If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
                        !Found
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = epr:Ref_Number
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Found

                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = epr:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            ! #11309 Make sure STock Item is not in use (DBH: 30/06/2010)
                            IF (StockInUse(1))
                                Return Level:Fatal
                            End

                            If sto:ReturnFaultySpare
                                Access:USERS.Clearkey(use:User_Code_Key)
                                use:User_Code = job:Engineer
                                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found

                                Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                                End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                Case Missive('This faulty part must be returned before a new part can be issued:'&|
                                  '<13,10>Part: ' & Clip(sto:Part_Number) & ' - ' & Clip(sto:Description) & '.'&|
                                  '<13,10>Job: ' & Clip(job:Ref_Number) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) & '.','ServiceBase 3g',|
                                               'mquest.jpg','Decline|Confirm')
                                    Of 2 ! Confirm Button
                                        Local.AddFaulty('E')
                                    Of 1 ! Decline Button
                                        Error# = 1
                                End ! Case Missive

                            End !If sto:ReturnFaultySpare

                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                    Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
                        !Error
                        Error# = 1
                    End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
            End !Case stl:PartType
        End !If func:PartNumber = 'EXCH'

        If Error# = 0
            sto:Quantity_Stock  -= func:Quantity
            If sto:Quantity_Stock < 0
                sto:Quantity_Stock = 0
            End !If sto:Quantity_Stock < 0
            IF Access:STOCK.Update()
                !Error
            END

            Case func:PartType
            Of 'CHA'
                Access:PARTS.Clearkey(par:RecordNumberKey)
                par:Record_Number   = func:PartRecordNumber
                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                                         par:Ref_Number, | ! Job_Number
                                         0, | ! Sales_Number
                                         func:Quantity, | ! Quantity
                                         par:Purchase_Cost, | ! Purchase_Cost
                                         par:Sale_Cost, | ! Sale_Cost
                                         par:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '') ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    par:WebOrder    = False
                    par:Status      = ''
                    par:PartAllocated   = 1
                    if AA20Relocated then       !need to allocate fully immeditately
                        par:WebOrder = 0
                        par:Date_Received = today()
                    END
                    Access:PARTS.Update()
                Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign

            Of 'WAR'
                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                wpr:Record_Number   = func:PartRecordNumber
                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Found
                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'DEC', | ! Transaction_Type
                                         wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                         wpr:Ref_Number, | ! Job_Number
                                         0, | ! Sales_Number
                                         func:Quantity, | ! Quantity
                                         wpr:Purchase_Cost, | ! Purchase_Cost
                                         wpr:Sale_Cost, | ! Sale_Cost
                                         wpr:Retail_Cost, | ! Retail_Cost
                                         'STOCK DECREMENTED', | ! Notes
                                         '') ! Information
                        ! Added OK

                    Else ! AddToStockHistory
                        ! Error
                    End ! AddToStockHistory
                    wpr:WebOrder    = False
                    wpr:Status      = ''
                    wpr:PartAllocated   = 1
                    if AA20Relocated then       !need to allocate fully immeditately
                        wpr:WebOrder = 0
                        wpr:Date_Received = today()
                    END

                    Access:WARPARTS.Update()
                Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                    ! Error
                End ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
            End ! Case func:PartType
            ! #10396 Suspend part if none in stock and suspended at main store. (DBH: 16/07/2010)
            SuspendedPartCheck()
        Else !If Error# = 0
            Return Level:Fatal
        End !If Error# = 0
    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = stl:JobNumber
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Found

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_COde   = job:Engineer
        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Found

        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

        !Blank entries are still being left after a refresh.
        !This will ensure that they can be deleted. - 3451 (DBH: 28-10-2003)
        Case Missive('Unable to fund the selected part in Stock Control.'&|
          '<13,10>Part:' & Clip(stl:PartNumber) & ' - ' & Clip(stl:Description) & '.'&|
          '<13,10>Job: ' & Clip(job:Ref_NUmber) & ' - ' & Clip(use:Forename) & ' ' & Clip(use:Surname) &|
          '<13,10>Do you wish to REMOVE this part line from Stock Allocation?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Remove')
            Of 2 ! Remove Button
                RemoveFromStockAllocation(func:PartRecordNumber,func:PartType)
            Of 1 ! Cancel Button
        End ! Case Missive
        Return Level:Fatal
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Return Level:Benign
Local.FindOtherParts            Procedure(Long func:JobNumber,Long func:RecordNumber)    !Job Number, Record Number
Code
        Found# = 0
        !Lets find some other parts in the allocation list

        Save_stl_ID = Access:STOCKALL.SaveFile()
        Access:STOCKALL.ClearKey(stl:StatusJobNumberKey)
        stl:Location  = tmp:Location
        stl:Status    = 'WEB'
        Set(stl:StatusJobNumberKey,stl:StatusJobNumberKey)
        Loop
            If Access:STOCKALL.NEXT()
               Break
            End !If
            If stl:Location  <> tmp:Location      |
            Or stl:Status    <> 'WEB'      |
                Then Break.  ! End If

            !Only look for parts from the same job
            If stl:JobNumber <> func:JobNumber
                Cycle
            End !If stl:JobNumber <> tmp:JobNumber
            !Make sure you haven't found the original record
            If stl:RecordNumber = func:RecordNumber
                Cycle
            End !If stl:RecordNumber = rapque:RecordNumber

            !Are there any in stock
            Case stl:PartType
                Of 'CHA'
                    Access:PARTS.Clearkey(par:RecordNumberKey)
                    par:Record_Number   = stl:PartRecordNumber
                    If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = par:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If par:Quantity > sto:Quantity_Stock
                                Found# = 1
                                Break
                            End !If par:Quantity < sto:Quantity_Stock
                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                            Found# = 1
                            Break
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                        !Error
                    End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                Of 'WAR'
                    Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                    wpr:Record_Number   = stl:PartRecordNumber
                    If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                        !Found
                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                        sto:Ref_Number  = wpr:Part_Ref_Number
                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Found
                            If wpr:Quantity > sto:Quantity_Stock
                                Found# = 1
                                Break
                            End !If wpr:Quantity > sto:Quantity_Stock
                        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            !Error
                            Found# = 1
                            Break
                        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    Else ! If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
                        !Error
                    End !If Access:WARPARTS.Tryfetch(war:RecordNumberKey) = Level:Benign
            End !Case stl:PartType
        End !Loop
        Access:STOCKALL.RestoreFile(Save_stl_ID)

        Return Found#
SkipCorruptJobs     PROCEDURE(Long fRefNumber)
    CODE
        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = fRefNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key))
            RETURN 1
        END

        If (job:Cancelled = 'YES')
            RETURN 1
        END

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = fRefNumber
        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            RETURN 1
        END

        RETURN 0
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW2.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet1) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?Sheet1) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?Sheet1) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  ELSIF Choice(?Sheet1) = 4
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Engineer
  ELSIF Choice(?Sheet1) = 5
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Blank_Byte
  ELSIF Choice(?Sheet1) = 6
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Pro_Bye
  ELSIF Choice(?Sheet1) = 7
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = PIK_Byte
  ELSIF Choice(?Sheet1) = 8
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = RET_Byte
  ELSIF Choice(?Sheet1) = 9
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Web_String
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:Location
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW2.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet1) = 3
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?Sheet1) = 4
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?Sheet1) = 5
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?Sheet1) = 6
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?Sheet1) = 7
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?Sheet1) = 8
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?Sheet1) = 9
    RETURN SELF.SetSort(9,Force)
  ELSE
    RETURN SELF.SetSort(10,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = stl:Engineer
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:FullEngineer    = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
      tmp:FullEngineer    = ''
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  
  !------------------------------------------------------------------
  Web_Temp_Job = stl:JobNumber
  access:Webjob.clearkey(wob:RefNumberKey)
  wob:RefNumber = stl:JobNumber
  if access:Webjob.fetch(wob:refNumberKey)=level:benign then
      access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      access:tradeacc.fetch(tra:Account_Number_Key)
      Web_Temp_Job = stl:JobNumber & '-' & tra:BranchIdentification & wob:JobNumber
  END
  !------------------------------------------------------------------
  
  !Show the pretty green box to show the stock is available
  tmp:StockAvailable = 0
  tmp:QuantityStock = 0
  
  Access:STOCK.Clearkey(sto:Ref_Number_Key)
  sto:Ref_Number  = stl:PartRefNumber
  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Found
      tmp:QuantityStock   = sto:Quantity_Stock
      If tmp:QuantityStock >= stl:Quantity
          tmp:StockAvailable = 1
      End !If sto:Quantity_Stock <= stl:Quantity
  Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      !Error
      Access:STOCK.ClearKey(sto:Location_Key)
      sto:Location    = use:Location
      sto:Part_Number = stl:PartNumber
      If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
          !Found
          tmp:QuantityStock = sto:Quantity_Stock
          If tmp:QuantityStock >= stl:Quantity
              tmp:StockAvailable = 1
          End !If sto:Quantity_Stock <= stl:Quantity
      Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          tmp:QuantityStock = ''
      End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
  End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
  Access:StockAlx.clearkey(STLX:KeySTLRecordNumber)
  STLX:StlRecordNumber = Stl:RecordNumber
  if access:StockAlx.fetch(STLX:KeySTLRecordNumber)
      ReqDate = 'UNKNOWN'
      ReqTime = 'UNKNOWN'
  ELSE
      ReqDate = format(STLX:RequestDate,@d06)
      ReqTime = format(STLX:RequestTime,@T1)
  END
  PARENT.SetQueueRecord
  SELF.Q.stl:ShelfLocation_NormalFG = -1
  SELF.Q.stl:ShelfLocation_NormalBG = -1
  SELF.Q.stl:ShelfLocation_SelectedFG = -1
  SELF.Q.stl:ShelfLocation_SelectedBG = -1
  SELF.Q.stl:Description_NormalFG = -1
  SELF.Q.stl:Description_NormalBG = -1
  SELF.Q.stl:Description_SelectedFG = -1
  SELF.Q.stl:Description_SelectedBG = -1
  SELF.Q.stl:PartNumber_NormalFG = -1
  SELF.Q.stl:PartNumber_NormalBG = -1
  SELF.Q.stl:PartNumber_SelectedFG = -1
  SELF.Q.stl:PartNumber_SelectedBG = -1
  SELF.Q.tmp:FullEngineer_NormalFG = -1
  SELF.Q.tmp:FullEngineer_NormalBG = -1
  SELF.Q.tmp:FullEngineer_SelectedFG = -1
  SELF.Q.tmp:FullEngineer_SelectedBG = -1
  SELF.Q.Web_Temp_Job_NormalFG = -1
  SELF.Q.Web_Temp_Job_NormalBG = -1
  SELF.Q.Web_Temp_Job_SelectedFG = -1
  SELF.Q.Web_Temp_Job_SelectedBG = -1
  SELF.Q.stl:Quantity_NormalFG = -1
  SELF.Q.stl:Quantity_NormalBG = -1
  SELF.Q.stl:Quantity_SelectedFG = -1
  SELF.Q.stl:Quantity_SelectedBG = -1
  SELF.Q.tmp:QuantityStock_NormalFG = -1
  SELF.Q.tmp:QuantityStock_NormalBG = -1
  SELF.Q.tmp:QuantityStock_SelectedFG = -1
  SELF.Q.tmp:QuantityStock_SelectedBG = -1
  SELF.Q.tmp:StockAvailable_NormalFG = -1
  SELF.Q.tmp:StockAvailable_NormalBG = -1
  SELF.Q.tmp:StockAvailable_SelectedFG = -1
  SELF.Q.tmp:StockAvailable_SelectedBG = -1
  IF (tmp:stockavailable = 1)
    SELF.Q.tmp:StockAvailable_Icon = 1
  ELSE
    SELF.Q.tmp:StockAvailable_Icon = 2
  END
  SELF.Q.stl:RecordNumber_NormalFG = -1
  SELF.Q.stl:RecordNumber_NormalBG = -1
  SELF.Q.stl:RecordNumber_SelectedFG = -1
  SELF.Q.stl:RecordNumber_SelectedBG = -1
  SELF.Q.ReqDate_NormalFG = -1
  SELF.Q.ReqDate_NormalBG = -1
  SELF.Q.ReqDate_SelectedFG = -1
  SELF.Q.ReqDate_SelectedBG = -1
  SELF.Q.ReqTime_NormalFG = -1
  SELF.Q.ReqTime_NormalBG = -1
  SELF.Q.ReqTime_SelectedFG = -1
  SELF.Q.ReqTime_SelectedBG = -1
   
   
   IF (stl:Status = 'PRO')
     SELF.Q.stl:ShelfLocation_NormalFG = 16711935
     SELF.Q.stl:ShelfLocation_NormalBG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedFG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.stl:ShelfLocation_NormalFG = 8388608
     SELF.Q.stl:ShelfLocation_NormalBG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedFG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.stl:ShelfLocation_NormalFG = 255
     SELF.Q.stl:ShelfLocation_NormalBG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedFG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.stl:ShelfLocation_NormalFG = 8388736
     SELF.Q.stl:ShelfLocation_NormalBG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedFG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedBG = 8388736
   ELSE
     SELF.Q.stl:ShelfLocation_NormalFG = 0
     SELF.Q.stl:ShelfLocation_NormalBG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedFG = 16777215
     SELF.Q.stl:ShelfLocation_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.stl:Description_NormalFG = 16711935
     SELF.Q.stl:Description_NormalBG = 16777215
     SELF.Q.stl:Description_SelectedFG = 16777215
     SELF.Q.stl:Description_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.stl:Description_NormalFG = 8388608
     SELF.Q.stl:Description_NormalBG = 16777215
     SELF.Q.stl:Description_SelectedFG = 16777215
     SELF.Q.stl:Description_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.stl:Description_NormalFG = 255
     SELF.Q.stl:Description_NormalBG = 16777215
     SELF.Q.stl:Description_SelectedFG = 16777215
     SELF.Q.stl:Description_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.stl:Description_NormalFG = 8388736
     SELF.Q.stl:Description_NormalBG = 16777215
     SELF.Q.stl:Description_SelectedFG = 16777215
     SELF.Q.stl:Description_SelectedBG = 8388736
   ELSE
     SELF.Q.stl:Description_NormalFG = 0
     SELF.Q.stl:Description_NormalBG = 16777215
     SELF.Q.stl:Description_SelectedFG = 16777215
     SELF.Q.stl:Description_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.stl:PartNumber_NormalFG = 16711935
     SELF.Q.stl:PartNumber_NormalBG = 16777215
     SELF.Q.stl:PartNumber_SelectedFG = 16777215
     SELF.Q.stl:PartNumber_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.stl:PartNumber_NormalFG = 8388608
     SELF.Q.stl:PartNumber_NormalBG = 16777215
     SELF.Q.stl:PartNumber_SelectedFG = 16777215
     SELF.Q.stl:PartNumber_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.stl:PartNumber_NormalFG = 255
     SELF.Q.stl:PartNumber_NormalBG = 16777215
     SELF.Q.stl:PartNumber_SelectedFG = 16777215
     SELF.Q.stl:PartNumber_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.stl:PartNumber_NormalFG = 8388736
     SELF.Q.stl:PartNumber_NormalBG = 16777215
     SELF.Q.stl:PartNumber_SelectedFG = 16777215
     SELF.Q.stl:PartNumber_SelectedBG = 8388736
   ELSE
     SELF.Q.stl:PartNumber_NormalFG = 0
     SELF.Q.stl:PartNumber_NormalBG = 16777215
     SELF.Q.stl:PartNumber_SelectedFG = 16777215
     SELF.Q.stl:PartNumber_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.tmp:FullEngineer_NormalFG = 16711935
     SELF.Q.tmp:FullEngineer_NormalBG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedFG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.tmp:FullEngineer_NormalFG = 8388608
     SELF.Q.tmp:FullEngineer_NormalBG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedFG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.tmp:FullEngineer_NormalFG = 255
     SELF.Q.tmp:FullEngineer_NormalBG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedFG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.tmp:FullEngineer_NormalFG = 8388736
     SELF.Q.tmp:FullEngineer_NormalBG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedFG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:FullEngineer_NormalFG = 0
     SELF.Q.tmp:FullEngineer_NormalBG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedFG = 16777215
     SELF.Q.tmp:FullEngineer_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.Web_Temp_Job_NormalFG = 16711935
     SELF.Q.Web_Temp_Job_NormalBG = 16777215
     SELF.Q.Web_Temp_Job_SelectedFG = 16777215
     SELF.Q.Web_Temp_Job_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.Web_Temp_Job_NormalFG = 8388608
     SELF.Q.Web_Temp_Job_NormalBG = 16777215
     SELF.Q.Web_Temp_Job_SelectedFG = 16777215
     SELF.Q.Web_Temp_Job_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.Web_Temp_Job_NormalFG = 255
     SELF.Q.Web_Temp_Job_NormalBG = 16777215
     SELF.Q.Web_Temp_Job_SelectedFG = 16777215
     SELF.Q.Web_Temp_Job_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.Web_Temp_Job_NormalFG = 8388736
     SELF.Q.Web_Temp_Job_NormalBG = 16777215
     SELF.Q.Web_Temp_Job_SelectedFG = 16777215
     SELF.Q.Web_Temp_Job_SelectedBG = 8388736
   ELSE
     SELF.Q.Web_Temp_Job_NormalFG = 0
     SELF.Q.Web_Temp_Job_NormalBG = 16777215
     SELF.Q.Web_Temp_Job_SelectedFG = 16777215
     SELF.Q.Web_Temp_Job_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.stl:Quantity_NormalFG = 16711935
     SELF.Q.stl:Quantity_NormalBG = 16777215
     SELF.Q.stl:Quantity_SelectedFG = 16777215
     SELF.Q.stl:Quantity_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.stl:Quantity_NormalFG = 8388608
     SELF.Q.stl:Quantity_NormalBG = 16777215
     SELF.Q.stl:Quantity_SelectedFG = 16777215
     SELF.Q.stl:Quantity_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.stl:Quantity_NormalFG = 255
     SELF.Q.stl:Quantity_NormalBG = 16777215
     SELF.Q.stl:Quantity_SelectedFG = 16777215
     SELF.Q.stl:Quantity_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.stl:Quantity_NormalFG = 8388736
     SELF.Q.stl:Quantity_NormalBG = 16777215
     SELF.Q.stl:Quantity_SelectedFG = 16777215
     SELF.Q.stl:Quantity_SelectedBG = 8388736
   ELSE
     SELF.Q.stl:Quantity_NormalFG = 0
     SELF.Q.stl:Quantity_NormalBG = 16777215
     SELF.Q.stl:Quantity_SelectedFG = 16777215
     SELF.Q.stl:Quantity_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.tmp:QuantityStock_NormalFG = 16711935
     SELF.Q.tmp:QuantityStock_NormalBG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedFG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.tmp:QuantityStock_NormalFG = 8388608
     SELF.Q.tmp:QuantityStock_NormalBG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedFG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.tmp:QuantityStock_NormalFG = 255
     SELF.Q.tmp:QuantityStock_NormalBG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedFG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.tmp:QuantityStock_NormalFG = 8388736
     SELF.Q.tmp:QuantityStock_NormalBG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedFG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:QuantityStock_NormalFG = 0
     SELF.Q.tmp:QuantityStock_NormalBG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedFG = 16777215
     SELF.Q.tmp:QuantityStock_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.tmp:StockAvailable_NormalFG = 16711935
     SELF.Q.tmp:StockAvailable_NormalBG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedFG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.tmp:StockAvailable_NormalFG = 8388608
     SELF.Q.tmp:StockAvailable_NormalBG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedFG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.tmp:StockAvailable_NormalFG = 255
     SELF.Q.tmp:StockAvailable_NormalBG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedFG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.tmp:StockAvailable_NormalFG = 8388736
     SELF.Q.tmp:StockAvailable_NormalBG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedFG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedBG = 8388736
   ELSE
     SELF.Q.tmp:StockAvailable_NormalFG = 0
     SELF.Q.tmp:StockAvailable_NormalBG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedFG = 16777215
     SELF.Q.tmp:StockAvailable_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.stl:RecordNumber_NormalFG = 16711935
     SELF.Q.stl:RecordNumber_NormalBG = 16777215
     SELF.Q.stl:RecordNumber_SelectedFG = 16777215
     SELF.Q.stl:RecordNumber_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.stl:RecordNumber_NormalFG = 8388608
     SELF.Q.stl:RecordNumber_NormalBG = 16777215
     SELF.Q.stl:RecordNumber_SelectedFG = 16777215
     SELF.Q.stl:RecordNumber_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.stl:RecordNumber_NormalFG = 255
     SELF.Q.stl:RecordNumber_NormalBG = 16777215
     SELF.Q.stl:RecordNumber_SelectedFG = 16777215
     SELF.Q.stl:RecordNumber_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.stl:RecordNumber_NormalFG = 8388736
     SELF.Q.stl:RecordNumber_NormalBG = 16777215
     SELF.Q.stl:RecordNumber_SelectedFG = 16777215
     SELF.Q.stl:RecordNumber_SelectedBG = 8388736
   ELSE
     SELF.Q.stl:RecordNumber_NormalFG = 0
     SELF.Q.stl:RecordNumber_NormalBG = 16777215
     SELF.Q.stl:RecordNumber_SelectedFG = 16777215
     SELF.Q.stl:RecordNumber_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.ReqDate_NormalFG = 16711935
     SELF.Q.ReqDate_NormalBG = 16777215
     SELF.Q.ReqDate_SelectedFG = 16777215
     SELF.Q.ReqDate_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.ReqDate_NormalFG = 8388608
     SELF.Q.ReqDate_NormalBG = 16777215
     SELF.Q.ReqDate_SelectedFG = 16777215
     SELF.Q.ReqDate_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.ReqDate_NormalFG = 255
     SELF.Q.ReqDate_NormalBG = 16777215
     SELF.Q.ReqDate_SelectedFG = 16777215
     SELF.Q.ReqDate_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.ReqDate_NormalFG = 8388736
     SELF.Q.ReqDate_NormalBG = 16777215
     SELF.Q.ReqDate_SelectedFG = 16777215
     SELF.Q.ReqDate_SelectedBG = 8388736
   ELSE
     SELF.Q.ReqDate_NormalFG = 0
     SELF.Q.ReqDate_NormalBG = 16777215
     SELF.Q.ReqDate_SelectedFG = 16777215
     SELF.Q.ReqDate_SelectedBG = 0
   END
   IF (stl:Status = 'PRO')
     SELF.Q.ReqTime_NormalFG = 16711935
     SELF.Q.ReqTime_NormalBG = 16777215
     SELF.Q.ReqTime_SelectedFG = 16777215
     SELF.Q.ReqTime_SelectedBG = 16711935
   ELSIF(stl:Status = 'PIK')
     SELF.Q.ReqTime_NormalFG = 8388608
     SELF.Q.ReqTime_NormalBG = 16777215
     SELF.Q.ReqTime_SelectedFG = 16777215
     SELF.Q.ReqTime_SelectedBG = 8388608
   ELSIF(stl:Status = 'RET')
     SELF.Q.ReqTime_NormalFG = 255
     SELF.Q.ReqTime_NormalBG = 16777215
     SELF.Q.ReqTime_SelectedFG = 16777215
     SELF.Q.ReqTime_SelectedBG = 255
   ELSIF(stl:Status = 'WEB')
     SELF.Q.ReqTime_NormalFG = 8388736
     SELF.Q.ReqTime_NormalBG = 16777215
     SELF.Q.ReqTime_SelectedFG = 16777215
     SELF.Q.ReqTime_SelectedBG = 8388736
   ELSE
     SELF.Q.ReqTime_NormalFG = 0
     SELF.Q.ReqTime_NormalBG = 16777215
     SELF.Q.ReqTime_SelectedFG = 16777215
     SELF.Q.ReqTime_SelectedBG = 0
   END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 200
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Exchange_Check_Procedure PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
Added_In             LONG
save_eau_id          USHORT,AUTO
Stock_Query          STRING(30)
LOC:Amount           DECIMAL(15,2)
No_Find              BYTE
BrLocator1           STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Master_Location      STRING(30)
Master_Number        LONG
Variance             LONG
Temp_Cut_String      STRING(10)
New_level            LONG
Use_Exch_Byte        STRING('YES')
Exchange_IMEI        STRING(20)
In_stock             LONG
Master_Type          STRING(30)
Loc_Status           STRING(100)
CurrentStatus        STRING(30)
LocalAuditNo         LONG
DisplayString1       STRING(50)
DisplayString2       STRING(50)
FullCount            LONG
ExportFilename       STRING(255),STATIC
ExportPath           STRING(255)
ErrorCount           LONG
LocalErrorString     STRING(100)
BRW1::View:Browse    VIEW(EXCHPREP)
                       PROJECT(EAP:RefNumber)
                       PROJECT(EAP:Audit_Number)
                       PROJECT(EAP:IMEI)
                       PROJECT(EAP:User_Code)
                       PROJECT(EAP:Method)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
EAP:RefNumber          LIKE(EAP:RefNumber)            !List box control field - type derived from field
EAP:Audit_Number       LIKE(EAP:Audit_Number)         !List box control field - type derived from field
EAP:IMEI               LIKE(EAP:IMEI)                 !List box control field - type derived from field
EAP:User_Code          LIKE(EAP:User_Code)            !List box control field - type derived from field
EAP:Method             LIKE(EAP:Method)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Exchange'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Exchange Audit Procedure'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,284,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Scanning IMEI'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s50),AT(72,76,267,10),USE(DisplayString1),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING(@s50),AT(72,90,267,10),USE(DisplayString2),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING(@n_6),AT(164,252,68,14),USE(New_level),TRN,CENTER,FONT(,12,010101H,FONT:bold,CHARSET:ANSI)
                           STRING('Full Count'),AT(171,270,68,12),USE(?StringFullCount),TRN,HIDE,CENTER,FONT(,,010101H,FONT:bold,CHARSET:ANSI)
                           STRING(@n6),AT(168,282,68,14),USE(FullCount),TRN,HIDE,CENTER,FONT(,,010101H,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(172,306),USE(?ButtonRefresh),TRN,FLAT,HIDE,ICON('refreshp.jpg')
                           STRING('COUNTED'),AT(172,240,68,12),USE(?String6),TRN,CENTER,FONT(,,010101H,FONT:bold)
                           PANEL,AT(172,236,68,60),USE(?Panel1:2),FILL(COLOR:Yellow)
                           ENTRY(@s20),AT(144,144,124,10),USE(Exchange_IMEI),DISABLE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                         END
                       END
                       SHEET,AT(352,54,264,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Scanned Units'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(436,80,96,276),USE(?List),IMM,FONT(,,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('0L(2)|M@n8@0L(2)|M@n8@80L(2)|M~IMEI~@s20@0L(2)|M@s3@0L(2)|M@s10@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(544,366),USE(?SuspendAudit),SKIP,TRN,FLAT,LEFT,ICON('susaudp.jpg')
                       BUTTON,AT(316,368),USE(?ButtonFinScan),TRN,FLAT,HIDE,ICON('finscanp.jpg')
                       BUTTON,AT(173,368),USE(?ButtonImportFile),SKIP,TRN,FLAT,HIDE,ICON('ImpFileP.jpg')
                       BUTTON('Close'),AT(28,400,76,20),USE(?Close),HIDE,LEFT,ICON('Cancel.ico')
                       BUTTON,AT(476,366),USE(?Button15:2),SKIP,TRN,FLAT,LEFT,ICON('compaudp.jpg')
                       BUTTON,AT(544,366),USE(?ContinueAudit),SKIP,TRN,FLAT,ICON('contaudp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
MyExcel8             Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ExportFile    File,Driver('BASIC'),Pre(expfil),Name(ExportFilename),Create,Bindable,Thread
Record        Record
SiteLocation   String(30)
AuditNumber    String(10)
StockRef       String(30)
ErrorString    String(100)
END
END


Expo    MyExportClass   !this is only wanted for the progress screen
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Refresh_New_Level   Routine

    New_Level = 0
    !TB13194 - count from the new system
    Access:Exchprep.clearkey(EAP:KeyAuditUser)
    EAP:Audit_Number = emf:Audit_Number
    EAP:User_Code    = glo:userCode
    Set(EAP:KeyAuditUser,EAP:KeyAuditUser)
    Loop
        if access:EXCHPREP.next() then break.
        if EAP:Audit_Number <> emf:Audit_Number then break.
        if  EAP:User_Code   <> glo:userCode then break.
        New_level += 1
    END

    display(?New_Level)

    if emf:User = glo:userCode then
        !this is the supervising user and we need the full count as well

        FullCount = 0
        ?FullCount{prop:hide}=false
        ?buttonRefresh{prop:hide}=false
        ?StringFullCount{prop:hide}=false

        Access:EXCHPREP.clearkey(EAP:KeyAudit)
        EAP:Audit_Number = emf:Audit_Number
        set(EAP:KeyAudit,EAP:KeyAudit)
        loop
            if access:Exchprep.next() then break.
            if EAP:Audit_Number <> emf:Audit_Number then break.

            FullCount += 1

        END !loop
        display()
    END


    exit




!
!    Access:Exchaui.clearkey(eau:Audit_Number_Key)
!    eau:Audit_Number = emf:Audit_Number
!    Set(eau:Audit_Number_Key,eau:Audit_Number_Key)
!    loop
!
!        if access:EXchaui.next() then break.
!        if eau:Audit_Number <> emf:Audit_Number then break.
!        
!        New_Level += eau:Confirmed      !one or zero
!
!    END



ContinueSuspend     Routine
      If emf:Status   <> 'SUSPENDED'

          emf:Status = 'SUSPENDED'
          ACcess:EXCHAMF.Update()
          Post(Event:CloseWindow)
      Else !emf:Status   <> 'SUSPENDED'
          emf:Status = 'AUDIT IN PROGRESS'
          ?SuspendAudit{prop:Hide} = 0
          ?ContinueAudit{prop:Hide} = 1
          !?buttonLookupStockType{prop:Disable} = 0
          !?master_Type{prop:Disable} = 0
          ?Exchange_IMEI{prop:disable}=0
          !?Button15{PROP:Text} = 'Suspend Audit'
!          ENABLE(?Master_Location)
      End !emf:Status   <> 'SUSPENDED'
      
      CurrentStatus = emf:status

      !IF Stom:Send = ''
      !  stom:Send = 'S'
      !  Access:STMASAUD.Update()
ProcessPrepFile     Routine

    !Check for export file - needs to be set for ServiceBase, not for webmaster
    if not glo:webjob
        if clip(GETINI('EXCEPTION','AUDITIMPORT',,CLIP(Path()) & '\SB2KDEF.INI')) = '' then
            miss# = missive('No exception file path has been set up in defaults. This procedure cannot work until that is completed.','ServiceBase 3g','mexclam.jpg','CANCEL')
            EXIT
        END
    END !if not glo:Webjob

    ExportFilename = ''
    do Refresh_New_Level    !gets number in FullCount
    ErrorCount = 0


    Expo.OpenProgressWindow(0,FullCount)

    Expo.UpdateProgressText()          !blank line
    
    Expo.UpdateProgressText('Completion Process Started: ' & FORMAT(Expo.StartDate,@d06) & |
        ' ' & FORMAT(Expo.StartTime,@t04))

    Expo.UpdateProgressText()
    Expo.UpdateProgressText('This is a two part process, and will take some time.')
    Expo.UpdateProgressText()
    Expo.UpdateProgressText('Records to process: (Twice) '&clip(FullCount))
    Expo.UpdateProgressText()


    Access:EXCHPREP.clearkey(EAP:KeyAudit)
    EAP:Audit_Number = emf:Audit_Number
    set(EAP:KeyAudit,EAP:KeyAudit)
    Loop

        if access:EXCHPREP.next() then break.
        if EAP:Audit_Number <> emf:Audit_Number then break.


        !display progress
        if Expo.UpdateProgressWindow()
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Completion Process Interrupted ' & FORMAT(Today(),@d6) & |
                ' ' & FORMAT(clock(),@t1))
            Expo.FinishProgress()
            !ReportCancelled = true
            break
        END

        IF (Expo.CancelPressed = 2)
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Completion Process Cancelled: ' & FORMAT(Today(),@d6) & |
                ' ' & FORMAT(clock(),@t1))
            Expo.FinishProgress()
            !ReportCancelled = true
            break
        END

        !now we do the checking
        Access:EXCHAUI.Clearkey(eau:Locate_IMEI_Key)
        eau:Audit_Number    = EAP:Audit_Number
        eau:Site_Location   = Master_Location               !SentLocation
        eau:IMEI_Number     = EAP:IMEI
        If Access:EXCHAUI.Tryfetch(eau:Locate_IMEI_Key) = Level:Benign
            !Found
            !Check to see if the IMEI exists in another location
            !and if not, check the Loan Database - L883 (DBH: 12-09-2003)
            Error#= 0
            Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
            xch_ali:ESN = upper(EAP:IMEI)
            If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
                !Found
                If xch_ali:Location <> Master_Location
                    LocalErrorString = 'This unit exists in another location: '&clip(xch_ali:Location)
                    Do ExportError
                    Error# = 1
                End !If xch_ali:Location <> SentLocation
            Else !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
                !Error
                Access:LOAN_ALIAS.ClearKey(loa_ali:ESN_Only_Key)
                loa_ali:ESN = EAP:IMEI
                If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
                    !Found
                    LocalErrorString = 'This unit exists in the Loan Database not the exchange.'
                    Do ExportError
                    Error# = 1
                Else !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
                    !Error
                    LocalErrorString = 'This unit does not exist in the Exchange Database and must be added manually.'
                    Do ExportError
                    Error# = 1

                End !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
            End !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign

            If Error# = 0
                IF eau:Confirmed = TRUE   ! #12276 Don't allow to scan the same IMEI more than once. (Bryan: 10/10/2011)
                    LocalErrorString = 'This IMEI Number has already been scanned or imported.'
                    do ExportError
                ELSE ! IF (eau:Confirmed = TRUE)
                    eau:Confirmed = TRUE
                    Access:EXCHAUI.TryUpdate()
                END ! IF (eau:Confirmed = TRUE)
            End !If Error# = 0


        Else! If Access:EXCHAUI.Tryfetch(eau:AuditIMEIKey) = Level:Benign
            !Error
            LocalErrorString = 'This IMEI is not recognized in the exchange audit database. Add manually as an EXCESS if required.'
            Do ExportError

        End! If Access:EXCHAUI.Tryfetch(eau:AuditIMEIKey) = Level:Benign

    END !loop through EXCHPREP

    Expo.UpdateProgressText('===============')
    Expo.UpdateProgressText('Completion Process Completed: ' & FORMAT(Today(),@d06) & |
           ' ' & FORMAT(clock(),@t04))

    if ErrorCount = 0 then
        !no errors have been generated
    ELSE
        close(ExportFile)
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Errors have occured during this import')
        if glo:Webjob then
            !we cannot show the export!
            ExportPath = ''
            Expo.UpdateProgressText('The error file will now download')
            flq:Filename = ExportFilename
            add(FileListQueue)
            Return" = ClarioNET:SendFilesToClient(1,0)
        END
    END
    Expo.FinishProgress(clip(ExportPath))

    EXIT
ExportError         Routine

    ErrorCount += 1

    if ExportFilename = '' then
        !not been generated yet
        if glo:Webjob then
            ExportPath     = path()
        ELSE
            ExportPath     = GETINI('EXCEPTION','AUDITIMPORT',,CLIP(Path()) & '\SB2KDEF.INI')
        END
        ExportFilename = clip(ExportPath) |
                         & '\ExchangeAudit'&'_'&format(today(),@D12)&'-'&left(clip(format(emf:Audit_Number,@n09)))&'.csv'
                        !StockAudit_Date_AuditNumber.csv,

        if exists(ExportFilename) then remove(exportFilename).

        Create(ExportFile)
        Open(ExportFile)

        !first line
        !expfil:LineNo = 'Line No'
        expfil:siteLocation = 'Location'
        expfil:AuditNumber  = 'Audit No'
        expfil:StockRef     = 'IMEI'
        expfil:ErrorString  = 'Reason'
        Add(ExportFile)

    END !if exportfilename is still blank

    !expfil:LineNo = LocalCount
    expfil:SiteLocation = Master_Location
    expfil:AuditNumber  = format(EAP:Audit_Number,@n10)
    expfil:StockRef     = EAP:IMEI     !imei number
    expfil:ErrorString  = clip(LocalErrorString)&': '&EAP:User_Code&' '&clip(EAP:Method)&' entry.'
    Add(ExportFile)

    exit
    

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020479'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Exchange_Check_Procedure')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:EXCEXCH.Open
  Relate:EXCHALC.Open
  Relate:EXCHAMF.Open
  Relate:EXCHPREP.Open
  Relate:GENSHORT.Open
  Relate:LOAN_ALIAS.Open
  Relate:STOAUUSE.Open
  Relate:STOCK.Open
  Access:EXCHANGE_ALIAS.UseFile
  Access:STOCKTYP.UseFile
  Access:EXCHANGE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:EXCHPREP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
    if emf:User <> glo:userCode then
        !this must be a joining user - must ignore suspended status
        !hide most buttons but show new [FINISH SCAN] button
        
        ?ContinueAudit{prop:Hide} = 1
        ?SuspendAudit{prop:Hide} = 1
        ?Button15:2{prop:Hide} = 1
        ?ButtonFinScan{prop:Hide} = 0
        ?Exchange_IMEI{prop:disable}=0
        CurrentStatus = 'JOINED AUDIT'

    ELSE

      CurrentStatus = emf:status

      IF emf:Status = 'SUSPENDED'
        ?SuspendAudit{prop:Hide} = 1
        ?ContinueAudit{prop:Hide} = 0
     !   ?Button15{PROP:Text} = 'Continue Audit'
        !DISABLE(?Master_Location)
              !?buttonLookupStockType{prop:Disable} = 1
              !?master_Type{prop:Disable} = 1
    !          DISABLE(?Exchange_IMEI)
      ELSE
        ?SuspendAudit{prop:Hide} = 0
        ?ContinueAudit{prop:Hide} = 1
        ?Exchange_IMEI{prop:disable}=0
        !    ?Button15{PROP:Text} = 'Suspend Audit'
        !ENABLE(?Master_Location)
      END

    END

    Master_Location = emf:Site_location
    Master_Number   = emf:Audit_Number
    DisplayString1  = 'Location: ' & clip(Master_Location)
    DisplayString2  = 'Audit Number: '&clip(Master_Number)

    if glo:Webjob = 0
      ?ButtonImportFile{prop:hide}=0
    END
      ! Save Window Name
   AddToLog('Window','Open','Exchange_Check_Procedure')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,EAP:KeyAuditUser)
  BRW1.AddRange(EAP:Audit_Number,emf:Audit_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,EAP:User_Code,1,BRW1)
  BIND('glo:UserCode',glo:UserCode)
  BRW1.AddField(EAP:RefNumber,BRW1.Q.EAP:RefNumber)
  BRW1.AddField(EAP:Audit_Number,BRW1.Q.EAP:Audit_Number)
  BRW1.AddField(EAP:IMEI,BRW1.Q.EAP:IMEI)
  BRW1.AddField(EAP:User_Code,BRW1.Q.EAP:User_Code)
  BRW1.AddField(EAP:Method,BRW1.Q.EAP:Method)
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:EXCEXCH.Close
    Relate:EXCHALC.Close
    Relate:EXCHAMF.Close
    Relate:EXCHPREP.Close
    Relate:GENSHORT.Close
    Relate:LOAN_ALIAS.Close
    Relate:STOAUUSE.Close
    Relate:STOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Exchange_Check_Procedure')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?SuspendAudit
 !check no one else is joined into the audit
 Miss# = 0
 Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
 STOU:AuditType = 'E'
 STOU:Audit_No  = emf:Audit_Number
 Set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
 Loop
     If access:StoAuUse.next() then break.
     if STOU:Audit_No <> emf:Audit_Number then break.
     if STOU:AuditType <> 'E' then break.
     if STOU:User_code = glo:userCode then cycle.     !don't stop at being themselves
     if STOU:Current = 'Y' then
         Miss# = missive('You cannot proceed with this request as user '&clip(STOU:User_code)&' is still recorded as being joined to the audit.',|
                         'ServiceBase 3g','Mstop.jpg','OK')
     END
     
 END !loop
 if miss# then cycle.     !someone was found joined to the audit



    Do ContinueSuspend
    OF ?ButtonFinScan
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020479'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020479'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020479'&'0')
      ***
    OF ?ButtonRefresh
      ThisWindow.Update
      do Refresh_New_Level
    OF ?Exchange_IMEI
      !13194 - J - 20/02/14 - No checking of IMEI on entry - just chuck 'em into a new file
      
      Access:EXCHPREP.primerecord()
      EAP:Audit_Number = emf:Audit_Number
      EAP:IMEI         = Exchange_IMEI
      EAP:User_Code    = glo:userCode
      EAP:Method       = 'MANUAL'
      Access:Exchprep.update()
      
      Exchange_IMEI = ''
      UPDATE()
      DISPLAY()
      SELECT(?Exchange_IMEI)
      
      
      
      !Access:EXCHAUI.Clearkey(eau:Locate_IMEI_Key)
      !eau:Audit_Number    = emf:Audit_Number
      !eau:Site_Location   = Master_Location
      !eau:IMEI_Number     = Exchange_IMEI
      !If Access:EXCHAUI.Tryfetch(eau:Locate_IMEI_Key) = Level:Benign
      !    !Found
      !    !TB12462 - remove checking on type
      !!    IF eau:Stock_Type <> Master_Type
      !!        Case Missive('This unit exists in a different Loan/Stock type. Please ensure it is in the correct location.','ServiceBase 3g',|
      !!                       'mstop.jpg','/OK')
      !!            Of 1 ! OK Button
      !!        End ! Case Missive
      !!    ELSE
      !        !Check to see if the IMEI exists in another location
      !        !and if not, check the Loan Database - L883 (DBH: 12-09-2003)
      !        Error#= 0
      !        Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
      !        xch_ali:ESN = Exchange_IMEI
      !        If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
      !            !Found
      !            If xch_ali:Location <> Master_Location
      !                Case Missive('This unit exists in another location. Please ensure it is in the correct location.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !                Error# = 1
      !            End !If xch_ali:Location <> Master_Location
      !        Else !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
      !            !Error
      !            Access:LOAN_ALIAS.ClearKey(loa_ali:ESN_Only_Key)
      !            loa_ali:ESN = Exchange_IMEI
      !            If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
      !                !Found
      !                Case Missive('This unit exists in the Loan Database. It must be counted as part of a loan audit.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !                Error# = 1
      !            Else !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
      !                !Error
      !            End !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
      !        End !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
      !
      !        If Error# = 0
      !            IF (eau:Confirmed = TRUE)   ! #12276 Don't allow to scan the same IMEI more than once. (Bryan: 10/10/2011)
      !                Beep(Beep:SystemHand)  ;  Yield()
      !                Case Missive('The selected I.M.E.I. Number has already been scanned.','ServiceBase',|
      !                               'mstop.jpg','/&OK') 
      !                Of 1 ! &OK Button
      !                End!Case Message
      !            ELSE ! IF (eau:Confirmed = TRUE)
      !                eau:Confirmed = TRUE
      !                !New_Level += 1  !now dealt with as a count of what is in the queue
      !                Access:EXCHAUI.TryUpdate()
      !            END ! IF (eau:Confirmed = TRUE)
      !        End !If Error# = 0
      !!    END !if matches master type
      !
      !Else! If Access:EXCHAUI.Tryfetch(eau:AuditIMEIKey) = Level:Benign
      !    !Error
      !    Case Missive('This I.M.E.I. number is not recognized in the exchange or loan databases. Do you wish to enter it now?','ServiceBase 3g',|
      !                   'mquest.jpg','\No|/Yes')
      !        Of 2 ! Yes Button
      !        !Check to see if the IMEI exists in another location
      !        !and if not, check the Loan Database - L883 (DBH: 12-09-2003)
      !        Error#= 0
      !        Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
      !        xch_ali:ESN = Exchange_IMEI
      !        If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
      !            !Found
      !            If xch_ali:Location <> Master_Location
      !                Case Missive('This unit exists in another location. Please ensure it is in the correct location.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !                Error# = 1
      !            End !If xch_ali:Location <> Master_Location
      !        Else !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
      !            !Error
      !            Access:LOAN_ALIAS.ClearKey(loa_ali:ESN_Only_Key)
      !            loa_ali:ESN = Exchange_IMEI
      !            If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
      !                !Found
      !                Case Missive('This unit exists in the Loan Database. It must be counted as part of a Loan Audit.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !                Error# = 1
      !            Else !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
      !                !Error
      !            End !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
      !        End !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
      !
      !        If Error# = 0
      !            GlobalRequest = InsertRecord
      !            xch:ESN       = Exchange_IMEI
      !            
      !            Update_Exchange
      !            !Got it? Now add into the list!
      !            !Get it back!
      !            Access:Exchange.ClearKey(xch:ESN_Only_Key)
      !            xch:ESN = Exchange_IMEI
      !            IF Access:Exchange.Fetch(xch:ESN_Only_Key)
      !              !Error!
      !            ELSE
      !              !!New_Level += 1  !now dealt with as a count of what is in the queue New_Level += 1  ! #12276 Only count if the exchange unit is added (Bryan: 10/10/2011)
      !              Access:EXCHAUI.PrimeRecord()
      !              eau:Audit_Number = emf:Audit_Number
      !              eau:Site_Location = xch:Location
      !              eau:Stock_Type = xch:Stock_Type
      !              eau:Ref_Number = xch:Ref_Number
      !              !eau:Existsstoa:Original_Level = sto:Quantity_Stock
      !              eau:Exists = 'N'
      !              eau:Shelf_Location = xch:Shelf_Location
      !              eau:Location = xch:Location
      !              eau:IMEI_Number = xch:ESN
      !              eau:New_IMEI    = True
      !              eau:Confirmed = TRUE
      !              Access:Exchaui.Update()
      !            END
      !        End !If Error# = 0
      !        Of 1 ! No Button
      !    End ! Case Missive
      !
      !End! If Access:EXCHAUI.Tryfetch(eau:AuditIMEIKey) = Level:Benign
      !Exchange_IMEI = ''
      !UPDATE()
      !DISPLAY()
      !SELECT(?Exchange_IMEI)
      BRW1.ResetSort(1)
      !update the display
      !do Refresh_New_Level
          New_Level += 1
          display(?New_level)
    OF ?ButtonImportFile
      ThisWindow.Update
      ImportExcAuditFile(emf:Audit_Number,Master_Location)
      ThisWindow.Reset
      !return from code
      
      do Refresh_New_Level
      
      !update the display
      BRW1.resetsort(1)
      display()
    OF ?Button15:2
      ThisWindow.Update
      !check no one else is joined into the audit
       Miss# = 0
       Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
       STOU:AuditType = 'E'
       STOU:Audit_No  = emf:Audit_Number
       Set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
       Loop
           If access:StoAuUse.next() then break.
           if STOU:Audit_No <> emf:Audit_Number then break.
           if STOU:AuditType <> 'E' then break.
           if STOU:User_code = glo:userCode then cycle.     !don't stop at being themselves
           if STOU:Current = 'Y' then
               Miss# = missive('You cannot proceed with this request as user '&clip(STOU:User_code)&' is still recorded as being joined to the audit.',|
                               'ServiceBase 3g','Mstop.jpg','OK')
           END
           
       END !loop
       if miss# then cycle.     !someone was found joined to the audit
      
      
      IF emf:Complete_Flag = 0
          Case Missive('Do you wish to complete the current stock audit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
      
      
                  Do ProcessPrepFile
      
                  !Lets check if we can!
                  !Anything left?
      
                  !tb12462 - locations are now being ignored
      !            All_Complete# = 0
      !            !Check no locations have been messed with!
      !            all_complete# = 0
      !            Access:ExchAlc.ClearKey(eac:Locate_key)
      !            eac:Audit_Number = emf:Audit_Number
      !            SET(eac:Locate_key,eac:Locate_key)
      !            LOOP
      !              IF Access:ExchAlc.Next()
      !                BREAK
      !              END
      !              IF eac:Audit_Number <> emf:Audit_Number
      !                BREAK
      !              END
      !              IF eac:Confirmed = 0
      !                all_complete# = 1
      !                BREAK
      !              END
      !            END
      !
      !            IF all_complete# = 1
      !              !Not all locations locked down!
      !                Case Missive('There are unconfirmed stock type audits. Please check you have completed the audit correctly.','ServiceBase 3g',|
      !                               'mstop.jpg','/OK')
      !                    Of 1 ! OK Button
      !                End ! Case Missive
      !            ELSE
      
                    !remove bits left over in StoAuUse before they get too big - not needed any more
                    Loop
      
                        Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
                        STOU:AuditType = 'S'
                        STOU:Audit_No  = stom:Audit_No
                        set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
      
                        if access:StoAuUse.next() then break.
                        if STOU:AuditType <> 'S' then break.
                        if STOU:Audit_No  <> stom:Audit_No then break.
      
                        Access:StoAuUse.deleteRecord(0)
      
                    END !loop through StoAuUse
      
                    !Loop audited stock types!
                    Access:ExchAlc.ClearKey(eac:Locate_key)
                    eac:Audit_Number = emf:Audit_Number
                    SET(eac:Locate_key,eac:Locate_key)
                    LOOP
                      IF Access:ExchAlc.Next()
                        BREAK
                      END
                      IF eac:Audit_Number <> emf:Audit_Number
                        BREAK
                      END
                      IF emf:Complete_Flag = 0
                        CYCLE !Shoudl never happen...but you *never* know!
                      END
                      Save_eau_ID = Access:EXCHAUI.SaveFile()
                      Access:EXCHAUI.ClearKey(eau:Main_Browse_Key)
                      eau:Audit_Number   = emf:Audit_Number
                      eau:Confirmed      = 0
                      eau:Site_Location  = Master_Location
                      eau:Stock_Type     = eac:Stock_Type
                      Set(eau:Main_Browse_Key,eau:Main_Browse_Key)
                      Loop
                          If Access:EXCHAUI.NEXT()
                             Break
                          End !If
                          If eau:Audit_Number   <> emf:Audit_Number      |
                          Or eau:Confirmed      <> 0      |
                              Then Break.  ! End If
                          IF eau:Site_Location <> Master_Location
                            BREAK
                          END
                          IF eau:Stock_Type <> eac:Stock_Type
                            BREAK
                          END
                          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                          xch:Ref_Number  = eau:Ref_Number
                          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              !Found
                              xch:Available = 'AUS'
                              xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                              Access:EXCHANGE.Update()
                              get(exchhist,0)
                              if access:exchhist.primerecord() = Level:Benign
                                  exh:ref_number      = xch:ref_number
                                  exh:date            = Today()
                                  exh:time            = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password        = glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status          = 'AUDIT SHORTAGE'
                                  access:exchhist.insert()
                              end!if access:exchhist.primerecord() = Level:Benign
      
                          Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
                      End !Loop     If Access:EXCHAUI.NEXT()
                      Access:EXCHAUI.RestoreFile(Save_eau_ID)
                    End   !loop   IF Access:ExchAlc.Next()
      
                    emf:Complete_Flag = 1
                    emf:Status = 'AUDIT COMPLETED'
                    Access:EXCHAMF.Update()
       
       Exchange_Audit_report((emf:audit_number))
              Of 1 ! No Button
          End ! Case Missive
      ELSE
          Case Missive('The stock audit is already marked as complete.','ServiceBase 3g',|
                         'mexclam.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
      
       POST(Event:CloseWindow)
    OF ?ContinueAudit
      ThisWindow.Update
      Do ContinueSuspend
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    MyExcel8.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          !update the display
          !New_Level = records(Queue:Browse:1)
          do Refresh_New_Level
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!---------------------------------------------------------------------------------
MyExcel8.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
MyExcel8.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
MyExcel8.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  if EAP:User_Code <> glo:usercode then return(Record:filtered).
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue

Update_Stock_Audit PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
Found_Match          BYTE
ActionMessage        CSTRING(40)
List_Query           STRING(30)
Available_Counter    LONG
Stock_Level          LONG
pos                  STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::stoa:Record LIKE(stoa:RECORD),STATIC
QuickWindow          WINDOW('Stock Take Update'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend Stock Take'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Stock Details'),USE(?Tab:1)
                           STRING('PRELIMINARY STOCK CHECK TAKEN'),AT(267,248),USE(?String2),HIDE,FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(228,164),USE(?STO:Internal_Stock_Code:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,164,,10),USE(sto:Part_Number),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Description'),AT(228,184),USE(?STO:Description:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,184,,10),USE(sto:Description),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Shelf Location'),AT(228,204),USE(?STO:Category:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(304,204,,10),USE(sto:Shelf_Location),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           SPIN(@n-14),AT(304,224,56,10),USE(stoa:New_Level),RIGHT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           STRING('Stock Take Level'),AT(228,224),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020492'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Stock_Audit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:STOAUDIT)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stoa:Record,History::stoa:Record)
  SELF.AddHistoryField(?stoa:New_Level,5)
  Relate:GENSHORT.Open
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOAUDIT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Update_Stock_Audit')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:GENSHORT.Close
    Relate:STOAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Stock_Audit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020492'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020492'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020492'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case Missive('Would you like to CONFIRM this quantity, or mark this stock item to be CHECKed again? (Preliminary Check)','ServiceBase 3g',|
                 'mquest.jpg','Check|Confirm')
      Of 2 ! Confirm Button
        STOA:Confirmed = 'Y'
        STOA:Preliminary = ''
      Of 1 ! Check Button
        STOA:Preliminary = 'Y'
        STOA:Confirmed = ''
      ELSE
        Cycle
  End ! Case Missive
  
  IF STOA:Preliminary = ''
    !Nothing Recorded!
    IF STOA:New_Level <> STOA:Original_Level
      IF STOA:New_Level < STOA:Original_Level
        Global_Long = sto:Ref_Number
        !hah!
        GlobalRequest=InsertRecord
        Deplete_Stock_2(STOA:Original_Level-STOA:New_Level)
        IF Global_Long <> 0 THEN
          !sto:Quantity_Stock -= STOA:Original_Level-STOA:New_Level
          !STO:Quantity_Available -= STOA:Original_Level-STOA:New_Level
          !STO:Send = 'Y'
        END !IF
        Access:Stock.Update()
        GlobalRequest=ChangeRecord
        !Add one the general shortage!
        Access:GenShort.PrimeRecord()
        GENS:Audit_No      = STOA:Audit_Ref_No
        GENS:Stock_Ref_No  = sto:Ref_Number
        GENS:Stock_Qty     = (STOA:Original_Level-STOA:New_Level) * -1
        Access:GenShort.Update()
      ELSE
        Global_Long = sto:Ref_Number
        !Global_One_String = STO:Stock_Type
        GlobalRequest=InsertRecord
        Add_Stock_2(STOA:New_Level-STOA:Original_Level)
        IF Global_Long <> 0 THEN
          !sto:Quantity_Stock += STOA:New_Level-STOA:Original_Level
          !STO:Quantity_Available += STOA:New_Level-STOA:Original_Level
          !STO:Send = 'Y'
        END !IF
        Access:Stock.Update()
        GlobalRequest=ChangeRecord
        !Add one the general shortage!
        Access:GenShort.PrimeRecord()
        GENS:Audit_No      = STOA:Audit_Ref_No
        GENS:Stock_Ref_No  = sto:Ref_Number
        GENS:Stock_Qty     = STOA:New_Level-STOA:Original_Level
        Access:GenShort.Update()
      END
    END
  END
  
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      IF Keycode() = ESCKey
        CYCLE
      END
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Add_Stock_2 PROCEDURE (Passed_Quantity)               !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
percentage_mark_up_temp REAL
no_of_labels_temp    REAL
retail_cost_temp     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Add Stock'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(460,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Stock Item'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Add Stock'),USE(?AddStock)
                           PROMPT('Quantity'),AT(234,132),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@p<<<<<<<#p),AT(322,132,64,10),USE(quantity_temp),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY,RANGE(1,99999999)
                           PROMPT('Despatch Note'),AT(234,148),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(322,148,124,10),USE(despatch_note_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Purchase Cost'),AT(234,164),USE(?glo:select3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(322,164,64,10),USE(purchase_cost_Temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Percentage Mark Up'),AT(234,180),USE(?percentage_mark_up_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n7.2),AT(322,180,64,10),USE(percentage_mark_up_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('  %  '),AT(386,180),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Trade Price'),AT(234,196),USE(?glo:select4:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(322,196,64,10),USE(sale_cost_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Retail Price'),AT(234,212),USE(?retail_cost_temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(322,212,64,10),USE(retail_cost_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Date Received'),AT(234,228),USE(?glo:select5:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(322,228,64,10),USE(date_received_temp),SKIP,FONT('Tahoma',,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Additional Notes'),AT(234,244),USE(?Prompt6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(322,244,124,28),USE(additional_notes_temp),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Number Of Labels'),AT(234,276),USE(?no_of_labels_temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n6),AT(322,276,64,10),USE(no_of_labels_temp),DISABLE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,STEP(1)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Hide_Fields     Routine

    If percentage_mark_up_temp <> ''
        ?sale_cost_temp{prop:readonly} = 1
        ?sale_cost_temp{prop:skip} = 1
        ?sale_cost_temp{prop:color} = color:silver
        sale_cost_temp = purchase_cost_temp + (purchase_cost_temp * (percentage_mark_up_temp/100))
    Else
        ?sale_cost_temp{prop:readonly} = 0
        ?sale_cost_temp{prop:skip} = 0
        ?sale_cost_temp{prop:color} = color:white
    End
    Display()

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020487'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Add_Stock_2')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Add_Stock_2')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:STOAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Add_Stock_2')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CancelButton
      GlobalCancelled = true
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020487'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020487'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020487'&'0')
      ***
    OF ?purchase_cost_Temp
      !Do Hide_Fields
    OF ?percentage_mark_up_temp
      !Do Hide_Fields
    OF ?OkButton
      ThisWindow.Update
      finish# = 1
      If date_received_temp = ''
          Select(?date_received_temp)
          finish# = 0
      End
      
      If finish# = 1
          If quantity_temp = ''
              Select(?quantity_temp)
              finish# = 0
          End
      End
      
      If finish# = 1
          If sto:purchase_cost <> purchase_cost_temp Or sto:sale_cost <> sale_cost_temp Or sto:retail_cost <> retail_cost_temp
              Case Missive('Are you sure you want to alter the costs of this stock item?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Of 1 ! No Button
                      purchase_cost_temp = sto:purchase_cost
                      sale_cost_temp = sto:sale_cost
                      retail_cost_temp    = sto:retail_cost
                      percentage_mark_up_temp = sto:percentage_mark_up
                      Do Hide_Fields
                      Display()
              End ! Case Missive
          End
      
          AvPurchaseCost$ = sto:AveragePurchaseCost
          PurchaseCost$ = sto:Purchase_Cost
          SaleCost$ = sto:Sale_Cost
          RetailCost$ = sto:Retail_Cost
      
          sto:purchase_cost           = purchase_cost_temp
          sto:sale_cost               = sale_cost_temp
          sto:retail_cost             = retail_cost_temp
          sto:percentage_mark_up      = percentage_mark_up_temp
          sto:quantity_stock          += quantity_temp
          access:stock.update()
          Set(defaults)
          access:Defaults.next()
          Loop x# = 1 to no_of_labels_temp
              glo:select1 = sto:ref_number
              Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      Stock_Label(Today(),quantity_temp,'N/A',despatch_note_temp,sale_cost_temp)
                  Of 'TEC B-452'
                      Stock_Label_B452(Today(),quantity_temp,'N/A',despatch_note_temp,sale_cost_temp)
              End!Case def:label_printer_type
          End!Loop x# = 1 to no_of_labels_temp
      
          glo:select1 = ''
          If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                             'ADD', | ! Transaction_Type
                             'STOCK AUDIT ' & Clip(stoa:Audit_Ref_No), | ! Depatch_Note_Number
                             0, | ! Job_Number
                             0, | ! Sales_Number
                             Quantity_Temp, | ! Quantity
                             Purchase_Cost_Temp, | ! Purchase_Cost
                             Sale_Cost_Temp, | ! Sale_Cost
                             Retail_Cost_Temp, | ! Retail_Cost
                             'STOCK ADDED', | ! Notes
                             Clip(Additional_Notes_Temp),|
                             AvPurchaseCost$,|
                             PurchaseCost$,|
                             SaleCost$,|
                             RetailCost$)
            ! Added OK
          Else ! AddToStockHistory
            ! Error
          End ! AddToStockHistory
      
          Post(Event:CloseWindow)
      End
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      IF Keycode() = ESCKey
        CYCLE
      END
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      quantity_temp = Passed_Quantity
      
      !access:stock.clearkey(sto:ref_number_key)
      !sto:ref_number  = glo:select1
      !If access:stock.fetch(sto:ref_number_key)
      !    Return Level:Fatal
      !End
      
      date_received_temp = Today()
      purchase_cost_Temp = STO:Purchase_Cost
      sale_cost_temp = STO:Sale_Cost
      retail_cost_temp    = sto:Retail_cost
      percentage_mark_up_temp = STO:Percentage_Mark_Up
      despatch_note_temp = 'STOCK AUDIT'
      
      !Do Hide_Fields
      !Labels
      set(defaults)
      access:defaults.next()
      If def:receive_stock_label = 'YES'
          Enable(?no_of_labels_temp)
          no_of_labels_temp = 1
      Else!If def:receive_stock_label = 'YES'
          Disable(?no_of_labels_temp)
          no_of_labels_temp = 0
      End!If def:receive_stock_label = 'YES'
      Display()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Deplete_Stock_2 PROCEDURE (Passed_Quantity)           !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Decrement Stock Level'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(380,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Decrement Stock Level'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Decrement Stock Level'),USE(?Tab1)
                           PROMPT('Quantity'),AT(248,174),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@p<<<<<<<#p),AT(324,174,64,10),USE(quantity_temp),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY,RANGE(1,99999999)
                           PROMPT('Date'),AT(248,192),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(324,194,64,10),USE(date_received_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Additional Notes'),AT(248,208),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(248,220,184,32),USE(additional_notes_temp),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020489'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Deplete_Stock_2')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Deplete_Stock_2')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Deplete_Stock_2')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      If date_received_temp = ''
          Select(?date_received_temp)
      Else
          if quantity_temp > sto:quantity_stock
              Case Missive('You cannot decrement the stock level below zero.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              quantity_temp = ''
              Select(?quantity_temp)
          Else
              sto:quantity_stock -= quantity_temp
              if sto:quantity_stock < 0
                  sto:quantity_stock = 0
              End
              access:stock.update()
      
              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                 'DEC', | ! Transaction_Type
                                 'STOCK AUDIT ' & Clip(stoa:Audit_Ref_No), | ! Depatch_Note_Number
                                 0, | ! Job_Number
                                 0, | ! Sales_Number
                                 Quantity_Temp, | ! Quantity
                                 sto:Purchase_Cost, | ! Purchase_Cost
                                 sto:Sale_Cost, | ! Sale_Cost
                                 sto:Retail_Cost, | ! Retail_Cost
                                 'STOCK DECREMENTED', | ! Notes
                                 Clip(Additional_Notes_Temp)) ! Information
                ! Added OK
              Else ! AddToStockHistory
                ! Error
              End ! AddToStockHistory
      
              Post(Event:CloseWindow)
          End
      End
    OF ?CancelButton
      GlobalCancelled = true
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020489'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020489'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020489'&'0')
      ***
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      IF Keycode() = ESCKey
        CYCLE
      END
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Quantity_Temp = Passed_Quantity
      
      !access:stock.clearkey(sto:ref_number_key)
      !sto:ref_number = glo:select1
      !If access:stock.fetch(sto:ref_number_key)
      !    Return Level:Fatal
      !end
      
      date_received_temp = Today()
      DISPLAY()
      UPDATE()
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseExcAudit PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Yes_Byte             STRING('Y')
save_eau_id          USHORT,AUTO
Status_Info          STRING(20)
NoLogError           BYTE
FoundUser            BYTE
BRW1::View:Browse    VIEW(EXCHAMF)
                       PROJECT(emf:Audit_Number)
                       PROJECT(emf:Date)
                       PROJECT(emf:User)
                       PROJECT(emf:Status)
                       PROJECT(emf:Site_location)
                       PROJECT(emf:Complete_Flag)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
emf:Audit_Number       LIKE(emf:Audit_Number)         !List box control field - type derived from field
emf:Date               LIKE(emf:Date)                 !List box control field - type derived from field
emf:User               LIKE(emf:User)                 !List box control field - type derived from field
emf:Status             LIKE(emf:Status)               !List box control field - type derived from field
emf:Site_location      LIKE(emf:Site_location)        !List box control field - type derived from field
emf:Complete_Flag      LIKE(emf:Complete_Flag)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::emf:Complete_Flag    LIKE(emf:Complete_Flag)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse Exchange Audits'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Exchange Audits'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,114,272,210),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('56L(2)|M~Audit Number~@n-14@49L(2)|M~Date~@d17@29L(2)|M~User~@s3@120L(2)|M~Statu' &|
   's~@s30@0L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,140),USE(?ButtonNewAudit),TRN,FLAT,LEFT,ICON('newaud2p.jpg')
                       BUTTON,AT(446,114),USE(?ButtonExport),TRN,FLAT,ICON('genrepp.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Exchange Audit Table'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14),AT(168,100,64,10),USE(emf:Audit_Number),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(446,270),USE(?ButtonJoinAudit),TRN,FLAT,ICON('joinaudp.jpg')
                           BUTTON,AT(448,298),USE(?ButtonContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                         END
                         TAB('Completed Audits'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(448,297),USE(?ButtonReprintAudit),TRN,FLAT,LEFT,ICON('prnaudp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!CheckEntry and LogOutUser  routines

CheckEntry          Routine

    !reset defaults
    NoLogError = true
    FoundUser = false

    !are they already joined on this audit
    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
    STOU:AuditType = 'E'
    STOU:Audit_No  = stom:Audit_No
    STOU:User_code = glo:userCode
    set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
    Loop

        If access:StoAuUse.next() then break.
        If STOU:AuditType <> 'E' then break.
        If STOU:Audit_No  <> stom:Audit_No then break.
        if STOU:User_code <> glo:userCode then break.

        !ok got a matching record
        FoundUser = true

        !is this one active
        if STOU:Current = 'Y' then
            IF Missive('Records show that you are already currently working within this audit. |'&|
                       'If you are using a "shared" log in then you are warned that this will cause problems, and will render this audit worthless.|'&|
                       'Are you sure you want to continue and re-join this audit?','ServiceBase 3g Warning','mstop.jpg','\YES|/NO') = 2 then
                NoLogError = false
                EXIT
            ELSE
                Break   !got an answer to one question that will do
            END !if missive = NO
        END
    END !loop through StoAuUse by typeNoUser

    if foundUser then
        !set all records to active
        Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
        STOU:AuditType = 'E'
        STOU:Audit_No  = stom:Audit_No
        STOU:User_code = glo:userCode
        set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
        Loop

            If access:StoAuUse.next() then break.
            If STOU:AuditType <> 'E' then break.
            If STOU:Audit_No  <> stom:Audit_No then break.
            if STOU:User_code <> glo:userCode then break.

            STOU:Current = 'Y'
            Access:StoAuUse.update()
        END !loop through all StoAuUse
    ELSE
        !first time in
        Access:StoAuUse.primerecord()
        STOU:AuditType       = 'E'
        STOU:Audit_No        = Stom:Audit_no
        STOU:User_code       = glo:UserCode
        STOU:Current         = 'Y'
        STOU:StockPartNumber = 'PRIMARY LOG-IN'
        Access:StoAuUse.update()
    END


    !check for someone else currently on the system
    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
    STOU:AuditType  = 'E'
    STOU:Audit_No   = Stom:Audit_no
    set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
    loop
        if access:StoAuUse.next() then break.
        if STOU:AuditType  <> 'E' then break.
        if STOU:Audit_No   <> Stom:Audit_no then break.

        if STOU:Current = 'Y' and STOU:User_code <> glo:userCode then
            !not the current log in - but someone else is actively scanning
            Miss# = Missive('Records show that other people are already currently joined in with this audit. |'&|
                       'You should be aware of this and ensure that you are not auditing the same items that have already been scanned',|
                       'ServiceBase 3g Warning','mstop.jpg','OK') 
            BREAK
        END
    END !loop through audit users

    EXIT


LogOutUser          Routine

    !set all records to inactive
    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
    STOU:AuditType = 'E'
    STOU:Audit_No  = stom:Audit_No
    STOU:User_code = glo:userCode
    set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
    Loop

        If access:StoAuUse.next() then break.
        If STOU:AuditType <> 'E' then break.
        If STOU:Audit_No  <> stom:Audit_No then break.
        if STOU:User_code <> glo:userCode then break.

        STOU:Current = 'N'
        Access:StoAuUse.update()

    END !loop through all StoAuUse records for this user

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020477'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseExcAudit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:EXCEXCH.Open
  Relate:EXCHAMF.Open
  Relate:STOAUUSE.Open
  Relate:USERS.Open
  Access:EXCHAUI.UseFile
  Access:EXCHANGE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCHAMF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      glo:userCode = use:User_Code
      if Glo:WebJob = 0
          glo:Default_Site_Location = use:Location
      END
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      ! Save Window Name
   AddToLog('Window','Open','BrowseExcAudit')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,emf:Secondary_Key)
  BRW1.AddRange(emf:Complete_Flag)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,emf:Audit_Number,1,BRW1)
  BRW1.AddSortOrder(,emf:Secondary_Key)
  BRW1.AddRange(emf:Complete_Flag)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?emf:Audit_Number,emf:Audit_Number,1,BRW1)
  BRW1.AddField(emf:Audit_Number,BRW1.Q.emf:Audit_Number)
  BRW1.AddField(emf:Date,BRW1.Q.emf:Date)
  BRW1.AddField(emf:User,BRW1.Q.emf:User)
  BRW1.AddField(emf:Status,BRW1.Q.emf:Status)
  BRW1.AddField(emf:Site_location,BRW1.Q.emf:Site_location)
  BRW1.AddField(emf:Complete_Flag,BRW1.Q.emf:Complete_Flag)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:EXCEXCH.Close
    Relate:EXCHAMF.Close
    Relate:STOAUUSE.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseExcAudit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonNewAudit
      ! Create Header Record
      IF (Access:EXCHAMF.PrimeRecord() = Level:Benign)
          emf:Date = Today()
          emf:Time = Clock()
          emf:User = glo:UserCode
          
          emf:Complete_Flag = 0
          emf:Status = 'AUDIT IN PROGRESS'
          ! Use engineer's Location
          emf:Site_Location = use:Location
          If (Access:EXCHAMF.TryUpdate())
              Access:EXCHAMF.CancelAutoInc()
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('Unable to create an Exchange Audit Entry.','ServiceBase',|
                             Icon:Hand,'&OK',1)
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END ! If (Access:EXCHAMF.TryUpdate())
      ELSE ! IF (Access:EXCHAMF.PrimeRecord() = Level:Benign)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('Unable to create an Exchange Audit Entry.','ServiceBase',|
                         Icon:Hand,'&OK',1)
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END ! IF (Access:EXCHAMF.PrimeRecord() = Level:Benign)
      
      ! Add a quick record count
      
      ! #13150 Count records (hopefully won't take too long) (DBH: 29/08/2013)
      IF (glo:WebJob = 0)
          ! Not an RRC problem, so far, so no need for progress bar
          SETCURSOR(CURSOR:WAIT)
          countRecords# = 0
          Access:Exchange.ClearKey(xch:AvailLocRef)
          xch:Available = 'AVL'
          xch:Location = use:Location
          SET(xch:AvailLocRef,xch:AvailLocRef)
          LOOP UNTIL Access:Exchange.Next() <> Level:Benign
              IF (xch:Available <> 'AVL' OR |
                  xch:Location <> use:Location)
                  BREAK
              END ! IF
              countRecords# += 1
          END ! LOOP
          SETCURSOR()
      END ! IF
      
      prog.ProgressSetup(countRecords#)
      prog.ProgressText('Building List Of Units...')
      
      pressedBreak# = 0
      
      !Now loop through and make the stock ready for audit!
      Access:Exchange.ClearKey(xch:AvailLocRef)
      xch:Available = 'AVL'
      xch:Location = use:Location
      SET(xch:AvailLocRef,xch:AvailLocRef)
      LOOP
          IF Access:Exchange.Next()
              BREAK
          END
      
          IF xch:Available <> 'AVL'
              BREAK
          END
      
          IF xch:Location <> use:Location
              BREAK
          END
      
          IF (prog.InsideLoop())
              pressedBreak# = 1
              BREAK
          END ! IF
      
          !TBN12462 - removed the reliance on excluded types
      !    Access:excexch.ClearKey(eix1:StatusTypeKey) ! WIP Status exclusions
      !    eix1:Location = emf:Site_location
      !    eix1:Status = xch:Stock_Type
      !    if Access:excexch.Fetch(eix1:StatusTypeKey)
      !      !Error!
      !    ELSE
      !        CYCLE
      !    end
      
          IF glo:Default_Site_Location <> ''
              IF xch:Location <> use:Location
                  CYCLE
              END
          END
      
          Access:Exchaui.PrimeRecord()
          eau:Audit_Number = emf:Audit_Number
          eau:Site_Location = xch:Location
          eau:Ref_Number = xch:Ref_Number
          !eau:Existsstoa:Original_Level = sto:Quantity_Stock
          eau:Exists = 'N'
          eau:Stock_Type = xch:Stock_Type
          eau:Shelf_Location = xch:Shelf_Location
          eau:Location = xch:Location
          eau:New_IMEI    = FALSE
          eau:IMEI_Number = xch:ESN
      
          Access:Exchaui.Update()
      END
      
      
      prog.ProgressFinish()
      
      IF (pressedBreak# = 1)
          ! #13150 Don't carry on if cancelled process (DBH: 29/08/2013)
          CYCLE
      END ! IF
      
      Exchange_Check_Procedure()
      BRW1.REsetSort(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020477'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020477'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020477'&'0')
      ***
    OF ?ButtonExport
      ThisWindow.Update
      AvailableExchangeUnitsReport
      ThisWindow.Reset
    OF ?ButtonJoinAudit
      ThisWindow.Update
      ThisWindow.Update()
      
      
      if emf:Complete_Flag = true
          Miss# = missive('You cannot join this audit. This audit is already completed','ServiceBase 3g','mstop.jpg','OK')
          cycle
      END
      
      if emf:user = glo:userCode then
          miss# = missive('You cannot join this audit - you started it. Use the [Continue Audit] button','ServiceBase 3g','mstop.jpg','OK')
          cycle
      END
      
      
      if missive('Are you sure you want to be added as a secondary scanner to assist in scanning for this audit?',|
                 'ServiceBase 3g','mquest.jpg','/YES|\NO') =2  then cycle.
      
      !CheckEntry and LogOutUser  routines
      do CheckEntry  
      
      if NoLogError
      
          !call the scanning screen
          Exchange_Check_Procedure
      
          !after the audit you need to record the leaving
          do LogOutUser
      
      END !if NoLogError
      
      BRW1.ResetSort(1)
      
    OF ?ButtonContinueAudit
      ThisWindow.Update
      ThisWindow.Update()
      
      !message('Sending: Got emp:Audit_number '&clip(emf:Audit_Number))
      if emf:User <> glo:userCode then
          miss# = missive('You cannot continue this audit - you did not start it. Use the [Join Audit] button','ServiceBase 3g','mstop.jpg','OK')
          cycle
      END
      
      
      IF emf:Complete_Flag = 0
      
          !CheckEntry and LogOutUser  routines
          do CheckEntry  
      
          if NoLogError
      
              !call the scanning screen
              Exchange_Check_Procedure
      
              !after the audit you need to record the leaving
              do LogOutUser
      
          END !if NoLogError
      
      ELSE
          Case Missive('This stock audit is completed. Please start a new audit, or select an incomplete one.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
      
      BRW1.ResetSort(1)
    OF ?ButtonReprintAudit
      ThisWindow.Update
      !IF emf:Complete_Flag = 0
      !    !Cannot continue!
      !    Case Missive('This audit is not complete. You cannot reprint the audit.','ServiceBase 3g',|
      !                   'mstop.jpg','/OK')
      !        Of 1 ! OK Button
      !    End ! Case Missive
      !ELSE
       Exchange_Audit_report((emf:audit_number))
          ThisWindow.Update()
      !END
      BRW1.UpdateBuffer()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF CHOICE(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  IF glo:Default_Site_Location <> ''
    IF emf:Site_location <> glo:Default_Site_Location
      RETURN Record:Filtered
    ELSE
      RETURN Record:OK
    END
  END
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 100
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Income_report_Criteria2 PROCEDURE                     !Generated from procedure template - Window

FilesOpened          BYTE
tag_temp             STRING(1)
trade_account_temp   BYTE(0)
TempLong             LONG
manfuacturer_temp    STRING(30)
single_page_temp     STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccountType      BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
window               WINDOW('Income Report Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Income Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Exchange Units In Channel Criteria'),USE(?Tab1)
                           OPTION('Trade Account'),AT(215,134,111,28),USE(trade_account_temp),DISABLE,BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(226,147),USE(?trade_account_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Individual'),AT(266,147),USE(?trade_account_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           ENTRY(@s40),AT(342,166,124,10),USE(GLO:Select6),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           OPTION,AT(214,158,20,32),USE(tmp:AccountType)
                             RADIO,AT(218,166),USE(?Option3:Radio1),VALUE('0')
                             RADIO,AT(218,179),USE(?Option3:Radio2),VALUE('1')
                           END
                           PROMPT('Header Account Number'),AT(238,166),USE(?HeadAccountNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Sub Account Number'),AT(238,178),USE(?SubAccountNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s40),AT(342,178,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('60L(2)|M@s15@120L(2)|M@s30@'),DROP(10,160),FROM(Queue:FileDropCombo)
                           OPTION('Invoice Types To Include'),AT(214,202,236,48),USE(?Option2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           CHECK('Service Invoices'),AT(314,214),USE(GLO:Select4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Retail Invoices'),AT(314,230),USE(GLO:Select5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Start Date'),AT(214,258),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(314,258,64,10),USE(GLO:Select1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('End Date'),AT(214,274),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(314,274,64,10),USE(GLO:Select2),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

show_hide       Routine
    Case trade_account_temp
        Of 0
            ?tmp:AccountType{prop:Disable} = 1
            ?HeadAccountNumber:Prompt{prop:Disable} = 1
            ?SubAccountNumber:Prompt{prop:Disable} = 1
            ?glo:Select3{prop:Disable} = 1
            ?glo:Select6{prop:Disable} = 1
        Of 1
            ?tmp:AccountType{prop:Disable} = 0
            Post(Event:Accepted,?tmp:AccountType)
    End!Case trade_account_temp
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020498'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Income_report_Criteria2')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SUBTRACC.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  glo:select1 = Deformat('1/1/1998',@d6)
  glo:select2  = Today()
  glo:select3 = ''
  glo:select4 = 'YES'
  glo:select5 = 'YES'
  glo:Select6 = ClarioNET:Global.Param2
  g_acc_no    = ClarioNET:Global.Param2
  trade_account_temp = 1
  DISPLAY()
  UPDATE()
      ! Save Window Name
   AddToLog('Window','Open','Income_report_Criteria2')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB2.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(sub:Main_Account_Key)
  FDCB2.AddRange(sub:Main_Account_Number,g_acc_no)
  FDCB2.AddField(sub:Account_Number,FDCB2.Q.sub:Account_Number)
  FDCB2.AddField(sub:Company_Name,FDCB2.Q.sub:Company_Name)
  FDCB2.AddField(sub:RecordNumber,FDCB2.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBTRACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Income_report_Criteria2')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020498'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020498'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020498'&'0')
      ***
    OF ?trade_account_temp
      Do show_hide
    OF ?tmp:AccountType
      Case tmp:AccountType
          Of 0
              ?HeadAccountNumber:Prompt{prop:Disable} = 0
              ?SubAccountNumber:Prompt{prop:Disable} = 1
              ?glo:Select6{prop:Disable} = 0
              ?glo:Select3{prop:Disable} = 1
      
          Of 1
              ?HeadAccountNumber:Prompt{prop:Disable} = 1
              ?SubAccountNumber:Prompt{prop:Disable} = 0
              ?glo:Select6{prop:Disable} = 1
              ?glo:Select3{prop:Disable} = 0
      
      End !tmp:AccountType
    OF ?OkButton
      ThisWindow.Update
      error# = 0
      If glo:select4 = 'NO' And glo:select5 = 'NO'
          Case Missive('You must select at least one Invoice Type.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
          Error# = 1
      End!If glo:select4 = 'NO' And glo:select5 = 'NO'
      IF error# = 0
          Income_Report(Trade_Account_Temp,tmp:AccountType)
      End!IF error# = 0
    OF ?CancelButton
      ThisWindow.Update
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select4 = ''
      glo:select5 = ''
      Post(event:Closewindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Do show_hide
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
