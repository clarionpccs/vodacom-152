

   MEMBER('sbj03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ03003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBJ03002.INC'),ONCE        !Req'd for module callout resolution
                     END


Stock_Check_Procedure PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
Stock_Query          STRING(30)
LOC:Amount           DECIMAL(15,2)
No_Find              BYTE
BrLocator1           STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Master_Location      STRING(30)
Variance             REAL
Temp_Cut_String      STRING(10)
New_level            STRING(4)
tmp:Filtered_Manufacturer STRING(30)
tmp:part_number      STRING(30)
ScanningField        STRING(30)
UserOK               BYTE
BrowseRefresh        BYTE
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Master_Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:Filtered_Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOAUDIT)
                       PROJECT(stoa:Original_Level)
                       PROJECT(stoa:New_Level)
                       PROJECT(stoa:Shelf_Location)
                       PROJECT(stoa:Second_Location)
                       PROJECT(stoa:Internal_AutoNumber)
                       PROJECT(stoa:Audit_Ref_No)
                       PROJECT(stoa:Confirmed)
                       PROJECT(stoa:Site_Location)
                       PROJECT(stoa:Stock_Ref_No)
                       JOIN(sto:Ref_Number_Key,stoa:Stock_Ref_No)
                         PROJECT(sto:Description)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Supplier)
                         PROJECT(sto:Shelf_Location)
                         PROJECT(sto:Location)
                         PROJECT(sto:Second_Location)
                         PROJECT(sto:Manufacturer)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
stoa:Original_Level    LIKE(stoa:Original_Level)      !List box control field - type derived from field
stoa:New_Level         LIKE(stoa:New_Level)           !List box control field - type derived from field
stoa:Shelf_Location    LIKE(stoa:Shelf_Location)      !List box control field - type derived from field
stoa:Second_Location   LIKE(stoa:Second_Location)     !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
stoa:Internal_AutoNumber LIKE(stoa:Internal_AutoNumber) !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
sto:Supplier           LIKE(sto:Supplier)             !Browse hot field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !Browse hot field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse hot field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !Browse hot field - type derived from field
sto:Manufacturer       LIKE(sto:Manufacturer)         !Browse hot field - type derived from field
stoa:Audit_Ref_No      LIKE(stoa:Audit_Ref_No)        !Browse key field - type derived from field
stoa:Confirmed         LIKE(stoa:Confirmed)           !Browse key field - type derived from field
stoa:Site_Location     LIKE(stoa:Site_Location)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK8::stoa:Audit_Ref_No    LIKE(stoa:Audit_Ref_No)
HK8::stoa:Confirmed       LIKE(stoa:Confirmed)
HK8::stoa:Site_Location   LIKE(stoa:Site_Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB4::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB13::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
QuickWindow          WINDOW('Browse Stock '),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Audit Procedure'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(548,366),USE(?ContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                       BUTTON,AT(316,366),USE(?ButtonFinScan),TRN,FLAT,HIDE,ICON('finscanp.jpg')
                       BUTTON,AT(177,366),USE(?ButtonImportFile),TRN,FLAT,HIDE,RIGHT,ICON('ImpFileP.jpg'),DEFAULT
                       SHEET,AT(64,54,284,310),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Stock Audit Procedure'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING('AUDIT IN PROGRESS'),AT(100,88,212,15),USE(?String8),TRN,CENTER,FONT(,16,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING('Site Location'),AT(108,112),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(72,138,268,16),USE(sto:Description),TRN,CENTER,FONT(,14,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(72,151,268,16),USE(sto:Part_Number),TRN,CENTER,FONT(,14,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(72,164,268,16),USE(sto:Shelf_Location),TRN,CENTER,FONT(,14,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(320,264),USE(?Button0),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key0),ALRT(Key0),ICON('0p.jpg')
                           STRING(@s30),AT(72,177,268,16),USE(sto:Second_Location),TRN,CENTER,FONT(,14,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PANEL,AT(256,197,90,60),USE(?Panel1:3),FILL(COLOR:Red)
                           STRING(@n4),AT(60,208,103,30),USE(stoa:Original_Level),TRN,CENTER,FONT(,28,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING(@n4),AT(148,208,103,30),USE(New_level),TRN,CENTER,FONT(,28,039262EH,FONT:bold,CHARSET:ANSI)
                           STRING(@n-5),AT(244,208),USE(Variance),TRN,CENTER,FONT(,28,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING('VARIANCE'),AT(267,240,68,12),USE(?String7),TRN,CENTER,FONT(,,COLOR:White,FONT:bold)
                           STRING('IN STOCK'),AT(79,240,68,12),USE(?String5),TRN,CENTER,FONT(,,COLOR:White,FONT:bold)
                           STRING('COUNTED'),AT(173,240,68,12),USE(?String6),TRN,CENTER,FONT(,,039262EH,FONT:bold)
                           BUTTON,AT(104,288),USE(?BackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                           BUTTON,AT(176,288),USE(?ClearButton),TRN,FLAT,ICON('clearp.jpg')
                           BUTTON,AT(252,288),USE(?NextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                           COMBO(@s30),AT(168,112,124,10),USE(Master_Location),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           PROMPT('Manufacturer'),AT(108,126),USE(?Manufacturer:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(168,126,124,10),USE(tmp:Filtered_Manufacturer),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|@s30@'),DROP(5,124),FROM(Queue:FileDropCombo:1)
                           PANEL,AT(68,198,90,60),USE(?Panel1),FILL(COLOR:Green)
                           PANEL,AT(162,198,90,60),USE(?Panel1:2),FILL(COLOR:Yellow)
                           BUTTON,AT(68,264),USE(?Button1),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key1),ALRT(Key1),ICON('1p.jpg')
                           BUTTON,AT(96,264),USE(?Button2),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key2),ALRT(Key2),ICON('2p.jpg')
                           BUTTON,AT(124,264),USE(?Button3),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key3),ALRT(Key3),ICON('3p.jpg')
                           BUTTON,AT(152,264),USE(?Button4),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key4),ALRT(Key4),ICON('4p.jpg')
                           BUTTON,AT(180,264),USE(?Button5),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key5),ALRT(Key5),ICON('5p.jpg')
                           BUTTON,AT(208,264),USE(?Button6),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key6),ALRT(Key6),ICON('6p.jpg')
                           BUTTON,AT(236,264),USE(?Button7),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key7),ALRT(Key7),ICON('7p.jpg')
                           BUTTON,AT(264,264),USE(?Button8),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key8),ALRT(Key8),ICON('8p.jpg')
                           BUTTON,AT(292,264),USE(?Button9),TRN,FLAT,FONT(,,,FONT:bold),KEY(Key9),ALRT(Key9),ICON('9p.jpg')
                         END
                         TAB('Stock Audit Scanning'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Stock Part Number'),AT(101,140),USE(?Prompt4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(101,152,219,11),USE(ScanningField),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                         END
                       END
                       SHEET,AT(352,54,264,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Awaiting Audit'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Audited Parts'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(369,338,243,22),USE(?GroupUpdateButtons),HIDE
                             BUTTON,AT(392,336),USE(?ButtonIncStock),SKIP,FLAT,ICON('IncStock.jpg')
                             BUTTON,AT(470,336),USE(?ButtonDecStock),SKIP,FLAT,ICON('DecStock.jpg')
                             BUTTON,AT(546,336),USE(?ButtonChQty),SKIP,FLAT,ICON('ChAuQty.jpg')
                           END
                         END
                       END
                       LIST,AT(358,72,252,258),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),VCR,FORMAT('90L(2)|FM~Description~@s30@91L(2)|M~Part Number~@s30@0R(4)|M@n6@0R(4)|M@n6@100L(' &|
   '2)|M~Shelf Location~@s30@100L(2)|M~Second Location~@s30@0D(2)|M~Quantity In Stoc' &|
   'k~L@N8@0L(2)|M~Internal Auto Number~@n-14@32L(2)|M~Reference Number~@s8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,366),USE(?SuspendAudit),TRN,FLAT,LEFT,ICON('susaudp.jpg')
                       BUTTON('Close'),AT(20,400,76,20),USE(?Close),HIDE,LEFT,ICON('Cancel.ico')
                       BUTTON,AT(392,366),USE(?ButtonTempComplete),FLAT,HIDE,ICON('TempComp.jpg')
                       BUTTON,AT(472,366),USE(?ButtonCompleteAudit),TRN,FLAT,LEFT,ICON('compaudp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB13               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

CompleteScannedAudit        Routine

    !Need to mark the audit items as confirmed even if none have been scanned
    !Update the general shortage file to show what is missing

    !if something has been scanned (even just one item) this will already have been done.

    Access:StoAudit.clearkey(stoa:Audit_Ref_No_Key)
    stoa:Audit_Ref_No  = stom:Audit_No
    Set(stoa:Audit_Ref_No_Key,stoa:Audit_Ref_No_Key)
    loop

        if access:StoAudit.next() then break.
        if stoa:Audit_Ref_No  <> stom:Audit_No then break.

        if stoa:Confirmed <> 'Y' then
            !update it
            stoa:Confirmed = 'Y'
            Access:StoAudit.update()

            !General shortage needs to be kept up to date
            if STOA:New_Level = STOA:Original_Level
                !if stock levels are the same this can be deleted
                Access:Genshort.clearkey(gens:Lock_Down_Key)
                gens:Audit_No      = STOA:Audit_Ref_No
                gens:Stock_Ref_No  = stoa:Stock_Ref_No
                If access:GenShort.fetch(gens:Lock_Down_Key)
                    !not found - OK = probably 1 = 1
                ELSE
                    Access:GenShort.deleteRecord()
                END

            ELSE

                !stock levels are different - record this
                Access:Genshort.clearkey(gens:Lock_Down_Key)
                gens:Audit_No      = STOA:Audit_Ref_No
                gens:Stock_Ref_No  = stoa:Stock_Ref_No
                If access:GenShort.fetch(gens:Lock_Down_Key)
                    !new record
                    Access:GenShort.primerecord()
                    gens:Audit_No     = STOA:Audit_Ref_No
                    gens:Stock_Ref_No = stoa:Stock_Ref_No
                END !if new record needed
                GENS:Stock_Qty     = STOA:New_Level - STOA:Original_Level
                Access:GenShort.Update()

            END !if stock levels match
        END !if not confirmed
    END !loop

    EXIT
SuspendContinue  Routine

    IF Stom:Send = ''
        stom:Send = 'S'
        Access:STMASAUD.Update()
        Post(Event:CloseWindow)
    ELSE
        Stom:Send = ''
        Access:STMASAUD.Update()
        ?String8{PROP:Text} = 'AUDIT IN PROGRESS'
        ?SuspendAudit{prop:Hide} = 0
        ?ContinueAudit{prop:Hide} = 1
        !ENABLE(?Master_Location)
        if stom:CompleteType = 'S' then
            unhide(?ButtonTempComplete)
            enable(?ScanningField)
            unhide(?GroupUpdateButtons)
        END
    END

    if glo:Webjob = 0  then
        unhide(?ButtonImportFile)
    END


    EXIT

WORKOUT     ROUTINE
    !need to look back at live stock, and readjust if necessary!
    !Temp!
    tmp:part_number = sto:part_number
    Access:Stock.ClearKey(sto:Location_Key)
    sto:Location = stoa:Site_Location
    sto:Part_Number = tmp:part_number
    IF Access:Stock.Fetch(sto:Location_Key)
      !Error!
      !mESSAGE('not here')
    ELSE
      !MESSAGE('here')
      !Get STOA record!
      Access:StoAudit.ClearKey(stoa:Internal_AutoNumber_Key)
      stoa:Internal_AutoNumber = BRW1.Q.stoa:Internal_AutoNumber
      Access:StoAudit.Fetch(stoa:Internal_AutoNumber_Key)
      IF sto:Quantity_Stock <> stoa:Original_Level
        stoa:Original_Level = sto:Quantity_Stock
        BRW1.Q.stoa:Original_Level = sto:Quantity_Stock
        IF Access:StoAudit.Update()
          !MESSAGE('error')
        END
        BRW1.ResetQueue(Reset:Queue)
      END
      !need this if the new_level has been preset, but not if a button press or number press
      if BrowseRefresh = true then New_Level = stoa:New_Level.
    END
    Variance = New_level - stoa:Original_Level
    BrowseRefresh = false
    UPDATE()
    DISPLAY()
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020490'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Check_Procedure')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:GENSHORT.Open
  Relate:LOCATION.Open
  Relate:STOAUUSE.Open
  Access:STMASAUD.UseFile
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOAUDIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if stom:Audit_User <> glo:userCode then
      !this must be a joining user - must ignore suspended status
      !hide most buttons but show new [FINISH SCAN] button
      ?String8{PROP:Text} = 'AUDIT JOINED'
      ?ContinueAudit{prop:Hide} = 1
      ?SuspendAudit{prop:Hide} = 1
      ?ButtonFinScan{prop:hide} = 0
      ?buttonCompleteAudit{prop:hide}=1
  
      enable(?ScanningField)
  
  ELSE
  
      case stom:Send
          of 'S'
              ?String8{PROP:Text} = 'AUDIT SUSPENDED'
              ?ContinueAudit{prop:Hide} = 0
              ?SuspendAudit{prop:Hide} = 1
  
              DISABLE(?Master_Location)
              disable(?ScanningField)
  
          of 'T'
              ?String8{PROP:Text} = 'TEMP COMPLETED'
              ?ContinueAudit{prop:Hide} = 1
              ?SuspendAudit{prop:Hide} = 1
  
              DISABLE(?Master_Location)
              disable(?ScanningField)
              select(?Tab2)
  
          ELSE
  
              ?String8{PROP:Text} = 'AUDIT IN PROGRESS'
              ?ContinueAudit{prop:Hide} = 1
              ?SuspendAudit{prop:Hide} = 0
  
              ENABLE(?Master_Location)
              enable(?ScanningField)
  
      END
  END !if user codes don't match
  
  Master_Location = stom:branch
  
  if glo:WebJob = 1
      !Master_Location = glo:Default_Site_Location
      DISABLE(?Master_Location) ! Disable the location - we have only processed stock records for this location
  ELSE
      !can only iport files through non-clarionet
      unhide(?ButtonImportFile)
  end !If glo:WebJob = 1
  
  if stom:CompleteType = '' then
      if missive('How do you intend to complete this audit? By scanning the items, or by manually entering stock quanities?|Note: Once you have chosen a method you cannot change it.',|
                 'ServiceBase 3g','mquest.jpg','SCAN|MANUAL') = 1 then
          stom:CompleteType = 'S'
      ELSE
          stom:CompleteType = 'M'
      END
      Access:Stmasaud.update()
  END
  
  !change the screen appearance
  If stom:CompleteType = 'S' then   !scanning mode
      
      if stom:Send <> 'S' and stom:Audit_User = glo:userCode then
          !not suspended and this is the originating user
          unhide(?GroupUpdateButtons)
          if glo:Webjob = 0   !can only do this direct mode
              unhide(?ButtonTempComplete)
          END
      END
  
      hide(?tab3)
      ?Browse:1{PROP:FORMAT} ='90L(2)|FM~Description~@s30@#1#91L(2)|M~Part Number~@s30@#2#95L(2)|M~Shelf Location~@s30@#5#100L(2)|M~Second Location~@s30@#6#0D(2)|M~Quantity In Stock~L@N8@#7#0L(2)|M~Internal Auto Number~@n-14@#8#32L(2)|M~Reference Number~@s8@#9#'
  
  ELSE
  
      ?Browse:1{PROP:FORMAT} ='90L(2)|FM~Description~@s30@#1#91L(2)|M~Part Number~@s30@#2#35R(4)|M~Orig Qty~L(2)@n6@#3#35R(4)|M~Audit Qty~L(2)@n6@#4#100L(2)|M~Shelf Location~@s30@#5#100L(2)|M~Second Location~@s30@#6#0D(2)|M~Quantity In Stock~L@N8@#7#0L(2)|M~Internal Auto Number~@n-14@#8#32L(2)|M~Reference Number~@s8@#9#'
      hide(?tab4)
      
  END
  
      ! Save Window Name
   AddToLog('Window','Open','Stock_Check_Procedure')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stoa:Main_Browse_Key)
  BRW1.AddRange(stoa:Site_Location)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,stoa:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,stoa:Main_Browse_Key)
  BRW1.AddRange(stoa:Site_Location)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(stoa:Original_Level,BRW1.Q.stoa:Original_Level)
  BRW1.AddField(stoa:New_Level,BRW1.Q.stoa:New_Level)
  BRW1.AddField(stoa:Shelf_Location,BRW1.Q.stoa:Shelf_Location)
  BRW1.AddField(stoa:Second_Location,BRW1.Q.stoa:Second_Location)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(stoa:Internal_AutoNumber,BRW1.Q.stoa:Internal_AutoNumber)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Second_Location,BRW1.Q.sto:Second_Location)
  BRW1.AddField(sto:Manufacturer,BRW1.Q.sto:Manufacturer)
  BRW1.AddField(stoa:Audit_Ref_No,BRW1.Q.stoa:Audit_Ref_No)
  BRW1.AddField(stoa:Confirmed,BRW1.Q.stoa:Confirmed)
  BRW1.AddField(stoa:Site_Location,BRW1.Q.stoa:Site_Location)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB4.Init(Master_Location,?Master_Location,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder()
  FDCB4.AddField(loc:Location,FDCB4.Q.loc:Location)
  FDCB4.AddField(loc:RecordNumber,FDCB4.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB13.Init(tmp:Filtered_Manufacturer,?tmp:Filtered_Manufacturer,Queue:FileDropCombo:1.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo:1
  FDCB13.AddSortOrder(man:Manufacturer_Key)
  FDCB13.AddField(man:Manufacturer,FDCB13.Q.man:Manufacturer)
  FDCB13.AddField(man:RecordNumber,FDCB13.Q.man:RecordNumber)
  FDCB13.AddUpdateField(man:Manufacturer,tmp:Filtered_Manufacturer)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  FDCB13.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:GENSHORT.Close
    Relate:LOCATION.Close
    Relate:STOAUUSE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Stock_Check_Procedure')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ScanningField
      !Find the stockAudit record for this
      
      if ScanningField <> '' then
          !Stock from stocklocation key
          Access:Stock.clearkey(sto:Location_Key)
          sto:Location = Master_Location      
          sto:Part_Number = ScanningField
          if access:Stock.fetch(sto:Location_Key)
              miss# = missive('Stock record not found for this Part number at this location','ServiceBase 3g','mwarn.jpg','OK')
          ELSE
              Access:StoAudit.clearkey(stoa:Lock_Down_Key)
              stoa:Audit_Ref_No  = stom:Audit_No
              stoa:Stock_Ref_No  = sto:Ref_Number
              if access:StoAudit.fetch(stoa:Lock_Down_Key)
                  Miss# = missive('This part number does not appear in this audit.','ServiceBase 3g','mwarn.jpg','OK')
              ELSE
      
                  !does this user have "permission" to scan this item?
                  UserOK = 2      !will check for = 0 or 1 on completion
                  Access:StoAuUse.clearkey(STOU:KeyTypeNoPart)
                  STOU:AuditType       = 'S'
                  STOU:Audit_No        = stom:Audit_No
                  STOU:StockPartNumber = ScanningField
                  set(STOU:KeyTypeNoPart,STOU:KeyTypeNoPart)
                  loop
                      if access:StoAuUse.next() then break.
                      if STOU:AuditType       <> 'S' then break.
                      if STOU:Audit_No        <> stom:Audit_No then break.
                      if STOU:StockPartNumber <> ScanningField then break.
                      !found a record
                      if STOU:User_code = glo:UserCode then
                          UserOK = 1
                          break    !this user is OK to scan already
                      ELSE
                          !a different user is/was scanning
                          UserOK = 0
                      END
                  END !loop through StoAuUser
                  Case UserOK
                      of 0
                          !A different user is/was scanning and this one not permitted yet
                          IF Missive('Another user has begun the Audit on this Part Number already.|'&|
                             'Are you sure you want to proceed?','ServiceBase 3g query','mquest.jpg','/PROCEED|\CANCEL') = 1 then
      
                              !permit this one
                              Access:StoAuUse.primerecord()
                              STOU:AuditType       = 'S'
                              STOU:Audit_No        = stom:Audit_No
                              STOU:User_code       = glo:UserCode
                              STOU:Current         = 'Y'
                              STOU:StockPartNumber = ScanningField
                              Access:StoAuUse.update()
                              UserOK = 1
      
                          END !if they said yes
      
                      of 1
                          !this user is already permitted - UserOK = 1
      
                      of 2
                          !no user is permitted - permit this one
                          Access:StoAuUse.primerecord()
                          STOU:AuditType       = 'S'
                          STOU:Audit_No        = stom:Audit_No
                          STOU:User_code       = glo:UserCode
                          STOU:Current         = 'Y'
                          STOU:StockPartNumber = ScanningField
                          Access:StoAuUse.update()
                          UserOK = 1
                  END !case UserOK
                  !END - does this user have "permission" to scan this item?
      
      
                  if UserOK = 1 then
                      !message('UserOK = 1, going for an update')
                      stoa:New_Level += 1
                      stoa:Confirmed = 'Y'
                      if Access:StoAudit.update() = level:Benign
                          !message('Updated stoaudit ok. New level now stands at '&clip(Stoa:New_level))
                      !ELSE
                          !message('Unable to update stoaudit - error = '&clip(error()))
                      END
      
                      !General shortage needs to be kept up to date as this is not done on [next] button
                      if STOA:New_Level = STOA:Original_Level
                          !if stock levels are the same this can be deleted
                          Access:Genshort.clearkey(gens:Lock_Down_Key)
                          gens:Audit_No      = STOA:Audit_Ref_No
                          gens:Stock_Ref_No  = sto:Ref_Number
                          If access:GenShort.fetch(gens:Lock_Down_Key)
                              !not found - OK = probably 1 = 1
                          ELSE
                              Access:GenShort.deleteRecord()
                          END
                      ELSE
                          !stock levels are different - record this
                          Access:Genshort.clearkey(gens:Lock_Down_Key)
                          gens:Audit_No      = STOA:Audit_Ref_No
                          gens:Stock_Ref_No  = sto:Ref_Number
                          If access:GenShort.fetch(gens:Lock_Down_Key)
                              !new record
                              Access:GenShort.primerecord()
                              gens:Audit_No     = STOA:Audit_Ref_No
                              gens:Stock_Ref_No = sto:Ref_Number
                          END !if new record needed
                          GENS:Stock_Qty     = STOA:New_Level - STOA:Original_Level
                          Access:GenShort.Update()
                      END !if new record neeed
      
                      !update display
                      BRW1.resetsort(1)
                  !ELSE
                      !message('UserOK is not one but '&clip(UserOK))
                  END !if userOK = 1
              END
          END !if stock found
      
          ScanningField = ''
          DISPLAY()
          select(?ScanningField)
      
      END
      
    OF ?ButtonIncStock
      !about to increase the stock to match the audited amount...
      
      !fetch the buffer
      BRW1.updateBuffer()
      
      !check for sensibile qty
      if STOA:New_Level <= STOA:Original_Level then
          miss# = missive('The audited quantity is lower than the stock level.|'&|
                          'Increase Stock is not a valid option for this part.','ServiceBase 3g','mexclam.jpg','OK')
      
          cycle
      END
      
      !sto:r4ef_number is part of the buffer record, so is the Internal_AutoNumber
      Global_Long = sto:Ref_Number
      GlobalCancelled = false
      
      
      !call the update form
      GlobalRequest=InsertRecord
      Add_Stock_2(STOA:New_Level-STOA:Original_Level)
      Access:Stock.Update()
      GlobalRequest=ChangeRecord
      
      if GlobalCancelled = false then
      
          !stock levels have been updated to the new level so ...
          STOA:Original_Level = STOA:New_Level
          Access:StoAudit.update()
      
      
          !update the general shortage - this is no longer a shortage
          Access:Genshort.clearkey(gens:Lock_Down_Key)
          gens:Audit_No      = STOA:Audit_Ref_No
          gens:Stock_Ref_No  = sto:Ref_Number
          If access:GenShort.fetch(gens:Lock_Down_Key)
              !not found??
          ELSE
              access:Genshort.deleterecord()
          END !if record found
      
      END !GlobalCancelled = false
    OF ?ButtonDecStock
      !about to decrease the stock level to match the audited qty ...
      
      !fetch the buffer
      BRW1.updateBuffer()
      
      !check for sensibile qty
      if STOA:New_Level >= STOA:Original_Level then
          miss# = missive('The audited quantity is higher than the stock level.|'&|
                          'Decrease Stock is not a valid option for this part.','ServiceBase 3g','mexclam.jpg','OK')
      
          cycle
      END
      
      
      !sto:r4ef_number is part of the buffer record, so is the Internal_AutoNumber
      Global_Long = sto:Ref_Number
      GlobalCancelled = false
      
      !call the update form
      GlobalRequest=InsertRecord
      Deplete_Stock_2(STOA:Original_Level-STOA:New_Level)
      Access:Stock.Update()
      GlobalRequest=ChangeRecord
      
      if GlobalCancelled = false
      
          !stock levels have been updated to the "new level" so ...
          STOA:Original_Level = STOA:New_Level
          Access:StoAudit.update()
      
          !remove the general shortage - this is no longer an excess
          Access:Genshort.clearkey(gens:Lock_Down_Key)
          gens:Audit_No      = STOA:Audit_Ref_No
          gens:Stock_Ref_No  = sto:Ref_Number
          If access:GenShort.fetch(gens:Lock_Down_Key)
              !not found??
          ELSE
              access:Genshort.deleterecord()
          END !if record
      
      end !GlobalCancelled = false
    OF ?ButtonChQty
      !fetch the buffer
      BRW1.updateBuffer()
      
      !call the update form
      Global_long = 0
      GetNewAuditLevel(STOA:New_Level)
      if Global_Long = 989898 then
          !this has been cancelled
      ELSE
          STOA:New_Level = Global_Long
          Access:StoAudit.Update()
      
          if STOA:Original_Level = STOA:New_Level
              !remove the general shortage - this is no longer an excess/Shortage
              Access:Genshort.clearkey(gens:Lock_Down_Key)
              gens:Audit_No      = STOA:Audit_Ref_No
              gens:Stock_Ref_No  = sto:Ref_Number
              If access:GenShort.fetch(gens:Lock_Down_Key)
                  !not found??
              ELSE
                  access:Genshort.deleterecord()
              END !if record
      
          ELSE
      
              !update the general shortage - The numbers still differ
              Access:Genshort.clearkey(gens:Lock_Down_Key)
              gens:Audit_No      = STOA:Audit_Ref_No
              gens:Stock_Ref_No  = sto:Ref_Number
              If access:GenShort.fetch(gens:Lock_Down_Key)
                  !not found??
                  Access:GenShort.primerecord()
                  gens:Audit_No      = STOA:Audit_Ref_No
                  gens:Stock_Ref_No  = sto:Ref_Number
      
              END !if record
              GENS:Stock_Qty     = STOA:New_Level - STOA:Original_Level
              Access:GenShort.Update()
      
          END !if stoa levels match
      END !if global long = 999999
    OF ?SuspendAudit
          !check no one else is joined into the audit
          Miss# = 0
          Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
          STOU:AuditType = 'S'
          STOU:Audit_No  = stom:Audit_No
          Set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
          Loop
              If access:StoAuUse.next() then break.
              if STOU:Audit_No <> stom:Audit_No then break.
              if STOU:AuditType <> 'S' then break.
              if STOU:Current = 'Y' and STOU:User_code<>glo:Usercode then
                  Miss# = missive('You cannot proceed with this request as user '&clip(STOU:User_code)&' is still recorded as being joined to the audit.',|
                                  'ServiceBase 3g','Mstop.jpg','OK')
              END
              
          END !loop
      
          if miss# then cycle.     !someone was found joined to the audit
      
          Do SuspendContinue
    OF ?ButtonTempComplete
         !check no one else is joined into the audit
         Miss# = 0
         Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
         STOU:AuditType = 'S'
         STOU:Audit_No  = stom:Audit_No
         Set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
         Loop
             If access:StoAuUse.next() then break.
             if STOU:Audit_No <> stom:Audit_No then break.
             if STOU:AuditType <> 'S' then break.
      
             if STOU:User_code = glo:userCode then cycle.     !don't stop at being themselves
      
             if STOU:Current = 'Y' then
                 Miss# = missive('You cannot proceed with this request as user '&clip(STOU:User_code)&' is still recorded as being joined to the audit.',|
                                 'ServiceBase 3g','Mstop.jpg','OK')
             END
             
         END !loop
         if miss# then cycle.     !someone was found joined to the audit
      
      
         if missive('Are you sure you want to Temporarily Complete this Audit?|'&|
                    'This means you have finished scanning.|'&|
                    '(Any further amendments will need to be made via increase/decrease/change buttons)','ServiceBase 3g','mstop.jpg','/Continue|\Cancel') = 2 then cycle.
      
      !mark as temp suspended
      stom:Send = 'T'
      Access:STMASAUD.Update()
      
      Do CompleteScannedAudit
      
      !call the shortages/excesses report
      Stock_Audit_Report(STOM:Audit_No)
      
      !bye bye
      Post(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020490'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020490'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020490'&'0')
      ***
    OF ?ContinueAudit
      ThisWindow.Update
      !Call the suspend/continue code - 3971 (DBH: 27-02-2004)
      Do SuspendContinue
    OF ?ButtonFinScan
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?ButtonImportFile
      ThisWindow.Update
      ImportAuditFile(Stom:Audit_No,Master_Location)
      ThisWindow.Reset
      BRW1.resetsort(1)
    OF ?Button0
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'0'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?BackButton
      ThisWindow.Update
      !Get what I have!
      BRW1.UpdateBuffer()
      Access:StoAudit.ClearKey(stoa:Main_Browse_Key)
      stoa:Audit_Ref_No = stom:Audit_No
      stoa:Confirmed = 'Z'
      stoa:Site_Location = Master_Location
      SET(Stoa:Main_Browse_Key,Stoa:Main_Browse_Key,)
      IF Access:StoAudit.Previous()
        !MESSAGE('broken')
      ELSE
        IF stoa:Site_Location = Master_Location
          IF stoa:Confirmed = 'Y'
            stoa:Confirmed = 'N'
            Access:StoAudit.Update()
            New_Level = Stoa:New_Level
            DO Workout
            !BRW1.ResetQueue(1)
            BRW1.UpdateBuffer()
            BRW1.ResetSort(1)
            Brw1.TakeScroll(Event:ScrollTop)
            ThisWindow.Reset()
          END
        END
      END
    OF ?ClearButton
      ThisWindow.Update
      New_Level = ''
      ThisWindow.Reset()
    OF ?NextButton
      ThisWindow.Update
      stoa:New_Level = New_Level
      stoa:Confirmed = 'Y'
      
      IF STOA:New_Level <> STOA:Original_Level
        IF STOA:New_Level < STOA:Original_Level
          Global_Long = sto:Ref_Number
          !hah!
          GlobalRequest=InsertRecord
          Deplete_Stock_2(STOA:Original_Level-STOA:New_Level)
          IF Global_Long <> 0 THEN
            !sto:Quantity_Stock -= STOA:Original_Level-STOA:New_Level
            !STO:Quantity_Available -= STOA:Original_Level-STOA:New_Level
            !STO:Send = 'Y'
          END !IF
          Access:Stock.Update()
          GlobalRequest=ChangeRecord
          !Add one the general shortage!
          Access:GenShort.PrimeRecord()
          GENS:Audit_No      = STOA:Audit_Ref_No
          GENS:Stock_Ref_No  = sto:Ref_Number
          GENS:Stock_Qty     = (STOA:Original_Level-STOA:New_Level) * -1
          Access:GenShort.Update()
        ELSE
          Global_Long = sto:Ref_Number
          !Global_One_String = STO:Stock_Type
          GlobalRequest=InsertRecord
          Add_Stock_2(STOA:New_Level-STOA:Original_Level)
          IF Global_Long <> 0 THEN
            !sto:Quantity_Stock += STOA:New_Level-STOA:Original_Level
            !STO:Quantity_Available += STOA:New_Level-STOA:Original_Level
            !STO:Send = 'Y'
          END !IF
          Access:Stock.Update()
          GlobalRequest=ChangeRecord
          !Add one the general shortage!
          Access:GenShort.PrimeRecord()
          GENS:Audit_No      = STOA:Audit_Ref_No
          GENS:Stock_Ref_No  = sto:Ref_Number
          GENS:Stock_Qty     = STOA:New_Level-STOA:Original_Level
          Access:GenShort.Update()
        END
      END
      
      Access:StoAudit.Update()
      New_Level=0
      BRW1.ResetQueue(1)
      BRW1.UpdateBuffer()
      ThisWindow.Reset()
      DO Workout
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?tmp:Filtered_Manufacturer
      BRW1.ResetSort(1)
    OF ?Button1
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'1'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'2'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button3
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'3'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button4
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'4'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button5
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'5'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button6
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'6'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button7
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'7'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button8
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'8'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button9
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'9'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?ButtonCompleteAudit
      ThisWindow.Update
         !check no one else is joined into the audit
         Miss# = 0
         Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
         STOU:AuditType = 'S'
         STOU:Audit_No  = stom:Audit_No
         Set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
         Loop
             If access:StoAuUse.next() then break.
             if STOU:Audit_No <> stom:Audit_No then break.
             if STOU:AuditType <> 'S' then break.
      
             if STOU:User_code = glo:userCode then cycle.     !don't stop at being themselves
      
             if STOU:Current = 'Y' then
                 Miss# = missive('You cannot proceed with this request as user '&clip(STOU:User_code)&' is still recorded as being joined to the audit.',|
                                 'ServiceBase 3g','Mstop.jpg','OK')
             END
             
         END !loop
         if miss# then cycle.     !someone was found joined to the audit
      
      
      IF STOM:Complete = 'N'
          Case Missive('Do you wish to complete the current stock audit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
      
            !Lets check if we can!
      !      None# = 0 !temp to check
      !      Access:StoAudit.ClearKey(STOA:Audit_Ref_No_Key)
      !      STOA:Audit_Ref_No = STOM:Audit_No
      !      SET(STOA:Audit_Ref_No_Key,STOA:Audit_Ref_No_Key)
      !      LOOP
      !        IF Access:StoAudit.Next()
      !          BREAK
      !        END
      !        IF STOA:Audit_Ref_No <> STOM:Audit_No
      !          BREAK
      !        END
      !        IF STOA:Preliminary = 'Y'
      !          None# = 1
      !          BREAK
      !        END
      !      END
      !      IF None# = 0
      
             !if scanning need to make sure the audit bits are completed, and the shortages noted
             if stom:CompleteType = 'S' then
                  do CompleteScannedAudit
             end
      
             STOM:Complete = 'Y'
             Access:StMasAud.Update()
      
             Loop
      
                 Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
                 STOU:AuditType = 'S'
                 STOU:Audit_No  = stom:Audit_No
                 set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
      
                 if access:StoAuUse.next() then break.
                 if STOU:AuditType <> 'S' then break.
                 if STOU:Audit_No  <> stom:Audit_No then break.
      
                 Access:StoAuUse.deleteRecord(0)
      
             END !loop through StoAuUse
       Stock_Audit_Report (STOM:Audit_No)
              !BRW1.ResetSort(1)
      !      ELSE
      !        Case MessageEx('Unable to complete Stock Audit. There are items still marked as preliminary.','ServiceBase 2000',|
      !               'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,3000)
      !          Of 1 ! &OK Button
      !        End!Case MessageEx
      !      END
              Of 1 ! No Button
          End ! Case Missive
      ELSE
          Case Missive('The stock audit is already marked as complete.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?ScanningField
    ?scanningField{prop:touched}=true
  OF ?Tab:2
    ThisWindow.Reset(1)
    BRW1.ResetSort(1)
    BRW1.TakeScroll(Event:ScrollTop)
  OF ?Browse:1
    BrowseRefresh = true
    DO Workout
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      case keycode()
          of Key1
              post(event:Accepted,?Button1)
          of Key2
              post(event:Accepted,?Button2)
          of Key3
              post(event:Accepted,?Button3)
          of Key4
              post(event:Accepted,?Button4)
          of Key5
              post(event:Accepted,?Button5)
          of Key6
              post(event:Accepted,?Button6)
          of Key7
              post(event:Accepted,?Button7)
          of Key8
              post(event:Accepted,?Button8)
          of Key9
              post(event:Accepted,?Button9)
          of Key0
              post(event:Accepted,?Button0)
          of F2Key
              post(event:Accepted,?BackButton)
          of F3Key
              post(event:Accepted,?ClearButton)
          of F4Key
              post(event:Accepted,?NextButton)
      end
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF CHOICE(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = stom:Audit_No
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'Y'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Master_Location
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = STOM:Audit_No
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'N'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Master_Location
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  !DO Workout
  !
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  if tmp:Filtered_Manufacturer <> ''                          ! Manufacturer filter
      if sto:Manufacturer <> tmp:Filtered_Manufacturer        ! Manual filter on the manufacturer because there is no key,
          ReturnValue = Record:Filtered                       ! infact the field does not even exist in StoAudit.
      end
  end
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseAudit PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
Yes_Byte             STRING('Y')
Status_Info          STRING(20)
No_Byte              STRING('N')
tmp:UserLocation     STRING(30)
tmp:MainStoreLocation STRING(30)
LocalLocation        STRING(30)
QueueCount           LONG
NoLogError           BYTE
FoundUser            BYTE
StockQueue           QUEUE,PRE()
Ref_no               LONG
Quantity             LONG
Location1            STRING(30)
Location2            STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STMASAUD)
                       PROJECT(stom:Audit_No)
                       PROJECT(stom:Audit_Date)
                       PROJECT(stom:Audit_User)
                       PROJECT(stom:branch)
                       PROJECT(stom:CompleteType)
                       PROJECT(stom:Complete)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stom:Audit_No          LIKE(stom:Audit_No)            !List box control field - type derived from field
stom:Audit_No_NormalFG LONG                           !Normal forground color
stom:Audit_No_NormalBG LONG                           !Normal background color
stom:Audit_No_SelectedFG LONG                         !Selected forground color
stom:Audit_No_SelectedBG LONG                         !Selected background color
stom:Audit_Date        LIKE(stom:Audit_Date)          !List box control field - type derived from field
stom:Audit_Date_NormalFG LONG                         !Normal forground color
stom:Audit_Date_NormalBG LONG                         !Normal background color
stom:Audit_Date_SelectedFG LONG                       !Selected forground color
stom:Audit_Date_SelectedBG LONG                       !Selected background color
stom:Audit_User        LIKE(stom:Audit_User)          !List box control field - type derived from field
stom:Audit_User_NormalFG LONG                         !Normal forground color
stom:Audit_User_NormalBG LONG                         !Normal background color
stom:Audit_User_SelectedFG LONG                       !Selected forground color
stom:Audit_User_SelectedBG LONG                       !Selected background color
Status_Info            LIKE(Status_Info)              !List box control field - type derived from local data
Status_Info_NormalFG   LONG                           !Normal forground color
Status_Info_NormalBG   LONG                           !Normal background color
Status_Info_SelectedFG LONG                           !Selected forground color
Status_Info_SelectedBG LONG                           !Selected background color
stom:branch            LIKE(stom:branch)              !List box control field - type derived from field
stom:branch_NormalFG   LONG                           !Normal forground color
stom:branch_NormalBG   LONG                           !Normal background color
stom:branch_SelectedFG LONG                           !Selected forground color
stom:branch_SelectedBG LONG                           !Selected background color
stom:CompleteType      LIKE(stom:CompleteType)        !List box control field - type derived from field
stom:CompleteType_NormalFG LONG                       !Normal forground color
stom:CompleteType_NormalBG LONG                       !Normal background color
stom:CompleteType_SelectedFG LONG                     !Selected forground color
stom:CompleteType_SelectedBG LONG                     !Selected background color
stom:Complete          LIKE(stom:Complete)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Stock Audits'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock Audit File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,114,276,210),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('64L(2)|M*~Audit Number~@n-14@64L(2)|M*~Audit Date~@d6@40L(2)|M*~User Code~@S3@80' &|
   'L(2)|M*~Status~@s20@0L(2)|M*~branch~@s30@0L(2)|M*@s1@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,144),USE(?ButtonNewAudit),TRN,FLAT,LEFT,ICON('newaud2p.jpg')
                       BUTTON,AT(448,296),USE(?ButtonReprintAudit),TRN,FLAT,LEFT,ICON('prnaudp.jpg')
                       BUTTON,AT(448,114),USE(?StockCheckReport),TRN,FLAT,LEFT,ICON('stochkp.jpg')
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Incomplete Audits'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14b),AT(168,100,64,10),USE(stom:Audit_No,,?stom:Audit_No:2),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(446,208),USE(?ButtonJoinAudit),TRN,FLAT,ICON('joinaudp.jpg')
                           BUTTON,AT(448,236),USE(?ButtonContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                         END
                         TAB('Completed Audits'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14b),AT(168,100,64,10),USE(stom:Audit_No,,?stom:Audit_No:3),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                         END
                         TAB('All Audits'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14b),AT(168,100,64,10),USE(stom:Audit_No),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

CreateAudit     Routine     !Include Everything

    Prog.ProgressSetup(records(Stock)*1.5 / RECORDS(Location)) ! A guess that seems to work!

    !Start the loop through stock
    loop QueueCount = 1 to records(Glo:Queue)

        !this needs to be done for one location - for each of the shelf locations in the global queue
        Get(glo:Queue,QueueCount)

        !sto:Shelf_Location_Key has Location as first and sto:Shelf_Location as second part
        ! #13142 Changed to SecondLocKey as Vodacom want the Audit to appear to be
        ! in the same order as the Stock Check Report (DBH: 29/08/2013)

        Access:Stock.clearkey(sto:SecondLocKey)
        sto:Location       = LocalLocation
        sto:Shelf_Location = glo:Queue.GLO:Pointer        !from the queue
        set(sto:SecondLocKey,sto:SecondLocKey)
        LOOP

            IF Access:Stock.Next() then break.
            if sto:Location <> LocalLocation then break.
            if sto:Shelf_Location <> glo:Queue.GLO:Pointer then break.

            If ~glo:WebJob
                Prog.ProgressText('Records processed: ' & Count#)
            End !If ~glo:WebJob
                if Prog.InsideLoop()
                    break
                end
            

            !Filter of the bits we don't want -
            If glo:select2 <> 'YES'     !Include accessories
                If sto:accessory = 'YES'
                    cycle
                End!If sto:accessory = 'YES'
            End!If glo:select2 = 'YES'

            If glo:select4 <> ''        !Manufacturer
                If sto:manufacturer <> glo:select4
                    cycle
                End!If sto:manufacturer <> glo:select4
            End!If glo:select4 <> ''

            if clip(glo:Select9) <> '' then     !pricebanding
                if sto:AveragePurchaseCost < prb:MinPrice or sto:AveragePurchaseCost > prb:MaxPrice then
                    !outisde the banding system
                    cycle
                END
            END

            if clip(glo:Select10) <> 'YES'  !Include suspended stock
                !exclude stock
                if sto:Suspend = true  then
                    cycle
                END !if stock suspended
            END

            Count# += 1

            StockQueue.Ref_no    = sto:Ref_Number
            StockQueue.Quantity  = sto:Quantity_Stock
            StockQueue.Location1 = sto:Shelf_Location
            StockQueue.Location2 = sto:Second_Location
            Add(StockQueue)

        END  !loop through stock by location and shelf
    END !loop through glo:Queue to get shelf_locations

    Prog.ProgressFinish()


    if missive('This will create an audit with '&clip(records(StockQueue))&' entries are you sure?','ServiceBase 3g','mstop.jpg','/OK|\ABANDON') = 1 then

        Prog.ProgressSetup(records(StockQueue))
        Count# = 1

        Loop QueueCount = 1 to records(StockQueue)

            Get(StockQueue,QueueCount)

            Access:StoAudit.PrimeRecord()
            stoa:Audit_Ref_No = stom:Audit_No
            stoa:Site_Location = LocalLocation
            stoa:Stock_Ref_No = StockQueue.Ref_no
            stoa:Original_Level = StockQueue.Quantity
            stoa:New_Level = 0
            stoa:Audit_Reason = ''
            stoa:Preliminary  = ''
            stoa:Confirmed = 'N'
            stoa:Shelf_Location = StockQueue.Location1
            stoa:Second_Location = StockQueue.Location2
            access:StoAudit.update()
            !access:StoAudit.tryupdate()
            !Access:StoAudit.tryinsert()

            If ~glo:WebJob
                Prog.ProgressText('Records processed: ' & Count#)
            End !If ~glo:WebJob

                if Prog.InsideLoop()
                    break
                end
            Count# += 1

        END !loop through stock queue
        NoLogError = true

    ELSE
        NoLogError = false
    END !if missive



    EXIT
!CheckEntry and LogOutUser  routines

CheckEntry          Routine

    !reset defaults
    NoLogError = true
    FoundUser = false

    !are they already joined on this audit
    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
    STOU:AuditType = 'S'
    STOU:Audit_No  = stom:Audit_No
    STOU:User_code = glo:userCode
    set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
    Loop

        If access:StoAuUse.next() then break.
        If STOU:AuditType <> 'S' then break.
        If STOU:Audit_No  <> stom:Audit_No then break.
        if STOU:User_code <> glo:userCode then break.

        !ok got a matching record
        FoundUser = true

        !is this one active
        if STOU:Current = 'Y' then
            IF Missive('Records show that you are already currently working within this audit. |'&|
                       'If you are using a "shared" log in then you are warned that this will cause problems, and will render this audit worthless.|'&|
                       'Are you sure you want to continue and re-join this audit?','ServiceBase 3g Warning','mstop.jpg','\YES|/NO') = 2 then
                NoLogError = false
                EXIT
            ELSE
                Break   !got an answer to one question that will do
            END !if missive = NO
        END
    END !loop through StoAuUse by typeNoUser

    if foundUser then
        !set all records to active
        Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
        STOU:AuditType = 'S'
        STOU:Audit_No  = stom:Audit_No
        STOU:User_code = glo:userCode
        set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
        Loop

            If access:StoAuUse.next() then break.
            If STOU:AuditType <> 'S' then break.
            If STOU:Audit_No  <> stom:Audit_No then break.
            if STOU:User_code <> glo:userCode then break.

            STOU:Current = 'Y'
            Access:StoAuUse.update()
        END !loop through all StoAuUse
    ELSE
        !first time in
        Access:StoAuUse.primerecord()
        STOU:AuditType       = 'S'
        STOU:Audit_No        = Stom:Audit_no
        STOU:User_code       = glo:UserCode
        STOU:Current         = 'Y'
        STOU:StockPartNumber = 'PRIMARY LOG-IN'
        Access:StoAuUse.update()
    END


    !check for someone else currently on the system
    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNo)
    STOU:AuditType  = 'S'
    STOU:Audit_No   = Stom:Audit_no
    set(STOU:KeyAuditTypeNo,STOU:KeyAuditTypeNo)
    loop
        if access:StoAuUse.next() then break.
        if STOU:AuditType  <> 'S' then break.
        if STOU:Audit_No   <> Stom:Audit_no then break.

        if STOU:Current = 'Y' and STOU:User_code <> glo:userCode then
            !not the current log in - but someone else is actively scanning
            Miss# = Missive('Records show that other people are already currently joined in with this audit. |'&|
                       'You should be aware of this and ensure that you are not auditing the same items that have already been scanned',|
                       'ServiceBase 3g Warning','mstop.jpg','OK') 
            BREAK
        END
    END !loop through audit users

    EXIT


LogOutUser          Routine

    !set all records to inactive
    Access:StoAuUse.clearkey(STOU:KeyAuditTypeNoUser)
    STOU:AuditType = 'S'
    STOU:Audit_No  = stom:Audit_No
    STOU:User_code = glo:userCode
    set(STOU:KeyAuditTypeNoUser,STOU:KeyAuditTypeNoUser)
    Loop

        If access:StoAuUse.next() then break.
        If STOU:AuditType <> 'S' then break.
        If STOU:Audit_No  <> stom:Audit_No then break.
        if STOU:User_code <> glo:userCode then break.

        STOU:Current = 'N'
        Access:StoAuUse.update()

    END !loop through all StoAuUse records for this user

    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020488'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseAudit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOCATION_ALIAS.Open
  Relate:PRIBAND.Open
  Relate:STMASAUD.Open
  Relate:STOAUUSE.Open
  Access:STOAUDIT.UseFile
  Access:USERS.UseFile
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STMASAUD,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If ~glo:WebJob
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserLocation    = use:Location
          glo:UserCode = use:User_Code
  
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      tmp:MainStoreLocation   = MainStoreLocation()
  End !glo:WebJob            
  
      ! Save Window Name
   AddToLog('Window','Open','BrowseAudit')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stom:Compeleted_Key)
  BRW1.AddRange(stom:Complete,Yes_Byte)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?stom:Audit_No:3,stom:Complete,1,BRW1)
  BRW1.AddSortOrder(,stom:AutoIncrement_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?stom:Audit_No,stom:Audit_No,1,BRW1)
  BRW1.AddSortOrder(,stom:Compeleted_Key)
  BRW1.AddRange(stom:Complete,No_Byte)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?stom:Audit_No:2,stom:Complete,1,BRW1)
  BIND('Status_Info',Status_Info)
  BRW1.AddField(stom:Audit_No,BRW1.Q.stom:Audit_No)
  BRW1.AddField(stom:Audit_Date,BRW1.Q.stom:Audit_Date)
  BRW1.AddField(stom:Audit_User,BRW1.Q.stom:Audit_User)
  BRW1.AddField(Status_Info,BRW1.Q.Status_Info)
  BRW1.AddField(stom:branch,BRW1.Q.stom:branch)
  BRW1.AddField(stom:CompleteType,BRW1.Q.stom:CompleteType)
  BRW1.AddField(stom:Complete,BRW1.Q.stom:Complete)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOCATION_ALIAS.Close
    Relate:PRIBAND.Close
    Relate:STMASAUD.Close
    Relate:STOAUUSE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseAudit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonNewAudit
      !create a new audit and start it immediately
      
      !TB12462 Stock and exchange audit means this is now critera baseed
      glo:ErrorText = ''
      Stock_Check_Criteria(2)
      if glo:ErrorText = 'CANCELLED' then
          glo:ErrorText = ''
          cycle
      END
      
      !setup some variables to be used later
      LocalLocation = GLO:Select1     !whatever this was set up to do
      
      !Pricebanding setup
      if clip(GLO:Select9) = '' then
          !leave this alone
      ELSE
          Access:priband.clearkey(prb:KeyBandName)
          prb:BandName = Glo:Select9
          if access:Priband.fetch(prb:KeyBandName)
              !error
              Glo:Select9 = ''
          END
      END
      
      !create a new audit
      Access:StMasAud.PrimeRecord()
      STOM:Audit_Date = TODAY()
      STOM:Audit_Time = CLOCK()
      !find the user code
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          STOM:Audit_User = use:User_Code
      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      STOM:Complete   = 'N'
      stom:branch = LocalLocation     !whatever it was set up to be
      
      if missive('How do you intend to complete this audit? By scanning the items, or by manually entering stock quanities?|Note: Once you have chosen a method you cannot change it.',|
                 'ServiceBase 3g','mquest.jpg','SCAN|MANUAL') = 1 then
          stom:CompleteType = 'S'
      ELSE
          stom:CompleteType = 'M'
      END
      
      Access:StMasAud.Update()
      
      Count# = 1
      
      !testing only the full type = JC 18/03/13
      do CreateAudit
      
      
      if noLogError then  !the audit was generated in the procedure
      
          Prog.ProgressFinish()
      
          !reset the globals
          glo:select1 = ''
          glo:select2 = ''
          glo:select3 = ''
          glo:select4 = ''
          glo:select5 = ''
          glo:Select6 = ''
          glo:select7 = ''
          glo:select8 = ''
          glo:select9 = ''
          glo:Select10= ''
          glo:Select11= ''
      
          !CheckEntry and LogOutUser  routines
          do CheckEntry  
      
          if NoLogError
      
              Stock_Check_Procedure
      
              !after the audit you need to record the leaving
              do LogOutUser
      
          END !if NoLogError
      
      ELSE
      
          !This audit is not wanted any more - remove it
          QueueCount = stom:Audit_No      !reusing the long
          Access:StMASAud.clearkey(stom:AutoIncrement_Key)
          stom:Audit_No = QueueCount
          If access:STMASAUD.fetch(stom:AutoIncrement_Key)
              !error?
          ELSE
              Access:STMASAUD.deleteRecord()
          END
      
      END !if no log error first time
      
      
      BRW1.ResetSort(1)
    OF ?ButtonJoinAudit
      ThisWindow.Update()
      
      if stom:Complete = 'Y'
          Miss# = missive('You cannot join this audit. This audit is marked as completed','ServiceBase 3g','mstop.jpg','OK')
      END
      
      if stom:Audit_User = glo:userCode then
          miss# = missive('You cannot join this audit - you started it. Use the [Continue Audit] button','ServiceBase 3g','mstop.jpg','OK')
          cycle
      END
      
      if stom:CompleteType <> 'S' then
          miss# = missive('You cannot join this audit. This is a manual entry audit. You can only join scanning audits','ServiceBase 3g','mstop.jpg','OK')
          cycle
      END
      
      if missive('Are you sure you want to be added as a secondary scanner to assist in scanning for this audit?',|
                 'ServiceBase 3g','mquest.jpg','/YES|\NO') =2  then cycle.
      
      !CheckEntry and LogOutUser  routines
      do CheckEntry  
      
      if NoLogError
      
          Stock_Check_Procedure
      
          !after the audit you need to record the leaving
          do LogOutUser
      
      END !if NoLogError
      
      
      
      
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020488'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020488'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020488'&'0')
      ***
    OF ?ButtonReprintAudit
      ThisWindow.Update
      ThisWindow.Update()
       Stock_Audit_Report (STOM:Audit_No)
    OF ?StockCheckReport
      ThisWindow.Update
      Stock_Check_Criteria(1)
      ThisWindow.Reset
    OF ?ButtonContinueAudit
      ThisWindow.Update
      ThisWindow.Update()
      
      if stom:Audit_User <> glo:userCode then
          miss# = missive('You cannot continue this audit - you did not start it. Use the [Join Audit] button','ServiceBase 3g','mstop.jpg','OK')
          cycle
      END
      
      
      
      
      IF STOM:Complete = 'N' or Stom:Complete = 'T'
      
          !CheckEntry and LogOutUser  routines
          do CheckEntry  
      
          if NoLogError
      
      
      Stock_Check_Procedure()
              !after the audit you need to record the leaving
              do LogOutUser
      
          END !if NoLogError
      
      ELSE
          Case Missive('This stock audit is complete. Please start a new audit or select an incomplete one.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (stom:Send = 'S')
    Status_Info = 'AUDIT SUSPENDED'
  ELSE
    IF (stom:Complete <> 'Y')
      Status_Info = 'AUDIT IN PROGRESS'
    ELSE
      Status_Info = 'AUDIT COMPLETED'
    END
  END
  Case Stom:Send
      of 'S'
          Status_Info = 'SUSPENDED'
      of 'T'
          Status_Info = 'TEMP COMPLETED'
  
      ELSE
          Status_Info = 'IN PROGRESS'
  END !Case Stom:Send
  
  IF (stom:Complete = 'Y')
    Status_Info = 'COMPLETED'
  END
  PARENT.SetQueueRecord
  SELF.Q.stom:Audit_No_NormalFG = -1
  SELF.Q.stom:Audit_No_NormalBG = -1
  SELF.Q.stom:Audit_No_SelectedFG = -1
  SELF.Q.stom:Audit_No_SelectedBG = -1
  SELF.Q.stom:Audit_Date_NormalFG = -1
  SELF.Q.stom:Audit_Date_NormalBG = -1
  SELF.Q.stom:Audit_Date_SelectedFG = -1
  SELF.Q.stom:Audit_Date_SelectedBG = -1
  SELF.Q.stom:Audit_User_NormalFG = -1
  SELF.Q.stom:Audit_User_NormalBG = -1
  SELF.Q.stom:Audit_User_SelectedFG = -1
  SELF.Q.stom:Audit_User_SelectedBG = -1
  SELF.Q.Status_Info_NormalFG = -1
  SELF.Q.Status_Info_NormalBG = -1
  SELF.Q.Status_Info_SelectedFG = -1
  SELF.Q.Status_Info_SelectedBG = -1
  SELF.Q.stom:branch_NormalFG = -1
  SELF.Q.stom:branch_NormalBG = -1
  SELF.Q.stom:branch_SelectedFG = -1
  SELF.Q.stom:branch_SelectedBG = -1
  SELF.Q.stom:CompleteType_NormalFG = -1
  SELF.Q.stom:CompleteType_NormalBG = -1
  SELF.Q.stom:CompleteType_SelectedFG = -1
  SELF.Q.stom:CompleteType_SelectedBG = -1
  SELF.Q.Status_Info = Status_Info                    !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  If glo:WebJob
      IF stom:branch <> glo:Default_Site_Location
          RETURN Record:Filtered
      END
  Else !glo:WebJob
      !Not a web job, show for Main Store And the User's Location
      If stom:Branch <> tmp:MainStoreLocation And stom:Branch <> tmp:UserLocation
          Return Record:Filtered
      End !If stom:Branch <> tmp:MainStoreLocation Or stom:Branch <> tmp:UserLocation
  End !glo:WebJob
  
  
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 500
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Loan_Check_Procedure PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
Added_In             LONG
save_eau_id          USHORT,AUTO
Stock_Query          STRING(30)
LOC:Amount           DECIMAL(15,2)
No_Find              BYTE
BrLocator1           STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Master_Location      STRING(30)
Variance             REAL
Temp_Cut_String      STRING(10)
New_level            STRING(4)
Use_Exch_Byte        STRING('YES')
Exchange_IMEI        STRING(20)
In_stock             REAL
Master_Type          STRING(30)
Loc_Status           STRING(100)
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Master_Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(LOANAUI)
                       PROJECT(lau:Internal_No)
                       PROJECT(lau:Audit_Number)
                       PROJECT(lau:Confirmed)
                       PROJECT(lau:Site_Location)
                       PROJECT(lau:Stock_Type)
                       PROJECT(lau:Shelf_Location)
                       PROJECT(lau:Ref_Number)
                       JOIN(loa:Ref_Number_Key,lau:Ref_Number)
                         PROJECT(loa:ESN)
                         PROJECT(loa:Model_Number)
                         PROJECT(loa:Shelf_Location)
                         PROJECT(loa:Manufacturer)
                         PROJECT(loa:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
loa:ESN                LIKE(loa:ESN)                  !List box control field - type derived from field
loa:Model_Number       LIKE(loa:Model_Number)         !List box control field - type derived from field
loa:Shelf_Location     LIKE(loa:Shelf_Location)       !Browse hot field - type derived from field
loa:Manufacturer       LIKE(loa:Manufacturer)         !Browse hot field - type derived from field
lau:Internal_No        LIKE(lau:Internal_No)          !Primary key field - type derived from field
lau:Audit_Number       LIKE(lau:Audit_Number)         !Browse key field - type derived from field
lau:Confirmed          LIKE(lau:Confirmed)            !Browse key field - type derived from field
lau:Site_Location      LIKE(lau:Site_Location)        !Browse key field - type derived from field
lau:Stock_Type         LIKE(lau:Stock_Type)           !Browse key field - type derived from field
lau:Shelf_Location     LIKE(lau:Shelf_Location)       !Browse key field - type derived from field
loa:Ref_Number         LIKE(loa:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK8::lau:Audit_Number     LIKE(lau:Audit_Number)
HK8::lau:Confirmed        LIKE(lau:Confirmed)
HK8::lau:Site_Location    LIKE(lau:Site_Location)
HK8::lau:Stock_Type       LIKE(lau:Stock_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB10::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse Loans'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Loan Audit Procedure'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,264,310),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(92,90,212,15),USE(lmf:Status),TRN,CENTER,FONT(,16,,FONT:bold),COLOR(09A6A7CH)
                           STRING('Location'),AT(92,110),USE(?String13),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s20),AT(144,110,124,10),USE(Master_Location),IMM,DISABLE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo:1)
                           STRING('Stock Type'),AT(92,123),USE(?String1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,123,124,10),USE(Master_Type),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(272,120),USE(?buttonLookupStockType),TRN,FLAT,ICON('lookupp.jpg')
                           STRING(@s30),AT(92,138,212,16),USE(loa:Manufacturer),TRN,CENTER,FONT(,16,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(92,154,212,16),USE(loa:Model_Number),TRN,CENTER,FONT(,16,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(92,170,212,16),USE(loa:ESN),TRN,CENTER,FONT(,16,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(92,186,212,16),USE(loa:Shelf_Location),TRN,CENTER,FONT(,16,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PANEL,AT(236,203,68,60),USE(?Panel1:3),FILL(COLOR:Red)
                           STRING(@n_6),AT(96,218,60,24),USE(In_stock),TRN,RIGHT,FONT(,12,010101H,FONT:bold,CHARSET:ANSI)
                           STRING(@n_6),AT(168,218,60,32),USE(New_level),TRN,RIGHT,FONT(,12,010101H,FONT:bold,CHARSET:ANSI)
                           STRING(@n-7),AT(240,218,60,32),USE(Variance),TRN,RIGHT,FONT(,12,010101H,FONT:bold,CHARSET:ANSI)
                           STRING('VARIANCE'),AT(236,250,68,12),USE(?String7),TRN,CENTER,FONT(,,010101H,FONT:bold)
                           ENTRY(@s20),AT(136,270,124,10),USE(Exchange_IMEI),DISABLE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           STRING('IN STOCK'),AT(92,250,68,12),USE(?String5),TRN,CENTER,FONT(,,010101H,FONT:bold)
                           STRING('COUNTED'),AT(164,250,68,12),USE(?String6),TRN,CENTER,FONT(,,010101H,FONT:bold)
                           BUTTON('Back'),AT(92,286,56,16),USE(?Button11),HIDE,LEFT,ICON('VCRPRIOR.ICO')
                           BUTTON,AT(160,286),USE(?buttonCompleteStockType),TRN,FLAT,ICON('compstop.jpg')
                           BUTTON('Next'),AT(248,286,56,16),USE(?Button11:3),HIDE,LEFT,ICON('VCRNEXT.ICO')
                           STRING(@s100),AT(68,314,256,20),USE(Loc_Status),FONT('Tahoma',18,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(332,56,284,308),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Awaiting Audit'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Audited Units'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       LIST,AT(336,74,276,284),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('127L(2)|M~I.M.E.I. Number~@s30@81L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:1)
                       PANEL,AT(92,204,68,60),USE(?Panel1),FILL(COLOR:Green)
                       PANEL,AT(164,204,68,60),USE(?Panel1:2),FILL(COLOR:Yellow)
                       BUTTON('1'),AT(172,394,20,19),USE(?Button2),HIDE,FONT(,,,FONT:bold)
                       BUTTON('2'),AT(192,394,20,19),USE(?Button2:2),HIDE,FONT(,,,FONT:bold)
                       BUTTON('3'),AT(212,394,20,19),USE(?Button2:3),HIDE,FONT(,,,FONT:bold)
                       BUTTON('4'),AT(232,394,20,19),USE(?Button2:4),HIDE,FONT(,,,FONT:bold)
                       BUTTON('5'),AT(252,394,20,19),USE(?Button2:5),HIDE,FONT(,,,FONT:bold)
                       BUTTON('6'),AT(284,394,20,19),USE(?Button2:6),HIDE,FONT(,,,FONT:bold)
                       BUTTON('7'),AT(304,394,20,19),USE(?Button2:7),HIDE,FONT(,,,FONT:bold)
                       BUTTON('8'),AT(324,394,20,19),USE(?Button2:8),HIDE,FONT(,,,FONT:bold)
                       BUTTON('9'),AT(344,394,20,19),USE(?Button2:9),HIDE,FONT(,,,FONT:bold)
                       BUTTON('0'),AT(364,394,20,19),USE(?Button2:10),HIDE,FONT(,,,FONT:bold)
                       BUTTON,AT(548,366),USE(?SuspendAudit),TRN,FLAT,LEFT,ICON('susaudp.jpg')
                       BUTTON,AT(548,366),USE(?ContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                       BUTTON('Close'),AT(168,398,76,20),USE(?Close),HIDE,LEFT,ICON('Cancel.ico')
                       BUTTON,AT(476,366),USE(?Button15:2),TRN,FLAT,LEFT,ICON('compaudp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:Master_Type                Like(Master_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

SuspendContinue  Routine
    If lmf:Status   <> 'SUSPENDED'
        lmf:Status = 'SUSPENDED'
        ACcess:LoanAMF.Update()
        Post(Event:CloseWindow)
    Else !emf:Status   <> 'SUSPENDED'
        lmf:Status = 'AUDIT IN PROGRESS'
        !?Button15{PROP:Text} = 'Suspend Audit'
        ?SuspendAudit{prop:Hide} = 0
        ?ContinueAudit{prop:Hide} = 1
        !ENABLE(?Master_Location)
        ?Master_Type{Prop:Disable} = 0
        ?buttonLookupStockType{prop:Disable}= 0
        ?Exchange_IMEI{prop:Disable} = 0
        ?buttonCompleteStockType{prop:Disable} =0
    End !emf:Status   <> 'SUSPENDED'


    !IF Stom:Send = ''
    !  stom:Send = 'S'
    !  Access:STMASAUD.Update()
WORKOUT     ROUTINE

    Variance = New_level - In_stock
    UPDATE()
    DISPLAY()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020480'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Loan_Check_Procedure')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOANALC.Open
  Relate:LOANAMF.Open
  Relate:LOCATION.Open
  Access:LOANHIST.UseFile
  Access:STOCKTYP.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOANAUI,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF lmf:Status = 'SUSPENDED'
    !?Button15{PROP:Text} = 'Continue Audit'
      ?SuspendAudit{prop:Hide} = 1
      ?ContinueAudit{prop:Hide} = 0
      ?Master_Location{prop:disable} = 1
      ?Master_Type{prop:Disable} = 1
      ?buttonLookupStockType{prop:Disable} = 1
      !?Exchange_IMEI{prop:Disable} = 1
      ?buttonCompleteStockType{prop:Disable} =1
  ELSE
      !?Button15{PROP:Text} = 'Suspend Audit'
      ?SuspendAudit{prop:Hide} = 0
      ?ContinueAudit{prop:Hide} = 1
      !ENABLE(?Master_Location)
  END
  
  Master_Location = lmf:Site_location
  if glo:WebJob = 1
      DISABLE(?Master_Location) ! Disable the location - we have only processed stock records for this location
  end !If glo:WebJob = 1
  
      ! Save Window Name
   AddToLog('Window','Open','Loan_Check_Procedure')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Master_Type{Prop:Tip} AND ~?buttonLookupStockType{Prop:Tip}
     ?buttonLookupStockType{Prop:Tip} = 'Select ' & ?Master_Type{Prop:Tip}
  END
  IF ?Master_Type{Prop:Msg} AND ~?buttonLookupStockType{Prop:Msg}
     ?buttonLookupStockType{Prop:Msg} = 'Select ' & ?Master_Type{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,lau:Main_Browse_Key)
  BRW1.AddRange(lau:Stock_Type)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,lau:Shelf_Location,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,lau:Main_Browse_Key)
  BRW1.AddRange(lau:Stock_Type)
  BRW1.AddField(loa:ESN,BRW1.Q.loa:ESN)
  BRW1.AddField(loa:Model_Number,BRW1.Q.loa:Model_Number)
  BRW1.AddField(loa:Shelf_Location,BRW1.Q.loa:Shelf_Location)
  BRW1.AddField(loa:Manufacturer,BRW1.Q.loa:Manufacturer)
  BRW1.AddField(lau:Internal_No,BRW1.Q.lau:Internal_No)
  BRW1.AddField(lau:Audit_Number,BRW1.Q.lau:Audit_Number)
  BRW1.AddField(lau:Confirmed,BRW1.Q.lau:Confirmed)
  BRW1.AddField(lau:Site_Location,BRW1.Q.lau:Site_Location)
  BRW1.AddField(lau:Stock_Type,BRW1.Q.lau:Stock_Type)
  BRW1.AddField(lau:Shelf_Location,BRW1.Q.lau:Shelf_Location)
  BRW1.AddField(loa:Ref_Number,BRW1.Q.loa:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB10.Init(Master_Location,?Master_Location,Queue:FileDropCombo:1.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:1
  FDCB10.AddSortOrder()
  FDCB10.AddField(loc:Location,FDCB10.Q.loc:Location)
  FDCB10.AddField(loc:RecordNumber,FDCB10.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOANALC.Close
    Relate:LOANAMF.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Loan_Check_Procedure')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickLoanStockType
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?SuspendAudit
      Do SuspendContinue
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020480'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020480'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020480'&'0')
      ***
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Master_Type
      IF Master_Type OR ?Master_Type{Prop:Req}
        stp:Stock_Type = Master_Type
        stp:Use_Loan = 'YES'
        !Save Lookup Field Incase Of error
        look:Master_Type        = Master_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Loan_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Master_Type = stp:Stock_Type
          ELSE
            CLEAR(stp:Use_Loan)
            !Restore Lookup On Error
            Master_Type = look:Master_Type
            SELECT(?Master_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      if (NOT 0{prop:AcceptAll})
      
          ! Quick Record Count
          Save_eau_ID = Access:LOANAUI.SaveFile()
      
      !    Prog.ProgressSetup(Records(LOANAUI) * 2)
      
          In_stock    = 0
      
          Access:LOANAUI.ClearKey(lau:Main_Browse_Key)
          lau:Audit_Number   = lmf:Audit_Number
          lau:Confirmed      = 0
          lau:Site_Location  = Master_Location
          lau:Stock_Type = Master_type
          Set(lau:Main_Browse_Key,lau:Main_Browse_Key)
          Loop
              If Access:LOANAUI.NEXT()
                 Break
              End !If
              If lau:Audit_Number   <> lmf:Audit_Number      |
              Or lau:Confirmed      <> 0      |
              Or lau:Site_Location  <> Master_Location      |
              Or lau:Stock_Type <> Master_type            |
                  Then Break.  ! End If
      !        rtn# = Prog.InsideLoop()
              In_stock += 1
          End !Loop
      
          Added_In = 0
          New_Level = 0
      
          Access:LOANAUI.ClearKey(lau:Main_Browse_Key)
          lau:Audit_Number   = lmf:Audit_Number
          lau:Confirmed      = 1
          lau:Site_Location  = Master_Location
          lau:Stock_Type = Master_type
          Set(lau:Main_Browse_Key,lau:Main_Browse_Key)
          Loop
              If Access:LOANAUI.NEXT()
                 Break
              End !If
              If lau:Audit_Number   <> lmf:Audit_Number      |
              Or lau:Confirmed      <> 1   |
              Or lau:Site_Location  <> Master_Location      |
              Or lau:Stock_Type <> Master_type            |
                  Then Break.  ! End If
      !        rtn# = Prog.InsideLoop()
              IF lau:New_IMEI = TRUE
                Added_In += 1
              END
              New_Level += 1
          End !Loop
          Access:LOANAUI.RestoreFile(Save_eau_ID)
          !DISABLE(?Master_type)
      
      !    Prog.ProgressFinish()
      
          In_Stock = (In_Stock+New_Level) - Added_In
      
          !Ok, need to update the other file!
          Access:loanalc.ClearKey(lac1:Locate_key)
          lac1:Audit_Number = lmf:Audit_Number
          lac1:Location = Master_Location
          lac1:Stock_Type = Master_type
          IF Access:loanalc.Fetch(lac1:Locate_key)
            !Error - not here, add it! This gives me a location which has been visited, but not yet audited completely.
            !Ask!
              Case Missive('Do you wish to audit this stock type?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Access:loanalc.PrimeRecord()
                      lac1:Audit_Number = lmf:Audit_Number
                      lac1:Location = Master_Location
                      lac1:Stock_Type = Master_type
                      lac1:Confirmed = 0
                      Access:loanalc.Insert()
                      Loc_Status = 'PRELIMINARY AUDIT'
                      ENABLE(?Exchange_IMEI)
                  Of 1 ! No Button
                      Loc_Status = 'VIEW ONLY'
                      DISABLE(?Exchange_IMEI)
              End ! Case Missive
          ELSE
            !Already been here, update screen varaible
            IF lac1:Confirmed = 1
                Loc_Status = 'STOCK TYPE AUDIT COMPLETE'
                ENABLE(?Exchange_IMEI)
            ELSE
                Loc_Status = 'PRELIMINARY AUDIT'
                ENABLE(?Exchange_IMEI)
            END
          END
          brw1.ResetSort(1)
      end !if (NOT 0{prop:AcceptAll})
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?buttonLookupStockType
      ThisWindow.Update
      stp:Stock_Type = Master_Type
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Master_Type = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?Master_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Master_Type)
    OF ?Exchange_IMEI
      Access:LoanAui.Clearkey(lau:Locate_IMEI_Key)
      lau:Audit_Number    = lmf:Audit_Number
      lau:Site_Location   = Master_Location
      lau:IMEI_Number     = Exchange_IMEI
      If Access:LoanAui.Tryfetch(lau:Locate_IMEI_Key) = Level:Benign
          !Found
          IF lau:Stock_Type <> Master_Type
              Case Missive('This unit exists in a different loan/stock type. Please ensure it is in the correct location.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          ELSE
              IF (lau:Confirmed = TRUE)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('The selected I.M.E.I. Number has already been scanned.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
              ELSE
                  lau:Confirmed = TRUE
                  New_Level += 1
                  Access:LoanAui.TryUpdate()
              END
          END
      Else! If Access:LoanAui.Tryfetch(lau:AuditIMEIKey) = Level:Benign
          !Error
          Case Missive('This I.M.E.I. number is not recognized in the exchange or loan databases. Do you wish to enter it now?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                GlobalRequest = InsertRecord
                loa:ESN       = Exchange_IMEI
                
                UpdateLoan
                !Got it? Now add into the list!
                !Get it back!
                Access:Loan.ClearKey(loa:ESN_Only_Key)
                loa:ESN = Exchange_IMEI
                IF Access:Loan.Fetch(loa:ESN_Only_Key)
                  !Error!
                ELSE
                  New_Level += 1  ! #12276 Only count if the exchange unit is added (Bryan: 10/10/2011)
                  Access:LoanAui.PrimeRecord()
                  lau:Audit_Number = lmf:Audit_Number
                  lau:Site_Location = loa:Location
                  lau:Stock_Type = loa:Stock_Type
                  lau:Ref_Number = loa:Ref_Number
                  !lau:Existsstoa:Original_Level = sto:Quantity_Stock
                  lau:Exists = 'N'
                  lau:Shelf_Location = loa:Shelf_Location
                  lau:Location = loa:Location
                  lau:IMEI_Number = loa:ESN
                  lau:New_IMEI    = True
                  lau:Confirmed = TRUE
                  Access:LoanAui.Update()
                END
              Of 1 ! No Button
          End ! Case Missive
      
      End! If Access:LoanAui.Tryfetch(lau:AuditIMEIKey) = Level:Benign
      Exchange_IMEI = ''
      UPDATE()
      DISPLAY()
      SELECT(?Exchange_IMEI)
      BRW1.ResetSort(1)
    OF ?Button11
      ThisWindow.Update
      !Get what I have!
      BRW1.UpdateBuffer()
      Access:StoAudit.ClearKey(stoa:Main_Browse_Key)
      stoa:Audit_Ref_No = stom:Audit_No
      stoa:Confirmed = 'Z'
      stoa:Site_Location = Master_Location
      SET(Stoa:Main_Browse_Key,Stoa:Main_Browse_Key,)
      IF Access:StoAudit.Previous()
        !MESSAGE('broken')
      ELSE
        IF stoa:Site_Location = Master_Location
          IF stoa:Confirmed = 'Y'
            stoa:Confirmed = 'N'
            Access:StoAudit.Update()
            New_Level = Stoa:New_Level
            DO Workout
            !BRW1.ResetQueue(1)
            BRW1.UpdateBuffer()
            BRW1.ResetSort(1)
            Brw1.TakeScroll(Event:ScrollTop)
            ThisWindow.Reset()
          END
        END
      END
    OF ?buttonCompleteStockType
      ThisWindow.Update
      Case Missive('Do you wish to complete the audit of this stock type?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
            Access:LoanAlc.ClearKey(lac1:Locate_key)
            lac1:Audit_Number = lmf:Audit_Number
            lac1:Location = Master_Location
            lac1:Stock_Type = Master_type
            IF Access:LoanAlc.Fetch(lac1:Locate_key)
              !Not here?!
              Case Missive('This stock type is not currently being audited. Therefore you cannot complete it.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
            ELSE
              IF lac1:Confirmed = 0
                lac1:Confirmed = 1
                Access:LoanAlc.Update()
                Loc_Status = 'STOCK TYPE AUDIT COMPLETE'
                display()
              ELSE
                  Case Missive('This stock type has already been completed. You cannot complete it again.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              END
            END
      
          Of 1 ! No Button
      End ! Case Missive
    OF ?Button2
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'1'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:2
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'2'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:3
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'3'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:4
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'4'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:5
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'5'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:6
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'6'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:7
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'7'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:8
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'8'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:9
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'9'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?Button2:10
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'0'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
      DO workout
    OF ?ContinueAudit
      ThisWindow.Update
      Do SuspendContinue
    OF ?Button15:2
      ThisWindow.Update
      IF lmf:Complete_Flag = 0
          Case Missive('Do you wish to complete the current stock audit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  !Lets check if we can!
                  !Anything left?
      
                  !Check no locations have been messed with!
                  all_complete# = 0
                  Access:loanalc.ClearKey(lac1:Locate_key)
                  lac1:Audit_Number = lmf:Audit_Number
                  SET(lac1:Locate_key,lac1:Locate_key)
                  LOOP
                    IF Access:loanalc.Next()
                      BREAK
                    END
                    IF lac1:Audit_Number <> lmf:Audit_Number
                      BREAK
                    END
                    IF lac1:Confirmed = 0
                      all_complete# = 1
                      BREAK
                    END
                  END
      
                  IF all_complete# = 1
                    !Not all locations locked down!
                      Case Missive('There are unconfirmed stock type audits. Please check you have completed the audit correctly.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  ELSE
                    !Loop audited stock types!
                    Access:loanalc.ClearKey(lac1:Locate_key)
                    lac1:Audit_Number = lmf:Audit_Number
                    SET(lac1:Locate_key,lac1:Locate_key)
                    LOOP
                      IF Access:loanalc.Next()
                        BREAK
                      END
                      IF lac1:Audit_Number <> lmf:Audit_Number
                        BREAK
                      END
                      IF lmf:Complete_Flag = 0
                        CYCLE !Shoudl never happen...but you *never* know!
                      END
                      Save_eau_ID = Access:LoanAui.SaveFile()
                      Access:LoanAui.ClearKey(lau:Main_Browse_Key)
                      lau:Audit_Number   = lmf:Audit_Number
                      lau:Confirmed      = 0
                      lau:Site_Location  = Master_Location
                      lau:Stock_Type     = lac1:Stock_Type
                      Set(lau:Main_Browse_Key,lau:Main_Browse_Key)
                      Loop
                          If Access:LoanAui.NEXT()
                             Break
                          End !If
                          If lau:Audit_Number   <> lmf:Audit_Number      |
                          Or lau:Confirmed      <> 0      |
                              Then Break.  ! End If
                          IF lau:Site_Location <> Master_Location
                            BREAK
                          END
                          IF lau:Stock_Type <> lac1:Stock_Type
                            BREAK
                          END
                          Access:Loan.Clearkey(loa:Ref_Number_Key)
                          loa:Ref_Number  = lau:Ref_Number
                          If Access:Loan.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                              !Found
                              loa:Available = 'AUS'
                              !Paul 02/06/2009 Log No 10684
                              loa:StatusChangeDate = today()
                              Access:Loan.Update()
                              get(loanhist,0)
                              if access:loanhist.primerecord() = Level:Benign
                                  loh:ref_number      = loa:ref_number
                                  loh:date            = Today()
                                  loh:time            = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password        = glo:password
                                  access:users.fetch(use:password_key)
                                  loh:user = use:user_code
                                  loh:status          = 'AUDIT SHORTAGE'
                                  access:loanhist.insert()
                              end!if access:loanhist.primerecord() = Level:Benign
      
                          Else! If Access:Loan.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:Loan.Tryfetch(loa:Ref_Number_Key) = Level:Benign
      
                      End !Loop
                      Access:LoanAui.RestoreFile(Save_eau_ID)
                    End
                    lmf:Complete_Flag = 1
                    lmf:Status = 'AUDIT COMPLETED'
                    Access:Loanamf.Update()
                  End!Case MessageEx
       Loan_Audit_report((lmf:audit_number))
              Of 1 ! No Button
          End ! Case Missive
      ELSE
          Case Missive('This stock audit is already marked as complete.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF CHOICE(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = lmf:Audit_Number
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 1
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Master_Location
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Master_Type
  ELSE
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = lmf:Audit_Number
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 0
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Master_Location
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = Master_Type
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  DO Workout
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
BrowseLoanAudit PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Yes_Byte             STRING('Y')
save_eau_id          USHORT,AUTO
Status_Info          STRING(20)
BRW1::View:Browse    VIEW(LOANAMF)
                       PROJECT(lmf:Audit_Number)
                       PROJECT(lmf:Date)
                       PROJECT(lmf:User)
                       PROJECT(lmf:Status)
                       PROJECT(lmf:Site_location)
                       PROJECT(lmf:Complete_Flag)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
lmf:Audit_Number       LIKE(lmf:Audit_Number)         !List box control field - type derived from field
lmf:Date               LIKE(lmf:Date)                 !List box control field - type derived from field
lmf:User               LIKE(lmf:User)                 !List box control field - type derived from field
lmf:Status             LIKE(lmf:Status)               !List box control field - type derived from field
lmf:Site_location      LIKE(lmf:Site_location)        !List box control field - type derived from field
lmf:Complete_Flag      LIKE(lmf:Complete_Flag)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::lmf:Complete_Flag    LIKE(lmf:Complete_Flag)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse Loan  Audits'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Loan Audit File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,100,276,224),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('56R(2)|M~Audit Number~L@n-14@49R(2)|M~Date~@d17@29R(2)|M~User~L@s3@120L(2)|M~Sta' &|
   'tus~@s30@0L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,164),USE(?ButtonNewAudit),TRN,FLAT,LEFT,ICON('newaud2p.jpg')
                       BUTTON,AT(448,208),USE(?ButtonContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                       BUTTON,AT(448,252),USE(?ButtonCompleteAudit),TRN,FLAT,HIDE,LEFT,ICON('compaudp.jpg')
                       BUTTON,AT(448,296),USE(?ButtonReprintAudit),TRN,FLAT,LEFT,ICON('prnaudp.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Loan Audit Table'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Completed Audits'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020478'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseLoanAudit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOAN.Open
  Relate:LOANALC.Open
  Relate:USERS.Open
  Access:LOANAUI.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOANAMF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if glo:WebJob = 0
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          glo:Default_Site_Location = use:Location
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  end !If glo:WebJob = 1
      ! Save Window Name
   AddToLog('Window','Open','BrowseLoanAudit')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,lmf:Secondary_Key)
  BRW1.AddRange(lmf:Complete_Flag)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,lmf:Audit_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,lmf:Secondary_Key)
  BRW1.AddRange(lmf:Complete_Flag)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,lmf:Audit_Number,1,BRW1)
  BRW1.AddField(lmf:Audit_Number,BRW1.Q.lmf:Audit_Number)
  BRW1.AddField(lmf:Date,BRW1.Q.lmf:Date)
  BRW1.AddField(lmf:User,BRW1.Q.lmf:User)
  BRW1.AddField(lmf:Status,BRW1.Q.lmf:Status)
  BRW1.AddField(lmf:Site_location,BRW1.Q.lmf:Site_location)
  BRW1.AddField(lmf:Complete_Flag,BRW1.Q.lmf:Complete_Flag)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOAN.Close
    Relate:LOANALC.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseLoanAudit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonNewAudit
      ! Create Header Reacord
      IF (Access:LOANAMF.PrimeRecord() = Level:Benign)
          lmf:Date = Today()
          lmf:Time = Clock()
          Access:USERS.Clearkey(use:Password_Key)
          use:Password = glo:Password
          IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
              lmf:User = use:User_Code
          END
          lmf:Complete_Flag = 0
          lmf:Status = 'AUDIT IN PROGRESS'
          lmf:Site_Location = glo:Default_Site_Location
          IF (Access:LOANAMF.TryUpdate())
              Access:LOANAMF.CancelAutoInc()
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('Unable to create a Loan Audit Entry.','ServiceBase',|
                             Icon:Hand,'&OK',1)
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      ELSE
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('Unable to create a Loan Audit Entry.','ServiceBase',|
                         Icon:Hand,'&OK',1)
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END ! IF (Access:LOANAMF.PrimeRecord() = Level:Benign)
      
      ! If location is set (and I'm sure it should be), use the key
      If (glo:Default_Site_Location <> '')
          Access:LOAN.Clearkey(loa:LocRefKey)
          loa:Location = glo:Default_Site_Location
          SET(loa:LocRefKey,loa:LocRefKey)
      ELSE
          Access:LOAN.Clearkey(loa:Ref_Number_Key)
          loa:Ref_Number = 1
          SET(loa:Ref_Number_Key,loa:Ref_Number_Key)
      END
      LOOP UNTIL Access:LOAN.Next()
          If (glo:Default_Site_Location <> '')
              If (loa:Location <> glo:Default_Site_Location)
                  BREAK
              END
          END
      
          ! Create Child Records Ready For Auditing
          IF (Access:LOANAUI.PrimeRecord() = Level:Benign)
              lau:Audit_Number = lmf:Audit_Number
              lau:Site_Location = loa:Location
              lau:Ref_Number = loa:Ref_Number
              !lau:Existsstoa:Original_Level = sto:Quantity_Stock
              lau:Exists = 'N'
              lau:Stock_Type = loa:Stock_Type
              lau:Shelf_Location = loa:Shelf_Location
              lau:Location = loa:Location
              lau:New_IMEI    = FALSE
              lau:IMEI_Number = loa:ESN
              IF (Access:LOANAUI.TryUpdate())
                  Access:LOANAUI.CancelAutoInc()
              END
          END ! If (Access:LOANAUI.PrimeRecord() = Level:Benign)
      END
      
      
      
      
      
      Loan_Check_Procedure()
      BRW1.REsetSort(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020478'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020478'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020478'&'0')
      ***
    OF ?ButtonContinueAudit
      ThisWindow.Update
      ThisWindow.Update()
      IF lmf:Complete_Flag = 0
      Loan_Check_Procedure()
      ELSE
          Case Missive('This loan audit is completed. Please start a new audit, or select an incomplete one.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
      BRW1.ResetSort(1)
    OF ?ButtonCompleteAudit
      ThisWindow.Update
      ThisWindow.Update()
      IF emf:Complete_Flag = 0
          Case Missive('Do you wish to complete the current stock audit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  !Lets check if we can!
                  !Anything left?
      
                  Save_eau_ID = Access:EXCHAUI.SaveFile()
                  Access:EXCHAUI.ClearKey(eau:Main_Browse_Key)
                  eau:Audit_Number   = emf:Audit_Number
                  eau:Confirmed      = 0
                  Set(eau:Main_Browse_Key,eau:Main_Browse_Key)
                  Loop
                      If Access:EXCHAUI.NEXT()
                         Break
                      End !If
                      If eau:Audit_Number   <> emf:Audit_Number      |
                      Or eau:Confirmed      <> 0      |
                          Then Break.  ! End If
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number  = eau:Ref_Number
                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Found
                          xch:Available = 'AUS'
                          xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                          Access:EXCHANGE.Update()
                          get(exchhist,0)
                          if access:exchhist.primerecord() = Level:Benign
                              exh:ref_number      = xch:ref_number
                              exh:date            = Today()
                              exh:time            = Clock()
                              access:users.clearkey(use:password_key)
                              use:password        = glo:password
                              access:users.fetch(use:password_key)
                              exh:user = use:user_code
                              exh:status          = 'AUDIT SHORTAGE'
                              access:exchhist.insert()
                          end!if access:exchhist.primerecord() = Level:Benign
      
                      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
                  End !Loop
                  Access:EXCHAUI.RestoreFile(Save_eau_ID)
      
                  emf:Complete_Flag = 1
                  Access:EXCHAMF.Update()
                  Case Missive('Report printed.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
      !            Stock_Audit_Report (STOM:Audit_No)
              Of 1 ! No Button
          End ! Case Missive
      ELSE
          Case Missive('The stock audit is already marked as complete.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
      
    OF ?ButtonReprintAudit
      ThisWindow.Update
      IF lmf:Complete_Flag = 0
        !Cannot continue!
          Case Missive('This audit is not complete. You cannot reprint the audit.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      ELSE
       Loan_Audit_report((lmf:audit_number))
          ThisWindow.Update()
      END
      BRW1.UpdateBuffer()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF CHOICE(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  IF glo:Default_Site_Location <> ''
    IF lmf:Site_location <> glo:Default_Site_Location
      RETURN Record:Filtered
    ELSE
      RETURN Record:OK
    END
  END
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


WIPExcludedStatuses PROCEDURE                         !Generated from procedure template - Browse

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:Tag1             STRING(1)
tmp:Tag2             STRING(1)
tmp:Location         STRING(30)
tmp:StatusDays       LONG
BRW5::View:Browse    VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag1               LIKE(tmp:Tag1)                 !List box control field - type derived from local data
tmp:Tag1_Icon          LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(WIPEXC)
                       PROJECT(wix:Status)
                       PROJECT(wix:WIPEXCID)
                       PROJECT(wix:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:Tag2               LIKE(tmp:Tag2)                 !List box control field - type derived from local data
tmp:Tag2_Icon          LONG                           !Entry's icon ID
wix:Status             LIKE(wix:Status)               !List box control field - type derived from field
wix:WIPEXCID           LIKE(wix:WIPEXCID)             !Primary key field - type derived from field
wix:Location           LIKE(wix:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Excluded Status'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Exclude Status Types'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,236,310),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Included Statuses'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location:'),AT(68,76),USE(?tmp:Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(125,76,124,10),USE(tmp:Location),SKIP,DISABLE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           BUTTON,AT(257,71),USE(?CallLookup),FLAT,ICON('lookupp.jpg')
                           PROMPT('Status Days:'),AT(68,92),USE(?tmp:StatusDays:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n-14),AT(125,92,47,11),USE(tmp:StatusDays),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH)
                           LIST,AT(68,112,160,248),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11LJ@s1@120L(2)~Status~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(232,120),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(232,152),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(232,184),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       PANEL,AT(304,54,72,310),USE(?Panel1),FILL(09A6A7CH)
                       SHEET,AT(380,54,236,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Excluded Statuses'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(384,74,160,286),USE(?List:2),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11LJ@s1@120L(2)~Status~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(548,120),USE(?DASTAG:2),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(548,152),USE(?DASTAGAll:2),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(548,186),USE(?DASUNTAGALL:2),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       STRING('Move to the'),AT(309,58),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       STRING('Excluded List'),AT(309,66),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(308,120),USE(?btnAddStatus),TRN,FLAT,RIGHT,ICON('addstap.jpg')
                       BUTTON,AT(308,330),USE(?btnRemoveStatus),TRN,FLAT,LEFT,ICON('remstap.jpg')
                       BUTTON('&Rev tags'),AT(336,400,50,13),USE(?DASREVTAG:2),DISABLE,HIDE
                       BUTTON('sho&W tags'),AT(392,400,50,13),USE(?DASSHOWTAG:2),DISABLE,HIDE
                       BUTTON('&Rev tags'),AT(224,400,50,13),USE(?DASREVTAG),DISABLE,HIDE
                       BUTTON('sho&W tags'),AT(280,400,50,13),USE(?DASSHOWTAG),DISABLE,HIDE
                       BUTTON,AT(548,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
AuditsInProgress    PROCEDURE(),LONG
    END ! MAP
!Save Entry Fields Incase Of Lookup
look:tmp:Location                Like(tmp:Location)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = sts:Status
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sts:Status
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag1 = 'Y'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag1 = ''
  END
    Queue:Browse.tmp:Tag1 = tmp:Tag1
  IF (tmp:Tag1 = 'Y')
    Queue:Browse.tmp:Tag1_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag1_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sts:Status
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = sts:Status
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sts:Status
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW7.UpdateBuffer
   glo:Queue2.Pointer2 = wix:WIPEXCID
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = wix:WIPEXCID
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:Tag2 = 'Y'
  ELSE
    DELETE(glo:Queue2)
    tmp:Tag2 = ''
  END
    Queue:Browse:1.tmp:Tag2 = tmp:Tag2
  IF (tmp:Tag2 = 'Y')
    Queue:Browse:1.tmp:Tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:Tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = wix:WIPEXCID
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::8:QUEUE = glo:Queue2
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue2)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer2 = wix:WIPEXCID
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = wix:WIPEXCID
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddTaggedStatus Routine

    Case Missive('This will move all the tagged status types to the excluded list.||Are you sure?',|
                 'ServiceBase 3g','mquest.jpg','\No|/Yes')

        Of 2 ! Yes Button

            IF (AuditsInProgress())
                EXIT
            END

              Prog.ProgressSetup(Records(glo:Queue))

              loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  if error() then break.
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()

                  Access:STATUS.Clearkey(sts:Status_Key)
                  sts:Status  = glo:Pointer
                  If Access:STATUS.Tryfetch(sts:Status_Key) = Level:Benign
                      !Found
                      Access:WIPEXC.Clearkey(wix:StatusTypeKey)
                      wix:Location = tmp:Location
                      wix:Status = sts:Status
                      If Access:WIPEXC.Tryfetch(wix:StatusTypeKey) = Level:Benign
                          !Found
                      Else
                          !Error
                          If Access:WIPEXC.PrimeRecord() = Level:Benign
                              wix:Location = tmp:Location
                              wix:Status = sts:Status
                              If Access:WIPEXC.TryUpdate() = Level:Benign
                                  !Insert Successful
                              Else !If Access:ACCSTAT.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:ACCSTAT.TryInsert() = Level:Benign
                          End !If Access:ACCSTAT.PrimeRecord() = Level:Benign
                      End !If Access:ACCSTAT.Tryfetch(acs:StatusKey) = Level:Benign
                  Else ! If Access:STATUS.Tryfetch(sta:Status_Key) = Level:Benign
                      !Error
                  End !If Access:STATUS.Tryfetch(sta:Status_Key) = Level:Benign
              End !Loop x# = 1 To Records(glo:Queue)

              Prog.ProgressFinish()
              Post(Event:Accepted,?DASUNTAGALL)

              BRW7.ResetSort(1)
              BRw5.ResetSort(1)
       
    End ! Case Missive
RemoveTaggedStatus Routine

    Case Missive('This will remove all the tagged statuses from the excluded list.||Are you sure?',|
                 'ServiceBase 3g','mquest.jpg','\No|/Yes')

        Of 2 ! Yes Button

            IF (AuditsInProgress())
                EXIT
            END

            Prog.ProgressSetup(Records(glo:queue2))

              Loop x# = 1 To Records(glo:Queue2)
                  Get(glo:Queue2,x#)

                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()

                  Access:WIPEXC.Clearkey(wix:WIPEXCKey)
                  wix:WIPEXCID = glo:Pointer2
                  If Access:WIPEXC.Fetch(wix:WIPEXCKey) = Level:Benign
                      !Found
                      Relate:WIPEXC.Delete(0)
                  Else ! If Access:ACCSTATS.Tryfetch(acs:RecordNumberKey) = Level:Benign
                      !Error
                  End !If Access:ACCSTATS.Tryfetch(acs:RecordNumberKey) = Level:Benign
              End !Loop x# = 1 To Records(glo:Queue2)

              Prog.ProgressFinish()
              Post(Event:Accepted,?DASUNTAGALL:2)

              BRW7.ResetSort(1)
              BRw5.ResetSort(1)
       
    End ! Case Missive

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020496'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WIPExcludedStatuses')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Relate:STATUS.Open
  Relate:WIPEXC.Open
  Access:USERS.UseFile
  Access:WIPAMF.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:STATUS,SELF)
  BRW7.Init(?List:2,Queue:Browse:1.ViewPosition,BRW7::View:Browse,Queue:Browse:1,Relate:WIPEXC,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:Location = glo:Default_Site_Location
  if clip(tmp:location) = ''
      !look it up
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.fetch(use:Password_Key) = Level:Benign
          !Found
          glo:userCode = use:User_Code
          glo:Default_Site_Location = use:Location
          tmp:Location = use:location
      End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  END
  tmp:StatusDays = getini(tmp:location,'StatusDays',0,clip(path())&'\WIPAudit.ini')
  
  IF (glo:WebJob = 1)
       ! #13432 Don't allow RRC to change location (DBH: 14/01/2015)
      ?CallLookup{prop:Hide} = 1
  END ! IF
      ! Save Window Name
   AddToLog('Window','Open','WIPExcludedStatuses')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Location{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,sts:Status_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,sts:Status,1,BRW5)
  BIND('tmp:Tag1',tmp:Tag1)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tmp:Tag1,BRW5.Q.tmp:Tag1)
  BRW5.AddField(sts:Status,BRW5.Q.sts:Status)
  BRW5.AddField(sts:Ref_Number,BRW5.Q.sts:Ref_Number)
  BRW7.Q &= Queue:Browse:1
  BRW7.AddSortOrder(,wix:StatusTypeKey)
  BRW7.AddRange(wix:Location,tmp:Location)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,wix:Status,1,BRW7)
  BIND('tmp:Tag2',tmp:Tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:Tag2,BRW7.Q.tmp:Tag2)
  BRW7.AddField(wix:Status,BRW7.Q.wix:Status)
  BRW7.AddField(wix:WIPEXCID,BRW7.Q.wix:WIPEXCID)
  BRW7.AddField(wix:Location,BRW7.Q.wix:Location)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  do DASBRW::6:DASUNTAGALL
  do DASBRW::8:DASUNTAGALL
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOCATION.Close
    Relate:STATUS.Close
    Relate:WIPEXC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','WIPExcludedStatuses')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSiteLocations
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?btnAddStatus
      do AddTaggedStatus
    OF ?btnRemoveStatus
      do RemoveTaggedStatus
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020496'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020496'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020496'&'0')
      ***
    OF ?tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loc:Location = tmp:Location
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      !Location has changed get the new status days
      tmp:StatusDays = getini(tmp:location,'StatusDays',0,clip(path())&'\WIPAudit.ini')
      display(?tmp:StatusDays)
      
      !renew the browses
      BRW7.ResetSort(1)
      BRw5.ResetSort(1)
    OF ?CallLookup
      ThisWindow.Update
      loc:Location = tmp:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:StatusDays
      !status days has been updated - save the new settings
      putiniext(tmp:location,'StatusDays',tmp:StatusDays,clip(path())&'\WIPAudit.ini')
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

AuditsInProgress    PROCEDURE()!,LONG
RetValue    LONG(FALSE)
    CODE
        ! #13432 Search for open audits for this site (DBH: 16/01/2015)
        Access:WIPAMF.ClearKey(wim:Secondary_Key)
        wim:Complete_Flag = 0
        SET(wim:Secondary_Key,wim:Secondary_Key)
        LOOP UNTIL Access:WIPAMF.Next() <> Level:Benign
            IF (wim:Complete_Flag <> 0)
                BREAK
            END ! IF
            IF (wim:Site_Location = tmp:Location)
                RetValue = TRUE
                BEEP(BEEP:SystemHand)  ;  YIELD()
                CASE Missive('Please complete/cancel any open or suspended audits for this site before changing available statuses.','ServiceBase',|
                               'mstop.jpg','/&OK') 
                OF 1 ! &OK Button
                END!CASE MESSAGE

                BREAK
            END ! IF
        END ! LOOP

        RETURN RetValue
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sts:Status
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag1 = ''
    ELSE
      tmp:Tag1 = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag1 = 'Y')
    SELF.Q.tmp:Tag1_Icon = 2
  ELSE
    SELF.Q.tmp:Tag1_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  Access:WIPEXC.CLearkey(wix:StatusTypeKey)
  wix:LOcation = tmp:Location
  wix:Status = sts:Status
  If (Access:WIPEXC.TryFetch(wix:StatusTypeKey) = Level:Benign)
      Return Record:Filtered
  end
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sts:Status
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = wix:WIPEXCID
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:Tag2 = ''
    ELSE
      tmp:Tag2 = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag2 = 'Y')
    SELF.Q.tmp:Tag2_Icon = 2
  ELSE
    SELF.Q.tmp:Tag2_Icon = 1
  END


BRW7.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = wix:WIPEXCID
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Browse
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
AdjustedWebOrderReceipts PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SiteLocation     STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:SiteLocation
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(RETSTOCK)
                       PROJECT(res:Ref_Number)
                       PROJECT(res:Part_Number)
                       PROJECT(res:Description)
                       PROJECT(res:Item_Cost)
                       PROJECT(res:Quantity)
                       PROJECT(res:QuantityReceived)
                       PROJECT(res:Amend_Reason)
                       PROJECT(res:Record_Number)
                       PROJECT(res:Amend_Site_Loc)
                       PROJECT(res:Amended)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
res:Ref_Number         LIKE(res:Ref_Number)           !List box control field - type derived from field
res:Part_Number        LIKE(res:Part_Number)          !List box control field - type derived from field
res:Description        LIKE(res:Description)          !List box control field - type derived from field
res:Item_Cost          LIKE(res:Item_Cost)            !List box control field - type derived from field
res:Quantity           LIKE(res:Quantity)             !List box control field - type derived from field
res:QuantityReceived   LIKE(res:QuantityReceived)     !List box control field - type derived from field
res:Amend_Reason       LIKE(res:Amend_Reason)         !List box control field - type derived from field
res:Record_Number      LIKE(res:Record_Number)        !Primary key field - type derived from field
res:Amend_Site_Loc     LIKE(res:Amend_Site_Loc)       !Browse key field - type derived from field
res:Amended            LIKE(res:Amended)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::res:Amend_Site_Loc   LIKE(res:Amend_Site_Loc)
HK9::res:Amended          LIKE(res:Amended)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB8::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Adjusted Web Order Receipts'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Adjusted Web Order Receipts'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(68,84,544,272),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('52L(2)|M~Sales Number~@s8@80L(2)|M~Part Number~@s30@124L(2)|M~Description~@s30@5' &|
   '6L(2)|M~Item Cost~@n14.2@42L(2)|M~Qty Req.~@n8@42L(2)|M~Qty Recvd.~@n8@1016L(2)|' &|
   'M~Note~@s254@'),FROM(Queue:Browse:1)
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Adjusted Web Order Receipts'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s20),AT(68,70,124,10),USE(tmp:SiteLocation),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           STRING('Site Location'),AT(196,70),USE(?String1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020461'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AdjustedWebOrderReceipts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:RETSTOCK.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RETSTOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF Glo:WebJob
    tmp:SiteLocation = glo:Default_Site_Location
    DISABLE(?tmp:SiteLocation)
  END
      ! Save Window Name
   AddToLog('Window','Open','AdjustedWebOrderReceipts')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,res:Amended_Receipt_Key)
  BRW1.AddRange(res:Amended)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,res:Ref_Number,1,BRW1)
  BRW1.AddField(res:Ref_Number,BRW1.Q.res:Ref_Number)
  BRW1.AddField(res:Part_Number,BRW1.Q.res:Part_Number)
  BRW1.AddField(res:Description,BRW1.Q.res:Description)
  BRW1.AddField(res:Item_Cost,BRW1.Q.res:Item_Cost)
  BRW1.AddField(res:Quantity,BRW1.Q.res:Quantity)
  BRW1.AddField(res:QuantityReceived,BRW1.Q.res:QuantityReceived)
  BRW1.AddField(res:Amend_Reason,BRW1.Q.res:Amend_Reason)
  BRW1.AddField(res:Record_Number,BRW1.Q.res:Record_Number)
  BRW1.AddField(res:Amend_Site_Loc,BRW1.Q.res:Amend_Site_Loc)
  BRW1.AddField(res:Amended,BRW1.Q.res:Amended)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB8.Init(tmp:SiteLocation,?tmp:SiteLocation,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder()
  FDCB8.AddField(loc:Location,FDCB8.Q.loc:Location)
  FDCB8.AddField(loc:RecordNumber,FDCB8.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:RETSTOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AdjustedWebOrderReceipts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020461'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020461'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020461'&'0')
      ***
    OF ?tmp:SiteLocation
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:SiteLocation
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = tmp:SiteLocation
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = 1
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateEXCHAMF PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::emf:Record  LIKE(emf:RECORD),STATIC
QuickWindow          WINDOW('Create New Audit'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Create New Audit'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Create New Audit'),USE(?Tab:1)
                           PROMPT('Audit Number'),AT(240,160),USE(?emf:Audit_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14b),AT(316,160,64,10),USE(emf:Audit_Number),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           PROMPT('Date'),AT(240,178),USE(?emf:Date:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(316,178,64,10),USE(emf:Date),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           PROMPT('Time'),AT(240,196),USE(?emf:Time:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14b),AT(316,196,64,10),USE(emf:Time),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           PROMPT('User Code'),AT(240,214),USE(?emf:User:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(316,214,64,10),USE(emf:User),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Status'),AT(240,232),USE(?emf:Status:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,232,124,10),USE(emf:Status),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a EXCHAMF Record'
  OF ChangeRecord
    ActionMessage = 'Changing a EXCHAMF Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020468'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateEXCHAMF')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:EXCHAMF)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(emf:Record,History::emf:Record)
  SELF.AddHistoryField(?emf:Audit_Number,1)
  SELF.AddHistoryField(?emf:Date,2)
  SELF.AddHistoryField(?emf:Time,3)
  SELF.AddHistoryField(?emf:User,4)
  SELF.AddHistoryField(?emf:Status,5)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:EXCHAMF.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EXCHAMF
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateEXCHAMF')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHAMF.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateEXCHAMF')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    emf:Date = TODAY()
    emf:Time = CLOCK()
    emf:Status = 'AUDIT CREATED'
    emf:Complete_Flag = 0
    Access:Users.ClearKey(use:password_key)
    use:Password = glo:Password
    IF Access:Users.Fetch(use:password_key)
      !Error!
      emf:User = 'UNK'
    ELSE
      emf:User = use:User_Code
    END
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020468'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020468'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020468'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Exchange_Audits PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(EXCHAMF)
                       PROJECT(emf:Audit_Number)
                       PROJECT(emf:Date)
                       PROJECT(emf:Time)
                       PROJECT(emf:User)
                       PROJECT(emf:Status)
                       PROJECT(emf:Complete_Flag)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
emf:Audit_Number       LIKE(emf:Audit_Number)         !List box control field - type derived from field
emf:Date               LIKE(emf:Date)                 !List box control field - type derived from field
emf:Time               LIKE(emf:Time)                 !List box control field - type derived from field
emf:User               LIKE(emf:User)                 !List box control field - type derived from field
emf:Status             LIKE(emf:Status)               !List box control field - type derived from field
emf:Complete_Flag      LIKE(emf:Complete_Flag)        !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Exchange Stock Audit Routine'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Exchange Stock Audit Routine'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,110,344,188),USE(?Browse:1),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('64R(2)|M~Audit Number~C(0)@n-14@80R(2)|M~Date~C(0)@d17@64R(2)|M~Time~C(0)@n-14@4' &|
   '0L(2)|M~User Code~L(2)@s3@80L(2)|M~Status~L(2)@s30@56R(2)|M~Complete Flag~C(0)@n' &|
   '3@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('Exchange Stock Audit Routine'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Audit Number'),AT(252,98),USE(?emf:Audit_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('newaud2p.jpg')
                           BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('contaudp.jpg'),DEFAULT
                           BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('remaudp.jpg')
                           ENTRY(@n-14b),AT(168,98,,10),USE(emf:Audit_Number),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020463'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Exchange_Audits')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHAMF.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCHAMF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Browse_Exchange_Audits')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,emf:Audit_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?emf:Audit_Number,emf:Audit_Number,1,BRW1)
  BRW1.AddField(emf:Audit_Number,BRW1.Q.emf:Audit_Number)
  BRW1.AddField(emf:Date,BRW1.Q.emf:Date)
  BRW1.AddField(emf:Time,BRW1.Q.emf:Time)
  BRW1.AddField(emf:User,BRW1.Q.emf:User)
  BRW1.AddField(emf:Status,BRW1.Q.emf:Status)
  BRW1.AddField(emf:Complete_Flag,BRW1.Q.emf:Complete_Flag)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHAMF.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Exchange_Audits')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateEXCHAMF
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020463'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020463'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020463'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?emf:Audit_Number
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

