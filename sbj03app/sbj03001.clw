

   MEMBER('sbj03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ03001.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
AllocateJob PROCEDURE (f_refNumber)                   !Generated from procedure template - Window

tmp:engineer         STRING(30)
tmp:Location         STRING(30)
tmp:status           STRING(30)
tmp:usercode         STRING(3)
tmp:DisableLocation  BYTE(0)
locAuditNotes        STRING(255)
window               WINDOW('Allocate Job'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Allocate Job'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Allocate Job'),USE(?Tab1)
                           PROMPT('Engineer'),AT(248,184),USE(?tmp:engineer:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(284,184,64,10),USE(tmp:engineer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Engineers'),TIP('Engineers'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(352,180),USE(?LookupEngineer),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Location'),AT(248,206),USE(?tmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(284,206,124,10),USE(tmp:Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Location'),TIP('Location'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(408,202),USE(?LookupLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Status'),AT(248,228),USE(?tmp:status:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(284,228,124,10),USE(tmp:status),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Status'),TIP('Status'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(408,224),USE(?LookupStatus),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(304,258),USE(?Button4),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:engineer                Like(tmp:engineer)
look:tmp:Location                Like(tmp:Location)
look:tmp:status                Like(tmp:status)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020471'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AllocateJob')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Access:USERS.UseFile
  Access:LOCINTER.UseFile
  Access:STATUS.UseFile
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  SELF.FilesOpened = True
  tmp:engineer    = Clip(GETINI('ALLOCATEJOB','Engineer',,CLIP(PATH())&'\CELRAPEN.INI'))
  tmp:location    = Clip(GETINI('ALLOCATEJOB','Location',,CLIP(PATH())&'\CELRAPEN.INI'))
  tmp:status      = Clip(GETINI('ALLOCATEJOB','Status',,CLIP(PATH())&'\CELRAPEN.INI'))
  tmp:usercode    = Clip(GETINI('ALLOCATEJOB','UserCode',,CLIP(PATH())&'\CELRAPEN.INI'))
  
  SET(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
      if tmp:DisableLocation
          ?tmp:Location{prop:Disable} = 1
          ?LookupLocation{prop:Disable} = 1
      else
          ?tmp:Location{prop:Hide} = 1
          ?tmp:Location:Prompt{prop:Hide} = 1
          ?LookupLocation{prop:Hide} = 1
      end
  End !def:HideLocation
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','AllocateJob')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:status{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?tmp:status{Prop:Tip}
  END
  IF ?tmp:status{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?tmp:status{Prop:Msg}
  END
  IF ?tmp:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  IF ?tmp:engineer{Prop:Tip} AND ~?LookupEngineer{Prop:Tip}
     ?LookupEngineer{Prop:Tip} = 'Select ' & ?tmp:engineer{Prop:Tip}
  END
  IF ?tmp:engineer{Prop:Msg} AND ~?LookupEngineer{Prop:Msg}
     ?LookupEngineer{Prop:Msg} = 'Select ' & ?tmp:engineer{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AllocateJob')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Users_Job_Assignment
      Browse_Available_Locations
      Browse_Status
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020471'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020471'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020471'&'0')
      ***
    OF ?tmp:engineer
      IF tmp:engineer OR ?tmp:engineer{Prop:Req}
        use:Surname = tmp:engineer
        use:User_Type = 'ENGINEER'
        use:Active = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:engineer        = tmp:engineer
        IF Access:USERS.TryFetch(use:User_Type_Active_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:engineer = use:Surname
            tmp:usercode = USE:User_Code
          ELSE
            CLEAR(tmp:usercode)
            CLEAR(use:User_Type)
            CLEAR(use:Active)
            !Restore Lookup On Error
            tmp:engineer = look:tmp:engineer
            SELECT(?tmp:engineer)
            CYCLE
          END
        ELSE
          tmp:usercode = USE:User_Code
        END
      END
      ThisWindow.Reset()
    OF ?LookupEngineer
      ThisWindow.Update
      use:Surname = tmp:engineer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:engineer = use:Surname
          Select(?+1)
      ELSE
          Select(?tmp:engineer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:engineer)
    OF ?tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loi:Location = tmp:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = tmp:Location
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:status
      IF tmp:status OR ?tmp:status{Prop:Req}
        sts:Status = tmp:status
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:status        = tmp:status
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:status = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            tmp:status = look:tmp:status
            SELECT(?tmp:status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = tmp:status
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:status = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:status)
    OF ?Button4
      ThisWindow.Update
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = f_refnumber
      If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
          !Found
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_number
          If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
              !Found
      
          Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
          If tmp:Status <> ''
              GetStatus(Sub(Clip(tmp:status),1,3),1,'JOB')
          End!If tmp:Status <> ''
          If tmp:Location <> ''
              !Add To Old Location
                  If job:location <> ''
                      access:locinter.clearkey(loi:location_key)
                      loi:location = job:location
                      If access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces+= 1
                              loi:location_available = 'YES'
                              access:locinter.update()
                          End
                      end !if
                  End!If job:location <> ''
              !Take From New Location
                  job:location = tmp:Location
      
                  access:locinter.clearkey(loi:location_key)
                  loi:location = job:location
                  If access:locinter.fetch(loi:location_key) = Level:Benign
                      If loi:allocate_spaces = 'YES'
                          loi:current_spaces -= 1
                          If loi:current_spaces< 1
                              loi:current_spaces = 0
                              loi:location_available = 'NO'
                          End
                          access:locinter.update()
                      End
                  end !if
      
          End!If tmp:Location <> ''
      
          IF tmp:usercode = ''
            IF job:engineer <> ''
                Case Missive('You have not entered a User code. There is currently an engineer allocated to this job. Do you wish to REMOVE this engineer from the job?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                         job:engineer    = tmp:usercode
                    Of 1 ! No Button
                End ! Case Missive
            END
          ELSE
            job:engineer    = tmp:usercode
              Access:USERS.Clearkey(use:User_Code_Key)
              use:User_Code   = job:Engineer
              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Found
      
              Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
          END
          access:jobs.update()
          If tmp:Usercode <> '' Or tmp:Location <> '' Or tmp:Status <> ''
              locAuditNotes         = 'FIELDS UPDATED:'
              If tmp:Usercode <> ''
                  locAuditNotes       = Clip(locAuditNotes) & '<13,10>ENGINEER: ' & CLIP(tmp:Usercode) & |
                                      '<13,10>SKILL LEVEL: ' & Clip(use:SkillLevel)
              End!If tmp:Usercode <> ''
              If tmp:Location <> ''
                  locAuditNotes       = Clip(locAuditNotes) & '<13,10>LOCATION: ' & Clip(tmp:Location)
              End!If tmp:Location <> ''
              If tmp:Status <> ''
                  locAuditNotes       = Clip(locAuditNotes) & '<13,10>STATUS: ' & CLip(tmp:Status)
              End!If tmp:Status <> ''
      
              IF (AddToAudit(job:Ref_Number,'JOB','RAPID ENGINEER UPDATE: JOB ALLOCATED',locAuditNotes))
              END ! IF
      
          End!If tmp:Usercode <> '' Or tmp:Location <> '' Or tmp:Status <> ''
      
          If Access:JOBSENG.PrimeRecord() = Level:Benign
              joe:JobNumber     = job:Ref_Number
              joe:UserCode      = job:Engineer
              joe:DateAllocated = Today()
              joe:TimeAllocated = Clock()
              joe:AllocatedBy   = use:User_Code
              access:users.clearkey(use:User_Code_Key)
              use:User_Code   = job:Engineer
              access:users.fetch(use:User_Code_Key)
      
              joe:EngSkillLevel = use:SkillLevel
              joe:JobSkillLevel = jobe:SkillLevel
      
              If Access:JOBSENG.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:JOBSENG.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:JOBSENG.TryInsert() = Level:Benign
          End !If Access:JOBSENG.PrimeRecord() = Level:Benign
      
      
          PUTINI('ALLOCATEJOB','Engineer',tmp:Engineer,CLIP(PATH()) & '\CELRAPEN.INI')
          PUTINI('ALLOCATEJOB','Location',tmp:Location,CLIP(PATH()) & '\CELRAPEN.INI')
          PUTINI('ALLOCATEJOB','Status',tmp:Status,CLIP(PATH()) & '\CELRAPEN.INI')
          PUTINI('ALLOCATEJOB','UserCode',tmp:UserCode,CLIP(PATH()) & '\CELRAPEN.INI')
          Post(event:closewindow)
      Else! If access:.tryfetch(job:ref_number_key) = Level:Benign
          !Error
      End! If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Browse_Status PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:yes              STRING('YES')
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                       PROJECT(sts:Exchange)
                       PROJECT(sts:Loan)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
sts:Exchange           LIKE(sts:Exchange)             !Browse key field - type derived from field
sts:Loan               LIKE(sts:Loan)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Status Type File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Status Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(260,114,164,210),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Status~L(2)@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Status'),USE(?Tab:2)
                           ENTRY(@s30),AT(260,100,124,10),USE(sts:Status),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'JOB'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'EXC'
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'LOA'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020464'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Status')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STATUS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STATUS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Browse_Status')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sts:JobKey)
  BRW1.AddRange(sts:Job,tmp:yes)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sts:Status,sts:Status,1,BRW1)
  BRW1.AddSortOrder(,sts:ExchangeKey)
  BRW1.AddRange(sts:Exchange,tmp:yes)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sts:Status,sts:Status,1,BRW1)
  BRW1.AddSortOrder(,sts:LoanKey)
  BRW1.AddRange(sts:Loan,tmp:yes)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?sts:Status,sts:Status,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sts:Status_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sts:Status,sts:Status,1,BRW1)
  BRW1.AddField(sts:Status,BRW1.Q.sts:Status)
  BRW1.AddField(sts:Ref_Number,BRW1.Q.sts:Ref_Number)
  BRW1.AddField(sts:Job,BRW1.Q.sts:Job)
  BRW1.AddField(sts:Exchange,BRW1.Q.sts:Exchange)
  BRW1.AddField(sts:Loan,BRW1.Q.sts:Loan)
  QuickWindow{PROP:MinWidth}=248
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATUS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Status')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020464'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020464'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020464'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sts:Status
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'JOB'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'EXC'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'LOA'
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STS:Status, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

Allocate_JobSelf PROCEDURE (JobNumber)                !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Temp_JobNumber       REAL
window               WINDOW('Allocate Job'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Allocate Job'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Allocate Job'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(248,206),USE(?Temp_JobNumber:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n10b),AT(324,206,64,10),USE(Temp_JobNumber),LEFT(2),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020472'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Allocate_JobSelf')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Allocate_JobSelf')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Allocate_JobSelf')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CancelButton
      JobNumber = 0
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020472'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020472'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020472'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      JobNumber = Temp_JobNumber
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Engineer_Job PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
tmp:WobNumber        STRING(15)
save_rapl_id         USHORT,AUTO
save_job_id          USHORT,AUTO
save_joe_id          USHORT,AUTO
tmp:CommonRef        LONG
engineer_ref_number_temp REAL
main_store_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_quantity_temp REAL
main_store_sundry_temp STRING(3)
engineer_sundry_temp STRING(3)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
ref_number_temp      REAL
serial_number_temp   STRING(30)
common_fault_temp    STRING(30)
tmp:return           STRING(20)
JobsListQueue        QUEUE,PRE(jobque)
JobNumber            LONG
DateAllocated        DATE
ReportedFault        STRING(50)
JobStatus            STRING(30)
                     END
tmp:tag              STRING(1)
tmp:TotalRecords     LONG
tmp:NewUserCode      STRING(30)
tmp:ReturnedJob      STRING(20)
wob_number_temp      STRING(8)
tmp:estimate_accepted BYTE
tmp:OldEngineer      STRING(30)
tmp:CurrentUserCode  STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(RAPENGLS)
                       PROJECT(rapl:JobNumber)
                       PROJECT(rapl:DateAllocated)
                       PROJECT(rapl:TimeAllocated)
                       PROJECT(rapl:ReportedFault)
                       PROJECT(rapl:JobStatus)
                       PROJECT(rapl:SkillLevel)
                       PROJECT(rapl:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_NormalFG       LONG                           !Normal forground color
tmp:tag_NormalBG       LONG                           !Normal background color
tmp:tag_SelectedFG     LONG                           !Selected forground color
tmp:tag_SelectedBG     LONG                           !Selected background color
tmp:tag_Icon           LONG                           !Entry's icon ID
rapl:JobNumber         LIKE(rapl:JobNumber)           !List box control field - type derived from field
rapl:JobNumber_NormalFG LONG                          !Normal forground color
rapl:JobNumber_NormalBG LONG                          !Normal background color
rapl:JobNumber_SelectedFG LONG                        !Selected forground color
rapl:JobNumber_SelectedBG LONG                        !Selected background color
tmp:WobNumber          LIKE(tmp:WobNumber)            !List box control field - type derived from local data
tmp:WobNumber_NormalFG LONG                           !Normal forground color
tmp:WobNumber_NormalBG LONG                           !Normal background color
tmp:WobNumber_SelectedFG LONG                         !Selected forground color
tmp:WobNumber_SelectedBG LONG                         !Selected background color
rapl:DateAllocated     LIKE(rapl:DateAllocated)       !List box control field - type derived from field
rapl:DateAllocated_NormalFG LONG                      !Normal forground color
rapl:DateAllocated_NormalBG LONG                      !Normal background color
rapl:DateAllocated_SelectedFG LONG                    !Selected forground color
rapl:DateAllocated_SelectedBG LONG                    !Selected background color
rapl:TimeAllocated     LIKE(rapl:TimeAllocated)       !List box control field - type derived from field
rapl:TimeAllocated_NormalFG LONG                      !Normal forground color
rapl:TimeAllocated_NormalBG LONG                      !Normal background color
rapl:TimeAllocated_SelectedFG LONG                    !Selected forground color
rapl:TimeAllocated_SelectedBG LONG                    !Selected background color
rapl:ReportedFault     LIKE(rapl:ReportedFault)       !List box control field - type derived from field
rapl:ReportedFault_NormalFG LONG                      !Normal forground color
rapl:ReportedFault_NormalBG LONG                      !Normal background color
rapl:ReportedFault_SelectedFG LONG                    !Selected forground color
rapl:ReportedFault_SelectedBG LONG                    !Selected background color
rapl:JobStatus         LIKE(rapl:JobStatus)           !List box control field - type derived from field
rapl:JobStatus_NormalFG LONG                          !Normal forground color
rapl:JobStatus_NormalBG LONG                          !Normal background color
rapl:JobStatus_SelectedFG LONG                        !Selected forground color
rapl:JobStatus_SelectedBG LONG                        !Selected background color
rapl:SkillLevel        LIKE(rapl:SkillLevel)          !List box control field - type derived from field
rapl:SkillLevel_NormalFG LONG                         !Normal forground color
rapl:SkillLevel_NormalBG LONG                         !Normal background color
rapl:SkillLevel_SelectedFG LONG                       !Selected forground color
rapl:SkillLevel_SelectedBG LONG                       !Selected background color
tmp:estimate_accepted  LIKE(tmp:estimate_accepted)    !List box control field - type derived from local data
tmp:estimate_accepted_NormalFG LONG                   !Normal forground color
tmp:estimate_accepted_NormalBG LONG                   !Normal background color
tmp:estimate_accepted_SelectedFG LONG                 !Selected forground color
tmp:estimate_accepted_SelectedBG LONG                 !Selected background color
rapl:RecordNumber      LIKE(rapl:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Rapid Engineer Update'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Rapid Engineer Update'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,28,672,354),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Tab2)
                           PROMPT('Jobs Allocated'),AT(8,34),USE(?JobsAllocated),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(156,215,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(208,215,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(404,350),USE(?View_Job),TRN,FLAT,LEFT,ICON('viewjobp.jpg')
                           BUTTON,AT(472,350),USE(?EstimatePartsButton),TRN,FLAT,HIDE,LEFT,ICON('addestp.jpg')
                           BUTTON,AT(8,350),USE(?DASTAG),TRN,FLAT,LEFT,FONT(,,,,CHARSET:ANSI),ICON('tagitemp.jpg')
                           BUTTON,AT(76,350),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(144,350),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BOX,AT(388,86,8,8),USE(?Box1),COLOR(COLOR:Green),FILL(COLOR:Green)
                           STRING('Estimate Accepted'),AT(400,34),USE(?String2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(216,350),USE(?OkButton),TRN,FLAT,LEFT,ICON('refreshp.jpg')
                           BUTTON,AT(540,350),USE(?Button15),TRN,FLAT,LEFT,KEY(F3Key),ALRT(F3Key),ICON('alljobp.jpg')
                           BUTTON,AT(608,350),USE(?ReAllocate),TRN,FLAT,LEFT,ICON('realjobp.jpg')
                           PROMPT('Total Records:'),AT(568,34),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(632,34),USE(tmp:TotalRecords),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(92,46),USE(?rapl:JobNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(20,46,64,10),USE(rapl:JobNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                         END
                       END
                       LIST,AT(8,60,664,286),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)|*I@s1@0L(2)|M*@s8@63R(2)|M*~Job No~L@s15@52R(2)|M*~Date Allocated~@d6@52R' &|
   '(2)|M*~Time Allocated~@t1b@320L(2)|M*~Reported Fault~@s255@125L(2)|M*~Job Status' &|
   '~@s30@32L(2)|M*~Level~@s2@0L(2)|M*~tmp : estimate accepted~@n3@'),FROM(Queue:Browse)
                       BUTTON,AT(608,384),USE(?CancelButton),TRN,FLAT,LEFT,ICON('finishp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = rapl:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = rapl:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  Queue:Browse.tmp:tag_NormalFG = -1
  Queue:Browse.tmp:tag_NormalBG = -1
  Queue:Browse.tmp:tag_SelectedFG = -1
  Queue:Browse.tmp:tag_SelectedBG = -1
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 1
  ELSE
    Queue:Browse.tmp:tag_Icon = 0
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = rapl:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = rapl:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = rapl:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
BuildJobsList       Routine
    Setcursor(Cursor:Wait)
    Access:USERS.ClearKey(use:password_key)
    use:Password = glo:Password
    If Access:USERS.TryFetch(use:password_key) = Level:Benign
        !Found

    Else!If Access:USERS.TryFetch(use:password_key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:USERS.TryFetch(use:password_key) = Level:Benign

    !Remove all records from rapengls
    Save_rapl_ID = Access:RAPENGLS.SaveFile()
    Set(rapl:RecordNumberKey)
    Loop
        If Access:RAPENGLS.NEXT()
           Break
        End !If
        Delete(RAPENGLS)
    End !Loop

    Access:RAPENGLS.RestoreFile(Save_rapl_ID)

    Save_job_ID = Access:JOBS.SaveFile()
    Access:JOBS.ClearKey(job:EngDateCompKey)
    job:Engineer       = use:User_Code
    job:Date_Completed = 0
    Set(job:EngDateCompKey,job:EngDateCompKey)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:Engineer       <> use:User_Code      |
        Or job:Date_Completed <> 0      |
            Then Break.  ! End If
        Access:STATUS.ClearKey(sts:Ref_Number_Only_Key)
        sts:Ref_Number = Sub(job:Current_Status,1,3)
        If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
            !Found
            If sts:EngineerStatus
                Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                jbn:RefNumber = job:Ref_Number
                If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                    !Found
                End!If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number

                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found

                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                if glo:webjob then
                   access:webjob.clearkey(wob:RefNumberKey)
                   wob:refNUmber = job:ref_number
                   if access:webjob.fetch(wob:refNumberKey) = level:benign
                       If Access:RAPENGLS.PrimeRecord() = Level:Benign
                                rapl:JobNumber      = job:Ref_Number
                                rapl:DateAllocated  = job:Date_Booked
                                rapl:TimeAllocated  = job:Time_Booked
                                IF INSTRING('606',job:current_status,1)
                                  rapl:ReportedFault  = 'QA Required on this unit.'
                                ELSE
                                  rapl:ReportedFault  = jbn:Fault_Description
                                END
                                rapl:JobStatus      = job:Current_Status
                                rapl:SkillLevel     = jobe:SkillLevel
                            If Access:RAPENGLS.TryInsert() = Level:Benign
                                !Insert Successful
                            End !If Access:RAPENGLS.TryInsert() = Level:Benign
                        End !If Access:RAPENGLS.PrimeRecord() = Level:Benign
                   END !If access webjob
                ELSE   !if not webjob
                   If Access:RAPENGLS.PrimeRecord() = Level:Benign
                            rapl:JobNumber      = job:Ref_Number
                            rapl:DateAllocated  = job:Date_Booked
                            rapl:TimeAllocated  = job:Time_Booked
                            IF INSTRING('606',job:current_status,1)
                              rapl:ReportedFault  = 'QA Required on this unit.'
                            ELSE
                              rapl:ReportedFault  = jbn:Fault_Description
                            END
                            rapl:JobStatus      = job:Current_Status
                            rapl:SkillLevel     = jobe:SkillLevel
                        If Access:RAPENGLS.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:RAPENGLS.TryInsert() = Level:Benign
                            !Insert Failed
                        End !If Access:RAPENGLS.TryInsert() = Level:Benign
                    End !If Access:RAPENGLS.PrimeRecord() = Level:Benign
                END !if glo:webjob

            End !If sts:EngineerStatus
        Else!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:STATUS.TryFetch(sts:Ref_Number_Only_Key) = Level:Benign
    End !Loop
    Access:JOBS.RestoreFile(Save_job_ID)
    tmp:TotalRecords    = Records(RAPENGLS)
    Setcursor()
Fill_Lists      Routine
Pricing         Routine
stock_history       Routine        !Do The Prime, and Set The Quantity First
Allocate_To_Self    ROUTINE

    error_alloc# = 0
    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !Found
        !I think the engineer is being lost when calling the password screen.
        !So save the current user code and compare that - L832 (DBH: 08-07-2003)
        tmp:CurrentUserCode = use:User_Code
        If InsertEngineerPassword() <> tmp:CurrentUserCode
            Case Missive('Incorrect Password.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            error_alloc# = 1
        End !If InsertEngineerPassword() <> use:User_Code
    Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign

    If error_alloc# = 0
        tmp:NewUserCode = use:User_Code

        If tmp:NewUserCode <> ''
            !Save Old Engineer - L832 (DBH: 08-07-2003)
            tmp:OldEngineer  = job:Engineer
            job:Engineer    = tmp:NewUserCode

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found

            Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


            GetStatus(Sub(Clip('310 ALLOCATED TO ENGINEER'),1,3),1,'JOB')
            If Access:JOBS.TryUpdate() = Level:Benign
                Access:USERS.Clearkey(use:User_Code_Key)
                use:User_Code   = tmp:NewUserCode
                If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Found

                Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

                IF (AddToAudit(job:Ref_Number,'JOB','RAPID ENGINEER UPDATE: JOB REALLOCATED','ENGINEER: ' & Clip(use:Forename) & ' ' & Clip(use:Surname)))
                END ! IF

                If glo:WebJob and job:Date_Completed <> '' And SentToHub(job:ref_Number)
                    !The engineer is being changed at the RRC after the job has come back from hub
                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                        joe:JobNumber     = job:Ref_Number
                        joe:UserCode      = job:Engineer
                        joe:DateAllocated = Today()
                        joe:TimeAllocated = Clock()
                        joe:AllocatedBy   = use:User_Code
                        joe:Status        = 'ENGINEER QA'
                        joe:StatusDate    = Today()
                        joe:StatusTime    = Clock()
                        access:users.clearkey(use:User_Code_Key)
                        use:User_Code   = job:Engineer
                        access:users.fetch(use:User_Code_Key)

                        joe:EngSkillLevel = use:SkillLevel
                        joe:JobSkillLevel = jobe:SkillLevel

                        If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Failed
                        End !If Access:JOBSENG.TryInsert() = Level:Benign
                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                Else !If glo:WebJob and job:Date_Completed <> ''
                    !Lookup current engineer and mark as escalated
                    If tmp:OldEngineer <> ''
                        Save_joe_ID = Access:JOBSENG.SaveFile()
                        Access:JOBSENG.ClearKey(joe:UserCodeKey)
                        joe:JobNumber     = job:Ref_Number
                        joe:UserCode      = tmp:OldEngineer
                        joe:DateAllocated = Today()
                        Set(joe:UserCodeKey,joe:UserCodeKey)
                        Loop
                            If Access:JOBSENG.PREVIOUS()
                               Break
                            End !If
                            If joe:JobNumber     <> job:Ref_Number       |
                            Or joe:UserCode      <> tmp:OldEngineer      |
                            Or joe:DateAllocated > Today()       |
                                Then Break.  ! End If
                            If joe:Status = 'ALLOCATED'
                                joe:Status = 'ESCALATED'
                                joe:StatusDate = Today()
                                joe:StatusTime = Clock()
                                Access:JOBSENG.Update()
                            End !If joe:Status = 'ALLOCATED'
                            Break
                        End !Loop
                        Access:JOBSENG.RestoreFile(Save_joe_ID)
                    End !If tmp:OldEngineer <> ''

                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                        joe:JobNumber     = job:Ref_Number
                        joe:UserCode      = job:Engineer
                        joe:DateAllocated = Today()
                        joe:TimeAllocated = Clock()
                        joe:AllocatedBy   = use:User_Code
                        joe:Status        = 'ALLOCATED'
                        joe:StatusDate    = Today()
                        joe:StatusTime    = Clock()
                        access:users.clearkey(use:User_Code_Key)
                        use:User_Code   = job:Engineer
                        access:users.fetch(use:User_Code_Key)

                        joe:EngSkillLevel = use:SkillLevel
                        joe:JobSkillLevel = jobe:SkillLevel

                        If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:JOBSENG.TryInsert() = Level:Benign
                            !Insert Failed
                        End !If Access:JOBSENG.TryInsert() = Level:Benign
                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                End !If glo:WebJob and job:Date_Completed <> ''
                Delete(RAPENGLS)

            End !If Access:JOBS.TryUpdate() = Level:Benign

        End !tmp:NewUserCode <> ''

        tmp:TotalRecords    = Records(RAPENGLS)
        Display()

    End !error_alloc# = 0

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020476'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Engineer_Job')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COMMONFA.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:RAPENGLS.Open
  Relate:STAHEAD.Open
  Relate:USERS_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:JOBNOTES.UseFile
  Access:CHARTYPE.UseFile
  Access:ESTPARTS.UseFile
  Access:STDCHRGE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:JOBSE.UseFile
  Access:COURIER.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSENG.UseFile
  Access:JOBSE2.UseFile
  Access:CONTHIST.UseFile
  SELF.FilesOpened = True
  Do BuildJobsList
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:RAPENGLS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Access:USERS.ClearKey(use:password_key)
  use:Password = glo:Password
  If Access:USERS.TryFetch(use:password_key) = Level:Benign
      !Found
      ?JobsAllocated{prop:Text} = 'Jobs Allocated To: ' & Clip(use:Forename) & ' ' & Clip(use:Surname) &|
                          '  -  Skill Level: ' & Clip(use:SkillLevel)
  Else!If Access:USERS.TryFetch(use:password_key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:USERS.TryFetch(use:password_key) = Level:Benign
  
  
      ! Save Window Name
   AddToLog('Window','Open','Update_Engineer_Job')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,rapl:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?rapl:JobNumber,rapl:JobNumber,1,BRW1)
  BIND('tmp:tag',tmp:tag)
  BIND('tmp:WobNumber',tmp:WobNumber)
  BIND('tmp:estimate_accepted',tmp:estimate_accepted)
  ?List{PROP:IconList,1} = '~bluetick.ico'
  BRW1.AddField(tmp:tag,BRW1.Q.tmp:tag)
  BRW1.AddField(rapl:JobNumber,BRW1.Q.rapl:JobNumber)
  BRW1.AddField(tmp:WobNumber,BRW1.Q.tmp:WobNumber)
  BRW1.AddField(rapl:DateAllocated,BRW1.Q.rapl:DateAllocated)
  BRW1.AddField(rapl:TimeAllocated,BRW1.Q.rapl:TimeAllocated)
  BRW1.AddField(rapl:ReportedFault,BRW1.Q.rapl:ReportedFault)
  BRW1.AddField(rapl:JobStatus,BRW1.Q.rapl:JobStatus)
  BRW1.AddField(rapl:SkillLevel,BRW1.Q.rapl:SkillLevel)
  BRW1.AddField(tmp:estimate_accepted,BRW1.Q.tmp:estimate_accepted)
  BRW1.AddField(rapl:RecordNumber,BRW1.Q.rapl:RecordNumber)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COMMONFA.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:RAPENGLS.Close
    Relate:STAHEAD.Close
    Relate:USERS_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Engineer_Job')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button15
      tmp:ReturnedJob = ''
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020476'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020476'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020476'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?View_Job
      ThisWindow.Update
          Serial_Number_Temp = InsertSerialNumber()
          If Serial_Number_Temp <> ''
              Ref_Number_Temp = brw1.q.rapl:JobNumber
      
              error# = 0
              !Neil!
              !Require User Code!
              Access:Users_Alias.ClearKey(use_ali:password_key)
              use_ali:Password = glo:Password
              IF Access:Users_Alias.Fetch(use_ali:password_key)
                !Error!
              END
              If error# = 0
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number   = ref_number_temp
                  If access:jobs.fetch(job:ref_number_key)
                      Case Missive('Unable to find selected job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If access:jobs.fetch(job:ref_number_key)
                      !Added by Neil!
                      If job:Engineer <> use_ali:User_Code
                         !Security Check!
                          If SecurityCheck('RAPID ENG - ALLOCATE JOB')
                              Case Missive('Warning! This job is allocated to another engineer.'&|
                                '<13,10>'&|
                                '<13,10>You do not have sufficient access to edit this job.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                                    Error# = 2
                          Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
                              Case Missive('Warning! This job is allocated to another engineer.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to reallocate it to yourself?','ServiceBase 3g',|
                                             'mquest.jpg','\No|/Yes')
                                  Of 2 ! Yes Button
                                      !Allocation code - ask Bryan
                                      DO Allocate_to_Self
                                      Do BuildJObsList
                                      Error# = 1
                                  Of 1 ! No Button
                                      error# = 1
                              End ! Case Missive
                        End
                      End!If job:date_completed <> ''
                      If job:date_completed <> '' AND Error# = 0
                          Case Missive('Warning! This job has been completed.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue.','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                              Of 1 ! No Button
                                  error# = 1
                          End ! Case Missive
                      End!If job:date_completed <> ''
      
                      If job:workshop <> 'YES' and error# = 0
                          Case Missive('This job is not in the workshop.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      End!If job:workshop <> 'YES' and erro# = 0
      
                      If serial_number_temp <> job:esn
                          If job:exchange_unit_number <> ''
                              access:exchange.clearkey(xch:ref_number_key)
                              xch:ref_number = job:exchange_unit_number
                              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                                  If xch:esn = serial_number_temp
                                      Case Missive('This job is not in the workshop.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                      error# = 1
                                  Else!If xch:esn = serial_number_temp
                                      Case Missive('The selected Serial Number does not match the selected Job Number.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                      error# = 1
                                  End!If xch:esn = serial_number_temp
                              End!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                          Else!If job:exchange_unit_number <> ''
                              Case Missive('The selected Serial Number does not match the selected Job Number.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              error# = 1
                          End!If job:exchange_unit_number <> ''
                      End!If serial_number_temp <> job:esn
      
                      If error# = 0
                          If job:Chargeable_Job = 'YES' And job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' Or job:Estimate_Rejected <> 'YES')
                              Case Missive('The selected job requires an Estimate.','ServiceBase 3g',|
                                             'mexclam.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Unhide(?EstimatePartsButton)
                          End!If job:Estimate = 'YES' and (job:Estimate_Required <> 'YES' And job:Estimate_Refused <> 'YES')
      
                      End!If error# = 0
      
                  End!If access:jobs.fetch(job:ref_number_key)
              End!If error# = 0
              !Automatically amend status!
              IF Error# = 0
                CASE job:Current_Status
                  OF '310 ALLOCATED TO ENGINEER'
                    GetStatus(Sub(Clip('315 IN REPAIR'),1,3),1,'JOB')
                    Access:JOBS.TryUpdate()
                  OF '535 ESTIMATE ACCEPTED'
                    GetStatus(Sub(Clip('315 IN REPAIR'),1,3),1,'JOB')
                    Access:JOBS.TryUpdate()
                  OF '540 ESTIMATE REFUSED'
                    GetStatus(Sub(Clip('315 IN REPAIR'),1,3),1,'JOB')
                    Access:JOBS.TryUpdate()
                  OF '320 ON TEST'
                    GetStatus(Sub(Clip('315 IN REPAIR'),1,3),1,'JOB')
                    Access:JOBS.TryUpdate()
                  OF '345 SPARES RECEIVED'
                    GetStatus(Sub(Clip('315 IN REPAIR'),1,3),1,'JOB')
                    Access:JOBS.TryUpdate()
                  OF '420 RETURNED FROM 3RD PARTY'
                    GetStatus(Sub(Clip('315 IN REPAIR'),1,3),1,'JOB')
                    Access:JOBS.TryUpdate()
                END
                IF CLIP(SUB(job:Current_Status,1,3)) <> '606'
                  !?Button16{PROP:Hide} = TRUE
                ELSE
                  !?Button16{PROP:Hide} = FALSE
                END
              END
      
              If Error# = 0
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = ref_number_temp
                  If access:jobs.fetch(job:ref_number_key) = Level:Benign
                      saverequest# = globalrequest
                      globalrequest = changerecord
                      update_jobs_rapid
                      globalrequest = saverequest#
                      !Neil!
                      !Brw1.ResetSort(1) !Removed at Bryans Request.
                      !Try This
                      POST(Event:Accepted,?OkButton)
                  Else!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Case Missive('Unable to find the selected job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      
              End !If Error# = 0
          End !If Serial_Number_Temp <> ''
    OF ?EstimatePartsButton
      ThisWindow.Update
       BrowseEstimateParts((Ref_Number_Temp))
      If job:Estimate_Ready <> 'YES'
          job:Estimate_Ready = 'YES'
          GetStatus(510,0,'JOB')
          Access:JOBS.Update()
      End!If job:Estimate_Ready <> 'YES'
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ref_number_temp = ''
      serial_number_temp = ''
      Hide(?EstimatePartsButton)
      Display()
      Do BuildJobsList
      BRW1.ResetSort(1)
    OF ?Button15
      ThisWindow.Update
      Allocate_JobSelf(tmp:ReturnedJob)
      ThisWindow.Reset
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number   = tmp:ReturnedJob
              If access:jobs.fetch(job:ref_number_key)
                  Case Missive('Unable to find selected job.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If access:jobs.fetch(job:ref_number_key)
      
                  !TB12540 - Show contact history if there is a sticky note
                  !need to find one that has not been cancelled, and is valid for this request
                  Access:conthist.clearkey(cht:KeyRefSticky)
                  cht:Ref_Number = tmp:ReturnedJob
                  cht:SN_StickyNote = 'Y'
                  Set(cht:KeyRefSticky,cht:KeyRefSticky)      
                  Loop
                      if access:Conthist.next() then break.
                      IF cht:Ref_Number <> tmp:ReturnedJob then break.
                      if cht:SN_StickyNote <> 'Y' then break.
                      if cht:SN_Completed <> 'Y' and cht:SN_EngAlloc = 'Y' then
                          glo:select12 = tmp:ReturnedJob
                          Browse_Contact_History
                          BREAK
                      END
                  END
      
                  !Added by Neil!
                  IF job:engineer = ''
                    If SecurityCheck('RAPID ENG - ALLOCATE JOB')
                        Case Missive('Warning! Reallocating a Job.'&|
                          '<13,10>'&|
                          '<13,10>You do not have sufficient access to edit this job.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
                        Case Missive('Warning! Reallocating a Job.'&|
                          '<13,10>'&|
                          '<13,10>Do you wish to reallocate it to yourself?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                            Of 2 ! Yes Button
                              !Allocation code - ask Bryan
                              DO Allocate_to_Self
                              Do BuildJObsList
                              Error# = 1
                            Of 1 ! No Button
                              error# = 1
                        End ! Case Missive
                     END
                  ElsIf job:Engineer <> use_ali:User_Code
                     !Security Check!
                     If SecurityCheck('RAPID ENG - ALLOCATE JOB')
                          Case Missive('Warning! This job is allocated to another engineer.'&|
                            '<13,10>'&|
                            '<13,10>You do not have sufficient access to edit this job.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
                          Case Missive('Warning! This job is allocated to another engineer.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to reallocate it yourself?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                              !Allocation code - ask Bryan
                              DO Allocate_to_Self
                              Do BuildJObsList
                              BRW1.ResetSort(1)
                              Error# = 1
                              Of 1 ! No Button
                              error# = 1
                          End ! Case Missive
                    End
                  End!If job:date_completed <> ''
               END
      
      BRW1.resetsort(1)
    OF ?ReAllocate
      ThisWindow.Update
      error# = 0
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = glo:Password
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          If InsertEngineerPassword() <> use:User_Code
              Case Missive('Incorrect Password.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Error# = 1
          End !If InsertEngineerPassword() <> use:User_Code
      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      If Error# = 0
          tmp:NewUserCode = ReAllocateEngineer()
      
          If tmp:NewUserCode <> ''
              Clear(glo:Queue)
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Access:RAPENGLS.ClearKey(rapl:RecordNumberKey)
                  rapl:RecordNumber = glo:Pointer
                  If Access:RAPENGLS.TryFetch(rapl:RecordNumberKey) = Level:Benign
                      !Found
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = rapl:JobNumber
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Found
                          !Save Old Engineer on the job before changing it. - L832 (DBH: 08-07-2003
                          tmp:OldEngineer = job:Engineer
                          job:Engineer    = tmp:NewUserCode
      
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
      
                          Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
      
                          If Access:JOBS.TryUpdate() = Level:Benign
                              Access:USERS.Clearkey(use:User_Code_Key)
                              use:User_Code   = tmp:NewUserCode
                              If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Found
      
                              Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
                              IF (AddToAudit(job:Ref_Number,'JOB','RAPID ENGINEER UPDATE: JOB REALLOCATED','ENGINEER: ' & Clip(use:Forename) & ' ' & Clip(use:Surname)))
                              END ! IF
      
                              !Update the engineer history - L832 (DBH: 08-07-2003)
                              If glo:WebJob and job:Date_Completed <> '' And SentToHub(job:ref_Number)
                                  !The engineer is being changed at the RRC after the job has come back from hub
                                  If Access:JOBSENG.PrimeRecord() = Level:Benign
                                      joe:JobNumber     = job:Ref_Number
                                      joe:UserCode      = job:Engineer
                                      joe:DateAllocated = Today()
                                      joe:TimeAllocated = Clock()
                                      joe:AllocatedBy   = use:User_Code
                                      joe:Status        = 'ENGINEER QA'
                                      joe:StatusDate    = Today()
                                      joe:StatusTime    = Clock()
                                      access:users.clearkey(use:User_Code_Key)
                                      use:User_Code   = job:Engineer
                                      access:users.fetch(use:User_Code_Key)
      
                                      joe:EngSkillLevel = use:SkillLevel
                                      joe:JobSkillLevel = jobe:SkillLevel
      
                                      If Access:JOBSENG.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:JOBSENG.TryInsert() = Level:Benign
                                  End !If Access:JOBSENG.PrimeRecord() = Level:Benign
      
                              Else !If glo:WebJob and job:Date_Completed <> ''
                                  !Lookup current engineer and mark as escalated
                                  !Escalate the current engineer. If attached - L832 (DBH: 08-07-2003
                                  If tmp:OldEngineer <> ''
                                      Save_joe_ID = Access:JOBSENG.SaveFile()
                                      Access:JOBSENG.ClearKey(joe:UserCodeKey)
                                      joe:JobNumber     = job:Ref_Number
                                      joe:UserCode      = tmp:OldEngineer
                                      joe:DateAllocated = Today()
                                      Set(joe:UserCodeKey,joe:UserCodeKey)
                                      Loop
                                          If Access:JOBSENG.PREVIOUS()
                                             Break
                                          End !If
                                          If joe:JobNumber     <> job:Ref_Number       |
                                          Or joe:UserCode      <> tmp:OldEngineer      |
                                          Or joe:DateAllocated > Today()       |
                                              Then Break.  ! End If
                                          If joe:Status = 'ALLOCATED'
                                              joe:Status = 'ESCALATED'
                                              joe:StatusDate = Today()
                                              joe:StatusTime = Clock()
                                              Access:JOBSENG.Update()
                                          End !If joe:Status = 'ALLOCATED'
                                          Break
                                      End !Loop
                                      Access:JOBSENG.RestoreFile(Save_joe_ID)
                                  End !If tmp:OldEngineer <> ''
      
      
                                  If Access:JOBSENG.PrimeRecord() = Level:Benign
                                      joe:JobNumber     = job:Ref_Number
                                      joe:UserCode      = job:Engineer
                                      joe:DateAllocated = Today()
                                      joe:TimeAllocated = Clock()
                                      joe:AllocatedBy   = use:User_Code
                                      joe:Status        = 'ALLOCATED'
                                      joe:StatusDate    = Today()
                                      joe:StatusTime    = Clock()
      
                                      access:users.clearkey(use:User_Code_Key)
                                      use:User_Code   = job:Engineer
                                      access:users.fetch(use:User_Code_Key)
                                      joe:EngSkillLevel = use:SkillLevel
                                      joe:JobSkillLevel = jobe:SkillLevel
      
                                      If Access:JOBSENG.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:JOBSENG.TryInsert() = Level:Benign
                                  End !If Access:JOBSENG.PrimeRecord() = Level:Benign
      
                              End !If glo:WebJob and job:Date_Completed <> ''
      
                              Delete(RAPENGLS)
      
                          End !If Access:JOBS.TryUpdate() = Level:Benign
      
                      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  Else!If Access:RAPENGLS.TryFetch(rapl:RecordNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:RAPENGLS.TryFetch(rapl:RecordNumberKey) = Level:Benign
              End !Loop x# = 1 To Records(glo:Queue)
          End !tmp:NewUserCode <> ''
      
          tmp:TotalRecords    = Records(RAPENGLS)
          Display()
      
      End !Error# = 0
      Do BuildJobsList
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of MouseLeft2
              Post(Event:Accepted,?DasTag)
      End !KeyCode()
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F6Key
              If ?View_Job{prop:Hide} = 0
                  Post(Event:Accepted,?View_Job)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F9Key
              If ?EstimatePartsButton{prop:Hide} = 0
                  Post(Event:Accepted,?EstimatePartsButton)
              End !If ?ButtonGroup{prop:Disable} = 0
          Of F10Key
              Post(Event:Accepted,?OkButton)
      End !KeyCode()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rapl:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  tmp:estimate_accepted = FALSE
  !Check to see if it is an accepted estimate
  Access:Jobs.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = rapl:JobNumber
  IF Access:Jobs.Fetch(job:Ref_Number_Key)
    !Error!
    tmp:estimate_accepted = FALSE
  ELSE
    IF job:Estimate = 'YES'
      IF job:Estimate_Accepted = 'YES'
        tmp:estimate_accepted = TRUE
      END
    END
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:tag_NormalFG = -1
  SELF.Q.tmp:tag_NormalBG = -1
  SELF.Q.tmp:tag_SelectedFG = -1
  SELF.Q.tmp:tag_SelectedBG = -1
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 1
  ELSE
    SELF.Q.tmp:tag_Icon = 0
  END
  SELF.Q.rapl:JobNumber_NormalFG = -1
  SELF.Q.rapl:JobNumber_NormalBG = -1
  SELF.Q.rapl:JobNumber_SelectedFG = -1
  SELF.Q.rapl:JobNumber_SelectedBG = -1
  SELF.Q.tmp:WobNumber_NormalFG = -1
  SELF.Q.tmp:WobNumber_NormalBG = -1
  SELF.Q.tmp:WobNumber_SelectedFG = -1
  SELF.Q.tmp:WobNumber_SelectedBG = -1
  SELF.Q.rapl:DateAllocated_NormalFG = -1
  SELF.Q.rapl:DateAllocated_NormalBG = -1
  SELF.Q.rapl:DateAllocated_SelectedFG = -1
  SELF.Q.rapl:DateAllocated_SelectedBG = -1
  SELF.Q.rapl:TimeAllocated_NormalFG = -1
  SELF.Q.rapl:TimeAllocated_NormalBG = -1
  SELF.Q.rapl:TimeAllocated_SelectedFG = -1
  SELF.Q.rapl:TimeAllocated_SelectedBG = -1
  SELF.Q.rapl:ReportedFault_NormalFG = -1
  SELF.Q.rapl:ReportedFault_NormalBG = -1
  SELF.Q.rapl:ReportedFault_SelectedFG = -1
  SELF.Q.rapl:ReportedFault_SelectedBG = -1
  SELF.Q.rapl:JobStatus_NormalFG = -1
  SELF.Q.rapl:JobStatus_NormalBG = -1
  SELF.Q.rapl:JobStatus_SelectedFG = -1
  SELF.Q.rapl:JobStatus_SelectedBG = -1
  SELF.Q.rapl:SkillLevel_NormalFG = -1
  SELF.Q.rapl:SkillLevel_NormalBG = -1
  SELF.Q.rapl:SkillLevel_SelectedFG = -1
  SELF.Q.rapl:SkillLevel_SelectedBG = -1
  SELF.Q.tmp:estimate_accepted_NormalFG = -1
  SELF.Q.tmp:estimate_accepted_NormalBG = -1
  SELF.Q.tmp:estimate_accepted_SelectedFG = -1
  SELF.Q.tmp:estimate_accepted_SelectedBG = -1
   
   
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.tmp:tag_NormalFG = 32768
     SELF.Q.tmp:tag_NormalBG = 16777215
     SELF.Q.tmp:tag_SelectedFG = 16777215
     SELF.Q.tmp:tag_SelectedBG = 32768
   ELSE
     SELF.Q.tmp:tag_NormalFG = -1
     SELF.Q.tmp:tag_NormalBG = -1
     SELF.Q.tmp:tag_SelectedFG = -1
     SELF.Q.tmp:tag_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.rapl:JobNumber_NormalFG = 32768
     SELF.Q.rapl:JobNumber_NormalBG = 16777215
     SELF.Q.rapl:JobNumber_SelectedFG = 16777215
     SELF.Q.rapl:JobNumber_SelectedBG = 32768
   ELSE
     SELF.Q.rapl:JobNumber_NormalFG = -1
     SELF.Q.rapl:JobNumber_NormalBG = -1
     SELF.Q.rapl:JobNumber_SelectedFG = -1
     SELF.Q.rapl:JobNumber_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.tmp:WobNumber_NormalFG = 32768
     SELF.Q.tmp:WobNumber_NormalBG = 16777215
     SELF.Q.tmp:WobNumber_SelectedFG = 16777215
     SELF.Q.tmp:WobNumber_SelectedBG = 32768
   ELSE
     SELF.Q.tmp:WobNumber_NormalFG = -1
     SELF.Q.tmp:WobNumber_NormalBG = -1
     SELF.Q.tmp:WobNumber_SelectedFG = -1
     SELF.Q.tmp:WobNumber_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.rapl:DateAllocated_NormalFG = 32768
     SELF.Q.rapl:DateAllocated_NormalBG = 16777215
     SELF.Q.rapl:DateAllocated_SelectedFG = 16777215
     SELF.Q.rapl:DateAllocated_SelectedBG = 32768
   ELSE
     SELF.Q.rapl:DateAllocated_NormalFG = -1
     SELF.Q.rapl:DateAllocated_NormalBG = -1
     SELF.Q.rapl:DateAllocated_SelectedFG = -1
     SELF.Q.rapl:DateAllocated_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.rapl:TimeAllocated_NormalFG = 32768
     SELF.Q.rapl:TimeAllocated_NormalBG = 16777215
     SELF.Q.rapl:TimeAllocated_SelectedFG = 16777215
     SELF.Q.rapl:TimeAllocated_SelectedBG = 32768
   ELSE
     SELF.Q.rapl:TimeAllocated_NormalFG = -1
     SELF.Q.rapl:TimeAllocated_NormalBG = -1
     SELF.Q.rapl:TimeAllocated_SelectedFG = -1
     SELF.Q.rapl:TimeAllocated_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.rapl:ReportedFault_NormalFG = 32768
     SELF.Q.rapl:ReportedFault_NormalBG = 16777215
     SELF.Q.rapl:ReportedFault_SelectedFG = 16777215
     SELF.Q.rapl:ReportedFault_SelectedBG = 32768
   ELSE
     SELF.Q.rapl:ReportedFault_NormalFG = -1
     SELF.Q.rapl:ReportedFault_NormalBG = -1
     SELF.Q.rapl:ReportedFault_SelectedFG = -1
     SELF.Q.rapl:ReportedFault_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.rapl:JobStatus_NormalFG = 32768
     SELF.Q.rapl:JobStatus_NormalBG = 16777215
     SELF.Q.rapl:JobStatus_SelectedFG = 16777215
     SELF.Q.rapl:JobStatus_SelectedBG = 32768
   ELSE
     SELF.Q.rapl:JobStatus_NormalFG = -1
     SELF.Q.rapl:JobStatus_NormalBG = -1
     SELF.Q.rapl:JobStatus_SelectedFG = -1
     SELF.Q.rapl:JobStatus_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.rapl:SkillLevel_NormalFG = 32768
     SELF.Q.rapl:SkillLevel_NormalBG = 16777215
     SELF.Q.rapl:SkillLevel_SelectedFG = 16777215
     SELF.Q.rapl:SkillLevel_SelectedBG = 32768
   ELSE
     SELF.Q.rapl:SkillLevel_NormalFG = -1
     SELF.Q.rapl:SkillLevel_NormalBG = -1
     SELF.Q.rapl:SkillLevel_SelectedFG = -1
     SELF.Q.rapl:SkillLevel_SelectedBG = -1
   END
   IF (tmp:estimate_accepted = TRUE)
     SELF.Q.tmp:estimate_accepted_NormalFG = 32768
     SELF.Q.tmp:estimate_accepted_NormalBG = 16777215
     SELF.Q.tmp:estimate_accepted_SelectedFG = 16777215
     SELF.Q.tmp:estimate_accepted_SelectedBG = 32768
   ELSE
     SELF.Q.tmp:estimate_accepted_NormalFG = -1
     SELF.Q.tmp:estimate_accepted_NormalBG = -1
     SELF.Q.tmp:estimate_accepted_SelectedFG = -1
     SELF.Q.tmp:estimate_accepted_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  !change job number to wob number
  If glo:WebJob
      access:WebJob.clearkey(wob:RefNumberKey)
      wob:RefNumber = rapl:jobNumber
      if access:Webjob.fetch(wob:RefNumberkey) then
          !error not found
          return(record:filtered)
          !tmp:WobNumber = 111
          !return(record:ok)
      ELSE
          !if wob:HeadAccountNumber = ClarioNET:Global.Param2
          access:tradeacc.clearkey(tra:Account_Number_Key)
          tra:Account_Number = ClarioNET:Global.Param2
          access:tradeacc.fetch(tra:Account_Number_Key)
          tmp:WobNumber = wob:refnumber & '-' & tra:BranchIdentification & wob:JobNumber
          return(record:OK)
          !ELSE
          !    return(Record:filtered)
          !END !if matching head account number
      END  !If access:wob.fetch
  Else
      tmp:wobNumber =  rapl:jobNumber!Job:ref_number
      return(record:ok)
  END
  
  
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rapl:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

BrowseEstimateParts PROCEDURE (func:JobNumber)        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:JobNumber        LONG
tmp:Return           STRING(20)
BRW1::View:Browse    VIEW(ESTPARTS)
                       PROJECT(epr:Part_Number)
                       PROJECT(epr:Description)
                       PROJECT(epr:Quantity)
                       PROJECT(epr:Record_Number)
                       PROJECT(epr:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
epr:Part_Number        LIKE(epr:Part_Number)          !List box control field - type derived from field
epr:Description        LIKE(epr:Description)          !List box control field - type derived from field
epr:Quantity           LIKE(epr:Quantity)             !List box control field - type derived from field
epr:Record_Number      LIKE(epr:Record_Number)        !Primary key field - type derived from field
epr:Ref_Number         LIKE(epr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Estimate Parts'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Esimate Part File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,100,344,198),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|M~Part Number~@s30@124L(2)|M~Description~@s30@32D(2)|M~Quantity~L@n8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(168,300),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,300),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,300),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020473'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseEstimateParts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ESTPARTS.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ESTPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','BrowseEstimateParts')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,epr:Part_Number_Key)
  BRW1.AddRange(epr:Ref_Number,tmp:JobNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,epr:Part_Number,1,BRW1)
  BRW1.AddField(epr:Part_Number,BRW1.Q.epr:Part_Number)
  BRW1.AddField(epr:Description,BRW1.Q.epr:Description)
  BRW1.AddField(epr:Quantity,BRW1.Q.epr:Quantity)
  BRW1.AddField(epr:Record_Number,BRW1.Q.epr:Record_Number)
  BRW1.AddField(epr:Ref_Number,BRW1.Q.epr:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESTPARTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseEstimateParts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Estimate_Part
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020473'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020473'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020473'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Update_QA PROCEDURE (f_type)                          !Generated from procedure template - Window

FilesOpened          BYTE
save_webjob_id       USHORT,AUTO
Web_Temp_Job         STRING(20)
tmp:alloc_use_code   STRING(3)
date_error_Temp      STRING(30)
Date_Completed_Temp  DATE
Time_Completed_Temp  STRING(20)
Check_For_Bouncers_Temp STRING(30)
Print_Despatch_Note_Temp STRING(30)
Saved_Ref_Number_Temp LONG
Saved_Esn_Temp       STRING(30)
Saved_MSN_Temp       STRING(30)
tmp:DespatchClose    BYTE(0)
tmp:RestockingNote   BYTE(0)
save_wpr_id          USHORT,AUTO
save_qap_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
save_lac_id          USHORT,AUTO
save_joe_id          USHORT,AUTO
job_queue_temp       QUEUE,PRE(jobque)
job_number           LONG
type                 STRING(7)
record_number        STRING(20)
                     END
error_temp           BYTE(1)
Job_Number_Temp      REAL
wob_number_temp      REAL
serial_number_temp   STRING(20)
model_details_temp   STRING(60)
handset_type_temp    STRING(30)
date_booked_temp     DATE
save_jac_id          USHORT,AUTO
update_text_temp     STRING(100)
tmp:ConsignNo        STRING(30)
tmp:AccountNumber    STRING(30)
tmp:OldConsignno     STRING(30)
sav:Path             STRING(255)
tmp:LabelError       STRING(30)
account_number2_temp STRING(30)
save_cou_ali_id      USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ParcellineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
tmp:ExistingLocation STRING(30)
tmp:Paid             BYTE(0)
tmp:PassedJobType    STRING(3)
tmp:OldJobNo         LONG
tmp:NewJobNo         LONG
tmp:audittype        BYTE
tmp:notes            STRING(255)
BER_SMS_Send         STRING('N')
locAuditNotes        STRING(255)
locAction            STRING(80)
window               WINDOW('Rapid QA Update'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid QA Update'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,208,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Rapid QA Update'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(168,134),USE(?Job_Number_Temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s9),AT(236,134,64,10),USE(Job_Number_Temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s9),AT(312,134,44,10),USE(wob_number_temp),HIDE,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Serial Number'),AT(168,154),USE(?serial_number_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(236,154,120,10),USE(serial_number_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           GROUP('Unit Details'),AT(176,168,184,84),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Model:'),AT(184,178),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s60),AT(184,186,148,10),USE(model_details_temp),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Handset Type:'),AT(184,202),USE(?Prompt4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(184,210,124,12),USE(handset_type_temp),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Date Booked:'),AT(184,227),USE(?Prompt5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@d6b),AT(184,235),USE(date_booked_temp),FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           END
                           STRING(@s100),AT(176,264,184,12),USE(update_text_temp),CENTER,FONT(,10,COLOR:Lime,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(168,296),USE(?FaultCodes),DISABLE,TRN,FLAT,LEFT,ICON('amdfaup.jpg')
                           BUTTON,AT(236,296),USE(?QA_PAss),DISABLE,TRN,FLAT,LEFT,ICON('qapassp.jpg')
                           BUTTON,AT(304,296),USE(?qa_fail),DISABLE,TRN,FLAT,LEFT,ICON('qafailp.jpg')
                         END
                       END
                       SHEET,AT(376,82,140,246),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(380,98,132,224),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('47L(2)|M~Job Number~@s8@38L(2)|M~Type~@s7@0L(2)|M~Record Number~@s8@12L(2)|M~HO ' &|
   'Kind~@s3@'),FROM(QAQueue)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON,AT(164,332),USE(?Button:PrintHandover),TRN,FLAT,ICON('prnhandp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
OutFile FILE,DRIVER('ASCII'),PRE(OUF),NAME(filename3),CREATE,BINDABLE,THREAD
Record  Record
line1       String(255)
            End
        End
TempFilePath         CSTRING(255)
ParcelLineExport    File,Driver('ASCII'),Pre(Parcel),Name(tmp:ParcelLineName),CREATE,THREAD,BINDABLE
Record                  Record
OrderNO                 String(25)
Labels                  String(3)
Service                 String(2)
CompanyName             String(35)
AddressLine1            String(35)
AddressLine2            String(35)
Town                    String(35)
County                  String(35)
Postcode                String(8)
Instructions            String(25)
Instructions2           String(25)
AccountCode             String(10)
Type                    String(1)
Workstation             String(2)
                        End
                    End

local           Class
ReturnInTransitUnit  Procedure(String f:IMEINumber,String f:CurrentAvailable, String f:Available)
!ValidateParts        PROCEDURE (),Byte
ValidateNetwork      PROCEDURE (),Byte
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

FailParts       ROUTINE
    DATA
locReason   STRING(255)
locNotes    STRING(255)  

i   LONG(0)
    CODE    
        QA_Failure_Reason(locReason,locNotes)
        
        locAuditNotes = 'PARTS USED:-'
        
        IF (job:Chargeable_Job = 'YES')
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number = job:Ref_Number
            SET(par:Part_Number_Key,par:Part_Number_Key)
            LOOP UNTIL Access:PARTS.Next() <> Level:Benign
                IF (par:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                
                locAuditNotes = CLIP(locAuditNotes) & '<13,10>' & CLIP(par:Part_Number)
            END ! LOOP
        END ! IF
        
        IF (job:Warranty_Job = 'YES')
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
            LOOP UNTIL Access:WARPARTS.Next() <> Level:Benign
                IF (wpr:Ref_Number <> job:Ref_Number)
                    BREAK
                END ! IF
                
                locAuditNotes = CLIP(locAuditNotes) & '<13,10>' & CLIP(wpr:Part_Number)
            END ! LOOP
        END ! IF        
        
        locAuditNotes = CLIP(locAuditNotes) & '<13,10,13,10>PARTS VALIDATED:-'
        
        LOOP i = 1 TO RECORDS(glo:Queue)
            GET(glo:Queue,i)
            Access:QAPARTSTEMP.ClearKey(qap:RecordNumberKey)
            qap:RecordNumber = glo:Pointer
            IF (Access:QAPARTSTEMP.TryFetch(qap:RecordNumberKey) = Level:Benign)
                locAuditNotes = CLIP(locAuditNotes) & '<13,10>' & CLIP(qap:PartNumber)
            END ! IF
        END ! LOOP
        
        ! Can't see how this would work here, so instead of fixing it I will remove for now
        !Added by Paul - 03/09/2009 - log no 10785
        !If clip(notes") <> '' then
        !  Do AddEngNotes
        !End !If clip(notes") <> '' then
        !End Addition        
        
        locAuditNotes = CLIP(locAuditNotes) & '<13,10>' & CLIP(locNotes)
        
        glo:Notes_Global     = CLIP(locAuditNotes)
        
        CASE f_Type
        OF 'PRE'
            locAction = 'QA REJECTION ELECTRONIC: ' & Clip(locReason)
        OF 'PRI'
            locAction = 'QA REJECTION MANUAL: ' & Clip(locReason)
        END ! CASE
        
        IF (AddToAudit(job:Ref_Number,'JOB',locAction,locAuditNotes))
        END ! IF
        
        glo:Select1 = job:Ref_Number
        IF (def:QA_Failed_Label = 'YES')
            QA_Failed_Label()
        END ! IF
        
        IF (def:QA_Failed_Report = 'YES')
            QA_Failed()
        END ! IF
        glo:Select1 = ''
        glo:Notes_Global = ''
        
        CASE f_Type
        OF 'PRE'
            GetStatus(625,1,'JOB')
        ELSE
            GetStatus(615,1,'JOB')
        END !CASE
        
        DO Allocate_Prev_Eng
        job:QA_Rejected = 'YES'
        job:Date_QA_Rejected = TODAY()
        job:Time_QA_Rejected = CLOCK()
        Access:JOBS.Update()
        
        DO Passed_Fail_Update
Check_Fields        Routine
    error_temp = 1
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        found# = 0
        If job:esn = serial_number_temp
            model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
            If job:third_party_site <> ''
                handset_type_temp  = 'Thid Party Repair'
            Else!If job:third_party_site <> ''
                handset_type_temp  = 'Repair'
            End!If job:third_party_site <> ''

            date_booked_temp   = job:date_booked
            found# = 1
        End!If job:esn = serial_number_temp
        If job:msn = serial_number_temp And found# = 0
            model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
            If job:third_party_site <> ''
                handset_type_temp  = 'Thid Party Repair'
            Else!If job:third_party_site <> ''
                handset_type_temp  = 'Repair'
            End!If job:third_party_site <> ''
            date_booked_temp   = job:date_booked
        End!If job:esn = serial_number_temp
        If job:exchange_unit_number <> '' and found# = 0
            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number = job:exchange_unit_number
            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                If xch:esn = serial_number_temp
                    model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                    handset_type_temp  = 'Exchange'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
                If xch:msn = serial_number_temp And found# = 0
                    model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                    handset_type_temp  = 'Exchange'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
            end
        End!If job:exchange_unit_number <> ''
        If job:loan_unit_number <> '' And found# = 0
            access:loan.clearkey(loa:ref_number_key)
            loa:ref_number = job:loan_unit_number
            if access:loan.fetch(loa:ref_number_key) = Level:Benign
                If loa:esn = serial_number_temp
                    model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                    handset_type_temp  = 'Loan'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
                If loa:msn = serial_number_temp And found# = 0
                    model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                    handset_type_temp  = 'Loan'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
            end
        End!If job:loan_unit_number <> '' And found# = 0
        If found# = 0
            Case Missive('The selected serial number does not exist on the selected job.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            if glo:webjob then
                update_text_temp = 'Job Number: ' & Clip(wob_number_temp) & ' - Failed'
            else
                update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' - Failed'
            end
            ?update_text_temp{prop:color} = COLOR:Red
            ?update_text_temp{prop:fontcolor}     = COLOR:WHite

            Select(?job_number_temp)
        End!If found# = 0
        If found# = 1
            error_temp = 0
        End
    Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        Case Missive('Unable to find the selected job.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' - Failed'
        ?update_text_temp{prop:color} = COLOR:Red
        ?update_text_temp{prop:fontcolor}     = COLOR:WHite

        Select(?job_number_temp)
    end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
Failed_Update       Routine
        update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' - Failed'
        ?update_text_temp{prop:color} = COLOR:Red
        ?update_text_temp{prop:fontcolor}     = COLOR:WHite
        DO Disable_Buttons

Passed_update       Routine
        !try and send the sms from here
        If handset_type_temp = 'Exchange'
            !Code added 28/04/2009 by PS log no 10321
            If glo:WebJob = 1 then
                !being used at an RRC
                If jobe:WebJob = 1 then
                    !job is an RRC job
                    ! Change --- Don't send if PUP Job (DBH: 18/03/2010) #11014
                    !If jobe:ExchangedATRRC = 0 then
                    ! To --- (DBH: 18/03/2010) #11014
                    If (jobe:ExchangedATRRC = 0 AND job:Who_Booked <> 'WEB')
                    ! end --- (DBH: 18/03/2010) #11014
                        !unit exchanged at RRC
                        Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                        jobe2:RefNumber = job:Ref_Number
                        If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign

                        Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                            !Error
                        End !If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                        !ok - we should be able to send the SMS or email now
                        If jobe2:SMSNotification
                            !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'COMP','SMS',jobe2:SMSAlertNumber,'','','')
                            SendSMSText('J','Y','N')

                            !Debug erroring turned off for distruibution
!                            if glo:ErrorText[1:5] = 'ERROR' then
!
!                                miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!
!                            ELSE
!                                !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                                miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!                            END
                            glo:ErrorText = ''

                        End ! If jobe2:SMSNotification
                        If jobe2:EmailNotification
                            AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'COMP','EMAIL','',jobe2:EmailAlertAddress,'','')
                        End ! If jobe2:EmailNotification
                    End!If jobe:ExchangedATRRC = 0 then
                End!If jobe:WebJob = 1 then
            End!If glo:WebJob = 1 then
        end !If handset_type_temp = 'Exchange'
        beep(beep:systemasterisk)
        gloque:JobNumber = Web_Temp_Job
        gloque:RecordNumber += 1
        gloque:Type = 'QA PASS'
        gloque:JobType = tmp:PassedJobType
        Add(QAQueue)
        Sort(QAQueue,-gloque:RecordNumber)

        update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' - Success'
        ?update_text_temp{prop:fontcolor} = COLOR:Aqua
        ?update_text_temp{prop:color} = 09A6A7CH
        job_number_temp = ''
        serial_number_temp = ''
        DO Disable_Buttons

Passed_fail_update       Routine
        beep(beep:systemasterisk)
        gloque:JobNumber = Web_Temp_Job
        gloque:RecordNumber += 1
        gloque:Type = 'QA FAIL'
        gloque:JobType = tmp:PassedJobType
        Add(QAQueue)
        Sort(QAQueue,-gloque:RecordNumber)

        update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' - Success'
        ?update_text_temp{prop:fontcolor} = COLOR:Aqua
        ?update_text_temp{prop:color} = 09A6A7CH
        job_number_temp = ''
        serial_number_temp = ''
        DO Disable_Buttons

Disable_Buttons     ROUTINE
        Disable(?qa_pass)
        Disable(?qa_fail)
        Disable(?FaultCodes)
QA_Group        Routine
!Check_For_Despatch        Routine
!    Set(defaults)
!    access:defaults.next()
!
!    despatch# = 0
!
!    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
!        despatch# = 1
!    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
!        access:subtracc.clearkey(sub:account_number_key)
!        sub:account_number = job:account_number
!        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!            access:tradeacc.clearkey(tra:account_number_key)
!            tra:account_number = sub:main_account_number
!            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!                If tra:use_sub_accounts = 'YES'
!                    If job:chargeable_job = 'YES'
!                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
!                            despatch# = 1
!                        End!If sub:despatch_invoiced_jobs <> 'YES'
!                    Else!If job:chargeable_job = 'YES'
!                        despatch# = 1
!                    End!If job:chargeable_job = 'YES'
!                Else!If tra:use_sub_accounts = 'YES'
!                    If job:chargeable_job = 'YES'
!                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
!                            despatch# = 1
!                        End!If sub:despatch_invoiced_jobs <> 'YES'
!                    Else!If job:chargeable_job = 'YES'
!                        despatch# = 1
!                    End!If job:chargeable_job = 'YES'
!                End!If tra:use_sub_accounts = 'YES'
!            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
!
!    If despatch# = 1
!        tmp:DespatchClose   = 0
!        tmp:RestockingNote  = 0
!        If job:despatched <> 'YES'
!            If ToBeLoaned() = 1
!                restock# = 0
!                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
!                    Case Missive('A loan unit has been issued to this job but the initial transit type has been set-up so that an exchange unit is required.'&|
!                      '<13,10>Do you wish to CONTINUE and mark this unit for despatch back to the customer, or do you want to RESTOCK it?','ServiceBase 3g',|
!                                   'mquest.jpg','Restock|Continue')
!                        Of 2 ! Continue Button
!                        Of 1 ! Restock Button
!                            ForceDespatch()
!                            tmp:restockingnote = 1
!                            restock# = 1
!                    End ! Case Missive
!                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
!                If restock# = 0
!                    IF GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                        job:despatched  = 'REA'
!                    Else !IF GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!                        job:despatched  = 'LAT'
!                    End !IF GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                    job:despatch_type   = 'JOB'
!                    job:Current_Courier  = job:Courier
!                End!If restock# = 0
!            Else!IF ToBeLoaned() = 1
!                If ToBeExchanged() ! Job Will Be Exchanged
!                    ForceDespatch()
!                    tmp:restockingnote = 1
!                Else!If ToBeExchanged() ! Job Will Be Exchanged
!                    job:date_Despatched = DespatchANC(job:courier,'JOB')
!
!                    access:courier.clearkey(cou:courier_key)
!                    cou:courier = job:courier
!                    if access:courier.tryfetch(cou:courier_key) = Level:Benign
!                        If cou:despatchclose = 'YES'
!                            tmp:DespatchClose   = 1
!                        Else!If cou:despatchclose = 'YES'
!                            tmp:DespatchClose   = 0
!                        End!If cou:despatchclose = 'YES'
!                    End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
!                End!If ToBeExchanged() ! Job Will Be Exchanged
!            End!If ToBeLoaned() = 1
!        End!If job:despatched <> 'YES'
!    End!If despatch# = 1
CompletedBit        Routine
Data
local:FailedAssessment      Byte(0)
local:CallDespatch          Byte(0)
Code
        !To avoid not changing to the right status,
        !change to completed, first, and then it should
        !Change again to one of the below status's if necessary
        !message('Debug 1|Completed bit start.')
        !(for Vodcom's turnaround report, the status HAS to change
        !to completed, first.)
        GetStatus(705,1,'JOB')

        !Now change to what Joe thinks the status should be:
        Access:COURIER.Clearkey(cou:Courier_Key)
        cou:Courier = job:Courier
        If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
            !Found
            tmp:Paid = 0
            If job:Warranty_Job = 'YES'
                If job:EDI = 'FIN'
                    tmp:Paid = 1
                End !If job:EDI = 'FIN'
            End !If job:Warranty_Job = 'YES'
            If job:Chargeable_Job = 'YES'
                tmp:Paid = 0
                Total_Price('C',vat",total",balance")
                If total" = 0 Or balance" <= 0
                    tmp:Paid = 1
                End !If total" = 0 Or balance" <= 0

            End !If job:Chargeable_Job = 'YES'
            ! Inserting (DBH 07/07/2006) # 7149 - If PUP booked job, change status to SEND TO PUP
! Changing (DBH 02/03/2007) # 8821 - Only change if at the RRC
!            If job:Who_Booked = 'WEB'
! to (DBH 02/03/2007) # 8821
            If job:Who_Booked = 'WEB' And glo:WebJob = 1
! End (DBH 02/03/2007) #8821
                GetStatus(Sub(GETINI('RRC','StatusDespatchToPUP',,Clip(Path()) & '\SB2KDEF.INI'),1,3),0,'JOB')
            Else ! If job:Who_Booked = 'WEB'
            ! End (DBH 07/07/2006) #7149
                If cou:CustomerCollection
                    If tmp:Paid
                        GetStatus(915,0,'JOB') !Paid Awaiting Collection
                    Else !If tmp:Paid
                        GetStatus(805,0,'JOB') !Ready To Collection
                    End !If tmp:Paid
                Else !If cou:CustomerCollection
                    If tmp:Paid
                        GetStatus(916,0,'JOB') !Paid Awaiting Despatch
                    Else !If tmp:Paid
                        GetStatus(810,0,'JOB') !Ready To Despatch
                    End !If tmp:Paid
                End !If cou:CustomerCollection
            End ! If job:Who_Booked = 'WEB'
        Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
            !Error
        End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

! Deleting (DBH 23/01/2007) # 8678 - Not needed
!        NormalStatus# = 1
!        If InvoiceSubAccounts(job:Account_Number)
!            If sub:SetDespatchJobStatus
!                GetStatus(Sub(sub:DespatchedJobStatus,1,3),0,'JOB')
!                NormalStatus# = 0
!            End !If sub:SetDespatchJobStatus
!            If sub:SetInvoicedJobStatus
!                GetStatus(Sub(sub:InvoicedJobStatus,1,3),0,'JOB')
!                NormalStatus# = 0
!            End !If sub:SetInvoicedJobStatus
!        Else !If InvoiceSubAccounts(job:Account_Number)
!            If tra:SetDespatchJobStatus
!                GetStatus(Sub(tra:DespatchedJobStatus,1,3),0,'JOB')
!                NormalStatus# = 0
!            End !If sub:SetDespatchJobStatus
!            If tra:SetInvoicedJobStatus
!                GetStatus(Sub(tra:InvoicedJobStatus,1,3),0,'JOB')
!                NormalStatus# = 0
!            End !If sub:SetInvoicedJobStatus
!        End !If InvoiceSubAccounts(job:Account_Number)
! End (DBH 23/01/2007) #8678
        !message('Debug 2|Before the exchange check.')
        Print_Despatch_Note_Temp = ''

        If ExchangeAccount(job:Account_Number)
            !Is the Trade Account used for Exchange Units
            !if so, then there's no need to despatch the
            !unit normal. Just auto fill the despatch fields
            !and mark the job as to return to exchange stock.
            ForceDespatch
            GetStatus(707,1,'JOB')
        Else !If ExchangeAccount(job:Account_Number)
            !Has the OBF been validated?
            !message('Debug 3|exchange check failed.')
            If jobe:OBFValidated
                ForceDespatch
                GetStatus(710,1,'JOB')
            Else !If jobe:OBFValidated

                !Will the job be restocked, i.e. has, or will have, an
                !Exchange Unit attached, or is it a normal despatch.
                restock# = 0
                If ExchangeAccount(job:Account_Number)
                    restock# = 1
                Else !If ExchangeAccount(job:Account_Number)
                    glo:select1  = job:ref_number
                    If ToBeExchanged()

                        !Has this unit got an Exchange, or is it expecting an exchange
                        !Then it needs to be restock, even if there is a loan unit attached also

                        !If this job is a 48 Hour exchange repair at the
                        !ARC, but it has failed assessment, then it
                        !must be returned to the RRC, not restocked -  (DBH: 06-11-2003)
                        !If unit back at the RRC, proceed to despatch
                        !instead of returning to Exchange Stock - 3651 (DBH: 04-12-2003)
                        If jobe:Engineer48HourOption And jobe:WebJob
                            If PassExchangeAssessment() = Level:Benign
                                Restock# = 0
                                If ~glo:WebJob
                                    Case Missive('The selected unit has been repaired under the 48 Hour Exchange process and has FAILED assessment.'&|
                                      '<13,10>'&|
                                      '<13,10>It must now be returned to the RRC.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                End !If ~glo:WebJob
                                local:FailedAssessment = 1
                            Else !If PassExchangeAssessment() = Level:Benign
                                Restock# = 1
                            End !If PassExchangeAssessment() = Level:Benign
                        Else !If ~glo:WebJob And jobe:Engineer48HourOption
                            Restock# = 1
                        End !If ~glo:WebJob And jobe:Engineer48HourOption
                    Else !If ToBeExchanged()
                        !If there is an loan unit attached, assume that a despatch note is required.
                        If job:loan_unit_number <> ''
                            restock# = 0
                        End!If job:loan_unit_number <> ''
                    End !If ToBeExchanged()
                End !If ExchangeAccount(job:Account_Number)

                If CompletePrintDespatch(job:Account_Number) = Level:Benign
                    Print_Despatch_Note_Temp = 'YES'
                End !CompletePrintDespatch(job:Account_Number) = Level:Benign

                If restock# = 0

! Deleting (DBH 24/01/2007) # 8678 - Not needed
!                    !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
!                    !If either of those are ticked then don't despatch yet
!                    !print_despatch_note_temp = ''
!                    If AccountAllowedToDespatch()
! End (DBH 24/01/2007) #8678
                    !Do we need to despatch at completion?
                    ! Insert --- Unit should be returned to Exchange Stock. It's a 48hr refurb (DBH: 16/03/2009) #10473
                    if (jobe:JobReceived = 1)
                        ! Add to queue, so can print the report (DBH: 20/03/2009) #10473
                        tmp:PassedJobType = '48H'

                        Beep(Beep:SystemExclamation)  ;  Yield()
                        Case Missive('Unit should now be returned to Exchange Stock.','ServiceBase',|
                                       'mexclam.jpg','/&OK')
                            Of 1 ! &OK Button
                        End!Case Message
                        GetStatus(707,1,'JOB')
                        restock# = 1

                        ! Mark incoming exchange unit as "In Transit" (DBH: 17/03/2009) #10473
                        local.ReturnInTransitUnit(job:ESN,'NOA','IT4')
                    else ! if (jobe:JobReceived = 1)
                    ! end --- (DBH: 16/03/2009) #10473

                        local:CallDespatch = 1
!                    Else !If AccountAllowedToDespatch
!                        GetStatus(705,1,'JOB')
!                    End !If AccountAllowedToDespatch
                    end !if (jobe:JobReceived = 1)
                Else !If restock# = 0
                    If job:Exchange_Unit_Number <> ''
                        !Are either of the Chargeable or Warranty repair types BERs?
                        !If so, then don't change the status to 707 because it shouldn't
                        !be returned to the Exchange Stock.
                        Comp# = 0
                        If job:Chargeable_Job = 'YES'
                            If BERRepairType(job:Manufacturer,job:Repair_Type) = Level:Benign
                                 Comp# = 1
                            End !If BERRepairType(job:Repair_Type) = Level:Benign
                        End !If job:Chageable_Job = 'YES'
                        If job:Warranty_Job = 'YES' and Comp# = 0
                            If BERRepairType(job:Manufacturer,job:Repair_Type_Warranty) = Level:Benign
                                Comp# = 1
                            End !If BERRepairType(job:Repair_Type) = Level:Benign
                        End !If job:Warranty_Job = 'YES'
                        If Comp# = 1
                            Case Missive('This unit has been exchanged but is a B.E.R. Repair.'&|
                              '<13,10>DO NOT place this unit into exchange stock.','ServiceBase 3g',|
                                           'mexclam.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
!                            GetStatus(705,1,'JOB')
                            Print_Despatch_Note_Temp = 'NO'
! Change --- Show the BER message regardless (DBH: 22/05/2009) #10822
!                        Else !If Comp# = 1
!
! To --- (DBH: 22/05/2009) #10822
                        End !If Comp# = 1

! end --- (DBH: 22/05/2009) #10822
                        ! Insert --- If ARC Repair and exchange attached. Ask the new questions. (DBH: 18/03/2009) #10473
                        normalProcess# = 1
                        ! Change --- All for 7 Day TAT (DBH: 09/06/2009) #10859
                        !If (glo:WebJob = 0 and jobe:Engineer48HourOption = 2 and jobe:ExchangedAtRRC = 0)
                        ! To --- (DBH: 09/06/2009) #10859
                        if (glo:WebJob = 0 and jobe:ExchangedAtRRC = 0)
                            if (jobe:Engineer48HourOption = 2 Or jobe:Engineer48HourOption = 3)
                        ! end --- (DBH: 09/06/2009) #10859
                                Beep(Beep:SystemQuestion)  ;  Yield()
                                Case Missive('Select Option:'&|
                                    '|Scrap, Return To Manufacturer,  Refurb or Phantom Job?','ServiceBase',|
                                               'mquest.jpg','/&Continue|Phantom|Refurbs|RTM|Scrap')
                                    Of 5 ! Scrap Button
                                        ! Add to queue, so can print the report (DBH: 20/03/2009) #10473
                                        tmp:PassedJobType = 'SCR'

                                        normalProcess# = 0
                                        ! Mark incoming exchange unit as "Scrap" (DBH: 17/03/2009) #10473
                                        local.ReturnInTransitUnit(job:ESN,'REP','ITS')
                                        if (AddToAudit(job:Ref_Number,'JOB','QA OPTION: SCRAP',''))
                                        end ! if (AddToAudit(job:Ref_Number,'JOB','QA OPTION: SCRAP','')
                                        GetStatus(730,1,'JOB') ! In Transit To Scrapping
                                    Of 4 ! RTM Button
                                        ! Add to queue, so can print the report (DBH: 20/03/2009) #10473
                                        tmp:PassedJobType = 'RTM'

                                        normalProcess# = 0
                                        ! Mark incoming exchange unit as "RTM" (DBH: 17/03/2009) #10473
                                        local.ReturnInTransitUnit(job:ESN,'REP','ITR')
                                        if (AddToAudit(job:Ref_Number,'JOB','QA OPTION: RTM',''))
                                        end ! if (AddToAudit(job:Ref_Number,'JOB','QA OPTION: SCRAP','')
                                        GetStatus(732,1,'JOB') ! In Transit To RTM
                                    Of 3 ! Refurbs Button
                                        ! Add to queue, so can print the report (DBH: 20/03/2009) #10473
                                        tmp:PassedJobType = 'REF'

                                        normalProcess# = 0
                                        Access:JOBSE.TryUpdate()
                                        Beep(Beep:SystemExclamation)  ;  Yield()
                                        Case Missive('This unit will now be booked in for repair.','ServiceBase',|
                                                       'mexclam.jpg','/&OK')
                                            Of 1 ! &OK Button
                                        End!Case Message
                                        CompleteAndRebookJob(job:Ref_Number,'','48HR')
                                        Exit

                                    Of 2 ! Phantom Button
                                        ! Add to queue, so can print the report (DBH: 20/03/2009) #10473
                                        tmp:PassedJobType = 'PHA'

                                        normalProcess# = 0
                                        local.ReturnInTransitUnit(job:ESN,'REP','ITP')
                                        if (AddToAudit(job:Ref_Number,'JOB','QA OPTION: PHANTOM',''))
                                        end ! if (AddToAudit(job:Ref_Number,'JOB','QA OPTION: SCRAP','')
                                        GetStatus(736,1,'JOB') ! Phantom In Transit
                                    Of 1 ! &Continue Button
                                End!Case Message
                            end ! if (jobe:Engineer48HourOption = 2 Or jobe:Engineer48HourOption = 2)
                        end ! if (glo:WebJob = 0)

                        if (normalProcess# = 1)
                        ! end --- (DBH: 18/03/2009) #10473

                            If jobe:ExchangedAtRRC

                              If jobe:SecondExchangeNumber > 0 !Added By NB 09-01-04
                                  Case Missive('This unit has been exchange by an RRC. It has a 2nd Exchange.'&|
                                    '<13,10>'&|
                                    '<13,10>It must be restocked to ARC STORES.','ServiceBase 3g',|
                                                 'mexclam.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              Else
                                  Case Missive('This unit has been exchange by an RRC.'&|
                                    '<13,10>'&|
                                    '<13,10>It must be restocked to NATIONAL STORES.','ServiceBase 3g',|
                                                 'mexclam.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              End
                            Else !If jobe:ExchangedAtRRC
                                Case Missive('This unit has been exchanged by an ARC.'&|
                                  '<13,10>'&|
                                  '<13,10>It must be restocked to ARC STORES.','ServiceBase 3g',|
                                               'mexclam.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                            End !If jobe:ExchangedAtRRC
                            GetStatus(707,1,'JOB')
                        end ! if (normalProcess# = 1)
!                        End !If Comp# = 1
!                    Else !If job:Exchange_Unit_Number <> ''
!                        GetStatus(705,1,'JOB')
                    End !If job:Exchange_Unit_Number <> ''

                End !If restock# = 0
                !Does the Account care about Bouncers?
                IF (glo:WebJob = 1)
                    IF (SentToHub(job:Ref_Number) = 0)
                        ! #13622 Do not Bouncer Check a job that has been sent to the ARC (DBH: 02/11/2015)
                        Check_For_Bouncers_Temp = 1
                        If CompleteCheckForBouncer(job:Account_Number)
                            Check_For_Bouncers_Temp = 0
                        End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign
                    END ! IF
                END ! IF
            End !If jobe:OBFValidated
        End !If ExchangeAccount(job:Account_Number)

        !Fill in the relevant fields

        saved_ref_number_temp   = job:ref_number
        saved_esn_temp  = job:esn
        saved_msn_temp  = job:msn

        !only fill in date IF it's not already filled in.
        !this should make sure the date doesn't change again when QA's by RRC from ARC
        If job:Date_Completed = ''
            job:date_completed = Today()
            job:time_completed = Clock()
            date_completed_temp = job:date_completed
            time_completed_temp = job:time_completed

            !Write the date completed info into WEBJOBS -  (DBH: 14-10-2003)
            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
                wob:DateCompleted = job:Date_Completed
                wob:TimeCompleted = job:Time_Completed
                wob:Completed       = 'YES'
                Access:WEBJOB.Update()
            Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            ! Inserting (DBH 15/03/2007) # 8718 - Update the obf processed file accordingly
            Relate:JOBSOBF.Open()
            Access:JOBSOBF.ClearKey(jof:RefNumberKey)
            jof:RefNumber = job:Ref_Number
            If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                !Found
                jof:DateCompleted = job:Date_Completed
                jof:TimeCompleted = job:Time_Completed
                jof:HeadAccountNumber = wob:HeadAccountNumber
                jof:IMEINumber = job:ESN
                Access:JOBSOBF.Update()
            Else ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:JOBSOBF.TryFetch(jof:RefNumberKey) = Level:Benign
            Relate:JOBSOBF.Close()
            ! End (DBH 15/03/2007) #8718

        End !If job:Date_Completed = ''

        job:completed = 'YES'

        !Lookup current engineer and mark as escalated
        Save_joe_ID = Access:JOBSENG.SaveFile()
        Access:JOBSENG.ClearKey(joe:UserCodeKey)
        joe:JobNumber     = job:Ref_Number
        joe:UserCode      = job:Engineer
        joe:DateAllocated = Today()
        Set(joe:UserCodeKey,joe:UserCodeKey)
        Loop
            If Access:JOBSENG.PREVIOUS()
               Break
            End !If
            If joe:JobNumber     <> job:Ref_Number       |
            Or joe:UserCode      <> job:Engineer      |
            Or joe:DateAllocated > Today()       |
                Then Break.  ! End If
            joe:Status = 'COMPLETED'
            joe:StatusDate = Today()
            joe:StatusTime = Clock()
            Access:JOBSENG.Update()
            Break
        End !Loop
        Access:JOBSENG.RestoreFile(Save_joe_ID)

        If Check_For_Bouncers_Temp
            !Check if the job is a Bouncer and add
            !it to the Bouncer List
            If CountBouncer()
                !Bouncer
                AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
            Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

            End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

        End !If Check_For_Bouncers_Temp

        !Check if this job is an EDI job
        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
        mod:Model_Number    = job:Model_Number
        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Found
            job:edi = PendingJob(mod:Manufacturer)
            job:Manufacturer    = mod:Manufacturer
        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

        !Mark the job's exchange unit as being available
        CompleteDoExchange(job:Exchange_Unit_Number)

        Do QA_Group
!        ThisMakeover.SetWindow(win:form)
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
            !Only change the status to "send to rrc" if there is no exchange unit attached.
            If jobe:WebJob and ~glo:WebJob And job:Exchange_Unit_Number = 0 and ~jobe:OBFValidated
                stanum# = Sub(GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                GetStatus(stanum#,0,'JOB')
            Else!If jobe:WebJob and ~glo:WebJob And job:Exchange_Unit_Number = 0 and ~jobe:OBFValidated
                ! Change --- Add 48hr refurbs (DBH: 16/03/2009) #10473
                !If job:Exchange_Unit_Number <> 0 or jobe:OBFValidated
                ! To --- (DBH: 16/03/2009) #10473
                If job:Exchange_Unit_Number <> 0 or jobe:OBFValidated or jobe:JobReceived = 1
                ! end --- (DBH: 16/03/2009) #10473
                    !Change the location to despatched, because this unit will not be going
                    !back to the RRC, or getting despatched.
                    LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
                Else
                    ! Inserting (DBH 12/05/2006) #7597 - Send Alert if ARC only job, or RRC QAing
                    !Added by PS on 13/05/2009 log no 10321
                    If job:who_booked <> 'WEB' then
                        If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                            Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                            jobe2:RefNumber = job:Ref_Number
                            If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                ! Found
                                If jobe2:SMSNotification
                                    !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'COMP','SMS',jobe2:SMSAlertNumber,'',0,'')
                                    SendSMSText('J','Y','N')
                                    !Debug erroring turned off for distruibution
!                                    if glo:ErrorText[1:5] = 'ERROR' then
!
!                                        miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!
!                                    ELSE
!                                        !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
!                                        miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!                                    END
                                    Glo:ErrorText = ''

                                End ! If jobe2:SMSNotification
                                If jobe2:EmailNotification
                                    AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'COMP','EMAIL','',jobe2:EmailAlertAddress,0,'')
                                End ! If jobe2:EmailNotification
                            Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                ! Error
                            End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                        End ! If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                    End !If job:who_booked <> 'WEB' then
                    ! End (DBH 12/05/2006) #7597
                End !If job:Exchange_Unit_Number <> 0 or jobe:OBFValidated
            End !If jobe:WebJob
            If job:Warranty_Job = 'YES'
                If jobe:WarrantyClaimStatus = ''
                    jobe:WarrantyClaimStatus = 'PENDING'
                    Access:JOBSE.Update()
                End !If jobe:WarrantyClaimStatus = ''
            End !If job:Warranty_Job = 'YES'
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        ! Inserting (DBH 01/03/2007) # 8678 - A bodge to call the code to change status on competion AFTER the send to RRC code.
        If local:CallDespatch = 1
            !Part of ... TB 012477 Set the status to 811 (if relevant) all the time, and let the next procedure change it.
            If job:Loan_Unit_Number <> ''
                GetStatus(811,1,'JOB')
            End !If job:Loan_Unit_Number <> ''

            If DespatchAtClosing(local:FailedAssessment)
                !Does the Trade Account have "Skip Despatch" set?
                If AccountSkipDespatch(job:Account_Number) = Level:Benign
                    access:courier.clearkey(cou:courier_key)
                    cou:courier = job:courier
                    if access:courier.tryfetch(cou:courier_key)
                        Case Missive('Cannot find the courier attached to this job.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                        !Give the job a unique Despatch Number
                        !then call the despsing.inc.
                        !This calls the relevant exports etc, and different
                        !despatch procedures depending on the Courier.
                        !It will then print a Despatch Note and change
                        !The status as required.
                        If access:desbatch.primerecord() = Level:Benign
                            If access:desbatch.tryinsert()
                                access:desbatch.cancelautoinc()
                            Else!If access:despatch.tryinsert()
                                DespatchSingle()
                                Print_Despatch_Note_Temp = ''
                            End!If access:despatch.tryinsert()
                        End!If access:desbatch.primerecord() = Level:Benign
                    End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
!                            Else
!                                GetStatus(705,1,'JOB')
                End !If AccountSkipDespatch = Level:Benign
!            Else !If DespatchAtClosing() !TB01477 changes this see above
!                If job:Loan_Unit_Number <> ''
!                    GetStatus(811,1,'JOB')
!!                            Else
!!                                GetStatus(705,1,'JOB')
!                End !If job:Loan_Unit_Number <> ''
            End !If DespatchAtClosing()

        End ! If local.CallDespatch = 1
        ! End (DBH 01/03/2007) #8678

        Access:JOBS.TryUpdate()

        If ExchangeAccount(job:Account_Number)
            Case Missive('This is an Exchange Repair. Please re-stock the unit.','ServiceBase 3g',|
                           'mexclam.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            If def:Use_Job_Label= 'YES'
                glo:Select1 = job:Ref_Number
                Set(DEFAULTS)
                Access:DEFAULTS.Next()
                Case def:label_printer_type
                    Of 'TEC B-440 / B-442'
                        Thermal_Labels('ER')
                    Of 'TEC B-452'
                        Thermal_Labels_B452
                End!Case def:themal_printer_type
            End !If def:Use_Job_Label= 'YES'
        End !If ExchangeAccount(job:Account_Number)

        If print_despatch_note_temp = 'YES'
            If restock# = 1
                restocking_note
            Else!If restock# = 1
                despatch_note
            End!If restock# = 1
            glo:select1  = ''
        End!If print_despatch_note_temp = 'YES'
    !Auto print invoice?
    If job:Chargeable_Job = 'YES'
        PrintInvoice# = 0
        If InvoiceSubAccounts(job:Account_Number)
            If sub:InvoiceAtCompletion
                Case sub:InvoiceTypeComplete
                    Of 0 !Manual
                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                            DespatchInvoice(1)
                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                    Of 1 !Automatic
                        If job:Invoice_Number = ''
                            If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                If job:Invoice_Number = ''
                                    If CreateInvoice() = Level:Benign
                                        PrintInvoice# = 1
                                    End !If CreateInvoice() = Level:Benign
                                Else !If job:Invoice_Number = ''
                                    PrintInvoice# = 1
                                End !If job:Invoice_Number = ''
                            End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                        End !If job:Invoice_Number = ''
                End !Case sub:InvoiceType
            End !If sub:InvoiceAtCompletion
        Else !If InvoiceSubAccounts(job:Account_Number)
            If tra:InvoiceAtCompletion
                Case tra:InvoiceTypeComplete
                    Of 0 !Manual
                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                            DespatchInvoice(1)
                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                    Of 1 !Automatic
                        If job:Invoice_Number = ''
                            If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                If job:Invoice_Number = ''
                                    If CreateInvoice() = Level:Benign
                                        PrintInvoice# = 1
                                    End !If CreateInvoice() = Level:Benign
                                Else !If job:Invoice_Number = ''
                                    PrintInvoice# = 1
                                End !If job:Invoice_Number = ''
                            End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                        End !If job:Invoice_Number = ''
                End !Case sub:InvoiceType
            End !If sub:InvoiceAtCompletion

        End !If InvoiceSubAccounts(job:Account_Number)
        If PrintInvoice#
            glo:Select1 = job:Invoice_Number
            !Single_Invoice('','')
            VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
            glo:Select1 = ''
        End !If PrintInvoice#
    End !If job:Chargeable_Job = 'YES'
Allocate_Prev_Eng   ROUTINE

    !Check to see if this is valid!
    Continue_Alloc# = 0
    Access:SubTracc.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = Job:Account_Number
    IF Access:SubTracc.Fetch(sub:Account_Number_Key)
      !Error!
    ELSE
      Access:TradeAcc.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = sub:Main_Account_Number
      IF Access:TradeAcc.Fetch(tra:Account_Number_Key)
        !Error!
      ELSE
        IF tra:AllocateQAEng = TRUE
          Continue_Alloc# = 1
        END
      END
    END

    tmp:ExistingLocation = ''

    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      tmp:ExistingLocation = use:Location   ! Store engineers current location
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    IF Continue_Alloc# = 1

      !Allocate job back to Engineer prior to this engineer!
      Access:JobsEng.ClearKey(joe:JobNumberKey)
      joe:JobNumber = job:Ref_Number
      SET(joe:JobNumberKey,joe:JobNumberKey)
      xcount# = 0
      LOOP
        IF Access:JobsEng.Next()
          BREAK
        END
        IF joe:JobNumber <> job:Ref_Number
          BREAK
        END
        xcount#+=1
        IF xcount#=2
          tmp:alloc_use_code = joe:UserCode
          BREAK
        END
      END

      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code   = tmp:alloc_use_code
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

      if use:Location = tmp:ExistingLocation
        job:engineer    = tmp:alloc_use_code

        locAuditNotes         = 'FIELDS UPDATED:'
          If tmp:alloc_use_code <> ''
            locAuditNotes       = Clip(locAuditNotes) & '<13,10>ENGINEER: ' & CLIP(tmp:alloc_use_code) & |
                              '<13,10>SKILL LEVEL: ' & Clip(use:SkillLevel)
          End!If tmp:Usercode <> ''

        IF (AddToAudit(job:Ref_Number,'JOB','RAPID ENGINEER UPDATE: JOB ALLOCATED',locAuditNotes))
        END ! IF


        If Access:JOBSENG.PrimeRecord() = Level:Benign
            joe:JobNumber     = job:Ref_Number
            joe:UserCode      = job:Engineer
            joe:DateAllocated = Today()
            joe:TimeAllocated = Clock()
            joe:AllocatedBy   = use:User_Code
            access:users.clearkey(use:User_Code_Key)
            use:User_Code   = job:Engineer
            access:users.fetch(use:User_Code_Key)

            joe:EngSkillLevel = use:SkillLevel
            joe:JobSkillLevel = jobe:SkillLevel

            If Access:JOBSENG.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                !Insert Failed
            End !If Access:JOBSENG.TryInsert() = Level:Benign
        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

      end
     !Access:Jobs.Update()
   End
ExchangeScrap   ROUTINE        !always called - hijacked to also do BER setup
  !message('In the exchange scrap routine')
  !reset the default
  gotscrapexc# = 0
  !check for warranty part
  IF job:Warranty_Job = 'YES' THEN
     Access:REPTYDEF.CLEARKEY(rtd:WarManRepairTypeKey)
     rtd:Manufacturer = job:Manufacturer
     rtd:Warranty = 'YES'
     rtd:Repair_Type = job:Warranty_Charge_Type
     IF ~Access:REPTYDEF.Fetch(rtd:WarManRepairTypeKey) THEN
        IF rtd:ScrapExchange THEN
           gotscrapexc# = 1
        END !IF
        !message('Warrenty job have rtd:Ber ='&rtd:BER&' send type = '&rtd:SMSSendType)
        if rtd:BER = 1 or rtd:BER = 4 then
            !message('Setting local variable to '&rtd:SMSSendType)
            BER_SMS_Send = rtd:SMSSendType
            !will be used later in  SendSMSText('J','Y',BER_SMS_Send)    !beyond economic repair
        END !if BER etc
     END !IF
  END !IF

  !check for chargeable part
  IF job:Chargeable_Job = 'YES' THEN
     Access:REPTYDEF.CLEARKEY(rtd:ChaManRepairTypeKey)
     rtd:Manufacturer = job:Manufacturer
     rtd:Chargeable = 'YES'
     rtd:Repair_Type = job:Repair_Type
     IF ~Access:REPTYDEF.Fetch(rtd:ChaManRepairTypeKey) THEN
        IF rtd:ScrapExchange THEN
           gotscrapexc# = 1
       End
       !message('Chargeable job have rtd:Ber ='&rtd:BER&' send type = '&rtd:SMSSendType)
        if rtd:BER = 1 or rtd:BER = 4 then
            IF BER_SMS_Send = 'N' then   !only change it if it has not been set
                !message('Setting local variable to '&rtd:SMSSendType)
                BER_SMS_Send = rtd:SMSSendType
            ENd
            !will be used later in  SendSMSText('J','Y',BER_SMS_Send)    !beyond economic repair
        END !if BER etc
     END !IF
  END !IF

  exitflag# = 1
  IF gotscrapexc# = 1 THEN
     Access:EXCHANGE.CLEARKEY(xch:Ref_Number_Key)
     xch:Ref_Number = job:Exchange_Unit_Number
     IF ~Access:EXCHANGE.Fetch(xch:Ref_Number_Key) THEN
        exitflag# = 0
     END !IF
  END !IF
  IF exitflag# = 1 THEN EXIT.
  !now, ask the question
    Case Missive('This job has an exchange attached. The repair type indicated that this unit should be scrapped.'&|
      '<13,10>'&|
      '<13,10>Do you wish to continue and SCRAP the unit, or RESTOCK it?','ServiceBase 3g',|
                   'mquest.jpg','Restock|Scrap')
        Of 2 ! Scrap Button
            IF (AddToAudit(job:Ref_Number,'EXC','SCRAPPED EXCHANGE UNIT','SCRAPPED EXCHANGE UNIT'))
            END ! IF

         !update exc hist
         xch:Available = 'ESC'
         xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
         Access:EXCHANGE.TRYUPDATE()
        Of 1 ! Restock Button
            IF (AddToAudit(job:Ref_Number,'EXC','EXCHANGE SCRAP NOT CONFIRMED','EXCHANGE SCRAP NOT CONFIRMED - RETURN TO STOCK'))
            END ! IF


         ViewExchangeUnit !let 'em do stuff there - complex gear going on with all that.
    End ! Case Missive

CreateNewJob routine
    ! The job/unit was booked by the RRC and sent to the ARC. When the ARC returns the unit
    ! back the RRC will QA the unit again.
    ! If the unit fails QA for any reason except accessories mismatch then prompt the user
    ! to rebook the job/unit again.

    data
saveStateJOBS   ushort
saveStateJOBSE  ushort
    code

    Case Missive('This job has been completed at the ARC and cannot be re-completed. If you wish to return the job to the ARC then a new job must be booked.'&|
      '<13,10>Do you wish to book a new job NOW, or LATER?','ServiceBase 3g',|
                   'mquest.jpg','Later|Now')
        Of 2 ! Now Button
            ! Display new job booking screen, prime fields with current job record
            saveStateJOBS = Access:JOBS.SaveFile()
            saveStateJOBSE = Access:JOBSE.SaveFile()

            GlobalRequest = InsertRecord

            GLO:Select30 = 'REPLICATE'
            GLO:Select31 = job:Ref_Number
            !added by Paul 03/09/2009 - log no 10785
            tmp:OldJobNo = job:Ref_Number
            tmp:NewJobNo = 0
            GLO:Select32 = 'COPY'
            If tmp:audittype = 1 then
                GLO:Select33 = 'EXCH'
            Else
                GLO:Select33 = ''
            End

            Update_Jobs()

            !now add an audit entry to the old job
            tmp:NewJobNo = GLO:Select31
            !message('old job no = ' & tmp:OldJobNo & ', New job no = ' & tmp:NewJobNo)
            If tmp:OldJobNo <> tmp:NewJobNo then
                !new job created ok - so add an audit trail to the old job

                get(audit,0)
                case tmp:audittype
                of 1
                    IF (AddToAudit(tmp:OldJobNo,'JOB','EXCHANGE REBOOKED','EXCHANGE REBOOKED FOR RETURN TO ARC. NEW JOB NUMBER = ' & tmp:NewJobNo))
                    END ! IF


                of 2
                    IF (AddToAudit(tmp:OldJobNo,'JOB','JOB REBOOKED','JOB REBOOKED FOR RETURN TO ARC. NEW JOB NUMBER = ' & tmp:NewJobNo))
                    END ! IF

                End
            End !If OldJobNo <> NewJobNo then

            !if we have successfully added a new job - then print the relevant job card
            If tmp:NewJobNo <> 0 then
                !job added ok
                Loop x# = 1 To Records(glo:Queue)
                    Get(glo:Queue,x#)
                    Case glo:Pointer
                    Of 'THERMAL LABEL'
                        glo:Select1 = job:Ref_Number
                        If job:Bouncer = 'B'
                            Thermal_Labels('SE')
                        Else ! If job:Bouncer = 'B'
                            If ExchangeAccount(job:Account_Number)
                                Thermal_Labels('ER')
                            Else ! If ExchangeAccount(job:Account_Number)
                                Thermal_Labels('')
                            End ! If ExchangeAccount(job:Account_Number)
                        End ! If job:Bouncer = 'B'
                        glo:Select1 = ''
                    Of 'JOB CARD'
                        glo:Select1 = job:Ref_Number
                        Job_Card
                        glo:Select1 = ''
                    Of 'RETAINED ACCESSORIES LABEL'
                        glo:Select1 = job:Ref_Number
                        Job_Retained_Accessories_Label
                        glo:Select1 = ''
                    Of 'RECEIPT'
                        glo:Select1 = job:Ref_Number
                        Job_Receipt
                        glo:Select1 = ''
                    End ! Case glo:Pointer
                End ! Loop x# = 1 To Records(glo:Queue)
            End



            GLO:Select30 = ''
            GLO:Select31 = ''
            GLO:Select32 = ''
            GLO:Select33 = ''

            Access:JOBS.RestoreFile(saveStateJOBS)
            Access:JOBSE.RestoreFile(saveStateJOBSE)
        Of 1 ! Later Button
    End ! Case Missive
AddEngNotes                 ROUTINE

    !notes added - so add them to the engineers notes field
    !message('Adding failure notes to the engineering notes = ' & clip(tmp:notes))
    access:Jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
        !found the jobnotes
        If clip(jbn:Engineers_Notes) = '' then
            jbn:Engineers_Notes = clip(tmp:notes)
        Else
            jbn:Engineers_Notes = clip(jbn:Engineers_Notes) & '; ' & clip(tmp:notes)
        End !If clip(jbn:Engineers_Notes) = '' then

        access:Jobnotes.update()

    End !If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then

    !message('engineer notes are now = ' & clip(jbn:Engineers_Notes ))

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020481'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_QA')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:JOBS2_ALIAS.Open
  Relate:JOBSE3.Open
  Relate:NETWORKS.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:LOAN.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:LOANACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:MANUFACT.UseFile
  Access:REPTYDEF.UseFile
  Access:MODELNUM.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSE2.UseFile
  Access:JOBACC.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !check for web job
  !If glo:WebJob
  !    ?Job_Number_Temp{prop:Hide} = 1
  !    ?Wob_Number_Temp{prop:Hide} = 0
  !Else !glo:WebJob
  !    ?Job_Number_Temp{prop:Hide} = 0
  !    ?Wob_Number_Temp{prop:Hide} = 1
  !End !glo:WebJob
  IF GLO:Select49 <> ''
    Job_Number_Temp = GLO:Select49
    serial_number_temp = GLO:Select50
    UPDATE()
    DISPLAY()
    POST(Event:Accepted,?serial_number_temp)
  END
  
  !message('Going into QA')
      ! Save Window Name
   AddToLog('Window','Open','Update_QA')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:JOBS2_ALIAS.Close
    Relate:JOBSE3.Close
    Relate:NETWORKS.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_QA')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020481'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020481'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020481'&'0')
      ***
    OF ?wob_number_temp
      !get the wob number
      
      access:webjob.clearkey(wob:HeadJobNumberKey)
      wob:HeadAccountNumber = ClarioNET:Global.Param2
      wob:JobNumber = wob_number_temp
      access:webjob.fetch(wob:HeadJobNumberKey)
      
      Job_Number_Temp = wob:RefNumber&'-'&job:ref_number
      
      display()
      thiswindow.update
    OF ?serial_number_temp
      IF LEN(CLIP(serial_number_temp)) = 18
          ! Ericsson IMEI!
          serial_number_temp = SUB(serial_number_temp, 4, 15)
          ! ESN_Entry_Temp = Job:ESN
          UPDATE()
          DISPLAY()
      ELSE
        ! Job:ESN = ESN_Entry_Temp
      END ! IF
      !message('In the serial temp loop')
      
      Disable(?qa_pass)
      Disable(?qa_fail)
      Disable(?FaultCodes)
      
      !------------------------------------------------------------------
      Web_Temp_Job = job_number_temp
      if glo:WebJob then
          ! Change the tmp ref number if you
          ! can Look up from the wob file
          access:Webjob.clearkey(wob:RefNumberKey)
          wob:RefNumber = job_number_temp
          if access:Webjob.fetch(wob:refNumberKey) = level:benign then
              access:tradeacc.clearkey(tra:Account_Number_Key)
              tra:Account_Number = ClarioNET:Global.Param2
              access:tradeacc.fetch(tra:Account_Number_Key)
              Web_Temp_Job = job_number_temp & '-' & tra:BranchIdentification & wob:JobNumber
          end ! if
          ! Set up no preview on client
          ! ClarioNET:UseReportPreview(0)
      end ! if
      !------------------------------------------------------------------
      
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
      
          ! Start - Check to see if the selected job is in use - TrkBs: 5873 (DBH: 02-06-2005)
          If JobInUse(job:Ref_Number, 1)
              Do failed_update
              Select(?Job_Number_Temp)
              Cycle
          End ! If JobInUse(job:Ref_Number,1)If JobInUse(job:Ref_Number,1)
          ! End   - Check to see if the selected job is in use - TrkBs: 5873 (DBH: 02-06-2005)
      
          found# = 0
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber  = job:Ref_Number
          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Found
      
          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          ! Error
          End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
          If job:esn = serial_number_temp
              ! Check the location of the job, and make sure it
              ! matches the location of the user - 4488 (DBH: 19-07-2004)
              If glo:WebJob
                  ! Allow for jobs "ready" to be sent to ARC, as they will still be AT the RRC. - TrkBs: 4488 (DBH: 17-02-2005)
                  If job:Location <>  GETINI('RRC', 'RRCLocation',, CLIP(Path()) & '\SB2KDEF.INI') Or |
                      jobe:HubRepair = True Or |
                      wob:HeadAccountNumber <> Clarionet:Global.Param2
                      Case Missive('The selected job is out of RRC Control.', 'ServiceBase 3g', |
                                   'mstop.jpg', '/OK')
                      Of 1 ! OK Button
                      End ! Case Missive
                      DO failed_update
                      Select(?job_number_temp)
                      Cycle
                  End ! If job:Location <>  GETINI('RRC','RRCLocation',,CLIP(Path()) & '\SB2KDEF.INI')
              Else ! If glo:WebJob
                  If job:Location <> GETINI('RRC', 'ARCLocation',, CLIP(Path()) & '\SB2KDEF.INI')
                      Case Missive('The selected job is out of ARC Control.', 'ServiceBase 3g', |
                                   'mstop.jpg', '/OK')
                      Of 1 ! OK Button
                      End ! Case Missive
                      DO failed_update
                      Select(?job_number_temp)
                      Cycle
                  End ! If job:Location <> GETINI('RRC','ARCLocation',,CLIP(Path()) & '\SB2KDEF.INI')
              End ! If glo:WebJob
      
              If job:workshop <> 'YES'
                  found# = 4
              Else! If job:workshop <> 'YES'
                  model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
                  If job:third_party_site <> ''
                      handset_type_temp  = 'Third Party Repair'
                  Else! If job:third_party_site <> ''
                      handset_type_temp  = 'Repair'
                  End! If job:third_party_site <> ''
      
                  date_booked_temp = job:date_booked
                  found#           = 1
      
              End! If job:workshop <> 'YES'
      
          End! If job:esn = serial_number_temp
      
      
          If job:msn = serial_number_temp And found# = 0
              If job:workshop <> 'YES'
                  found# = 4
              Else! If job:workshop <> 'YES'
                  model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
                  If job:third_party_site <> ''
                      handset_type_temp  = 'Third Party Repair'
                  Else! If job:third_party_site <> ''
                      handset_type_temp  = 'Repair'
                  End! If job:third_party_site <> ''
                  date_booked_temp = job:date_booked
                  found#           = 1
      
              End! If job:workshop <> 'YES'
          End! If job:esn = serial_number_temp
      
          If job:exchange_unit_number <> '' and found# = 0
              access:exchange.clearkey(xch:ref_number_key)
              xch:ref_number = job:exchange_unit_number
              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                  If xch:esn = serial_number_temp
      
                      ! Quick fix. Can't think of all the scenarios to put in proper error checking
                      Found# = 1
      
                      if found# = 1
                          model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                          handset_type_temp  = 'Exchange'
                          date_booked_temp   = job:date_booked
                      end ! if
                  End! If job:esn = serial_number_temp
                  If xch:msn = serial_number_temp And found# = 0
      
                      Found# = 1
      
                      if found# = 1
                          model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                          handset_type_temp  = 'Exchange'
                          date_booked_temp   = job:date_booked
                      end ! if
                  End! If job:esn = serial_number_temp
              end ! if
      
          End! If job:exchange_unit_number <> ''
          If job:loan_unit_number <> '' And found# = 0
              access:loan.clearkey(loa:ref_number_key)
              loa:ref_number = job:loan_unit_number
              if access:loan.fetch(loa:ref_number_key) = Level:Benign
                  If loa:esn = serial_number_temp
      
                      Found# = 1
      
                      if found# = 1
                          model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                          handset_type_temp  = 'Loan'
                          date_booked_temp   = job:date_booked
                      end ! if
                  End! If job:esn = serial_number_temp
                  If loa:msn = serial_number_temp And found# = 0
      
                      Found# = 1
      
                      if found# = 1
                          model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                          handset_type_temp  = 'Loan'
                          date_booked_temp   = job:date_booked
                      end ! if
      
                  End! If job:esn = serial_number_temp
              end ! if
      
          End! If job:loan_unit_number <> '' And found# = 0
      
          If Found# = 0
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  ! Found
                  If jobe:SecondExchangeNumber <> 0 And Found# = 0
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number  = jobe:SecondExchangeNumber
                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          ! Found
                          If xch:ESN = Serial_Number_Temp
                              Model_Details_Temp = Clip(xch:Model_Number) & ' - ' & Clip(xch:Manufacturer)
                              Handset_Type_Temp  = '2nd Exchange'
                              Date_Booked_Temp   = job:Date_Booked
                              Found#             = 1
                          End ! If xch:ESN = Serial_Number_Temp
                      Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                      ! Error
                      End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                  End ! If jobe:SecondExchangeNumber <> 0
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              ! Error
              End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          End ! If Found# = 0
      
      
          !TB13287 - Always check - do not allow to complete if specific needs job and software not installed
          if found# > 0       !has been found
              Access:JOBSE3.Clearkey(jobe3:KeyRefNumber)
              jobe3:RefNumber  = job:Ref_Number
              If Access:JOBSE3.Tryfetch(jobe3:KeyRefNumber) = Level:Benign
                  !Found
                  if jobe3:SpecificNeeds and not(jobe3:SN_Software_Installed)
                      Found# = 5  !new error code
                  END
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !not found - no Error let it pass
              End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          END !if a match has been found
          !message('Have found = '&Found#)
      
          Case found#
          Of 0
              Case Missive('The selected serial number does not exist on the selected job.', 'ServiceBase 3g', |
                           'mstop.jpg', '/OK')
              Of 1 ! OK Button
              End ! Case Missive
              Do failed_update
      
              Select(?job_number_temp)
          Of 1
              Enable(?qa_pass)
              Enable(?qa_fail)
              Enable(?FaultCodes)
              DO ExchangeScrap
          Of 2
              Case Missive('Error! You have entered the serial number of the customer''s unit. The selected job has an Exchange Unit attached that has yet to be despatched. This must pass manual QA first.' &|
                           '<13,10>You must enter the serial number of the exchange unit to continue.', 'ServiceBase 3g', |
                           'mstop.jpg', '/OK')
              Of 1 ! OK Button
              End ! Case Missive
              Select(?serial_number_temp)
              Do failed_update
          Of 3
              Do failed_update
              Select(?job_number_temp)
          Of 4
              Do failed_update
              Case Missive('Error! The selected unit is not in the workshop.', 'ServiceBase 3g', |
                           'mstop.jpg', '/OK')
              Of 1 ! OK Button
              End ! Case Missive
              Select(?job_number_temp)
          of 5
              !TB13287 - do not allow to complete if specific needs job and software not installed
              Do failed_update
              miss# = missive('The mobile device booked is a Specific Needs Device. |Please confirm that you '&|
                               'have installed the Specific Needs Software before the QA can be completed.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
              Select(?job_number_temp)
          End! Case found#
      
      
          Else! if access:jobs.fetch(job:ref_number_key) = Level:Benign
          Case Missive('Unable to find the selected job.', 'ServiceBase 3g', |
                       'mstop.jpg', '/OK')
          Of 1 ! OK Button
          End ! Case Missive
          DO failed_update
          Select(?job_number_temp)
      end! if access:jobs.fetch(job:ref_number_key) = Level:Benign
      
      Display()
    OF ?FaultCodes
      ThisWindow.Update
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
      
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
      Access:WEBJOB.Clearkey(wob:RefNumberKey)
      wob:RefNumber   = job:Ref_Number
      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Found
      
      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          ! Error
      End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
      If job:Chargeable_Job = 'YES'
          JobType"    = 'C'
          ChargeType"   = job:Charge_Type
          RepairType"   = job:Repair_Type
      End !If job:Chargeable_Job = 'YES'
      If job:Warranty_Job = 'YES'
          JobType"    = 'W'
          ChargeType"   = job:Warranty_Charge_Type
          RepairType"   = job:Repair_Type_Warranty
      End !If job:Warranty_Job = 'YES'
      
      Clear(glo:FaultCodeGroup)
      glo:FaultCode1 = job:Fault_Code1
      glo:FaultCode2 = job:Fault_Code2
      glo:FaultCode3 = job:Fault_Code3
      glo:FaultCode4 = job:Fault_Code4
      glo:FaultCode5 = job:Fault_Code5
      glo:FaultCode6 = job:Fault_Code6
      glo:FaultCode7 = job:Fault_Code7
      glo:FaultCode8 = job:Fault_Code8
      glo:FaultCode9 = job:Fault_Code9
      glo:FaultCode10 = job:Fault_Code10
      glo:FaultCode11 = job:Fault_Code11
      glo:FaultCode12 = job:Fault_Code12
      glo:FaultCode13 = wob:FaultCode13
      glo:FaultCode14 = wob:FaultCode14
      glo:FaultCode15 = wob:FaultCode15
      glo:FaultCode16 = wob:FaultCode16
      glo:FaultCode17 = wob:FaultCode17
      glo:FaultCode18 = wob:FaultCode18
      glo:FaultCode19 = wob:FaultCode19
      glo:FaultCode20 = wob:FaultCode20
      GenericFaultCodes(job:Ref_Number,JobType",ThisWindow.Request,job:account_Number,job:Manufacturer,|
                          ChargeType",RepairType",job:Date_Completed)
      job:Fault_Code1   = glo:FaultCode1
      job:Fault_Code2   = glo:FaultCode2
      job:Fault_Code3   = glo:FaultCode3
      job:Fault_Code4   = glo:FaultCode4
      job:Fault_Code5   = glo:FaultCode5
      job:Fault_Code6   = glo:FaultCode6
      job:Fault_Code7   = glo:FaultCode7
      job:Fault_Code8   = glo:FaultCode8
      job:Fault_Code9   = glo:FaultCode9
      job:Fault_Code10  = glo:FaultCode10
      job:Fault_Code11  = glo:FaultCode11
      job:Fault_Code12  = glo:FaultCode12
      wob:FaultCode13   = glo:FaultCode13
      wob:FaultCode14   = glo:FaultCode14
      wob:FaultCode15   = glo:FaultCode15
      wob:FaultCode16   = glo:FaultCode16
      wob:FaultCode17   = glo:FaultCode17
      wob:FaultCode18   = glo:FaultCode18
      wob:FaultCode19   = glo:FaultCode19
      wob:FaultCode20   = glo:FaultCode20
      Clear(glo:FaultCodeGroup)
      Access:JOBSE.Update()
      Access:Jobs.Update()
      Access:WEBJOB.Update()
    OF ?QA_PAss
      ThisWindow.Update
      update_text_temp = ''
      Set(Defaults)
      access:Defaults.next()
      
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
      
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Found
      
              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              !Found
              If f_Type = 'PRE' And ~man:UseElectronicQA
                  Case Missive('The manufacturer of the selected job is not set-up to do electronic QA.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else !If f_Type = 'PRE' And ~man:UseElectronicQA
                  error# = 0
                  If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan' Or Handset_Type_Temp = '2nd Exchange'
                      If ~man:QALoanExchange And Handset_Type_Temp = 'Exchange'
                          error# = 1
                          Case Missive('This manufacturer is not set-up to QA exchange units.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      End !If ~man:QALoanExchange And Handset_Type_Temp = 'Exchange
                      If ~man:QALoan And Handset_Type_Temp = 'Loan'
                          error# = 1
                          Case Missive('This manufacturer is not set-up to QA loan units.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      End !If ~man:QALoan And Handset_Type_Temp = 'Loan'
                      If error# = 0
                          error# = 1                                                              !Stop from doing the job check
                          If Handset_Type_Temp = '2nd Exchange'
                              Case f_type                                                             !to be despatched.
                                  Of 'PRE'
                                      Do passed_update
      
                                      IF (AddToAudit(job:ref_number,'2NE','RAPID QA UPDATE: ELECTRONIC QA PASSED (EXC)','EXCHANGE UNIT NUMBER: ' & Clip(jobe:SecondExchangeNumber)))
                                      END ! IF
      
      
                                      GetStatus(620,1,'2NE') !Electronic QA passed
      
                                      Access:JOBS.Update()
                                      Access:JOBSE.Update()
      
                                      access:exchange.clearkey(xch:ref_number_key)
                                      xch:ref_number = jobe:SecondExchangeNumber
                                      if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                          xch:available = 'QA2'
                                          xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                          access:exchange.update()
                                          get(exchhist,0)
                                          if access:exchhist.primerecord() = level:benign
                                              exh:ref_number   = xch:ref_number
                                              exh:date          = today()
                                              exh:time          = clock()
                                              access:users.clearkey(use:password_key)
                                              use:password =glo:password
                                              access:users.fetch(use:password_key)
                                              exh:user = use:user_code
                                              exh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                              access:exchhist.insert()
                                          end!if access:exchhist.primerecord() = level:benign
                                      end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                  Of 'PRI'
                                      do passed_update
                                      IF (AddToAudit(job:ref_number,'2NE','RAPID QA UPDATE: MANUAL QA PASSED (EXC)','EXCHANGE UNIT NUMBER: ' & Clip(jobe:SecondExchangeNumber)))
                                      END ! IF
      
                                      GetStatus(708,1,'2NE') !RETURN TO STOCK
                                      Access:JOBS.Update()
                                      Access:JOBSE.Update()
      
                                      access:exchange.clearkey(xch:ref_number_key)
                                      xch:ref_number = jobe:SecondExchangeNumber
                                      if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                          get(exchhist,0)
                                          if access:exchhist.primerecord() = level:benign
                                              exh:ref_number   = xch:ref_number
                                              exh:date          = today()
                                              exh:time          = clock()
                                              access:users.clearkey(use:password_key)
                                              use:password =glo:password
                                              access:users.fetch(use:password_key)
                                              exh:user = use:user_code
                                              exh:status        = 'QA PASS ON JOB NO: ' & CLip(job:Ref_number)
                                              access:exchhist.insert()
                                          end!if access:exchhist.primerecord() = level:benign
                                      end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
      
                                      Case Missive('The selected unit must now be restocked to national stores.','ServiceBase 3g',|
                                                     'mexclam.jpg','/OK')
                                          Of 1 ! OK Button
                                      End ! Case Missive
                              End !Case f_Type
                          End !If Handset_Type_Temp = '2nd Exchange'
                          If handset_type_temp = 'Exchange'
                              type# = 2 !1 - Job, 2 - Exchange, 3 - Loan
                              temp# = AccessoryCheck('EXCHANGE')
                              Case temp#
                                  Of 1 Orof 2
                                      IF temp# = 1 THEN reason" = 'ACCESSORIES MISSING'.
                                      reason" = ''
                                      notes" = ''
                                      QA_Failure_Reason(reason",notes")
                                      locAuditNotes         = 'ACCESSORIES BOOKED IN:-'
                                      save_jac_id = access:jobacc.savefile()
                                      access:jobacc.clearkey(jac:ref_number_key)
                                      jac:ref_number = job:Ref_number
                                      set(jac:ref_number_key,jac:ref_number_key)
                                      loop
                                          if access:jobacc.next()
                                             break
                                          end !if
                                          if jac:ref_number <> job:Ref_number      |
                                              then break.  ! end if
                                          ! --- Only count the "attached" accessories ----
                                          ! Inserting:  DBH 28/01/2009 #10658
                                          If ~glo:WebJob
                                              If jac:Attached <> 1
                                                  Cycle
                                              End ! If jac:Attached <> 1
                                          End ! If ~glo:WebJob
                                          ! End: DBH 28/01/2009 #10658
                                          ! -----------------------------------------
      
                                          locAuditNotes   = Clip(locAuditNotes) & '<13,10>' & Clip(jac:accessory)
                                      end !loop
                                      access:jobacc.restorefile(save_jac_id)
      
                                      locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                      Loop x# = 1 To Records(glo:queue)
                                          Get(glo:queue,x#)
                                          locAuditNotes       = CLip(locAuditNotes) & '<13,10>' & Clip(glo:pointer)
                                      End!Loop x# = 1 To Records(glo:queue)
                                      !Save Notes For Report
      
                                      !Added by Paul - 03/09/2009 - log no 10785
                                      If clip(notes") <> '' then
                                        Do AddEngNotes
                                      End !If clip(notes") <> '' then
                                      !End Addition
                                      locAuditNotes       = CLip(locAuditNotes) & '<13,10>' & clip(notes")
      
                                      glo:notes_global    = Clip(locAuditNotes)
      
                                      Case f_type
                                          Of 'PRE'
                                              locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                          Of 'PRI'
                                              locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                      End !Case f_type
      
                                      IF (AddToAudit(job:ref_number,'EXC',locAction,locAuditNotes))
                                      END ! IF
      
                                      glo:select1 = job:exchange_unit_number
                                      If def:qa_failed_label = 'YES'
                                          QA_Failed_Label_Exchange
                                      End!If def:qa_failed_label = 'YES'
                                      If def:qa_failed_report = 'YES'
                                          QA_Failed_Exchange
                                      End!If def:qa_failed_report = 'YES'
                                      glo:select1 = ''
                                      glo:notes_global    = ''
                                      Case f_type
                                          Of 'PRE'
                                              GetStatus(625,1,'EXC')
                                          Else!Case f_type
                                              GetStatus(615,1,'EXC')
                                      End!Case f_type
                                      DO Allocate_Prev_Eng
                                      job:qa_rejected = 'YES'
                                      job:date_qa_rejected    = Today()
                                      job:time_qa_rejected    = Clock()
                                      access:jobs.update()
                                      Do passed_fail_update
      
                                  Of 3
      
                                  Of 0
      
      
      
                                      Case f_type                                                             !to be despatched.
                                          Of 'PRE'
                                              Do passed_update
      
                                              IF (AddToAudit(job:ref_number,'EXC','RAPID QA UPDATE: ELECTRONIC QA PASSED (EXC)','EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number)))
                                              END ! IF
      
                                              GetStatus(620,1,'EXC') !Electronic QA passed
      
                                              access:jobs.update()
      
                                              if glo:WebJob = true and job:date_completed <> ''
                                                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                                  jobe:RefNumber = job:ref_number
                                                  if not Access:JOBSE.Fetch(jobe:RefNumberKey)
                                                      jobe:Despatched = 'REA'
                                                      jobe:DespatchType = 'EXC'
                                                      Access:JOBSE.TryUpdate()
      
                                                      save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                                      Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                                      wob:RefNumber = job:Ref_Number
                                                      If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                          !Found
                                                          wob:ReadyToDespatch = 1
                                                          wob:DespatchCourier = job:Exchange_Courier
                                                          Access:WEBJOB.TryUpdate()
      
                                                      Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                          !Error
                                                      End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                      Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                                                  end
                                              end
      
                                              access:exchange.clearkey(xch:ref_number_key)
                                              xch:ref_number = job:exchange_unit_number
                                              if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                  xch:available = 'QA2'
                                                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                                  access:exchange.update()
                                                  get(exchhist,0)
                                                  if access:exchhist.primerecord() = level:benign
                                                      exh:ref_number   = xch:ref_number
                                                      exh:date          = today()
                                                      exh:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      exh:user = use:user_code
                                                      exh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                                      access:exchhist.insert()
                                                  end!if access:exchhist.primerecord() = level:benign
                                              end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                              If def:QAPassLabel = 1
                                                  glo:Select1  = job:Exchange_Unit_Number
                                                  QA_Passed_Label_Exchange
                                                  glo:Select1  = ''
                                              End!If def:QAPassLabel = 1
      
      
                                          Of 'PRI'
                                              error_courier# = 0
                                              If job:exchange_courier = ''
                                                  Case Missive('There is no courier attached to this exchange unit. You will not be able to QA this unit until you have attached one, as it will not able to be despatched correctly. '&|
                                                    '<13,10>'&|
                                                    '<13,10>Do you wish to select a courier now?','ServiceBase 3g',|
                                                                 'mquest.jpg','\No|/Yes')
                                                      Of 2 ! Yes Button
                                                          saverequest#      = globalrequest
                                                          globalresponse    = requestcancelled
                                                          globalrequest     = selectrecord
                                                          browse_courier
                                                          if globalresponse = requestcompleted
                                                              job:exchange_courier = cou:courier
                                                          else
                                                              error_courier# = 1
                                                              Case Missive('This unit has not been QA''d at this time.','ServiceBase 3g',|
                                                                             'mstop.jpg','/OK')
                                                                  Of 1 ! OK Button
                                                              End ! Case Missive
                                                          end
                                                          globalrequest     = saverequest#
                                                      Of 1 ! No Button
                                                          error_courier# = 1
                                                  End ! Case Missive
                                              End!If job:exchange_courier = ''
                                              If error_courier# = 1
                                                  do failed_update
                                              Else!If error_courier# = 1
                                                  do passed_update
      
                                                  job:Exchange_Despatched = DespatchANC(job:Exchange_Courier,'EXC')
      
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  job:current_courier = job:exchange_courier
                                                  Access:courier.clearkey(cou:courier_key)
                                                  cou:courier = job:exchange_courier
                                                  access:courier.tryfetch(cou:courier_key)
                                                  If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                      job:excservice = cou:service
                                                  Else!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                      job:excservice = ''
                                                  End!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
      
                                                  GetStatus(110,1,'EXC') !Despatch Exchange Unit
      
                                                  IF (AddToAudit(job:ref_number,'EXC','RAPID QA UPDATE: MANUAL QA PASSED (EXC)','EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number)))
                                                  END ! IF
      
                                                  !Reprice the job so the warranty despatch note
                                                  !is correct. Hopefully -  (DBH: 02-12-2003)
                                                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                                  jobe:RefNumber    = job:Ref_Number
                                                  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                    !Found
                                                      JobPricingRoutine(0)
                                                      Access:JOBSE.Update()
                                                  Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                    !Error
                                                  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                  access:jobs.update()
      
                                                  if glo:WebJob = true and job:date_completed <> ''
                                                      Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                                      jobe:RefNumber = job:ref_number
                                                      if not Access:JOBSE.Fetch(jobe:RefNumberKey)
                                                          jobe:Despatched = 'REA'
                                                          jobe:DespatchType = 'EXC'
                                                          Access:JOBSE.TryUpdate()
      
                                                          save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                                          Access:WEBJOB.Clearkey(wob:RefNumberKEy)
                                                          wob:RefNumber = job:Ref_Number
                                                          If Access:WEBJOB.Tryfetch(wob:RefNumberKEy) = Level:Benign
                                                            !Found
                                                            wob:ReadyToDespatch = 1
                                                            wob:DespatchCourier = job:Exchange_Courier
      
                                                            Access:WEBJOB.TryUpdate()
                                                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKEy) = Level:Benign
                                                            !Error
                                                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKEy) = Level:Benign
                                                          Access:WEBJOB.RestoreFile(save_WEBJOB_id)
      
                                                      end
                                                  end
      
                                                  access:exchange.clearkey(xch:ref_number_key)
                                                  xch:ref_number = job:exchange_unit_number
                                                  if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                      !Don't understand why the unit is being marked
                                                      !as "exchanged" again.. it should of already been set
                                                      !xch:available = 'EXC'
                                                      access:exchange.update()
                                                      get(exchhist,0)
                                                      if access:exchhist.primerecord() = level:benign
                                                          exh:ref_number   = xch:ref_number
                                                          exh:date          = today()
                                                          exh:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          exh:user = use:user_code
                                                          exh:status        = 'QA PASS ON JOB NO: ' & CLip(job:Ref_number)
                                                          access:exchhist.insert()
                                                      end!if access:exchhist.primerecord() = level:benign
                                                  end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
      
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Exchange_Unit_Number
                                                      QA_Passed_Label_Exchange
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
      
                                              End!If error_courier# = 0
                                      End!Case f_type
                              End!Case AccessoryCheck('EXCHANGE')
                          End!If handset_type_temp = 'Exchange'
                          IF handset_type_temp = 'Loan'
      
                              continueLOA# = True
      
      !                        if SentToHub(job:Ref_Number) and jobe:hubRepair = 0
      !                            ! unit was sent to ARC and is now at RRC
      !                            continueLOA# = true
      !                        else
      !                            if job:loan_consignment_number = ''
      !                                continueLOA# = true
      !                            end
      !                        end
      
                              if continueLOA# = true
                                  temp# = AccessoryCheck('LOAN')
                                  Case temp#
                                      Of 1 Orof 2 !Failed QA
                                          IF temp# = 1 THEN reason" = 'ACCESSORIES MISSING'.
                                          notes" = ''
                                          QA_Failure_Reason(reason",notes")
      
                                              locAuditNotes         = 'ACCESSORIES BOOKED IN:-'
                                              save_lac_id = access:loanacc.savefile()
                                              access:loanacc.clearkey(lac:ref_number_key)
                                              lac:ref_number = job:Loan_unit_Number
                                              set(lac:ref_number_key,lac:ref_number_key)
                                              loop
                                                  if access:loanacc.next()
                                                     break
                                                  end !if
                                                  if lac:ref_number <> job:loan_unit_number      |
                                                      then break.  ! end if
                                                  locAuditNotes   = Clip(locAuditNotes) & '<13,10>' & Clip(lac:accessory)
                                              end !loop
                                              access:loanacc.restorefile(save_lac_id)
      
                                              locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                              Loop x# = 1 To Records(glo:queue)
                                                  Get(glo:queue,x#)
                                                  locAuditNotes       = CLip(locAuditNotes) & '<13,10>' & Clip(glo:pointer)
                                              End!Loop x# = 1 To Records(glo:queue)
      
                                              !Added by Paul - 03/09/2009 - log no 10785
                                              If clip(notes") <> '' then
                                                Do AddEngNotes
                                              End !If clip(notes") <> '' then
                                              !End Addition
      
                                              !Save Notes For Report
                                              locAuditNotes       = CLip(locAuditNotes) & '<13,10>' & clip(notes")
                                              glo:notes_global    = Clip(locAuditNotes)
      
                                              Case f_type
                                                  Of 'PRE'
                                                      locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                  Of 'PRI'
                                                      locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                              End !Case f_type
      
                                          IF (AddToAudit(job:ref_number,'LOA',locAction,locAuditNotes))
                                          END ! IF
      
                                          glo:select1 = job:loan_unit_number
                                          If def:qa_failed_label = 'YES'
                                              QA_Failed_Label_Loan
                                          End!If def:qa_failed_label = 'YES'
                                          If def:qa_failed_report = 'YES'
                                              QA_Failed_Loan
                                          End!If def:qa_failed_report = 'YES'
                                          glo:select1 = ''
                                          glo:notes_global    = ''
                                          Case f_type
                                              Of 'PRE'
                                                  GetStatus(625,1,'LOA')
                                              Else!Case f_type
                                                  GetStatus(615,1,'LOA')
                                          End!Case f_type
                                          DO Allocate_Prev_Eng
                                          job:qa_rejected = 'YES'
                                          job:date_qa_rejected    = Today()
                                          job:time_qa_rejected    = Clock()
                                          access:jobs.update()
                                          Do passed_fail_update
      
                                      Of 3   !Cancelled
      
                                      Of 0 !Passed
                                          Case f_type                                                             !to be despatched.
                                              Of 'PRE'
                                                  Do passed_update
      
                                                  IF (AddToAudit(job:ref_number,'LOA','RAPID QA UPDATE: ELECTRONIC QA PASSED (LOA)','LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number)))
                                                  END ! IF
      
                                                  GetStatus(620,1,'LOA')
      
                                                  access:jobs.update()
      
                                                  if glo:WebJob = true and job:date_completed <> ''
                                                      Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                                      jobe:RefNumber = job:ref_number
                                                      if not Access:JOBSE.Fetch(jobe:RefNumberKey)
                                                          jobe:Despatched = 'REA'
                                                          jobe:DespatchType = 'LOA'
                                                          Access:JOBSE.TryUpdate()
                                                          save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                                          wob:RefNumber = job:Ref_Number
                                                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                            !Found
                                                              wob:ReadyToDespatch = 1
                                                              wob:DespatchCourier = job:Loan_Courier
                                                              Access:WEBJOB.TryUpdate()
                                                          Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                              !Error
                                                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                          Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                                                      end
                                                  end
      
                                                  access:loan.clearkey(loa:ref_number_key)
                                                  loa:ref_number = job:loan_unit_number
                                                  if access:loan.tryfetch(loa:ref_number_key) = Level:benign
                                                      loa:available = 'QA2'
                                                      !Paul 02/06/2009 Log No 10684
                                                      loa:StatusChangeDate = today()
                                                      access:loan.update()
                                                      get(loanhist,0)
                                                      if access:loanhist.primerecord() = level:benign
                                                          loh:ref_number   = xch:ref_number
                                                          loh:date          = today()
                                                          loh:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          loh:user = use:user_code
                                                          loh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                                          access:loanhist.insert()
                                                      end!if access:exchhist.primerecord() = level:benign
                                                  end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Loan_Unit_Number
                                                      QA_Passed_Label_Loan
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
      
      
                                              Of 'PRI'
                                                  error_courier# = 0
                                                  If job:loan_courier = ''
                                                      Case Missive('There is no courier attached to this loan unit. You will not be able to QA this unit until you have attached one, as it  will not be able to be despatched correctly.'&|
                                                        '<13,10>'&|
                                                        '<13,10>Do you wish to select a Courier now?','ServiceBase 3g',|
                                                                     'mquest.jpg','\No|/Yes')
                                                          Of 2 ! Yes Button
                                                              saverequest#      = globalrequest
                                                              globalresponse    = requestcancelled
                                                              globalrequest     = selectrecord
                                                              browse_courier
                                                              if globalresponse = requestcompleted
                                                                  job:loan_courier = cou:courier
                                                              else
                                                                  error_courier# = 1
                                                                  Case Missive('This unit has not been QA''d at this time.','ServiceBase 3g',|
                                                                                 'mstop.jpg','/OK')
                                                                      Of 1 ! OK Button
                                                                  End ! Case Missive
                                                              end
                                                              globalrequest     = saverequest#
                                                          Of 1 ! No Button
                                                              error_courier# = 1
                                                      End ! Case Missive
      
                                                  End!If job:loan_courier = ''
                                                  If job:error_Courier# = 1
                                                      do failed_update
                                                  Else!If job:error_Courier# = 1
                                                      do passed_update
      
                                                      job:Loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')
                                                      job:current_courier = job:loan_courier
      
                                                      Access:courier.clearkey(cou:courier_key)
                                                      cou:courier = job:loan_courier
                                                      access:courier.tryfetch(cou:courier_key)
                                                      If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                          job:loaservice = cou:service
                                                      Else!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                          job:loaservice = ''
                                                      End!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
      
                                                      GetStatus(105,1,'LOA')
      
                                                      IF (AddToAudit(job:ref_number,'LOA','RAPID QA UPDATE: MANUAL QA PASSED (LOA)','LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number)))
                                                      END ! IF
      
                                                      access:jobs.update()
      
                                                      if glo:WebJob = true and job:date_completed <> ''
                                                          Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                                          jobe:RefNumber = job:ref_number
                                                          if not Access:JOBSE.Fetch(jobe:RefNumberKey)
                                                              jobe:Despatched = 'REA'
                                                              jobe:DespatchType = 'LOA'
                                                              Access:JOBSE.TryUpdate()
                                                              save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                                              Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                                              wob:RefNumber = job:Ref_Number
                                                              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                                  !Found
                                                                  wob:ReadyToDespatch = 1
                                                                  wob:DespatchCourier = job:Loan_Courier
                                                                  Access:WEBJOB.TryUpdate()
                                                              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                                  !Error
                                                              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                              Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                                                          end
                                                      end
      
                                                      access:loan.clearkey(loa:ref_number_key)
                                                      loa:ref_number = job:loan_unit_number
                                                      if access:loan.tryfetch(loa:ref_number_key) = Level:benign
                                                          !loa:available = 'LOA'
                                                          access:loan.update()
                                                          get(loanhist,0)
                                                          if access:loanhist.primerecord() = level:benign
                                                              loh:ref_number   = xch:ref_number
                                                              loh:date          = today()
                                                              loh:time          = clock()
                                                              access:users.clearkey(use:password_key)
                                                              use:password =glo:password
                                                              access:users.fetch(use:password_key)
                                                              loh:user = use:user_code
                                                              loh:status        = 'QA PASS ON JOB NO: ' & CLip(job:Ref_number)
                                                              access:loanhist.insert()
                                                          end!if access:exchhist.primerecord() = level:benign
                                                      end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                  End!If job:error_Courier# = 1
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Loan_Unit_Number
                                                      QA_Passed_Label_Loan
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
                                          End!Case f_type
                                  End!If pass# = 1
                              end
                          End!IF handset_type_temp = 'Loan'
                      End!If error# = 0
                  ELse!!If hanset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
    !QA Pass part too... too long for one embed
                    !message('Debug just past the break')
                    If ~man:UseQA
                        Case Missive('This manufacturer is not set-up to QA units.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    Else !If ~man:UseQA
                        if handset_type_temp = 'Repair' or handset_type_temp = 'Third Party Repair'
                            if SentToHub(job:Ref_Number) and jobe:hubRepair = 0
                                ! unit was sent to ARC and is now at RRC
                            else
                                if job:date_Completed <> ''
                                    Case Missive('Cannot QA! The selected job has been completed.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    error# = 1
                                end !If job:date_Completed = ''
                            end
                        end !If handset_type_temp = 'Repair'

                        If error# = 0
                            !message('Debug got error# = 0')
                            If CheckRefurb()
                                Case Missive('This unit is authorised for refurbishment.'&|
                                  '<13,10>'&|
                                  '<13,10>Is the refurbishment complete?','ServiceBase 3g',|
                                               'mquest.jpg','\No|/Yes')
                                    Of 2 ! Yes Button
                                    Of 1 ! No Button
                                        error# = 1
                                        job:qa_rejected = 'YES'
                                        job:date_qa_rejected    = Today()
                                        job:time_qa_Rejected    = Clock()
                                        status_audit# = 1
                                        Case f_type
                                            Of 'PRE'
                                                GetStatus(625,1,'JOB')
                                            Else
                                                GetStatus(615,1,'JOB')
                                        End!Case f_type
                                        access:jobs.update()
                                        reason" = ''
                                        notes" = ''
                                        QA_Failure_Reason(reason",notes")

                                        !Added by Paul - 03/09/2009 - log no 10785
                                        If clip(notes") <> '' then
                                          Do AddEngNotes
                                        End !If clip(notes") <> '' then
                                        !End Addition

                                        Case f_type
                                            Of 'PRE'
                                                locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                            Of 'PRI'
                                                locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                        End !Case f_type
                                        IF (AddToAudit(job:ref_number,'JOB',locAction,'REFURBISHMENT NOT COMPLETE' & '<13,10>' & clip(notes")))
                                        END ! IF

                                        glo:notes_global    = Clip(Reason")
                                        glo:select1 = job:ref_number
                                        If def:qa_failed_label = 'YES'
                                            QA_Failed_Label
                                        End!If def:qa_failed_label = 'YES'
                                        If def:qa_failed_report = 'YES'
                                            QA_Failed
                                        End!If def:qa_failed_report = 'YES'
                                        glo:select1 = ''
                                        glo:notes_global    = ''
                                        if job:date_Completed = ''
                                            do Allocate_Prev_Eng
                                        else
                                            do CreateNewJob
                                        end

                                End ! Case Missive
                            End!If CheckRefurb()
                            If error# = 0
                                If job:exchange_unit_number = ''
                                    type# = 1 !Job
                                    accchk# =  AccessoryCheck('JOB')
                                    Case accchk#
                                        Of 1 orof 2
                                            reason" = ''
                                            If accchk# = 1
                                                Reason" = 'ACCESSORIES MISSING'
                                            End !If accchk# = 1
                                            notes" = ''
                                            QA_Failure_Reason(reason",notes")

                                                locAuditNotes         = 'ACCESSORIES BOOKED IN:-'
                                                save_jac_id = access:jobacc.savefile()
                                                access:jobacc.clearkey(jac:ref_number_key)
                                                jac:ref_number = job:Ref_number
                                                set(jac:ref_number_key,jac:ref_number_key)
                                                loop
                                                    if access:jobacc.next()
                                                       break
                                                    end !if
                                                    if jac:ref_number <> job:Ref_number      |
                                                        then break.  ! end if
                                                    !Only show accessory attached/received by ARC - 4285 (DBH: 11-06-2004)
                                                    If ~glo:WebJob And ~jac:Attached
                                                        Cycle
                                                    End !If ~glo:WebJob And ~jac:Attached
                                                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>' & Clip(jac:accessory)
                                                end !loop
                                                access:jobacc.restorefile(save_jac_id)

                                                !Added by Paul - 03/09/2009 - log no 10785
                                                If clip(notes") <> '' then
                                                  Do AddEngNotes
                                                End !If clip(notes") <> '' then
                                                !End Addition

                                                locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                                Loop x# = 1 To Records(glo:queue)
                                                    Get(glo:queue,x#)
                                                    locAuditNotes       = CLip(locAuditNotes) & '<13,10>' & Clip(glo:pointer)
                                                End!Loop x# = 1 To Records(glo:queue)
                                                !Save Notes For Report
                                                locAuditNotes       = CLip(locAuditNotes) & '<13,10>' & clip(notes")
                                                glo:notes_global    = Clip(locAuditNotes)

                                                Case f_type
                                                    Of 'PRE'
                                                        locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                    Of 'PRI'
                                                        locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                                End !Case f_type
                                            IF (AddToAudit(job:ref_number,'JOB',locAction,locAuditNotes))
                                            END ! IF


                                            glo:select1 = job:ref_number
                                            If def:qa_failed_label = 'YES'
                                                QA_Failed_Label
                                            End!If def:qa_failed_label = 'YES'
                                            If def:qa_failed_report = 'YES'
                                                QA_Failed
                                            End!If def:qa_failed_report = 'YES'
                                            glo:select1 = ''
                                            glo:notes_global    = ''
                                            Case f_type
                                                Of 'PRE'
                                                    GetStatus(625,1,'JOB')
                                                Else!Case f_type
                                                    GetStatus(615,1,'JOB')
                                            End!Case f_type
                                            if job:date_Completed = ''
                                                do Allocate_Prev_Eng
                                            end
                                            job:qa_rejected = 'YES'
                                            job:date_qa_rejected    = Today()
                                            job:time_qa_rejected    = Clock()
                                            access:jobs.update()
                                            Do passed_fail_update

                                    Of 3
                                        pass# = 0
                                    Of 0
                                        pass# = 1
                                    End!If AccessoryCheck('EXCHANGE')
                                Else!If job:exchange_uit_number = ''
                                    pass# = 1
                                End!If job:exchange_uit_number = ''
                            End!If error# = 0

!                            If pass# = 1
!                                pass# = Local.ValidateParts()
!                            End !If pass# = 1

                            If pass# = 1
                                pass# = Local.ValidateNetwork()
                            End !If pass# = 1

                            If pass# = 1                                                                    !If passed fill in all the relevant
                                !message('Debug got f_type of '&clip(f_type))
                                Case f_type
                                    Of 'PRE'
                                        GetStatus(620,1,'JOB')

                                        IF (AddToAudit(job:ref_number,'JOB','RAPID QA UPDATE: ELECTRONIC QA PASSED',''))
                                        END ! IF

                                        If def:QAPassLabel = 1
                                            glo:Select1  = job:Ref_Number
                                            QA_Passed_Label
                                            glo:Select1  = ''
                                        End!If def:QAPassLabel = 1
                                        access:jobs.update()

    !                                    if glo:WebJob = true and job:date_completed <> ''
    !                                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
    !                                        jobe:RefNumber = job:ref_number
    !                                        if not Access:JOBSE.Fetch(jobe:RefNumberKey)
    !                                            jobe:Despatched = 'REA'
    !                                            Access:JOBSE.TryUpdate()
    !                                        end
    !                                    end

                                        Do passed_update


                                    Of 'PRI'
                                        !message('Debug got pri')
                                        !Auto complete the job... will need to default this later
                                        Complete# = 0
                                        CompleteError# = 0
                                        If man:UseQA
                                            If man:QAAtCompletion
                                                ! Inserting (DBH 19/04/2006) #7551 - Need to get the web job file to make sure we know the faultcodes 13-20
                                                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                                wob:RefNumber   = job:Ref_Number
                                                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                    ! Found

                                                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                    ! Error
                                                End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                ! End (DBH 19/04/2006) #7551
                                                glo:ErrorText = ''
                                                CompulsoryFieldCheck('C')
                                                If glo:ErrorText <> ''
                                                    glo:ErrorText = 'QA FAILURE! You cannot QA because of the following error(s): <13,10>' & Clip(glo:ErrorText)
                                                    Error_Text
                                                    glo:errortext = ''

                                                    reason" = ''
                                                    notes" = ''
                                                    QA_Failure_Reason(reason",notes")
                                                    CompleteError# = 1

                                                    !Added by Paul - 03/09/2009 - log no 10785
                                                    If clip(notes") <> '' then
                                                      Do AddEngNotes
                                                    End !If clip(notes") <> '' then
                                                    !End Addition

                                                    Case f_type
                                                        Of 'PRE'
                                                            locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                        Of 'PRI'
                                                            locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                                    End !Case f_type
                                                    IF (AddToAudit(job:ref_number,'JOB',locAction,'FAILED COMPLETION CHECK' & '<13,10>' & clip(notes")))
                                                    END ! IF


                                                    glo:select1 = job:ref_number
                                                    ! Inserting (DBH 20/04/2006) #7562 - Not show reason if failed due to missing fields
                                                    glo:Notes_Global = Clip(Reason")
                                                    ! End (DBH 20/04/2006) #7562
                                                    If def:qa_failed_label = 'YES'
                                                        QA_Failed_Label
                                                    End!If def:qa_failed_label = 'YES'
                                                    If def:qa_failed_report = 'YES'
                                                        QA_Failed
                                                    End!If def:qa_failed_report = 'YES'
                                                    glo:select1 = ''
                                                    glo:notes_global    = ''
                                                    GetStatus(615,1,'JOB')
                                                    job:qa_rejected = 'YES'
                                                    job:date_qa_rejected    = Today()
                                                    job:time_qa_rejected    = Clock()
                                                    access:jobs.update()
                                                    Do passed_fail_update
                                                    if job:date_Completed = ''
                                                        do Allocate_Prev_Eng
                                                    else
                                                        do CreateNewJob
                                                    end

                                                Else
                                                    Complete# = 1
                                                End !If glo:ErrorText <> ''
                                            Else
                                                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                                jobe:RefNumber  = job:Ref_Number
                                                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                    !Found
                                                    If jobe:WebJob and ~glo:WebJob
                                                        stanum# = Sub(GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                                                        GetStatus(stanum#,0,'JOB')
                                                    Else !If jobe:WebJob
                                                        GetStatus(610,1,'JOB')
                                                    End !If jobe:WebJob
                                                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                    !Error
                                                    GetStatus(610,1,'JOB')
                                                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                                            End !If man:QAAtCompletion
                                            !message('Debug CompleteError#='&clip(CompleteError#))
                                            If CompleteError# = 0
                                                job:qa_passed      = 'YES'
                                                job:date_qa_passed = Today()
                                                job:time_qa_passed = Clock()
!
!                                                IF (AddToAudit(job:ref_number,'JOB','RAPID QA UPDATE: MANUAL QA PASSED',''))
!                                                END ! IF

                                                If def:QAPassLabel = 1
                                                    glo:Select1  = job:Ref_Number
                                                    QA_Passed_Label
                                                    glo:Select1  = ''
                                                End!If def:QAPassLabel = 1

                                                If Complete# = 1
                                                    !Call new pricing routine - L945 (DBH: 04-09-2003)
                                                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                                    jobe:RefNumber = job:Ref_Number
                                                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                      !Found

                                                    Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                      !Error
                                                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                                                    JobPricingRoutine(0)
                                                    Access:JOBSE.Update()

                                                    !Text too long, so will call a procedure
                                                    Do CompletedBit

    !                                                ThisMakeover.SetWindow(win:form)
                                                End !If Complete# = 1
                                                IF (Access:JOBS.Update())
                                                    AddToLog('Debug',ERROR(),'JOBS Save Error',job:Ref_Number)   ! #13614 Extra logging incase of error (DBH: 12/10/2015)
                                                    BEEP(BEEP:SystemHand)  ;  YIELD()
                                                    CASE Missive('An error occurred saving the job record. '&|
                                                        '|Please check that the selected job is not in use and try again.','ServiceBase',|
                                                                   'mstop.jpg','/&OK') 
                                                    OF 1 ! &OK Button
                                                    END!CASE MESSAGE
                                                ELSE ! IF
                                                    IF (AddToAudit(job:ref_number,'JOB','RAPID QA UPDATE: MANUAL QA PASSED',''))
                                                        ! #13610 Add Audit after the job has saved. A double check to see if the job has saved (DBH: 12/10/2015)
                                                    END ! IF
                                                !message('Debug - before check for date completed|Have glo:Webjob='&clip(glo:Webjob)&' date='&clip(job:Datecompleted))
                                                    if glo:WebJob = true and job:date_completed <> ''

                                                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                                        jobe:RefNumber = job:Ref_Number
                                                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                            !Found
                                                            !message('DEbug - jobse found OK')
                                                            jobe:Despatched = 'REA'
                                                            jobe:DespatchType = 'JOB'
                                                            If Access:JOBSE.Update()
                                                                Case Missive('An error has occurred. This job will NOT appear in the despatch table.'&|
                                                                  '<13,10>'&|
                                                                  '<13,10>Please contact your system supervisor.','ServiceBase 3g',|
                                                                               'mstop.jpg','/OK')
                                                                    Of 1 ! OK Button
                                                                End ! Case Missive
                                                            Else
                                                                save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                                                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                                                wob:RefNumber = job:Ref_Number
                                                                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                                    !Found
                                                                    !message('Debug webjob found OK')
                                                                    wob:ReadyToDespatch = 1
                                                                    wob:DespatchCourier = job:Courier
                                                                    Access:WEBJOB.TryUpdate()
                                                                    !added log no 10321 bt PS 29/04/09
                                                                    !check if this is a BER or Liquid and if so - send an SMS message
                                                                    !Warranty Job
                                                                    If job:Warranty_Job = 'YES' THEN
                                                                        !message('Debug this is the warranty bit')
                                                                        !check the job type
                                                                        Access:REPTYDEF.CLEARKEY(rtd:WarManRepairTypeKey)
                                                                        rtd:Manufacturer = job:Manufacturer
                                                                        rtd:Warranty = 'YES'
                                                                        rtd:Repair_Type = job:Warranty_Charge_Type
                                                                        IF ~Access:REPTYDEF.Fetch(rtd:WarManRepairTypeKey) THEN
                                                                           !message('Debug - got repair type '&clip(rtd:Repair_Type)&' with BER='&cliP(rtd:BER)&' SMSsend of '&clip(rtd:SMSSendType))
                                                                           IF rtd:BER = 1 or rtd:BER = 4 THEN
                                                                               !this is a BER or Liquid damage job
                                                                               ! Insert --- Don't send message for VCP jobs (DBH: 04/01/2010) #11014
                                                                                if (job:Who_Booked <> 'WEB')
                                                                                ! end --- (DBH: 04/01/2010) #11014
                                                                                   !TB012477 - SMS notification dealt with elsewhere  JC 13/04/12
    !                                                                               If jobe2:SMSNotification
    !                                                                                    if rtd:BER = 1 or rtd:BER = 4 then
    !                                                                                        Case rtd:SMSSendType
    !                                                                                            of 'B'
    !                                                                                                SendSMSText('J','Y','B')    !beyond economic repair
    !                                                                                            of 'L'
    !                                                                                                SendSMSText('J','Y','L')    !liquid damage = BER
    !                                                                                         END !case SMSSendType
    !                                                                                     END !if BER etc
    !                                                                                     SendSMSText('J','Y','L')    !liquid damage = BER
    !                                                                                    if glo:ErrorText[1:5] = 'ERROR' then
    !                                                                                        miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
    !                                                                                    ELSE
    !                                                                                        miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
    !                                                                                    END
    !
    !                                                                                   !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BERL','SMS',jobe2:SMSAlertNumber,'','','')
    !                                                                               End ! If jobe2:SMSNotification
                                                                                   If jobe2:EmailNotification
                                                                                       AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BERL','EMAIL','',jobe2:EmailAlertAddress,'','')
                                                                                   END
                                                                               end ! if (job:Who_Booked <> 'WEB')
                                                                           END !IF
                                                                        END !IF
                                                                    End!If job:Warranty_Job = 'YES' THEN
                                                                    !chargeable job
                                                                    IF job:Chargeable_Job = 'YES' THEN
                                                                        !check the job type
                                                                        !message('Debug |This is the chargeable bit.')
                                                                        Access:REPTYDEF.CLEARKEY(rtd:ChaManRepairTypeKey)
                                                                        rtd:Manufacturer = job:Manufacturer
                                                                        rtd:Chargeable = 'YES'
                                                                        rtd:Repair_Type = job:Repair_Type
                                                                        IF ~Access:REPTYDEF.Fetch(rtd:ChaManRepairTypeKey) THEN
                                                                            !message('Debug - got repair type '&clip(rtd:Repair_Type)&' with BER='&cliP(rtd:BER)&' SMSsend of '&clip(rtd:SMSSendType))
                                                                            IF rtd:BER = 1 or rtd:BER = 4 THEN
                                                                                !this is a BER or Liquid damage job
                                                                               ! Insert --- Don't send message for VCP jobs (DBH: 04/01/2010) #11014
                                                                                if (job:Who_Booked <> 'WEB')
                                                                                ! end --- (DBH: 04/01/2010) #11014
    !                                                                                If jobe2:SMSNotification
                                                                                        !AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BERL','SMS',jobe2:SMSAlertNumber,'','','')
                                                                                        !TB012477 - SMS notification dealt with elsewhere  JC 13/04/12
    !                                                                                    Case rtd:SMSSendType
    !                                                                                        of 'B'
    !                                                                                            SendSMSText('J','Y','B')    !beyond economic repair
    !                                                                                        of 'L'
    !                                                                                            SendSMSText('J','Y','L')    !beyond economic repair
    !                                                                                    END !case SMSSendType
    !!
    !                                                                                    if glo:ErrorText[1:5] = 'ERROR' then
    !                                                                                        miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
    !                                                                                    ELSE
    !                                                                                        miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
    !                                                                                    END
    !                                                                                End ! If jobe2:SMSNotification

                                                                                    If jobe2:EmailNotification
                                                                                        AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'BERL','EMAIL','',jobe2:EmailAlertAddress,'','')
                                                                                    End ! If jobe2:EmailNotification
                                                                                end !if (job:Who_Booked <> 'WEB')
                                                                            END !IF
                                                                        END !IF
                                                                    END !IF
                                                                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                                                  !Error
                                                                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
    !                                                              ! Inserting (DBH 23/01/2007) # 8678 - Set the status depending of it the account is "despatch/invoice paid" only
    !                                                              SetDespatchStatus()
    !                                                              Stop('Status Set')
    !                                                              ! End (DBH 23/01/2007) #8678
                                                                Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                                                                ! Inserting (DBH 06/03/2007) # 8703 - If the job has come back from the ARC, and the RRC retained accessories, display a message.
                                                                If SentToHub(job:Ref_Number)
                                                                    Access:JOBACC.Clearkey(jac:Ref_Number_Key)
                                                                    jac:Ref_Number = job:Ref_Number
                                                                    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                                                                    Loop ! Begin Loop
                                                                        If Access:JOBACC.Next()
                                                                            Break
                                                                        End ! If Access:JOBACC.Next()
                                                                        If jac:Ref_Number <> job:Ref_Number
                                                                            Break
                                                                        End ! If jac:Ref_Number <> job:Ref_Number
                                                                        If jac:Attached = 0
                                                                            Case Missive('The selected job has accessories that have been retained by the RRC.','ServiceBase 3g',|
                                                                                           'mexclam.jpg','/OK')
                                                                                Of 1 ! OK Button
                                                                            End ! Case Missive
                                                                            Break
                                                                        End ! If jac:Attached = 0
                                                                    End ! Loop
                                                                End ! If SentToHub(job:Ref_Number)
                                                                ! End (DBH 06/03/2007) #8703

                                                            End !If Access:JOBSE.Update()

                                                        Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                                            !Error
                                                            Case Missive('An error has occurred. This job will NOT appear in the despatch table.'&|
                                                              '<13,10>'&|
                                                              '<13,10>Please contact your system supervisor.','ServiceBase 3g',|
                                                                           'mstop.jpg','/OK')
                                                                Of 1 ! OK Button
                                                            End ! Case Missive
                                                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                                                    end !if glo:WebJob = true and job:date_completed <> ''

                                                    !message('debug - beyond the glo:webjob and date check')
                                                    !Do closing
                                                    Do Passed_Update
                                                END ! IF (Access:JOBS.Update())
                                            End !If CompleteError# = 0
                                        End !If man:UseQA
                                End!Case f_type
                            End!If pass# = 1
                        Else!If error# = 0
                            Do failed_update
                        End!If error# = 0
                    End !If ~man:UseQA
                End!If hanset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
            End !If f_Type = 'PRE' And ~man:UseElectronicQA
        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            Case Missive('Error! Cannot find the manufacturer attached to this job.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    end!if access:jobs.fetch(job:ref_number_key) = Level:Benign

    job_number_temp = ''
    wob_number_temp = ''
    model_details_temp = ''
    handset_type_temp  = ''
    date_booked_temp   = ''
    tmp:PassedJobType = ''
      glo:Select40 = 'REPEAT'
      
      !TB012477 - SMS notification dealt with here  JC 13/04/12
      !Message('In the last bit of QA pass accepted with '&clip(BER_SMS_SEND)&clip(Job:Who_Booked)&clip(jobe2:SMSNotification))
      if BER_SMS_Send <> 'N' and job:Who_Booked <> 'WEB' and jobe2:SMSNotification then
          SendSMSTEXT('J','Y',BER_SMS_Send)
          !Debug erroring turned off for distruibution
!          if glo:ErrorText[1:5] = 'ERROR' then
!              miss# = missive('Unable to send SMS as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'Service Base 3g','mstop.jpg','OK' )
!          ELSE
!              miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'Service Base 3g','mwarn.jpg','OK' )
!          END
          glo:errorText = ''
      END !Ifthe sms for ber or liquid damage needs to be sent
      
      Post(Event:CloseWindow)
    OF ?qa_fail
      ThisWindow.Update
      ThisWindow.PostCompleted()
      Set(Defaults)
      access:Defaults.next()
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          error# = 0
          Access:JOBSE.Clearkey(jobe:RefNumberKey)
          jobe:RefNumber  = job:Ref_Number
          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              !Found
      
          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              !Error
          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Found
              If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan' Or Handset_Type_Temp = '2nd Exchange'
                  If ~man:QALoanExchange
                      error# = 1
                      Case Missive('This manufacturer is not set-up to QA Loan/Exchange units.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  Else!If def:qaexchLoan <> 'YES'
                      error# = 1
                      If Handset_Type_Temp = '2nd Exchange'
                          Do Passed_Fail_Update
                          Reason" = ''
                          tmp:notes = ''
                          QA_Failure_Reason(Reason",tmp:notes)
                          glo:Notes_Global = Clip(Reason")
                          If def:qa_failed_label = 'YES'
                              glo:select1  = jobe:SecondExchangeNumber
                              QA_Failed_Label_Exchange
                              glo:select1  = ''
                          End!If def:qa_failed_label = 'YES'
                          If def:qa_failed_report = 'YES'
                              glo:select1 = jobe:SecondExchangeNumber
                              QA_Failed_Exchange
                              glo:select1 = ''
                          End!If def:qa_failed_report = 'YES'
                          glo:notes_global = ''
                          Case F_Type
                              Of 'PRE'
                                  GetStatus(625,1,'2NE')
                              Of 'PRI'
                                  GetStatus(615,1,'2NE')
                          End !Case F_Type
                          Access:JOBSE.Update()
                          Access:JOBS.Update()
      
                          !Added by Paul - 03/09/2009 - log no 10785
                          If clip(tmp:notes) <> '' then
                            Do AddEngNotes
                          End !If clip(tmp:notes) <> '' then
                          !End Addition
      
                          Case f_type
                              Of 'PRE'
                                  locAction        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (EXC)'
                              Of 'PRI'
                                  locAction        = 'RAPID QA UPDATE: MANUAL QA FAILED (EXC)'
                          End!Case f_type
      
                          IF (AddToAudit(job:ref_number,'2NE',locAction,'EXCHANGE UNIT NUMBER: ' & Clip(jobe:SecondExchangeNumber) &|
                                                  '<13,10>QA FAILURE REASON: ' & CLip(Reason") & '<13,10>' & clip(tmp:notes)))
                          END ! IF
      
                          access:jobs.update()
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = jobe:SecondExchangeNumber
                          if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                              xch:available = 'QAF'
                              xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                              access:exchange.update()
                              get(exchhist,0)
                              if access:exchhist.primerecord() = level:benign
                                  exh:ref_number   = xch:ref_number
                                  exh:date          = today()
                                  exh:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  exh:user = use:user_code
                                  exh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLip(Reason")
                                  access:exchhist.insert()
                              end!if access:exchhist.primerecord() = level:benign
                          end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                      End !If Handset_Type_Temp = '2nd Exchange'
                      If handset_type_temp = 'Exchange'                                               !Is it an Exchange that was scanned?
                          If job:exchange_consignment_number = '' OR jobe:ExchangedATRRC = 0          !Check that is isn't already ready
                              Do passed_fail_update
                              reason" = ''
                              tmp:notes = ''
                              qa_failure_reason(reason",tmp:notes)
                              glo:notes_global = Clip(reason")
                              If def:qa_failed_label = 'YES'
                                  glo:select1  = job:exchange_unit_number
                                  QA_Failed_Label_Exchange
                                  glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  glo:select1 = job:exchange_unit_number
                                  QA_Failed_Exchange
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
                              DO Allocate_Prev_Eng
                              job:exchange_status = 'QA FAILED'
                              job:despatched = ''
                              job:despatch_type = ''
                              Case f_type
                                  Of 'PRE'
                                      GetStatus(625,1,'EXC') !electronic qa rejected
      
                                  Of 'PRI'
                                      GetStatus(615,1,'EXC') !manual qa rejected
      
                              End!Case f_type
      
                              !Added by Paul - 03/09/2009 - log no 10785
                              If clip(tmp:notes) <> '' then
                                Do AddEngNotes
                              End !If clip(tmp:notes) <> '' then
                              !End Addition
      
                              Case f_type
                                  Of 'PRE'
                                      locAction        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (EXC)'
                                  Of 'PRI'
                                      locAction        = 'RAPID QA UPDATE: MANUAL QA FAILED (EXC)'
                              End!Case f_type
      
                              IF (AddToAudit(job:ref_number,'EXC',locAction,'EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number) &|
                                                      '<13,10>QA FAILURE REASON: ' & CLip(reason") & '<13,10>' & clip(tmp:notes)))
                              END ! IF
      
                              access:jobs.update()
                              access:exchange.clearkey(xch:ref_number_key)
                              xch:ref_number = job:exchange_unit_number
                              if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                  xch:available = 'QAF'
                                  xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                  access:exchange.update()
                                  get(exchhist,0)
                                  if access:exchhist.primerecord() = level:benign
                                      exh:ref_number   = xch:ref_number
                                      exh:date          = today()
                                      exh:time          = clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      exh:user = use:user_code
                                      exh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLip(Reason")
                                      access:exchhist.insert()
                                  end!if access:exchhist.primerecord() = level:benign
                              end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
      
                              !added by Paul 04/09/2009 - log no 10785
                              !check if this is a '7DAY TAT' as well as an exchange
                              If jobe:Engineer48HourOption = 3 and glo:webJob = 1 then
                                  !yep - its a 7 day tat with an exchange unit so process new information
                                  ! #11550 Only show if qa failing at the RRC (DBH: 10/08/2010)
                                  GetStatus(126,1,'EXC')
      
                                  IF (AddToAudit(job:ref_number,'JOB','EXCHANGE QA FAILED: ' & Clip(reason"),clip(tmp:notes)))
                                  END ! IF
      
      
                                  tmp:audittype = 1
                                  Access:Jobs.update()
                                  do CreateNewJob
                              End !If jobe:Engineer48HourOption = 3 then
      
                          End!If job:exchange_consignment_number = '' And job:despatched <> 'EXC'
                      End!If handset_type_temp = 'Exchange'
                      IF handset_type_temp = 'Loan'
                          If job:loan_consignment_number = ''
                              Do passed_fail_update
                              reason" = ''
                              tmp:notes = ''
                              qa_failure_reason(reason",tmp:notes)
                              glo:notes_global = Clip(reason")
                              If def:qa_failed_label = 'YES'
                                  glo:select1  = job:loan_unit_number
                                  QA_Failed_Label_Loan
                                  glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  glo:select1 = job:loan_unit_number
                                  QA_Failed_Loan
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
                              DO Allocate_Prev_Eng
                              job:loan_status = 'QA FAILED'
                              job:despatched = ''
                              job:despatch_type = ''
                              Case f_type
                                  Of 'PRE'
                                      GetStatus(625,1,'LOA') !electronic qa rejected
      
                                  Of 'PRI'
                                      GetStatus(615,1,'LOA') !electronic qa rejected
      
                              End!Case f_type
                              job:status_end_date = end_date"
                              job:status_end_time = end_time"
      
                              !Added by Paul - 03/09/2009 - log no 10785
                              If clip(tmp:notes) <> '' then
                                Do AddEngNotes
                              End !If clip(tmp:notes) <> '' then
                              !End Addition
      
                              Case f_type
                                  Of 'PRE'
                                      locAction        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (LOA)'
                                  Of 'PRI'
                                      locAction        = 'RAPID QA UPDATE: MANUAL QA FAILED (LOA)'
                              End!Case f_type
                              IF (AddToAudit(job:ref_number,'LOA',locAction,'LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number) &|
                                                      '<13,10>QA FAILURE REASON: ' & CLip(reason") & clip(tmp:notes)))
                              END ! IF
      
                              access:jobs.update()
                              access:loan.clearkey(loa:ref_number_key)
                              loa:ref_number = job:loan_unit_number
                              if access:loan.tryfetch(loa:ref_number_key) = Level:benign
                                  loa:available = 'QAF'
                                  !Paul 02/06/2009 Log No 10684
                                  loa:StatusChangeDate = today()
                                  access:loan.update()
                                  get(loanhist,0)
                                  if access:loanhist.primerecord() = level:benign
                                      loh:ref_number   = xch:ref_number
                                      loh:date          = today()
                                      loh:time          = clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      loh:user = use:user_code
                                      loh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLip(Reason")
                                      access:loanhist.insert()
                                  end!if access:exchhist.primerecord() = level:benign
                              end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                          End!If job:loan_consignment_number = '' And job:despatched <> 'LOA'
                      End!IF handset_type_temp = 'Loan'
      
                  End!If def:qaexchLoan <> 'YES'
      
              Else!!If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber  = job:Ref_Number
                  Access:JOBSE.Fetch(jobe:RefNumberKey)
      
                  if SentToHub(job:Ref_Number) and jobe:hubRepair = 0
                      ! unit was sent to ARC and is now at RRC
                  else
                      if job:date_completed <> ''
                          error# = 1
                          Case Missive('Cannot QA! The selected job has been completed.','ServiceBase 3g',|
                                         'mstop.jpg','/OK')
                              Of 1 ! OK Button
                          End ! Case Missive
                      end
                  End!If job:date_completed <> ''
      
                  If error# = 0
                      job:qa_rejected      = 'YES'
                      job:date_qa_rejected = Today()
                      job:time_qa_rejected = Clock()
                      Case f_type
                          Of 'PRE'
                              GetStatus(625,1,'JOB') !electronic qa rejected
                          Of 'PRI'
                              GetStatus(615,1,'JOB') !manual qa rejected
                      End!Case f_type
      
                      access:jobs.update()
                      Do passed_fail_update
      
                      beep(beep:systemasterisk)
                      reason" = ''
                      tmp:notes = ''
                      qa_failure_reason(reason",tmp:notes)
      
                      !Added by Paul - 03/09/2009 - log no 10785
                      If clip(tmp:notes) <> '' then
                        Do AddEngNotes
                      End !If clip(tmp:notes) <> '' then
                      !End Addition
      
                      Case f_type
                          Of 'PRE'
                              locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                          Of 'PRI'
                              locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                      End !Case f_type
                      IF (AddToAudit(job:ref_number,'JOB',locAction,clip(tmp:notes)))
                      END ! IF
      
                      glo:notes_global = Clip(reason")
                      If def:qa_failed_label = 'YES'
                          glo:select1  = job:ref_number
                          QA_Failed_Label
                          glo:select1  = ''
                      End!If def:qa_failed_label = 'YES'
                      If def:qa_failed_report = 'YES'
                          glo:select1 = job:ref_number
                          QA_Failed
                          glo:select1 = ''
                      End!If def:qa_failed_report = 'YES'
                      glo:notes_global = ''
      
                      if job:date_completed = ''
                          do Allocate_Prev_Eng
                      else
                          ! Job already complete
                          !changes by Paul 03/09/2009 Log No 10785
                          !change the job status to 'ARC QA FAILED - no 626
                          GetStatus(626,1,'JOB')
                          IF (AddToAudit(job:ref_number,'JOB','ARC QA FAILED: ' & Clip(reason"),clip(tmp:notes)))
                          END ! IF
      
                          job:Location = 'DESPATCHED'
                          access:jobs.update()
                          tmp:audittype = 2
                          !Changes End
                          do CreateNewJob
                      end
                  Else!If error# = 0
                      Do failed_update
                  End!If error# = 0
              End!If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
      
          Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Error
              Case Missive('Error! Cannot find the manufacturer attached to this job.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
      Display()
      job_number_temp = ''
      wob_number_temp = ''
      model_details_temp = ''
      handset_type_temp  = ''
      date_booked_temp   = ''
      serial_number_temp = ''
      tmp:PassedJobType = ''
      Display()
      Select(?job_number_temp)
      glo:Select40 = 'REPEAT'
      Post(Event:CloseWindow)
    OF ?Finish
      ThisWindow.Update
      glo:Select40 = ''
      Clear(QAQueue)
      Free(QAQueue)
      Post(Event:CloseWindow)
    OF ?Button:PrintHandover
      ThisWindow.Update
      Free(glo:Q_Invoice)
      
      loop x# = 1 to Records(QAQueue)
          get(QAQueue,x#)
          if (gloque:JobType <> '')
              glo:Account_Number = gloque:JobType
              glo:Invoice_Number = gloque:JobNumber
              Add(glo:Q_Invoice)
          end ! if gloque:JobType <> '')
      end !loop x# = 1 to Records(QAQueue)
      
      if (Records(glo:Q_Invoice) = 0)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('There are no "Handover" units to print.','ServiceBase',|
                         'mstop.jpg','/&OK')
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      end ! if (Records(glo:Q_Invoice) = 0)
      
      QAHandOver
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F5Key
              If ?FaultCodes{prop:Disable} = 0
                  Post(Event:Accepted,?FaultCodes)
              End !If ?FaultCodes{prop:Disable} = 0
          Of F9Key
              If ?QA_Pass{prop:Disable} = 0
                  Post(Event:Accepted,?QA_Pass)
              End !If ?QA_Pass{prop:Disable} = 0
          Of F10Key
              If ?QA_Fail{prop:Disable} = 0
                  Post(Event:Accepted,?QA_Fail)
              End !If ?QA_Fail{prop:Disable} = 0
          Of EscKey
              Post(Event:Accepted,?Finish)
      End !KeyCode()
    OF EVENT:OpenWindow
      Case f_type
          Of 'PRE'
              ?WindowTitle{prop:text} = 'Rapid QA Update: Electronic'
          Of 'PRI'
              ?WindowTitle{prop:text} = 'Rapid QA Update: Manual'
      End!Case f_type
        IF GetComputerName(JB:ComputerName,JB:ComputerLen).
        tmp:WorkstationName=JB:ComputerName
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.ReturnInTransitUnit   Procedure(String f:IMEINumber,String f:CurrentAvailable, String f:Available)
Code
    Access:EXCHANGE.Clearkey(xch:AvailIMEIOnlyKey)
    xch:Available   = f:CurrentAvailable
    xch:ESN         = f:IMEINumber
    if (Access:EXCHANGE.TryFetch(xch:AvailIMEIOnlyKey) = Level:Benign)
        ! Found

        xch:Available = f:Available
        xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)

        case f:Available
        of 'IT4'
            xch:Stock_Type = '48-HOUR SWAP UNITS'
            ! Unit will be returned normally if going to Main Store, so shouldn't appear in Transit Browse (DBH: 20/05/2009) #10822
            if (xch:Location <> 'MAIN STORE')
                xch:InTransit = 1
            end ! if (xch:Location <> 'MAIN STORE')
        of 'ITS'
            xch:Stock_Type = 'SCRAPPING'
            xch:InTransit = 1 ! In Transit
        of 'ITR'
            xch:Stock_Type = 'RTM'
            xch:InTransit = 1 ! In Transit
        of 'ITP'
            xch:Stock_Type = 'PHANTOM'
            xch:InTransit = 1 ! In Transit
        end ! case f:Available
        if (Access:EXCHANGE.TryUpdate() = Level:Benign)
            if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                exh:Ref_Number = xch:Ref_Number
                exh:Date = Today()
                exh:Time = Clock()
                Access:USERS.Clearkey(use:Password_Key)
                use:Password    = glo:Password
                if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                    ! Found
                else ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                    ! Error
                end ! if (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                exh:User = use:User_Code
                case f:Available
                of 'IT4'
                    exh:Status = 'IN TRANSIT TO 48HR STORES'
                of 'ITS'
                    exh:Status = 'IN TRANSIT TO SCRAPPING'
                of 'ITR'
                    exh:Status = 'IN TRANSIT TO RTM'
                of 'ITP'
                    exh:Status = 'PHANTOM IN TRANSIT'
                end ! case f:Available
                if Access:EXCHHIST.TryInsert() = Level:Benign
                    ! Inserted
                else ! if Access:EXCHHIST.TryInsert() = Level:Benign
                    ! Error
                    Access:EXCHHIST.CancelAutoInc()
                end ! if Access:EXCHHIST.TryInsert() = Level:Benign

            end ! if (Access:EXCHHIST.PrimeRecord() = Level:Benign)
        else ! if (Access:EXCHANGE.TryUpdate() = Level:Benign)
       end ! if (Access:EXCHANGE.TryUpdate() = Level:Benign)

    else ! if (Access:EXCHANGE.TryFetch(xch:AvailLocIMEI) = Level:Benign)
        ! Error
    end ! if (Access:EXCHANGE.TryFetch(xch:AvailLocIMEI) = Level:Benign)

!Local.ValidateParts     Procedure()
!Code
!      !OK, not do Parts/Network check
!      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!      man:Manufacturer    = job:Manufacturer
!      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!          !Found
!          If man:QAParts
!              If GetTempPathA(255,TempFilePath)
!                  If Sub(TempFilePath,-1,1) = '\'
!                      glo:FileName = Clip(TempFilePath) & ''
!                  Else !If Sub(TempFilePath,-1,1) = '\'
!                      glo:FileName = Clip(TempFilePath) & '\'
!                  End !If Sub(TempFilePath,-1,1) = '\'
!              End
!
!              glo:FileName = Clip(glo:FileName) & 'QAPARTS' & Clock() & '.TMP'
!
!              Remove(glo:FileName)
!              Access:QAPARTSTEMP.Open()
!              Access:QAPARTSTEMP.UseFile()
!              If job:Chargeable_Job = 'YES'
!                  Save_par_ID = Access:PARTS.SaveFile()
!                  Access:PARTS.ClearKey(par:Part_Number_Key)
!                  par:Ref_Number  = job:Ref_Number
!                  Set(par:Part_Number_Key,par:Part_Number_Key)
!                  Loop
!                      If Access:PARTS.NEXT()
!                         Break
!                      End !If
!                      If par:Ref_Number  <> job:Ref_Number      |
!                          Then Break.  ! End If
!                      If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign
!                          qap:PartNumber       = par:Part_Number
!                          qap:Description      = par:Description
!                          qap:PartRecordNumber = par:Record_Number
!                          qap:PartType         = 'CHA'
!                          If Access:QAPARTSTEMP.TryInsert() = Level:Benign
!                              !Insert Successful
!                          Else !If Access:QAPARTSTEMP.TryInsert() = Level:Benign
!                              !Insert Failed
!                          End !QAPIf Access:QAPARTSTEMP.TryInsert() = Level:Benign
!                      End !If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign
!
!                  End !Loop
!                  Access:PARTS.RestoreFile(Save_par_ID)
!
!              End !If job:Chargable_Job = 'YES'
!              If job:Warranty_Job = 'YES'
!                  Save_wpr_ID = Access:WARPARTS.SaveFile()
!                  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
!                  wpr:Ref_Number  = job:Ref_Number
!                  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!                  Loop
!                      If Access:WARPARTS.NEXT()
!                         Break
!                      End !If
!                      If wpr:Ref_Number  <> job:Ref_Number      |
!                          Then Break.  ! End If
!                      If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign
!                          qap:PartNumber       = wpr:Part_Number
!                          qap:Description      = wpr:Description
!                          qap:PartRecordNumber = wpr:Record_Number
!                          qap:PartType         = 'WAR'
!                          If Access:QAPARTSTEMP.TryInsert() = Level:Benign
!                              !Insert Successful
!                          Else !If Access:QAPARTSTEMP.TryInsert() = Level:Benign
!                              !Insert Failed
!                          End !QAPIf Access:QAPARTSTEMP.TryInsert() = Level:Benign
!                      End !If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign
!
!                  End !Loop
!                  Access:WARPARTS.RestoreFile(Save_wpr_ID)
!              End !If job:Chargable_Job = 'YES'
!
!              Case ValidateParts()
!                 Of 0
!                     Access:QAPARTSTEMP.Close()
!                     Remove(glo:FileName)
!
!                     Return 0
!                 Of 1 !Passed
!                     Access:QAPARTSTEMP.Close()
!                     Remove(glo:FileName)
!
!                     Return 1
!                 Of 2 !Failed
!                     Do FailParts
!                     Access:QAPARTSTEMP.Close()
!                     Remove(glo:FileName)
!
!                     Return 0
!
!              End !Case ValidateParts()
!          Else
!                Return 1
!          End !If man:QAParts
!      Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!          !Error
!          !Assert(0,'<13,10>Fetch Error<13,10>')
!          Return 1
!      End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!
Local.ValidateNetwork     Procedure()
Code
      !OK, not do Parts/Network check
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:QANetwork

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                SaveRequest#      = GlobalRequest
                GlobalResponse    = RequestCancelled
                GlobalRequest     = SelectRecord
                PickNetworks
                If Globalresponse = RequestCompleted
                    If jobe:Network = net:Network
                        Return 1
                    Else !If jobe:Network = net:Network

                        Beep(Beep:SystemHand)  ;  Yield()
                        Case message('Mismatch Network.'&|
                                '||This job will now fail QA.', |
                                'ServiceBase 2000', Icon:Hand, |
                                 Button:OK, Button:OK, 0)
                        Of Button:OK
                        End !CASE

                        reason" = ''
                        notes" = ''
                        QA_Failure_Reason(reason",notes")

                        !Added by Paul - 03/09/2009 - log no 10785
                        If clip(notes") <> '' then
                          Do AddEngNotes
                        End !If clip(notes") <> '' then
                        !End Addition

                        Case f_type
                            Of 'PRE'
                                locAction        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                            Of 'PRI'
                                locAction        = 'QA REJECTION MANUAL: ' & Clip(reason")
                        End !Case f_type
                        IF (AddToAudit(job:ref_number,'JOB',locAction,'NETWORK RECORDED: ' & Clip(jobe:Network) & |
                                                '<13,10,13,10>NETWORK VALIDATED: ' & Clip(net:Network) & '<13,10>' & clip(notes")))
                        END ! IF

                        glo:select1 = job:ref_number
                        If def:qa_failed_label = 'YES'
                            QA_Failed_Label
                        End!If def:qa_failed_label = 'YES'
                        If def:qa_failed_report = 'YES'
                            QA_Failed
                        End!If def:qa_failed_report = 'YES'
                        glo:select1 = ''
                        glo:notes_global    = ''
                        Case f_type
                            Of 'PRE'
                                GetStatus(625,1,'JOB')
                            Else!Case f_type
                                GetStatus(615,1,'JOB')
                        End!Case f_type
                        job:qa_rejected = 'YES'
                        job:date_qa_rejected    = Today()
                        job:time_qa_rejected    = Clock()
                        access:jobs.update()
                        Do passed_fail_update
                        if job:date_completed = ''
                            do Allocate_Prev_Eng
                        else
                            do CreateNewJob
                        end

                        Return 0

                    End !If jobe:Network <> net:Network
                Else
                    Return 0
                End
                GlobalRequest     = SaveRequest#

            Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        Else
            Return 1
        End !If man:QANetwork
    Else
        Return 1
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?update_text_temp{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?update_text_temp{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReAllocateEngineer PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CurrentEngineer  STRING(60)
tmp:NewEngineer      STRING(60)
tmp:NewUserCode      STRING(3)
tmp:CurrentSkillLevel LONG
tmp:NewSkillLevel    LONG
window               WINDOW('Re-Allocate Engineer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Re-Allocate Engineer'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT(''),AT(168,114),USE(?Prompt1)
                           PROMPT('Current Engineer'),AT(224,172),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(296,172),USE(tmp:CurrentEngineer),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Skill Level'),AT(296,184),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(340,184),USE(tmp:CurrentSkillLevel),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Skill Level'),AT(296,222),USE(?Prompt3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(340,222),USE(tmp:NewSkillLevel),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('New Engineer'),AT(224,206),USE(?tmp:NewEngineer:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(296,206,124,10),USE(tmp:NewEngineer),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('New Engineer'),TIP('New Engineer'),UPR,READONLY
                           BUTTON,AT(424,202),USE(?LookupEngineer),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('allocatp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:NewUserCode)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020475'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReAllocateEngineer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:USERS.Open
  SELF.FilesOpened = True
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      tmp:CurrentEngineer = Clip(use:Forename) & ' ' & Clip(use:Surname)
      tmp:CurrentSkillLevel   = use:SkillLevel
  Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','ReAllocateEngineer')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReAllocateEngineer')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020475'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020475'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020475'&'0')
      ***
    OF ?LookupEngineer
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Users_Job_Assignment
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tmp:NewUserCode = use:User_Code
          Of Requestcancelled
              tmp:NewUserCode = ''
      End!Case Globalreponse
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_code   = tmp:NewUserCode
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:NewEngineer = Clip(use:Forename) & ' ' & Clip(use:Surname)
          tmp:NewSkillLevel    = Clip(use:SkillLevel)
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
      
      Display()
    OF ?Cancel
      ThisWindow.Update
      tmp:NewUserCode = ''
      Post(Event:CloseWindow)
    OF ?Finish
      ThisWindow.Update
      If tmp:NewUserCode = ''
          Case Missive('You must select an engineer.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !tmp:NewUserCode = ''
          If tmp:NewEngineer = tmp:CurrentEngineer
              Case Missive('You must select a different engineer.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              !You must select a different engineer
          Else !If tmp:NewEngineer = tmp:CurrentEngineer
              Case Missive('Are you sure you want to re-allocate the tagged jobs?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Post(event:CloseWindow)
                  Of 1 ! No Button
              End ! Case Missive
      
          End !If tmp:NewEngineer = tmp:CurrentEngineer
      End !tmp:NewUserCode = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Update_Jobs_Status PROCEDURE                          !Generated from procedure template - Window

FilesOpened          BYTE
save_joe_id          USHORT,AUTO
Web_Temp_Job         STRING(20)
job_queue_temp       QUEUE,PRE(jobque)
Job_Number           STRING(20)
record_number        LONG
                     END
job_number_temp      REAL
wob_number_temp      LONG
engineer_temp        STRING(3)
engineer_name_temp   STRING(30)
Old_name_temp        STRING(30)
old_status_temp      STRING(30)
Location_temp        STRING(30)
update_text_temp     STRING(100)
tmp:status           STRING(30)
tmp:SkillLevel       LONG
tmp:ARCLocation      STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:status
acs:Status             LIKE(acs:Status)               !List box control field - type derived from field
acs:RecordNumber       LIKE(acs:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(ACCSTAT)
                       PROJECT(acs:Status)
                       PROJECT(acs:RecordNumber)
                     END
window               WINDOW('Rapid Status Update / Job Allocation'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Job Status Update / Job Allocation'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,224,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Defaults'),USE(?Tab1)
                           PROMPT('New Status'),AT(168,148),USE(?tmp:Status:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(260,148,124,10),USE(tmp:status),IMM,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L|M~Status~L(2)@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           PROMPT('Job Skill Level'),AT(168,168),USE(?tmp:SkillLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(260,168,64,10),USE(tmp:SkillLevel),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Engineer Skill Level'),TIP('Engineer Skill Level'),UPR,RANGE(1,10),STEP(1)
                           PROMPT('Engineer'),AT(168,188),USE(?tmp:SkillLevel:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(228,184),USE(?Lookup_Engineer),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(260,188,124,10),USE(engineer_name_temp),SKIP,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Location'),AT(168,208),USE(?Location_Temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(228,204),USE(?Lookup_Location),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(260,208,124,10),USE(Location_temp),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           LINE,AT(175,228,189,0),USE(?Line1),COLOR(COLOR:White)
                           PROMPT('Select Job Number'),AT(168,240),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s9),AT(260,240,48,12),USE(job_number_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           STRING(@s100),AT(168,288,216,12),USE(update_text_temp),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(392,84,124,244),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(396,144,116,108),USE(?List1),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('32L(2)|M~Job Number~@s20@'),FROM(job_queue_temp)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

show_old_status     Routine
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        old_status_temp = job:current_status
    Else
        old_status_temp = ''
    end

show_engineer       Routine
    access:users.clearkey(use:user_code_key)
    use:user_code = engineer_temp
    if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
    Else!if access:users.fetch(use:user_code_key) = Level:Benign
        engineer_name_temp = ''
    end!!if access:users.fetch(use:user_code_key) = Level:Benign
    Display(?engineer_name_temp)

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020483'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Jobs_Status')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  
  Relate:ACCSTAT.Open
  Relate:AUDIT.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:LOCINTER.UseFile
  Access:STAHEAD.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSENG.UseFile
  Access:LOCATLOG.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE2.UseFile
  Access:CONTHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If def:HideLocation Or GETINI('RAPIDSTATUS','HideLocation',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?Location_Temp{prop:Hide} = 1
      ?Location_Temp:Prompt{prop:Hide} = 1
      ?Lookup_Location{prop:Hide} = 1
  End !def:HideLocation
  If de2:UserSkillLevel
      ?tmp:SkillLevel{prop:Req} = 1
  End !de2:UserSkillLevel
  
  If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1 Or GETINI('RAPIDSTATUS','HideSkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:SkillLevel{prop:Hide} = 1
      ?tmp:SkillLevel:Prompt{prop:Hide} = 1
  End !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  If GETINI('RAPIDSTATUS','HideStatus',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:Status{prop:Hide} = 1
      ?tmp:Status:Prompt{prop:Hide} = 1
  End !If GETINI('RAPIDSTATUS','HideStatus',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  If GETINI('RAPIDSTATUS','SetStatus',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      tmp:Status = GETINI('RAPIDSTATUS','Status',,CLIP(PATH())&'\SB2KDEF.INI')
      ?tmp:Status{prop:Skip} = 1
  End !GETINI('RAPIDSTATUS','SetStatus',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      ! Save Window Name
   AddToLog('Window','Open','Update_Jobs_Status')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB2.Init(tmp:status,?tmp:status,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:ACCSTAT,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(acs:StatusKey)
  FDCB2.AddRange(acs:AccessArea,use:User_Level)
  FDCB2.AddField(acs:Status,FDCB2.Q.acs:Status)
  FDCB2.AddField(acs:RecordNumber,FDCB2.Q.acs:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCSTAT.Close
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Jobs_Status')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020483'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020483'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020483'&'0')
      ***
    OF ?Lookup_Engineer
      ThisWindow.Update
      GlobalRequest   = SelectRecord
      If ?tmp:SkillLevel{prop:Req} And ?tmp:SkillLEvel{prop:Hide} = 0
          If tmp:SKillLevel = 0
              Case Missive('You must select a skill level before you can select an engineer.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?tmp:SkillLevel)
          Else !If tmp:SKillLevel = 0
              PickEngineersSkillLevel(tmp:SkillLevel)
          End !If tmp:SKillLevel = 0
      
      Else !?tmp:SkillLevel{prop:Req}
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
      
          Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
          End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          glo:Select1 = use:Location
          PickLocationEngineers
          glo:Select1 = ''
      End !?tmp:SkillLevel{prop:Req}
      
      case globalresponse
          of requestcompleted
              engineer_temp = use:user_code
              select(?+2)
          of requestcancelled
              engineer_temp = ''
              select(?-1)
      end!case globalreponse
      Do show_engineer
    OF ?Lookup_Location
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Available_Locations
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
              location_temp = loi:location
              select(?+2)
          of requestcancelled
      !        location_temp = ''
              select(?-1)
      end!case globalreponse
      display(?location_temp)
    OF ?Location_temp
      ! Amend Location Levels
      
      fetch_error# = 0
      access:locinter.clearkey(loi:location_available_key)
      loi:location_available = 'YES'
      loi:location           = location_temp
      if access:locinter.fetch(loi:location_available_key)
      
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_available_locations
          if globalresponse = requestcompleted
              location_temp = loi:location
          Else!if globalresponse = requestcompleted
              location_temp = ''
          End!if globalresponse = requestcompleted
      
      End!if access:locinter.fetch(loi:location_available_key)
    OF ?job_number_temp
    if glo:Webjob = 0 then
        !TB12540 - Show contact history if there is a sticky note
        !need to find one that has not been cancelled, and is valid for this request
        Access:conthist.clearkey(cht:KeyRefSticky)
        cht:Ref_Number = job_number_temp
        cht:SN_StickyNote = 'Y'
        Set(cht:KeyRefSticky,cht:KeyRefSticky)      
        Loop
            if access:Conthist.next() then break.
            IF cht:Ref_Number <> job_number_temp then break.
            if cht:SN_StickyNote <> 'Y' then break.
            if cht:SN_Completed <> 'Y' and cht:SN_EngAlloc = 'Y' then
                glo:select12 = job_number_temp
                !Browse_Contact_History
                Browse_Contact_hist2
                BREAK
            END
        END
    END !if glo:Webjob


    update_Text_temp = ''
    !------------------------------------------------------------------
    Web_Temp_Job = job_number_temp
    !MESSAGE(glo:WebJob)
    if glo:WebJob then
        !Change the tmp ref number if you
        !can Look up from the wob file
        access:Webjob.clearkey(wob:RefNumberKey)
        wob:RefNumber = job_number_temp
        if access:Webjob.fetch(wob:refNumberKey)=level:benign then
            access:tradeacc.clearkey(tra:Account_Number_Key)
            tra:Account_Number = ClarioNET:Global.Param2
            access:tradeacc.fetch(tra:Account_Number_Key)
            Web_Temp_Job = job_number_temp & '-' & tra:BranchIdentification & wob:JobNumber
        END

    END
    !------------------------------------------------------------------
    IF tmp:status = '' And ?tmp:Status{prop:Hide} = 0
        Case Missive('You must select a New Status.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else!IF tmp:status = ''

        If Engineer_Temp = ''
            Case Missive('you must select an engineer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        Else !If Engineer_Temp = ''

            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = job_number_temp
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Found

                Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                    If Access:JOBSE.PrimeRecord() = Level:Benign
                        jobe:RefNumber  = job:Ref_Number
                        If Access:JOBSE.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:JOBSE.TryInsert() = Level:Benign
                            !Insert Failed
                        End!If Access:JOBSE.TryInsert() = Level:Benign
                    End !If Access:JOBSE.PrimeRecord() = Level:Benign
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                error# = 0
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    !If at the RRC, should only be able to allocate a job from the SAME RRC
                    !If at the ARC, should only be able to allocate if the job is AT the ARC - 3474 (DBH: 06-01-2004)
                    If glo:WebJob
                        If wob:HeadAccountNumber <> ClarioNET:Global.Param2
                            Case Missive('The selected job was booked by a different RRC.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            Cycle
                        End !If wob:HeadAccountNumber <> ClarioNET:Global.Param2
                    Else !If glo:WebJob
                        If jobe:HubRepair <> True
                            Case Missive('The selected job is not at the ARC.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            Cycle
                        End !If jobe:HubRepair <> True
                    End !If glo:WebJob
                Else !If Access:WEBJOB.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(jobe:RefNumberKey) = Level:Benign

                If job:workshop <> 'YES'
                    If engineer_temp <> ''
                        Case Missive('This job is not in the workshop. You cannot assign an engineer to it.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If engineer_temp <> ''
                    If location_temp <> '' And ?Location_Temp{prop:Hide} = 0
                        Case Missive('This job is not in the workshop. You cannot assign an location to it.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If location_temp <> ''

                End!If job:workshop <> 'YES'

                If error# = 0

                    If job:date_completed <> ''
                        If glo:WebJob And SentToHub(job:ref_Number)
                            Case Missive('Warning! The selected job has been completed. Are you sure you want to allocate an engineer to it?','ServiceBase 3g',|
                                           'mquest.jpg','\No|/Yes')
                                Of 2 ! Yes Button
                                Of 1 ! No Button
                                    Error# = 1
                            End ! Case Missive
                        Else !If glo:WebJob And SentToHub(job:ref_Number
                            Case Missive('Error! The selected job has been completed.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            error# = 1

                        End !If glo:WebJob And SentToHub(job:ref_Number
                    End!If job:date_completed <> ''
                End!If error# = 0

                if error# = 0
                     if engineer_temp <> ''
                         if job:engineer = engineer_temp
                            Case Missive('This job is already allocated to the selected engineer.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                             error# = 1
                         Else
                            If glo:WebJob
                                If jobe:HubRepair
                                    Case Missive('Cannot update. The selected job is not at the RRC.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    Error# = 1
                                End !If jobe:HubRepair
                            Else !If glo:WebJob
                                If ~jobe:HubRepair
                                    Case Missive('Cannot update. The selected job is not at the ARC.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK')
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    Error# = 1
                                Else !If ~jobe:HubRepair
                                    !Check the job is at the ARC Location - L921 (DBH: 13-08-2003)
                                    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                                    lot:RefNumber   = job:Ref_Number
                                    lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                                    If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                                        !Found
                                    Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                                        !Error
                                        Case Missive('Error! This job is not at the ARC Location. Please ensure that the Waybill Confirmation procedure has been completed.','ServiceBase 3g',|
                                                       'mstop.jpg','/OK')
                                            Of 1 ! OK Button
                                        End ! Case Missive
                                        Error# = 1
                                    End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                                End !If ~jobe:HubRepair
                            End !If glo:WebJob
                         end
                     end
                END! if error#=0

                If error# = 0
                    If tmp:Status <> ''
                        GetStatus(Sub(tmp:Status,1,3),1,'JOB')
                    End !If ?tmp:Status{prop:Hide} = 0

                    If job:workshop = 'YES'

                        If engineer_temp <> ''
                            !Check current engineer
                            If job:Engineer <> ''
                                !Lookup current engineer and mark as escalated
                                Save_joe_ID = Access:JOBSENG.SaveFile()
                                Access:JOBSENG.ClearKey(joe:UserCodeKey)
                                joe:JobNumber     = job:Ref_Number
                                joe:UserCode      = job:Engineer
                                joe:DateAllocated = Today()
                                Set(joe:UserCodeKey,joe:UserCodeKey)
                                Loop
                                    If Access:JOBSENG.PREVIOUS()
                                       Break
                                    End !If
                                    If joe:JobNumber     <> job:Ref_Number       |
                                    Or joe:UserCode      <> job:Engineer      |
                                    Or joe:DateAllocated > Today()       |
                                        Then Break.  ! End If
                                    If joe:Status = 'ALLOCATED'
                                        joe:Status = 'ESCALATED'
                                        joe:StatusDate = Today()
                                        joe:StatusTime = Clock()
                                        Access:JOBSENG.Update()
                                    End !If joe:Status = 'ALLOCATED'
                                    Break
                                End !Loop
                                Access:JOBSENG.RestoreFile(Save_joe_ID)
                            Else !If job:Engineer <> ''
                                ! Inserting (DBH 11/05/2006) #7597 - First allocation of engineer. Send SMS/Email Alert
                                If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                                    ! Do not send if ARC engineer being assign to RRC job (DBH: 11-05-2006)
                                    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                                    jobe2:RefNumber = job:Ref_Number
                                    If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                        ! Found
                                        If jobe2:SMSNotification
                                            AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','SMS',jobe2:SMSAlertNumber,'',0,'')
                                        End ! If jobe2:SMSNotification

                                        If jobe2:EmailNotification
                                            AddEmailSMS(job:Ref_Number,wob:HeadAccountNumber,'2ENG','EMAIL','',jobe2:EmailAlertAddress,0,'')
                                        End ! If jobe2:EmailNotification
                                    Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                        ! Error
                                    End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                End ! If glo:WebJob = 1 Or (glo:WebJob = 0 And jobe:WebJob = 0)
                                ! End (DBH 11/05/2006) #7597
                            End !If job:Engineer <> ''
                            job:engineer       = engineer_temp
                            ! Inserting (DBH 16/12/2005) #6633 - Record and entry in the Audit Trail for the engineer allocation

                            IF (AddToAudit(job:Ref_Number,'JOB','ENGINEER ALLOCATED: ' & Clip(use:User_Code),Clip(use:Forename) & ' ' & Clip(use:Surname) & ' - ' & Clip(use:Location) & ' - LEVEL ' & Clip(use:SkillLevel)))
                            END ! IF


                            ! End (DBH 16/12/2005) #6633
                        End!If engineer_temp <> ''

                        If ?tmp:SkillLevel{prop:Hide} = 0
                            jobe:SkillLevel = tmp:SkillLevel
                        End !If ?tmp:SkillLevel{prop:Hide} = 0

                        Access:JOBSE.Update()

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel
                            If job:Date_Completed <> '' And SentToHub(job:Ref_Number) And glo:WebJob
                                joe:Status  = 'ENGINEER QA'
                            Else !If job:Date_Completed <> '' And SentToHub(job:Ref_Number) And glo:WebJob
                                joe:Status = 'ALLOCATED'
                            End !If job:Date_Completed <> '' And SentToHub(job:Ref_Number) And glo:WebJob
                            joe:StatusDate  = Today()
                            joe:StatusTime  = Clock()

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                        If location_temp <> '' And ?Location_Temp{prop:Hide} = 0

                        !Add To Old Location
                            access:locinter.clearkey(loi:location_key)
                            loi:location = job:location
                            If access:locinter.fetch(loi:location_key) = Level:Benign
                                If loi:allocate_spaces = 'YES'
                                    loi:current_spaces+= 1
                                    loi:location_available = 'YES'
                                    access:locinter.update()
                                End
                            end !if
                        !Take From New Location
                            !Write To Log
                            If Access:LOCATLOG.PrimeRecord() = Level:Benign
                                lot:RefNumber   = job:Ref_Number
                                Access:USERS.Clearkey(use:Password_Key)
                                use:Password    = glo:Password
                                If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                    !Found
                                    lot:UserCode    = use:User_code
                                Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign

                                lot:PreviousLocation    = job:Location
                                lot:NewLocation         = Location_Temp
                                If Access:LOCATLOG.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:LOCATLOG.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:LOCATLOG.TryInsert() = Level:Benign
                            End !If Access:LOCATLOG.PrimeRecord() = Level:Benign
                            job:location = location_temp

                            access:locinter.clearkey(loi:location_key)
                            loi:location = job:location
                            If access:locinter.fetch(loi:location_key) = Level:Benign
                                If loi:allocate_spaces = 'YES'
                                    loi:current_spaces -= 1
                                    If loi:current_spaces< 1
                                        loi:current_spaces = 0
                                        loi:location_available = 'NO'
                                    End
                                    access:locinter.update()
                                End
                            end !if

                        End!If location_temp <> ''
                    End!If job:workshop = 'YES'
                    access:jobs.update()
                    jobque:job_number = Web_Temp_Job
                    jobque:record_number += 1
                    Add(job_queue_temp)
                    Sort(job_queue_temp,-jobque:record_number)

                    update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' Updated Successfully'
                    ?update_text_temp{prop:fontcolor} = color:navy
                    job_number_temp = ''
                    old_status_temp = ''
                    Display()
                    beep(beep:systemasterisk)
                End!If error# = 0
                Select(?job_number_temp)
            Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
                Case Missive('Unable to find selected job.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                old_status_temp = ''
                Select(?job_number_temp)
                update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' Update Failed'
                ?update_text_temp{prop:fontcolor} = color:red
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        End !If Engineer_Temp = ''
    End!IF tmp:status = ''
    Display()
    OF ?Finish
      ThisWindow.Update
      If Records(Job_Queue_Temp)
          Print# = 0
          If GETINI('RAPIDSTATUS','AutoPrint',,CLIP(PATH())&'\SB2KDEF.INI') = 1
              Print# = 1
          Else !If GETINI('RAPIDSTOCK','AutoPrint',,CLIP(PATH())&'\SB2KDEF.INI') = 1
              Case Missive('Do you wish to print a report of all jobs updated in this batch?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      Print# = 1
                  Of 1 ! No Button
              End ! Case Missive
      
          End !If GETINI('RAPIDSTOCK','AutoPrint',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          If Print# = 1
              Free(glo:Queue)
              Clear(glo:Queue)
              Loop x# = 1 To Records(job_queue_temp)
                  Get(job_queue_temp,x#)
                  glo:pointer  = jobque:Job_Number
                  Add(glo:Queue)
              End!Loop x# = 1 To Records(job_queue_temp)
      
              If GETINI('RAPIDSTATUS','ReportType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                  RapidStatusAllocationsReport(engineer_temp,GETINI('RAPIDSTATUS','Copies',,CLIP(PATH())&'\SB2KDEF.INI'),'RS')
              Else !If GETINI('RAPIDSTOCK','ReportType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                  Rapid_Status_Update_Report(GETINI('RAPIDSTATUS','Copies',,CLIP(PATH())&'\SB2KDEF.INI'))
              End !If GETINI('RAPIDSTOCK','ReportType',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
      
              Free(glo:Queue)
          End !If Print# = 1
      
      End !Records(Job_Queue_Temp)
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateJobExchangeStatus PROCEDURE                     !Generated from procedure template - Window

Web_Temp_Job         STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:status           STRING(30)
tmp:jobnumber        LONG
tmp:wob_number       LONG
JobNumberQueue       QUEUE,PRE(QUE)
JobNumber            STRING(20)
RecordNumber         LONG
                     END
Update_Text_Temp     STRING(100)
Proceed              BYTE
Change_Charges       BYTE
tmp:Charge_Type      STRING(30)
tmp:Warranty_Type    STRING(30)
window               WINDOW('Rapid Status Update'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Exchange Status Update'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,248,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Exchange Status'),AT(168,154),USE(?tmp:status:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(256,154,124,10),USE(tmp:status),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Status'),TIP('Exchange Status'),UPR
                           BUTTON,AT(384,150),USE(?LookupExchangeStatus),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           STRING('Change Charge Types'),AT(168,174),USE(?String2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK,AT(256,174),USE(Change_Charges),VALUE('1','0')
                           PROMPT('Charge Type'),AT(168,194),USE(?tmp:Charge_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(256,194,124,10),USE(tmp:Charge_Type),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(384,190),USE(?CallLookup),SKIP,DISABLE,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Warranty Charge Type'),AT(168,214),USE(?tmp:Warranty_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(256,214,124,10),USE(tmp:Warranty_Type),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(384,210),USE(?CallLookup:2),SKIP,DISABLE,TRN,FLAT,ICON('lookupp.jpg')
                           LINE,AT(166,234,215,0),USE(?Line1),COLOR(COLOR:White)
                           PROMPT('Job Number'),AT(168,246),USE(?tmp:jobnumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(256,246,64,10),USE(tmp:jobnumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                           STRING(@s100),AT(168,290,240,12),USE(Update_Text_Temp),FONT('Tahoma',10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(416,82,100,248),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Jobs Updated'),USE(?Tab2)
                           LIST,AT(420,150,92,92),USE(?List1),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('32L(2)|M~Job Number~@s20@'),FROM(JobNumberQueue)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:status                Like(tmp:status)
look:tmp:Charge_Type                Like(tmp:Charge_Type)
look:tmp:Warranty_Type                Like(tmp:Warranty_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020482'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJobExchangeStatus')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:STAHEAD.Open
  Relate:WEBJOB.Open
  Access:STATUS.UseFile
  Access:JOBS.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBSTAGE.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateJobExchangeStatus')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:status{Prop:Tip} AND ~?LookupExchangeStatus{Prop:Tip}
     ?LookupExchangeStatus{Prop:Tip} = 'Select ' & ?tmp:status{Prop:Tip}
  END
  IF ?tmp:status{Prop:Msg} AND ~?LookupExchangeStatus{Prop:Msg}
     ?LookupExchangeStatus{Prop:Msg} = 'Select ' & ?tmp:status{Prop:Msg}
  END
  IF ?tmp:Charge_Type{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Charge_Type{Prop:Tip}
  END
  IF ?tmp:Charge_Type{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Charge_Type{Prop:Msg}
  END
  IF ?tmp:Warranty_Type{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Warranty_Type{Prop:Tip}
  END
  IF ?tmp:Warranty_Type{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Warranty_Type{Prop:Msg}
  END
  IF ?Change_Charges{Prop:Checked} = True
    ENABLE(?tmp:Charge_Type)
    ENABLE(?tmp:Warranty_Type)
    ENABLE(?CallLookup)
    ENABLE(?CallLookup:2)
  END
  IF ?Change_Charges{Prop:Checked} = False
    DISABLE(?tmp:Charge_Type)
    DISABLE(?tmp:Warranty_Type)
    DISABLE(?CallLookup)
    DISABLE(?CallLookup:2)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:STAHEAD.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateJobExchangeStatus')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      SelectStatus
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020482'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020482'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020482'&'0')
      ***
    OF ?tmp:status
      IF tmp:status OR ?tmp:status{Prop:Req}
        sts:Status = tmp:status
        sts:Exchange = 'YES'
        GLO:Select1 = 'EXC'
        !Save Lookup Field Incase Of error
        look:tmp:status        = tmp:status
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:status = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:status = look:tmp:status
            SELECT(?tmp:status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExchangeStatus
      ThisWindow.Update
      sts:Status = tmp:status
      sts:Exchange = 'YES'
      GLO:Select1 = 'EXC'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:status = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:status)
    OF ?Change_Charges
      IF ?Change_Charges{Prop:Checked} = True
        ENABLE(?tmp:Charge_Type)
        ENABLE(?tmp:Warranty_Type)
        ENABLE(?CallLookup)
        ENABLE(?CallLookup:2)
      END
      IF ?Change_Charges{Prop:Checked} = False
        DISABLE(?tmp:Charge_Type)
        DISABLE(?tmp:Warranty_Type)
        DISABLE(?CallLookup)
        DISABLE(?CallLookup:2)
      END
      ThisWindow.Reset
    OF ?tmp:Charge_Type
      IF tmp:Charge_Type OR ?tmp:Charge_Type{Prop:Req}
        cha:Charge_Type = tmp:Charge_Type
        GLO:Select1 = 'NO'
        cha:Warranty = 'NO'
        !Save Lookup Field Incase Of error
        look:tmp:Charge_Type        = tmp:Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(GLO:Select1)
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tmp:Charge_Type = look:tmp:Charge_Type
            SELECT(?tmp:Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      cha:Charge_Type = tmp:Charge_Type
      GLO:Select1 = 'NO'
      cha:Warranty = 'NO'
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tmp:Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Charge_Type)
    OF ?tmp:Warranty_Type
      IF tmp:Warranty_Type OR ?tmp:Warranty_Type{Prop:Req}
        cha:Charge_Type = tmp:Warranty_Type
        GLO:Select1 = 'YES'
        cha:Warranty = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:Warranty_Type        = tmp:Warranty_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:Warranty_Type = cha:Charge_Type
          ELSE
            CLEAR(GLO:Select1)
            CLEAR(cha:Warranty)
            !Restore Lookup On Error
            tmp:Warranty_Type = look:tmp:Warranty_Type
            SELECT(?tmp:Warranty_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      cha:Charge_Type = tmp:Warranty_Type
      GLO:Select1 = 'YES'
      cha:Warranty = 'YES'
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:Warranty_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?tmp:Warranty_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Warranty_Type)
    OF ?tmp:jobnumber
      update_Text_temp = ''
      !------------------------------------------------------------------
      ! Webify tmp:Ref_Number
      !
      Web_Temp_Job = tmp:JobNumber
      if glo:WebJob then
          !Change the tmp ref number if you
          !can Look up from the wob file
          access:Webjob.clearkey(wob:RefNumberKey)
          wob:RefNumber = tmp:JobNumber
          if access:Webjob.fetch(wob:refNumberKey)=level:benign then
              access:tradeacc.clearkey(tra:Account_Number_Key)
              tra:Account_Number = ClarioNET:Global.Param2
              access:tradeacc.fetch(tra:Account_Number_Key)
              Web_Temp_Job = tmp:JobNumber & '-' & tra:BranchIdentification & wob:JobNumber
          END
          !Set up no preview on client
          !ClarioNET:UseReportPreview(0)
      END
      !------------------------------------------------------------------
      IF tmp:status = ''
          Case Missive('You must select a New Status.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!IF tmp:status = ''
          Proceed = TRUE
          !Added By Neil 21/08/01
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number = tmp:JobNumber
          if access:jobs.fetch(job:ref_number_key) = Level:Benign
            IF job:Exchange_Unit_Number <> 0
              Case Missive('An exchange unit has been allocated to this job. Do you wish to continue to change the status?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                  Proceed = TRUE
                  Of 1 ! No Button
                  Proceed = FALSE
              End ! Case Missive
            END
          END
          IF Proceed = TRUE
            !Added By Neil 15/08/01!
            SORT(JobNumberQueue,-QUE:JobNumber)
            JobNumberQueue.QUE:JobNumber = tmp:JobNumber
            GET(JobNumberQueue,JobNumberQueue.QUE:JobNumber)
            IF ERROR()
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number = tmp:JobNumber
              if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  error# = 0
      
                  If error# = 0
      
                      IF (AddToAudit(job:ref_number,'EXC','RAPID EXCHANGE STATUS UPDATE: ' & Clip(tmp:status),'PREVIOUS STATUS: ' & Clip(job:exchange_status) & '<13,10>NEW STATUS: ' & Clip(tmp:status)))
                      END ! IF
      
                      GetStatus(Sub(tmp:Status,1,3),1,'EXC')
      
                      IF Change_Charges = TRUE
                        error1# = 0
                        If tmp:Charge_Type <> job:Charge_Type
                            If tmp:Charge_Type <> ''
                                  Case Missive('Are you sure you want to change the Chargeable charge type?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                      Of 1 ! No Button
                                        error1# = 1
                                  End ! Case Missive
                            End!If tmp:chargetype <> ''
                            If error1# = 0
                                If CheckPricing('C')
                                    error1# = 1
                                End!If CheckPricing('C')
                            End!If error# = 0
      
                            If error1# = 0
                                job:Charge_Type = tmp:Charge_Type
                            Else!If error# = 0
                                tmp:Charge_Type  = job:Charge_Type
                            End!If error# = 0
      
                        End!If tmp:chargetype   <> job:Charge_Type
                        Display()
      
                        error2# = 0
                        If tmp:Warranty_Type   <> job:warranty_Charge_Type
                            If tmp:Warranty_Type <> ''
                                  Case Missive('Are you sure you want to change the Warranty charge type?','ServiceBase 3g',|
                                                 'mquest.jpg','\No|/Yes')
                                      Of 2 ! Yes Button
                                      Of 1 ! No Button
                                        error2# = 1
                                  End ! Case Missive
                            End!If tmp:chargetype <> ''
                            If error2# = 0
                                If CheckPricing('W')
                                    error2# = 1
                                End!If CheckPricing('C')
                            End!If error# = 0
      
                            If error2# = 0
                                job:Warranty_Charge_Type = tmp:Warranty_Type
                            Else!If error# = 0
                                tmp:Warranty_Type  = job:Warranty_Charge_Type
                            End!If error# = 0
      
                        End!If tmp:chargetype   <> job:Charge_Type
                      END
                      Display()
                      access:jobs.update()
                      que:jobnumber = Web_Temp_Job
                      que:recordnumber += 1
                      Add(JobNumberQueue)
                      Sort(JobNumberQueue,-que:recordnumber)
      
                      update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' Updated Successfully'
                      ?update_text_temp{prop:fontcolor} = color:navy
                      tmp:JobNumber = ''
                      Display()
                      beep(beep:systemasterisk)
                    End!If error# = 0
                  Select(?tmp:JobNumber)
              Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  Case Missive('Unable to find the selected job.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Select(?tmp:JobNumber)
                  update_text_temp = 'Job Number: ' & Clip(Web_Temp_Job) & ' Update Failed'
                  ?update_text_temp{prop:fontcolor} = color:red
              end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
            ELSE
              !Error, already done this one!
              Case Missive('This job has already been processed. An Exchange Status has already been allocated during this session.','ServiceBase 3g',|
                             'mexclam.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
            END
          END
      End!IF tmp:status = ''
      Display()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
dummy PROCEDURE                                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('dummy')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPGEN.Open
  Relate:QAPARTSTEMP.Open
  Relate:RAPENGLS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','dummy')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPGEN.Close
    Relate:QAPARTSTEMP.Close
    Relate:RAPENGLS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','dummy')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
