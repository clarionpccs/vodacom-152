

   MEMBER('sbj03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ03005.INC'),ONCE        !Local module procedure declarations
                     END


UpdateOBFRejectionReasons PROCEDURE                   !Generated from procedure template - Window

ActionMessage        CSTRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Description'),AT(200,144),USE(?obf:Description:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s60),AT(264,144,196,10),USE(obf:Description),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Description'),TIP('Description'),REQ,UPR
                       PROMPT('Rejection Text'),AT(200,170),USE(?obf:RejectionText:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(264,168,196,83),USE(obf:RejectionText),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Rejection Text'),TIP('Rejection Text'),REQ,UPR
                       CHECK('Active'),AT(264,264),USE(obf:Active),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Active'),TIP('Active'),VALUE('1','0')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend OBF Rejection Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(372,332),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020681'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOBFRejectionReasons')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:OBFREASN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:OBFREASN.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:OBFREASN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateOBFRejectionReasons')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:OBFREASN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateOBFRejectionReasons')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020681'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020681'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020681'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
SelectOBFRejectionReason PROCEDURE                    !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:Tag              STRING(1)
tmp:Active           BYTE(1)
BRW8::View:Browse    VIEW(OBFREASN)
                       PROJECT(obf:Description)
                       PROJECT(obf:RejectionText)
                       PROJECT(obf:RecordNumber)
                       PROJECT(obf:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
obf:Description        LIKE(obf:Description)          !List box control field - type derived from field
obf:RejectionText      LIKE(obf:RejectionText)        !List box control field - type derived from field
obf:RecordNumber       LIKE(obf:RecordNumber)         !Primary key field - type derived from field
obf:Active             LIKE(obf:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Tag Text To Add To Rejection Reason'),AT(168,86),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s60),AT(168,100,124,10),USE(obf:Description),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Description')
                       LIST,AT(168,112,152,212),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@240L(2)|M~Description~@s60@0L(2)|M~Rejection Text~@s255@'),FROM(Queue:Browse)
                       TEXT,AT(328,112,184,76),USE(obf:RejectionText),SKIP,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY,MSG('Rejection Text')
                       BUTTON('&Rev tags'),AT(199,217,50,13),USE(?DASREVTAG)
                       BUTTON('sho&W tags'),AT(203,247,70,13),USE(?DASSHOWTAG)
                       BUTTON,AT(324,244),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(324,272),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(324,300),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select OBF Rejection Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(375,332),USE(?Close),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = obf:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = obf:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = obf:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::9:QUEUE = glo:Queue
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer = obf:RecordNumber
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = obf:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020680'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectOBFRejectionReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:OBFREASN.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:OBFREASN,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','SelectOBFRejectionReason')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,obf:ActiveDescriptionKey)
  BRW8.AddRange(obf:Active,tmp:Active)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?obf:Description,obf:Description,1,BRW8)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(tmp:Tag,BRW8.Q.tmp:Tag)
  BRW8.AddField(obf:Description,BRW8.Q.obf:Description)
  BRW8.AddField(obf:RejectionText,BRW8.Q.obf:RejectionText)
  BRW8.AddField(obf:RecordNumber,BRW8.Q.obf:RecordNumber)
  BRW8.AddField(obf:Active,BRW8.Q.obf:Active)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:OBFREASN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SelectOBFRejectionReason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      Free(glo:Queue)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020680'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020680'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020680'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DASTAG)
      End ! If KeyCode() = MouseLeft2
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = obf:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = obf:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

InsertOBFRejectionReason PROCEDURE (f:RejectionReason) !Generated from procedure template - Window

tmp:RejectionReason  STRING(255)
tmp:Return           BYTE(0)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Rejection Reason'),AT(228,162),USE(?tmp:RejectionReason:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(230,175,213,79),USE(tmp:RejectionReason),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Rejection Reason'),TIP('Rejection Reason'),REQ,UPR
                       BUTTON,AT(448,174),USE(?Button2),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Rejection Reason'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(372,332),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020678'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertOBFRejectionReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:OBFREASN.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','InsertOBFRejectionReason')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:OBFREASN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','InsertOBFRejectionReason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      f:RejectionReason = ''
      tmp:Return = 0
    OF ?OK
      If Clip(tmp:RejectionReason) = ''
          Select(?tmp:RejectionReason)
          Cycle
      End ! If Clip(tmp:RejectionReason) = ''
      f:RejectionReason = Upper(tmp:RejectionReason)
      tmp:Return = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      SelectOBFRejectionReason
      ThisWindow.Reset
      If Records(glo:Queue) > 0
          Loop x# = 1 To Records(Glo:Queue)
              Get(glo:Queue,x#)
              Access:OBFREASN.ClearKey(obf:RecordNumberKey)
              obf:RecordNumber = glo:Pointer
              If Access:OBFREASN.TryFetch(obf:RecordNumberKey) = Level:Benign
                  !Found
                  tmp:RejectionReason = Clip(tmp:RejectionReason) & '. ' & Clip(obf:RejectionText)
              Else ! If Access:OBFREASN.TryFetch(obf:RecordNumberKey) = Level:Benign
                  !Error
              End ! If Access:OBFREASN.TryFetch(obf:RecordNumberKey) = Level:Benign
      
          End ! Loop x# = 1 To Records(Glo:Queue)
      End ! If Records(glo:Queue) > 0
      If Sub(tmp:RejectionReason,1,2) = '. '
          tmp:RejectionReason = Sub(tmp:RejectionReason,3,255)
      End ! If Sub(tmp:RejectionReason,1,2) = '. '
      Display()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020678'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020678'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020678'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
StockImport PROCEDURE                                 !Generated from procedure template - Window

tmp:ImportFile       STRING(255),STATIC
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Import Stock'),AT(301,136),USE(?Prompt4),FONT(,12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('You must ensure than any Locations, Manufacturers, Model Numbers, Shelf Location' &|
   's and Suppliers included in the import are setup correctly within ServiceBase.'),AT(199,154,277,26),USE(?Prompt5),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('(Note: Parts are only imported once.)'),AT(267,184),USE(?Prompt6),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('File To Import'),AT(180,224),USE(?tmp:ImportFile:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(276,222,200,10),USE(tmp:ImportFile),LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('File To Import'),TIP('File To Import'),UPR,SINGLE
                       BUTTON,AT(480,218),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Stock Import Routine'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(372,332),USE(?Button3),TRN,FLAT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup9          SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
PartNumber          String(30)
Description         String(30)
Location            String(30)
ShelfLocation       String(30)
Supplier            String(30)
PurchaseCost        Real
Accessory           Byte
AccessoryCost       Real
AccWarrantyPeriod   String(30)
SundryItem          Byte
SuspendPart         Byte
ExchangeUnit        Byte
AllowDuplicatePart  Byte
ReturnFaultySpare   Byte
ChargeablePartOnly  Byte
AttachBySolder      Byte
Lev1                Byte
Lev2                Byte
Lev3                Byte
RepairIndex         Long
SkillLevel          Long
Manufacturer        String(30)
ModelNumber         String(30)
                    End
                End
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico')
     END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020682'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StockImport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:LOCATION.Open
  Relate:USERS_ALIAS.Open
  Access:STOCK.UseFile
  Access:MANMARK.UseFile
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','StockImport')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('CSV Files','*.csv')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:LOCATION.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','StockImport')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      ThisWindow.Update
      tmp:ImportFile = Upper(FileLookup9.Ask(1)  )
      DISPLAY
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020682'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020682'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020682'&'0')
      ***
    OF ?Button3
      ThisWindow.Update
      If Clip(tmp:ImportFile) = ''
          Cycle
      End ! If Clip(tmp:ImportFile) = ''
      
      If SecurityCheck('STOCK IMPORT')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If SecurityCheck('STOCK IMPORT')
      
      Case Missive('Are you sure you want to import the selected file?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Count# = 0
              Open(ImportFile)
              If Error()
                  Stop(Error())
              End ! If Error()
              SetCursor(Cursor:Wait)
              Set(ImportFile,0)
              Loop
                  Next(ImportFile)
                  If Error()
                      Break
                  End ! If Error()
                  Count# += 1
              End ! Loop
              SetCursor()
      
              Do Prog:ProgressSetup
              Prog:TotalRecords = Count#
              Prog:ShowPercentage = 1 !Show Percentage Figure
      
              Set(ImportFile,0)
      
              Accept
                  Case Event()
                      Of Event:Timer
                          Loop 25 Times
                              !Inside Loop
                              Next(ImportFile)
                              If Error()
                                  Prog:Exit = 1
                                  Break
                              End ! If Access:ORDERS.Next()
                              ! Ignore Title Bar
                              If Clip(Upper(impfil:PartNumber)) = 'PART NUMBER'
                                  Cycle
                              End ! If Clip(Upper(impfil:PartNumber)) = 'PART NUMBER'
      
                              Access:LOCATION.ClearKey(loc:Location_Key)
                              loc:Location = Clip(Upper(impfil:Location))
                              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                                  !Found
                              Else ! If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                                  !Error
                              End ! If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
      
      
                              Access:STOCK.ClearKey(sto:Location_Key)
                              sto:Location = Clip(Upper(impfil:Location))
                              sto:Part_Number = Clip(Upper(impfil:PartNumber))
                              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                  !Found
                                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                                  stm:Ref_Number   = sto:Ref_Number
                                  stm:Manufacturer = sto:Manufacturer
                                  stm:Model_Number = Clip(Upper(impfil:ModelNumber))
                                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key)
                                      If Access:STOMODEL.PrimeRecord() = Level:Benign
                                          stm:Manufacturer = sto:Manufacturer
                                          stm:Model_Number = Clip(Upper(impfil:ModelNumber))
                                          stm:Part_Number = sto:Part_Number
                                          stm:Description = sto:Description
                                          stm:Location = sto:Location
                                          stm:Accessory = sto:Accessory
                                          stm:Ref_Number = sto:Ref_Number
                                          If Access:STOMODEL.TryInsert() = Level:Benign
                                              !Insert
                                          Else ! If Access:STOMODEL.TryInsert() = Level:Benign
                                              Access:STOMODEL.CancelAutoInc()
                                          End ! If Access:STOMODEL.TryInsert() = Level:Benign
                                      End ! If Access.STOMODEL.PrimeRecord() = Level:Benign
                                  End ! If Access:STOMODEL.TryFetch(stm:Model_Number_Key)
                              Else ! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                  !Error
                                  If Access:STOCK.PrimeRecord() = Level:Benign
                                      sto:Part_Number = Clip(Upper(impfil:PartNumber))
                                      sto:Description = Clip(Upper(impfil:Description))
                                      sto:Location = Clip(Upper(impfil:Location))
                                      sto:Shelf_Location = Clip(Upper(impfil:ShelfLocation))
                                      sto:Supplier     = Clip(upper(impfil:Supplier))
      
                                      If loc:Main_Store = 'YES'
                                          sto:Purchase_Cost = Round(impfil:PurchaseCost,.01)
                                          sto:Sale_Cost = Markups(sto:Sale_Cost,sto:Purchase_Cost,sto:Percentage_Mark_up)
                                      Else ! If loc:Main_Store = 'YES'
                                          sto:AveragePurchaseCost = Round(impfil:PurchaseCost,.01)
                                          sto:Percentage_Mark_Up = loc:OutWarrantyMarkup
                                          sto:Sale_Cost = Markups(sto:Sale_Cost,sto:AveragePurchaseCost,loc:OutWarrantyMarkup)
      
                                          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                                          man:Manufacturer = Clip(Upper(impfil:Manufacturer))
                                          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                              !Found
                                              Access:MANMARK.ClearKey(mak:SiteLocationKey)
                                              mak:RefNumber = man:RecordNumber
                                              mak:SiteLocation = loc:Location
                                              If Access:MANMARK.TryFetch(mak:SiteLocationKey) = Level:Benign
                                                  !Found
                                                  sto:PurchaseMarkup = mak:InWarrantyMarkup
                                                  sto:Purchase_Cost = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,mak:InWarrantyMarkup)
                                              Else ! If Access:MANMARK.TryFetch(mak:SiteLocationKey) = Level:Benign
                                                  !Error
                                              End ! If Access:MANMARK.TryFetch(mak:SiteLocationKey) = Level:Benign
      
                                          Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                              !Error
                                          End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      
                                      End ! If loc:Main_Store = 'YES'
      
                                      If Clip(Upper(impfil:Accessory)) = 1
                                          sto:Accessory = 'YES'
                                      Else ! If Clip(Upper(impfil:Accessory)) = 1
                                          sto:Accessory = 'NO'
                                      End ! If Clip(Upper(impfil:Accessory)) = 1
                                      sto:AccessoryCost = Round(impfil:AccessoryCost,.01)
                                      sto:AccWarrantyPeriod = Clip(Upper(impfil:AccWarrantyPeriod))
                                      If Clip(Upper(impfil:SundryItem)) = 1
                                          sto:Sundry_Item = 'YES'
                                      Else ! If Clip(Upper(impfil:Sundry)) = 1
                                          sto:Sundry_Item = 'NO'
                                      End ! If Clip(Upper(impfil:Sundry)) = 1
                                      sto:Suspend = Clip(Upper(impfil:SuspendPart))
                                      If Clip(Upper(impfil:ExchangeUnit)) = 1
                                          sto:ExchangeUnit = 'YES'
                                      Else ! If Clip(Upper(impfil:ExchangeUnit)) = 1
                                          sto:ExchangeUnit = 'NO'
                                      End ! If Clip(Upper(impfil:ExchangeUnit)) = 1
                                      sto:AllowDuplicate = Clip(Upper(impfil:AllowDuplicatePart))
                                      sto:ReturnFaultySpare = Clip(Upper(impfil:ReturnFaultySpare))
                                      sto:ChargeablePartOnly = Clip(Upper(impfil:ChargeablePartOnly))
                                      sto:AllowDuplicate = Clip(Upper(impfil:AttachBySolder))
                                      sto:E1 = Clip(Upper(impfil:Lev1))
                                      sto:E2 = Clip(Upper(impfil:Lev2))
                                      sto:E3 = Clip(Upper(impfil:Lev3))
                                      sto:RepairLevel = Clip(Upper(impfil:RepairIndex))
                                      sto:SkillLevel = Clip(Upper(impfil:SkillLevel))
                                      sto:Manufacturer = Clip(Upper(impfil:Manufacturer))
                                      If Access:STOCK.TryInsert() = Level:Benign
                                          !Insert
                                          If Access:STOMODEL.PrimeRecord() = Level:Benign
                                              stm:Manufacturer = sto:Manufacturer
                                              stm:Model_Number = Clip(Upper(impfil:ModelNumber))
                                              stm:Part_Number = sto:Part_Number
                                              stm:Description = sto:Description
                                              stm:Location = sto:Location
                                              stm:Accessory = sto:Accessory
                                              stm:Ref_Number = sto:Ref_Number
                                              If Access:STOMODEL.TryInsert() = Level:Benign
                                                  !Insert
                                              Else ! If Access:STOMODEL.TryInsert() = Level:Benign
                                                  Access:STOMODEL.CancelAutoInc()
                                              End ! If Access:STOMODEL.TryInsert() = Level:Benign
                                          End ! If Access.STOMODEL.PrimeRecord() = Level:Benign
                                          If Access:STOHIST.PrimeRecord() = Level:Benign
                                              shi:Ref_Number = sto:Ref_Number
                                              shi:User = ''
                                              shi:Transaction_Type = 'ADD'
                                              shi:Job_Number = 0
                                              shi:Sales_Number = 0
                                              shi:Quantity = 0
                                              shi:Date = Today()
                                              shi:Purchase_Cost = sto:Purchase_Cost
                                              shi:Sale_Cost = sto:Sale_Cost
                                              shi:Retail_Cost = sto:Retail_Cost
                                              shi:Notes = 'NEW STOCK ITEM CREDITED'
                                              shi:Information = 'FROM IMPORT FILE: ' & Clip(Upper(tmp:ImportFile))
                                              If Access:STOHIST.TryInsert() = Level:Benign
                                                  !Insert
                                              Else ! If Access:STOHIST.TryInsert() = Level:Benign
                                                  Access:STOHIST.CancelAutoInc()
                                              End ! If Access:STOHIST.TryInsert() = Level:Benign
                                          End ! If Access.STOHIST.PrimeRecord() = Level:Benign
                                      Else ! If Access:STOCK.TryInsert() = Level:Benign
                                          Access:STOCK.CancelAutoInc()
                                      End ! If Access:STOCK.TryInsert() = Level:Benign
                                  End ! If Access.STOCK.PrimeRecord() = Level:Benign
                              End ! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      
      
                              Prog:RecordCount += 1
      
                              ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                              Do Prog:UpdateScreen
                          End ! Loop 25 Times
                      Of Event:CloseWindow
                          Prog:Exit = 1
                          Prog:Cancelled = 1
                          Break
                      Of Event:Accepted
                          If Field() = ?Prog:Cancel
                              Beep(Beep:SystemQuestion)  ;  Yield()
                              Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                             icon:Question,'&Yes|&No',2,2)
                                  Of 1 ! &Yes Button
                                      Prog:Exit = 1
                                      Prog:Cancelled = 1
                                      Break
                                  Of 2 ! &No Button
                              End!Case Message
                          End ! If Field() = ?ProgressCancel
                  End ! Case Event()
                  If Prog:Exit
                      Break
                  End ! If Prog:Exit
              End ! Accept
              Do Prog:ProgressFinished
              Case Missive('File imported.','ServiceBase 3g',|
                             'midea.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReceiveExchangeLoanStock PROCEDURE (LONG fRefNumber,BYTE fLoan) !Generated from procedure template - Window

locIMEINumber        STRING(30)
locRESRecordNumber   LONG
qToBeScanned         QUEUE,PRE()
xchIMEINumber        STRING(30)
xchRefNumber         LONG
                     END
qIMEI                QUEUE,PRE()
IMEINumber           STRING(30)
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,4,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(164,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Receive Exchange IMEI Numbers'),AT(168,70),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,82,352,248),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Enter the IMEI Number(s) to receive'),AT(272,119),USE(?Prompt4),TRN,FONT(,,COLOR:Yellow,FONT:bold)
                       PROMPT('I.M.E.I.(s) successfully received.'),AT(280,188),USE(?Prompt4:2),TRN,FONT(,,COLOR:Yellow,FONT:bold)
                       PROMPT('I.M.E.I. Number'),AT(240,138),USE(?locIMEINumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s30),AT(316,138,124,10),USE(locIMEINumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                       LIST,AT(264,202,152,124),USE(?List1),VSCROLL,FORMAT('120L(2)|M~I.M.E.I. Number~@s30@'),FROM(qIMEI)
                       PANEL,AT(164,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Finish),TRN,FLAT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020752'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReceiveExchangeLoanStock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Finish,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:RETSALES.Open
  Relate:STOCKRECEIVETMP.Open
  Relate:USERS.Open
  Access:RETSTOCK.UseFile
  Access:EXCHHIST.UseFile
  Access:LOAN.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  Access:RETSALES.Clearkey(ret:Ref_Number_Key)
  ret:Ref_Number = fRefNumber
  Access:RETSALES.TryFetch(ret:Ref_Number_Key)
  
  Access:RETSTOCK.Clearkey(res:DespatchedPartKey)
  res:Ref_Number = ret:Ref_Number
  res:Despatched = 'YES'
  SET(res:DespatchedPartKey,res:DespatchedPartKey)
  LOOP UNTIL Access:RETSTOCK.Next()
      IF (res:Ref_Number <> ret:Ref_Number OR |
          res:Despatched <> 'YES')
          BREAK
      END
      IF (res:Received = 1)
          CYCLE
      END
  
      Loop i# = 1 TO RECORDS(glo:Queue)
          GET(glo:Queue,i#)
          Access:STOCKRECEIVETMP.Clearkey(stotmp:RecordNumberKey)
          stotmp:RecordNumber = glo:Pointer
          IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
              IF (stotmp:PartNumber = res:Part_Number)
                  IF (fLoan)  ! #12341 Add Loan Units (DBH: 26/01/2012)
                      Access:LOAN.Clearkey(loa:Ref_Number_Key)
                      loa:Ref_Number = res:LoanRefNumber
                      IF (Access:LOAN.TryFetch(loa:Ref_Number_key) = Level:Benign)
                          qToBeScanned.xchRefNumber = loa:Ref_Number
                          GET(qToBeScanned,qToBeScanned.xchRefNumber)
                          IF (ERROR())
                              qToBeScanned.xchIMEINumber = loa:ESN
                              qToBeScanned.xchRefNumber = loa:Ref_Number
                              ADD(qToBeScanned)
                              BREAK
                          END
                      END ! IF (Access:LOAN.TryFetch(loa:Ref_Number_key) = Level:Benign)
                  ELSE
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number = res:ExchangeRefNumber
                      IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                          qToBeScanned.xchRefNumber = xch:Ref_Number
                          GET(qToBeScanned,qToBeScanned.xchRefNumber)
                          IF (ERROR())
                              qToBeScanned.xchIMEINumber  = xch:ESN
                              qToBeScanned.xchRefNumber   = xch:Ref_Number
                              ADD(qToBeScanned)
                              BREAK
                          END
                      END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
                  END
              END ! IF (stotmp:PartNumber = res:Part_Number)
          END !IF (Access:STOCKRECEIVETMP.TryFetch(stotmp:RecordNumberKey) = Level:Benign)
      END ! Loop i# = 1 TO RECORDS(glo:Queue)
  END
  
  
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (fLoan)
      ?WindowTitle{prop:Text} = 'Receive Loan IMEI Numbers'    ! #12341 Use same process for Exchange & Loan Units (DBH: 26/01/2012)
  END
      ! Save Window Name
   AddToLog('Window','Open','ReceiveExchangeLoanStock')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:RETSALES.Close
    Relate:STOCKRECEIVETMP.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReceiveExchangeLoanStock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020752'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020752'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020752'&'0')
      ***
    OF ?locIMEINumber
      IF (locIMEINumber = '')
          CYCLE
      END
      
      qIMEI.IMEINumber = locIMEINumber
      GET(qIMEI,qIMEI.IMEINumber)
      IF (NOT ERROR())
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You have already processed this I.M.E.I. Number.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          SELECT(?locIMEINumber)
          CYCLE
      END
      
      qToBeScanned.xchIMEINumber = locIMEINumber
      GET(qToBeScanned,qToBeScanned.xchIMEINumber)
      IF (ERROR())
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Error! The selected I.M.E.I. does not match any of the tagged items.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          SELECT(?locIMEINumber)
          CYCLE
      END
      
      IF (fLoan)
          Access:LOAN.Clearkey(loa:Ref_Number_Key)
          loa:Ref_Number = qToBeScanned.xchRefNumber
          IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign)
              loa:Available = 'AVL'
              loa:StatusChangeDate = Today()
              IF (Access:LOAN.TryUpdate() = Level:Benign)
                  Access:RETSTOCK.Clearkey(res:LoanRefNumberKey)
                  res:Ref_Number = ret:Ref_Number
                  res:LoanRefNumber = loa:Ref_Number
                  IF (Access:RETSTOCK.Tryfetch(res:LoanRefNumberKey) = Level:Benign)
                      res:Received = 1
                      res:DateReceived = TODAY()
                      IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
      
                      END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
                  END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                  IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                      loh:Ref_Number     = loa:Ref_Number
                      loh:Date        = TODAY()
                      loh:Time        = CLOCK()
                      loh:User    = glo:Usercode
                      loh:Status        = 'UNIT RECEIVED'
                      loh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                                          '<13,10>RETAIL INVOICE NUMBER: ' & ret:Invoice_Number & |
                                          '<13,10>PRICE: ' & Format(res:Item_Cost,@n14.2)
      
                      IF (Access:LOANHIST.TryInsert())
                          Access:LOANHIST.CancelAutoInc()
                      END
                  END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
      
              ELSE ! IF (Access:LOAN.TryUpdate() = Level:Benign)
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Failed to update Loan Unit.'&|
                      '|'&|
                      '|Please try again.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END ! IF (Access:LOAN.TryUpdate() = Level:Benign)
          END ! IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign)
      ELSE ! IF (fLoan)
          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
          xch:Ref_Number  = qToBeScanned.xchRefNumber
          IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
              xch:Available = 'AVL'
              xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
              IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
                  Access:RETSTOCK.Clearkey(res:ExchangeRefNumberKey)
                  res:Ref_Number = ret:Ref_Number
                  res:ExchangeRefNumber = xch:Ref_Number
                  IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                      res:Received = 1
                      res:DateReceived = TODAY()
                      IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
      
                      END !IF (Access:RETSTOCK.TryUpdate() = Level:Benign)
                  END ! IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey) = Level:Benign)
                  IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                      exh:Ref_Number     = xch:Ref_Number
                      exh:Date        = TODAY()
                      exh:Time        = CLOCK()
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password = glo:Password
                      IF (Access:USERS.TryFetch(use:Password_Key))
                      END
                      exh:User        = use:User_Code
                      exh:Status        = 'UNIT RECEIVED'
                      exh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                                          '<13,10>RETAIL INVOICE NUMBER: ' & ret:Invoice_Number & |
                                          '<13,10>PRICE: ' & Format(res:Item_Cost,@n14.2)
      
                      IF (Access:EXCHHIST.TryInsert())
                          Access:EXCHHIST.CancelAutoInc()
                      END
                  END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
              ELSE
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Failed to update Exchange Unit.'&|
                      '|'&|
                      '|Please try again.','ServiceBase',|
                                 'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message
                  CYCLE
              END ! IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
          END ! IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
      END ! IF (fLoan)
      
      qIMEI.IMEINumber = locIMEINumber
      ADD(qIMEI)
      locIMEINumber = ''
      SELECT(?locIMEINumber)
      DISPLAY()
      
      DELETE(qToBeScanned)
      ! All Done?
      IF (Records(qToBeScanned) = 0)
          Beep(Beep:SystemAsterisk)  ;  Yield()
          Case Missive('All I.M.E.I. Numbers for the tagged items have been scanned.','ServiceBase',|
                         'midea.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Post(Event:CloseWindow)
      END ! IF (Records(qToBeScanned) = 0)
      
      
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
WIP_Check_Procedure PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
save_eau_id          USHORT,AUTO
Stock_Query          STRING(30)
LOC:Amount           DECIMAL(15,2)
No_Find              BYTE
BrLocator1           STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Master_Location      STRING(30)
Temp_Cut_String      STRING(10)
New_level            STRING(4)
Use_Exch_Byte        STRING('YES')
Exchange_IMEI        STRING(20)
Master_Type          STRING(30)
Temp_Job             STRING(20)
Tmp:Ref_Number       STRING(20)
pass:status          STRING(30)
ignore_imei          BYTE
ignore_job_number    BYTE
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Master_Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(WIPAUI)
                       PROJECT(wia:Internal_No)
                       PROJECT(wia:Audit_Number)
                       PROJECT(wia:Site_Location)
                       PROJECT(wia:Ref_Number)
                       JOIN(job:Ref_Number_Key,wia:Ref_Number)
                         PROJECT(job:ESN)
                         PROJECT(job:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
Temp_Job               LIKE(Temp_Job)                 !List box control field - type derived from local data
Tmp:Ref_Number         LIKE(Tmp:Ref_Number)           !Browse hot field - type derived from local data
wia:Internal_No        LIKE(wia:Internal_No)          !Primary key field - type derived from field
wia:Audit_Number       LIKE(wia:Audit_Number)         !Browse key field - type derived from field
wia:Site_Location      LIKE(wia:Site_Location)        !Browse key field - type derived from field
wia:Ref_Number         LIKE(wia:Ref_Number)           !Browse key field - type derived from field
job:Ref_Number         LIKE(job:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK8::wia:Audit_Number     LIKE(wia:Audit_Number)
HK8::wia:Site_Location    LIKE(wia:Site_Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB10::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse W.I.P.'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('WIP Audit Procedure'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,56,256,310),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('W.I.P. Audit Procedure'),USE(?Tab3)
                           STRING('Location'),AT(104,94),USE(?String13),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s20),AT(156,94,124,10),USE(Master_Location),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(5,124),FROM(Queue:FileDropCombo:1)
                           CHECK(' Ignore Job Number'),AT(156,110),USE(ignore_job_number),FONT(,8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           CHECK(' Ignore IMEI'),AT(156,124),USE(ignore_imei),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           BUTTON,AT(160,146),USE(?buttonRefresh),DISABLE,TRN,FLAT,ICON('refreshp.jpg')
                           STRING(@n_6),AT(164,193,60,24),USE(New_level),TRN,RIGHT,FONT(,20,010101H,FONT:bold,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(104,246),USE(?Tmp:Ref_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(160,246,124,10),USE(Tmp:Ref_Number),DISABLE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           STRING('IMEI Number'),AT(104,262),USE(?String10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(160,262,124,10),USE(Exchange_IMEI),DISABLE,FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           PROMPT('If scanning only job numbers, the WIP Audit will only run against the original j' &|
   'ob status and not the exchange status.'),AT(72,342,232,20),USE(?Message:Prompt),FONT(,,COLOR:Yellow,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('COUNTED'),AT(160,225,68,12),USE(?String6),TRN,CENTER,FONT(,,010101H,FONT:bold)
                         END
                       END
                       SHEET,AT(324,56,292,308),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Scanned Job / IMEI Numbers'),USE(?Tab2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       LIST,AT(328,74,284,282),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~I.M.E.I. Number~@s20@80L(2)|M~Job Number~@s20@'),FROM(Queue:Browse:1)
                       PANEL,AT(160,178,68,60),USE(?Panel1:2),FILL(COLOR:Green)
                       BUTTON('1'),AT(240,368,20,19),USE(?Button2),HIDE,FONT(,,,FONT:bold)
                       BUTTON('2'),AT(276,370,20,19),USE(?Button2:2),HIDE,FONT(,,,FONT:bold)
                       BUTTON('3'),AT(296,372,20,19),USE(?Button2:3),HIDE,FONT(,,,FONT:bold)
                       BUTTON('4'),AT(256,370,20,19),USE(?Button2:4),HIDE,FONT(,,,FONT:bold)
                       BUTTON('5'),AT(316,372,20,19),USE(?Button2:5),HIDE,FONT(,,,FONT:bold)
                       BUTTON('6'),AT(336,374,20,19),USE(?Button2:6),HIDE,FONT(,,,FONT:bold)
                       BUTTON('7'),AT(328,356,20,19),USE(?Button2:7),HIDE,FONT(,,,FONT:bold)
                       BUTTON('8'),AT(348,356,20,19),USE(?Button2:8),HIDE,FONT(,,,FONT:bold)
                       BUTTON('9'),AT(368,356,20,19),USE(?Button2:9),HIDE,FONT(,,,FONT:bold)
                       BUTTON('0'),AT(388,356,20,19),USE(?Button2:10),HIDE,FONT(,,,FONT:bold)
                       BUTTON('Close'),AT(68,370,76,20),USE(?Close),HIDE,LEFT,ICON('CANCEL.ICO')
                       BUTTON,AT(420,366),USE(?buttonComplete),TRN,FLAT,LEFT,ICON('compaudp.jpg')
                       BUTTON,AT(548,366),USE(?ContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                       BUTTON,AT(484,366),USE(?SuspendAudit),TRN,FLAT,LEFT,ICON('susaudp.jpg')
                       BUTTON,AT(548,366),USE(?FinishAudit),TRN,FLAT,HIDE,LEFT,ICON('finscanp.jpg')
                     END

myProg        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
myProg:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(myProg.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(myProg.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(myProg.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?myProg:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
myProg:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(myProg.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?myProg:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

SuspendContinue  Routine
    data
otherScanners   byte(0)
    code
    If wim:Status   <> 'SUSPENDED'

        ! Don't allow the audit to be suspended if other users are still scanning items
        Access:WIPSCAN.ClearKey(wsc:Completed_Audit_Key)
        wsc:Completed = 0
        wsc:Audit_Number = wim:Audit_Number
        set(wsc:Completed_Audit_Key, wsc:Completed_Audit_Key)
        loop
            if Access:WIPSCAN.Next() then break.
            if wsc:Completed <> 0 then break.
            if wsc:Audit_Number <> wim:Audit_Number then break.
            otherScanners = true
            break
        end

        if otherScanners
            Case Missive('Other users are still joined in the audit scanning. Please recheck.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
                Of 1 ! OK Button
            End !
        else
            wim:Status = 'SUSPENDED'
            Access:WIPAMF.Update()
            Post(Event:CloseWindow)
        end
    Else !emf:Status   <> 'SUSPENDED'
        wim:Status = 'AUDIT IN PROGRESS'
        Access:WIPAMF.Update()    ! #12491 Save status, so others can join (DBH: 15/05/2012)
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
            IF (use:User_Code = wim:User)
                ! This is the use that created the audit
                ?SuspendAudit{prop:Hide} = 0
                ?ContinueAudit{prop:Hide} = 1
                ?buttonRefresh{prop:Hide} = 0
                ?FinishAudit{prop:Hide} = 0
                ?tmp:Ref_Number{prop:Disable} = 0
                ?Exchange_IMEI{prop:Disable} = 0
                ?buttonComplete{prop:Hide} = 0
            ELSE ! IF 

                ! Hide the options and buttons for secondary scanners
                ?ignore_IMEI{prop:Disable} = 1
                ?ignore_Job_Number{prop:Disable} = 1
                ?buttonRefresh{prop:Disable} = 0
                ?tmp:Ref_Number{prop:Disable} = 0
                ?Exchange_IMEI{prop:Disable} = 0
                ?ContinueAudit{prop:Hide} = 1
                ?buttonComplete{prop:Hide} = 1
                ?SuspendAudit{prop:Hide} = 1
                ?FinishAudit{prop:Hide} = 0
                        
            END ! IF
        END ! IF
    End !emf:Status   <> 'SUSPENDED'
Calculate_Amount    ROUTINE
    myProg.ProgressSetup(Records(WIPAUI))
    myProg.ProgressText('Status: Refreshing Figures...')

    New_Level = 0
    Save_eau_ID = Access:WIPAUI.SaveFile()
    Access:WIPAUI.ClearKey(wia:Locate_IMEI_Key)
    wia:Audit_Number   = wim:Audit_Number
    wia:Site_Location  = Master_Location
    Set(wia:Locate_IMEI_Key,wia:Locate_IMEI_Key)
    Loop
        If Access:WipAUI.NEXT()
           Break
        End !If
        If wia:Audit_Number   <> wim:Audit_Number      |
        Or wia:Site_Location  <> Master_Location      |                  |
            Then Break.  ! End If
                IF (myProg.InsideLoop())
                BREAK
            END
        ! No need to check confirmed flag as we are no longer building an initial
        ! list of jobs to scan.
        New_Level += 1
    End !Loop
    Access:WIPAUI.RestoreFile(Save_eau_ID)

    myProg.ProgressFinish()
ProcessJob Routine
    data
jobStatus   like(sts:Status)
notFound    byte(0)
isExchange  byte(0)
latestJobNo like(job:Ref_Number)
    code

    if not ignore_job_number
        ! Original version just checked the job number in the jobs table, rather than webjob
        Access:Jobs.ClearKey(job:Ref_Number_Key)
        Job:Ref_number = Tmp:Ref_Number
        if Access:Jobs.Fetch(job:Ref_Number_Key)
            Case Missive('A job cannot be found matching the entered job number. Please re-check.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            exit
        end

        if not ignore_imei

            if Exchange_IMEI <> job:ESN

                notFound = true
                ! Check if exchange IMEI, if found use the exchange status rather than the job status
                If job:exchange_unit_number <> ''
                    Access:Exchange.ClearKey(xch:ref_number_key)
                    xch:ref_number = job:exchange_unit_number
                    if Access:Exchange.Fetch(xch:ref_number_key) = Level:Benign
                        if xch:ESN = Exchange_IMEI
                            notFound = false
                            isExchange = true
                        end
                    end
                end

                if notFound
                    Case Missive('The I.M.E.I. number on the job does not match the entered I.M.E.I. number. Please re-check.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    exit
                end

            end

        end

    else
        ! Fetch job by IMEI (or exchange IMEI) only - if bouncer just get most recent job
        Access:Jobs.ClearKey(job:ESN_Key)
        job:ESN = Exchange_IMEI
        set(job:ESN_Key, job:ESN_Key)
        loop
            if Access:JOBS.Next() then break.
            if job:ESN <> Exchange_IMEI then break.
            if job:Ref_Number > latestJobNo
                latestJobNo = job:Ref_Number
            end
        end

        if latestJobNo = 0
            ! Check exchange IMEI
            Access:Exchange.ClearKey(xch:ESN_Only_Key)
            xch:ESN = Exchange_IMEI
            set(xch:ESN_Only_Key, xch:ESN_Only_Key)
            loop
                if Access:Exchange.Next() then break.
                if xch:ESN <> Exchange_IMEI then break.
                if xch:Job_Number = 0 then cycle.
                if xch:Job_Number > latestJobNo
                    latestJobNo = job:Ref_Number
                    isExchange = true
                end
            end
        end

        if latestJobNo = 0
            Case Missive('A job cannot be found matching the entered I.M.E.I. number. Please re-check.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            exit
        else
            ! Fetch the latest job
            Access:Jobs.ClearKey(job:Ref_Number_Key)
            Job:Ref_number = latestJobNo
            if Access:Jobs.Fetch(job:Ref_Number_Key)
                Case Missive('A job cannot be found matching the entered I.M.E.I. number. Please re-check.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                exit
            end
        end
    end

! #12491 Don't care where the job is now (DBH: 23/05/2012)
!    IF (glo:WebJob = 1)
!        ! #12491 Don't allow RRC to scan jobs AT the ARC (DBH: 19/04/2012)
!        Access:JOBSE.Clearkey(jobe:RefNumberKey)
!        jobe:RefNumber  = job:Ref_Number
!        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!            If jobe:HubRepair = 1
!                Case Missive('This job is at the ARC. Please re-check.','ServiceBase 3g',|
!                             'mstop.jpg','/OK')
!                    Of 1 ! OK Button
!                End ! Case Missive
!                exit
!            End
!        End
!    END

    ! Check we haven't already audited this job
    Access:WIPAUI.ClearKey(wia:AuditRefNumberKey)
    wia:Audit_Number    = wim:Audit_Number
    wia:Ref_Number      = job:Ref_Number
    if Access:WIPAUI.Fetch(wia:AuditRefNumberKey) = Level:Benign
        Case Missive('This job is already on this audit.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        exit
    end

    ! Get the job status
    if isExchange
        jobStatus = job:Exchange_Status
    else
        jobStatus = job:Current_Status
    end

    If glo:WebJob
        Access:Webjob.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_number
        if access:Webjob.fetch(wob:refNumberKey) = Level:Benign
            if isExchange
                jobStatus = wob:Exchange_Status
            else
                jobStatus = wob:Current_Status
            end
        end
    End

    ! Insert WipAUI record
    If Access:WIPAUI.PrimeRecord() = Level:Benign
        wia:Site_Location   = wim:Site_Location
        wia:Status          = jobStatus
        wia:Audit_Number    = wim:Audit_Number
        wia:Ref_Number      = job:Ref_Number
        wia:New_In_Status   = False
        wia:Confirmed       = True
        wia:IsExchange      = isExchange
        If Access:WIPAUI.TryInsert() <> Level:Benign
            Access:WIPAUI.CancelAutoInc()
        End
    End !If Access:WIPAUI.PrimeRecord() = Level:Benign

    do Calculate_Amount
FinishSecondaryScan routine
    ! A user who had joined in the scanning has finished.

    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    if Access:USERS.Fetch(use:Password_Key) <> Level:Benign then exit.

    Access:WIPSCAN.ClearKey(wsc:Completed_Audit_Key)
    wsc:Completed = 0
    wsc:Audit_Number = wim:Audit_Number
    set(wsc:Completed_Audit_Key, wsc:Completed_Audit_Key)
    loop
        if Access:WIPSCAN.Next() then break.
        if wsc:Completed <> 0 then break.
        if wsc:Audit_Number <> wim:Audit_Number then break.
        if wsc:User = use:User_Code
            ! Update the WIPSCAN record as completed
            wsc:Completed = 1
            Access:WIPSCAN.Update()
            break
        end
    end

    ! Don't update the main WIP audit record, leave this to the user who created the audit

    Post(Event:CloseWindow)
SetupListColumns        ROUTINE
    ! #12491 Hide column if ignoring IMEIs (DBH: 19/04/2012)
    IF (ignore_job_number = 1)
        ?Browse:1{proplist:Width,2} = 0
    ELSE
        ?Browse:1{proplist:width,2} = 80
    END

    ! #12491 Hide column if ignoring IMEIs (DBH: 19/04/2012)
    IF (ignore_imei = 1)
        ?Browse:1{proplist:Width,1} = 0
    ELSE
        ?Browse:1{proplist:width,1} = 80
    END

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020497'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WIP_Check_Procedure')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHANGE.Open
  Relate:JOBSE.Open
  Relate:WEBJOB.Open
  Relate:WIPALC.Open
  Relate:WIPEXC.Open
  Relate:WIPSCAN.Open
  Access:WIPAMF.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:WIPAUI,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !IF wim:Status = 'SUSPENDED'
  !  !?Button15{PROP:Text} = 'Continue Audit'
  !  ?ContinueAudit{prop:Hide} = 0
  !  ?SuspendAudit{prop:Hide} = 1
  !  ?tmp:Ref_Number{prop:Disable} = 1
  !  ?Exchange_IMEI{prop:Disable} = 1
  !ELSE
  !  !?Button15{PROP:Text} = 'Suspend Audit'
  !  ?ContinueAudit{prop:Hide} = 1
  !  ?SuspendAudit{prop:Hide} = 0
  !END
  Master_Location = wim:Site_location
  !if glo:WebJob = 1
  !    Master_Location = glo:Default_Site_Location
      DISABLE(?Master_Location) ! Disable the location - we have only processed stock records for this location
  !End !If glo:WebJob = 1
  
  ! Restore the ignore imei/job number checkboxes
  ignore_imei = wim:Ignore_IMEI
  ignore_job_number = wim:Ignore_Job_Number
  
  if ignore_imei
      HIDE(?Exchange_IMEI)
      HIDE(?String10)
  end
  
  if ignore_job_number
      HIDE(?Tmp:Ref_Number:Prompt)
      HIDE(?Tmp:Ref_Number)
  end
  
  
  IF (wim:Status = 'SUSPENDED')
      ?buttonComplete{prop:Hide} = 1
      ?ContinueAudit{prop:Hide} = 0
      ?SuspendAudit{prop:Hide} = 1
      ?tmp:Ref_Number{prop:Disable} = 1
      ?Exchange_IMEI{prop:Disable} = 1
  ELSE ! IF
      Access:USERS.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:USERS.Tryfetch(use:Password_Key) = Level:Benign)
          IF (use:User_Code = wim:User)
              ! This is the use that created the audit
              ?SuspendAudit{prop:Hide} = 0
              ?ContinueAudit{prop:Hide} = 1
              ?buttonRefresh{prop:Hide} = 0
              ?FinishAudit{prop:Hide} = 0
              ?tmp:Ref_Number{prop:Disable} = 0
              ?Exchange_IMEI{prop:Disable} = 0
              ?buttonComplete{prop:Hide} = 0
          ELSE ! IF 
              ! A new challenger...
  
              ! Hide the options and buttons for secondary scanners
              ?ignore_IMEI{prop:Disable} = 1
              ?ignore_Job_Number{prop:Disable} = 1
              ?buttonRefresh{prop:Disable} = 0
              ?tmp:Ref_Number{prop:Disable} = 0
              ?Exchange_IMEI{prop:Disable} = 0
              ?ContinueAudit{prop:Hide} = 1
              ?buttonComplete{prop:Hide} = 1
              ?SuspendAudit{prop:Hide} = 1
              ?FinishAudit{prop:Hide} = 0
                      
          END ! IF
      END ! IF
  END ! IF
  
  Do SetupListColumns
      
      ! Save Window Name
   AddToLog('Window','Open','WIP_Check_Procedure')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,wia:Locate_IMEI_Key)
  BRW1.AddRange(wia:Site_Location)
  BIND('Temp_Job',Temp_Job)
  BIND('Tmp:Ref_Number',Tmp:Ref_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(Temp_Job,BRW1.Q.Temp_Job)
  BRW1.AddField(Tmp:Ref_Number,BRW1.Q.Tmp:Ref_Number)
  BRW1.AddField(wia:Internal_No,BRW1.Q.wia:Internal_No)
  BRW1.AddField(wia:Audit_Number,BRW1.Q.wia:Audit_Number)
  BRW1.AddField(wia:Site_Location,BRW1.Q.wia:Site_Location)
  BRW1.AddField(wia:Ref_Number,BRW1.Q.wia:Ref_Number)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?ignore_job_number{Prop:Checked} = True
    HIDE(?Tmp:Ref_Number:Prompt)
    HIDE(?Tmp:Ref_Number)
  END
  IF ?ignore_job_number{Prop:Checked} = False
    UNHIDE(?Tmp:Ref_Number:Prompt)
    UNHIDE(?Tmp:Ref_Number)
  END
  IF ?ignore_imei{Prop:Checked} = True
    HIDE(?Exchange_IMEI)
    HIDE(?String10)
  END
  IF ?ignore_imei{Prop:Checked} = False
    UNHIDE(?Exchange_IMEI)
    UNHIDE(?String10)
  END
  FDCB10.Init(Master_Location,?Master_Location,Queue:FileDropCombo:1.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:1
  FDCB10.AddSortOrder()
  FDCB10.AddField(loc:Location,FDCB10.Q.loc:Location)
  FDCB10.AddField(loc:RecordNumber,FDCB10.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBSE.Close
    Relate:WEBJOB.Close
    Relate:WIPALC.Close
    Relate:WIPEXC.Close
    Relate:WIPSCAN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','WIP_Check_Procedure')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ignore_job_number
      ! Save the setting immediately in case another scanner joins the audit
      wim:Ignore_Job_Number = ignore_job_number
      Access:WIPAMF.Update()
      
      
      Do SetupListColumns
    OF ?ignore_imei
      ! Save the setting immediately in case another scanner joins the audit
      wim:Ignore_IMEI = ignore_imei
      Access:WIPAMF.Update()
      
      
      Do SetupListColumns
    OF ?Tmp:Ref_Number
      if ~0{prop:AcceptAll}
          if Tmp:Ref_Number = 0
              SELECT(?Tmp:Ref_Number)
              CYCLE
          end
      
          if Ignore_IMEI = False
              if Exchange_IMEI = ''
                  SELECT(?Exchange_IMEI)
                  CYCLE
              end
          end
      
          do ProcessJob
      
          Tmp:Ref_Number = 0
          Exchange_IMEI = ''
          UPDATE()
          DISPLAY()
          SELECT(?Tmp:Ref_Number)
      
          BRW1.ResetSort(1)
      end
    OF ?SuspendAudit
      Do SuspendContinue
    OF ?FinishAudit
      Do FinishSecondaryScan
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020497'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020497'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020497'&'0')
      ***
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?ignore_job_number
      IF ?ignore_job_number{Prop:Checked} = True
        HIDE(?Tmp:Ref_Number:Prompt)
        HIDE(?Tmp:Ref_Number)
      END
      IF ?ignore_job_number{Prop:Checked} = False
        UNHIDE(?Tmp:Ref_Number:Prompt)
        UNHIDE(?Tmp:Ref_Number)
      END
      ThisWindow.Reset
    OF ?ignore_imei
      IF ?ignore_imei{Prop:Checked} = True
        HIDE(?Exchange_IMEI)
        HIDE(?String10)
      END
      IF ?ignore_imei{Prop:Checked} = False
        UNHIDE(?Exchange_IMEI)
        UNHIDE(?String10)
      END
      ThisWindow.Reset
    OF ?buttonRefresh
      ThisWindow.Update
      Do Calculate_Amount
    OF ?Exchange_IMEI
      if ~0{prop:AcceptAll}
          if Exchange_IMEI = ''
              SELECT(?Exchange_IMEI)
              CYCLE
          end
      
          if ignore_job_number = False
              if Tmp:Ref_Number = 0
                  SELECT(?Tmp:Ref_Number)
                  CYCLE
              end
          end
      
          do ProcessJob
      
          Tmp:Ref_Number = 0
          Exchange_IMEI = ''
          UPDATE()
          DISPLAY()
          if ignore_job_number = False
              SELECT(?Tmp:Ref_Number)
          else
              SELECT(?Exchange_IMEI)
          end
      
          BRW1.ResetSort(1)
      end
    OF ?Button2
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'1'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:2
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'2'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:3
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'3'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:4
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'4'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:5
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'5'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:6
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'6'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:7
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'7'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:8
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'8'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:9
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'9'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?Button2:10
      ThisWindow.Update
      Temp_Cut_String = CLIP(New_Level)
      Temp_Cut_String = CLIP(Temp_Cut_String)&'0'
      New_Level = Temp_Cut_String
      ThisWindow.Update()
    OF ?buttonComplete
      ThisWindow.Update
      IF wim:Complete_Flag = 0
      
          otherScanners# = false
      
          ! Check if any other users are still scanning items
          Access:WIPSCAN.ClearKey(wsc:Completed_Audit_Key)
          wsc:Completed = 0
          wsc:Audit_Number = wim:Audit_Number
          set(wsc:Completed_Audit_Key, wsc:Completed_Audit_Key)
          loop
              if Access:WIPSCAN.Next() then break.
              if wsc:Completed <> 0 then break.
              if wsc:Audit_Number <> wim:Audit_Number then break.
              otherScanners# = true
              break
          end
      
          if otherScanners#
              Case Missive('Other users are still joined in the audit scanning. Please recheck.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              CYCLE  ! #12491 Do not quit the screen as this is causing confusion (DBH: 15/05/2012)
          else
              Case Missive('Do you wish to complete the current WIP Audit?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      cont# = 0
                      Case Missive('Are you sure? Once completed the audit cannot be edited.','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes')
                          Of 2 ! Yes Button
                         cont# = 1
                          Of 1 ! No Button
                         cont# = 0
                      End ! Case Missive
                      !Lets check if we can!
                      !Anything left?
                      DO Calculate_Amount
          
                      wim:Complete_Flag = 1
                      wim:Date_Completed = TODAY()
                      wim:Time_Completed = CLOCK()
                      wim:Status = 'AUDIT COMPLETED'
                      Access:WIPAMF.Update()
              
                      pass:status = '' !Pick_Status()
      
                      WIPAuditReportCriteria(wim:audit_number)
                  Of 1 ! No Button
                      CYCLE
              End ! Case Missive
          end
      ELSE ! IF wim:Complete_Flag = 0
          Case Missive('This audit is already marked as complete.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END ! IF wim:Complete_Flag = 0
       POST(Event:CloseWindow)
    OF ?ContinueAudit
      ThisWindow.Update
      Do SuspendContinue
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = wim:Audit_Number
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = Master_Location
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !------------------------------------------------------------------
          ! Webify tmp:Ref_Number
          !
          Temp_Job = job:Ref_Number
          if glo:WebJob then
              !Change the tmp ref number if you
              !can Look up from the wob file
              access:Webjob.clearkey(wob:RefNumberKey)
              wob:RefNumber = job:Ref_number
              if access:Webjob.fetch(wob:refNumberKey)=level:benign then
                  access:tradeacc.clearkey(tra:Account_Number_Key)
                  tra:Account_Number = ClarioNET:Global.Param2
                  access:tradeacc.fetch(tra:Account_Number_Key)
                  Temp_Job = Job:Ref_Number & '-' & tra:BranchIdentification & wob:JobNumber
              END
              !Set up no preview on client
              ClarioNET:UseReportPreview(0)
          END
          !------------------------------------------------------------------
  PARENT.SetQueueRecord


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

myProg.Init                 PROCEDURE(LONG func:Records)
    CODE
        myProg.ProgressSetup(func:Records)
myProg.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        myProg.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(myProg:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        ?myProg:CancelButton{prop:Hide} = 1
        myProg.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.ResetProgress      Procedure(Long func:Records)
CODE

    myProg.recordsToProcess = func:Records
    myProg.recordsprocessed = 0
    myProg.percentProgress = 0
    myProg.progressThermometer = 0
    myProg.CNprogressThermometer = 0
    myProg.skipRecords = 0
    myProg.userText = ''
    myProg.CNuserText = ''
    myProg.percentText = ''
    myProg.CNpercentText = myProg.percentText


myProg.Update      Procedure(<String func:String>)
    CODE
        RETURN (myProg.InsideLoop(func:String))
myProg.InsideLoop     Procedure(<String func:String>)
CODE

    myProg.SkipRecords += 1
    If myProg.SkipRecords < 20
        myProg.RecordsProcessed += 1
        Return 0
    Else
        myProg.SkipRecords = 0
    End
    if (func:String <> '')
        myProg.UserText = Clip(func:String)
        myProg.CNUserText = myProg.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        myProg.NextRecord()
        if (myProg.CancelLoop())
            return 1
        end ! if (myProg.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

myProg.ProgressText        Procedure(String    func:String)
CODE

    myProg.UserText = Clip(func:String)
    myProg.CNUserText = myProg.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.Kill     Procedure()
    CODE
        myProg.ProgressFinish()
myProg.ProgressFinish     Procedure()
CODE

    myProg.ProgressThermometer = 100
    myProg.CNProgressThermometer = 100

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(myProg:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(myProg:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

myProg.NextRecord      Procedure()
CODE
    Yield()
    myProg.RecordsProcessed += 1
    !If myProg.percentprogress < 100
        myProg.percentprogress = (myProg.recordsprocessed / myProg.recordstoprocess)*100
        If myProg.percentprogress > 100 or myProg.percentProgress < 0
            myProg.percentprogress = 0
        End
        If myProg.percentprogress <> myProg.ProgressThermometer then
            myProg.ProgressThermometer = myProg.percentprogress
        End
    !End
    myProg.CNPercentText = myProg.PercentText
    myProg.CNProgressThermometer = myProg.ProgressThermometer
    Display()

myProg.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?myProg:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
BrowseWIPAudit PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Yes_Byte             STRING('Y')
save_eau_id          USHORT,AUTO
Status_Info          STRING(20)
pass:Status          STRING(30)
recordCount          LONG
BRW1::View:Browse    VIEW(WIPAMF)
                       PROJECT(wim:Audit_Number)
                       PROJECT(wim:Date)
                       PROJECT(wim:User)
                       PROJECT(wim:Status)
                       PROJECT(wim:Site_location)
                       PROJECT(wim:Complete_Flag)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
wim:Audit_Number       LIKE(wim:Audit_Number)         !List box control field - type derived from field
wim:Date               LIKE(wim:Date)                 !List box control field - type derived from field
wim:User               LIKE(wim:User)                 !List box control field - type derived from field
wim:Status             LIKE(wim:Status)               !List box control field - type derived from field
wim:Site_location      LIKE(wim:Site_location)        !List box control field - type derived from field
wim:Complete_Flag      LIKE(wim:Complete_Flag)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::wim:Complete_Flag    LIKE(wim:Complete_Flag)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse WIP Audits'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The WIP Audit File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,110,276,214),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('56R(2)|M~Audit Number~L@n-14@49R(2)|M~Date~@d17@29R(2)|M~User~L@s3@120L(2)|M~Sta' &|
   'tus~@s30@0L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,110),USE(?Button:NewAudit),TRN,FLAT,LEFT,ICON('newaud2p.jpg')
                       BUTTON,AT(448,172),USE(?Button:JoinAudit),TRN,FLAT,HIDE,LEFT,ICON('joinaudp.jpg')
                       BUTTON,AT(448,198),USE(?Button:ContinueAudit),TRN,FLAT,LEFT,ICON('contaudp.jpg')
                       BUTTON,AT(448,242),USE(?Button:CompleteAudit),TRN,FLAT,HIDE,LEFT,ICON('compaudp.jpg')
                       BUTTON,AT(448,300),USE(?Button:ReprintAudit),TRN,FLAT,LEFT,ICON('prnaudp.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('W.I.P Audit Table'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14),AT(168,98,64,10),USE(emf:Audit_Number),RIGHT(1),FONT(,,010101H,FONT:bold),COLOR(COLOR:White)
                           BUTTON,AT(448,140),USE(?btnCancelAudit),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                           BUTTON,AT(448,272),USE(?ButtonViewStatuses),FLAT,ICON('ViewASp.jpg')
                         END
                         TAB('Completed Audits'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateBuffer           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020495'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseWIPAudit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:STATUS.Open
  Relate:WEBJOB.Open
  Relate:WIPALC.Open
  Relate:WIPEXC.Open
  Relate:WIPSCAN.Open
  Access:USERS.UseFile
  Access:EXCHAUI.UseFile
  Access:WIPAMF.UseFile
  Access:WIPAUI.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:WIPAMF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  if glo:WebJob = 0
      Set(defstock)
      access:defstock.next()
      If dst:use_site_location = 'YES'
          glo:Default_Site_Location = dst:site_location
      Else
          glo:Default_Site_Location = ''
      End
  end !If glo:WebJob = 1
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      emf:User = use:User_Code
  Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  If glo:WebJob = 1
    !wim:Site_location = glo:Default_Site_Location
  Else !If glo:WebJob = 1
    glo:Default_Site_Location = use:Location
  End !If glo:WebJob = 1
      ! Save Window Name
   AddToLog('Window','Open','BrowseWIPAudit')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,wim:Secondary_Key)
  BRW1.AddRange(wim:Complete_Flag)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,wim:Audit_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,wim:Secondary_Key)
  BRW1.AddRange(wim:Complete_Flag)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?emf:Audit_Number,wim:Audit_Number,1,BRW1)
  BRW1.AddField(wim:Audit_Number,BRW1.Q.wim:Audit_Number)
  BRW1.AddField(wim:Date,BRW1.Q.wim:Date)
  BRW1.AddField(wim:User,BRW1.Q.wim:User)
  BRW1.AddField(wim:Status,BRW1.Q.wim:Status)
  BRW1.AddField(wim:Site_location,BRW1.Q.wim:Site_location)
  BRW1.AddField(wim:Complete_Flag,BRW1.Q.wim:Complete_Flag)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:WEBJOB.Close
    Relate:WIPALC.Close
    Relate:WIPEXC.Close
    Relate:WIPSCAN.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseWIPAudit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:NewAudit
      If Access:WIPAMF.PrimeRecord() = Level:Benign
          wim:Date    = Today()
          wim:Time    = Clock()
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              ! Found
              emf:User    = use:User_Code
          Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              ! Error
          End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          wim:User    = use:User_Code
          wim:Complete_Flag = 0
          wim:Status  = 'AUDIT IN PROGRESS'
          If glo:WebJob = 1
              wim:Site_LOcation = glo:Default_Site_Location
          Else ! End ! If glo:WebJob = 1
              wim:Site_Location   = use:Location
          End !If glo:WebJob = 1
          If Access:WIPAMF.TryInsert() = Level:Benign
              ! Insert Successful
              
          Else ! If Access:WIPAMF.TryInsert() = Level:Benign
              ! Insert Failed
              Access:WIPAMF.CancelAutoInc()
          End ! If Access:WIPAMF.TryInsert() = Level:Benign
      End !If Access:WIPAMF.PrimeRecord() = Level:Benign
      WIP_Check_Procedure()
      BRW1.REsetSort(1)
      !ELSE
      !  Case MessageEx('There are incomplete Stock Audits currently outstanding. You cannot start another Stock Audit.','ServiceBase 2000',|
      !               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,3000)
      !    Of 1 ! &OK Button
      !  End!Case MessageEx
      !END
      !BRW1.ResetSort(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020495'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020495'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020495'&'0')
      ***
    OF ?Button:ContinueAudit
      ThisWindow.Update
          BRW1.UpdateViewRecord()
          IF (wim:Complete_Flag = 0)
              FREE(glo:Queue)
              
              Access:USERS.Clearkey(use:Password_Key)
              use:Password = glo:Password
              IF (Access:USERS.TryFetch(use:Password_Key))
                  CYCLE
              END ! IF
              
              IF (wim:User = use:User_Code)
                  ! This user created the audit, allow them back in as the owner is case of a crash
                  ! They are the only user who can complete the audit
              ELSE ! IF
                  IF (wim:Status = 'AUDIT IN PROGRESS')
                      Beep(Beep:SystemQuestion)  ;  Yield()
                      Case Missive('This audit is already in progress. Would you like to assist in scanning for this audit?','ServiceBase',|
                                     'mquest.jpg','\&No|/&Yes') 
                      Of 2 ! &Yes Button
                      Of 1 ! &No Button
                          CYCLE
                      End!Case Message            
                  ELSIF (wim:Status = 'SUSPENDED')
                      Beep(Beep:SystemQuestion)  ;  Yield()
                      Case Missive('This audit has been suspended. Do you wish continue this audit.','ServiceBase',|
                                     'mquest.jpg','\&No|/&Yes') 
                      Of 2 ! &Yes Button
                      Of 1 ! &No Button
                          CYCLE
                      End!Case Message            
                  ELSE
                      CYCLE        
                  END
                  
                  newUser# = TRUE
                  ! Check if they are already in the audit
                  Access:WIPSCAN.ClearKey(wsc:Completed_Audit_Key)
                  wsc:Completed = 0
                  wsc:Audit_Number = wim:Audit_Number
                  set(wsc:Completed_Audit_Key, wsc:Completed_Audit_Key)
                  loop
                      if Access:WIPSCAN.Next() then break.
                      if wsc:Completed <> 0 then break.
                      if wsc:Audit_Number <> wim:Audit_Number then break.
                      if wsc:User = use:User_Code
                          newUser# = FALSE
                      end
                  end
                        
                  if newUser#
                      if Access:WIPSCAN.PrimeRecord() = Level:Benign
                          wsc:Audit_Number = wim:Audit_Number
                          wsc:User = use:User_Code
                          wsc:Completed = 0
                          if Access:WIPSCAN.Insert() <> Level:Benign
                              Access:WIPSCAN.CancelAutoInc()
                          end
                      end
                  end            
              END ! IF
      
              WIP_Check_Procedure()
                  
              BRW1.ResetSort(1)
          ELSE ! IF
              Case Missive('This WIP audit is complete. Please start a new audit or select an incomplete audit.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive    
          END ! IF
    OF ?Button:CompleteAudit
      ThisWindow.Update
      ThisWindow.Update()
      IF emf:Complete_Flag = 0
          Case Missive('Do you wish to complete the current stock audit?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  !Lets check if we can!
                  !Anything left?
      
                  Save_eau_ID = Access:EXCHAUI.SaveFile()
                  Access:EXCHAUI.ClearKey(eau:Main_Browse_Key)
                  eau:Audit_Number   = emf:Audit_Number
                  eau:Confirmed      = 0
                  Set(eau:Main_Browse_Key,eau:Main_Browse_Key)
                  Loop
                      If Access:EXCHAUI.NEXT()
                         Break
                      End !If
                      If eau:Audit_Number   <> emf:Audit_Number      |
                      Or eau:Confirmed      <> 0      |
                          Then Break.  ! End If
                      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                      xch:Ref_Number  = eau:Ref_Number
                      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Found
                          xch:Available = 'AUS'
                          xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                          Access:EXCHANGE.Update()
                          get(exchhist,0)
                          if access:exchhist.primerecord() = Level:Benign
                              exh:ref_number      = xch:ref_number
                              exh:date            = Today()
                              exh:time            = Clock()
                              access:users.clearkey(use:password_key)
                              use:password        = glo:password
                              access:users.fetch(use:password_key)
                              exh:user = use:user_code
                              exh:status          = 'AUDIT SHORTAGE'
                              access:exchhist.insert()
                          end!if access:exchhist.primerecord() = Level:Benign
      
                      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
                  End !Loop
                  Access:EXCHAUI.RestoreFile(Save_eau_ID)
      
                  emf:Complete_Flag = 1
                  Access:EXCHAMF.Update()
                  Case Missive('Report printed.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
      !            Stock_Audit_Report (STOM:Audit_No)
              Of 1 ! No Button
          End ! Case Missive
      ELSE
          Case Missive('This stock audit is already marked as complete.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      END
      
    OF ?Button:ReprintAudit
      ThisWindow.Update
      IF wim:Complete_Flag = 0
        !Cannot continue!
          Case Missive('This audit is not complete. You cannot reprint the audit.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      ELSE
        pass:status = '' !Pick_Status()
       WIPAuditReportCriteria(wim:audit_number)
          ThisWindow.Update()
      END
      BRW1.UpdateBuffer()
    OF ?btnCancelAudit
      ThisWindow.Update
      ! #13432 Add option to cancel an audit (DBH: 15/01/2015)
      IF (SecurityCheck('WIP AUDIT CANCELLATION'))
          BEEP(BEEP:SystemHand)  ;  YIELD()
          CASE Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          OF 1 ! &OK Button
          END!CASE MESSAGE
          CYCLE
      END ! IF
      
      BEEP(BEEP:SystemQuestion)  ;  YIELD()
      CASE Missive('Please confirm that you wish to cancel the highlighted Audit?'&|
          '|Select CONFIRM to continue.','ServiceBase',|
                     'mquest.jpg','\STOP|/Confirm') 
      OF 2 ! Confirm Button
      OF 1 ! STOP Button
          CYCLE
      END!CASE MESSAGE
      
      
      BRW1.UpdateViewRecord()
      wim:Complete_Flag = 1
      wim:Date_Completed = TODAY()
      wim:Time_Completed = CLOCK()
      wim:Status = 'AUDIT CANCELLED'
      Access:WIPAMF.TryUpdate()
      
      BRW1.ResetSort(1)
    OF ?ButtonViewStatuses
      ThisWindow.Update
      PickJobStatus_Exclusions
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF CHOICE(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 1
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 0
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.UpdateBuffer PROCEDURE

  CODE
  PARENT.UpdateBuffer
  if wim:Status  = 'AUDIT IN PROGRESS'
      ?Button:ContinueAudit{Prop:Icon} = '~joinaudp.jpg'
  else
      ?Button:ContinueAudit{Prop:Icon} = '~contaudp.jpg'
  end
  
  display(?Button:ContinueAudit)


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  IF glo:Default_Site_Location <> ''
    IF wim:Site_location <> glo:Default_Site_Location
      RETURN Record:Filtered
    ELSE
      RETURN Record:OK
    END
  END
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
GetNewAuditLevel PROCEDURE (SentCurrentLevel)         !Generated from procedure template - Window

DisplayCurrentLevel  LONG
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,-1,28,24),USE(?ButtonHelp),SKIP,TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(245,148,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('New Audit Level'),AT(249,151),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(385,151),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(245,162,191,94),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Current Audit Level:'),AT(264,190),USE(?DisplayCurrentLevel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@n-14),AT(356,190,60,10),USE(DisplayCurrentLevel),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                       PROMPT('New Audit Level:'),AT(264,220),USE(?Global_Long:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@n-14),AT(356,220,60,10),USE(Global_Long),RIGHT(1),FONT(,,,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(245,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(304,258),USE(?ButtonOK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(372,258),USE(?ButtonCancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020787'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GetNewAuditLevel')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !(SentCurrentLevel)
  DisplayCurrentLevel = SentCurrentLevel
      ! Save Window Name
   AddToLog('Window','Open','GetNewAuditLevel')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','GetNewAuditLevel')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonCancel
      Global_Long = 989898     !flag to say cancelled
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020787'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020787'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020787'&'0')
      ***
    OF ?ButtonOK
      ThisWindow.Update
       POST(Event:CloseWindow)
    OF ?ButtonCancel
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ImportAuditFile PROCEDURE (SentAuditNo,SentLocation)  !Generated from procedure template - Window

LocalCount           LONG
ErrorCount           LONG
LocalErrorString     STRING(100)
LocalFilename        STRING(255),STATIC
ExportFilename       STRING(225),STATIC
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,4,28,24),USE(?ButtonHelp),SKIP,TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(165,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Import Audit File'),AT(169,71),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,84,351,244),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Import File Name'),AT(176,176),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(472,188),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                       ENTRY(@s255),AT(176,190,291,11),USE(LocalFilename)
                       BUTTON,AT(449,242),USE(?ButtonImport),TRN,FLAT,ICON('ImpFileP.jpg')
                       PANEL,AT(165,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?ButtonCancel),FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup9          SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(LocalFilename),Create,Bindable,Thread
Record        Record
Field1         String(30)
Field2         String(30)
Field3         long
END
END

ExportFile    File,Driver('BASIC'),Pre(expfil),Name(ExportFilename),Create,Bindable,Thread
Record        Record
SiteLocation   String(30)
AuditNumber    String(10)
StockRef       String(30)
ErrorString    String(100)
END
END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

TheImport       Routine

!ImportFile    File,Driver('BASIC'),Pre(impfil),Name(LocalFilename),Create,Bindable,Thread
!Record              Record
!    Field1         String(30)
!    Field2         String(30)
!    Field3         long
!END
!END

    if clip(GETINI('EXCEPTION','AUDITIMPORT',,CLIP(Path()) & '\SB2KDEF.INI')) = '' then
        miss# = missive('No exception file has been set up in defaults. This procedure cannot work until that is completed.','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END


    if clip(LocalFilename) = '' then
        miss# = missive('Please supply a filename to import','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END


    if not exists(LocalFilename) then
        miss# = missive('Please supply a correct filename to import','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END


    Open(importFile)
    if error() then
        miss# = missive('Unable to open requested file, please try an alternative','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END


    Set(ImportFile)
    Next(ImportFile)
    if error() then
        miss# = missive('Unable to read requested file, please try an alternative','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END

    !First line MUST be 'H',Location,auditnumber
    if upper(impfil:Field1) <> 'H' then
        miss# = missive('This file does not have a header row','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END
    if Upper(impfil:field2) <> Upper(SentLocation) then
        miss# = missive('This file is not for the same location as the audit','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END
    if Impfil:Field3 <> SentAuditNo then
        miss# = missive('This file does not match the current audit number','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END

    !OK ready to go for the import
    ErrorCount = 0
    LocalCount = 1  !Already read the first line :-)

    Loop

        Next(ImportFile)

        if error() then
            ErrorCount += 1
            LocalErrorString = 'This file has ended without a final Total Line'
            Do ExportError
            Break
        END

        LocalCount += 1

        Case impfil:Field1

            of 'D'      !Data line

                Do ImportLine

            of 'T'      !totals line
                !does the total match the count?
                if LocalCount - 2 <> deformat(impfil:Field2) then
                    LocalErrorString = 'Mismatch of totals '&clip(LocalCount-2)& ' counted. File imports '&clip(impfil:field2)
                    Do ExportError
                END
                LocalCount -= 1     !so that the final line is not counted as an import
                Break


            ELSE
                LocalErrorString = 'Non compliant line type encountered'
                Do ExportError

        END !case first field
    END !loop through import file

    If ErrorCount = 0 then
        Miss# = missive('This file has imported without errors. '&clip(LocalCount-1)&' records have been imported','ServiceBase 3g','mexclam.jpg','OK')
    ELSE
    
        Miss# = missive('Certain lines were not imported, review exception file.','ServiceBase 3g','mexclam.jpg','OK')

    END


    Close(ImportFile)

    if clip(ExportFilename) <> '' then
        Close(ExportFile)
    END !if export filename exists

    exit
ImportLine      Routine

    !Find the stockAudit record for this

    If impfil:Field2 = ''
        LocalErrorString = 'Stock Part Number not provided'
        Do ExportError
    ELSE
        !Stock from stocklocation key
        Access:Stock.clearkey(sto:Location_Key)
        sto:Location = SentLocation
        sto:Part_Number = upper(IMPFIL:Field2)
        if access:Stock.fetch(sto:Location_Key)
            LocalErrorString = 'Stock record not found for this Part number at this location'
            Do ExportError
        ELSE
            Access:StoAudit.clearkey(stoa:Lock_Down_Key)
            stoa:Audit_Ref_No  = stom:Audit_No
            stoa:Stock_Ref_No  = sto:Ref_Number
            if access:StoAudit.fetch(stoa:Lock_Down_Key)
                LocalErrorString = 'This part number does not appear in this audit.'
                Do ExportError
            ELSE
                If stom:CompleteType = 'S' then   !scanning mode

                    stoa:New_Level += Impfil:Field3
                    stoa:Confirmed = 'Y'
                    Access:StoAudit.update()

                    !General shortage needs to be kept up to date as this is not done on [next] button
                    if STOA:New_Level = STOA:Original_Level
                        !if stock levels are the same this can be deleted
                        Access:Genshort.clearkey(gens:Lock_Down_Key)
                        gens:Audit_No      = STOA:Audit_Ref_No
                        gens:Stock_Ref_No  = sto:Ref_Number
                        If access:GenShort.fetch(gens:Lock_Down_Key)
                            !not found - OK = probably 1 = 1
                        ELSE
                            Access:GenShort.deleteRecord()
                        END
                    ELSE
                        !stock levels are different - record this
                        Access:Genshort.clearkey(gens:Lock_Down_Key)
                        gens:Audit_No      = STOA:Audit_Ref_No
                        gens:Stock_Ref_No  = sto:Ref_Number
                        If access:GenShort.fetch(gens:Lock_Down_Key)
                            !new record
                            Access:GenShort.primerecord()
                            gens:Audit_No     = STOA:Audit_Ref_No
                            gens:Stock_Ref_No = sto:Ref_Number
                        END !if new record needed
                        GENS:Stock_Qty     = STOA:New_Level - STOA:Original_Level
                        Access:GenShort.Update()
                    END !if new record neeed
                ELSE
                    !manual mode
                    if stoa:Confirmed = 'Y' then
                        LocalErrorString = 'This part is already marked as complete.'
                        do ExportERror
                    ELSE

                        stoa:New_Level += Impfil:Field3
                        Access:StoAudit.update()

                    END
                END !If stom:CompleteType = 'S'
            END !if stoaudit recordFound
        END !if stock found
    END !if field2 exists

ExportError         Routine

    ErrorCount += 1

    if ExportFilename = '' then
        !not been generated yet
        ExportFilename = GETINI('EXCEPTION','AUDITIMPORT',,CLIP(Path()) & '\SB2KDEF.INI')  |
                         & '\StockAudit'&'_'&format(today(),@D12)&'-'&clip(format(SentAuditNo,@n_9))&'.csv'
                        !StockAudit_Date_AuditNumber.csv,

        if exists(ExportFilename) then remove(exportFilename).

        Create(ExportFile)
        Open(ExportFile)

        !first line
        !expfil:LineNo = 'Line No'
        expfil:siteLocation = 'Location'
        expfil:AuditNumber = 'Audit Number'
        expfil:StockRef = 'Part No'
        expfil:ErrorString = 'Reason'
        Add(ExportFile)

    END !if exportfilename is still blank

    !expfil:LineNo = LocalCount
    expfil:SiteLocation = SentLocation
    expfil:AuditNumber = format(stoa:Audit_Ref_No,@n10)
    expfil:StockRef = impfil:Field2
    expfil:ErrorString = LocalErrorString
    Add(ExportFile)

    exit

!ExportFile    File,Driver('BASIC'),Pre(expfil),Name(ExportFilename),Create,Bindable,Thread
!Record        Record
!SiteLocation   String(30)
!AuditNumber    String(10)
!StockRef       String(30)
!ErrorString    String(100)
!END
!END

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020788'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ImportAuditFile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:GENSHORT.Open
  Relate:STMASAUD.Open
  Access:STOCK.UseFile
  Access:STOAUDIT.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !reset the stored variables to they have to pick/create new ones
  LocalFilename = ''
  ExportFilename = ''
      ! Save Window Name
   AddToLog('Window','Open','ImportAuditFile')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('CSV Files','*.CSV')
  FileLookup9.WindowTitle='Audit File'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:GENSHORT.Close
    Relate:STMASAUD.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ImportAuditFile')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonImport
      Do TheImport
    OF ?ButtonCancel
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020788'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020788'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020788'&'0')
      ***
    OF ?LookupFile
      ThisWindow.Update
      LocalFilename = FileLookup9.Ask(1)
      DISPLAY
    OF ?ButtonImport
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ImportExcAuditFile PROCEDURE (SentAuditNo,SentLocation) !Generated from procedure template - Window

LocalCount           LONG
ErrorCount           LONG
LocalErrorString     STRING(100)
LocalFilename        STRING(255),STATIC
ExportFilename       STRING(225),STATIC
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(649,4,28,24),USE(?ButtonHelp),SKIP,TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(165,68,352,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Import Exchange Audit File'),AT(169,71),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(465,71),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(165,84,351,244),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Import File Name'),AT(176,172),USE(?Prompt3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(472,188),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                       ENTRY(@s255),AT(176,190,291,11),USE(LocalFilename)
                       BUTTON,AT(449,228),USE(?ButtonImport),TRN,FLAT,ICON('ImpFileP.jpg')
                       PANEL,AT(165,332,352,28),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?ButtonCancel),FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup9          SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(LocalFilename),Create,Bindable,Thread
Record        Record
Field1         String(30)
Field2         String(30)
Field3         String(30)   !imei
Field4         String(30)
END
END

ExportFile    File,Driver('BASIC'),Pre(expfil),Name(ExportFilename),Create,Bindable,Thread
Record        Record
SiteLocation   String(30)
AuditNumber    String(10)
StockRef       String(30)
ErrorString    String(100)
END
END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

TheImport       Routine

    Global_Long = 0     !used to return how many were imported

    if clip(GETINI('EXCEPTION','AUDITIMPORT',,CLIP(Path()) & '\SB2KDEF.INI')) = '' then
        miss# = missive('No exception file path has been set up in defaults. This procedure cannot work until that is completed.','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END

    if clip(LocalFilename) = '' then
        miss# = missive('Please supply a filename to import','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END

    if not exists(LocalFilename) then
        miss# = missive('Please supply a correct filename to import','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END

    Open(importFile)
    if error() then
        miss# = missive('Unable to open requested file, please try an alternative','ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END

    Set(ImportFile)
    Next(ImportFile)
    if error() then
        miss# = missive('Unable to read requested file, please try an alternative','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END

    !First line MUST be 'H',Location,auditnumber
    if impfil:Field1 <> 'H' then
        miss# = missive('This file does not have a header row','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END
    if upper(impfil:field2) <> upper(SentLocation) then
        miss# = missive('This file is not for the same location as the audit','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END
    if Impfil:Field3 <> SentAuditNo then
        miss# = missive('This file does not match the current audit number','ServiceBase 3g','mexclam.jpg','OK')
        Close(importFile)
        EXIT
    END

    !OK ready to go for the import
    ErrorCount = 0
    LocalCount = 1  !Already read the first line :-)

    Loop

        Next(ImportFile)

        if error() then
            ErrorCount += 1
            LocalErrorString = 'This file has ended without a final Total Line'
            Do ExportError
            Break
        END

        LocalCount += 1

        Case impfil:Field1

            of 'D'      !Data line

                Do ImportLine

            of 'T'      !totals line
                !does the total match the count?
                if LocalCount - 2 <> deformat(impfil:Field2) then
                    LocalErrorString = 'Mismatch of totals '&clip(LocalCount-2)& ' counted. File imports '&clip(impfil:field2)
                    Do ExportError
                END
                LocalCount -= 1     !so that the final line is not counted as an import
                Break


            ELSE
                LocalErrorString = 'Non compliant line type encountered'
                Do ExportError

        END !case first field
    END !loop through import file

    Close(ImportFile)

    If ErrorCount = 0 then
        Miss# = missive('This file has imported without errors. '&clip(LocalCount-1)&' records have been imported','ServiceBase 3g','mexclam.jpg','OK')
    ELSE
    
        Miss# = missive('Certain lines were not imported, review exception file. '&clip(LocalCount-1)&' records have been imported','ServiceBase 3g','mexclam.jpg','OK')
        Close(ExportFile)
    END

    Global_Long = LocalCount - ErrorCount

    exit
ImportLine         Routine

    !TB13194 - J - 24/02/14 - Method change - just read the file into the prep file
    If impfil:Field3 = ''
        LocalErrorString = 'IMEI Number not provided'
        Do ExportError
    ELSE
        Access:EXCHPREP.primerecord()
        EAP:Audit_Number = format(SentAuditNo,@n10)     !emf:Audit_number
        EAP:IMEI         = impfil:Field3
        EAP:User_Code    = glo:userCode
        EAP:Method       = 'FILE'
        Access:EXCHPREP.update()
    END !if imenumber empty

    EXIT

!
!
!oldImportLine      Routine
!
!    !this needs rewriting for the exchange audit procedure
!    !Find the stockAudit record for this
!
!    If impfil:Field3 = ''
!        LocalErrorString = 'IMEI Number not provided'
!        Do ExportError
!    ELSE
!
!        Access:EXCHAUI.Clearkey(eau:Locate_IMEI_Key)
!        eau:Audit_Number    = emf:Audit_Number
!        eau:Site_Location   = SentLocation
!        eau:IMEI_Number     = impfil:Field3
!        If Access:EXCHAUI.Tryfetch(eau:Locate_IMEI_Key) = Level:Benign
!            !Found
!            !Check to see if the IMEI exists in another location
!            !and if not, check the Loan Database - L883 (DBH: 12-09-2003)
!            Error#= 0
!            Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
!            xch_ali:ESN = upper(impfil:Field3)
!            If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
!                !Found
!                If xch_ali:Location <> SentLocation
!                    LocalErrorString = 'This unit exists in another location: '&clip(xch_ali:Location)
!                    Do ExportError
!                    Error# = 1
!                End !If xch_ali:Location <> SentLocation
!            Else !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
!                !Error
!                Access:LOAN_ALIAS.ClearKey(loa_ali:ESN_Only_Key)
!                loa_ali:ESN = impfil:Field3
!                If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
!                    !Found
!                    LocalErrorString = 'This unit exists in the Loan Database not the exchange.'
!                    Do ExportError
!                    Error# = 1
!                Else !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
!                    !Error
!                    LocalErrorString = 'This unit does not exist in the Exchange Database and must be added manually.'
!                    Do ExportError
!                    Error# = 1
!
!                End !If Access:LOAN_ALIAS.TryFetch(loa_ali:ESN_Only_Key) = Level:Benign
!            End !If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
!
!            If Error# = 0
!                IF eau:Confirmed = TRUE   ! #12276 Don't allow to scan the same IMEI more than once. (Bryan: 10/10/2011)
!                    LocalErrorString = 'This IMEI Number has already been scanned or imported.'
!                    do ExportError
!                ELSE ! IF (eau:Confirmed = TRUE)
!                    eau:Confirmed = TRUE
!                    Access:EXCHAUI.TryUpdate()
!                END ! IF (eau:Confirmed = TRUE)
!            End !If Error# = 0
!
!
!        Else! If Access:EXCHAUI.Tryfetch(eau:AuditIMEIKey) = Level:Benign
!            !Error
!            LocalErrorString = 'This IMEI is not recognized in the exchange or loan databases. Add manually as an EXCESS if required.'
!            Do ExportError
!
!        End! If Access:EXCHAUI.Tryfetch(eau:AuditIMEIKey) = Level:Benign
!
!    END !if field2 exists
ExportError         Routine

    ErrorCount += 1

    if ExportFilename = '' then
        !not been generated yet
        ExportFilename = GETINI('EXCEPTION','AUDITIMPORT',,CLIP(Path()) & '\SB2KDEF.INI')  |
                         & '\ExchangeAudit'&'_'&format(today(),@D12)&'-'&left(clip(format(SentAuditNo,@n_9)))&'.csv'
                        !StockAudit_Date_AuditNumber.csv,

        if exists(ExportFilename) then remove(exportFilename).

        Create(ExportFile)
        Open(ExportFile)

        !first line
        !expfil:LineNo = 'Line No'
        expfil:siteLocation = 'Location'
        expfil:AuditNumber  = 'Audit Number'
        expfil:StockRef     = 'IMEI No'
        expfil:ErrorString  = 'Reason'
        Add(ExportFile)

    END !if exportfilename is still blank

    !expfil:LineNo = LocalCount
    expfil:SiteLocation = SentLocation
    expfil:AuditNumber  = format(SentAuditNo,@n10)
    expfil:StockRef     = impfil:Field3     !imei number
    expfil:ErrorString  = LocalErrorString
    Add(ExportFile)

    exit

!ExportFile    File,Driver('BASIC'),Pre(expfil),Name(ExportFilename),Create,Bindable,Thread
!Record        Record
!SiteLocation   String(30)
!AuditNumber    String(10)
!StockRef       String(30)
!ErrorString    String(100)
!END
!END

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020789'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ImportExcAuditFile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHAUI.Open
  Relate:EXCHPREP.Open
  Relate:GENSHORT.Open
  Relate:LOAN_ALIAS.Open
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !reset the stored variables to they have to pick/create new ones
  LocalFilename = ''
  ExportFilename = ''
      ! Save Window Name
   AddToLog('Window','Open','ImportExcAuditFile')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('CSV Files','*.CSV')
  FileLookup9.WindowTitle='Audit File'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHAUI.Close
    Relate:EXCHPREP.Close
    Relate:GENSHORT.Close
    Relate:LOAN_ALIAS.Close
    Relate:STOAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ImportExcAuditFile')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonImport
      Do TheImport
    OF ?ButtonCancel
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020789'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020789'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020789'&'0')
      ***
    OF ?LookupFile
      ThisWindow.Update
      LocalFilename = FileLookup9.Ask(1)
      DISPLAY
    OF ?ButtonImport
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
