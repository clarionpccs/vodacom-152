

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01008.INC'),ONCE        !Local module procedure declarations
                     END


DeleteChargeablePart PROCEDURE  (func:Type)           ! Declare Procedure
save_orp_id          USHORT,AUTO
save_ori_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
save_orw_id          USHORT,AUTO
tmp:Delete           BYTE(0)
tmp:Scrap            BYTE(0)
tmp:StockNumber      LONG
tmp:AttachBySolder   BYTE(0)
locAuditNotes        STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
Local                CLASS
CreateStockHistory   PROCEDURE(String func:Information)
RemoveExistingWebOrder PROCEDURE (),Byte
                     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    tmp:Delete  = 0
    tmp:Scrap   = 0
    If par:WebOrder
        Case Missive('Warning! A RETAIL Order has been raised for this part.'&|
          '<13,10>'&|
          '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
        Of 1 ! No Button
        Of 2 ! Yes Button
                If Local.RemoveExistingWebOrder() = Level:Benign
                    Case Missive('Cannot find a pending RETAIL order for this part.'&|
                      '<13,10>'&|
                      '<13,10>This usually means that the order has already been raised.'&|
                      '<13,10>Do you still wish to delete this part?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                    Of 1 ! No Button
                    Of 2 ! Yes Button
                        tmp:Delete = 1

                    End ! Case Missive
                Else !End !If RemoveExistingWebOrder()
                    tmp:Delete = 1
                End !If RemoveExistingWebOrder()
        End ! Case Missive
        If tmp:Delete = 1
            RemoveFromStockAllocation(par:Record_Number,'CHA')
        End !If tmp:Delete = 1
    Else !If par:WebOrder

        If par:Adjustment = 'YES'
            If par:Part_Number <> 'EXCH'
                Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                Of 1 ! No Button
                Of 2 ! Yes Button
                        tmp:Delete = 1
                End ! Case Missive
            Else !If par:Part_Number <> 'EXCH'
                tmp:Delete = 1
            End !If par:Part_Number <> 'EXCH'
        Else !If par:Adjustment = 'YES'
            tmp:StockNumber = 0
            tmp:AttachBySolder = 0

            If par:Part_Ref_Number <> ''
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = par:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    tmp:StockNumber = sto:Ref_Number
                    tmp:AttachBySolder  = sto:AttachBySolder
                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            Else !If par:Part_Ref_Number <>

            End !If par:Part_Ref_Number <>

            !Just delete if the part is a sundry - 3870 (DBH: 05-02-2004)
            If tmp:StockNumber And sto:Sundry_Item = 'YES'
                Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                Of 1 ! No Button
                Of 2 ! Yes Button
                        tmp:Delete = 1
                End ! Case Missive
            Else !If tmp:StockNumber And sto:Sundry_Item = 'YES'
                If par:Order_Number <> ''
                    If par:Date_Received <> ''
                        If tmp:StockNumber and sto:Sundry_Item = 'YES'
                            tmp:Delete = 1
                        Else !If tmp:StockNumber and sto:Sundry_Item = 'YES'
                            If func:Type = 0 And RapidLocation(sto:Location)
                                If tmp:AttachBySolder
                                    Case Missive('The selected part is "Attached By Solder" and therefore will be scrapped.','ServiceBase 3g',|
                                                   'mquest.jpg','\No|/Scrap')

                                    Of 1 ! No Button
                                        Return 0
                                    Of 2 ! Scrap Button
                                        tmp:Delete  = 1
                                        tmp:Scrap   = 1
                                        Do AddSolder
                                    End ! Case Missive
                                Else !If tmp:AttachBySolder
                                    If par:PartAllocated = 0
                                        Case Missive('This part has not yet been allocated.'&|
                                          '<13,10>'&|
                                          '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                                                       'mquest.jpg','\No|/Yes')
                                        Of 1 ! No Button
                                        Of 2 ! Yes Button
                                               tmp:Delete = 1
                                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                                sto:Ref_Number  = tmp:StockNumber
                                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Found
                                                    sto:Quantity_Stock += par:Quantity

                                                    If Access:STOCK.Update() = Level:Benign
                                                        Local.CreateStockHistory('UNALLOCATED PART REMOVED')
                                                        tmp:Delete = 1
                                                        ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                        if (sto:Suspend)
                                                            sto:Suspend = 0
                                                            If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                 'ADD', | ! Transaction_Type
                                                                                 '', | ! Depatch_Note_Number
                                                                                 0, | ! Job_Number
                                                                                 0, | ! Sales_Number
                                                                                 0, | ! Quantity
                                                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                                                 par:Sale_Cost, | ! Sale_Cost
                                                                                 par:Retail_Cost, | ! Retail_Cost
                                                                                 'PART UNSUSPENDED', | ! Notes
                                                                                 '') ! Information
                                                                End ! AddToStockHistory
                                                            End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                        end
                                                    End !If Access:STOCK.Update() = Level:Benign
                                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Error
                                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                        End ! Case Missive
                                    Else !If par:PartAllocated = 0
                                        Case Missive('Are you sure you want to mark this part to be returned by allocations?','ServiceBase 3g',|
                                                       'mquest.jpg','\No|/Yes')
                                        Of 1 ! No Button
                                        Of 2 ! Yes Button
                                            par:PartAllocated = 0
                                            par:Status  = 'RET'
                                            Access:PARTS.Update()
                                            AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'RET',job:Engineer)
                                            Return 0
                                        End ! Case Missive
                                    End !If par:PartAllocated = 0
                                End !If tmp:AttachBySolder
                            Else !If func:Type = 0 And RapidLocation(sto:Location)
                                Case Missive('This part exists on Order Number ' & Clip(par:Order_Number) & ' and has been RECEIVED.'&|
                                  '<13,10>'&|
                                  '<13,10>Do you wish to RETSTOCK this part, or SCRAP it?','ServiceBase 3g',|
                                               'mquest.jpg','\Cancel|Scrap|Restock')
                                Of 1 ! Cancel Button
                                Of 2 ! Scrap Button
                                        tmp:Delete  = 1
                                        tmp:Scrap   = 1
                                Of 3 ! Restock Button
                                        If tmp:StockNumber
                                            Case Missive('This item was originally taken from location ' & Clip(sto:Location) & '.'&|
                                              '<13,10>'&|
                                              '<13,10>Do you wish to return it to it''s ORIGINAL location, or to a NEW location?','ServiceBase 3g',|
                                                           'mquest.jpg','\Cancel|New|Original')
                                            Of 1 ! Cancel Button
                                            Of 2 ! New Button
                                                glo:Select1 = ''
                                                Pick_New_Location
                                                If glo:Select1 <> ''
                                                    Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                                    sto:Location    = glo:Select1
                                                    sto:Part_Number = par:Part_Number
                                                    sto:Description = par:Description
                                                    If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                        !Found
                                                        sto:Quantity_Stock += par:Quantity
                                                        If Access:STOCK.Update() = Level:Benign
                                                            Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                                            tmp:Delete = 1
                                                            ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                            if (sto:Suspend)
                                                                sto:Suspend = 0
                                                                If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                     'ADD', | ! Transaction_Type
                                                                                     '', | ! Depatch_Note_Number
                                                                                     0, | ! Job_Number
                                                                                     0, | ! Sales_Number
                                                                                     0, | ! Quantity
                                                                                     par:Purchase_Cost, | ! Purchase_Cost
                                                                                     par:Sale_Cost, | ! Sale_Cost
                                                                                     par:Retail_Cost, | ! Retail_Cost
                                                                                     'PART UNSUSPENDED', | ! Notes
                                                                                     '') ! Information
                                                                    End ! AddToStockHistory
                                                                End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                            end

                                                        End !If Access:STOCK.Update() = Level:Benign
                                                    Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                        !Error
                                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                                        Case Missive('Cannot find the selected part in location ' & (glo:Select1) & '.'&|
                                                          '<13,10>'&|
                                                          '<13,10>Do you wish to add it as a NEW Item, or SCRAP it?','ServiceBase 3g',|
                                                                       'mquest.jpg','\Cancel|Scrap|New')
                                                        Of 1 ! Cancel Button
                                                        Of 2 ! Scrap Button
                                                            tmp:Delete  = 1
                                                            tmp:Scrap   = 1
                                                        Of 3 ! New Button
                                                            glo:Select2 = ''
                                                            glo:Select3 = ''
                                                            Pick_Locations
                                                            If glo:Select1 <> ''
                                                                If Access:STOCK.PrimeRecord() = Level:Benign
                                                                    sto:Part_Number     = par:Part_Number
                                                                    sto:Description     = par:Description
                                                                    sto:Supplier        = par:Supplier
                                                                    sto:Purchase_Cost   = par:Purchase_Cost
                                                                    sto:Sale_Cost       = par:Sale_Cost
                                                                    sto:Retail_Cost     = par:Retail_Cost
                                                                    sto:Shelf_Location  = glo:Select2
                                                                    sto:Manufacturer    = job:Manufacturer
                                                                    sto:Location        = glo:Select1
                                                                    sto:Second_Location = glo:Select3
                                                                    If Access:STOCK.TryInsert() = Level:Benign
                                                                        !Insert Successful
                                                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                                                        tmp:Delete = 1
                                                                    Else !If Access:STOCK.TryInsert() = Level:Benign
                                                                        !Insert Failed
                                                                    End !If Access:STOCK.TryInsert() = Level:Benign
                                                                End !If Access:STOCK.PrimeRecord() = Level:Benign

                                                            End !If glo:Select1 <> ''

                                                        End ! Case Missive
                                                    End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign

                                                End !If glo:Select1 <> ''

                                            Of 3 ! Original Button
                                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                                sto:Ref_Number  = tmp:StockNumber
                                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Found
                                                    sto:Quantity_Stock += par:Quantity
                                                    If Access:STOCK.Update() = Level:Benign
                                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                                        tmp:Delete = 1
                                                        ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                        if (sto:Suspend)
                                                            sto:Suspend = 0
                                                            If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                 'ADD', | ! Transaction_Type
                                                                                 '', | ! Depatch_Note_Number
                                                                                 0, | ! Job_Number
                                                                                 0, | ! Sales_Number
                                                                                 0, | ! Quantity
                                                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                                                 par:Sale_Cost, | ! Sale_Cost
                                                                                 par:Retail_Cost, | ! Retail_Cost
                                                                                 'PART UNSUSPENDED', | ! Notes
                                                                                 '') ! Information
                                                                End ! AddToStockHistory
                                                            End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                        end

                                                    End !If Access:STOCK.Update() = Level:Benign
                                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Error
                                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                            End ! Case Missive
                                        Else !If tmp:StockNumber
                                            glo:Select1 = ''
                                            glo:Select2 = ''
                                            glo:Select3 = ''
                                            Pick_Locations
                                            If glo:Select1 <> ''
                                                If Access:STOCK.PrimeRecord() = Level:Benign
                                                    sto:Part_Number     = par:Part_Number
                                                    sto:Description     = par:Description
                                                    sto:Supplier        = par:Supplier
                                                    sto:Purchase_Cost   = par:Purchase_Cost
                                                    sto:Sale_Cost       = par:Sale_Cost
                                                    sto:Retail_Cost     = par:Retail_Cost
                                                    sto:Shelf_Location  = glo:Select2
                                                    sto:Manufacturer    = job:Manufacturer
                                                    sto:Location        = glo:Select1
                                                    sto:Second_Location = glo:Select3
                                                    If Access:STOCK.TryInsert() = Level:Benign
                                                        !Insert Successful
                                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & par:order_number)
                                                        tmp:Delete = 1
                                                    Else !If Access:STOCK.TryInsert() = Level:Benign
                                                        !Insert Failed
                                                    End !If Access:STOCK.TryInsert() = Level:Benign
                                                End !If Access:STOCK.PrimeRecord() = Level:Benign
                                            End !If glo:Select1 <> ''
                                        End !If tmp:StockNumber

                                End ! Case Missive
                            End !If func:Type = 0 And RapidLocation(sto:Location)

                        End !If tmp:StockNumber and sto:Sundry_Item = 'YES'
                    Else !If par:Date_Recevied <> ''
                        Case Missive('The selected part is on Order Number ' & Clip(par:Order_Number) & ' but has NOT been received.'&|
                          '<13,10>If you choose to delete it, the part on the order will be marked as RECEIVED.'&|
                          '<13,10>Are you sure you want to delete this part?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                        Of 1 ! No Button
                        Of 2 ! Yes Button
                            Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
                            orp:Record_Number   = par:Order_Part_Number
                            If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                                !Found
                                orp:Quantity    = 0
                                orp:Date_Received   = Today()
                                orp:Number_Received = 0
                                orp:All_Received    = 'YES'
                                If Access:ORDPARTS.Update() = Level:Benign
                                    !Check if all received
                                    found# = 0
                                    Setcursor(cursor:wait)
                                    save_orp_id = access:ordparts.savefile()
                                    access:ordparts.clearkey(orp:order_Number_key)
                                    orp:order_number    = par:order_number
                                    Set(orp:order_number_key,orp:order_number_key)
                                    Loop
                                        If access:ordparts.next()
                                            Break
                                        End!If access:ordparts.next()
                                        If orp:order_Number <> par:order_Number
                                            Break
                                        End!If orp:order_Number <> par:order_Number
                                        If orp:all_received <> 'YES'
                                            found# = 1
                                            Break
                                        End!If orp:all_received <> 'YES'
                                    End!Loop
                                    access:ordparts.restorefile(save_orp_id)
                                    setcursor()

                                    If found# = 0
                                        access:orders.clearkey(ord:order_Number_key)
                                        ord:order_number    = par:order_number
                                        If access:orders.tryfetch(ord:order_number_key) = Level:Benign
                                            ord:all_received = 'YES'
                                            access:orders.update()
                                        End!If access:orders.tryfetch(ord:order_number_key) = Level:Benign
                                    End!If found# = 0
                                    tmp:Delete = 1
                                End !If Access:ORDPARTS.Update() = Level:Benign
                            Else! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign

                        End ! Case Missive
                    End !If par:Date_Recevied <> ''
                Else  !If par:Order_Number <> ''
                    If par:Pending_Ref_Number <> ''
                        Case Missive('This part has been marked to appear on the NEXT Parts Order Generation.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                        Of 1 ! No Button
                        Of 2 ! Yes Button
                            Case def:SummaryOrders
                                Of 0!Old Way
                                    Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                                    ope:Ref_Number  = par:Pending_Ref_Number
                                    If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                        !Found
                                        Delete(ORDPEND)
                                    Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                Of 1!New Way
                                    Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                    ope:Supplier    = par:Supplier
                                    ope:Part_Number = par:Part_Number
                                    If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                        !Found
                                        ope:Quantity -= par:Quantity
                                        If ope:Quantity <=0
                                            Delete(ORDPEND)
                                        Else !If ope:Quantity <=0
                                            Access:ORDPEND.Update()
                                        End !If ope:Quantity <=0
                                    Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign

                            End !Case def:SummaryOrders
                            tmp:Delete = 1
                        End ! Case Missive
                    Else !If par:Pending_Ref_Number <> ''
                        If func:Type = 0 And RapidLocation(sto:Location)
                            If tmp:AttachBySolder
                                Case Missive('The selected part is "Attached By Solder" and therefore will be scrapped.','ServiceBase 3g',|
                                               'mquest.jpg','\No|/Scrap')
                                Of 1 ! No Button
                                    Return 0

                                Of 2 ! Scrap Button
                                    tmp:Delete  = 1
                                    tmp:Scrap   = 1
                                    Do AddSolder
                                End ! Case Missive
                            Else !If tmp:AttachBySolder
                                If par:PartAllocated = 0
                                    Case Missive('This part has not yet been allocated.'&|
                                      '<13,10>'&|
                                      '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                                                   'mquest.jpg','\No|/Yes')
                                    Of 1 ! No Button
                                    Of 2 ! Yes Button
                                        tmp:Delete = 1
                                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                        sto:Ref_Number  = tmp:StockNumber
                                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                            !Found
                                            sto:Quantity_Stock += par:Quantity
                                            If Access:STOCK.Update() = Level:Benign
                                                Local.CreateStockHistory('UNALLOCATED PART REMOVED')
                                                tmp:Delete = 1
                                                ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                if (sto:Suspend)
                                                    sto:Suspend = 0
                                                    If (Access:STOCK.TryUpdate() = Level:Benign)
                                                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                         'ADD', | ! Transaction_Type
                                                                         '', | ! Depatch_Note_Number
                                                                         0, | ! Job_Number
                                                                         0, | ! Sales_Number
                                                                         0, | ! Quantity
                                                                         par:Purchase_Cost, | ! Purchase_Cost
                                                                         par:Sale_Cost, | ! Sale_Cost
                                                                         par:Retail_Cost, | ! Retail_Cost
                                                                         'PART UNSUSPENDED', | ! Notes
                                                                         '') ! Information
                                                        End ! AddToStockHistory
                                                    End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                end

                                            End !If Access:STOCK.Update() = Level:Benign
                                        Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                            !Error
                                            !Assert(0,'<13,10>Fetch Error<13,10>')
                                        End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                    End ! Case Missive
                                Else !If par:PartAllocated = 0
                                    Case Missive('Are you sure you want to mark this part to be returned by allocations?','ServiceBase 3g',|
                                                   'mquest.jpg','\No|/Yes')
                                    Of 1 ! No Button
                                    Of 2 ! Yes Button
                                        par:PartAllocated = 0
                                        par:Status  = 'RET'
                                        Access:PARTS.Update()
                                        AddToStockAllocation(par:Record_Number,'CHA',par:Quantity,'RET',job:Engineer)
                                        Return 0

                                    End ! Case Missive
                                End !If par:PartAllocated = 0
                            End !If tmp:AttachBySolder

                        Else !If func:Type = 0 And RapidLocation(sto:Location)
                            Case Missive('You have selected to delete this part.'&|
                              '<13,10>'&|
                              '<13,10>Do you wish to RESTOCK it, or SCRAP it?','ServiceBase 3g',|
                                           'mquest.jpg','\Cancel|Scrap|Restock')
                            Of 1 ! Cancel Button
                            Of 2 ! Scrap Button
                                tmp:Delete  = 1
                                tmp:Scrap   = 1
                            Of 3 ! Restock Button
                                If tmp:StockNumber
                                    Case Missive('The selected part was originally taken from location ' & Clip(sto:Location) & '.'&|
                                      '<13,10>'&|
                                      '<13,10>Do you wish to return it to it''s ORIGINAL location, or to a NEW location?','ServiceBase 3g',|
                                                   'mquest.jpg','\Cancel|New|Original')
                                    Of 1 ! Cancel Button
                                    Of 2 ! New Button
                                        glo:Select1 = ''
                                        Pick_New_Location
                                        If glo:Select1 <> ''
                                            Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                            sto:Location    = glo:Select1
                                            sto:Part_Number = par:Part_Number
                                            sto:Description = par:Description
                                            If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                !Found
                                                sto:Quantity_Stock += par:Quantity
                                                If Access:STOCK.Update() = Level:Benign
                                                    Local.CreateStockHistory('')
                                                    tmp:Delete = 1
                                                    ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                    if (sto:Suspend)
                                                        sto:Suspend = 0
                                                        If (Access:STOCK.TryUpdate() = Level:Benign)
                                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                             'ADD', | ! Transaction_Type
                                                                             '', | ! Depatch_Note_Number
                                                                             0, | ! Job_Number
                                                                             0, | ! Sales_Number
                                                                             0, | ! Quantity
                                                                             par:Purchase_Cost, | ! Purchase_Cost
                                                                             par:Sale_Cost, | ! Sale_Cost
                                                                             par:Retail_Cost, | ! Retail_Cost
                                                                             'PART UNSUSPENDED', | ! Notes
                                                                             '') ! Information
                                                            End ! AddToStockHistory
                                                        End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                    end

                                                End !If Access:STOCK.Update() = Level:Benign
                                            Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                !Error
                                                Case Missive('Cannot find the selected part in location ' & Clip(glo:Select1) & '.'&|
                                                  '<13,10>'&|
                                                  '<13,10>Do you wish to add it as a NEW part in this location, or SCRAP it?','ServiceBase 3g',|
                                                               'mquest.jpg','\Cancel|Scrap|New')
                                                Of 1 ! Cancel Button
                                                Of 2 ! Scrap Button
                                                    tmp:Delete  = 1
                                                    tmp:Scrap   = 1
                                                Of 3 ! New Button
                                                    glo:Select2 = ''
                                                    glo:Select3 = ''
                                                    Pick_Locations
                                                    If glo:Select1 <> ''
                                                        If Access:STOCK.PrimeRecord() = Level:Benign
                                                            sto:Part_Number     = par:Part_Number
                                                            sto:Description     = par:Description
                                                            sto:Supplier        = par:Supplier
                                                            sto:Purchase_Cost   = par:Purchase_Cost
                                                            sto:Sale_Cost       = par:Sale_Cost
                                                            sto:Retail_Cost     = par:Retail_Cost
                                                            sto:Shelf_Location  = glo:select2
                                                            sto:Manufacturer    = job:Manufacturer
                                                            sto:Location        = glo:Select1
                                                            sto:Second_Location = glo:Select3
                                                            If Access:STOCK.TryInsert() = Level:Benign
                                                                !Insert Successful
                                                                Local.CreateStockHistory('')
                                                                tmp:Delete = 1
                                                            Else !If Access:STOCK.TryInsert() = Level:Benign
                                                                !Insert Failed
                                                            End !If Access:STOCK.TryInsert() = Level:Benign
                                                        End !If Access:STOCK.PrimeRecord() = Level:Benign
                                                    End !If glo:Select1 <> ''

                                                End ! Case Missive
                                            End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign

                                        End !If glo:Select1 <> ''

                                    Of 3 ! Original Button
                                        Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                        sto:Ref_Number  = tmp:StockNumber
                                        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                            !Found
                                            sto:Quantity_stock += par:Quantity
                                            If Access:STOCK.Update() = Level:Benign
                                                Local.CreateStockHistory('')
                                                tmp:Delete = 1
                                                ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                if (sto:Suspend)
                                                    sto:Suspend = 0
                                                    If (Access:STOCK.TryUpdate() = Level:Benign)
                                                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                         'ADD', | ! Transaction_Type
                                                                         '', | ! Depatch_Note_Number
                                                                         0, | ! Job_Number
                                                                         0, | ! Sales_Number
                                                                         0, | ! Quantity
                                                                         par:Purchase_Cost, | ! Purchase_Cost
                                                                         par:Sale_Cost, | ! Sale_Cost
                                                                         par:Retail_Cost, | ! Retail_Cost
                                                                         'PART UNSUSPENDED', | ! Notes
                                                                         '') ! Information
                                                        End ! AddToStockHistory
                                                    End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                end

                                            End !If Access:STOCK.Update() = Level:Benign
                                        Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                            !Error
                                            !Assert(0,'<13,10>Fetch Error<13,10>')
                                        End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                    End ! Case Missive

                                Else !If tmp:StockNumber
                                    glo:Select1 = ''
                                    glo:Select2 = ''
                                    glo:Select3 = ''
                                    Pick_Locations
                                    If glo:Select1 <> ''
                                        If Access:STOCK.PrimeRecord() = Level:Benign
                                            sto:Part_Number     = par:Part_Number
                                            sto:Description     = par:Description
                                            sto:Supplier        = par:Supplier
                                            sto:Purchase_Cost   = par:Purchase_Cost
                                            sto:Sale_Cost       = par:Sale_Cost
                                            sto:Retail_Cost     = par:Retail_Cost
                                            sto:Shelf_Location  = glo:Select2
                                            sto:Manufacturer    = job:Manufacturer
                                            sto:Location        = glo:Select1
                                            sto:Second_Location = glo:Select3
                                            If Access:STOCK.TryInsert() = Level:Benign
                                                !Insert Successful
                                                Local.CreateStockHistory('')
                                                tmp:Delete = 1
                                            Else !If Access:STOCK.TryInsert() = Level:Benign
                                                !Insert Failed
                                            End !If Access:STOCK.TryInsert() = Level:Benign
                                        End !If Access:STOCK.PrimeRecord() = Level:Benign
                                    End !If glo:Select1 <> ''
                                End !If tmp:StockNumber

                            End ! Case Missive

                        End !If func:Type = 0 And RapidLocation(sto:Location)

                    End !If par:Pending_Ref_Number <> ''
                End !If par:Order_Number <> ''

            End !If tmp:StockNumber And sto:Sundry_Item = 'YES'

        End !If par:Adjustment = 'YES'
        If tmp:Scrap
            !Remove part from stock allocation tabe - L873 (DBH: 24-07-2003)
            RemoveFromStockAllocation(par:Record_Number,'CHA')

            locAuditNotes = 'SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number)
            
            IF (tmp:StockNumber > 0)
                ! #11368 Add Extra Scrap Information (DBH: 11/06/2010)
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number = tmp:StockNumber
                If (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                    locAuditNotes = clip(locAuditNotes) & '<13,10>LOCATION: ' & clip(sto:Location) & |
                                                    '<13,10>QTY: ' & par:Quantity
                End    
            END ! IF
            
            IF (AddToAudit(job:Ref_Number,'JOB','SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number),CLIP(locAuditNotes)))
            END ! IF
            
            ! #11368 Add Scrap information to Stock History (DBH: 11/06/2010)
            if (tmp:StockNumber > 0)
                If AddToStockHistory(sto:Ref_Number,'ADD','',par:Ref_Number,0,0,sto:Purchase_Cost,sto:Sale_Cost,sto:Retail_Cost,|
                          'PART SCRAPPED',|
                          'TYPE: CHARGEABLE' & |
                          '<13,10>QUANTITY: ' & par:Quantity)
                End ! '<13,10>NEW PART: ' & Clip(local:NewPartNumber) & ' - ' & Clip(local:NewDescription))
            end


        Else
            If tmp:Delete
                !Delete part from stock allocation table -  (DBH: 23-07-2003)
                RemoveFromStockAllocation(par:Record_Number,'CHA')

                IF (AddToAudit(job:Ref_Number,'JOB','CHARGEABLE PART DELETED: ' & Clip(par:Part_Number),'DESCRIPTION: ' & Clip(par:Description)))
                END ! IF

            End !If tmp:Delete
        End !If Scrap# = 1
    End !If par:WebOrder
    Return tmp:Delete
AddSolder       Routine
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        stf:PartNumber  = par:Part_Number
        stf:Description = par:Description
        stf:Quantity    = par:Quantity
        stf:PurchaseCost    = par:Purchase_Cost
        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Found

        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        stf:StoreUserCode   = use:User_Code
        stf:PartType        = 'SOL'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign



Local.CreateStockHistory          Procedure(String    func:Information)
Code
    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                         'ADD', | ! Transaction_Type
                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                         job:Ref_Number, | ! Job_Number
                         0, | ! Sales_Number
                         par:Quantity, | ! Quantity
                         par:Purchase_Cost, | ! Purchase_Cost
                         par:Sale_Cost, | ! Sale_Cost
                         par:Retail_Cost, | ! Retail_Cost
                         func:Information, | ! Notes
                         'CHARGEABLE PART REMOVED FROM JOB') ! Information
        ! Added OK
        tmp:Delete = 1
    Else ! AddToStockHistory
        ! Error
    End ! AddToStockHistory
Local.RemoveExistingWebOrder        Procedure()
tmp:FoundOrder      Byte(0)
tmp:FoundPart       Byte(0)
tmp:WebAccountNumber    String(30)
Code
    !Look for un processed order
    If glo:WebJob
        tmp:WebAccountNumber   = ClarioNET:Global.Param2
    Else !If glo:WebJob
        tmp:WebAccountNumber   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
    End !If glo:WebJob

    Save_orw_ID = Access:ORDWEBPR.SaveFile()
    Access:ORDWEBPR.ClearKey(orw:PartNumberKey)
    orw:AccountNumber   = tmp:WebAccountNumber
    orw:PartNumber    = par:Part_Number
    Set(orw:PartNumberKey,orw:PartNumberKey)
    Loop
        If Access:ORDWEBPR.NEXT()
           Break
        End !If
        If orw:AccountNumber <> tmp:WebAccountNumber      |
        Or orw:PartNumber    <> par:Part_Number      |
            Then Break.  ! End If
        If orw:Description  = par:Description
            tmp:FoundOrder = 1
            orw:Quantity    -= par:Quantity
            If orw:Quantity <= 0
                Delete(ORDWEBPR)
                Break
            End !If orw:Quantity <= 0
        End !If orw:Description  = par:Description

    End !Loop
    Access:ORDWEBPR.RestoreFile(Save_orw_ID)

!    Save_orh_ID = Access:ORDHEAD.SaveFile()
!    Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
!    orh:procesed    = 0
!    Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
!    Loop
!        If Access:ORDHEAD.NEXT()
!           Break
!        End !If
!        If orh:procesed    <> 0      |
!            Then Break.  ! End If
!        If orh:Account_No   = job:Account_Number
!            Save_ori_ID = Access:ORDITEMS.SaveFile()
!            Access:ORDITEMS.ClearKey(ori:Keyordhno)
!            ori:ordhno = orh:Order_No
!            Set(ori:Keyordhno,ori:Keyordhno)
!            Loop
!                If Access:ORDITEMS.NEXT()
!                   Break
!                End !If
!                If ori:ordhno <> orh:Order_No      |
!                    Then Break.  ! End If
!
!                If ori:partno = par:Part_Number And |
!                    ori:partdiscription = par:Description
!                    tmp:FoundOrder = 1
!                    !Found a part awaiting order with same part number
!                    !and description. Decrease it's quantity
!                    ori:qty -= par:Quantity
!                    If ori:qty <= 0
!                        !If there is non left to order delete the part.
!                        Delete(ORDITEMS)
!                    End !If ori:qty = 0
!                End !ori:partdiscription = par:Description
!            End !Loop
!            Access:ORDITEMS.RestoreFile(Save_ori_ID)
!
!            !Are there still any parts on this order?
!
!            Save_ori_ID = Access:ORDITEMS.SaveFile()
!            Access:ORDITEMS.ClearKey(ori:Keyordhno)
!            ori:ordhno = orh:Order_No
!            Set(ori:Keyordhno,ori:Keyordhno)
!            Loop
!                If Access:ORDITEMS.NEXT()
!                   Break
!                End !If
!                If ori:ordhno <> orh:Order_No      |
!                    Then Break.  ! End If
!                tmp:FoundPart = 1
!                Break
!            End !Loop
!            Access:ORDITEMS.RestoreFile(Save_ori_ID)
!
!            If tmp:FoundPart = 0
!                !No more parts on the order
!                Delete(ORDHEAD)
!            End !If Found# = 0
!        End !If orh:account_no = job:Account_Number
!    End !Loop
!    Access:ORDHEAD.RestoreFile(Save_orh_ID)

    Return tmp:FoundOrder
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Pick_Locations PROCEDURE                              !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Deleting An Unknown Part'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,246),USE(?Sheet1),FONT('Tahoma',8,,,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('The original part cannot be found in Stock Control. Either this part has been de' &|
   'leted, or this item was an external part. Please fill in the information below t' &|
   'o create a new stock item for this part.'),AT(196,142,292,36),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                           PROMPT('Second Location'),AT(236,240),USE(?glo:select3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(312,240,124,10),USE(GLO:Select3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Note: Before this part can be used you will need to edit the parts details in St' &|
   'ock Control, attaching Model Numbers(s) and/or Fault Codes.'),AT(236,258,204,24),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Shelf Location'),AT(236,216),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(312,216,124,10),USE(GLO:Select2),FONT(,,,FONT:bold),COLOR(080FFFFH),REQ,UPR
                           BUTTON,AT(440,212),USE(?LookupShelfLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Site Location'),AT(236,192),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,192,124,10),USE(GLO:Select1),SKIP,FONT(,,,FONT:bold),COLOR(COLOR:Silver),UPR,READONLY
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Deleting An Unknown Part'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:GLO:Select2                Like(GLO:Select2)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020010'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Pick_Locations')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCSHELF.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_Code   = job:Engineer
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      glo:Select1 = use:Location
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Pick_Locations')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?GLO:Select2{Prop:Tip} AND ~?LookupShelfLocation{Prop:Tip}
     ?LookupShelfLocation{Prop:Tip} = 'Select ' & ?GLO:Select2{Prop:Tip}
  END
  IF ?GLO:Select2{Prop:Msg} AND ~?LookupShelfLocation{Prop:Msg}
     ?LookupShelfLocation{Prop:Msg} = 'Select ' & ?GLO:Select2{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCSHELF.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Pick_Locations')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseShelfLocations
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?GLO:Select2
      IF GLO:Select2 OR ?GLO:Select2{Prop:Req}
        los:Shelf_Location = GLO:Select2
        los:Site_Location = GLO:Select1
        !Save Lookup Field Incase Of error
        look:GLO:Select2        = GLO:Select2
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            GLO:Select2 = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            !Restore Lookup On Error
            GLO:Select2 = look:GLO:Select2
            SELECT(?GLO:Select2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupShelfLocation
      ThisWindow.Update
      los:Shelf_Location = GLO:Select2
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          GLO:Select2 = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?GLO:Select2)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?GLO:Select2)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select2 = ''
          Select(?glo:select2)
      Else!If glo:select1 = ''
          Post(Event:CloseWindow)
      End!If glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020010'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020010'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020010'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Pick_New_Location PROCEDURE                           !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Returning A Part To Stock'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Returning A Part To Stock'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT('Tahoma',8,,,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Select the Location to return this selected part to'),AT(248,188,188,12),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(284,210,124,10),USE(GLO:Select1),IMM,DISABLE,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020011'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Pick_New_Location')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  Access:USERS.Clearkey(use:User_Code_Key)
  use:User_code   = job:Engineer
  If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Found
      glo:Select1 = use:Location
      If use:StockFromLocationOnly = 0
          ?glo:Select1{prop:Disable} = 0
      End !If use:StockFromLocationOnly = 0
  Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Pick_New_Location')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB1.Init(GLO:Select1,?GLO:Select1,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(loc:Location_Key)
  FDCB1.AddField(loc:Location,FDCB1.Q.loc:Location)
  FDCB1.AddField(loc:RecordNumber,FDCB1.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Pick_New_Location')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020011'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020011'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020011'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select1 = ''
          Select(?glo:select1)
      Else!If glo:select1 = ''
          Post(Event:CloseWindow)
      End!If glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

DeleteWarrantyPart   PROCEDURE  (func:Type)           ! Declare Procedure
save_orp_id          USHORT,AUTO
save_ori_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
save_orw_id          USHORT,AUTO
tmp:Delete           BYTE(0)
tmp:Scrap            BYTE(0)
tmp:StockNumber      LONG
tmp:AttachBySolder   BYTE(0)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
Local                CLASS
CreateStockHistory   PROCEDURE(String func:Information)
RemoveExistingWebOrder PROCEDURE (),Byte
                     END
locAuditNotes STRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    tmp:Delete  = 0
    tmp:Scrap   = 0
    If wpr:WebOrder
        Case Missive('Warning! A RETAIL Order has been raised for this part.'&|
          '<13,10>'&|
          '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                If Local.RemoveExistingWebOrder() = Level:Benign
                    Case Missive('Cannot find a pending RETAIL order for this part.'&|
                      '<13,10>'&|
                      '<13,10>This usually means that the order has already been raised. Do you still wish to delete this part?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            tmp:Delete = 1
                        Of 1 ! No Button
                    End ! Case Missive
                Else !If RemoveExistingWebOrder()
                    tmp:Delete = 1
                End !If RemoveExistingWebOrder()
            Of 1 ! No Button
        End ! Case Missive
        If tmp:Delete = 1
            RemoveFromStockAllocation(wpr:Record_Number,'WAR')
        End !If tmp:Delete = 1
    Else !If wpr:WebOrder

        If wpr:Adjustment = 'YES'
            If wpr:Part_Number = 'EXCH'
                tmp:Delete = 1
            Else !If wpr:Part_Number = 'EXCH'
                Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                        tmp:Delete = 1
                    Of 1 ! No Button
                End ! Case Missive

            End !If wpr:Part_Number = 'EXCH'
        Else !If wpr:Adjustment = 'YES'
            tmp:StockNumber = 0
            tmp:AttachBySolder = 0

            If wpr:Part_Ref_Number <> ''
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = wpr:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    tmp:StockNumber = sto:Ref_Number
                    tmp:AttachBySolder = sto:AttachBySolder
                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            Else !If wpr:Part_Ref_Number <>

            End !If wpr:Part_Ref_Number <>

            !Just delete if the part is a sundry - 3870 (DBH: 05-02-2004)
            If tmp:StockNumber And sto:Sundry_Item = 'YES'
                Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                        tmp:Delete = 1
                    Of 1 ! No Button
                End ! Case Missive
            Else !If tmp:StockNumber And sto:Sundry_Item = 'YES'


                If wpr:Order_Number <> ''
                    If wpr:Date_Received <> ''
                        If tmp:StockNumber and sto:Sundry_Item = 'YES'
                            tmp:Delete = 1
                        Else !If tmp:StockNumber and sto:Sundry_Item = 'YES'
                            If func:Type = 0 And RapidLocation(sto:Location)
                                If tmp:AttachBySolder
                                    Case Missive('The selected part is "Attached By Solder" and therefore will be scrapped.','ServiceBase 3g',|
                                                   'mquest.jpg','\No|/Scrap')
                                        Of 2 ! Scrap Button
                                            tmp:Delete  = 1
                                            tmp:Scrap   = 1
                                            Do AddSolder
                                        Of 1 ! No Button
                                            Return 0
                                    End ! Case Missive
                                Else !If tmp:AttachBySolder
                                    If wpr:PartAllocated = 0
                                        Case Missive('This part has not yet been allocated. '&|
                                          '<13,10>'&|
                                          '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                                                       'mquest.jpg','\No|/Yes')
                                            Of 2 ! Yes Button
                                                tmp:Delete = 1
                                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                                sto:Ref_Number  = tmp:StockNumber
                                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Found
                                                    sto:Quantity_Stock += wpr:Quantity
                                                    If Access:STOCK.Update() = Level:Benign
                                                        Local.CreateStockHistory('UNALLOCATED PART REMOVED')
                                                        tmp:Delete = 1
                                                        ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                        if (sto:Suspend)
                                                            sto:Suspend = 0
                                                            If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                 'ADD', | ! Transaction_Type
                                                                                 '', | ! Depatch_Note_Number
                                                                                 0, | ! Job_Number
                                                                                 0, | ! Sales_Number
                                                                                 0, | ! Quantity
                                                                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                                                                 wpr:Sale_Cost, | ! Sale_Cost
                                                                                 wpr:Retail_Cost, | ! Retail_Cost
                                                                                 'PART UNSUSPENDED', | ! Notes
                                                                                 '') ! Information
                                                                End ! AddToStockHistory
                                                            End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                        end

                                                    End !If Access:STOCK.Update() = Level:Benign
                                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Error
                                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                            Of 1 ! No Button
                                        End ! Case Missive
                                    Else !If par:PartAllocated = 0
                                        Case Missive('Are you sure you want to mark this part to be returned by allocations?','ServiceBase 3g',|
                                                       'mquest.jpg','\No|/Yes')
                                            Of 2 ! Yes Button
                                                wpr:PartAllocated = 0
                                                wpr:Status  = 'RET'
                                                Access:WARPARTS.Update()
                                                AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'RET',job:Engineer)
                                                Return 0
                                            Of 1 ! No Button
                                        End ! Case Missive
                                    End !If par:PartAllocated = 0
                                End !If tmp:AttachBySolder

                            Else !If func:Type = 0

                                Case Missive('This part exists on Order Number ' & clip(wpr:Order_Number) & ' and has been RECEIVED.'&|
                                  '<13,10>'&|
                                  '<13,10>Do you wish to RESTOCK this part, or SCRAP it?','ServiceBase 3g',|
                                               'mquest.jpg','\Cancel|Scrap|Restock')
                                    Of 3 ! Restock Button
                                        If tmp:StockNumber
                                            Case Missive('This item was originally taken from location ' & clip(sto:Location) & '.'&|
                                              '<13,10>'&|
                                              '<13,10>Do you wish to return it to it''s ORIGINAL location, or to a NEW location?','ServiceBase 3g',|
                                                           'mquest.jpg','\Cancel|New|Original')
                                                Of 3 ! Original Button
                                                    Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                                    sto:Ref_Number  = tmp:StockNumber
                                                    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                        !Found
                                                        sto:Quantity_Stock += wpr:Quantity
                                                        If Access:STOCK.Update() = Level:Benign
                                                            Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & wpr:order_number)
                                                            tmp:Delete = 1
                                                            ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                            if (sto:Suspend)
                                                                sto:Suspend = 0
                                                                If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                     'ADD', | ! Transaction_Type
                                                                                     '', | ! Depatch_Note_Number
                                                                                     0, | ! Job_Number
                                                                                     0, | ! Sales_Number
                                                                                     0, | ! Quantity
                                                                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                                                                     wpr:Sale_Cost, | ! Sale_Cost
                                                                                     wpr:Retail_Cost, | ! Retail_Cost
                                                                                     'PART UNSUSPENDED', | ! Notes
                                                                                     '') ! Information
                                                                    End ! AddToStockHistory
                                                                End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                            end

                                                        End !If Access:STOCK.Update() = Level:Benign
                                                    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                        !Error
                                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                                    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                                Of 2 ! New Button
                                                    glo:Select1 = ''
                                                    Pick_New_Location
                                                    If glo:Select1 <> ''
                                                        Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                                        sto:Location    = glo:Select1
                                                        sto:Part_Number = wpr:Part_Number
                                                        sto:Description = wpr:Description
                                                        If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                            !Found
                                                            sto:Quantity_Stock += wpr:Quantity
                                                            If Access:STOCK.Update() = Level:Benign
                                                                Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & wpr:order_number)
                                                                tmp:Delete = 1
                                                                ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                                if (sto:Suspend)
                                                                    sto:Suspend = 0
                                                                    If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                         'ADD', | ! Transaction_Type
                                                                                         '', | ! Depatch_Note_Number
                                                                                         0, | ! Job_Number
                                                                                         0, | ! Sales_Number
                                                                                         0, | ! Quantity
                                                                                         wpr:Purchase_Cost, | ! Purchase_Cost
                                                                                         wpr:Sale_Cost, | ! Sale_Cost
                                                                                         wpr:Retail_Cost, | ! Retail_Cost
                                                                                         'PART UNSUSPENDED', | ! Notes
                                                                                         '') ! Information
                                                                        End ! AddToStockHistory
                                                                    End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                end

                                                            End !If Access:STOCK.Update() = Level:Benign
                                                        Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                            !Error
                                                            Case Missive('Cannot find the selected part in location ' & Clip(glo:Select1) & '.'&|
                                                              '<13,10>'&|
                                                              '<13,10>Do you wish to add it as a NEW item, or SCRAP it?','ServiceBase 3g',|
                                                                           'mquest.jpg','\Cancel|Scrap|New')
                                                                Of 3 ! New Button
                                                                    glo:Select2 = ''
                                                                    glo:Select3 = ''
                                                                    Pick_Locations
                                                                    If glo:Select1 <> ''
                                                                        If Access:STOCK.PrimeRecord() = Level:Benign
                                                                            sto:Part_Number     = wpr:Part_Number
                                                                            sto:Description     = wpr:Description
                                                                            sto:Supplier        = wpr:Supplier
                                                                            sto:Purchase_Cost   = wpr:Purchase_Cost
                                                                            sto:Sale_Cost       = wpr:Sale_Cost
                                                                            sto:Retail_cost     = wpr:Retail_Cost
                                                                            sto:Shelf_Location  = glo:Select2
                                                                            sto:Manufacturer    = job:Manufacturer
                                                                            sto:Location        = glo:Select1
                                                                            sto:Second_Location = glo:Select3
                                                                            If Access:STOCK.TryInsert() = Level:Benign
                                                                                !Insert Successful
                                                                                Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & wpr:order_number)
                                                                                tmp:Delete = 1
                                                                            Else !If Access:STOCK.TryInsert() = Level:Benign
                                                                                !Insert Failed
                                                                            End !If Access:STOCK.TryInsert() = Level:Benign
                                                                        End !If Access:STOCK.PrimeRecord() = Level:Benign

                                                                    End !If glo:Select1 <> ''
                                                                Of 2 ! Scrap Button
                                                                    tmp:Delete  = 1
                                                                    tmp:Scrap   = 1
                                                                Of 1 ! Cancel Button
                                                            End ! Case Missive
                                                        End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign

                                                    End !If glo:Select1 <> ''
                                                Of 1 ! Cancel Button
                                            End ! Case Missive
                                        Else !If tmp:StockNumber
                                            glo:Select1 = ''
                                            glo:Select2 = ''
                                            glo:Select3 = ''
                                            Pick_Locations
                                            If glo:Select1 <> ''
                                                If Access:STOCK.PrimeRecord() = Level:Benign
                                                    sto:Part_Number     = wpr:Part_Number
                                                    sto:Description     = wpr:Description
                                                    sto:Supplier        = wpr:Supplier
                                                    sto:Purchase_Cost   = wpr:Purchase_Cost
                                                    sto:Sale_Cost       = wpr:Sale_Cost
                                                    sto:Retail_Cost     = wpr:Retail_Cost
                                                    sto:Shelf_Location  = glo:Select2
                                                    sto:Manufacturer    = job:Manufacturer
                                                    sto:Location        = glo:Select1
                                                    sto:Second_Location = glo:Select3
                                                    If Access:STOCK.TryInsert() = Level:Benign
                                                        !Insert Successful
                                                        Local.CreateStockHistory('PART EXISTS ON ORDER NUMBER: ' & wpr:order_number)
                                                        tmp:Delete = 1
                                                    Else !If Access:STOCK.TryInsert() = Level:Benign
                                                        !Insert Failed
                                                    End !If Access:STOCK.TryInsert() = Level:Benign
                                                End !If Access:STOCK.PrimeRecord() = Level:Benign
                                            End !If glo:Select1 <> ''
                                        End !If tmp:StockNumber
                                    Of 2 ! Scrap Button
                                        tmp:Delete  = 1
                                        tmp:Scrap   = 1
                                    Of 1 ! Cancel Button
                                End ! Case Missive
                            End !If func:Type = 0

                        End !If tmp:StockNumber and sto:Sundry_Item = 'YES'
                    Else !If wpr:Date_Recevied <> ''
                        Case Missive('The selected part is on Order Number ' & Clip(wpr:Order_Number) & ' but has not been received.'&|
                          '<13,10>If you choose to delete it, the part on the order will be marked as RECEIVED. Are you sure you want to delete this part?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                            Of 2 ! Yes Button
                                Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
                                orp:Record_Number   = wpr:Order_Part_Number
                                If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                                    !Found
                                    orp:Quantity    = 0
                                    orp:Date_Received   = Today()
                                    orp:Number_Received = 0
                                    orp:All_Received    = 'YES'
                                    If Access:ORDPARTS.Update() = Level:Benign
                                        !Check if all received
                                        found# = 0
                                        Setcursor(cursor:wait)
                                        save_orp_id = access:ordparts.savefile()
                                        access:ordparts.clearkey(orp:order_Number_key)
                                        orp:order_number    = wpr:order_number
                                        Set(orp:order_number_key,orp:order_number_key)
                                        Loop
                                            If access:ordparts.next()
                                                Break
                                            End!If access:ordparts.next()
                                            If orp:order_Number <> wpr:order_Number
                                                Break
                                            End!If orp:order_Number <> wpr:order_Number
                                            If orp:all_received <> 'YES'
                                                found# = 1
                                                Break
                                            End!If orp:all_received <> 'YES'
                                        End!Loop
                                        access:ordparts.restorefile(save_orp_id)
                                        setcursor()

                                        If found# = 0
                                            access:orders.clearkey(ord:order_Number_key)
                                            ord:order_number    = wpr:order_number
                                            If access:orders.tryfetch(ord:order_number_key) = Level:Benign
                                                ord:all_received = 'YES'
                                                access:orders.update()
                                            End!If access:orders.tryfetch(ord:order_number_key) = Level:Benign
                                        End!If found# = 0
                                        tmp:Delete = 1
                                    End !If Access:ORDPARTS.Update() = Level:Benign
                                Else! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                            Of 1 ! No Button
                        End ! Case Missive
                    End !If wpr:Date_Recevied <> ''
                Else  !If wpr:Order_Number <> ''
                    If wpr:Pending_Ref_Number <> ''
                        Case Missive('This part has been marked to appear on the NEXT Part Order Generation.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes')
                            Of 2 ! Yes Button
                                Case def:SummaryOrders
                                    Of 0!Old Way
                                        Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                                        ope:Ref_Number  = wpr:Pending_Ref_Number
                                        If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                            !Found
                                            Delete(ORDPEND)
                                        Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                            !Error
                                            !Assert(0,'<13,10>Fetch Error<13,10>')
                                        End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                    Of 1!New Way
                                        Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                        ope:Supplier    = wpr:Supplier
                                        ope:Part_Number = wpr:Part_Number
                                        If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                            !Found
                                            ope:Quantity -= wpr:Quantity
                                            If ope:Quantity <=0
                                                Delete(ORDPEND)
                                            Else !If ope:Quantity <=0
                                                Access:ORDPEND.Update()
                                            End !If ope:Quantity <=0
                                        Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                            !Error
                                            !Assert(0,'<13,10>Fetch Error<13,10>')
                                        End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign

                                End !Case def:SummaryOrders
                                tmp:Delete = 1
                            Of 1 ! No Button
                        End ! Case Missive
                    Else !If wpr:Pending_Ref_Number <> ''
                        If func:Type = 0 And RapidLocation(sto:Location)
                            If tmp:AttachBySolder
                                Case Missive('The selected part is "Attached By Solder" and therefore will be scrapped.','ServiceBase 3g',|
                                               'mquest.jpg','\No|/Scrap')
                                    Of 2 ! Scrap Button
                                        tmp:Delete  = 1
                                        tmp:Scrap   = 1
                                        Do AddSolder
                                    Of 1 ! No Button
                                        Return 0
                                End ! Case Missive
                            Else !If tmp:AttachBySolder
                                If wpr:PartAllocated = 0
                                    Case Missive('This part has not yet been allocated. '&|
                                      '<13,10>'&|
                                      '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                                                   'mquest.jpg','\No|/Yes')
                                        Of 2 ! Yes Button
                                            tmp:Delete = 1
                                            Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                            sto:Ref_Number  = tmp:StockNumber
                                            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                !Found
                                                sto:Quantity_Stock += wpr:Quantity
                                                If Access:STOCK.Update() = Level:Benign
                                                    Local.CreateStockHistory('UNALLOCATED PART REMOVED')
                                                    tmp:Delete = 1
                                                    ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                    if (sto:Suspend)
                                                        sto:Suspend = 0
                                                        If (Access:STOCK.TryUpdate() = Level:Benign)
                                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                             'ADD', | ! Transaction_Type
                                                                             '', | ! Depatch_Note_Number
                                                                             0, | ! Job_Number
                                                                             0, | ! Sales_Number
                                                                             0, | ! Quantity
                                                                             wpr:Purchase_Cost, | ! Purchase_Cost
                                                                             wpr:Sale_Cost, | ! Sale_Cost
                                                                             wpr:Retail_Cost, | ! Retail_Cost
                                                                             'PART UNSUSPENDED', | ! Notes
                                                                             '') ! Information
                                                            End ! AddToStockHistory
                                                        End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                    end

                                                End !If Access:STOCK.Update() = Level:Benign
                                            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                !Error
                                                !Assert(0,'<13,10>Fetch Error<13,10>')
                                            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                                        Of 1 ! No Button
                                    End ! Case Missive
                                Else !If par:PartAllocated = 0
                                    Case Missive('Are you sure you want to mark this part to be returned by allocations?','ServiceBase 3g',|
                                                   'mquest.jpg','\No|/Yes')
                                        Of 2 ! Yes Button
                                            wpr:PartAllocated = 0
                                            wpr:Status  = 'RET'
                                            Access:WARPARTS.Update()
                                            AddToStockAllocation(wpr:Record_Number,'WAR',wpr:Quantity,'RET',job:Engineer)
                                            Return 0
                                        Of 1 ! No Button
                                    End ! Case Missive
                                End !If par:PartAllocated = 0
                            End !If tmp:AttachBySolder
                        Else !If func:Type = 0

                            Case Missive('You have selected to delete this part?'&|
                              '<13,10>'&|
                              '<13,10>Do you wish to RESTOCK it, or SCRAP it?','ServiceBase 3g',|
                                           'mquest.jpg','\Cancel|Scrap|Restock')
                                Of 3 ! Restock Button
                                    If tmp:StockNumber
                                        Case Missive('The selected part was originally taken from  location ' & Clip(sto:Location) & '.'&|
                                          '<13,10>'&|
                                          '<13,10>Do you wish to return it to it''s ORIGINAL location, or to a NEW location?','ServiceBase 3g',|
                                                       'mquest.jpg','\Cancel|New|Original')
                                            Of 3 ! Original Button
                                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                                sto:Ref_Number  = tmp:StockNumber
                                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Found
                                                    sto:Quantity_stock += wpr:Quantity
                                                    If Access:STOCK.Update() = Level:Benign
                                                        Local.CreateStockHistory('')
                                                        tmp:Delete = 1
                                                        ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                        if (sto:Suspend)
                                                            sto:Suspend = 0
                                                            If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                 'ADD', | ! Transaction_Type
                                                                                 '', | ! Depatch_Note_Number
                                                                                 0, | ! Job_Number
                                                                                 0, | ! Sales_Number
                                                                                 0, | ! Quantity
                                                                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                                                                 wpr:Sale_Cost, | ! Sale_Cost
                                                                                 wpr:Retail_Cost, | ! Retail_Cost
                                                                                 'PART UNSUSPENDED', | ! Notes
                                                                                 '') ! Information
                                                                End ! AddToStockHistory
                                                            End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                        end

                                                    End !If Access:STOCK.Update() = Level:Benign
                                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                                    !Error
                                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                            Of 2 ! New Button

                                                glo:Select1 = ''
                                                Pick_New_Location
                                                If glo:Select1 <> ''
                                                    Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                                    sto:Location    = glo:Select1
                                                    sto:Part_Number = wpr:Part_Number
                                                    sto:Description = wpr:Description
                                                    If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                        !Found
                                                        sto:Quantity_Stock += wpr:Quantity
                                                        If Access:STOCK.Update() = Level:Benign
                                                            Local.CreateStockHistory('')
                                                            tmp:Delete = 1
                                                            ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                                            if (sto:Suspend)
                                                                sto:Suspend = 0
                                                                If (Access:STOCK.TryUpdate() = Level:Benign)
                                                                    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                                     'ADD', | ! Transaction_Type
                                                                                     '', | ! Depatch_Note_Number
                                                                                     0, | ! Job_Number
                                                                                     0, | ! Sales_Number
                                                                                     0, | ! Quantity
                                                                                     wpr:Purchase_Cost, | ! Purchase_Cost
                                                                                     wpr:Sale_Cost, | ! Sale_Cost
                                                                                     wpr:Retail_Cost, | ! Retail_Cost
                                                                                     'PART UNSUSPENDED', | ! Notes
                                                                                     '') ! Information
                                                                    End ! AddToStockHistory
                                                                End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                                            end

                                                        End !If Access:STOCK.Update() = Level:Benign
                                                    Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                                        !Error
                                                        Case Missive('Cannot find the selected part in location ' & Clip(glo:Select1) & '.'&|
                                                          '<13,10>'&|
                                                          '<13,10>Do you wish to add it as a NEW part in this location, or SCRAP it?','ServiceBase 3g',|
                                                                       'mquest.jpg','\Cancel|Scrap|New')
                                                            Of 3 ! New Button

                                                                glo:Select2 = ''
                                                                glo:Select3 = ''
                                                                Pick_Locations
                                                                If glo:Select1 <> ''
                                                                    If Access:STOCK.PrimeRecord() = Level:Benign
                                                                        sto:Part_Number     = wpr:Part_Number
                                                                        sto:Description     = wpr:Description
                                                                        sto:Supplier        = wpr:Supplier
                                                                        sto:Purchase_Cost   = wpr:Purchase_Cost
                                                                        sto:Sale_Cost       = wpr:Sale_Cost
                                                                        sto:Retail_Cost     = wpr:Retail_cost
                                                                        sto:Shelf_Location  = glo:select2
                                                                        sto:Manufacturer    = job:Manufacturer
                                                                        sto:Location        = glo:Select1
                                                                        sto:Second_Location = glo:Select3
                                                                        If Access:STOCK.TryInsert() = Level:Benign
                                                                            !Insert Successful
                                                                            Local.CreateStockHistory('')
                                                                            tmp:Delete = 1
                                                                        Else !If Access:STOCK.TryInsert() = Level:Benign
                                                                            !Insert Failed
                                                                        End !If Access:STOCK.TryInsert() = Level:Benign
                                                                    End !If Access:STOCK.PrimeRecord() = Level:Benign
                                                                End !If glo:Select1 <> ''
                                                            Of 2 ! Scrap Button
                                                                tmp:Delete  = 1
                                                                tmp:Scrap   = 1
                                                            Of 1 ! Cancel Button
                                                        End ! Case Missive
                                                    End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign

                                                End !If glo:Select1 <> ''
                                            Of 1 ! Cancel Button
                                        End ! Case Missive
                                    Else !If tmp:StockNumber
                                        glo:Select1 = ''
                                        glo:Select2 = ''
                                        glo:Select3 = ''
                                        Pick_Locations
                                        If glo:Select1 <> ''
                                            If Access:STOCK.PrimeRecord() = Level:Benign
                                                sto:Part_Number     = wpr:Part_Number
                                                sto:Description     = wpr:Description
                                                sto:Supplier        = wpr:Supplier
                                                sto:Purchase_Cost   = wpr:Purchase_Cost
                                                sto:Sale_Cost       = wpr:Sale_Cost
                                                sto:Retail_Cost     = wpr:Retail_cost
                                                sto:Shelf_Location  = glo:Select2
                                                sto:Manufacturer    = job:Manufacturer
                                                sto:Location        = glo:Select1
                                                sto:Second_Location = glo:Select3
                                                If Access:STOCK.TryInsert() = Level:Benign
                                                    !Insert Successful
                                                    Local.CreateStockHistory('')
                                                    tmp:Delete = 1
                                                Else !If Access:STOCK.TryInsert() = Level:Benign
                                                    !Insert Failed
                                                End !If Access:STOCK.TryInsert() = Level:Benign
                                            End !If Access:STOCK.PrimeRecord() = Level:Benign
                                        End !If glo:Select1 <> ''
                                    End !If tmp:StockNumber
                                Of 2 ! Scrap Button
                                    tmp:Delete  = 1
                                    tmp:Scrap   = 1
                                Of 1 ! Cancel Button
                            End ! Case Missive
                        End !If func:Type = 0
                    End !If wpr:Pending_Ref_Number <> ''
                End !If wpr:Order_Number <> ''
            End !If tmp:StockNumber And sto:Sundry_Item = 'YES'
        End !If wpr:Adjustment = 'YES'
        If tmp:Scrap
            !Remove part from stock allocation tabe - L873 (DBH: 24-07-2003)
            RemoveFromStockAllocation(wpr:Record_Number,'WAR')

            locAuditNotes = 'DESCRIPTION: ' & Clip(wpr:Description)
            IF tmp:AttachBySolder
                locAuditNotes   = Clip(locAuditNotes) & '<13,10,13,10>PART ATTACHED BY SOLDER'
            End !IF tmp:AttachBySolder    
            
            if (tmp:StockNumber > 0)
                ! #11368 Add Extra Scrap Information (DBH: 11/06/2010)
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number = tmp:StockNumber
                If (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                    locAuditNotes = clip(locAuditNotes) & '<13,10>LOCATION: ' & clip(sto:Location) & |
                                                    '<13,10>QTY: ' & wpr:Quantity
                End
            end
            
            IF (AddToAudit(job:Ref_Number,'JOB','SCRAP WARRANTY PART: ' & Clip(wpr:Part_Number),CLIP(locAuditNotes)))
            END ! IF

            ! #11368 Add Scrap information to Stock History (DBH: 11/06/2010)
            if (tmp:StockNumber > 0)
                If AddToStockHistory(sto:Ref_Number,'ADD','',wpr:Ref_Number,0,0,sto:Purchase_Cost,sto:Sale_Cost,sto:Retail_Cost,|
                          'PART SCRAPPED',|
                          'TYPE: WARRANTY' & |
                          '<13,10>QUANTITY: ' & wpr:Quantity)
                End ! '<13,10>NEW PART: ' & Clip(local:NewPartNumber) & ' - ' & Clip(local:NewDescription))
            end
        Else
            If tmp:Delete
                !Delete part from stock allocation table -  (DBH: 23-07-2003)
                RemoveFromStockAllocation(wpr:Record_Number,'WAR')

                locAuditNotes         = 'DESCRIPTION: ' & Clip(wpr:Description)
                IF tmp:AttachBySolder
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10,13,10>PART ATTACHED BY SOLDER'
                End !IF tmp:AttachBySolder
                
                IF (AddToAudit(job:Ref_Number,'JOB','WARRANTY PART DELETED: ' & Clip(wpr:Part_Number),locAuditNotes))
                END ! IF
            End !If tmp:Delete
        End !If Scrap# = 1
    End !If wpr:WebOrder


    Return tmp:Delete
AddSolder       Routine
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        stf:PartNumber  = wpr:Part_Number
        stf:Description = wpr:Description
        stf:Quantity    = wpr:Quantity
        stf:PurchaseCost    = wpr:Purchase_Cost
        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Found

        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        stf:StoreUserCode   = use:User_Code
        stf:PartType        = 'SOL'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign



Local.CreateStockHistory          Procedure(String    func:Information)
Code
    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                         'ADD', | ! Transaction_Type
                         wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                         job:Ref_Number, | ! Job_Number
                         0, | ! Sales_Number
                         wpr:Quantity, | ! Quantity
                         wpr:Purchase_Cost, | ! Purchase_Cost
                         wpr:Sale_Cost, | ! Sale_Cost
                         wpr:Retail_Cost, | ! Retail_Cost
                         func:Information, | ! Notes
                         'WARRANTY PART REMOVED FROM JOB') ! Information
        ! Added OK
        tmp:Delete = 1
    Else ! AddToStockHistory
        ! Error
    End ! AddToStockHistory
Local.RemoveExistingWebOrder        Procedure()
tmp:FoundOrder      Byte(0)
tmp:FoundPart       Byte(0)
tmp:WebAccountNumber    String(30)
Code
    !Look for un processed order
    If glo:WebJob
        tmp:WebAccountNumber   = ClarioNET:Global.Param2
    Else !If glo:WebJob
        tmp:WebAccountNumber   = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
    End !If glo:WebJob

    Save_orw_ID = Access:ORDWEBPR.SaveFile()
    Access:ORDWEBPR.ClearKey(orw:PartNumberKey)
    orw:AccountNumber   = tmp:WebAccountNumber
    orw:PartNumber    = wpr:Part_Number
    Set(orw:PartNumberKey,orw:PartNumberKey)
    Loop
        If Access:ORDWEBPR.NEXT()
           Break
        End !If
        If orw:AccountNumber <> tmp:WebAccountNumber      |
        Or orw:PartNumber    <> wpr:Part_Number      |
            Then Break.  ! End If
        If orw:Description  = wpr:Description
            tmp:FoundOrder = 1
            orw:Quantity    -= wpr:Quantity
            If orw:Quantity <= 0
                Delete(ORDWEBPR)
                Break
            End !If orw:Quantity <= 0
        End !If orw:Description  = par:Description

    End !Loop
    Access:ORDWEBPR.RestoreFile(Save_orw_ID)
    !Look for un processed order

!    Save_orh_ID = Access:ORDHEAD.SaveFile()
!    Access:ORDHEAD.ClearKey(orh:ProcessSaleNoKey)
!    orh:procesed    = 0
!    Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
!    Loop
!        If Access:ORDHEAD.NEXT()
!           Break
!        End !If
!        If orh:procesed    <> 0      |
!            Then Break.  ! End If
!        If orh:Account_No   = job:Account_Number
!            Save_ori_ID = Access:ORDITEMS.SaveFile()
!            Access:ORDITEMS.ClearKey(ori:Keyordhno)
!            ori:ordhno = orh:Order_No
!            Set(ori:Keyordhno,ori:Keyordhno)
!            Loop
!                If Access:ORDITEMS.NEXT()
!                   Break
!                End !If
!                If ori:ordhno <> orh:Order_No      |
!                    Then Break.  ! End If
!
!                If ori:partno = wpr:Part_Number And |
!                    ori:partdiscription = wpr:Description
!                    tmp:FoundOrder = 1
!                    !Found a part awaiting order with same part number
!                    !and description. Decrease it's quantity
!                    ori:qty -= wpr:Quantity
!                    If ori:qty <= 0
!                        !If there is non left to order delete the part.
!                        Delete(ORDITEMS)
!                    End !If ori:qty = 0
!                End !ori:partdiscription = wpr:Description
!            End !Loop
!            Access:ORDITEMS.RestoreFile(Save_ori_ID)
!
!            !Are there still any parts on this order?
!
!            Save_ori_ID = Access:ORDITEMS.SaveFile()
!            Access:ORDITEMS.ClearKey(ori:Keyordhno)
!            ori:ordhno = orh:Order_No
!            Set(ori:Keyordhno,ori:Keyordhno)
!            Loop
!                If Access:ORDITEMS.NEXT()
!                   Break
!                End !If
!                If ori:ordhno <> orh:Order_No      |
!                    Then Break.  ! End If
!                tmp:FoundPart = 1
!                Break
!            End !Loop
!            Access:ORDITEMS.RestoreFile(Save_ori_ID)
!
!            If tmp:FoundPart = 0
!                !No more parts on the order
!                Delete(ORDHEAD)
!            End !If Found# = 0
!        End !If orh:account_no = job:Account_Number
!    End !Loop
!    Access:ORDHEAD.RestoreFile(Save_orh_ID)

    Return tmp:FoundOrder
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ProductCodeRequired  PROCEDURE  (func:Manufacturer)   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:UseProductCode
            !Sometimes this is to be compulsory
            !J 30/03/12 - TB12360
            if man:ProductCodeCompulsory = 'YES' then
                Return Level:Notify      !this is compulsory
            ELSE
                Return Level:Benign    !may use but not compulsory
            END

        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    Return Level:Fatal
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DeleteStockPart      PROCEDURE  (func:Location,func:Request,func:RefNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!func:Location = Location_Temp
!func:Request  = Request
!func:RefNumber = sto:Ref_Number

    If func:Location = '' And func:Request = 1
        Case Missive('You must select a location.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Access:STOCK.CancelAutoInc()
        Return False
    End !If func:Location = '' And func:Request = InsertRecord

    Case func:Request
        Of 1
            If SecurityCheck('STOCK - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Access:STOCK.CancelAutoInc()
                Return False
            End !If SecurityCheck('STOCK - INSERT')

        Of 2
            If SecurityCheck('STOCK - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return False
            End !If SecurityCheck('STOCK - CHANGE')

        Of 3
            If SecurityCheck('STOCK - DELETE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return False
            End !If SecurityCheck('STOCK - DELETE')

    End !Case func:Request

    If func:Request = 3
        Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes')
            Of 2 ! Yes Button
                Case Missive('Do you wish to delete all other occurances of this part?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                    Of 2 ! Yes Button
                        Case Missive('If you continue ALL other occurances of this part will be deleted for ALL Site Locations.'&|
                          '<13,10>'&|
                          '<13,10>You will NOT be able to recover this information. Are you sure?','ServiceBase 3g',|
                                       'mexclam.jpg','\No|/Yes')
                            Of 2 ! Yes Button
                                Access:STOCK_ALIAS.Clearkey(sto_ali:Ref_Number_Key)
                                sto_ali:Ref_Number  = func:RefNumber
                                If Access:STOCK_ALIAS.Tryfetch(sto_ali:Ref_Number_Key) = Level:Benign
                                    !Found


                                    Prog.ProgressSetup(Records(LOCATION))

                                    save_loc_id = access:location.savefile()
                                    set(loc:location_key)
                                    loop
                                        if access:location.next()
                                           break
                                        end !if
                                        If (Prog.InsideLoop())
                                            Beep(Beep:SystemHand)  ;  Yield()
                                            Case Missive('You cannot cancel this process.','ServiceBase',|
                                                           'mstop.jpg','/&OK')
                                            Of 1 ! &OK Button
                                            End!Case Message
                                        End

                                        save_sto_id = access:stock.savefile()
                                        access:stock.clearkey(sto:location_part_description_key)
                                        sto:location    = loc:location
                                        sto:part_number = sto_ali:Part_Number
                                        sto:description = sto_ali:Description
                                        set(sto:location_part_description_key,sto:location_part_description_key)
                                        loop
                                            if access:stock.next()
                                               break
                                            end !if
                                            if sto:location    <> loc:location      |
                                            or sto:part_number <> sto_ali:Part_Number      |
                                            or sto:description <> sto_ali:Description      |
                                                then break.  ! end if
                                            relate:stock.delete(0)
                                        end !loop
                                        access:stock.restorefile(save_sto_id)
                                    end !loop

                                    Prog.ProgressFinish()

                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign


                            Of 1 ! No Button
                        End ! Case Missive
                    Of 1 ! No Button
                End ! Case Missive
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = func:RefNumber
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found
                    Relate:Stock.Delete(0)
                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            Of 1 ! No Button
        End ! Case Missive
        Return False
    End !If func:Request = DeleteRecord
    Return True
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Source
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 5
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
VirtualAccount       PROCEDURE  (func:AccountNumber)  ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return (VodacomClass.RemoteAccount(func:AccountNumber))

!    !Is this a virtual account
!    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!    sub:Account_Number  = func:AccountNumber
!    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!        !Found
!        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!        tra:Account_Number  = sub:Main_Account_Number
!        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!            !Found
!            If tra:RemoteRepairCentre
!                Return Level:Fatal
!            End !If tra:RemoteRepairCentre
!        Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!            !Error
!        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!
!    Else !If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
!        !Error
!    End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
EngineerSkillLevel   PROCEDURE  (func:UserCode)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Return the Engineer's Skill Level
    IF func:UserCode <> ''
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code   = func:UserCode
        If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Found
            Return use:SkillLevel
        Else ! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    End !IF func:UserCode <> ''

    !No Skill Level
    Return 0

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
VirtualSite          PROCEDURE  (func:Location)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:LOCATION_ALIAS.ClearKey(loc_ali:Location_Key)
    loc_ali:Location = func:Location
    If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
        !Found
        If loc_ali:VirtualSite
            Return Level:Fatal
        End !If loc_ali:VirtualSite
    Else!If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:LOCATION_ALIAS.TryFetch(loc_ali:Location_Key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AveragePurchaseCost  PROCEDURE  (func:RefNumber,func:Quantity,func:Cost) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_shi_id          USHORT,AUTO
tmp:Cost             REAL
tmp:Count            LONG
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    glo:errortext = ''
    If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        !Now try and work out the Average Purchase Price
        !Count Stock
        glo:ErrorText = 'IN STOCK = ' & sto:Quantity_Stock
        tmp:Count   = 0
        tmp:Cost    = 0
        !If sto:Location = MainStoreLocation()
            Save_shi_ID = Access:STOHIST.SaveFile()
            Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
            shi:Ref_Number       = func:RefNumber
            shi:Transaction_Type = 'ADD'
            shi:Date             = Today()
            Set(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
            Loop
                If Access:STOHIST.Previous()
                   Break
                End !If
                If shi:Ref_Number       <> func:RefNumber      |
                Or shi:Transaction_Type <> 'ADD'      |
                    Then Break.  ! End If

                If shi:Notes = 'STOCK ADDED FROM ORDER' Or shi:Notes = 'INITIAL STOCK QUANTITY'
                    If shi:Quantity <= func:Quantity - tmp:Count
                        tmp:Count   += shi:Quantity
                        tmp:Cost    += (shi:Quantity * shi:Purchase_Cost)
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>' & (shi:Quantity) & '@' & Format(shi:Purchase_Cost,@n14.2) & ' = ' & Format((shi:Quantity * shi:Purchase_Cost),@n14.2)
                    Else !If shi:Quantity <= sto:Quantity_Stock - tmp:Count
                        tmp:Cost    += (func:Quantity - tmp:Count) * shi:Purchase_Cost
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>' & (func:Quantity - tmp:Count) & '@' & Format(shi:Purchase_Cost,@n14.2) & ' = ' & Format((func:Quantity - tmp:Count) * shi:Purchase_Cost,@n14.2)
                        tmp:Count   += (func:Quantity - tmp:Count)
                    End !If shi:Quantity <= sto:Quantity_Stock - tmp:Count

                    If tmp:Count >= func:Quantity
                        !Found enough orders to make up the
                        !quantity in stock
                        Break
                    End !If tmp:Count >= sto:Quantity_Stock

                End !If shi:Notes = 'STOCK ADDED FROM ORDER'
            End !Loop
            Access:STOHIST.RestoreFile(Save_shi_ID)
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>AVERAGE: ' & Format(tmp:Cost,@n14.2) & |
                            ' / ' & tmp:Count & ' = ' & Format(tmp:Cost / tmp:Count,@n14.2)
            Return Format(tmp:Cost / tmp:Count,@n14.2)

        !Else !If sto:Location = MainStoreLocation()
        !    Return func:Cost
        !End !If sto:Location = MainStoreLocation()

    Else !If GETINI('STOCK','IgnorePriceChange',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
        Return Format(func:Cost,@n14.2)
    End !If GETINI('STOCK','IgnorePriceChange',,CLIP(PATH())&'\SB2KDEF.INI') <> 1

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
