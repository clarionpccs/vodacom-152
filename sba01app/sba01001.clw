

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01001.INC'),ONCE        !Local module procedure declarations
                     END


DoNotDelete PROCEDURE                                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DoNotDelete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDITE.Open
  Relate:DEFAULTV.Open
  Relate:EXPCITY.Open
  Relate:EXPGEN.Open
  Relate:EXPLABG.Open
  Relate:JOBSE_ALIAS.Open
  Relate:LOANACC.Open
  Relate:MULDESP.Open
  Relate:PARAMSS.Open
  Relate:STATUS.Open
  Relate:TRADETMP.Open
  Relate:TRANTYPE.Open
  Access:INVOICE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSE2.UseFile
  Access:JOBSWARR.UseFile
  Access:JOBSLOCK.UseFile
  Access:MULDESPJ.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','DoNotDelete')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDITE.Close
    Relate:DEFAULTV.Close
    Relate:EXPCITY.Close
    Relate:EXPGEN.Close
    Relate:EXPLABG.Close
    Relate:JOBSE_ALIAS.Close
    Relate:LOANACC.Close
    Relate:MULDESP.Close
    Relate:PARAMSS.Close
    Relate:STATUS.Close
    Relate:TRADETMP.Close
    Relate:TRANTYPE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','DoNotDelete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Postcode_Routine     PROCEDURE  (f_postcode,f_add1,f_add2,f_add3) ! Declare Procedure
tmp:os               STRING(20)
szpostcode           CSTRING(20)
szpath               CSTRING(255)
szaddress            CSTRING(255)
tmp:high             BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
full_postcode       String(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Postcode bit
    Setclipboard(f_postcode)
    Set(Defaults)
    access:defaults.next()
    tmp:os  = GetVersion()
    tmp:high    = Bshift(tmp:os,-8)


    If def:use_postcode = 'YES'

        If def:postcodedll <> 'YES'

            If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',0)
            Else!If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',1)
            End!If tmp:high = 0
            if error()
                error# = 1
            Else
                IF Sub(Clipboard(),1,1) = '#'
                    Case Missive('Invalid Postcode.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    error# = 1
                    f_postcode = ''
                    f_add1 = ''
                    f_add2 = ''
                    f_add3 = ''
                Else!if error
                    full_postcode = Clipboard()
                end!if error
            End
        Else!If def:postcodedll <> 'YES'
            szpath  = Clip(def:postcode_path)
!            If Initaddress(szpath)
!                szpostcode  = f_postcode
!                If getaddress(szpostcode,szaddress)
!                    full_postcode   = szaddress
!                Else!
!                    error# = 1
!                End!If getaddress(szpostcode,szaddress)
!            Else!If Initaddress(szpath)
                error# = 1
!            End!If Initaddress(szpath)
        End!If def:postcodedll <> 'YES'

        If error# = 0
            Loop x# = 1 to len(full_postcode)
                If Sub(full_postcode,x#,1) = ';'
                    f_add1 = Sub(full_postcode,1,(x#-1))
                    Break
                End
            End
            Loop y# = (x#+1) to len(full_postcode)
                If sub(full_postcode,y#,1) = ';'
                    f_add2 = Sub(full_postcode,(x#+1),(y#-1)-(x#))
                    Break
                End
            End
            Loop z# = (y#+1) to len(full_postcode)
                If sub(full_postcode,z#,1) = ';'
                    f_add3 = Sub(full_postcode,(y#+1),(z#-1)-(y#))
                    f_postcode = Sub(full_postcode,(z#+1),len(full_postcode)-z#)
                    Break
                Else
                    f_add3 = ''
                    f_postcode = Sub(full_postcode,(y#+1),len(full_postcode)-y#)
                End
            End
            f_add1  = Upper(f_add1)
            f_add2  = Upper(f_add2)
            f_add3  = Upper(f_add3)
            f_postcode  = Upper(f_postcode)
        Else!If error# = 0
            Case Missive('A problem occured with the Postcode program.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End!If error# = 0
    End
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Pricing_Routine      PROCEDURE  (f_type,f_labour,f_parts,f_pass,f_claim,f_handling,f_exchange,f_RRCRate,f_RRCParts) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccessoryExists  BYTE(0)
tmp:TradeAccount     STRING(15)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_epr_id   ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !This does nothing anymore...
    !Replaced by JobPricingRoutine
    Return




!
!
!! Source Bit
!    f_labour = 0
!    f_parts = 0
!    f_pass = 0
!    f_claim = 0
!    f_handling = 0
!    f_exchange = 0
!    f_RRCRate   = 0
!    f_RRCParts = 0
!! Chargeable Job?
!
!    If job:account_number = ''
!        Return
!    End
!    Case f_type
!        Of 'C'
!            If job:chargeable_job = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!            ! No Charge?
!!                If job:ignore_chargeable_charges = 'YES'
!!                    Return
!!                End
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate = 0
!                        f_RRCParts = 0
!
!                    Else !If cha:no_charge = 'YES'
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            rrc_total$   = 0
!                            save_par_id = access:parts.savefile()
!                            access:parts.clearkey(par:part_number_key)
!                            par:ref_number  = job:ref_number
!                            set(par:part_number_key,par:part_number_key)
!                            loop
!                                if access:parts.next()
!                                   break
!                                end !if
!                                if par:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += par:sale_cost * par:quantity
!                                rrc_total$  += par:RRCSaleCost * par:Quantity   !was + changed JC
!                                !message(par:rrcSaleCost * par:quantity&' - '&rcc_total$)
!                            end !loop
!                            access:parts.restorefile(save_par_id)
!                            f_parts = parts_total$
!                            f_rrcparts  = rrc_total$
!
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_rrcparts = 0
!
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!
!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    f_handling  = trc:HandlingFee
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                f_handling  = suc:HandlingFee
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:invoice_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                f_handling  = trc:HandlingFee
!                                use_standard# = 0
!
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:invoice_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                                f_handling  = sta:HandlingFee
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'W'
!
!        !Warranty Job
!            If job:warranty_job = 'YES'
!                !Do not reprice Warranty Jobs that are completed - L945 (DBH: 03-09-2003)
!                If job:Date_Completed <> 0
!
!                    Return
!                End !If job:Date_Completed <> 0
!                parts_discount# = 0
!                labour_discount# = 0
!                f_pass = 1
!!               If job:ignore_warranty_charges = 'YES'
!!                   Return
!!               End
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:warranty_charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCParts  = 0
!                    f_RRCRate   = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCParts  = 0
!                        f_RRCRate   = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!
!                            Access:STOCK.Open()
!                            Access:STOCK.UseFile()
!
!                            tmp:AccessoryExists = false
!                            tmp:TradeAccount = ''
!
!                            save_wpr_id = access:warparts.savefile()
!                            access:warparts.clearkey(wpr:part_number_key)
!                            wpr:ref_number  = job:ref_number
!                            set(wpr:part_number_key,wpr:part_number_key)
!                            loop
!                                if access:warparts.next()
!                                   break
!                                end !if
!                                if wpr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += wpr:purchase_cost * wpr:quantity
!                                RRCParts_Total$ += wpr:RRCPurchaseCost * wpr:Quantity
!
!                                ! --------------------------------------------------------------------
!                                ! Check if any parts are accessories
!                                Access:STOCK.ClearKey(sto:Ref_Number_Key)
!                                sto:Ref_Number = wpr:Part_Ref_Number
!                                if not Access:STOCK.Fetch(sto:Ref_Number_Key)
!                                    if sto:Accessory = 'YES'
!                                        tmp:AccessoryExists = true
!                                    end
!                                    i# = instring('<32>',clip(sto:Location),1,1)
!                                    if i# <> 0
!                                        tmp:TradeAccount = sto:Location[1:i#]
!                                    end
!                                end
!                                ! --------------------------------------------------------------------
!                            end !loop
!                            access:warparts.restorefile(save_wpr_id)
!                            Access:STOCK.Close()
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!
!                        use_standard# = 1
!!                        If TRA:ZeroChargeable = 'YES'
!!                            f_parts = 0
!!                        End!If TRA:ZeroChargeable = 'YES'
!                        If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:Warranty_charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type_warranty
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:warranty_charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type_warranty
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_claim     = trc:WarrantyClaimRate
!                                    f_handling  = trc:HandlingFee
!                                    f_Exchange  = trc:Exchange
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_claim     = suc:WarrantyClaimRate
!                                f_handling  = suc:HandlingFee
!                                f_Exchange  = suc:Exchange
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = tra:account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:warranty_charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type_warranty
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_claim     = trc:WarrantyClaimRate
!                                f_handling  = trc:HandlingFee
!                                f_Exchange  = trc:Exchange
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:warranty_charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type_warranty
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_claim     = sta:WarrantyClaimRate
!                                f_handling  = sta:HandlingFee
!                                f_Exchange  = sta:Exchange
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                        ! ------------------------------------------------------------------------
!!                        if tmp:AccessoryExists = true and job:warranty_charge_type = 'WARRANTY (MFTR)'
!!
!!                            if tmp:TradeAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!!                                ! ARC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_Exchange = 0
!!                                else
!!                                    f_Exchange = 0
!!                                    f_handling  = 0
!!                                end
!!                            else
!!                                ! RRC
!!                                if SentToHub(job:Ref_Number) and jobe:WebJob = true
!!                                    f_handling  = 0
!!                                else
!!                                    f_handling  = 0
!!                                end
!!                            end
!!                        else
!                            If job:Exchange_Unit_Number <> 0 AND jobe:ExchangedATRRC = TRUE
!                                !jobe:ExchangeRate = Exchange"
!                                f_handling  = 0
!                            Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                                IF SentToHub(job:Ref_Number)
!                                    f_Exchange = 0
!                                    !jobe:HandlingFee  = Handling"
!                                ELSE
!                                    f_Exchange = 0
!                                    f_handling  = 0
!                                END
!                            End
!                        !End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangeRate
!                        ! ------------------------------------------------------------------------
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:chargeable_job = 'YES'
!
!        Of 'E'
!            If job:estimate = 'YES'
!                labour_discount# = 0
!                parts_discount#  = 0
!                f_pass  = 1
!!                If job:ignore_estimate_charges = 'YES'
!!                    Return
!!                End!If job:ignore_estimate_charges = 'YES'
!            ! No Charge?
!                access:chartype.clearkey(cha:charge_type_key)
!                cha:charge_type = job:charge_type
!                If access:chartype.fetch(cha:charge_type_key)
!                    f_labour    = 0
!                    f_parts     = 0
!                    f_RRCRate   = 0
!                    f_RRCParts  = 0
!                    f_pass      = 0
!                Else !If access:chartype.fetch(cha:charge_type_key)
!                    If cha:no_charge = 'YES'
!                        f_labour = 0
!                        f_parts  = 0
!                        f_RRCRate  = 0
!                        f_RRCParts = 0
!                    Else !If cha:no_charge = 'YES'
!
!                        If cha:zero_parts <> 'YES'
!                            parts_total$ = 0
!                            RRCParts_Total$ = 0
!                            save_epr_id = access:estparts.savefile()
!                            access:estparts.clearkey(epr:part_number_key)
!                            epr:ref_number  = job:ref_number
!                            set(epr:part_number_key,epr:part_number_key)
!                            loop
!                                if access:estparts.next()
!                                   break
!                                end !if
!                                if epr:ref_number  <> job:ref_number      |
!                                    then break.  ! end if
!                                parts_total$ += epr:sale_cost * epr:quantity
!                                RRCParts_Total$ += epr:RRCSaleCost * epr:Quantity
!                            end !loop
!                            access:estparts.restorefile(save_epr_id)
!                            f_parts = parts_total$
!                            f_RRCParts = RRCParts_Total$
!                        Else !If cha:zero_parts <> 'YES'
!                            f_parts = 0
!                            f_RRCParts = 0
!                        End !If cha:zero_parts <> 'YES'
!
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                        Else!if access:subtracc.fetch(sub:account_number_key)
!                            access:tradeacc.clearkey(tra:account_number_key)
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                            end!if access:tradeacc.fetch(tra:account_number_key)
!                        end!if access:subtracc.fetch(sub:account_number_key)
!                        use_standard# = 1
!                        If TRA:ZeroChargeable = 'YES'
!                            f_parts = 0
!                        End!If TRA:ZeroChargeable = 'YES'
!
!                        If tra:invoice_sub_accounts = 'YES'
!                            access:subchrge.clearkey(suc:model_repair_type_key)
!                            suc:account_number = job:account_number
!                            suc:model_number   = job:model_number
!                            suc:charge_type    = job:charge_type
!                            suc:unit_type      = job:unit_type
!                            suc:repair_type    = job:repair_type
!                            if access:subchrge.fetch(suc:model_repair_type_key)
!                                access:trachrge.clearkey(trc:account_charge_key)
!                                trc:account_number = sub:main_account_number
!                                trc:model_number   = job:model_number
!                                trc:charge_type    = job:charge_type
!                                trc:unit_type      = job:unit_type
!                                trc:repair_type    = job:repair_type
!                                if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                    f_labour    = trc:cost
!                                    f_RRCRate   = trc:RRCRate
!                                    use_standard# = 0
!                                End!if access:trachrge.fetch(trc:account_charge_key)
!                            Else
!                                f_labour    = suc:cost
!                                f_RRCRate   = suc:RRCRate
!                                use_standard# = 0
!                            End!if access:subchrge.fetch(suc:model_repair_type_key)
!                        Else!If tra:use_sub_accounts = 'YES'
!                            access:trachrge.clearkey(trc:account_charge_key)
!                            trc:account_number = sub:main_account_number
!                            trc:model_number   = job:model_number
!                            trc:charge_type    = job:charge_type
!                            trc:unit_type      = job:unit_type
!                            trc:repair_type    = job:repair_type
!                            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
!                                f_labour    = trc:cost
!                                f_RRCRate   = trc:RRCRate
!                                use_standard# = 0
!                            End!if access:trachrge.fetch(trc:account_charge_key)
!                        End!If tra:use_sub_accounts = 'YES'
!
!                        If use_standard# = 1
!                            access:stdchrge.clearkey(sta:model_number_charge_key)
!                            sta:model_number = job:model_number
!                            sta:charge_type  = job:charge_type
!                            sta:unit_type    = job:unit_type
!                            sta:repair_type  = job:repair_type
!                            if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour     = 0
!                                f_pass       = 0
!                            Else !if access:stdchrge.fetch(sta:model_number_charge_key)
!                                f_labour    = sta:cost
!                                f_RRCRate   = sta:RRCRate
!                            end !if access:stdchrge.fetch(sta:model_number_charge_key)
!                        End!If use_standard# = 1
!
!                    End !If cha:no_charge = 'YES'
!                End !If access:chartype.clearkey(cha:charge_type_key)
!            End !If job:estimate = 'YES'
!    End!Case f_type
!
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Check_Access         PROCEDURE  (f_area,f_access)     ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:USERS_ALIAS.Open
   Relate:ACCAREAS_ALIAS.Open
! security check
    f_access    = False
    If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
        f_access = True
    Else!If password = 'JOB:ENTER'
        access:users_alias.clearkey(use_ali:password_key)
        use_ali:password    = glo:password
        If access:users_alias.fetch(use_ali:password_key) = Level:Benign
            access:accareas_alias.clearkey(acc_ali:access_level_key)
            acc_ali:user_level = use_ali:user_level
            acc_ali:access_area = f_area
            If access:accareas_alias.fetch(acc_ali:access_level_key) = Level:Benign
                f_access = True
            End
        End
    End!If password = 'JOB:ENTER'
   Relate:USERS_ALIAS.Close
   Relate:ACCAREAS_ALIAS.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Total_Price          PROCEDURE  (f_type,f_vat,f_total,f_balance) ! Declare Procedure
paid_chargeable_temp REAL
paid_warranty_temp   REAL
labour_rate_temp     REAL
parts_rate_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_jobpaymt_alias_id   ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !How much has been paid?

    Paid_Warranty_Temp = 0
    Paid_Chargeable_Temp = 0
    Save_JOBPAYMT_ALIAS_ID = Access:JOBPAYMT_ALIAS.SaveFile()
    Access:JOBPAYMT_ALIAS.Clearkey(jpt_ali:All_Date_Key)
    jpt_ali:Ref_Number = job:Ref_Number
    Set(jpt_ali:All_Date_Key,jpt_ali:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT_ALIAS.Next()
            Break
        End ! If Access:JOBPAYMT_ALIAS.Next()
        If jpt_ali:Ref_Number <> job:Ref_Number
            Break
        End ! If jpt_ali:Ref_Number <> job:Ref_Number

        Paid_Chargeable_Temp += jpt_ali:Amount
    End ! Loop
    Access:JOBPAYMT_ALIAS.RestoreFile(Save_JOBPAYMT_ALIAS_ID)

    !Look up VAT Rates
    Labour_Rate_Temp = 0
    Parts_Rate_Temp = 0

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Labour_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:VAT_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    Parts_Rate_Temp = vat:VAT_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    Case f_Type
    Of 'C' !Chargeable
        If glo:WebJob
            f_vat    = jobe:RRCCLabourCost * (labour_rate_temp/100) + |
                        jobe:RRCCPartsCost * (parts_rate_temp/100) + |
                        job:courier_cost * (labour_rate_temp/100)

            f_total = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:courier_cost + f_vat

            f_balance = f_total - paid_chargeable_temp

        Else !If glo:WebJob
            f_vat    = job:labour_cost * (labour_rate_temp/100) + |
                        job:parts_cost * (parts_rate_temp/100) + |
                        job:courier_cost * (labour_rate_temp/100)

            f_total = job:labour_cost + job:parts_cost + job:courier_cost + f_vat

            f_balance = f_total - paid_chargeable_temp
        End !If glo:WebJob
    Of 'W' ! Warranty
        If glo:WebJob
            f_vat    = jobe:RRCWLabourCost * (labour_rate_temp/100) + |
                        jobe:RRCWPartsCost * (parts_rate_temp/100) + |
                        job:courier_cost_warranty * (labour_rate_temp/100)

            f_total = jobe:RRCWLabourCost + jobe:RRCWPartsCost + job:courier_cost_warranty + f_vat

        Else !If glo:WebJob
            f_vat    = job:labour_cost_warranty * (labour_rate_temp/100) + |
                        job:parts_cost_warranty * (parts_rate_temp/100) + |
                        job:courier_cost_warranty * (labour_rate_temp/100)

            f_total = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty + f_vat

        End !If glo:WebJob
    Of 'E' !Estimate
        If jobe:WebJob
            f_vat    = jobe:RRCELabourCost * (labour_rate_temp/100) + |
                        jobe:RRCEPartsCost * (parts_rate_temp/100) + |
                        job:courier_cost_estimate * (labour_rate_temp/100)

            f_total = jobe:RRCELabourCost + jobe:RRCEPartsCost + job:courier_cost_estimate + f_vat

            f_balance = 0

        Else !If jobe:WebJob
            f_vat    = job:labour_cost_estimate * (labour_rate_temp/100) + |
                        job:parts_cost_estimate * (parts_rate_temp/100) + |
                        job:courier_cost_estimate * (labour_rate_temp/100)

            f_total = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate + f_vat

            f_balance = 0

        End !If jobe:WebJob
    Of 'I' ! Chargealbe Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If glo:webJOb
                f_vat    = jobe:InvRRCCLabourCost * (inv:vat_rate_labour/100) + |
                            jobe:InvRRCCPartsCost * (inv:vat_rate_parts/100) + |
                            job:invoice_courier_cost * (inv:vat_rate_labour/100)

                f_total = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:invoice_courier_cost + f_vat

                f_balance = f_total - paid_chargeable_temp
            Else !If glo:webJOb
                f_vat    = job:invoice_labour_cost * (inv:vat_rate_labour/100) + |
                            job:invoice_parts_cost * (inv:vat_rate_parts/100) + |
                            job:invoice_courier_cost * (inv:vat_rate_labour/100)

                f_total = job:invoice_labour_cost + job:invoice_parts_cost + job:invoice_courier_cost + f_vat

                f_balance = f_total - paid_chargeable_temp
            End !If glo:webJOb

        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    Of 'V' ! Warranty Invoice
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number_Warranty
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If glo:WebJob
                f_vat    = jobe:InvRRCWLabourCost * (inv:vat_rate_labour/100) + |
                            jobe:InvRRCWPartsCost * (inv:vat_rate_parts/100) + |
                            job:Winvoice_courier_cost * (inv:vat_rate_labour/100)
                f_total = jobe:InvRRCWLabourCost + jobe:InvRRCWPartsCost + |
                            job:Winvoice_courier_cost + f_vat

                f_balance = f_total - paid_chargeable_temp
            Else !If glo:WebJob
                f_vat    = job:Winvoice_labour_cost * (inv:vat_rate_labour/100) + |
                            job:Winvoice_parts_cost * (inv:vat_rate_parts/100) + |
                            job:Winvoice_courier_cost * (inv:vat_rate_labour/100)
                f_total = job:Winvoice_labour_cost + job:Winvoice_parts_cost + |
                            job:Winvoice_courier_cost + f_vat

                f_balance = f_total - paid_chargeable_temp
            End !If glo:WebJob
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! Case f:Type

    f_Vat = Round(f_Vat,.01)
    f_Total = Round(f_Total,.01)
    f_Balance = Round(f_Balance,.01)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Esn_Model_Routine    PROCEDURE  (f_esn,f_model_number,f_new_model_number,f_pass) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_esn_id     ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    f_pass = 0
    add_model# = 0
    If f_esn <> 'N/A' And f_esn <> ''
        If f_model_number <> ''
            found# = 1
            access:esnmodel.clearkey(esn:esn_key)
            esn:esn          = Sub(f_esn,1,6)
            esn:model_number = f_model_number
            if access:esnmodel.fetch(esn:esn_key)
                found# = 0
            end!if access:esnmodel.fetch(esn:esn_key)

            If found# = 0
                setcursor(cursor:wait)
                count# = 0
                save_esn_id = access:esnmodel.savefile()
                access:esnmodel.clearkey(esn:esn_only_key)
                esn:esn = Sub(f_esn,1,6)
                set(esn:esn_only_key,esn:esn_only_key)
                loop
                    if access:esnmodel.next()
                       break
                    end !if
                    if esn:esn <> Sub(f_esn,1,6)      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    If count# > 1
                        Break
                    End!If count# > 1
                end !loop
                access:esnmodel.restorefile(save_esn_id)
                setcursor()
                If count# = 0
                    f_new_model_number = f_model_number
                    add_model# = 1
                End!If count# = 0

                If count# = 1
                    access:esnmodel.clearkey(esn:esn_only_key)
                    esn:esn = Sub(f_esn,1,6)
                    if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
                        Case Missive('There is a mismatch between the selected Model Number and the entered I.M.E.I. Number.'&|
                          '<13,10>This I.M.E.I. Number corresponds to a ' & CLip(esn:Model_Number) & '.'&|
                          '<13,10>Do you wish to CHANGE the model, or IGNORE the mismatch?','ServiceBase 3g',|
                                       'mquest.jpg','\Cancel|Ignore|Change')
                            Of 3 ! Change Button
                                f_new_model_number = esn:model_number
                                f_pass = 1
                            Of 2 ! Ignore Button
                                f_pass = 1
                                f_new_model_number = f_model_number
                                add_model# = 1
                            Of 1 ! Cancel Button
                        End ! Case Missive
                    end!if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
                End!If count# = 1
                If count# > 1
                    Case Missive('There is a mismatch between the selected Model Number and the entered I.M.E.I. Number.'&|
                      '<13,10>This I.M.E.I. Number corresponds to more than one Model Number.'&|
                      '<13,10>Do you wish to SELECT a Model Number, or IGNORE the mismatch?','ServiceBase 3g',|
                                   'mquest.jpg','\Cancel|Ignore|Select')
                        Of 3 ! Select Button
                            saverequest#      = globalrequest
                            globalresponse    = requestcancelled
                            globalrequest     = selectrecord
                            glo:select7 = f_esn
                            select_esn_model
                            glo:select7 = ''
                            if globalresponse = requestcompleted
                                f_new_model_number = esn:model_number
                                f_pass = 1
                            end
                            globalrequest     = saverequest#
                        Of 2 ! Ignore Button
                            f_new_model_number = f_model_number
                            f_pass = 1
                           add_model# = 1
                        Of 1 ! Cancel Button
                    End ! Case Missive
                End!If count# > 1
            End!If found# = 0
        Else!If job:model_number <> ''
            count# = 0
            setcursor(cursor:wait)
            save_esn_id = access:esnmodel.savefile()
            access:esnmodel.clearkey(esn:esn_only_key)
            esn:esn = Sub(f_esn,1,6)
            set(esn:esn_only_key,esn:esn_only_key)
            loop
                if access:esnmodel.next()
                   break
                end !if
                if esn:esn <> Sub(f_esn,1,6)      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                count# += 1
                If count# > 1
                    Break
                End!If count# > 1
            end !loop
            access:esnmodel.restorefile(save_esn_id)
            setcursor()

            If count# = 1
                access:esnmodel.clearkey(esn:esn_only_key)
                esn:esn = Sub(f_esn,1,6)
                if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
                    f_new_model_number    = esn:model_number
                    f_pass = 1
                end!if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
            End!If count# = 1

            If count# > 1

                saverequest#      = globalrequest
                globalresponse    = requestcancelled
                globalrequest     = selectrecord
                glo:select7 = f_esn
                select_esn_model
                glo:select7 = ''
                if globalresponse = requestcompleted
                    f_new_model_number = esn:model_number
                    f_pass = 1
                end
                globalrequest     = saverequest#

            End!If count# > 1
        End!If job:model_number <> ''

        If add_model# = 1
            get(esnmodel,0)
            if access:esnmodel.primerecord() = Level:Benign
                esn:esn           = f_esn
                esn:model_number  = f_new_model_number
                Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                mod:Model_Number    = f_new_Model_Number
                If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                    !Found
                    esn:Manufacturer = mod:Manufacturer
                Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                    !Error
                End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                access:esnmodel.insert()
            end!if access:esnmodel.primerecord() = Level:Benign
        End!If add_model# = 1
    End!If f_esn <> 'N/A'

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Weekend_Routine      PROCEDURE  (f_date,f_days,f_end_date) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(defaults)
    access:defaults.next()
    f_end_date = f_date
    count# = 0
    Loop
        f_end_date += 1
        If def:include_saturday <> 'YES'
            If (f_end_date) % 7 = 6
                Cycle
            End!If (ord:date + f_end_date) % 7 = 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If (f_end_date) % 7 = 0
                Cycle
            End!If (ord:date + f_end_date) % 7 = 6
        End!If def:include_saturday <> 'YES'
        count# += 1
        If count# >= f_days
            Break
        End
    End!Loop
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SecurityCheck        PROCEDURE  (f_area)              ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
! security check
    rtnValue# = TRUE
    If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
        Return FALSE
    Else!If password = 'JOB:ENTER'
        Relate:USERS_ALIAS.Open()
        Relate:ACCAREAS_ALIAS.Open()

        access:users_alias.clearkey(use_ali:password_key)
        use_ali:password    = glo:password
        If access:users_alias.fetch(use_ali:password_key) = Level:Benign
            access:accareas_alias.clearkey(acc_ali:access_level_key)
            acc_ali:user_level = use_ali:user_level
            acc_ali:access_area = f_area
            If access:accareas_alias.fetch(acc_ali:access_level_key) = Level:Benign
                rtnValue# = FALSE
            End
        End
        Relate:USERS_ALIAS.Close()
        Relate:ACCAREAS_ALIAS.Close()
    End!If password = 'JOB:ENTER'

    Return rtnValue#

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AccessoryCheck       PROCEDURE  (f_type)              ! Declare Procedure
save_lac_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Case f_type
        OF 'LOAN'
            !Accessory list should show the accessories for the Loan's Model Number
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found

            Else ! If Access:LOAN.Tryfetc(loa:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign

            glo:Select1    = job:ref_number
            glo:Select2    = ''
            glo:Select12    = loa:Model_Number
            Validate_Job_Accessories
            If globalresponse <> 1
                Return(3)
            End!If globalresponse   = 2


            !Check For Missing Accessory
            save_lac_id    = access:loanacc.savefile()
            access:loanacc.clearkey(lac:ref_number_key)
            lac:ref_number    = job:loan_unit_number
            set(lac:ref_number_key,lac:ref_number_key)
            Loop
                If access:loanacc.next()
                    Break
                End!If access:jobacc.next()
                If lac:Ref_number <> job:loan_unit_number
                    Break
                End!If lac:Ref_number <> job:loan_unit_number

                Sort(glo:queue,glo:pointer)
                glo:pointer = lac:accessory
                Get(glo:queue,glo:pointer)
                If Error()
                    Case Missive('This unit has a missing accessory.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                    Return(1)
                    Break
                End!If Error()
            End!Loop

            !Check For Mismatch

            Loop x# = 1 To Records(glo:queue)
                Get(glo:queue,x#)
                access:loanacc.clearkey(lac:ref_number_key)
                lac:ref_number  = job:loan_unit_number
                lac:accessory   = glo:pointer
                If access:loanacc.tryfetch(lac:ref_number_key)
                    Case Missive('There is a mismatch between the selected Accessories and the Accessories booked in with the unit.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                    Return(2) !Mismatch Accessory
                End!If access:jobacc.tryfetch(jac:ref_number_key)
            End!Loop x# = 1 To Records(glo:queue)

        ELSE

            glo:Select1    = job:ref_number
            glo:Select2    = ''
            glo:Select12    = job:model_number
            Validate_Job_Accessories
            If globalresponse   <> 1
                Return(3)
            End!If globalresponse   = 2

            !Check For Missing Accessory
            save_jac_id    = access:jobacc.savefile()
            access:jobacc.clearkey(jac:ref_number_key)
            jac:ref_number    = job:ref_number
            set(jac:ref_number_key,jac:ref_number_key)
            Loop
                If access:jobacc.next()
                    Break
                End!If access:jobacc.next()
                If jac:ref_number    <> job:ref_number
                    Break
                End!If jac:ref_number    <> job:ref_number

                !Only count accessories that have been attached, or received
                !at the ARC - 4285 (DBH: 11-06-2004)
! ---- Also apply "attached" rule to ARC accessories ----
! Changing:  DBH 27/01/2009 #10658
!                If f_type = 'JOB' and ~glo:WebJob

! to: DBH 27/01/2009 # 10658
                If (f_type = 'JOB' Or f_type = 'EXCHANGE') And ~glo:WebJob
! End: DBH 27/01/2009 #10658
! -----------------------------------------
                    If ~jac:Attached
                        Cycle
                    End !If ~jac:Attached
                End !If f_type = 'JOB'

                Sort(glo:queue,glo:pointer)
                glo:pointer = jac:accessory
                Get(glo:queue,glo:pointer)
                If Error()
                    Case Missive('This unit has a missing accessory.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                    Return(1)
                    Break
                End!If Error()
            End!Loop

            !Check For Mismatch

            Loop x# = 1 To Records(glo:queue)
                Get(glo:queue,x#)
                access:jobacc.clearkey(jac:ref_number_key)
                jac:ref_number  = job:ref_number
                jac:accessory   = glo:pointer
                If access:jobacc.tryfetch(jac:ref_number_key)
                    Case Missive('There is a mismatch between the selected Accessories and the Accessories included with the unit.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                    Return(2) !Mismatch Accessory
                Else !!If access:jobacc.tryfetch(jac:ref_number_key)
                    !Accessory found, but was it attached (or sent)
                    !to the ARC? - 4285 (DBH: 11-06-2004)
                    If ~glo:WebJob
                        If ~jac:Attached
                            Case Missive('There is a mismatch between the selected Accessories and the Accessories included with the unit.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                            Of 1 ! OK Button
                            End ! Case Missive
                            Return(2) !Mismatch Accessory
                        End !If ~jac:Attached
                    End !If ~glo:WebJob

                End!If access:jobacc.tryfetch(jac:ref_number_key)
            End!Loop x# = 1 To Records(glo:queue)


    End!Case f_type
    Return Level:Benign

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CheckRefurb          PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type    = job:transit_type
    If access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        If trt:exchange_unit    = 'YES' Or job:exchange_unit_number <> '' Or Sub(job:exchange_status,1,3) = '108'
            access:subtracc.clearkey(sub:account_number_key)
            sub:account_number  = job:account_number
            If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number  = sub:main_account_number
                If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                    If tra:refurbcharge = 'YES'
                        Return Level:Fatal
                    End!If tra:refurbcharge = 'YES'
                End!If access:tradeacc.tryfetch(tra:account_number_key)
            End!If access:subtracc.tryfetch(sub:account_number_key)
        End!If trt:exchange_unit    = 'YES' Or job:exchange_unit_number <> '' Or Sub(job:exchange_status,1,3) = '108'
    End!If access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
