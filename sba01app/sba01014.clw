

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01014.INC'),ONCE        !Local module procedure declarations
                     END


IsUnitLiquidDamaged  PROCEDURE  (long f:JobNumber, String f:ESN,<BYTE ignoreARCCheck>) ! Declare Procedure
tmp:JobDate          DATE
save_jobe_ali_id     USHORT,AUTO
save_job_ali_id      USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBSE_ALIAS.Open
   Relate:JOBS_ALIAS.Open
    Return (VodacomClass.IsUnitLiquidDamage(f:ESN,f:JobNumber,ignoreARCCheck))

    ! Use common function here and in SBOnline

!    Found# = False
!    Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
!    Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
!    job_ali:ESN = f:ESN
!    Set(job_ali:ESN_Key,job_ali:ESN_Key)
!    Loop
!        If Access:JOBS_ALIAS.NEXT()
!           Break
!        End !If
!        If job_ali:ESN <> f:ESN |
!            Then Break.  ! End If
!
!        If job_ali:Ref_Number = f:JobNumber
!            Cycle
!        End ! If job_ali:Ref_Number <> job:Ref_Number
!
!        ! Only check historic jobs (DBH: 27-06-2006)
!        If job_ali:Ref_Number > f:JobNumber
!            Cycle
!        End ! If job_ali:Ref_Number > job:Ref_Number
!
!        ! Only get the latest job (DBH: 27-06-2006)
!        If job_ali:Date_Booked < tmp:JobDate
!            Cycle
!        End ! If job_ali:Date_Booked > tmp:JobDate
!
!        tmp:JobDate = job_ali:Date_Booked
!
!        ! Only look for jobs that were sent to or booked by the ARC (DBH: 27-06-2006)
!        Access:JOBSE_ALIAS.Clearkey(jobe_ali:RefNumberKey)
!        jobe_ali:RefNumber = job_ali:Ref_Number
!        If Access:JOBSE_ALIAS.Tryfetch(jobe_ali:RefNumberKey) = Level:Benign
!            ! Found
!            If jobe_ali:HubRepair = 0 And jobe_ali:HubRepairDate = ''
!                Cycle
!            End ! If jobe_ali:HubRepair Or jobe:HubRepairDate <> ''
!        Else ! If Access:JOBSE_ALIAS.Tryfetch(jobe_ali:Ref_Number_Key) = Level:Benign
!            ! Error
!        End ! If Access:JOBSE_ALIAS.Tryfetch(jobe_ali:Ref_Number_Key) = Level:Benign
!
!        ! Check to see if the job was marked as Liquid Damage (DBH: 27-06-2006)
!        If job_ali:Chargeable_Job = 'YES'
!            If job_ali:Warranty_Job = 'YES'
!                If job_ali:Repair_Type = 'LIQUID DAMAGE' Or job_ali:Repair_Type_Warranty = 'LIQUID DAMAGE'
!                    Found# = True
!                Else ! If job_ali:Repair_Type = 'LIQUID DAMAGE' Or job_ali:Repair_Type_Warranty = 'LIQUID DAMAGE'
!                    Found# = False
!                End ! If job_ali:Repair_Type = 'LIQUID DAMAGE' Or job_ali:Repair_Type_Warranty = 'LIQUID DAMAGE'
!            Else ! If job_ali:Warranty_Job = 'YES'
!                If job_ali:Repair_Type = 'LIQUID DAMAGE'
!                    Found# = True
!                Else ! If job_ali:Repair_Type = 'LIQUID DAMAGE'
!                    Found# = False
!                End ! If job_ali:Repair_Type = 'LIQUID DAMAGE'
!            End ! If job_ali:Warranty_Job = 'YES'
!        Else ! If job_ali:Chargeable_Job = 'YES'
!            If job_ali:Warranty_Job = 'YES'
!                If job_ali:Repair_Type_Warranty = 'LIQUID DAMAGE'
!                    Found# = True
!                Else ! If job_ali:Repair_Type_Warranty = 'LIQUID DAMAGE'
!                    Found# = False
!                End ! If job_ali:Repair_Type_Warranty = 'LIQUID DAMAGE'
!            End ! If job_ali:Warranty_Job = 'YES'
!        End ! If job_ali:Chargeable_Job = 'YES'
!    End !Loop
!    Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
!
!    Return Found#
   Relate:JOBSE_ALIAS.Close
   Relate:JOBS_ALIAS.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
WeekNumber           PROCEDURE  (f:TheDate)           ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
iwdnGroup                              Group, Type
lDay                                     Long              ! Day of date to check.
lMonth                                   Long              ! Month of date to check.
lYear                                    Long              ! Year of date to check.
lDOY                                     Long              ! On return: ordinal day of the year (lYear)
lWOY                                     Long              ! On return: week of the year (lYear)
lNewYear                                 Long              ! On return: year the week is actually in.
                                       End

iwd                                    Group(iwdnGroup).
    Map
        isDayWeekNum                       (*iwdnGroup iwdnGrp)
    End ! Map
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    iwd:lDay        = Day(f:TheDate)
    iwd:lMonth      = Month(f:TheDate)
    iwd:lYear       = Year(f:TheDate)

    isDayWeekNum(iwd)
    Return(iwd:lWOY)

isDayWeekNum                           Procedure(*iwdnGroup iGrp)
! ***************************************************************************************
! * This procedure was developed using information obtained from:                       *
! *                                                                                     *
! *   Frequently Asked Questions about Calendars Version 2.8. You can obtain a copy at: *
! *                                                                                     *
! *                   http://www.tondering.dk/claus/calendar.html                       *
! ***************************************************************************************
a                                      Long
b                                      Long
c                                      Long
s                                      Long
e                                      Long
f                                      Long
g                                      Long
d                                      Long
n                                      Long
weekOfYear                             Long
dayOfYear                              Long
  CODE
  Case iGrp.lMonth
  Of 1 OrOf 2
    a = iGrp.lYear - 1
    b = Int(a / 4) - Int(a / 100) + Int(a / 400)
    c = Int((a - 1) / 4) - Int((a - 1) / 100) + Int((a - 1) / 400)
    s = b - c
    e = 0
    f = iGrp.lDay - 1 + 31 * (iGrp.lMonth - 1)
  Else
    a = iGrp.lYear
    b = Int(a / 4) - Int(a / 100) + Int(a / 400)
    c = Int((a - 1) / 4) - Int((a - 1) / 100) + Int((a - 1) / 400)
    s = b - c
    e = s + 1
    f = iGrp.lDay + Int((153 * (iGrp.lMonth - 3) + 2) / 5) + 58 + s
  End

  g = (a + b) % 7
  d = (f + g - e) % 7
  n = f + 3 - d

  If n < 0 Then
    iGrp.lWOY = 53 - Int((g - s) / 5)
    iGrp.lNewYear = iGrp.lYear - 1
  ElsIf n > (364 + s) Then
    iGrp.lWOY = 1
    iGrp.lNewYear = iGrp.lYear + 1
  Else
    iGrp.lWOY = Int(n / 7) + 1
    iGrp.lNewYear = iGrp.lYear
  End

  iGrp.lDOY = f + 1
! ***************************************************************************************
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SelectIMEIModelNumber PROCEDURE (f:IMEINumber)        !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
tmp:ModelNumber      STRING(30)
tmp:IMEINumber       STRING(30)
QuickWindow          WINDOW('Sellfone'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By ESN'),USE(?Tab:3)
                           PROMPT('You have selected the I.M.E.I. Number:'),AT(217,138,148,16),USE(?Prompt1),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(365,138),USE(tmp:IMEINumber),TRN,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI)
                           PROMPT('The following Model Number correspond to that I.M.E.I. Number. Please select one' &|
   '.'),AT(184,150,312,16),USE(?Prompt2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(264,164,148,160),USE(?List1),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('120L(2)|M~Model Number~@s30@'),FROM(glo:Q_ModelNumber)
                           BUTTON,AT(445,164),USE(?Button:Select),TRN,FLAT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Model Number'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ModelNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020001'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SelectIMEIModelNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:IMEINumber = f:IMEINumber
      ! Save Window Name
   AddToLog('Window','Open','SelectIMEIModelNumber')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','SelectIMEIModelNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:Select
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Select, Accepted)
      Get(glo:Q_ModelNumber,Choice(?List1))
      tmp:ModelNumber = glo:Model_Number_Pointer
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Select, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020001'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020001'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020001'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

PreviousIMEI         PROCEDURE  (f:JobNumber,f:IMEI)  ! Declare Procedure
save_jobs_alias_id   USHORT,AUTO
save_exchange_id     USHORT,AUTO
save_jobthird_id     USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
        Return False
    End ! If Clip(f:IMEI) = '' Or f:IMEI = 'N/A'
    Found# = False
    Save_JOBS_ALIAS_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
    job_ali:ESN = f:IMEI
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop ! Begin Loop
        If Access:JOBS_ALIAS.Next()
            Break
        End ! If Access:JOBS_ALIAS.Next()
        If job_ali:ESN <> f:IMEI
            Break
        End ! If job_ali:ESN <> f:IMEI
        ! Don't find the same job (DBH: 21/11/2006)
        If job_ali:Ref_number = f:JobNumber
            Cycle
        End ! If job_ali:Ref_number = f:JobNumber
        Found# = True
        Break
    End ! Loop
    Access:JOBS_ALIAS.RestoreFile(Save_JOBS_ALIAS_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_JOBTHIRD_ID = Access:JOBTHIRD.SaveFile()
    Access:JOBTHIRD.Clearkey(jot:OriginalIMEIKey)
    jot:OriginalIMEI     = f:IMEI
    Set(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
    Loop ! Begin Loop
        If Access:JOBTHIRD.Next()
            Break
        End ! If Access:JOBTHIRD.Next()
        If jot:OriginalIMEI <> f:IMEI
            Break
        End ! If jot:OriginalIMEI <> f:IMEI
        ! Don't find the same job (DBH: 21/11/2006)
        If jot:RefNumber = f:JobNumber
            Cycle
        End ! If jot:RefNumber = f:JobNumber
        Found# = True
        Break
    End ! Loop
    Access:JOBTHIRD.RestoreFile(Save_JOBTHIRD_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
    Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
    xch:ESN = f:IMEI
    Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
    Loop ! Begin Loop
        If Access:EXCHANGE.Next()
            Break
        End ! If Access:EXCHANGE.Next()
        IF xch:ESN <> f:IMEI
            Break
        End ! IF xch:ESN <> f:IMEI
        If xch:Job_Number <> ''
            Found# = True
            Break
        End ! If xch:Job_Number <> ''
    End ! Loop
    Access:EXCHANGE.RestoreFile(Save_EXCHANGE_ID)

    If Found# = True
        Return True
    End ! If Found# = True

    Return False



! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
BuildIMEISearch      PROCEDURE  (f:IMEINumber)        ! Declare Procedure
save_job_ali_id      USHORT,AUTO
save_jot_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
    job_ali:ESN = f:IMEINumber
    Set(job_ali:ESN_Key,job_ali:ESN_Key)
    Loop
        If Access:JOBS_ALIAS.NEXT()
           Break
        End !If
        If job_ali:ESN <> f:IMEINumber |
            Then Break.  ! End If

        If Access:ADDSEARCH.PrimeRecord() = Level:Benign
            addtmp:AddressLine1  = job_ali:Address_Line1
            addtmp:AddressLine2  = job_ali:Address_Line2
            addtmp:AddressLine3  = job_ali:Address_Line3
            addtmp:Postcode      = job_ali:Postcode
            addtmp:JobNumber     = job_ali:Ref_Number
            addtmp:FinalIMEI     = job_ali:ESN
            addtmp:Surname       = job_ali:Surname
            If Access:ADDSEARCH.TryInsert() = Level:Benign
                !Insert Successful
            Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                !Insert Failed
            End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
        End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
    End !Loop
    Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)

    Save_jot_ID = Access:JOBTHIRD.SaveFile()
    Access:JOBTHIRD.ClearKey(jot:OriginalIMEIKey)
    jot:OriginalIMEI = f:IMEINumber
    Set(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
    Loop
        If Access:JOBTHIRD.NEXT()
           Break
        End !If
        If jot:OriginalIMEI <> f:IMEINumber      |
            Then Break.  ! End If
        Access:ADDSEARCH.ClearKey(addtmp:JobNumberKey)
        addtmp:JobNumber = jot:RefNumber
        If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
            !Found
            addtmp:IncomingIMEI  = jot:OriginalIMEI
            Access:ADDSEARCH.Update()
        Else!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
            !Error
! Inserting (DBH 06/12/2006) # 8559 - Make sure the job still exists
            Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
            job_ali:Ref_Number = jot:RefNumber
            If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
! End (DBH 06/12/2006) #8559
                If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                    addtmp:IncomingIMEI  = jot:OriginalIMEI
                    addtmp:JobNumber     = jot:RefNumber
                    addtmp:AddressLine1  = job_ali:Address_Line1
                    addtmp:AddressLine2  = job_ali:Address_Line2
                    addtmp:AddressLine3  = job_ali:Address_Line3
                    addtmp:Postcode      = job_ali:Postcode
                    addtmp:JobNumber     = job_ali:Ref_Number
                    addtmp:Surname       = job_ali:Surname
                    If Access:ADDSEARCH.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                        !Insert Failed
                    End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
                End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
            Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
                !Error
            End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        End!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign

    End !Loop
    Access:JOBTHIRD.RestoreFile(Save_jot_ID)

    Save_xch_ID = Access:EXCHANGE.SaveFile()
    Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
    xch:ESN = f:IMEINumber
    Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
    Loop
        If Access:EXCHANGE.NEXT()
           Break
        End !If
        If xch:ESN <> f:IMEINumber    |
            Then Break.  ! End If
        If xch:Job_number <> ''
            Access:ADDSEARCH.ClearKey(addtmp:JobNumberKey)
            addtmp:JobNumber = xch:Job_Number
            If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
                !Found
                addtmp:ExchangedIMEI    = xch:ESN
                Access:ADDSEARCH.Update()
            Else!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
                job_ali:Ref_Number  = xch:Job_Number
                If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                    !Found
                    If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                        addtmp:AddressLine1  = job_ali:Address_Line1
                        addtmp:AddressLine2  = job_ali:Address_Line2
                        addtmp:AddressLine3  = job_ali:Address_Line3
                        addtmp:Postcode      = job_ali:Postcode
                        addtmp:JobNumber     = job_ali:Ref_Number
                        addtmp:Surname       = job_ali:Surname
                        If Access:ADDSEARCH.TryInsert() = Level:Benign
                            !Insert Successful
                        Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                            !Insert Failed
                        End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
                    End ! If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
            End!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
        End !if xch:Job_number <> ''
    End !Loop
    Access:EXCHANGE.RestoreFile(Save_xch_ID)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceTransitType     PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Initial Transit Type
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    If (def:Force_Initial_Transit_Type ='B' and func:Type = 'B') Or |
        (def:Force_Initial_Transit_Type <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceIMEI            PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !ESN
    If (def:Force_ESN = 'B' And func:Type = 'B') Or |
        (def:Force_ESN <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_ESN = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceMSN             PROCEDURE  (func:Manufacturer,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !MSN
    If (def:Force_MSN = 'B' And func:Type = 'B') Or |
        (def:Force_MSN <> 'I' And func:Type = 'C')
        If MSNRequired(func:Manufacturer) = Level:Benign

            Return Level:Fatal
        End!If MSNRequired(job:Manufacturer) = Level:Benign
    End!If def:Force_MSN = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceModelNumber     PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Model Number
    If (def:Force_Model_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Model_Number <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Model_Number = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceUnitType        PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Unit Type
    If (def:Force_Unit_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Unit_Type <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Unit_Type = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
