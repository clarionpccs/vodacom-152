

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01020.INC'),ONCE        !Local module procedure declarations
                     END


IsTheJobAlreadyInBatch PROCEDURE  (LONG jobNumber,STRING accountNumber) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.IsTheJobAlreadyInBatch(jobNumber,accountNumber))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
RepairTypesNoParts   PROCEDURE  (STRING chargeableJob,STRING warrantyJob,STRING manufacturer,STRING repairTypeC,STRING repairTypeW) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.RepairTypesNoParts(chargeableJob,warrantyJob,manufacturer,|
            repairTypeC,repairTypeW))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobHasSparesAttached PROCEDURE  (LONG jobNumber,STRING estimateJob,STRING chargeableJob,STRING warrantyJob) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN(VodacomClass.JobHasSparesAttached(jobNumber,estimateJob,chargeableJob,warrantyJob))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetExchangePartCosts PROCEDURE  (STRING fManufacturer,STRING fModelNumber,STRING fSupplier,*REAL fPurchaseCost,*REAL fSaleCost,*BYTE fFreeStock) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    VodacomClass.GetExchangePartCosts(fManufacturer,fModelNumber,fSupplier,fPurchaseCost,fSaleCost,fFreeStock)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SetReportsFolder     PROCEDURE  (STRING fRoot,STRING fReportName,<BYTE fFolderType>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.SetReportsFolder(fRoot,fReportName,fFolderType))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetTempFolder        PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.GetTempFolder())
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetInFault           PROCEDURE  (gFaultCodes gPassedGroup,<*STRING fDescription>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.GetInFault(gPassedGroup,fDescription))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetOutFault          PROCEDURE  (gFaultCodes gPassedGroup,<*STRING fDescription>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.GetOutFault(gPassedGroup,fDescription))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ReturnUnitAlreadyOnOrder PROCEDURE  (LONG fRefNumber,BYTE fExchangeOrder,<LONG fOrderNumber>) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_RTNORDER_id        USHORT,AUTO
rtnValue                LONG(0)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.ReturnUnitAlreadyOnOrder(fRefNumber,fExchangeOrder,fOrderNumber))

!    Relate:RTNORDER.Open()
!    save_RTNORDER_id     = Access:RTNORDER.SaveFile()
!
!    Access:RTNORDER.Clearkey(rtn:RefNumberKey)
!    rtn:ExchangeOrder = fExchangeOrder
!    rtn:RefNumber = fRefNumber
!    SET(rtn:RefNumberKey,rtn:RefNumberKey)
!    LOOP UNTIL Access:RTNORDER.Next()
!        IF (rtn:ExchangeORder <> fExchangeOrder OR |
!            rtn:RefNumber <> fRefNumber)
!            BREAK
!        END
!        IF (fOrderNumber > 0)
!            IF (rtn:OrderNumber <> fOrderNumber)
!                CYCLE
!            END
!        END
!        ! Ignore received orders in case the item has been "returned" before
!        IF (rtn:Received)
!            CYCLE
!        END
!
!        If (rtn:Status = 'NEW' OR rtn:Status = 'PRO')
!            IF (rtn:ExchangeOrder)
!                rtnValue = 1
!                BREAK
!            ELSE
!                rtnValue += rtn:QuantityReturned
!            END
!        END
!    END ! IF (Access:RTNORDER.TryFetch(rtn:RefNumberKey) = Level:Benign)
!
!    Access:RTNORDER.RestoreFile(save_RTNORDER_id)
!    Relate:RTNORDER.Close()
!
!    RETURN rtnValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddStockHistory      PROCEDURE  (STRING fTran,LONG fQty,STRING fNotes,<STRING fInfo>,<LONG fJobNo>,<LONG fSaleNo>,<STRING fDesp>,<REAL fPC>,<REAL fSC>,<REAL fRC>,<REAL fPAvPC>,<REAL fPAcPC>,<REAL fPPC>,<REAL fPSC>,<REAL fPRC>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!(STRING fTran,LONG fQty,STRING fNotes,<STRING fInfo>,<LONG fJobNo>,<LONG fSaleNo>,<STRING fDesp>,<REAL fPC>,<REAL fSC>,<REAL fRC>,<REAL fPAvPC>,<REAL fPAcPC>,<REAL fPPC>,<REAL fPSC>,<REAL fPRC>)
!(STRING fTran,LONG fQty,STRING fNotes,<STRING fInfo>,<LONG fJobNo>,<LONG fSaleNo>,<STRING fDesp>,<REAL fPC>,<REAL fSC>,<REAL fRC>,<REAL fPAvPC>,<REAL fPAcPC>,<REAL fPPC>,<REAL fPSC>,<REAL fPRC>)
!<REAL fPAvPC>,
!<REAL fPAcPC>

    ! Assumes that we are in a stock record
    IF (fTran = '')
        RETURN
    END

    Relate:STOHIST.Open()
    Relate:STOHISTE.Open()

    IF (Access:STOHIST.PrimeRecord() = Level:Benign)
        shi:Ref_Number          = sto:Ref_Number
        shi:User                = glo:Usercode
        shi:Transaction_Type    = fTran
        shi:Despatch_Note_Number= fDesp
        shi:Job_Number          = fJobNo
        shi:Sales_Number        = fSaleNo
        shi:Quantity            = fQty
        shi:Date                = TODAY()
        IF (fPC = 0)
            shi:Purchase_Cost = sto:Purchase_Cost
        ELSE
            shi:Purchase_Cost = fPC
        END
        IF (fSC = 0)
            shi:Sale_Cost      = sto:Sale_Cost
        ELSE
            shi:Sale_Cost      = fSC
        END
        IF (fRC = 0)
            shi:Retail_Cost     = sto:Retail_Cost
        ELSE
            shi:Retail_Cost     = sto:Retail_Cost
        END
        shi:Notes               = fNotes
        shi:Information         = fInfo
        shi:StockOnHand         = sto:Quantity_Stock
        IF (Access:STOHIST.TryInsert())
            Access:STOHIST.CancelAutoInc()
        ELSE
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber = shi:Record_Number
                If (fPAvPC = 0 AND fPPC = 0 AND fPSC = 0)
                    stoe:PreviousAveragePurchaseCost = sto:AveragePurchaseCost
                    stoe:PurchaseCost = sto:Purchase_Cost
                    stoe:SaleCost = sto:Sale_Cost
                    stoe:RetailCost = sto:Retail_Cost
                ELSE ! If (f:PAvPC = 0 AND f:PPC = 0 AND f:PSC = 0)
                    stoe:PreviousAveragePurchaseCost = fPAvPC
                    stoe:PurchaseCost = fPPC
                    stoe:SaleCost = fPSC
                    stoe:RetailCost = fPRC
                END ! If (f:PAvPC = 0 AND f:PPC = 0 AND f:PSC = 0)
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END ! IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
        END
    END

    Relate:STOHIST.Close()
    Relate:STOHISTE.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
