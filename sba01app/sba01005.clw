

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01005.INC'),ONCE        !Local module procedure declarations
                     END


Auto_Total_Price     PROCEDURE  (f_type,f_vat,f_total,f_balance) ! Declare Procedure
paid_chargeable_temp REAL
paid_warranty_temp   REAL
labour_rate_temp     REAL
parts_rate_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_jpt_ali_id   ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    paid_warranty_temp = 0
    paid_chargeable_temp = 0
    save_jpt_ali_id = access:jobpaymt_alias.savefile()
    access:jobpaymt_alias.clearkey(jpt_ali:all_date_key)
    jpt_ali:ref_number = job:ref_number
    set(jpt_ali:all_date_key,jpt_ali:all_date_key)
    loop
        if access:jobpaymt_alias.next()
           break
        end !if
        if jpt_ali:ref_number <> job:ref_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        paid_chargeable_temp += jpt_ali:amount
    end !loop
    access:jobpaymt_alias.restorefile(save_jpt_ali_id)

    labour_rate_temp = 0
    parts_rate_temp = 0

    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job_ali:account_number
    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
            If tra:invoice_sub_accounts = 'YES'
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = sub:labour_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    labour_rate_temp = vat:vat_rate
                end
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = sub:parts_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    parts_rate_temp = vat:vat_rate
                end
            Else
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = tra:labour_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    labour_rate_temp = vat:vat_rate
                end
                access:vatcode.clearkey(vat:vat_code_key)
                vat:vat_code = tra:parts_vat_code
                if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
                    parts_rate_temp = vat:vat_rate
                end

            End!If tra:use_sub_accounts = 'YES'
        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
    end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign

    Case f_type
        Of 'C' ! Chargeable
            f_vat    = Round(job:labour_cost * (labour_rate_temp/100),.01) + |
                        Round(job_ali:parts_cost * (parts_rate_temp/100),.01) + |
                        Round(job_ali:courier_cost * (labour_rate_temp/100),.01)

            f_total = job_ali:labour_cost + job_ali:parts_cost + job_ali:courier_cost + f_vat

            f_balance = f_total - paid_chargeable_temp
        Of 'W' ! Warranty
            f_vat    = Round(job_ali:labour_cost_warranty * (labour_rate_temp/100),.01) + |
                        Round(job_ali:parts_cost_warranty * (parts_rate_temp/100),.01) + |
                        Round(job_ali:courier_cost_warranty * (labour_rate_temp/100),.01)

            f_total = job_ali:labour_cost_warranty + job_ali:parts_cost_warranty + job_ali:courier_cost_warranty + f_vat

!            f_balance = f_total - paid_warranty_temp

        Of 'E' ! estimate
            f_vat    = Round(job_ali:labour_cost_estimate * (labour_rate_temp/100),.01) + |
                        Round(job_ali:parts_cost_estimate * (parts_rate_temp/100),.01) + |
                        Round(job_ali:courier_cost_estimate * (labour_rate_temp/100),.01)

            f_total = job_ali:labour_cost_estimate + job_ali:parts_cost_estimate + job_ali:courier_cost_estimate + f_vat

            f_balance = 0

        Of 'I' ! Chargeable Invoice

            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number = job_ali:invoice_number
            if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                f_vat    = Round(job_ali:invoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                            Round(job_ali:invoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                            Round(job_ali:invoice_courier_cost * (inv:vat_rate_labour/100),.01)

                f_total = job_ali:invoice_labour_cost + job_ali:invoice_parts_cost + job_ali:invoice_courier_cost + f_vat

                f_balance = f_total - paid_chargeable_temp
            end!if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
        Of 'V' ! Warranty Invoice
            access:invoice.clearkey(inv:invoice_number_key)
            inv:invoice_number = job_ali:invoice_number_warranty
            if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                f_vat    = Round(job_ali:Winvoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                            Round(job_ali:Winvoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                            Round(job_ali:Winvoice_courier_cost * (inv:vat_rate_labour/100),.01)
                f_total = job_ali:Winvoice_labour_cost + job_ali:Winvoice_parts_cost + |
                            job_ali:Winvoice_courier_cost + f_vat

                f_balance = f_total - paid_chargeable_temp
            end!if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
    End!Case f_type
    f_vat = Round(f_vat,.01)
    f_total = Round(f_total,.01)
    f_balance = Round(f_balance,.01)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PricingStructure     PROCEDURE  (func:AccountNumber,func:ModelNumber,func:UnitType,func:ChargeType,func:RepairType) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Is there a pricing structure setup?
    Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
    sta:Model_Number = func:ModelNumber
    sta:Charge_Type  = func:ChargeType
    sta:Unit_Type    = func:UnitType
    Set(sta:Model_Number_Charge_Key,sta:Model_Number_Charge_Key)
    If Access:STDCHRGE.NEXT()

    Else !If Access:STDCHRGE.NEXT()
        If sta:Model_Number = func:ModelNumber      |
        And sta:Charge_Type  = func:ChargeType      |
        And sta:Unit_Type    = func:UnitType
            Return Level:Benign
        End !Or sta:Unit_Type    <> func:UnitType

    End !If Access:STDCHRGE.NEXT()

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
            trc:Account_Number = tra:Account_Number
            trc:Model_Number   = func:ModelNumber
            trc:Charge_Type    = func:ChargeType
            trc:Unit_Type      = func:UnitType
            Set(trc:Account_Charge_Key,trc:Account_Charge_Key)
            If Access:TRACHRGE.NEXT()

            Else !If Access:TRACHRGE.NEXT()
                If trc:Account_Number = tra:Account_Number      |
                And trc:Model_Number   = func:ModelNumber      |
                And trc:Charge_Type    = func:ChargeType      |
                And trc:Unit_Type      = func:UnitType
                    Return Level:Benign
                End !Or trc:Unit_Type      <> func:UnitType
            End !If Access:TRACHRGE.NEXT()

            If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                suc:Account_Number = func:AccountNumber
                suc:Model_Number   = func:ModelNumber
                suc:Charge_Type    = func:ChargeType
                suc:Unit_Type      = func:UnitType
                Set(suc:Model_Repair_Type_Key,suc:Model_Repair_Type_Key)
                If Access:SUBCHRGE.NEXT()

                Else !If Access:SUBCHRGE.NEXT()
                    If suc:Account_Number = func:AccountNumber      |
                    And suc:Model_Number   = func:ModelNumber      |
                    And suc:Charge_Type    = func:ChargeType      |
                    And suc:Unit_Type      = func:UnitType
                        Return Level:Benign
                    End !Or suc:Unit_Type      <> func:UnitType

                End !Loop

            End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Fatal
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CompletePrintDespatch PROCEDURE  (func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Print Despatch Note At Completion
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            IF tra:Print_Despatch_Complete = 'YES'
                Return Level:Benign
            End !IF tra:Print_Despatch_Complete = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Fatal

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CompleteCheckForBouncer PROCEDURE  (func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Check For Bouncer AT Completion?
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_accounts = 'YES'
            If sub:ExcludeBouncer
                Return Level:Fatal
            End !If sub:ExcludeBouncer
        Else !If tra:Use_Sub_Accounts = 'YES' and sub:Invoice_Sub_accounts = 'YES'
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                 If tra:excludebouncer
                    Return Level:Fatal
                End!If tra:excludebouncer
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        End !If tra:Use_Sub_Accounts = 'YES' and sub:Invoice_Sub_accounts = 'YES'

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CompleteDoExchange   PROCEDURE  (func:ExchangeUnitNumber) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Do Replacement Exchange Unit Bit At Completion
    If func:ExchangeUnitNumber <> 0
        access:exchange_alias.clearkey(xch_ali:ref_number_key)
        xch_ali:ref_number  = func:ExchangeUnitNUmber
        If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
            If xch_ali:audit_number <> ''                                               !If this has an audit number
                access:excaudit.clearkey(exa:audit_number_key)                          !Mark the replacement as ready
                exa:stock_type  = xch_ali:stock_type                                    !To be returned to stock
                exa:audit_number    = xch_ali:audit_number
                If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:stock_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = ''
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                    access:exchange.clearkey(xch:ref_number_key)
                    xch:ref_number  = exa:replacement_unit_number
                    If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        get(exchhist,0)
                        if access:exchhist.primerecord() = level:benign
                            exh:ref_number   = xch:ref_number
                            exh:date          = today()
                            exh:time          = clock()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status        = 'UNIT REPAIRED. (AUDIT:' &|
                                                Clip(exa:audit_number) & ')'
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = level:benign
                        xch:audit_number    = exa:audit_number
                        xch:available   = 'RTS'
                        xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                        access:exchange.update()
                    End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                End!If access:excaudit.tryfetch(exa:audit_number_key) = Level:Benign
            Else!If xch_ali:audit_number <> ''
                access:exchange.clearkey(xch:esn_available_key)
                xch:available  = 'REP'
                xch:stock_type = xch_ali:stock_type
                xch:esn        = job:esn
                if access:exchange.tryfetch(xch:esn_available_key) = Level:Benign
                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        exh:user = use:user_code
                        exh:status        = 'REPAIR COMPLETED'
                        access:exchhist.insert()
                    end!if access:exchhist.primerecord() = level:benign
                    xch:available = 'RTS'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    access:exchange.update()
                end!if access:exchange.tryfetch(xch:esn_available_key)
            End!If xch:audit_number <> ''
        End!If access:Exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign

    End !If func:ExchangeUnitNumber <> 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ValidateIMEI PROCEDURE (func:IMEINumber)              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:IMEINumber       STRING(30)
tmp:Error            BYTE(0)
window               WINDOW('Validate I.M.E.I. Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Validate I.M.E.I. Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Validate I.M.E.I.'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(280,206,124,10),USE(tmp:IMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Error)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020014'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ValidateIMEI')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','ValidateIMEI')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','ValidateIMEI')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020014'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020014'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020014'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      tmp:Error = 0
      If tmp:IMEINumber <> func:IMEINumber
          tmp:Error = 1
      End !tmp:IMEINumber <> func:IMEINumber
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Error = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
StripReturnToComma   PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF VAL(SUB(in_string,STR_POS#,1)) < 32 Or VAL(SUB(in_string,STR_POS#,1)) > 126
        in_string = SUB(in_string,1,STR_POS#-1) & ',' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ValidatePostcode     PROCEDURE  (f_Postcode)          ! Declare Procedure
tmp:os               STRING(20)
szpostcode           CSTRING(20)
szpath               CSTRING(255)
szaddress            CSTRING(255)
tmp:high             BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
full_postcode       String(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Postcode bit
    Setclipboard(f_postcode)
    Set(Defaults)
    access:defaults.next()
    tmp:os  = GetVersion()
    tmp:high    = Bshift(tmp:os,-8)

    If def:use_postcode = 'YES'

        If def:postcodedll <> 'YES'

            If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',0)
            Else!If tmp:high = 0
                Run(Clip(def:postcode_path) & '\addressc.exe',1)
            End!If tmp:high = 0
            if error()
                Return Level:Fatal
            Else
                IF Sub(Clipboard(),1,1) = '#'
                    Return Level:Fatal
                end!if error
            End
        Else!If def:postcodedll <> 'YES'
            szpath  = Clip(def:postcode_path)
!            If Initaddress(szpath)
!                szpostcode  = f_postcode
!                If getaddress(szpostcode,szaddress)
!                    full_postcode   = szaddress
!                Else!
!                    Return Level:Fatal
!                End!If getaddress(szpostcode,szaddress)
!            Else!If Initaddress(szpath)
                Return Level:Fatal
!            End!If Initaddress(szpath)
        End!If def:postcodedll <> 'YES'
    End
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
NormalDespatchCourier PROCEDURE  (func:Courier)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:COURIER_ALIAS.ClearKey(cou_ali:Courier_Key)
    cou_ali:Courier = func:Courier
    If Access:COURIER_ALIAS.TryFetch(cou_ali:Courier_Key) = Level:Benign
        !Found
        If cou_ali:Courier_Type     = 'ANC' Or |
            cou_ali:Courier_Type    = 'ROYAL MAIL' Or |
            cou_ali:Courier_Type    = 'PARCELINE' Or |
            cou_ali:Courier_Type    = 'UPS'
            Return Level:Fatal
        End !cou_ali:Courier_Type    = 'UPS'
    Else!If Access:COURIER_ALIAS.TryFetch(cou_ali:Courier_Key)e = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:COURIER_ALIAS.TryFetch(cou_ali:Courier_Key)e = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PendingJob           PROCEDURE  (func:Manufacturer)   ! Declare Procedure
save_jobse_id        USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_chartype_id     USHORT,AUTO
save_reptydef_id     USHORT,AUTO
save_jobswarr_id     USHORT,AUTO
save_jobse2_id       USHORT,AUTO
tmp:Return           STRING(3)
locPreClaim          BYTE,AUTO
PrimaryWarrInsert    BYTE
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    ! Inserting (DBH 05/02/2007) # 8707 - Open file and save states
    Relate:JOBSE.Open()
    Relate:JOBSE2.Open()
    Relate:JOBSWARR.Open()
    Relate:CHARTYPE.Open()
    Relate:REPTYDEF.Open()
    Relate:WEBJOB.Open()
    Relate:TRADEACC.Open()
    Relate:LOCATLOG.Open()
    Relate:TRDMAN.Open()
    Relate:TRDPARTY.Open()
    Relate:Audit.open()
    Relate:users.open()


    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
    Save_TRADEACC_ID = Access:TRADEACC.SaveFile()
    Save_JOBSE_ID = Access:JOBSE.SaveFile()
    Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()


    ! DBH 20/10/2008 #10522
    ! Inserting: Quick and dirty fix, because did not set job:EDI to QUE on the Warranty Screen
    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
    jow:RefNumber = job:Ref_Number
    If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        !Found
        If jow:Status = 'QUE'
            job:EDI = 'QUE'
        End ! If jow:Status = 'QUE'
    Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
    ! End: DBH 20/10/2008 #10522


    ! Return the same value incase anything goes wrong (DBH: 05/02/2007)
    tmp:Return = job:EDI

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If  man:EDIFileType   = 'ALCATEL' Or    |
            man:EDIFileType   = 'APPLE' or      | !Added J 04/02/15 TB13393
            man:EDIFileType   = 'BOSCH' Or      |
            man:EDIFileType   = 'BLACKBERRY' Or | !Added J 26/05/13 TB13053
            man:EDIFileType   = 'ERICSSON' Or   |
            man:EDIFileType   = 'HTC' Or        | ! Paul forgot this... oops :)  DBH 22/05/2009
            man:EDIFileType   = 'HUAWEI' Or     | !Added J 24/04/12  TB 12360
            man:EDIFileType   = 'LG' Or         |
            man:EDIFileType   = 'MAXON' Or      |
            man:EDIFileType   = 'MOTOROLA' Or   |
            man:EDIFileType   = 'NEC' Or        |
            man:EDIFileType   = 'NOKIA' Or      |
            man:EDIFileType   = 'SIEMENS' Or    |
            man:EDIFileType   = 'SAMSUNG' Or    |
            man:EDIFileType   = 'SAMSUNG SA' Or |
            man:EDIFileType   = 'MITSUBISHI' Or |
            man:EDIFileType   = 'TELITAL' Or    |
            man:EDIFileType   = 'SAGEM' Or      |
            man:EDIFileType   = 'SONY' Or       |
            man:EDIFileType   = 'PHILIPS' Or    |
            man:EDIFileType   = 'PANASONIC' Or  |
            man:EDIFileType   = 'ZTE'

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign


            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = wob:HeadAccountNumber
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign


            Access:JOBSE.ClearKey(jobe:RefNumberKey)
            jobe:RefNumber = job:Ref_Number
            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

            tmp:Return = ''

            !Job should never becaome an EDI exception by mistake
            !So no need to double check if it's edi is set to exception

            If job:EDI = 'EXC'
                wob:EDI = 'EXC'
                Access:WEBJOB.Update()
                tmp:Return = 'EXC'
            End ! If job:EDI = 'EXC'

            If Clip(tmp:Return) = ''
                If job:EDI = 'REJ'
                    wob:EDI = 'REJ'
                    Access:WEBJOB.Update()
                    tmp:Return = 'REJ'
                End ! If job:EDI = 'REJ'
            End ! If tmp:Return = ''

            If Clip(tmp:Return) = ''
                If job:EDI = 'EXM'
                    tmp:Return = 'EXM'
                End ! If job:EDI = 'EXM'
            End ! If Clip(tmp:Return) = ''

            If Clip(tmp:Return) = ''
                If job:EDI = 'AAJ'
                    tmp:Return = 'AAJ'
                End ! If job:EDI = 'AAJ'
            End ! If Clip(tmp:Return) = ''

            If Clip(tmp:Return) = ''
                If job:EDI = 'QUE'
                    tmp:Return = 'QUE'
                End ! If job:EDI = 'QUE'
            End ! If Clip(tmp:Return) = ''

! --- Make sure accepted, or reconciled are not changed ----
! Inserting:  DBH 12/01/2009 #10626
            If Clip(tmp:Return) = ''
                If job:EDI = 'YES'
                    tmp:Return = 'YES'
                End ! If job:EDI = 'YES'
            End ! If Clip(tmp:Return) = ''

            If Clip(tmp:Return) = ''
                If job:EDI = 'FIN'
                    tmp:Return = 'FIN'
                End ! If job:EDI = 'FIN'
            End ! If Clip(tmp:Return) = ''

! End: DBH 12/01/2009 #10626
! -----------------------------------------

            ! Third Party Pre-Claim Check
            locPreClaim = 0
            !IF (jobe:WebJob = 0 AND job:Third_Party_Site <> '' AND job:Date_Completed = '' AND job:Workshop <> 'YES')
            !TB13053 - for all manufacturers remove reliance on ARC booked jobs - so remove jobe:webjob
            IF (job:Third_Party_Site <> '' AND job:Date_Completed = '' AND job:Workshop <> 'YES')
                Access:TRDPARTY.Clearkey(trd:Company_Name_Key)
                trd:Company_Name = job:Third_Party_Site
                IF (Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign)
                    IF (trd:PreCompletionClaiming)
                        Access:TRDMAN.Clearkey(tdm:ManufacturerKey)
                        tdm:ThirdPartyCompanyName = job:Third_Party_Site
                        tdm:Manufacturer = job:Manufacturer
                        IF (Access:TRDMAN.Tryfetch(tdm:ManufacturerKey) = Level:Benign)
                            ! Pre Claiming Is Go!
                            locPreClaim = 1
                        END ! IF (Access:TRDMAN.Tryfetch(tdm:ManufacturerKey) = Level:Benign)
                    END ! IF (trd:PreCompletionClaiming)
                END ! IF (Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign)
            END ! IF (jobe:WebJob = 0 AND job:Third_Party_Site <> '')


            !Do the calculation to check if a job should appear in the Pending Claims Table
            ! Job must be completed, a warranty job, and not already have a batch number. Not be an exception, a bouncer and not be excluded (by Charge TYpe or Repair Type)

            If Clip(tmp:Return) = ''
    !Stop(1)
                If (job:Date_Completed <> '' OR locPreClaim = 1)    ! #12363 If pre-claim then job doesn't need to be completed (DBH: 03/02/2012)
    !Stop(2)
                    If job:Warranty_Job = 'YES'
    !Stop(3)
!                        If job:EDI_Batch_Number = 0
    !Stop(4)
                            If job:EDI <> 'AAJ'
    !Stop(5)
! -----------------------------------------
! DBH 20/10/2008 #10522
! Changi0ng: Also exclude Queries
!                                If job:EDI <> 'EXC'
! to: DBH 20/10/2008 # 10522
                                If job:EDI <> 'EXC' And job:EDI <> 'QUE'
! End: DBH 20/10/2008 #10522
! -----------------------------------------
    !Stop(6)
                                    If ~(job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR'))
    !Stop(7)
                                        Save_CHARTYPE_ID = Access:CHARTYPE.SaveFile()
                                        Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                                        cha:Charge_Type = job:Warranty_Charge_Type
                                        If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                            !Found
                                            If cha:Exclude_EDI <> 'YES'
    !Stop(8)
                                                Save_REPTYDEF_ID = Access:REPTYDEF.SaveFile()
                                                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                                                rtd:Manufacturer = job:Manufacturer
                                                rtd:Warranty = 'YES'
                                                rtd:Repair_Type = job:Repair_Type_Warranty
                                                If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                    !Found
                                                    If ~rtd:ExcludeFromEDI
    !Stop(9)
                                                        If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                                                            wob:EDI = 'NO'
                                                            Access:WEBJOB.Update()
                                                        End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'

                                                        !Check if the job is a 48 Hour Scrap/RTM with only one exchange - 4449
                                                        ScrapCheck# = 0
                                                        ! Is this a 48 hour job? (DBH: 05/02/2007)
                                                        If jobe:Engineer48HourOption = 1
                                                            ! Is there only one exchange attached? (DBH: 05/02/2007)
                                                            If job:Exchange_Unit_Number > 0
                                                                If jobe:SecondExchangeNumber = 0
                                                                    If job:Repair_Type_Warranty = 'R.T.M.' Or job:Repair_Type_Warranty = 'SCRAP'
                                                                        ScrapCheck# = 1
                                                                        ! Do not claim for this job (DBH: 05/02/2007)
                                                                    End ! If job:Repair_Type_Warranty = 'R.T.M.' Or job:Repair_Type_Warranty = 'SCRAP'
                                                                End ! If jobe:SecondExchangeNumber = 0
                                                            End ! If job:Exchange_Unit_Number > 0
                                                        End ! If jobe:Engineer48HourOption = 1

                                                        If ScrapCheck# = 0
    !Stop(10)
                                                            If job:EDI <> 'NO'
                                                                ! Inserting (DBH 05/02/2007) # 7252 - EDI Is changing to 'NO' to appear in the EDI table, so record the date
                                                                Save_JOBSE2_ID = Access:JOBSE2.SaveFile()
                                                                Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                                                                jobe2:RefNumber = job:Ref_Number
                                                                If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                                                    !Found
                                                                    jobe2:InPendingDate = Today()
                                                                    Access:JOBSE2.Update()
                                                                Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                                                    !Error
                                                                    If Access:JOBSE2.PrimeRecord() = Level:Benign
                                                                        jobe2:RefNumber = job:Ref_Number
                                                                        jobe2:InPendingDate = Today()
                                                                        If Access:JOBSE2.TryInsert() = Level:Benign
                                                                            !Insert
                                                                        Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                                                                            Access:JOBSE2.CancelAutoInc()
                                                                        End ! If Access:JOBSE2.TryInsert() = Level:Benign
                                                                    End ! If Access.JOBSE2.PrimeRecord() = Level:Benign
                                                                End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign

                                                                Access:JOBSE2.RestoreFile(Save_JOBSE2_ID)
                                                                ! End (DBH 05/02/2007) #7252

                                                            End ! If job:EDI <> 'NO'
                                                            tmp:Return = 'NO'
                                                        End ! If ScrapCheck# = 0
                                                    End ! If ~rtd:ExcludeFromEDI
                                                Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                                    !Error
                                                End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign

                                                Access:REPTYDEF.RestoreFile(Save_REPTYDEF_ID)
                                             End ! If cha:Exclude_EDI <> 'YES'
                                        Else ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                                            !Error
                                        End ! If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign

                                        Access:CHARTYPE.RestoreFile(Save_CHARTYPE_ID)
                                    End ! If (~job:Bouncer <> '' And (job:Bouncer_Type = 'BOT' Or job:Bouncer_Type = 'WAR'))
                                Else ! If job:EDI <> 'EXC'
                                    If glo:WebJob
                                        wob:EDI = 'AAJ'
                                        Access:WEBJOB.Update()
                                        tmp:Return = 'AAJ'
                                    Else ! If glo:WebJob
                                        tmp:Return = 'EXC'
                                    End ! If glo:WebJob
                                End ! If job:EDI <> 'EXC'
                            Else ! If job:EDI <> 'AAJ'
                            End ! If job:EDI <> 'AAJ'
!                        Else ! If job:EDI_Batch_Number = 0
! ---- Batch number is not applicable anymore, so this check is unecessary ----
! Deleting: DBH 12/01/2009 #10625
!                            ! This is an error check (DBH: 05/02/2007)
!                            ! If the job has a batch number then it can only be 3 things. Invoice, Reconciled or exception (DBH: 05/02/2007)
!                            If job:Invoice_Number_Warranty <> ''
!                                If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
!                                    wob:EDI = 'FIN'
!                                    Access:WEBJOB.Update()
!                                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
!                                tmp:Return = 'FIN'
!                            Else ! If job:Invoice_Number_Warranty <> ''
!                                If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
!                                    wob:EDI = 'YES'
!                                    Access:WEBJOB.Update()
!                                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
!                                tmp:Return = 'YES'
!                            End ! If job:Invoice_Number_Warranty <> ''

! End: DBH 12/01/2009 #10625
! -----------------------------------------

!                        End ! If job:EDI_Batch_Number = 0
                    End ! If job:Warranty_Job = 'YES'
                End ! If job:Date_Completed <> ''
            End ! If Clip(tmp:Return) = ''
        End ! man:EDIFileType   = 'PANASONIC'

        ! Make sure the EDI value doesn't change once it's set (DBH: 05/02/2007)

        ! Reconciled (DBH: 05/02/2007)
        If Clip(tmp:Return) = ''
            If job:EDI = 'YES'
                If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI = 'APP'
                    wob:EDI = 'YES'
                    Access:WEBJOB.Update()
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI = 'APP'
                tmp:Return = 'YES'
            End ! If job:EDI = 'YES'
        End ! If Clip(tmp:Return) = ''

        ! Invoiced (DBH: 05/02/2007)
        If Clip(tmp:Return) = ''
            If job:EDI = 'FIN'
                If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                    wob:EDI = 'FIN'
                    Access:WEBJOB.Update()
                End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                tmp:Return = 'FIN'
            End ! If job:EDI = 'FIN'
        End ! If Clip(tmp:Return) = ''


        ! If all else failes, then this shouldn't be a warranty claim (DBH: 05/02/2007)
        If Clip(tmp:Return) = ''
            If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
                wob:EDI = 'XXX'
                Access:WEBJOB.Update()
            End ! If wob:EDI <> 'PAY' And wob:EDI <> 'AAJ' And wob:EDI <> 'APP'
            tmp:Return = 'XXX'
        End ! If Clip(tmp:Return) = ''

        ! Inserting (DBH 05/02/2007) # 8707 - Write warranty information to new file
        PrimaryWarrInsert = false
        Access:JOBSWARR.ClearKey(jow:RefNumberKey)
        jow:RefNumber = job:Ref_Number
        If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
            !Error
            If Access:JOBSWARR.PrimeRecord() = Level:Benign
                jow:RefNumber = job:Ref_Number
                jow:Orig_sub_date = today()     !TB12588 - new field to be added on insert - JC 04/08/12
                jow:ClaimSubmitted = today()   !TB12588 - set up the claim submitted date - JC 04/08/12
                If Access:JOBSWARR.TryInsert() = Level:Benign
                    !Inserted
                    !add the audit trail TB12588 - new item to be added on insert - JC 04/08/12
                    !but only if this is not imediatly deleted - so moved to below see: JJJJJ
                    PrimaryWarrInsert = true
                Else ! If Access:JOBSWARR.TryInsert() = Level:Benign
                    Access:JOBSWARR.CancelAutoInc()
                End ! If Access:JOBSWARR.TryInsert() = Level:Benign
            End ! If Access.JOBSWARR.PrimeRecord() = Level:Benign
        End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign


        If tmp:Return = 'XXX' Or Clip(tmp:Return) = '' Or job:Cancelled = 'YES'
            Relate:JOBSWARR.Delete(0)
        Else ! If tmp:Return = 'XXX'   etc
            !JJJJJ
            if PrimaryWarrInsert then

                    IF (AddToAudit(job:Ref_number,'JOB','ORIGINAL SUBMISSION TO WARRANTY PROCESS SCREEN','JOB HAS BEEN PUT ON THE PENDING TAB OF THE WARRANTY PROCESS SCREEN'))
                    END ! IF

!                    !need the user details
!                    access:users.clearkey(use:password_key)
!                    use:password = glo:Password
!                    if access:users.fetch(use:password_key)
!                        !error
!                    END
!
!                    Access:Audit.primerecord()
!                    aud:Ref_Number = job:Ref_number
!                    aud:Date       = today()
!                    aud:Time       = clock()
!                    aud:User       = use:User_Code      !glo:usercode
!                    aud:Action     = 'ORIGINAL SUBMISSION TO WARRANTY PROCESS SCREEN'
!                    aud:Notes      = 'JOB HAS BEEN PUT ON THE PENDING TAB OF THE WARRANTY PROCESS SCREEN'
!                    aud:Type       = 'JOB'
!                    Access:Audit.update()
            END
            jow:Status = tmp:Return
            jow:RRCStatus = wob:EDI
            jow:BranchID = tra:BranchIdentification
            If SentToHub(job:Ref_Number)
                jow:RepairedAt = 'ARC'
            Else ! If SentToHub(job:Ref_Number)
                jow:RepairedAt = 'RRC'
            End ! If SentToHub(job:Ref_Number)
            jow:Manufacturer = job:Manufacturer
! Deleting (DBH 02/06/2008) # 9792 - Not used
!            jow:BatchNumber = job:EDI_Batch_Number
! End (DBH 02/06/2008) #9792
            Access:CHARTYPE.ClearKey(cha:Warranty_Key)
            cha:Warranty = 'YES'
            cha:Charge_Type = job:Warranty_Charge_Type
            If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Found
            Else ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
                !Error
            End ! If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
            If cha:SecondYearWarranty
                jow:FirstSecondYear = 1
            Else ! If cha:SecondYearWarranty
                jow:FirstSecondYear = 0
            End ! If cha:SecondYearWarranty

            ! Inserting (DBH 02/06/2008) # 9792 - Date should always be filled, but just in case...
            If jow:ClaimSubmitted = 0
                jow:ClaimSubmitted = job:Date_Completed
            End ! If jow:SubmittedDate = 0
            ! End (DBH 02/06/2008) #9792

            If tmp:Return = 'NO'
                If jow:Submitted = 0
                    jow:Submitted = 1
                    ! Inserting (DBH 02/06/2008) # 9792 - Record when a job put into "Pending"
                    jow:ClaimSubmitted = Today()
                    ! End (DBH 02/06/2008) #9792
                End ! If jow:Submitted = 0
            End ! If tmp:Return = 'NO'
            Access:JOBSWARR.Update()
        End ! If tmp:Return = 'XXX'
        ! End (DBH 05/02/2007) #8707

    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign


    Access:TRADEACC.RestoreFile(Save_TRADEACC_ID)
    Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
    Access:JOBSE.RestoreFile(Save_JOBSE_ID)

    Relate:JOBSE.Close()
    Relate:JOBSE2.Close()
    Relate:JOBSWARR.Close()
    Relate:CHARTYPE.Close()
    Relate:REPTYDEF.Close()
    Relate:WEBJOB.Close()
    Relate:TRADEACC.Close()
    Relate:LOCATLOG.Close()
    Relate:TRDMAN.Close()
    Relate:TRDPARTY.Close()
    Relate:Audit.close()

    ! Inserting (DBH 05/02/2007) # 8707 - Double check incase EDI hasn't been affected
    If tmp:Return = ''
        tmp:Return = job:EDI
    End ! It tmp:Return = ''
    ! End (DBH 05/02/2007) #8707

    Return tmp:Return
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
