

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01006.INC'),ONCE        !Local module procedure declarations
                     END


AddBouncers          PROCEDURE  (func:RefNumber,func:DateBooked,func:IMEINumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_job_id          USHORT,AUTO
tmp:Count            LONG
save_job2_id         USHORT,AUTO
save_bou_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        If job:Warranty_Job <> 'YES'
            Return
        End !If job:Warranty_Job <> 'YES'
    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    If GETINI('BOUNCER','IgnoreWarranty',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        If job:Chargeable_Job <> 'YES'
            Return
        End !If job:Warranty_Job <> 'YES'
    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1


    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    !Clear Blank Entries From Bouncer Table
    Save_bou_ID = Access:BOUNCER.SaveFile()
    Access:BOUNCER.ClearKey(bou:Bouncer_Job_Number_Key)
    bou:Original_Ref_Number = 0
    bou:Bouncer_Job_Number  = 0
    Set(bou:Bouncer_Job_Number_Key,bou:Bouncer_Job_Number_Key)
    Loop
        If Access:BOUNCER.NEXT()
           Break
        End !If
        If bou:Original_Ref_Number <> 0      |
        Or bou:Bouncer_Job_Number  <> 0      |
            Then Break.  ! End If
        Delete(BOUNCER)
    End !Loop
    Access:BOUNCER.RestoreFile(Save_bou_ID)


    !Only check for bouncers if the account number isn't ticked "Exclude From Bouncer"
    If CompleteCheckForBouncer(job:Account_Number)
        job:Bouncer = ''
    Else
        tmp:Count = 0
        If func:IMEINumber <> 'N/A' And func:IMEINumber <> ''

!            zup# = CountBouncer(func:RefNumber,job:Date_Booked,func:IMEINumber,job:Chargeable_Job,|
!                        job:Charge_Type,job:Repair_Type,job:Warranty_Job,job:Warranty_Charge_Type,job:Repair_Type_Warranty)
            zup# = CountBouncer()

            Loop x# = 1 To Records(glo:Queue20)
                Get(glo:Queue20,x#)
                If Access:BOUNCER.PrimeRecord() = Level:Benign
                    bou:Original_Ref_Number = func:RefNumber
                    bou:Bouncer_Job_Number  = glo:Pointer20
                    tmp:Count += 1
                    If Access:BOUNCER.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:BOUNCER.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:BOUNCER.TryInsert() = Level:Benign
                End !If Access:BOUNCER.PrimeRecord() = Level:Benign

            End !Loop x# = 1 To Records(glo:Queue20)

!            Setcursor(Cursor:Wait)
!            Save_job2_ID = Access:JOBS2_ALIAS.SaveFile()
!            Access:JOBS2_ALIAS.ClearKey(job2:ESN_Key)
!            job2:ESN = func:IMEINumber
!            Set(job2:ESN_Key,job2:ESN_Key)
!            Loop
!                If Access:JOBS2_ALIAS.NEXT()
!                   Break
!                End !If
!                If job2:ESN <> func:IMEINumber      |
!                    Then Break.  ! End If
!                If job2:Cancelled = 'YES'
!                    Cycle
!                End !If job2:Cancelled = 'YES'
!                If job2:Exchange_Unit_Number <> ''
!                    Cycle
!                End !If job2:Exchange_Unit_Number <> ''
!
!
!                If job2:ref_number <> func:RefNumber and job2:Cancelled <> 'YES'
!                    If job2:Date_Booked + def:BouncerTime > func:DateBooked And job2:date_booked <= func:DateBooked
!                        If Access:BOUNCER.PrimeRecord() = Level:Benign
!                            bou:Original_Ref_Number = func:RefNumber
!                            bou:Bouncer_Job_Number  = job2:Ref_Number
!                            tmp:Count += 1
!                            If Access:BOUNCER.TryInsert() = Level:Benign
!                                !Insert Successful
!                            Else !If Access:BOUNCER.TryInsert() = Level:Benign
!                                !Insert Failed
!                            End !If Access:BOUNCER.TryInsert() = Level:Benign
!                        End !If Access:BOUNCER.PrimeRecord() = Level:Benign
!                    End!If job2:date_booked + man:warranty_period < Today()
!                End!If job2:esn <> job2:ref_number
!
!            End !Loop
!            Access:JOBS2_ALIAS.RestoreFile(Save_job2_ID)
!            Setcursor()

            If tmp:Count
                If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                    job:bouncer_type    = 'BOT'
                End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                    job:bouncer_type    = 'CHA'
                End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                    job:bouncer_type    = 'WAR'
                End!IF job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                job:bouncer = 'X'

                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'JOB MARKED AS BOUNCER'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign
                Case Missive('This job has been found to be a Bouncer.||It can now only be Invoiced after it has been authorised from the Bouncer List.','ServiceBase 3g','MIdea.jpg','/OK')
                    Of 1  !/OK
                End !Case Missive
            Else !If tmp:Count
                job:Bouncer = ''
            End !If tmp:Count
        End !If func:IMEINumber <> 'N/A' And func:IMEINumber <> ''

    End !If CompleteCheckForBouncer(job:Account_Number) = Level:Benign

    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Found
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ExchangeAccount      PROCEDURE  (func:AccountNumber)  ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:ExchangeAcc = 'YES'
                Return Level:Fatal
            End !If tra:ExchangeAcc
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CheckLengthNoError   PROCEDURE  (f_type,f_ModelNumber,f_number) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Return The Correct Length Of A Model Number
    access:modelnum.clearkey(mod:model_number_key)
    mod:model_number = f_ModelNumber
    if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
        If f_number <> 'N/A'
            Case f_type
                Of 'IMEI'
                    If Len(Clip(f_number)) < mod:esn_length_from Or Len(Clip(f_number)) > mod:esn_length_to
                        Return Level:Fatal
                    End!If Clip(Len(f_number)) < mod:esn_length_from Or Clip(Len(f_number)) > mod:esn_length_to


                Of 'MSN'
                    If Len(Clip(f_number)) < mod:msn_length_from Or len(clip(f_number)) > mod:msn_length_to
                        Return Level:Fatal
                    End!If Clip(Len(f_number)) < mod:esn_length_from
            End!Case f_type
        End!If f_number <> 'N/A'

    End!if access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AccountSkipDespatch  PROCEDURE  (func:AccountNumber)  ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
InvoiceSubAccounts   PROCEDURE  (func:AccountNumber)  ! Declare Procedure
save_subtracc_id     USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return# = Level:Benign

    Relate:SUBTRACC.Open
    Relate:TRADEACC.Open

    Save_SUBTRACC_ID = Access:SUBTRACC.SaveFile()
    Save_TRADEACC_ID = Access:TRADEACC.SaveFile()
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = func:AccountNumber
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                Return# = Level:Fatal
            End ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Access:SUBTRACC.RestoreFile(Save_SUBTRACC_ID)
    Access:TRADEACC.RestoreFile(Save_TRADEACC_ID)

    Relate:SUBTRACC.Close
    Relate:TRADEACC.Close

    Return Return#
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CanInvoiceBePrinted  PROCEDURE  (func:JobNumber,func:ShowMessages) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Have made a CanJobInvoiceBePrinted which doesn't use the Alias file
!this will be better to use when actually IN a job.

!Can the invoice of the selected job be printed/created?
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = func:JobNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
        If job_ali:Chargeable_Job <> 'YES'
            If func:ShowMessages
                Case Missive('Cannot invoice! The selected job is NOT a Chargeable Job.','ServiceBase 3g','MStop.jpg','/OK')
                    Of 1  !/OK
                End !Case Missive
            End !If func:ShowMessages
            Return Level:Fatal
        End !If job:Chargeable_Job <> 'YES'

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job_ali:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Found
            If ~glo:WebJob And ~SentToHub(job_ali:Ref_Number)
                IF func:ShowMessages
                    Case Missive('Cannot Invoice! The selected job in an RRC job.','ServiceBase 3g','MStop.jpg','/OK')
                        Of 1  !/OK
                    End !Case Missive
                End !IF func:ShowMessages
                Return Level:Fatal
            End !If ~glo:WebJob And ~jobe:HubRepair
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        If job_ali:Bouncer = 'X'
            If func:ShowMessages
                Case Missive('Cannot invoice! The selected job has been marked as a Bouncer.','ServiceBase 3g','MStop.jpg','/OK')
                    Of 1  !/OK
                End !Case Missive
            End !If func:ShowMessages
            Return Level:Fatal
        End !If job_ali:Bouncer <> ''

        If job_ali:Date_Completed = '' and job_ali:Exchange_Unit_Number = 0
            If func:ShowMessages
                Case Missive('Cannot invoice! The selected job has not been completed.','ServiceBase 3g','MStop.jpg','/OK')
                    Of 1  !/OK
                End !Case Missive
            End !If func:ShowMessages
            Return Level:Fatal
        End !If job_ali:Date_Completed = ''

        !There should now be now way to complete a job without a default structure

        If job_ali:Ignore_Chargeable_Charges = 'YES'
            !Check the sub total depending on where the invoice
            !is being raised - L907 (DBH: 05-08-2003)
            If glo:WebJob
                If jobe:RRCCSubTotal = 0
                    If func:ShowMessages
                        Case Missive('Cannot invoice! The selected job has not been priced.','ServiceBase 3g','MStop.jpg','/OK')
                            Of 1  !/OK
                        End !Case Missive
                    End !If func:ShowMessages
                    Return Level:Fatal

                End !If jobe:RRCCSubTotal = 0
            Else !If glo:WebJob
                If job_ali:Sub_Total = 0
                    If func:ShowMessages
                        Case Missive('Cannot invoice! The selected job has not been priced.','ServiceBase 3g','MStop.jpg','/OK')
                            Of 1  !/OK
                        End !Case Missive
                    End !If func:ShowMessages
                    Return Level:Fatal

                End !If job:Sub_Total = 0
            End !If glo:WebJob
        End !If job_ali:Ignore_Chargeable_Charges = 'YES' and job:Sub_Total = 0

        !Is this job a single invoice, and not part of a multiple invoice?
        If job_ali:Invoice_Number <> ''
            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
            inv:Invoice_Number =job_ali:Invoice_Number
            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
               !Found
                If inv:Invoice_Type <> 'SIN'
                    If func:ShowMessages
                        Case Missive('Cannot print invoice! The selected job is NOT a Single Invoice. |It may be part of a Multiple Invoice','ServiceBase 3g','MStop.jpg','/OK')
                            Of 1  !/OK
                        End !Case Missive
                    End !If func:ShowMessages
                    Return Level:Fatal
                End !If inv:Invoice_Type <> 'SIN'
            Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
                Assert(0,'<13,10>Fetch Error<13,10>')
                Return Level:Fatal
            End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        End !If job_ali:Invoice_Number <> ''

        If InvoiceSubAccounts(job_ali:Account_Number)
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number = job_ali:Account_Number
            IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
            END


            access:vatcode.clearkey(vat:vat_code_key)
            vat:vat_code = sub:labour_vat_code
            if access:vatcode.fetch(vat:vat_code_key)
                If func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>A V.A.T. rate has not been setup for this Sub Account.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                End !If func:ShowMessages

                Return Level:Fatal
            end!if access:vatcode.fetch(vat:vat_code_key)
            access:vatcode.clearkey(vat:vat_code_key)
            vat:vat_code = sub:parts_vat_code
            if access:vatcode.fetch(vat:vat_code_key)
                If func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>A V.A.T. rate has not been setup for this Sub Account.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                End !If func:ShowMessages
                Return Level:Fatal
            end!if access:vatcode.fetch(vat:vat_code_key)
        Else !If InvoiceSubAccounts(job_ali:Account_Number)
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number = job_ali:Account_Number
            IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
            END
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (ACcess:TRADEACC.Tryfetch(Tra:Account_Number_Key))
            END

            access:vatcode.clearkey(vat:vat_code_key)
            vat:vat_code = tra:labour_vat_code
            if access:vatcode.fetch(vat:vat_code_key)
                If func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>A V.A.T. rate has not been setup for this Head Account.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive

                End !If func:ShowMessages
                Return Level:Fatal
            end!if access:vatcode.fetch(vat:vat_code_key)
            access:vatcode.clearkey(vat:vat_code_key)
            vat:vat_code = tra:parts_vat_code
            if access:vatcode.fetch(vat:vat_code_key)
                If Func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>A V.A.T. rate has not been setup for this Head Account.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                End !If Func:ShowMessages
                Return Level:Fatal
            end!if access:vatcode.fetch(vat:vat_code_key)

        End !If InvoiceSubAccounts(job_ali:Account_Number)

    Else!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
        Assert(0,'<13,10>Fetch Error<13,10>')
        Return Level:Fatal
    End!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CreateInvoice        PROCEDURE                        ! Declare Procedure
tmp:LabourVATRate    REAL
tmp:PrintDespatch    BYTE(0)
tmp:PartsVATRate     REAL
Invoice_Number_Temp  LONG
tmp:Paid             BYTE(0)
save_webjob_id       USHORT,AUTO
tmp:CourierCost      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
WindowsDir      CSTRING(255)
SystemDir       CSTRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Create a Chargeable Invoice inv:RRCVatRateLabour
!    message('In create invoice - have job number '&job:ref_number)
    If job:Invoice_Number <> 0
        !This job has already been invoiced
        Return Level:Benign
    End !If job:Invoice_Number <> 0

    If JobInUse(job:Ref_Number,1)
        Return Level:Benign
    End !If JobInUse(job:Ref_Number,0)

    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))

    If InvoiceSubAccounts(job:Account_Number)
        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = sub:Labour_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:LabourVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = sub:Parts_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:PartsVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        !tmp:PrintDespatch = 1 - Print Despatch Note
        !tmp:PrintDespatch = 2 - Do not print, but change status to READY TO DESPATCH
        If sub:Despatch_Invoiced_Jobs = 'YES'
            If sub:Despatch_Paid_Jobs = 'YES'
                If job:Paid = 'YES'
                    tmp:PrintDespatch = 1
                Else !If job:Paid = 'YES'
                    tmp:PrintDespatch = 2
                End !If job:Paid = 'YES'
            Else !If sub:Despatch_Paid_Jobs = 'YES'
                tmp:PrintDespatch = 1
            End !If sub:Despatch_Paid_Jobs = 'YES'
        End !If sub:Despatch_Invoiced_Jobs = 'YES'

    Else !If InvoiceSubAccounts(job:Account_Number)
        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = tra:Labour_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:LabourVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

        Access:VATCODE.Clearkey(vat:VAT_Code_Key)
        vat:VAT_Code    = tra:Parts_Vat_Code
        If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Found
            tmp:PartsVATRate   = vat:VAT_Rate
        Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign

! Deleting (DBH 24/01/2007) # 8678 - Shouldn't needed as the job is not being stopped from going to the despatch table
!        If tra:Despatch_Invoiced_Jobs = 'YES'
!            If tra:Despatch_Paid_Jobs = 'YES'
!                If job:Paid = 'YES'
!                    tmp:PrintDespatch = 1
!                End !If job:Paid = 'YES'
!            Else !If sub:Despatch_Paid_Jobs = 'YES'
!                tmp:PrintDespatch = 1
!            End !If sub:Despatch_Paid_Jobs = 'YES'
!        End !If sub:Despatch_Invoiced_Jobs = 'YES'
! End (DBH 24/01/2007) #8678
    End !If InvoiceSubAccounts(job:Account_Number)

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    JobPricingRoutine(0)

    invoice_error# = 1
    If Access:INVOICE.PrimeRecord() = Level:Benign
        If def:use_sage = 'YES'
            inv:invoice_number = invoice_number_temp
        End!If def:use_sage = 'YES'
        inv:invoice_type       = 'SIN'
        inv:job_number         = job:ref_number
        inv:date_created       = Today()
        inv:account_number     = job:account_number
        If tra:invoice_sub_accounts = 'YES'
            inv:AccountType = 'SUB'
        Else!If tra:invoice_sub_accounts = 'YES'
            inv:AccountType = 'MAI'
        End!If tra:invoice_sub_accounts = 'YES'
        inv:total              = job:sub_total
!            if glo:webjob then
            inv:RRCVatRateLabour   = tmp:LabourVATRate
            inv:RRCVatRateParts    = tmp:PartsVATRate
            inv:RRCVatRateRetail   = tmp:LabourVATRate
!            ELSE
            inv:vat_rate_labour    = tmp:LabourVATRate
            inv:vat_rate_parts     = tmp:PartsVATRate
            inv:vat_rate_retail    = tmp:LabourVATRate
!            END !If glo:webjob
        inv:vat_number         = def:vat_number
        INV:Courier_Paid       = job:courier_cost
        inv:parts_paid         = job:parts_cost
        inv:labour_paid        = job:labour_cost
        inv:invoice_vat_number = vat_number"

        !If called from webjob, make sure the web
        !part is marked as invoiced - 3308 (DBH: 26-09-2003)
        If glo:WebJob
            inv:RRCInvoiceDate = Today()
            inv:ExportedRRCOracle = TRUE
        End !If glo:WebJob


        If Access:INVOICE.TryInsert() = Level:Benign
            !Insert Successful
            Access:COURIER.Clearkey(cou:Courier_Key)
            cou:Courier = job:Courier
            If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                !Found

            Else ! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                !Error
            End !If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
            tmp:Paid = 0
            If job:Warranty_Job = 'YES'
                If job:EDI = 'FIN'
                    tmp:Paid = 1
                End !If job:EDI = 'FIN'
            End !If job:Warranty_Job = 'YES'
            If job:Chargeable_Job = 'YES'
                tmp:Paid = 0
                Total_Price('C',vat",total",balance")
                If total" = 0 Or balance" <= 0
                    tmp:Paid = 1
                End !If total" = 0 Or balance" <= 0
            End !If job:Chargeable_Job = 'YES'

! Deleting (DBH 24/01/2007) # 8678 - Not needed as the job is not being stopped from going into the despatch table
!            Case tmp:PrintDespatch
!                Of 1
!                    If glo:WebJob
!                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
!                        jobe:RefNumber  = job:Ref_Number
!                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                            !Found
!                            If jobe:Despatched <> 'REA'
!                                If tmp:Paid
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(910,0,'JOB') !Despatch Paid
!                                    End !If job:Exchange_Unit_Number = ''
!                                Else !If job:Paid = 'YES'
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(905,0,'JOB') !Despatch Unpaid
!                                    End !If job:Exchange_Unit_Number = ''
!                                End !If job:Paid = 'YES'
!                            Else !If jobe:Despatched = ''
!                                If tmp:Paid
!                                    If cou:CustomerCollection
!                                        If job:Exchange_Unit_Number = ''
!                                            GetStatus(915,0,'JOB') !Paid Awaiting Collection
!                                        End !If job:Exchange_Unit_Number = ''
!                                    Else !If cou:CustomerCollection
!                                        If job:Exchange_Unit_Number = ''
!                                            GetStatus(916,0,'JOB') !Paid Awaiting Despatch
!                                        End !If job:Exchange_Unit_Number = ''
!                                    End !If cou:CustomerCollection
!                                Else !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
!                                    If cou:CustomerCollection
!                                        If job:Exchange_Unit_Number = ''
!                                            GetStatus(805,0,'JOB') !Ready To Collect
!                                        End !If job:Exchange_Unit_Number = ''
!                                    Else !If cou:CustomerCollection
!                                        If job:Exchange_Unit_Number = ''
!                                            GetStatus(810,0,'JOB') !Ready To Despatch
!                                        End !If job:Exchange_Unit_Number = ''
!                                    End !If cou:CustomerCollection
!                                End !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
!                                jobe:Despatched = 'REA'
!                                jobe:DespatchType = 'JOB'
!
!                                !Update WEBJOB with despatch info -  (DBH: 23-10-2003)
!                                save_WEBJOB_id = Access:WEBJOB.SaveFile()
!                                Access:WEBJOB.Clearkey(wob:RefNumberKey)
!                                wob:RefNumber   = jobe:RefNumber
!                                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                                    !Found
!                                    wob:ReadyToDespatch = 1
!                                    wob:DespatchCourier = job:Courier
!
!                                    ! Inserting (DBH 23/01/2007) # 8678 - Set the status on when enteriing the despatch table
!                                    SetDespatchStatus()
!                                    ! End (DBH 23/01/2007) #8678
!
!                                    Access:WEBJOB.TryUpdate()
!                                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                                    !Error
!                                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                                Access:WEBJOB.RestoreFile(save_WEBJOB_id)
!
!                            End !If jobe:Despatched = ''
!                            ACcess:JOBSE.Update()
!                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                            !Error
!                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                    Else !If glo:WebJob
!                        If job:Despatched <> 'REA'
!                            If tmp:Paid
!                                If job:Exchange_Unit_Number = ''
!                                    GetStatus(910,0,'JOB') !Despatch Paid
!                                End !If job:Exchange_Unit_Number = ''
!                            Else !If job:Paid = 'YES'
!                                If job:Exchange_Unit_Number = ''
!                                    GetStatus(905,0,'JOB') !Despatch Unpaid
!                                End !If job:Exchange_Unit_Number = ''
!                            End !If job:Paid = 'YES'
!                        Else !If job:Despatch = 'YES'
!                            If tmp:Paid
!                                If cou:CustomerCollection
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(915,0,'JOB') !Paid Awaiting Collection
!                                    End !If job:Exchange_Unit_Number = ''
!                                Else !If cou:CustomerCollection
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(916,0,'JOB') !Paid Awaiting Despatch
!                                    End !If job:Exchange_Unit_Number = ''
!                                End !If cou:CustomerCollection
!                            Else !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
!                                If cou:CustomerCollection
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(805,0,'JOB') !Ready To Collect
!                                    End !If job:Exchange_Unit_Number = ''
!                                Else !If cou:CustomerCollection
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(810,0,'JOB') !Ready To Despatch
!                                    End !If job:Exchange_Unit_Number = ''
!                                End !If cou:CustomerCollection
!                            End !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
!
!                            job:Despatched = 'REA'
!                            job:Despatch_Type = 'JOB'
!                            job:Current_Courier = job:Courier
!
!                            ! Inserting (DBH 23/01/2007) # 8678 - Set Status Entering the despatch browse
!                            SetDespatchStatus()
!                            ! End (DBH 23/01/2007) #8678
!
!                        End !If job:Despatch = 'YES'
!
!                    End !If glo:WebJob
!                Of 2
!                Else
! End (DBH 24/01/2007) #8678
                    !Change status
                    If glo:WebJob
                        If jobe:Despatched <> 'REA'
                            If tmp:Paid
                                If job:Exchange_Unit_Number = ''
                                    GetStatus(910,0,'JOB') !Despatch Paid
                                End !If job:Exchange_Unit_Number = ''
                            Else !If job:Paid = 'YES'
                                If job:Exchange_Unit_Number = ''
                                    GetStatus(905,0,'JOB') !Despatch Unpaid
                                End !If job:Exchange_Unit_Number = ''
                            End !If job:Paid = 'YES'

                        Else !If joe:Despatched <> 'REA'
                            If tmp:Paid
                                If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(915,0,'JOB') !Paid Awaiting Collection
                                    End !If job:Exchange_Unit_Number = ''
                                Else !If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(916,0,'JOB') !Paid Awaiting Despatch
                                    End !If job:Exchange_Unit_Number = ''
                                End !If cou:CustomerCollection
                            Else !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
                                If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(805,0,'JOB') !Ready To Collect
                                    End !If job:Exchange_Unit_Number = ''
                                Else !If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(810,0,'JOB') !Ready To Despatch
                                    End !If job:Exchange_Unit_Number = ''
                                End !If cou:CustomerCollection
                            End !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0

                        End !If joe:Despatched <> 'REA'
                    Else !If glo:WebJob
                        If job:Despatched <> 'REA'
                            If tmp:Paid
                                If job:Exchange_Unit_Number = ''
                                    GetStatus(910,0,'JOB') !Despatch Paid
                                End !If job:Exchange_Unit_Number = ''
                            Else !If job:Paid = 'YES'
                                If job:Exchange_unit_Number = ''
                                    GetStatus(905,0,'JOB') !Despatch Unpaid
                                End !If job:Exchange_unit_Number = ''
                            End !If job:Paid = 'YES'

                        Else !If job:Despatched <> 'REA'
                            If tmp:Paid
                                If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(915,0,'JOB') !Paid Awaiting Collection
                                    End !If job:Exchange_Unit_Number = ''
                                Else !If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(916,0,'JOB') !Paid Awaiting Despatch
                                    End !If job:Exchange_Unit_Number = ''
                                End !If cou:CustomerCollection
                            Else !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
                                If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(805,0,'JOB') !Ready To Collect
                                    End !If job:Exchange_Unit_Number = ''
                                Else !If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(810,0,'JOB') !Ready To Despatch
                                    End !If job:Exchange_Unit_Number = ''
                                End !If cou:CustomerCollection
                            End !If job:Paid = 'YES' or jobe:InvRRCCSubTotal = 0
                            ! Inserting (DBH 15/03/2007) # 8853 - Reset The job status back to "send to rrc"
                            If job:Despatch_Type = 'JOB' And jobe:WebJob = 1
                                GetStatus(Sub(GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB') ! Sent To RRC
                            End ! If job:Despatch_Type = 'JOB' And jobe:WebJob = 1
                            ! End (DBH 15/03/2007) #8853

                        End !If job:Despatched <> 'REA'
                    End !If glo:WebJob
!            End !Case tmp:PrintDespatch

            job:invoice_number        = inv:invoice_number
            job:invoice_date          = Today()
            job:invoice_courier_cost  = job:courier_cost
            job:invoice_labour_cost   = job:labour_cost
            job:invoice_parts_cost    = job:parts_cost
            job:invoice_sub_total     = job:sub_total
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                jobe:InvRRCCLabourCost  = jobe:RRCCLabourCost
                jobe:InvRRCCPartsCost   = jobe:RRCCPartsCost
                jobe:InvRRCCSubTotal    = jobe:RRCCSubTotal
                jobe:InvoiceHandlingFee = jobe:HandlingFee
                jobe:InvoiceExchangeRate    = jobe:ExchangeRate
                Access:JOBSE.TryUpdate()
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            Access:JOBS.Update()

            If glo:WebJob
                ! If the RRC is created a brand new invoice, send the xml message from here - TrkBs: 5110 (DBH: 18-08-2005)
                Line500_XML('RIV')
!                if glo:ErrorText <> '' then
!                    lineprint(glo:ErrorText,clip(path())&'\invoicexml.log')
!                END
            End !If glo:WebJob


            Return Level:Benign

        Else !If Access:INVOICE.TryInsert() = Level:Benign
            !Insert Failed
            Return Level:Fatal
        End !If Access:INVOICE.TryInsert() = Level:Benign
    End !If Access:INVOICE.PrimeRecord() = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
LiveBouncers         PROCEDURE  (f_RefNumber,f_DateBooked,f_IMEI) ! Declare Procedure
tmp:DateBooked       DATE
tmp:count            LONG
save_job2_id         USHORT,AUTO
tmp:IMEI             STRING(30)
tmp:Manufacturer     STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
    !From that see if any of the bouncer jobs are still live, i.e. not completed.

    tmp:count    = 0
    If f_IMEI <> 'N/A' And f_IMEI <> ''
        Set(defaults)
        Access:Defaults.Next()

        save_job2_id = access:jobs2_alias.savefile()
        access:jobs2_alias.clearkey(job2:esn_key)
        job2:esn = f_imei
        set(job2:esn_key,job2:esn_key)
        loop
            if access:jobs2_alias.next()
               break
            end !if
            if job2:esn <> f_imei      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if

            If job2:Cancelled = 'YES'
                Cycle
            End !If job2:Cancelled = 'YES'


            If job2:ref_number <> f_refnumber and job2:cancelled <> 'YES'
!                If job2:date_booked + def:bouncertime > f_datebooked And job2:date_booked <= f_datebooked
                    If job2:Date_Completed = ''
                        Return Level:Fatal
                        Break
                    End !If job2:Date_Completed = ''
!                End!If job2:date_booked + man:warranty_period < Today()
            End!If job2:esn <> job2:ref_number
        end !loop
        access:jobs2_alias.restorefile(save_job2_id)
    End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StripQuotes          PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(in_string,STR_POS#,1) = '"'
        in_string = SUB(in_string,1,STR_POS#-1) & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CheckFaultFormat     PROCEDURE  (func:FaultCode,func:FieldFormat) ! Declare Procedure
tmp:MonthStart       LONG
tmp:MonthValue       LONG
tmp:YearStart        LONG
tmp:YearValue        LONG
tmp:StartQuotes      LONG
tmp:FaultYearStart   LONG
tmp:FaultMonthStart  LONG
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
i                           LONG
pos                         LONG
posStar                     LONG
posPct                      LONG
posDollar                   LONG
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    y# = 1
    tmp:MonthStart = 0
    tmp:YearStart = 0
    Loop x# = 1 To Len(Clip(func:FieldFormat))
        !If this is in a quotes just do a straight comparison of characters
        If tmp:StartQuotes
            If Sub(func:FieldFormat,x#,1) = '"'
                !End Of The Quotes
                tmp:StartQuotes = 0
                Cycle
            End !If Sub(func:FieldFormat,x#,1) = '"'
            If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,y#,1)
                Return Level:Fatal
            Else! !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)
                y# += 1
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> Sub(func:FaultCode,x#,1)

        End !If tmp:StartQuotes

        If tmp:YearStart
            If Sub(func:FieldFormat,x#,1) <> 'Y'
                If x# - tmp:YearStart = 4
                    If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2020
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 4

                If x# - tmp:YearStart = 2
                    If Sub(func:FaultCode,tmp:FaultYearStart,2) > 10 And Sub(func:FaultCode,tmp:FaultYearStart,2) < 90
                        !Failed Year
                        Return Level:Fatal
                    End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
                End !If x# - tmp:YearStart = 2
                tmp:YearStart = 0
                y# += 1
            Else !If Sub(func:FieldFormat,x#,1) <> 'Y'
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'Y'
        End !If tmp:YearStart

        If tmp:MonthStart
            If Sub(func:FieldFormat,x#,1) <> 'M'
                If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                    !Month Fail
                    Return Level:Fatal
                End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
                tmp:MonthStart = 0
                y# += 1
            Else !If Sub(func:FieldFormat,x#,1) <> 'M'
                Cycle
            End !If Sub(func:FieldFormat,x#,1) <> 'M'
        End !If tmp:MonthStart

        Case Sub(func:FieldFormat,x#,1)
            Of 'A'
                If Val(Sub(func:FaultCode,y#,1)) < 65 Or Val(Sub(func:FaultCode,y#,1)) > 90
                    !Alpha Error
                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 65 Or Val(Sub(func:FaultCode,x#,1)) > 90
            Of '0'
                If Val(Sub(func:FaultCode,y#,1)) < 48 Or Val(Sub(func:FaultCode,y#,1)) > 57
                    !Numeric Error

                    Return Level:Fatal
                End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
            Of 'X'
                If ~((Val(Sub(func:FaultCode,y#,1)) >=48 And Val(Sub(func:FaultCode,y#,1)) <= 57) Or |
                    (Val(Sub(func:FaultCode,y#,1)) >=65 And Val(Sub(func:FaultCode,y#,1)) <=90))

                    !Alphanumeric Error
                    Return Level:Fatal
                End !(Val(Sub(func:FaultCode,x#,1)) >=65 And Val(Sub(func:FaultCode,x#,1)) <=90))
            Of '"'
                tmp:StartQuotes = 1
                Cycle
            Of 'Y'
                tmp:YearStart   = x#
                tmp:FaultYearStart  = y#
            Of 'M'
                tmp:MonthStart  = x#
                tmp:FaultMonthStart = y#
            Of '%'
                ! A % has been found. Check that all the remaining digits are numbers (DBH: 30/10/2007)
                Loop z# = y# To Len(Clip(func:FaultCode))
                    If Val(Sub(func:FaultCode,z#,1)) < 48 Or Val(Sub(func:FaultCode,z#,1)) > 57
                        !Numeric Error
                        Return Level:Fatal
                    End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
                End ! Loop Until y# = Len(Clip(func:FaultCode))
                Return Level:Benign

            Of '$'
                Loop z# = y# To Len(Clip(func:FaultCode))
                    If Val(Sub(func:FaultCode,z#,1)) < 65 Or Val(Sub(func:FaultCode,z#,1)) > 90
                        !Numeric Error
                        Return Level:Fatal
                    End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
                End ! Loop Until y# = Len(Clip(func:FaultCode))
                Return Level:Benign

            Of '*'
                Loop z# = y# To Len(Clip(func:FaultCode))
                    If ~((Val(Sub(func:FaultCode,z#,1)) >= 65 AND Val(Sub(func:FaultCode,z#,1)) <= 90) OR |
                        (Val(Sub(func:FaultCode,z#,1)) >= 48 AND Val(Sub(func:FaultCode,z#,1)) <= 57))
                            !Numeric Error
                        Return Level:Fatal
                    End !If Val(Sub(func:FaultCode,x#,1)) < 48 Or Val(Sub(func:FaultCode,x#,1)) > 57
                End ! Loop Until y# = Len(Clip(func:FaultCode))            
                Return Level:Benign  
        End !Case Sub(func:FaultCode,x#,1)
        y# += 1
    End !Loop x# = 1 To Len(func:FaultCode)

    If tmp:YearStart

        If x# - tmp:YearStart = 4
            If Sub(func:FaultCode,tmp:FaultYearStart,4) < 1990 Or Sub(func:FaultCode,tmp:FaultYearStart,4) > 2020
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 4

        If x# - tmp:YearStart = 2
            If Sub(func:FaultCode,tmp:FaultYearStart,2) < 90 And Sub(func:FaultCode,tmp:FaultYearStart,2) > 10
                !Failed Year
                Return Level:Fatal
            End !If Sub(func:FaultCode,tmp:YearStart,4) < 1990 Or Sub(func:FaultCode,tmp:YearStart,4) > 2010
        End !If x# - tmp:YearStart = 2
    End !If tmp:YearStart

    If tmp:MonthStart
        If Sub(func:FaultCode,tmp:FaultMonthStart,2) <1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
            !Month Fail
            Return Level:Fatal
        End !If Sub(func:FaultCode,tmp:FaultMonthStart,2) < 1 Or Sub(func:FaultCode,tmp:FaultMonthStart,2) > 12
    End !If tmp:MonthStart

    Return Level:Benign



! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
