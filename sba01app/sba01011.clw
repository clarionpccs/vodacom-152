

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01011.INC'),ONCE        !Local module procedure declarations
                     END


ProgramVersionNumber PROCEDURE                        ! Declare Procedure
tmp:VersionNumber    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:DEFAULTV.Open()
    Access:DEFAULTV.UseFile()
    Set(DEFAULTV)
    Access:DEFAULTV.Next()
    !Save in variable to make sure the file is closed
    !before returning
    tmp:VersionNumber   = defv:VersionNumber
    Access:DEFAULTV.Close()

    Return tmp:VersionNumber
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobInUse             PROCEDURE  (LONG func:JobNumber,BYTE func:ShowError) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    rtnValue# = 0
    pointer# = Pointer(JOBS)
    Hold(JOBS,1)
    Get(JOBS,pointer#)
    if (errorcode()  = 43)
        rtnValue# = 1
    else
        ! Account for jobs locked by SBOnline (DBH: 15/02/2010)
        relate:JOBSLOCK.open()

        ! First step. Remove any blanks from the file
        Access:JOBSLOCK.ClearKey(lock:JobNumberKey)
        lock:JobNumber = 0
        SET(lock:JobNumberKey,lock:JobNumberKey)
        LOOP
            IF (Access:JOBSLOCK.Next())
                BREAK
            END
            IF (lock:JobNumber <> 0)
                BREAK
            END
            Access:JOBSLOCK.DeleteRecord(0)
        END

        access:JOBSLOCK.clearkey(lock:jobNumberKey)
        lock:jobNUmber = job:ref_number
        if (access:JOBSLOCK.tryfetch(lock:jobNumberKey) = level:benign)
            if (TODAY() = lock:DateLocked and clock() < (lock:timelocked + 120000)) ! #13550 Increaes timeout to 20 mins to account for SBOnline timeout of 10 mins (DBH: 10/06/2015)
            ! If it's the same user then there's no need to give the locked error?!
                rtnValue# = 1
            else
                ! Delete the record if accessed via Fat/Thin CLient (DBH: 15/02/2010)
                access:JOBSLOCK.deleteRecord(0)
            end
        end
        relate:JOBSLOCK.close()
    end ! if (errorcode()  = 43)
    if (rtnValue# = 1 and func:ShowError)
        Case Missive('The selected job (' & Clip(func:JobNumber) & ') is currently is use by another station.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    end !if (rtnValue# = 1 and func:ShowError)
    return rtnValue#

!    Pointer# = Pointer(JOBS)
!    Hold(JOBS,1)
!    Get(JOBS,Pointer#)
!    If Errorcode() = 43
!        If func:ShowError
!            Case Missive('The selected job (' & Clip(func:JobNumber) & ') is currently is use by another station.','ServiceBase 3g',|
!                           'mstop.jpg','/OK')
!                Of 1 ! OK Button
!            End ! Case Missive
!        End !If func:ShowError
!        Return Level:Fatal
!    End !If Errorcode() = 43
!    Return Level:Benign



! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CountHistory         PROCEDURE  (func:IMEINumber,func:JobNumber,func:DateBooked) ! Declare Procedure
save_job2_id         USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If func:IMEINumber = '' Or func:IMEINumber = 'N/A'
        Return 0
    End ! If func:IMEINumber = '' Or func:IMEINumber = 'N/A'
    Count# = 0

    Relate:DEFAULTS.Open()
    Relate:JOBS2_ALIAS.Open()

    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    Access:JOBS2_ALIAS.Clearkey(job2:ESN_Key)
    job2:ESN = func:IMEINumber
    Set(job2:ESN_Key,job2:ESN_Key)
    Loop
        If Access:JOBS2_ALIAS.Next()
            Break
        End ! If Access:JOB2_ALIAS.Next()
        If job2:ESN <> func:IMEINumber
            Break
        End ! If jobe2:ESN <> func:IMEINumber
        If job2:Cancelled = 'YES'
            Cycle
        End ! If job2:Cancelled = 'YES'
        If job2:Ref_Number <> func:JobNumber
! Changing (DBH 14/02/2008) # 9718 - Don't make the previous job check date dependant
!            If job2:date_booked + def:bouncertime > func:DateBooked And job2:date_booked <= func:datebooked
!                Count# += 1
!            End!If job2:date_booked + man:warranty_period < Today()
! to (DBH 14/02/2008) # 9718
            Count# += 1
! End (DBH 14/02/2008) #9718
        End ! If job2:Ref_Number <> func:JobNumber
    End ! Loop

    Relate:DEFAULTS.Close()
    Relate:JOBS2_ALIAS.Close()

    Return Count#
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IsJobInvoiced        PROCEDURE  (func:InvoiceNumber)  ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return (VodacomClass.HasJobBeenInvoiced(func:InvoiceNumber,glo:WebJob))

!    If func:InvoiceNumber = 0
!        Return Level:Benign
!    Else !If job:Invoice_Number = 0
!        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
!        inv:Invoice_Number  = func:InvoiceNumber
!        If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
!            !Found
!            If glo:WebJob
!                If inv:RRCInvoiceDate <> ''
!                    Return Level:Fatal
!                End !If inv:RRCInvoiceDate <> ''
!            Else !If glo:WebJob
!                If inv:ARCInvoiceDate <> ''
!                    Return Level:Fatal
!                End !If inv:ARCInvoiceDate <> ''
!            End !If glo:WebJob
!        Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
!            !Error
!        End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
!    End !If job:Invoice_Number = 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Insert_Password PROCEDURE                             !Generated from procedure template - Window

LocalRequest         LONG
tries_temp           BYTE
FilesOpened          BYTE
Version_Temp         STRING(40)
tmp:OSVersion        STRING(25)
tmp:makedate         DATE
tmp:maketime         TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:AccessNumber     STRING(20)
tmp:NewPassword      STRING(30)
tmp:ConfirmPassword  STRING(30)
tmp:Tries            BYTE(0)
CountedRRC           LONG
licensedRRC          LONG
window               WINDOW('Logon'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbwall1.jpg'),CENTERED,GRAY,DOUBLE
                       STRING(@s40),AT(68,392,148,9),USE(Version_Temp),TRN,CENTER,FONT('Tahoma',7,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       IMAGE('passlong.jpg'),AT(68,350),USE(?Image1)
                       ENTRY(@s20),AT(84,356,116,14),USE(glo:Password),CENTER,FONT(,,,FONT:bold),COLOR(COLOR:White),REQ,UPR,PASSWORD
                       BUTTON,AT(620,14),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpw.jpg')
                       PROMPT('ServiceBase'),AT(244,334),USE(?Prompt5),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       PROMPT('New Password'),AT(360,334),USE(?tmp:NewPassword:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       IMAGE('passlong.jpg'),AT(464,350),USE(?Image:ConfirmPassword),HIDE
                       ENTRY(@s30),AT(328,356,116,14),USE(tmp:NewPassword),HIDE,CENTER,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('New Password'),TIP('New Password'),UPR,PASSWORD
                       PROMPT('Confirm Password'),AT(504,334),USE(?tmp:ConfirmPassword:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       PROMPT('Password'),AT(124,334),USE(?tmp:NewPassword:Prompt:2),TRN,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       ENTRY(@s30),AT(480,356,116,14),USE(tmp:ConfirmPassword),HIDE,CENTER,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Confirm Password'),TIP('Confirm Password'),UPR,PASSWORD
                       IMAGE('passlong.jpg'),AT(312,350),USE(?Image:NewPassword),HIDE
                       IMAGE('logomain.jpg'),AT(218,6),USE(?Image2),HIDE,CENTERED
                       PROMPT('SRN:0000000'),AT(612,2),USE(?SRNNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       PROMPT('SB2000 No'),AT(68,382,148,10),USE(?Prompt1),CENTER,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(248,348),USE(?OkButton),TRN,FLAT,FONT(,,,FONT:bold),ICON('salesw.jpg'),DEFAULT
                       BUTTON,AT(624,326),USE(?CancelButton),TRN,FLAT,LEFT,ICON('exitverw.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
MobileCheck     Routine

    !message('In mobile check')
    if clip(use:MobileNumber) = '' then
        use:MobileNumber = CaptureUserMobile()
        Access:users.update()
    END

    EXIT
CheckTries  Routine
    If tmp:Tries => 3
        Case Missive('You have unsuccessfully tried to log into ServiceBase three times.'&|
          '<13,10>'&|
          '<13,10>The program will now quit.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        glo:Select1 = 'Y'
        Post(Event:CloseWindow)
    End !tmp:Tries > 3
Add_Login       Routine
    Clear(log:record)
    log:user_code   = use:user_code
    log:surname     = use:surname
    log:forename    = use:forename
    log:date        = Today()
    log:time        = Clock()
    access:logged.Insert()
    glo:timelogged = log:time
    if glo:webjob then
    !Remember this time at the client as well
    ClarioNET:CallClientProcedure('SETTIME',glo:timelogged)
    ClarioNET:Global.Param3 = glo:timeLogged
    END
    glo:select1 = 'N'
    Post(Event:CloseWindow)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020006'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Insert_Password')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Version_Temp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:LOCATION.Open
  Relate:LOGGED.Open
  Relate:TRADEAC2.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Start - Close the splash window (DBH: 25-05-2005)
      If glo:WebJob
          ClarioNET:CallClientProcedure('CLOSESPLASH')
      Else ! glo:WebJob
          PUTINI('STARTING','Run',True,)
      End ! glo:WebJob
      ! End   - Close the splash window (DBH: 25-05-2005)
      !message('At the open window bits')
      ! Save Window Name
   AddToLog('Window','Open','Insert_Password')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Set(defaults)
  access:defaults.next()
  
  tries_temp = 0
  
  glo:PassAccount    = Clip(Today()) & Clip(Clock())
  
  Set(DEFAULTV)
  Access:DEFAULTV.Next()
  If Error()
      If Access:DEFAULTV.PrimeRecord() = Level:Benign
          defv:VersionNumber    = '???'
          defv:NagDate    = Today()
          If Access:DEFAULTV.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFAULTV..TryInsert() = Level:Benign
              !Insert Failed
          End !DIf Access:DEFAULTV..TryInsert() = Level:Benign
      End !If Access:DEFAULTV..PrimeRecord() = Level:Benign
      Set(DEFAULTV)
      Access:DEFAULTV.Next()
  End !Error()
  
  Version_Temp = 'Version: ' & Clip(defv:VersionNumber)
  
  If defv:ReUpdate = 0
      NagScreen(1)
  Else !defv:ReUpdate = 0
      If Today() => defv:NagDate
          NagScreen(0)
      End !Today() > defv:NagDate
  End !defv:ReUpdate = 0
  
  If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') <> 1
      ?Image2{prop:Hide} = 0
  Else ! If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') = 1
      If GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI') <> ''
          ?Image2{prop:Text} = Clip(GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI'))
          ?Image2{prop:Hide} = 0
      End ! If GETINI('SBDEF','LogoPath',,Clip(Path()) & '\SB2KDEF.INI') <> ''
  End ! If GETINI('SBDEF','HideCompanyLogos',,Clip(Path()) & '\SB2KDEF.INI') = 1
  !set default location
  if glo:webjob then
      glo:UserAccountNumber = clip(ClarioNET:Global.Param2)
  Else
      glo:UserAccountNumber = getini('BOOKING','HeadAccount',,clip(path())&'\SB2KDEF.INI')
      !message('Looking up default account number gets '&clip(glo:UserAccountNumber))
  End !if glo webjob
  
  !lookup the location from the trade account
  access:tradeacc.clearkey(tra:Account_Number_Key)
  tra:Account_Number = glo:UserAccountNumber
  if access:tradeacc.fetch(tra:Account_Number_Key)
      !error can't find trade account - hard luck
      glo:Location = glo:UserAccountNumber
      !message('cant fetch trade account for ' &glo:userAccountNumber)
  ELSE
      glo:location = tra:SiteLocation
      !message('Found trade account location '&tra:sitelocation)
  
  END !If tradeacc.fetch
  
  !TB13512 - do not allow non-rrc or stopped accounts to log on
  if glo:webjob then
  
      if tra:RemoteRepairCentre = 0 or tra:Stop_Account = 'YES'
          hide(?glo:Password)
          hide(?OkButton)
          hide(?Prompt5)
          message('The selected account is not configured as a Remote Repair Centre')
      END !if not RRC or is stopped
  
  ELSE
      !not glo:Webjob - this is ARC signing in
      !TB13512 second part
      !At ServiceBase login check RRC Sites Enabled figure and RRC Licence Qty figure.
  
      !count up the number of active RRC
      CountedRRC = 0
      ACCESS:TradeAcc_Alias.clearkey(tra_ali:RecordNumberKey)
      tra_ali:RecordNumber = 0
      Set(tra_ali:RecordNumberKey,tra_ali:RecordNumberKey)
      Loop
          if access:Tradeacc_Alias.next() then break.
          if tra_ali:Stop_Account = 'NO' and tra_ali:RemoteRepairCentre = 1 then
              CountedRRC += 1
          END
      END
  
      !the number RRC permitted by licence
      LicensedRRC = def:IrishVatRate / 0.42
  
      if LicensedRRC < CountedRRC then
          !send an email saying this is so
          
          !J - 30/04/15 - when we started getting emails from the test system Harvey decided he didn't want fifty emails every day ...
          !so check we haven't already sent it today
          If today() > getini('VALIDATE','LastLogon',0,clip(path())&'\SB2kDef.ini')
  
              SendEmail('system@pccs.co.za', 'j.berry@pccsuk.com;n.pearson@pccsuk.com', 'Vodacom RRC Sites Exceed Licence','h.jackson@pccsuk.com','','',|
                        'Warning: There may have been a violation of the Vodacom license agreement.'   &|
                        '<13,10,13,10>Vodacom RRC Licence Quantity = '&clip(LicensedRRC) &|
                        '<13,10,13,10>Vodacom RRC Sites Enabled = '&clip(CountedRRC)     &|
                        '<13,10,13,10>Data path = ' &clip(path())                        &|
                        '<13,10,13,10>Please check the legitimacy of this email with Harvey before approaching Vodacom '&|
                        '- it is possible this is a false statement from a test environment'&|
                        '<13,10,13,10>---------------------------------------------')
  
              putini('VALIDATE','LastLogon',today(),clip(path())&'\SB2kDef.ini')
  
          END !if not already sent this today
      END
  
  END !if glo:Webjob
  
  
  !end TB13512
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:LOCATION.Close
    Relate:LOGGED.Close
    Relate:TRADEAC2.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Insert_Password')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      glo:Select1 = 'Y'                                                     
      If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
          glo:Select1 = 'N'
          Post(Event:CloseWindow)
      Else !FullAccess(glo:PassAccount,glo:Password) = Level:Benign

          LicenceError# = FALSE

!          !TB1329 - remove use of license file and use the new field in defaults2
!          share(license)
!          if (error())
!              create(license)
!              share(license)
!          end
!
!          set(license,0)
!          next(license)
!          if (error())
!              !lic:ExpiryDate = Today() + 14
!              lic:ExpiryDate = DEFORMAT('01/07/2014',@d06b)
!              add(license)
!          end
!          if (lic:ExpiryDate = 0)
!              lic:ExpiryDate = DEFORMAT('01/07/2014',@d06b)
!              !lic:ExpiryDate = Today() + 14
!              add(license)
!          end
      
          !#12598 Update the field for VCP and PUP (DBH: 21/06/2012)
          !Relate:DEFAULT2.Open()
          SET(DEFAULT2,0)
          Access:DEFAULT2.Next()
          if de2:J_Collection_Rate = 0 then
            de2:J_Collection_Rate = deformat('01/07/2014',@d06b)
          END
          de2:PLE = de2:J_Collection_Rate
          Access:DEFAULT2.TryUpdate()
          !Relate:DEFAULT2.Close()
      
          if (de2:J_Collection_Rate < Today())            !was LIC:ExpiryDate
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Licence EXPIRED!'&|
                      '|'&|
                      '|You can no longer use ServiceBase.'&|
                      '|Please contact PC Control Systems to renew your licence.','ServiceBase',|
                                 'mexclam.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message 
              LicenceError# = 1
          elsif (de2:J_Collection_Rate < Today() + 14)        !was LIC:ExpiryDate
              CASE (de2:J_Collection_Rate - TODAY())          !was LIC:ExpiryDate
              OF 0
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Licence Warning!'&|
                      '|'&|
                      '|Your ServiceBase licence is due to expire today.'&|
                      '|Please contact PC Control Systems to renew your licence.','ServiceBase',|
                                 'mexclam.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message            
              OF 1
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Licence Warning!'&|
                      '|'&|
                      '|Your ServiceBase licence is due to expire tomorrow.'&|
                      '|Please contact PC Control Systems to renew your licence.','ServiceBase',|
                                 'mexclam.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message            
              ELSE
                  Beep(Beep:SystemExclamation)  ;  Yield()
                  Case Missive('Licence Warning!'&|
                      '|'&|
                      '|Your ServiceBase licence is due to expire in ' & de2:J_Collection_Rate - TODAY() & ' days.'&|
                      '|Please contact PC Control Systems to renew your licence.','ServiceBase',|
                                 'mexclam.jpg','/&OK') 
                  Of 1 ! &OK Button
                  End!Case Message            
              END
          end
          close(license)

          IF (LicenceError# = 0)
      
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key)
                  !Not Found
                  glo:Password = ''
                  Case Missive('Invalid Password!','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Select(?glo:Password)
                  tmp:Tries += 1
                  Do CheckTries
              Else ! If Access:USERS.Tryfetch(usePassword_Key) = LevelBenign
                  !check for location
                  !message('Got global = '&clip(glo:location)&' user = '&clip(use:location))
                  LocationError# = 0
                  If glo:WebJob
                      If glo:Location <> use:Location
                          LocationError# = 1
                      End !If glo:Location <> use:Location
                  Else !If glo:WebJob
                      access:Location.clearkey(loc:Location_Key)
                      loc:Location = use:Location
                      if access:location.fetch(loc:location_key) then
                          !Error
                      ELSE
                          if loc:Main_Store = 'YES' then
                              !OK
                          ELSE
                              If use:Location <> glo:Location
                                  LocationError# = 1
                              End !If use:Location <> glo:Location
                          END  !if main store
                      END !if access fetch location
      
                  End !If glo:WebJob
      
                  If LocationError# = 1
      
                      glo:Password = ''
                      Case Missive('User not valid at this location!','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      Select(?glo:Password)
                      tmp:Tries += 1
                      error# = 1
                      Do CheckTries
      
                  ELSE
                      Error# = 0
                      If ?tmp:NewPassword{prop:Hide} = 0
                          If tmp:NewPassword <> tmp:ConfirmPassword
                              Case Missive('The entered passwords do not match.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Error# = 1
                              Select(?tmp:NewPassword)
                          End!If tmp:NewPassword <> tmp:ConfirmPassword
                          If tmp:NewPassword = ''
                              Select(?tmp:NewPassword)
                              Error# = 1
                          End !If tmp:NewPassword = ''
      
                          If tmp:NewPassword = glo:Password
                              Case Missive('You must select a different password.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Select(?tmp:NewPassword)
                              Error# = 1
                          End !If tmp:NewPassword = glo:Password
      
                          ! Inserting (DBH 16/12/2005) #6635 - Show a message if the user selects a password that is already in use
                          If Error# = 0
                              Access:USERS_ALIAS.ClearKey(use_ali:password_key)
                              use_ali:Password = tmp:NewPassword
                              If Access:USERS_ALIAS.TryFetch(use_ali:password_key) = Level:Benign
                                  !Found
                                  Case Missive('The selected password has already been used.'&|
                                    '|Please select another.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Error# = 1
                              Else !If Access:USERS_ALIAS.TryFetch(use_ali:password_key) = Level:Benign
                                  !Error
                              End !If Access:USERS_ALIAS.TryFetch(use_ali:password_key) = Level:Benign
                          End ! If Error# = 0
                          ! End (DBH 16/12/2005) #6635
      
                          If Error# = 0
                              use:Password    = tmp:NewPassword
                              use:PasswordLastChanged = Today()
                              If Access:USERS.Update() = Level:Benign
                                  glo:Password    = tmp:NewPassword
                                  Do Add_Login
                              Else
                                  Error# = 1
                                  Select(?tmp:NewPassword)
                              End !If Access:USERS.Update() = Level:Benign
                          End !If tmp:NewPassword <> tmp:ConfirmPassword Or tmp:NewPassword = ''
                      End !?tmp:NewPassword{prop:Hide} = 0
                  END !if locations mismatch
      
                  If use:Active <> 'YES' And Error# = 0
                      Case Missive('The entered password is no longer active.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      glo:Password = ''
                      Select(?glo:Password)
                      tmp:Tries += 1
                      Do CheckTries
                      Error# = 1
                  Else !If use:Active <> 'YES'
                      If use:Restrict_Logins = 'YES'
                          Count# = 1
                          Access:LOGGED.ClearKey(log:User_Code_Key)
                          log:User_Code   = use:User_Code
                          Set(log:User_Code_Key,log:User_Code_Key)
                          Loop
                              If Access:LOGGED.Next()
                                  Break
                              End !If Access:LOGGED.Next()
                              If log:User_Code <> use:User_Code
                                  Break
                              End !If log:User_Code <> use:User_Code
                              Count# += 1
                          End !Loop
                          If Count# > use:Concurrent_Logins
                              Case Missive('You have exceeded your maximum number of logins.','ServiceBase 3g',|
                                             'mstop.jpg','/OK')
                                  Of 1 ! OK Button
                              End ! Case Missive
                              glo:Password = ''
                              Select(?glo:Password)
                              tmp:Tries += 1
                              Do CheckTries
                              Error# = 1
                          End !If Count# > use:Concurrent_Logins
                      End !If use:Restrict_Logins = 'YES'
      
                      If use:RenewPassword <> 0 And Error# = 0
                          If use:PasswordLastChanged = ''
                              use:PasswordLastChanged = Today()
                              Access:USERS.Update()
                          Else !If use:PasswordLastChanged = ''
                              If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today()
                                  Case Missive('Your password has expired.'&|
                                    '<13,10>'&|
                                    '<13,10>Please insert a new one.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  ?glo:Password{prop:Disable} = 1
                                  ?tmp:NewPassword{prop:Hide} = 0
                                  ?Image:NewPassword{prop:Hide} = False
                                  ?tmp:ConfirmPassword{prop:Hide} = 0
                                  ?Image:ConfirmPassword{prop:Hide} = False
                                  ?tmp:NewPassword:Prompt{prop:Hide} = 0
                                  ?tmp:ConfirmPassword:Prompt{prop:Hide} = 0
                                  Select(?tmp:NewPassword)
                                  Error# = 1
                              End !If use:PasswordLastChanged + use:RenewPassword < Today()
                          End !If use:PasswordLastChanged = ''
                      End !If use:RenewPassword <> 0
      
                      if Error# = 0
                          glo:ErrorText = ''
                          Do MobileCheck
                          if glo:ErrorText = 'CANCEL' then
                              Error# = 1
                              glo:Password = ''
                              Select(?glo:Password)
                              tmp:Tries += 1
                              Do CheckTries
      
                          END !if mobile checked, missing and not addeed
                      END
      
                      If Error# = 0
                          glo:UserCode = use:User_Code
                          glo:ErrorText = ''
                          Do Add_Login
                      End !If Error# = 0
                  End !If use:Active <> 'YES'
              End !If AccessUSERS.Tryfetch(usePassword_Key) = LevelBenign
          END   !if LicenceError# = 0
      End !FullAccess(glo:PassAccount,glo:Password) = Level:Benign
      
      
      !reset globals in case this is a re-login
      glo:RelocateStore     = false
      glo:RelocatedFrom     = ''
      glo:RelocatedMark_Up  = 0
      
      
      !this bit here needs to be called only if AA20
      !!lookup the location from the trade account
      !!Message('Now have user location as '&clip(use:Location))
      !
      access:tradeacc.clearkey(tra:SiteLocationKey)
      tra:SiteLocation = use:Location
      if access:Tradeacc.fetch(tra:SiteLocationKey)
          !error
          !message('Error on fetch trade account by '&clip(Use:location))
      ELSE
          !message('Found trade account site location as '&clip(tra:SiteLocation)&' '&clip(tra:Account_Number)&' '&tra:UseSparesRequest)
      
          Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
          tra2:Account_Number = tra:Account_Number
          if access:TradeAc2.fetch(TRA2:KeyAccountNumber)
              tra2:UseSparesRequest = ''
          END
      
          if tra:Account_Number = 'AA20' and tra2:UseSparesRequest = 'YES' then
            !message('Relocating store')
            !Set the global flag
            glo:RelocateStore = true
            glo:RelocatedFrom = tra:SiteLocation
      
            !now find the markup ...
            Access:LOCATION.ClearKey(loc:ActiveLocationKey)
            loc:Active   = 1
            loc:Location = tra:SiteLocation
            If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
                !Found - remember the markup from this location
                glo:RelocatedMark_Up  = loc:OutWarrantyMarkUp
            END
      
          END
      END !If tradeacc.fetch
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020006'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020006'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020006'&'0')
      ***
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      glo:select1 = 'Y'
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?glo:Password
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?glo:Password)
    ?glo:Password{prop:Touched}=true
    
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?glo:Password)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
      glo:select1 = 'Y'
      glo:password = ''
      ?Prompt1{prop:Text} = 'SB3g No: ' & Clip(glo:PassAccount)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
NagScreen PROCEDURE (func:Type)                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(160,17,360,385),USE(?Sheet1),COLOR(0D6EAEFH),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PANEL,AT(164,22,352,12),USE(?Panel1),FILL(09A6A7CH)
                           PROMPT('Update Required'),AT(168,24),USE(?Prompt6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('SRN: 0000000'),AT(460,24),USE(?SRNNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PANEL,AT(164,38,352,148),USE(?Panel2),FILL(COLOR:White),BEVEL(-1,-1)
                           IMAGE('sbtitle.jpg'),AT(176,42),USE(?Image2),CENTERED
                           PANEL,AT(164,190,352,176),USE(?Panel4),FILL(09A6A7CH)
                           PROMPT('Update Required'),AT(264,198),USE(?Prompt1),FONT(,20,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('ServiceBase 3g is overdue it''s latest update.'),AT(176,236,332,36),USE(?ErrorText),CENTER,FONT(,14,COLOR:Aqua,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Please contact your System Supervisor who should be able to download the latest ' &|
   'version.'),AT(250,286,180,16),USE(?Prompt3),CENTER,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('If you have any problems/questions, please contact PC Control Systems.'),AT(250,308,180,16),USE(?Prompt3:2),CENTER,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Click ''OK'' to clear this message.'),AT(281,340),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(448,370),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                           PANEL,AT(164,370,352,28),USE(?Panel3),FILL(09A6A7CH)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020007'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('NagScreen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','NagScreen')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','NagScreen')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If func:Type = 1
          ?ErrorText{prop:Text} = 'An Error has occured with you program. Please install your latest update again.'
      End !func:Type = 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateESNMODEL PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
save_ESNMODEL_id     USHORT,AUTO
save_esn_id          USHORT,AUTO
save_esa_ali_id      USHORT,AUTO
save_esa_id          USHORT,AUTO
FilesOpened          BYTE
ActionMessage        CSTRING(40)
tmp:ReplacementValue REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW14::View:Browse   VIEW(ESNMODAL)
                       PROJECT(esa:TacCode)
                       PROJECT(esa:ModelNumber)
                       PROJECT(esa:RecordNumber)
                       PROJECT(esa:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
esa:TacCode            LIKE(esa:TacCode)              !List box control field - type derived from field
esa:ModelNumber        LIKE(esa:ModelNumber)          !List box control field - type derived from field
esa:RecordNumber       LIKE(esa:RecordNumber)         !Primary key field - type derived from field
esa:RefNumber          LIKE(esa:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::esn:Record  LIKE(esn:RECORD),STATIC
QuickWindow          WINDOW('Update the ESNMODEL File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,308),USE(?CurrentTab),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           SHEET,AT(372,104,208,214),USE(?Sheet2),COLOR(09A6A7CH),SPREAD
                             TAB('Alternative TAC Codes'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s6),AT(392,120,124,10),USE(esa:TacCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('TAC Code'),TIP('TAC Code'),UPR
                               LIST,AT(392,133,168,145),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('43L(2)|M~TAC Code~@s8@120L(2)|M~Model Number~@s30@'),FROM(Queue:Browse)
                               BUTTON,AT(376,286),USE(?Insert),TRN,FLAT,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ICON('insertp.jpg')
                               BUTTON,AT(444,286),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                               BUTTON,AT(512,286),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                             END
                           END
                           PROMPT('I.M.E.I. Number'),AT(96,144),USE(?ESN:ESN:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(172,144,64,10),USE(esn:ESN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),REQ,UPR,MSG('I.M.E.I. Number')
                           PROMPT('Manufacturer'),AT(96,168),USE(?esn:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(172,168,124,10),USE(esn:Manufacturer),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(300,164),USE(?LookupManufacturer),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model Number'),AT(96,194),USE(?esn:Model_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(172,194,124,10),USE(esn:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(300,190),USE(?LookupModelNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Replacement Value'),AT(96,220),USE(?mod:ReplacementValue:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(172,220,64,10),USE(tmp:ReplacementValue),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Replacement Value'),TIP('Replacement Value'),UPR,READONLY
                           CHECK('Include For 48 Hour Exchange'),AT(172,246),USE(esn:Include48Hour),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Include For 48 Hour Exchange'),TIP('Include For 48 Hour Exchange'),VALUE('1','0')
                           CHECK('  Active'),AT(172,262),USE(esn:Active),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('Y','N')
                           BUTTON,AT(68,312),USE(?ReplicateTACCodes),TRN,FLAT,ICON('reptacp.jpg')
                         END
                       END
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Inserting / Updating A Model Number'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW14                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:esn:Manufacturer                Like(esn:Manufacturer)
look:esn:Model_Number                Like(esn:Model_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Replicate       Routine
Data
local:CurrentRecordNumber       Long()
Code
    Setcursor(Cursor:Wait)

    If Access:ESNMODEL.Update() = Level:Benign

        Prog.ProgressSetup(30)

        local:CurrentRecordNumber = esn:Record_Number

        save_ESNMODEL_id = Access:ESNMODEL.SaveFile()

        Access:ESNMODEL_ALIAS.Clearkey(esn_ali:Record_Number_Key)
        esn_ali:Record_Number    = local:CurrentRecordNumber
        If Access:ESNMODEL_ALIAS.Tryfetch(esn_ali:Record_Number_Key) = Level:Benign
            !Found

            !Go through other entries with same model number -  (DBH: 11-11-2003)
            Save_esn_ID = Access:ESNMODEL.SaveFile()
            Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
            esn:Model_Number = esn_ali:Model_Number
            Set(esn:Model_Number_Key,esn:Model_Number_Key)
            Loop
                If Access:ESNMODEL.NEXT()
                   Break
                End !If
                If esn:Model_Number <> esn_ali:Model_Number      |
                    Then Break.  ! End If

                If Prog.InsideLoop()
                    Break
                End !If Prog.InsideLoop()
                !Ignore the current record -  (DBH: 11-11-2003)
                If esn:Record_Number = local:CurrentRecordNumber
                    Cycle
                End !If esn:Record_Number = local:CurrentRecordNumber

                esn:Include48Hour = esn_Ali:Include48Hour
                Access:ESNMODEL.Update()

                !Remove exist alternative models first. So that the list is identical
                !to the original - 3791 (DBH: 16-01-2004)
                Save_esa_ID = Access:ESNMODAL.SaveFile()
                Access:ESNMODAL.ClearKey(esa:TACCodeKey)
                esa:RefNumber = esn:Record_Number
                Set(esa:TACCodeKey,esa:TACCodeKey)
                Loop
                    If Access:ESNMODAL.NEXT()
                       Break
                    End !If
                    If esa:RefNumber <> esn:Record_Number      |
                        Then Break.  ! End If
                    Delete(ESNMODAL)
                End !Loop
                Access:ESNMODAL.RestoreFile(Save_esa_ID)

                !Go through attached TAC Codes and
                !add to this record -  (DBH: 11-11-2003)
                Save_esa_ali_ID = Access:ESNMODAL_ALIAS.SaveFile()
                Access:ESNMODAL_ALIAS.ClearKey(esa_ali:TACCodeKey)
                esa_ali:RefNumber = local:CurrentRecordNumber
                Set(esa_ali:TACCodeKey,esa_ali:TACCodeKey)
                Loop
                    If Access:ESNMODAL_ALIAS.NEXT()
                       Break
                    End !If
                    If esa_ali:RefNumber <> local:CurrentRecordNumber      |
                        Then Break.  ! End If
                    Access:ESNMODAL.ClearKey(esa:TacModelKey)
                    esa:RefNumber   = esn:Record_Number
                    esa:TacCode     = esa_ali:TacCode
                    esa:ModelNumber = esa_ali:ModelNumber
                    If Access:ESNMODAL.TryFetch(esa:TacModelKey) = Level:Benign
                        !Found

                    Else !If Access:ESNMODAL.TryFetch(esa:TacModelKey) = Level:Benign
                        !Error
                        If Access:ESNMODAL.PrimeRecord() = Level:Benign
                            esa:RefNumber    = esn:Record_Number
                            esa:TacCode      = esa_ali:TACCode
                            esa:ModelNumber  = esa_ali:ModelNumber
                            If Access:ESNMODAL.TryInsert() = Level:Benign
                                !Insert Successful

                            Else !If Access:ESNMODAL.TryInsert() = Level:Benign
                                !Insert Failed
                                Access:ESNMODAL.CancelAutoInc()
                            End !If Access:ESNMODAL.TryInsert() = Level:Benign
                        End !If Access:ESNMODAL.PrimeRecord() = Level:Benign

                    End !If Access:ESNMODAL.TryFetch(esa:TacModelKey) = Level:Benign
                End !Loop
                Access:ESNMODAL_ALIAS.RestoreFile(Save_esa_ali_ID)
            End !Loop
            Access:ESNMODEL.RestoreFile(Save_esn_ID)
        Else ! If Access:ESNMODEL_ALIAS.Tryfetch(esn_ali:Record_Number_Key) = Level:Benign
            !Error
        End !If Access:ESNMODEL_ALIAS.Tryfetch(esn_ali:Record_Number_Key) = Level:Benign


        Access:ESNMODEL.RestoreFile(save_ESNMODEL_id)

        Prog.ProgressFinish()

        SetCursor()
        Case Missive('Replication Completed.','ServiceBase 3g','MIdea.jpg','/OK')
            Of 1  !/OK
        End !Case Missive
        Post(Event:Accepted,?OK)
    End !If Access:ESNMODEL.Update() = Level:Benign
GetReplacementValue     Routine
    !Get the Replacement Value -  (DBH: 14-10-2003)
    tmp:ReplacementValue = 0
    If esn:Model_Number <> ''
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = esn:Model_Number
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            tmp:ReplacementValue = mod:ReplacementValue
            Display()
        Else !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
    End !If esn:Model_Number <> ''
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Model Number'
  OF ChangeRecord
    ActionMessage = 'Changing A Model Number'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020005'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateESNMODEL')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?esa:TacCode
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:ESNMODEL)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(esn:Record,History::esn:Record)
  SELF.AddHistoryField(?esn:ESN,2)
  SELF.AddHistoryField(?esn:Manufacturer,4)
  SELF.AddHistoryField(?esn:Model_Number,3)
  SELF.AddHistoryField(?esn:Include48Hour,5)
  SELF.AddHistoryField(?esn:Active,6)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ESNMODAL.Open
  Relate:ESNMODAL_ALIAS.Open
  Relate:ESNMODEL_ALIAS.Open
  Relate:MANUFACT.Open
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ESNMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW14.Init(?List,Queue:Browse.ViewPosition,BRW14::View:Browse,Queue:Browse,Relate:ESNMODAL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Do GetReplacementValue
  
  
      ! Save Window Name
   AddToLog('Window','Open','UpdateESNMODEL')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?esn:Manufacturer{Prop:Tip} AND ~?LookupManufacturer{Prop:Tip}
     ?LookupManufacturer{Prop:Tip} = 'Select ' & ?esn:Manufacturer{Prop:Tip}
  END
  IF ?esn:Manufacturer{Prop:Msg} AND ~?LookupManufacturer{Prop:Msg}
     ?LookupManufacturer{Prop:Msg} = 'Select ' & ?esn:Manufacturer{Prop:Msg}
  END
  IF ?esn:Model_Number{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?esn:Model_Number{Prop:Tip}
  END
  IF ?esn:Model_Number{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?esn:Model_Number{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW14.Q &= Queue:Browse
  BRW14.AddSortOrder(,esa:TACCodeKey)
  BRW14.AddRange(esa:RefNumber,Relate:ESNMODAL,Relate:ESNMODEL)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(?esa:TacCode,esa:TacCode,1,BRW14)
  BRW14.AddField(esa:TacCode,BRW14.Q.esa:TacCode)
  BRW14.AddField(esa:ModelNumber,BRW14.Q.esa:ModelNumber)
  BRW14.AddField(esa:RecordNumber,BRW14.Q.esa:RecordNumber)
  BRW14.AddField(esa:RefNumber,BRW14.Q.esa:RefNumber)
  BRW14.AskProcedure = 3
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODAL.Close
    Relate:ESNMODAL_ALIAS.Close
    Relate:ESNMODEL_ALIAS.Close
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateESNMODEL')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  !Pass the IMEI Number to the Alternative Form -  (DBH: 15-10-2003)
  glo:Select1 = esn:ESN
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      SelectModelKnownMake
      UpdateAlternativeTACCodes
    END
    ReturnValue = GlobalResponse
  END
  glo:Select1 = ''
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      !check to see if we this combination already exists
      
      checkit# = 0
      !if ThisWindow.Request = InsertRecord then
      
          Access:ESNModel_Alias.clearkey(esn_ali:ESN_Key)
          esn_ali:ESN             = esn:ESN
          esn_ali:Model_Number    = esn:Model_Number
          Set(esn_ali:ESN_Key,esn_ali:ESN_Key)
          loop
              If Access:ESNModel_Alias.next() then
                  break
              End
              If esn_ali:ESN <> esn:ESN then
                  break
              End
              If esn_ali:Model_Number <> esn:Model_Number then
                  break
              End
              !now we need to compare the manufacturer
              if esn:Manufacturer = esn_ali:Manufacturer then
                  IF (ThisWindow.Request <> InsertRecord)
                      ! #11855 Check for duplicates (Bryan: 15/12/2010)
                      If (esn_ali:Record_Number = esn:Record_Number)
                          CYCLE
                      END
                  END
      
                  !this is a duplicate record - dont allow it
                  miss# = missive('This Combination already exists','Servicebase 3g Error','Mstop.jpg','/OK')
                  Checkit# = 1
                  Break
              End
          End
      
      !end
      
      if checkit# = 1 then
          cycle
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?esn:Manufacturer
      IF esn:Manufacturer OR ?esn:Manufacturer{Prop:Req}
        man:Manufacturer = esn:Manufacturer
        !Save Lookup Field Incase Of error
        look:esn:Manufacturer        = esn:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            esn:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            esn:Manufacturer = look:esn:Manufacturer
            SELECT(?esn:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupManufacturer
      ThisWindow.Update
      man:Manufacturer = esn:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          esn:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?esn:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?esn:Manufacturer)
    OF ?esn:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn:Model_Number, Accepted)
      Do GetReplacementValue
      IF esn:Model_Number OR ?esn:Model_Number{Prop:Req}
        mod:Model_Number = esn:Model_Number
        mod:Manufacturer = esn:Manufacturer
        GLO:Select2 = esn:Manufacturer
        !Save Lookup Field Incase Of error
        look:esn:Model_Number        = esn:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            esn:Model_Number = mod:Model_Number
          ELSE
            CLEAR(mod:Manufacturer)
            CLEAR(GLO:Select2)
            !Restore Lookup On Error
            esn:Model_Number = look:esn:Model_Number
            SELECT(?esn:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn:Model_Number, Accepted)
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = esn:Model_Number
      GLO:Select2 = esn:Manufacturer
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          esn:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?esn:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?esn:Model_Number)
    OF ?ReplicateTACCodes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateTACCodes, Accepted)
      If esn:ESN = '' Or esn:Model_Number = ''
          Case Missive('The I.M.E.I. Number And Model Number must be filled in before you can replicate the TAC Codes.','ServiceBase 3g','MStop.jpg','/OK')
              Of 1  !/OK
          End !Case Missive
      Else !esn:ESN = '' Or esn:Model_Number = ''
          Case Missive('Replicate ALL TAC Codes attached to IMEI No:|' & Clip(esn:ESN) & '|to all other IMEI nos associted with Model No:|' & Clip(esn:Model_Number)&'?','ServiceBase 3g','MQuest.jpg','\No|/Yes')
              Of 1  !No
      
              Of 2  !Yes
                  Do Replicate
          End !Case Missive
      End !esn:ESN = '' Or esn:Model_Number = ''
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateTACCodes, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020005'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020005'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020005'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?esn:Manufacturer
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn:Manufacturer, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn:Manufacturer, AlertKey)
    END
  OF ?esn:Model_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn:Model_Number, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn:Model_Number, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW14.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
UpdateAlternativeTACCodes PROCEDURE                   !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::esa:Record  LIKE(esa:RECORD),STATIC
QuickWindow          WINDOW('Update Alternative TAC Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Update Alternative TAC Codes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,246),USE(?CurrentTab),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Original TAC Code'),AT(216,189),USE(?glo:Select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(312,189,64,10),USE(GLO:Select1),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(0D6EAEFH),READONLY
                           PROMPT('Alternative TAC Code'),AT(216,208),USE(?esa:TacCode:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(312,208,64,10),USE(esa:TacCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('TAC Code'),TIP('TAC Code'),REQ,UPR
                           PROMPT('Model Number'),AT(215,224),USE(?esa:ModelNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(312,224,124,10),USE(esa:ModelNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),MSG('Model Number'),TIP('Model Number'),REQ,UPR
                           BUTTON,AT(438,219),USE(?LookupModelNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:esa:ModelNumber                Like(esa:ModelNumber)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Alternative TAC Code'
  OF ChangeRecord
    ActionMessage = 'Changing An Alternative TAC Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020004'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateAlternativeTACCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:ESNMODAL)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(esa:Record,History::esa:Record)
  SELF.AddHistoryField(?esa:TacCode,3)
  SELF.AddHistoryField(?esa:ModelNumber,4)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ESNMODAL.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ESNMODAL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','UpdateAlternativeTACCodes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?esa:ModelNumber{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?esa:ModelNumber{Prop:Tip}
  END
  IF ?esa:ModelNumber{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?esa:ModelNumber{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODAL.Close
    Relate:MODELNUM.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateAlternativeTACCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Model_Numbers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020004'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020004'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020004'&'0')
      ***
    OF ?esa:ModelNumber
      IF esa:ModelNumber OR ?esa:ModelNumber{Prop:Req}
        mod:Model_Number = esa:ModelNumber
        !Save Lookup Field Incase Of error
        look:esa:ModelNumber        = esa:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            esa:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            esa:ModelNumber = look:esa:ModelNumber
            SELECT(?esa:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = esa:ModelNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          esa:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?esa:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?esa:ModelNumber)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Select_ESN_Model PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
esn_temp             STRING(6)
FilesOpened          BYTE
model_number_temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ESNMODEL)
                       PROJECT(esn:Model_Number)
                       PROJECT(esn:Record_Number)
                       PROJECT(esn:ESN)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
esn:Model_Number       LIKE(esn:Model_Number)         !List box control field - type derived from field
esn:Record_Number      LIKE(esn:Record_Number)        !Primary key field - type derived from field
esn:ESN                LIKE(esn:ESN)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Sellfone'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,178,148,148),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Model Number~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,244),USE(?Insert:3),FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(448,274),USE(?Change:3),FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(448,302),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By ESN'),USE(?Tab:3)
                           PROMPT('You have selected the I.M.E.I. Number:'),AT(217,138,148,16),USE(?Prompt1),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(365,138),USE(GLO:Select7),TRN,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI)
                           PROMPT('The following Model Number correspond to that I.M.E.I. Number. Please select one' &|
   '.'),AT(184,150,312,16),USE(?Prompt2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,164,124,10),USE(esn:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Model Number'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,188),USE(?Select),FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(448,332),USE(?Close),FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020001'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Select_ESN_Model')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ESNMODEL.Open
  SELF.FilesOpened = True
  esn_temp = Sub(glo:select7,1,6)
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ESNMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Select_ESN_Model')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,esn:ESN_Key)
  BRW1.AddRange(esn:ESN,esn_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?esn:Model_Number,esn:Model_Number,1,BRW1)
  BRW1.AddField(esn:Model_Number,BRW1.Q.esn:Model_Number)
  BRW1.AddField(esn:Record_Number,BRW1.Q.esn:Record_Number)
  BRW1.AddField(esn:ESN,BRW1.Q.esn:ESN)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODEL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Select_ESN_Model')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateESNMODEL
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020001'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020001'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020001'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

AddToStockAllocation PROCEDURE  (func:PartRecordNumber,func:PartType,func:Quantity,func:Status,func:Engineer) ! Declare Procedure
save_sto_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_epr_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
tmp:CurrentAccount   STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:STOCKALL.Open
   Relate:STOCKALX.Open
            If glo:WebJob
                tmp:CurrentAccount  = Clarionet:Global.Param2
            Else !If glo:WebJob
                tmp:CurrentAccount  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
            End !If glo:WebJob


    VodacomClass.AddPartToStockAllocation(func:PartRecordNumber,func:PartType,func:Quantity,func:Status,func:Engineer,tmp:CurrentAccount)


!    !Part Record Number
!    !Part Type
!    !Quantity
!
!    Save_sto_id     = Access:STOCK.SaveFile()
!    Save_par_id     = Access:PARTS.SaveFile()
!    Save_wpr_id     = Access:WARPARTS.SaveFile()
!    Save_epr_id     = Access:ESTPARTS.SaveFile()
!    save_tra_id     = Access:TRADEACC.SaveFile()
!
!    If glo:WebJob
!        tmp:CurrentAccount  = Clarionet:Global.Param2
!    Else !If glo:WebJob
!        tmp:CurrentAccount  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
!    End !If glo:WebJob
!
!    !Add to Rapid Stock Allocation - L873 (DBH: 22-07-2003)
!
!    !Does this part already exist in Stock Allocation? -  (DBH: 23-07-2003)#
!    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!    tra:Account_Number  = tmp:CurrentAccount
!    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!        !Found
!
!    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!        !Error
!    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!
!    Access:STOCKALL.ClearKey(stl:PartRecordNumberKey)
!    stl:Location         = tra:SiteLocation
!    stl:PartType         = func:PartType
!    stl:PartRecordNumber = func:PartRecordNumber
!    If Access:STOCKALL.TryFetch(stl:PartRecordNumberKey) = Level:Benign
!        !Found
!        If local.WriteRecord() = True
!            Access:STOCKALL.Update()
!        End ! If local.WriteRecord() = True
!
!    Else !If Access:STOCKALL.TryFetch(stl:PartRecordNumberKey) = Level:Benign
!        !Error
!
!        ! Only add to Stock Allocation if not a "Correction" part - TrkBs: 6245 (DBH: 23-08-2005)
!        If Access:STOCKALL.PrimeRecord() = Level:Benign
!            If local.WriteRecord() = True
!                If Access:STOCKALL.TryInsert() = Level:Benign
!                    !Insert Successful
!                Else !If Access:STOCKALL.TryInsert() = Level:Benign
!                    !Insert Failed
!                    Access:STOCKALL.CancelAutoInc()
!                End !If Access:STOCKALL.TryInsert() = Level:Benign
!            Else ! If local:WriteRecord() = True
!                Access:STOCKALL.CancelAutoInc()
!            End ! If local:WriteRecord() = True
!        End !If Access:STOCKALL.PrimeRecord() = Level:Benign
!    End !If Access:STOCKALL.TryFetch(stl:PartRecordNumberKey) = Level:Benign
!
!
!    Access:STOCK.RestoreFile(save_sto_id)
!    Access:PARTS.RestoreFile(save_par_id)
!    Access:WARPARTS.RestoreFile(save_wpr_id)
!    Access:ESTPARTS.RestoreFile(save_epr_id)
!    Access:TRADEACC.RestoreFile(save_tra_id)
!
!local.WriteRecord   Procedure()
!Code
!    stl:Location         = tra:SiteLocation
!    stl:Status          = func:Status
!    stl:Engineer        = func:Engineer
!    Case func:PartType
!        Of 'CHA'
!            Access:PARTS.Clearkey(par:RecordNumberKey)
!            par:Record_Number   = func:PartRecordNumber
!            If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
!                !Found
!                ! Start - Do not add to Stock Allocation if part is a "Correction" - TrkBs: 6245 (DBH: 23-08-2005)
!                If par:Correction
!                    Return False
!                End ! If par:Correction
!                ! End   - Do not add to Stock Allocation if part is a "Correction" - TrkBs: 6245 (DBH: 23-08-2005)
!
!                stl:PartNumber  = par:Part_Number
!                stl:Description = par:Description
!                Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                sto:Ref_Number  = par:Part_Ref_Number
!                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Found
!                    stl:PartRefNumber   = sto:Ref_Number
!                    stl:ShelfLocation   = sto:Shelf_Location
!                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Error
!                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                stl:JobNumber       = par:Ref_Number
!                stl:PartRecordNumber = par:Record_Number
!                stl:PartType         = 'CHA'
!                stl:Quantity         = func:Quantity
!            Else ! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
!                !Error
!            End !If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
!        Of 'WAR'
!            Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
!            wpr:Record_Number   = func:PartRecordNumber
!            If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
!                !Found
!                ! Start - Do not add to Stock Allocation if part is a "Correction" - TrkBs: 6245 (DBH: 23-08-2005)
!                If wpr:Correction
!                    Return False
!                End ! If par:Correction
!                ! End   - Do not add to Stock Allocation if part is a "Correction" - TrkBs: 6245 (DBH: 23-08-2005)
!
!                stl:PartNumber  = wpr:Part_number
!                stl:Description = wpr:Description
!                Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                sto:Ref_Number  = wpr:Part_Ref_Number
!                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Found
!                    stl:PartRefNumber   = sto:Ref_Number
!                    stl:ShelfLocation   = sto:Shelf_Location
!                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Error
!                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                stl:JobNumber   = wpr:Ref_Number
!                stl:PartRecordNumber    = wpr:Record_Number
!                stl:PartType    = 'WAR'
!                stl:Quantity    = func:Quantity
!            Else ! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
!                !Error
!            End !If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
!        Of 'EST'
!            Access:ESTPARTS.Clearkey(epr:Record_Number_Key)
!            epr:Record_Number   = func:PartRecordNumber
!            If Access:ESTPARTS.Tryfetch(epr:Record_Number_Key) = Level:Benign
!                !Found
!                stl:PartNumber  = epr:Part_number
!                stl:Description = epr:Description
!                Access:STOCK.Clearkey(sto:Ref_Number_Key)
!                sto:Ref_Number  = epr:Part_Ref_Number
!                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Found
!                    stl:PartRefNumber   = sto:Ref_Number
!                    stl:ShelfLocation   = sto:Shelf_Location
!                Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                    !Error
!                End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
!                stl:JobNumber   = epr:Ref_Number
!                stl:PartRecordNumber    = epr:Record_Number
!                stl:PartType    = 'EST'
!                stl:Quantity    = func:Quantity
!            Else ! If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
!                !Error
!            End !If Access:ESTPARTS.Tryfetch(epr:RecordNumberKey) = Level:Benign
!    End !func:PartType
!    Return True
   Relate:STOCKALL.Close
   Relate:STOCKALX.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
