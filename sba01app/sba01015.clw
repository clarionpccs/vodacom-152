

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01015.INC'),ONCE        !Local module procedure declarations
                     END


ForceMobileNumber    PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Force Mobile Number
    If (def:Force_Mobile_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Mobile_Number <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Mobile_Number = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceColour          PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Colour!
    If (def:ForceCommonFault = 'B' And func:Type = 'B') Or |
        (def:ForceCommonFault <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Unit_Type = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceDOP             PROCEDURE  (func:TransitType,func:Manufacturer,func:WarrantyJob,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Date Of Purchase
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        error# = 0
        If trt:force_dop = 'YES' And func:WarrantyJob = 'YES'
            Return Level:Fatal
        End !If trt:force_dop = 'YES'
    end
    !Only check this if it hasn't already been forced by the Transit Type
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        IF man:DOPCompulsory And func:WarrantyJob = 'YES'
            Return Level:Fatal
        End !IF man:DOPCompulsory and job:dop = '' and func:Type = 'C'
    Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceLocation        PROCEDURE  (func:TransitType,func:Workshop,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Location
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        If trt:force_location = 'YES' and func:Workshop = 'YES'
            Return Level:Fatal
        End!If trt:force_location = 'YES' and job:location = ''
    end
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceAuthorityNumber PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Authority Number
    If (def:Force_Authority_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Authority_Number <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Authority_Number = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceOrderNumber     PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Order Number
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_ACcounts = 'YES'
                If sub:ForceOrderNumber
                    Return Level:Fatal
                End !If tra:ForceOrderNumber

            Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_ACcounts = 'YES'
                If tra:ForceOrderNumber
                    Return Level:Fatal
                End !If tra:ForceOrderNumber

            End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceCustomerName    PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ReturnByte           BYTE
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !TB13307 - J - 01/07/14 - change needed to allow show name(1) and Force name(2)
    !changed again at 1600 - Defaults setting only forces if the trade account permits

    ReturnByte = 0

    ReturnByte = CustomerNameRequired(func:AccountNumber)  !this can return 0, 1 or 2  hide, force or show

    If returnByte = 2
        !trade account says show, does default now force it
        Set(DEFAULTS)
        Access:DEFAULTS.Next()
        !Customer Name
        If (def:Customer_Name = 'B' and func:Type = 'B') Or |
             (def:Customer_Name <> 'I' and func:Type = 'C')
             ReturnByte = 1
        End!If def:Customer_Name = 'B'
    End !if trade account says show

    Return ReturnByte
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceIncomingCourier PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Incoming Courier
    If (def:Force_Incoming_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Incoming_Courier <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Incoming_Courier = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForcePostcode        PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Postcode
    If (def:ForcePostcode = 'B' and func:Type = 'B') Or |
        (def:ForcePostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForcePostcode = 'B''
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceDeliveryPostcode PROCEDURE  (func:Type)          ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Delivery Postcode
    If (def:ForceDelPostcode = 'B' And func:Type = 'B') Or |
        (def:ForceDelPostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForceDelPostcode = 'B'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
