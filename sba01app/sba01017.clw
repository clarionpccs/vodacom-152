

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01017.INC'),ONCE        !Local module procedure declarations
                     END


ShowFaultDescription PROCEDURE (f_number)             !Generated from procedure template - Window

LocalRequest         LONG
tmp:text             STRING(255)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Invoice Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Fault Description'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel1),FILL(09A6A7CH)
                       TEXT,AT(202,150,276,130),USE(jbn_ali:Fault_Description),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020669'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ShowFaultDescription')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBNOTES_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','ShowFaultDescription')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  access:jobnotes_alias.clearkey(jbn_ali:refnumberkey)
  jbn_ali:refnumber = f_number
  if access:jobnotes_alias.tryfetch(jbn_ali:refnumberkey) = Level:Benign
  End!if access:jobnotes_alias.tryfetch(jbn_ali:refnumberkey) = Level:Benign
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ShowFaultDescription')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020669'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020669'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020669'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ShowEngineersNotes PROCEDURE (f_number)               !Generated from procedure template - Window

LocalRequest         LONG
tmp:text             STRING(255)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Invoice Text'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Engineers Notes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel1),FILL(09A6A7CH)
                       TEXT,AT(204,142,276,130),USE(jbn_ali:Engineers_Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020670'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ShowEngineersNotes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBNOTES_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','ShowEngineersNotes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  access:jobnotes_alias.clearkey(jbn_ali:refnumberkey)
  jbn_ali:refnumber = f_number
  if access:jobnotes_alias.tryfetch(jbn_ali:refnumberkey) = Level:Benign
  End!if access:jobnotes_alias.tryfetch(jbn_ali:refnumberkey) = Level:Benign
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ShowEngineersNotes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020670'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020670'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020670'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ChkPaid              PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            Despatch# = 0
! Deleting (DBH 24/01/2007) # 8678 - Not needed as the jobs are not being prevented from going into the despatch table
!            If tra:Use_Sub_Accounts = 'YES'
!                If sub:Despatch_Paid_Jobs = 'YES'
!                    If sub:Despatch_Invoiced_Jobs = 'YES'
!                        If job:Invoice_Number <> ''
!                            Despatched# = 1
!                        End ! If job:Invoice_Number <> ''
!                    Else ! If sub:Despatch_Invoiced_Jobs = 'YES'
!                        Despatch# = 1
!                    End ! If sub:Despatch_Invoiced_Jobs = 'YES'
!                End ! If sub:Despatch_Paid_Jobs = 'YES'
!            Else ! If tra:Use_Sub_Accounts = 'YES'
!                If tra:Despatch_Paid_Jobs = 'YES'
!                    If tra:Despatch_Invoiced_Jobs = 'YES'
!                        If job:Invoice_Number <> ''
!                            Despatch# = 1
!                        End ! If job:Invoice_Number <> ''
!                    Else ! If tra:Despatch_Invoiced_Jobs = 'YES'
!                        Despatch# = 1
!                    End ! If tra:Despatch_Invoiced_Jobs = 'YES'
!                End ! If tra:Despatch_Paid_Jobs = 'YES'
!            End ! If tra:Use_Sub_Accounts = 'YES'
! End (DBH 24/01/2007) #8678        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    If job:Date_Completed = ''
        job:Paid = 'NO'
        job:Paid_Warranty = 'NO'
    Else ! If job:Date_Completed = ''
        If job:Chargeable_Job = 'YES'
            Total_Price('C',vat",total",balance")
            If total" = 0
                job:Paid = 'YES'
            Else ! If total" = 0
                If balance" <= 0
                    If job:Paid <> 'YES'
                        job:Paid = 'YES'
                    End ! If job:Paid <> 'YES'
                    Access:COURIER.ClearKey(cou:Courier_Key)
                    cou:Courier = job:Courier
                    If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                        !Found
                    Else ! If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                        !Error
                    End ! If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
! Deleting (DBH 24/01/2007) # 8678 - Not needed as job is not stopped from going into despatch table
!                    If Despatch# = 1
!                        If glo:WebJob
!                            Access:JOBSE.ClearKey(jobe:RefNumberKey)
!                            jobe:RefNumber = job:Ref_Number
!                            If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                                !Found
!                                If jobe:DEspatched <> 'YES'
!                                    jobe:Despatched = 'REA'
!                                    jobe:DespatchType = 'JOB'
!
!                                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
!                                    wob:RefNumber = job:Ref_Number
!                                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!                                        !Found
!                                        wob:ReadyToDespatch = 1
!                                        wob:DespatchCourier = job:Courier
!                                        Access:WEBJOB.TryUpdate()
!                                    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!                                        !Error
!                                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!
!                                    If cou:CustomerCollection
!                                        If job:Exchange_Unit_Number = ''
!                                            GetStatus(915,0,'JOB') ! Paid Awaiting Collection
!                                        End ! If job:Exchange_Unit_Number = ''
!                                    Else ! If cou:CustomerCollection
!                                        If job:Exchange_Unit_Number = ''
!                                            GetStatus(916,1,'JOB') ! Paid Awaiting Despatch
!                                        End ! If job:Exchange_Unit_Number = ''
!                                    End ! If cou:CustomerCollection
!                                Else ! If jobe:DEspatched <> 'YES'
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(910,1,'JOB') ! Despatch Unpaid
!                                    End ! If job:Exchange_Unit_Number = ''
!                                End ! If jobe:DEspatched <> 'YES'
!                                Access:JOBSE.TryUpdate()
!                            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                                !Error
!                            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                        Else ! If glo:WebJob
!                            If job:Despatched <> 'YES'
!                                job:Despatched = 'REA'
!                                job:Despatch_Type = 'JOB'
!                                job:Current_Courier = job:Courier
!                                If cou:CustomerCollection
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(915,0,'JOB') ! Paid Awaiting Collection
!                                    End ! If job:Exchange_Unit_Number = ''
!                                Else ! If cou:CustomerCollection
!                                    If job:Exchange_Unit_Number = ''
!                                        GetStatus(916,1,'JOB') ! Paid Awaiting Despatch
!                                    End ! If job:Exchange_Unit_Number = ''
!                                End ! If cou:CustomerCollection
!                            Else ! If job:Despatched <> 'YES'
!                                If job:Exchange_Unit_Number = ''
!                                    GetStatus(910,1,'JOB') ! Despatched Unpaid
!                                End ! If job:Exchange_Unit_Number = ''
!                            End ! If job:Despatched <> 'YES'
!                        End ! If glo:WebJob
!                    End ! If Despatch# = 1
! End (DBH 24/01/2007) #8678
                    If Despatch# = 0
                        If glo:WebJob
                            If job:Location = GETINI('RRC','DespatchToCustomer',,Clip(Path()) & '\SB2KDEF.INI')
                                If job:Exchange_Unit_Number = ''
                                    GetStatus(910,1,'JOB') ! Despatch Paid
                                End ! If job:Exchange_Unit_Number = ''
                            Else ! If job:Location = GETINI('RRC','DespatchToCustomer',,Clip(Path()) & '\SB2KDEF.INI')
                                If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(915,0,'JOB') ! Paid Awaiting Collection
                                    End ! If job:Exchange_Unit_Number = ''
                                Else ! If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(916,1,'JOB') ! Paid Awaiting Despatch
                                    End ! If job:Exchange_Unit_Number = ''
                                End ! If cou:CustomerCollection
                            End ! If job:Location = GETINI('RRC','DespatchToCustomer',,Clip(Path()) & '\SB2KDEF.INI')
                        Else ! If glo:WebJob
                            ! If job has been despatch, then status is despatch paid
                            If job:Date_Despatched <> ''
                                If job:Exchange_Unit_Number = ''
                                    GetStatus(910,1,'JOB') ! Despatch Paid
                                End ! If job:Exchange_Unit_Number = ''
                            Else ! If job:Date_Despatched <> ''
                                If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(915,0,'JOB') ! Paid Awaiting Collection
                                    End ! If job:Exchange_Unit_Number = ''
                                Else ! If cou:CustomerCollection
                                    If job:Exchange_Unit_Number = ''
                                        GetStatus(916,1,'JOB') ! Paid Awaiting Despatch
                                    End ! If job:Exchange_Unit_Number = ''
                                End ! If cou:CustomerCollection
                            End ! If job:Date_Despatched <> ''

                            !commenting this bit as #8853 was cancelled six years ago and this is causing a problem on jobs!
                            !JC 10/04/13 - TB 13018
                            !! Inserting (DBH 15/03/2007) # 8853 - Reset the status back to "Send To RRC"
                            !If job:Despatched = 'REA' and job:Despatch_Type = 'JOB'
                            !    GetStatus(Sub(GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),0,'JOB') ! Sent To RRC
                            !End ! If job:Despatched = 'REA' and job:Despatch_Type = 'JOB'
                            ! End (DBH 15/03/2007) #8853
                            !End of commenting this bit for 13018 :-)

                        End ! If glo:WebJob
                    End ! If Despatch# = 0
                End ! If balance" <= 0
                If balance" > 0
                    If job:Paid = 'YES'
                        job:Paid = 'NO'
                    End ! If job:Paid = 'YES'
                End ! If balance" > 0
            End ! If total" = 0

            ! (l389/G154) If status is '910 Despatch Paid' and job is not paid, then revert status (DBH: 22/01/2007)
            If Sub(job:Current_Status,1,3) = '910' And job:Paid = 'NO'
                If job:Exchange_Unit_Number = ''
                    GetStatus(905,1,'JOB') ! Despatch unpaid
                End ! If job:Exchange_Unit_Number = ''
            End ! If Sub(job:Current_Status,1,3) = '910' And job:Paid = 'NO'
        End ! If job:Chargeable_Job = 'YES'
        If job:Warranty_Job = 'YES'
            If job:Invoice_Number_Warranty <> ''
                job:Paid_Warranty = 'YES'
            Else ! If job:Invoice_Number_Warranty <> ''
                job:Paid_Warranty = 'NO'
            End ! If job:Invoice_Number_Warranty <> ''
        End ! If job:Warranty_Job = 'YES'
        Access:JOBS.Update()
    End ! If job:Date_Completed = ''

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StockUsage           PROCEDURE  (LONG f:RefNumber,*LONG f:7,*LONG f:30,*LONG f:60,*LONG f:90,*LONG f:Average) ! Declare Procedure
save_stohist_id      USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    VodacomClass.StockUsage(f:RefNumber,f:7,f:30,f:60,f:90,f:Average)
!    f:7 = 0
!    f:30 = 0
!    f:60 = 0
!    f:90 = 0
!    f:Average = 0
!    Save_STOHIST_ID = Access:STOHIST.SaveFile()
!    Access:STOHIST.Clearkey(shi:Ref_Number_Key)
!    shi:Ref_Number = f:RefNumber
!    shi:Date = (Today() - 90)
!    Set(shi:Ref_Number_Key,shi:Ref_Number_Key)
!    Loop ! Begin Loop
!        If Access:STOHIST.Next()
!            Break
!        End ! If Access:STOHIST.Next()
!        If shi:Ref_Number <> f:RefNumber
!            Break
!        End ! If shi:Ref_Number <> f:RefNumber
!        If shi:Date > Today()
!            Break
!        End ! If shi:Date > Today()
!        If shi:notes = 'STOCK READJUSTED FROM SERVICEBASE DOS'
!            Cycle
!        End!If shi:action = 'STOCK READJUSTED FROM SERVICEBASE DOS'
!        If shi:notes = 'STOCK DECREMENTED' and shi:job_number = ''
!            Cycle
!        End!If shi:notes = 'STOCK DECREMENTED'
!
!        If shi:date <= Today() And shi:date >= Today() - 7
!            If shi:transaction_type = 'DEC'
!                f:7 += shi:quantity
!            End!If shi:transaction_type = 'DEC'
!            If shi:transaction_type = 'REC'
!                f:7 -= shi:quantity
!            End!If shi:transaction_type = 'REC'
!        End!If shi:date <= Today() And shi:date >= Today() - 7
!
!        If shi:date <= Today() And shi:date >= Today() - 30
!            If shi:transaction_type = 'DEC'
!                f:30 += shi:quantity
!            End!If shi:transaction_type = 'DEC'
!            If shi:transaction_type = 'REC'
!                f:30 -= shi:quantity
!            End!If shi:transaction_type = 'REC'
!        End!If shi:date <= Today() And shi:date >= Today() - 7
!
!        If shi:date <= Today() - 31 And shi:date >= Today() - 60
!            If shi:transaction_type = 'DEC'
!                f:60 += shi:quantity
!            End!If shi:transaction_type = 'DEC'
!            If shi:transaction_type = 'REC'
!                f:60 -= shi:quantity
!            End!If shi:transaction_type = 'REC'
!        End!If shi:date <= Today() And shi:date >= Today() - 7
!
!        If shi:date <= Today() - 61 And shi:date >= Today() - 90
!            If shi:transaction_type = 'DEC'
!                f:90 += shi:quantity
!            End!If shi:transaction_type = 'DEC'
!            If shi:transaction_type = 'REC'
!                f:90 -= shi:quantity
!            End!If shi:transaction_type = 'REC'
!        End!If shi:date <= Today() And shi:date >= Today() - 7
!    End ! Loop
!    Access:STOHIST.RestoreFile(Save_STOHIST_ID)
!
!    f:Average = INT(Round((f:7 + f:30 + f:60 + f:90) / 90,.01))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobPaid              PROCEDURE  (f:RefNumber)         ! Declare Procedure
save_job_ali_id      USHORT,AUTO
save_jobe_id         USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_inv_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
save_vat_id          USHORT,AUTO
tmp:Vat              REAL
tmp:Total            REAL
tmp:AmountPaid       REAL
tmp:Paid             BYTE(0)
tmp:LabourVatRate    REAL
tmp:PartsVATRate     REAL
tmp:Balance          REAL
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBS_ALIAS.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:JOBPAYMT.Open
   Relate:JOBSE.Open
   Relate:INVOICE.Open
   Relate:VATCODE.Open
    save_job_ali_id = Access:JOBS_ALIAS.SaveFile()
    save_sub_id = Access:SUBTRACC.SaveFile()
    save_tra_id = Access:TRADEACC.SaveFile()
    save_inv_id = Access:INVOICE.SaveFile()
    save_vat_id = Access:VATCODE.SaveFile()
    save_jobe_id = Access:JOBSE.SaveFile()
    save_jpt_id = Access:JOBPAYMT.SaveFile()


    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = f:RefNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
    Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
    End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign

    tmp:Paid = 0
    tmp:AmountPaid = 0
    Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
    jpt:Ref_Number = job_ali:Ref_Number
    Set(jpt:All_Date_Key,jpt:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT.Next()
            Break
        End ! If Access:JOBPAYMT.Next()
        If jpt:Ref_Number <> job_ali:Ref_Number
            Break
        End ! If jpt:RefNumber <> f:RefNumber
        tmp:AmountPaid += jpt:Amount
    End ! Loop

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = job_ali:Account_Number
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = sub:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            Else ! If tra:Invoice_Sub_Accounts = 'YES'
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Labour_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:LabourVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
                vat:Vat_Code = tra:Parts_Vat_Code
                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Found
                    tmp:PartsVatRate = vat:Vat_Rate
                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
                    !Error
                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

            End ! If tra:Invoice_Sub_Accounts = 'YES'
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job_ali:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    If job_ali:Invoice_Number > 0
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    End ! If job_ali:Invoice_Number > 0

    If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:InvRRCCLabourCost *(inv:Vat_Rate_Labour / 100) + |
                        jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts / 100) + |
                        job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)

            tmp:Total = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job_ali:Invoice_Courier_Cost + tmp:VAT

        Else ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            tmp:VAT = jobe:RRCCLabourCost *(tmp:LabourVatRate / 100) + |
                        jobe:RRCCPartsCost * (tmp:PartsVatRate / 100) + |
                        job_ali:Courier_Cost * (tmp:LabourVatRate / 100)

            tmp:Total = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job_ali:Courier_Cost + tmp:VAT

        End ! If job_ali:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    Else ! If glo:WebJob
        If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100) + |
                        job_ali:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) + |
                        job_ali:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100)

            tmp:Total = job_ali:Invoice_Labour_Cost + job_ali:Invoice_Parts_Cost + job_ali:Invoice_Courier_Cost + tmp:VAT

        Else ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            tmp:VAT = job_ali:Labour_Cost * (tmp:LabourVatRate / 100) + |
                        job_ali:Parts_Cost * (tmp:PartsVatRate / 100) + |
                        job_ali:Courier_Cost * (tmp:LabourVatRate / 100)

            tmp:Total = job_ali:Labour_Cost + job_ali:Parts_Cost + job_ali:Courier_Cost + tmp:VAT

        End ! If job_ali:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
        tmp:Balance = tmp:Total - tmp:AmountPaid
    End ! If glo:WebJob

    If tmp:Balance < 0.01
        tmp:Paid = 1
    End ! If tmp:Balance < 0

    Access:JOBS_ALIAS.RestoreFile(save_job_ali_id)
    Access:SUBTRACC.RestoreFile(save_sub_id)
    Access:TRADEACC.RestoreFile(save_tra_id)
    Access:INVOICE.RestoreFile(save_inv_id)
    Access:VATCODE.RestoreFile(save_vat_id)
    Access:JOBSE.RestoreFile(save_jobe_id)
    Access:JOBPAYMT.RestoreFile(save_jpt_id)


   Relate:JOBS_ALIAS.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:JOBPAYMT.Close
   Relate:JOBSE.Close
   Relate:INVOICE.Close
   Relate:VATCODE.Close
    Return tmp:Paid
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
JobInvoiced          PROCEDURE  (f:RefNumber)         ! Declare Procedure
tmp:Return           BYTE(0)
save_job_ali_id      USHORT,AUTO
save_inv_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:INVOICE.Open
   Relate:JOBS_ALIAS.Open
    save_job_ali_id = Access:JOBS_ALIAS.SaveFile()
    save_inv_id  = Access:INVOICE.SaveFile()

    tmp:Return = 0
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = f:RefNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job_ali:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If glo:WebJob
                If inv:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
                    tmp:Return = 1
                End ! If inv:Invoice_Number > 0 And inv:RRCInvoiceDate > 0
            Else ! If glo:WebJob
                If inv:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
                    tmp:Return = 1
                End ! If inv:Invoice_Number > 0 And inv:ARCInvoiceDate > 0
            End ! If glo:WebJob
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
    Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number) = Level:Benign
        !Error
    End ! If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number) = Level:Benign

    Access:INVOICE.RestoreFile(save_inv_id)
    Access:JOBS_ALIAS.RestoreFile(save_job_ali_id)
   Relate:INVOICE.Close
   Relate:JOBS_ALIAS.Close
    Return tmp:Return
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SetDespatchStatus    PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    If glo:WebJob
        tra:Account_Number = wob:HeadAccountNumber
    Else ! If glo:WebJob
        tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
    End ! If glo:WebJob
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
        If tra:SetDespatchJobStatus! And ~JobPaid(job:Ref_Number)
            GetStatus(Sub(tra:DespatchedJobStatus,1,3),0,'JOB') ! Set Completed Status
        End ! If sub:SetInvoicedJobStatus
        If tra:SetInvoicedJobStatus! And ~JobInvoiced(job:Ref_Number)
            GetStatus(Sub(tra:InvoicedJobStatus,1,3),0,'JOB') ! Set Completed Status
        End ! If tra:SetInvoicedJobStatus
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CanJobBeDespatched   PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If job:Chargeable_Job = 'YES'
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        If glo:WebJob
            tra:Account_Number = wob:HeadAccountNumber
        Else ! If glo:WebJob
            tra:Account_Number = GETINI('BOOKING','HeadAccount',,Clip(Path()) & '\SB2KDEF.INI')
        End ! If glo:WebJob
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Despatch_Paid_Jobs = 'YES' And ~JobPaid(job:Ref_Number)
                Case Missive('Unable to despatch. The selected job has not been paid.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return 0
            End ! If sub:Despatch_Paid_Jobs = 'YES' And ~JobPaid(job:Ref_Number)
            If tra:Despatch_Invoiced_Jobs = 'YES' And ~JobInvoiced(job:Ref_Number)
                Case Missive('Unable to despatch. The selected job has not been invoiced.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Return 0
            End ! If sub:Despatch_Invoiced_Jobs = 'YES' And ~JobInvoiced(job:Ref_Number)
        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    End ! If job:Chargeable_Job = 'YES'
    Return 1
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddToPartNumberHistory PROCEDURE  (f:RefNumber,f:Location,f:OldPartNumber,f:OldDescription,f:NewPartNumber,f:NewDescription,f:ChangedFrom) ! Declare Procedure
save_stoparts_id     USHORT,AUTO
save_users_id        USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:STOPARTS.Open
   Relate:USERS.Open
    Return# = 0
    Save_STOPARTS_ID = Access:STOPARTS.SaveFile()
    Save_USERS_ID = Access:USERS.SaveFile()

    If Access:STOPARTS.PrimeRecord() = Level:Benign
        spt:STOCKRefNumber      = f:RefNumber
        spt:Location            = f:Location
        spt:DateChanged         = Today()
        spt:TimeChanged         = Clock()
        spt:OldPartNumber       = f:OldPartNumber
        spt:OldDescription      = f:OldDescription
        spt:NewPartNumber       = f:NewPartNumber
        spt:NewDescription      = f:NewDescription
        spt:Notes               = f:ChangedFrom
        Access:USERS.ClearKey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            !Found
        Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            !Error
        End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
        spt:UserCode = use:User_Code

        If Access:STOPARTS.TryInsert() = Level:Benign
            !Insert
            Return# = 1
        Else ! If Access:STOPARTS.TryInsert() = Level:Benign
            Access:STOPARTS.CancelAutoInc()
        End ! If Access:STOPARTS.TryInsert() = Level:Benign
    End ! If Access.STOPARTS.PrimeRecord() = Level:Benign

    Access:USERS.RestoreFile(Save_USERS_ID)
    Access:STOPARTS.RestoreFile(Save_STOPARTS_ID)

    Return Return#
   Relate:STOPARTS.Close
   Relate:USERS.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ChangeAllPartNumbers PROCEDURE  (f:OldPartNumber,f:NewPartNumber,f:NewDescription,f:SkipRefNumber,f:ChangedFrom) ! Declare Procedure
tmp:OriginalDescription STRING(30)
save_ordpend_id      USHORT,AUTO
save_stock_id        USHORT,AUTO
save_location_id     USHORT,AUTO
save_parts_id        USHORT,AUTO
save_warparts_id     USHORT,AUTO
save_jobs_id         USHORT,AUTO
save_estparts_id     USHORT,AUTO
save_stomodel_id     USHORT,AUTO
save_retstock_id     USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_ordwebpr_id     USHORT,AUTO
save_ordhead_id      USHORT,AUTO
save_orditems_id     USHORT,AUTO
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico'),HIDE
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:STOCK.Open
   Relate:LOCATION.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBS.Open
   Relate:ESTPARTS.Open
   Relate:STOMODEL.Open
   Relate:TRADEACC.Open
   Relate:ORDWEBPR.Open
   Relate:RETSTOCK.Open
   Relate:ORDITEMS.Open
   Relate:ORDHEAD.Open
   Relate:ORDPEND.Open
   Relate:STOCKALL.Open
    Save_LOCATION_ID = Access:LOCATION.SaveFile()
    Save_STOCK_ID = Access:STOCK.SaveFile()
    Save_PARTS_ID = Access:PARTS.SaveFile()
    Save_WARPARTS_ID = Access:WARPARTS.SaveFile()
    Save_JOBS_ID = Access:JOBS.SaveFile()
    Save_ESTPARTS_ID = Access:ESTPARTS.SaveFile()
    Save_STOMODEL_ID = Access:STOMODEL.SaveFile()
    Save_RETSTOCK_ID = Access:RETSTOCK.SaveFile()
    Save_TRADEACC_ID = Access:TRADEACC.SaveFile()
    Save_ORDWEBPR_ID = Access:ORDWEBPR.SaveFile()
    Save_ORDITEMS_ID = Access:ORDITEMS.SaveFile()
    Save_ORDHEAD_ID = Access:ORDHEAD.SaveFile()
    Save_ORDPEND_ID = Access:ORDPEND.SaveFile()

    Do Prog:ProgressSetup

    Access:ORDWEBPR.Clearkey(orw:PartNumberKey)
    orw:AccountNumber = ''
    Set(orw:PartNumberKey,orw:PartNumberKey)
    Loop ! Begin Loop
        If Access:ORDWEBPR.Next()
            Break
        End ! If Access:ORDWEBPR.Next()
        If orw:PartNumber <> f:OldPartNumber
            Cycle
        End ! If orw:PartNumber <> f:OldPartNumber
        orw:PartNumber = f:NewPartNumber
        orw:Description = f:NewDescription
        Access:ORDWEBPR.TryUpdate()
    End ! Loop

    ! Inserting (DBH 23/01/2008) # 9706 - Update the part details in Stock Allocation
    Access:STOCKALL.Clearkey(stl:RecordNumberKey)
    stl:RecordNumber = 0
    Set(stl:RecordNumberKey,stl:RecordNumberKey)
    Loop
        If Access:STOCKALL.Next()
            Break
        End ! If Access:STOCKALL.Next()
        If stl:PartNumber <> f:OldPartNumber
            Cycle
        End ! If stl:PartNumber <> f:OldPartNumber
        stl:PartNumber = f:NewPartNumber
        stl:Description = f:NewDescription
        Access:STOCKALL.TryUpdate()
    End ! Loop
    ! End (DBH 23/01/2008) #9706

    Prog:TotalRecords = Records(LOCATION)
    Prog:ShowPercentage = 1 !Show Percentage Figure
    Access:LOCATION.Clearkey(loc:Location_Key)
    loc:Location = ''
    Set(loc:Location_Key,loc:Location_Key)
    Accept
        Case Event()
        Of Event:Timer
            Loop 25 Times
                !Inside Loop
                If Access:LOCATION.Next()
                    Prog:Exit = 1
                    Break
                End ! If Access:LOCATION.Next()

                Access:STOCK.ClearKey(sto:Location_Key)
                sto:Location = loc:Location
                sto:Part_Number = f:OldPartNumber
                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Found
                    ! May already be in this record, so skip it (DBH: 10/08/2007)
                    If sto:Ref_Number <> f:SkipRefNumber

                        !Record in Use?
                        Pointer# = Pointer(Stock)
                        Hold(Stock,1)
                        Get(Stock,Pointer#)
                        If Errorcode() = 43
                            Case Missive('Could not update part. May be be in use:'&|
                              '|Part No: '& Clip(sto:Part_Number) & |
                              '|Location: ' & Clip(sto:Location),'ServiceBase 3g',|
                                           'midea.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            Cycle
                        End !If Errorcode() = 43

                        ! In case the description doesn't match the default (DBH: 10/08/2007)
                        tmp:OriginalDescription = sto:Description

                        sto:Part_Number = f:NewPartNumber
                        sto:Description = f:NewDescription
                        If Access:STOCK.TryUpdate() = Level:Benign
                            ! Part updated correctly, add all the history stuff (DBH: 10/08/2007)
                            If AddToPartNumberHistory(sto:Ref_Number,sto:Location,f:OldPartNumber,tmp:OriginalDescription,f:NewPartNumber,f:NewDescription,f:ChangedFrom)

                            End ! If AddToPartNumberHistory(sto:Ref_Number,sto:Location,f:OldPartNumber,tmp:OldDescription,f:NewPartNumber,f:NewDescription,f:ChangedFrom)
                            If AddToStockHistory(sto:Ref_Number,'ADD','',0,0,0,sto:Purchase_Cost,sto:Sale_Cost,sto:Retail_Cost,|
                                      'PART DETAILS UPDATED',|
                                      'ORIGINAL PART NO: ' & Clip(f:OldPartNumber) & '<13,10>ORIGINAL DESC:' & Clip(tmp:OriginalDescription) & |
                                      '<13,10>NEW PART NO: ' & Clip(f:NewPartNumber) & '<13,10>NEW DESC: ' & Clip(f:NewDescription))

                            End ! '<13,10>NEW PART: ' & Clip(local:NewPartNumber) & ' - ' & Clip(local:NewDescription))
                        Else ! If Access:STOCK.TryUpdate() = Level:Benign
                            Cycle
                        End ! If Access:STOCK.TryUpdate() = Level:Benign

                    End ! If sto:Ref_Number = f:SkipRefNumbe

                    Access:STOMODEL.Clearkey(stm:Ref_Part_Description)
                    stm:Ref_Number = sto:Ref_Number
                    stm:Part_Number = f:OldPartNumber
                    Set(stm:Ref_Part_Description,stm:Ref_Part_Description)
                    Loop ! Begin Loop
                        If Access:STOMODEL.Next()
                            Break
                        End ! If Access:STOMODEL.Next()
                        If stm:Ref_Number <> sto:Ref_Number
                            Break
                        End ! If stm:Ref_Number <> sto:Ref_Number
                        If stm:Part_Number <> f:OldPartNumber
                            Break
                        End ! If stm:Part_Number <> f:OldPartNumber
                        stm:Part_Number = f:NewPartNumber
                        stm:Description = f:NewDescription
                        Access:STOMODEL.TryUpdate()
                    End ! Loop


                    Access:PARTS.Clearkey(par:PartRefNoKey)
                    par:Part_Ref_Number = sto:Ref_Number
                    Set(par:PartRefNoKey,par:PartRefNoKey)
                    Loop ! Begin Loop
                        If Access:PARTS.Next()
                            Break
                        End ! If Access:PARTS.Next()
                        If par:Part_Ref_Number <> sto:Ref_Number
                            Break
                        End ! If par:Part_Ref_Number <> sto:Ref_Number
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = par:Ref_Number
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                            If job:Date_Completed <> ''
                                Cycle
                            End ! If job:Date_Completed <> ''
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        par:Part_Number = f:NewPartNumber
                        par:Description = f:NewDescription
                        Access:PARTS.TryUpdate()
                    End ! Loop

                    Access:WARPARTS.Clearkey(wpr:PartRefNoKey)
                    wpr:Part_Ref_Number = sto:Ref_Number
                    Set(wpr:PartRefNoKey,wpr:PartRefNoKey)
                    Loop ! Begin Loop
                        If Access:WARPARTS.Next()
                            Break
                        End ! If Access:PARTS.Next()
                        If wpr:Part_Ref_Number <> sto:Ref_Number
                            Break
                        End ! If par:Part_Ref_Number <> sto:Ref_Number
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = wpr:Ref_Number
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                            If job:Date_Completed <> ''
                                Cycle
                            End ! If job:Date_Completed <> ''
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        wpr:Part_Number = f:NewPartNumber
                        wpr:Description = f:NewDescription
                        Access:WARPARTS.TryUpdate()
                    End ! Loop


                    Access:ESTPARTS.Clearkey(epr:Part_Ref_Number2_Key)
                    epr:Part_Ref_Number = sto:Ref_Number
                    Set(epr:Part_Ref_Number2_Key,epr:Part_Ref_Number2_Key)
                    Loop ! Begin Loop
                        If Access:ESTPARTS.Next()
                            Break
                        End ! If Access:ESTPARTS.Next()
                        If epr:Part_Ref_Number <> sto:Ref_Number
                            Break
                        End ! If epr:Part_Ref_Number <> sto:Ref_Number
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = epr:Ref_Number
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                            If job:Date_Completed <> ''
                                Cycle
                            End ! If job:Date_Completed <> ''
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        epr:Part_Number = f:NewPartNumber
                        epr:Description = f:NewDescription
                        Access:ESTPARTS.TryUpdate()
                    End ! Loop

                    Access:RETSTOCK.Clearkey(res:DespatchPartKey)
                    res:Despatched = 'PEN'
                    res:Part_Number = f:OldPartNumber
                    Set(res:DespatchPartKey,res:DespatchPartKey)
                    Loop ! Begin Loop
                        If Access:RETSTOCK.Next()
                            Break
                        End ! If Access:RETSTOCK.Next()
                        If res:Despatched <> 'PEN'
                            Break
                        End ! If res:Despatched <> 'PEN'
                        If res:Part_Number <> f:OldPartNumber
                            Break
                        End ! If res:Part_Number <> f:OldPartNumber
                        If res:Part_Ref_Number <> sto:Ref_Number
                            Cycle
                        End ! If res:Part_Ref_Number <> sto:Ref_Number
                        res:Part_Number = f:NewPartNumber
                        res:Description = f:NewDescription
                        Access:RETSTOCK.TryUpdate()
                    End ! Loop

                    ! Inserting (DBH 22/01/2008) # 9706 - Update "Invoice" retail parts
                    Access:RETSTOCK.Clearkey(res:DespatchPartKey)
                    res:Despatched = 'YES'
                    res:Part_Number = f:OldPartNumber
                    Set(res:DespatchPartKey,res:DespatchPartKey)
                    Loop ! Begin Loop
                        If Access:RETSTOCK.Next()
                            Break
                        End ! If Access:RETSTOCK.Next()
                        If res:Despatched <> 'YES'
                            Break
                        End ! If res:Despatched <> 'PEN'
                        If res:Part_Number <> f:OldPartNumber
                            Break
                        End ! If res:Part_Number <> f:OldPartNumber
                        If res:Part_Ref_Number <> sto:Ref_Number
                            Cycle
                        End ! If res:Part_Ref_Number <> sto:Ref_Number
                        res:Part_Number = f:NewPartNumber
                        res:Description = f:NewDescription
                        Access:RETSTOCK.TryUpdate()
                    End ! Loop
                    ! End (DBH 22/01/2008) #9706

                    Access:ORDHEAD.Clearkey(orh:ProcessSaleNoKey)
                    orh:Procesed = 0
                    Set(orh:ProcessSaleNoKey,orh:ProcessSaleNoKey)
                    Loop ! Begin Loop
                        If Access:ORDHEAD.Next()
                            Break
                        End ! If Access:ORDHEAD.Next()
                        If orh:Procesed <> 0
                            Break
                        End ! If orh:Procesed <> 0
                        Access:ORDITEMS.Clearkey(ori:OrdHNoPartKey)
                        ori:OrdHNO = orh:Order_No
                        ori:PartNo = f:OldPartNumber
                        Set(ori:OrdHNoPartKey,ori:OrdHNoPartKey)
                        Loop ! Begin Loop
                            If Access:ORDITEMS.Next()
                                Break
                            End ! If Access:ORDITEMS.Next()
                            If ori:OrdHNo <> orh:Order_No
                                Break
                            End ! If ori:OrdHNo <> orh:Order_No
                            If ori:PartNo <> f:OldPartNumber
                                Break
                            End ! If ori:PartNo <> f:OldPartNumber
                            ori:PartNo = f:NewPartNumber
                            ori:PartDiscription = f:NewDescription
                            Access:ORDITEMS.TryUpdate()
                        End ! Loop
                    End ! Loop

                    Access:ORDPEND.Clearkey(ope:Part_Ref_Number_Key)
                    ope:Part_Ref_Number = sto:Ref_Number
                    Set(ope:Part_Ref_Number_Key,ope:Part_Ref_Number_Key)
                    Loop ! Begin Loop
                        If Access:ORDPEND.Next()
                            Break
                        End ! If Access:ORDPEND.Next()
                        If ope:Part_Ref_Number <> sto:Ref_Number
                            Break
                        End ! If ope:Part_Ref_Number <> sto:Ref_Number
                        ope:Part_Number = f:NewPartNumber
                        ope:Description = f:NewDescription
                        Access:ORDPEND.TryUpdate()
                    End ! Loop

                Else ! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Error
                End ! If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign

                Do Prog:UpdateScreen

                Prog:RecordCount += 1
                ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
            End ! Loop 25 Times
        Of Event:CloseWindow
        Of Event:Accepted
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept

    Do Prog:ProgressFinished

    Access:PARTS.RestoreFile(Save_PARTS_ID)
    Access:WARPARTS.RestoreFile(Save_WARPARTS_ID)
    Access:JOBS.RestoreFile(Save_JOBS_ID)
    Access:LOCATION.RestoreFile(Save_LOCATION_ID)
    Access:STOCK.RestoreFile(Save_STOCK_ID)
    Access:ESTPARTS.RestoreFile(Save_ESTPARTS_ID)
    Access:STOMODEL.RestoreFile(Save_STOMODEL_ID)
    Access:ORDHEAD.RestoreFile(Save_ORDHEAD_ID)
    Access:ORDITEMS.RestoreFile(Save_ORDITEMS_ID)
    Access:ORDWEBPR.RestoreFile(Save_ORDWEBPR_ID)
    Access:TRADEACC.RestoreFile(Save_TRADEACC_ID)
    Access:RETSTOCK.RestoreFile(Save_RETSTOCK_ID)
    Access:ORDPEND.RestoreFile(Save_ORDPEND_ID)
   Relate:STOCK.Close
   Relate:LOCATION.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBS.Close
   Relate:ESTPARTS.Close
   Relate:STOMODEL.Close
   Relate:TRADEACC.Close
   Relate:ORDWEBPR.Close
   Relate:RETSTOCK.Close
   Relate:ORDITEMS.Close
   Relate:ORDHEAD.Close
   Relate:ORDPEND.Close
   Relate:STOCKALL.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    If glo:WebJob
        Clarionet:OpenPushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Open(Prog:ProgressWindow)
        ?Prog:Cancel{prop:Hide} = 0
    End ! If glo:WebJob
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    If glo:WebJob
        Clarionet:UpdatePushWindow(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    If glo:WebJob
        Clarionet:ClosePushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Close(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
