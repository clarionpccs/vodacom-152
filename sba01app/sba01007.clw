

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('SBA01007.INC'),ONCE        !Local module procedure declarations
                     END


UseAlternativeContactNos PROCEDURE  (func:AccountNumber,func:UseHead) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If func:UseHead
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = func:AccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:UseDespatchDetails
                Return 1 !Use Trade Details
            End !If tra:UseDespatchDetails
        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else !If func:UseHead
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = func:AccountNumber
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Use_Sub_Accounts = 'YES'
                    If sub:UseDespatchDetails
                        Return 2 !Use Sub Details
                    End !If sub:UseDespatchDetails
                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:UseDespatchDetails
                        Return 1 !Use Trade Details
                    End !If tra:UseDespatchDetails
                End !If tra:Use_Sub_Accounts = 'YES'
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    End !If func:UseHead

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
BERRepairType        PROCEDURE  (func:Manufacturer,func:RepairType) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Is the selected Repair Type a BER?
    Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
    rtd:Manufacturer = func:Manufacturer
    rtd:Repair_Type  = func:RepairType
    If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
        !Found
        If rtd:BER = 1
            Return Level:Benign
        End !If rtd:BER = 1
    Else!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign

    Return Level:Fatal
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SendEmail PROCEDURE (string pEmailFrom, string pEmailTo, string pEmailSubject, string pEmailCC, string pEmailBcc, string pEmailFileList, string pEmailMessageText) !Generated from procedure template - Window

FilesOpened          BYTE
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(252)
EmailTo              STRING(1024)
EmailSubject         STRING(252)
EmailCC              STRING(1024)
EmailBCC             STRING(1024)
EmailFileList        STRING(1024)
EmailMessageText     STRING(16384)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Email_Spacer         USHORT
window               WINDOW('Sending Email'),AT(,,135,29),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),RESIZE
                       BUTTON('&Send'),AT(9,8,51,16),USE(?EmailSend),LEFT,TIP('Send Email Now')
                       BUTTON('Close'),AT(69,8,51,16),USE(?Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisSendEmail        CLASS(NetEmailSend)              !Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('SendEmail')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EmailSend
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
      ! Save Window Name
   AddToLog('Window','Open','SendEmail')
  Window{prop:Hide} = 1
  post(event:accepted,?EmailSend)
  Bryan.CompFieldColour()
                                               ! Generated by NetTalk Extension (Start)
  ThisSendEmail.init()
  if ThisSendEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ! Generated by NetTalk Extension
  ThisSendEmail.OptionsMimeTextTransferEncoding = '7bit'           ! '7bit', '8bit' or 'quoted-printable'
  ThisSendEmail.OptionsMimeHtmlTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisSendEmail.Kill()                      ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SendEmail')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?EmailSend
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EmailSend, Accepted)
      EmailTo          = pEmailTo
      EmailFrom        = pEmailFrom
      EmailCC          = pEmailCC
      EmailBCC         = pEmailBCC
      EmailSubject     = pEmailSubject
      EmailFileList    = pEmailFileList
      EmailMessageText = pEmailMessageText
      ! Generated by NetTalk Extension
      ThisSendEmail.Server = def:EmailServerAddress
      ThisSendEmail.Port = def:EmailServerPort
      ThisSendEmail.From = EmailFrom
      ThisSendEmail.ToList = EmailTo
      ThisSendEmail.ccList = EmailCC
      ThisSendEmail.bccList = EmailBCC
      
      ThisSendEmail.Subject = EmailSubject
      ThisSendEmail.AttachmentList = EmailFileList
      ThisSendEmail.SetRequiredMessageSize (0, len(clip(EmailMessageText)), 0) ! You must call this function before populating self.MessageText  #ELSIF ( <> '')
      if ThisSendEmail.Error = 0
        ThisSendEmail.MessageText = EmailMessageText
        display()
        ThisSendEmail.SendMail(NET:EMailMadeFromPartsMode)
        display()
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EmailSend, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisSendEmail.TakeEvent()                 ! Generated by NetTalk Extension
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      ! Generated by NetTalk Extension
      if records (ThisSendEmail.DataQueue) > 0
        if Message ('The email is still being sent.|Are you sure you want to quit?','Email Sending',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No) = Button:No
          cycle
        end
      end
      ! Generated by NetTalk Extension
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

ThisSendEmail.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
  ! Before Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, ErrorTrap, (string errorStr,string functionName))
  PARENT.ErrorTrap(errorStr,functionName)
  post(event:closewindow)
  ! After Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, ErrorTrap, (string errorStr,string functionName))


ThisSendEmail.MessageSent PROCEDURE


  CODE
  ! Before Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, MessageSent, ())
  PARENT.MessageSent
  post(event:closewindow)
  ! After Embed Point: %NetTalkMethodCodeSection) DESC(NetTalk Method Executable Code Section) ARG(1, MessageSent, ())

ProofOfPurchaseWindow PROCEDURE (func:Manufacturer)   !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:POPType          STRING(30)
tmp:Result           STRING(30)
tmp:POPDate          DATE
tmp:Month            STRING(30)
tmp:Year             STRING(30)
tmp:Return           STRING(3)
FDB4::View:FileDrop  VIEW(POPTYPES)
                       PROJECT(pop:POPType)
                       PROJECT(pop:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?tmp:POPType
pop:POPType            LIKE(pop:POPType)              !List box control field - type derived from field
pop:RecordNumber       LIKE(pop:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Proof Of Purchase'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This unit is outside the Manufacturer''s Warranty Period. P.O.P. is required date' &|
   'd after:'),AT(210,158,260,20),USE(?ProofText),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@d6),AT(310,178),USE(tmp:POPDate),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(316,214,124,10),USE(tmp:POPType),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),FORMAT('240L(2)|M@s60@'),DROP(10,124),FROM(Queue:FileDrop)
                           STRING(@s30),AT(278,244,124,12),USE(tmp:Result),CENTER,FONT(,10,080FFFFH,FONT:bold)
                           PROMPT('P.O.P. Type'),AT(240,214),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,334),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Proof Of Purchase'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB4                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020012'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ProofOfPurchaseWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ProofText
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:MANUFACT.Open
  Relate:POPTYPES.Open
  SELF.FilesOpened = True
  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
  man:Manufacturer    = func:Manufacturer
  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Found
      Case man:Manufacturer
          Of 'MOTOROLA'
              tmp:Month   = Month(Today())
              tmp:Year    = Year(Today())
              Loop x# = 1 To man:POPPeriod
                  tmp:Month -= 1
                  If tmp:Month < 1
                      tmp:Month = 12
                      tmp:Year -= 1
                  End !If tmp:Month > 12
              End !Loop x# = 1 To man:ClaimDate
              tmp:POPDate = Deformat(Day(Today()) & '/' & Clip(tmp:Month) & '/' & Clip(tmp:Year),@d6)
      End !Case man:Manufacturer
  Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','ProofOfPurchaseWindow')
  ?tmp:POPType{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:POPType{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDB4.Init(?tmp:POPType,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:POPTYPES,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(pop:POPTypeKey)
  FDB4.AddField(pop:POPType,FDB4.Q.pop:POPType)
  FDB4.AddField(pop:RecordNumber,FDB4.Q.pop:RecordNumber)
  FDB4.AddUpdateField(pop:POPType,tmp:POPType)
  FDB4.AddUpdateField(pop:Result,tmp:Result)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
    Relate:POPTYPES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ProofOfPurchaseWindow')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      Case tmp:Result
          Of 'WARRANTY ACCEPTED'
              tmp:Return  = ''
          Of 'WARRANTY REJECTED'
              tmp:Return  = 'REJ'
          Of 'WARRANTY PENDING'
              tmp:Return  = 'PEN'
          Else
              Select(?tmp:POPType)
      End !tmp:Result
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020012'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020012'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020012'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
DateCodeValidation   PROCEDURE  (func:Manufacturer,func:DateCode,func:DateBooked) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:YearCode         STRING(30)
tmp:MonthCode        STRING(30)
tmp:Year             STRING(30)
tmp:Month            STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!    Case func:Manufacturer
!        Of 'MOTOROLA'
!            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!            man:Manufacturer    = 'MOTOROLA'
!            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                !Found
!
!            Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!
!            tmp:YearCode    = Sub(func:DateCode,5,1)
!            tmp:MonthCode   = Sub(func:DateCode,6,1)
!
!            Access:MANUDATE.Clearkey(mad:DateCodeKey)
!            mad:Manufacturer    = 'MOTOROLA'
!            mad:DateCode        = Clip(tmp:YearCode) & Clip(tmp:MonthCode)
!            If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
!                !Found
!                tmp:Year    = mad:TheYear
!                tmp:Month   = mad:TheMonth
!
!                Loop x# = 1 To man:ClaimPeriod
!                    tmp:Month += 1
!                    If tmp:Month > 12
!                        tmp:Month = 1
!                        tmp:Year += 1
!                    End !If tmp:Month > 12
!                End !Loop x# = 1 To man:ClaimDate
!                !MESSAGE(tmp:month)
!                !MESSAGE(tmp:year)
!
!                If tmp:Year < Year(func:DateBooked)
!                    !POP Required
!                    Return Level:Fatal
!                End !If tmp:Year > func:DateBooked
!                If tmp:Year = Year(func:DateBooked) And tmp:Month < Month(func:DateBooked)
!                    Return Level:Fatal
!                End !If tmp:Year = func:DateBooked And tmp:Month < Month(func:DateBooked)
!            Else! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!                Return Level:Fatal
!            End! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
!
!        Of 'ALCATEL'
!            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!            man:Manufacturer    = 'ALCATEL'
!            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                !Found
!                tmp:YearCode    = Sub(func:DateCode,3,1)
!                tmp:MonthCode   = Sub(func:DateCode,2,1)
!                tmp:DayCode     = Sub(func:DateCode,1,1)
!
!                Access:MANUDATE.Clearkey(mad:DateCodeKey)
!                mad:Manufacuturer   = 'MOTOROLA'
!                mad:DateCode        = Clip(tmp:DayCode) & Clip(tmp:MonthCode) & Clip(tmp:YearCode)
!                If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
!                    !Found
!                    tmp:Year    = mad:TheYear
!                    tmp:Month   = mad:TheMonth
!                    tmp:Day     = mad:AlcatelDay
!
!
!                Else ! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
!                    !Error
!                End !If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
!            Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!                !Error
!            End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
!    End !Case func:Manufacturer
!    Return Level:Benign
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer

    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found

    Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


    !Now depending on the manufacturer determines where the
    !The date code is picked up from
    DoWeekCheck# = 0
    Case func:Manufacturer
        Of 'ALCATEL'
            tmp:YearCode    = Sub(func:DateCode,3,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'ERICSSON'
            tmp:YearCode    = Sub(func:DateCode,1,2)
            tmp:MonthCode   = Sub(func:DateCode,3,2)
            DoWeekCheck#    = 1
        Of 'PHILIPS'
            tmp:YearCode    = Sub(func:DateCode,5,2)
            tmp:MonthCode   = Sub(func:DateCode,7,2)
            DoWeekCheck#    = 1
        Of 'SAMSUNG'
            tmp:YearCode    = Sub(func:DateCode,4,1)
            tmp:MonthCode   = Sub(func:DateCode,5,1)
        Of 'BOSCH'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode   = Sub(func:DateCode,2,1)
        Of 'SIEMENS'
            tmp:YearCode    = Sub(func:DateCode,1,1)
            tmp:MonthCode    = Sub(func:DateCode,2,1)
        Of 'MOTOROLA'
            tmp:YearCode    = Sub(func:DateCode,5,1)
            tmp:MonthCode   = Sub(func:DateCode,6,1)
    End !Case func:Manufacturer

    If DoWeekCheck#
        tmp:Year    = tmp:YearCode
        StartOfYear# = Deformat('1/1/' & tmp:Year,@d5)

        DayOfYear#   = StartOfYear# + (tmp:MonthCode * 7)

        tmp:Year    = Year(DayOfYear#)
        tmp:Month   = Month(DayOfYear#)

    Else
        Access:MANUDATE.Clearkey(mad:DateCodeKey)
        mad:Manufacturer    = func:Manufacturer
        If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Sub(func:DateCode,1,1) & Clip(tmp:MonthCode) & Clip(tmp:YearCode)
        Else !If func:Manufacturer = 'ALCATEL'
            mad:DateCode        = Clip(tmp:YearCode) & Clip(tmp:MonthCode)
        End !If func:Manufacturer = 'ALCATEL'

        If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Found
            tmp:Year    = mad:TheYear
            tmp:Month   = mad:TheMonth

        Else! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:MANUDATE.Tryfetch(mad:DateCodeKey) = Level:Benign
    End !If DoWeekCheck#

    Loop x# = 1 To man:ClaimPeriod
        tmp:Month += 1
        If tmp:Month > 12
            tmp:Month = 1
            tmp:Year += 1
        End !If tmp:Month > 12
    End !Loop x# = 1 To man:ClaimDate

    If tmp:Year < Year(func:DateBooked)
        !POP Required
        Return Level:Fatal
    End !If tmp:Year > func:DateBooked
    If tmp:Year = Year(func:DateBooked) And tmp:Month < Month(func:DateBooked)
        Return Level:Fatal
    End !If tmp:Year = func:DateBooked And tmp:Month < Month(func:DateBooked)

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IMEIModelRoutine     PROCEDURE  (func:IMEI,func:ModelNumber) ! Declare Procedure
tmp:IMEI             STRING(6)
LocalActiveFlag      STRING(1)
save_esnmodel_id     USHORT,AUTO
ManufactInactive     BYTE
ManufactName         STRING(30)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_esn_id     ushort,auto
save_man_id     ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Free(glo:Q_ModelNumber)
    Clear(glo:Q_ModelNumber)
    LocalActiveFlag = 'Y'
    ManufactInactive = false

    If func:IMEI <> 'N/A' And func:IMEI <> ''
        If func:ModelNumber = ''
            ! The model number hasn't been filled yet (DBH: 13/10/2006)
            Save_ESNMODEL_ID = Access:ESNMODEL.SaveFile()
            Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
            esn:ESN    = Sub(func:IMEI,1,6)
            Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
            Loop ! Begin Loop
                If Access:ESNMODEL.Next()
                    Break
                End ! If Access:ESNMODEL.Next()
                If esn:ESN <> Sub(func:IMEI,1,6)
                    Break
                End ! If esn:ESN <> Sub(func:MEI,1,6)
                !added by Paul 08/06/2010 - Log no 11466
                If esn:Active <> 'Y' then
                    cycle
                End
                !check to see if the model number selected is active
                Access:ModelNum.clearkey(mod:Model_Number_Key)
                mod:Model_Number = esn:Model_Number
                If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
                    If mod:Active <> 'Y' then
                        cycle
                    End
                End
                !End Addition
                !tb12488 Disable Manufacturers - is this manufacturer inactive
                save_man_id = Access:Manufact.saveFile()
                Access:Manufact.clearkey(man:Manufacturer_Key)
                man:Manufacturer = mod:Manufacturer
                if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
                    !if man:Notes[1:8] = 'INACTIVE' then
                    !TB13214 - change to using field for inactive
                    if man:Inactive = 1 then
                        Access:manufact.restorefile(save_man_id)
                        ManufactInactive = true
                        ManufactName = man:Manufacturer
                        cycle
                    END !if inactive
                END !if manufact.fetch
                Access:manufact.restorefile(save_man_id)
                !END 12488

                GLO:Model_Number_Pointer = esn:Model_Number
                Add(glo:Q_ModelNumber)
            End ! Loop
            Access:ESNMODEL.RestoreFile(Save_ESNMODEL_ID)
            ! Find any TAC codes that match the first 6 digits of the IMEI (DBH: 13/10/2006)

            Save_ESNMODEL_ID = Access:ESNMODEL.SaveFile()
            Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
            esn:ESN = Sub(Func:IMEI,1,8)
            Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
            Loop ! Begin Loop
                If Access:ESNMODEL.Next()
                    Break
                End ! If Access:ESNMODEL.Next()
                If esn:ESN <> Sub(func:IMEI,1,8)
                    Break
                End ! If esn:ESN <> Sub(func:IMEI,1,8)
                !added by Paul 08/06/2010 - Log no 11466
                If esn:Active <> 'Y' then
                    cycle
                End
                Access:ModelNum.clearkey(mod:Model_Number_Key)
                mod:Model_Number = esn:Model_Number
                If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
                    If mod:Active <> 'Y' then
                        cycle
                    End
                End
                !End Addition

                !tb12488 Disable Manufacturers - is this manufacturer inactive
                save_man_id = Access:Manufact.saveFile()
                Access:Manufact.clearkey(man:Manufacturer_Key)
                man:Manufacturer = mod:Manufacturer
                if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
                    !if man:Notes[1:8] = 'INACTIVE' then
                    !TB13214 - change to using field for inactive
                    if man:Inactive = 1 then
                        Access:manufact.restorefile(save_man_id)
                        ManufactInactive = true
                        ManufactName = man:Manufacturer
                        cycle
                    END !if inactive
                END !if manufact.fetch
                Access:manufact.restorefile(save_man_id)
                !END 12488

                GLO:Model_Number_Pointer = esn:Model_Number
                Add(glo:Q_ModelNumber)
            End ! Loop
            Access:ESNMODEL.RestoreFile(Save_ESNMODEL_ID)
            ! Find any TAC Codes that match the first 8 digits of the IMEI (DBH: 13/10/2006)


!            !TB13243 - bug not showing message if there were several matches.
!            if ManufactInactive = true then
!                miss# = missive('A Model has been identified for this I.M.E.I. but the manufacturer '&clip(ManufactName)&' is marked as inactive.'&|
!                      '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
!                                   'mstop.jpg','/OK')
!                Return ''
!            END

            Case Records(glo:Q_ModelNumber)
            Of 0
                ! No TAC Codes have been found. So cannot return a Model Number (DBH: 13/10/2006)
                if ManufactInactive = true then
                    miss# = missive('A Model has been identified for this I.M.E.I. but the manufacturer '&clip(ManufactName)&' is marked as inactive.'&|
                          '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')

                ELSE
                    If SecurityCheck('IMEI TO MODEL - INSERT')
                        Case Missive('A Model has not been recognised for this I.M.E.I. Number.'&|
                          '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If SecurityCheck('IMEI TO MODEL - INSERT')
                END
                Return ''
            Of 1
                ! Only 1 TAC Code was found. Return that Model Number (DBH: 13/10/2006)
                Get(glo:Q_ModelNumber,1)
                Return glo:Model_Number_Pointer
            Else
                ! Call browse showing the queue values.
                Return SelectIMEIModelNumber(func:IMEI)
            End ! Case Records(ModelQUeue)
        Else ! If func:ModelNumber = ''
            ! The model number has also been passed. Do the IMEI and the Model Number match? (DBH: 13/10/2006)
            Found# = False
            Save_ESNMODEL_ID = Access:ESNMODEL.SaveFile()
            Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
            esn:ESN = Sub(func:IMEI,1,6)
            Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
            Loop ! Begin Loop
                If Access:ESNMODEL.Next()
                    Break
                End ! If Access:ESNMODEL.Next()
                If esn:ESN <> Sub(Func:IMEI,1,6)
                    Break
                End ! If esn:ESN <> Sub(Func:IMEI,1,6)
                !added by Paul 08/06/2010 - Log no 11466
                If esn:Active <> 'Y' then
                    cycle
                End
                Access:ModelNum.clearkey(mod:Model_Number_Key)
                mod:Model_Number = esn:Model_Number
                If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
                    If mod:Active <> 'Y' then
                        cycle
                    End
                End
                !End Addition
                !tb12488 Disable Manufacturers - is this manufacturer inactive
                save_man_id = Access:Manufact.saveFile()
                Access:Manufact.clearkey(man:Manufacturer_Key)
                man:Manufacturer = mod:Manufacturer
                if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
                    !if man:Notes[1:8] = 'INACTIVE' then
                    !TB13214 - change to using field for inactive
                    if man:Inactive = 1 then
                        ManufactInactive = true
                        ManufactName = man:Manufacturer
                        Access:manufact.restorefile(save_man_id)
                        cycle
                    END !if inactive
                END !if manufact.fetch
                Access:manufact.restorefile(save_man_id)
                !END 12488

                GLO:Model_Number_Pointer = esn:Model_Number
                Add(glo:Q_ModelNumber)
                If esn:Model_Number = func:ModelNumber
                    Found# = True
                    Break
                End ! If esn:Model_Number = func:ModelNumber
            End ! Loop
            Access:ESNMODEL.RestoreFile(Save_ESNMODEL_ID)
            ! Find any TAC Codes that match the first 6 digits of the IMEI (DBH: 13/10/2006)

            If Found# = False
                Save_ESNMODEL_ID = Access:ESNMODEL.SaveFile()
                Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
                esn:ESN = Sub(func:IMEI,1,8)
                Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
                Loop ! Begin Loop
                    If Access:ESNMODEL.Next()
                        Break
                    End ! If Access:ESNMODEL.Next()
                    If esn:ESN <> Sub(func:IMEI,1,8)
                        Break
                    End ! If esn:ESN <> CLip(func:IMEI,1,8)
                    !added by Paul 08/06/2010 - Log no 11466
                    If esn:Active <> 'Y' then
                        cycle
                    End
                    Access:ModelNum.clearkey(mod:Model_Number_Key)
                    mod:Model_Number = esn:Model_Number
                    If Access:ModelNum.fetch(mod:Model_Number_Key) = level:benign then
                        If mod:Active <> 'Y' then
                            cycle
                        End
                    End
                    !End Addition
                    !tb12488 Disable Manufacturers - is this manufacturer inactive
                    save_man_id = Access:Manufact.saveFile()
                    Access:Manufact.clearkey(man:Manufacturer_Key)
                    man:Manufacturer = mod:Manufacturer
                    if access:manufact.fetch(man:Manufacturer_Key) = level:Benign then
                        !if man:Notes[1:8] = 'INACTIVE' then
                        !TB13214 - change to using field for inactive
                        if man:Inactive = 1 then
                            ManufactInactive = true
                            ManufactName = man:Manufacturer
                            Access:manufact.restorefile(save_man_id)
                            cycle
                        END !if inactive
                    END !if manufact.fetch
                    Access:manufact.restorefile(save_man_id)
                    !END 12488

                    GLO:Model_Number_Pointer = esn:Model_Number
                    Add(glo:Q_ModelNumber)
                    If esn:Model_Number = func:ModelNumber
                        Found# = True
                        Break
                    End ! If esn:Model_Number = func:ModelNumber
                End ! Loop
                Access:ESNMODEL.RestoreFile(Save_ESNMODEL_ID)
                ! Find any TAC Codes that match the first 8 digits of the IMEI (DBH: 13/10/2006)
            End ! If FOund# = False

            If Found# = True
                ! There may be more than one TAC Code for this IMEI, but the entered Model Number is one of them. (DBH: 13/10/2006)
                Return func:ModelNumber
            End ! If Found# = True

            Case Records(glo:Q_ModelNumber)
            Of 0
                ! No TAC Codes were found (DBH: 13/10/2006)
                If SecurityCheck('IMEI TO MODEL - INSERT') or ManufactInactive
                    if ManufactInactive then
                        Miss# = missive('A model has been recoginsed for this I.M.E.I. but the manufacturer '&clip(ManufactName)&' is marked as inactive',|
                                        'ServiceBase 3g','Mstop.jpg','OK')
                    ELSE
                        ! User does not have access to add new TAC Codes, so you can't return a model number (DBH: 13/10/2006)
                        Case Missive('A Model has not been recognised for this I.M.E.I. Number.'&|
                          '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    END !if inactive
                    Return ''
                Else ! If SecurityCheck('IMEI TO MODEL - INSERT')
                    ! If the user has access, add the TAC Code and Model Number to the default list (DBH: 13/10/2006)
                    !Added by Paul 15/06/2010 - Log no 11466
                    !before we add a record - check to see if this is an inactive correlation - if so - dont add a new one
                    Access:ESNModel.Clearkey(esn:ESN_Key)
                    esn:ESN             = Sub(func:IMEI,1,8)
                    esn:Model_Number    = func:ModelNumber
                    If Access:ESNModel.Fetch(esn:ESN_Key) = level:benign then
                        !there is at least 1 record for this model - so dont add a new one
                        Case Missive('A Model has not been recognised for this I.M.E.I. Number.'&|
                          '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        Return 'ERROR'
                    Else

                        If Access:ESNMODEL.PrimeRecord() = Level:Benign
                            esn:ESN    = Sub(func:IMEI,1,8)
                            esn:Model_Number = func:ModelNumber
                            esn:Active = 'Y'

                            Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                            mod:Model_Number = func:ModelNumber
                            If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                !Found
                                esn:Manufacturer = mod:Manufacturer
                            Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                !Error
                            End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                            If Access:ESNMODEL.TryInsert() = Level:Benign
                                !Insert
                            Else ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                Access:ESNMODEL.CancelAutoInc()
                            End ! If Access:ESNMODEL.TryInsert() = Level:Benign
                        End ! If Access.ESNMODEL.PrimeRecord() = Level:Benign
                        Return func:ModelNumber

                    End !Added by Paul
                End ! If SecurityCheck('IMEI TO MODEL - INSERT')
            Of 1
                If SecurityCheck('IMEI TO MODEL - INSERT') or ManufactInactive
                    if ManufactInactive then
                        miss# = missive('There was a match between the I.M.I.E. but the manufacturer '&clip(ManufactName)&' is marked as inactive',|
                                        'ServiceBase 3g','mstop.jpg','/OK')
                    ELSE
                        ! The TAC Code was for a different model, but the user doesn't have access to insert a new TAC Code. Return nothing. (DBH: 13/10/2006)
                        Case Missive('There is a mismatch between the I.M.E.I. Number and Model Number.'&|
                          '|You do not have access to match the two together.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    END
                    Return ''
                Else ! If SecurityCheck('IMEI TO MODEL - INSERT')
                    ! Does the user want to ignore the mismatch is TAC/Model Number? (DBH: 13/10/2006)
                    Case IMEIModelMismatch(func:IMEI,func:ModelNumber,glo:Model_Number_Pointer)
                    Of 1 ! Use Existing Number
                        If SecurityCheck('IMEI TO MODEL - INSERT')
                            ! User has selected to use the new Model Number but does not have access to add new TAC Codes. Return nothing. (DBH: 13/10/2006)
                            Case Missive('A Model Number has not been recognised for this I.M.E.I. Number.'&|
                              '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            Return ''
                        Else ! If SecurityCheck('IMEI TO MODEL - INSERT')
                            ! User has selected to use the new Model Number. Add to the TAC Codes file. (DBH: 13/10/2006)
                            !Added by Paul 15/06/2010 - Log no 11466
                            !before we add a record - check to see if this is an inactive correlation - if so - dont add a new one
                            Access:ESNModel.Clearkey(esn:ESN_Key)
                            esn:ESN             = Sub(func:IMEI,1,8)
                            esn:Model_Number    = func:ModelNumber
                            If Access:ESNModel.Fetch(esn:ESN_Key) = level:benign then
                                !there is at least 1 record for this model - so dont add a new one
                                Case Missive('A Model has not been recognised for this I.M.E.I. Number.'&|
                                  '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Return 'ERROR'
                            Else
                                If Access:ESNMODEL.PrimeRecord() = Level:Benign
                                    esn:ESN = Sub(func:IMEI,1,8)
                                    esn:Model_Number = func:ModelNumber
                                    esn:Active = 'Y'

                                    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                                    mod:Model_Number = func:ModelNumber
                                    If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                        !Found
                                        esn:Manufacturer = mod:Manufacturer
                                    Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                        !Error
                                    End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                    If Access:ESNMODEL.TryInsert() = Level:Benign
                                        !Insert
                                    Else ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                        Access:ESNMODEL.CancelAutoInc()
                                    End ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                End ! If Access.ESNMODEL.PrimeRecord() = Level:Benign
                                Return func:ModelNumber
                            End !Added by Paul
                        End ! If SecurityCheck('IMEI TO MODEL - INSERT')
                    Of 2 ! Use New Mdel Number
                        ! User has selected to use the original Model Number that should be associated with the TAC Code. (DBH: 13/10/2006)
                        Return glo:Model_Number_Pointer
                    Else
                        Return func:ModelNumber
                    End ! Case IMEIModelMismatch(func:IMEI,func:ModelNumber,modque:ModelNumber)
                End ! If SecurityCheck('IMEI TO MODEL - INSERT')
            Else
                ! There are a number of Model Numbers attached to the entered TAC Code (DBH: 13/10/2006)
                Case Missive('There is a mismatch between the selected Model Number and I.M.E.I. Number.'&|
                  '|The I.M.E.I. corresponds to more than one Model Number.'&|
                  '|Do you wish to SELECT a Model Number, or IGNORE the mismatch?','ServiceBase 3g',|
                               'mquest.jpg','\Cancel|Ignore|/Select')
                    Of 3 ! Select Button
                        ! Call browse to select a model
                        Return SelectIMEIModelNumber(func:IMEI)
                    Of 2 ! Ignore Button
                        If SecurityCheck('IMEI TO MODEL - INSERT')
                            Case Missive('A Model Number has not been recognised for this I.M.E.I. Number.'&|
                              '|Please consult your supervisor to link this I.M.E.I. Number to this Model Number you require.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                            Return ''
                        Else ! If SecurityCheck('IMEI TO MODEL - INSERT')
                            Access:ESNModel.Clearkey(esn:ESN_Key)
                            esn:ESN             = Sub(func:IMEI,1,8)
                            esn:Model_Number    = func:ModelNumber
                            If Access:ESNModel.Fetch(esn:ESN_Key) = level:benign then
                                !there is at least 1 record for this model - so dont add a new one
                                Case Missive('A Model has not been recognised for this I.M.E.I. Number.'&|
                                  '|Please consult your supervisor to link this I.M.E.I. Number to the Model Number you require.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Return 'ERROR'
                            Else
                                If Access:ESNMODEL.PrimeRecord() = Level:Benign
                                    esn:ESN = Sub(func:IMEI,1,8)
                                    esn:Model_Number = func:ModelNumber
                                    esn:Active = 'Y'

                                    Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                                    mod:Model_Number = func:ModelNumber
                                    If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                        !Found
                                        esn:Manufacturer = mod:Manufacturer
                                    Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                        !Error
                                    End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                    If Access:ESNMODEL.TryInsert() = Level:Benign
                                        !Insert
                                    Else ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                        Access:ESNMODEL.CancelAutoInc()
                                    End ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                End ! If Access.ESNMODEL.PrimeRecord() = Level:Benign
                                Return func:ModelNumber
                            End !Added by Paul
                        End ! If SecurityCheck('IMEI TO MODEL - INSERT')
                    Of 1 ! Cancel Button
                        Return func:ModelNumber
                End ! Case Missive
            End ! Case Recods(ModelQUeue)
       End ! If func:ModelNumber = ''
    End ! If func:IMEI <> 'N/A' And func:IMEI <> ''
! Deleting (DBH 13/10/2006) # 8213 - Replaced by newer code
!    If func:IMEI <> 'N/A' And func:IMEI <> ''
!        tmp:IMEI    = Sub(func:IMEI,1,6)
!        If func:ModelNumber <> ''
!            !Does the IMEI match the Model?
!            Access:ESNMODEL.Clearkey(esn:ESN_Key)
!            esn:ESN             = tmp:IMEI
!            esn:Model_Number    = func:ModelNumber
!            If Access:ESNMODEL.Tryfetch(esn:ESN_Key) = Level:Benign
!                !Found
!                Return func:ModelNumber
!            Else! If Access:ESNMODEL.Tryfetch(esn:ESN_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!                !If not, count how many other entries for that IMEI Number
!                Count# = 0
!                Save_esn_ID = Access:ESNMODEL.SaveFile()
!                Access:ESNMODEL.ClearKey(esn:ESN_Only_Key)
!                esn:ESN = tmp:IMEI
!                Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
!                Loop
!                    If Access:ESNMODEL.NEXT()
!                       Break
!                    End !If
!                    If esn:ESN <> tmp:IMEI      |
!                        Then Break.  ! End If
!                    Count# += 1
!                    If Count# > 1
!                        Break
!                    End !If Count# > 1
!                End !Loop
!                Access:ESNMODEL.RestoreFile(Save_esn_ID)
!
!                Case Count#
!                    Of 0
!                        !There are no other Models' for this IMEI
!                        If SecurityCheck('IMEI TO MODEL - INSERT')
!                            Case Missive('A Model has not been recognised for this I.M.E.I. '&|
!                              '<13,10>'&|
!                              '<13,10>Please consult your supervisor to link this I.M.E.I. Number to the Model you require.','ServiceBase 3g',|
!                                           'mstop.jpg','/OK')
!                                Of 1 ! OK Button
!                            End ! Case Missive
!                            Return ''
!                        Else
!                            get(esnmodel,0)
!                            if access:esnmodel.primerecord() = Level:Benign
!                                esn:esn           = tmp:IMEI
!                                esn:model_number  = func:ModelNumber
!
!                                Access:MODELNUM.Clearkey(mod:Model_Number_Key)
!                                mod:Model_Number    = func:ModelNumber
!                                If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                                    !Found
!                                    esn:Manufacturer    = mod:Manufacturer
!                                Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                                    !Error
!                                End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!
!                                access:esnmodel.insert()
!                            end!if access:esnmodel.primerecord() = Level:Benign
!                            Return func:ModelNumber
!                        End !If SecurityCheck('IMEI TO MODEL - INSERT')
!
!                    Of 1
!                        !There is only one Model for this IMEI
!                        Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
!                        esn:ESN = tmp:IMEI
!                        If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
!                            !Found a mismatch
!                            If SecurityCheck('IMEI TO MODEL - INSERT')
!                                Case Missive('There is a mismatch between the I.M.E.I. Number and Model Number.'&|
!                                  '<13,10>'&|
!                                  '<13,10>You do not have access to match this Model to this I.M.E.I.','ServiceBase 3g',|
!                                               'mstop.jpg','/OK')
!                                    Of 1 ! OK Button
!                                End ! Case Missive
!                                Return ''
!                            END
!
!                            Case IMEIModelMismatch(func:IMEI,func:ModelNumber,esn:Model_Number)
!                                Of 1 !Use Existing Number
!                                    !There are no other Models' for this IMEI
!                                    If SecurityCheck('IMEI TO MODEL - INSERT')
!                                        Case Missive('A Model has not been recognised for this I.M.E.I. '&|
!                                          '<13,10>'&|
!                                          '<13,10>Please consult your supervisor to link this I.M.E.I. Number to the Model you require.','ServiceBase 3g',|
!                                                       'mstop.jpg','/OK')
!                                            Of 1 ! OK Button
!                                        End ! Case Missive
!                                        Return ''
!                                    Else
!                                        get(esnmodel,0)
!                                        if access:esnmodel.primerecord() = Level:Benign
!                                            esn:esn           = tmp:IMEI
!                                            esn:model_number  = func:ModelNumber
!
!                                            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
!                                            mod:Model_Number    = func:ModelNumber
!                                            If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                                                !Found
!                                                esn:Manufacturer    = mod:Manufacturer
!                                            Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                                                !Error
!                                            End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!
!                                            access:esnmodel.insert()
!                                        end!if access:esnmodel.primerecord() = Level:Benign
!                                        Return func:ModelNumber
!                                    End !If SecurityCheck('IMEI TO MODEL - INSERT')
!
!
!                                Of 2 !Use New Model Number
!                                    Return esn:Model_Number
!                                Else
!                                    Return func:ModelNumber
!                            End !If IMEIModelMismatch(func:IMEI,func:ModelNumber,esn:Model_Number)
!                        Else! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
!                            !Error
!                            !Assert(0,'<13,10>Fetch Error<13,10>')
!                        End! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
!
!                    Else
!                        !There are more than one IMEI for this model
!                        Case Missive('There is a mismatch between the selected Model Number and I.M.E.I. Number. The I.M.E.I. corresponds to more than one Model Number.'&|
!                          '<13,10>'&|
!                          '<13,10>Do you wish to SELECT a Model Number, or IGNORE the mismatch?','ServiceBase 3g',|
!                                       'mquest.jpg','\Cancel|Ignore|Select')
!                            Of 3 ! Select Button
!                                saverequest#      = globalrequest
!                                globalresponse    = requestcancelled
!                                globalrequest     = selectrecord
!                                glo:select7 = func:IMEI
!                                select_esn_model
!                                glo:select7 = ''
!                                if globalresponse = requestcompleted
!                                    Return esn:Model_Number
!                                Else
!                                    Return func:ModelNumber
!                                end
!                                globalrequest     = saverequest#
!                            Of 2 ! Ignore Button
!
!                                If SecurityCheck('IMEI TO MODEL - INSERT')
!                                    Case Missive('A Model has not been recognised for this I.M.E.I.'&|
!                                      '<13,10>'&|
!                                      '<13,10>Please consult your supervisor to link this I.M.E.I. Number to the Model you require.','ServiceBase 3g',|
!                                                   'mstop.jpg','/OK')
!                                        Of 1 ! OK Button
!                                    End ! Case Missive
!                                    Return ''
!                                Else
!                                    get(esnmodel,0)
!                                    if access:esnmodel.primerecord() = Level:Benign
!                                        esn:esn           = tmp:IMEI
!                                        esn:model_number  = func:ModelNumber
!
!                                        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
!                                        mod:Model_Number    = func:ModelNumber
!                                        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                                            !Found
!                                            esn:Manufacturer    = mod:Manufacturer
!                                        Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                                            !Error
!                                        End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!
!                                        access:esnmodel.insert()
!                                    end!if access:esnmodel.primerecord() = Level:Benign
!                                    Return func:ModelNumber
!                                End !If SecurityCheck('IMEI TO MODEL - INSERT')
!                            Of 1 ! Cancel Button
!                                Return func:ModelNumber
!                        End ! Case Missive
!                End !Case Count#
!            End! If Access:ESNMODEL.Tryfetch(esn:ESN_Key) = Level:Benign
!        Else !If func:ModelNumber <> ''
!            !If there is no model number already, go off and find one
!            Count# = 0
!            Save_esn_ID = Access:ESNMODEL.SaveFile()
!            Access:ESNMODEL.ClearKey(esn:ESN_Only_Key)
!            esn:ESN = tmp:IMEI
!            Set(esn:ESN_Only_Key,esn:ESN_Only_Key)
!            Loop
!                If Access:ESNMODEL.NEXT()
!                   Break
!                End !If
!                If esn:ESN <> tmp:IMEI      |
!                    Then Break.  ! End If
!                Count# += 1
!                If Count# > 1
!                    Break
!                End !If Count# > 1
!            End !Loop
!            Access:ESNMODEL.RestoreFile(Save_esn_ID)
!
!            Case Count#
!                Of 0
!                    !There are no corresponding Model Numbers
!                    !There are no other Models' for this IMEI
!                    If SecurityCheck('IMEI TO MODEL - INSERT')
!                        Case Missive('A Model has not been recognised for this I.M.E.I.'&|
!                          '<13,10>'&|
!                          '<13,10>Please consult your supervisor to link this I.M.E.I. Number to the Model you require.','ServiceBase 3g',|
!                                       'mstop.jpg','/OK')
!                            Of 1 ! OK Button
!                        End ! Case Missive
!                    End !If SecurityCheck('IMEI TO MODEL - INSERT')
!                    Return ''
!                Of 1
!                    !There is only ONE corresponding Model Number
!                    Access:ESNMODEL.Clearkey(esn:ESN_Only_Key)
!                    esn:ESN = tmp:IMEI
!                    If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
!                        !Found
!                        Return esn:Model_Number
!                    Else! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                        Return ''
!                    End! If Access:ESNMODEL.Tryfetch(esn:ESN_Only_Key) = Level:Benign
!                Else
!                    !There are many corresponding Model Numbers
!                    saverequest#      = globalrequest
!                    globalresponse    = requestcancelled
!                    globalrequest     = selectrecord
!                    glo:select7 = func:IMEI
!                    select_esn_model
!                    glo:select7 = ''
!                    if globalresponse = requestcompleted
!                        Return esn:Model_Number
!                    Else
!                        Return func:ModelNumber
!                    end
!                    globalrequest     = saverequest#
!            End !Case Count#
!        End !If func:ModelNumber <> ''
!    End !If func:IMEI <> 'N/A' And func:IMEI <> ''
!
!    Return func:ModelNumber
! End (DBH 13/10/2006) #8213
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IMEIModelMismatch PROCEDURE (func:IMEI,func:ModelNumber,func:ActualModel) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:IMEI             STRING(30)
tmp:ModelNumber      STRING(30)
tmp:ActualModelNumber STRING(30)
tmp:Return           BYTE(0)
window               WINDOW('I.M.E.I. / Model Number Mismatch'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,84,352,244),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1),COLOR(09A6A7CH)
                           PROMPT('There is a mismatch between the selected I.M.E.I. Number and Model Number.'),AT(188,142,320,20),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number:'),AT(213,166),USE(?Prompt2),FONT(,9,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(301,166),USE(tmp:IMEI),FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s15),AT(316,222),USE(tmp:ModelNumber),LEFT,FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(316,256,74,11),USE(tmp:ActualModelNumber),LEFT,FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           BUTTON,AT(420,214),USE(?Button3:2),FLAT,ICON('igmisp.jpg')
                           PROMPT('Do you wish to:?'),AT(188,196,164,12),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('a) Ignore Mismatch - Use Entered Model Number:'),AT(188,216,104,20),USE(?Prompt2:2),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('b) Change Model Number - Use Corresponding Model:'),AT(188,250,104,24),USE(?Prompt4),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(420,250),USE(?Button3),FLAT,ICON('chamodp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('I.M.E.I. / Model Number Mismatch'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020009'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('IMEIModelMismatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','IMEIModelMismatch')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  tmp:IMEI    = func:IMEI
  tmp:ModelNumber = func:ModelNumber
  tmp:ActualModelNumber = func:ActualModel
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','IMEIModelMismatch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button3:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3:2, Accepted)
      tmp:Return = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3:2, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      tmp:Return = 2
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Return = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020009'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020009'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020009'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
RemovePartsAndInvoiceText PROCEDURE                   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If SecurityCheck('REMOVE PARTS AND INVOICE TEXT')
        Case Missive('You do not have access to this option.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    Else !SecurityCheck('REMOVE PARTS AND INVOICE TEXT')
        FoundWar# = 0
        FoundChar# = 0
        Error# = 0

        If job:Chargeable_Job = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                FoundChar# = 1
                Break
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'

        If job:Warranty_Job = 'YES'
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                FoundWar# = 1
                Break
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If job:Warranty_Job = 'YES'

        If FoundWar# And FoundChar#
            Case Missive('There are Warranty And Chargeable parts attached to this job.'&|
              '<13,10>'&|
              '<13,10>Which do you want to remove?','ServiceBase 3g',|
                           'mquest.jpg','\Cancel|Chargeable|Warranty|All Parts')
                Of 4 ! All Parts Button
                Of 3 ! Warranty Button
                    FoundChar# = 0
                Of 2 ! Chargeable Button
                    FoundWar# = 0
                Of 1 ! Cancel Button
                    Error# = 1
            End ! Case Missive
        End !If FoundWar# And FoundChar#
        If Error# = 0
            Case Missive('Are you sure you want to RESTOCK the parts and DELETE the invoice text from this job?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes')
                Of 2 ! Yes Button
                    If FoundChar#
                        Save_par_ID = Access:PARTS.SaveFile()
                        Access:PARTS.ClearKey(par:Part_Number_Key)
                        par:Ref_Number  = job:Ref_Number
                        Set(par:Part_Number_Key,par:Part_Number_Key)
                        Loop
                            If Access:PARTS.NEXT()
                               Break
                            End !If
                            If par:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            If par:Order_Number <> ''
                                Case Missive('Cannot delete Chargeable Part ' & Clip(par:Part_Number) & '.'&|
                                  '<13,10>'&|
                                  '<13,10>It exists on an order.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Cycle
                            End !If par:Order_Number <> ''
                            StockError# = 0
                            If par:Part_Ref_Number <> ''
                                !From Stock
                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                sto:Ref_Number  = par:Part_Ref_Number
                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Found
                                    If sto:Sundry_Item <> 'YES'
                                        sto:quantity_stock += par:quantity
                                        If access:stock.update() = Level:Benign
                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'ADD', | ! Transaction_Type
                                                                 par:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                 job:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 par:Quantity, | ! Quantity
                                                                 par:Purchase_Cost, | ! Purchase_Cost
                                                                 par:Sale_Cost, | ! Sale_Cost
                                                                 par:Retail_Cost, | ! Retail_Cost
                                                                 '', | ! Notes
                                                                 'CHARGEABLE PART REMOVED FROM JOB') ! Information
                                                ! Added OK

                                            Else ! AddToStockHistory
                                                ! Error
                                            End ! AddToStockHistory
                                        End!If access:stock.update = Level:Benign
                                    End !If sto:Sundry_Item = 'YES'
                                    Delete(PARTS)
                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                    StockError# = 1
                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            Else !If par:Part_Ref_Number <> ''
                                StockError# = 1
                            End !If par:Part_Ref_Number <> ''

                            If StockError#
                                Case Missive('Cannot restock Chargeable Part ' & Clip(par:Part_Number) & '.'&|
                                  '<13,10>'&|
                                  '<13,10>Unable to find Stock Item.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Cycle
                            End !If StockError#

                        End !Loop
                        Access:PARTS.RestoreFile(Save_par_ID)
                    End !If FoundChar#
                    If FoundWar#
                        Save_wpr_ID = Access:WARPARTS.SaveFile()
                        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                        wpr:Ref_Number  = job:Ref_Number
                        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                        Loop
                            If Access:WARPARTS.NEXT()
                               Break
                            End !If
                            If wpr:Ref_Number  <> job:Ref_Number      |
                                Then Break.  ! End If
                            If wpr:Order_Number <> ''
                                Case Missive('Cannot Delete Warranty Part ' & Clip(wpr:Part_Number) & '.'&|
                                  '<13,10>'&|
                                  '<13,10>It exists on an order.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Cycle
                            End !If wpr:Order_Number <> ''
                            StockError# = 0
                            If wpr:Part_Ref_Number <> ''
                                !From Stock
                                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                                sto:Ref_Number  = wpr:Part_Ref_Number
                                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Found
                                    If sto:Sundry_Item <> 'YES'
                                        sto:quantity_stock += wpr:quantity
                                        If access:stock.update() = Level:Benign
                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                                 'ADD', | ! Transaction_Type
                                                                 wpr:Despatch_Note_Number, | ! Depatch_Note_Number
                                                                 job:Ref_Number, | ! Job_Number
                                                                 0, | ! Sales_Number
                                                                 wpr:Quantity, | ! Quantity
                                                                 wpr:Purchase_Cost, | ! Purchase_Cost
                                                                 wpr:Sale_Cost, | ! Sale_Cost
                                                                 wpr:Retail_Cost, | ! Retail_Cost
                                                                 '', | ! Notes
                                                                 'WARRANTY PART REMOVED FROM JOB') ! Information
                                                ! Added OK
                                            Else ! AddToStockHistory
                                                ! Error
                                            End ! AddToStockHistory
                                        End!If access:stock.update = Level:Benign
                                    End !If sto:Sundry_Item = 'YES'
                                    Delete(WARPARTS)
                                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                    StockError# = 1
                                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                            Else !If wpr:Part_Ref_Number <> ''
                                StockError# = 1
                            End !If wpr:Part_Ref_Number <> ''

                            If StockError#
                                Case Missive('Cannot restock Warranty Part ' & Clip(wpr:Part_Number) & '.'&|
                                  '<13,10>'&|
                                  '<13,10>Unable to find Stock Item.','ServiceBase 3g',|
                                               'mstop.jpg','/OK')
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Cycle
                            End !If StockError#

                        End !Loop
                        Access:WARPARTS.RestoreFile(Save_wpr_ID)
                    End !If FoundChar#

                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = job:Ref_Number
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Found
                        jbn:Invoice_Text = ''
                        Access:JOBNOTES.Update()
                    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign

                    If Access:AUDIT.PrimeRecord() = Level:Benign
                        aud:Ref_Number    = job:ref_number
                        aud:Date          = Today()
                        aud:Time          = Clock()
                        aud:Type          = 'JOB'
                        Access:USERS.ClearKey(use:Password_Key)
                        use:Password      = glo:Password
                        Access:USERS.Fetch(use:Password_Key)
                        aud:User          = use:User_Code
                        aud:Action        = 'PARTS AND INVOICE TEXT REMOVED'
                        Access:AUDIT.Insert()
                    End!If Access:AUDIT.PrimeRecord() = Level:Benign

                Of 1 ! No Button
            End ! Case Missive
        End !If Error# = 0
    End !SecurityCheck('REMOVE PARTS AND INVOICE TEXT')
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Error_Text PROCEDURE                                  !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       PANEL,AT(164,82,352,246),USE(?Panel5),FILL(COLOR:Red)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('The Following Errors Have Occurred'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       TEXT,AT(224,116,232,186),USE(glo:ErrorText),SKIP,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020008'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Error_Text')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Close
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','Error_Text')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Error_Text')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020008'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020008'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020008'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Validate_Job_Accessories PROCEDURE                    !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_jac_id          USHORT,AUTO
FilesOpened          BYTE
tag_temp             STRING(1)
tmp:Close            BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ACCESSOR)
                       PROJECT(acr:Accessory)
                       PROJECT(acr:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
acr:Accessory          LIKE(acr:Accessory)            !List box control field - type derived from field
acr:Model_Number       LIKE(acr:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Validate Accessories '),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(252,124,180,202),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)~Accessory~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(308,194,50,13),USE(?DASREVTAG),HIDE
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Validate Accessories'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON('sho&W tags'),AT(308,218,70,13),USE(?DASSHOWTAG),HIDE
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Accessory'),USE(?Tab:2)
                           ENTRY(@s30),AT(252,110,124,10),USE(acr:Accessory),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,219),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,251),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,283),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(448,164),USE(?Validate_Accessories),TRN,FLAT,LEFT,ICON('valaccp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,HIDE,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = acr:Accessory
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = acr:Accessory
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = acr:Accessory
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = acr:Accessory
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = acr:Accessory
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020003'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Validate_Job_Accessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCESSOR.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ACCESSOR,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Show the job number on the title bar# -  (DBH: 05-12-2003)
  If glo:Select1 <> ''
      ?WindowTitle{prop:Text} = 'Validate Accessories. Job Number: ' & Clip(glo:Select1)
  End !glo:Select1 <> ''
      ! Save Window Name
   AddToLog('Window','Open','Validate_Job_Accessories')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,acr:Accesory_Key)
  BRW1.AddRange(acr:Model_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?acr:Accessory,acr:Accessory,1,BRW1)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(acr:Accessory,BRW1.Q.acr:Accessory)
  BRW1.AddField(acr:Model_Number,BRW1.Q.acr:Model_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCESSOR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Validate_Job_Accessories')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020003'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020003'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020003'&'0')
      ***
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Validate_Accessories
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Accessories, Accepted)
      !error# = 0
      !setcursor(cursor:wait)
      !Loop x# = 1 To Records(glo:Queue)
      !    Get(glo:Queue,x#)
      !    access:jobacc.clearkey(jac:ref_number_key)
      !    jac:ref_number = glo:select1
      !    jac:accessory  = glo:pointer
      !    if access:jobacc.tryfetch(jac:ref_number_key)
      !        error# = 1
      !        Break
      !    end
      !End!Loop x# = 1 To Records(glo:Queue)
      !setcursor()
      !If error# = 1
      !    glo:select2 = 'FAIL'
      !Else!If error# = 1
      !    glo:select2 = ''
      !End!If error# = 1
      tmp:Close = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Accessories, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      !To prevent tick boxes being ticked when
      !scrolling - 3356 (DBH: 13-10-2003)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End !KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If tmp:Close <> 1
          Cycle
      End !tmp:Close <> 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?Dastag)
          Of F6Key
              Post(Event:Accepted,?Dastagall)
          Of F7Key
              Post(Event:Accepted,?Dasuntagall)
          Of F10Key
              Post(Event:Accepted,?Validate_Accessories)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Validate_Accessories


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = acr:Accessory
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = acr:Accessory
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

