

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01012.INC'),ONCE        !Local module procedure declarations
                     END


RemoveFromStockAllocation PROCEDURE  (func:PartRecordNumber,func:PartType) ! Declare Procedure
save_sto_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_epr_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
tmp:CurrentAccount   STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Part Record Number
    !Part Type
    !Quantity

    Access:STOCKALL.ClearKey(stl:PartRecordTypeKey)
    stl:PartType         = func:PartType
    stl:PartRecordNumber = func:PartRecordNumber
    If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Found
        Relate:STOCKALL.Delete(0)
    Else !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
        !Error
    End !If Access:STOCKALL.TryFetch(stl:PartRecordTypeKey) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
NextWaybillNumber    PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_way_id          USHORT,AUTO
tmp:WayBillNumber    LONG
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:WAYBILLS.Open
   Relate:WAYBCONF.Open
    Save_way_ID = Access:WAYBILLS.SaveFile()

    !Remove all blanks from waybill file - 3432 (DBH: 27-10-2003)
    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = 0
    Set(way:WayBillNumberKey,way:WayBillNumberKey)
    Loop
        If Access:WAYBILLS.NEXT()
           Break
        End !If
        If way:WayBillNumber <> 0      |
            Then Break.  ! End If
        Relate:WAYBILLS.Delete(0)
    End !Loop

!    !Remove Blank Waybill Numbers - 3309 (DBH: 24-09-2003)
!    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
!    way:WayBillNumber = 0
!    If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
!        !Found
!        Relate:WAYBILLS.Delete(0)
!    Else !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey). = Level:Benign
!        !Error
!    End !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey). = Level:Benign

! Changing (DBH 02/10/2007) # 9371 - Put some extra checking to prevent duplicate waybill numbers
!    tmp:WayBillNumber = 0
!
!    Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
!    Set(way:WayBillNumberKey)
!    Loop
!        If Access:WAYBILLS.PREVIOUS()
!            KeyField# = 1
!        Else !If Access:WAYBILLS.PREVIOUS()
!            KeyField# = way:WaybillNumber + 1
!        End !If Access:WAYBILLS.PREVIOUS()
!
!        If Access:WAYBILLS.PrimeRecord() = Level:Benign
!            way:WaybillNumber = KeyField#
!            If Access:WAYBILLS.TryInsert() = Level:Benign
!                !Insert Successful
!                tmp:WayBillNumber =  way:WaybillNumber
!                Break
!            Else !If Access:WAYBILLS.TryInsert() = Level:Benign
!                !Insert Failed
!                Access:WAYBILLS.Cancelautoinc()
!            End !If Access:WAYBILLS.TryInsert() = Level:Benign
!        End !If Access:WAYBILLS.PrimeRecord() = Level:Benign
!    End !Loop
! to (DBH 02/10/2007) # 9371
    tmp:WaybillNumber = 0
    Loop
        Access:WAYBILLS.Clearkey(way:WayBillNumberKey)
        way:WaybillNumber = 99999999
        Set(way:WayBillNumberKey,way:WayBillNumberKey)
        Loop ! Begin Loop
            If Access:WAYBILLS.Previous()
                ! An error. Usually means that there are no records in the file (DBH: 02/10/2007)
                KeyField# = 1
                Break
            End ! If Access:WAYBILLS.Next()
            ! Found the last entry. Add 1 to that waybill number (DBH: 02/10/2007)
            KeyField# = way:WaybillNumber + 1
            Break
        End ! Loop

        Access:WAYBILLS.ClearKey(way:WaybillNumberKey)
        way:WaybillNumber = KeyField#
        If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
            !Found
            If KeyField# = 1
                ! Loop has returned 1, but 1 already exists. Something has gone wrong (DBH: 02/10/2007)
                Break
            End ! If KeyField# = 1
            ! A waybill entry already exists with that waybill number. To prevent a duplicate, try again. (DBH: 02/10/2007)
            Cycle
        Else ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
            !Error
            ! Couldn't find the new waybill number. Job done.! (DBH: 02/10/2007)
            If Access:WAYBILLS.PrimeRecord() = Level:Benign
                way:WaybillNumber = KeyField#
                If Access:WAYBILLS.TryInsert() = Level:Benign
                    !Insert
                    tmp:WaybillNumber = way:WayBillNumber
                    Break
                Else ! If Access:WAYBILLS.TryInsert() = Level:Benign
                    Access:WAYBILLS.CancelAutoInc()
                End ! If Access:WAYBILLS.TryInsert() = Level:Benign
            End ! If Access.WAYBILLS.PrimeRecord() = Level:Benign
        End ! If Access:WAYBILLS.TryFetch(way:WaybillNumberKey) = Level:Benign
    End ! Loop

! End (DBH 02/10/2007) #9371
    Access:WAYBILLS.RestoreFile(Save_way_ID)

    If tmp:WayBillNumber = 0
        Case Missive('Unable to create a new Waybill Number.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    ELSE
        !TB13370 - J - 30/03/15
        !Waybill generated fine - copy details to confirmation file
        Access:Waybconf.primeRecord()
        WAC:WaybillNo        = tmp:WayBillNumber        !way:WayBillNumber
        WAC:AccountNumber    = way:AccountNumber
        WAC:GenerateDate     = today()
        WAC:GenerateTime     = clock()
        WAC:ConfirmationSent = 0
        Access:Waybconf.update()
    End !If tmp:WayBillNumber = 0

    Return tmp:WayBillNumber
   Relate:WAYBILLS.Close
   Relate:WAYBCONF.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AccountActivate48Hour PROCEDURE  (func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = func:AccountNumber
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        If tra:Activate48Hour
            Return True
        End !If tra:Activate48Hour
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Return False
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Allow48Hour          PROCEDURE  (func:IMEI,func:ModelNumber,f:AccountNumber) ! Declare Procedure
save_ESNMODEL_id     USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Relate:ESNMODEL.Open()

    save_ESNMODEL_id = Access:ESNMODEL.SaveFile()

    Return# = 0

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(func:IMEI,1,6)
    esn:Model_Number = func:ModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    Access:ESNMODEL.ClearKey(esn:ESN_Key)
    esn:ESN          = Sub(func:IMEI,1,8)
    esn:Model_Number = func:ModelNumber
    If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Found
        If esn:Include48Hour
            Return# = 1
        End !If esn:Include48Hour
    Else !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
        !Error
    End !If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign

    ! Inserting (DBH 21/02/2008) # 9717 - If Man/Model is setup for replenishment, then don't use 48 Hour
    If UseReplenishmentProcess(func:ModelNumber,f:AccountNumber) = 1
        Return# = 0
    End ! If UseReplenishmentProcess(f:ModelNumber,f:AccountNumber) = 1
    ! End (DBH 21/02/2008) #9717

    Access:ESNMODEL.RestoreFile(save_ESNMODEL_id)

    Relate:ESNMODEL.Close()

    Return Return#
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PassExchangeAssessment PROCEDURE                      ! Declare Procedure
tmp:FaultCode        STRING(255)
save_joo_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Loop through outfaults

    Save_joo_ID = Access:JOBOUTFL.SaveFile()
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = job:Ref_Number
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
           Break
        End !If
        If joo:JobNumber <> job:Ref_Number      |
            Then Break.  ! End If
        !Which is the main out fault?
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Lookup the Fault Code lookup to see if it's excluded
            Save_mfo_ID = Access:MANFAULO.SaveFile()
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                   Break
                End !If
                If mfo:Manufacturer <> job:Manufacturer      |
                Or mfo:Field_Number <> maf:Field_Number      |
                Or mfo:Field        <> joo:FaultCode      |
                    Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
                    !Make sure the descriptions match in case of duplicates
                    If mfo:ReturnToRRC
                        Return Level:Benign
                    End !If mfo:ReturnToRRC
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop
            Access:MANFAULO.RestoreFile(Save_mfo_ID)

        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    End !Loop
    Access:JOBOUTFL.RestoreFile(Save_joo_ID)

    !Is an outfault records on parts for this manufacturer
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
    map:Manufacturer = job:Manufacturer
    map:MainFault    = 1
    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Found
        !Loop through the parts as see if any of the faults codes are excluded
        If job:Warranty_Job = 'YES'

            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = wpr:Fault_Code1
                        Of 2
                            tmp:FaultCode        = wpr:Fault_Code2
                        Of 3
                            tmp:FaultCode        = wpr:Fault_Code3
                        Of 4
                            tmp:FaultCode        = wpr:Fault_Code4
                        Of 5
                            tmp:FaultCode        = wpr:Fault_Code5
                        Of 6
                            tmp:FaultCode        = wpr:Fault_Code6
                        Of 7
                            tmp:FaultCode        = wpr:Fault_Code7
                        Of 8
                            tmp:FaultCode        = wpr:Fault_Code8
                        Of 9
                            tmp:FaultCode        = wpr:Fault_Code9
                        Of 10
                            tmp:FaultCode        = wpr:Fault_Code10
                        Of 11
                            tmp:FaultCode        = wpr:Fault_Code11
                        Of 12
                            tmp:FaultCode        = wpr:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = tmp:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> tmp:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ReturnToRRC
                            Return Level:Benign
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)
        End !If job:Warranty_Job = 'YES'

        If job:Chargeable_Job = 'YES'
            !Loop through the parts as see if any of the faults codes are excluded
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                !Which is the main out fault?
                Access:MANFAULT.ClearKey(maf:MainFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:MainFault    = 1
                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Lookup the Fault Code lookup to see if it's excluded

                    !Work out which part fault code is the outfault
                    !and use that for the lookup
                    Case map:Field_Number
                        Of 1
                            tmp:FaultCode        = par:Fault_Code1
                        Of 2
                            tmp:FaultCode        = par:Fault_Code2
                        Of 3
                            tmp:FaultCode        = par:Fault_Code3
                        Of 4
                            tmp:FaultCode        = par:Fault_Code4
                        Of 5
                            tmp:FaultCode        = par:Fault_Code5
                        Of 6
                            tmp:FaultCode        = par:Fault_Code6
                        Of 7
                            tmp:FaultCode        = par:Fault_Code7
                        Of 8
                            tmp:FaultCode        = par:Fault_Code8
                        Of 9
                            tmp:FaultCode        = par:Fault_Code9
                        Of 10
                            tmp:FaultCode        = par:Fault_Code10
                        Of 11
                            tmp:FaultCode        = par:Fault_Code11
                        Of 12
                            tmp:FaultCode        = par:Fault_Code12
                    End !Case map:Field_Number

                    Save_mfo_ID = Access:MANFAULO.SaveFile()
                    Access:MANFAULO.ClearKey(mfo:Field_Key)
                    mfo:Manufacturer = job:Manufacturer
                    mfo:Field_Number = maf:Field_Number
                    mfo:Field        = tmp:FaultCode
                    Set(mfo:Field_Key,mfo:Field_Key)
                    Loop
                        If Access:MANFAULO.NEXT()
                           Break
                        End !If
                        If mfo:Manufacturer <> job:Manufacturer      |
                        Or mfo:Field_Number <> maf:Field_Number      |
                        Or mfo:Field        <> tmp:FaultCode      |
                            Then Break.  ! End If
                        !This fault relates to a specific part fault code number??
                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                            If mfo:RelatedPartCode <> maf:Field_Number
                                Cycle
                            End !If mfo:RelatedPartCode <> maf:Field_Number
                        End !If mfo:RelatedPartCode <> 0
                        IF mfo:ReturnToRRC
                            Return Level:Benign
                        End !IF mfo:ExcludeFromBouncer
                    End !Loop
                    Access:MANFAULO.RestoreFile(Save_mfo_ID)

                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                    !Error
                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)
        End !If job:Chargeable_Job = 'YES'
    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
        !Error
    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

    Return Level:Fatal
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IsThisModelAlternative PROCEDURE  (func:OriginalModel,func:NewModel) ! Declare Procedure
save_esn_id          USHORT,AUTO
save_esa_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_esn_ID = Access:ESNMODEL.SaveFile()
    Access:ESNMODEL.ClearKey(esn:Model_Number_Key)
    esn:Model_Number = func:OriginalModel
    Set(esn:Model_Number_Key,esn:Model_Number_Key)
    Loop
        If Access:ESNMODEL.NEXT()
           Break
        End !If
        If esn:Model_Number <> func:OriginalModel      |
            Then Break.  ! End If
        !Now check the alternative models to see if the
        !scanned model matches -  (DBH: 29-10-2003)
        Access:ESNMODAL.ClearKey(esa:RefModelNumberKey)
        esa:RefNumber   = esn:Record_Number
        esa:ModelNumber = func:NewModel
        If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Found
            Return True
        Else !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign
            !Error
        End !If Access:ESNMODAL.TryFetch(esa:RefModelNumberKey) = Level:Benign

    End !Loop
    Access:ESNMODEL.RestoreFile(Save_esn_ID)
    Return False
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ExcludeFromRRCRepair PROCEDURE  (func:ModelNumber)    ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If glo:WebJob
        !Only applies to RRC booking -  (DBH: 24-11-2003)
        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
        mod:Model_Number = func:ModelNumber
        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Found
            If mod:ExcludedRRCRepair
                Return True
            End !If mod:ExcludedRRCRepair
        Else !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
    End !If glo:WebJob


    Return False
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Is48HourOrderCreated PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
save_ex4_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_ex4_ID = Access:EXCHOR48.SaveFile()
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 0
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 0      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
            Return True
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop
    Access:EXCHOR48.RestoreFile(Save_ex4_ID)

    Return False
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Is48HourOrderProcessed PROCEDURE  (func:Location,func:JobNumber) ! Declare Procedure
save_ex4_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_ex4_ID = Access:EXCHOR48.SaveFile()
    Access:EXCHOR48.ClearKey(ex4:LocationJobKey)
    ex4:Received  = 1
    ex4:Returning = 0
    ex4:Location  = func:Location
    ex4:JobNumber = func:JobNumber
    Set(ex4:LocationJobKey,ex4:LocationJobKey)
    Loop
        If Access:EXCHOR48.NEXT()
           Break
        End !If
        If ex4:Received  <> 1      |
        Or ex4:Returning <> 0      |
        Or ex4:Location  <> func:Location      |
        Or ex4:JobNumber <> func:JobNumber      |
            Then Break.  ! End If
        If ex4:AttachedToJob = 0 And ex4:DateOrdered <> 0
            Return True
        End !If ex4:AttachedToJob = 0 And ex4:DateOrdered = 0
    End !Loop
    Access:EXCHOR48.RestoreFile(Save_ex4_ID)

    Return False
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ValidateBookedAccessories PROCEDURE                   !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_jac_id          USHORT,AUTO
FilesOpened          BYTE
tag_temp             STRING(1)
tmp:Close            BYTE(0)
tmp:Return           BYTE(0)
BRW1::View:Browse    VIEW(JOBACC)
                       PROJECT(jac:Accessory)
                       PROJECT(jac:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
jac:Accessory          LIKE(jac:Accessory)            !List box control field - type derived from field
jac:Ref_Number         LIKE(jac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select Accessories To Send To ARC'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(272,118,140,208),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)I@s1@120L(2)|M~Accessory~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(164,82,352,250),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Select Accessories To Send To ARC'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,334,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,335),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON('&Rev tags'),AT(572,274,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(572,298,70,13),USE(?DASSHOWTAG),HIDE
                       ENTRY(@s30),AT(272,106,124,10),USE(jac:Accessory),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       BUTTON,AT(448,242),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                       BUTTON,AT(448,272),USE(?DASTAGAll),FLAT,LEFT,ICON('tagallp.jpg')
                       BUTTON,AT(448,300),USE(?DASUNTAGALL),FLAT,LEFT,ICON('untagalp.jpg')
                       BUTTON,AT(448,158),USE(?Validate_Accessories),TRN,FLAT,LEFT,ICON('valaccp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = jac:Accessory
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = jac:Accessory
     ADD(glo:Queue,glo:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = jac:Accessory
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::4:QUEUE = glo:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = jac:Accessory
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = jac:Accessory
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020002'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ValidateBookedAccessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBACC.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','ValidateBookedAccessories')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,jac:Ref_Number_Key)
  BRW1.AddRange(jac:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?jac:Accessory,jac:Accessory,1,BRW1)
  BIND('tag_temp',tag_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(jac:Accessory,BRW1.Q.jac:Accessory)
  BRW1.AddField(jac:Ref_Number,BRW1.Q.jac:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:JOBACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ValidateBookedAccessories')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              tmp:Close = 1
              tmp:Return = 1
          Of 1 ! No Button
          Cycle
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020002'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020002'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020002'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Validate_Accessories
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Accessories, Accepted)
      tmp:Return = 0
      tmp:Close = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Accessories, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      !To prevent tick boxes being ticked when
      !scrolling - 3356 (DBH: 13-10-2003)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?DasTag)
      End !KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If tmp:Close <> 1
          Cycle
      End !tmp:Close <> 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?Dastag)
          Of F6Key
              Post(Event:Accepted,?Dastagall)
          Of F7Key
              Post(Event:Accepted,?Dasuntagall)
          Of F10Key
              Post(Event:Accepted,?Validate_Accessories)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::4:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Validate_Accessories


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jac:Accessory
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jac:Accessory
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

