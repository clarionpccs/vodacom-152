

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01013.INC'),ONCE        !Local module procedure declarations
                     END


HelpBrowser PROCEDURE (func:Path)                     !Generated from procedure template - Window

fepath               STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

                     !Start: FE Class Declaration
ThisViewer1          CLASS(FeBrowser)
Load                   FUNCTION   (string CurrentPath, <long CurrentControl>, <string pFlags>, <string pTargetFrameName>, <string pPostData>, <string pHeaders>),byte,proc  ,virtual
TakeEvent              PROCEDURE  ()  ,virtual
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()

feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill()
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit

feWatchingParentPropHideStatus_ThisViewer1  routine
  if ?feControl{prop:parent} > 0       ! Parent control is not the window (eg. Could be on a tab)
    if ((?feControl{prop:parent}){prop:visible}) = false     ! e.g. User selected a different tab, parent control no longer visisble
      ThisViewer1.HideControl ()
    else
      ThisViewer1.UnhideControl ()
      !ThisViewer1.Resize ()
    end
  end
 exit

feWatchingZOrdering_ThisViewer1  routine
 exit

feFixLeakingThroughTabsOnWindowOpen_ThisViewer1  routine
 exit


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('HelpBrowser')
  !fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & Clip(func:Path)
  ! Call the url directly, don't use the asp launching script
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  ! ThisViewer1.UseLogFile()  ! Being phased out. [GJc]
  ! Set up how File Explorer handles Dial Up Networking, set these options in your global extensions
  ThisViewer1.AutoDialDefaultInternetConnection = 0
  ThisViewer1.AutoDisconnectDefaultInternetConnection = 0
  ThisViewer1.KeepInternetConnectionOpenForLifeOfApp = 0
  ! Before Embed Point: %TopOfProcedure3) DESC(File Explorer) ARG(Top Of Procedure, 2) After template code)
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
  ! After Embed Point: %TopOfProcedure3) DESC(File Explorer) ARG(Top Of Procedure, 2) After template code)
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','HelpBrowser')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','HelpBrowser')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)
  do feFixLeakingThroughTabsOnWindowOpen_ThisViewer1


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF (event:feCallback)
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom)
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue

ThisViewer1.Load                   FUNCTION   (string CurrentPath, <long CurrentControl>, <string pFlags>, <string pTargetFrameName>, <string pPostData>, <string pHeaders>)      !File Explorer Object Procedure
ReturnValue             byte

  Code
    do feWatchingZOrdering_ThisViewer1
    ReturnValue = parent.Load (CurrentPath, CurrentControl, pFlags, pTargetFrameName, pPostData, pHeaders)
    Return(ReturnValue)

ThisViewer1.TakeEvent              PROCEDURE  ()    !File Explorer Object Procedure

  Code
    do feWatchingParentPropHideStatus_ThisViewer1
    parent.TakeEvent ()


Missive PROCEDURE (SentMessage,SentTitle,SentIcon,SentButtons) !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
MissiveText          STRING(255)
IconName             STRING(20)
FullButtonText       STRING(60)
ButtonText           STRING(10),DIM(6)
ButtonCount          SHORT
ButtonBreak          SHORT
RetValue             BYTE
locCallingWindowName STRING(255)
window               WINDOW('Caption'),AT(,,408,95),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6E7EFH),CENTER,GRAY,DOUBLE
                       BUTTON('Button 6'),AT(20,69,63,22),USE(?Button6),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 5'),AT(84,69,63,22),USE(?Button5),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 4'),AT(148,69,63,22),USE(?Button4),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 3'),AT(212,69,63,22),USE(?Button3),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 2'),AT(276,69,63,22),USE(?Button2),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 1'),AT(340,69,63,22),USE(?Button1),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI),DEFAULT
                       PANEL,AT(4,1,400,11),USE(?Panel3),FILL(09A6A7CH)
                       STRING('this string is changed on entry to the procedure - it is known as the stringtitl' &|
   'e.'),AT(12,2),USE(?StringTitle),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,13,400,52),USE(?Panel1),FILL(09A6A7CH)
                       IMAGE,AT(12,15,48,44),USE(?Image1)
                       TEXT,AT(68,16,332,46),USE(MissiveText),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                       PANEL,AT(4,69,400,24),USE(?Panel2),FILL(09A6A7CH)
                       IMAGE('MPurp.jpg'),AT(20,70,63,22),USE(?ButtonImage6)
                       IMAGE('MPurp.jpg'),AT(84,70,63,22),USE(?ButtonImage5)
                       IMAGE('MPurp.jpg'),AT(148,70,63,22),USE(?ButtonImage4)
                       IMAGE('MPurp.jpg'),AT(212,70,63,22),USE(?ButtonImage3)
                       IMAGE('MPurp.jpg'),AT(276,70,63,22),USE(?ButtonImage2)
                       IMAGE('MPurp.jpg'),AT(340,70,63,22),USE(?ButtonImage1)
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(RetValue)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Missive')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button6
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  locCallingWindowName = 0{prop:Text}
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !set up the window from sent variables
  ?StringTitle{Prop:Text}   = SentTitle
  IconName       = SentIcon
  if IconName    = '' then IconName = 'MBlank.jpg'.
  FullButtonText = SentButtons
  
  !sort out the Missive Text
  y# = 1
  loop x# = 1 to len(clip(SentMessage))
      if SentMessage[x#] = '|' then
          MissiveText[y#] = chr(13)
          y#+=1
          MissiveText[y#] = chr(10)
      ELSE
          MissiveText[y#]=SentMessage[x#]
      END
      y#+=1
  END !Loop through sent message
  
  if x# = y# then
      !there were no line breaks if they match
      !Add one to the start
      missiveText=chr(13)&chr(10)&MissiveText
  END !if x = y
  
  
  !Sort out the button texts
  ButtonCount = 0
  loop
      !Look for the divider
      ButtonBreak = instring('|',FullButtonText,1,1)
      if ButtonBreak = 0 then
          !No more buttons - just the last one to do
          ButtonCount += 1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount]=Clip(FullButtonText)
          Break  !From the loop
      ELSE
          !this button and another exists
          ButtonCount +=1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount] = FullButtonText[1:ButtonBreak-1]
          FullButtonText = FullButtonText[ButtonBreak+1:len(clip(FullButtonText))]
      END
  End!Loop through button text
  
  
  
  !Sort out the buttons
  !there must always be one!
  Case buttonText[1,1]
      of '/'
          ?ButtonImage1{prop:text} = 'Mgreen.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      of '\'
          ?ButtonImage1{prop:text} = 'Mred.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      else
          ?Button1{Prop:text}=ButtonText[1]
  END !Case
  
  if clip(buttonText[2]) = '' then
      hide(?Button2)
      ?ButtonImage2{prop:Text}='Blank.jpg'
  ELSE
      Case buttonText[2,1]
          of '/'
              ?ButtonImage2{prop:text} = 'Mgreen.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          of '\'
              ?ButtonImage2{prop:text} = 'Mred.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          else
              ?Button2{Prop:text}=ButtonText[2]
      END !Case
  END
  
  if clip(buttonText[3]) = '' then
      hide(?Button3)
      ?ButtonImage3{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[3,1]
          of '/'
              ?ButtonImage3{prop:text} = 'Mgreen.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          of '\'
              ?ButtonImage3{prop:text} = 'Mred.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          else
              ?Button3{Prop:text}=ButtonText[3]
      END !Case
  END
  
  if clip(buttonText[4]) = '' then
      hide(?Button4)
      ?ButtonImage4{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[4,1]
          of '/'
              ?ButtonImage4{prop:text} = 'Mgreen.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          of '\'
              ?ButtonImage4{prop:text} = 'Mred.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          else
              ?Button4{Prop:text}=ButtonText[4]
      END !Case
  END
  
  if clip(buttonText[5]) = '' then
      hide(?Button5)
      ?ButtonImage5{prop:text}='blank.jpg'
  ELSE
      Case buttonText[5,1]
          of '/'
              ?ButtonImage5{prop:text} = 'Mgreen.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          of '\'
              ?ButtonImage5{prop:text} = 'Mred.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          else
              ?Button5{Prop:text}=ButtonText[5]
      END !Case
  END
  
  if clip(buttonText[6]) = '' then
      hide(?Button6)
      ?ButtonImage6{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[6,1]
          of '/'
              ?ButtonImage6{prop:text} = 'Mgreen.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          of '\'
              ?ButtonImage6{prop:text} = 'Mred.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          else
              ?Button6{Prop:text}=ButtonText[6]
      END !Case
  
  END
  ?image1{prop:text}= IconName
  
  thiswindow.update()
  display()
  AddToLog('Message Box',CLIP(SentMessage),CLIP(locCallingWindowName))
   
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  AddToLog('Message Box Result',CLIP(SentMessage),CLIP(locCallingWindowName),'','Button Pressed = ' & RetValue)
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
      RetValue = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      RetValue = 6
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?Button5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      RetValue = 5
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      RetValue = 4
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      RetValue = 3
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      RetValue = 2
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      select(?Button1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

HasCurrencyBeenUpdated PROCEDURE  (func:Supplier,func:CurrencyCode,func:Message) ! Declare Procedure
save_currency_id     USHORT,AUTO
save_supplier_id     USHORT,AUTO
tmp:Return           BYTE(0)
tmp:Error            BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:CURRENCY.Open
   Relate:SUPPLIER.Open
    save_SUPPLIER_id = Access:SUPPLIER.SaveFile()
    save_CURRENCY_id = Access:CURRENCY.SaveFile()
    tmp:Return = True
    If func:Supplier <> ''
        ! If supplier specified, use this to lookup the currency - TrkBs: 5110 (DBH: 18-05-2005)
        Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
        sup:Company_Name = func:Supplier
        If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
            !Found
            If sup:UseForeignCurrency
                Access:CURRENCY.Clearkey(cur:CurrencyCodeKey)
                cur:CurrencyCode    = sup:CurrencyCode
                If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
                    ! Found
                    If cur:LastUpdateDate <> Today()
                        tmp:Return = False
                    End ! If cur:LastUpdateDate <> Today()
                Else ! If Access:CURRENCY.Tryfetch(cur:CurrencyCode) = Level:Benign
                    ! Error
                    Case Missive('Error! Unable to find the Currency Code ' & Clip(cur:CurrencyCode) & '.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    tmp:Return  = False
                    tmp:Error   = True
                End ! If Access:CURRENCY.Tryfetch(cur:CurrencyCode) = Level:Benign
            Else ! If sup:UseForeignCurrency
                tmp:Return = True
                ! This supplier is not using Currencies - TrkBs: 5110 (DBH: 18-05-2005)
            End ! If sup:UseForeignCurrency
        Else ! Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
            !Error
            Case Missive('Error! Unable to find the selected Supplier.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            tmp:Return = False
            tmp:Error  = True
        End ! Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
    Else ! If func:Supplier <> ''
        ! No supplier specified, hopefully the currency was - TrkBs: 5110 (DBH: 18-05-2005)
        Access:CURRENCY.Clearkey(cur:CurrencyCodeKey)
        cur:CurrencyCode    = func:CurrencyCode
        If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
            ! Found
            If cur:LastUpdateDate <> Today()
                tmp:Return = False
            End ! If cur:LastUpdateDate <> Today()
        Else ! If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
            ! Error
            Case Missive('Error! Unable to find the Currency Code ' & Clip(func:CurrencyCode) & '.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            tmp:Return = False
            tmp:Error  = True
        End ! If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
    End ! If func:Supplier <> ''
    Access:SUPPLIER.RestoreFile(save_SUPPLIER_id)
    Access:CURRENCY.RestoreFile(save_CURRENCY_id)

    If tmp:Return = False And tmp:Error = False
        If func:Message
            Case Missive('The Daily Rate for the selected currency has not been updated today.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! If func:Message
    End ! If tmp:Return = False

    Return tmp:Return





   Relate:CURRENCY.Close
   Relate:SUPPLIER.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetCurrencyConversion PROCEDURE  (func:Supplier,func:Amount,func:CurrencyCode,func:ConvertedAmount) ! Declare Procedure
save_currency_id     USHORT,AUTO
save_supplier_id     USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:CURRENCY.Open
   Relate:SUPPLIER.Open
    func:ConvertedAmount = 0
    func:CurrencyCode = ''
    save_SUPPLIER_id = Access:SUPPLIER.SaveFile()
    Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
    sup:Company_Name = func:Supplier
    If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
        ! Found
        If sup:UseForeignCurrency
            save_CURRENCY_id = Access:CURRENCY.SaveFile()
            Access:CURRENCY.Clearkey(cur:CurrencyCodeKey)
            cur:CurrencyCode = sup:CurrencyCode
            If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
                ! Found
                func:CurrencyCode = cur:CurrencyCode
                Case cur:DivideMultiply
                    Of '*'
                        func:ConvertedAmount = func:Amount / cur:DailyRate
                    Of '/'
                        func:ConvertedAmount = func:Amount * cur:DailyRate
                End ! Case cur:DivideMultiply
            Else ! If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
                ! Error
            End ! If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
            Access:CURRENCY.RestoreFile(save_CURRENCY_id)
        End ! If sup:UseForeignCurrency
    Else ! If Access:SUPPLIER.Tryfetch(sup:Compay_Name_Key) = Level:Benign
        ! Error
    End ! If Access:SUPPLIER.Tryfetch(sup:Compay_Name_Key) = Level:Benign
    Access:SUPPLIER.RestoreFile(save_SUPPLIER_id)
   Relate:CURRENCY.Close
   Relate:SUPPLIER.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetCurrencyConversionReverse PROCEDURE  (func:Supplier,func:Amount,func:CurrencyCode,func:ConvertedAmount) ! Declare Procedure
save_currency_id     USHORT,AUTO
save_supplier_id     USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:CURRENCY.Open
   Relate:SUPPLIER.Open
    func:ConvertedAmount = 0
    func:CurrencyCode = ''
    save_SUPPLIER_id = Access:SUPPLIER.SaveFile()
    Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
    sup:Company_Name = func:Supplier
    If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
        ! Found
        If sup:UseForeignCurrency
            save_CURRENCY_id = Access:CURRENCY.SaveFile()
            Access:CURRENCY.Clearkey(cur:CurrencyCodeKey)
            cur:CurrencyCode = sup:CurrencyCode
            If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
                ! Found
                func:CurrencyCode = cur:CurrencyCode
                Case cur:DivideMultiply
                    Of '*'
                        func:ConvertedAmount = func:Amount * cur:DailyRate
                    Of '/'
                        func:ConvertedAmount = func:Amount / cur:DailyRate
                End ! Case cur:DivideMultiply
            Else ! If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
                ! Error
            End ! If Access:CURRENCY.Tryfetch(cur:CurrencyCodeKey) = Level:Benign
            Access:CURRENCY.RestoreFile(save_CURRENCY_id)
        End ! If sup:UseForeignCurrency
    Else ! If Access:SUPPLIER.Tryfetch(sup:Compay_Name_Key) = Level:Benign
        ! Error
    End ! If Access:SUPPLIER.Tryfetch(sup:Compay_Name_Key) = Level:Benign
    Access:SUPPLIER.RestoreFile(save_SUPPLIER_id)
   Relate:CURRENCY.Close
   Relate:SUPPLIER.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddToStockHistory    PROCEDURE  (Long f:RefNo,String f:Trans,String f:Desp,Long f:JobNo,Long f:SaleNo,Long f:Qty,Real f:Purch,Real f:Sale,Real f:Retail,String f:Notes,String f:Info,<Real f:PAvPC>,<Real f:PPC>, <Real f:PSC>,<Real f:PRC>) ! Declare Procedure
retValue             BYTE
Local_RefNo          LONG
Local_SOH            LONG
Local_notes          STRING(255)
Local_Trans          STRING(3)
Local_Sale           DECIMAL(7,2)
Local_Purchase       DECIMAL(7,2)
Local_Retail         DECIMAL(7,2)
Local_info           STRING(255)
save_users_id        USHORT,AUTO
save_stohist_id      USHORT,AUTO
Save_StockAlias      USHORT
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!this assumes stock is open at the correct place
!Old Method passed variables
!Long   f:RefNo
!String f:Trans
!String f:Desp
!Long   f:JobNo
!Long   f:SaleNo
!Long   f:Qty
!Real   f:Purch
!Real   f:Sale
!Real   f:Retail
!String f:Notes
!String f:Info
!Real   f:AvPC
!Real   f:PPC
!Real   f:PSC
!Real   f:PRC

    relate:STOHIST.Open()
    relate:USERS.Open()
    relate:STOHISTE.Open()
    relate:STOCK_ALIAS.Open()

    save_users_id = Access:USERS.SaveFile()
    save_stohist_id = Access:STOHIST.SaveFile()
    Save_StockAlias = Access:STOCK_ALIAS.SaveFile()

    !will need the user details - only need to fetch once
    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        ! Found
    End ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign

   !preset return value to "FAIL"
    retValue = FALSE

    !remember the sent reference number / notes etc
    Local_RefNo     = f:RefNo
    Local_notes     = f:notes
    Local_SOH       = sto:Quantity_Stock
    Local_trans     = f:Trans
    Local_purchase  = f:Purch
    Local_Retail    = f:Retail
    Local_Sale      = f:Sale
    Local_info      = f:info

    if glo:RelocateStore then
        do Create_Complex_History
    ELSE
        do Create_Standard_History
    END

    Access:USERS.RestoreFile(save_users_id)
    Access:STOHIST.RestoreFile(save_stohist_id)
    Access:STOCK_ALIAS.RestoreFile(Save_StockAlias)

    relate:USERS.Close()
    relate:STOHIST.Close()
    relate:STOHISTE.Close()
    relate:STOCK_ALIAS.Close()

    return retValue


!subroutines===============================================================================

Create_Complex_History    Routine

    !this is going to be a standard history entry, but with some values changed
    Local_notes  = clip(f:notes)
    Local_trans  = f:trans

    !Use variables to work out the in warranty and out warranty values
    Local_Sale      = sto:Sale_Cost*(1+glo:RelocatedMark_Up/100)   
    Local_purchase  = sto:Sale_Cost*(1+InWarrantyMarkup(sto:Manufacturer,glo:RelocatedFrom)/100)   

    !add these into the info notes
    Local_info = 'ARC USAGE: Job Number '&f:JobNO&'<13,10>'&|
                 'In Warranty Cost '&clip(Local_Sale)&'<13,10>'&|
                 'Out Warranty Cost '&clip(Local_Purchase)&'<13,10>'&|
                  clip(F:Info)

    !Reset the sale costs to Main Store normals
    Local_Purchase = sto:Sale_Cost     
    Local_Sale     = sto:Purchase_Cost 
    Local_Retail   = sto:Retail_Cost

    do Create_Standard_History

    EXIT


Create_Standard_History       Routine


    If Access:STOHIST.PrimeRecord() = Level:Benign
        shi:Ref_Number           = Local_RefNo          !f:RefNo
        shi:User                 = use:User_Code
        shi:Transaction_Type     = Local_trans
        shi:Despatch_Note_Number = f:Desp
        shi:Job_Number           = f:JobNo
        shi:Sales_Number         = f:SaleNo
        shi:Quantity             = f:Qty
        shi:Date                 = Today()
        shi:Purchase_Cost        = Local_Purchase   !f:Purch
        shi:Sale_Cost            = Local_Sale       !f:Sale
        shi:Retail_Cost          = Local_Retail     !f:Retail
        shi:Notes                = Local_notes
        shi:Information          = Local_info
        shi:StockOnHand          = Local_SOH        !sto:Quantity_Stock
        If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Successful
            retValue = TRUE
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber = shi:Record_Number
                stoe:HistTime        = CLOCK()
                if glo:RelocateStore and Local_trans = 'DEC' then
                    stoe:ARC_Status = 'R'
                end !if glo:RelocateStore
                If (f:PAvPC = 0 AND f:PPC = 0 AND f:PSC = 0)
                    stoe:PreviousAveragePurchaseCost = sto:AveragePurchaseCost
                    stoe:PurchaseCost = sto:Purchase_Cost
                    stoe:SaleCost = sto:Sale_Cost
                    stoe:RetailCost = sto:Retail_Cost
                ELSE ! If (f:PAvPC = 0 AND f:PPC = 0 AND f:PSC = 0)
                    stoe:PreviousAveragePurchaseCost = f:PAvPC
                    stoe:PurchaseCost = f:PPC
                    stoe:SaleCost = f:PSC
                    stoe:RetailCost = f:PRC
                END ! If (f:PAvPC = 0 AND f:PPC = 0 AND f:PSC = 0)
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END ! IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
        Else ! If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Failed
            Access:STOHIST.CancelAutoInc()
        End ! If Access:STOHIST.TryInsert() = Level:Benign
    End ! If Access:STOHIST.PrimeRecord() = Level:Benign

    EXIT
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddToAudit           PROCEDURE  (LONG pJobNumber,STRING pType,STRING pAction,STRING pNotes) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
retValue    LONG(0)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:AUDIT.Open
   Relate:AUDIT2.Open
   Relate:AUDITE.Open
   Relate:USERS.Open
        retValue = 0
        
        IF (Access:AUDIT.PrimeRecord() = Level:Benign)
            aud:Date        = TODAY()
            aud:Time        = CLOCK()
            aud:Ref_Number  = pJobNumber
            
            Access:USERS.ClearKey(use:Password_Key)
            use:Password    = glo:Password
            IF (Access:USERS.TryFetch(use:Password_Key) = Level:Benign)
                aud:User    = use:User_Code    
            END ! IF
            
            aud:Action      = pAction
            aud:Type        = pType
            IF (Access:AUDIT.TryInsert() = Level:Benign)
                ! Insert Successful
                IF (Access:AUDITE.PrimeRecord() = Level:Benign)
                    aude:RefNumber  = aud:Record_Number
                    aude:IPAddress  = glo:IPAddress
                    aude:HostName   = glo:HostName
                    IF (Access:AUDITE.TryInsert())
                        Access:AUDITE.CancelAutoInc()
                    END ! IF
                END ! IF
            
                IF (Access:AUDIT2.PrimeRecord() = Level:Benign)
                    aud2:AUDRecordNumber    = aud:Record_Number
                    aud2:Notes              = pNotes
                    IF (Access:AUDIT2.TryInsert())
                        Access:AUDIT2.CancelAutoInc()
                    END ! IF
                END ! IF

                AddToLog('Audit Entry',CLIP(aud2:Notes),CLIP(AUD:Action),aud:Ref_Number)
                
                retValue = 1
            ELSE ! IF
                Access:AUDIT.CancelAutoInc()
                BEEP(BEEP:SystemHand)  ;  YIELD()
                CASE Missive('Unable to write to Audit Trail - AUDIT file appears to be locked.'&|
                    '|'&|
                    '|Please not what you were doing and contact Vodacom with this information.','ServiceBase',|
                               'mstop.jpg','/&OK') 
                OF 1 ! &OK Button
                END!CASE MESSAGE                
            END ! IF
            
        END ! IF
        
   Relate:AUDIT.Close
   Relate:AUDIT2.Close
   Relate:AUDITE.Close
   Relate:USERS.Close
    RETURN retValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PassMobileNumberFormat PROCEDURE  (String f:MobileNumber,Byte f:ShowError) ! Declare Procedure
tmp:Format           STRING(30),AUTO
tmp:StartQuotes      BYTE(0)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    ! Inserting (DBH 22/06/2006) #7597 - Check length if the mobile number is present
    If f:MobileNumber = ''
        Return True
    End ! If Clip(f:MobileNumber) = ''

    Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
    Of 0 ! Mobile Number Length
        If CheckLength('MOBILE','',f:MobileNumber)
            Return False
        End ! If CheckLength('MOBILE','',f:MobileNumber)
        Return True
    Of 1 ! Mobile Number Format
    ! End (DBH 22/06/2006) #7597

        tmp:Format  = GETINI('MOBILENUMBER','Format',,Clip(Path()) & '\SB2KDEF.INI')

        If tmp:Format = ''
            Return True
        End ! If Clip(tmp:Format) = ''

        ! Inserting (DBH 22/06/2006) #7597 - Check the length of the mobile before starting the format check
        Len# = 0
        Loop x# = 1 To Len(tmp:Format)
            If Sub(tmp:Format,x#,1) <> '*' And Sub(tmp:Format,x#,1) <> ''
                Len# += 1
            End ! If Sub(tmp:Format,x#,1) <> '*'
        End ! Loop x# = 1 To Len(tmp:Format)
        MobLen# = 0
        Loop x# = 1 To Len(f:MobileNumber)
            If Sub(f:MobileNumber,x#,1) <> ''
                MobLen# += 1
            End ! If Sub(f:MobileNumber,x#,1) <> ''
        End ! Loop x# = 1 To Len(f:MobileNumber)

        If MobLen# <> Len#
            If f:ShowError
                Case Missive('The length of the Mobile Number is invalid.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If f:ShowError
            Return False
        End ! If Len(f:MobileNumber) <> Len#
        ! End (DBH 22/06/2006) #7597


        Error# = 0
        y# = 1
        Loop x# = 1 To Len(tmp:Format)
            If tmp:StartQuotes
                If Sub(tmp:Format,x#,1) = '*'
                    tmp:StartQuotes = 0
                    Cycle
                End ! If Sub(tmp:Format,x#,1) = '"'
                If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    Error# = 1
                    Break
                Else ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
                    y# += 1
                    Cycle
                End ! If Sub(tmp:Format,x#,1) <> Sub(f:MobileNumber,y#,1)
            End ! If tmp:StartQuotes
            If Sub(tmp:Format,x#,1) = '0'
                If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
                    Error# = 1
                    Break
                End ! If Val(Sub(f:MobileNumber,y#,1)) < 48 Or Val(Sub(f:MobileNumber,y#,1)) > 57
            End ! If Sub(tmp:Format,x#,1) = '0'

            If Sub(tmp:Format,x#,1) = '*'
                tmp:StartQuotes = 1
                Cycle
            End ! If Sub(tmp:Format,x#,1) = '"'

            y# += 1
        End ! Loop x# = 1 To Len(Clip(tmp:Format))

        If Error# = 0
            Return True
        Else ! If Error# = 0
            If f:ShowError
                Case Missive('The format of the Mobile Number is invalid.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            End ! If f:ShowError
            Return False
        End ! If Error# = 0
    Else
        Return True
    End ! Case GETINI('MOBILENUMBER','FormatType',,Clip(Path()) & '\SB2KDEF.INI')
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
AddEmailSMS          PROCEDURE  (Long f:JobNumber, String f:AccountNumber, String f:MessageType, String f:SMSEmail, String f:MobileNumber, String f:EmailAddress, Real f:Cost, String f:EstimatePath) ! Declare Procedure
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:CompanyName      STRING(40)
tmp:TelephoneNumber  STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    VodacomClass.AddEmailSMS(f:JobNumber,f:AccountNumber,f:MessageType,f:SMSEmail,|
                f:MobileNumber,f:EmailAddress,f:Cost,f:EstimatePath)
!    include('AddEmailSMS.inc')

!        ! Message Types:
!    ! BOOK - Booking
!    ! 2ENG - Allocated To Engineer
!    ! ESTM - Estimate
!    ! 2ARC - Sent To ARC
!    ! 3PAR - Sent To 3rd Party
!    ! COMP - Job Completed
!    ! LOAN - Loan SMS
!
!    ! Error checking (DBH: 12-05-2006)
!    If Clip(f:MessageType) = '' Or |
!        Clip(f:SMSEmail) = '' Or |
!        (f:SMSEmail = 'SMS' And Clip(f:MobileNumber) = '') Or |
!        (f:SMSEmail = 'EMAIL' And Clip(f:EmailAddress) = '') Or |
!        f:JobNumber = 0 Or |
!        Clip(f:AccountNumber) = ''
!        Return
!    End ! f:JobNumber = 0
!
!    save_tra_id = Access:TRADEACC.SaveFile()
!    save_sub_id = Access:SUBTRACC.SaveFile()
!    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
!    tra:Account_Number  = f:AccountNumber
!    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!        ! Found
!        tmp:CompanyName = tra:Company_Name
!        tmp:TelephoneNumber = tra:Telephone_Number
!        !added by PS on 11/05/209 log no 10321
!        !check to see if VODACARE is contained in the company name
!        if ~instring('VODACARE',clip(tmp:CompanyName),1,1)
!            tmp:CompanyName = 'VODACARE ' & clip(tmp:CompanyName)
!        end
!    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!        ! Error
!        ! Insert --- If VCP then the Sub Account will be passed instead of the Trade Account (DBH: 03/02/2010) #11014
!        Access:SUBTRACC.clearkey(sub:Account_Number_Key)
!        sub:Account_Number = f:AccountNumber
!        if (access:SUBTRACC.tryfetch(sub:Account_Number_Key) = Level:Benign)
!            tmp:CompanyName = sub:Company_Name
!            tmp:TelephoneNumber = sub:Telephone_Number
!        else
!            tmp:CompanyName = ''
!            tmp:TelephoneNumber = ''
!        end ! if (access:SUBTRACC.tryfetch(sub:Account_Number_Key) = Level:Benign)
!        ! end --- (DBH: 03/02/2010) #11014
!    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
!
!
!    If Access:SMSMAIL.PrimeRecord() = Level:Benign
!        sms:RefNumber       = f:JobNumber
!        sms:MSISDN          = f:MobileNumber
!        sms:EmailAddress    = f:EmailAddress
!        ! Inserting (DBH 15/06/2006) #7597 - Tell vodacom what sort of message this is
!        If Clip(f:MobileNumber) <> ''
!            sms:SendToSMS       = 'T'
!        Else ! If Clip(f:MobileNumber) <> ''
!            sms:SendToSMS       = 'F'
!        End ! If Clip(f:MobileNumber) <> ''
!        If Clip(f:EmailAddress) <> ''
!            sms:SendToEmail     = 'T'
!        Else ! If Clip(f:EmailAddress) <> ''
!            sms:SendToEmail     = 'F'
!        End ! If Clip(f:EmailAddress) <> ''
!        ! End (DBH 15/06/2006) #7597
!        sms:DateInserted    = Today()
!        sms:TimeInserted    = Clock()
!        sms:SMSSent         = 'F'
!        !added Log no 10321 PS 28/04/09
!        If f:MessageType = 'LOAN' then
!            sms:PathToEstimate = ''
!        Else
!            sms:PathToEstimate  = f:EstimatePath
!        End!If f:MessageType = 'LOAN' then
!        sms:SBUpdated       = 0
!
!!        Case f:MessageType
!!        Of 'BOOK'
!!            sms:MSG = 'Your handset has been booked in at Vodacare ' & Clip(tmp:CompanyName) & ' for repair. Please quote job number ' & Clip(f:JobNumber) & ' as your reference.'
!!        Of '2ENG'
!!            sms:MSG = 'Job number ' & Clip(f:JobNumber) & ' has been allocated to a technician for assessment and/or repair at Vodacare ' & Clip(tmp:CompanyName) & '.'
!!        Of 'ESTM'
!!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' will cost R' & Format(f:Cost,@n8.2) & '. Please contact Vodacare ' & Clip(tmp:CompanyName) & ' to accept or reject this cost.'
!!        Of '2ARC'
!!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' has been sent to a higher level repair centre. You will be notified when it is returned and is ready for collection.'
!!        Of '3PAR'
!!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' has been sent to the manufacturer for repair. You will be notified when it is returned as is ready for collection.'
!!        Of 'COMP'
!!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' is ready for collection at Vodacare ' & Clip(tmp:CompanyName) & '.'
!!        End ! Case f:MessageType
!
!        !change made 27/04/09 by PS Log No 10321
!        Case f:MessageType
!        Of 'BOOK'
!            sms:MSG = 'Your device has been booked in at ' & Clip(tmp:CompanyName) & ' for repair. ' & |
!                            'Please quote job number as your reference. Tel no ' & clip(tmp:TelephoneNumber) & '.'
!        Of '2ENG'
!            sms:MSG = 'Job number ' & Clip(f:JobNumber) & ' has been allocated to a technician for assessment / repair. ' & |
!                                     Clip(tmp:CompanyName) & ' ' & clip(tmp:TelephoneNumber) & '.'
!        Of 'ESTM'
!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' repair quote R' & Format(f:Cost,@n8.2) & '. Please contact ' & |
!                        Clip(tmp:CompanyName) & ' ' & clip(tmp:TelephoneNumber) & ' to accept or reject this quote.'
!        Of '2ARC'
!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' has been sent to Vodacare head office. You will be notified ' & |
!                    'when it is ready for collection at ' & clip(tmp:CompanyName) & ' ' & clip(tmp:TelephoneNumber) & '.'
!        Of '3PAR'
!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' has been sent to the manufacturer for repair. ' & |
!                                            'You will be notified when it is returned as is ready for collection.'
!        Of 'COMP'
!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' is ready for collection at ' & Clip(tmp:CompanyName) & ' ' & |
!                                clip(tmp:TelephoneNumber) & '. Please consult office hours prior to collection.'
!        Of 'LOAN'
!            sms:MSG = 'Dear Customer kindly return the loan issued to you by ' & Clip(tmp:CompanyName) & |
!                        '. Non Compliance will result in the unit being blacklisted on the ' & clip(f:EstimatePath) & '.'
!        Of 'BERL'
!            sms:MSG = 'Job Number ' & Clip(f:JobNumber) & ' is ready for collection at ' & Clip(tmp:CompanyName) & ' ' & |
!                                clip(tmp:TelephoneNumber) & ', please note that your handset was not repairable.'
!        End ! Case f:MessageType
!        If Access:SMSMAIL.TryInsert() = Level:Benign
!            ! Insert Successful
!
!        Else ! If Access:SMSMAIL.TryInsert() = Level:Benign
!            ! Insert Failed
!            Access:SMSMAIL.CancelAutoInc()
!        End ! If Access:SMSMAIL.TryInsert() = Level:Benign
!    End !If Access:SMSMAIL.PrimeRecord() = Level:Benign
!
!    Access:TRADEACC.RestoreFile(save_tra_id)
!    access:SUBTRACC.RestoreFIle(save_Sub_id)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
IsIMEIValid          PROCEDURE  (String f:IMEINumber,String f:ModelNumber,Byte f:ShowError) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:MODELNUM.Open
    If Clip(f:IMEINumber) = '' Or Clip(f:ModelNumber) = ''
        Return True
    Else ! If Clip(f:IMEINumber) = '' Or Clip(f:ModelNumber) = ''
        Found# = False
        Loop x# = 1 To Len(Clip(f:IMEINumber))
            If IsAlpha(Sub(f:IMEINumber,x#,1))
                Found# = True
                Break
            End ! If IsAlpha(Sub(f:IMEINumber,x#,1))
        End ! Loop x# = 1 To Len(Clip(f:IMEINumber))
        If Found# = True
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = f:ModelNumber
            If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                ! Found
                If mod:AllowIMEICharacters
                    Return True
                Else ! If mod:AllowIMEICharacters
                    If f:ShowError
                        Case Missive('The format of the entered IMEI Number is not valid.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                    End ! If f:ShowError
                    Return False
                End ! If mod:AllowIMEICharacters
            Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                ! Error
                Return True
            End ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        Else ! If IsAlpha(f:IMEINumber)
            Return True
        End ! If IsAlpha(f:IMEINumber)
    End ! If Clip(f:IMEINumber) = '' Or Clip(f:ModelNumber) = ''
    Return True

   Relate:MODELNUM.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
