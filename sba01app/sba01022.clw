

   MEMBER('sba01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA01022.INC'),ONCE        !Local module procedure declarations
                     END


AddLoanHistory       PROCEDURE  (LONG fRefNumber,STRING fStatus,<STRING fNotes>) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    IF (fRefNumber = 0)
        RETURN
    END
    Relate:LOANHIST.Open()
    IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
        loh:Ref_Number  = fRefNumber
        loh:Date        = TODAY()
        loh:Time        = CLOCK()
        loh:User        = glo:Usercode
        loh:Status      = fStatus
        loh:Notes       = fNotes
        IF (Access:LOANHIST.TryInsert())
            Access:LOANHIST.CancelAutoInc()
        END
    END ! IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
    Relate:LOANHIST.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
_PreClaimThirdParty  PROCEDURE  (STRING fThirdParty,STRING fManufacturer) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.PreClaimThirdParty(fThirdParty,fManufacturer))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CaptureUserMobile PROCEDURE                           !Generated from procedure template - Window

ReturnMobile         STRING(20)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(648,3,28,24),USE(?ButtonHelp),SKIP,TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(244,150,192,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('You must supply a mobile number'),AT(248,152),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(384,152),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,166,192,95),USE(?Panel3),FILL(09A6A7CH)
                       STRING('You do not currently have a mobile number'),AT(252,174),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       STRING('registered against your details'),AT(252,182),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       STRING('You must supply one now'),AT(252,200),USE(?String3),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                       STRING('If you cancel from this screen you will not be'),AT(252,216),USE(?String4),TRN,FONT(,,COLOR:White,FONT:regular,CHARSET:ANSI)
                       STRING('able to log on to ServiceBase'),AT(252,226),USE(?String5),TRN,FONT(,,COLOR:White,FONT:regular,CHARSET:ANSI)
                       ENTRY(@s20),AT(278,244,124,10),USE(ReturnMobile)
                       PANEL,AT(244,262,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(369,262),USE(?ButtonOK),FLAT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(300,262),USE(?ButtonCancel),FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(ReturnMobile)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g '
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g '
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020779'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CaptureUserMobile')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
      ! Save Window Name
   AddToLog('Window','Open','CaptureUserMobile')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','CaptureUserMobile')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
      if clip(ReturnMobile) = '' then cycle.
      
      
      !check for other bits
      If PassMobileNumberFormat(ReturnMobile,1) = False
          ReturnMobile = ''
          Cycle
      End ! If PassMobileNumberFormat(jobe2:SMSAlertNumber,1) = False
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    OF ?ButtonCancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
      glo:ErrorText = 'CANCEL'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020779'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020779'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020779'&'0')
      ***
    OF ?ButtonOK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
PutIniEXT            PROCEDURE  (SentSection,SentEntry,SentValue,SentFile) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!format (SentSection,SentEntry,SentValue,SentFile)
!this mimics the normal putini format so all you need to do is to change the putini(...) to putiniEXT(...)
!Putinix (as a name) is already in use so this gets called PutIniExt
!note that this is intended for one or two off changes, there is a faster PUTINIUpdateAll to deal with major changes

    x# = vodacomclass.PutiniEX(Upper(SentSection),upper(SentEntry),SentValue,SentFile)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CallSiebelExt        PROCEDURE  (String SentMobile,long SentJob) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
!Local File and Variables Setup
SFileName               string(255), static
LFileName               string(255), static
loc:LoyaltyStatus       STRING(255)
loc:Error               LONG
loc:ErrorDescription    STRING(255)
FMWUserName             STRING(100)
FMWPassword             STRING(100)
CustomerLookup          STRING(1)
IdentifyStack           STRING(1)
CustomerExceptionFolder STRING(255)
CallUsing               STRING(10)
PathToSiebel            STRING(255)
PathToSiebel2           STRING(255)
PathToExe               STRING(255)
SB2KDEFINI                  STRING(255)

!Output file from Brian's programme - pipe deliniated
SiebelFile file, driver('BASIC', '/COMMA=124 /ALWAYSQUOTE=OFF'), pre(SOUT), name(SFileName), create, BINDABLE, THREAD
SFRec         record
SFField1      string(80)
SFField2      string(80)
SFField3      string(80)
SFField4      string(80)
SFField5      string(80)
SFField6      string(80)
SFField7      string(80)
SFField8      string(80)
SFField9      string(250)    !large to hold the loyalty status
                end ! record
            end ! file

!logging file for errors
LoggingFile file, driver('BASIC'), pre(LOUT), name(LFileName), create
LFRec         record
LFField1      string(80)
LFField2      string(80)
LFField3      string(80)
LFField4      string(80)
LFField5      string(80)
LFField6      string(80)
LFField7      string(80)
LFField8      string(80)
                end ! record
            end ! file
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!    VodacomClass.SeibelCall(SentMobile,SentJob)
!    VodacomClass.SeibelCall    PROCEDURE(String SentMobile,long SentJob,<NetWebServerWorker p_web>)
!Removed from Vodacomclass, because it did not work there for webserver

    !Files
    Relate:JOBSE3.Open()
    Relate:C3DMONIT.Open()
    Relate:SOAPERRS.Open()
    Relate:SMSMAIL.Open()

    !Defaults
    SB2KDEFINI = clip(path())&'\SB2kDef.ini'
    CustomerLookup          = GetIni('C3D','CustomerLookup', 'N',    SB2KDEFINI)

    if CustomerLookup = 'N' then

        !Do nothing
        glo:Select29 = 'MANUAL'

    ELSE

        
        !read the other defaults
        loc:Error = 0
        loc:ErrorDescription    = ''
        FMWUserName             = GetIni('C3D','FMWUserName',    '',    SB2KDEFINI)
        FMWPassword             = GetIni('C3D','FMWPassword',    '',    SB2KDEFINI)
        IdentifyStack           = GetIni('C3D','IdentifyStack',  '',    SB2KDEFINI)
        CustomerExceptionFolder = GetIni('C3D','ExceptionFolder','',    SB2KDEFINI)
        CallUsing               = GetIni('C3D','CallUsing',      '',    SB2KDEFINI)
        PathToSiebel            = GetIni('C3D','PathToSiebel',   '',    SB2KDEFINI)
        PathToSiebel2           = GetIni('C3D','PathToSiebel2',  '',    SB2KDEFINI)
        PathToExe               = GetIni('C3D','PathToExe',      '',    SB2KDEFINI)

        !blank the used variables
        !Glo:Select21-29 are only used in CompletedJobsStatisticsExport -
        !Glo:Select20 is used to make print calls, but is set and reset immediately before and after
        !so I can use them again here to return the data
        glo:Select20 = ''     !ERROR CODE
        glo:Select21 = ''     !Migrated (Y/N)
        glo:Select22 = ''     !account ID
        glo:Select23 = ''     !title
        glo:Select24 = ''     !firstname
        glo:Select25 = ''     !lastname
        glo:Select26 = ''     !Add1
        glo:Select27 = ''     !Add2
        glo:Select28 = ''     !Add3 (Suburb)
        glo:Select29 = ''     !Action to be taken AUTO/MANUAL/MANAUL PLUS/RETRY

        !checks
        if clip(FMWUserName)    = '' then loc:Error += 1.
        if clip(FMWPassword)    = '' then loc:Error += 2.
        if clip(CustomerLookup) = '' then loc:Error += 4.
        if clip(IdentifyStack)  = '' then loc:Error += 8.
        if clip(CustomerExceptionFolder) = '' then loc:Error += 16.
        if clip(CallUsing)      = '' then loc:Error += 32.
        if clip(PathToSiebel)   = '' then loc:Error += 64.
        if clip(PathToExe)      = '' then loc:Error += 128.
        if exists(PathToExe) = false  then loc:Error += 256.
        if exists(clip(PathToExe)&'\SiebelCom.exe') = false then loc:Error += 512.

        if loc:Error > 0 then

            glo:Select20 = 'SETUP'
            loc:ErrorDescription = 'CODE ='&clip(Loc:Error)

        ELSE

            !if IdentifyStack = 'Y' then
            !Cannot use IdentifyStack yet - not available on the server
            !cannot use CallUsing yet either for same reason

            !Brian's new interface - this now does all the work
            run(clip(PathToExe)&'\SiebelCom.exe '&|
                                '  "Username:'&clip(FMWUserName)&|
                                '" "Password:'&clip(FMWPassword)&|
                                '" "URL:'     &clip(PathToSiebel)&|
                                '" "URL2:'    &clip(PathToSiebel2)&|
                                '" "MSISDN:'  &clip(SentMobile)&|
                                '" "Output:'&clip(PathToExe)&'\S1'&clip(SentJob)&'.in"',1)

            !look for incoming data/Import
            SFileName = CLIP(PathToExe) & '\S1'&clip(SentJob)&'.in'

            if ~exists(Sfilename) then

                Glo:Select20 = 'SYSTEM'
                loc:ErrorDescription = 'RETURNING FILE NOT FOUND '&clip(SFilename)

            ELSE

                Open(SiebelFile)
                Set(SiebelFile)
                Next(SiebelFile)
                if error() then

                    Glo:Select20 = 'SYSTEM'
                    loc:ErrorDescription = 'UNABLE TO READ RETURNED FILE'

                ELSE

                    if upper(clip(SOUT:SFField1)) = 'ERROR' then

                        Next(SiebelFile)
                        if clip(SOUT:SFField1) = '' then SOUT:SFField1 = 'SYSTEM'.

                        GLO:Select20  = SOUT:SFField1
                        loc:ErrorDescription = SOUT:SFField2

                    ELSE

                        Next(SiebelFile)
                        if error() then
                            glo:Select20 = 'SYSTEM'
                            loc:ErrorDescription = 'CANNOT READ SECOND LINE'
                        ELSE
                            !Interpret data to variables
                            glo:Select20 = 'OK'                !Error Code
                            glo:Select21 = SOUT:SFField1       !Migration Status
                            glo:Select22 = SOUT:SFField2       !Account ID
                            glo:Select23 = ''                  !'TITLE'
                            glo:Select24 = SOUT:SFField4       !'FORENAME'
                            glo:Select25 = SOUT:SFField5       !'SURNAME'
                            glo:Select26 = SOUT:SFField6       !'LINE1'
                            glo:Select27 = SOUT:SFField7       !'LINE2'
                            glo:Select28 = SOUT:SFField8       !'LINE3'
                            !GLO:Select29 will be filled with SOP:Action
                            loc:LoyaltyStatus = SOUT:SFField9  !XMLTrack('LOYALTYSTATUS')
                        END !if error on read second line
                    END !if the first item on the header said ERROR
                END !if error on open

                Close(SiebelFile)
                !remove(SFileName)

            END !if filename exists

            !Final checks for errors
            if clip(Glo:Select20) = '' or clip(glo:Select20) = 'OK'

                If Glo:Select21 = 'N' then
                    glo:Select20 = 'NOT MIGRATED'    !Error code
                END

            END !if no error yet

            If clip(loc:LoyaltyStatus) <> '' then

                !Write something to the jobs data file
                Access:JOBSE3.clearkey(jobe3:KeyRefNumber)
                jobe3:RefNumber = SentJob
                if access:jobse3.fetch(jobe3:KeyRefNumber)
                    !not found create it
                    Access:jobse3.primerecord()
                    jobe3:RefNumber = SentJob
                END
                !now write the status into the file
                jobe3:LoyaltyStatus = loc:LoyaltyStatus
                Access:jobse3.update()

            END !if the loyalty status exists
        END !if setup did not work

        !Write something into the monitoring file
        Access:c3dmonit.primerecord()
        C3D:RefNumber = SentJob
        C3D:MSISDN    = SentMobile
        C3D:CallDate  = Today()
        C3D:CallTime  = clock()
        C3D:ErrorCode = glo:Select20
        Access:c3dmonit.update()

        !look up the required action AUTO/MANUAL/MANUAL PLUS/RETRY
        Access:SoapErrs.clearkey(SOP:KeyErrorCode)
        SOP:ErrorCode = glo:Select20
        if access:SoapErrs.fetch(SOP:KeyErrorCode)
            SOP:Action = 'ERROR NOT FOUND'
        END
        GLO:Select29 = SOP:Action

        !All "errors" must be logged in an exception file together with the MSISDN that was queried. The log file will contain the following information:
        If glo:Select20 <> 'OK' then

           !check if today's log file exists
            LFileName = clip(CustomerExceptionFolder)&'\MSISDN_Query_Exception_'&format(today(),@d10-)&'.csv'
           
            if ~exists(Lfilename) then

                !make a new one
                Create(LoggingFile)
                Share(LoggingFile)
                Lout:LFField1 = 'DATE'
                Lout:LFField2 = 'TIME'
                Lout:LFField3 = 'MOBILE'
                Lout:LFField4 = 'ERROR'
                Lout:LFField5 = 'FAULT'
                Lout:LFField6 = 'TYPE'
                Lout:LFField7 = 'DESCRIPTION'
                Lout:LFField8 = 'DETAILS'
                Add(LoggingFile)

            ELSE

                !already exists
                Share(LoggingFile)

            END

            Lout:LFField1 = format(Today(),@d06)
            Lout:LFField2 = format(Clock(),@t04)
            Lout:LFField3 = clip(SentMobile)
            Lout:LFField4 = clip(Glo:Select20)
            Lout:LFField5 = clip(SOP:SoapFault)
            Lout:LFField6 = clip(SOP:ErrorType)
            Lout:LFField7 = clip(SOP:Description)
            Lout:LFField8 = loc:ErrorDescription
            ADD(LoggingFile)
            Close(LoggingFile)

            !for some transactions failed an email must be sent to servicebase.support@vodacom.co.za with a copy of exception file.
            If SOP:Action = 'MANUAL PLUS' then

                !Email must be sent
                Access:SMSMAil.primerecord()
                !sms:RefNumber       = SentJob
                sms:SendToSMS       = 'F'
                sms:SendToEmail     = 'T'
                sms:SMSSent         = 'F'
                sms:EmailSent       = 'F'
                sms:DateInserted    = today()
                sms:TimeInserted    = clock()
                sms:EmailSubject    = clip(SOP:Subject)
                sms:EmailAddress    = 'servicebase.support@vodacom.co.za'
                sms:PathToEstimate  = clip(LFileName)
                sms:MSG             = clip(SOP:Body) &|
                                        '<13,10>Date: '&format(today(),@D06)&|
                                        '<13,10>Time: '&format(clock(),@t04)
                Access:SMSMail.update()
            END !if MANUAL PLUS

        END !if an error has occured

    END !if lookup requested

    !finished with the files
    Relate:JOBSE3.Close()
    Relate:C3DMONIT.Close()
    Relate:SOAPERRS.Close()
    Relate:SMSMAIL.Close()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
UpdateExchLocation   PROCEDURE  (Long SentJob,String SentUser,String SentLocation) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!TB13233 - J - 10/12/14 - adding exchange location and history
    VodacomClass.UpdateExchLocation(SentJob,SentUser,SentLocation)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
