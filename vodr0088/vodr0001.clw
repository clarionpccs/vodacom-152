

   MEMBER('vodr0088.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


VCPPerformanceReport PROCEDURE                        !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
tmp:Clipboard        ANY
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
TempFilePath         CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
JobsBooked           LONG
JobsOpen             LONG
                     END
tmp:Tag              STRING(1)
SummaryQueue         QUEUE,PRE(sumque)
HeadAccountNumber    STRING(30)
JobsBooked           LONG
                     END
BRW12::View:Browse   VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('VCP Performace Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,36,560,362),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('VCP Performance Report'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Booking Date From'),AT(255,122),USE(?tmp:StartDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(331,122,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(399,118),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Booking Date To'),AT(255,142),USE(?tmp:EndDate:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(331,142,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           BUTTON,AT(399,138),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT(''),AT(255,154),USE(?StatusText)
                           LIST,AT(214,160,252,186),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Browsing Records'),FORMAT('11L(2)|I@s1@67L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(472,190),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(472,228),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(472,268),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON('&Rev tags'),AT(111,267,5,5),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(119,287,5,5),USE(?DASSHOWTAG),HIDE
                           PROMPT('Ensure Excel is NOT running before you begin!'),AT(238,350,204,10),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(72,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
WriteLine               Procedure()
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                       String(2000)
                        End
                    End
ARCExportFile    File,Driver('BASIC'),Pre(arcexp),Name(glo:ARCExportFile),Create,Bindable,Thread
Record                  Record

                        End
                    End
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW12.UpdateBuffer
   glo:Queue.Pointer = sub:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sub:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = sub:Account_Number
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sub:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()
local:RecordCount       Long()
local:LineNumber        Long()
local:Desktop           CString(255)
Code
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'VCP Booking Performance Report'

    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    glo:ExportFile = Clip(local:LocalPath) & 'VCP' & Clock() & '.CSV'
    Remove(glo:ExportFile)
    Create(ExportFile)
    Open(ExportFile)

    Clear(SummaryQueue)
    Free(SummaryQueue)
    Access:JOBS.Clearkey(job:Date_Booked_Key)
    job:Date_Booked = tmp:StartDate
    Set(job:Date_Booked_Key,job:Date_Booked_Key)
    Loop ! Begin Loop
        If Access:JOBS.Next()
            Break
        End ! If Access:JOBS.Next()
        If job:Date_booked > tmp:EndDate
            Break
        End ! job:Date_booked > tmp:EndDate

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End ! tmp:Cancel

        ! Inserting (DBH 30/08/2006) # 8183 - Only include VCP jobs
        If job:Who_Booked <> 'WEB'
            Cycle
        End ! If job:Who_Booked <> 'WEB'
        ! End (DBH 30/08/2006) #8183

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
            !Found
! Changing (DBH 30/08/2006) # 8183 - Use Sub Account
!            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
!            tra:Account_Number = sub:Main_Account_Number
!            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!                !Found
!                glo:Pointer = tra:Account_Number
!                Get(glo:Queue,glo:Pointer)
!                If Error()
!                    Cycle
!                End ! Error()
!            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!                !Error
!            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
! to (DBH 30/08/2006) # 8183
            glo:Pointer = sub:Account_Number
            Get(glo:Queue,glo:POinter)
            If Error()
                Cycle
            End ! If Error()
! End (DBH 30/08/2006) #8183
        Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
            !Error
        End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
        
        local.WriteLine()
        local:RecordCount += 1

        sumque:HeadAccountNumber = sub:Account_Number
        Get(SummaryQueue,sumque:HeadAccountNumber)
        If Error()
            sumque:HeadAccountNumber = sub:Account_Number
            sumque:JobsBooked = 1
            Add(SummaryQueue)
        Else ! If Error()
            sumque:JobsBooked += 1
            Put(SummaryQueue)
        End ! If Error()
    End ! Loop
    Close(ExportFile)

    !_____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

!        ExcelSetup(0)
!        ExcelMakeWorkBook('** REPORT NAME **','','** REPORT NAME **')
!        ExcelSheetName('Summary')
!
!        Loop x# = 1 To Records(TempFileQueue)
!            Get(TempFileQueue,x#)
!            Do GetNextRecord2
!            If tmpque:Count = 0
!                Cycle
!            End !If tmpque:Count = 0
!            ExcelMakeSheet()
!            ExcelSheetName(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName))
!            ExcelSelectRange('A11')
!            ExcelCell('**COLUMN**',1)
!            ExcelAutoFilter('A11:Z11')
!            ExcelFreeze('B12')
!        End !Loop x# = 1 To Records(TempFileQueue)
!
!        ExcelSaveWorkBook(excel:FileName)
!        ExcelClose()
!
!        local.UpdateProgressWindow('Creating Excel Sheets...')
!        local.UpdateProgressWindow('')
!
        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        E1.OpenWorkBook(glo:ExportFile)
        E1.Copy('A1','AH' & local:RecordCount)
        tmp:Clipboard = ClipBoard()
        SetClipBoard('Blank')
        E1.CloseWorkBook(3)

        E1.NewWorkBook()
        E1.SelectCells('A12')
        SetClipBoard(tmp:Clipboard)
        E1.Paste()
        SetClipBoard('Blank')
        E1.RenameWorkSheet('VCP Performance')

        Do DrawTitle

        E1.SetCellBorders('A9','AG9',oix:BorderEdgeTop,oix:LineStyleContinuous)
        E1.SetCellBorders('A9','AG9',oix:BorderEdgeBottom,oix:LineStyleContinuous)
        E1.SetCellBorders('A9','AG9',oix:BorderEdgeLeft,oix:LineStyleContinuous)
        E1.SetCellBorders('A9','AG9',oix:BorderEdgeRight,oix:LineStyleContinuous)

        E1.SetCellBorders('A11','AG11',oix:BorderEdgeTop,oix:LineStyleContinuous)
        E1.SetCellBorders('A11','AG11',oix:BorderEdgeBottom,oix:LineStyleContinuous)
        E1.SetCellBorders('A11','AG11',oix:BorderEdgeLeft,oix:LineStyleContinuous)
        E1.SetCellBorders('A11','AG11',oix:BorderEdgeRight,oix:LineStyleContinuous)

        E1.WriteToCell('Section Name','A9')
        E1.WriteToCell('VCP Performance Report','B9')
        E1.WriteToCell('Total Records','E9')
        E1.WriteToCell(local:RecordCount,'F9')
        E1.WriteToCell('Showing','G9')
        E1.WriteToCell('=SUBTOTAL(2, A12:A' & local:RecordCount + 12,'H9')

        E1.WriteToCell('VCP Job Number','A11')
        E1.WriteToCell('Date Booked','B11')
        E1.WriteToCell('Account No','C11')
        E1.WriteToCell('Account Name','D11')
        E1.WriteToCell('Dealer ID','E11')
        E1.WriteToCell('Chargeable Charge Type','F11')
        E1.WriteToCell('Warranty Charge Type','G11')
        E1.WriteToCell('Make','H11')
        E1.WriteToCell('Model Number','I11')
        E1.WriteToCell('Unit Type','J11')
        E1.WriteToCell('IMEI','K11')
        E1.WriteToCell('MSN','L11')
        E1.WriteToCell('Exch Terms & Conditions Explained To Customer','M11')
        E1.WriteTocell('Number Of Days At PUP','N11')
        E1.WriteToCell('RRC Despatched To','O11')
        E1.WriteToCell('Date Despatched To RRC','P11')
        E1.WriteToCell('Time Despatched To RRC','Q11')
        E1.WriteToCell('PUP Waybill Number','R11')
        E1.WriteToCell('Date Received At RRC','S11')
        E1.WriteToCell('Time Received At RRC','T11')
        E1.WriteToCell('Days At RRC','U11')
        E1.WriteToCell('Date Sent To ARC','V11')
        E1.WriteToCell('Time Send To ARC','W11')
        E1.WriteToCell('RRC Waybill Number To ARC','X11')
        E1.WriteToCell('Date Received At ARC','Y11')
        E1.WriteToCell('Time Received At ARC','Z11')
        E1.WriteToCell('Date Despatched From ARC','AA11')
        E1.WriteToCell('Time Despatched From ARC','AB11')
        E1.WriteToCell('ARC Waybill Number','AC11')
        E1.WriteToCell('Date Received At RRC','AD11')
        E1.WriteToCell('Time Received At RRC','AE11')
        E1.WriteToCell('Date Despatched From RRC','AF11')
        E1.WriteToCell('Time Despatched From RRC','AG11')
        E1.WriteToCell('RRC Waybill Number','AH11')
        E1.SetCellFontName('Tahoma','A9','AH' & local:RecordCount + 12)
        E1.SetCellFontSize(8,'A1','AH' & local:RecordCount + 12)
        E1.SetCellFontStyle('Bold','A11','AH11')
        E1.SetCellFontStyle('Bold','A9')
        E1.SetCellFontStyle('Bold','F9')
        E1.SetCellFontStyle('Bold','H9')
        E1.SetCellBackgroundColor(color:Silver,'A9','AH9')
        E1.SetCellBackgroundColor(color:Silver,'A11','AH11')
        E1.SelectCells('J12','K' & local:RecordCount + 12)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,'J12','K' & local:RecordCount + 12)
        E1.SetColumnWidth('A','AH')
        E1.SelectCells('B12')
        E1.FreezePanes()

        E1.InsertWorkSheet()
        E1.RenameWorkSheet('Summary')

        Do DrawTitle

        E1.SetCellBorders('A9','E9',oix:BorderEdgeTop,oix:LineStyleContinuous)
        E1.SetCellBorders('A9','E9',oix:BorderEdgeBottom,oix:LineStyleContinuous)
        E1.SetCellBorders('A9','E9',oix:BorderEdgeLeft,oix:LineStyleContinuous)
        E1.SetCellBorders('A9','E9',oix:BorderEdgeRight,oix:LineStyleContinuous)
        E1.SetCellBorders('A10','E10',oix:BorderEdgeBottom,oix:LineStyleContinuous)
        E1.SetCellBorders('A10','E10',oix:BorderEdgeLeft,oix:LineStyleContinuous)
        E1.SetCellBorders('A10','E10',oix:BorderEdgeRight,oix:LineStyleContinuous)
        E1.SetCelLBackgroundColor(color:Silver,'A9','E10')

        E1.WriteToCell('Summary','A9')
        E1.WriteToCell('Account Number','A10')
        E1.WriteToCell('Account Name','B10')
        E1.WriteToCell('Dealer ID','C10')
        E1.WriteToCell('Jobs Booked','D10')
        E1.WriteToCell('Hit Rate %','E10')

        E1.SetCellFontStyle('Bold','A9','E10')

        local:LineNumber = 11
        Sort(SummaryQueue,sumque:HeadAccountNumber)
        Loop x# = 1 To Records(SummaryQueue)
            Get(SummaryQueue,x#)
            E1.WriteToCell(sumque:HeadAccountNumber,'A' & local:LineNumber)
! Changing (DBH 30/08/2006) # 8183 - Use sub account
!            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
!            tra:Account_Number = sumque:HeadAccountNumber
!            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!                !Found
!                E1.WriteToCell(tra:Company_Name,'B' & local:LineNumber)
!            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!                !Error
!                E1.WriteToCell('** UNKNOWN ACCOUNT **','B' & local:LineNumber)
!            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
! to (DBH 30/08/2006) # 8183
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = sumque:HeadACcountNumber
            If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Found
                E1.WriteToCell(sub:Company_Name,'B' & local:LineNumber)
                E1.WriteToCell(sub:DealerID,'C' & local:LineNumber)
            Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                !Error
                E1.WriteToCell('** UNKNOWN ACCOUNT **','B' & local:LineNumber)
                E1.WriteToCell('','C' & local:LineNumber)
            End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
! End (DBH 30/08/2006) #8183

            E1.WriteToCell(sumque:JobsBooked,'D' & local:LineNumber)
            E1.WriteToCell('=D' & local:LineNumber & '/' & local:RecordCount ,'E' & local:LineNumber)
            local:LineNumber += 1
        End ! Loop x# = 1 To Records(SummaryQueue)
        E1.WriteToCell(local:RecordCount,'D' & local:LineNumber)
        E1.WriteToCell('100%','E' & local:LineNumber)
        E1.SetCellBorders('A' & local:LineNumber,'E' & local:LineNumber,oix:BorderEdgeTop,oix:LineStyleContinuous)
        E1.SetCellBorders('A' & local:LineNumber,'E' & local:LineNumber,oix:BorderEdgeBottom,oix:LineStyleContinuous)
        E1.SetCellBorders('A' & local:LineNumber,'E' & local:LineNumber,oix:BorderEdgeLeft,oix:LineStyleContinuous)
        E1.SetCellBorders('A' & local:LineNumber,'E' & local:LineNumber,oix:BorderEdgeRight,oix:LineStyleContinuous)
        E1.SetCellBackgroundColor(color:Silver,'A' & local:LineNumber,'E' & local:LineNumber)
        E1.SetCellFontStyle('Bold','A' & local:LineNumber,'E' & local:LineNumber)
        E1.SetCellNumberFormat(oix:NumberFormatPercentage,,2,,'E11','E' & local:LineNumber)
        E1.SetCellFontName('Tahoma','A1','E' & local:LineNumber)
        E1.SetCellFontSize(8,'A1','E' & local:LineNumber)
        E1.SetColumnWidth('A','E')

        E1.SaveAs(excel:FileName)
        E1.CloseWorkBook(3)
        E1.Kill()

        ExcelSetup(0)
        ExcelOpenDoc(excel:FileName)
        ExcelSelectSheet('VCP Performance')
        ExcelAutoFilter('A11:AH11')
        ExcelAutoFit('A:AH')
        ExcelSelectSheet('Summary')
        ExcelSelectRange('A1')
        ExcelSaveWorkBook(excel:FileName)
        ExcelCLose()
!        Loop x# = 1 To Records(TempFileQueue)
!            Get(TempFileQueue,x#)
!            Do GetNextRecord2
!            Yield()
!            If tmpque:Count = 0
!                Cycle
!            End !If tmpque:Count = 0
!
!            local.UpdateProgressWindow('Writing Account: ' & Clip(tmpque:AccountNumber) & ' - ' & Clip(tmpque:CompanyName))
!
!            !Open account csv file. Copy the data (DBH: 22-03-2004)
!            E1.OpenWorkBook(Clip(tmpque:FileName))
!            E1.Copy('A1','Z' & tmpque:Count)
!
!            tmp:Clipboard = ClipBoard()
!            SetClipBoard('')
!            E1.CloseWorkBook(3)
!
!            !Open "final" document and paste the data (DBH: 22-03-2004)
!            E1.OpenWorkBook(excel:FileName)
!            E1.SelectWorkSheet(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName))
!            E1.SelectCells('A12')
!            SetClipBoard(tmp:ClipBoard)
!            E1.Paste()
!            E1.SelectCells('B12')
!            !MUST Clear the clipboard (DBH: 22-03-2004)
!            SetClipBoard('')
!            E1.CloseWorkBook(3)
!            local.UpdateProgressWindow('Done.')
!            local.UpdateProgressWindow('')
!        End !Loop x# = 1 To Records(TempFileQueue)
!        SetClipBoard('')
!        E1.OpenWorkBook(excel:FileName)
!    !_____________________________________________________________________
!
!    !SUMMARY
!    !_____________________________________________________________________
!
!        !Fill In Summary Sheet  (DBH: 03-03-2004)
!        local.UpdateProgressWindow('================')
!        local.UpdateProgressWindow('Building Summary..')
!        local.UpdateProgressWindow('')
!
!        E1.SelectWorkSheet('Summary')
!
!        Do DrawSummaryTitle
!    !_____________________________________________________________________
!
!    !FORMATTING
!    !_____________________________________________________________________
!
!        local.UpdatePRogressWindow('Finishing Off Formatting..')
!
!        Loop x# = 1 To Records(TempFileQueue)
!            Get(TempFileQueue,x#)
!            If tmpque:Count = 0
!                Cycle
!            End !If tmpque:Count = 0
!            Do GetNextRecord2
!            E1.SelectWorkSheet(Clip(tmpque:AccountNumber) & '-' & Clip(tmpque:CompanyName))
!            If tmpque:AccountNumber = tmp:HeadAccountNumber
!                tmp:ARCAccount = 1
!            Else !If tmpque:AccountNumber = tmp:HeadAccountNumber
!                tmp:ARCAccount = 0
!            End !If tmpque:AccountNumber = tmp:HeadAccountNumber
!            Do DrawDetailTitle
!
!            E1.SetCellFontSize(12,'A1')
!            E1.SetCellFontStyle('Bold','A1')
!            E1.WriteToCell('** REPORT NAME ** (' & Clip(tmpque:CompanyName) & ')'   ,'A1')
!
!            !Records and Records Shown (DBH: 22-03-2004)
!            E1.WriteToCell('=SUBTOTAL(2, A12:A' & tmpque:Count + 11,'E4')
!            E1.WriteToCell(tmpque:Count,'D4')
!
!
!            E1.SetCellFontStyle('Bold','A' & tmpque:Count + 12,'Z' & tmpque:Count + 13)
!            E1.SetCellFontStyle('Bold','K4','Z5')
!
!        End !Loop x# = 1 To Records(TempFileQueue)
!        SetClipBoard('')
!        E1.SelectWorkSheet('Summary')
!        E1.CloseWorkBook(3)
!        E1.Kill()
!
!
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)
    
    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    Else
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End
    Close(ProgressWindow)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawSummaryTitle        Routine   !Set Summary Screen (DBH: 22-03-2004)
    E1.SetColumnWidth('A','','20')
    E1.SetColumnWidth('B','Z','15')
    E1.WriteToCell('** Summary Title **')
    E1.SetCellFontStyle('Bold','A1')
    E1.SetCellFontSize(12,'A1')
    


    E1.WriteToCell('Jobs Completed From','A3')
    E1.WriteToCell(Format(tmp:StartDate,@d18),'B3')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B3')
    E1.WriteToCell('Jobs Completed To','A4')
    E1.WriteToCell(Format(tmp:EndDate,@d18),'B4')
    E1.SetCellNumberFormat(oix:NumberFormatDate,,,,'B4')
    E1.WriteTocell('Date Created','A5')
    E1.WriteToCell(Format(Today(),@d18),'B5')
    E1.SetCellFontStyle('Bold','B3','E5')

    !Colour
    Local.DrawBox('A1','E1','A1','E1',oix:Color15)
    Local.DrawBox('A3','E3','A5','E5',oix:Color15)

    E1.WriteToCell('** EXE NAME ** - ' & loc:Version  ,'D3')
DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell('VCP Performance Report', 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    E1.SetCellBorders('A1','D1',oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders('A1','D1',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders('A1','D1',oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders('A1','D1',oix:BorderEdgeRight,oix:LineStyleContinuous)

    E1.SetCellBorders('A3','D3',oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders('A7','D7',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders('A3','A7',oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders('D3','D7',oix:BorderEdgeRight,oix:LineStyleContinuous)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')
    





getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020653'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('VCPPerformanceReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:REPSCHAC.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:JOBSE2.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSCONS.UseFile
  Access:REPSCHCR.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:SUBTRACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5005'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW12.Q &= Queue:Browse
  BRW12.AddSortOrder(,sub:Account_Number_Key)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,sub:Account_Number,1,BRW12)
  BRW12.SetFilter('(Upper(sub:Region) <<> '''')')
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tmp:Tag,BRW12.Q.tmp:Tag)
  BRW12.AddField(sub:Account_Number,BRW12.Q.sub:Account_Number)
  BRW12.AddField(sub:Company_Name,BRW12.Q.sub:Company_Name)
  BRW12.AddField(sub:RecordNumber,BRW12.Q.sub:RecordNumber)
  BRW12.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              If rpc:AllAccounts
                  DO DASBRW::13:DASTAGALL
              Else ! If rpc:AllAccounts
                  Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
                  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
                  Loop ! Begin Loop
                      If Access:REPSCHAC.Next()
                          Break
                      End ! If Access:REPSCHAC.Next()
                      If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                      glo:Pointer = rpa:AccountNumber
                      Add(glo:Queue)
                  End ! Loop
  
              End ! If rpc:AllAccounts
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              If rpc:AllAccounts
                  Post(Event:Accepted,?DasTagAll)
              End ! If rpc:AllAccounts
  
  
              Do Reporting
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:REPSCHAC.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020653'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020653'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020653'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.WriteLine         Procedure() !Write CSV Files (DBH: 22-03-2004)
local:TheDate               Date()
local:TheTime               Time()

local:DateReceivedAtRRCFromPUP      Date()
local:TimeReceivedAtRRCFromPUP      Time()
local:DateDespatchedFromRRCToPUP    Date()
local:TimeDespatchedFromRRCToPUP    Time()
local:DateReceivedATARC             Date()
local:TimeREceivedATARC             Time()
local:DateDespatchedFromARC         Date()
local:TimeDespatchedFromARC         Time()
local:DateDespatchedToRRCFromPUP    Date()
local:TimeDespatchedToRRCFromPUP    Time()
local:DateDespatchedToPUPFromRRC    Date()
local:TimeDespatchedToPUPFromRRC    Time()
local:DateDespatchedFromPUP         Date()
local:TimeDespatchedFromPUP         Time()
local:DateReceivedATRRCFromARC      Date()
local:TimeReceivedAtRRCFromARC      Time()
Code
    ! Get the key date/times about the job (DBH: 03/07/2006)
    local:TheDate = ''
    local:TheTime = ''
    Access:AUDIT.ClearKey(aud:Action_Key)
    aud:Ref_Number = job:Ref_Number
    aud:Action     = 'UNIT RECEIVED AT RRC FROM PUP'
    Set(aud:Action_Key,aud:Action_Key)
    Loop
        If Access:AUDIT.NEXT()
           Break
        End !If
        If aud:Ref_Number <> job:Ref_Number      |
        Or aud:Action     <> 'UNIT RECEIVED AT RRC FROM PUP'      |
            Then Break.  ! End If
        If aud:Date < local:TheDate
            Cycle
        End ! If aud:Date < local:TheDate
        If aud:Time < local:TheTime
            Cycle
        End ! If aud:Time < local:TheTime
        local:TheDate = aud:Date
        local:TheTime = aud:Time
    End !Loop
    local:DateReceivedAtRRCFromPUP = local:TheDate
    local:TimeReceivedAtRRCFromPUP = local:TheTime

    local:TheDate = ''
    local:TheTime = ''
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFROM = 'RRC' and joc:DespatchTO = 'PUP'
            If joc:TheDate < local:TheDate
                Cycle
            End ! If job:TheDate < local:TheDate
            If joc:TheTime < local:TheTime
                Cycle
            End ! If joc:TheTime < local:TheTime
            local:TheDate = joc:TheDate
            local:TheTime = joc:TheTime
        End ! If joc:DespatchFROM = 'PUP' and job:DespatchTO = 'RRC'
    End !Loop
    local:DateDespatchedFromRRCToPUP = local:TheDate
    local:TimeDespatchedFromRRCToPUP = local:TheTime

    local:TheDate = ''
    local:TheTime = ''
    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
    lot:RefNumber   = job:Ref_Number
    lot:NewLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
    Set(lot:NewLocationKey,lot:NewLocationKey)
    Loop
        If Access:LOCATLOG.NEXT()
           Break
        End !If
        If lot:RefNumber   <> job:Ref_Number      |
        Or lot:NewLocation <> GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')      |
            Then Break.  ! End If
        If lot:TheDate < local:TheDate
            Cycle
        End ! If aud:Date < local:TheDate
        If lot:TheTime < local:TheTime
            Cycle
        End ! If aud:Time < local:TheTime
        local:TheDate = lot:TheDate
        local:TheTime = lot:TheTime
    End !Loop
    local:DateReceivedATARC = local:TheDate
    local:timeReceivedATARC = local:TheTime

    local:TheDate = ''
    local:TheTime = ''
    Access:AUDIT.ClearKey(aud:Action_Key)
    aud:Ref_Number = job:Ref_Number
    aud:Action     = 'DESPATCH FROM ARC'
    Set(aud:Action_Key,aud:Action_Key)
    Loop
        If Access:AUDIT.NEXT()
           Break
        End !If
        If aud:Ref_Number <> job:Ref_Number      |
        Or aud:Action     <> 'DESPATCH FROM ARC'      |
            Then Break.  ! End If
        If aud:Date < local:TheDate
            Cycle
        End ! If aud:Date < local:TheDate
        If aud:Time < local:TheTime
            Cycle
        End ! If aud:Time < local:TheTime
        local:TheDate = aud:Date
        local:TheTime = aud:Time
    End !Loop
    local:DateDespatchedFromARC = local:TheDate
    local:TimeDespatchedFromARC = local:TheTime

    local:TheDate = ''
    local:TheTime = ''
    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
    lot:RefNumber   = job:Ref_Number
    lot:NewLocation = GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
    Set(lot:NewLocationKey,lot:NewLocationKey)
    Loop
        If Access:LOCATLOG.NEXT()
           Break
        End !If
        If lot:RefNumber   <> job:Ref_Number      |
        Or lot:NewLocation <> GETINI('RRC','InTransitFromPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')      |
            Then Break.  ! End If
        If lot:TheDate < local:TheDate
            Cycle
        End ! If aud:Date < local:TheDate
        If lot:TheTime < local:TheTime
            Cycle
        End ! If aud:Time < local:TheTime
        local:TheDate = lot:TheDate
        local:TheTime = lot:TheTime
    End !Loop
    local:DateDespatchedFromRRCToPUP = local:TheDate
    local:TimeDespatchedFromRRCToPUP = local:TheTime

    local:TheDate = ''
    local:TheTime = ''

    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFROM = 'PUP' and joc:DespatchTO = 'CUSTOMER'
            If joc:TheDate < local:TheDate
                Cycle
            End ! If job:TheDate < local:TheDate
            If joc:TheTime < local:TheTime
                Cycle
            End ! If joc:TheTime < local:TheTime
            local:TheDate = joc:TheDate
            local:TheTime = joc:TheTime
        End ! If joc:DespatchFROM = 'PUP' and job:DespatchTO = 'RRC'
    End !Loop

    local:DateDespatchedFromPUP = local:TheDate
    local:TimeDespatchedFromPUP = local:TheTime

    local:TheDate = ''
    local:TheTime = ''
    Access:LOCATLOG.ClearKey(lot:NewLocationKey)
    lot:RefNumber   = job:Ref_Number
    lot:NewLocation = GETINI('RRC','InTransitToPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')
    Set(lot:NewLocationKey,lot:NewLocationKey)
    Loop
        If Access:LOCATLOG.NEXT()
           Break
        End !If
        If lot:RefNumber   <> job:Ref_Number      |
        Or lot:NewLocation <> GETINI('RRC','InTransitToPUPLocation',,Clip(Path()) & '\SB2KDEF.INI')      |
            Then Break.  ! End If
        If lot:TheDate < local:TheDate
            Cycle
        End ! If aud:Date < local:TheDate
        If lot:TheTime < local:TheTime
            Cycle
        End ! If aud:Time < local:TheTime
        local:TheDate = lot:TheDate
        local:TheTime = lot:TheTime
    End !Loop
    local:DateDespatchedToPUPFromRRC = local:TheDate
    local:TimeDespatchedToPUPFromRRC = local:TheTime

    local:TheDate = ''
    local:TheTime = ''
    AtARC# = False
    Access:LOCATLOG.ClearKey(lot:DateKey)
    lot:RefNumber = job:Ref_Number
    lot:TheDate   = job:Date_Booked
    Set(lot:DateKey,lot:DateKey)
    Loop
        If Access:LOCATLOG.NEXT()
           Break
        End !If
        If lot:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If AtARC# = True
            If lot:NewLocation = GETINI('RRC','RRCLocation',,Clip(Path()) & '\SB2KDEF.INI')
                ! First time at RRC after being at ARC (DBH: 03/07/2006)
                local:TheDate = lot:TheDate
                local:TheTIme = lot:TheTime
                Break
            End ! If lot:NewLocation = GETINI('RRC','RRCLocation',,Clip(Path()) & '\SB2KDEF.INI')
        End ! If AtARC# = True
        If lot:NewLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
            AtARC# = True
        Else ! If lot:NewLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
            Cycle
        End ! If lot:NewLocation = GETINI('RRC','ARCLocation',,Clip(Path()) & '\SB2KDEF.INI')
    End !Loop
    local:DateReceivedATRRCFromARC = local:TheDate
    local:TimeReceivedATRRCFromARC = local:TheTime

    ! ____________________________________________________________________________________
    ! Start Exporting (DBH: 03/07/2006)

    Clear(exp:Record)
    !Job Number
    exp:Line1 = '"' & job:Ref_Number
    !Date Booked
    exp:Line1    = Clip(exp:Line1) & '","' & Format(job:Date_Booked,@d06)
    !Head Account No
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(sub:Account_Number)
    !Head Account Name
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(sub:Company_Name)
    !Dealer ID
    exp:Line1   = Clip(exp:Line1) & '","' & clip(sub:DealerID)  ! #11815 ! New field (Bryan: 29/11/2010)
    !Chargeable Charge Type
    If job:Chargeable_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Charge_Type)
    Else ! If job:Chargeable_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' 
    End ! If job:Chargeable_Job = 'YES'
    !Warranty Charge Type
    If job:Warranty_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Warranty_Charge_Type)
    Else ! job:Warranty_Job = 'YES'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! job:Warranty_Job = 'YES'
    !Make
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Manufacturer)
    !Model Number
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Model_Number)
    !Unit Type
    exp:Line1   = Clip(exp:Line1) & '","' & Clip(job:Unit_Type)
    !IMEI
    exp:Line1   = Clip(exp:Line1) & '","''' & Clip(job:ESN)
    !MSN
    If job:MSN <> '' And job:MSN <> 'N/A'
        exp:Line1   = Clip(exp:Line1) & '","''' & Clip(job:MSN)
    Else ! If job:MSN <> '' And job:MSN <> 'N/A'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If job:MSN <> '' And job:MSN <> 'N/A'
    !Exchange Terms Explained To Customer
    If jobe2:ExchangeTerms
        exp:Line1   = Clip(exp:Line1) & '","' & Clip('YES')
    Else ! If jobe2:ExchangeTerms
        exp:Line1   = Clip(exp:Line1) & '","' & Clip('NO')
    End ! If jobe2:ExchangeTerms
    !Number Of Days At PUP
        ! Difference between date booked and despatch to customer (DBH: 03/07/2006)
    If local:DateDespatchedFromPUP <> ''
        Mins# = BHTimeDifference24Hr(job:Date_Booked,local:DateDespatchedFromPUP,job:Time_Booked,local:TimeDespatchedFromPUP)
        BHReturnDaysHoursMins(Mins#,D#,H#,M#)
        If D# > 0
            exp:Line1   = Clip(exp:Line1) & '","' & D#
        Else ! If D# > 0
            exp:Line1   = Clip(exp:Line1) & '","0'
        End ! If D# > 0
    Else ! If local:DateDespatchedFromPUP <> ''
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If local:DateDespatchedFromPUP <> ''
    
    !RRC Despatched To
    Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
    tra_ali:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Found
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(tra_ali:Company_Name)
    Else ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
        !Error
        exp:Line1   = Clip(exp:Line1) & '","' & Clip('')
    End ! If Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign
    !Date Despatched To RRC
        !Time Despatched To RRC
        !PUP Waybill Number
    Found# = False
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFROM = 'PUP' and joc:DespatchTO = 'RRC'
            exp:Line1   = Clip(exp:Line1) & '","' & Format(joc:TheDate,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Format(joc:TheTime,@t01)
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(joc:ConsignmentNumber)
            Found# = True
            Break
        End ! If joc:DespatchFROM = 'PUP' and job:DespatchTO = 'RRC'
    End !Loop
    If Found# = False
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Found# = False
    !Date Received At RRC
        !Time Received At RRC
    If local:DateReceivedAtRRCFromPUP <> ''
        exp:Line1   = Clip(exp:Line1) & '","' & Format(local:DateReceivedAtRRCFromPUP,@d06)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(Local:TimeReceivedAtRRCFromPUP,@t01)
    Else ! If local:TheDate <> ''
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If local:TheDate <> ''
    !Days At RRC
    If local:DateReceivedAtRRCfromPUP <> '' And local:DateDespatchedFromRRCToPUP <> ''
        Mins# = BHTimeDifference24Hr(local:DateReceivedATRRCFromPup,local:DateDespatchedFromRRCToPUP,|
                                    local:TimeReceivedATRRCFromPUP,local:TimeDespatchedFromRRCToPUP)
        BHReturnDaysHoursMins(Mins#,D#,H#,M#)
        If D# > 0
            exp:Line1   = Clip(exp:Line1) & '","' & D#
        Else ! If D# > 0
            exp:Line1   = Clip(exp:Line1) & '","0'
        End ! If D# > 0
    Else ! If local:DateReceivedAtRRCfromPUP <> '' And local:DateDespatchedFromRRCToPUP <> ''
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If local:DateReceivedAtRRCfromPUP <> '' And local:DateDespatchedFromRRCToPUP <> ''
    
    !Date Sent To ARC
        !Time Sent To ARC
    If jobe:HubRepairDate <> ''
        exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe:HubRepairDate,@d06)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe:HubRepairTime,@t01)
    Else ! If jobe:HubRepairDate <> ''
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If jobe:HubRepairDate <> ''
    !RRC Waybill Number
    Found# = False
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFROM = 'RRC' and joc:DespatchTO = 'ARC'
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(joc:ConsignmentNumber)
            Found# = True
            Break
        End ! If joc:DespatchFROM = 'PUP' and job:DespatchTO = 'RRC'
    End !Loop
    If Found# = False
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Found# = False
    !Date Received At ARC
        !Time Received At ARC
    If local:DateReceivedATARC <> ''
        exp:Line1   = Clip(exp:Line1) & '","' & Format(local:DateReceivedATARC,@d06)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(local:TimeReceivedATARC,@t01)
    Else ! If local:DateReceivedATARC <> ''
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If local:DateReceivedATARC <> ''
    !Date Despatched From ARC
        !Time Despatched From ARC
        !ARC Waybill Number
    Found# = False
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFROM = 'ARC' and joc:DespatchTO = 'RRC'
            exp:Line1   = Clip(exp:Line1) & '","' & Format(joc:TheDate,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Format(joc:TheTime,@t01)
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(joc:ConsignmentNumber)
            Found# = True
            Break
        End ! If joc:DespatchFROM = 'PUP' and job:DespatchTO = 'RRC'
    End !Loop
    If Found# = False
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Found# = False
    !Date Received AT RRC
        !Time Received AT RRC
    If local:DateReceivedAtRRCfromARC <> ''
        exp:Line1   = Clip(exp:Line1) & '","' & Format(local:DateReceivedAtRRCFromARC,@d06)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(local:TimeReceivedAtRRCFromARC,@t01)
    Else ! If local:DateReceivedAtRRCfromARC <> '''
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If local:DateReceivedAtRRCfromARC <> '''
    !Date Despatched From RRC
        !Time Despatched From RRC
        !RRC Waybill Number
    Found# = False
    Access:JOBSCONS.ClearKey(joc:DateKey)
    joc:RefNumber = job:Ref_Number
    Set(joc:DateKey,joc:DateKey)
    Loop
        If Access:JOBSCONS.NEXT()
           Break
        End !If
        If joc:RefNumber <> job:Ref_Number      |
            Then Break.  ! End If
        If joc:DespatchFROM = 'RRC' and joc:DespatchTO = 'PUP'
            exp:Line1   = Clip(exp:Line1) & '","' & Format(joc:TheDate,@d06)
            exp:Line1   = Clip(exp:Line1) & '","' & Format(joc:TheTime,@t01)
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(joc:ConsignmentNumber)
            Found# = True
            Break
        End ! If joc:DespatchFROM = 'PUP' and job:DespatchTO = 'RRC'
    End !Loop
    If Found# = False
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    End ! If Found# = False

    exp:Line1   = Clip(exp:Line1) & '"'
    Add(ExportFile)

Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sub:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue

!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
