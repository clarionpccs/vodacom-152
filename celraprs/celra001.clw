

   MEMBER('celraprs.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


FirstScreen PROCEDURE                                 !Generated from procedure template - Window

LocalRequest         LONG
tmp:VersionNumber    STRING(30)
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AppFrame             WINDOW('Rapid Return To Stock - ServiceBase 2000'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,162,192,94),USE(?Panel6),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Return To Stock'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(368,258),USE(?Button6),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(360,182),USE(?Status_Update:2),TRN,FLAT,LEFT,ICON('retnatp.jpg')
                       BUTTON,AT(312,216),USE(?JobAwaitingReturn),TRN,FLAT,LEFT,ICON('awaitp.jpg')
                       BUTTON,AT(252,182),USE(?Status_Update),TRN,FLAT,LEFT,ICON('retstop.jpg')
                       PANEL,AT(584,386,1,21),USE(?Panel2),BEVEL(0,0,02010H)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.open
        access:users.usefile
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
        access:users.close
    End
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if
    access:users.close
    access:logged.close
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020571'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, AppFrame, 1)  !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('FirstScreen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  !!Look for a redirection file
  !!set path and update procedure
  !g_path = GETINI('global','datapath','xx7fail8zz', clip(path()) & '\celweb.ini')
  !!message('g_path is ' & clip(g_path))
  !if clip(g_path)='xx7fail8zz' then
  !    !not using an ini file - let it go onto own checks
  !ELSE
  !    setpath (clip(g_path))
  !end
  
  !Show the version number on the bottom of the screen
  !Useful for screen prints
  Access:DEFAULTV.Open()
  Access:DEFAULTV.UseFile()
  Set(DEFAULTV)
  Access:DEFAULTV.Next()
  tmp:VersionNumber   = '   (Ver: ' & Clip(defv:VersionNumber) & ')'
  Access:DEFAULTV.Close()
  Relate:DEFAULTS.Open
  Relate:DEFAULTV.Open
  Relate:LOGGED.Open
  Relate:STATUS.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(AppFrame)
  SELF.Opened=True
      ! Close Splash Window (DBH: 24-05-2005)
      PUTINI('STARTING','Run',True,)
  ! Save Window Name
   AddToLog('Window','Open','FirstScreen')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  error# = 1
  if GLO:webJob then
      !glo:Password was set on enty to the passed variable - LogonUserPassword
      access:users.clearkey(use:password_key)
      use:password = glo:Password
      if access:users.fetch(use:password_key) = level:benign then error#=0.
      If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() then error#=1.
      
      !check the traders details
      a#=instring(',',ClarioNET:Global.Param2,1,1)
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = clip(ClarioNET:Global.Param2[1:a#-1])
      access:tradeacc.fetch(tra:account_number_key)
      If Clip(tra:password)<> ClarioNET:Global.Param2[a#+1:len(clip(ClarioNET:Global.Param2))]
          halt(0,'Incorrect Trade details')
      END
      ClarioNET:Global.Param2=clip(ClarioNET:Global.Param2[1:a#-1])
      !Send back the version number to the client
      set(defaultv)
      if access:defaultv.next()
          !error  - this is handled elsewhere ignored here
      ELSE
          !pass current version number back to client
          ClarioNET:CallClientProcedure('VERSIONSAVE',defv:VersionNumber)
      END
  
  ELSE
      Loop x# = 1 To Len(Clip(Command()))
          If Sub(Command(),x#,1) = '%'
              glo:Password = Clip(Sub(Command(),x#+1,30))
              Access:USERS.Clearkey(use:Password_Key)
              use:Password    = glo:Password
              If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Found
                  error# = 0
                  If use:RenewPassword <> 0 
                      If use:PasswordLastChanged = ''
                          use:PasswordLastChanged = Today()
                          Access:USERS.Update()
                      Else !If use:PasswordLastChanged = ''
                          If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() 
                              error#=1
                          End
                      End
                  End !If use:RenewPassword <> 0
              Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              
              Break
          End!If Sub(Command(),x#,1) = '%'
      End!Loop x# = 1 To Len(Comman())
  END !if GLO:WebJob
  
  If error# = 1
      Do Login
  End
  
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Do log_out
    Relate:DEFAULTS.Close
    Relate:DEFAULTV.Close
    Relate:LOGGED.Close
    Relate:STATUS.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','FirstScreen')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
         if glo:webJob
          ClarioNET:CallClientProcedure('CLIPBOARD','BREAK')   !'WM30SYS.exe' & ' %' & Clip(glo:Password))
          !Now log out
          access:users.clearkey(use:password_key)
          use:password = glo:password
          if access:users.fetch(use:password_key) = Level:Benign
              access:logged.clearkey(log:user_code_key)
              log:user_code = use:user_code
              log:time      = clip(ClarioNET:Global.Param3)   !glo:TimeLogged
              if access:logged.fetch(log:user_code_key) = Level:Benign
                  delete(logged)
              end !if
          end !if
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020571'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020571'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020571'&'0')
      ***
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?Status_Update:2
      ThisWindow.Update
      Return_To_Stock(1)
      ThisWindow.Reset
    OF ?JobAwaitingReturn
      ThisWindow.Update
      START(BrowseAwaitingReturn, 25000)
      ThisWindow.Reset
    OF ?Status_Update
      ThisWindow.Update
      Return_To_Stock(0)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Do Log_out
      Do Login
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
         AppFrame{Prop:Active}=1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Return_To_Stock PROCEDURE (func:NatStores)            !Generated from procedure template - Window

FilesOpened          BYTE
save_aud_id          USHORT,AUTO
save_loa_id          USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_xch_id          USHORT,AUTO
job_queue_temp       QUEUE,PRE(jobque)
Job_Number           LONG
Type                 STRING(4)
record_number        LONG
                     END
job_number_temp      REAL
engineer_temp        STRING(3)
engineer_name_temp   STRING(30)
old_status_temp      STRING(30)
Location_temp        STRING(30)
update_text_temp     STRING(100)
tmp:esn              STRING(16)
error_text_temp      STRING(60)
tmp:NewLocation      STRING(30)
tmp:Courier          STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Rapid Return Loan / Exchange Unit To Stock'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       LIST,AT(408,204,96,64),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('45L(2)|M~Unit Number~@s8@16L(2)|M~Type~@s4@'),FROM(job_queue_temp)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Return Loan / Exchange Unit To Stock'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Defaults'),USE(?Tab1)
                           PROMPT('Select the I.M.E.I. Number of the Loan/Exchange Unit you wish to make Available.'),AT(168,168,204,24),USE(?Prompt2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(168,204),USE(?tmp:esn:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(232,204,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Units Successfully Updated'),AT(404,168),USE(?Prompt5),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s60),AT(168,232,200,12),USE(error_text_temp),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ReturnExchangeUnit      Routine
Data
local:SecondExchangeUnit        Byte(0)
Code
    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:Password
    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !Found
        tmp:NewLocation = use:Location
    Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !Error
    End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign


    found# = 0
    set(defaults)
    access:defaults.next()
    save_xch_id = access:exchange.savefile()
    access:exchange.clearkey(xch:esn_only_key)
    xch:esn = tmp:esn
    set(xch:esn_only_key,xch:esn_only_key)
    loop
        if access:exchange.next()
           break
        end !if
        if xch:esn <> tmp:esn      |
            then break.  ! end if
            !Produce error if unit is already available.
            !And only allow those with access to continue restocking in that case - 3805 (DBH: 16-01-2004)
            If xch:Available = 'AVL'
                If SecurityCheck('RESTOCK AVAILABLE EXCHANGE')
                    Case Missive('The selected unit is already available.'&|
                      '<13,10>'&|
                      '<13,10>You do not have sufficient access to retock this unit again.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    jobque:record_number += 1
                    jobque:Job_Number    = xch:ref_number
                    jobque:type = 'EXCH'
                    Add(job_queue_temp)
                    Sort(job_queue_temp,-jobque:record_number)
                    Do EFailed_update
                    Exit
                Else !If SecurityCheck('RESTOCK AVAILABLE EXCHANGE')
                    Case Missive('The selected unit is already available in exchange stock.'&|
                      '<13,10>'&|
                      '<13,10>Are you sure you still want to restock it?','ServiceBase 3g',|
                                   'mquest.jpg','\No|/Yes') 
                        Of 2 ! Yes Button
                        Of 1 ! No Button
                            jobque:record_number += 1
                            jobque:Job_Number    = xch:ref_number
                            jobque:type = 'EXCH'
                            Add(job_queue_temp)
                            Sort(job_queue_temp,-jobque:record_number)
                            Do EFailed_update
                            Exit
                    End ! Case Missive
                End !If SecurityCheck('RESTOCK AVAILABLE EXCHANGE')
            End !If xch:Available = 'AVL'

            If xch:job_number <> ''
                access:jobs.clearkey(job:ref_number_key)
                job:ref_number  = xch:job_number
                If access:jobs.tryfetch(job:ref_number_key) = Level:Benign

                    !Error check where the unit was exchange,
                    !and see if that corresponds with where the user
                    !is trying to return it to -  (DBH: 05-12-2003)
                    Access:JOBSE.Clearkey(jobe:RefNumberKEy)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKEy) = Level:Benign
                        !Found

                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKEy) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKEy) = Level:Benign

                    !Is this the first or second exchange? -  (DBH: 05-01-2004)
                    If Jobe:SecondExchangeNumber > 0
                      If jobe:SecondExchangeNumber = xch:Ref_Number
                          local:SecondExchangeUnit = True
                          If func:NatStores <> 1
                              Case Missive('You are attempting to return a second exchange unit.'&|
                                '<13,10>'&|
                                '<13,10>This must be returned to national stores.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Exit
                          End !If func:NatStores <> 1
                       ELSE
                         If local:SecondExchangeUnit = False AND func:NatStores = 1
                              Case Missive('The selected unit was exchanged at the ARC and should be returned to ARC stock.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                              Exit
                         End!Case MessageEx
                       END
                    Else !If jobe:SecondExchangeNumber = xch:Ref_Number
                        If jobe:ExchangedATRRC
                            If func:NatStores <> 1
                                Case Missive('The selected unit was exchange at the RRC and should be returned to national stores.'&|
                                  '<13,10>'&|
                                  '<13,10>Are you sure you want to return it to ARC stock?','ServiceBase 3g',|
                                               'mquest.jpg','\No|/Yes') 
                                    Of 2 ! Yes Button
                                    Of 1 ! No Button
                                        Exit
                                End ! Case Missive
                            End !If func:NatStores <> 0
                        Else !If jobe:ExchangedATRRC And func:NatStores = 1
                            If func:NatStores = 1
                                Case Missive('The selected unit was exchanged at the ARC and should be returned to ARC stock.'&|
                                  '<13,10>'&|
                                  '<13,10>Are you sure you want to return it to national stores?','ServiceBase 3g',|
                                               'mquest.jpg','\No|/Yes') 
                                    Of 2 ! Yes Button
                                    Of 1 ! No Button
                                        Exit
                                End ! Case Missive
                            End !If func:NatStores = 1

                        End !If jobe:ExchangedATRRC And func:NatStores = 1
                    End !If jobe:SecondExchangeNumber = xch:Ref_Number

                    !Amended by NB 9/12/2003
                    !original Code!
                    ! ------ job:despatched = 'YES'
                    !new Code:

                    !If Returning Second Exchange Unit, then
                    !don't do anything nasty to the job -  (DBH: 05-01-2004)
                    If local:SecondExchangeUnit = False

                        IF job:Despatch_Type = 'EXC' AND jobe:Engineer48HourOption = 1
                          !Do Nothing to the despatch!
                        ELSE
                          job:despatched = 'YES'
                        END


                        If job:Chargeable_Job = 'YES'
                            !Call the despatch screen, so the user can invoice the job
                            DespatchInvoice(1)
                        End !If job:Chargeable_Job = 'YES'

                        GetStatus(815,1,'JOB') !returned to exchange stock

                        access:jobs.update()
                    Else !If local:SecondExchangeUnit = True
                        GetStatus(815,1,'2NE')
                        Access:JOBSE.Update()
                    End !If local:SecondExchangeUnit = True

                End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                xch:job_number  = ''
            End!If xch:job_number <> ''

            !Revert the location of this unit to the user's engineer

            !Revert it to National Stores if they have chosen
            !that button, otherwise return to the engineer's location -  (DBH: 10-11-2003)
            If func:NatStores = 1
                !Added by NB 0-01-04 - take Page 7 Line 2.3 into account.
                !If jobe:SecondExchangeNumber > 0 AND local:SecondExchangeUnit = False
                !  xch:Location =  tmp:NewLocation
                !ELSE
                xch:Location    = 'MAIN STORE'

                !END

            Else !If func:NatStores = 1
                xch:Location    = tmp:NewLocation
            End !If func:NatStores = 1

            !Log 3777 DBH 9/1/04
            If local:SecondExchangeUnit = True
                !We are returning the second unit. Find the first exchange unit on the job
                !and change the stock type to that one. DBH 9/11/04
                IF job:Exchange_Unit_Number <> 0
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.ClearKey(aud:Action_Key)
                    aud:Ref_Number = job:Ref_Number
                    aud:Action     = 'EXCHANGE UNIT ATTACHED TO JOB'
                    aud:Date       = Today()
                    Set(aud:Action_Key,aud:Action_Key)
                    Loop
                        If Access:AUDIT.NEXT()
                           Break
                        End !If
                        If aud:Ref_Number <> job:Ref_Number      |
                        Or aud:Action     <> 'EXCHANGE UNIT ATTACHED TO JOB'      |
                            Then Break.  ! End If

                        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                        aud2:AUDRecordNumber = aud:Record_Number
                        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                            If Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),aud2:Notes,1,1)
                                x# = Instring('STOCK TYPE: ',aud2:Notes,1,1)
                                If x# <> 0
                                    xch:Stock_Type = Sub(aud2:Notes,x# + 12,30)
                                End !If x# <> 0
                            End !If Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),aud:Notes,1,1)

                        END ! IF

                    End !Loop
                    Access:AUDIT.RestoreFile(Save_aud_ID)
                End !IF job:Exchange_Unit_Number <> 0
            Else !If local:SecondExchangeUnit = True
                !We are returning the first exchange. If there was a second
                !exchange. Find that one and change the stock type to that. DBH: 9/1/04
                IF jobe:SecondExchangeNumber <> 0
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.ClearKey(aud:Action_Key)
                    aud:Ref_Number = job:Ref_Number
                    aud:Action     = 'SECOND EXCHANGE UNIT ATTACHED TO JOB'
                    aud:Date       = Today()
                    Set(aud:Action_Key,aud:Action_Key)
                    Loop
                        If Access:AUDIT.NEXT()
                           Break
                        End !If
                        If aud:Ref_Number <> job:Ref_Number      |
                        Or aud:Action     <> 'SECOND EXCHANGE UNIT ATTACHED TO JOB'      |
                            Then Break.  ! End If
                        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                        aud2:AUDRecordNumber = aud:Record_Number
                        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                            If Instring('UNIT NUMBER: ' & Clip(jobe:SecondExchangeNumber),aud2:Notes,1,1)
                                x# = Instring('STOCK TYPE: ',aud2:Notes,1,1)
                                If x# <> 0
                                    xch:Stock_Type = Sub(aud2:Notes,x# + 12,30)
                                End !If x# <> 0
                            End !If Instring('UNIT NUMBER: ' & Clip(job:Exchange_Unit_Number),aud:Notes,1,1)
                        END ! IF

                    End !Loop
                    Access:AUDIT.RestoreFile(Save_aud_ID)
                End!IF jobe:SecondExcangeNumber <> 0
            End !If local:SecondExchangeUnit = True
            ! Inserting (DBH 0106/2006) #6419 - Add the option to make a unit unavailable
            Case Missive('Do you want to make this unit Unavailable?','ServiceBase 3g',|
                           'mquest.jpg','\No|/Yes') 
                Of 2 ! Yes Button
                    If SecurityCheck('EXCHANGE - NOT AVAILABLE')
                        Case Missive('You do not have access to this option.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        jobque:record_number += 1
                        jobque:Job_Number    = xch:ref_number
                        jobque:type = 'EXCH'
                        Add(job_queue_temp)
                        Sort(job_queue_temp,-jobque:record_number)
                        Do EFailed_update
                        Exit
                    Else !SecurityCheck('EXCHANGE - NOT AVAILABLE')
                        xch:Available = 'NOA'
                        xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                        Access:EXCHANGE.Update()
                        get(exchhist,0)
                        if access:exchhist.primerecord() = Level:Benign
                            exh:ref_number      = xch:ref_number
                            exh:date            = Today()
                            exh:time            = Clock()
                            access:users.clearkey(use:password_key)
                            use:password        = glo:password
                            access:users.fetch(use:password_key)
                            exh:user = use:user_code
                            exh:status          = Clip(NotAvailableReason())
                            access:exchhist.insert()
                        end!if access:exchhist.primerecord() = Level:Benign
                    End !SecurityCheck('EXCHANGE - NOT AVAILABLE')
            ! End (DBH 01/06/2006) #6419
                Of 1 ! No Button
                    xch:available = 'AVL'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password =glo:password
                        access:users.fetch(use:password_key)
                        exh:user = use:user_code
                        exh:status        = 'UNIT AVAILABLE: RESTOCKED'
                        access:exchhist.insert()
                    end!if access:exchhist.primerecord() = level:benign
            End ! Case Missive

            access:exchange.update()

            If DEF:Use_Loan_Exchange_Label = 'YES'
                glo:select1  = xch:ref_number
                Exchange_Unit_Label
                glo:select1  = ''
            End!If DEF:Loan_Exchange_Label = 'YES'


            found#  = 1
            jobque:record_number += 1
            jobque:Job_Number    = xch:ref_number
            jobque:type = 'EXCH'
            Add(job_queue_temp)
            Sort(job_queue_temp,-jobque:record_number)
            Do Epassed_update

    !    End!If xch:available = 'RTS'
    end !loop
    access:exchange.restorefile(save_xch_id)

    If found# = 0
        found# = 0

        save_loa_id = access:loan.savefile()
        access:loan.clearkey(loa:esn_only_key)
        loa:esn = tmp:esn
        set(loa:esn_only_key,loa:esn_only_key)
        loop
            if access:loan.next()
               break
            end !if
            if loa:esn <> tmp:esn      |
                then break.  ! end if
        !    If loa:available = 'RTS'
                loa:available = 'AVL'
                !Paul 02/06/2009 Log No 10684
                loa:StatusChangeDate = today()
                get(loanhist,0)
                if access:loanhist.primerecord() = level:benign
                    loh:ref_number   = loa:ref_number
                    loh:date          = today()
                    loh:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    loh:user = use:user_code
                    loh:status        = 'UNIT AVAILABLE: RESTOCKED'
                    access:loanhist.insert()
                end!if access:loanhist.primerecord() = level:benign

                If loa:job_number <> ''
                    access:jobs.clearkey(job:ref_number_key)
                    job:ref_number  = loa:job_number
                    If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                        GetStatus(816,0,'LOA') !returned to loan stock

                        access:jobs.update()
                    End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                    loa:job_number  = ''
                End!If loa:job_number <> ''
                If DEF:Use_Loan_Exchange_Label = 'YES'
                    glo:select1  = loa:ref_number
                    loan_Unit_Label
                    glo:select1  = ''
                End!If DEF:Loan_Exchange_Label = 'YES'

                access:loan.update()

                found#  = 1
                jobque:record_number += 1
                jobque:Job_Number    = loa:ref_number
                jobque:type = 'LOAN'
                Add(job_queue_temp)
                Sort(job_queue_temp,-jobque:record_number)
                Do LPassed_update

        !    End!If loa:available = 'RTS'
        end !loop
        access:loan.restorefile(save_loa_id)

    End!If found# = 0
    If found# = 0
        Case Missive('Unable to find the selected I.M.E.I. number in the loan or exchange stock.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    End!If found# = 0
Epassed_update     Routine
    error_text_temp   = 'Exchange Unit: ' & Clip(xch:ref_number) & ' Updated Successfully.'
    ?error_text_temp{prop:fontcolor} = color:navy
    tmp:esn = ''
    Display()
    Select(?tmp:esn)

Lpassed_update     Routine
    error_text_temp   = 'Loan Unit: ' & Clip(loa:ref_number) & ' Updated Successfully.'
    ?error_text_temp{prop:fontcolor} = color:navy
    tmp:esn = ''
    Display()
    Select(?tmp:esn)

Efailed_update     Routine
    error_text_temp   = 'Exchange Unit: ' & Clip(xch:ref_number) & ' Update Failed.'
    ?error_text_temp{prop:fontcolor} = color:red
    tmp:Esn = ''
    Display()
    Select(?tmp:esn)

Lfailed_update     Routine
    error_text_temp   = 'Loan Unit: ' & Clip(loa:ref_number) & ' Update Failed.'
    ?error_text_temp{prop:fontcolor} = color:red
    tmp:Esn = ''
    Display()
    Select(?tmp:esn)
Waybill         Routine
Data
local:ARCAccountNumber  String(30)
local:WaybillNumber     Long()
Code
    local:ARCAccountNumber = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))

    local:WaybillNumber = NextWaybillNumber()

    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = local:WaybillNumber
    If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !Found
        way:AccountNumber = local:ARCAccountNumber
        way:WaybillID = 100 ! Exchange waybill
        way:FromAccount = local:ARCAccountNumber
        way:ToAccount = ''
        if Access:WAYBILLS.TryUpdate()
            !Access:WAYBILLS.CancelAutoInc()
        else
            ! Add items onto waybill
            Loop x# = 1 To Records(Job_Queue_Temp)
                Get(Job_Queue_Temp,x#)
                If Access:WAYITEMS.PrimeRecord() = Level:Benign
                    wai:WaybillNumber = way:WaybillNumber
                    wai:IsExchange  = True
                    wai:Ref_Number  = jobque:Job_Number
                    If Access:WAYITEMS.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:WAYITEMS.TryInsert() = Level:Benign
                        !Insert Failed
                        Access:WAYITEMS.CancelAutoInc()
                    End !If Access:WAYITEMS.TryInsert() = Level:Benign
                End !If Access:WAYITEMS.PrimeRecord() = Level:Benign
            End !Loop x# = 1 To Records(ProcessedUnitsQueue)
        end
    end

    WayBillExchange(local:ARCAccountNumber,'TRA',|
                    local:ARCAccountNumber,'DEF',|
                    local:WaybillNumber,tmp:Courier,0)




! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020573'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Return_To_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Relate:USERS_ALIAS.Open
  Relate:WAYBILLS.Open
  Relate:WAYITEMS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  Access:STATUS.UseFile
  Access:JOBS.UseFile
  Access:LOANHIST.UseFile
  Access:LOAN.UseFile
  Access:INVOICE.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  !fetch default courier
  tmp:courier = getini('DESPATCH','RETcourier','',clip(path())&'\SB2KDEF.INI')
  if tmp:courier = '' then
      Case Missive('A default courier has not been set-up for exchange transfers.'&|
        '<13,10>'&|
        '<13,10>To set this up, go to the option menu on the rapid exchange transfer screen.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  END
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Return_To_Stock')
  ?List1{prop:vcr} = TRUE
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
    Relate:USERS_ALIAS.Close
    Relate:WAYBILLS.Close
    Relate:WAYITEMS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Return_To_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020573'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020573'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020573'&'0')
      ***
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      Do ReturnExchangeUnit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      !Create a waybill for the National Store Transfers -  (DBH: 10-11-2003)
      If func:NatStores
          If Records(Job_Queue_Temp)
              Do Waybill
          End !If Records(Job_Queue_Temp)
      End !func:NatStores
      Post(Event:Closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

BrowseAwaitingReturn PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:WobNumber        LONG
tmp:exchangestatus   STRING(30)
FilesOpened          BYTE
Account_Number_temp  STRING(15)
tmp:rts              STRING('RTS')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Bouncer)
                       PROJECT(job:Current_Status)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
tmp:WobNumber          LIKE(tmp:WobNumber)            !List box control field - type derived from local data
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Bouncer            LIKE(job:Bouncer)              !List box control field - type derived from field
job:Current_Status     LIKE(job:Current_Status)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Jobs File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Job File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,112,344,184),USE(?Browse:1),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('0L(2)|M@p<<<<<<<<<<<<<<<<#p@44R(2)|M~Job Number~L@n-14@60L(2)|M~Account Number~@s15@120L' &|
   '(2)|M~Model Number~@s30@64L(2)|M~ESN/IMEI~@s16@120L(2)|M~Order Number~@s30@32L(2' &|
   ')|M~Bouncer~@s8@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Job Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14),AT(168,98,64,10),USE(tmp:WobNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(384,300),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(452,300),USE(?RemoveEntry),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020572'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseAwaitingReturn')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Relate:WEBJOB.Open
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  SELF.FilesOpened = True
  access:status.clearkey(sts:ref_number_only_key)
  sts:ref_number  = '707'
  If access:status.tryfetch(sts:ref_number_only_key) = Level:Benign
      tmp:ExchangeStatus  = Clip(sts:status)
  Compile('***',Debug=1)
      Message('tmp:exchangestatus: ' & tmp:exchangestatus,'Debug Message',icon:exclamation)
  ***
  End!If access:status.tryfetch(sts:ref_number_key) = Level:Benign
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseAwaitingReturn')
  ?Browse:1{prop:vcr} = TRUE
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,job:By_Status)
  BRW1.AddRange(job:Current_Status,tmp:exchangestatus)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job:Ref_Number,1,BRW1)
  BIND('tmp:WobNumber',tmp:WobNumber)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(tmp:WobNumber,BRW1.Q.tmp:WobNumber)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Bouncer,BRW1.Q.job:Bouncer)
  BRW1.AddField(job:Current_Status,BRW1.Q.job:Current_Status)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseAwaitingReturn')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = True
  If Request    <> Changerecord
      do_update# = False
  End!If Request    = Changerecord
  If do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJobs
    ReturnValue = GlobalResponse
  END
  End!If do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020572'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020572'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020572'&'0')
      ***
    OF ?RemoveEntry
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveEntry, Accepted)
      Case Missive('This should only be used in the event of an error.'&|
      '<13,10>'&|
      '<13,10>Are you sure you want to remove the selected entry from this list?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes') 
        Of 2 ! Yes Button
          
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw1.q.job:ref_number
              If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                  job:despatched = 'YES'
                  access:jobs.update()
              End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RemoveEntry, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tmp:WobNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:WobNumber, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:WobNumber, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  !change job/wob numbers
  
  if glo:WebJob then
      access:webjob.clearkey(wob:RefNumberKey)
      wob:RefNumber = Job:Ref_number
      if access:webjob.fetch(wob:RefNumberKey) then
          !error not found
          return(record:filtered)
      ELSE
          if wob:HeadAccountNumber = ClarioNET:Global.Param2 then
              tmp:WobNumber = wob:JobNumber&'-'&job:ref_number
          ELSE
              return(record:filtered)
          END
      END
  ELSE
      tmp:WobNumber = job:Ref_number
  END
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

XFiles PROCEDURE                                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','XFiles')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','XFiles')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

