

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01003.INC'),ONCE        !Local module procedure declarations
                     END



BrowseWaybConf PROCEDURE                              !Generated from procedure template - Browse

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGFLAG         BYTE(0)
DASBRW::10:TAGMOUSE        BYTE(0)
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LimitAccountNumber   STRING(30)
LocalTag             STRING(1)
Count                LONG
ShowDays             LONG
DifferentValue       LONG
LimitDate            DATE
BRW8::View:Browse    VIEW(WAYBCONF)
                       PROJECT(WAC:AccountNumber)
                       PROJECT(WAC:WaybillNo)
                       PROJECT(WAC:GenerateDate)
                       PROJECT(WAC:GenerateTime)
                       PROJECT(WAC:ConfirmationSent)
                       PROJECT(WAC:ConfirmResentQty)
                       PROJECT(WAC:ConfirmationReceived)
                       PROJECT(WAC:ConfirmDate)
                       PROJECT(WAC:ConfirmTime)
                       PROJECT(WAC:Delivered)
                       PROJECT(WAC:DeliverDate)
                       PROJECT(WAC:DeliverTime)
                       PROJECT(WAC:Received)
                       PROJECT(WAC:ReceiveDate)
                       PROJECT(WAC:ReceiveTime)
                       PROJECT(WAC:RecordNo)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
WAC:AccountNumber      LIKE(WAC:AccountNumber)        !List box control field - type derived from field
WAC:WaybillNo          LIKE(WAC:WaybillNo)            !List box control field - type derived from field
WAC:GenerateDate       LIKE(WAC:GenerateDate)         !List box control field - type derived from field
WAC:GenerateTime       LIKE(WAC:GenerateTime)         !List box control field - type derived from field
WAC:ConfirmationSent   LIKE(WAC:ConfirmationSent)     !List box control field - type derived from field
WAC:ConfirmationSent_Icon LONG                        !Entry's icon ID
WAC:ConfirmResentQty   LIKE(WAC:ConfirmResentQty)     !List box control field - type derived from field
WAC:ConfirmationReceived LIKE(WAC:ConfirmationReceived) !List box control field - type derived from field
WAC:ConfirmationReceived_Icon LONG                    !Entry's icon ID
WAC:ConfirmDate        LIKE(WAC:ConfirmDate)          !List box control field - type derived from field
WAC:ConfirmTime        LIKE(WAC:ConfirmTime)          !List box control field - type derived from field
WAC:Delivered          LIKE(WAC:Delivered)            !List box control field - type derived from field
WAC:Delivered_Icon     LONG                           !Entry's icon ID
WAC:DeliverDate        LIKE(WAC:DeliverDate)          !List box control field - type derived from field
WAC:DeliverTime        LIKE(WAC:DeliverTime)          !List box control field - type derived from field
WAC:Received           LIKE(WAC:Received)             !List box control field - type derived from field
WAC:Received_Icon      LONG                           !Entry's icon ID
WAC:ReceiveDate        LIKE(WAC:ReceiveDate)          !List box control field - type derived from field
WAC:ReceiveTime        LIKE(WAC:ReceiveTime)          !List box control field - type derived from field
WAC:RecordNo           LIKE(WAC:RecordNo)             !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(647,3,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(63,41,552,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Waybill Procedure'),AT(67,43),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(563,43),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(63,56,552,308),USE(?Panel3),FILL(09A6A7CH)
                       OPTION('Show Waybills generated in the last:'),AT(323,62,281,44),USE(ShowDays),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                         RADIO('One day'),AT(340,78),USE(?ShowDays:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                         RADIO('Different Number of days'),AT(451,90),USE(?ShowDays:Radio4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                         RADIO('Three Days'),AT(340,90),USE(?ShowDays:Radio2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('3')
                         RADIO('Seven Days'),AT(452,78),USE(?ShowDays:Radio3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('7')
                       END
                       BUTTON,AT(272,69),USE(?SelectButton),FLAT,HIDE,ICON('lookupp.jpg')
                       PROMPT('Account Number:'),AT(71,75),USE(?LimitAccountNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       ENTRY(@s30),AT(140,74,124,10),USE(LimitAccountNumber),DISABLE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                       ENTRY(@n-14),AT(564,88,36,12),USE(DifferentValue),FONT(,,,FONT:bold,CHARSET:ANSI)
                       LIST,AT(68,116,544,240),USE(?List),IMM,MSG('Browsing Records'),FORMAT('10R(2)|MI~T~L@s1@60R(2)|M~Account No~L@s30@56R(2)|M~Waybill No~L@n_14@[55R(2)|M~' &|
   'Date~L@d17@30R(2)|M~Time~L@t7@]|M~Generated~[10L(2)|MI@n3@20R(2)|M~Qty~L@n-14@]|' &|
   'M~Sent~[10L(2)|MI@n3@55R(2)|M~Date~L@d17@30L(2)|M~Time~@t7@]|M~Confirmed~[10R(2)' &|
   '|MI@n3@55R(2)|M~Date~L@d17@30R(2)|M~Time~L@t7@]|M~Delivered~[10R(2)|MI@n3@55R(2)' &|
   '|M~Date~L@d17@30R(2)|M~Time~L@t7@]|M~Received~'),FROM(Queue:Browse)
                       PANEL,AT(63,367,553,30),USE(?panelSellfoneButtons),FILL(09A6A7CH)
                       BUTTON,AT(64,368),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON,AT(544,368),USE(?ButtonClose),TRN,FLAT,ICON('closep.jpg')
                       BUTTON('&Rev tags'),AT(280,374,1,1),USE(?DASREVTAG),HIDE
                       STRING('All Tagged Lines'),AT(380,376),USE(?String1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(480,368),USE(?ButtonExport),FLAT,ICON('exportp.jpg')
                       BUTTON,AT(312,368),USE(?ButtonResubmit),FLAT,ICON('resubp.jpg')
                       BUTTON,AT(136,368),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON('sho&W tags'),AT(276,386,1,1),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(204,368),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = WAC:RecordNo
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = WAC:RecordNo
     ADD(glo:Queue,glo:Queue.Pointer)
    LocalTag = '*'
  ELSE
    DELETE(glo:Queue)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = '*')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = WAC:RecordNo
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::10:QUEUE = glo:Queue
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer = WAC:RecordNo
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = WAC:RecordNo
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020803'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseWaybConf')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:TRADEACC.Open
  Relate:WAYBCONF.Open
  Relate:WAYBILLS.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:WAYBCONF,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseWaybConf')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  if glo:webjob > 0 then
      LimitAccountNumber = ClarioNET:Global.Param2            !glo:Account_Number
      
  ELSE
      LimitAccountNumber = 'AA20'
      Unhide(?SelectButton)
  END
  
  ShowDays = 3
  LimitDate = today() - ShowDays
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,WAC:KeyAccountGenerateDateTime)
  BRW8.AddRange(WAC:AccountNumber,LimitAccountNumber)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,WAC:GenerateDate,1,BRW8)
  BRW8.SetFilter('(WAC:GenerateDate > LimitDate)')
  BIND('LocalTag',LocalTag)
  BIND('LimitDate',LimitDate)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(LocalTag,BRW8.Q.LocalTag)
  BRW8.AddField(WAC:AccountNumber,BRW8.Q.WAC:AccountNumber)
  BRW8.AddField(WAC:WaybillNo,BRW8.Q.WAC:WaybillNo)
  BRW8.AddField(WAC:GenerateDate,BRW8.Q.WAC:GenerateDate)
  BRW8.AddField(WAC:GenerateTime,BRW8.Q.WAC:GenerateTime)
  BRW8.AddField(WAC:ConfirmationSent,BRW8.Q.WAC:ConfirmationSent)
  BRW8.AddField(WAC:ConfirmResentQty,BRW8.Q.WAC:ConfirmResentQty)
  BRW8.AddField(WAC:ConfirmationReceived,BRW8.Q.WAC:ConfirmationReceived)
  BRW8.AddField(WAC:ConfirmDate,BRW8.Q.WAC:ConfirmDate)
  BRW8.AddField(WAC:ConfirmTime,BRW8.Q.WAC:ConfirmTime)
  BRW8.AddField(WAC:Delivered,BRW8.Q.WAC:Delivered)
  BRW8.AddField(WAC:DeliverDate,BRW8.Q.WAC:DeliverDate)
  BRW8.AddField(WAC:DeliverTime,BRW8.Q.WAC:DeliverTime)
  BRW8.AddField(WAC:Received,BRW8.Q.WAC:Received)
  BRW8.AddField(WAC:ReceiveDate,BRW8.Q.WAC:ReceiveDate)
  BRW8.AddField(WAC:ReceiveTime,BRW8.Q.WAC:ReceiveTime)
  BRW8.AddField(WAC:RecordNo,BRW8.Q.WAC:RecordNo)
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:TRADEACC.Close
    Relate:WAYBCONF.Close
    Relate:WAYBILLS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseWaybConf')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonResubmit
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonResubmit, Accepted)
      !update the records so they are resent
      Loop count = 1 to records(glo:Queue)
      
          get(Glo:Queue,count)
          Access:Waybconf.clearkey(WAC:KeyRecordNo)
          WAC:RecordNo = glo:Queue.GLO:Pointer
          If access:waybconf.fetch(WAC:KeyRecordNo) = level:Benign
              WAC:ConfirmationSent = 0
              access:Waybconf.update()
          END !if record found
      
      END !loop through glo:Queue
      
      !untag everything
      free(Glo:Queue)
      
      !update the display
      BRW8.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonResubmit, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020803'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020803'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020803'&'0')
      ***
    OF ?ShowDays
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ShowDays, Accepted)
      if ShowDays > 0 then
          LimitDate = today() - ShowDays
      ELSE
          LimitDate = today() - DifferentValue
      END
      brw8.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ShowDays, Accepted)
    OF ?SelectButton
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickHeadAccounts
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectButton, Accepted)
      LimitAccountNumber = tra:Account_Number
      display()
      BRW8.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SelectButton, Accepted)
    OF ?DifferentValue
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DifferentValue, Accepted)
      if ShowDays > 0 then
          LimitDate = today() - ShowDays
      ELSE
          LimitDate = today() - DifferentValue
      END
      brw8.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DifferentValue, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonClose
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonClose, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonClose, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonExport
      ThisWindow.Update
      CourierTATExport
      ThisWindow.Reset
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::10:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = WAC:RecordNo
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = '*')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END
  IF (wac:ConfirmationSent = true)
    SELF.Q.WAC:ConfirmationSent_Icon = 2
  ELSE
    SELF.Q.WAC:ConfirmationSent_Icon = 1
  END
  IF (WAC:ConfirmationReceived = true)
    SELF.Q.WAC:ConfirmationReceived_Icon = 2
  ELSE
    SELF.Q.WAC:ConfirmationReceived_Icon = 1
  END
  IF (wac:Delivered = true)
    SELF.Q.WAC:Delivered_Icon = 2
  ELSE
    SELF.Q.WAC:Delivered_Icon = 1
  END
  IF (wac:Received = true)
    SELF.Q.WAC:Received_Icon = 2
  ELSE
    SELF.Q.WAC:Received_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = WAC:RecordNo
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue

