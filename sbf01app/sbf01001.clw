

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01001.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Colour_Model PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
model_number_temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                       PROJECT(moc:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
moc:Model_Number       LIKE(moc:Model_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Colour File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Colour File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       ENTRY(@s30),AT(264,116,124,10),USE(moc:Colour),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       LIST,AT(264,130,148,192),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Colour~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,84,352,244),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Colour'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(420,128),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020320'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Colour_Model')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:MODELCOL.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MODELCOL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Colour_Model')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,moc:Colour_Key)
  BRW1.AddRange(moc:Model_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?MOC:Colour,moc:Colour,1,BRW1)
  BRW1.AddField(moc:Colour,BRW1.Q.moc:Colour)
  BRW1.AddField(moc:Record_Number,BRW1.Q.moc:Record_Number)
  BRW1.AddField(moc:Model_Number,BRW1.Q.moc:Model_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:MODELCOL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Colour_Model')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020320'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020320'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020320'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?moc:Colour
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?moc:Colour, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?moc:Colour, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Model_Colour PROCEDURE (f_model_number)        !Generated from procedure template - Window

CurrentTab           STRING(80)
model_number_temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                       PROJECT(moc:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
moc:Model_Number       LIKE(moc:Model_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Colour File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Colour File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       ENTRY(@s30),AT(264,108,124,10),USE(moc:Colour),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       LIST,AT(264,122,148,202),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Colour~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,84,352,244),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Colour'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(420,122),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020321'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Model_Colour')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MODELCOL.Open
  SELF.FilesOpened = True
  model_number_temp   = f_model_number
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MODELCOL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Model_Colour')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,moc:Colour_Key)
  BRW1.AddRange(moc:Model_Number,model_number_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?MOC:Colour,moc:Colour,1,BRW1)
  BRW1.AddField(moc:Colour,BRW1.Q.moc:Colour)
  BRW1.AddField(moc:Record_Number,BRW1.Q.moc:Record_Number)
  BRW1.AddField(moc:Model_Number,BRW1.Q.moc:Model_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELCOL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Model_Colour')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020321'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020321'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020321'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?moc:Colour
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?moc:Colour, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?moc:Colour, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

DoNotDelete PROCEDURE                                 !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DoNotDelete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EPSCSV.Open
  Relate:EPSIMP.Open
  Relate:EXPAUDIT.Open
  Relate:EXPBUS.Open
  Relate:EXPCITY.Open
  Relate:EXPGEN.Open
  Relate:EXPJOBS.Open
  Relate:EXPLABG.Open
  Relate:EXPPARTS.Open
  Relate:EXPSPARES.Open
  Relate:EXPWARPARTS.Open
  Relate:IMPCITY.Open
  Relate:PARAMSS.Open
  Relate:XREPACT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','DoNotDelete')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EPSCSV.Close
    Relate:EPSIMP.Close
    Relate:EXPAUDIT.Close
    Relate:EXPBUS.Close
    Relate:EXPCITY.Close
    Relate:EXPGEN.Close
    Relate:EXPJOBS.Close
    Relate:EXPLABG.Close
    Relate:EXPPARTS.Close
    Relate:EXPSPARES.Close
    Relate:EXPWARPARTS.Close
    Relate:IMPCITY.Close
    Relate:PARAMSS.Close
    Relate:XREPACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','DoNotDelete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Select_Model_Number PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
Manufacturer_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !List box control field - type derived from field
Manufacturer_Temp      LIKE(Manufacturer_Temp)        !Browse hot field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Model Number File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Model Number File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,114,248,210),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('124L(2)|M~Model Number~@s30@80L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(424,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Model Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,100,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Manufacturer'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,100,124,10),USE(mod:Model_Number,,?MOD:Model_Number:2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           COMBO(@s30),AT(296,100,124,10),USE(Manufacturer_Temp),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 2
FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020314'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Model_Number')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MODELNUM,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Select_Model_Number')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,mod:Manufacturer_Key)
  BRW1.AddRange(mod:Manufacturer,Manufacturer_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?MOD:Model_Number:2,mod:Model_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mod:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW1)
  BIND('Manufacturer_Temp',Manufacturer_Temp)
  BRW1.AddField(mod:Model_Number,BRW1.Q.mod:Model_Number)
  BRW1.AddField(mod:Manufacturer,BRW1.Q.mod:Manufacturer)
  BRW1.AddField(Manufacturer_Temp,BRW1.Q.Manufacturer_Temp)
  FDCB6.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(man:Manufacturer_Key)
  FDCB6.AddField(man:Manufacturer,FDCB6.Q.man:Manufacturer)
  FDCB6.AddField(man:RecordNumber,FDCB6.Q.man:RecordNumber)
  FDCB6.AddUpdateField(man:Manufacturer,Manufacturer_Temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Select_Model_Number')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          check_access('MODEL NUMBER - INSERT',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
      of changerecord
          check_access('MODEL NUMBER - CHANGE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
      of deleterecord
          check_access('MODEL NUMBER - DELETE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  end!if do_update# = true
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020314'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020314'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020314'&'0')
      ***
    OF ?Select:2
      ThisWindow.Update
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?Select:2, Accepted)
      glo:select1 = mod:model_number
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?Select:2, Accepted)
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mod:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Model_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Model_Number, Selected)
    OF ?MOD:Model_Number:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MOD:Model_Number:2, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MOD:Model_Number:2, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Internal_Mail PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
Sender_hold          STRING(40)
Recipient_Hold       STRING(20)
Email_Hold           STRING(20)
User_Name_Temp       STRING(60)
user_code_temp       STRING(3)
message_from_temp    STRING(40)
all_users_temp       STRING('NO {1}')
message_to_temp      STRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MESSAGES)
                       PROJECT(mes:Date)
                       PROJECT(mes:Time)
                       PROJECT(mes:Comment)
                       PROJECT(mes:Message_Memo)
                       PROJECT(mes:Record_Number)
                       PROJECT(mes:Read)
                       PROJECT(mes:Message_For)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mes:Date               LIKE(mes:Date)                 !List box control field - type derived from field
mes:Time               LIKE(mes:Time)                 !List box control field - type derived from field
message_to_temp        LIKE(message_to_temp)          !List box control field - type derived from local data
message_from_temp      LIKE(message_from_temp)        !List box control field - type derived from local data
mes:Comment            LIKE(mes:Comment)              !List box control field - type derived from field
mes:Message_Memo       LIKE(mes:Message_Memo)         !Browse hot field - type derived from field
mes:Record_Number      LIKE(mes:Record_Number)        !Primary key field - type derived from field
mes:Read               LIKE(mes:Read)                 !Browse key field - type derived from field
mes:Message_For        LIKE(mes:Message_For)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Internal Mail'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Internal Email File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       ENTRY(@s3),AT(136,78,28,10),USE(user_code_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                       ENTRY(@s60),AT(200,78,124,10),USE(User_Name_Temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                       CHECK('All Users'),AT(336,78),USE(all_users_temp),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                       LIST,AT(136,98,404,172),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('47R(2)|M~Date~L(0)@d6@27R(2)|M~Time~L(0)@t1@113L(2)|M~Message To~@s40@111L(2)|M~' &|
   'Message From~@s40@320L(2)|M~Comment~@s80@'),FROM(Queue:Browse:1)
                       BUTTON,AT(168,74),USE(?Lookup_Users),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(548,166),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       TEXT,AT(136,280,404,76),USE(mes:Message_Memo),SKIP,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                       BUTTON,AT(548,196),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(548,226),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(64,56,552,306),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('New Messages'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('All Messages'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'NO'
BRW1::Sort3:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'YES'
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'YES'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort2:StepClass CLASS(StepRealClass)            !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'NO'
Init                   PROCEDURE(BYTE Controls)
                     END

BRW1::Sort3:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'YES'
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'NO'
BRW1::Sort4:StepClass StepRealClass                   !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020313'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Internal_Mail')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MESSAGES.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MESSAGES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  access:users.clearkey(use:password_key)
  use:password =glo:password
  if access:users.fetch(use:password_key) = Level:Benign
     user_code_temp = use:user_code
     user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  end
  Display()
  ! Save Window Name
   AddToLog('Window','Open','Internal_Mail')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,mes:Read_Key)
  BRW1.AddRange(mes:Message_For,user_code_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,mes:Date,1,BRW1)
  BRW1::Sort3:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort3:StepClass,mes:Read_Only_Key)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(,mes:Read,1,BRW1)
  BRW1.SetFilter('(Upper(mes:read) = ''NO'')')
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,mes:Who_To_Key)
  BRW1.AddRange(mes:Message_For,user_code_temp)
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort4:StepClass,mes:Date_Only_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,mes:Date,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mes:Read_Key)
  BRW1.AddRange(mes:Message_For,user_code_temp)
  BIND('message_to_temp',message_to_temp)
  BIND('message_from_temp',message_from_temp)
  BRW1.AddField(mes:Date,BRW1.Q.mes:Date)
  BRW1.AddField(mes:Time,BRW1.Q.mes:Time)
  BRW1.AddField(message_to_temp,BRW1.Q.message_to_temp)
  BRW1.AddField(message_from_temp,BRW1.Q.message_from_temp)
  BRW1.AddField(mes:Comment,BRW1.Q.mes:Comment)
  BRW1.AddField(mes:Message_Memo,BRW1.Q.mes:Message_Memo)
  BRW1.AddField(mes:Record_Number,BRW1.Q.mes:Record_Number)
  BRW1.AddField(mes:Read,BRW1.Q.mes:Read)
  BRW1.AddField(mes:Message_For,BRW1.Q.mes:Message_For)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?all_users_temp{Prop:Checked} = True
    DISABLE(?Lookup_Users)
    DISABLE(?user_code_temp)
    DISABLE(?User_Name_Temp)
  END
  IF ?all_users_temp{Prop:Checked} = False
    ENABLE(?user_code_temp)
    ENABLE(?Lookup_Users)
    ENABLE(?User_Name_Temp)
  END
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MESSAGES.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Internal_Mail')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          check_access('INTERNAL EMAIL - INSERT',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of changerecord
          check_access('INTERNAL EMAIL - CHANGE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of deleterecord
          check_access('INTERNAL EMAIL - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMessages
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020313'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020313'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020313'&'0')
      ***
    OF ?user_code_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?user_code_temp, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browse_users
      if globalresponse = requestcompleted
          user_code_temp = use:user_code
          user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
          Display()
      end
      globalrequest     = saverequest#
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?user_code_temp, Accepted)
    OF ?all_users_temp
      IF ?all_users_temp{Prop:Checked} = True
        DISABLE(?Lookup_Users)
        DISABLE(?user_code_temp)
        DISABLE(?User_Name_Temp)
      END
      IF ?all_users_temp{Prop:Checked} = False
        ENABLE(?user_code_temp)
        ENABLE(?Lookup_Users)
        ENABLE(?User_Name_Temp)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_users_temp, Accepted)
      thiswindow.reset(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?all_users_temp, Accepted)
    OF ?Lookup_Users
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Users
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Users, Accepted)
      case globalresponse
          of requestcompleted
              user_code_temp = use:user_code
              user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
              select(?+2)
          of requestcancelled
              user_code_temp = ''
              user_name_temp = ''
              select(?-1)
      end!case globalreponse
      Display()
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Users, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'NO'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(all_users_temp) = 'YES'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'NO'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(all_users_temp) = 'YES'
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  access:users.clearkey(use:user_code_key)
  use:user_code = mes:message_from
  if access:users.fetch(use:user_code_key) = Level:Benign
     message_from_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  end
  access:users.clearkey(use:user_code_key)
  use:user_code = mes:message_for
  if access:users.fetch(use:user_code_key) = Level:Benign
     message_to_temp = Clip(use:forename) & ' ' & Clip(use:surname)
  end
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW1::Sort2:StepClass.Init PROCEDURE(BYTE Controls)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls))
  mes:read = 'NO'
  PARENT.Init(Controls)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 2, Init, (BYTE Controls))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateMessages PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
user_name_temp       STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mes:Record  LIKE(mes:RECORD),STATIC
QuickWindow          WINDOW('Update the MESSAGES File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Internal Message'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,244),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Message Date'),AT(202,149),USE(?MES:Date:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@d6),AT(278,149,60,10),USE(mes:Date),SKIP,RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           ENTRY(@t1),AT(342,149,60,10),USE(mes:Time),SKIP,RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           BUTTON,AT(306,165),USE(?Lookup_User),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('User Code'),AT(202,169),USE(?MES:Message_For:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s3),AT(278,169,24,10),USE(mes:Message_For),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           ENTRY(@s60),AT(338,169,124,10),USE(user_name_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Comment'),AT(202,189),USE(?MES:Comment:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s80),AT(278,189,200,10),USE(mes:Comment),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Message'),AT(202,209),USE(?MES:Message_Memo:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(278,209,200,48),USE(mes:Message_Memo),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Read Message'),AT(278,269),USE(mes:Read),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Message'
  OF ChangeRecord
    ActionMessage = 'Changing A Message'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020317'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMessages')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mes:Record,History::mes:Record)
  SELF.AddHistoryField(?mes:Date,2)
  SELF.AddHistoryField(?mes:Time,3)
  SELF.AddHistoryField(?mes:Message_For,4)
  SELF.AddHistoryField(?mes:Comment,6)
  SELF.AddHistoryField(?mes:Message_Memo,8)
  SELF.AddHistoryField(?mes:Read,7)
  SELF.AddUpdateFile(Access:MESSAGES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MESSAGES.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MESSAGES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If Thiswindow.request = InsertRecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      if access:users.fetch(use:password_key) = Level:Benign
         mes:message_from = use:user_code
      end
      mes:message_for = ''
  Else
      access:users.clearkey(use:user_code_key)
      use:user_code = mes:message_for
      if access:users.fetch(use:user_code_key) = Level:Benign
         user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
      end
  End!If Thiswindow.request = InsertRecord
  ! Save Window Name
   AddToLog('Window','Open','UpdateMessages')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MESSAGES.Close
    Relate:USERS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMessages')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mes:Date = Today()
    mes:Time = Clock()
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020317'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020317'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020317'&'0')
      ***
    OF ?Lookup_User
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Users
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_User, Accepted)
      case globalresponse
          of requestcompleted
              mes:message_for = use:user_code
              user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
              select(?+2)
          of requestcancelled
              mes:message_for = ''
              user_name_temp = ''
              select(?-1)
      end!case globalreponse
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_User, Accepted)
    OF ?mes:Message_For
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mes:Message_For, Accepted)
      access:users.clearkey(use:user_code_key)
      use:user_code = mes:message_for
      if access:users.fetch(use:user_code_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_users
          if globalresponse = requestcompleted
              mes:message_for = use:user_code
              user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
              Display()
          end
          globalrequest     = saverequest#
      Else
          user_name_temp = Clip(use:forename) & ' ' & Clip(use:surname)
      end!if access:users.fetch(use:user_code_key)
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mes:Message_For, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateJOBPAYMT PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
tmp:Contact_History_Action STRING(80)
locAction            STRING(30)
tmp:RememberTotal    REAL
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?jpt:Payment_Type
pay:Payment_Type       LIKE(pay:Payment_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(PAYTYPES)
                       PROJECT(pay:Payment_Type)
                     END
History::jpt:Record  LIKE(jpt:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBPAYMT File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Payment'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Payment Type'),AT(240,175,64,10),USE(?jpt:payment_type:prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(316,175,64,10),USE(jpt:Payment_Type),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Date'),AT(240,157,64,10),USE(?JPT:Date:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,157,64,10),USE(jpt:Date),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(384,153),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Payment Received'),AT(240,235,64,10),USE(?JPT:Amount:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(236,189,232,42),USE(?Credit_Card_Group),DISABLE,HIDE
                             PROMPT('Credit Card Number'),AT(240,194),USE(?JPT:Credit_Card_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s20),AT(316,194,124,10),USE(jpt:Credit_Card_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('Expiry Date'),AT(240,213),USE(?JPT:Expiry_Date:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@p##/##p),AT(316,213,28,10),USE(jpt:Expiry_Date),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('Issue Number'),AT(352,213),USE(?JPT:Issue_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s5),AT(408,213,40,10),USE(jpt:Issue_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           END
                           ENTRY(@n-14.2),AT(316,233,64,10),USE(jpt:Amount),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(168,300),USE(?Button4),TRN,FLAT,ICON('costsp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_jpt_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Credit_Card_Bit     Routine
! #12274 Don't want to see credit card bits (Bryan: 05/09/2011)
!    access:paytypes.clearkey(pay:payment_type_key)
!    pay:payment_type = jpt:payment_type
!    if access:paytypes.fetch(pay:payment_type_key) = Level:Benign
!        If pay:credit_card = 'YES'
!            Enable(?credit_card_group)
!            UnHide(?credit_card_group)
!            ?jpt:credit_card_number{prop:req} = 1
!            ?jpt:expiry_date{prop:req} = 1
!        Else
!            Disable(?credit_card_group)
!            Hide(?credit_card_group)
!            ?jpt:credit_card_number{prop:req} = 0
!            ?jpt:expiry_date{prop:req} = 0
!        End
!    end
!
!    Thiswindow.update()
!    display()
!    !thisMakeover.refresh()
!
!
!
!
!
Credit_Card_Bit_2     Routine
! #12274 Don't want to see credit card bits (Bryan: 05/09/2011)
!    If clip(jpt:Payment_Type) <> '' Then
!        !this is an edit
!        !NOTE - GLOBALREQUEST DIDNT WORK HERE
!        access:paytypes.clearkey(pay:payment_type_key)
!        pay:payment_type = jpt:payment_type
!        if access:paytypes.fetch(pay:payment_type_key) = Level:Benign
!            If pay:credit_card = 'YES'
!                Disable(?credit_card_group)
!                UnHide(?credit_card_group)
!                ?jpt:credit_card_number{prop:req} = 1
!                ?jpt:expiry_date{prop:req} = 1
!            Else
!                Disable(?credit_card_group)
!                Hide(?credit_card_group)
!                ?jpt:credit_card_number{prop:req} = 0
!                ?jpt:expiry_date{prop:req} = 0
!            End
!        end
!    End
!
!    Thiswindow.update()
!    display()
!    !thisMakeover.refresh()
Loan:Refund:Contact_History routine
    ! Contact history entry for loan unit refunds
    tmp:Contact_History_Action = 'REFUND MADE ON LOAN UNIT'
    do Loan:Contact_History
Loan:Deposit:Contact_History routine
    ! Contact history entry for loan unit deposits
    tmp:Contact_History_Action = 'DEPOSIT TAKEN ON LOAN UNIT'
    do Loan:Contact_History
Loan:Contact_History routine
    ! Add contact history entry for deposits/refunds
    if not Access:ContHist.PrimeRecord()        ! Insert new contact history record
        cht:Ref_Number  = glo:Select1
        cht:Date = Today()
        cht:Time = Clock()
        Access:Users.Clearkey(use:password_key)
        use:password = glo:password
        Access:Users.Fetch(use:password_key)
        cht:User = use:user_code
        cht:Action = tmp:Contact_History_Action
        cht:Notes = 'AMOUNT TAKEN: ' & format(jpt:Amount,@n14.2) & '<13,10>' & |
                    'PAYMENT TYPE: ' & clip(jpt:Payment_Type) & '<13,10>'
        Access:ContHist.TryUpdate()
    end
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Ask, ())
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Payment'
  OF ChangeRecord
    ActionMessage = 'Changing A Payment'
  END
  QuickWindow{Prop:Text} = ActionMessage
  case GLO:Select3
      of 'REFUND'
          ActionMessage = clip(ActionMessage) & ' (Refund)'       ! Update window title
          ?WindowTitle{Prop:Text} = ActionMessage
      of 'DEPOSIT'
          ActionMessage = clip(ActionMessage) & ' (Deposit)'      ! Update window title
          ?WindowTitle{Prop:Text} = ActionMessage
  end
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020590'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Ask, ())


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateJOBPAYMT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(jpt:Record,History::jpt:Record)
  SELF.AddHistoryField(?jpt:Payment_Type,4)
  SELF.AddHistoryField(?jpt:Date,3)
  SELF.AddHistoryField(?jpt:Credit_Card_Number,5)
  SELF.AddHistoryField(?jpt:Expiry_Date,6)
  SELF.AddHistoryField(?jpt:Issue_Number,7)
  SELF.AddHistoryField(?jpt:Amount,8)
  SELF.AddUpdateFile(Access:JOBPAYMT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:JOBPAYMT.Open
  Relate:PAYTYPES.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:USERS.UseFile
  Access:CONTHIST.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBPAYMT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
   if glo:webjob
      hide(?PopCalendar)
   end

  ! I have re-commented the fetch of the job record.
  ! Please do not uncomment this code as it will break the
  ! job booking screen (new inserted jobs will be cleared).
  ! You must make sure you have fetched the current job
  ! record and jobse record before calling this procedure.

  !!this was remmed but is needed to do a fetch of the full record
  ! access:jobs.clearkey(job:ref_number_key)
  ! job:ref_number = glo:select1
  ! if access:jobs.fetch(job:ref_number_key)
  !     Return(Level:Fatal)
  ! end
  !!up to here was remmed

    If thiswindow.request = Insertrecord
        access:users.clearkey(use:password_key)
        use:password =glo:password
        if access:users.fetch(use:password_key) = Level:Benign
           jpt:user_code = use:user_code
        end

        case GLO:Select3
            of 'REFUND'     ! Loan Refund - set amount to total loan deposit value ?
                jpt:amount = 0

                save_jpt_ali_id = Access:JobPaymt_Alias.SaveFile()
                Access:JobPaymt_Alias.ClearKey(jpt_ali:Loan_Deposit_Key)
                jpt_ali:Ref_Number = glo:select1
                jpt_ali:Loan_Deposit = True
                set(jpt_ali:Loan_Deposit_Key,jpt_ali:Loan_Deposit_Key)
                loop until Access:JobPaymt_Alias.Next()     ! Loop through loan deposit payments
                    if jpt_ali:Ref_Number <> glo:select1 then break.
                    if jpt_ali:Loan_Deposit <> True then break.
                    jpt:amount += jpt_ali:Amount
                end
                Access:JobPaymt_Alias.RestoreFile(save_jpt_ali_id)
                tmp:RememberTotal = jpt:Amount

            of 'DEPOSIT'    ! Loan Deposit - set default amount to zero
                jpt:amount = 0
            else            ! Standard Payment - set default to amount outstanding
                invoiced# = 0
                If job:Invoice_Number <> 0
                    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                    inv:Invoice_Number = job:Invoice_Number
                    If Access:INVOICE.Fetch(inv:Invoice_Number_Key) = Level:Benign
                        If inv:RRCInvoiceDate <> ''
                            invoiced# = 1
                        End !If inv:RRCInvoiceDate <> ''
                    End !If Access:INVOICE.Fetch(inv:Invoice_Number_Key) = Level:Benign
                End !If job:Invoice_Number <> 0
                If invoiced#
                    Total_Price('I',vat",total",balance")
                Else !If invoiced#
                    Total_Price('C',vat",total",balance")
                End !If invoiced#


                jpt:amount = balance"


                tmp:RememberTotal = jpt:amount
        end
    else
        case GLO:Select3
            of 'REFUND'     ! Loan Refund - set amount to total loan deposit value ?
                tmp:RememberTotal = 0

                save_jpt_ali_id = Access:JobPaymt_Alias.SaveFile()
                Access:JobPaymt_Alias.ClearKey(jpt_ali:Loan_Deposit_Key)
                jpt_ali:Ref_Number = glo:select1
                jpt_ali:Loan_Deposit = True
                set(jpt_ali:Loan_Deposit_Key,jpt_ali:Loan_Deposit_Key)
                loop until Access:JobPaymt_Alias.Next()     ! Loop through loan deposit payments
                    if jpt_ali:Ref_Number <> glo:select1 then break.
                    if jpt_ali:Loan_Deposit <> True then break.
                    tmp:RememberTotal += jpt_ali:Amount
                end
                Access:JobPaymt_Alias.RestoreFile(save_jpt_ali_id)

            of 'DEPOSIT'    ! Loan Deposit - set default amount to zero
                !nothing
            else            ! Standard Payment - set default to amount outstanding
                If job:Invoice_Number <> 0
                    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                    inv:Invoice_Number = job:Invoice_Number
                    If Access:INVOICE.Fetch(inv:Invoice_Number_Key) = Level:Benign
                        If inv:ARCInvoiceDate <> ''
                            invoiced# = 1
                        End !If inv:RRCInvoiceDate <> ''
                    End !If Access:INVOICE.Fetch(inv:Invoice_Number_Key) = Level:Benign
                End !If job:Invoice_Number <> 0
                If invoiced#
                    Total_Price('I',vat",total",balance")
                Else !If invoiced#
                    Total_Price('C',vat",total",balance")
                End !If invoiced#


                tmp:RememberTotal = balance"

        end
    End!If thiswindow.request = Insertrecord

  Do credit_card_bit_2
  ! Save Window Name
   AddToLog('Window','Open','UpdateJOBPAYMT')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(jpt:Payment_Type,?jpt:Payment_Type,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:PAYTYPES,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(pay:Payment_Type_Key)
  FDCB7.AddField(pay:Payment_Type,FDCB7.Q.pay:Payment_Type)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:JOBPAYMT.Close
    Relate:PAYTYPES.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateJOBPAYMT')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    jpt:Ref_Number = glo:select1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      !access:jobs.clearkey(job:Ref_Number_Key)
      !job:Ref_Number = jpt:Ref_Number
      !access:jobs.fetch(job:Ref_number_key)
      
      ! See comments in self.Init() before uncommenting the above code..
      
      if jobe:RefNumber <> job:Ref_Number     ! Is the JobSE record synch'ed to the job record
          Access:JOBSE.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:Ref_Number
          Access:JOBSE.Fetch(jobe:RefNumberKey)
      end
      
      Access:JOBSE.TryUpdate()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020590'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020590'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020590'&'0')
      ***
    OF ?jpt:Payment_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jpt:Payment_Type, Accepted)
      Do credit_card_bit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?jpt:Payment_Type, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          jpt:Date = TINCALENDARStyle1(jpt:Date)
          Display(?JPT:Date)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button4
      ThisWindow.Update
      Viewcosts
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF GLO:Select3 <> 'DEPOSIT' AND GLO:Select3 <> 'REFUND' AND jpt:Amount > tmp:RememberTotal THEN
      Case Missive('This amount exceeds the current outstanding balance. Please check the current balance and re-enter.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
     SELECT(?jpt:Amount)
     CYCLE
  END !IF
  !IF GLO:Select3 = 'REFUND' THEN
  !   IF jpt:Amount > 0 THEN jpt:Amount = -(jpt:Amount).
  !   IF tmp:RememberTotal + jpt:Amount < 0 THEN
  !      Case MessageEx('This amount exceeds the current outstanding balance. Please check the current balance and re-enter.','ServiceBase 2000',|
  !                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
  !      Of 1 ! &OK Button
  !      End!Case MessageEx
  !      SELECT(?jpt:Amount)
  !      CYCLE
  !   END !IF
  !END !IF
  If ThisWindow.Request = InsertRecord
  
      case GLO:Select3
          of 'REFUND'
              if jpt:Amount > 0 then jpt:Amount = -(jpt:Amount).
              do Loan:Refund:Contact_History
          of 'DEPOSIT'
              jpt:Loan_Deposit = True
              do Loan:Deposit:Contact_History
      end
  
      CASE glo:Select3
      OF 'REFUND'
          locAction = 'REFUND ISSUED'
      OF 'DEPOSIT'
          locAction = 'LOAN DEPOSIT'
      ELSE ! CASE
          locAction =  'PAYMENT RECEIVED'
      END ! CASE
  
      IF (AddToAudit(glo:Select1,'JOB',locAction,'PAYMENT TYPE: ' & Clip(jpt:Payment_Type) & |
                              '<13,10>AMOUNT: ' & Format(jpt:Amount,@n14.2)))
      END ! IF
  
  End !ThisWindow.Request = InsertRecord
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
      !Do credit_card_bit_2
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB7.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Payments PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
locAuditNotes        STRING(255)
tmp:LabourRate       REAL
tmp:PartsRate        REAL
save_jpt_id          USHORT,AUTO
FilesOpened          BYTE
Payment_Type_Temp    STRING('BOTH {8}')
grand_total          REAL
tmp:IssueRefund      BYTE(0)
tmp:LoanDeposit      BYTE(0)
save:Preview         STRING(1)
tmp:CreditAmount     REAL
tmp:TotalAmount      REAL
tmp:LastSuffix       STRING(1)
tmp:NextSuffix       STRING(1)
tmp:LastSuffixNumber LONG
tmp:JobNumber        LONG
BRW1::View:Browse    VIEW(JOBPAYMT)
                       PROJECT(jpt:Date)
                       PROJECT(jpt:Payment_Type)
                       PROJECT(jpt:User_Code)
                       PROJECT(jpt:Amount)
                       PROJECT(jpt:Record_Number)
                       PROJECT(jpt:Ref_Number)
                       PROJECT(jpt:Loan_Deposit)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jpt:Date               LIKE(jpt:Date)                 !List box control field - type derived from field
jpt:Payment_Type       LIKE(jpt:Payment_Type)         !List box control field - type derived from field
jpt:User_Code          LIKE(jpt:User_Code)            !List box control field - type derived from field
jpt:Amount             LIKE(jpt:Amount)               !List box control field - type derived from field
jpt:Record_Number      LIKE(jpt:Record_Number)        !Primary key field - type derived from field
jpt:Ref_Number         LIKE(jpt:Ref_Number)           !Browse key field - type derived from field
jpt:Loan_Deposit       LIKE(jpt:Loan_Deposit)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Payments'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Payment File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,114,276,182),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48L(2)|M~Date~@d6b@104L(2)|M~Payment Type~@s30@23L(2)|M~User~@s3@56R(2)|M~Paymen' &|
   't Received~L@n-14.2@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,84,352,244),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total Paid'),AT(168,100),USE(?grand_total:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(212,100,64,10),USE(grand_total),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON,AT(448,116),USE(?Button:CreateCreditNote),TRN,FLAT,HIDE,LEFT,ICON('creditp.jpg')
                           BUTTON,AT(448,145),USE(?Issue_Refund),TRN,FLAT,LEFT,ICON('issrefp.jpg')
                           BUTTON,AT(448,177),USE(?CreateInvoice),TRN,FLAT,LEFT,ICON('invoicep.jpg')
                           BUTTON,AT(448,220),USE(?Button:Costs),TRN,FLAT,ICON('costsp.jpg')
                           BUTTON,AT(240,298),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(168,298),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(312,298),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - tmp:LoanDeposit = 1
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CreditNote      Routine
Data
local:JobTotal      Real()
local:Amount        Real()
local:CreditSuffix  String(1)
local:InvoiceSuffix String(1)
local:NewInvoiceNumber String(30)
local:CreditRecordNumber Long()
local:HandlingFee       Real()
local:ExchangeRate      Real()
local:RRCLostLoanCharge Real()
local:PartsCost         Real()
local:ARCPartsCost      Real()
local:Paid              Real()
local:Refund            Real()
local:ARCCharge         Real()
local:ARCMarkup         Real()
local:ARCLocation       String(30)
Code

    ! Has it been invoiced?
    Error# = 0
    If job:Invoice_Number = 0
        Error# = 1
    Else ! If job:Invoice_Number = 0
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            If inv:RRCInvoiceDate = 0
                Error# = 1
            End ! If inv:RRCInvoiceDate = 0
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

    End ! If job:Invoice_Number = 0
    If Error# = 1
        Case Missive('The selected job has not been invoiced.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Exit
    End ! If Error# = 1

    ! Is there anything left to credit? (DBH: 23/07/2007)
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        !Found
        local:ARCLocation  = tra:SiteLocation
    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadAccountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    local:JobTotal = jobe:InvRRCCSubTotal + (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                        jobe:InvRRCCLabourCost * (inv:Vat_Rate_Labour/100) + |
                                        jobe:InvRRCCPartsCOst * (inv:Vat_Rate_Labour/100))

    local:Amount = 0
    Access:JOBSINV.Clearkey(jov:TypeRecordKey)
    jov:RefNumber = job:Ref_Number
    jov:Type = 'C'
    Set(jov:TypeRecordKey,jov:TypeRecordKey)
    Loop ! Begin Loop
        If Access:JOBSINV.Next()
            Break
        End ! If Access:JOBSINV.Next()
        If jov:RefNumber <> job:Ref_Number
            Break
        End ! If jov:RefNumber <> job:Ref_Number
        If jov:Type <> 'C'
            Break
        End ! If jov:Type <> 'C'
        local:Amount += jov:CreditAmount
    End ! Loop

    If local:Amount >= local:JobTotal
        Case Missive('There is nothing remaining to credit on the selected job.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Exit
    End ! If Amount$ >= jobe:InvRRCCSubTotal

    ! Find Exchange and Handing Values (DBH: 03/08/2007)
    local:ExchangeRate = 0
    local:HandlingFee = 0
    If job:Exchange_Unit_Number > 0
        If jobe:ExchangedAtRRC
            local:ExchangeRate = jobe:InvoiceExchangeRate
        Else ! If jobe:ExchangedAtRRC
            local:HandlingFee = jobe:InvoiceHandlingFee
        End ! If jobe:ExchangedAtRRC
    Else ! If job:Exchange_Unit_Number > 0
        If SentToHub(job:Ref_Number)
            local:HandlingFee = jobe:InvoiceHandlingFee
        End ! If SentToHub(job:Ref_Number)
    End ! If job:Exchange_Unit_Number > 0

    ! Find ARC Charge. Check if invoiced or not (DBH: 03/08/2007)
    If SentToHub(job:Ref_Number)
        If inv:ARCInvoiceDate > 0
            local:ARCCharge = job:Invoice_Courier_Cost + job:Invoice_Parts_Cost + job:Invoice_Labour_Cost + |
                                    job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                    job:Invoice_Parts_Cost * (inv:Vat_Rate_Parts/100) + |
                                    job:Invoice_Labour_Cost * (inv:Vat_Rate_Labour/100)
            local:ARCPartsCost = job:Invoice_Parts_Cost
        Else ! If inv:ARCInvoiceDate > 0
            local:ARCCharge = job:Courier_Cost + job:Labour_Cost + job:Parts_Cost + |
                                    (job:Courier_Cost * (VatRate(job:Account_Number,'L') /100)) + |
                                    (job:Parts_Cost * (VatRate(job:Account_Number,'P') /100)) +  |
                                    (job:Labour_Cost * (VatRate(job:Account_Number,'L')/100))
            local:ARCPartsCost = job:Parts_Cost
        End ! If inv:ARCInvoiceDate > 0
    Else ! If SentToHub(job:Ref_Number)
        local:ARCCharge = 0
        local:ARCPartsCost = 0
    End ! If SentToHub(job:Ref_Number)

    ! Lost Loan Charge (DBH: 03/08/2007)
    local:RRCLostLoanCharge = 0
    If LoanAttachedToJob(job:Ref_Number)
        If ~SentToHub(job:Ref_Number)
            local:RRCLostLoanCharge = job:Invoice_Courier_Cost
        End ! If ~SentToHub(job:Ref_Number)
    End ! If LoanAttached(job:Ref_Number)

    ! Get Parts Cost Price (DBH: 03/08/2007)
    local:PartsCost = 0
    Access:PARTS.Clearkey(par:Part_Number_Key)
    par:Ref_Number = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop ! Begin Loop
        If Access:PARTS.Next()
            Break
        End ! If Access:PARTS.Next()
        If par:Ref_Number <> job:Ref_Number
            Break
        End ! If par:Ref_Number <> job:Ref_Number
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:Location <> local:ARCLocation
                local:PartsCost += par:RRCAveragePurchaseCost
            End ! If sto:Location <> tmp:ARCLocation
        Else ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    End ! Loop

    ! Get ARC Markup (DBH: 03/08/2007)
    FixedPrice# = 0
    local:ARCMarkup = 0
    Access:CHARTYPE.ClearKey(cha:Warranty_Key)
    cha:Warranty    = 'NO'
    cha:Charge_Type = job:Charge_Type
    If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !Found
        If cha:Zero_Parts_ARC Or cha:Zero_Parts = 'YES'
            FixedPrice# = 1
        End !If cha:Zero_Parts_ARC Or cha:Zero_Parts = 'YES'
    Else !If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
        !Error
    End !If Access:CHARTYPE.TryFetch(cha:Warranty_Key) = Level:Benign
    If FixedPrice#
        local:ARCMarkup = jobe:InvRRCCPartsCost - local:ARCPArtsCost
        If local:ARCMarkup < 0
            local:ARCMarkup = 0
        End ! If local:ARCMarkup < 0
    End ! If FixedPrice#

    ! How much has been paid (DBH: 03/08/2007)
    local:Paid = 0
    local:Refund = 0
    Access:JOBPAYMT.Clearkey(jpt:All_Date_Key)
    jpt:Ref_Number = job:Ref_Number
    Set(jpt:All_Date_Key,jpt:All_Date_Key)
    Loop ! Begin Loop
        If Access:JOBPAYMT.Next()
            Break
        End ! If Access:JOBPAYMT.Next()
        IF jpt:Ref_Number <> job:Ref_Number
            Break
        End ! IF jpt:Ref_Number <> job:Ref_Number
        If jpt:Amount > 0
            local:Paid += jpt:Amount
        Else
            local:Refund += -jpt:Amount
        End ! If jpt:Amount > 0
    End ! Loop

! ____________________________________________________________________________________

    tmp:TotalAmount  = local:JobTotal - local:Amount
    tmp:CreditAmount = CreateCreditNote(tmp:TotalAmount)

    If tmp:CreditAmount > 0
        ! Find last credit note (DBH: 24/07/2007)
        tmp:LastSuffix = ''
        Found# = 0
        Access:JOBSINV.Clearkey(jov:TypeRecordKey)
        jov:RefNumber = job:Ref_Number
        jov:Type = 'C'
        jov:RecordNumber = 0
        Set(jov:TypeRecordKey,jov:TypeRecordKey)
        Loop ! Begin Loop
            If Access:JOBSINV.Next()
                Break
            End ! If Access:JOBSINV.Next()
            If jov:RefNumber <> job:Ref_Number
                Break
            End ! If jov:RefNumber <> job:Ref_Number
            If jov:Type <> 'C'
                Break
            End ! If jov:Type <> 'C'
            tmp:LastSuffix = jov:Suffix
            Found# = 1
        End ! Loop
        If tmp:LastSuffix = ''
            If Found# = 0
                tmp:NextSuffix = ''
            Else ! If Found# = 0
                tmp:NextSuffix = 'A'
            End ! If Found# = 0
        Else ! If tmp:LastSuffix = ''
            tmp:LastSuffixNumber = Val(tmp:LastSuffix)
            tmp:NextSuffix = Chr(tmp:LastSuffixNumber + 1)
        End ! If tmp:LastSuffix = ''

        If Access:JOBSINV.PrimeRecord() = Level:Benign
            Access:USERS.ClearKey(use:Password_Key)
            use:Password = glo:Password
            If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                !Found
            Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                !Error
            End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            jov:BookingAccount = wob:HeadAccountNumber
            jov:UserCode     = use:User_Code
            jov:Type         = 'C'
            jov:RefNumber    = job:Ref_Number
            jov:InvoiceNumber  = job:Invoice_Number
            jov:CreditAmount = tmp:CreditAmount
            jov:NewTotalCost = tmp:TotalAmount - tmp:CreditAmount
            jov:Suffix       = tmp:NextSuffix
            jov:OriginalTotalCost = local:JobTotal
            jov:NewInvoiceNumber = ''
            jov:ChargeType      = job:Charge_Type
            jov:RepairType      = job:Repair_Type
            jov:HandlingFee     = local:HandlingFee
            jov:ExchangeRate    = local:ExchangeRate
            jov:ARCCharge       = local:ARCCharge
            jov:RRCLostLoanCost = local:RRCLostLoanCharge
            jov:RRCPartsCost    = local:PartsCost
            jov:RRCPartsSelling = jobe:InvRRCCPartsCost
            jov:RRCLabour       = jobe:InvRRCCLabourCost
            jov:ARCMarkup       = local:ARCMarkup
            jov:RRCVat          = job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                    jobe:InvRRCCLabourCost * (inv:Vat_Rate_Labour/100) + |
                                    jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts/100)
            jov:Paid            = local:Paid
            jov:Outstanding     = local:JobTotal - local:Paid
            jov:Refund          = local:Refund

            If Access:JOBSINV.TryInsert() = Level:Benign
                !Insert
                local:CreditSuffix = jov:Suffix
                local:CreditRecordNumber = jov:RecordNumber
            Else ! If Access:JOBSINV.TryInsert() = Level:Benign
                Access:JOBSINV.CancelAutoInc()
            End ! If Access:JOBSINV.TryInsert() = Level:Benign
        End ! If Access.JOBSINV.PrimeRecord() = Level:Benign

        ! Find last Invoice Number (DBH: 24/07/2007)
        tmp:LastSuffix = ''
        Access:JOBSINV.Clearkey(jov:TypeRecordKey)
        jov:RefNumber = job:Ref_Number
        jov:Type = 'I'
        jov:RecordNumber = 0
        Set(jov:TypeRecordKey,jov:TypeRecordKey)
        Loop ! Begin Loop
            If Access:JOBSINV.Next()
                Break
            End ! If Access:JOBSINV.Next()
            If jov:RefNumber <> job:Ref_Number
                Break
            End ! If jov:RefNumber <> job:Ref_Number
            If jov:Type <> 'I'
                Break
            End ! If jov:Type <> 'C'
            tmp:LastSuffix = jov:Suffix
        End ! Loop
        If tmp:LastSuffix = ''
            tmp:NextSuffix = 'A'
        Else ! If tmp:LastSuffix = ''
            tmp:LastSuffixNumber = Val(tmp:LastSuffix)
            tmp:NextSuffix = Chr(tmp:LastSuffixNumber + 1)
        End ! If tmp:LastSuffix = ''

        If Access:JOBSINV.PrimeRecord() = Level:Benign
            Access:USERS.ClearKey(use:Password_Key)
            use:Password = glo:Password
            If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                !Found
            Else ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
                !Error
            End ! If Access:USERS.TryFetch(use:Password_Key) = Level:Benign
            jov:BookingAccount = wob:HeadAccountNumber
            jov:UserCode     = use:User_Code
            jov:Type         = 'I'
            jov:RefNumber    = job:Ref_Number
            jov:InvoiceNumber  = job:Invoice_Number
            jov:CreditAmount = tmp:CreditAmount
            jov:NewTotalCost = tmp:TotalAmount - tmp:CreditAmount
            jov:Suffix       = tmp:NextSuffix
            jov:OriginalTotalCost = local:JobTotal
            jov:NewInvoiceNumber = ''
            jov:ChargeType      = job:Charge_Type
            jov:RepairType      = job:Repair_Type
            jov:HandlingFee     = local:HandlingFee
            jov:ExchangeRate    = local:ExchangeRate
            jov:ARCCharge       = local:ARCCharge
            jov:RRCLostLoanCost = local:RRCLostLoanCharge
            jov:RRCPartsCost    = local:PartsCost
            jov:RRCPartsSelling = jobe:InvRRCCPartsCost
            jov:RRCLabour       = jobe:InvRRCCLabourCost
            jov:ARCMarkup       = local:ARCMarkup
            jov:RRCVat          = job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour/100) + |
                                    jobe:InvRRCCLabourCost * (inv:Vat_Rate_Labour/100) + |
                                    jobe:InvRRCCPartsCost * (inv:Vat_Rate_Parts/100)
            jov:Paid            = local:Paid
            jov:Outstanding     = local:JobTotal - local:Paid
            jov:Refund          = local:Refund
            If Access:JOBSINV.TryInsert() = Level:Benign
                !Insert
                local:InvoiceSuffix = jov:Suffix
                local:NewInvoiceNumber = Clip(jov:InvoiceNumber) & '-' & Clip(tra:BranchIdentification) & Clip(jov:Suffix)

                ! Record the associated invoice number on the credit note (DBH: 03/08/2007)
                Access:JOBSINV.ClearKey(jov:RecordNumberKey)
                jov:RecordNumber = local:CreditRecordNumber
                If Access:JOBSINV.TryFetch(jov:RecordNumberKey) = Level:Benign
                    !Found
                    jov:NewInvoiceNumber = local:NewInvoiceNumber
                    Access:JOBSINV.Update()
                Else ! If Access:JOBSINV.TryFetch(jov:RecordNumberKey) = Level:Benign
                    !Error
                End ! If Access:JOBSINV.TryFetch(jov:RecordNumberKey) = Level:Benign

            Else ! If Access:JOBSINV.TryInsert() = Level:Benign
                Access:JOBSINV.CancelAutoInc()
            End ! If Access:JOBSINV.TryInsert() = Level:Benign
        End ! If Access.JOBSINV.PrimeRecord() = Level:Benign

        ! glo:Select1 is used to filter the browse, so need to save it and then set it back (DBH: 01/10/2007)
        tmp:JobNumber = glo:Select1
        glo:Select1 = job:Invoice_Number

        VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'CN',local:CreditSuffix)  ! #11881 New Single Invoice (Bryan: 16/03/2011)

        VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'',local:InvoiceSuffix)  ! #11881 New Single Invoice (Bryan: 16/03/2011)

!        If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!            Vodacom_Delivery_Note_Web('CN',local:CreditSuffix)
!        Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!            Single_Invoice('CN',local:CreditSuffix)
!        End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!        If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!            Vodacom_Delivery_Note_Web('',local:InvoiceSuffix)
!        Else ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
!            Single_Invoice('',local:InvoiceSuffix)
!        End ! If GETINI('PRINTING','PrintA5Invoice',,Clip(Path()) & '\SB2KDEF.INI') = 1
        glo:Select1 = tmp:JobNumber
        If (local:JobTotal - local:Paid > 0) And local:Paid > 0 And local:Paid <> local:Refund
            Case Missive('There is a payment allocated to this job.'&|
              '||If necessary, a refund should be issued to the customer.','ServiceBase 3g',|
                           'mexclam.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
        End ! IF local:JobTotal - local:Paid > 0
    End ! If tmp:CreditAmount > 0

! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020322'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Payments')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DESBATCH.Open
  Relate:JOBPAYMT.Open
  Relate:STAHEAD.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:INVOICE.UseFile
  Access:USERS.UseFile
  Access:STATUS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSINV.UseFile
  Access:JOBS.UseFile
  Access:CHARTYPE.UseFile
  Access:STOCK.UseFile
  Access:PARTS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBPAYMT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Inserting (DBH 23/07/2007) # 9141 - Verify that the current account can do credit notes
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = glo:Select1
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
  If glo:WebJob
      CreditAllowed# = 1
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      sub:Account_Number = job:Account_Number
      If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Found
          If sub:Generic_Account = 1
              ! No Credit
              CreditAllowed# = 0
          Else ! If sub:Generic_Account = 1
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = Clarionet:Global.Param2
              If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:CompanyOwned = 1
                      !No Credit
                      CreditAllowed# = 0
                  End ! If tra:CompanyOwned = 1
              Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  !Error
              End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
          End ! If sub:Generic_Account = 1
      Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
          !Error
      End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
  
      If CreditAllowed# = 1
          Access:TRADEACC.ClearKey(tra:Account_Number_Key)
          tra:Account_Number = Clarionet:Global.Param2
          If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:AllowCreditNotes
                  ?Button:CreateCreditNote{prop:Hide} = 0
              End ! If tra:AllowCreditNotes
          Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      End ! If CreditAllowed# = 1
  End ! If glo:WebJob
  
  
  
  
  ! End (DBH 23/07/2007) #9141
  
  ! Save Window Name
   AddToLog('Window','Open','Browse_Payments')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,jpt:Loan_Deposit_Key)
  BRW1.AddRange(jpt:Ref_Number,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,jpt:Loan_Deposit,1,BRW1)
  BRW1.SetFilter('(jpt:Loan_Deposit = 1)')
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,jpt:All_Date_Key)
  BRW1.AddRange(jpt:Ref_Number,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,jpt:Date,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BIND('tmp:LoanDeposit',tmp:LoanDeposit)
  BRW1.AddField(jpt:Date,BRW1.Q.jpt:Date)
  BRW1.AddField(jpt:Payment_Type,BRW1.Q.jpt:Payment_Type)
  BRW1.AddField(jpt:User_Code,BRW1.Q.jpt:User_Code)
  BRW1.AddField(jpt:Amount,BRW1.Q.jpt:Amount)
  BRW1.AddField(jpt:Record_Number,BRW1.Q.jpt:Record_Number)
  BRW1.AddField(jpt:Ref_Number,BRW1.Q.jpt:Ref_Number)
  BRW1.AddField(jpt:Loan_Deposit,BRW1.Q.jpt:Loan_Deposit)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DESBATCH.Close
    Relate:JOBPAYMT.Close
    Relate:STAHEAD.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Payments')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  GLO:Select3 = ''
  
  if tmp:LoanDeposit
      GLO:Select3 = 'DEPOSIT'
  end
  
  if tmp:IssueRefund
      GLO:Select3 = 'REFUND'
      tmp:IssueRefund = False         ! Reset variable (button control)
  end
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBPAYMT
    ReturnValue = GlobalResponse
  END
  If Request = DeleteRecord
      If ReturnValue = 1
          locAuditNotes = 'PAYMENT AMOUNT: ' & Format(jpt:Amount,@n14.2) &|
                                  '<13,10>PAYMENT TYPE: ' & Clip(jpt:Payment_Type)
  
          If jpt:Credit_Card_Number <> ''
              locAuditNotes = Clip(locAuditNotes) & '<13,10>CREDIT CARD NO: ' & Clip(jpt:Credit_Card_Number) & |
                          '<13,10>EXPIRY DATE: ' & Clip(jpt:Expiry_Date) & |
                          '<13,10>ISSUE NO: ' & Clip(jpt:Issue_Number)
          End !If jpt:Credit_Card_Number <> ''
  
          IF (AddToAudit(job:ref_number,'JOB','PAYMENT DETAIL DELETED',locAuditNotes))
          END ! IF
  
  
  
      End !If ReturnValue = 1
  End !If Request = DeleteRecord
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Issue_Refund
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Issue_Refund, Accepted)
      If SecurityCheck('ISSUE REFUND')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('ISSUE REFUND')
          tmp:IssueRefund = True
          post(event:accepted,?Insert:2)
      End !SecurityCheck('ISSUE REFUND')
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Issue_Refund, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020322'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020322'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020322'&'0')
      ***
    OF ?Button:CreateCreditNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CreateCreditNote, Accepted)
      ! Inserting (DBH 23/07/2007) # 9141 - Create a credit note
      Do CreditNote
      ! End (DBH 23/07/2007) #9141
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CreateCreditNote, Accepted)
    OF ?CreateInvoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateInvoice, Accepted)
      If CanJobInvoiceBePrinted(1) = Level:Benign
          If CreateInvoice() = Level:Benign
              ! glo:Select1 is used for the browse filter. Make sure it's kept that way (DBH: 01/10/2007)
              tmp:JobNumber = glo:Select1
              glo:Select1  = job:Invoice_Number
              VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','') ! #11881 New Single Invoice (Bryan: 16/03/2011)
      
      !        IF glo:webjob
      !            Vodacom_Delivery_Note_Web('','')
      !        ELSE
      !            Single_Invoice('','')
      !        END
              glo:Select1  = tmp:JobNumber
          End !CreateInvoice()
      End !CanJobInvoiceBePrinted(1) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateInvoice, Accepted)
    OF ?Button:Costs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Costs, Accepted)
      ! Inserting (DBH 23/07/2007) # 9141 - Make the costs screen view only
      save:Preview = glo:Preview
      glo:Preview = 'V'
      ViewCosts
      glo:Preview = save:Preview
      ! End (DBH 23/07/2007) #9141
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Costs, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.ResetFromView PROCEDURE

grand_total:Sum      REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:JOBPAYMT.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    grand_total:Sum += jpt:Amount
  END
  grand_total = grand_total:Sum
  PARENT.ResetFromView
  Relate:JOBPAYMT.SetQuickScan(0)
  SETCURSOR()


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:LoanDeposit = 1
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)

Process_Minimum_Stock PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
save_shi_id          USHORT,AUTO
Days_7_Temp          LONG
Days_30_Temp         LONG
Days_60_Temp         LONG
Days_90_Temp         LONG
Average_Temp         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
quantity_temp        REAL
Average_text_temp    STRING(8)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sto:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::sto:Record  LIKE(sto:RECORD),STATIC
QuickWindow          WINDOW('Update the STOCK File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend The Minimum Stock Item'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,196,244),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('General Details'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Part Number'),AT(168,100),USE(?STO:Part_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(232,100,124,10),USE(sto:Part_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Description'),AT(168,116),USE(?STO:Description:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(232,116,124,10),USE(sto:Description),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Supplier'),AT(168,132),USE(?sto:supplier:prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(232,132,124,10),USE(sto:Supplier),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Purchase Cost'),AT(168,148),USE(?STO:Purchase_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(232,148,64,10),USE(sto:Purchase_Cost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Sale Cost'),AT(168,164),USE(?STO:Sale_Cost:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(232,164,64,10),USE(sto:Sale_Cost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Minimum Level'),AT(168,180),USE(?sto:minimum_level:prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<<#p),AT(232,180,64,10),USE(sto:Minimum_Level),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(0,99999999),STEP(1)
                           PROMPT('Make Up To '),AT(168,196),USE(?Prompt12),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<<<#p),AT(232,196,64,10),USE(sto:Reorder_Level),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(0,99999999),STEP(1)
                           PROMPT('Qty To Order'),AT(168,212),USE(?quantity_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@p<<<<<<#p),AT(232,212,64,10),USE(quantity_temp),RIGHT,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR,RANGE(0,9999999),STEP(1)
                           BUTTON,AT(168,250),USE(?Order),TRN,FLAT,LEFT,ICON('uporderp.jpg')
                           BUTTON,AT(244,250),USE(?Remove),TRN,FLAT,LEFT,ICON('updordp.jpg')
                         END
                       END
                       SHEET,AT(364,84,152,244),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Stock Details'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Quantity In Stock'),AT(368,100),USE(?Prompt9),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@N8),AT(465,100),USE(sto:Quantity_Stock),RIGHT,FONT(,8,,FONT:bold)
                           PROMPT('Quantity Awaiting Order'),AT(368,116),USE(?Prompt10),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@N8),AT(465,116),USE(sto:Quantity_To_Order),RIGHT,FONT(,8,,FONT:bold)
                           PROMPT('Quantity On Order'),AT(368,132),USE(?Prompt11),FONT(,8,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@N8),AT(465,132),USE(sto:Quantity_On_Order),RIGHT,FONT(,8,,FONT:bold)
                           PROMPT('Stock Usage'),AT(368,152),USE(?Prompt13),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('0 - 7 Days'),AT(368,164),USE(?Prompt14),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,164),USE(Days_7_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(368,176),USE(?Prompt14:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,176),USE(Days_30_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(368,188),USE(?Prompt14:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,188),USE(Days_60_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(368,200),USE(?Prompt14:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@n-14),AT(440,200),USE(Days_90_Temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(435,211,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(368,216),USE(?Prompt17),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(456,216,60,12),USE(Average_text_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,HIDE,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Minimum Stock Level Item'
  OF ChangeRecord
    ActionMessage = 'Minimum Stock Level Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020319'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Process_Minimum_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sto:Record,History::sto:Record)
  SELF.AddHistoryField(?sto:Part_Number,3)
  SELF.AddHistoryField(?sto:Description,4)
  SELF.AddHistoryField(?sto:Supplier,5)
  SELF.AddHistoryField(?sto:Purchase_Cost,6)
  SELF.AddHistoryField(?sto:Sale_Cost,7)
  SELF.AddHistoryField(?sto:Minimum_Level,18)
  SELF.AddHistoryField(?sto:Reorder_Level,19)
  SELF.AddHistoryField(?sto:Quantity_Stock,15)
  SELF.AddHistoryField(?sto:Quantity_To_Order,16)
  SELF.AddHistoryField(?sto:Quantity_On_Order,17)
  SELF.AddUpdateFile(Access:STOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:STOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  quantity_temp = STO:Reorder_Level - STO:Quantity_Stock
  If quantity_temp < 0
      quantity_temp = 0
  End!If quantity_temp < 0
  Display(?quantity_temp)
  
  StockUsage(sto:Ref_Number,Days_7_Temp,Days_30_Temp,Days_60_Temp,Days_90_Temp,Average_Temp)
  
  If average_temp < 1
      average_text_temp = '< 1'
  Else!If average_temp < 1
      average_text_temp = Int(average_temp)
  End!If average_temp < 1
  ! Save Window Name
   AddToLog('Window','Open','Process_Minimum_Stock')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(sto:Supplier,?sto:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:STOCK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Process_Minimum_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020319'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020319'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020319'&'0')
      ***
    OF ?Order
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order, Accepted)
      get(ordpend,0)
      if access:ordpend.primerecord() = level:benign
      
          ope:part_ref_number = sto:ref_number
          ope:part_type       = 'STO'
          ope:supplier        = sto:supplier
          ope:part_number     = sto:part_number
          ope:description     = sto:description
          ope:quantity        = quantity_temp
          access:ordpend.insert()
      
          sto:quantity_to_order += quantity_temp
          sto:pending_ref_number = ope:ref_number
          sto:minimum_stock = 'NO'
      end!if access:ordpend.primerecord() = level:benign
      Thiswindow.postcompleted()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order, Accepted)
    OF ?Remove
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove, Accepted)
      sto:minimum_stock = 'NO'
      thiswindow.postcompleted()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Minimum_Stock PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
pos                  STRING(255)
save_sto_id          USHORT,AUTO
FilesOpened          BYTE
Location_Temp        STRING(30)
save_ope_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Minimum_Level)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Minimum_Stock)
                       PROJECT(sto:Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Minimum_Level      LIKE(sto:Minimum_Level)        !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Primary key field - type derived from field
sto:Minimum_Stock      LIKE(sto:Minimum_Stock)        !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Minimum Stock File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Minimum Stock File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Location'),AT(68,60),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(112,60,124,10),USE(Location_Temp),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(68,110,436,248),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('105L(2)|M~Part Number~@s30@123L(2)|M~Description~@s30@106L(2)|M~Supplier~@s30@41' &|
   'R(2)|M~Min Level~@n8@50D(2)|M~Qty In Stock~L@N8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,158),USE(?Change),TRN,FLAT,LEFT,ICON('proitemp.jpg')
                       BUTTON,AT(548,190),USE(?ProcessAll),TRN,FLAT,LEFT,ICON('praitemp.jpg')
                       SHEET,AT(64,80,552,282),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Part Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(68,96,124,10),USE(sto:Part_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Description'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(68,96,124,10),USE(sto:Description),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass CLASS(StepStringClass)          !Default Step Manager
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort1:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 2
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020318'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Minimum_Stock')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Minimum_Stock')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  !Work Out The Minimum Stock
  Prog.ProgressSetup(Records(STOCK))
  
  save_sto_id = access:stock.savefile()
  set(sto:ref_number_key)
  loop
      if access:stock.next()
         break
      end !if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If Prog.InsideLoop()
  
      End ! If Prog.InsideLoop()
  
      If sto:sundry_item = 'YES'
          Cycle
      End!If sto:sundry_item = 'YES'
  
      sto:minimum_stock = 'NO'
      STO:Quantity_To_Order = 0                                                               !Go throught the pending order
      setcursor(cursor:wait)                                                                  !file. And work out the quantity
      save_ope_id = access:ordpend.savefile()                                                 !awaiting order
      access:ordpend.clearkey(ope:part_ref_number_key)
      ope:part_ref_number =  sto:ref_number
      set(ope:part_ref_number_key,ope:part_ref_number_key)
      loop
          if access:ordpend.next()
             break
          end !if
          if ope:part_ref_number <> sto:ref_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          sto:quantity_to_order += ope:quantity
      end !loop
      access:ordpend.restorefile(save_ope_id)
      setcursor()
  
  
      sto:quantity_on_order = 0                                                               !Go throught the order parts
      setcursor(cursor:wait)                                                                  !file. But check with the order
      save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
      access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
      orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
      set(orp:ref_number_key,orp:ref_number_key)                                              !up.
      loop
          if access:ordparts.next()
             break
          end !if
          if orp:part_ref_number <> sto:ref_number      |
              then break.  ! end if
          access:orders.clearkey(ord:order_number_key)
          ord:order_number = orp:order_number
          if access:orders.fetch(ord:order_number_key) = Level:Benign
              If ord:all_received <> 'YES'
                  If orp:all_received <> 'YES'
                      sto:quantity_on_order += orp:quantity
                  End!If orp:all_received <> 'YES'
              End!If ord:all_received <> 'YES'
          end!if access:orders.fetch(ord:order_number_key) = Level:Benign
      end !loop
      access:ordparts.restorefile(save_orp_id)
      setcursor()
  
  
      If (sto:quantity_stock + sto:quantity_to_order + sto:quantity_on_order) < sto:minimum_level
          sto:minimum_stock = 'YES'
      End!If (sto:quantity_stock + sto:quantity_to_order + sto:quantity_on_order) < sto:minimum_level
      access:stock.update()
  end !loop
  access:stock.restorefile(save_sto_id)
  setcursor()
  
  Set(defstock)
  access:defstock.next()
  If dst:use_site_location = 'YES'
      location_temp = dst:site_location
      Display(?location_temp)
  End
  
  Prog.ProgressFinish()
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,sto:Minimum_Description_Key)
  BRW1.AddRange(sto:Location,Location_Temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sto:Minimum_Part_Number_Key)
  BRW1.AddRange(sto:Location,Location_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Minimum_Level,BRW1.Q.sto:Minimum_Level)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Minimum_Stock,BRW1.Q.sto:Minimum_Stock)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB5.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(loc:Location_Key)
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='105L(2)|M~Part Number~@s30@#1#123L(2)|M~Description~@s30@#2#106L(2)|M~Supplier~@s30@#3#41R(2)|M~Min Level~@n8@#4#50D(2)|M~Qty In Stock~L@N8@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Minimum_Stock')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Process_Minimum_Stock
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020318'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020318'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020318'&'0')
      ***
    OF ?Location_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
    OF ?Change
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change, Accepted)
    OF ?ProcessAll
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAll, Accepted)
      Case Missive('If you continue all the parts displayed in this Minimum Stock Browse will be ordered.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              x# =  Missive('Do you want to create orders for the SELECTED location ' & Clip(Location_Temp) & ' or for ALL Location?','ServiceBase 3g',|
                             'mquest.jpg','\Cancel|All|Selected')
      
              If x# <> 1
      
                  Prog.ProgressSetup(Records(STOCK))
      
                  save_sto_id = access:stock.savefile()
                  access:stock.clearkey(sto:minimum_part_number_key)
                  sto:minimum_stock = 'YES'
                  If x# = 3
                      sto:location      = location_temp
                  End!If x# = 1
                  set(sto:minimum_part_number_key,sto:minimum_part_number_key)
                  loop
                      if access:stock.next()
                         break
                      end !if
                      if sto:minimum_stock <> 'YES'      |
                          then break.  ! end if
                      If x# = 3
                          If sto:location      <> location_temp
                              Break
                          End!If sto:location      <> location_temp
                      End!If x# = 1
                      If Prog.InsideLoop()
      
                      End ! If Prog.InsideLoop()
      
                      If sto:reorder_level - sto:quantity_stock >0
                          get(ordpend,0)
                          if access:ordpend.primerecord() = level:benign
                              ope:part_ref_number = sto:ref_number
                              ope:part_type       = 'STO'
                              ope:supplier        = sto:supplier
                              ope:part_number     = sto:part_number
                              ope:description     = sto:description
                              ope:quantity        = STO:Reorder_Level - STO:Quantity_Stock
                              access:ordpend.insert()
                          end!if access:ordpend.primerecord() = level:benign
                      End!If sto:reorder_level - sto:quantity_stock >0
                      sto:pending_ref_number = ope:ref_number
                      pos = Position(sto:minimum_part_number_key)
                      sto:minimum_stock = 'NO'
                      access:stock.update()
                      Reset(sto:minimum_part_number_key,pos)
      
                  end !loop
                  access:stock.restorefile(save_sto_id)
                  Prog.ProgressFinish()
                  Case Missive('Process Completed.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
      
              End!If x# = 3
      
          Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessAll, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='105L(2)|M~Part Number~@s30@#1#123L(2)|M~Description~@s30@#2#106L(2)|M~Supplier~@s30@#3#41R(2)|M~Min Level~@n8@#4#50D(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab:2{PROP:TEXT} = 'By Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='123L(2)|M~Description~@s30@#2#105L(2)|M~Part Number~@s30@#1#106L(2)|M~Supplier~@s30@#3#41R(2)|M~Min Level~@n8@#4#50D(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sto:Part_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Part_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Part_Number, Selected)
    OF ?sto:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Description, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort0:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Method Executable Code Section) ARG(1, , Init, (BYTE Controls,BYTE Mode))
  sto:minimum_stock = 'YES'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Method Executable Code Section) ARG(1, , Init, (BYTE Controls,BYTE Mode))


BRW1::Sort1:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))
  sto:minimum_stock = 'YES'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
