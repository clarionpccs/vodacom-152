

   MEMBER('sbf01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBF01002.INC'),ONCE        !Local module procedure declarations
                     END


Spares_Used_Export   PROCEDURE                        ! Declare Procedure
save_sup_id          USHORT,AUTO
savepath             STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
spares_queue         QUEUE,PRE(spaque)
supplier             STRING(30)
part_number          STRING(30)
description          STRING(30)
quantity             REAL
purchase_cost        REAL
total                REAL
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:SUPPLIER.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:STOCK.Open
    savepath = path()
    set(defaults)
    access:defaults.next()
    If def:exportpath <> ''
        glo:file_name = Clip(def:exportpath) & '\EXP_SPA.CSV'
    Else!If def:exportpath <> ''
        glo:file_name = 'C:\EXP_SPA.CSV'
    End!If def:exportpath <> ''

    If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        setpath(savepath)
    else
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(supplier)
        ?progress:userstring{prop:text} = 'Compiling Data ...'

        Remove(expspares)
        access:expspares.open()
        access:expspares.usefile()
        access:expspares.usefile()
        Clear(spares_queue)
        Free(spares_queue)
        setpath(savepath)
        setcursor(cursor:wait)
        save_sup_id = access:supplier.savefile()
        set(sup:company_name_key)
        loop
            if access:supplier.next()
               break
            end !if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            do getnextrecord2
            count# = 0
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:suppdateordkey)
            par:supplier     = sup:company_name
            par:date_ordered = glo:select1
            set(par:suppdateordkey,par:suppdateordkey)
            loop
                if access:parts.next()
                   break
                end !if
                if par:supplier     <> sup:company_name      |
                or par:date_ordered >  glo:select2     |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Sort(spares_queue,spaque:part_number)
                spaque:part_number  = par:part_number
                Get(spares_queue,spaque:part_number)
                If Error()
                    spaque:supplier     = sup:company_name
                    spaque:part_number  = par:part_number
                    spaque:description  = par:description
                    If par:part_ref_number <> ''
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = par:part_ref_number
                        if access:stock.fetch(sto:ref_number_key) = Level:Benign
                            If sto:location <> glo:select3
                                Cycle
                            End!If sto:location <> glo:select3
                            spaque:purchase_cost    = sto:purchase_cost
                        end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    Else!If par:part_ref_number <> ''
                        spaque:purchase_cost    = par:purchase_cost
                    End!If par:part_ref_number <> ''
                    spaque:quantity     = par:quantity
                    Add(spares_queue)
                Else!If Error
                    spaque:quantity    += par:quantity
                    Put(spares_queue)
                End!If Error
            end !loop
            access:parts.restorefile(save_par_id)

            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:suppdateordkey)
            wpr:supplier     = sup:company_name
            wpr:date_ordered = glo:select1
            set(wpr:suppdateordkey,wpr:suppdateordkey)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:supplier     <> sup:company_name      |
                or wpr:date_ordered >  glo:select2      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Sort(spares_queue,spaque:part_number)
                spaque:part_number  = wpr:part_number
                Get(spares_queue,spaque:part_number)
                If Error()
                    spaque:supplier     = sup:company_name
                    spaque:part_number  = wpr:part_number
                    spaque:description  = wpr:description
                    If wpr:part_ref_number <> ''
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = wpr:part_ref_number
                        if access:stock.fetch(sto:ref_number_key) = Level:Benign
                            If sto:location <> glo:select3
                                Cycle
                            End!If sto:location <> glo:select3
                            spaque:purchase_cost    = sto:purchase_cost
                        end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    Else!If par:part_ref_number <> ''
                        spaque:purchase_cost    = wpr:purchase_cost
                    End!If par:part_ref_number <> ''
                    spaque:quantity     = wpr:quantity
                    Add(spares_queue)
                Else!If Error
                    spaque:quantity    += wpr:quantity
                    Put(spares_queue)
                End!If Error

            end !loop
            access:warparts.restorefile(save_wpr_id)

        end !loop
        access:supplier.restorefile(save_sup_id)
        setcursor()

        setcursor()
        close(progresswindow)


        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(spares_queue)
        ?progress:userstring{prop:text} = 'Creating File ...'

        Sort(spares_queue,spaque:supplier)
        Loop x# = 1 To Records(spares_queue)
            Do getnextrecord2
            Get(spares_queue,x#)
            get(expspares,0)
            if access:expspares.primerecord() = Level:Benign
                expspa:supplier    = Stripcomma(spaque:supplier)
                expspa:part_number = Stripcomma(spaque:part_number)
                expspa:description = Stripcomma(spaque:description)
                expspa:quantity    = Stripcomma(spaque:quantity)
                expspa:sale_cost   = (Format(spaque:purchase_cost,@n_14.2))
                expspa:total_cost  = (Format(spaque:purchase_cost * spaque:quantity,@n_14.2))
                access:expspares.insert()
                count# += 1
            end!if access:expspares.primerecord() = Level:Benign
        End!Loop x# = 1 To Records(spares_queue)
        access:expspares.close()
        setcursor()
        close(progresswindow)
        If count# = 0
            beep(beep:systemhand)  ;  yield()
            message('There are no records that match the selected criteria.', |
                    'ServiceBase 2000', icon:hand)
        Else!If count# = 0
            beep(beep:systemexclamation)  ;  yield()
            message('Export Completed.', |
                    'ServiceBase 2000', icon:exclamation)
        End!If count# = 0

    end
   Relate:SUPPLIER.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:STOCK.Close
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Spares_Date_Range PROCEDURE (f_title)                 !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select3
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Date Range'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Date Range'),USE(?Tab1)
                           PROMPT('Start Date'),AT(240,179),USE(?glo:select1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,179,64,10),USE(GLO:Select1),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(AltD),CAP
                           BUTTON,AT(384,175),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(240,199),USE(?glo:select2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,199,64,10),USE(GLO:Select2),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(AltD),UPR
                           BUTTON,AT(384,195),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Site Location'),AT(240,219),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s40),AT(316,219,124,10),USE(GLO:Select3),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020588'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Spares_Date_Range')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?WindowTitle{prop:text} = Clip(f_title) & ' Date Range'
  glo:select1 = Deformat('1/1/1990',@d6b)
  glo:select2 = Today()
  glo:select3 = ''
  Display()
  ! Save Window Name
   AddToLog('Window','Open','Spares_Date_Range')
  Bryan.CompFieldColour()
  ?glo:select1{Prop:Alrt,255} = MouseLeft2
  ?glo:select2{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB4.Init(GLO:Select3,?GLO:Select3,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(loc:Location_Key)
  FDCB4.AddField(loc:Location,FDCB4.Q.loc:Location)
  FDCB4.AddField(loc:RecordNumber,FDCB4.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  Post(Event:CloseWindow)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Spares_Date_Range')
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020588'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020588'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020588'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select1 = TINCALENDARStyle1(GLO:Select1)
          Display(?glo:select1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          GLO:Select2 = TINCALENDARStyle1(GLO:Select2)
          Display(?glo:select2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If glo:select3 = ''
          Select(?glo:select3)
      Else!If glo:select3 = ''
          Case f_title
              Of 'Spares Used Export'
                  Spares_Used_Export
              Of 'Spares Received Export'
                  Spares_Received_Export
          End!
      End!If glo:select3 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?GLO:Select1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?GLO:Select2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?GLO:Select1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, AlertKey)
      glo:select1 = Deformat('1/1/1990',@d6b)
      Display(?glo:select1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select1, AlertKey)
    END
  OF ?GLO:Select2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, AlertKey)
      glo:select2 = Today()
      Display(?glo:select2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GLO:Select2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Spares_Received_Export PROCEDURE                      ! Declare Procedure
save_sup_id          USHORT,AUTO
savepath             STRING(255)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
spares_queue         QUEUE,PRE(spaque)
supplier             STRING(30)
part_number          STRING(30)
description          STRING(30)
quantity             REAL
purchase_cost        REAL
total                REAL
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:SUPPLIER.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:STOCK.Open
   Relate:DEFAULTS.Open
    savepath = path()
    set(defaults)
    access:defaults.next()
    If def:exportpath <> ''
        glo:file_name = Clip(def:exportpath) & '\EXP_REC.CSV'
    Else!If def:exportpath <> ''
        glo:file_name = 'C:\EXP_REC.CSV'
    End!If def:exportpath <> ''

    If not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        setpath(savepath)
    else
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(supplier)
        ?progress:userstring{prop:text} = 'Compiling Data ...'
        count# = 0
        Remove(expspares)
        access:expspares.open()
        access:expspares.usefile()
        Clear(spares_queue)
        Free(spares_queue)
        setpath(savepath)
        setcursor(cursor:wait)
        save_sup_id = access:supplier.savefile()
        set(sup:company_name_key)
        loop
            if access:supplier.next()
               break
            end !if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            do getnextrecord2

            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:SuppDateRecKey)
            par:supplier     = sup:company_name
            par:date_received = glo:select1
            set(par:SuppDateRecKey,par:SuppDateRecKey)
            loop
                if access:parts.next()
                   break
                end !if
                if par:supplier     <> sup:company_name      |
                or par:date_received >  glo:select2     |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Sort(spares_queue,spaque:part_number)
                spaque:part_number  = par:part_number
                Get(spares_queue,spaque:part_number)
                If Error()
                    spaque:supplier     = sup:company_name
                    spaque:part_number  = par:part_number
                    spaque:description  = par:description
                    If par:part_ref_number <> ''
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = par:part_ref_number
                        if access:stock.fetch(sto:ref_number_key) = Level:Benign
                            If sto:location <> glo:select3
                                Cycle
                            End!If sto:location <> glo:select3
                            spaque:purchase_cost    = sto:purchase_cost
                        end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    Else!If par:part_ref_number <> ''
                        spaque:purchase_cost    = par:purchase_cost
                    End!If par:part_ref_number <> ''
                    spaque:quantity     = par:quantity
                    Add(spares_queue)
                Else!If Error
                    spaque:quantity    += par:quantity
                    Put(spares_queue)
                End!If Error
            end !loop
            access:parts.restorefile(save_par_id)

            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:SuppDateRecKey)
            wpr:supplier     = sup:company_name
            wpr:date_received = glo:select1
            set(wpr:SuppDateRecKey,wpr:SuppDateRecKey)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:supplier     <> sup:company_name      |
                or wpr:date_received >  glo:select2      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Sort(spares_queue,spaque:part_number)
                spaque:part_number  = wpr:part_number
                Get(spares_queue,spaque:part_number)
                If Error()
                    spaque:supplier     = sup:company_name
                    spaque:part_number  = wpr:part_number
                    spaque:description  = wpr:description
                    If wpr:part_ref_number <> ''
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number = wpr:part_ref_number
                        if access:stock.fetch(sto:ref_number_key) = Level:Benign
                            If sto:location <> glo:select3
                                Cycle
                            End!If sto:location <> glo:select3
                            spaque:purchase_cost    = sto:purchase_cost
                        end!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    Else!If par:part_ref_number <> ''
                        spaque:purchase_cost    = wpr:purchase_cost
                    End!If par:part_ref_number <> ''
                    spaque:quantity     = wpr:quantity
                    Add(spares_queue)
                Else!If Error
                    spaque:quantity    += wpr:quantity
                    Put(spares_queue)
                End!If Error

            end !loop
            access:warparts.restorefile(save_wpr_id)

        end !loop
        access:supplier.restorefile(save_sup_id)
        setcursor()

        setcursor()
        close(progresswindow)


        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'

        recordstoprocess    = Records(spares_queue)

        ?progress:userstring{prop:text} = 'Creating File ...'
        Sort(spares_queue,spaque:supplier)
        Loop x# = 1 To Records(spares_queue)
            Do getnextrecord2
            Get(spares_queue,x#)
            get(expspares,0)
            if access:expspares.primerecord() = Level:Benign
                expspa:supplier    = Stripcomma(spaque:supplier)
                expspa:part_number = Stripcomma(spaque:part_number)
                expspa:description = Stripcomma(spaque:description)
                expspa:quantity    = Stripcomma(spaque:quantity)
                expspa:sale_cost   = Stripcomma(Format(spaque:purchase_cost,@n_14.2))
                expspa:total_cost  = Stripcomma(Format(spaque:purchase_cost * spaque:quantity,@n_14.2))
                access:expspares.insert()
                count# += 1
            end!if access:expspares.primerecord() = Level:Benign
        End!Loop x# = 1 To Records(spares_queue)
        access:expspares.close()
        setcursor()
        close(progresswindow)

        If count# = 0
            beep(beep:systemhand)  ;  yield()
            message('There are no records that match the selected criteria.', |
                    'ServiceBase 2000', icon:hand)
        Else!If count# = 0
            beep(beep:systemexclamation)  ;  yield()
            message('Export Completed.', |
                    'ServiceBase 2000', icon:exclamation)
        End!If count# = 0


    end
   Relate:SUPPLIER.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:STOCK.Close
   Relate:DEFAULTS.Close
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Engineer_Status_Screen PROCEDURE                      !Generated from procedure template - Window

FilesOpened          BYTE
save_job_id          USHORT,AUTO
save_joe_id          USHORT,AUTO
team_temp            STRING(30)
repaired_temp        REAL
achieved_temp        REAL
tmp:Repair           LONG
tmp:Modification     LONG
tmp:Exchange         LONG
tmp:LiquidDamage     LONG
tmp:NFF              LONG
tmp:BER              LONG
tmp:RTM              LONG
tmp:TotalPoints      LONG
tmp:Allocated        LONG
tmp:RNR              LONG
tmp:Hub              LONG
tmp:FullName         STRING(60)
tmp:Escalated        LONG
tmp:HubPoints        LONG
tmp:EngineerQA       LONG
tmp:EscalatedPoints  LONG
tmp:EngineerQAPoints LONG
tmp:ExchangePoints   LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?team_temp
tea:Team               LIKE(tea:Team)                 !List box control field - type derived from field
tea:Record_Number      LIKE(tea:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(TEAMS)
                       PROJECT(tea:Team)
                       PROJECT(tea:Record_Number)
                     END
BRW3::View:Browse    VIEW(USERS)
                       PROJECT(use:Repair_Target)
                       PROJECT(use:User_Code)
                       PROJECT(use:IncludeInEngStatus)
                       PROJECT(use:Team)
                       PROJECT(use:Surname)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:FullName           LIKE(tmp:FullName)             !List box control field - type derived from local data
tmp:Allocated          LIKE(tmp:Allocated)            !List box control field - type derived from local data
tmp:Allocated_NormalFG LONG                           !Normal forground color
tmp:Allocated_NormalBG LONG                           !Normal background color
tmp:Allocated_SelectedFG LONG                         !Selected forground color
tmp:Allocated_SelectedBG LONG                         !Selected background color
tmp:Escalated          LIKE(tmp:Escalated)            !List box control field - type derived from local data
tmp:EngineerQA         LIKE(tmp:EngineerQA)           !List box control field - type derived from local data
tmp:Hub                LIKE(tmp:Hub)                  !List box control field - type derived from local data
tmp:Repair             LIKE(tmp:Repair)               !List box control field - type derived from local data
tmp:Modification       LIKE(tmp:Modification)         !List box control field - type derived from local data
tmp:Exchange           LIKE(tmp:Exchange)             !List box control field - type derived from local data
tmp:LiquidDamage       LIKE(tmp:LiquidDamage)         !List box control field - type derived from local data
tmp:NFF                LIKE(tmp:NFF)                  !List box control field - type derived from local data
tmp:BER                LIKE(tmp:BER)                  !List box control field - type derived from local data
tmp:RTM                LIKE(tmp:RTM)                  !List box control field - type derived from local data
tmp:RNR                LIKE(tmp:RNR)                  !List box control field - type derived from local data
tmp:TotalPoints        LIKE(tmp:TotalPoints)          !List box control field - type derived from local data
use:Repair_Target      LIKE(use:Repair_Target)        !List box control field - type derived from field
achieved_temp          LIKE(achieved_temp)            !List box control field - type derived from local data
use:User_Code          LIKE(use:User_Code)            !Primary key field - type derived from field
use:IncludeInEngStatus LIKE(use:IncludeInEngStatus)   !Browse key field - type derived from field
use:Team               LIKE(use:Team)                 !Browse key field - type derived from field
use:Surname            LIKE(use:Surname)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK6::use:IncludeInEngStatus LIKE(use:IncludeInEngStatus)
HK6::use:Team             LIKE(use:Team)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Engineer Status'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         ITEM('Defaults'),USE(?Defaults)
                       END
                       PROMPT('Engineer Status'),AT(8,6),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,5,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,7),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,396,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT(''),AT(84,29,168,12),USE(?Date),TRN,FONT('Tahoma',10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       PROMPT('Team'),AT(8,31),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(608,396),USE(?CancelButton),TRN,FLAT,LEFT,ICON('closep.jpg'),STD(STD:Close)
                       PANEL,AT(4,27,672,20),USE(?Panel1:2),FILL(09A6A7CH)
                       COMBO(@s30),AT(84,31,124,10),USE(team_temp),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       SHEET,AT(4,51,672,344),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('By Engineer'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,67,664,324),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('110L(2)|M~Name~@s60@35R(2)|M*~Allocated~@s8@36R(2)|M~Escalated~@s8@29R(2)|M~Eng ' &|
   'QA~@s8@25R(2)|M~Hub~@s8@31R(2)|M~Repair~@s8@25R(2)|M~Mod~@s8@38R(2)|M~Exchange~@' &|
   's8@46R(2)|M~Liqu Damage~@s8@31R(2)|M~N.F.F.~@s8@33R(2)|M~B.E.R.~@s8@32R(2)|M~R.T' &|
   '.M.~@s8@32R(2)|M~R.N.R.~@n-14@38R(2)|M~Total~@s8@38R(2)|M~Target~@s8@38R(2)|M~Ac' &|
   'hieved (%)~@n10@'),FROM(Queue:Browse)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
WorkOutWeighting        Routine
    !Found
    Case rtd:BER
        Of 0 !Repair
            tmp:Repair += rtd:JobWeighting
        Of 1 !BER
            tmp:BER    += rtd:JobWeighting
        Of 2 !Mod
            tmp:Modification     += rtd:JobWeighting
        !This status doesn't really apply.
        !Will use whether a job has an exchange unit attached instead - L832 (DBH: 16-07-2003)
!        Of 3 !Exc
!            tmp:Exchange    += rtd:JobWeighting
        Of 4 !Liq
            tmp:LiquidDamage    += rtd:JobWeighting
        Of 5 !NFF
            tmp:NFF += rtd:JobWeighting
        Of 6 !RTM
            tmp:RTM += rtd:JobWeighting
        !of 7 not needed - this is "Excluded"
        of 8 !RNR
            tmp:RNR += rtd:JobWeighting
    End !Case rtd:BER
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020316'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Engineer_Status_Screen')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:JOBS.Open
  Relate:USERS_ALIAS.Open
  Access:REPTYDEF.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:LOCATLOG.UseFile
  Access:JOBSENG.UseFile
  SELF.FilesOpened = True
  tmp:HubPoints   = GETINI('ENGINEERSTATUS','HubPoints',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:EscalatedPoints = GETINI('ENGINEERSTATUS','EscalatedPoints',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:EngineerQAPoints    = GETINI('ENGINEERSTATUS','EngineerQAPoints',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ExchangePoints    = GETINI('ENGINEERSTATUS','ExchangePoints',,CLIP(PATH())&'\SB2KDEF.INI')
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:USERS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !If called from Web Job
  if glo:webjob then
      access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = ClarioNET:Global.Param2
      if access:tradeacc.fetch(tra:Account_Number_Key) then
          !error
      END
      team_temp = tra:SiteLocation
      ?Team_temp{prop:disable}=true
  END
  ! Save Window Name
   AddToLog('Window','Open','Engineer_Status_Screen')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,use:TeamStatusKey)
  BRW3.AddRange(use:Team)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,use:Surname,1,BRW3)
  BIND('tmp:FullName',tmp:FullName)
  BIND('tmp:Allocated',tmp:Allocated)
  BIND('tmp:Escalated',tmp:Escalated)
  BIND('tmp:EngineerQA',tmp:EngineerQA)
  BIND('tmp:Hub',tmp:Hub)
  BIND('tmp:Repair',tmp:Repair)
  BIND('tmp:Modification',tmp:Modification)
  BIND('tmp:Exchange',tmp:Exchange)
  BIND('tmp:LiquidDamage',tmp:LiquidDamage)
  BIND('tmp:NFF',tmp:NFF)
  BIND('tmp:BER',tmp:BER)
  BIND('tmp:RTM',tmp:RTM)
  BIND('tmp:RNR',tmp:RNR)
  BIND('tmp:TotalPoints',tmp:TotalPoints)
  BIND('achieved_temp',achieved_temp)
  BRW3.AddField(tmp:FullName,BRW3.Q.tmp:FullName)
  BRW3.AddField(tmp:Allocated,BRW3.Q.tmp:Allocated)
  BRW3.AddField(tmp:Escalated,BRW3.Q.tmp:Escalated)
  BRW3.AddField(tmp:EngineerQA,BRW3.Q.tmp:EngineerQA)
  BRW3.AddField(tmp:Hub,BRW3.Q.tmp:Hub)
  BRW3.AddField(tmp:Repair,BRW3.Q.tmp:Repair)
  BRW3.AddField(tmp:Modification,BRW3.Q.tmp:Modification)
  BRW3.AddField(tmp:Exchange,BRW3.Q.tmp:Exchange)
  BRW3.AddField(tmp:LiquidDamage,BRW3.Q.tmp:LiquidDamage)
  BRW3.AddField(tmp:NFF,BRW3.Q.tmp:NFF)
  BRW3.AddField(tmp:BER,BRW3.Q.tmp:BER)
  BRW3.AddField(tmp:RTM,BRW3.Q.tmp:RTM)
  BRW3.AddField(tmp:RNR,BRW3.Q.tmp:RNR)
  BRW3.AddField(tmp:TotalPoints,BRW3.Q.tmp:TotalPoints)
  BRW3.AddField(use:Repair_Target,BRW3.Q.use:Repair_Target)
  BRW3.AddField(achieved_temp,BRW3.Q.achieved_temp)
  BRW3.AddField(use:User_Code,BRW3.Q.use:User_Code)
  BRW3.AddField(use:IncludeInEngStatus,BRW3.Q.use:IncludeInEngStatus)
  BRW3.AddField(use:Team,BRW3.Q.use:Team)
  BRW3.AddField(use:Surname,BRW3.Q.use:Surname)
  FDCB2.Init(team_temp,?team_temp,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:TEAMS,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(tea:Team_Key)
  FDCB2.AddField(tea:Team,FDCB2.Q.tea:Team)
  FDCB2.AddField(tea:Record_Number,FDCB2.Q.tea:Record_Number)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW3.AskProcedure = 0
      CLEAR(BRW3.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:JOBS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Engineer_Status_Screen')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Defaults
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Defaults, Accepted)
      If SecurityCheck('ENGINEER STATUS DEFAULTS')
          NoAccess('ENGINEER STATUS DEFAULTS')
      Else !SecurityCheck('ENGINEER STATUS DEFAULTS')
          EngineerStatusDefaults
      End !SecurityCheck('ENGINEER STATUS DEFAULTS')
      
      tmp:HubPoints   = GETINI('ENGINEERSTATUS','HubPoints',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:EscalatedPoints = GETINI('ENGINEERSTATUS','EscalatedPoints',,CLIP(PATH())&'\SB2KDEF.INI')
      tmp:EngineerQAPoints    = GETINI('ENGINEERSTATUS','EngineerQAPoints',,CLIP(PATH())&'\SB2KDEF.INI')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Defaults, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020316'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020316'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020316'&'0')
      ***
    OF ?team_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?team_temp, Accepted)
      BRW3.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW3.ApplyRange
      BRW3.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?team_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?team_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW3.ApplyRange
      BRW3.ResetSort(1)
      Select(?List)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case Today() % 7
          Of 0
              day" = 'Sunday'
          Of 1
              day" = 'Monday'
          Of 2
              day" = 'Tuesday'
          Of 3
              day" = 'Wednesday'
          Of 4
              day" = 'Thursday'
          Of 5
              day" = 'Friday'
          Of 6
              day"    = 'Saturday'
      End!Case Today() % 7
      ?date{prop:text} = Clip(day") & ', ' & Format(Today(),@d18)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW3.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 1
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = team_temp
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())
  achieved_temp = ''
  tmp:Hub = ''
  tmp:Escalated = ''
  tmp:Repair  = ''
  tmp:BER = ''
  tmp:Modification = ''
  tmp:Exchange = ''
  tmp:LiquidDamage = ''
  tmp:NFF = ''
  tmp:RTM = ''
  tmp:RNR = ''
  tmp:TotalPoints = ''
  tmp:EngineerQA = ''
  tmp:Allocated = ''
  
  Save_joe_ID = Access:JOBSENG.SaveFile()
  Access:JOBSENG.ClearKey(joe:UserCodeStatusKey)
  joe:UserCode   = use:User_Code
  joe:StatusDate = Today()
  Set(joe:UserCodeStatusKey,joe:UserCodeStatusKey)
  Loop
      If Access:JOBSENG.NEXT()
         Break
      End !If
      If joe:UserCode   <> use:User_Code      |
      Or joe:StatusDate <> Today()      |
          Then Break.  ! End If
      Case joe:Status
          Of 'ALLOCATED'
              tmp:Allocated += 1
          Of 'HUB'
              !Check to see if there is an exchange unit attached.
              !If so, count that instead of HUB - L832 (DBH: 16-07-2003)
              Exchanged# = 0
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = joe:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  If job:Exchange_Unit_Number <> 0
                      Exchanged# = 1
                  Else !If job:Exchange_Unit_Number <> 0
                  End !If job:Exchange_Unit_Number <> 0
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
  
              If Exchanged#
                  tmp:Exchange += tmp:ExchangePoints
              Else !If Exchanged#
                  tmp:Hub += tmp:HubPoints
              End !If Exchanged#
          Of 'ESCALATED'
              tmp:Escalated += tmp:EscalatedPoints
          Of 'ENGINEER QA'
              tmp:EngineerQA += tmp:EngineerQAPoints
          Of 'COMPLETED'
              Access:JOBS.Clearkey(job:Ref_Number_Key)
              job:Ref_Number  = joe:JobNumber
              If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  If job:Warranty_Job = 'YES'
                      If job:Repair_Type_Warranty <> ''
                          Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                          rtd:Manufacturer = job:Manufacturer
                          rtd:Warranty     = 'YES'
                          rtd:Repair_Type  = job:Repair_Type_Warranty
                          If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                              !Found
                              Do WorkOutWeighting
                          Else !If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                               !Error
                          End!If Access:REPTYDEF.TryFetch(rtd:Warranty_Key) = Level:Benign
                      End !If job:Repair_Type_Warranty <> ''
                  Else
                      If job:Chargeable_Job = 'YES'
                          Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                          rtd:Manufacturer = job:Manufacturer
                          rtd:Chargeable   = 'YES'
                          rtd:Repair_Type  = job:Repair_Type
                          If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                              !Found
                              Do WorkOutWeighting
                          Else! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:REPTYDEF.Tryfetch(rtd:Chargeable_Key) = Level:Benign
  
                      End !If job:Chargeable_Job = 'YES'
                  End !If job:Warranty_Job = 'YES'
  
              Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  !Error
              End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      End !Case joe:Status
  End !Loop
  Access:JOBSENG.RestoreFile(Save_joe_ID)
  
  tmp:TotalPoints = tmp:Hub + tmp:Escalated + tmp:EngineerQA + tmp:Repair + tmp:BER + tmp:Modification + tmp:Exchange + tmp:LiquidDamage + tmp:NFF + tmp:RTM + tmp:RNR
  achieved_temp = ((tmp:Hub + tmp:Escalated + tmp:EngineerQA + tmp:Repair + tmp:BER + tmp:Modification + tmp:Exchange + tmp:LiquidDamage + tmp:NFF + tmp:RTM + tmp:RNR) / use:repair_target) * 100
  tmp:FullName = CLIP(use:Surname) & ', ' & CLIP(use:Forename)
  PARENT.SetQueueRecord
  SELF.Q.tmp:Allocated_NormalFG = 255
  SELF.Q.tmp:Allocated_NormalBG = 16777215
  SELF.Q.tmp:Allocated_SelectedFG = 16777215
  SELF.Q.tmp:Allocated_SelectedBG = 255
  SELF.Q.tmp:FullName = tmp:FullName                  !Assign formula result to display queue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, SetQueueRecord, ())


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  If use:User_Type <> 'ENGINEER'
      ReturnValue = Record:Filtered
  End !use:User_Type <> 'ENGINEER'
  BRW3::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(3, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Re_Despatch PROCEDURE                                 !Generated from procedure template - Window

job_number_temp      LONG
esn_temp             STRING(16)
tmp:JobType          BYTE(0)
save_webjob_id       USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Re-Submit For Despatch'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Re-Submit For Despatch'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,72),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('WARNING! This procedure should only be used in the event of an error.'),AT(168,89,344,21),USE(?Prompt1),CENTER,FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('This procedure will remove all Despatch Details from the selected job and return' &|
   ' it to a ''Ready To Despatch'' state.'),AT(168,113,344,29),USE(?Prompt2),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('(Note: An entry will be recorded in the Audit Trail)'),AT(247,140),USE(?Prompt3),CENTER,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(164,156,352,174),USE(?Sheet2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Re-Submit For Despatch'),USE(?Tab2)
                           PROMPT('Re-Submit For Despatch'),AT(295,161),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(240,180),USE(?job_number_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(316,180,64,10),USE(job_number_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('I.M.E.I. Number'),AT(240,201),USE(?esn_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s16),AT(316,201,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           OPTION('Despatch Job Type'),AT(316,212,124,28),USE(tmp:JobType),BOXED,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('A.R.C.'),AT(328,225),USE(?tmp:JobType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('R.R.C.'),AT(392,225),USE(?tmp:JobType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                         END
                       END
                       BUTTON,AT(448,298),USE(?Resubmit),TRN,FLAT,LEFT,ICON('resubp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('finishp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020325'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Re_Despatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:LOAN.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Re_Despatch')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Re_Despatch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020325'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020325'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020325'&'0')
      ***
    OF ?Resubmit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Resubmit, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.tryfetch(job:ref_number_key)
          Case Missive('Cannot find selected Job Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Select(?job_number_temp)
      Else!if access:jobs.tryfetch(job:ref_number_key)
          If esn_temp = job:esn
              despatch# = 1
      
              If job:date_despatched = '' And tmp:JobType = 0
                  Case Missive('The selected job has NOT been previously despatched.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish to mark this job as "Ready To Despatch"?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                      Of 1 ! No Button
                          despatch# = 0
                          Select(?job_number_temp)
                          esn_temp = ''
                  End ! Case Missive
              End!If job:despatched <> 'YES'
              If despatch# = 1
                  If tmp:JobType = 0
                      IF (AddToAudit(job:ref_number,'JOB','RE-SUBMIT FOR DESPATCH: JOB','DESPATCH DETAILS REMOVED:-<13,10,13,10>CONSIGNMENT NUMBER: ' & CLip(job:consignment_number) & |
                                              '<13,10>DESPATCH NUMBER: ' & Clip(job:despatch_number) & |
                                              '<13,10>DESPATCH USER: ' & Clip(job:despatch_user) & |
                                              '<13,10>DESPATCH DATE: ' & Clip(job:date_despatched)))
                      END ! IF
                  ELSE ! IF
                      IF (AddToAudit(job:ref_number,'JOB','RE-SUBMIT FOR DESPATCH: JOB (RRC)',''))
                      END ! IF
                  End !If tmp:JobType = 1
      
                  Case tmp:JobType
                      Of 0
                          job:Date_Despatched = DespatchANC(job:Courier,'JOB')
              !            Include('companc.inc')
                          job:Current_Courier = job:Courier
                          job:consignment_number = ''
                          job:despatch_number = ''
                          job:despatch_user = ''
      
                      Of 1
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobe:RefNumber  = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
                              jobe:DespatchType = 'JOB'
                              jobe:Despatched = 'REA'
                              Access:JOBSE.Update()
      
                              save_WEBJOB_id = Access:WEBJOB.SaveFile()
                              Access:WEBJOB.Clearkey(wob:RefNumberKey)
                              wob:RefNumber   = job:Ref_Number
                              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                  !Found
                                  wob:ReadyToDespatch = 1
                                  wob:DespatchCourier = job:Courier
                                  Access:WEBJOB.TryUpdate()
                              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                  !Error
                              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                              Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  End !Case tmp:JobType
      
                  GetStatus(810,1,'JOB')
                  access:jobs.update()
                  job_number_temp = ''
                  esn_temp = ''
                  Select(?job_number_temp)
                  Case Missive('Job Updated.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              End!If despatch# = 1
      
          Else!If esn_temp = job:esn
              If job:loan_unit_number <> ''
                  access:loan.clearkey(loa:ref_number_key)
                  loa:ref_number = job:loan_unit_number
                  if access:loan.tryfetch(loa:ref_number_key)
                      error# = 1
                      Case Missive('The selected I.M.E.I. Number cannot be found on the selected job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      esn_temp = ''
                      Select(?esn_temp)
                  Else!if access:loan.tryfetch(loa:ref_number_key)
                      error# = 0
      
                      If tmp:JobType = 0
                          IF (AddToAudit(job:ref_number,'LOA','RE-SUBMIT FOR DESPATCH: LOAN UNIT','DESPATCH DETAILS REMOVED:-<13,10,13,10>CONSIGNMENT NUMBER: ' & CLip(job:loan_consignment_number) & |
                                                  '<13,10>DESPATCH NUMBER: ' & Clip(job:loan_despatch_number) & |
                                                  '<13,10>DESPATCH USER: ' & Clip(job:loan_despatched_user) & |
                                                  '<13,10>DESPATCH DATE: ' & Clip(job:loan_despatched)))
                          END ! IF
                      ELSE
                          IF (AddToAudit(job:ref_number,'LOA','RE-SUBMIT FOR DESPATCH: LOAN UNIT (RRC)',''))
                          END ! IF
                      END !
      
                      Case tmp:JobType
                          Of 0
                              job:loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')
                              job:Current_Courier = job:Loan_Courier
                              job:loan_consignment_number = ''
                              job:loan_despatch_number = ''
                              job:loan_despatched_user = ''
                              !call the loan status routine
                          Of 1
                              Access:JOBSE.Clearkey(jobe:RefNumberKey)
                              jobe:RefNumber  = job:Ref_Number
                              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  !Found
                                  jobe:DespatchType = 'LOA'
                                  jobe:Despatched = 'REA'
                                  Access:JOBSE.Update()
                                  save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                  wob:RefNumber   = job:Ref_Number
                                  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                      !Found
                                      wob:ReadyToDespatch = 1
                                      wob:DespatchCourier = job:Loan_Courier
                                      Access:WEBJOB.TryUpdate()
                                  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                  Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  !Error
                              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      End !Case tmp:JobType
      
                      GetStatus(105,1,'LOA')
                      access:jobs.update()
                      job_number_temp = ''
                      esn_temp = ''
                      Select(?job_number_temp)
                      Case Missive('Job Updated.','ServiceBase 3g',|
                                     'midea.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!if access:loan.tryfetch(loa:ref_number_key)
              End!If job:loan_unit_number <> ''
              If job:exchange_unit_number <> ''
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number  = job:exchange_unit_number
                  If access:exchange.tryfetch(xch:ref_number_key)
                      error# = 1
                      Case Missive('The selected I.M.E.I. Number cannot be found on the selected job.','ServiceBase 3g',|
                                     'mstop.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                      esn_temp = ''
                      Select(?esn_temp)
                  Else!If access:exchange.tryfetch(xch:ref_number_key)
                      error# = 0
      
                      If tmp:JobType = 0
                          IF (AddToAudit(job:ref_number,'EXC','RE-SUBMIT FOR DESPATCH: EXCHANGE UNIT','DESPATCH DETAILS REMOVED:-<13,10,13,10>CONSIGNMENT NUMBER: ' & CLip(job:exchange_consignment_number) & |
                                                  '<13,10>DESPATCH NUMBER: ' & Clip(job:exchange_despatch_number) & |
                                                  '<13,10>DESPATCH USER: ' & Clip(job:exchange_despatched_user) & |
                                                  '<13,10>DESPATCH DATE: ' & Clip(job:exchange_despatched)))
                          END ! IF
                      ELSE
                          IF (AddToAudit(job:ref_number,'EXC','RE-SUBMIT FOR DESPATCH: EXCHANGE UNIT (RRC)',''))
                          END ! IF
                      END !
      
      
                      Case tmp:JobType
                          Of 0
                              job:Exchange_Despatched = DespatchANC(job:Exchange_Courier,'EXC')
                              job:Current_Courier     = job:Exchange_Courier
                              job:exchange_consignment_number = ''
                              job:exchange_despatch_number = ''
                              job:exchange_despatched_user = ''
                          Of 1
                              Access:JOBSE.Clearkey(jobe:RefNumberKey)
                              jobe:RefNumber  = job:Ref_Number
                              If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  !Found
                                  jobe:DespatchType = 'EXC'
                                  jobe:Despatched = 'REA'
                                  Access:JOBSE.Update()
                                  save_WEBJOB_id = Access:WEBJOB.SaveFile()
                                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                  wob:RefNumber   = job:Ref_Number
                                  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                      !Found
                                      wob:ReadyToDespatch = 1
                                      wob:DespatchCourier = job:Exchange_Courier
                                      Access:WEBJOB.TryUpdate()
                                  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                  Access:WEBJOB.RestoreFile(save_WEBJOB_id)
                              Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                  !Error
                              End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      End !Case tmp:JobType
      
                      !call the exchange status routine
                      GetStatus(110,1,'EXC')
                      access:jobs.update()
                      job_number_temp = ''
                      esn_temp = ''
                      Select(?job_number_temp)
                      Case Missive('Job Updated.','ServiceBase 3g',|
                                     'midea.jpg','/OK')
                          Of 1 ! OK Button
                      End ! Case Missive
                  End!If access:exchange.tryfetch(xch:ref_number_key)
              End!If job:exchange_unit_number <> ''
          End!If esn_temp = job:esn
      End!!if access:jobs.tryfetch(job:ref_number_key)
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Resubmit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Count_Stock_Export PROCEDURE                          !Generated from procedure template - Window

Location_Temp        STRING(30)
savepath             STRING(255)
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
total_temp           LONG
location_name_temp   STRING(30),DIM(100)
stock_amount_temp    LONG,DIM(100)
tmp:supplier         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
FDCB5::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('All Stock Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('All Stock Export'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Select Site Location'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(248,202),USE(?Prompt1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(308,203,124,10),USE(Location_Temp),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Supplier'),AT(248,218),USE(?Prompt1:2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(308,218,124,10),USE(tmp:supplier),IMM,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020323'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Count_Stock_Export')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:STOCK_ALIAS.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Count_Stock_Export')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB3.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(loc:Location_Key)
  FDCB3.AddField(loc:Location,FDCB3.Q.loc:Location)
  FDCB3.AddField(loc:RecordNumber,FDCB3.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  FDCB5.Init(tmp:supplier,?tmp:supplier,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder(sup:Company_Name_Key)
  FDCB5.AddField(sup:Company_Name,FDCB5.Q.sup:Company_Name)
  FDCB5.AddField(sup:RecordNumber,FDCB5.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:STOCK_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Count_Stock_Export')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020323'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020323'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020323'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      savepath = path()
      set(defaults)
      access:defaults.next()
      If def:exportpath <> ''
          glo:file_name = Clip(def:exportpath) & '\EXPSTOK.CSV'
      Else!If def:exportpath <> ''
          glo:file_name = 'C:\EXPSTOK.CSV'
      End!If def:exportpath <> ''
      
      if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !failed
          setpath(savepath)
      else!if not filedialog
          !found
          setpath(savepath)
      
          Remove(expgen)
      
          access:expgen.open()
          access:expgen.usefile()
      
          Clear(gen:record)
          gen:line1   = 'Stock Level Export'
          access:expgen.insert()
      
          Clear(gen:record)
          gen:line1   = 'Site Location:,' & CLip(location_temp) & ',Supplier:,' & Clip(tmp:Supplier)
          access:expgen.insert()
      
          Clear(gen:record)
          gen:line1   = 'Description,Purchase Cost,Trade Cost,'
      
          location_name_temp[1] = location_temp
      
          gen:line1   = Clip(gen:line1) & CLip(location_name_temp[1]) & ','
      
          count# = 1
          setcursor(cursor:wait)
          save_loc_id = access:location.savefile()
          set(loc:location_key)
          loop
              if access:location.next()
                 break
              end !if
              If loc:location <> location_temp
                  count# += 1
                  location_name_temp[count#] = loc:location
                  gen:line1   = Clip(gen:line1) & Clip(location_name_temp[count#]) & ','
              End!If loc:location <> location_temp
          end !loop
          access:location.restorefile(save_loc_id)
          setcursor()
      
          gen:line1   = Clip(gen:line1) & 'Total Qty'
          access:expgen.insert()
      
          Prog.ProgressSetup(Records(STOCK))
      
          Clear(gen:record)
      
          setcursor(cursor:wait)
          save_sto_id = access:stock.savefile()
          access:stock.clearkey(sto:description_key)
          sto:location    = location_temp
          set(sto:description_key,sto:description_key)
          loop
              if access:stock.next()
                 break
              end !if
              if sto:location    <> location_temp      |
                  then break.  ! end if
      
              If Prog.InsideLoop()
                  Break
              End ! If Prog.InsideLoop()
      
      
              If tmp:supplier <> ''
                  If sto:supplier <> tmp:supplier
                      Cycle
                  End!If sto:supplier <> tmp:supplier
              End!If tmp:supplier <> ''
      
              stock_amount_temp[1] = sto:quantity_stock
              total_temp  = stock_amount_temp[1]
              Clear(gen:Record)
              gen:line1   = Stripcomma(Clip(sto:description)) & ','
              gen:line1   = Clip(gen:line1) & Format(sto:purchase_Cost,@n14.2) & ','
              gen:line1   = Clip(gen:line1) & Format(sto:sale_cost,@n14.2) & ','
              gen:line1   = Clip(gen:line1) & Clip(stock_amount_temp[1]) & ','
      
              Loop x# = 2 to count#
                  stock_amount_temp[x#] = 0
      
                  save_sto_ali_id = access:stock_alias.savefile()
                  access:stock_alias.clearkey(sto_ali:location_part_description_key)
                  sto_ali:location    = location_name_temp[x#]
                  sto_ali:part_number = sto:part_number
                  sto_ali:description = sto:description
                  set(sto_ali:location_part_description_key,sto_ali:location_part_description_key)
                  loop
                      if access:stock_alias.next()
                         break
                      end !if
                      if sto_ali:location    <> location_name_temp[x#]      |
                      or sto_ali:part_number <> sto:part_number      |
                      or sto_ali:description <> sto:description      |
                          then break.  ! end if
                      stock_amount_temp[x#] += sto_ali:quantity_stock
                      total_temp  += sto_ali:quantity_stock
                  end !loop
                  access:stock_alias.restorefile(save_sto_ali_id)
      
                  gen:line1   = Clip(gen:line1) & Clip(stock_amount_temp[x#]) & ','
              End!Loop x# = 2 to count#
              gen:line1   = Clip(gen:line1) & Clip(total_temp)
              access:expgen.insert()
          end !loop
          access:stock.restorefile(save_sto_id)
          setcursor()
          access:expgen.close()
      
          Prog.ProgressFinish()
      
          Case Missive('Export Completed.','ServiceBase 3g',|
                         'midea.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      
      end!if not filedialog
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

FDCB3.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Delete_Invoice_Parts PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::ivptmp:Record LIKE(ivptmp:RECORD),STATIC
QuickWindow          WINDOW('Update the INVPATMP File'),AT(,,208,182),FONT('MS Sans Serif',8,,),IMM,HLP('Delete_Invoice_Parts'),SYSTEM,GRAY,NOFRAME
                       SHEET,AT(4,4,200,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number:'),AT(8,20),USE(?IVPTMP:RecordNumber:Prompt)
                           ENTRY(@n-14),AT(76,20,64,10),USE(ivptmp:RecordNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('AutoNumber Field'),TIP('AutoNumber Field'),UPR
                           PROMPT('Invoice Number:'),AT(8,34),USE(?IVPTMP:InvoiceNumber:Prompt)
                           ENTRY(@n-14),AT(76,34,64,10),USE(ivptmp:InvoiceNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To Invoice'),TIP('Link To Invoice'),UPR
                           PROMPT('Retstock Number:'),AT(8,48),USE(?IVPTMP:RetstockNumber:Prompt)
                           ENTRY(@n-14),AT(76,48,64,10),USE(ivptmp:RetstockNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link to the Retail Item''s Record Number'),TIP('Link to the Retail Item''s Record Number'),UPR
                           PROMPT('Credit Quantity'),AT(8,62),USE(?IVPTMP:CreditQuantity:Prompt)
                           SPIN(@n-14),AT(76,62,79,10),USE(ivptmp:CreditQuantity),LEFT,MSG('Quantity Credited'),TIP('Quantity Credited'),UPR,STEP(1)
                           PROMPT('Part Number'),AT(8,76),USE(?IVPTMP:PartNumber:Prompt)
                           ENTRY(@s30),AT(76,76,124,10),USE(ivptmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Part Number'),TIP('Retail Stock Part Number'),UPR
                           PROMPT('Description'),AT(8,90),USE(?IVPTMP:Description:Prompt)
                           ENTRY(@s30),AT(76,90,124,10),USE(ivptmp:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Description'),TIP('Retail Stock Description'),UPR
                           PROMPT('Supplier'),AT(8,104),USE(?IVPTMP:Supplier:Prompt)
                           ENTRY(@s30),AT(76,104,124,10),USE(ivptmp:Supplier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Supplier'),TIP('Retail Stock Supplier'),UPR
                           PROMPT('Purchase Cost'),AT(8,118),USE(?IVPTMP:PurchaseCost:Prompt)
                           ENTRY(@n14.2),AT(76,118,60,10),USE(ivptmp:PurchaseCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Purchase Cost'),TIP('Retail Stock Purchase Cost'),UPR
                           PROMPT('Sale Cost'),AT(8,132),USE(?IVPTMP:SaleCost:Prompt)
                           ENTRY(@n14.2),AT(76,132,60,10),USE(ivptmp:SaleCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Sale Cost'),TIP('Retail Stock Sale Cost'),UPR
                           PROMPT('Retail Cost'),AT(8,146),USE(?IVPTMP:RetailCost:Prompt)
                           ENTRY(@n14.2),AT(76,146,60,10),USE(ivptmp:RetailCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Retail Stock Retail Cost'),TIP('Retail Stock Retail Cost'),UPR
                         END
                       END
                       BUTTON('OK'),AT(110,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(159,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(159,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delete_Invoice_Parts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?IVPTMP:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ivptmp:Record,History::ivptmp:Record)
  SELF.AddHistoryField(?ivptmp:RecordNumber,1)
  SELF.AddHistoryField(?ivptmp:InvoiceNumber,2)
  SELF.AddHistoryField(?ivptmp:RetstockNumber,3)
  SELF.AddHistoryField(?ivptmp:CreditQuantity,4)
  SELF.AddHistoryField(?ivptmp:PartNumber,5)
  SELF.AddHistoryField(?ivptmp:Description,6)
  SELF.AddHistoryField(?ivptmp:Supplier,7)
  SELF.AddHistoryField(?ivptmp:PurchaseCost,8)
  SELF.AddHistoryField(?ivptmp:SaleCost,9)
  SELF.AddHistoryField(?ivptmp:RetailCost,10)
  SELF.AddUpdateFile(Access:INVPATMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVPATMP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVPATMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Delete_Invoice_Parts')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVPATMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Delete_Invoice_Parts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Perceived_Site_Location PROCEDURE                     !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Tmp:Job_Number       LONG
Tmp:Location         STRING(30)
Tmp:Current_Status   STRING(30)
Tmp:Remove_Waybill_Entry BYTE(0)
Tmp:Status_Number    REAL
Tmp:Remove_Hub_Repair BYTE(0)
tmp:WayBillNumber    STRING(30)
tmp:Job_Updated_String STRING(255)
window               WINDOW('Change Perceived Site Location'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Change Perceived Site Location'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Change Perceived Site Location'),USE(?Tab1)
                           PROMPT('Location'),AT(184,128),USE(?job:Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(272,128,124,10),USE(job:Location),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('New Location'),AT(184,144),USE(?Tmp:Location:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(272,144,124,10),USE(Tmp:Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(400,140),USE(?CallLookup),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Current Status'),AT(184,170),USE(?job:Current_Status:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(272,170,124,10),USE(job:Current_Status),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('New Status'),AT(184,186),USE(?Tmp:Current_Status:Prompt),TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(272,186,124,10),USE(Tmp:Current_Status),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(400,182),USE(?CallLookup:2),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           PROMPT('Current Waybill Entry'),AT(184,208),USE(?job:Incoming_Consignment_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(272,208,124,10),USE(job:Incoming_Consignment_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Waybill Number'),AT(364,244),USE(?tmp:WayBillNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(432,244,64,10),USE(tmp:WayBillNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Waybill Number'),TIP('Waybill Number'),REQ,UPR
                           PROMPT('Job Number'),AT(184,268),USE(?Tmp:Job_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(256,268,60,10),USE(Tmp:Job_Number),RIGHT(1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           CHECK(' Remove Waybill Entry'),AT(256,244),USE(Tmp:Remove_Waybill_Entry),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK(' Untick Hub Repair'),AT(256,228),USE(Tmp:Remove_Hub_Repair),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       STRING(@s255),AT(176,310,331,15),USE(tmp:Job_Updated_String),TRN,CENTER,FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(448,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),STD(STD:Close),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:Tmp:Location                Like(Tmp:Location)
look:Tmp:Current_Status                Like(Tmp:Current_Status)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020324'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Perceived_Site_Location')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Relate:STATUS.Open
  Relate:WEBJOB.Open
  Access:JOBSE.UseFile
  Access:LOCINTER.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  CLEAR(job:RECORD)
  CLEAR(jobe:RECORD)
  
  ! Save Window Name
   AddToLog('Window','Open','Perceived_Site_Location')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?Tmp:Location{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?Tmp:Location{Prop:Tip}
  END
  IF ?Tmp:Location{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?Tmp:Location{Prop:Msg}
  END
  IF ?Tmp:Current_Status{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?Tmp:Current_Status{Prop:Tip}
  END
  IF ?Tmp:Current_Status{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?Tmp:Current_Status{Prop:Msg}
  END
  IF ?Tmp:Remove_Waybill_Entry{Prop:Checked} = True
    UNHIDE(?tmp:WayBillNumber:Prompt)
    UNHIDE(?tmp:WayBillNumber)
  END
  IF ?Tmp:Remove_Waybill_Entry{Prop:Checked} = False
    HIDE(?tmp:WayBillNumber:Prompt)
    HIDE(?tmp:WayBillNumber)
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Perceived_Site_Location')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickAvailableLocations
      Browse_Status
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Tmp:Job_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Tmp:Job_Number, Accepted)
      Error# = 0                                              !TB13274 - J - 16/04/14 - moved reset to here so can note error on not finding job
      tmp:Job_Updated_String = 'No Changes have been made'    !TB13274 - J - 16/04/14 - added this as a default
      
      Access:Jobs.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = Tmp:Job_Number
      IF Access:Jobs.Fetch(job:Ref_Number_Key)
          !No job!
          Case Missive('Unable to locate selected job.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1                                          !TB13274 - J - 16/04/14 - added this so it will not do the update below - error# already in use for waybill number
      ELSE
        !Found Job!
        Access:Jobse.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF Access:Jobse.Fetch(jobe:RefNumberKey)
          !No Jobse!
        END
      END !if job found
      
      UPDATE()
      DISPLAY()
      
      If tmp:Remove_WayBill_Entry
          If tmp:WaybillNumber = ''
              Error# = 1
              Case Missive('You must select a Waybill Number.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?tmp:WaybillNumber)
          End !If tmp:WaybillNumber = ''
      End !tmp:Remove_WayBill_Entry
      
      !TB13274 - J - 16/04/14 - this check for error# was already in place
      If Error# = 0
          !Case MessageEx('Do you wish to apply this change to the job?','ServiceBase 2000',|
          !               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
           !   Of 1 ! &Yes Button
                LocationChange(Tmp:Location)
                IF Tmp:Current_Status <> ''
                  GetStatus(tmp:Status_number,1,'JOB')
                END
                IF Tmp:Remove_Waybill_Entry
                  If  job:Incoming_Consignment_Number = tmp:WayBillNumber
                      job:Incoming_Consignment_Number = ''
                  End !If  job:Incoming_Consignment_Number = tmp:WayBillNumber
      
                  If job:Consignment_Number = tmp:WayBillNumber
                      job:Consignment_Number = ''
                  End !If job:Consignment_Number = tmp:WayBillNumber
      
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber   = job:Ref_Number
                  If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Found
                      If wob:ExcWayBillNumber = tmp:WayBillNumber
                          wob:ExcWayBillNumber = ''
                      End !If wob:ExcWayBillNumber = tmp:WayBillNumber
      
                      If wob:LoaWayBillNumber = tmp:WayBillNumber
                          wob:LoaWayBillNumber = ''
                      End !If wob:LoaWayBillNumber = tmp:WayBillNumber
      
                      If wob:JobWayBillNumber = tmp:WayBillNumber
                          wob:JobWayBillNumber = ''
                      End !If wob:JobWayBillNumber = tmp:WayBillNumber
                      Access:WEBJOB.Update()
                  Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                      !Error
                  End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                END
                IF Tmp:Remove_Hub_Repair
                  jobe:HubRepair = FALSE
                END
                Access:Jobs.TryUpdate()
                Access:Jobse.TryUpdate()
                tmp:Job_Updated_String = 'JOB - '&CLIP(job:ref_number)&' HAS BEEN UPDATED'
                DISPLAY()
                Select(?tmp:Job_Number)
        !          Post(event:CloseWindow)
        !      Of 2 ! &No Button
        !  End!Case MessageEx
      End !Error# = 0
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Tmp:Job_Number, Accepted)
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !Error# = 0
      !If tmp:Remove_WayBill_Entry
      !    If tmp:WaybillNumber = ''
      !        Error# = 1
      !        Case MessageEx('You must select a Waybill Number.','ServiceBase 2000',|
      !                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
      !            Of 1 ! &OK Button
      !        End!Case MessageEx
      !        Select(?tmp:WaybillNumber)
      !    End !If tmp:WaybillNumber = ''
      !End !tmp:Remove_WayBill_Entry
      !If Error# = 0
      !    Case MessageEx('Do you wish to apply this change to the job?','ServiceBase 2000',|
      !                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !        Of 1 ! &Yes Button
      !          LocationChange(Tmp:Location)
      !          IF tmp:Status <> ''
      !            GetStatus(tmp:Status_number,1,'JOB')
      !          END
      !          IF Tmp:Remove_Waybill_Entry
      !            If  job:Incoming_Consignment_Number = tmp:WayBillNumber
      !                job:Incoming_Consignment_Number = ''
      !            End !If  job:Incoming_Consignment_Number = tmp:WayBillNumber
      !
      !            If job:Consignment_Number = tmp:WayBillNumber
      !                job:Consignment_Number = ''
      !            End !If job:Consignment_Number = tmp:WayBillNumber
      !
      !            Access:WEBJOB.Clearkey(wob:RefNumberKey)
      !            wob:RefNumber   = job:Ref_Number
      !            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !                !Found
      !                If wob:ExcWayBillNumber = tmp:WayBillNumber
      !                    wob:ExcWayBillNumber = ''
      !                End !If wob:ExcWayBillNumber = tmp:WayBillNumber
      !
      !                If wob:LoaWayBillNumber = tmp:WayBillNumber
      !                    wob:LoaWayBillNumber = ''
      !                End !If wob:LoaWayBillNumber = tmp:WayBillNumber
      !
      !                If wob:JobWayBillNumber = tmp:WayBillNumber
      !                    wob:JobWayBillNumber = ''
      !                End !If wob:JobWayBillNumber = tmp:WayBillNumber
      !                Access:WEBJOB.Update()
      !            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !                !Error
      !            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      !
      !          END
      !          IF Tmp:Remove_Hub_Repair
      !            jobe:HubRepair = FALSE
      !          END
      !          Access:Jobs.TryUpdate()
      !          Access:Jobse.TryUpdate()
      !            Post(event:CloseWindow)
      !        Of 2 ! &No Button
      !    End!Case MessageEx
      !End !Error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020324'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020324'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020324'&'0')
      ***
    OF ?Tmp:Location
      IF Tmp:Location OR ?Tmp:Location{Prop:Req}
        loi:Location = Tmp:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:Tmp:Location        = Tmp:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Tmp:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            Tmp:Location = look:Tmp:Location
            SELECT(?Tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      loi:Location = Tmp:Location
      loi:Location_Available = 'YES'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Tmp:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?Tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Tmp:Location)
    OF ?Tmp:Current_Status
      IF Tmp:Current_Status OR ?Tmp:Current_Status{Prop:Req}
        sts:Status = Tmp:Current_Status
        Tmp:Status_Number = sts:Ref_Number
        !Save Lookup Field Incase Of error
        look:Tmp:Current_Status        = Tmp:Current_Status
        IF Access:STATUS.TryFetch(sts:Status_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            Tmp:Current_Status = sts:Status
          ELSE
            CLEAR(Tmp:Status_Number)
            !Restore Lookup On Error
            Tmp:Current_Status = look:Tmp:Current_Status
            SELECT(?Tmp:Current_Status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      sts:Status = Tmp:Current_Status
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          Tmp:Current_Status = sts:Status
          Select(?+1)
      ELSE
          Select(?Tmp:Current_Status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Tmp:Current_Status)
    OF ?Tmp:Remove_Waybill_Entry
      IF ?Tmp:Remove_Waybill_Entry{Prop:Checked} = True
        UNHIDE(?tmp:WayBillNumber:Prompt)
        UNHIDE(?tmp:WayBillNumber)
      END
      IF ?Tmp:Remove_Waybill_Entry{Prop:Checked} = False
        HIDE(?tmp:WayBillNumber:Prompt)
        HIDE(?tmp:WayBillNumber)
      END
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
EngineerStatusDefaults PROCEDURE                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:HubPoints        LONG
tmp:EscalatedPoints  LONG
tmp:EngineerQAPoints LONG
tmp:ExchangePoints   LONG
window               WINDOW('Engineer Status Defaults'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Eng Status Weighting Defaults'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Weighting Defaults'),USE(?Tab1)
                           PROMPT('Hub Points'),AT(252,172),USE(?tmp:HubPoints:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(353,172,64,10),USE(tmp:HubPoints),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Hub Weighting Points'),TIP('Hub Weighting Points'),UPR
                           PROMPT('Escalated Points'),AT(252,192),USE(?tmp:EscalatedPoints:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(353,192,64,10),USE(tmp:EscalatedPoints),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Escalated Weighting Points'),TIP('Escalated Weighting Points'),UPR
                           PROMPT('Engineer QA Points'),AT(252,212),USE(?tmp:EngineerQAPoints:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(353,212,64,10),USE(tmp:EngineerQAPoints),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Engineer QA Weighting Points'),TIP('Engineer QA Weighting Points'),UPR
                           PROMPT('Exchange Points'),AT(252,232),USE(?tmp:ExchangePoints:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(353,232,64,10),USE(tmp:ExchangePoints),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Points'),TIP('Exchange Points'),UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020315'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('EngineerStatusDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  tmp:HubPoints = GETINI('ENGINEERSTATUS','HubPoints',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:EscalatedPoints = GETINI('ENGINEERSTATUS','EscalatedPoints',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:EngineerQAPoints = GETINI('ENGINEERSTATUS','EngineerQAPoints',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:ExchangePoints = GETINI('ENGINEERSTATUS','ExchangePoints',,CLIP(PATH())&'\SB2KDEF.INI')
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EngineerStatusDefaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','EngineerStatusDefaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020315'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020315'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020315'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      PUTINIEXT('ENGINEERSTATUS','HubPoints',tmp:HubPoints,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIEXT('ENGINEERSTATUS','EscalatedPoints',tmp:EscalatedPoints,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIEXT('ENGINEERSTATUS','EngineerQAPoints',tmp:EngineerQAPoints,CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINIEXT('ENGINEERSTATUS','ExchangePoints',tmp:ExchangePoints,CLIP(PATH()) & '\SB2KDEF.INI')
      
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
CreateCreditNote PROCEDURE (f:TotalValue)             !Generated from procedure template - Window

tmp:TotalValue       REAL
tmp:EnterValue       BYTE(0)
tmp:CreditAmount     REAL
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       GROUP,AT(272,268,142,11),USE(?Group:CreditAmount),HIDE
                         PROMPT('Credit Amount'),AT(272,270),USE(?tmp:CreditAmount:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@n-14.2),AT(350,268,64,10),USE(tmp:CreditAmount),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Credit Amount'),TIP('Credit Amount'),UPR
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,244),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Total Value Of Job:'),AT(216,142),USE(?Prompt3),FONT(,12,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@n-14.2),AT(348,142),USE(tmp:TotalValue),TRN,RIGHT,FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       PROMPT('Do you wish to credit the full amount, or enter a specific amount?'),AT(200,180),USE(?Prompt4),TRN,FONT(,10,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       OPTION('Select Option'),AT(268,208,146,48),USE(tmp:EnterValue),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Credit Full Amount'),AT(293,220),USE(?tmp:EnterValue:Radio1),VALUE('0')
                         RADIO('Credit Specific Amount'),AT(293,240),USE(?tmp:EnterValue:Radio2),VALUE('1')
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Create Credit Note'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(372,332),USE(?Button:CreateCreditNote),TRN,FLAT,ICON('creditp.jpg')
                       BUTTON,AT(444,332),USE(?Button:Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:CreditAmount)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020687'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CreateCreditNote')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:CreditAmount:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:TotalValue = f:TotalValue
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','CreateCreditNote')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','CreateCreditNote')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:CreateCreditNote
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CreateCreditNote, Accepted)
      If tmp:EnterValue
          If tmp:CreditAmount > tmp:TotalValue Or tmp:CreditAmount = 0
              Case Missive('Invalid Amount.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              tmp:CreditAmount = 0
              Select(?tmp:CreditAmount)
              Cycle
          End ! If tmp:CreditAmount > tmp:TotalValue
      Else ! If tmp:EnterValue
          tmp:CreditAmount = tmp:TotalValue
      End ! If tmp:EnterValue
      
      Case Missive('Are you sure you want to create a credit note?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              tmp:CreditAmount = 0
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CreateCreditNote, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:EnterValue
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:EnterValue, Accepted)
      If tmp:EnterValue = 1
          ?Group:CreditAmount{prop:Hide} = 0
      Else ! If tmp:EnterValue = 1
          ?Group:CreditAmount{prop:Hide} = 1
      End ! If tmp:EnterValue = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:EnterValue, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020687'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020687'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020687'&'0')
      ***
    OF ?Button:CreateCreditNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CreateCreditNote, Accepted)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CreateCreditNote, Accepted)
    OF ?Button:Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Cancel, Accepted)
      tmp:CreditAmount = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
