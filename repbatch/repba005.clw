

   MEMBER('repbatch.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE

                     MAP
                       INCLUDE('REPBA005.INC'),ONCE        !Local module procedure declarations
                     END


ThirdPartyReceiptReport PROCEDURE (Long f:PONumber)        ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
tmp:True             BYTE(1)                               !True
tmp:IN               STRING('IN {28}')                     !IN
tmp:LineTotal        REAL                                  !Line Total
total:LineTotal      REAL                                  !Line Total
tmp:PurchaseOrderNumber LONG                               !Purchase Order Number
Process:View         VIEW(TRDBATCH)
                       PROJECT(trb:PurchaseOrderNumber)
                       PROJECT(trb:Ref_Number)
                       PROJECT(trb:ThirdPartyInvoiceNo)
                       PROJECT(trb:ThirdPartyVAT)
                       JOIN(TRDPARTY,'Upper(trd:Company_Name) = Upper(trb:Company_Name)')
                         PROJECT(trd:Account_Number)
                         PROJECT(trd:Company_Name)
                       END
                     END
ProgressWindow       WINDOW('Report TRDBATCH'),AT(,,142,59),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),LEFT,MSG('Cancel Report'),TIP('Cancel Report'),ICON('WACANCEL.ICO')
                     END

Report               REPORT('TRDBATCH Report'),AT(250,2542,7750,8104),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,COLOR:Black,FONT:regular),THOUS
                       HEADER,AT(250,219,7750,2323)
                         STRING('Supplier:'),AT(5156,573),USE(?Text:SupplierName),TRN,FONT(,,,FONT:bold)
                         STRING(@s30),AT(5938,573,1667,156),USE(trd:Company_Name),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@s30),AT(5938,833,1667,156),USE(trd:Account_Number),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@PSS3#######/01P),AT(5938,1094,1667,156),USE(trb:PurchaseOrderNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('PO Number:'),AT(5156,1094),USE(?Text:AccountNumber:2),TRN,FONT(,,,FONT:bold)
                         STRING('Account No:'),AT(5156,833),USE(?Text:AccountNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Report Date:'),AT(5156,1354),USE(?ReportDatePrompt),TRN,FONT(,,,FONT:bold)
                         STRING('<<-- Date Stamp -->'),AT(5990,1354),USE(?ReportDateStamp),TRN
                       END
detail1                DETAIL,AT(,,,156)
                         STRING(@s8),AT(156,-10),USE(trb:Ref_Number),TRN,RIGHT(1)
                         STRING(@s30),AT(2031,-10),USE(trb:ThirdPartyInvoiceNo),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@n-14.2),AT(4427,-10),USE(trb:ThirdPartyVAT),TRN,RIGHT
                         STRING(@n-14.2),AT(6406,-10),USE(tmp:LineTotal),TRN,RIGHT
                       END
                       FOOTER,AT(250,11188,7750,250),USE(?Footer)
                       END
                       FORM,AT(250,250,7750,11188),USE(?Form),FONT('Tahoma',8,,)
                         BOX,AT(156,2031,7552,8490),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver),LINEWIDTH(2)
                         BOX,AT(5052,469,2600,1300),USE(?BoxGrey:TopDetails),ROUND,FILL(COLOR:Silver),LINEWIDTH(4)
                         BOX,AT(5000,417,2600,1300),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H),LINEWIDTH(4)
                         BOX,AT(104,1979,7552,260),USE(?Box:Heading),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,2240,7552,8229),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White),LINEWIDTH(2)
                         STRING('SERVICEBASE RECEIPTING DETAILS - OUT OF WARRANTY'),AT(2656,52),USE(?Text:OutOfWarranty),TRN,RIGHT,FONT(,12,,FONT:bold)
                         STRING('Job Number'),AT(208,2031),USE(?Text:JobNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Invoice Number'),AT(2031,2031),USE(?Text:InvoiceNumber),TRN,FONT(,,,FONT:bold)
                         STRING('V.A.T.'),AT(5156,2031),USE(?Text:VAT),TRN,FONT(,,,FONT:bold)
                         STRING('Amount incl. VAT'),AT(6458,2031),USE(?Text:Amount),TRN,FONT(,,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ThirdPartyReceiptReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:TRDBATCH.SetOpenRelated()
  Relate:TRDBATCH.Open                                     ! File TRDBATCH used by this procedure, so make sure it's RelationManager is open
  Access:TRDPARTY.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  tmp:PurchaseOrderNumber = f:PONumber
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:TRDBATCH, ?Progress:PctText, Progress:Thermometer, ProgressMgr, trb:Ref_Number)
  ThisReport.AddSortOrder(trb:PurchaseOrderKey)
  ThisReport.AddRange(trb:PurchaseOrderNumber,tmp:PurchaseOrderNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:TRDBATCH.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = True
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDBATCH.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
      Return
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ReturnValue = PARENT.TakeRecord()
  ! Start - Update the Third Party Batch to ensure this entry is not picked up on the next batch run - TrkBs: 5110 (DBH: 10-06-2005)
  tmp:LineTotal = trb:ThirdPartyInvoiceCharge + trb:ThirdPartyVAT
  trb:BatchRunNotPrinted = False
  Access:TRDBATCH.TryUpdate()
  ! End   - Update the Third Party Batch to ensure this entry is not picked up on the next batch run - TrkBs: 5110 (DBH: 10-06-2005)
  PRINT(RPT:detail1)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName(glo:PDFExportPath)
  SELF.SetDocumentInfo('Third Party Receipt','ServiceBase 3g','Batch Reporting','Batch Reporting','ServiceBase 3g','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

