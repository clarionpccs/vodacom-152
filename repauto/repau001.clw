

   MEMBER('repauto.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('REPAU001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:TAT              STRING('0')
tmp:TagStatus        STRING('0')
tmp:TagBooking       STRING('0')
MarkQueue            QUEUE,PRE()
AccountNumber        STRING(30)
CompanyName          STRING(30)
TATReport            STRING(1)
StatusAnalysisReport STRING(1)
BookingPerformanceReport STRING(1)
EngineerPerformanceReport STRING(1)
GoodsReceivedReport  STRING(1)
BackorderReport      STRING(1)
SMSRecivedReport     STRING(1)
Flag                 STRING(30)
                     END
MarkQueue2           QUEUE,PRE()
SiteLocation         STRING(30)
StockAdjustmentReport STRING(1)
PartsUsageReport     STRING(1)
Flag2                BYTE(0)
                     END
DailyReportQueue     QUEUE,PRE(day)
Filename             STRING(255)
                     END
WeeklyReportQueue    QUEUE,PRE(week)
FileName             STRING(255)
                     END
MonthlyReportQueue   QUEUE,PRE(month)
FileName             STRING(255)
                     END
local:FileName       STRING(255)
window               WINDOW('Report Automation Criteria'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Report Automation Criteria'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,644,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(596,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,30,672,352),USE(?Sheet1),COLOR(0D6EAEFH),SPREAD
                         TAB('Account/Location Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,118,284,260),USE(?List1),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('82L(2)|M~Account Number~@s30@120L(2)|M~Company Name~@s30@10C(2)|M~T~@s1@11C(2)|M' &|
   '~S~@s1@11C(2)|M~B~@s1@11C(2)|M~E~@s1@10C(2)|M~G~@s1@10C(2)|M~B~@s1@10C(2)|M~M~@s' &|
   '1@'),MARK(MarkQueue.Flag),FROM(MarkQueue)
                           BUTTON,AT(424,86),USE(?Button12:2),TRN,FLAT,ICON('selallp.jpg')
                           STRING('SMS Recvd'),AT(272,66,20,60),USE(?String1:9),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Back Order   '),AT(260,62,20,60),USE(?String1:8),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Status Analy.'),AT(212,62,20,60),USE(?String1:2),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           BUTTON,AT(312,120),USE(?Button1),TRN,FLAT,ICON('tatrepp.jpg')
                           STRING('TAT Report    '),AT(200,62,20,60),USE(?String1),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           BUTTON,AT(312,151),USE(?Button2),TRN,FLAT,ICON('starepp.jpg')
                           PROMPT('* Highlight the desired accounts and then assign to report. That account will th' &|
   'en be used in the automated report criteria.'),AT(308,52,92,60),USE(?Prompt1),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('* Highlight the desired locations and then assign to report. That account will t' &|
   'hen be used in the automated report criteria.'),AT(584,50,92,60),USE(?Prompt1:3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Parts Usage  '),AT(548,62,20,60),USE(?String1:6),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           LINE,AT(412,46,0,293),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Goods Recvd'),AT(248,62,20,60),USE(?String1:7),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Engineer Perf.'),AT(236,60,20,60),USE(?String1:4),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           STRING('Booking Perf.'),AT(224,62,20,60),USE(?String1:3),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                           BUTTON,AT(312,182),USE(?Button3),TRN,FLAT,ICON('boorepp.jpg')
                           BUTTON,AT(312,213),USE(?Button4),TRN,FLAT,ICON('engrepp.jpg')
                           BUTTON,AT(580,120),USE(?Button6),TRN,FLAT,ICON('stoadjp.jpg')
                           BUTTON,AT(312,244),USE(?Button7),TRN,FLAT,ICON('goodrecp.jpg')
                           BUTTON,AT(312,275),USE(?Button8),TRN,FLAT,ICON('natstop.jpg')
                           BUTTON,AT(312,306),USE(?ButtonSMSRecvd),TRN,FLAT,ICON('SMSRecp.jpg')
                           PROMPT('X = Account To Be Used In Automated Criteria'),AT(312,346,88,24),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('X = Location To Be Used In Automated Criteria'),AT(580,346,88,24),USE(?Prompt4:2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(424,118,148,260),USE(?List2),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('120L(2)|M~Location~@s30@12C(2)|M~S~@s1@12C(2)|M~P~@s1@'),MARK(MarkQueue2.Flag2),FROM(MarkQueue2)
                           BUTTON,AT(580,151),USE(?Button9),TRN,FLAT,ICON('parusep.jpg')
                           BUTTON,AT(8,54),USE(?Button10),TRN,FLAT,ICON('clrselp.jpg')
                           BUTTON,AT(8,86),USE(?Button12),TRN,FLAT,ICON('selallp.jpg')
                           BUTTON,AT(424,54),USE(?Button10:2),TRN,FLAT,ICON('clrselp.jpg')
                           STRING('Stock Adjust.'),AT(528,62,20,60),USE(?String1:5),TRN,LEFT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),ANGLE(900)
                         END
                         TAB('Report Orders'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Daily Reports'),AT(8,54),USE(?Prompt5),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(8,66,212,136),USE(?List3),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('1020L(2)|M~Report Filename~@s255@'),FROM(DailyReportQueue)
                           PROMPT('Weekly Reports'),AT(232,54),USE(?Prompt5:2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(232,66,212,136),USE(?List4),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('1020L(2)|M~Report Filename~@s255@'),FROM(WeeklyReportQueue)
                           PROMPT('Monthly Reports'),AT(456,54),USE(?Prompt5:3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(456,66,212,136),USE(?List5),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('1020L(2)|M~Report Filename~@s255@'),FROM(MonthlyReportQueue)
                           BUTTON,AT(8,206),USE(?Button14),TRN,FLAT,ICON('addrepp.jpg')
                           BUTTON,AT(232,206),USE(?Button14:2),TRN,FLAT,ICON('addrepp.jpg')
                           BUTTON,AT(456,206),USE(?Button14:3),TRN,FLAT,ICON('addrepp.jpg')
                           BUTTON,AT(604,206),USE(?Button14:6),TRN,FLAT,ICON('delrepp.jpg')
                           PROMPT('The above reports will be run when "DAILY.EXE" is run.'),AT(24,236,184,22),USE(?Prompt8),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('The above reports will be run when "WEEKLY.EXE" is run.'),AT(248,236,184,22),USE(?Prompt8:2),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('The above reports will be run when "MONTHLY.EXE" is run.'),AT(472,234,184,22),USE(?Prompt8:3),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(376,206),USE(?Button14:5),TRN,FLAT,ICON('delrepp.jpg')
                           BUTTON,AT(152,206),USE(?Button14:4),TRN,FLAT,ICON('delrepp.jpg')
                         END
                       END
                       BUTTON,AT(604,384),USE(?Button11),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

BuildListAccount       Routine
    Clear(MarkQueue)
    Free(MarkQueue)
    Set(tra:Account_Number_Key)
    Loop
        If Access:TRADEACC.NEXT()
           Break
        End !If
        If tra:BranchIdentification = ''
            Cycle
        End !If tra:BranchIdentification = ''

        MarkQueue.AccountNumber = tra:Account_Number
        MarkQueue.CompanyName   = tra:Company_Name
        If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.TATReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.TATReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

        If GETINI('STATUSANALYSIS',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.StatusAnalysisReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.StatusAnalysisReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('BOOKINGPERFORMANCE',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.BookingPerformanceReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.BookingPerformanceReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('ENGINEERPERFORMANCE',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.EngineerPerformanceReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.EngineerPerformanceReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('GOODSRECEIVED',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.GoodsReceivedReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.GoodsReceivedReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('BACKORDER',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.BackorderReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.BackorderReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

        If GETINI('SMSRECEIVED',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.SMSRecivedReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.SMSRecivedReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True


        Add(MarkQueue)
    End !Loop

BuildListLocation       Routine

    Clear(MarkQueue2)
    Free(MarkQueue2)
    Set(loc:Location_Key)
    Loop
        If Access:LOCATION.NEXT()
           Break
        End !If
        MarkQueue2.SiteLocation = loc:Location
        If GETINI('STOCKADJUSTMENT',Clip(loc:Location),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue2.StockAdjustmentReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue2.StockAdjustmentReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

        If GETINI('PARTSUSAGE',Clip(loc:Location),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue2.PartsUsageReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue2.PartsUsageReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        Add(MarkQueue2)
    End !Loop
SaveListAccount        Routine
    Loop x# = 1 To Records(MarkQueue)
        Get(MarkQueue,x#)
        If MarkQueue.TATReport = 'X'
            PUTINI('TAT',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.TATReport = 'X'
            PUTINI('TAT',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.TATReport = 'X'

        If MarkQueue.StatusAnalysisReport = 'X'
            PUTINI('STATUSANALYSIS',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.StatusAnalysisReport = 'X'
            PUTINI('STATUSANALYSIS',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.StatusAnalysisReport = 'X'

        If MarkQueue.BookingPerformanceReport = 'X'
            PUTINI('BOOKINGPERFORMANCE',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.BookingPerformanceReport = 'X'
            PUTINI('BOOKINGPERFORMANCE',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.BookingPerformanceReport = 'X'

        If MarkQueue.EngineerPerformanceReport = 'X'
            PUTINI('ENGINEERPERFORMANCE',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.EngineerPerformanceReport = 'X'
            PUTINI('ENGINEERPERFORMANCE',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.EngineerPerformanceReport = 'X'

        If MarkQueue.GoodsReceivedReport = 'X'
            PUTINI('GOODSRECEIVED',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.GoodsReceivedReport = 'X'
            PUTINI('GOODSRECEIVED',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.GoodsReceivedReport = 'X'

        If MarkQueue.BackorderReport = 'X'
            PUTINI('BACKORDER',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.BackorderReport = 'X'
            PUTINI('BACKORDER',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.BackorderReport = 'X'

        If  MarkQueue.SMSRecivedReport = 'X'
            PUTINI('SMSRECEIVED',MarkQueue.AccountNumber,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            PUTINI('SMSRECEIVED',MarkQueue.AccountNumber,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True



    End !Loop x# = 1 To Records(MarkQueue)

SaveListLocation        Routine
    Loop x# = 1 To Records(MarkQueue2)
        Get(MarkQueue2,x#)
        If MarkQueue2.StockAdjustmentReport = 'X'
            PUTINI('STOCKADJUSTMENT',MarkQueue2.SiteLocation,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.TATReport = 'X'
            PUTINI('STOCKADJUSTMENT',MarkQueue2.SiteLocation,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.TATReport = 'X'

        If MarkQueue2.PartsUsageReport = 'X'
            PUTINI('PARTSUSAGE',MarkQueue2.SiteLocation,1,CLIP(Path()) & '\REPAUTO.INI')
        Else !If MarkQueue.TATReport = 'X'
            PUTINI('PARTSUSAGE',MarkQueue2.SiteLocation,0,CLIP(Path()) & '\REPAUTO.INI')
        End !If MarkQueue.TATReport = 'X'
    End !Loop x# = 1 To Records(MarkQueue2)
SelectAllACcount       Routine
    Do SaveListAccount

    Clear(MarkQueue)
    Free(MarkQueue)
    Set(tra:Account_Number_Key)
    Loop
        If Access:TRADEACC.NEXT()
           Break
        End !If
        If tra:BranchIdentification = ''
            Cycle
        End !If tra:BranchIdentification = ''

        MarkQueue.AccountNumber = tra:Account_Number
        MarkQueue.CompanyName   = tra:Company_Name
        If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.TATReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.TATReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

        If GETINI('STATUSANALYSIS',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.StatusAnalysisReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.StatusAnalysisReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('BOOKINGPERFORMANCE',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.BookingPerformanceReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.BookingPerformanceReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('ENGINEERPERFORMANCE',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.EngineerPerformanceReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.EngineerPerformanceReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('GOODSRECEIVED',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.GoodsReceivedReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.GoodsReceivedReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        
        If GETINI('BACKORDER',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.BackorderReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.BackorderReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

        If GETINI('SMSRECEIVED',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue.SMSRecivedReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue.SMSRecivedReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True


        MarkQueue.Flag = 1
        Add(MarkQueue)
    End !Loop
SelectAllLocation       Routine
    Do SaveListLocation

    Clear(MarkQueue2)
    Free(MarkQueue2)
    Set(loc:Location_Key)
    Loop
        If Access:LOCATION.NEXT()
           Break
        End !If
        MarkQueue2.SiteLocation = loc:Location
        If GETINI('STOCKADJUSTMENT',Clip(loc:Location),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue2.StockAdjustmentReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue2.StockAdjustmentReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True

        If GETINI('PARTSUSAGE',Clip(loc:Location),,CLIP(Path()) & '\REPAUTO.INI') = 1
            MarkQueue2.PartsUsageReport = 'X'
        Else !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
            MarkQueue2.PartsUsageReport = ''
        End !If GETINI('TAT',Clip(tra:Account_Number),,CLIP(Path()) & '\REPAUTO') = True
        MarkQueue2.Flag2 = 1
        Add(MarkQueue2)
    End !Loop

UpdateReportLists   Routine
    !Clear existing entries  (DBH: 06-05-2004)
    x# = 1
    Loop
         If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
            PUTINI('DAILYREPORT','EXE ' & x#,'',CLIP(Path()) & '\REPAUTO.INI')
         Else !If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
            Break
         End !If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
         x# += 1
    End !Loop

    Loop x# = 1 To Records(DailyReportQueue)
        Get(DailyReportQueue,x#)
        PUTINI('DAILYREPORT','EXE ' & x#,day:FileName,CLIP(Path()) & '\REPAUTO.INI')
    End !Loop x# = 1 To Records(DailyReportQueue)

    !Clear existing entries  (DBH: 06-05-2004)
    x# = 1
    Loop
         If GETINI('WEEKLYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
            PUTINI('WEEKLYREPORT','EXE ' & x#,'',CLIP(Path()) & '\REPAUTO.INI')
         Else !If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
            Break
         End !If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
         x# += 1
    End !Loop

    Loop x# = 1 To Records(WeeklyReportQueue)
        Get(WeeklyReportQueue,x#)
        PUTINI('WEEKLYREPORT','EXE ' & x#,week:FileName,CLIP(Path()) & '\REPAUTO.INI')
    End !Loop x# = 1 To Records(DailyReportQueue)

    !Clear existing entries  (DBH: 06-05-2004)
    x# = 1
    Loop
         If GETINI('MONTHLYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
            PUTINI('MONTHLYREPORT','EXE ' & x#,'',CLIP(Path()) & '\REPAUTO.INI')
         Else !If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
            Break
         End !If GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI') <> ''
         x# += 1
    End !Loop

    Loop x# = 1 To Records(MonthlyReportQueue)
        Get(MonthlyReportQueue,x#)
        PUTINI('MONTHLYREPORT','EXE ' & x#,month:FileName,CLIP(Path()) & '\REPAUTO.INI')
    End !Loop x# = 1 To Records(DailyReportQueue)
FillReportLists   Routine
    !Clear existing entries  (DBH: 06-05-2004)
    x# = 1
    Loop
        day:FileName = GETINI('DAILYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI')
        If day:FileName <> ''
            Add(DailyReportQueue)
        Else !If day:FileName <> ''
            Break
        End !If day:FileName <> ''
         x# += 1
    End !Loop

    !Clear existing entries  (DBH: 06-05-2004)
    x# = 1
    Loop
        week:FileName = GETINI('WEEKLYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI')
        If week:FileName <> ''
            Add(WeeklyReportQueue)
        Else !If day:FileName <> ''
            Break
        End !If day:FileName <> ''
         x# += 1
    End !Loop

    !Clear existing entries  (DBH: 06-05-2004)
    x# = 1
    Loop
        month:FileName = GETINI('MONTHLYREPORT','EXE ' & x#,,CLIP(Path()) & '\REPAUTO.INI')
        If month:FileName <> ''
            Add(MonthlyReportQueue)
        Else !If day:FileName <> ''
            Break
        End !If day:FileName <> ''
         x# += 1
    End !Loop

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020627'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  Do BuildListAccount
  Do BuildListLocation
  Do FillReportLists
  OPEN(window)
  SELF.Opened=True
  ?List1{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  ?List4{prop:vcr} = TRUE
  ?List5{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020627'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020627'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020627'&'0')
      ***
    OF ?Button12:2
      ThisWindow.Update
      Do SelectAllLocation
    OF ?Button1
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.TATReport = 'X'
                  MarkQueue.TATReport = ''
              Else !If MarkQueue.TATReport = 'X'
                  MarkQueue.TATReport = 'X'
              End !If MarkQueue.TATReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button2
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.StatusAnalysisReport = 'X'
                  MarkQueue.StatusAnalysisReport = ''
              Else !If MarkQueue.StatusAnalysisReport = 'X'
                  MarkQueue.StatusAnalysisReport = 'X'
              End !If MarkQueue.StatusAnalysisReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button3
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.BookingPerformanceReport = 'X'
                  MarkQueue.BookingPerformanceReport = ''
              Else !If MarkQueue.BookingPerformanceReport = 'X'
                  MarkQueue.BookingPerformanceReport = 'X'
              End !If MarkQueue.BookingPerformanceReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button4
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.EngineerPerformanceReport = 'X'
                  MarkQueue.EngineerPerformanceReport = ''
              Else !If MarkQueue.EngineerPerformanceReport = 'X'
                  MarkQueue.EngineerPerformanceReport = 'X'
              End !If MarkQueue.EngineerPerformanceReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button6
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue2)
          Get(MarkQueue2,x#)
          If MarkQueue2.Flag2 = True
              If MarkQueue2.StockAdjustmentReport = 'X'
                  MarkQueue2.StockAdjustmentReport = ''
              Else !If MarkQueue.TATReport = 'X'
                  MarkQueue2.StockAdjustmentReport = 'X'
              End !If MarkQueue.TATReport = 'X'
              Put(MarkQueue2)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button7
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.GoodsReceivedReport = 'X'
                  MarkQueue.GoodsReceivedReport = ''
              Else !If MarkQueue.GoodsRecievedReport = 'X'
                  MarkQueue.GoodsReceivedReport = 'X'
              End !If MarkQueue.GoodsRecievedReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button8
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.BackOrderReport = 'X'
                  MarkQueue.BackOrderReport = ''
              Else !If MarkQueue.BackOrderReport = 'X'
                  MarkQueue.BackOrderReport = 'X'
              End !If MarkQueue.BackOrderReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?ButtonSMSRecvd
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue)
          Get(MarkQueue,x#)
          If MarkQueue.Flag = True
              If MarkQueue.SMSRecivedReport = 'X'
                  MarkQueue.SMSRecivedReport = ''
              Else !If MarkQueue.BackOrderReport = 'X'
                  MarkQueue.SMSRecivedReport = 'X'
              End !If MarkQueue.BackOrderReport = 'X'
              Put(MarkQueue)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button9
      ThisWindow.Update
      Loop x# = 1 To Records(MarkQueue2)
          Get(MarkQueue2,x#)
          If MarkQueue2.Flag2 = True
              If MarkQueue2.PartsUsageReport = 'X'
                  MarkQueue2.PartsUsageReport = ''
              Else !If MarkQueue.TATReport = 'X'
                  MarkQueue2.PartsUsageReport = 'X'
              End !If MarkQueue.TATReport = 'X'
              Put(MarkQueue2)
          End !If MarkQueue.Flag = True
      End !x# = 1 To Records(Queue.Mark)
    OF ?Button10
      ThisWindow.Update
      Do SaveListAccount
      Do BUildListAccount
    OF ?Button12
      ThisWindow.Update
      Do SelectAllAccount
    OF ?Button10:2
      ThisWindow.Update
      Do SaveListLocation
      Do BUildListLocation
    OF ?Button14
      ThisWindow.Update
      local:FileName = ''
      filedialog ('Choose File',local:FileName,'EXE Files|*.EXE', |
                  file:keepdir + file:noerror + file:longname)
      
      Loop x# = Len(local:FileName) To 1 By -1
          If Sub(local:FileName,x#,1) = '\'
              Break
          End !If Sub(local:FileName,x#,1) = '\'
      End !x# = Len(local:FileName) To 1 By -1
      day:FileName = Sub(local:FileName,x#+1,60)
      Get(DailyReportQueue,day:FileName)
      If Error()
          Add(DailyReportQueue)
      End !Error()
    OF ?Button14:2
      ThisWindow.Update
      local:FileName = ''
      filedialog ('Choose File',local:FileName,'EXE Files|*.EXE', |
                  file:keepdir + file:noerror + file:longname)
      Loop x# = Len(local:FileName) To 1 By -1
          If Sub(local:FileName,x#,1) = '\'
              Break
          End !If Sub(local:FileName,x#,1) = '\'
      End !x# = Len(local:FileName) To 1 By -1
      week:FileName = Sub(local:FileName,x#+1,60)
      Get(WeeklyReportQueue,week:FileName)
      If Error()
          Add(WeeklyReportQueue)
      End !Error()
    OF ?Button14:3
      ThisWindow.Update
      local:FileName = ''
      filedialog ('Choose File',local:FileName,'EXE Files|*.EXE', |
                  file:keepdir + file:noerror + file:longname)
      Loop x# = Len(local:FileName) To 1 By -1
          If Sub(local:FileName,x#,1) = '\'
              Break
          End !If Sub(local:FileName,x#,1) = '\'
      End !x# = Len(local:FileName) To 1 By -1
      month:FileName = Sub(local:FileName,x#+1,60)
      Get(MonthlyReportQueue,month:FileName)
      If Error()
          Add(MonthlyReportQueue)
      End !Error()
    OF ?Button14:6
      ThisWindow.Update
      Case Message('Are you sure you want to delete the selected report?','ServiceBase 2000',|
                     icon:Question,'&Yes|&No',2,2) 
          Of 1 ! &Yes Button
              Get(MonthlyReportQueue,Choice(?List5))
              Delete(MonthlyReportQueue)
          Of 2 ! &No Button
      End!Case Message
      
    OF ?Button14:5
      ThisWindow.Update
      Case Message('Are you sure you want to delete the selected report?','ServiceBase 2000',|
                     icon:Question,'&Yes|&No',2,2) 
          Of 1 ! &Yes Button
              Get(WeeklyReportQueue,Choice(?List4))
              Delete(WeeklyReportQueue)
          Of 2 ! &No Button
      End!Case Message
      
    OF ?Button14:4
      ThisWindow.Update
      Case Message('Are you sure you want to delete the selected report?','ServiceBase 2000',|
                     icon:Question,'&Yes|&No',2,2) 
          Of 1 ! &Yes Button
              Get(DailyReportQueue,Choice(?List3))
              Delete(DailyReportQueue)
          Of 2 ! &No Button
      End!Case Message
    OF ?Button11
      ThisWindow.Update
      Do SaveListAccount
      Do SaveListLocation
      Do UpdateReportLists
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    !Colour Force Fields
        ?List1{prop:Color} = color:White
        ?List2{prop:Color} = color:White
        ?List3{prop:Color} = color:White
        ?List4{prop:Color} = color:White
        ?List5{prop:Color} = color:White
    Display()
