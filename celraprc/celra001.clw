

   MEMBER('celraprc.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABEIP.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


BrowseReportSchedule PROCEDURE                        !Generated from procedure template - Window

tmp:Frequency        STRING(30)
tmp:Filter           BYTE(1)
BRW5::View:Browse    VIEW(REPSCHED)
                       PROJECT(rpd:ReportName)
                       PROJECT(rpd:ReportCriteriaType)
                       PROJECT(rpd:Frequency)
                       PROJECT(rpd:LastReportDate)
                       PROJECT(rpd:LastReportTime)
                       PROJECT(rpd:NextReportDate)
                       PROJECT(rpd:NextReportTime)
                       PROJECT(rpd:MachineIP)
                       PROJECT(rpd:RecordNumber)
                       PROJECT(rpd:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rpd:ReportName         LIKE(rpd:ReportName)           !List box control field - type derived from field
rpd:ReportName_NormalFG LONG                          !Normal forground color
rpd:ReportName_NormalBG LONG                          !Normal background color
rpd:ReportName_SelectedFG LONG                        !Selected forground color
rpd:ReportName_SelectedBG LONG                        !Selected background color
rpd:ReportCriteriaType LIKE(rpd:ReportCriteriaType)   !List box control field - type derived from field
rpd:ReportCriteriaType_NormalFG LONG                  !Normal forground color
rpd:ReportCriteriaType_NormalBG LONG                  !Normal background color
rpd:ReportCriteriaType_SelectedFG LONG                !Selected forground color
rpd:ReportCriteriaType_SelectedBG LONG                !Selected background color
rpd:Frequency          LIKE(rpd:Frequency)            !List box control field - type derived from field
rpd:Frequency_NormalFG LONG                           !Normal forground color
rpd:Frequency_NormalBG LONG                           !Normal background color
rpd:Frequency_SelectedFG LONG                         !Selected forground color
rpd:Frequency_SelectedBG LONG                         !Selected background color
tmp:Frequency          LIKE(tmp:Frequency)            !List box control field - type derived from local data
tmp:Frequency_NormalFG LONG                           !Normal forground color
tmp:Frequency_NormalBG LONG                           !Normal background color
tmp:Frequency_SelectedFG LONG                         !Selected forground color
tmp:Frequency_SelectedBG LONG                         !Selected background color
rpd:LastReportDate     LIKE(rpd:LastReportDate)       !List box control field - type derived from field
rpd:LastReportDate_NormalFG LONG                      !Normal forground color
rpd:LastReportDate_NormalBG LONG                      !Normal background color
rpd:LastReportDate_SelectedFG LONG                    !Selected forground color
rpd:LastReportDate_SelectedBG LONG                    !Selected background color
rpd:LastReportTime     LIKE(rpd:LastReportTime)       !List box control field - type derived from field
rpd:LastReportTime_NormalFG LONG                      !Normal forground color
rpd:LastReportTime_NormalBG LONG                      !Normal background color
rpd:LastReportTime_SelectedFG LONG                    !Selected forground color
rpd:LastReportTime_SelectedBG LONG                    !Selected background color
rpd:NextReportDate     LIKE(rpd:NextReportDate)       !List box control field - type derived from field
rpd:NextReportDate_NormalFG LONG                      !Normal forground color
rpd:NextReportDate_NormalBG LONG                      !Normal background color
rpd:NextReportDate_SelectedFG LONG                    !Selected forground color
rpd:NextReportDate_SelectedBG LONG                    !Selected background color
rpd:NextReportTime     LIKE(rpd:NextReportTime)       !List box control field - type derived from field
rpd:NextReportTime_NormalFG LONG                      !Normal forground color
rpd:NextReportTime_NormalBG LONG                      !Normal background color
rpd:NextReportTime_SelectedFG LONG                    !Selected forground color
rpd:NextReportTime_SelectedBG LONG                    !Selected background color
rpd:MachineIP          LIKE(rpd:MachineIP)            !List box control field - type derived from field
rpd:MachineIP_NormalFG LONG                           !Normal forground color
rpd:MachineIP_NormalBG LONG                           !Normal background color
rpd:MachineIP_SelectedFG LONG                         !Selected forground color
rpd:MachineIP_SelectedBG LONG                         !Selected background color
rpd:RecordNumber       LIKE(rpd:RecordNumber)         !Primary key field - type derived from field
rpd:Active             LIKE(rpd:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Report Schedule'),AT(68,58),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,68,544,256),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('126L(2)|M*~Report Name~@s60@102L(2)|M*~Criteria Type~@s60@0L(2)|M*~Frequency~@n1' &|
   '@48L(2)|M*~Frequency~@s30@[48R(2)|M*~Date~@d6@39R(2)|M*~Time~@t1b@]|M~Last Time ' &|
   'Report Run~[58R(2)|M*~Date~@d6@35R(2)|M*~Time~@t1b@]|M~Next Report Due~60L(2)|M*' &|
   '~Machine IP~@s15@'),FROM(Queue:Browse)
                           BUTTON,AT(400,334),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(472,334),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                           BUTTON,AT(544,334),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                           PANEL,AT(272,340,8,8),USE(?Panel5:2),FILL(COLOR:Green)
                           PROMPT('- Schedule Due Today'),AT(284,340),USE(?Prompt5)
                           PANEL,AT(272,352,8,8),USE(?Panel5:3),FILL(COLOR:Silver)
                           PROMPT('- Inactive / Completed'),AT(284,352),USE(?Prompt6)
                           OPTION('Filter Browse'),AT(68,332,196,24),USE(tmp:Filter),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Active Only'),AT(76,343),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Inactive Only'),AT(145,343),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('All'),AT(220,343),USE(?Option1:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('9')
                           END
                           PANEL,AT(272,328,8,8),USE(?Panel5),FILL(COLOR:Red)
                           PROMPT('- Schedule Missed'),AT(284,328),USE(?Prompt4)
                         END
                       END
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Report Scheduler'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(544,366),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(472,366),USE(?Button:ScheduleLog),TRN,FLAT,ICON('schedp.jpg')
                       BUTTON,AT(68,366),USE(?Button6),TRN,FLAT,ICON('repcrip.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5::Sort1:Locator  StepLocatorClass                 !Conditional Locator - tmp:Filter = 9
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020691'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseReportSchedule')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:REPSCHED.Open
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:REPSCHED,SELF)
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseReportSchedule')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,rpd:NextReportDateKey)
  BRW5.AddLocator(BRW5::Sort1:Locator)
  BRW5::Sort1:Locator.Init(,rpd:NextReportDate,1,BRW5)
  BRW5.AddSortOrder(,rpd:ActiveDateKey)
  BRW5.AddRange(rpd:Active,tmp:Filter)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,rpd:NextReportDate,1,BRW5)
  BIND('tmp:Frequency',tmp:Frequency)
  BRW5.AddField(rpd:ReportName,BRW5.Q.rpd:ReportName)
  BRW5.AddField(rpd:ReportCriteriaType,BRW5.Q.rpd:ReportCriteriaType)
  BRW5.AddField(rpd:Frequency,BRW5.Q.rpd:Frequency)
  BRW5.AddField(tmp:Frequency,BRW5.Q.tmp:Frequency)
  BRW5.AddField(rpd:LastReportDate,BRW5.Q.rpd:LastReportDate)
  BRW5.AddField(rpd:LastReportTime,BRW5.Q.rpd:LastReportTime)
  BRW5.AddField(rpd:NextReportDate,BRW5.Q.rpd:NextReportDate)
  BRW5.AddField(rpd:NextReportTime,BRW5.Q.rpd:NextReportTime)
  BRW5.AddField(rpd:MachineIP,BRW5.Q.rpd:MachineIP)
  BRW5.AddField(rpd:RecordNumber,BRW5.Q.rpd:RecordNumber)
  BRW5.AddField(rpd:Active,BRW5.Q.rpd:Active)
  BRW5.AskProcedure = 1
  BRW5.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPSCHED.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseReportSchedule')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateReportSchedule
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Filter
      BRW5.ResetSort(1)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020691'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020691'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020691'&'0')
      ***
    OF ?Button:ScheduleLog
      ThisWindow.Update
      BrowseReportLog(brw5.q.rpd:RecordNumber)
      ThisWindow.Reset
    OF ?Button6
      ThisWindow.Update
      BrowseReportCriterias
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      BRW5.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:Filter = 9
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  CASE (rpd:Frequency)
  OF 1
    tmp:Frequency = 'Run Once'
  OF 2
    tmp:Frequency = 'Daily'
  OF 3
    tmp:Frequency = 'Weekly'
  OF 4
    tmp:Frequency = 'Monthly'
  ELSE
    tmp:Frequency = 'Not Set'
  END
  PARENT.SetQueueRecord
  SELF.Q.rpd:ReportName_NormalFG = -1
  SELF.Q.rpd:ReportName_NormalBG = -1
  SELF.Q.rpd:ReportName_SelectedFG = -1
  SELF.Q.rpd:ReportName_SelectedBG = -1
  SELF.Q.rpd:ReportCriteriaType_NormalFG = -1
  SELF.Q.rpd:ReportCriteriaType_NormalBG = -1
  SELF.Q.rpd:ReportCriteriaType_SelectedFG = -1
  SELF.Q.rpd:ReportCriteriaType_SelectedBG = -1
  SELF.Q.rpd:Frequency_NormalFG = -1
  SELF.Q.rpd:Frequency_NormalBG = -1
  SELF.Q.rpd:Frequency_SelectedFG = -1
  SELF.Q.rpd:Frequency_SelectedBG = -1
  SELF.Q.tmp:Frequency_NormalFG = -1
  SELF.Q.tmp:Frequency_NormalBG = -1
  SELF.Q.tmp:Frequency_SelectedFG = -1
  SELF.Q.tmp:Frequency_SelectedBG = -1
  SELF.Q.rpd:LastReportDate_NormalFG = -1
  SELF.Q.rpd:LastReportDate_NormalBG = -1
  SELF.Q.rpd:LastReportDate_SelectedFG = -1
  SELF.Q.rpd:LastReportDate_SelectedBG = -1
  SELF.Q.rpd:LastReportTime_NormalFG = -1
  SELF.Q.rpd:LastReportTime_NormalBG = -1
  SELF.Q.rpd:LastReportTime_SelectedFG = -1
  SELF.Q.rpd:LastReportTime_SelectedBG = -1
  SELF.Q.rpd:NextReportDate_NormalFG = -1
  SELF.Q.rpd:NextReportDate_NormalBG = -1
  SELF.Q.rpd:NextReportDate_SelectedFG = -1
  SELF.Q.rpd:NextReportDate_SelectedBG = -1
  SELF.Q.rpd:NextReportTime_NormalFG = -1
  SELF.Q.rpd:NextReportTime_NormalBG = -1
  SELF.Q.rpd:NextReportTime_SelectedFG = -1
  SELF.Q.rpd:NextReportTime_SelectedBG = -1
  SELF.Q.rpd:MachineIP_NormalFG = -1
  SELF.Q.rpd:MachineIP_NormalBG = -1
  SELF.Q.rpd:MachineIP_SelectedFG = -1
  SELF.Q.rpd:MachineIP_SelectedBG = -1
  SELF.Q.tmp:Frequency = tmp:Frequency                !Assign formula result to display queue
   
   
   IF (rpd:Active = 0)
     SELF.Q.rpd:ReportName_NormalFG = 8421504
     SELF.Q.rpd:ReportName_NormalBG = -1
     SELF.Q.rpd:ReportName_SelectedFG = -1
     SELF.Q.rpd:ReportName_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:ReportName_NormalFG = 255
     SELF.Q.rpd:ReportName_NormalBG = -1
     SELF.Q.rpd:ReportName_SelectedFG = -1
     SELF.Q.rpd:ReportName_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:ReportName_NormalFG = 32768
     SELF.Q.rpd:ReportName_NormalBG = -1
     SELF.Q.rpd:ReportName_SelectedFG = -1
     SELF.Q.rpd:ReportName_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:ReportName_NormalFG = -1
     SELF.Q.rpd:ReportName_NormalBG = -1
     SELF.Q.rpd:ReportName_SelectedFG = -1
     SELF.Q.rpd:ReportName_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:ReportCriteriaType_NormalFG = 8421504
     SELF.Q.rpd:ReportCriteriaType_NormalBG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedFG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:ReportCriteriaType_NormalFG = 255
     SELF.Q.rpd:ReportCriteriaType_NormalBG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedFG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:ReportCriteriaType_NormalFG = 32768
     SELF.Q.rpd:ReportCriteriaType_NormalBG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedFG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:ReportCriteriaType_NormalFG = -1
     SELF.Q.rpd:ReportCriteriaType_NormalBG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedFG = -1
     SELF.Q.rpd:ReportCriteriaType_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:Frequency_NormalFG = 8421504
     SELF.Q.rpd:Frequency_NormalBG = -1
     SELF.Q.rpd:Frequency_SelectedFG = -1
     SELF.Q.rpd:Frequency_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:Frequency_NormalFG = 255
     SELF.Q.rpd:Frequency_NormalBG = -1
     SELF.Q.rpd:Frequency_SelectedFG = -1
     SELF.Q.rpd:Frequency_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:Frequency_NormalFG = 32768
     SELF.Q.rpd:Frequency_NormalBG = -1
     SELF.Q.rpd:Frequency_SelectedFG = -1
     SELF.Q.rpd:Frequency_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:Frequency_NormalFG = -1
     SELF.Q.rpd:Frequency_NormalBG = -1
     SELF.Q.rpd:Frequency_SelectedFG = -1
     SELF.Q.rpd:Frequency_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.tmp:Frequency_NormalFG = 8421504
     SELF.Q.tmp:Frequency_NormalBG = -1
     SELF.Q.tmp:Frequency_SelectedFG = -1
     SELF.Q.tmp:Frequency_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.tmp:Frequency_NormalFG = 255
     SELF.Q.tmp:Frequency_NormalBG = -1
     SELF.Q.tmp:Frequency_SelectedFG = -1
     SELF.Q.tmp:Frequency_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.tmp:Frequency_NormalFG = 32768
     SELF.Q.tmp:Frequency_NormalBG = -1
     SELF.Q.tmp:Frequency_SelectedFG = -1
     SELF.Q.tmp:Frequency_SelectedBG = 32768
   ELSE
     SELF.Q.tmp:Frequency_NormalFG = -1
     SELF.Q.tmp:Frequency_NormalBG = -1
     SELF.Q.tmp:Frequency_SelectedFG = -1
     SELF.Q.tmp:Frequency_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:LastReportDate_NormalFG = 8421504
     SELF.Q.rpd:LastReportDate_NormalBG = -1
     SELF.Q.rpd:LastReportDate_SelectedFG = -1
     SELF.Q.rpd:LastReportDate_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:LastReportDate_NormalFG = 255
     SELF.Q.rpd:LastReportDate_NormalBG = -1
     SELF.Q.rpd:LastReportDate_SelectedFG = -1
     SELF.Q.rpd:LastReportDate_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:LastReportDate_NormalFG = 32768
     SELF.Q.rpd:LastReportDate_NormalBG = -1
     SELF.Q.rpd:LastReportDate_SelectedFG = -1
     SELF.Q.rpd:LastReportDate_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:LastReportDate_NormalFG = -1
     SELF.Q.rpd:LastReportDate_NormalBG = -1
     SELF.Q.rpd:LastReportDate_SelectedFG = -1
     SELF.Q.rpd:LastReportDate_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:LastReportTime_NormalFG = 8421504
     SELF.Q.rpd:LastReportTime_NormalBG = -1
     SELF.Q.rpd:LastReportTime_SelectedFG = -1
     SELF.Q.rpd:LastReportTime_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:LastReportTime_NormalFG = 255
     SELF.Q.rpd:LastReportTime_NormalBG = -1
     SELF.Q.rpd:LastReportTime_SelectedFG = -1
     SELF.Q.rpd:LastReportTime_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:LastReportTime_NormalFG = 32768
     SELF.Q.rpd:LastReportTime_NormalBG = -1
     SELF.Q.rpd:LastReportTime_SelectedFG = -1
     SELF.Q.rpd:LastReportTime_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:LastReportTime_NormalFG = -1
     SELF.Q.rpd:LastReportTime_NormalBG = -1
     SELF.Q.rpd:LastReportTime_SelectedFG = -1
     SELF.Q.rpd:LastReportTime_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:NextReportDate_NormalFG = 8421504
     SELF.Q.rpd:NextReportDate_NormalBG = -1
     SELF.Q.rpd:NextReportDate_SelectedFG = -1
     SELF.Q.rpd:NextReportDate_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:NextReportDate_NormalFG = 255
     SELF.Q.rpd:NextReportDate_NormalBG = -1
     SELF.Q.rpd:NextReportDate_SelectedFG = -1
     SELF.Q.rpd:NextReportDate_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:NextReportDate_NormalFG = 32768
     SELF.Q.rpd:NextReportDate_NormalBG = -1
     SELF.Q.rpd:NextReportDate_SelectedFG = -1
     SELF.Q.rpd:NextReportDate_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:NextReportDate_NormalFG = -1
     SELF.Q.rpd:NextReportDate_NormalBG = -1
     SELF.Q.rpd:NextReportDate_SelectedFG = -1
     SELF.Q.rpd:NextReportDate_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:NextReportTime_NormalFG = 8421504
     SELF.Q.rpd:NextReportTime_NormalBG = -1
     SELF.Q.rpd:NextReportTime_SelectedFG = -1
     SELF.Q.rpd:NextReportTime_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:NextReportTime_NormalFG = 255
     SELF.Q.rpd:NextReportTime_NormalBG = -1
     SELF.Q.rpd:NextReportTime_SelectedFG = -1
     SELF.Q.rpd:NextReportTime_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:NextReportTime_NormalFG = 32768
     SELF.Q.rpd:NextReportTime_NormalBG = -1
     SELF.Q.rpd:NextReportTime_SelectedFG = -1
     SELF.Q.rpd:NextReportTime_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:NextReportTime_NormalFG = -1
     SELF.Q.rpd:NextReportTime_NormalBG = -1
     SELF.Q.rpd:NextReportTime_SelectedFG = -1
     SELF.Q.rpd:NextReportTime_SelectedBG = -1
   END
   IF (rpd:Active = 0)
     SELF.Q.rpd:MachineIP_NormalFG = 8421504
     SELF.Q.rpd:MachineIP_NormalBG = -1
     SELF.Q.rpd:MachineIP_SelectedFG = -1
     SELF.Q.rpd:MachineIP_SelectedBG = 8421504
   ELSIF(rpd:NextReportDate < Today())
     SELF.Q.rpd:MachineIP_NormalFG = 255
     SELF.Q.rpd:MachineIP_NormalBG = -1
     SELF.Q.rpd:MachineIP_SelectedFG = -1
     SELF.Q.rpd:MachineIP_SelectedBG = 255
   ELSIF(rpd:NextReportDate = Today())
     SELF.Q.rpd:MachineIP_NormalFG = 32768
     SELF.Q.rpd:MachineIP_NormalBG = -1
     SELF.Q.rpd:MachineIP_SelectedFG = -1
     SELF.Q.rpd:MachineIP_SelectedBG = 32768
   ELSE
     SELF.Q.rpd:MachineIP_NormalFG = -1
     SELF.Q.rpd:MachineIP_NormalBG = -1
     SELF.Q.rpd:MachineIP_SelectedFG = -1
     SELF.Q.rpd:MachineIP_SelectedBG = -1
   END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','XFiles')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','XFiles')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateReportScheduleCriteria PROCEDURE                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::10:TAGFLAG         BYTE(0)
DASBRW::10:TAGMOUSE        BYTE(0)
DASBRW::10:TAGDISPSTATUS   BYTE(0)
DASBRW::10:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer3                      LIKE(glo:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::15:TAGFLAG         BYTE(0)
DASBRW::15:TAGMOUSE        BYTE(0)
DASBRW::15:TAGDISPSTATUS   BYTE(0)
DASBRW::15:QUEUE          QUEUE
Pointer4                      LIKE(glo:Pointer4)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::22:TAGFLAG         BYTE(0)
DASBRW::22:TAGMOUSE        BYTE(0)
DASBRW::22:TAGDISPSTATUS   BYTE(0)
DASBRW::22:QUEUE          QUEUE
Pointer5                      LIKE(glo:Pointer5)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::24:TAGFLAG         BYTE(0)
DASBRW::24:TAGMOUSE        BYTE(0)
DASBRW::24:TAGDISPSTATUS   BYTE(0)
DASBRW::24:QUEUE          QUEUE
Pointer6                      LIKE(glo:Pointer6)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::26:TAGFLAG         BYTE(0)
DASBRW::26:TAGMOUSE        BYTE(0)
DASBRW::26:TAGDISPSTATUS   BYTE(0)
DASBRW::26:QUEUE          QUEUE
Pointer7                      LIKE(glo:Pointer7)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::32:TAGFLAG         BYTE(0)
DASBRW::32:TAGMOUSE        BYTE(0)
DASBRW::32:TAGDISPSTATUS   BYTE(0)
DASBRW::32:QUEUE          QUEUE
SiteLocation                  LIKE(glo:SiteLocation)
ShelfLocation                 LIKE(glo:ShelfLocation)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ActionMessage        CSTRING(40)
Tag:AccountNumber    STRING(1)
Tag:Manufacturer     STRING(1)
Tag:ChargeType       STRING(1)
Tag:ModelNumber      STRING(1)
Tag:StockType        STRING(1)
tmp:TempFilePath     CSTRING(255)
tmp:Warranty         STRING(3)
Tag:AccountNumber2   STRING(1)
Tag:Location         STRING(1)
tmp:Yes              STRING('YES')
tmp:AccountListFilter STRING(255)
Tag:ShelfLocation    STRING(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?rpc:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?rpc:ThirdPartySite
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?rpc:SiteLocation
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
Tag:AccountNumber      LIKE(Tag:AccountNumber)        !List box control field - type derived from local data
Tag:AccountNumber_Icon LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
Tag:Manufacturer       LIKE(Tag:Manufacturer)         !List box control field - type derived from local data
Tag:Manufacturer_Icon  LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
Tag:ChargeType         LIKE(Tag:ChargeType)           !List box control field - type derived from local data
Tag:ChargeType_Icon    LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(MODELPART)
                       PROJECT(model:ModelNumber)
                       PROJECT(model:PartNumber1)
                       PROJECT(model:PartNumber2)
                       PROJECT(model:PartNumber3)
                       PROJECT(model:PartNumber4)
                       PROJECT(model:PartNumber5)
                       PROJECT(model:Manufacturer)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
Tag:ModelNumber        LIKE(Tag:ModelNumber)          !List box control field - type derived from local data
Tag:ModelNumber_Icon   LONG                           !Entry's icon ID
model:ModelNumber      LIKE(model:ModelNumber)        !List box control field - type derived from field
model:PartNumber1      LIKE(model:PartNumber1)        !List box control field - type derived from field
model:PartNumber2      LIKE(model:PartNumber2)        !List box control field - type derived from field
model:PartNumber3      LIKE(model:PartNumber3)        !List box control field - type derived from field
model:PartNumber4      LIKE(model:PartNumber4)        !List box control field - type derived from field
model:PartNumber5      LIKE(model:PartNumber5)        !List box control field - type derived from field
model:Manufacturer     LIKE(model:Manufacturer)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                       PROJECT(sub:Main_Account_Number)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
Tag:AccountNumber2     LIKE(Tag:AccountNumber2)       !List box control field - type derived from local data
Tag:AccountNumber2_Icon LONG                          !Entry's icon ID
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
sub:Main_Account_Number LIKE(sub:Main_Account_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW23::View:Browse   VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:6
Tag:Location           LIKE(Tag:Location)             !List box control field - type derived from local data
Tag:Location_Icon      LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW25::View:Browse   VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse:6       QUEUE                            !Queue declaration for browse/combo box using ?List:7
Tag:StockType          LIKE(Tag:StockType)            !List box control field - type derived from local data
Tag:StockType_Icon     LONG                           !Entry's icon ID
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW31::View:Browse   VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:RecordNumber)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse:7       QUEUE                            !Queue declaration for browse/combo box using ?List:8
Tag:ShelfLocation      LIKE(Tag:ShelfLocation)        !List box control field - type derived from local data
Tag:ShelfLocation_Icon LONG                           !Entry's icon ID
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:RecordNumber       LIKE(los:RecordNumber)         !Primary key field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB18::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB27::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
FDCB28::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Criteria Type'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(476,366),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       SHEET,AT(292,54,324,310),USE(?Sheet1),SPREAD
                         TAB('T1'),USE(?Tab:AccountNumbers),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Accounts'),AT(296,84),USE(rpc:AllAccounts),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           LIST,AT(296,96,316,182),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@68L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(296,284),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(364,284),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('&Rev tags'),AT(349,192,50,13),USE(?DASREVTAG),HIDE
                           BUTTON,AT(432,284),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON('sho&W tags'),AT(353,224,70,13),USE(?DASSHOWTAG),HIDE
                         END
                         TAB('T2'),USE(?Tab:Manufacturers),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select which manufacturers to exclude.'),AT(296,72),USE(?Prompt:ExcludeManufacturers),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Manufacturers'),AT(296,84),USE(rpc:AllManufacturers),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                           LIST,AT(296,96,200,184),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(296,284),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(364,284),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('&Rev tags'),AT(371,192,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON,AT(432,284),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON('sho&W tags'),AT(371,210,70,13),USE(?DASSHOWTAG:2),HIDE
                         END
                         TAB('T3'),USE(?Tab:ChargeTypes),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(295,84),USE(rpc:AllChargeTypes),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           LIST,AT(296,96,208,182),USE(?List:3),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11LJ@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse:2)
                           BUTTON,AT(296,284),USE(?DASTAG:3),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(364,284),USE(?DASTAGAll:3),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('&Rev tags'),AT(363,176,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(359,202,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON,AT(432,284),USE(?DASUNTAGALL:3),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('T4'),USE(?Tab:PartNumbers),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select which Model Numbers / Part Numbers to exclude'),AT(296,70),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select Manufacturer'),AT(297,86),USE(rpc:SelectManufacturer),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select Manufacturer'),TIP('Select Manufacturer'),VALUE('1','0')
                           COMBO(@s20),AT(392,86,124,10),USE(rpc:Manufacturer),IMM,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~Manufacturer~@s30@'),DROP(5),FROM(Queue:FileDropCombo),MSG('Manufacturer')
                           ENTRY(@s30),AT(296,102,124,10),USE(model:ModelNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Model Number'),TIP('Model Number'),UPR
                           LIST,AT(296,116,316,158),USE(?List:4),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@80L(2)|_~Model Number~@s30@80L(2)|_~Part Number 1~@s30@80L(2)|_~Part ' &|
   'Number 2~@s30@80L(2)|_~Part Number 3~@s30@80L(2)|_~Part Number 4~@s30@80L(2)|_~P' &|
   'art Number 5~@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Insert'),AT(334,138,42,12),USE(?Insert),HIDE
                           BUTTON('&Change'),AT(378,138,42,12),USE(?Change),HIDE
                           BUTTON('&Delete'),AT(422,138,42,12),USE(?Delete),HIDE
                           BUTTON('&Rev tags'),AT(409,183,50,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(409,202,70,13),USE(?DASSHOWTAG:4),HIDE
                           BUTTON,AT(296,284),USE(?DASTAG:4),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(364,284),USE(?DASTAGAll:4),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(432,284),USE(?DASUNTAGALL:4),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('T5'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Report Style'),AT(296,72,200,28),USE(rpc:CharIncReportStyle),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('ChaInc Report Style')
                             RADIO('Chargeable Non-Invoiced'),AT(305,84),USE(?rpc:CharIncReportStyle:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Invoiced Only'),AT(420,84),USE(?rpc:CharIncReportStyle:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION('Zero Suppression'),AT(296,104,200,28),USE(rpc:CharIncZeroSuppression),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('ChaInc Zero Suppression')
                             RADIO('Supress Zeros'),AT(304,116),USE(?rpc:CharIncZeroSuppression:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Show Zeros'),AT(420,116),USE(?rpc:CharIncZeroSuppression:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION('Report Order'),AT(296,136,200,28),USE(?Option4),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Invoice Number'),AT(304,148),USE(?Option4:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Job Number'),AT(420,146),USE(?Option4:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                         END
                         TAB('T6'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Warranty Status'),AT(296,70,232,36),USE(?Group:WarrantyStatus),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('All'),AT(301,80),USE(rpc:WarIncAll),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarIncAll'),TIP('WarIncAll'),VALUE('1','0')
                             CHECK('Resubmitted'),AT(388,80),USE(rpc:WarIncResubmitted),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Resubmitted'),TIP('WarInc Resubmitted'),VALUE('1','0')
                             CHECK('Rejection Acknowledged'),AT(328,94),USE(rpc:WarIncRejectionAcknowledged),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Rejection Acknowledged'),TIP('WarInc Rejection Acknowledged'),VALUE('1','0')
                             CHECK('Submitted'),AT(328,80),USE(rpc:WarIncSubmitted),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Submitted'),TIP('WarInc Submitted'),VALUE('1','0')
                             CHECK('Rejected'),AT(460,80),USE(rpc:WarIncRejected),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Rejected'),TIP('WarInc Rejected'),VALUE('1','0')
                             CHECK('Reconciled'),AT(460,94),USE(rpc:WarIncReconciled),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Reconciled'),TIP('WarInc Reconciled'),VALUE('1','0')
                           END
                           OPTION('Zero Suppression'),AT(296,128,200,28),USE(rpc:WarIncSuppressZeros),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Supress Zeros')
                             RADIO('Supress Zeros'),AT(304,141),USE(?rpc:CharIncZeroSuppression:Radio1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Show Zeros'),AT(424,141),USE(?rpc:CharIncZeroSuppression:Radio2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           OPTION('Report Order'),AT(296,160,268,32),USE(rpc:WarIncReportOrder),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Report Order')
                             RADIO('By Manufacturer'),AT(304,174),USE(?rpc:WarIncReportOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Job Number'),AT(392,174),USE(?rpc:WarIncReportOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('By Date Completed'),AT(472,174),USE(?rpc:WarIncReportOrder:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           CHECK('Include ARC Jobs'),AT(444,112),USE(rpc:WarIncIncludeARCRepairedJobs),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Include ARC Repaired Jobs'),TIP('WarInc Include ARC Repaired Jobs'),VALUE('1','0')
                           CHECK('Include ARC Repaired RRC Jobs'),AT(296,112),USE(rpc:IncludeARCRepairedRRCJobs),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Include ARC Repaired RRC Jobs'),TIP('Include ARC Repaired RRC Jobs'),VALUE('1','0')
                         END
                         TAB,USE(?Tab:NoCriteria),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('T7'),USE(?Tab:SubAccounts),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Accounts'),AT(296,86),USE(rpc:AllAccounts,,?rpc:AllAccounts:2),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Accounts'),TIP('All Accounts'),VALUE('1','0')
                           LIST,AT(296,98,316,182),USE(?List:5),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@66L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse:4)
                           BUTTON,AT(296,286),USE(?DASTAG:5),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(347,148,50,13),USE(?DASREVTAG:5),HIDE
                           BUTTON,AT(364,286),USE(?DASTAGAll:5),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('sho&W tags'),AT(349,173,70,13),USE(?DASSHOWTAG:5),HIDE
                           BUTTON,AT(432,286),USE(?DASUNTAGALL:5),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('T8'),USE(?Tab:Locations),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Locations'),AT(296,86),USE(rpc:AllLocations),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Locations'),TIP('All Locations'),VALUE('1','0')
                           LIST,AT(296,98,208,178),USE(?List:6),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse:5)
                           BUTTON,AT(296,284),USE(?DASTAG:6),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON('&Rev tags'),AT(385,146,50,13),USE(?DASREVTAG:6),HIDE
                           BUTTON,AT(364,284),USE(?DASTAGAll:6),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON('sho&W tags'),AT(385,168,70,13),USE(?DASSHOWTAG:6),HIDE
                           BUTTON,AT(432,284),USE(?DASUNTAGALL:6),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('T9'),USE(?Tab:StockTypes),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Stock Types'),AT(296,86),USE(rpc:AllStockTypes),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Stock Types'),TIP('All Stock Types'),VALUE('1','0')
                           LIST,AT(296,98,208,178),USE(?List:7),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:6)
                           BUTTON('&Rev tags'),AT(365,134,50,13),USE(?DASREVTAG:7),HIDE
                           BUTTON('sho&W tags'),AT(385,172,70,13),USE(?DASSHOWTAG:7),HIDE
                           BUTTON,AT(296,284),USE(?DASTAG:7),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(364,284),USE(?DASTAGAll:7),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(432,284),USE(?DASUNTAGALL:7),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('T10'),USE(?Tab:ShelfLocations),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(296,102,208,178),USE(?List:8),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J~T~@s1@120L(2)|M~Shelf Location~@s30@'),FROM(Queue:Browse:7)
                           BUTTON('&Rev tags'),AT(360,162,1,1),USE(?DASREVTAG:8),HIDE
                           BUTTON('sho&W tags'),AT(368,180,1,1),USE(?DASSHOWTAG:8),HIDE
                           BUTTON,AT(296,284),USE(?DASTAG:8),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(368,284),USE(?DASTAGAll:8),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(440,284),USE(?DASUNTAGALL:8),TRN,FLAT,ICON('untagalp.jpg')
                         END
                         TAB('T 11'),USE(?Tab:11),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select All'),AT(296,72),USE(rpc:WarIncAll,,?rpc:WarIncAll:2),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Select All'),TIP('Select All'),VALUE('1','0')
                           GROUP('Warranty Status'),AT(296,86,316,50),USE(?Group:WarrantyStatus:2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Pending'),AT(303,100),USE(rpc:WarIncSubmitted,,?rpc:WarIncSubmitted:2),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Pending'),TIP('Pending'),VALUE('1','0')
                             CHECK('Approved'),AT(364,100),USE(rpc:WarIncResubmitted,,?rpc:WarIncResubmitted:2),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Approved'),TIP('Approved'),VALUE('1','0')
                             CHECK('Query'),AT(464,100),USE(rpc:WarIncRejected,,?rpc:WarIncRejected:2),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Query'),TIP('Query'),VALUE('1','0')
                             CHECK('On Technical Report'),AT(516,100),USE(rpc:WarIncRejectionAcknowledged,,?rpc:WarIncRejectionAcknowledg),TRN,FONT(,,COLOR:White,FONT:bold),MSG('On Technical Report'),TIP('On Technical Report'),VALUE('1','0')
                             CHECK('Rejected'),AT(304,116),USE(rpc:WarIncReconciled,,?rpc:WarIncReconciled:2),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Rejected'),TIP('Rejected'),VALUE('1','0')
                             CHECK('Submitted To MFTR'),AT(364,116),USE(rpc:WarIncIncludeARCRepairedJobs,,?rpc:WarIncIncludeARCRepaired),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Submitted To MFTR'),TIP('Submitted To MFTR'),VALUE('1','0')
                             CHECK('MFTR Paid'),AT(516,116),USE(rpc:WarIncSuppressZeros,,?rpc:WarIncSuppressZeros:3),TRN,FONT(,,COLOR:White,FONT:bold),MSG('MFTR Paid'),TIP('MFTR Paid'),VALUE('1','0')
                           END
                           OPTION('Date Range Type'),AT(356,150,200,62),USE(rpc:WarIncReportOrder,,?rpc:WarIncReportOrder:3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Report Order')
                             RADIO('Date Completed Date Range'),AT(396,162),USE(?tmp:DateRangeType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Claim Submitted Date Range'),AT(396,178),USE(?tmp:DateRangeType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Claim Approved Date Range'),AT(396,194),USE(?tmp:DateRangeType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                         END
                       END
                       SHEET,AT(64,54,224,310),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Generic Details'),AT(68,58),USE(?Prompt4),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Criteria Description'),AT(68,72),USE(?rpc:ReportCriteriaType:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(144,72,140,10),USE(rpc:ReportCriteriaType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Criteria Type'),TIP('Criteria Type'),REQ,UPR
                           LIST,AT(144,88,140,10),USE(rpc:ReportName),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('240L(2)|M@s60@'),DROP(26),FROM(ReportNameQueue),MSG('Report Name')
                           PROMPT('Report Name'),AT(68,88),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Start / End Date Range Settings'),AT(68,102,216,56),USE(rpc:DateRangeType),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Date Range Type')
                             RADIO('"Today" Only'),AT(76,114),USE(?Option2:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('1st of Month to "Today"'),AT(76,128),USE(?Option2:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Whole Of Last Month'),AT(76,142),USE(?Option2:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                           END
                           PROMPT('There is no criteria for this "Report".'),AT(108,108),USE(?Prompt:NoCriteria),HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Leeway Days'),AT(68,162),USE(?rpc:LeewayDays:Prompt),HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(144,162,64,10),USE(rpc:LeewayDays),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Leeway Days'),TIP('Leeway Days'),UPR
                           PROMPT('Number Of Days'),AT(68,178),USE(?rpc:NumberOfDays:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(144,178,64,10),USE(rpc:NumberOfDays),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Number Of Days'),TIP('Number Of Days'),UPR
                           PROMPT('Interval Period'),AT(68,194),USE(?rpc:IntervalPeriod:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(144,194,64,10),USE(rpc:IntervalPeriod),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Interval Period'),TIP('Interval Period'),UPR
                           OPTION('Report Order'),AT(68,208,216,28),USE(rpc:WarIncReportOrder,,?rpc:WarIncReportOrder:2),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Report Order')
                             RADIO('By Job No'),AT(72,220),USE(?rpc:WarIncReportOrder:Radio1:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Action Date'),AT(128,220),USE(?rpc:WarIncReportOrder:Radio2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('By Manufacturer'),AT(200,220),USE(?rpc:WarIncReportOrder:Radio3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           CHECK('All Third Party Sites'),AT(144,244),USE(rpc:AllThirdPartySites),HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Third Party Sites'),TIP('All Third Party Sites'),VALUE('1','0')
                           PROMPT('Third Party Site'),AT(68,258),USE(?rpc:ThirdPartySite:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(144,258,124,10),USE(rpc:ThirdPartySite),IMM,HIDE,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1),MSG('Third Party Site')
                           PROMPT('Site Location'),AT(68,276),USE(?rpc:SiteLocation:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(144,276,124,10),USE(rpc:SiteLocation),IMM,HIDE,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2),MSG('Site Location')
                           CHECK('Include Accessories'),AT(144,290),USE(rpc:IncludeAccessories),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Include Accessories'),TIP('Include Accessories'),VALUE('1','0')
                           CHECK('Suppress Zeros'),AT(144,302),USE(rpc:WarIncSuppressZeros,,?rpc:WarIncSuppressZeros:2),HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('WarInc Supress Zeros'),TIP('WarInc Supress Zeros'),VALUE('1','0')
                         END
                       END
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW11                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14::Sort1:Locator StepLocatorClass                 !Conditional Locator - rpc:SelectManufacturer = 1
BRW21                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
BRW23                CLASS(BrowseClass)               !Browse using ?List:6
Q                      &Queue:Browse:5                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW23::Sort0:Locator StepLocatorClass                 !Default Locator
BRW25                CLASS(BrowseClass)               !Browse using ?List:7
Q                      &Queue:Browse:6                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW25::Sort0:Locator StepLocatorClass                 !Default Locator
BRW31                CLASS(BrowseClass)               !Browse using ?List:8
Q                      &Queue:Browse:7                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW31::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14::EIPManager    BrowseEIPManager                 !Browse EIP Manager for Browse using ?List:4
EditInPlace::model:PartNumber1 EditEntryClass         !Edit-in-place class for field model:PartNumber1
EditInPlace::model:PartNumber2 EditEntryClass         !Edit-in-place class for field model:PartNumber2
EditInPlace::model:PartNumber3 EditEntryClass         !Edit-in-place class for field model:PartNumber3
EditInPlace::model:PartNumber4 EditEntryClass         !Edit-in-place class for field model:PartNumber4
EditInPlace::model:PartNumber5 EditEntryClass         !Edit-in-place class for field model:PartNumber5
FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB27               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB28               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW8.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    Tag:AccountNumber = '*'
  ELSE
    DELETE(glo:Queue)
    Tag:AccountNumber = ''
  END
    Queue:Browse.Tag:AccountNumber = Tag:AccountNumber
  IF (Tag:AccountNumber = '*')
    Queue:Browse.Tag:AccountNumber_Icon = 2
  ELSE
    Queue:Browse.Tag:AccountNumber_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW8.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW8.Reset
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::4:QUEUE = glo:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(glo:Queue)
  BRW8.Reset
  LOOP
    NEXT(BRW8::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW8.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW8.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::10:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW9.UpdateBuffer
   glo:Queue2.Pointer2 = man:Manufacturer
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    Tag:Manufacturer = '*'
  ELSE
    DELETE(glo:Queue2)
    Tag:Manufacturer = ''
  END
    Queue:Browse:1.Tag:Manufacturer = Tag:Manufacturer
  IF (Tag:Manufacturer = '*')
    Queue:Browse:1.Tag:Manufacturer_Icon = 2
  ELSE
    Queue:Browse:1.Tag:Manufacturer_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::10:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::10:QUEUE = glo:Queue2
    ADD(DASBRW::10:QUEUE)
  END
  FREE(glo:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::10:QUEUE.Pointer2 = man:Manufacturer
     GET(DASBRW::10:QUEUE,DASBRW::10:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = man:Manufacturer
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::10:DASSHOWTAG Routine
   CASE DASBRW::10:TAGDISPSTATUS
   OF 0
      DASBRW::10:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::10:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::10:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW11.UpdateBuffer
   glo:Queue3.Pointer3 = cha:Charge_Type
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = cha:Charge_Type
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    Tag:ChargeType = '*'
  ELSE
    DELETE(glo:Queue3)
    Tag:ChargeType = ''
  END
    Queue:Browse:2.Tag:ChargeType = Tag:ChargeType
  IF (Tag:ChargeType = '*')
    Queue:Browse:2.Tag:ChargeType_Icon = 2
  ELSE
    Queue:Browse:2.Tag:ChargeType_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = cha:Charge_Type
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::12:QUEUE = glo:Queue3
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue3)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer3 = cha:Charge_Type
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = cha:Charge_Type
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::15:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW14.UpdateBuffer
   glo:Queue4.Pointer4 = model:ModelNumber
   GET(glo:Queue4,glo:Queue4.Pointer4)
  IF ERRORCODE()
     glo:Queue4.Pointer4 = model:ModelNumber
     ADD(glo:Queue4,glo:Queue4.Pointer4)
    Tag:ModelNumber = '*'
  ELSE
    DELETE(glo:Queue4)
    Tag:ModelNumber = ''
  END
    Queue:Browse:3.Tag:ModelNumber = Tag:ModelNumber
  IF (Tag:ModelNumber = '*')
    Queue:Browse:3.Tag:ModelNumber_Icon = 2
  ELSE
    Queue:Browse:3.Tag:ModelNumber_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::15:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue4)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue4.Pointer4 = model:ModelNumber
     ADD(glo:Queue4,glo:Queue4.Pointer4)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::15:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue4)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::15:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::15:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue4)
    GET(glo:Queue4,QR#)
    DASBRW::15:QUEUE = glo:Queue4
    ADD(DASBRW::15:QUEUE)
  END
  FREE(glo:Queue4)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::15:QUEUE.Pointer4 = model:ModelNumber
     GET(DASBRW::15:QUEUE,DASBRW::15:QUEUE.Pointer4)
    IF ERRORCODE()
       glo:Queue4.Pointer4 = model:ModelNumber
       ADD(glo:Queue4,glo:Queue4.Pointer4)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::15:DASSHOWTAG Routine
   CASE DASBRW::15:TAGDISPSTATUS
   OF 0
      DASBRW::15:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::15:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::15:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::22:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW21.UpdateBuffer
   glo:Queue5.Pointer5 = sub:Account_Number
   GET(glo:Queue5,glo:Queue5.Pointer5)
  IF ERRORCODE()
     glo:Queue5.Pointer5 = sub:Account_Number
     ADD(glo:Queue5,glo:Queue5.Pointer5)
    Tag:AccountNumber2 = '*'
  ELSE
    DELETE(glo:Queue5)
    Tag:AccountNumber2 = ''
  END
    Queue:Browse:4.Tag:AccountNumber2 = Tag:AccountNumber2
  IF (Tag:AccountNumber2 = '*')
    Queue:Browse:4.Tag:AccountNumber2_Icon = 2
  ELSE
    Queue:Browse:4.Tag:AccountNumber2_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::22:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW21.Reset
  FREE(glo:Queue5)
  LOOP
    NEXT(BRW21::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue5.Pointer5 = sub:Account_Number
     ADD(glo:Queue5,glo:Queue5.Pointer5)
  END
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::22:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue5)
  BRW21.Reset
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::22:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::22:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue5)
    GET(glo:Queue5,QR#)
    DASBRW::22:QUEUE = glo:Queue5
    ADD(DASBRW::22:QUEUE)
  END
  FREE(glo:Queue5)
  BRW21.Reset
  LOOP
    NEXT(BRW21::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::22:QUEUE.Pointer5 = sub:Account_Number
     GET(DASBRW::22:QUEUE,DASBRW::22:QUEUE.Pointer5)
    IF ERRORCODE()
       glo:Queue5.Pointer5 = sub:Account_Number
       ADD(glo:Queue5,glo:Queue5.Pointer5)
    END
  END
  SETCURSOR
  BRW21.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::22:DASSHOWTAG Routine
   CASE DASBRW::22:TAGDISPSTATUS
   OF 0
      DASBRW::22:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::22:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::22:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:5{PROP:Text} = 'Show All'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:5{PROP:Text})
   BRW21.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::24:DASTAGONOFF Routine
  GET(Queue:Browse:5,CHOICE(?List:6))
  BRW23.UpdateBuffer
   glo:Queue6.Pointer6 = loc:Location
   GET(glo:Queue6,glo:Queue6.Pointer6)
  IF ERRORCODE()
     glo:Queue6.Pointer6 = loc:Location
     ADD(glo:Queue6,glo:Queue6.Pointer6)
    Tag:Location = '*'
  ELSE
    DELETE(glo:Queue6)
    Tag:Location = ''
  END
    Queue:Browse:5.Tag:Location = Tag:Location
  IF (Tag:Location = '*')
    Queue:Browse:5.Tag:Location_Icon = 2
  ELSE
    Queue:Browse:5.Tag:Location_Icon = 1
  END
  PUT(Queue:Browse:5)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::24:DASTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW23.Reset
  FREE(glo:Queue6)
  LOOP
    NEXT(BRW23::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue6.Pointer6 = loc:Location
     ADD(glo:Queue6,glo:Queue6.Pointer6)
  END
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::24:DASUNTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue6)
  BRW23.Reset
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::24:DASREVTAGALL Routine
  ?List:6{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::24:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue6)
    GET(glo:Queue6,QR#)
    DASBRW::24:QUEUE = glo:Queue6
    ADD(DASBRW::24:QUEUE)
  END
  FREE(glo:Queue6)
  BRW23.Reset
  LOOP
    NEXT(BRW23::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::24:QUEUE.Pointer6 = loc:Location
     GET(DASBRW::24:QUEUE,DASBRW::24:QUEUE.Pointer6)
    IF ERRORCODE()
       glo:Queue6.Pointer6 = loc:Location
       ADD(glo:Queue6,glo:Queue6.Pointer6)
    END
  END
  SETCURSOR
  BRW23.ResetSort(1)
  SELECT(?List:6,CHOICE(?List:6))
DASBRW::24:DASSHOWTAG Routine
   CASE DASBRW::24:TAGDISPSTATUS
   OF 0
      DASBRW::24:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::24:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:6{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::24:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:6{PROP:Text} = 'Show All'
      ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:6{PROP:Text})
   BRW23.ResetSort(1)
   SELECT(?List:6,CHOICE(?List:6))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::26:DASTAGONOFF Routine
  GET(Queue:Browse:6,CHOICE(?List:7))
  BRW25.UpdateBuffer
   glo:Queue7.Pointer7 = stp:Stock_Type
   GET(glo:Queue7,glo:Queue7.Pointer7)
  IF ERRORCODE()
     glo:Queue7.Pointer7 = stp:Stock_Type
     ADD(glo:Queue7,glo:Queue7.Pointer7)
    Tag:StockType = '*'
  ELSE
    DELETE(glo:Queue7)
    Tag:StockType = ''
  END
    Queue:Browse:6.Tag:StockType = Tag:StockType
    Queue:Browse:6.Tag:StockType_Icon = 0
  PUT(Queue:Browse:6)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::26:DASTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW25.Reset
  FREE(glo:Queue7)
  LOOP
    NEXT(BRW25::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue7.Pointer7 = stp:Stock_Type
     ADD(glo:Queue7,glo:Queue7.Pointer7)
  END
  SETCURSOR
  BRW25.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::26:DASUNTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue7)
  BRW25.Reset
  SETCURSOR
  BRW25.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::26:DASREVTAGALL Routine
  ?List:7{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::26:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue7)
    GET(glo:Queue7,QR#)
    DASBRW::26:QUEUE = glo:Queue7
    ADD(DASBRW::26:QUEUE)
  END
  FREE(glo:Queue7)
  BRW25.Reset
  LOOP
    NEXT(BRW25::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::26:QUEUE.Pointer7 = stp:Stock_Type
     GET(DASBRW::26:QUEUE,DASBRW::26:QUEUE.Pointer7)
    IF ERRORCODE()
       glo:Queue7.Pointer7 = stp:Stock_Type
       ADD(glo:Queue7,glo:Queue7.Pointer7)
    END
  END
  SETCURSOR
  BRW25.ResetSort(1)
  SELECT(?List:7,CHOICE(?List:7))
DASBRW::26:DASSHOWTAG Routine
   CASE DASBRW::26:TAGDISPSTATUS
   OF 0
      DASBRW::26:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::26:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:7{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::26:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:7{PROP:Text} = 'Show All'
      ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:7{PROP:Text})
   BRW25.ResetSort(1)
   SELECT(?List:7,CHOICE(?List:7))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::32:DASTAGONOFF Routine
  GET(Queue:Browse:7,CHOICE(?List:8))
  BRW31.UpdateBuffer
   glo:ShelfLocationQueue.SiteLocation = los:Site_Location
   glo:ShelfLocationQueue.ShelfLocation = los:Shelf_Location
   GET(glo:ShelfLocationQueue,glo:ShelfLocationQueue.SiteLocation,glo:ShelfLocationQueue.ShelfLocation)
  IF ERRORCODE()
     glo:ShelfLocationQueue.SiteLocation = los:Site_Location
     glo:ShelfLocationQueue.ShelfLocation = los:Shelf_Location
     ADD(glo:ShelfLocationQueue,glo:ShelfLocationQueue.SiteLocation,glo:ShelfLocationQueue.ShelfLocation)
    Tag:ShelfLocation = '*'
  ELSE
    DELETE(glo:ShelfLocationQueue)
    Tag:ShelfLocation = ''
  END
    Queue:Browse:7.Tag:ShelfLocation = Tag:ShelfLocation
  IF (Tag:ShelfLocation)
    Queue:Browse:7.Tag:ShelfLocation_Icon = 2
  ELSE
    Queue:Browse:7.Tag:ShelfLocation_Icon = 1
  END
  PUT(Queue:Browse:7)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::32:DASTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW31.Reset
  FREE(glo:ShelfLocationQueue)
  LOOP
    NEXT(BRW31::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:ShelfLocationQueue.SiteLocation = los:Site_Location
     glo:ShelfLocationQueue.ShelfLocation = los:Shelf_Location
     ADD(glo:ShelfLocationQueue,glo:ShelfLocationQueue.SiteLocation,glo:ShelfLocationQueue.ShelfLocation)
  END
  SETCURSOR
  BRW31.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::32:DASUNTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:ShelfLocationQueue)
  BRW31.Reset
  SETCURSOR
  BRW31.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::32:DASREVTAGALL Routine
  ?List:8{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::32:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:ShelfLocationQueue)
    GET(glo:ShelfLocationQueue,QR#)
    DASBRW::32:QUEUE = glo:ShelfLocationQueue
    ADD(DASBRW::32:QUEUE)
  END
  FREE(glo:ShelfLocationQueue)
  BRW31.Reset
  LOOP
    NEXT(BRW31::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::32:QUEUE.SiteLocation = los:Site_Location
     DASBRW::32:QUEUE.ShelfLocation = los:Shelf_Location
     GET(DASBRW::32:QUEUE,DASBRW::32:QUEUE.SiteLocation,DASBRW::32:QUEUE.ShelfLocation)
    IF ERRORCODE()
       glo:ShelfLocationQueue.SiteLocation = los:Site_Location
       glo:ShelfLocationQueue.ShelfLocation = los:Shelf_Location
       ADD(glo:ShelfLocationQueue,glo:ShelfLocationQueue.SiteLocation,glo:ShelfLocationQueue.ShelfLocation)
    END
  END
  SETCURSOR
  BRW31.ResetSort(1)
  SELECT(?List:8,CHOICE(?List:8))
DASBRW::32:DASSHOWTAG Routine
   CASE DASBRW::32:TAGDISPSTATUS
   OF 0
      DASBRW::32:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::32:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:8{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::32:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:8{PROP:Text} = 'Show All'
      ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:8{PROP:Text})
   BRW31.ResetSort(1)
   SELECT(?List:8,CHOICE(?List:8))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
BuildNumbers        Routine
    Stream(MODELPART)
    Access:MODELPART.Clearkey(model:ModelNumberKey)
    model:ModelNumber = ''
    Set(model:ModelNumberKey,model:ModelNumberKey)
    Loop ! Begin Loop
        If Access:MODELPART.Next()
            Break
        End ! If Access:MODELPART.Next()
        Delete(MODELPART)
    End ! Loop


    Prog.ProgressSetup(Records(MODELNUM))
    Prog.ProgressText('Compiling Criteria. Please wait...')
    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number = ''
    Set(mod:Model_Number_Key,mod:Model_Number_Key)
    Loop ! Begin Loop
        If Access:MODELNUM.Next()
            Break
        End ! If Access:MODELNUM.Next()
        If Access:MODELPART.PrimeRecord() = Level:Benign
            model:Manufacturer = mod:Manufacturer
            model:ModelNumber = mod:Model_Number
            If Access:MODELPART.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:MODELPART.TryInsert() = Level:Benign
                Access:MODELPART.CancelAutoInc()
            End ! If Access:MODELPART.TryInsert() = Level:Benign
        End ! If Access.MODELPART.PrimeRecord() = Level:Benign
    End ! Loop
    Prog.ProgressText('Building Criteria')
    Flush(MODELPART)
    Prog.ProgressFinish()
ShowTabs        Routine
    ?Tab:AccountNumbers{Prop:Hide} = 1
    ?Tab:Manufacturers{Prop:Hide} = 1
    ?Tab:ChargeTypes{Prop:Hide} = 1
    ?Tab:PartNumbers{Prop:Hide} = 1
    ?Tab:SubAccounts{prop:Hide} = 1
    ?Tab:Locations{prop:Hide} = 1
    ?Tab:StockTypes{prop:Hide} = 1
    ?Tab:ShelfLocations{prop:Hide} = 1
    ?Tab:11{prop:Hide} = 1

    ?Tab:NoCriteria{prop:Hide} = 0
    

    ?rpc:DateRangeType{prop:Hide} = 0

    ?rpc:LeewayDays{prop:Hide} = 1
    ?rpc:LeewayDays:Prompt{Prop:Hide} = 1
    ?rpc:NumberOfDays{prop:Hide} = 1
    ?rpc:NumberOfDays:Prompt{prop:Hide} = 1
    ?rpc:IntervalPeriod{prop:Hide} = 1
    ?rpc:IntervalPeriod:Prompt{prop:Hide} = 1
    ?rpc:WarIncReportOrder:2{prop:Hide} = 1
    ?rpc:AllThirdPartySites{prop:Hide} = 1
    ?rpc:ThirdPartySite{prop:Hide} = 1
    ?rpc:ThirdPartySite:prompt{Prop:Hide} = 1
    ?rpc:SiteLocation{prop:Hide} = 1
    ?rpc:SiteLocation:Prompt{prop:Hide} = 1
    ?rpc:WarIncSuppressZeros:2{Prop:Hide} = 1
    ?rpc:IncludeAccessories{prop:Hide} = 1
    ?Prompt:NoCriteria{prop:Hide} = 1

    ?Tab:1{prop:Hide} = 1
    ?Tab:2{Prop:Hide} = 1

    ?Prompt:ExcludeManufacturers{Prop:Hide} = 1

    Case rpc:ReportName
    Of '48 Hour TAT Report'
        ?Tab:AccountNumbers{Prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Booking Performance Report'
        ?Tab:AccountNumbers{Prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Bounce Report'
        ?rpc:LeewayDays{prop:Hide} = 0
        ?rpc:LeewayDays:Prompt{Prop:Hide} = 0
    Of 'Exchange Units Report'
        ?Tab:AccountNumbers{prop:Hide} = 0
        ?Tab:StockTypes{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Goods Received Report'
        ?Tab:AccountNumbers{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Goods Received Franchises Report'
        ?Tab:AccountNumbers{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Handling And Exchange Fee Report'
        ?Tab:ChargeTypes{prop:Hide} = 0
        ?Tab:Manufacturers{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        ?rpc:WarIncReportOrder:2{prop:Hide} = 0
    Of 'Income Report (ARC)'
    Of 'Income Report (Retail)'
    Of 'Monthly Warranty Claim Report'
        ?Tab:NoCriteria{prop:Hide} = 1
        ?Tab:ChargeTypes{prop:Hide} = 0
        ?Tab:Manufacturers{prop:Hide} = 0
        ?Tab:11{prop:Hide} = 0
        ?Tab:11{prop:Text} = 'Other Criteria'
        tmp:Warranty = 'YES'
    Of 'Parts Usage Report'
        ?rpc:DateRangeType{prop:Hide} = 1
        ?rpc:NumberOfDays{prop:Hide} = 0
        ?rpc:NumberOfDays:Prompt{prop:Hide} = 0
        ?Tab:Locations{prop:Hide} = 0
        ?Tab:NoCriteria{Prop:Hide} = 1
    Of 'Repeat IMEI Number Report'
    Of 'Returned Third Party Despatch Report'
        ?rpc:AllThirdPartySites{prop:Hide} = 0
        ?rpc:ThirdPartySite{prop:Hide} = 0
        ?rpc:ThirdPartySite:prompt{Prop:Hide} = 0
    Of 'RRC Chargeable Income Report'
        ?Tab:ChargeTypes{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        ?Tab:1{prop:Hide} = 0
        tmp:Warranty = 'NO'
    Of 'RRC TAT Report'
        ?Tab:AccountNumbers{Prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'RRC Warranty Income Report'
        ?Tab:Manufacturers{Prop:Hide} = 0
        ?Tab:ChargeTypes{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        ?Tab:2{prop:Hide} = 0
        tmp:Warranty = 'YES'
    Of 'Sent To ARC Report'
        ?Tab:AccountNumbers{Prop:Hide} = 0
        ?Tab:Manufacturers{Prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        ?Tab:PartNumbers{Prop:Hide} = 0
        ?Prompt:ExcludeManufacturers{Prop:Hide} = 0
        
    Of 'Spares Forecasting Report'
        ?Tab:Locations{Prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        ?rpc:NumberOfDays{prop:Hide} = 0
        ?rpc:NumberOfDays:Prompt{prop:Hide} = 0

        ?rpc:IntervalPeriod{prop:Hide} = 0
        ?rpc:IntervalPeriod:Prompt{prop:Hide} = 0
    Of 'Stock Adjustment Report'
        ?Tab:Locations{Prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Stock Value Report'
        ?rpc:DateRangeType{prop:Hide} = 1
        ?rpc:SiteLocation{prop:Hide} = 0
        ?rpc:SiteLocation:Prompt{prop:Hide} = 0
        ?rpc:WarIncSuppressZeros:2{Prop:Hide} = 0
        ?rpc:IncludeAccessories{prop:Hide} = 0
    Of 'Stock Value Detailed Report'
        ?rpc:DateRangeType{prop:Hide} = 1
        ?rpc:SiteLocation{prop:Hide} = 0
        ?rpc:SiteLocation:Prompt{prop:Hide} = 0
        ?rpc:WarIncSuppressZeros:2{Prop:Hide} = 0
        ?rpc:IncludeAccessories{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        ?Tab:ShelfLocations{prop:Hide} = 0
    Of 'VCP Performance Report'
        ?Tab:SubAccounts{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
    Of 'Warranty Technical Reports'
        ?rpc:DateRangeType{prop:Hide} = 1
        ?Prompt:NoCriteria{prop:Hide} = 0
    Of 'Warranty Rejection Report'
        ?Tab:AccountNumbers{Prop:Hide} = 0
        ?Tab:Manufacturers{prop:Hide} = 0
        ?Tab:ChargeTypes{prop:Hide} = 0
        ?Tab:NoCriteria{prop:Hide} = 1
        tmp:Warranty = 'YES'
    End ! Case rpc:ReportName

    If ?Tab:AccountNumbers{prop:Hide} = 0
        Brw8.ResetSort(1)
    End ! If ?Tab:AccountNumbers{prop:Hide} = 0
    If ?Tab:Manufacturers{prop:Hide} = 0
        Brw9.ResetSort(1)
    End !If ?Tab:Manufacturer{prop:Hide} = 0
    If ?Tab:ChargeTypes{prop:Hide} = 0
        Brw11.ResetSort(1)
    End ! If ?Tab:ChargeTypes{prop:Hide} = 0
    If ?Tab:PartNumbers{Prop:Hide} = 0
        Brw14.ResetSort(1)
    End ! If ?Tab:PartNumbers{Prop:Hide} = 0
    If ?Tab:SubAccounts{prop:Hide} = 0
        Brw21.ResetSort(1)
    End ! If ?Tab:SubAccounts{prop:Hide} = 0
    If ?Tab:Locations{prop:Hide} = 0
        Brw23.ResetSort(1)
    End ! If ?Tab:Locations{prop:Hide} = 0
    If ?Tab:StockTypes{prop:Hide} = 0
        BRw25.ResetSort(1)
    End ! If ?Tab:StockTypes{prop:Hide} = 0
    If ?Tab:ShelfLocations{prop:Hide} = 0
        Brw31.ResetSort(1)
    End ! If ?Tab:ShelfLocations{prop:Hide} = 0
    
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020689'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateReportScheduleCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:REPSCHCR)
  SELF.AddItem(?Cancel,RequestCancelled)
      glo:FileName = 'MOD' & Clock() & '.tmp'
      If GetTempPathA(255,tmp:TempFilePath)
          If Sub(tmp:TempFilePath,-1,1) = '\'
              glo:FileName = Clip(tmp:TempFilePath) & Clip(glo:FileName)
          Else ! If Sub(tmp:TempFilePath,-1,1) = '\'
              glo:FileName = Clip(tmp:TempFilePath) & '\' & Clip(glo:FileName)
          End ! If Sub(tmp:TempFilePath,-1,1) = '\'
      End ! If GetTempPathA(255,tmp:TempFilePath)
  
      Remove(glo:FileName)
      Create(MODELPART)
  Relate:CHARTYPE.Open
  Relate:MODELPART.Open
  Relate:REPSCHAC.Open
  Relate:STOCKTYP.Open
  Access:REPSCHMA.UseFile
  Access:REPSCHCT.UseFile
  Access:MODELNUM.UseFile
  Access:REPSCHMP.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPSCHCR
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If rpc:ReportName = 'Sent To ARC Report'
      Do BuildNumbers
  End ! If rpc:ReportName = 'Send To ARC Report'
  
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  BRW9.Init(?List:2,Queue:Browse:1.ViewPosition,BRW9::View:Browse,Queue:Browse:1,Relate:MANUFACT,SELF)
  BRW11.Init(?List:3,Queue:Browse:2.ViewPosition,BRW11::View:Browse,Queue:Browse:2,Relate:CHARTYPE,SELF)
  BRW14.Init(?List:4,Queue:Browse:3.ViewPosition,BRW14::View:Browse,Queue:Browse:3,Relate:MODELPART,SELF)
  BRW21.Init(?List:5,Queue:Browse:4.ViewPosition,BRW21::View:Browse,Queue:Browse:4,Relate:SUBTRACC,SELF)
  BRW23.Init(?List:6,Queue:Browse:5.ViewPosition,BRW23::View:Browse,Queue:Browse:5,Relate:LOCATION,SELF)
  BRW25.Init(?List:7,Queue:Browse:6.ViewPosition,BRW25::View:Browse,Queue:Browse:6,Relate:STOCKTYP,SELF)
  BRW31.Init(?List:8,Queue:Browse:7.ViewPosition,BRW31::View:Browse,Queue:Browse:7,Relate:LOCSHELF,SELF)
  OPEN(window)
  SELF.Opened=True
  ?Tab:AccountNumbers{Prop:Text} = 'Select Account Numbers'
  ?Tab:Manufacturers{prop:Text} = 'Select Manufacturers'
  ?Tab:ChargeTypes{prop:Text} = 'Select Charge Types'
  ?Tab:PartNumbers{Prop:Text} = 'Select Model/Part Nos'
  ?Tab:SubAccounts{prop:Text} = 'Select Sub Accounts'
  ?Tab:Locations{prop:Text} = 'Select Locations'
  ?Tab:StockTypes{prop:Text} = 'Select Stock Types'
  ?Tab:1{Prop:Text} = 'Other Criteria'
  ?Tab:2{prop:Text} = 'Other Criteria'
  ?Tab:ShelfLocations{prop:Text} = 'Select Shelf Locations'
  ! Save Window Name
   AddToLog('Window','Open','UpdateReportScheduleCriteria')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ?List:6{prop:vcr} = TRUE
  ?List:7{prop:vcr} = TRUE
  ?List:8{prop:vcr} = TRUE
  ?rpc:ReportName{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,tra:Account_Number_Key)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,tra:Account_Number,1,BRW8)
  BRW8.SetFilter('(tra:Stop_Account = ''NO'')')
  BIND('Tag:AccountNumber',Tag:AccountNumber)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW8.AddField(Tag:AccountNumber,BRW8.Q.Tag:AccountNumber)
  BRW8.AddField(tra:Account_Number,BRW8.Q.tra:Account_Number)
  BRW8.AddField(tra:Company_Name,BRW8.Q.tra:Company_Name)
  BRW8.AddField(tra:RecordNumber,BRW8.Q.tra:RecordNumber)
  BRW9.Q &= Queue:Browse:1
  BRW9.AddSortOrder(,man:Manufacturer_Key)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,man:Manufacturer,1,BRW9)
  BIND('Tag:Manufacturer',Tag:Manufacturer)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(Tag:Manufacturer,BRW9.Q.Tag:Manufacturer)
  BRW9.AddField(man:Manufacturer,BRW9.Q.man:Manufacturer)
  BRW9.AddField(man:RecordNumber,BRW9.Q.man:RecordNumber)
  BRW11.Q &= Queue:Browse:2
  BRW11.AddSortOrder(,cha:Warranty_Key)
  BRW11.AddRange(cha:Warranty,tmp:Warranty)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,cha:Charge_Type,1,BRW11)
  BIND('Tag:ChargeType',Tag:ChargeType)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW11.AddField(Tag:ChargeType,BRW11.Q.Tag:ChargeType)
  BRW11.AddField(cha:Charge_Type,BRW11.Q.cha:Charge_Type)
  BRW11.AddField(cha:Warranty,BRW11.Q.cha:Warranty)
  BRW14.Q &= Queue:Browse:3
  BRW14.AddSortOrder(,model:ManufacturerKey)
  BRW14.AddRange(model:Manufacturer,rpc:Manufacturer)
  BRW14.AddLocator(BRW14::Sort1:Locator)
  BRW14::Sort1:Locator.Init(,model:ModelNumber,1,BRW14)
  BRW14.AddSortOrder(,model:ModelNumberKey)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,model:ModelNumber,1,BRW14)
  BIND('Tag:ModelNumber',Tag:ModelNumber)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(Tag:ModelNumber,BRW14.Q.Tag:ModelNumber)
  BRW14.AddField(model:ModelNumber,BRW14.Q.model:ModelNumber)
  BRW14.AddField(model:PartNumber1,BRW14.Q.model:PartNumber1)
  BRW14.AddField(model:PartNumber2,BRW14.Q.model:PartNumber2)
  BRW14.AddField(model:PartNumber3,BRW14.Q.model:PartNumber3)
  BRW14.AddField(model:PartNumber4,BRW14.Q.model:PartNumber4)
  BRW14.AddField(model:PartNumber5,BRW14.Q.model:PartNumber5)
  BRW14.AddField(model:Manufacturer,BRW14.Q.model:Manufacturer)
  BRW21.Q &= Queue:Browse:4
  BRW21.AddSortOrder(,sub:Main_Account_Key)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,sub:Main_Account_Number,1,BRW21)
  BRW21.SetFilter('(Upper(sub:Region) <<> '''')')
  BIND('Tag:AccountNumber2',Tag:AccountNumber2)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW21.AddField(Tag:AccountNumber2,BRW21.Q.Tag:AccountNumber2)
  BRW21.AddField(sub:Account_Number,BRW21.Q.sub:Account_Number)
  BRW21.AddField(sub:Company_Name,BRW21.Q.sub:Company_Name)
  BRW21.AddField(sub:RecordNumber,BRW21.Q.sub:RecordNumber)
  BRW21.AddField(sub:Main_Account_Number,BRW21.Q.sub:Main_Account_Number)
  BRW23.Q &= Queue:Browse:5
  BRW23.AddSortOrder(,loc:Location_Key)
  BRW23.AddLocator(BRW23::Sort0:Locator)
  BRW23::Sort0:Locator.Init(,loc:Location,1,BRW23)
  BIND('Tag:Location',Tag:Location)
  ?List:6{PROP:IconList,1} = '~notick1.ico'
  ?List:6{PROP:IconList,2} = '~tick1.ico'
  BRW23.AddField(Tag:Location,BRW23.Q.Tag:Location)
  BRW23.AddField(loc:Location,BRW23.Q.loc:Location)
  BRW23.AddField(loc:RecordNumber,BRW23.Q.loc:RecordNumber)
  BRW25.Q &= Queue:Browse:6
  BRW25.AddSortOrder(,stp:Use_Exchange_Key)
  BRW25.AddRange(stp:Use_Exchange,tmp:Yes)
  BRW25.AddLocator(BRW25::Sort0:Locator)
  BRW25::Sort0:Locator.Init(,stp:Stock_Type,1,BRW25)
  BIND('Tag:StockType',Tag:StockType)
  BRW25.AddField(Tag:StockType,BRW25.Q.Tag:StockType)
  BRW25.AddField(stp:Stock_Type,BRW25.Q.stp:Stock_Type)
  BRW25.AddField(stp:Use_Exchange,BRW25.Q.stp:Use_Exchange)
  BRW31.Q &= Queue:Browse:7
  BRW31.AddSortOrder(,los:Shelf_Location_Key)
  BRW31.AddRange(los:Site_Location,rpc:SiteLocation)
  BRW31.AddLocator(BRW31::Sort0:Locator)
  BRW31::Sort0:Locator.Init(,los:Shelf_Location,1,BRW31)
  BIND('Tag:ShelfLocation',Tag:ShelfLocation)
  ?List:8{PROP:IconList,1} = '~notick1.ico'
  ?List:8{PROP:IconList,2} = '~tick1.ico'
  BRW31.AddField(Tag:ShelfLocation,BRW31.Q.Tag:ShelfLocation)
  BRW31.AddField(los:Shelf_Location,BRW31.Q.los:Shelf_Location)
  BRW31.AddField(los:RecordNumber,BRW31.Q.los:RecordNumber)
  BRW31.AddField(los:Site_Location,BRW31.Q.los:Site_Location)
  IF ?rpc:AllAccounts{Prop:Checked} = True
    DISABLE(?List)
  END
  IF ?rpc:AllAccounts{Prop:Checked} = False
    ENABLE(?List)
  END
  IF ?rpc:AllManufacturers{Prop:Checked} = True
    DISABLE(?List:2)
  END
  IF ?rpc:AllManufacturers{Prop:Checked} = False
    ENABLE(?List:2)
  END
  IF ?rpc:AllChargeTypes{Prop:Checked} = True
    DISABLE(?List:3)
  END
  IF ?rpc:AllChargeTypes{Prop:Checked} = False
    ENABLE(?List:3)
  END
  IF ?rpc:SelectManufacturer{Prop:Checked} = True
    ENABLE(?rpc:Manufacturer)
  END
  IF ?rpc:SelectManufacturer{Prop:Checked} = False
    DISABLE(?rpc:Manufacturer)
  END
  IF ?rpc:AllAccounts:2{Prop:Checked} = True
    DISABLE(?List:5)
  END
  IF ?rpc:AllAccounts:2{Prop:Checked} = False
    ENABLE(?List:5)
  END
  IF ?rpc:AllLocations{Prop:Checked} = True
    DISABLE(?List:6)
  END
  IF ?rpc:AllLocations{Prop:Checked} = False
    ENABLE(?List:6)
  END
  IF ?rpc:AllStockTypes{Prop:Checked} = True
    DISABLE(?List:7)
  END
  IF ?rpc:AllStockTypes{Prop:Checked} = False
    ENABLE(?List:7)
  END
  IF ?rpc:WarIncAll:2{Prop:Checked} = True
    DISABLE(?Group:WarrantyStatus:2)
  END
  IF ?rpc:WarIncAll:2{Prop:Checked} = False
    ENABLE(?Group:WarrantyStatus:2)
  END
  IF ?rpc:AllThirdPartySites{Prop:Checked} = True
    DISABLE(?rpc:ThirdPartySite)
  END
  IF ?rpc:AllThirdPartySites{Prop:Checked} = False
    ENABLE(?rpc:ThirdPartySite)
  END
  FDCB18.Init(rpc:Manufacturer,?rpc:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo
  FDCB18.AddSortOrder(man:Manufacturer_Key)
  FDCB18.AddField(man:Manufacturer,FDCB18.Q.man:Manufacturer)
  FDCB18.AddField(man:RecordNumber,FDCB18.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB27.Init(rpc:ThirdPartySite,?rpc:ThirdPartySite,Queue:FileDropCombo:1.ViewPosition,FDCB27::View:FileDropCombo,Queue:FileDropCombo:1,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB27.Q &= Queue:FileDropCombo:1
  FDCB27.AddSortOrder(trd:Company_Name_Key)
  FDCB27.AddField(trd:Company_Name,FDCB27.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB27.WindowComponent)
  FDCB27.DefaultFill = 0
  FDCB28.Init(rpc:SiteLocation,?rpc:SiteLocation,Queue:FileDropCombo:2.ViewPosition,FDCB28::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB28.Q &= Queue:FileDropCombo:2
  FDCB28.AddSortOrder(loc:Location_Key)
  FDCB28.AddField(loc:Location,FDCB28.Q.loc:Location)
  FDCB28.AddField(loc:RecordNumber,FDCB28.Q.loc:RecordNumber)
  FDCB28.AddUpdateField(loc:Location,rpc:SiteLocation)
  ThisWindow.AddItem(FDCB28.WindowComponent)
  FDCB28.DefaultFill = 0
  BRW8.AddToolbarTarget(Toolbar)
  BRW9.AddToolbarTarget(Toolbar)
  BRW11.AddToolbarTarget(Toolbar)
  BRW14.AddToolbarTarget(Toolbar)
  BRW21.AddToolbarTarget(Toolbar)
  BRW23.AddToolbarTarget(Toolbar)
  BRW25.AddToolbarTarget(Toolbar)
  BRW31.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  Do ShowTabs
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:3{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:4{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  ?DASSHOWTAG:5{PROP:Text} = 'Show All'
  ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:5{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue6)
  ?DASSHOWTAG:6{PROP:Text} = 'Show All'
  ?DASSHOWTAG:6{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:6{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:6{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  ?DASSHOWTAG:7{PROP:Text} = 'Show All'
  ?DASSHOWTAG:7{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:7{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:7{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:ShelfLocationQueue)
  ?DASSHOWTAG:8{PROP:Text} = 'Show All'
  ?DASSHOWTAG:8{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:8{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! Build tag list from file records
  Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
  rpa:REPSCHCRREcordNumber = rpc:RecordNumber
  Set(rpa:ACcountNumberKey,rpa:AccountNumberKey)
  Loop
      If Access:REPSCHAC.Next()
          Break
      End ! If Access:REPSCHAC.Next()
      If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpa:REPSCHRERecordNumber <> rpc:RecordNumber
      glo:Pointer = rpa:AccountNumber
      Add(glo:Queue)
      glo:Pointer5 = rpa:AccountNumber
      Add(glo:Queue5)
  End !Loop
  Brw8.ResetSort(1)
  Brw21.ResetSort(1)
  
  Access:REPSCHMA.Clearkey(rpu:ManufacturerKey)
  rpu:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpu:ManufacturerKey,rpu:ManufacturerKey)
  Loop ! Begin Loop
      If Access:REPSCHMA.Next()
          Break
      End ! If Access:REPSCHMA.Next()
      If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
      glo:Pointer2 = rpu:Manufacturer
      Add(glo:Queue2)
  End ! Loop
  Brw9.ResetSort(1)
  
  Access:REPSCHCT.Clearkey(rpr:ChargeTypeKey)
  rpr:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpr:ChargeTypeKey,rpr:ChargeTypeKey)
  Loop ! Begin Loop
      If Access:REPSCHCT.Next()
          Break
      End ! If Access:REPSCHCT.Next()
      If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
      glo:Pointer3 = rpr:ChargeType
      Add(glo:Queue3)
  End ! Loop
  Brw11.ResetSort(1)
  
  If rpc:ReportName = 'Send To ARC Report'
  !    Do BuildNumbers
      Access:REPSCHMP.Clearkey(rpo:ModelNumberKey)
      rpo:REPSCHCRRecordNumber = rpc:RecordNumber
      Set(rpo:ModelNumberKey,rpo:ModelNumberKey)
      Loop ! Begin Loop
          If Access:REPSCHMP.Next()
              Break
          End ! If Access:REPSCHMP.Next()
          If rpo:REPSCHCRREcordNumber <> rpc:RecordNumber
              Break
          End ! If rpo:REPSCHCRREcordNumber <> rpc:RecordNumber
          glo:Pointer4 = rpo:ModelNumber
          Add(Glo:Queue4)
          Access:MODELPART.ClearKey(model:ModelNumberKey)
          model:ModelNumber = rpo:ModelNumber
          If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
              !Found
              model:PartNumber1 = rpo:PartNumber1
              model:PartNumber2 = rpo:PartNumber2
              model:PartNumber3 = rpo:PartNumber3
              model:PartNumber4 = rpo:PartNumber4
              model:PartNumber4 = rpo:PartNumber5
              Access:MODELPART.Update()
          Else ! If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
              !Error
          End ! If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
  
      End ! Loop
      Brw14.ResetSort(1)
  End ! If rpc:ReportName = 'Send To ARC Report'
  
  Access:REPSCHLC.Clearkey(rpl:LocationKey)
  rpl:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpl:LocationKey,rpl:LocationKey)
  Loop ! Begin Loop
      If Access:REPSCHLC.Next()
          Break
      End ! If Access:REPSCHLC.Next()
      If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
      glo:Pointer6 = rpl:Location
      Add(glo:Queue6)
  End ! Loop
  Brw23.ResetSort(1)
  
  Access:REPSCHST.Clearkey(rpk:StockTypeKey)
  rpk:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpk:StockTypeKey,rpk:StockTypeKey)
  Loop ! Begin Loop
      If Access:REPSCHST.Next()
          Break
      End ! If Access:REPSCHST.Next()
      If rpk:REPSCHCRREcordNumber <> rpc:RecordNumber
          Break
      End ! If rpk:REPSCHCRREcordNumber <> rpc:RecordNumber
      glo:Pointer7 = rpk:StockType
      Add(glo:Queue7)
  End ! Loop
  Brw25.ResetSort(1)
  
  Access:REPSCHSL.Clearkey(rpf:LocationKey)
  rpf:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpf:LocationKey,rpf:LocationKey)
  Loop ! Begin Loop
      If Access:REPSCHSL.Next()
          Break
      End ! If Access:REPSCHST.Next()
      If rpf:REPSCHCRREcordNumber <> rpc:RecordNumber
          Break
      End ! If rpk:REPSCHCRREcordNumber <> rpc:RecordNumber
      glo:ShelfLocation = rpf:ShelfLocation
      glo:SiteLocation = rpc:SiteLocation
      Add(glo:ShelfLocationQueue)
  End ! Loop
  Brw31.ResetSort(1)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue6)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue7)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:ShelfLocationQueue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:MODELPART.Close
    Relate:REPSCHAC.Close
    Relate:STOCKTYP.Close
  Remove(glo:FileName)
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateReportScheduleCriteria')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    rpc:WarIncAll = 1
    rpc:LeewayDays = 90
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateModelPart
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020689'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020689'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020689'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?rpc:AllAccounts
      IF ?rpc:AllAccounts{Prop:Checked} = True
        DISABLE(?List)
      END
      IF ?rpc:AllAccounts{Prop:Checked} = False
        ENABLE(?List)
      END
      ThisWindow.Reset
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:AllManufacturers
      IF ?rpc:AllManufacturers{Prop:Checked} = True
        DISABLE(?List:2)
      END
      IF ?rpc:AllManufacturers{Prop:Checked} = False
        ENABLE(?List:2)
      END
      ThisWindow.Reset
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::10:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:AllChargeTypes
      IF ?rpc:AllChargeTypes{Prop:Checked} = True
        DISABLE(?List:3)
      END
      IF ?rpc:AllChargeTypes{Prop:Checked} = False
        ENABLE(?List:3)
      END
      ThisWindow.Reset
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:SelectManufacturer
      IF ?rpc:SelectManufacturer{Prop:Checked} = True
        ENABLE(?rpc:Manufacturer)
      END
      IF ?rpc:SelectManufacturer{Prop:Checked} = False
        DISABLE(?rpc:Manufacturer)
      END
      ThisWindow.Reset
      If ~0{prop:AcceptAll}
          Post(Event:Accepted,?DASUNTAGALL:4)
      End ! If ~0{prop:AcceptAll}
      BRW14.ResetSort(1)
    OF ?rpc:Manufacturer
      BRW14.ResetSort(1)
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:AllAccounts:2
      IF ?rpc:AllAccounts:2{Prop:Checked} = True
        DISABLE(?List:5)
      END
      IF ?rpc:AllAccounts:2{Prop:Checked} = False
        ENABLE(?List:5)
      END
      ThisWindow.Reset
    OF ?DASTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::22:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::22:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::22:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::22:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::22:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:AllLocations
      IF ?rpc:AllLocations{Prop:Checked} = True
        DISABLE(?List:6)
      END
      IF ?rpc:AllLocations{Prop:Checked} = False
        ENABLE(?List:6)
      END
      ThisWindow.Reset
    OF ?DASTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::24:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:AllStockTypes
      IF ?rpc:AllStockTypes{Prop:Checked} = True
        DISABLE(?List:7)
      END
      IF ?rpc:AllStockTypes{Prop:Checked} = False
        ENABLE(?List:7)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::32:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?rpc:WarIncAll:2
      IF ?rpc:WarIncAll:2{Prop:Checked} = True
        DISABLE(?Group:WarrantyStatus:2)
      END
      IF ?rpc:WarIncAll:2{Prop:Checked} = False
        ENABLE(?Group:WarrantyStatus:2)
      END
      ThisWindow.Reset
    OF ?rpc:ReportName
      If ~0{prop:AcceptAll}
          If rpc:ReportName = 'Sent To ARC Report'
              Do BuildNumbers
          End ! If rpc:ReportName = 'Sent To ARC Report'
          Do ShowTabs
      End ! If ~0{prop:AcceptAll}
      
    OF ?rpc:AllThirdPartySites
      IF ?rpc:AllThirdPartySites{Prop:Checked} = True
        DISABLE(?rpc:ThirdPartySite)
      END
      IF ?rpc:AllThirdPartySites{Prop:Checked} = False
        ENABLE(?rpc:ThirdPartySite)
      END
      ThisWindow.Reset
    OF ?rpc:SiteLocation
      If ~0{prop:AcceptAll}
          Post(Event:Accepted,?DASUNTAGALL:8)
      End ! If ~0{prop:AcceptAll}
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If rpc:DateRangeType = 0 And rpc:ReportName <> 'Parts Usage Report' And rpc:ReportName <> 'Stock Value Report' And rpc:ReportName <> 'Stock Value Detailed Report' And rpc:ReportName <> 'Warranty Technical Reports'
      Case Missive('You must select a "Start / End Date Range" setting.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Cycle
  End ! If rpc:DateRangeType = 0
  
  If rpc:ReportName = ''
      Select(?rpc:ReportName)
      Cycle
  End ! If rpc:ReportName = ''
  
  Case rpc:ReportName
  Of 'RRC TAT Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  Of '48 Hour TAT Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  Of 'Booking Performance Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  Of 'Sent To ARC Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  Of 'Warranty Rejection Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
      If rpc:AllManufacturers = 0
          If Records(glo:Queue2) = 0
              Case Missive('You have not selected any manufacturers.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
      If rpc:AllChargeTypes = 0
          If Records(glo:Queue3) = 0
              Case Missive('You have not selected any charge types.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  
  Of 'RRC Chargeable Income Report'
      If rpc:AllChargeTypes = 0
          If Records(glo:Queue3) = 0
              Case Missive('You have not selected any charge types.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  Of 'RRC Warranty Income Report'
      If rpc:AllChargeTypes = 0
          If Records(glo:Queue3) = 0
              Case Missive('You have not selected any charge types.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
      If rpc:AllManufacturers = 0
          If Records(glo:Queue2) = 0
              Case Missive('You have not selected any manufacturers.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue) = 0
      End ! If rpc:AllAccounts = 0
  Of 'VCP Performance Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue5) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue5) = 0
      End ! If rpc:AllAccounts = 0
  Of 'Spares Forecasting Report'
      If Records(glo:Queue6) = 0
          Case Missive('You have not selected any locations.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If Records(glo:Queue6) = 0
  Of 'Exchange Units Report'
      If rpc:AllAccounts = 0
          If Records(glo:Queue) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If Records(glo:Queue5) = 0
      End ! If rpc:AllAccounts = 0
      If rpc:AllStockTypes = 0
          If Records(glo:Queue7) = 0
              Case Missive('You have not selected any accounts.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End
      End ! If rpc:AllStockTypes = 0
  Of 'Returned Third Party Despatch Report'
      If rpc:AllThirdPartySites = 0
          If rpc:ThirdPartySite = ''
              Case Missive('You have not selected a Third Party Site.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End ! If rpc:ThirdPartySite = ''
      End ! If rpc:AllThirdPartySites = 0
  Of 'Stock Value Report'
      If rpc:SiteLocation = ''
          Case Missive('You have not selected a Site Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If rpc:SiteLocation = ''
  Of 'Stock Value Detailed Report'
      If rpc:SiteLocation = ''
          Case Missive('You have not selected a Site Location.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If rpc:SiteLocation = ''
  End ! Case rpc:ReportName
  ! Clear existing account records
  Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
  Loop
      If Access:REPSCHAC.Next()
          Break
      End ! If Access:REPSCHAC.Next()
      If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End !If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHAC)
  End !Loop
  
  If rpc:ReportName = 'VCP Performance Report'
      If Records(glo:Queue5)
          Loop x# = 1 To Records(glo:Queue5)
              Get(glo:Queue5,x#)
              If Access:REPSCHAC.PrimeRecord() = Level:Benign
                  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                  rpa:AccountNumber = glo:Pointer5
                  If Access:REPSCHAC.TryInsert() = Level:Benign
                      !Insert
                  Else ! If Access:REPSCHAC.TryInsert() = Level:Benign
                      Access:REPSCHAC.CancelAutoInc()
                  End ! If Access:REPSCHAC.TryInsert() = Level:Benign
              End ! If Access.REPSCHAC.PrimeRecord() = Level:Benign
          End ! Loop x# = 1 To Records(glo:Queue)
      End ! If Records(glo:Queue5)
  Else ! If rpc:ReportName = 'VCP Performance Report'
      If Records(glo:Queue)
          Loop x# = 1 To Records(glo:Queue)
              Get(glo:Queue,x#)
              If Access:REPSCHAC.PrimeRecord() = Level:Benign
                  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                  rpa:AccountNumber = glo:Pointer
                  Access:REPSCHAC.TryUpdate()
              Else !If Access:REPSCHC.PrimeRecord() = Level:Benign
                  Access:REPSCHAC.CancelAutoInc()
              End !If Access:REPSCHC.PrimeRecord() = Level:Benign
          End ! Loop x# = 1 To Records(glo:Queue)
      End ! If Records(glo:Queue)
  End ! If rpc:ReportName = 'VCP Performance Report'
  
  
  Access:REPSCHMA.Clearkey(rpu:ManufacturerKey)
  rpu:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpu:ManufacturerKey,rpu:ManufacturerKey)
  Loop ! Begin Loop
      If Access:REPSCHMA.Next()
          Break
      End ! If Access:REPSCHMA.Next()
      If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpu:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHMA)
  End ! Loop
  
  If Records(glo:Queue2)
      Loop x# = 1 To Records(glo:Queue2)
          Get(glo:Queue2,x#)
          If Access:REPSCHMA.PrimeRecord() = Level:Benign
              rpu:REPSCHCRRecordNumber = rpc:RecordNumber
              rpu:Manufacturer = glo:Pointer2
              If Access:REPSCHMA.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHMA.TryInsert() = Level:Benign
                  Access:REPSCHMA.CancelAutoInc()
              End ! If Access:REPSCHMA.TryInsert() = Level:Benign
          End ! If Access.REPSCHMA.PrimeRecord() = Level:Benign
      End ! Loop x# = 1 To Records(glo:Queue2)
  End ! If Records(glo:Queue2)
  
  Access:REPSCHCT.Clearkey(rpr:ChargeTypeKey)
  rpr:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpr:ChargeTypeKey,rpr:ChargeTypeKey)
  Loop ! Begin Loop
      If Access:REPSCHCT.Next()
          Break
      End ! If Access:REPSCHCT.Next()
      If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpr:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHCT)
  End ! Loop
  
  If Records(glo:Queue3)
      Loop x# = 1 To Records(glo:Queue3)
          Get(glo:Queue3,x#)
          If Access:REPSCHCT.PrimeRecord() = Level:Benign
              rpr:REPSCHCRRecordNumber = rpc:RecordNumber
              rpr:ChargeType = glo:Pointer3
              If Access:REPSCHCT.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHCT.TryInsert() = Level:Benign
                  Access:REPSCHCT.CancelAutoInc()
              End ! If Access:REPSCHCT.TryInsert() = Level:Benign
          End ! If Access.REPSCHCT.PrimeRecord() = Level:Benign
      End ! Loop x# = 1 To Records(glo:Queue3)
  End ! If Records(glo:Queue3)
  
  Access:REPSCHMP.Clearkey(rpo:ModelNumberKey)
  rpo:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpo:ModelNumberKey,rpo:ModelNumberKey)
  Loop ! Begin Loop
      If Access:REPSCHMP.Next()
          Break
      End ! If Access:REPSCHMP.Next()
      If rpo:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpo:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHMP)
  End ! Loop
  
  If rpc:ReportName = 'Send To ARC Report'
      If Records(glo:Queue4)
          Loop x# = 1 To Records(glo:Queue4)
              Get(glo:Queue4,x#)
              Access:MODELPART.ClearKey(model:ModelNumberKey)
              model:ModelNumber = glo:Pointer4
              If Access:MODELPART.TryFetch(model:ModelNumberKey) = Level:Benign
                  !Found
                  If Access:REPSCHMP.PrimeRecord() = Level:Benign
                      rpo:REPSCHCRRecordNumber = rpc:RecordNumber
                      rpo:Manufacturer = model:Manufacturer
                      rpo:ModelNumber = model:ModelNumber
                      rpo:PartNumber1 = model:PartNumber1
                      rpo:PartNumber2 = model:PartNumber2
                      rpo:PartNumber3 = model:PartNumber3
                      rpo:PartNumber4 = model:PartNumber4
                      rpo:PartNumber5 = model:PartNumber5
                      If Access:REPSCHMP.TryInsert() = Level:Benign
                          !Insert
                      Else ! If Access:REPSCHMP.TryInsert() = Level:Benign
                          Access:REPSCHMP.CancelAutoInc()
                      End ! If Access:REPSCHMP.TryInsert() = Level:Benign
                  End ! If Access.REPSCHMP.PrimeRecord() = Level:Benign
  
              Else ! If Access:MODELNUMBER.TryFetch(model:ModelNumberKey) = Level:Benign
                  !Error
              End ! If Access:MODELNUMBER.TryFetch(model:ModelNumberKey) = Level:Benign
  
          End ! Loop x# = 1 To Records(glo:Queue4)
  
      End ! If Records(glo:Queue4)
  End ! If rpc:ReportName = 'Send To ARC Report'
  
  Access:REPSCHLC.Clearkey(rpl:LocationKey)
  rpl:REPSCHCRRecordNumber = rpc:RecordNumber
  Set(rpl:LocationKey,rpl:LocationKey)
  Loop ! Begin Loop
      If Access:REPSCHLC.Next()
          Break
      End ! If Access:REPSCHLC.Next()
      If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHLC)
  End ! Loop
  
  If Records(glo:Queue6)
      Loop x# = 1 To Records(glo:Queue6)
          Get(glo:Queue6,x#)
          If Access:REPSCHLC.PrimeRecord() = Level:Benign
              rpl:REPSCHCRRecordNumber = rpc:RecordNumber
              rpl:Location = glo:Pointer6
              If Access:REPSCHLC.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHLC.TryInsert() = Level:Benign
                  Access:REPSCHLC.CancelAutoInc()
              End ! If Access:REPSCHLC.TryInsert() = Level:Benign
          End ! If Access.REPSCHLC.PrimeRecord() = Level:Benign
      End ! Loop x# = 1 To Records(glo:Queue6)
  End ! If Records(glo:Queue6)
  
  Access:REPSCHST.Clearkey(rpk:StockTypeKey)
  rpk:REPSCHCRREcordNumber = rpc:RecordNumber
  Set(rpk:StockTypeKey,rpk:StockTypeKey)
  Loop ! Begin Loop
      If Access:REPSCHST.Next()
          Break
      End ! If Access:REPSCHST.Next()
      If rpk:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpk:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHST)
  End ! Loop
  
  If Records(glo:Queue7)
      Loop x# = 1 To Records(Glo:Queue7)
          Get(glo:Queue7,x#)
          If Access:REPSCHST.PrimeRecord() = Level:Benign
              rpk:REPSCHCRRecordNumber = rpc:RecordNumber
              rpk:StockType = glo:Pointer7
              If Access:REPSCHST.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHST.TryInsert() = Level:Benign
                  Access:REPSCHST.CancelAutoInc()
              End ! If Access:REPSCHST.TryInsert() = Level:Benign
          End ! If Access.REPSCHST.PrimeRecord() = Level:Benign
      End ! Loop x# = 1 To Records(Glo:Queue7)
  End ! If Records(glo:Queue7)
  
  Access:REPSCHSL.Clearkey(rpf:LocationKey)
  rpf:REPSCHCRREcordNumber = rpc:RecordNumber
  Set(rpf:LocationKey,rpf:LocationKey)
  Loop ! Begin Loop
      If Access:REPSCHSL.Next()
          Break
      End ! If Access:REPSCHST.Next()
      If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
          Break
      End ! If rpk:REPSCHCRRecordNumber <> rpc:RecordNumber
      Delete(REPSCHSL)
  End ! Loop
  
  If Records(glo:ShelfLocationQueue)
      Loop x# = 1 To Records(Glo:ShelfLocationQueue)
          Get(glo:ShelfLocationQueue,x#)
          If Access:REPSCHSL.PrimeRecord() = Level:Benign
              rpf:REPSCHCRRecordNumber = rpc:RecordNumber
              rpf:ShelfLocation = glo:ShelfLocation
              If Access:REPSCHSL.TryInsert() = Level:Benign
                  !Insert
              Else ! If Access:REPSCHST.TryInsert() = Level:Benign
                  Access:REPSCHSL.CancelAutoInc()
              End ! If Access:REPSCHST.TryInsert() = Level:Benign
          End ! If Access.REPSCHST.PrimeRecord() = Level:Benign
      End ! Loop x# = 1 To Records(Glo:Queue7)
  End ! If Records(glo:Queue7)
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:3
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?rpc:Manufacturer
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:4
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:5
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:6
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:7
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:8
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?rpc:ThirdPartySite
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?rpc:SiteLocation
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:3)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:4)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:5)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:6)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:7)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::10:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:4{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:4{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::15:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:4)
               ?List:4{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:5{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:5{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::22:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:5)
               ?List:5{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:6
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:6{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:6{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::24:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:6)
               ?List:6{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:7
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:7{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:7{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::26:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:7)
               ?List:7{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:8
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:8{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:8{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::32:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:8)
               ?List:8{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?rpc:ReportName{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?rpc:ReportName{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      Tag:AccountNumber = ''
    ELSE
      Tag:AccountNumber = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:AccountNumber = '*')
    SELF.Q.Tag:AccountNumber_Icon = 2
  ELSE
    SELF.Q.Tag:AccountNumber_Icon = 1
  END


BRW8.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  ! Inserting (DBH 23/01/2008) # 9711 - Allow for variable browse filter
  Case rpc:ReportName
  Of 'Goods Received Report'
      If tra:Account_Number = 'XXXRRC' Or tra:SiteLocation = ''
          Return Record:Filtered
      End ! If tra:Account_Number = 'XXXRRC' Or tra:SiteLocation = ''
  Else
      If tra:BranchIdentification = ''
          Return Record:Filtered
      End ! If tra:BranchIdentification = ''
  End ! Case rpc:ReportName
  ! End (DBH 23/01/2008) #9711
  BRW8::RecordStatus=ReturnValue
  IF BRW8::RecordStatus NOT=Record:OK THEN RETURN BRW8::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW8::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  !FILTER HERE?
  ReturnValue=BRW8::RecordStatus
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      Tag:Manufacturer = ''
    ELSE
      Tag:Manufacturer = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:Manufacturer = '*')
    SELF.Q.Tag:Manufacturer_Icon = 2
  ELSE
    SELF.Q.Tag:Manufacturer_Icon = 1
  END


BRW9.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::10:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = cha:Charge_Type
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      Tag:ChargeType = ''
    ELSE
      Tag:ChargeType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:ChargeType = '*')
    SELF.Q.Tag:ChargeType_Icon = 2
  ELSE
    SELF.Q.Tag:ChargeType_Icon = 1
  END


BRW11.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = cha:Charge_Type
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue


BRW14.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.EIP &= BRW14::EIPManager
  SELF.AddEditControl(,2)
  SELF.AddEditControl(EditInPlace::model:PartNumber1,3)
  SELF.AddEditControl(EditInPlace::model:PartNumber2,4)
  SELF.AddEditControl(EditInPlace::model:PartNumber3,5)
  SELF.AddEditControl(EditInPlace::model:PartNumber4,6)
  SELF.AddEditControl(EditInPlace::model:PartNumber5,7)
  SELF.AddEditControl(,1)
  SELF.ArrowAction = EIPAction:Default+EIPAction:Remain+EIPAction:RetainColumn
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW14.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF rpc:SelectManufacturer = 1
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = model:ModelNumber
     GET(glo:Queue4,glo:Queue4.Pointer4)
    IF ERRORCODE()
      Tag:ModelNumber = ''
    ELSE
      Tag:ModelNumber = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:ModelNumber = '*')
    SELF.Q.Tag:ModelNumber_Icon = 2
  ELSE
    SELF.Q.Tag:ModelNumber_Icon = 1
  END


BRW14.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = model:ModelNumber
     GET(glo:Queue4,glo:Queue4.Pointer4)
    EXECUTE DASBRW::15:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue


BRW21.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = sub:Account_Number
     GET(glo:Queue5,glo:Queue5.Pointer5)
    IF ERRORCODE()
      Tag:AccountNumber2 = ''
    ELSE
      Tag:AccountNumber2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:AccountNumber2 = '*')
    SELF.Q.Tag:AccountNumber2_Icon = 2
  ELSE
    SELF.Q.Tag:AccountNumber2_Icon = 1
  END


BRW21.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW21::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW21::RecordStatus=ReturnValue
  IF BRW21::RecordStatus NOT=Record:OK THEN RETURN BRW21::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = sub:Account_Number
     GET(glo:Queue5,glo:Queue5.Pointer5)
    EXECUTE DASBRW::22:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW21::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW21::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW21::RecordStatus
  RETURN ReturnValue


BRW23.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue6.Pointer6 = loc:Location
     GET(glo:Queue6,glo:Queue6.Pointer6)
    IF ERRORCODE()
      Tag:Location = ''
    ELSE
      Tag:Location = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:Location = '*')
    SELF.Q.Tag:Location_Icon = 2
  ELSE
    SELF.Q.Tag:Location_Icon = 1
  END


BRW23.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW23.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW23.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW23::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW23::RecordStatus=ReturnValue
  IF BRW23::RecordStatus NOT=Record:OK THEN RETURN BRW23::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue6.Pointer6 = loc:Location
     GET(glo:Queue6,glo:Queue6.Pointer6)
    EXECUTE DASBRW::24:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW23::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW23::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW23::RecordStatus
  RETURN ReturnValue


BRW25.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = stp:Stock_Type
     GET(glo:Queue7,glo:Queue7.Pointer7)
    IF ERRORCODE()
      Tag:StockType = ''
    ELSE
      Tag:StockType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  SELF.Q.Tag:StockType_Icon = 0


BRW25.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW25.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW25.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW25::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW25::RecordStatus=ReturnValue
  IF BRW25::RecordStatus NOT=Record:OK THEN RETURN BRW25::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue7.Pointer7 = stp:Stock_Type
     GET(glo:Queue7,glo:Queue7.Pointer7)
    EXECUTE DASBRW::26:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW25::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW25::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW25::RecordStatus
  RETURN ReturnValue


BRW31.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:ShelfLocationQueue.SiteLocation = los:Site_Location
     glo:ShelfLocationQueue.ShelfLocation = los:Shelf_Location
     GET(glo:ShelfLocationQueue,glo:ShelfLocationQueue.SiteLocation,glo:ShelfLocationQueue.ShelfLocation)
    IF ERRORCODE()
      Tag:ShelfLocation = ''
    ELSE
      Tag:ShelfLocation = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (Tag:ShelfLocation)
    SELF.Q.Tag:ShelfLocation_Icon = 2
  ELSE
    SELF.Q.Tag:ShelfLocation_Icon = 1
  END


BRW31.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW31.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW31::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW31::RecordStatus=ReturnValue
  IF BRW31::RecordStatus NOT=Record:OK THEN RETURN BRW31::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:ShelfLocationQueue.SiteLocation = los:Site_Location
     glo:ShelfLocationQueue.ShelfLocation = los:Shelf_Location
     GET(glo:ShelfLocationQueue,glo:ShelfLocationQueue.SiteLocation,glo:ShelfLocationQueue.ShelfLocation)
    EXECUTE DASBRW::32:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW31::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW31::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW31::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
UpdateReportSchedule PROCEDURE                        !Generated from procedure template - Window

ActionMessage        CSTRING(40)
tmp:IPAddress        STRING(3),DIM(4)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Report Name'),AT(233,116),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(300,116,148,10),USE(rpd:ReportName),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('240L(2)|M@s60@'),DROP(26),FROM(ReportNameQueue),MSG('Report Name')
                       OPTION('Report Frequency'),AT(232,136,216,66),USE(rpd:Frequency),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Frequency')
                         RADIO('Run Once'),AT(272,146),USE(?rpd:Frequency:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         RADIO('Daily (every day)'),AT(272,160),USE(?rpd:Frequency:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                         RADIO('Weekly (every 7 days)'),AT(272,173),USE(?rpd:Frequency:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                         RADIO('Monthly (same day every month)'),AT(272,189),USE(?rpd:Frequency:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('4')
                       END
                       PROMPT(' . '),AT(408,266),USE(?Prompt8:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT(' . '),AT(376,266),USE(?Prompt8:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       OPTION('Next Time Report To Run'),AT(232,204,216,54),USE(?Option2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       PROMPT(' . '),AT(344,266),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Next Report Date'),AT(236,218),USE(?rpd:NextReportDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,218,64,10),USE(rpd:NextReportDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Next Report Date'),TIP('Next Report Date'),REQ,UPR
                       BUTTON,AT(388,214),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Next Report Time'),AT(236,240),USE(?rpd:NextReportTime:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@t1b),AT(320,240,64,10),USE(rpd:NextReportTime),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Next Report Time'),TIP('Next Report Time'),REQ,UPR
                       PROMPT('IP Of Address To Run Report'),AT(237,264,76,16),USE(?rpd:MachineIP:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s3),AT(320,266,20,10),USE(tmp:IPAddress[1]),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Machine IP'),TIP('Machine IP'),REQ,UPR
                       ENTRY(@s3),AT(353,266,20,10),USE(tmp:IPAddress[2]),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Machine IP'),TIP('Machine IP'),REQ,UPR
                       ENTRY(@s3),AT(386,266,20,10),USE(tmp:IPAddress[3]),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Machine IP'),TIP('Machine IP'),REQ,UPR
                       ENTRY(@s3),AT(416,266,20,10),USE(tmp:IPAddress[4]),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Machine IP'),TIP('Machine IP'),REQ,UPR
                       PROMPT('Criteria Type'),AT(237,286),USE(?rpd:ReportCriteriaType:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s60),AT(320,286,124,10),USE(rpd:ReportCriteriaType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Criteria Type'),TIP('Criteria Type'),REQ,UPR
                       BUTTON,AT(448,282),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Report Schedule'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       CHECK('Active'),AT(173,341),USE(rpd:Active),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Active'),TIP('Active'),VALUE('1','0')
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:rpd:ReportCriteriaType                Like(rpd:ReportCriteriaType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020690'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateReportSchedule')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:REPSCHED)
  Relate:REPSCHCR.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPSCHED
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If ThisWindow.Request <> InsertRecord
      ip# = 1
      lastdot# = 1
      Loop x# = 1 To 15
          If Sub(rpd:MachineIP,x#,1) = '.'
              tmp:IPAddress[ip#] = Sub(rpd:MachineIP,lastdot#,x# - lastdot#)
              lastdot# = x# + 1
              ip# += 1
          End ! If Sub(rpd:IPAddress,x#,1) = '.'
      End ! Loop x# = 1 To 15
      tmp:IPAddress[ip#] = Sub(rpd:MachineIP,lastdot#,x# - lastdot#)
  End ! If ThisWindow.Request <> InsertRecord
  
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','UpdateReportSchedule')
  ?rpd:ReportName{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?rpd:NextReportDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?rpd:ReportCriteriaType{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?rpd:ReportCriteriaType{Prop:Tip}
  END
  IF ?rpd:ReportCriteriaType{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?rpd:ReportCriteriaType{Prop:Msg}
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPSCHCR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateReportSchedule')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    rpd:Active = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseReportCriterias
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          rpd:NextReportDate = TINCALENDARStyle1(rpd:NextReportDate)
          Display(?rpd:NextReportDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?rpd:ReportCriteriaType
      IF rpd:ReportCriteriaType OR ?rpd:ReportCriteriaType{Prop:Req}
        rpc:ReportCriteriaType = rpd:ReportCriteriaType
        rpc:ReportName = rpd:ReportName
        glo:ReportName = rpd:ReportName
        !Save Lookup Field Incase Of error
        look:rpd:ReportCriteriaType        = rpd:ReportCriteriaType
        IF Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            rpd:ReportCriteriaType = rpc:ReportCriteriaType
          ELSE
            CLEAR(rpc:ReportName)
            CLEAR(glo:ReportName)
            !Restore Lookup On Error
            rpd:ReportCriteriaType = look:rpd:ReportCriteriaType
            SELECT(?rpd:ReportCriteriaType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:ReportName = ''
    OF ?CallLookup
      ThisWindow.Update
      If rpd:ReportName = ''
          Case Missive('You must select a "Report Name" first.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If rpd:ReportCriteriaType = ''
      rpc:ReportCriteriaType = rpd:ReportCriteriaType
      glo:ReportName = rpd:ReportName
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          rpd:ReportCriteriaType = rpc:ReportCriteriaType
          Select(?+1)
      ELSE
          Select(?rpd:ReportCriteriaType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?rpd:ReportCriteriaType)
      glo:ReportName = ''
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020690'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020690'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020690'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If rpd:Frequency = 0
      Case Missive('You must select a "Report Frequency".','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      Cycle
  End ! If rpd:Frequency = 0
  rpd:MachineIP = Clip(tmp:IPAddress[1]) & '.' & Clip(tmp:IPAddress[2]) & '.' & Clip(tmp:IPAddress[3]) & '.' & Clip(tmp:IPAddress[4])
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?rpd:NextReportDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?rpd:ReportName{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?rpd:ReportName{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseReportCriterias PROCEDURE                       !Generated from procedure template - Window

tmp:ReportName       STRING(255)
BRW5::View:Browse    VIEW(REPSCHCR)
                       PROJECT(rpc:ReportCriteriaType)
                       PROJECT(rpc:RecordNumber)
                       PROJECT(rpc:ReportName)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rpc:ReportCriteriaType LIKE(rpc:ReportCriteriaType)   !List box control field - type derived from field
rpc:RecordNumber       LIKE(rpc:RecordNumber)         !Primary key field - type derived from field
rpc:ReportName         LIKE(rpc:ReportName)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           LIST,AT(264,87,152,10),USE(tmp:ReportName),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('1020L(2)|M@s255@'),DROP(26,152),FROM(ReportNameQueue)
                           PROMPT('Report Name'),AT(204,87),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(264,100,152,226),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('240L(2)|M~Criteria Type~@s60@'),FROM(Queue:Browse)
                           BUTTON,AT(444,116),USE(?Select),TRN,FLAT,ICON('selectp.jpg')
                           BUTTON,AT(444,183),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(444,218),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                           BUTTON,AT(444,250),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Report Criterias'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020693'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseReportCriterias')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:REPSCHCR.Open
  SELF.FilesOpened = True
  If glo:ReportName <> ''
      tmp:ReportName = glo:ReportName
  End ! If glo:ReportName <> ''
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:REPSCHCR,SELF)
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseReportCriterias')
  ?tmp:ReportName{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,rpc:ReportCriteriaKey)
  BRW5.AddRange(rpc:ReportName,tmp:ReportName)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,rpc:ReportCriteriaType,1,BRW5)
  BRW5.AddField(rpc:ReportCriteriaType,BRW5.Q.rpc:ReportCriteriaType)
  BRW5.AddField(rpc:RecordNumber,BRW5.Q.rpc:RecordNumber)
  BRW5.AddField(rpc:ReportName,BRW5.Q.rpc:ReportName)
  BRW5.AskProcedure = 1
  BRW5.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  If glo:ReportName <> ''
      ?tmp:ReportName{prop:Disable} = True
  End ! If glo:ReportName <> ''
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPSCHCR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseReportCriterias')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateReportScheduleCriteria
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ReportName
      BRW5.ResetSort(1)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020693'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020693'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020693'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:ReportName{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:ReportName{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateModelPart PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::model:Record LIKE(model:RECORD),STATIC
QuickWindow          WINDOW('Update the MODELPART File'),AT(,,196,140),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateModelPart'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,188,114),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?model:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,20,124,10),USE(model:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                           PROMPT('Model Number'),AT(8,34),USE(?model:ModelNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,34,124,10),USE(model:ModelNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Model Number'),TIP('Model Number'),UPR
                           PROMPT('Part Number 1'),AT(8,48),USE(?model:PartNumber1:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,48,124,10),USE(model:PartNumber1),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 1'),TIP('Part Number 1'),UPR
                           PROMPT('Part Number 2'),AT(8,62),USE(?model:PartNumber2:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,62,124,10),USE(model:PartNumber2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 2'),TIP('Part Number 2'),UPR
                           PROMPT('Part Number 3'),AT(8,76),USE(?model:PartNumber3:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,76,124,10),USE(model:PartNumber3),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 3'),TIP('Part Number 3'),UPR
                           PROMPT('Part Number 4'),AT(8,90),USE(?model:PartNumber4:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,90,124,10),USE(model:PartNumber4),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 4'),TIP('Part Number 4'),UPR
                           PROMPT('Part Number 5'),AT(8,104),USE(?model:PartNumber5:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(64,104,124,10),USE(model:PartNumber5),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number 5'),TIP('Part Number 5'),UPR
                         END
                       END
                       BUTTON('OK'),AT(98,122,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(147,122,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(147,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateModelPart')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?model:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(model:Record,History::model:Record)
  SELF.AddHistoryField(?model:Manufacturer,1)
  SELF.AddHistoryField(?model:ModelNumber,2)
  SELF.AddHistoryField(?model:PartNumber1,3)
  SELF.AddHistoryField(?model:PartNumber2,4)
  SELF.AddHistoryField(?model:PartNumber3,5)
  SELF.AddHistoryField(?model:PartNumber4,6)
  SELF.AddHistoryField(?model:PartNumber5,7)
  SELF.AddUpdateFile(Access:MODELPART)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MODELPART.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MODELPART
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','UpdateModelPart')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELPART.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateModelPart')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseReportLog PROCEDURE (f:RecordNumber)            !Generated from procedure template - Window

tmp:RecordNumber     LONG
BRW6::View:Browse    VIEW(REPSCHLG)
                       PROJECT(rlg:TheDate)
                       PROJECT(rlg:TheTime)
                       PROJECT(rlg:Information)
                       PROJECT(rlg:RecordNumber)
                       PROJECT(rlg:REPSCHEDRecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rlg:TheDate            LIKE(rlg:TheDate)              !List box control field - type derived from field
rlg:TheTime            LIKE(rlg:TheTime)              !List box control field - type derived from field
rlg:Information        LIKE(rlg:Information)          !List box control field - type derived from field
rlg:RecordNumber       LIKE(rlg:RecordNumber)         !Primary key field - type derived from field
rlg:REPSCHEDRecordNumber LIKE(rlg:REPSCHEDRecordNumber) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       LIST,AT(168,84,104,244),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('49R(2)|M~Date~@d6@20L(2)|M~Time~R@t1b@0L(2)|M~Information~R@s255@'),FROM(Queue:Browse)
                       TEXT,AT(276,136,236,98),USE(rlg:Information),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Information')
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Report Log'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020692'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseReportLog')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:REPSCHLG.Open
  SELF.FilesOpened = True
  tmp:RecordNumber = f:RecordNumber
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:REPSCHLG,SELF)
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseReportLog')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,rlg:TheDateKey)
  BRW6.AddRange(rlg:REPSCHEDRecordNumber,tmp:RecordNumber)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,rlg:TheDate,1,BRW6)
  BRW6.AddField(rlg:TheDate,BRW6.Q.rlg:TheDate)
  BRW6.AddField(rlg:TheTime,BRW6.Q.rlg:TheTime)
  BRW6.AddField(rlg:Information,BRW6.Q.rlg:Information)
  BRW6.AddField(rlg:RecordNumber,BRW6.Q.rlg:RecordNumber)
  BRW6.AddField(rlg:REPSCHEDRecordNumber,BRW6.Q.rlg:REPSCHEDRecordNumber)
  BRW6.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPSCHLG.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseReportLog')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020692'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020692'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020692'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

