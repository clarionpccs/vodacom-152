

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBH01002.INC'),ONCE        !Req'd for module callout resolution
                     END


eps_defaults PROCEDURE                                !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?dee:Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?dee:Warranty_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?dee:Transit_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?dee:Job_Priority
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?dee:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB2::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB4::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB5::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB8::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
Window               WINDOW('Siemens Import Defaults'),AT(,,231,204),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,168),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,20),USE(?Prompt1)
                           COMBO(@s15),AT(84,20,124,10),USE(dee:Account_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('65L(2)|M@s15@120L(2)@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Charge Type'),AT(8,36),USE(?Prompt2)
                           COMBO(@s30),AT(84,36,124,10),USE(dee:Warranty_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Transit Type'),AT(8,52),USE(?Prompt4)
                           COMBO(@s30),AT(84,52,124,10),USE(dee:Transit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           PROMPT('Job Turnaround Time'),AT(8,68),USE(?Prompt5)
                           COMBO(@s30),AT(84,68,124,10),USE(dee:Job_Priority),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Unit Type'),AT(8,84),USE(?dee:UnitType:prompt)
                           COMBO(@s30),AT(84,84,124,10),USE(dee:Unit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                           PROMPT('Status'),AT(8,100),USE(?dee:Status_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,100,124,10),USE(dee:Status_Type),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,100,10,10),USE(?LookupStatus),SKIP,ICON('List3.ico')
                           PROMPT('Delivery Text'),AT(8,116),USE(?dee:DeliveryText:Prompt)
                           TEXT,AT(84,116,124,48),USE(dee:Delivery_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,176,224,24),USE(?Panel1),FILL(0D6EAEFH)
                       BUTTON('&OK'),AT(112,180,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(168,180,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('eps_defaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFEPS.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  Set(defeps)
  If access:defeps.next()
      get(defeps,0)
      if access:defeps.primerecord() = level:benign
          if access:defeps.insert()
              access:defeps.cancelautoinc()
          end
      end!if access:defeps.primerecord() = level:benign
  End!If access.defeps.next()
  
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','eps_defaults')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?dee:Status_Type{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?dee:Status_Type{Prop:Tip}
  END
  IF ?dee:Status_Type{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?dee:Status_Type{Prop:Msg}
  END
  FDCB1.Init(dee:Account_Number,?dee:Account_Number,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(sub:Account_Number_Key)
  FDCB1.AddField(sub:Account_Number,FDCB1.Q.sub:Account_Number)
  FDCB1.AddField(sub:Company_Name,FDCB1.Q.sub:Company_Name)
  FDCB1.AddField(sub:RecordNumber,FDCB1.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB2.Init(dee:Warranty_Charge_Type,?dee:Warranty_Charge_Type,Queue:FileDropCombo:1.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo:1,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo:1
  FDCB2.AddSortOrder(cha:Warranty_Key)
  FDCB2.AddField(cha:Charge_Type,FDCB2.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB4.Init(dee:Transit_Type,?dee:Transit_Type,Queue:FileDropCombo:3.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:3,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:3
  FDCB4.AddSortOrder(trt:Transit_Type_Key)
  FDCB4.AddField(trt:Transit_Type,FDCB4.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(dee:Job_Priority,?dee:Job_Priority,Queue:FileDropCombo:4.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:4,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:4
  FDCB5.AddSortOrder()
  FDCB5.AddField(tur:Turnaround_Time,FDCB5.Q.tur:Turnaround_Time)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB8.Init(dee:Unit_Type,?dee:Unit_Type,Queue:FileDropCombo:5.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:5,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:5
  FDCB8.AddSortOrder(uni:Unit_Type_Key)
  FDCB8.AddField(uni:Unit_Type,FDCB8.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFEPS.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','eps_defaults')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defeps.update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStatus
      ThisWindow.Update
      ! Lookup button pressed. Call select procedure if required
      sts:Status = dee:Status_Type                    ! Move value for lookup
      IF Access:STATUS.TryFetch(sts:Status_Key)       ! IF record not found
         sts:Status = dee:Status_Type                 ! Move value for lookup
         ! Based on ?dee:Status_Type
         SET(sts:Status_Key,sts:Status_Key)           ! Prime order for browse
         Access:STATUS.TryNext()
      END
      GlobalRequest = SelectRecord
      Browse_Status                                   ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?LookupStatus)                        ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?dee:Status_Type,sts:Status)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?LookupStatus+1)                         ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB1.ResetQueue(1)
      FDCB2.ResetQueue(1)
      FDCB4.ResetQueue(1)
      FDCB5.ResetQueue(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
FormatKey PROCEDURE                                   !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Format Key'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Format Key'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           GROUP('Field Format Types'),AT(168,118,344,186),USE(?FieldFormatTypes),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('MM - Month'),AT(172,171),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('YY - Two Digit Year'),AT(172,195),USE(?Prompt8:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('YYYY - Four Digit Year'),AT(172,216),USE(?Prompt8:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('A - Alpha Only Character'),AT(172,128),USE(?Prompt8:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('"" - Put any fixed text in Quotes, e.g. "TEST"'),AT(308,216),USE(?Prompt8:7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             LINE,AT(178,234,323,0),USE(?Line1),COLOR(COLOR:White)
                             PROMPT('* - All remaining characters can be Alphanumeric'),AT(172,242),USE(?Prompt10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('% - All remaining characters must only be Numeric'),AT(172,258),USE(?Prompt10:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('$ - All remaining characters must only be Alpha'),AT(172,274),USE(?Prompt10:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('(No other format characters will be recongised after the above characters)'),AT(199,290),USE(?Prompt13),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('0 - Numeric Only Character'),AT(172,150),USE(?Prompt8:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('X - Alphanumeric Character'),AT(308,128),USE(?Prompt8:6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                         END
                       END
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020360'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('FormatKey')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','FormatKey')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','FormatKey')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020360'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020360'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020360'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateMANFAULT PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
save_maf_id          USHORT,AUTO
tmp:DatePicture      STRING(30)
tmp:SaveState        LONG
tmp:FieldNumber      LONG
tmp:Manufacturer     STRING(30)
tmp:ScreenOrder      LONG
tmp:MainFault        BYTE(0)
tmp:InFault          BYTE(0)
tmp:GenericFault     BYTE(0)
tmp:BouncerFault     BYTE
BouncerFlag          BYTE
BRW8::View:Browse    VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:RecordNumber)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Field_NormalFG     LONG                           !Normal forground color
mfo:Field_NormalBG     LONG                           !Normal background color
mfo:Field_SelectedFG   LONG                           !Selected forground color
mfo:Field_SelectedBG   LONG                           !Selected background color
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:Description_NormalFG LONG                         !Normal forground color
mfo:Description_NormalBG LONG                         !Normal background color
mfo:Description_SelectedFG LONG                       !Selected forground color
mfo:Description_SelectedBG LONG                       !Selected background color
mfo:RecordNumber       LIKE(mfo:RecordNumber)         !Primary key field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Browse key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK15::mfo:Field_Number    LIKE(mfo:Field_Number)
HK15::mfo:Manufacturer    LIKE(mfo:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
History::maf:Record  LIKE(maf:RECORD),STATIC
QuickWindow          WINDOW('Update Fault Codes'),AT(0,0,679,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Insert / Amend Job Fault Codes'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       CHECK('Not Available'),AT(84,46),USE(maf:NotAvailable),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Available'),TIP('Not Available'),VALUE('1','0')
                       SHEET,AT(4,30,240,220),USE(?Sheet:DefaultSettings),SPREAD
                         TAB('Default Settings'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manufacturer'),AT(8,59),USE(?MAF:Manufacturer:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(84,59,124,10),USE(maf:Manufacturer),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,SINGLE,READONLY
                           PROMPT('Field Number'),AT(8,91),USE(?MAF:Field_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@n2),AT(84,91,64,10),USE(maf:Field_Number),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,RANGE(1,20),STEP(1),MSG('Fault Code Field Number')
                           PROMPT('Screen Order'),AT(8,108),USE(?maf:ScreenOrder:Prompt),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(84,108,64,10),USE(maf:ScreenOrder),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Screen Order'),TIP('Screen Order'),REQ,UPR,STEP(1)
                           OPTION('Field Type'),AT(84,128,148,54),USE(maf:Field_Type),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Generic String'),AT(88,138),USE(?maf:Field_Type:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('STRING')
                             RADIO('Date'),AT(88,152),USE(?maf:Field_Type:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('DATE')
                             RADIO('Number Only'),AT(88,166),USE(?maf:Field_Type:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('NUMBER')
                           END
                           PROMPT('Date Format'),AT(160,146),USE(?DatePicture),HIDE,FONT(,7,,FONT:bold,CHARSET:ANSI)
                           LIST,AT(160,154,64,10),USE(tmp:DatePicture),HIDE,LEFT(2),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),DROP(20),FROM('DD/MM/YYYY|DD/MM/YY|MM/DD/YYYY|MM/DD/YY|MM/YYYY|MM/YY|YYYY/MM|YY/MM|YYYY/MM/DD|YY/MM/DD|YYYYMMDD')
                           CHECK(' Main Out Fault'),AT(84,186),USE(maf:MainFault),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Main Fault'),TIP('Main Fault'),VALUE('1','0')
                           CHECK('Main In Fault'),AT(168,186),USE(maf:InFault),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('In Fault'),TIP('In Fault'),VALUE('1','0')
                           CHECK('Use As "Generic Fault Description"'),AT(84,199),USE(maf:GenericFault),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Generic Fault'),TIP('Generic Fault'),VALUE('1','0')
                           CHECK('Use as "Bouncer"'),AT(83,212),USE(maf:BouncerFault),VALUE('1','0')
                           PROMPT('Field Name'),AT(8,75),USE(?MAF:Field_Name:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(84,75,124,10),USE(maf:Field_Name),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,SINGLE
                         END
                       END
                       SHEET,AT(4,252,240,124),USE(?Sheet:FormatSettings),SPREAD
                         TAB('Format Settings'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(12,324,200,41),USE(?Group:FieldFormat)
                             PROMPT('Field Format'),AT(12,324),USE(?maf:FieldFormat:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             TEXT,AT(88,324,124,10),USE(maf:FieldFormat),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Field Format'),TIP('Field Format'),REQ,UPR,SINGLE
                             BUTTON,AT(84,338),USE(?FormatKey),TRN,FLAT,LEFT,ICON('keyp.jpg')
                           END
                           CHECK('Restrict Length'),AT(8,268),USE(maf:RestrictLength),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Restrict Length'),TIP('Restrict Length'),VALUE('1','0')
                           GROUP,AT(8,281,196,32),USE(?RestrictGroup)
                             PROMPT('Length From'),AT(12,284),USE(?MAF:LengthFrom:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(88,284,64,10),USE(maf:LengthFrom),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Length From'),TIP('Length From'),REQ,UPR
                             PROMPT('Length To'),AT(12,300),USE(?MAF:LengthTo:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(88,300,64,10),USE(maf:LengthTo),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Length To'),TIP('Length To'),REQ,UPR
                           END
                           CHECK('Force Format'),AT(88,313),USE(maf:ForceFormat),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Force Format'),TIP('Force Format'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(248,30,152,114),USE(?Sheet:ActionSettings),SPREAD
                         TAB('Action Settings'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(252,96,124,10),USE(?Group:BlankFaultCode)
                             PROMPT('Fault Code No'),AT(252,96),USE(?maf:BlankRelatedCode:Prompt),FONT(,,,FONT:bold)
                             SPIN(@s8),AT(312,96,64,10),USE(maf:BlankRelatedCode),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,RANGE(1,20),STEP(1),MSG('Blank Related Code')
                           END
                           CHECK('Replicate To Fault Description'),AT(252,46),USE(maf:ReplicateFault),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Replicate To Invoice Text'),AT(252,58),USE(maf:ReplicateInvoice),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Hide For 3rd Party Jobs'),AT(252,70),USE(maf:HideThirdParty),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hide For 3rd Party Jobs'),TIP('Hide For 3rd Party Jobs'),VALUE('1','0')
                           CHECK('Hide When Related Code Is Blank'),AT(252,84),USE(maf:HideRelatedCodeIfBlank),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hide When Related Code Is Blank'),TIP('Hide When Related Code Is Blank'),VALUE('1','0')
                           CHECK('Fill From Date Of Purchase'),AT(252,112),USE(maf:FillFromDOP),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Fill From Date Of Purchase'),TIP('Fill From Date Of Purchase'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(248,146,152,230),USE(?Sheet:CompulsorySettings),SPREAD
                         TAB('Compulsory Settings'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(256,304,144,32),USE(?Group:CompulsoryRepairType)
                             CHECK('Compulsory Only For Repair Type'),AT(256,306),USE(maf:CompulsoryForRepairType),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Compulsory For Repair Type'),TIP('Compulsory For Repair Type'),VALUE('1','0')
                             ENTRY(@s30),AT(256,320,112,10),USE(maf:CompulsoryRepairType),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Repair Type'),TIP('Repair Type'),UPR,READONLY
                             BUTTON,AT(372,316),USE(?Lookup:RepairTYpe),TRN,FLAT,ICON('lookupp.jpg')
                           END
                           GROUP('Warranty Job Settings'),AT(252,160,124,38),USE(?Group:Warranty),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Compulsory At Completion'),AT(256,170),USE(maf:Compulsory),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             CHECK('Compulsory At Booking'),AT(256,184),USE(maf:Compulsory_At_Booking),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           END
                           GROUP('Chargeable Job Settings'),AT(252,200,124,38),USE(?Group:Chargeable),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Compulsory At Completion'),AT(256,210),USE(maf:CharCompulsory),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Compulsory For Chargeable Jobs'),TIP('Compulsory For Chargeable Jobs'),VALUE('1','0')
                             CHECK('Compulsory At Booking'),AT(256,224),USE(maf:CharCompulsoryBooking),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Compulsory At Booking'),TIP('Compulsory At Booking'),VALUE('1','0')
                           END
                           CHECK('Restrict Availability'),AT(256,242),USE(maf:RestrictAvailability),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Restrict Availability'),TIP('Restrict Availability'),VALUE('1','0')
                           OPTION('Available For Service Centres'),AT(252,252,144,28),USE(maf:RestrictServiceCentre),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Available For Service Centres')
                             RADIO('Level 1'),AT(256,265),USE(?maf:RestrictServiceCentre:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Level 2'),AT(300,265),USE(?maf:RestrictServiceCentre:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Level 3'),AT(348,265),USE(?maf:RestrictServiceCentre:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                           END
                           CHECK('Compulsory If Exchange Issued'),AT(256,294),USE(maf:CompulsoryIfExchange),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Compulsory If Exchange Issued'),TIP('Compulsory If Exchange Issued'),VALUE('1','0')
                           CHECK('Not Compulsory For 3rd Party Job'),AT(256,282),USE(maf:NotCompulsoryThirdParty),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Compulsory For 3rd Party Job'),TIP('Not Compulsory For 3rd Party Job'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(404,30,272,346),USE(?Sheet:Lookup),COLOR(0D6E7EFH),SPREAD
                         TAB('Lookup Field'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Use Lookup'),AT(408,46),USE(maf:Lookup),FONT('Tahoma',8,,FONT:bold),VALUE('YES','NO')
                           ENTRY(@s30),AT(408,62,127,10),USE(mfo:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Force Lookup'),AT(480,46),USE(maf:Force_Lookup),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           BOX,AT(572,50,12,10),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Red)
                           PROMPT('- Primary Lookup'),AT(588,50),USE(?Prompt12),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(408,76,260,266),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('86L(2)|M*~Field~@s30@240L(2)|M*~Description~S(240)@s60@'),FROM(Queue:Browse)
                           BOX,AT(572,62,12,10),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Gray)
                           PROMPT('- Not Available'),AT(588,62),USE(?Prompt12:2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(472,346),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(540,346),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(608,346),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       PANEL,AT(4,380,672,26),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(544,380),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(612,380),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(8,380),USE(?Button:BrowseLinkedFaultCodes),TRN,FLAT,ICON('linkfaup.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Job Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020361'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANFAULT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(maf:Record,History::maf:Record)
  SELF.AddHistoryField(?maf:NotAvailable,21)
  SELF.AddHistoryField(?maf:Manufacturer,1)
  SELF.AddHistoryField(?maf:Field_Number,2)
  SELF.AddHistoryField(?maf:ScreenOrder,24)
  SELF.AddHistoryField(?maf:Field_Type,4)
  SELF.AddHistoryField(?maf:MainFault,17)
  SELF.AddHistoryField(?maf:InFault,19)
  SELF.AddHistoryField(?maf:GenericFault,27)
  SELF.AddHistoryField(?maf:BouncerFault,35)
  SELF.AddHistoryField(?maf:Field_Name,3)
  SELF.AddHistoryField(?maf:FieldFormat,15)
  SELF.AddHistoryField(?maf:RestrictLength,11)
  SELF.AddHistoryField(?maf:LengthFrom,12)
  SELF.AddHistoryField(?maf:LengthTo,13)
  SELF.AddHistoryField(?maf:ForceFormat,14)
  SELF.AddHistoryField(?maf:BlankRelatedCode,31)
  SELF.AddHistoryField(?maf:ReplicateFault,9)
  SELF.AddHistoryField(?maf:ReplicateInvoice,10)
  SELF.AddHistoryField(?maf:HideThirdParty,29)
  SELF.AddHistoryField(?maf:HideRelatedCodeIfBlank,30)
  SELF.AddHistoryField(?maf:FillFromDOP,32)
  SELF.AddHistoryField(?maf:CompulsoryForRepairType,33)
  SELF.AddHistoryField(?maf:CompulsoryRepairType,34)
  SELF.AddHistoryField(?maf:Compulsory,7)
  SELF.AddHistoryField(?maf:Compulsory_At_Booking,8)
  SELF.AddHistoryField(?maf:CharCompulsory,22)
  SELF.AddHistoryField(?maf:CharCompulsoryBooking,23)
  SELF.AddHistoryField(?maf:RestrictAvailability,25)
  SELF.AddHistoryField(?maf:RestrictServiceCentre,26)
  SELF.AddHistoryField(?maf:CompulsoryIfExchange,20)
  SELF.AddHistoryField(?maf:NotCompulsoryThirdParty,28)
  SELF.AddHistoryField(?maf:Lookup,5)
  SELF.AddHistoryField(?maf:Force_Lookup,6)
  SELF.AddUpdateFile(Access:MANFAULT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFAULO.Open
  Relate:MANFAULT_ALIAS.Open
  Relate:USELEVEL.Open
  Access:MANFAULT.UseFile
  Access:USERS.UseFile
  Access:REPTYDEF.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFAULT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:MANFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! What is the picture?
  Case maf:DateType
      Of '@d5'
          tmp:DatePicture = 'DD/MM/YY'
      Of '@d6'
          tmp:DatePicture = 'DD/MM/YYYY'
      Of '@d1'
          tmp:DatePicture = 'MM/DD/YY'
      Of '@d2'
          tmp:DatePicture = 'MM/DD/YYYY'
      Of '@d13'
          tmp:DatePicture = 'MM/YY'
      Of '@d14'
          tmp:DatePicture = 'MM/YYYY'
      Of '@d15'
          tmp:DatePicture = 'YY/MM'
      Of '@d16'
          tmp:DatePicture = 'YYYY/MM'
      Of '@d9'
          tmp:DatePicture = 'YY/MM/DD'
      Of '@d10'
          tmp:DatePicture = 'YYYY/MM/DD'
      Of '@d11'
          tmp:DatePicture = 'YYMMDD'
      Of '@d12'
          tmp:DatePicture = 'YYYYMMDD'
  End !tmp:DatePicture
  Post(Event:Accepted,?maf:Field_Type)
  ! Save Window Name
   AddToLog('Window','Open','UpdateMANFAULT')
  ?tmp:DatePicture{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?maf:CompulsoryRepairType{Prop:Tip} AND ~?Lookup:RepairTYpe{Prop:Tip}
     ?Lookup:RepairTYpe{Prop:Tip} = 'Select ' & ?maf:CompulsoryRepairType{Prop:Tip}
  END
  IF ?maf:CompulsoryRepairType{Prop:Msg} AND ~?Lookup:RepairTYpe{Prop:Msg}
     ?Lookup:RepairTYpe{Prop:Msg} = 'Select ' & ?maf:CompulsoryRepairType{Prop:Msg}
  END
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,mfo:Field_Key)
  BRW8.AddRange(mfo:Field_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?mfo:Field,mfo:Field,1,BRW8)
  BRW8.AddField(mfo:Field,BRW8.Q.mfo:Field)
  BRW8.AddField(mfo:Description,BRW8.Q.mfo:Description)
  BRW8.AddField(mfo:RecordNumber,BRW8.Q.mfo:RecordNumber)
  BRW8.AddField(mfo:Manufacturer,BRW8.Q.mfo:Manufacturer)
  BRW8.AddField(mfo:Field_Number,BRW8.Q.mfo:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?maf:NotAvailable{Prop:Checked} = True
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
  END
  IF ?maf:NotAvailable{Prop:Checked} = False
    UNHIDE(?Insert)
    UNHIDE(?Delete)
    UNHIDE(?Change)
  END
  IF ?maf:RestrictLength{Prop:Checked} = True
    ENABLE(?RestrictGroup)
  END
  IF ?maf:RestrictLength{Prop:Checked} = False
    DISABLE(?RestrictGroup)
  END
  IF ?maf:ForceFormat{Prop:Checked} = True
    ENABLE(?Group:FieldFormat)
  END
  IF ?maf:ForceFormat{Prop:Checked} = False
    DISABLE(?Group:FieldFormat)
  END
  IF ?maf:HideRelatedCodeIfBlank{Prop:Checked} = True
    ENABLE(?Group:BlankFaultCode)
  END
  IF ?maf:HideRelatedCodeIfBlank{Prop:Checked} = False
    DISABLE(?Group:BlankFaultCode)
  END
  IF ?maf:CompulsoryForRepairType{Prop:Checked} = True
    ENABLE(?maf:CompulsoryRepairType)
    ENABLE(?Lookup:RepairType)
  END
  IF ?maf:CompulsoryForRepairType{Prop:Checked} = False
    maf:CompulsoryRepairType = ''
    DISABLE(?maf:CompulsoryRepairType)
    DISABLE(?Lookup:RepairType)
  END
  IF ?maf:Compulsory{Prop:Checked} = True
    ENABLE(?maf:Compulsory_At_Booking)
    ENABLE(?maf:CompulsoryIfExchange)
    ENABLE(?Group:CompulsoryRepairType)
  END
  IF ?maf:Compulsory{Prop:Checked} = False
    maf:Compulsory_At_Booking = 'NO'
    maf:CompulsoryForRepairType = 0
    maf:CompulsoryRepairType = ''
    DISABLE(?maf:CompulsoryIfExchange)
    DISABLE(?maf:Compulsory_At_Booking)
    DISABLE(?Group:CompulsoryRepairType)
  END
  IF ?maf:CharCompulsory{Prop:Checked} = True
    ENABLE(?maf:CharCompulsoryBooking)
  END
  IF ?maf:CharCompulsory{Prop:Checked} = False
    maf:CharCompulsoryBooking = 0
    DISABLE(?maf:CharCompulsoryBooking)
  END
  IF ?maf:RestrictAvailability{Prop:Checked} = True
    ENABLE(?maf:RestrictServiceCentre)
  END
  IF ?maf:RestrictAvailability{Prop:Checked} = False
    DISABLE(?maf:RestrictServiceCentre)
  END
  IF ?maf:NotCompulsoryThirdParty{Prop:Checked} = True
    ENABLE(?maf:HideThirdParty)
  END
  IF ?maf:NotCompulsoryThirdParty{Prop:Checked} = False
    DISABLE(?maf:HideThirdParty)
  END
  IF ?maf:Lookup{Prop:Checked} = True
    UNHIDE(?Insert)
    UNHIDE(?Change)
    UNHIDE(?Delete)
    UNHIDE(?MAF:Force_Lookup)
    ENABLE(?List)
  END
  IF ?maf:Lookup{Prop:Checked} = False
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
    HIDE(?MAF:Force_Lookup)
    DISABLE(?List)
  END
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULO.Close
    Relate:MANFAULT_ALIAS.Close
    Relate:USELEVEL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANFAULT')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request  = Insertrecord
      If maf:field_number = ''
          Case Missive('You have not entered a Field Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = 0
      End
  End
  If do_update# = 1 And maf:lookup = 'YES'
      glo:select1  = maf:manufacturer
      glo:select2  = maf:field_number
      glo:select3  = maf:field_name
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULO
    ReturnValue = GlobalResponse
  END
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
  End !If do_update# = 1 And maf:lookup = 'YES'
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020361'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020361'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020361'&'0')
      ***
    OF ?maf:NotAvailable
      IF ?maf:NotAvailable{Prop:Checked} = True
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
      END
      IF ?maf:NotAvailable{Prop:Checked} = False
        UNHIDE(?Insert)
        UNHIDE(?Delete)
        UNHIDE(?Change)
      END
      ThisWindow.Reset
    OF ?maf:Field_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:Field_Type, Accepted)
      Case maf:Field_Type
          Of 'DATE'
              ?tmp:DatePicture{prop:Hide} = 0
              ?DatePicture{prop:Hide} = 0
              maf:ForceFormat = 0
              ?maf:ForceFormat{prop:Disable} = 1
              maf:Lookup = 'NO'
              maf:RestrictLength = 0
          Of 'STRING'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?maf:ForceFormat{prop:Disable} = 0
          Of 'NUMBER'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?maf:ForceFormat{prop:Disable} = 1
              maf:ForceFormat = 0
      End !maf:Field_Type
      Post(Event:Accepted,?maf:ForceFormat)
      Post(Event:Accepted,?maf:Lookup)
      Post(Event:Accepted,?maf:RestrictLength)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:Field_Type, Accepted)
    OF ?maf:BouncerFault
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:BouncerFault, Accepted)
      If ~0{prop:Acceptall}
          if maf:BouncerFault = 0 then
              !this has just been unticked - TB13231 - J - 16/12/14 - show a message
              If maf:InFault = false
                  Case Missive('One Job Fault Code must be enabled as "Use For Bouncer". |If you proceed then the Main In Fault will be used as "Use For Bouncer". '&|
                               '|Do you want to proceed?',|
                              'ServiceBase 3g','mstop.jpg','/YES|\NO')
                      Of 1 ! YES Button
                          !find the main in fault and mark as Bouncer
                          Access:Manfault_Alias.clearkey(maf_ali:InFaultKey)
                          maf_ali:Manufacturer = maf:Manufacturer
                          maf_ali:InFault = true
                          if access:Manfault_alias.fetch(maf_ali:InFaultKey) = level:Benign
                              maf_ali:BouncerFault = true
                              Access:manfault_alias.update()
                          END !if main fault found
                      of 2 ! NO
                          maf:BouncerFault=1
                  End ! Case Missive
      
              ELSE !if this is not the main in fault
      
                  Case Missive('One Job Fault Code must be enabled as "Use For Bouncer". |As no other fault is enabled this (the Main In Fault) will be used as "Use For Bouncer". ',|
                              'ServiceBase 3g','mstop.jpg','OK')
                      of 1
                          maf:BouncerFault=1
                  End ! Case Missive
      
              END !If this is not the main in fault
              
          ELSE
              !just been ticked - does one already exist?
              Access:Manfault_Alias.clearkey(maf_ali:BouncerFaultKey)
              maf_ali:Manufacturer = maf:Manufacturer
              maf_ali:BouncerFault = 1
              If access:Manfault_Alias.fetch(maf_ali:BouncerFaultKey) = level:Benign
                  !another one exists  - be sure this is not the same one
                  if maf_ali:Field_Number <> maf:Field_Number
                      Case Missive(clip(maf_ali:Field_Name) &' has already been marked as "Use For Bouncer". '&|
                                  'If you proceed then this fault code will be marked as "Use For Bouncer" instead of '&clip(maf_ali:Field_Name)&|
                                  '. |Do you want to proceed?',|
                                  'ServiceBase 3g','mstop.jpg','/YES|\NO')
                          Of 1 ! YES Button
                              maf_ali:BouncerFault = 0
                              Access:Manfault_Alias.update()
                          of 2 ! NO
                              maf:BouncerFault=0
                      End ! Case Missive
                  END !if maf_ali:Field_Number <> maf:Field_Number
              END
          END !if box has been unticked
          display()
      end !If ~0{prop:Acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:BouncerFault, Accepted)
    OF ?FormatKey
      ThisWindow.Update
      FormatKey
      ThisWindow.Reset
    OF ?maf:RestrictLength
      IF ?maf:RestrictLength{Prop:Checked} = True
        ENABLE(?RestrictGroup)
      END
      IF ?maf:RestrictLength{Prop:Checked} = False
        DISABLE(?RestrictGroup)
      END
      ThisWindow.Reset
    OF ?maf:ForceFormat
      IF ?maf:ForceFormat{Prop:Checked} = True
        ENABLE(?Group:FieldFormat)
      END
      IF ?maf:ForceFormat{Prop:Checked} = False
        DISABLE(?Group:FieldFormat)
      END
      ThisWindow.Reset
    OF ?maf:HideRelatedCodeIfBlank
      IF ?maf:HideRelatedCodeIfBlank{Prop:Checked} = True
        ENABLE(?Group:BlankFaultCode)
      END
      IF ?maf:HideRelatedCodeIfBlank{Prop:Checked} = False
        DISABLE(?Group:BlankFaultCode)
      END
      ThisWindow.Reset
    OF ?maf:CompulsoryForRepairType
      IF ?maf:CompulsoryForRepairType{Prop:Checked} = True
        ENABLE(?maf:CompulsoryRepairType)
        ENABLE(?Lookup:RepairType)
      END
      IF ?maf:CompulsoryForRepairType{Prop:Checked} = False
        maf:CompulsoryRepairType = ''
        DISABLE(?maf:CompulsoryRepairType)
        DISABLE(?Lookup:RepairType)
      END
      ThisWindow.Reset
    OF ?maf:CompulsoryRepairType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:CompulsoryRepairType, Accepted)
      glo:Select1 = maf:Manufacturer
      glo:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:CompulsoryRepairType, Accepted)
    OF ?Lookup:RepairTYpe
      ThisWindow.Update
      ! Lookup button pressed. Call select procedure if required
      rtd:Repair_Type = maf:CompulsoryRepairType      ! Move value for lookup
      rtd:Warranty = 'YES'
      rtd:Manufacturer = maf:Manufacturer
      GLO:Select1 = maf:Manufacturer
      IF Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) ! IF record not found
         rtd:Repair_Type = maf:CompulsoryRepairType   ! Move value for lookup
         rtd:Warranty = 'YES'
         rtd:Manufacturer = maf:Manufacturer
         GLO:Select1 = maf:Manufacturer
         ! Based on ?maf:CompulsoryRepairType
         SET(rtd:WarManRepairTypeKey,rtd:WarManRepairTypeKey) ! Prime order for browse
         Access:REPTYDEF.TryNext()
      END
      GlobalRequest = SelectRecord
      PickWarrantyRepairTypes                         ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?Lookup:RepairTYpe)                   ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?maf:CompulsoryRepairType,rtd:Repair_Type)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?Lookup:RepairTYpe+1)                    ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
    OF ?maf:Compulsory
      IF ?maf:Compulsory{Prop:Checked} = True
        ENABLE(?maf:Compulsory_At_Booking)
        ENABLE(?maf:CompulsoryIfExchange)
        ENABLE(?Group:CompulsoryRepairType)
      END
      IF ?maf:Compulsory{Prop:Checked} = False
        maf:Compulsory_At_Booking = 'NO'
        maf:CompulsoryForRepairType = 0
        maf:CompulsoryRepairType = ''
        DISABLE(?maf:CompulsoryIfExchange)
        DISABLE(?maf:Compulsory_At_Booking)
        DISABLE(?Group:CompulsoryRepairType)
      END
      ThisWindow.Reset
    OF ?maf:CharCompulsory
      IF ?maf:CharCompulsory{Prop:Checked} = True
        ENABLE(?maf:CharCompulsoryBooking)
      END
      IF ?maf:CharCompulsory{Prop:Checked} = False
        maf:CharCompulsoryBooking = 0
        DISABLE(?maf:CharCompulsoryBooking)
      END
      ThisWindow.Reset
    OF ?maf:RestrictAvailability
      IF ?maf:RestrictAvailability{Prop:Checked} = True
        ENABLE(?maf:RestrictServiceCentre)
      END
      IF ?maf:RestrictAvailability{Prop:Checked} = False
        DISABLE(?maf:RestrictServiceCentre)
      END
      ThisWindow.Reset
    OF ?maf:NotCompulsoryThirdParty
      IF ?maf:NotCompulsoryThirdParty{Prop:Checked} = True
        ENABLE(?maf:HideThirdParty)
      END
      IF ?maf:NotCompulsoryThirdParty{Prop:Checked} = False
        DISABLE(?maf:HideThirdParty)
      END
      ThisWindow.Reset
    OF ?maf:Lookup
      IF ?maf:Lookup{Prop:Checked} = True
        UNHIDE(?Insert)
        UNHIDE(?Change)
        UNHIDE(?Delete)
        UNHIDE(?MAF:Force_Lookup)
        ENABLE(?List)
      END
      IF ?maf:Lookup{Prop:Checked} = False
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
        HIDE(?MAF:Force_Lookup)
        DISABLE(?List)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button:BrowseLinkedFaultCodes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:BrowseLinkedFaultCodes, Accepted)
      save_maf_id = Access:MANFAULT.SaveFile()
      BrowseRelatedFaultCodes(maf:Manufacturer,maf:Field_Number,0,1)
      Access:MANFAULT.RestoreFile(save_maf_id)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:BrowseLinkedFaultCodes, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case tmp:DatePicture
      Of 'DD/MM/YY'
          maf:DateType = '@d5'
      Of 'DD/MM/YYYY'
          maf:DateType = '@d6'
      Of 'MM/DD/YY'
          maf:DateType = '@d1'
      Of 'MM/DD/YYYY'
          maf:DateType = '@d2'
      Of 'MM/YY'
          maf:DateType = '@d13'
      Of 'MM/YYYY'
          maf:DateType = '@d14'
      Of 'YY/MM'
          maf:DateType = '@d15'
      Of 'YYYY/MM'
          maf:DateType = '@d16'
      Of 'YY/MM/DD'
          maf:DateType = '@d9'
      Of 'YYYY/MM/DD'
          maf:DateType = '@d10'
      Of 'YYMMDD'
          maf:DateType = '@d11'
      Of 'YYYYMMDD'
          maf:DateType = '@d12'
  End !tmp:DatePicture
  !Main Fault Check
  tmp:Manufacturer = maf:Manufacturer
  tmp:FieldNumber  = maf:Field_Number
  tmp:ScreenOrder = maf:ScreenOrder
  tmp:MainFault = maf:MainFault
  tmp:InFault = maf:InFault
  tmp:GenericFault = maf:GenericFault
  !TB13231 J - 11/12/14 - adding new bouncer fault
  !TB13231 J - 17/12/14 - revised - no check on completion - only on box ticking
  !tmp:BouncerFault = maf:BouncerFault
  !BouncerFlag = maf:bouncerFault !(False if this is not one, so it checks for others, true if this is set)
  Error# = 0
  
  
  save_maf_id = Access:MANFAULT.SaveFile()
  Access:MANFAULT.Clearkey(maf:Field_Number_Key)
  maf:Manufacturer = tmp:Manufacturer
  maf:Field_Number = 0
  Set(maf:Field_Number_Key,maf:Field_Number_Key)
  Loop
      If Access:MANFAULT.Next()
          Break
      End ! If Access:MANFAULT.Next()
      If maf:Manufacturer <> tmp:Manufacturer
          Break
      End ! If maf:Manufacturer <> tmp:Manufacturer
      If maf:Field_Number = tmp:FieldNumber
          Cycle
      End ! If maf:Field_Number = tmp:FieldNumber
  
  
      If tmp:MainFault And maf:MainFault
          Case Missive('The fault code "' & Clip(maf:Field_Name) & '" has already been marked as a Main Out Fault.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
          !Break !TB13231 J - 11/12/14 - adding new bouncer fault must check all MANFAULTS to ensure one is set as bouncer
      End ! If MainFault# And maf:MainFault
  
      If tmp:InFault And maf:InFault
          Case Missive('The fault code "' & Clip(maf:Field_Name) & '" has already been marked as the Main In Fault.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
          !Break !TB13231 J - 11/12/14 - adding new bouncer fault must check all MANFAULTS to ensure one is set as bouncer
      End ! If InFault# And maf:InFault
  
      If maf:ScreenOrder = tmp:ScreenOrder
          Case Missive('The fault code "' & Clip(maf:Field_Name) & '" has the same Screen Order Number.','ServiceBase 3g',|
                          'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
          !Break !TB13231 J - 11/12/14 - adding new bouncer fault must check all MANFAULTS to ensure one is set as bouncer
      End ! If maf:ScreenOrder = tmp:ScreenOrder
  
      If tmp:GenericFault And maf:GenericFault
          Case Missive('The fault code "' & Clip(maf:Field_Name) & '" has already been marked as the "Generic Fault Description".','ServiceBase 3g',|
                      'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Error# = 1
          !Break !TB13231 J - 11/12/14 - adding new bouncer fault must check all MANFAULTS to ensure one is set as bouncer
      End ! If GenericFault# And maf:GenericFault
  
  !    !TB13231 J - 11/12/14 - adding new bouncer fault
  !    !there can only be one
  !    if tmp:BouncerFault and maf:BouncerFault
  !        Case Missive('The fault code "' & Clip(maf:Field_Name) & '" has already been marked as the "Bouncer Fault Description".','ServiceBase 3g',|
  !                    'mstop.jpg','/OK')
  !            Of 1 ! OK Button
  !        End ! Case Missive
  !        Error# = 1
  !    END !if bouncerfault
  !    !and there must be one
  !    if maf:BouncerFault then BouncerFlag = 1.
  !    !End TB13231 - J - 11/12/14
  
  End ! Loop
  Access:MANFAULT.RestoreFile(save_maf_id)
  
  !TB13231 revised - no bouncer comment on completion - J - 17/12/14
  !if BouncerFlag = 0 then
  !    Case Missive('Currently no Job Fault Code is enabled as "Use For Bouncer". If you proceed then the Main In Fault will be used as the  "Bouncer". Do you want to proceed?',|
  !                'ServiceBase 3g',|
  !                'mstop.jpg','/YES|\NO')
  !        Of 1 ! YES Button
  !        of 2 ! NO
  !            Error#=1
  !    End ! Case Missive
  !END !if bouncer flag set
  
  
  If Error#
      Cycle
  End ! If Error#
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(event:accepted,?maf:compulsory)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = maf:Manufacturer
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = maf:Field_Number
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.mfo:Field_NormalFG = -1
  SELF.Q.mfo:Field_NormalBG = -1
  SELF.Q.mfo:Field_SelectedFG = -1
  SELF.Q.mfo:Field_SelectedBG = -1
  SELF.Q.mfo:Description_NormalFG = -1
  SELF.Q.mfo:Description_NormalBG = -1
  SELF.Q.mfo:Description_SelectedFG = -1
  SELF.Q.mfo:Description_SelectedBG = -1
   
   
   IF (mfo:PrimaryLookup = 1)
     SELF.Q.mfo:Field_NormalFG = 255
     SELF.Q.mfo:Field_NormalBG = 16777215
     SELF.Q.mfo:Field_SelectedFG = 16777215
     SELF.Q.mfo:Field_SelectedBG = 255
   ELSIF(mfo:NotAvailable = 1)
     SELF.Q.mfo:Field_NormalFG = 8421504
     SELF.Q.mfo:Field_NormalBG = 16777215
     SELF.Q.mfo:Field_SelectedFG = 16777215
     SELF.Q.mfo:Field_SelectedBG = 8421504
   ELSE
     SELF.Q.mfo:Field_NormalFG = -1
     SELF.Q.mfo:Field_NormalBG = -1
     SELF.Q.mfo:Field_SelectedFG = -1
     SELF.Q.mfo:Field_SelectedBG = -1
   END
   IF (mfo:PrimaryLookup = 1)
     SELF.Q.mfo:Description_NormalFG = 255
     SELF.Q.mfo:Description_NormalBG = 16777215
     SELF.Q.mfo:Description_SelectedFG = 16777215
     SELF.Q.mfo:Description_SelectedBG = 255
   ELSIF(mfo:NotAvailable = 1)
     SELF.Q.mfo:Description_NormalFG = 8421504
     SELF.Q.mfo:Description_NormalBG = 16777215
     SELF.Q.mfo:Description_SelectedFG = 16777215
     SELF.Q.mfo:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.mfo:Description_NormalFG = -1
     SELF.Q.mfo:Description_NormalBG = -1
     SELF.Q.mfo:Description_SelectedFG = -1
     SELF.Q.mfo:Description_SelectedBG = -1
   END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Manufacturer_Fault_Coding PROCEDURE            !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Available        STRING(3)
BRW1::View:Browse    VIEW(MANFAULT)
                       PROJECT(maf:Field_Number)
                       PROJECT(maf:ScreenOrder)
                       PROJECT(maf:Field_Name)
                       PROJECT(maf:Field_Type)
                       PROJECT(maf:Compulsory_At_Booking)
                       PROJECT(maf:Compulsory)
                       PROJECT(maf:Lookup)
                       PROJECT(maf:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
maf:Field_Number       LIKE(maf:Field_Number)         !List box control field - type derived from field
maf:Field_Number_NormalFG LONG                        !Normal forground color
maf:Field_Number_NormalBG LONG                        !Normal background color
maf:Field_Number_SelectedFG LONG                      !Selected forground color
maf:Field_Number_SelectedBG LONG                      !Selected background color
maf:ScreenOrder        LIKE(maf:ScreenOrder)          !List box control field - type derived from field
maf:ScreenOrder_NormalFG LONG                         !Normal forground color
maf:ScreenOrder_NormalBG LONG                         !Normal background color
maf:ScreenOrder_SelectedFG LONG                       !Selected forground color
maf:ScreenOrder_SelectedBG LONG                       !Selected background color
maf:Field_Name         LIKE(maf:Field_Name)           !List box control field - type derived from field
maf:Field_Name_NormalFG LONG                          !Normal forground color
maf:Field_Name_NormalBG LONG                          !Normal background color
maf:Field_Name_SelectedFG LONG                        !Selected forground color
maf:Field_Name_SelectedBG LONG                        !Selected background color
maf:Field_Type         LIKE(maf:Field_Type)           !List box control field - type derived from field
maf:Field_Type_NormalFG LONG                          !Normal forground color
maf:Field_Type_NormalBG LONG                          !Normal background color
maf:Field_Type_SelectedFG LONG                        !Selected forground color
maf:Field_Type_SelectedBG LONG                        !Selected background color
maf:Compulsory_At_Booking LIKE(maf:Compulsory_At_Booking) !List box control field - type derived from field
maf:Compulsory_At_Booking_NormalFG LONG               !Normal forground color
maf:Compulsory_At_Booking_NormalBG LONG               !Normal background color
maf:Compulsory_At_Booking_SelectedFG LONG             !Selected forground color
maf:Compulsory_At_Booking_SelectedBG LONG             !Selected background color
maf:Compulsory         LIKE(maf:Compulsory)           !List box control field - type derived from field
maf:Compulsory_NormalFG LONG                          !Normal forground color
maf:Compulsory_NormalBG LONG                          !Normal background color
maf:Compulsory_SelectedFG LONG                        !Selected forground color
maf:Compulsory_SelectedBG LONG                        !Selected background color
maf:Lookup             LIKE(maf:Lookup)               !List box control field - type derived from field
maf:Lookup_NormalFG    LONG                           !Normal forground color
maf:Lookup_NormalBG    LONG                           !Normal background color
maf:Lookup_SelectedFG  LONG                           !Selected forground color
maf:Lookup_SelectedBG  LONG                           !Selected background color
tmp:Available          LIKE(tmp:Available)            !List box control field - type derived from local data
tmp:Available_NormalFG LONG                           !Normal forground color
tmp:Available_NormalBG LONG                           !Normal background color
tmp:Available_SelectedFG LONG                         !Selected forground color
tmp:Available_SelectedBG LONG                         !Selected background color
GLO:Select1            STRING(1)                      !Browse hot field - unable to determine correct data type
maf:Manufacturer       LIKE(maf:Manufacturer)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Fault Code File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code File'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(68,86,476,272),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('49R(2)|M*~Field Number~L@n2@47R(2)|M*~Screen Order~@n2@125L(2)|M*~Field Name~@s3' &|
   '0@83L(2)|M*~Field Type~@s30@[45L(2)|M*~Booking~@s3@40L(2)|M*~Completion~@s3@]|M~' &|
   'Compulsory~30L(2)|M*~Lookup~@s3@12L(2)|M*~Available~@s3@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,136),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(548,276),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(548,304),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(548,332),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n2),AT(68,70,59,12),USE(maf:Field_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code Field Number')
                         END
                         TAB('By Screen Order'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(70,72,64,10),USE(maf:ScreenOrder),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),SINGLE,MSG('Screen Order')
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BOX,AT(472,70,12,10),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Gray)
                       PROMPT('- Not Available'),AT(488,70),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020358'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manufacturer_Fault_Coding')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFAULT.Open
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAULT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Manufacturer_Fault_Coding')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,maf:ScreenOrderKey)
  BRW1.AddRange(maf:Manufacturer,Relate:MANFAULT,Relate:MANUFACT)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,maf:ScreenOrder,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,maf:Field_Number_Key)
  BRW1.AddRange(maf:Manufacturer,Relate:MANFAULT,Relate:MANUFACT)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?maf:Field_Number,maf:Field_Number,1,BRW1)
  BIND('tmp:Available',tmp:Available)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(maf:Field_Number,BRW1.Q.maf:Field_Number)
  BRW1.AddField(maf:ScreenOrder,BRW1.Q.maf:ScreenOrder)
  BRW1.AddField(maf:Field_Name,BRW1.Q.maf:Field_Name)
  BRW1.AddField(maf:Field_Type,BRW1.Q.maf:Field_Type)
  BRW1.AddField(maf:Compulsory_At_Booking,BRW1.Q.maf:Compulsory_At_Booking)
  BRW1.AddField(maf:Compulsory,BRW1.Q.maf:Compulsory)
  BRW1.AddField(maf:Lookup,BRW1.Q.maf:Lookup)
  BRW1.AddField(tmp:Available,BRW1.Q.tmp:Available)
  BRW1.AddField(GLO:Select1,BRW1.Q.GLO:Select1)
  BRW1.AddField(maf:Manufacturer,BRW1.Q.maf:Manufacturer)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Manufacturer_Fault_Coding')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('MANUFACT FAULT CODES - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('MANUFACT FAULT CODES - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
        of deleterecord
            check_access('MANUFACT FAULT CODES - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULT
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020358'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020358'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020358'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (maf:NotAvailable = 1)
    tmp:Available = 'NO'
  ELSE
    tmp:Available = 'YES'
  END
  PARENT.SetQueueRecord
  SELF.Q.maf:Field_Number_NormalFG = -1
  SELF.Q.maf:Field_Number_NormalBG = -1
  SELF.Q.maf:Field_Number_SelectedFG = -1
  SELF.Q.maf:Field_Number_SelectedBG = -1
  SELF.Q.maf:ScreenOrder_NormalFG = -1
  SELF.Q.maf:ScreenOrder_NormalBG = -1
  SELF.Q.maf:ScreenOrder_SelectedFG = -1
  SELF.Q.maf:ScreenOrder_SelectedBG = -1
  SELF.Q.maf:Field_Name_NormalFG = -1
  SELF.Q.maf:Field_Name_NormalBG = -1
  SELF.Q.maf:Field_Name_SelectedFG = -1
  SELF.Q.maf:Field_Name_SelectedBG = -1
  SELF.Q.maf:Field_Type_NormalFG = -1
  SELF.Q.maf:Field_Type_NormalBG = -1
  SELF.Q.maf:Field_Type_SelectedFG = -1
  SELF.Q.maf:Field_Type_SelectedBG = -1
  SELF.Q.maf:Compulsory_At_Booking_NormalFG = -1
  SELF.Q.maf:Compulsory_At_Booking_NormalBG = -1
  SELF.Q.maf:Compulsory_At_Booking_SelectedFG = -1
  SELF.Q.maf:Compulsory_At_Booking_SelectedBG = -1
  SELF.Q.maf:Compulsory_NormalFG = -1
  SELF.Q.maf:Compulsory_NormalBG = -1
  SELF.Q.maf:Compulsory_SelectedFG = -1
  SELF.Q.maf:Compulsory_SelectedBG = -1
  SELF.Q.maf:Lookup_NormalFG = -1
  SELF.Q.maf:Lookup_NormalBG = -1
  SELF.Q.maf:Lookup_SelectedFG = -1
  SELF.Q.maf:Lookup_SelectedBG = -1
  SELF.Q.tmp:Available_NormalFG = -1
  SELF.Q.tmp:Available_NormalBG = -1
  SELF.Q.tmp:Available_SelectedFG = -1
  SELF.Q.tmp:Available_SelectedBG = -1
  SELF.Q.tmp:Available = tmp:Available                !Assign formula result to display queue
   
   
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:Field_Number_NormalFG = 8421504
     SELF.Q.maf:Field_Number_NormalBG = 16777215
     SELF.Q.maf:Field_Number_SelectedFG = 16777215
     SELF.Q.maf:Field_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:Field_Number_NormalFG = -1
     SELF.Q.maf:Field_Number_NormalBG = -1
     SELF.Q.maf:Field_Number_SelectedFG = -1
     SELF.Q.maf:Field_Number_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:ScreenOrder_NormalFG = 8421504
     SELF.Q.maf:ScreenOrder_NormalBG = 16777215
     SELF.Q.maf:ScreenOrder_SelectedFG = 16777215
     SELF.Q.maf:ScreenOrder_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:ScreenOrder_NormalFG = -1
     SELF.Q.maf:ScreenOrder_NormalBG = -1
     SELF.Q.maf:ScreenOrder_SelectedFG = -1
     SELF.Q.maf:ScreenOrder_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:Field_Name_NormalFG = 8421504
     SELF.Q.maf:Field_Name_NormalBG = 16777215
     SELF.Q.maf:Field_Name_SelectedFG = 16777215
     SELF.Q.maf:Field_Name_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:Field_Name_NormalFG = -1
     SELF.Q.maf:Field_Name_NormalBG = -1
     SELF.Q.maf:Field_Name_SelectedFG = -1
     SELF.Q.maf:Field_Name_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:Field_Type_NormalFG = 8421504
     SELF.Q.maf:Field_Type_NormalBG = 16777215
     SELF.Q.maf:Field_Type_SelectedFG = 16777215
     SELF.Q.maf:Field_Type_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:Field_Type_NormalFG = -1
     SELF.Q.maf:Field_Type_NormalBG = -1
     SELF.Q.maf:Field_Type_SelectedFG = -1
     SELF.Q.maf:Field_Type_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:Compulsory_At_Booking_NormalFG = 8421504
     SELF.Q.maf:Compulsory_At_Booking_NormalBG = 16777215
     SELF.Q.maf:Compulsory_At_Booking_SelectedFG = 16777215
     SELF.Q.maf:Compulsory_At_Booking_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:Compulsory_At_Booking_NormalFG = -1
     SELF.Q.maf:Compulsory_At_Booking_NormalBG = -1
     SELF.Q.maf:Compulsory_At_Booking_SelectedFG = -1
     SELF.Q.maf:Compulsory_At_Booking_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:Compulsory_NormalFG = 8421504
     SELF.Q.maf:Compulsory_NormalBG = 16777215
     SELF.Q.maf:Compulsory_SelectedFG = 16777215
     SELF.Q.maf:Compulsory_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:Compulsory_NormalFG = -1
     SELF.Q.maf:Compulsory_NormalBG = -1
     SELF.Q.maf:Compulsory_SelectedFG = -1
     SELF.Q.maf:Compulsory_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.maf:Lookup_NormalFG = 8421504
     SELF.Q.maf:Lookup_NormalBG = 16777215
     SELF.Q.maf:Lookup_SelectedFG = 16777215
     SELF.Q.maf:Lookup_SelectedBG = 8421504
   ELSE
     SELF.Q.maf:Lookup_NormalFG = -1
     SELF.Q.maf:Lookup_NormalBG = -1
     SELF.Q.maf:Lookup_SelectedFG = -1
     SELF.Q.maf:Lookup_SelectedBG = -1
   END
   IF (maf:NotAvailable = 1)
     SELF.Q.tmp:Available_NormalFG = 8421504
     SELF.Q.tmp:Available_NormalBG = 16777215
     SELF.Q.tmp:Available_SelectedFG = 16777215
     SELF.Q.tmp:Available_SelectedBG = 8421504
   ELSE
     SELF.Q.tmp:Available_NormalFG = -1
     SELF.Q.tmp:Available_NormalBG = -1
     SELF.Q.tmp:Available_SelectedFG = -1
     SELF.Q.tmp:Available_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?MAF:Field_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

UpdatePartPrefixes PROCEDURE                          !Generated from procedure template - Form

ActionMessage        CSTRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('Part Number Prefix'),AT(248,192),USE(?msp:PartNumberPrefix:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(332,194,64,10),USE(msp:PartNumberPrefix),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Number Prefix'),TIP('Part Number Prefix'),REQ,UPR
                       PROMPT('Lab Type'),AT(248,212),USE(?msp:PartNumberPrefix:Prompt:2),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(332,212,64,10),USE(msp:LabType),LEFT(2),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,64),FROM('L1|L2|L3'),MSG('Lab Type')
                       BUTTON,AT(296,258),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Part Prefixes'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020712'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdatePartPrefixes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:MANSAMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANSAMP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANSAMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdatePartPrefixes')
  ?msp:LabType{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?msp:LabType{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANSAMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdatePartPrefixes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020712'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020712'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020712'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If msp:LabType = ''
      Cycle
  End ! If msp:LabType = ''
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?msp:LabType{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?msp:LabType{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BrowsePartPrefixLabTypes PROCEDURE                    !Generated from procedure template - Browse

BRW9::View:Browse    VIEW(MANSAMP)
                       PROJECT(msp:PartNumberPrefix)
                       PROJECT(msp:LabType)
                       PROJECT(msp:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
msp:PartNumberPrefix   LIKE(msp:PartNumberPrefix)     !List box control field - type derived from field
msp:LabType            LIKE(msp:LabType)              !List box control field - type derived from field
msp:RecordNumber       LIKE(msp:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       ENTRY(@s30),AT(268,90,124,10),USE(msp:PartNumberPrefix),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Part Number Prefix')
                       LIST,AT(268,104,148,220),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('68L(2)|M~Part Number Prefix~@s15@120L(2)|M~Lab Type~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(424,234),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                       BUTTON,AT(424,268),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                       BUTTON,AT(424,300),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Part Prefix / Lab Types'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020711'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsePartPrefixLabTypes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANSAMP.Open
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:MANSAMP,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowsePartPrefixLabTypes')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,msp:PartPrefixKey)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?msp:PartNumberPrefix,msp:PartNumberPrefix,1,BRW9)
  BRW9.AddField(msp:PartNumberPrefix,BRW9.Q.msp:PartNumberPrefix)
  BRW9.AddField(msp:LabType,BRW9.Q.msp:LabType)
  BRW9.AddField(msp:RecordNumber,BRW9.Q.msp:RecordNumber)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANSAMP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowsePartPrefixLabTypes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdatePartPrefixes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020711'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020711'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020711'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateMANUFACT PROCEDURE                              !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
tmp:ModelToDelete    STRING(30)
tmp:UseInternetWarranty STRING(3)
save_stm_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_sta_id          USHORT,AUTO
save_suc_id          USHORT,AUTO
save_trc_id          USHORT,AUTO
pos                  STRING(255)
tmp:manufacturer     STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
Charge_Type_Temp     STRING(30)
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
tmp:Yes              STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ManufacturerQueue QUEUE,PRE(tmpque)
EDIFile              STRING(30)
                     END
tmp:UseLocalFileValidation BYTE
tmp:WebUserName      STRING(30)
tmp:WebPassword      STRING(30)
tmp:CCTImportFile    STRING(255),STATIC
tmp:SavePath         STRING(255)
tmp:inactive         BYTE
Tmp:Long             LONG
BRW15::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:ESN_Length_From)
                       PROJECT(mod:ESN_Length_To)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:ESN_Length_From    LIKE(mod:ESN_Length_From)      !List box control field - type derived from field
mod:ESN_Length_To      LIKE(mod:ESN_Length_To)        !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::man:Record  LIKE(man:RECORD),STATIC
QuickWindow          WINDOW('Update Manufacturer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Procedures'),USE(?Procedures)
                           ITEM('Import Exchange Selling Price'),USE(?ProceduresImportExchangeSellingPrice)
                         END
                       END
                       PROMPT('Insert / Amend Manufacturer'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(588,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,23,216,358),USE(?Sheet3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab3)
                           PROMPT('Manufacturer'),AT(8,36),USE(?MAN:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(man:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,MSG('Manufacturer')
                           PROMPT('Account Number'),AT(8,50),USE(?man:AccountNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(84,53,64,10),USE(man:Account_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,MSG('Account Number')
                           PROMPT('Postcode'),AT(8,68),USE(?MAN:Postcode:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(84,66,64,10),USE(man:Postcode),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Postcode')
                           BUTTON,AT(152,60),USE(?Clear_Address),SKIP,TRN,FLAT,LEFT,ICON('clearp.jpg')
                           PROMPT('Address'),AT(8,88),USE(?man:AddressLine1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,88,124,10),USE(man:Address_Line1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Address Line 1')
                           ENTRY(@s30),AT(84,100,124,10),USE(man:Address_Line2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Address Line 2')
                           ENTRY(@s30),AT(84,112,124,10),USE(man:Address_Line3),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Address Line 3')
                           PROMPT('Contact Numbers'),AT(8,132,72,14),USE(?man:TelephoneNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(84,132,60,10),USE(man:Telephone_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Telephone Number')
                           ENTRY(@s15),AT(148,132,60,10),USE(man:Fax_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Fax Number')
                           PROMPT('Contact Name 1'),AT(8,164),USE(?man:ContactName1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s60),AT(84,164,124,10),USE(man:Contact_Name1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Contact Name 1')
                           STRING('Telephone No'),AT(84,124),USE(?String2),TRN,FONT(,7,COLOR:White,FONT:bold)
                           PROMPT('Contact Name 2'),AT(8,176),USE(?man:ContactName2:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s60),AT(84,176,124,10),USE(man:Contact_Name2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Contact Name 2')
                           STRING('Fax Number'),AT(148,124),USE(?String3),TRN,FONT(,7,COLOR:White,FONT:bold)
                           PROMPT('Head Office'),AT(8,200),USE(?man:HeadOfficeTel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           STRING('Telephone No'),AT(84,188),USE(?String2:2),TRN,FONT(,7,COLOR:White,FONT:bold)
                           ENTRY(@s15),AT(84,196,60,10),USE(man:Head_Office_Telephone),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Head Office Telephone Number')
                           STRING('Fax Number'),AT(148,188),USE(?String3:2),TRN,FONT(,7,COLOR:White,FONT:bold)
                           ENTRY(@s15),AT(148,196,60,10),USE(man:Head_Office_Fax),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Head Office Fax Number')
                           STRING('Telephone No'),AT(84,208),USE(?man:tech_support:prompt),TRN,FONT(,7,COLOR:White,FONT:bold)
                           STRING('Fax Number'),AT(148,208),USE(?man:FaxNumber:prompt),TRN,FONT(,7,COLOR:White,FONT:bold)
                           PROMPT('Technical Support'),AT(8,216),USE(?man:TechsuppTel:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(84,216,60,10),USE(man:Technical_Support_Telephone),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Technical Support Telephone Number')
                           ENTRY(@s15),AT(148,216,60,10),USE(man:Technical_Support_Fax),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Technical Support Fax Number')
                           PROMPT('Tech. Support Hrs'),AT(8,228),USE(?man:TechSuppHours:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,228,124,10),USE(man:Technical_Support_Hours),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Technical Support Hours')
                           PROMPT('Notes'),AT(8,242),USE(?MAN:Notes:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(84,242,124,34),USE(man:Notes),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('VAT Number'),AT(8,280),USE(?man:VATNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(84,280,124,10),USE(man:VATNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('VAT Number'),TIP('VAT Number'),UPR
                           PROMPT('Supplier'),AT(8,298),USE(?man:supplier:prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,298,104,10),USE(man:Supplier),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Supplier')
                           BUTTON,AT(188,294),USE(?LookupSupplier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Email Address'),AT(8,148),USE(?man:EmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,148,124,10),USE(man:EmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                         END
                       END
                       SHEET,AT(224,25,244,356),USE(?Sheet4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('General Defaults'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Use Product Code'),AT(312,41),USE(man:UseProductCode),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Product Code'),TIP('Use Product Code'),VALUE('1','0')
                           CHECK('Product Code Compulsory'),AT(312,53),USE(man:ProductCodeCompulsory),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Use M.S.N.'),AT(312,66),USE(man:Use_MSN),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO'),MSG('Models Use MSN (YES/NO)')
                           CHECK('Apply M.S.N. Format'),AT(372,66),USE(man:ApplyMSNFormat),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Apply M.S.N. Format'),TIP('Apply M.S.N. Format'),VALUE('1','0')
                           PROMPT('M.S.N. Format'),AT(228,81),USE(?man:MSNFormat:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(312,81,124,10),USE(man:MSNFormat),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.S.N. Format'),TIP('M.S.N. Format'),UPR
                           CHECK('Validate Date Code'),AT(312,94),USE(man:ValidateDateCode),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Validate Date Code (True/False)')
                           CHECK('Manufacturer Warranty 1 Year Only'),AT(312,108),USE(man:OneYearWarrOnly),TRN,FONT(,,COLOR:White,FONT:bold),VALUE('Y','N')
                           GROUP('Validate Date Code'),AT(228,250,236,68),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Force Status'),AT(232,271),USE(man:ForceStatus),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Status'),TIP('Force Status'),VALUE('1','0')
                             PROMPT('Status Required'),AT(312,263),USE(?man:StatusRequired:Prompt),TRN,LEFT,FONT('Tahoma',7,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(312,271,124,10),USE(man:StatusRequired),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Status Required'),TIP('Status Required'),UPR
                             BUTTON,AT(436,266),USE(?LookupStatus),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('P.O.P. Period'),AT(232,287),USE(?man:POPPeriod:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(312,287,64,10),USE(man:POPPeriod),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('P.O.P. Period'),TIP('P.O.P. Period'),UPR
                             PROMPT('Months'),AT(380,287),USE(?Prompt26),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             PROMPT('Claim Period'),AT(232,303),USE(?man:ClaimPeriod:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(312,303,64,10),USE(man:ClaimPeriod),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Claim Period'),TIP('Claim Period'),UPR
                             PROMPT('Months'),AT(380,303),USE(?Prompt26:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           END
                           PROMPT('Third Party Handling Fee'),AT(228,326,80,18),USE(?man:ThirdPartyHandlingFee:Prompt)
                           ENTRY(@n14.2),AT(312,326,64,10),USE(man:ThirdPartyHandlingFee),DECIMAL(14),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(228,347),USE(?DateCodeValidation),TRN,FLAT,HIDE,LEFT,ICON('datevalp.jpg')
                           BUTTON,AT(296,347),USE(?FormatKey),TRN,FLAT,HIDE,LEFT,ICON('msnkeyp.jpg')
                           BUTTON,AT(364,347),USE(?InWarrantyMarkups),TRN,FLAT,LEFT,ICON('inwarmkp.jpg')
                           GROUP('QA Defaults'),AT(228,118,236,58),USE(?QADefaults),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Use QA'),AT(236,129),USE(man:UseQA),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use QA'),TIP('Use QA'),VALUE('1','0')
                             CHECK('Use Electronic QA'),AT(360,129),USE(man:UseElectronicQA),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Electronic QA'),TIP('Use Electronic QA'),VALUE('1','0')
                             CHECK('QA Exchange Units'),AT(236,140),USE(man:QALoanExchange),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('QA Loan/Exchange Units'),TIP('QA Loan/Exchange Units'),VALUE('1','0')
                             CHECK('QA Loans'),AT(360,140),USE(man:QALoan),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('QA Loans'),TIP('QA Loans'),VALUE('1','0')
                             CHECK('Completion At QA Pass'),AT(236,153),USE(man:QAAtCompletion),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('QA At Completion'),TIP('QA At Completion'),VALUE('1','0')
                             CHECK('Validate Parts At QA'),AT(360,153),USE(man:QAParts),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('QAParts'),TIP('QAParts'),VALUE('1','0')
                             CHECK('Validate Network At QA'),AT(360,164),USE(man:QANetwork),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Validate Network At QA'),TIP('Validate Network At QA'),VALUE('1','0')
                           END
                           GROUP('Warranty Validation'),AT(228,178,236,68),USE(?InternetWarrantyGroup),BOXED,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Use Local File to Validate Warranty on Job Booking'),AT(232,189),USE(tmp:UseLocalFileValidation),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                             CHECK('Use Internet System to Validate Warranty on Job Booking'),AT(232,202),USE(tmp:UseInternetWarranty),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                             PROMPT('User Name'),AT(236,213),USE(?tmp:WebUserName:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(308,213,124,10),USE(tmp:WebUserName),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Password'),AT(236,229),USE(?tmp:WebPassword:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(308,229,124,10),USE(tmp:WebPassword),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           END
                         END
                         TAB('cont...'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(228,87,208,216),USE(?groupBouncerRules)
                             PROMPT('Bouncer Period'),AT(228,127),USE(?man:BouncerPeriod:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@n-14),AT(348,129,64,10),USE(man:BouncerPeriod),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Days'),AT(416,129),USE(?Prompt46:2),TRN,FONT(,,COLOR:White,FONT:bold)
                             OPTION('Bouncer Period Based On'),AT(228,147,184,56),USE(man:BouncerType),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI)
                               RADIO('Booking Date'),AT(240,159),USE(?man:BouncerType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                               RADIO('Completed Date'),AT(240,173),USE(?man:BouncerType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                               RADIO('Despatched Date'),AT(240,187),USE(?man:BouncerType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('2')
                             END
                             OPTION('When Below Bouncer Rules Apply'),AT(228,206,184,89),USE(man:BounceRulesType),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),MSG('Bounce Rules Type')
                               RADIO('Bouncer if ALL Rules Are Met'),AT(240,220),USE(?Option2:Radio1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('0')
                               RADIO('Bouncer if ANY Rules Are Met'),AT(240,234),USE(?Option2:Radio1:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1')
                             END
                             CHECK('Bouncer If Same Designated Fault Code'),AT(240,255),USE(man:BouncerInFault),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                             CHECK('Bouncer If Same Out Fault'),AT(240,268),USE(man:BouncerOutFault),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                             CHECK('Bouncer If Same Spares Used'),AT(240,281),USE(man:BouncerSpares),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                             PROMPT('Bouncer Period (IMEI Only)'),AT(228,89,112,12),USE(?man:BouncerPeriodIMEI:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@n-14),AT(348,89,64,10),USE(man:BouncerPeriodIMEI),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),TIP('This will bounce a job within the period only based on the IMEI Number. <13,10>None o' &|
   'f the other manufacturer criteria will be used.')
                             PROMPT('Note: Do not enter a period greater than the Bouncer Period below.'),AT(228,103),USE(?Prompt48),TRN,FONT(,7,COLOR:White,FONT:bold)
                             PROMPT('Enter "0" to disable the IMEI Only Check.'),AT(248,111),USE(?Prompt48:2),TRN,FONT(,7,COLOR:White,FONT:bold)
                             LINE,AT(229,123,232,0),USE(?Line1),COLOR(COLOR:White),LINEWIDTH(1)
                             PROMPT('Days'),AT(416,89),USE(?Prompt46),TRN,FONT(,,COLOR:White,FONT:bold)
                           END
                           CHECK('Exclude Manufacturer From Automatic Rebooking Process'),AT(228,43),USE(man:ExcludeAutomaticRebookingProcess),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Exclude Automatic Rebooking Process'),TIP('Exclude Automatic Rebooking Process'),VALUE('1','0')
                           CHECK('Exclude From Warranty Bouncer Table'),AT(228,57),USE(man:DoNotBounceWarranty),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           CHECK('Use Manufacturer Warranty Bouncer Rules'),AT(228,71),USE(man:UseBouncerRules),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Manufacturer Warranty Bouncer Rules'),TIP('Use Manufacturer Warranty Bouncer Rules'),VALUE('1','0')
                         END
                         TAB('E.D.I. Defaults'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('EDI Account Number'),AT(228,85),USE(?man:EDIAccountNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,85,64,10),USE(man:EDI_Account_Number),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('EDI Account Number')
                           PROMPT('EDI Export Path'),AT(228,97),USE(?man:EDIPath:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(312,99,124,10),USE(man:EDI_Path),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('EDI File Export Path')
                           BUTTON,AT(440,94),USE(?EDI_Path_lookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Warranty Period From DOP'),AT(228,110,72,16),USE(?man:WarrantyPeriod:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(312,113,64,10),USE(man:Warranty_Period),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,MSG('Warranty Period From DOP')
                           PROMPT('Days'),AT(380,113),USE(?man:WarrantyPeriod:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Months'),AT(380,126),USE(?man:WarrantyPeriod:Prompt:3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Transport Fee'),AT(228,141),USE(?man:EDItransportFee:Prompt),TRN,HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n-14.2),AT(312,141,64,10),USE(man:EDItransportFee),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('EDI transportation Fee for ZTE'),TIP('EDI transportation Fee for ZTE'),UPR
                           CHECK('Create Service History Report'),AT(336,57),USE(man:CreateEDIReport),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Create Service History Report'),TIP('Create Service History Report'),VALUE('1','0')
                           PROMPT('EDI Export Format'),AT(228,45),USE(?Prompt30),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(312,45,124,10),USE(man:EDIFileType),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),REQ,READONLY,FORMAT('120L(2)|M@s30@'),DROP(20,124),FROM(tmp:ManufacturerQueue),MSG('E.D.I. File Type')
                           CHECK('Create EDI Export File'),AT(228,57),USE(man:CreateEDIFile),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Create EDI Export File'),TIP('Create EDI Export File'),VALUE('1','0')
                           PROMPT('Claim Period'),AT(228,129),USE(?man:ClaimPeriod:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s8),AT(312,126,64,10),USE(man:ClaimPeriod,,?man:ClaimPeriod:2),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Claim Period'),TIP('Claim Period'),UPR
                           CHECK('D.O.P. Compulsory for Warranty Jobs'),AT(244,179),USE(man:DOPCompulsory),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('D.O.P. Compulsory for Warranty'),TIP('D.O.P. Compulsory for Warranty'),VALUE('1','0')
                           CHECK('POP Required For Claim'),AT(228,165),USE(man:POPRequired),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('POP Required For Claim'),TIP('POP Required For Claim'),VALUE('1','0')
                           CHECK('Remove Costs From Acc Claim Report'),AT(244,190),USE(man:RemAccCosts),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Remove Warranty Claim Costs'),TIP('Remove costs from Accessory Claims Report'),VALUE('1','0')
                           PROMPT('EDI Claim Number'),AT(228,211),USE(?MAN:SamsungCount:Prompt),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n-14),AT(312,211,64,10),USE(man:SamsungCount),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Count Of Samsung Claims'),TIP('Count Of Samsung Claims'),UPR
                           PROMPT('Technical Report Version'),AT(228,227,80,18),USE(?man:SagemVersionNumber:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,227,64,10),USE(man:SagemVersionNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Technical Report Version'),TIP('Technical Report Version')
                           CHECK('Use New EDI Format'),AT(312,246),USE(man:SiemensNewEDI),TRN,HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use New EDI Format'),TIP('Use New EDI Format'),VALUE('1','0')
                           PROMPT('1st Yr Warranty Acc'),AT(228,261),USE(?Prompt14),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,261,124,10),USE(man:Trade_Account),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Warranty Account')
                           BUTTON,AT(440,257),USE(?LookupAccountNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('2nd Yr Warranty Acc'),AT(228,281),USE(?Prompt14:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(312,281,124,10),USE(man:SecondYrTradeAccount),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,MSG('Second Year Warranty Account')
                           BUTTON,AT(440,278),USE(?Lookup2ndYrAccount),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(228,307),USE(?EDI_Defaults),TRN,FLAT,HIDE,LEFT,ICON('edidefp.jpg')
                           CHECK('Include Out Of Warranty Jobs'),AT(228,69),USE(man:IncludeCharJobs),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Include Out Of Warranty Jobs'),TIP('Include Out Of Warranty Jobs'),VALUE('1','0')
                         END
                         TAB('cont ...'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Include "Adjustments" in EDI Exports'),AT(228,41),USE(man:IncludeAdjustment),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Include Warranty Adjustments'),TIP('Include Warranty Adjustments'),VALUE('YES','NO')
                           CHECK('Part Number Required For Warranty "Adjustmements"'),AT(228,53),USE(man:AdjustPart),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Part Number'),TIP('Force Part Number'),VALUE('1','0')
                           CHECK('Force Warranty Adjustment If No Parts Used'),AT(228,65),USE(man:ForceParts),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Warranty Adjustment If No Parts Used'),TIP('Force Warranty Adjustment If No Parts Used'),VALUE('1','0')
                           CHECK('Use Main Fault For Invoice Text'),AT(228,77),USE(man:UseInvTextForFaults),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Use Invoice Text For Fault Codes'),TIP('Use Invoice Text For Fault Codes'),VALUE('1','0')
                           CHECK('Automatically Compute Repair Type'),AT(256,89),USE(man:AutoRepairType),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('AutoRepairType (True/False)')
                           CHECK('Display Billing Confirmation Screen'),AT(256,102),USE(man:BillingConfirmation),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Display Billing Confirmation Screen (True/False)')
                           CHECK('Force Accessory Fault Codes Only'),AT(228,126),USE(man:ForceAccessoryCode),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Accessory Fault Codes Only'),TIP('Force Accessory Fault Codes Only'),VALUE('1','0')
                           CHECK('Force "Key Repair" Part On Job'),AT(228,139),USE(man:KeyRepairRequired),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force "Key Repair" Part'),TIP('Force "Key Repair" Part'),VALUE('1','0')
                           CHECK('Force Out Of Warranty Fault Codes'),AT(228,150),USE(man:ForceCharFaultCodes),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Out Of Warranty Fault Codes'),TIP('Force Out Of Warranty Fault Codes'),VALUE('1','0')
                           PROMPT('First Year Warranty'),AT(312,163),USE(?Prompt34),TRN,FONT(,7,COLOR:White,FONT:bold)
                           PROMPT('Second Year Warranty'),AT(384,163),USE(?Prompt34:2),TRN,FONT(,7,COLOR:White,FONT:bold)
                           PROMPT('Exchange Fee'),AT(228,171),USE(?man:ExchangeFee:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n14.2),AT(312,171,64,10),USE(man:ExchangeFee),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Fee'),TIP('Exchange Fee'),UPR
                           ENTRY(@n14.2),AT(384,171,64,10),USE(man:SecondYrExchangeFee),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Fee'),TIP('Exchange Fee'),UPR
                           CHECK('Use Handset Part Number Of Exchanges'),AT(228,193),USE(man:UseProdCodesForEXC),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Use Handset Part Number Of Exchanges')
                           CHECK('Use Fault Codes For OBF Jobs'),AT(228,206),USE(man:UseFaultCodesForOBF),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Fault Code For OBF Jobs'),TIP('Use Fault Code For OBF Jobs'),VALUE('1','0')
                           CHECK('Auto "Final Reject" claims not resubmitted'),AT(228,222),USE(man:UseResubmissionLimit),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Warranty Claim Resubmission Limit'),TIP('Use Warranty Claim Resubmission Limit'),VALUE('1','0')
                           PROMPT('Days To Resubmit By'),AT(228,238),USE(?man:ResubmissionLimit:Prompt),DISABLE,TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(312,238,64,10),USE(man:ResubmissionLimit),DISABLE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('"Final Reject" claims not resubmitted in'),TIP('"Final Reject" claims not resubmitted in'),UPR
                           CHECK('Limit Resubmissions'),AT(228,253),USE(man:LimitResubmissions),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Limit Resubmissions'),TIP('Limit Resubmissions'),VALUE('1','0')
                           PROMPT('Times Allowed To Resubmit'),AT(228,265,80,18),USE(?man:TimesToResubmit:Prompt),DISABLE,TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(312,267,64,10),USE(man:TimesToResubmit),DISABLE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Times Allowed To Resubmit'),TIP('Times Allowed To Resubmit'),UPR
                           CHECK('Auto-create Technical Reports'),AT(228,291),USE(man:AutoTechnicalReports),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Auto-create Technical Reports'),TIP('Auto-create Technical Reports'),VALUE('1','0')
                           PROMPT('Alert Email Address'),AT(228,301),USE(?man:AlertEmailAddress:Prompt),DISABLE,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s255),AT(312,301,124,10),USE(man:AlertEmailAddress),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Alert Email Address'),TIP('Alert Email Address')
                           PROMPT('Seperate multiple email addresses with a comma.'),AT(228,313),USE(?Prompt40),DISABLE,TRN,FONT(,7,COLOR:White,)
                           CHECK('Copy D.O.P. From Bouncer Job'),AT(228,325),USE(man:CopyDOPFromBouncer),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Copy D.O.P. From Bouncer Job'),TIP('Copy D.O.P. From Bouncer Job'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(472,25,204,356),USE(?Sheet5),COLOR(0D6E7EFH),SPREAD
                         TAB('Model Numbers'),USE(?ModelNumberTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(476,55,196,286),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('116L(2)|M~Model Number~@s30@[29D(2)|M~From~L@n2@8D(2)|M~To~L@n2@]|M~Restrict I.M' &|
   '.E.I.~'),FROM(Queue:Browse)
                           CHECK('Use Replenishment Process'),AT(476,41),USE(man:UseReplenishmentProcess),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Replenishment Process'),TIP('Use Replenishment Process'),VALUE('1','0')
                           BUTTON,AT(476,347),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(544,347),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(608,347),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(8,352),USE(?JobFaultCoding),TRN,FLAT,LEFT,ICON('jobfaup.jpg')
                       BUTTON,AT(76,352),USE(?PartFaultCoding),TRN,FLAT,ICON('partfaup.jpg')
                       BUTTON,AT(540,384),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       CHECK('Inactive Manufacturer'),AT(8,325),USE(tmp:inactive),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                       BUTTON,AT(76,386),USE(?Button:LabTypes),TRN,FLAT,HIDE,ICON('labtypp.jpg')
                       BUTTON,AT(144,386),USE(?Button:RejectionCodes),TRN,FLAT,ICON('querejp.jpg')
                       BUTTON,AT(8,386),USE(?Button:ImportCCTRefNos),TRN,FLAT,HIDE,ICON('impcctp.jpg')
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('pcancel.ico'),HIDE
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW15                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_mod_id   ushort,auto
save_rep_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:man:Supplier                Like(man:Supplier)
look:man:StatusRequired                Like(man:StatusRequired)
look:man:Trade_Account                Like(man:Trade_Account)
look:man:SecondYrTradeAccount                Like(man:SecondYrTradeAccount)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
        Map
RemovePriceStructures       Procedure(String    func:ModelNumber,String     func:RepairType)
        End
CCTImportFile    File,Driver('BASIC'),Pre(cctimp),Name(tmp:CCTImportFile),Create,Bindable,Thread
Record              Record
Field1              String(1)
SalesModel          String(30)
CCTReference        String(30)
                    End
                End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Manufacturer'
  OF ChangeRecord
    ActionMessage = 'Changing A Manufacturer'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020378'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANUFACT')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(man:Record,History::man:Record)
  SELF.AddHistoryField(?man:Manufacturer,2)
  SELF.AddHistoryField(?man:Account_Number,3)
  SELF.AddHistoryField(?man:Postcode,4)
  SELF.AddHistoryField(?man:Address_Line1,5)
  SELF.AddHistoryField(?man:Address_Line2,6)
  SELF.AddHistoryField(?man:Address_Line3,7)
  SELF.AddHistoryField(?man:Telephone_Number,8)
  SELF.AddHistoryField(?man:Fax_Number,9)
  SELF.AddHistoryField(?man:Contact_Name1,12)
  SELF.AddHistoryField(?man:Contact_Name2,13)
  SELF.AddHistoryField(?man:Head_Office_Telephone,14)
  SELF.AddHistoryField(?man:Head_Office_Fax,15)
  SELF.AddHistoryField(?man:Technical_Support_Telephone,16)
  SELF.AddHistoryField(?man:Technical_Support_Fax,17)
  SELF.AddHistoryField(?man:Technical_Support_Hours,18)
  SELF.AddHistoryField(?man:Notes,33)
  SELF.AddHistoryField(?man:VATNumber,65)
  SELF.AddHistoryField(?man:Supplier,23)
  SELF.AddHistoryField(?man:EmailAddress,10)
  SELF.AddHistoryField(?man:UseProductCode,40)
  SELF.AddHistoryField(?man:ProductCodeCompulsory,91)
  SELF.AddHistoryField(?man:Use_MSN,11)
  SELF.AddHistoryField(?man:ApplyMSNFormat,41)
  SELF.AddHistoryField(?man:MSNFormat,42)
  SELF.AddHistoryField(?man:ValidateDateCode,43)
  SELF.AddHistoryField(?man:OneYearWarrOnly,78)
  SELF.AddHistoryField(?man:ForceStatus,44)
  SELF.AddHistoryField(?man:StatusRequired,45)
  SELF.AddHistoryField(?man:POPPeriod,46)
  SELF.AddHistoryField(?man:ClaimPeriod,47)
  SELF.AddHistoryField(?man:ThirdPartyHandlingFee,90)
  SELF.AddHistoryField(?man:UseQA,34)
  SELF.AddHistoryField(?man:UseElectronicQA,35)
  SELF.AddHistoryField(?man:QALoanExchange,36)
  SELF.AddHistoryField(?man:QALoan,60)
  SELF.AddHistoryField(?man:QAAtCompletion,37)
  SELF.AddHistoryField(?man:QAParts,48)
  SELF.AddHistoryField(?man:QANetwork,49)
  SELF.AddHistoryField(?man:BouncerPeriod,82)
  SELF.AddHistoryField(?man:BouncerType,83)
  SELF.AddHistoryField(?man:BounceRulesType,89)
  SELF.AddHistoryField(?man:BouncerInFault,84)
  SELF.AddHistoryField(?man:BouncerOutFault,85)
  SELF.AddHistoryField(?man:BouncerSpares,86)
  SELF.AddHistoryField(?man:BouncerPeriodIMEI,87)
  SELF.AddHistoryField(?man:ExcludeAutomaticRebookingProcess,77)
  SELF.AddHistoryField(?man:DoNotBounceWarranty,88)
  SELF.AddHistoryField(?man:UseBouncerRules,81)
  SELF.AddHistoryField(?man:EDI_Account_Number,20)
  SELF.AddHistoryField(?man:EDI_Path,21)
  SELF.AddHistoryField(?man:Warranty_Period,24)
  SELF.AddHistoryField(?man:EDItransportFee,79)
  SELF.AddHistoryField(?man:CreateEDIReport,56)
  SELF.AddHistoryField(?man:EDIFileType,57)
  SELF.AddHistoryField(?man:CreateEDIFile,58)
  SELF.AddHistoryField(?man:ClaimPeriod:2,47)
  SELF.AddHistoryField(?man:DOPCompulsory,32)
  SELF.AddHistoryField(?man:POPRequired,50)
  SELF.AddHistoryField(?man:RemAccCosts,30)
  SELF.AddHistoryField(?man:SamsungCount,25)
  SELF.AddHistoryField(?man:SagemVersionNumber,80)
  SELF.AddHistoryField(?man:SiemensNewEDI,31)
  SELF.AddHistoryField(?man:Trade_Account,22)
  SELF.AddHistoryField(?man:SecondYrTradeAccount,64)
  SELF.AddHistoryField(?man:IncludeCharJobs,62)
  SELF.AddHistoryField(?man:IncludeAdjustment,26)
  SELF.AddHistoryField(?man:AdjustPart,27)
  SELF.AddHistoryField(?man:ForceParts,28)
  SELF.AddHistoryField(?man:UseInvTextForFaults,51)
  SELF.AddHistoryField(?man:AutoRepairType,54)
  SELF.AddHistoryField(?man:BillingConfirmation,55)
  SELF.AddHistoryField(?man:ForceAccessoryCode,52)
  SELF.AddHistoryField(?man:KeyRepairRequired,68)
  SELF.AddHistoryField(?man:ForceCharFaultCodes,61)
  SELF.AddHistoryField(?man:ExchangeFee,59)
  SELF.AddHistoryField(?man:SecondYrExchangeFee,63)
  SELF.AddHistoryField(?man:UseProdCodesForEXC,66)
  SELF.AddHistoryField(?man:UseFaultCodesForOBF,67)
  SELF.AddHistoryField(?man:UseResubmissionLimit,70)
  SELF.AddHistoryField(?man:ResubmissionLimit,71)
  SELF.AddHistoryField(?man:LimitResubmissions,74)
  SELF.AddHistoryField(?man:TimesToResubmit,75)
  SELF.AddHistoryField(?man:AutoTechnicalReports,72)
  SELF.AddHistoryField(?man:AlertEmailAddress,73)
  SELF.AddHistoryField(?man:CopyDOPFromBouncer,76)
  SELF.AddHistoryField(?man:UseReplenishmentProcess,69)
  SELF.AddUpdateFile(Access:MANUFACT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Access:REPTYDEF.UseFile
  Access:STOCK.UseFile
  Access:STOMODEL.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUPPLIER.UseFile
  Access:SUBTRACC.UseFile
  Access:MODELCCT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAUPA.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANUFACT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW15.Init(?List,Queue:Browse.ViewPosition,BRW15::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:QAPreliminary = 'YES'
      ?man:UseElectronicQA{prop:Hide} = 1
  End !def:QAPreliminary = 'YES'
  POST(Event:Accepted,?man:UseInvTextForFaults)
  
  tmpque:EDIFIle = 'ALCATEL'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'APPLE'
  Add(tmp:ManufacturerQueue)   !added 02/02/15 - J - TB13393
  tmpque:EDIFIle = 'BLACKBERRY'
  Add(tmp:ManufacturerQueue)   !Added about 25/06/13 JC TB13053
  tmpque:EDIFIle = 'BOSCH'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'ERICSSON'
  Add(tmp:ManufacturerQueue)
  !Added 29/04/09 bt pS log no 10769
  tmpque:EDIFIle = 'HTC'
  Add(tmp:ManufacturerQueue)
  !added by J  30/03/12 - log 12360
  tmpque:EDIFIle = 'HUAWEI'    
  Add(tmp:ManufacturerQueue)
  ! Add LG - TrkBs: 5295 (DBH: 25-04-2005)
  tmpque:EDIFIle = 'LG'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'MAXON'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'MITSUBISHI'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'MOTOROLA'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'NEC'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'NOKIA'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'PANASONIC'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'PHILIPS'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'SAGEM'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'SAMSUNG'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'SAMSUNG SA'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'SIEMENS'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'SONY'
  Add(tmp:ManufacturerQueue)
  tmpque:EDIFIle = 'TELITAL'
  Add(tmp:ManufacturerQueue)
  !added by Paul 07/10/2010 - Log No 11629
  tmpque:EDIFIle = 'ZTE'
  Add(tmp:ManufacturerQueue)
  
  ! Inserting (DBH 29/04/2008) # 9723 - Import CCT Ref No
  If man:Manufacturer = 'NOKIA'
      ?Button:ImportCCTRefNos{prop:Hide} = 0
  Else ! If man:Manufacturer = 'NOKIA'
      ?Button:ImportCCTRefNos{prop:Hide} = 1
  End ! If man:Manufacturer = 'NOKIA'
  ! End (DBH 29/04/2008) #9723
  
  ! Inserting (DBH 06/05/2008) # 9723 - Show part number/lab types
  If man:Manufacturer = 'SAMSUNG'
      ?Button:LabTypes{prop:Hide} = 0
  End ! If man:Manufacturer = 'SAMSUNG'
  ! End (DBH 06/05/2008) #9723
  
  !added by paul - 14/10/2010 - log no 11637
  If clip(man:EDIFileType) = 'ZTE' then
      ?man:EDItransportFee{prop:Hide} = False
      ?man:EDItransportFee:Prompt{prop:hide} = False
  Else
      ?man:EDItransportFee{prop:Hide} = True
      ?man:EDItransportFee:Prompt{prop:hide} = True
  End
  If (CLIP(man:EDIFileType) = 'SAGEM')
      ! #11673 New field (Bryan: 03/02/2011)
      ?man:SagemVersionNumber{prop:Hide} = 0
      ?man:SagemVersionNumber:Prompt{prop:Hide} = 0
  ELSE
      ?man:SagemVersionNumber{prop:Hide} = 1
      ?man:SagemVersionNumber:Prompt{prop:Hide} = 1
  END
  
  !if man:Notes[1:8] = 'INACTIVE' then
  !TB13214 - change to using field for inactive  - J 05/02/14
  if man:Inactive = 1 then 
      tmp:inactive = true
  ELSE
      tmp:inactive = false
  END
  ! Save Window Name
   AddToLog('Window','Open','UpdateMANUFACT')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?man:EDIFileType{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?man:StatusRequired{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?man:StatusRequired{Prop:Tip}
  END
  IF ?man:StatusRequired{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?man:StatusRequired{Prop:Msg}
  END
  IF ?man:Trade_Account{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?man:Trade_Account{Prop:Tip}
  END
  IF ?man:Trade_Account{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?man:Trade_Account{Prop:Msg}
  END
  IF ?man:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?man:Supplier{Prop:Tip}
  END
  IF ?man:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?man:Supplier{Prop:Msg}
  END
  IF ?man:SecondYrTradeAccount{Prop:Tip} AND ~?Lookup2ndYrAccount{Prop:Tip}
     ?Lookup2ndYrAccount{Prop:Tip} = 'Select ' & ?man:SecondYrTradeAccount{Prop:Tip}
  END
  IF ?man:SecondYrTradeAccount{Prop:Msg} AND ~?Lookup2ndYrAccount{Prop:Msg}
     ?Lookup2ndYrAccount{Prop:Msg} = 'Select ' & ?man:SecondYrTradeAccount{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW15.Q &= Queue:Browse
  BRW15.AddSortOrder(,mod:Manufacturer_Key)
  BRW15.AddRange(mod:Manufacturer,Relate:MODELNUM,Relate:MANUFACT)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,mod:Model_Number,1,BRW15)
  BRW15.AddField(mod:Model_Number,BRW15.Q.mod:Model_Number)
  BRW15.AddField(mod:ESN_Length_From,BRW15.Q.mod:ESN_Length_From)
  BRW15.AddField(mod:ESN_Length_To,BRW15.Q.mod:ESN_Length_To)
  BRW15.AddField(mod:Manufacturer,BRW15.Q.mod:Manufacturer)
  IF ?man:UseProductCode{Prop:Checked} = True
    UNHIDE(?man:ProductCodeCompulsory)
  END
  IF ?man:UseProductCode{Prop:Checked} = False
    HIDE(?man:ProductCodeCompulsory)
  END
  IF ?man:Use_MSN{Prop:Checked} = True
    UNHIDE(?man:ApplyMSNFormat)
  END
  IF ?man:Use_MSN{Prop:Checked} = False
    man:ApplyMSNFormat = 0
    HIDE(?man:ApplyMSNFormat)
  END
  IF ?man:ApplyMSNFormat{Prop:Checked} = True
    UNHIDE(?man:MSNFormat:Prompt)
    UNHIDE(?man:MSNFormat)
    UNHIDE(?FormatKey)
  END
  IF ?man:ApplyMSNFormat{Prop:Checked} = False
    HIDE(?man:MSNFormat:Prompt)
    HIDE(?man:MSNFormat)
    HIDE(?FormatKey)
  END
  IF ?man:ValidateDateCode{Prop:Checked} = True
    UNHIDE(?Group3)
  END
  IF ?man:ValidateDateCode{Prop:Checked} = False
    HIDE(?Group3)
  END
  IF ?man:ForceStatus{Prop:Checked} = True
    UNHIDE(?LookupStatus)
    UNHIDE(?man:StatusRequired:Prompt)
    UNHIDE(?man:StatusRequired)
    UNHIDE(?LookupStatus)
  END
  IF ?man:ForceStatus{Prop:Checked} = False
    HIDE(?man:StatusRequired:Prompt)
    HIDE(?man:StatusRequired)
    HIDE(?LookupStatus)
  END
  IF ?man:UseQA{Prop:Checked} = True
    ENABLE(?man:UseElectronicQA)
    ENABLE(?man:QALoanExchange)
    ENABLE(?man:QAAtCompletion)
    ENABLE(?man:QAParts)
    ENABLE(?man:QANetwork)
  END
  IF ?man:UseQA{Prop:Checked} = False
    man:UseElectronicQA = 0
    man:QALoanExchange = 0
    man:QAAtCompletion = 0
    DISABLE(?man:UseElectronicQA)
    DISABLE(?man:QALoanExchange)
    DISABLE(?man:QAAtCompletion)
    DISABLE(?man:QAParts)
    DISABLE(?man:QANetwork)
  END
  IF ?tmp:UseInternetWarranty{Prop:Checked} = True
    UNHIDE(?tmp:WebUserName:Prompt)
    UNHIDE(?tmp:WebUserName)
    UNHIDE(?tmp:WebPassword:Prompt)
    UNHIDE(?tmp:WebPassword)
  END
  IF ?tmp:UseInternetWarranty{Prop:Checked} = False
    HIDE(?tmp:WebUserName:Prompt)
    HIDE(?tmp:WebUserName)
    HIDE(?tmp:WebPassword:Prompt)
    HIDE(?tmp:WebPassword)
  END
  IF ?man:UseBouncerRules{Prop:Checked} = True
    ENABLE(?groupBouncerRules)
  END
  IF ?man:UseBouncerRules{Prop:Checked} = False
    DISABLE(?groupBouncerRules)
  END
  IF ?man:CreateEDIFile{Prop:Checked} = True
    UNHIDE(?man:IncludeCharJobs)
  END
  IF ?man:CreateEDIFile{Prop:Checked} = False
    HIDE(?man:IncludeCharJobs)
  END
  IF ?man:IncludeAdjustment{Prop:Checked} = True
    UNHIDE(?MAN:AdjustPart)
  END
  IF ?man:IncludeAdjustment{Prop:Checked} = False
    HIDE(?MAN:AdjustPart)
  END
  IF ?man:UseInvTextForFaults{Prop:Checked} = True
    ENABLE(?man:AutoRepairType)
    ENABLE(?man:BillingConfirmation)
  END
  IF ?man:UseInvTextForFaults{Prop:Checked} = False
    DISABLE(?man:AutoRepairType)
    DISABLE(?man:BillingConfirmation)
  END
  IF ?man:UseResubmissionLimit{Prop:Checked} = True
    ENABLE(?man:ResubmissionLimit:Prompt)
    ENABLE(?man:ResubmissionLimit)
  END
  IF ?man:UseResubmissionLimit{Prop:Checked} = False
    DISABLE(?man:ResubmissionLimit:Prompt)
    DISABLE(?man:ResubmissionLimit)
  END
  IF ?man:LimitResubmissions{Prop:Checked} = True
    ENABLE(?man:TimesToResubmit:Prompt)
    ENABLE(?man:TimesToResubmit)
  END
  IF ?man:LimitResubmissions{Prop:Checked} = False
    DISABLE(?man:TimesToResubmit:Prompt)
    DISABLE(?man:TimesToResubmit)
  END
  IF ?man:AutoTechnicalReports{Prop:Checked} = True
    ENABLE(?man:AlertEmailAddress:Prompt)
    ENABLE(?man:AlertEmailAddress)
    ENABLE(?Prompt40)
  END
  IF ?man:AutoTechnicalReports{Prop:Checked} = False
    DISABLE(?man:AlertEmailAddress:Prompt)
    DISABLE(?man:AlertEmailAddress)
    DISABLE(?Prompt40)
  END
  BRW15.AskProcedure = 5
  BRW15.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW15.AskProcedure = 0
      CLEAR(BRW15.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANUFACT')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    man:Use_MSN = 'NO'
    man:Batch_Number = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  DoUpdate# = 1
  If man:Manufacturer = ''
      Case Missive('You must insert a Manufacturer.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
      DoUpdate# = 0
      If request = InsertRecord
          Access:MODELNUM.CancelAutoInc()
      End !If request = InsertRecord
  End !If man:Manufacturer = ''
  If DoUpdate# = 1
      glo:Select1   = man:Manufacturer
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSuppliers
      PickJobStatus
      PickSubAccounts
      PickHeadAccounts
      UpdateMODELNUM
    END
    ReturnValue = GlobalResponse
  END
  End !If DoUpdate# = 1
  
      !! ** Bryan Harrison (c)1998 **
  
      glo:G_Select1 = ''
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      if tmp:inactive = true then
          !if man:Notes[1:8] <> 'INACTIVE' then
          !TB13214 - change to using field for inactive  - J 05/02/14
          if man:Inactive <> 1 then
              !not already inactive need to check if we can move
              Tmp:long = 0    !use as a flag
              Access:jobs.clearkey(job:EDI_Key)       !this has Manufacturer as the first element - the rest I ignore
              job:Manufacturer = man:Manufacturer
              set(job:EDI_Key,job:EDI_Key)
              loop
                  if access:jobs.next() then break.
                  if job:Manufacturer <> man:Manufacturer then break.
                  if Job:completed = 'NO'
                      if job:Cancelled = 'NO'
                          !this will do
                          tmp:Long = job:Ref_Number
                          break
                      END
                  END
              END
              if Tmp:long = 0 then
                  !man:Notes = 'INACTIVE '&clip(Man:notes)
                  !TB13214 - change to using field for inactive  - J 05/02/14
                  man:Inactive = 1
              ELSE
                  !a job has been found
                  miss# = missive('There are incomplete jobs for this Manufacturer. These jobs should be completed or cancelled before the Manufacturer can be made inactive|Example is job number:'&clip(tmp:long),'ServiceBase 3g','Mwarn.jpg','OK')
                  cycle
              END
      
              !make faults not available
              Access:Manfault.clearkey(maf:ScreenOrderKey)
              maf:Manufacturer = man:Manufacturer
              set(maf:ScreenOrderKey,maf:ScreenOrderKey)
              loop
                  if access:manfault.next() then break.
                  if maf:Manufacturer <> man:Manufacturer then break.
                  maf:NotAvailable = 1
                  Access:Manfault.update()
              END
              Access:ManFauPa.clearkey(map:ScreenOrderKey)
              map:Manufacturer = man:Manufacturer
              set(map:ScreenOrderKey,map:ScreenOrderKey)
              loop
                  if access:ManFauPa.next() then break.
                  if map:Manufacturer <> man:Manufacturer then break.
                  map:NotAvailable = 1
                  Access:ManFauPa.update()
              END
              !make faults not available
      
              !TB13217 - make repair types unavailable
              ACCESS:Reptydef.clearkey(rtd:ManRepairTypeKey)      !has manufacturer as first element
              rtd:Manufacturer =  man:Manufacturer
              set(rtd:ManRepairTypeKey,rtd:ManRepairTypeKey)
              LOOP
                  IF access:reptydef.next() then break.
                  if rtd:Manufacturer <>  man:Manufacturer then break.
                  rtd:NotAvailable = 1
                  Access:Reptydef.update()
              END !loop through reptydef
      
          END
      ELSE
          !not inactive any more?
          !if man:Notes[1:8] = 'INACTIVE' then
          !TB13214 - change to using field for inactive  - J 05/02/14
          if man:Inactive = 1 then
!              Tmp:Long = len(clip(Man:notes))
!              if Tmp:Long <= 9 then
!                  man:notes = ''
!              ELSE
!                  man:Notes = left(Man:notes[9: tmp:Long])
!              END
              
              man:Inactive = 0
              miss# = missive('Now this manufacturer is no longer inactive you must remember to make fault codes, tac codes and repair types available.',|
                              'ServiceBase 3g','Mexclam.jpg','OK')
              !This will then make the manufacturer available for use once again. at this point Servicebase must open a message informing the user to make fault codes, tac codes and repair types available.
          !ELSE - the inactive tag has been manually removed.
          END
      END
      
      !        SELECT ref_number, completed, Cancelled FROM "JOBS" where cancelled = 'YES'
      !        completed NO cancelled YES
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ProceduresImportExchangeSellingPrice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresImportExchangeSellingPrice, Accepted)
      BHRunProg('ImpExchValue.exe','!' & clip(man:Manufacturer))
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresImportExchangeSellingPrice, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020378'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020378'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020378'&'0')
      ***
    OF ?man:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          Postcode_Routine (man:postcode,man:Address_Line1,man:Address_Line2,man:Address_Line3)
          Select(?man:Address_Line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      Case Missive('Are you sure you want to clear the address?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              man:address_line1    = ''
              man:address_line2    = ''
              man:address_line3    = ''
              man:postcode        = ''
              Select(?man:postcode)
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?man:Supplier
      IF man:Supplier OR ?man:Supplier{Prop:Req}
        sup:Company_Name = man:Supplier
        !Save Lookup Field Incase Of error
        look:man:Supplier        = man:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            man:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            man:Supplier = look:man:Supplier
            SELECT(?man:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = man:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          man:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?man:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?man:Supplier)
    OF ?man:UseProductCode
      IF ?man:UseProductCode{Prop:Checked} = True
        UNHIDE(?man:ProductCodeCompulsory)
      END
      IF ?man:UseProductCode{Prop:Checked} = False
        HIDE(?man:ProductCodeCompulsory)
      END
      ThisWindow.Reset
    OF ?man:Use_MSN
      IF ?man:Use_MSN{Prop:Checked} = True
        UNHIDE(?man:ApplyMSNFormat)
      END
      IF ?man:Use_MSN{Prop:Checked} = False
        man:ApplyMSNFormat = 0
        HIDE(?man:ApplyMSNFormat)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Use_MSN, Accepted)
      Post(Event:Accepted,?man:ApplyMSNFormat)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Use_MSN, Accepted)
    OF ?man:ApplyMSNFormat
      IF ?man:ApplyMSNFormat{Prop:Checked} = True
        UNHIDE(?man:MSNFormat:Prompt)
        UNHIDE(?man:MSNFormat)
        UNHIDE(?FormatKey)
      END
      IF ?man:ApplyMSNFormat{Prop:Checked} = False
        HIDE(?man:MSNFormat:Prompt)
        HIDE(?man:MSNFormat)
        HIDE(?FormatKey)
      END
      ThisWindow.Reset
    OF ?man:ValidateDateCode
      IF ?man:ValidateDateCode{Prop:Checked} = True
        UNHIDE(?Group3)
      END
      IF ?man:ValidateDateCode{Prop:Checked} = False
        HIDE(?Group3)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:ValidateDateCode, Accepted)
      If man:ValidateDateCode
          If man:Manufacturer = 'ALCATEL' Or |
              man:Manufacturer = 'SAMSUNG' Or |
              man:Manufacturer = 'MOTOROLA' Or |
              man:Manufacturer = 'PHILIPS' Or |
              man:Manufacturer = 'BOSCH'  Or |
              man:Manufacturer = 'SIEMENS'
              ?DateCodeValidation{prop:Hide} = 0
          Else !man:Manufacturer = 'SIEMENS'
              ?DateCodeValidation{prop:Hide} = 1
          End !man:Manufacturer = 'SIEMENS'
      
      Else
          ?DateCodeValidation{prop:Hide} = 1
      End !man:ValidateDateCode
      !ThisMakeover.Refresh()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:ValidateDateCode, Accepted)
    OF ?man:ForceStatus
      IF ?man:ForceStatus{Prop:Checked} = True
        UNHIDE(?LookupStatus)
        UNHIDE(?man:StatusRequired:Prompt)
        UNHIDE(?man:StatusRequired)
        UNHIDE(?LookupStatus)
      END
      IF ?man:ForceStatus{Prop:Checked} = False
        HIDE(?man:StatusRequired:Prompt)
        HIDE(?man:StatusRequired)
        HIDE(?LookupStatus)
      END
      ThisWindow.Reset
    OF ?man:StatusRequired
      IF man:StatusRequired OR ?man:StatusRequired{Prop:Req}
        sts:Status = man:StatusRequired
        sts:Job = tmp:YES
        !Save Lookup Field Incase Of error
        look:man:StatusRequired        = man:StatusRequired
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            man:StatusRequired = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            man:StatusRequired = look:man:StatusRequired
            SELECT(?man:StatusRequired)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = man:StatusRequired
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          man:StatusRequired = sts:Status
          Select(?+1)
      ELSE
          Select(?man:StatusRequired)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?man:StatusRequired)
    OF ?DateCodeValidation
      ThisWindow.Update
      BrowseDateCodes(man:Manufacturer)
      ThisWindow.Reset
    OF ?FormatKey
      ThisWindow.Update
      FormatKey
      ThisWindow.Reset
    OF ?InWarrantyMarkups
      ThisWindow.Update
      BrowseInWarrantyMarkup(man:RecordNumber)
      ThisWindow.Reset
    OF ?man:UseQA
      IF ?man:UseQA{Prop:Checked} = True
        ENABLE(?man:UseElectronicQA)
        ENABLE(?man:QALoanExchange)
        ENABLE(?man:QAAtCompletion)
        ENABLE(?man:QAParts)
        ENABLE(?man:QANetwork)
      END
      IF ?man:UseQA{Prop:Checked} = False
        man:UseElectronicQA = 0
        man:QALoanExchange = 0
        man:QAAtCompletion = 0
        DISABLE(?man:UseElectronicQA)
        DISABLE(?man:QALoanExchange)
        DISABLE(?man:QAAtCompletion)
        DISABLE(?man:QAParts)
        DISABLE(?man:QANetwork)
      END
      ThisWindow.Reset
    OF ?tmp:UseInternetWarranty
      IF ?tmp:UseInternetWarranty{Prop:Checked} = True
        UNHIDE(?tmp:WebUserName:Prompt)
        UNHIDE(?tmp:WebUserName)
        UNHIDE(?tmp:WebPassword:Prompt)
        UNHIDE(?tmp:WebPassword)
      END
      IF ?tmp:UseInternetWarranty{Prop:Checked} = False
        HIDE(?tmp:WebUserName:Prompt)
        HIDE(?tmp:WebUserName)
        HIDE(?tmp:WebPassword:Prompt)
        HIDE(?tmp:WebPassword)
      END
      ThisWindow.Reset
    OF ?man:UseBouncerRules
      IF ?man:UseBouncerRules{Prop:Checked} = True
        ENABLE(?groupBouncerRules)
      END
      IF ?man:UseBouncerRules{Prop:Checked} = False
        DISABLE(?groupBouncerRules)
      END
      ThisWindow.Reset
    OF ?EDI_Path_lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Path_lookup, Accepted)
      filedialog ('Choose Directory',man:EDI_Path,'All Directories|*.*', |
                  file:save+file:keepdir + file:noerror + file:longname + file:directory)
      man:EDI_Path = upper(man:EDI_Path)
      display(?man:EDI_Path)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Path_lookup, Accepted)
    OF ?man:EDIFileType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:EDIFileType, Accepted)
      !added by Paul - 14/10/2010 - log no 11637
      If clip(man:EDIFileType) = 'ZTE' then
          ?man:EDItransportFee{prop:Hide} = False
          ?man:EDItransportFee:Prompt{prop:hide} = False
      Else
          ?man:EDItransportFee{prop:Hide} = True
          ?man:EDItransportFee:Prompt{prop:hide} = True
      End
      
      If (CLIP(man:EDIFileType) = 'SAGEM')
          ! #11673 New field (Bryan: 03/02/2011)
          ?man:SagemVersionNumber{prop:Hide} = 0
          ?man:SagemVersionNumber:Prompt{prop:Hide} = 0
      ELSE
          ?man:SagemVersionNumber{prop:Hide} = 1
          ?man:SagemVersionNumber:Prompt{prop:Hide} = 1
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:EDIFileType, Accepted)
    OF ?man:CreateEDIFile
      IF ?man:CreateEDIFile{Prop:Checked} = True
        UNHIDE(?man:IncludeCharJobs)
      END
      IF ?man:CreateEDIFile{Prop:Checked} = False
        HIDE(?man:IncludeCharJobs)
      END
      ThisWindow.Reset
    OF ?man:Trade_Account
      IF man:Trade_Account OR ?man:Trade_Account{Prop:Req}
        sub:Account_Number = man:Trade_Account
        !Save Lookup Field Incase Of error
        look:man:Trade_Account        = man:Trade_Account
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            man:Trade_Account = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            man:Trade_Account = look:man:Trade_Account
            SELECT(?man:Trade_Account)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = man:Trade_Account
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          man:Trade_Account = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?man:Trade_Account)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?man:Trade_Account)
    OF ?man:SecondYrTradeAccount
      IF man:SecondYrTradeAccount OR ?man:SecondYrTradeAccount{Prop:Req}
        tra:Account_Number = man:SecondYrTradeAccount
        !Save Lookup Field Incase Of error
        look:man:SecondYrTradeAccount        = man:SecondYrTradeAccount
        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            man:SecondYrTradeAccount = tra:Account_Number
          ELSE
            !Restore Lookup On Error
            man:SecondYrTradeAccount = look:man:SecondYrTradeAccount
            SELECT(?man:SecondYrTradeAccount)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup2ndYrAccount
      ThisWindow.Update
      tra:Account_Number = man:SecondYrTradeAccount
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          man:SecondYrTradeAccount = tra:Account_Number
          Select(?+1)
      ELSE
          Select(?man:SecondYrTradeAccount)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?man:SecondYrTradeAccount)
    OF ?EDI_Defaults
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Defaults, Accepted)
      Case man:manufacturer
          Of 'MOTOROLA'
              Edi_Defaults
          Of 'SIEMENS'
              Eps_Defaults
          Of 'ERICSSON'
              Edi_Defaults_Ericsson
      End!Case man:manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Defaults, Accepted)
    OF ?man:IncludeAdjustment
      IF ?man:IncludeAdjustment{Prop:Checked} = True
        UNHIDE(?MAN:AdjustPart)
      END
      IF ?man:IncludeAdjustment{Prop:Checked} = False
        HIDE(?MAN:AdjustPart)
      END
      ThisWindow.Reset
    OF ?man:UseInvTextForFaults
      IF ?man:UseInvTextForFaults{Prop:Checked} = True
        ENABLE(?man:AutoRepairType)
        ENABLE(?man:BillingConfirmation)
      END
      IF ?man:UseInvTextForFaults{Prop:Checked} = False
        DISABLE(?man:AutoRepairType)
        DISABLE(?man:BillingConfirmation)
      END
      ThisWindow.Reset
    OF ?man:UseResubmissionLimit
      IF ?man:UseResubmissionLimit{Prop:Checked} = True
        ENABLE(?man:ResubmissionLimit:Prompt)
        ENABLE(?man:ResubmissionLimit)
      END
      IF ?man:UseResubmissionLimit{Prop:Checked} = False
        DISABLE(?man:ResubmissionLimit:Prompt)
        DISABLE(?man:ResubmissionLimit)
      END
      ThisWindow.Reset
    OF ?man:LimitResubmissions
      IF ?man:LimitResubmissions{Prop:Checked} = True
        ENABLE(?man:TimesToResubmit:Prompt)
        ENABLE(?man:TimesToResubmit)
      END
      IF ?man:LimitResubmissions{Prop:Checked} = False
        DISABLE(?man:TimesToResubmit:Prompt)
        DISABLE(?man:TimesToResubmit)
      END
      ThisWindow.Reset
    OF ?man:AutoTechnicalReports
      IF ?man:AutoTechnicalReports{Prop:Checked} = True
        ENABLE(?man:AlertEmailAddress:Prompt)
        ENABLE(?man:AlertEmailAddress)
        ENABLE(?Prompt40)
      END
      IF ?man:AutoTechnicalReports{Prop:Checked} = False
        DISABLE(?man:AlertEmailAddress:Prompt)
        DISABLE(?man:AlertEmailAddress)
        DISABLE(?Prompt40)
      END
      ThisWindow.Reset
    OF ?JobFaultCoding
      ThisWindow.Update
      Browse_Manufacturer_Fault_Coding
      ThisWindow.Reset
    OF ?PartFaultCoding
      ThisWindow.Update
      Browse_Manufacturer_Parts_Fault_Coding
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button:LabTypes
      ThisWindow.Update
      BrowsePartPrefixLabTypes
      ThisWindow.Reset
    OF ?Button:RejectionCodes
      ThisWindow.Update
      BrowseManufacturerRejectionCodes(man:RecordNumber,man:Manufacturer)
      ThisWindow.Reset
    OF ?Button:ImportCCTRefNos
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ImportCCTRefNos, Accepted)
          If SecurityCheck('IMPORT CCT REF NO')
              Beep(Beep:SystemHand);  Yield()
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !     Else ! If SecurityAccess('IMPORT CCT REF NO')
      
          tmp:SavePath = Path()
          tmp:CCTImportFile = ''
          If FileDialog('Choose File',tmp:CCTImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
              !Found
              SetPath(tmp:SavePath)
              Open(CCTImportFile)
              If Error()
                  Stop(Error())
              Else !If Error()
                  Count# = 0
                  Set(CCTImportFile,0)
                  Loop
                      Next(CCTImportFile)
                      If Error()
                          Break
                      End ! If Error()
                      Count# += 1
                  End ! Loop
      
                  Do Prog:ProgressSetup
                  Prog:TotalRecords = Count#
                  Prog:ShowPercentage = 1 !Show Percentage Figure
      
                  Set(CCTImportFile,0)
      
                  Accept
                      Case Event()
                          Of Event:Timer
                              Loop 25 Times
                                  !Inside Loop
                                  Next(CCTImportFile)
                                  If Error()
                                      Prog:Exit = 1
                                      Break
                                  End ! If Access:ORDERS.Next()
      
                                  Prog:RecordCount += 1
      
                                  ?Prog:UserString{Prop:Text} = 'Records Read: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                                  Do Prog:UpdateScreen
      
                                  Access:MODELNUM.Clearkey(mod:SalesModelKey)
                                  mod:Manufacturer = man:Manufacturer
                                  mod:SalesModel = cctimp:SalesModel
                                  Set(mod:SalesModelKey,mod:SalesModelKey)
                                  Loop
                                      If Access:MODELNUM.Next()
                                          Break
                                      End ! If Access:MODELNUM.Next()
                                      If mod:Manufacturer <> man:Manufacturer
                                          Break
                                      End ! If mod:Manufacturer <> man:Manufacturer
                                      If mod:SalesModel <> cctimp:SalesModel
                                          Break
                                      End ! If mod:SalesModel <> cctimp:SalesModel
                                      Access:MODELCCT.Clearkey(mcc:CCTRefKey)
                                      mcc:ModelNumber = mod:Model_Number
                                      mcc:CCTReferenceNumber = cctimp:CCTReference
                                      If Access:MODELCCT.TryFetch(mcc:CCTRefKey)
                                          If Access:MODELCCT.PrimeRecord() = Level:Benign
                                              mcc:ModelNumber = mod:Model_Number
                                              mcc:CCTReferenceNumber = cctimp:CCTReference
                                              If Access:MODELCCT.TryInsert() = Level:Benign
      
                                              Else ! If Access:MODELCCT.TryInsert() = Level:Benign
                                                  Access:MODELCCT.CancelAutoInc()
                                              End ! If Access:MODELCCT.TryInsert() = Level:Benign
                                          End ! If Access:MODELCCT.PrimeRecord() = Level:Benign
                                      End ! If Access:MODELCCT.TryFetch(mcc:CCTRefKey) = Level:Benign
      
                                  End ! Loop (MODELNUM)
      
                              End ! Loop 25 Times
                          Of Event:CloseWindow
                              Prog:Exit = 1
                              Prog:Cancelled = 1
                              Break
                          Of Event:Accepted
                              If Field() = ?Prog:Cancel
                                  Beep(Beep:SystemQuestion)  ;  Yield()
                                  Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                                 icon:Question,'&Yes|&No',2,2)
                                      Of 1 ! &Yes Button
                                          Prog:Exit = 1
                                          Prog:Cancelled = 1
                                          Break
                                      Of 2 ! &No Button
                                  End!Case Message
                              End ! If Field() = ?ProgressCancel
                      End ! Case Event()
                      If Prog:Exit
                          Break
                      End ! If Prog:Exit
                  End ! Accept
                  Do Prog:ProgressFinished
      
                  Beep(Beep:SystemAsterisk);  Yield()
                  Case Missive('Import Complete.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Close(CCTImportFile)
              End ! If Error()
      
          Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
              !Error
              SetPath(tmp:SavePath)
          End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ImportCCTRefNos, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Save Use of Internet Warranty to INI file
   case man:manufacturer
      of 'NOKIA' orof 'MOTOROLA'
          putiniext('Global', man:Manufacturer, tmp:useInternetWarranty, path() & '\webcheck.ini')
          if man:manufacturer = 'NOKIA'
              putiniext('WARVALIDATIONFILE', man:Manufacturer, tmp:UseLocalFileValidation, path() & '\SB2KDEF.INI')
          end
          putiniext(man:Manufacturer, 'WebUserName', tmp:WebUserName, path()&'\webcheck.ini')
          putiniext(man:Manufacturer, 'WebPassword', tmp:WebPassword, path()&'\webcheck.ini')
  
          if tmp:useInternetWarranty = 'YES'
              putiniext('Global', 'WEBCHECK', 'YES', path()&'\webcheck.ini')
          end
   END !Case
  If man:manufacturer <> tmp:manufacturer And tmp:Manufacturer <> ''
      Case Missive('You have changed the name of this manufacturer.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              setcursor(cursor:wait)
              save_sto_id = access:stock.savefile()
              access:stock.clearkey(sto:Manufacturer_Key)
              sto:manufacturer = tmp:manufacturer
              set(sto:Manufacturer_Key,sto:Manufacturer_Key)
              loop
                  if access:stock.next()
                     break
                  end !if
                  if sto:manufacturer <> tmp:manufacturer      |
                      then break.  ! end if
                  pos = Position(sto:Manufacturer_Key)
                  sto:manufacturer = man:manufacturer
                  access:stock.tryupdate()
                  Reset(sto:Manufacturer_Key,pos)
  
                  save_stm_id = access:stomodel.savefile()
                  access:stomodel.clearkey(stm:mode_number_only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  set(stm:mode_number_only_Key,stm:mode_number_only_Key)
                  loop
                      if access:stomodel.next()
                         break
                      end !if
                      if stm:Ref_Number   <> sto:Ref_Number      |
                          then break.  ! end if
                      stm:manufacturer = man:manufacturer
                      access:stomodel.tryupdate()
                  end !loop
                  access:stomodel.restorefile(save_stm_id)
              end !loop
              access:stock.restorefile(save_sto_id)
              setcursor()
          Of 1 ! No Button
              man:manufacturer = tmp:manufacturer
              Cycle
      End ! Case Missive
      Display()
  End!If sto:manufacturer <> tmp:manufacturer
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case man:manufacturer
          Of 'MOTOROLA'
              Unhide(?edi_defaults)
              If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
                  ?man:SiemensNewEDI{prop:Hide} = 0
              End !If FullAccess() = Level:Benign
              Unhide(?InternetWarrantyGroup)
              tmp:UseInternetWarranty = getini('GLOBAL',man:Manufacturer,'NO',path()&'\WEBCHECK.INI')
              tmp:WebUserName = getini(man:Manufacturer,'WebUserName','',path()&'\WEBCHECK.INI')
              tmp:WebPassword = getini(man:Manufacturer,'WebPassword','',path()&'\WEBCHECK.INI')
              post(Event:Accepted, ?tmp:UseInternetWarranty)
              hide(?tmp:UseLocalFileValidation)
          of 'ERICSSON'
              Unhide(?edi_defaults)
              If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
                  ?man:SiemensNewEDI{prop:Hide} = 0
              End !If FullAccess() = Level:Benign
          Of 'SAMSUNG'
              check_access('MANUFACT FAULT CODES - CHANGE',x")
              if x" = true
                  Unhide(?man:samsungcount)
                  Unhide(?man:samsungcount:prompt)
              end
          Of 'NOKIA'
              Unhide(?InternetWarrantyGroup)
              tmp:UseInternetWarranty = getini('GLOBAL',man:Manufacturer,'NO',path()&'\WEBCHECK.INI')
              tmp:WebUserName = getini(man:Manufacturer,'WebUserName','',path()&'\WEBCHECK.INI')
              tmp:WebPassword = getini(man:Manufacturer,'WebPassword','',path()&'\WEBCHECK.INI')
              tmp:UseLocalFileValidation = getini('WARVALIDATIONFILE', man:Manufacturer, '0', path() & '\SB2KDEF.INI')
              post(Event:Accepted, ?tmp:UseInternetWarranty)
              If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
                  ?man:SiemensNewEDI{prop:Hide} = 0
                  ?man:SiemensNewEDI{prop:Text} = 'Use XML Claim File'
              End !If FullAccess() = Level:Benign
          Of 'SIEMENS'
              Unhide(?man:SiemensNewEDI)
      End!Case man:manufacturer
      
      !Save Name
      tmp:manufacturer = man:manufacturer
      Post(Event:Accepted,?man:Use_MSN)
      Post(Event:Accepted,?man:ValidateDateCode)
      !ThisMakeOver.Refresh()
      Display()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
RemovePriceStructures       Procedure(String    func:ModelNumber,String     func:RepairType)
    Code

    Save_trc_ID = Access:TRACHRGE.SaveFile()
    Access:TRACHRGE.ClearKey(trc:Model_Repair_Key)
    trc:Model_Number = func:ModelNumber
    trc:Repair_Type  = func:RepairType
    Set(trc:Model_Repair_Key,trc:Model_Repair_Key)
    Loop
        If Access:TRACHRGE.NEXT()
           Break
        End !If
        If trc:Model_Number <> func:ModelNumber |
        Or trc:Repair_Type  <> func:RepairType      |
            Then Break.  ! End If
        Delete(TRACHRGE)
    End !Loop
    Access:TRACHRGE.RestoreFile(Save_trc_ID)

    Save_suc_ID = Access:SUBCHRGE.SaveFile()
    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Key)
    suc:Model_Number = func:ModelNumber
    suc:Repair_Type  = func:RepairType
    Set(suc:Model_Repair_Key,suc:Model_Repair_Key)
    Loop
        If Access:SUBCHRGE.NEXT()
           Break
        End !If
        If suc:Model_Number <> func:ModelNumber      |
        Or suc:Repair_Type  <> func:RepairType      |
            Then Break.  ! End If
        Delete(SUBCHRGE)
    End !Loop
    Access:SUBCHRGE.RestoreFile(Save_suc_ID)

    Save_sta_ID = Access:STDCHRGE.SaveFile()
    Access:STDCHRGE.ClearKey(sta:Repair_Type_Key)
    sta:Model_Number = func:ModelNumber
    sta:Repair_Type  = func:RepairType
    Set(sta:Repair_Type_Key,sta:Repair_Type_Key)
    Loop
        If Access:STDCHRGE.NEXT()
           Break
        End !If
        If sta:Model_Number <> func:ModelNumber      |
        Or sta:Repair_Type  <> func:RepairType      |
            Then Break.  ! End If
        Delete(STDCHRGE)
    End !Loop
    Access:STDCHRGE.RestoreFile(Save_sta_ID)
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?man:EDIFileType{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?man:EDIFileType{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW15.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
UpdateManufacturerRejectionCode PROCEDURE             !Generated from procedure template - Form

ActionMessage        CSTRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Codes'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('CodeNumber'),AT(248,194),USE(?mar:CodeNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s60),AT(308,194,124,10),USE(mar:CodeNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('CodeNumber'),TIP('CodeNumber'),REQ,UPR
                       PROMPT('Description'),AT(248,212),USE(?mar:Description:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s255),AT(308,212,124,10),USE(mar:Description),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Description'),TIP('Description'),REQ,UPR
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020717'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateManufacturerRejectionCode')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:MANREJR)
  Relate:MANREJR.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANREJR
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateManufacturerRejectionCode')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANREJR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateManufacturerRejectionCode')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020717'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020717'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020717'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BrowseManufacturerRejectionCodes PROCEDURE (f:MANRecordNumber,f:Manufacturer) !Generated from procedure template - Browse

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
tmp:MANRecordNumber  LONG
tmp:SavePath         STRING(255)
BRW9::View:Browse    VIEW(MANREJR)
                       PROJECT(mar:CodeNumber)
                       PROJECT(mar:Description)
                       PROJECT(mar:RecordNumber)
                       PROJECT(mar:MANRecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mar:CodeNumber         LIKE(mar:CodeNumber)           !List box control field - type derived from field
mar:Description        LIKE(mar:Description)          !List box control field - type derived from field
mar:RecordNumber       LIKE(mar:RecordNumber)         !Primary key field - type derived from field
mar:MANRecordNumber    LIKE(mar:MANRecordNumber)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Manufacturer:'),AT(168,88),USE(?Prompt:Manufacturer),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       LIST,AT(168,102,252,222),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~CodeNumber~@s60@1020L(2)|M~Description~@s255@'),FROM(Queue:Browse)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Query / Rejection Codes File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(168,332),USE(?Button:ImportCodes),TRN,FLAT,ICON('impcodp.jpg')
                       BUTTON,AT(444,242),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                       BUTTON,AT(444,272),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                       BUTTON,AT(444,302),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('pcancel.ico'),HIDE
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
tmp:ImportFile      String(255),Static
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
CodeNumber              String(60)
Description             String(255)
                    End
                End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    If glo:WebJob
        Clarionet:OpenPushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Open(Prog:ProgressWindow)
        ?Prog:Cancel{prop:Hide} = 0
    End ! If glo:WebJob
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    If glo:WebJob
        Clarionet:UpdatePushWindow(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    If glo:WebJob
        Clarionet:ClosePushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Close(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020716'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseManufacturerRejectionCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANREJR.Open
  SELF.FilesOpened = True
  tmp:MANRecordNumber = f:MANRecordNumber
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:MANREJR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ?Prompt:Manufacturer{prop:Text} = 'Manufacturer: ' & Clip(f:Manufacturer)
  ! Save Window Name
   AddToLog('Window','Open','BrowseManufacturerRejectionCodes')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,mar:CodeKey)
  BRW9.AddRange(mar:MANRecordNumber,tmp:MANRecordNumber)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,mar:CodeNumber,1,BRW9)
  BRW9.AddField(mar:CodeNumber,BRW9.Q.mar:CodeNumber)
  BRW9.AddField(mar:Description,BRW9.Q.mar:Description)
  BRW9.AddField(mar:RecordNumber,BRW9.Q.mar:RecordNumber)
  BRW9.AddField(mar:MANRecordNumber,BRW9.Q.mar:MANRecordNumber)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANREJR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseManufacturerRejectionCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateManufacturerRejectionCode
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020716'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020716'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020716'&'0')
      ***
    OF ?Button:ImportCodes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ImportCodes, Accepted)
      tmp:SavePath = Path()
      If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Found
          SetPath(tmp:SavePath)
      Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Error
          SetPath(tmp:SavePath)
          Cycle
      End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
      
      Count# = 0
      Open(ImportFile)
      Set(ImportFile,0)
      Loop
          Next(ImportFile)
          If Error()
              Break
          End ! If Error()
          Count# += 1
      End ! Loop
      
      Do Prog:ProgressSetup
      Prog:TotalRecords = Count#
      Prog:ShowPercentage = 1 !Show Percentage Figure
      Set(ImportFile,0)
      Accept
          Case Event()
          Of Event:Timer
              Loop 25 Times
                  !Inside Loop
                  Next(ImportFile)
                  If Error()
                      Prog:Exit = 1
                      Break
                  End ! If Access:IMPORTFILE.Next()
                  Do Prog:UpdateScreen
                  Prog:RecordCount += 1
      
                  Access:MANREJR.ClearKey(mar:CodeKey)
                  mar:MANRecordNumber = tmp:MANRecordNumber
                  mar:CodeNumber = Upper(impfil:CodeNumber)
                  If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign
                      !Found
                  Else ! If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign
                      !Error
                      If Access:MANREJR.PrimeRecord() = Level:Benign
                          mar:MANRecordNumber = tmp:MANRecordNumber
                          mar:CodeNumber = Upper(impfil:CodeNumber)
                          mar:Description = Upper(impfil:Description)
                          If Access:MANREJR.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:MANREJR.TryInsert() = Level:Benign
                              Access:MANREJR.CancelAutoInc()
                          End ! If Access:MANREJR.TryInsert() = Level:Benign
                      End ! If Access.MANREJR.PrimeRecord() = Level:Benign
                  End ! If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign
      
                  ?Prog:UserString{Prop:Text} = 'Records Imported: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
              End ! Loop 25 Times
          Of Event:CloseWindow
              Prog:Exit = 1
              Prog:Cancelled = 1
              Break
          Of Event:Accepted
              If Field() = ?Prog:Cancel
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                  Of 1 ! Yes
                      Prog:Exit = 1
                      Prog:Cancelled = 1
                      Break
                  Of 2 ! No
                  End ! Case Message
              End! If FIeld()
          End ! Case Event()
          If Prog:Exit
              Break
          End ! If Prog:Exit
      End ! Accept
      Do Prog:ProgressFinished
      Close(ImportFile)
      
      Beep(Beep:SystemAsterisk)  ;  Yield()
      Case Missive('Import Completed.','ServiceBase',|
                     'midea.jpg','/&OK')
          Of 1 ! &OK Button
      End!Case Message
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ImportCodes, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

AutomaticDateCode PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
YearCodeQueue        QUEUE,PRE(yque)
Year                 STRING(4)
Code                 STRING(2)
                     END
MonthCodeQueue       QUEUE,PRE(mque)
Month                STRING(2)
Code                 STRING(2)
                     END
DateCodeQueue        QUEUE,PRE(dque)
Day                  STRING(2)
Code                 STRING(2)
                     END
window               WINDOW('Automatic Date Code Generator'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Automatic Date Code Generator'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,148,310),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Year Codes'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(68,70,68,290),USE(?List1),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),ALRT(InsertKey),ALRT(DeleteKey),FORMAT('25L(2)|M~Year~@n04@12L(2)|M~Code~@s3@'),FROM(YearCodeQueue)
                           BUTTON,AT(140,306),USE(?Button1),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(140,332),USE(?Button2),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       SHEET,AT(216,54,148,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Month Codes'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(220,70,68,290),USE(?List2),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),ALRT(InsertKey),ALRT(DeleteKey),FORMAT('27L(2)|M~Month~@n02@8L(2)|M~Code~@s2@'),FROM(MonthCodeQueue)
                           BUTTON,AT(292,306),USE(?Button3),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(292,332),USE(?Button4),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       SHEET,AT(368,54,148,310),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('Day Codes'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(372,70,68,290),USE(?List3),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),ALRT(InsertKey),ALRT(DeleteKey),FORMAT('21L(2)|M~Day~@n02@8L(2)|M~Code~@s2@'),FROM(DateCodeQueue)
                           BUTTON,AT(444,306),USE(?Button5),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(444,332),USE(?Button6),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       PANEL,AT(520,54,96,310),USE(?Panel5),FILL(09A6A7CH)
                       BUTTON,AT(536,110),USE(?AutoGenerate),TRN,FLAT,LEFT,ICON('gencodep.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
InsertYear      Routine
Data
ytmp:Year    String(4)
ytmp:Code    String(2)

Code
    InsertYear(ytmp:Year,ytmp:Code)
    If ytmp:Year <> 0
        yque:Year = ytmp:Year
        Get(YearCodeQueue,yque:Year)
        If Error()
            yque:Year   = ytmp:Year
            yque:Code   = ytmp:Code
            Add(YearCodeQueue)
        End !If Error()
    End !Year" <> ''
InsertMonth      Routine
Data
mtmp:Month    String(2)
mtmp:Code     String(2)

Code
    InsertMonth(mtmp:Month,mtmp:Code)
    If mtmp:Month <> 0
        mque:Month = mtmp:Month
        Get(MonthCodeQueue,mque:Month)
        If Error()
            mque:Month  = mtmp:Month
            mque:Code   = mtmp:Code
            Add(MonthCodeQueue)
        End !If Error()
    End !Year" <> ''
InsertDay      Routine
Data
dtmp:Day     String(2)
dtmp:Code    String(2)

Code
    InsertDay(dtmp:Day,dtmp:Code)
    If dtmp:Day <> 0
        dque:Day = dtmp:Day
        Get(DateCodeQueue,dque:Day)
        If Error()
            dque:Day    = dtmp:Day
            dque:Code   = dtmp:Code
            Add(DateCodeQueue)
        End !If Error()
    End !Year" <> ''
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020352'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AutomaticDateCode')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUDATE.Open
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','AutomaticDateCode')
  ?List1{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUDATE.Close
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AutomaticDateCode')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020352'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020352'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020352'&'0')
      ***
    OF ?Button1
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
      Do InsertYear
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button1, Accepted)
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      Case Missive('Are you sure you want to delete this record?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
              Get(YearCodeQueue,Choice(?List1))
              Delete(YearCodeQueue)
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      Do InsertMonth
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      Case Missive('Are you sure you want to delete this record?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Get(MonthCodeQueue,Choice(?List2))
              Delete(MonthCodeQueue)
          Of 1 ! No Button
      End ! Case Missive
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?Button5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      Do InsertDay
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?Button6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      Case Missive('Are you sure you want to delete this record?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Get(DateCodeQueue,Choice(?List3))
              Delete(DateCodeQueue)
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?AutoGenerate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoGenerate, Accepted)
      Prog.ProgressSetup(Records(YearCodeQueue))
      Loop x# = 1 To Records(YearCodeQueue)
          Get(YearCodeQueue,x#)
      
          If Prog.InsideLoop()
              Break
          End !If Prog.InsideLoop()
      
          Loop y# = 1 To Records(MonthCodeQueue)
              Get(MonthCodeQueue,y#)
      
              If man:Manufacturer = 'SIEMENS' Or man:Manufacturer = 'SAMSUNG' Or man:Manufacturer = 'BOSCH' Or |
                  man:Manufacturer = 'MOTOROLA'
                  If Access:MANUDATE.PrimeRecord() = Level:Benign
                      mad:Manufacturer     = man:Manufacturer
                      mad:DateCode         = Clip(yque:Code) & Clip(mque:Code)
                      mad:TheYear          = yque:Year
                      mad:TheMonth         = mque:Month
                      If Access:MANUDATE.TryInsert() = Level:Benign
                          !Insert Successful
      
                      Else !If Access:MANUDATE.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:MANUDATE.CancelAutoInc()
                      End !If Access:MANUDATE.TryInsert() = Level:Benign
                  End !If Access:MANUDATE.PrimeRecord() = Level:Benign
                  Cycle
              End !If man:Manufacturer = 'SIEMENS'
      
      
              Loop z# = 1 To Records(DateCodeQueue)
                  Get(DateCodeQueue,z#)
      
                  Case man:Manufacturer
                      Of 'ALCATEL'
                          If Access:MANUDATE.PrimeRecord() = Level:Benign
                              mad:Manufacturer     = man:Manufacturer
                              mad:DateCode         = Clip(dque:Code) & Clip(mque:Code) & Clip(yque:Code)
                              mad:TheYear          = yque:Year
                              mad:TheMonth         = mque:Month
                              mad:AlcatelDay       = dque:Day
                              If Access:MANUDATE.TryInsert() = Level:Benign
                                  !Insert Successful
      
                              Else !If Access:MANUDATE.TryInsert() = Level:Benign
                                  !Insert Failed
                                  Access:MANUDATE.CancelAutoInc()
                              End !If Access:MANUDATE.TryInsert() = Level:Benign
                          End !If Access:MANUDATE.PrimeRecord() = Level:Benign
                  End !Case man:Manufacturer
              End !Loop z# = 1 To Records(DateCodeQueue)
          End !Loop y# = 1 To Records(MonthCodeQueue)
      End !x# = 1 To Records(YearCodeQueue)
      
      Prog.ProgressFinish()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoGenerate, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, AlertKey)
      Case KeyCode()
          Of InsertKey
              Post(Event:Accepted,?Button1)
          Of DeleteKey
              Post(Event:Accepted,?Button2)
      End !KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, AlertKey)
    END
  OF ?List2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List2, AlertKey)
      Case KeyCode()
          Of InsertKey
              Post(Event:Accepted,?Button3)
          Of DeleteKey
              Post(Event:Accepted,?Button4)
      End !KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List2, AlertKey)
    END
  OF ?List3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3, AlertKey)
      Case KeyCode()
          Of InsertKey
              Post(Event:Accepted,?Button5)
          Of DeleteKey
              Post(Event:Accepted,?Button6)
      End !KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
