                  MEMBER('sbh01app.clw')

CheckOpen         PROCEDURE(FILE File,<BYTE OverrideCreate>,<BYTE OverrideOpenMode>)
OpenMode          BYTE
  CODE
  IF OMITTED(3)
    OpenMode = 42h
  ELSE
    OpenMode = OverrideOpenMode
  END
  OPEN(File,OpenMode)
  IF ERRORCODE() = IsLockedErr
    SEND(File,'Recover=10')
    OPEN(File,OpenMode)
  END
  CASE ERRORCODE()                                    ! and check for errors
  OF NoError                                          !Return if no error
  OROF IsOpenErr                                      ! or if already open.
    DO ProcedureReturn
  OF NoFileErr                                        !If file was not found
    IF OMITTED(2)
    ELSIF OverrideCreate = TRUE
      DO CreateFile
    ELSE
      IF StandardWarning(Warn:CreateError,NAME(File)).
    END
  OF InvalidFileErr                                   !Invalid Record Declaration
    IF StandardWarning(Warn:InvalidFile,NAME(File)).
  OF BadKeyErr                                        !Key Files must be rebuilt
    IF StandardWarning(Warn:InvalidKey,NAME(File))
      BUILD(File)                              !Rebuild the key files
    END
    IF ERRORCODE()
      IF StandardWarning(Warn:RebuildError,NAME(File)).
    ELSE
      IF OMITTED(3)
        OPEN(File,42h)                                !Attempt to open the file
      ELSE
        OPEN(File,OverrideOpenMode)
      END
    END
  END                                                 !End of Case Structure
  IF ERRORCODE()
    IF StandardWarning(Warn:DiskError,NAME(File)) THEN HALT(0,'Disk Error').
  END
  DO ProcedureReturn
!--------------------------------------
ProcedureReturn ROUTINE
  RETURN
!--------------------------------------
CreateFile ROUTINE
  CREATE(File)                                        !Create the file
  IF ERRORCODE()
    IF ERRORCODE() = 90
      IF StandardWarning(Warn:CreateError,NAME(File)).
    ELSE
      IF StandardWarning(Warn:CreateError,NAME(File)).
    END
  END
  IF OMITTED(3)
    OPEN(File,42h)                                    !Attempt to open the file
  ELSE
    OPEN(File,OverrideOpenMode)
  END
  IF ~ERRORCODE()                                     !  And return if it opened
    DO ProcedureReturn
  ELSE
    IF StandardWarning(Warn:CreateOpenError,NAME(File)).
  END


StandardWarning      FUNCTION(LONG WarningID)
  CODE
  RETURN(StandardWarning(WarningID,'',''))

StandardWarning      FUNCTION(LONG WarningID,STRING WarningText1)
  CODE
  RETURN(StandardWarning(WarningID,WarningText1,''))

StandardWarning      FUNCTION(LONG WarningID,STRING WarningText1,STRING WarningText2)
ErrorText            STRING(150),AUTO
CurrentErrorCode     LONG,AUTO
ReturnValue          LONG
  CODE
  IF SaveErrorCode
    CurrentErrorCode = SaveErrorCode
    IF SaveErrorCode <> 90
      ErrorText = CLIP(SaveError) & ' (' & SaveErrorCode & ')'
    ELSE
      ErrorText = CLIP(SaveFileError) & ' (' & SaveFileErrorCode & ')'
    END
  ELSE
    CurrentErrorCode = ERRORCODE()
    IF ERRORCODE() <> 90
      ErrorText = CLIP(ERROR()) & ' (' & ERRORCODE() & ')'
    ELSE
      ErrorText = CLIP(FILEERROR()) & ' (' & CLIP(FILEERRORCODE()) & ')'
    END
  END
  SaveErrorCode = 0
  SaveError = ''
  SaveFileErrorCode = 0
  SaveFileError = ''
  CASE WarningID
  OF Warn:InvalidFile
    MESSAGE('Error: (' & CLIP(ErrorText) & ') accessing ' |
    & CLIP(WarningText1) & '.  Press OK to end this application.'|
    ,'Invalid File',ICON:Exclamation,Button:OK,BUTTON:OK,0)
    HALT(0,'Invalid File!')
  OF Warn:InvalidKey
    IF MESSAGE(CLIP(WarningText1) & ' key file is invalid.  Do you '|
    &'want to rebuild the key?','Invalid Key',Icon:Question,|
    Button:Yes+Button:No,Button:Yes,0)=Button:No
      HALT(0,'Invalid Key!')
    ELSE
      RETURN(Button:Yes)
    END
  OF Warn:RebuildError
    MESSAGE('Error: (' & CLIP(ErrorText) & ') repairing key for ' |
    & CLIP(WarningText1) & '.  Press OK to end this application.',|
    'Key Rebuild Error',ICON:Exclamation,Button:OK,BUTTON:OK,0)
    HALT(0,'Error Rebuilding Key!')
  OF Warn:CreateError
    MESSAGE('Error: (' & CLIP(ErrorText) & ') creating ' |
    & CLIP(WarningText1) & '.  Press OK to end this application.',|
    'File Creation Error',ICON:Exclamation,Button:OK,BUTTON:OK,0)
    HALT(0,'File Creation Error!')
  OF Warn:CreateOpenError
    MESSAGE('Error: (' & CLIP(ErrorText) & ') opening created ' |
    & 'file:' & CLIP(WarningText1) & '.  Press OK to end this application.',|
    'File Creation Error',ICON:Exclamation,Button:OK,BUTTON:OK,0)
    HALT(0,'File Creation Error!')
  OF Warn:ProcedureToDo
    RETURN(MESSAGE('The Procedure ' & CLIP(WarningText1) & ' has not '|
    &'been defined.','Procedure not defined',ICON:Exclamation,|
    Button:OK,BUTTON:OK,0))
  OF Warn:BadKeyedRec
    RETURN(MESSAGE('Unable to read keyed record.  Error: ' |
    & CLIP(ErrorText) & '.  Insert Aborted',ICON:Exclamation,|
    Button:OK,Button:OK,0))
  OF Warn:OutOfRangeHigh
    RETURN(MESSAGE('The value of ' & CLIP(WarningText1) & ' must'|
    &' be lower than ' & CLIP(WarningText2) & '.','Range Error',|
    ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:OutOfRangeLow
    RETURN(MESSAGE('The value of ' & CLIP(WarningText1) & ' must be'|
    &' higher than ' & CLIP(WarningText2) & '.','Range Error',|
    ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:OutOfRange
    RETURN(MESSAGE('The value of ' & CLIP(WarningText1) & ' must be '|
    &'between ' & CLIP(WarningText2)|
    & '.','Range Error',ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:NotInFile
    RETURN(MESSAGE('The value for ' & CLIP(WarningText1) & ' must be '|
    &'found in the ' & CLIP(WarningText2) & ' file.','Field Contents '|
    &'Error',ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:RestrictUpdate
    RETURN(MESSAGE('This record is referenced from the file '|
    & CLIP(WarningText1) & '.  Linking field(s) have been restricted'|
    & ' from change and have been reset to original values.',|
    'Referential Integrity Update Error',ICON:Exclamation,|
    Button:OK,Button:OK,0))
  OF Warn:RestrictDelete
    RETURN(MESSAGE('This record is referenced from the file '|
    & CLIP(WarningText1) & '.  This record cannot be deleted while'|
    & ' these references exist.','Referential Integrity Delete Error'|
    ,ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:InsertError
    RETURN(MESSAGE('An error was experienced during the update of'|
    & ' record.  Error: ' & CLIP(ErrorText) & '.'|
    ,'Record Insert Error'|
    ,ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:RIUpdateError
    IF CurrentErrorCode AND CurrentErrorCode <> RecordChangedErr
      RETURN(MESSAGE('An error (' & CLIP(ErrorText) & ') was experienced'|
      &' when attempting to update a record from the file.  Probable Cause: ' |
      & CLIP(WarningText1) & '.','Update Operation Error',Icon:Exclamation,|
      Button:OK,Button:OK,0))
    ELSE
      RETURN(MESSAGE('This record was changed by another station.'|
      ,'Update Operation Error',Icon:Exclamation,|
      Button:OK,Button:OK,0))
    END
  OF Warn:RIFormUpdateError
    RETURN(MESSAGE('This record was changed by another station. '|
    &'Those changes will now be displayed. Use the Ditto Button or Ctrl+'''|
    &' to recall your changes.'|
    ,'Record Was Not Updated',Icon:Exclamation,|
    Button:OK,Button:OK,0))
  OF Warn:UpdateError
    RETURN(MESSAGE('An error was experienced changing this record.  '|
    &'Do you want to try to save again?','Record Update Error',|
    Icon:Exclamation,Button:Yes+Button:No+Button:Cancel,Button:Cancel,0))
  OF Warn:RIDeleteError
    RETURN(MESSAGE('An error (' & CLIP(ErrorText) & ') was experienced'|
    &' when attempting to delete a record from the file ' |
    & CLIP(WarningText1) & '.','Delete Operation Error',Icon:Exclamation,|
    Button:OK,Button:OK,0))
  OF Warn:DeleteError
    RETURN(MESSAGE('An error was experienced deleting this record.  '|
    &'Do you want to try to save again?','Record Update Error',|
    Icon:Exclamation,Button:Yes+Button:No+Button:Cancel,Button:Cancel,0))
  OF Warn:InsertDisabled
    RETURN(MESSAGE('This procedure was called to insert a record, '|
    & 'however inserts are not allowed for this procedure.  Press OK '|
    & 'to return to the calling procedure','Invalid Request',|
    Icon:Exclamation,Button:OK,Button:OK,0))
  OF Warn:UpdateDisabled
    RETURN(MESSAGE('This procedure was called to change a record, '|
    & 'however changes are not allowed for this procedure.  Press OK '|
    & 'to return to the calling procedure','Invalid Request',|
    ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:DeleteDisabled
    RETURN(MESSAGE('This procedure was called to delete a record, '|
    & 'however deletions are not allowed for this procedure.  Press OK '|
    & 'to return to the calling procedure','Invalid Request',|
    ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:NoCreate
    MESSAGE('The File ' & CLIP(WarningText1) & 'was not found, '|
    &'and creation of the file is not allowed.  Press OK to end '|
    &'this application.','File Creation Not Allowed',ICON:Exclamation,|
    Button:OK,BUTTON:OK,0)
    HALT(0,'File Creation Error!')
  OF Warn:ConfirmCancel
    RETURN(MESSAGE('Are you sure you want to cancel?'|
    ,'Update Cancelled',ICON:Question,Button:Yes+Button:No,|
    Button:No,0))
  OF Warn:DuplicateKey
    RETURN(MESSAGE('Adding this record creates a duplicate entry '|
    &'for the key:' & CLIP(WarningText1),'Duplicate Key Error',|
    ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:AutoIncError
    RETURN(MESSAGE('Attempts to automatically number this record have '|
    &'failed.  Error: ' & CLIP(ErrorText) & '.',|
    'Auto Increment Error',Icon:Exclamation,Button:Cancel+Button:Retry,|
    Button:Cancel,0))
  OF Warn:FileLoadError
    RETURN(MESSAGE(CLIP(WarningText1) & ' File Load Error.  '|
    &'Error: ' & CLIP(ErrorText) & '.','File Load Error',ICON:Exclamation,|
    Button:OK,Button:OK,0))
  OF Warn:ConfirmCancelLoad
    RETURN(MESSAGE('Are you certain you want to stop loading ' |
    & CLIP(WarningText1) & '?','Cancel Request',|
    ICON:Question,Button:OK+Button:Cancel,Button:Cancel,0))
  OF Warn:FileZeroLength
    RETURN(MESSAGE(CLIP(WarningText1) & ' File Load Error.  '|
    &'The file you''ve requested contains no text.','File Load Error',|
    ICON:Exclamation,Button:OK,Button:OK,0))
  OF Warn:EndOfASCIIQueue
    IF WarningText1 = 'Down'
      RETURN(MESSAGE('The end of the viewed file was encountered.  '|
      & 'Do you want to start again from the beginning?',|
      'End of File Error',ICON:Question,Button:Yes+Button:No,Button:Yes,0))
    ELSE
      RETURN(MESSAGE('The beginning of the viewed file was encountered.  '|
      & 'Do you want to start again from the end of the file?',|
      'Beginning of File Error',ICON:Question,Button:Yes+Button:No,|
      Button:Yes,0))
    END
  OF Warn:DiskError
    RETURN(MESSAGE('File (' & CLIP(WarningText1) & ') could not be '|
    & 'opened.  Error: ' & CLIP(ErrorText) & '.','File Access Error'|
    ,Icon:Exclamation,Button:OK,Button:OK,0))
  OF Warn:ProcessActionError
    IF WarningText1 = 'Put'
      RETURN(MESSAGE('An error was experienced when making changes'|
      & ' to the ' & CLIP(WarningText2) & ' file.  Error: '|
      & CLIP(ErrorText),'Process PUT Error',Icon:Exclamation|
      ,Button:OK,Button:OK,0))
    ELSE
      RETURN(MESSAGE('An error was experienced when deleting a record'|
      & ' from the ' & CLIP(WarningText2) & ' file.  Error: '|
      & CLIP(ErrorText),'Process DELETE Error',Icon:Exclamation|
      ,Button:OK,Button:OK,0))
    END
  OF Warn:StandardDelete
    RETURN(MESSAGE('You''ve selected to delete the highlighted record.  '|
    &'Press OK to confirm deletion of this record.',|
    'Confirm Delete',Icon:Question,Button:OK+Button:Cancel,Button:Cancel,0))
  OF Warn:SaveOnCancel
    RETURN(MESSAGE('Do you want to save the changes to this record?'|
    ,'Update Cancelled',ICON:Question,Button:Yes+Button:No+Button:Cancel,|
    Button:No,0))
  OF Warn:LogoutError
    MESSAGE('Error: (' & CLIP(ErrorText) & ') attempting to frame the ' |
    & LOWER(CLIP(WarningText1)) & ' transaction on ' & CLIP(WarningText2)|
    & '.  ' & CLIP(WarningText1) & ' transaction cancelled.'            |
    ,'Transaction Framing Error',ICON:Exclamation,Button:OK,BUTTON:OK,0)
  OF Warn:RecordFetchError
    MESSAGE('Error: (' & CLIP(ErrorText) & ') attempting to access a ' |
    & 'record from the ' & CLIP(WarningText1) & ' file.  Returning to ' |
    & 'previous window.' |
    ,'Record Retrieval Error',ICON:Exclamation,Button:OK,Button:OK,0)
  OF Warn:ViewOpenError
    MESSAGE('Error: (' & CLIP(ErrorText) & ') attempting to open the ' |
    & 'current VIEW. Filter and Range Limits ignored.' |
    ,'View Open Error',ICON:Exclamation,Button:OK,BUTTON:OK,0)
  OF Warn:NewRecordAdded
    RETURN(MESSAGE('This record has been added to the file. Do you want to add another record?'|
    ,'Record Added',ICON:Question,Button:Yes+Button:No,BUTTON:Yes,0))
  END

RISaveError PROCEDURE
  CODE
  SaveErrorCode = ERRORCODE()
  SaveError = ERROR()
  SaveFileErrorCode = FILEERRORCODE()
  SaveFileError = FILEERROR()
  ROLLBACK

