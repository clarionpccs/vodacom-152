

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBH01001.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseDateCodes PROCEDURE (func:Manufacturer)         !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Manufacturer     STRING(30)
BRW1::View:Browse    VIEW(MANUDATE)
                       PROJECT(mad:TheYear)
                       PROJECT(mad:TheMonth)
                       PROJECT(mad:AlcatelDay)
                       PROJECT(mad:DateCode)
                       PROJECT(mad:RecordNumber)
                       PROJECT(mad:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mad:TheYear            LIKE(mad:TheYear)              !List box control field - type derived from field
mad:TheMonth           LIKE(mad:TheMonth)             !List box control field - type derived from field
mad:AlcatelDay         LIKE(mad:AlcatelDay)           !List box control field - type derived from field
mad:DateCode           LIKE(mad:DateCode)             !List box control field - type derived from field
mad:RecordNumber       LIKE(mad:RecordNumber)         !Primary key field - type derived from field
mad:Manufacturer       LIKE(mad:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Manufacturer Date Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Manufacturer Date Code File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,100,120,224),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('24R(2)|M~Year~C@n04@24R(2)|M~Month~C(0)@n02@20L(2)|M~Day~@n02@40L(2)|M~Date Code' &|
   '~@s6@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date Code'),USE(?MotorolaTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       PROMPT('Criteria'),AT(296,158,208,72),USE(?CriteriaPrompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       GROUP('Criteria'),AT(292,150,216,84),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       BUTTON,AT(292,238),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(448,298),USE(?GenerateDateCodes),TRN,FLAT,LEFT,ICON('gencodep.jpg')
                       BUTTON,AT(292,268),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(292,298),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020353'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseDateCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUDATE.Open
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  tmp:Manufacturer    = func:Manufacturer
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANUDATE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Case tmp:Manufacturer
      Of 'ALCATEL'
          ?CriteriaPrompt{prop:Text} = 'The Date Of Manufacturer is a three digit code that is captured in Job Fault Code 3. The first digit is the day the phone was manufactured, the second is the month and the third is the year.'
      Of 'MOTOROLA'
          ?CriteriaPrompt{prop:Text} = 'The Date Of Manufacturer is contained in the fifth and sixth characters of the M.S.N.  The fifth character represents the year, and the sixth the month.'
      Of 'SAMSUNG'
          ?CriteriaPrompt{prop:Text} = 'The Date Of Manufacturer is contained in the forth and fifth characters of the M.S.N. The forth characters represents the year and the fifth he month.'
      Of 'SIEMENS'
          ?CriteriaPrompt{prop:Text} = 'The Date Of Manufacturer is captured in Job Fault Code 3.'
      Of 'BOSCH'
          ?CriteriaPrompt{prop:Text} = ''
  End !tmp:Manufacturer
  ! Save Window Name
   AddToLog('Window','Open','BrowseDateCodes')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,mad:DateCodeKey)
  BRW1.AddRange(mad:Manufacturer,tmp:Manufacturer)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,mad:DateCode,1,BRW1)
  BRW1.AddField(mad:TheYear,BRW1.Q.mad:TheYear)
  BRW1.AddField(mad:TheMonth,BRW1.Q.mad:TheMonth)
  BRW1.AddField(mad:AlcatelDay,BRW1.Q.mad:AlcatelDay)
  BRW1.AddField(mad:DateCode,BRW1.Q.mad:DateCode)
  BRW1.AddField(mad:RecordNumber,BRW1.Q.mad:RecordNumber)
  BRW1.AddField(mad:Manufacturer,BRW1.Q.mad:Manufacturer)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUDATE.Close
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseDateCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateDateCodes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020353'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020353'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020353'&'0')
      ***
    OF ?GenerateDateCodes
      ThisWindow.Update
      AutomaticDateCode
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GenerateDateCodes, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GenerateDateCodes, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateDateCodes PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::mad:Record  LIKE(mad:RECORD),STATIC
QuickWindow          WINDOW('Update Date Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Update Date Codes'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Motorola Date Code'),USE(?MotorolaTab)
                           PROMPT('Date Code'),AT(270,168),USE(?mad:DateCode:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s6),AT(346,168,64,10),USE(mad:DateCode),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Date Code'),TIP('Date Code'),REQ,UPR
                           PROMPT('Year'),AT(270,186),USE(?mad:TheYear:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n04),AT(346,186,64,10),USE(mad:TheYear),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Year'),TIP('Year'),REQ,UPR
                           PROMPT('Month'),AT(270,206),USE(?mad:TheMonth:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n02),AT(346,206,64,10),USE(mad:TheMonth),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Month'),TIP('Month'),REQ,UPR
                           PROMPT('Day'),AT(270,226),USE(?mad:AlcatelDay:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n02),AT(346,226,64,10),USE(mad:AlcatelDay),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Day'),TIP('Day'),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Date Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Date Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020357'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateDateCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mad:Record,History::mad:Record)
  SELF.AddHistoryField(?mad:DateCode,3)
  SELF.AddHistoryField(?mad:TheYear,4)
  SELF.AddHistoryField(?mad:TheMonth,5)
  SELF.AddHistoryField(?mad:AlcatelDay,11)
  SELF.AddUpdateFile(Access:MANUDATE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANUDATE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANUDATE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Case man:Manufacturer
      Of 'SIEMENS'
          ?WindowTitle{prop:Text} = 'Insert / Amend Siemens Date Code'
      Of 'ALCATEL'
          ?mad:AlcatelDay{prop:Hide} = 0
          ?mad:AlcatelDay:Prompt{prop:Hide} = 0
          ?WindowTitle{prop:Text} = 'Insert / Amend Alcatel Date Code'
      Of 'SAMSUNG'
          ?WindowTitle{prop:Text} = 'Insert / Amend Samsung Date Code'
      Of 'BOSCH'
         ?WindowTitle{prop:Text} = 'Insert / Amend Bosch Date Code'
  
  End !mad:Manufacturer
  ! Save Window Name
   AddToLog('Window','Open','UpdateDateCodes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUDATE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateDateCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020357'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020357'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020357'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateMANMARK PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
save_sto_id          USHORT,AUTO
tmp:MaxMarkup        LONG
ActionMessage        CSTRING(40)
old:Price            REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mak:Record  LIKE(mak:RECORD),STATIC
QuickWindow          WINDOW('Update In Warranty Markup'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend In Warr Markup'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Site Location'),AT(264,174),USE(?mak:SiteLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,184,124,10),USE(mak:SiteLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Site Location'),TIP('Site Location'),REQ,UPR
                           BUTTON,AT(391,180),USE(?LookupSiteLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('In Warr Markup'),AT(264,202),USE(?mak:InWarrantyMarkup:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(264,212,64,10),USE(mak:InWarrantyMarkup),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('In Warranty Markup'),TIP('In Warranty Markup'),UPR
                           BUTTON,AT(368,226),USE(?ApplyMarkup),TRN,FLAT,LEFT,ICON('appmarkp.jpg')
                         END
                       END
                       BUTTON,AT(304,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:mak:SiteLocation                Like(mak:SiteLocation)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Site Location'
  OF ChangeRecord
    ActionMessage = 'Changing A Site Location'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020377'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMANMARK')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mak:Record,History::mak:Record)
  SELF.AddHistoryField(?mak:SiteLocation,3)
  SELF.AddHistoryField(?mak:InWarrantyMarkup,4)
  SELF.AddUpdateFile(Access:MANMARK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCATION.Open
  Access:STOCK.UseFile
  Access:STOAUDIT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANMARK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateMANMARK')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?mak:SiteLocation{Prop:Tip} AND ~?LookupSiteLocation{Prop:Tip}
     ?LookupSiteLocation{Prop:Tip} = 'Select ' & ?mak:SiteLocation{Prop:Tip}
  END
  IF ?mak:SiteLocation{Prop:Msg} AND ~?LookupSiteLocation{Prop:Msg}
     ?LookupSiteLocation{Prop:Msg} = 'Select ' & ?mak:SiteLocation{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANMARK')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSiteLocations
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020377'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020377'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020377'&'0')
      ***
    OF ?mak:SiteLocation
      IF mak:SiteLocation OR ?mak:SiteLocation{Prop:Req}
        loc:Location = mak:SiteLocation
        !Save Lookup Field Incase Of error
        look:mak:SiteLocation        = mak:SiteLocation
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            mak:SiteLocation = loc:Location
          ELSE
            !Restore Lookup On Error
            mak:SiteLocation = look:mak:SiteLocation
            SELECT(?mak:SiteLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSiteLocation
      ThisWindow.Update
      loc:Location = mak:SiteLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          mak:SiteLocation = loc:Location
          Select(?+1)
      ELSE
          Select(?mak:SiteLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?mak:SiteLocation)
    OF ?mak:InWarrantyMarkup
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mak:InWarrantyMarkup, Accepted)
      If ~0{prop:Acceptall}
          tmp:MaxMarkup = GETINI('STOCK','MaxPartMarkup',,CLIP(PATH())&'\SB2KDEF.INI')
          If mak:InWarrantyMarkup > tmp:MaxMarkup
              mak:InWarrantyMarkup = tmp:MaxMarkup
              Display()
              Case Missive('You cannot increase the markup above ' & Clip(tmp:MaxMarkup) & '%.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          End !If loc:OutWarrantyMarkup > tmp:MaxMarkup
      End !0{prop:Acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mak:InWarrantyMarkup, Accepted)
    OF ?ApplyMarkup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ApplyMarkup, Accepted)
      Case Missive('You are about to reprice all the stock in the selected location.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
              Prog.ProgressSetup(Records(stock)/Records(location))
              Prog.ProgressText('Updating Stock')
      
              Save_sto_ID = Access:STOCK.SaveFile()
              Access:STOCK.ClearKey(sto:Location_Manufacturer_Key)
              sto:Location     = mak:SiteLocation
      
              Access:MANUFACT.ClearKey(man:RecordNumberKey)
              man:RecordNumber = mak:RefNumber
              If Access:MANUFACT.TryFetch(man:RecordNumberKey) = Level:Benign
                  !Found
                  sto:Manufacturer = man:Manufacturer
                  Set(sto:Location_Manufacturer_Key,sto:Location_Manufacturer_Key)
                  Loop
                      If Access:STOCK.NEXT()
                         Break
                      End !If
                      If sto:Location     <> mak:SiteLocation      |
                      Or sto:Manufacturer <> man:Manufacturer      |
                          Then Break.  ! End If
                    IF Prog.InsideLoop()
                      BREAK
                    END
                    old:price = sto:Purchase_Cost
                    IF loc:VirtualSite = FALSE
          !            sto:Sale_Cost   = Markup(sto:Sale_Cost,sto:Purchase_Cost,loc:OutWarrantyMarkUp)
                      sto:RetailMarkup = mak:InWarrantyMarkup
                    ELSE
                      sto:Purchase_Cost   = Markups(sto:Purchase_Cost,sto:AveragePurchaseCost,mak:InWarrantyMarkup)
                      sto:PurchaseMarkUp = mak:InWarrantyMarkup
                    END
      
                    IF Access:Stock.Update()
                      !Error!
                    ELSE
                      If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                         'ADD', | ! Transaction_Type
                                         '', | ! Depatch_Note_Number
                                         0, | ! Job_Number
                                         0, | ! Sales_Number
                                         0, | ! Quantity
                                         sto:Purchase_Cost, | ! Purchase_Cost
                                         sto:Sale_Cost, | ! Sale_Cost
                                         sto:Retail_Cost, | ! Retail_Cost
                                         'IN WARRANTY PRICE ADJUSTMENT', | ! Notes
                                         'IN WARRANTY PRICE ADJUSTMENT FROM '&FORMAT(old:price,@n10.2)&' TO '&FORMAT(sto:Purchase_cost),|
                                         sto:AveragePurchaseCost,|
                                         old:Price,|
                                         sto:Sale_Cost,|
                                         sto:Retail_Cost)
      
                        ! Added OK
      
                      Else ! AddToStockHistory
                        ! Error
                      End ! AddToStockHistory
                    END
                  END
      
              Else !If Access:MANUFACT.TryFetch(man:RecordNumberKey) = Level:Benign
                  !Error
              End !If Access:MANUFACT.TryFetch(man:RecordNumberKey) = Level:Benign
      
              Prog.ProgressFinish()
      
      
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ApplyMarkup, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
BrowseInWarrantyMarkup PROCEDURE (func:RefNumber)     !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:RefNumber        LONG
BRW1::View:Browse    VIEW(MANMARK)
                       PROJECT(mak:SiteLocation)
                       PROJECT(mak:InWarrantyMarkup)
                       PROJECT(mak:RecordNumber)
                       PROJECT(mak:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mak:SiteLocation       LIKE(mak:SiteLocation)         !List box control field - type derived from field
mak:InWarrantyMarkup   LIKE(mak:InWarrantyMarkup)     !List box control field - type derived from field
mak:RecordNumber       LIKE(mak:RecordNumber)         !Primary key field - type derived from field
mak:RefNumber          LIKE(mak:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse In Warranty Markup'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The In Warranty Markup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(256,116,172,208),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('89L(2)|M~Site Location~@s30@76L(2)|M~In Warr Markup~@s8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,164),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(448,236),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(448,268),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(448,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Site Location'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(256,102,124,10),USE(mak:SiteLocation),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Site Location'),TIP('Site Location'),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020375'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseInWarrantyMarkup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANMARK.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANMARK,SELF)
  tmp:RefNumber   = func:RefNumber
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseInWarrantyMarkup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mak:SiteLocationKey)
  BRW1.AddRange(mak:RefNumber,tmp:RefNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mak:SiteLocation,mak:SiteLocation,1,BRW1)
  BRW1.AddField(mak:SiteLocation,BRW1.Q.mak:SiteLocation)
  BRW1.AddField(mak:InWarrantyMarkup,BRW1.Q.mak:InWarrantyMarkup)
  BRW1.AddField(mak:RecordNumber,BRW1.Q.mak:RecordNumber)
  BRW1.AddField(mak:RefNumber,BRW1.Q.mak:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANMARK.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseInWarrantyMarkup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANMARK
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020375'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020375'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020375'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateMANFAUPA PROCEDURE                              !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
tmp:SavePath         STRING(255)
save_manfault_id     USHORT,AUTO
save_manfpalo_id     USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
save_map_id          USHORT,AUTO
tmp:DatePicture      STRING(30)
tmp:SaveState        LONG
tmp:FieldNumber      LONG
tmp:Manufacturer     STRING(30)
tmp:ScreenOrder      LONG
tmp:MainFaultNumber  LONG
BRW8::View:Browse    VIEW(MANFPALO)
                       PROJECT(mfp:Field)
                       PROJECT(mfp:Description)
                       PROJECT(mfp:RecordNumber)
                       PROJECT(mfp:Manufacturer)
                       PROJECT(mfp:Field_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mfp:Field              LIKE(mfp:Field)                !List box control field - type derived from field
mfp:Description        LIKE(mfp:Description)          !List box control field - type derived from field
mfp:RecordNumber       LIKE(mfp:RecordNumber)         !Primary key field - type derived from field
mfp:Manufacturer       LIKE(mfp:Manufacturer)         !Browse key field - type derived from field
mfp:Field_Number       LIKE(mfp:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW16::View:Browse   VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:RecordNumber)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:RelatedPartCode)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:RecordNumber       LIKE(mfo:RecordNumber)         !Primary key field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Browse key field - type derived from field
mfo:RelatedPartCode    LIKE(mfo:RelatedPartCode)      !Browse key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::mfp:Field_Number    LIKE(mfp:Field_Number)
HK11::mfp:Manufacturer    LIKE(mfp:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK17::mfo:Field_Number    LIKE(mfo:Field_Number)
HK17::mfo:Manufacturer    LIKE(mfo:Manufacturer)
HK17::mfo:RelatedPartCode LIKE(mfo:RelatedPartCode)
! ---------------------------------------- Higher Keys --------------------------------------- !
History::map:Record  LIKE(map:RECORD),STATIC
QuickWindow          WINDOW('Update Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Fault Code'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,280,310),USE(?General_Sheet),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(68,66),USE(?map:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(144,66,124,10),USE(map:Manufacturer),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,SINGLE,READONLY
                           PROMPT('Field Name'),AT(68,80),USE(?map:Field_Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           TEXT,AT(144,80,124,10),USE(map:Field_Name),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,SINGLE
                           PROMPT('Field Number'),AT(68,94),USE(?map:Field_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           SPIN(@n2),AT(144,94,64,10),USE(map:Field_Number),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,RANGE(1,12),STEP(1),MSG('Fault Code Field Number')
                           PROMPT('Screen Order'),AT(220,94),USE(?map:ScreenOrder:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(276,94,64,10),USE(map:ScreenOrder),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Screen Order'),TIP('Screen Order'),REQ,UPR,RANGE(1,12),STEP(1)
                           CHECK('Main Out Fault'),AT(144,116),USE(map:MainFault),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Main Fault')
                           CHECK('Use Related Job Code'),AT(240,114),USE(map:UseRelatedJobCode),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('c'),TIP('c'),VALUE('1','0')
                           CHECK('Key Repair'),AT(144,126),USE(map:KeyRepair),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Key Repair'),TIP('Key Repair'),VALUE('1','0')
                           CHECK('"CCT Reference Fault Code"'),AT(144,300),USE(map:CCTReferenceFaultCode),HIDE,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('"CCT Reference Fault Code"'),TIP('"CCT Reference Fault Code"'),VALUE('1','0')
                           OPTION('Field Type'),AT(144,136,176,44),USE(map:Field_Type),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Generic String'),AT(152,144),USE(?map:Field_Type:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('STRING')
                             RADIO('Date'),AT(152,156),USE(?map:Field_Type:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('DATE')
                             RADIO('Number Only'),AT(152,168),USE(?map:Field_Type:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('NUMBER')
                           END
                           LIST,AT(252,156,64,10),USE(tmp:DatePicture),HIDE,LEFT(2),FONT(,8,010101H,FONT:bold),COLOR(COLOR:White),DROP(20),FROM('DD/MM/YYYY|DD/MM/YY|MM/DD/YYYY|MM/DD/YY|MM/YYYY|MM/YY|YYYY/MM|YY/MM|YYYY/MM/DD|YY/MM/DD|YYYYMMDD')
                           PROMPT('Date Picture'),AT(252,146),USE(?DatePicture),HIDE,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI)
                           CHECK('Compulsory At Completion'),AT(144,182),USE(map:Compulsory),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Compulsory For Adjustment'),AT(144,192),USE(map:CompulsoryForAdjustment),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Compulsory For Adjustment'),TIP('Compulsory For Adjustment'),VALUE('1','0')
                           CHECK('Restrict Length'),AT(144,202),USE(map:RestrictLength),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Restrict Length'),TIP('Restrict Length'),VALUE('1','0')
                           GROUP,AT(64,212,276,18),USE(?RestrictGroup)
                             PROMPT('Length From'),AT(68,217),USE(?map:LengthFrom:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(144,217,64,10),USE(map:LengthFrom),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Length From'),TIP('Length From'),REQ,UPR
                             PROMPT('Length To'),AT(224,217),USE(?map:LengthTo:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(272,217,64,10),USE(map:LengthTo),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Length To'),TIP('Length To'),REQ,UPR
                           END
                           PROMPT('Job Fault Code'),AT(272,230),USE(?map:CopyJobFaultCode:Prompt),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Copy From Job Fault Code'),AT(144,238),USE(map:CopyFromJobFaultCode),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Copy From Job Fault Code'),TIP('Copy From Job Fault Code'),VALUE('1','0')
                           SPIN(@s8),AT(272,238,64,10),USE(map:CopyJobFaultCode),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,RANGE(1,20),STEP(1),MSG('Job Fault Code')
                           CHECK('Fill With "N/A" For Accessory'),AT(144,250),USE(map:NAForAccessory),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('N/A For Accessory'),TIP('N/A For Accessory'),VALUE('1','0')
                           CHECK('Fill With "N/A" For Software Upgrade'),AT(144,262),USE(map:NAForSW),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Fill "N/A" For Software Upgrade'),TIP('Fill "N/A" For Software Upgrade'),VALUE('1','0')
                           CHECK('Force Format'),AT(144,274),USE(map:ForceFormat),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),MSG('Force Format'),TIP('Force Format'),VALUE('1','0')
                           GROUP,AT(68,278,271,27),USE(?Group:FieldFormat)
                             BUTTON,AT(272,278),USE(?Key),TRN,FLAT,ICON('keyp.jpg')
                             PROMPT('Field Format'),AT(68,286),USE(?map:FieldFormat:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                             TEXT,AT(144,286,124,10),USE(map:FieldFormat),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Field Format'),TIP('Field Format'),REQ,UPR,SINGLE
                           END
                         END
                       END
                       SHEET,AT(348,54,268,310),USE(?Sheet2),BELOW,COLOR(0D6E7EFH),SPREAD
                         TAB('Lookup Field'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Lookup Fields'),AT(352,56),USE(?Prompt12),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Lookup'),AT(352,70,52,12),USE(map:Lookup),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Force Lookup'),AT(416,70,66,12),USE(map:Force_Lookup),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           ENTRY(@s30),AT(352,86,127,10),USE(mfo:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(352,98,260,228),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('86L(2)|M~Field~@s30@240L(2)|M~Description~S(240)@s60@'),FROM(Queue:Browse)
                           BUTTON,AT(416,330),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(480,330),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(548,330),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(348,330),USE(?Button:CopyJobFaultCodes),TRN,FLAT,ICON('copjobp.jpg')
                         END
                         TAB('Related Fault Codes'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Related Fault Codes'),AT(352,58),USE(?Prompt13),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(352,98,260,240),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Field~@s30@240L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(136,366),USE(?Button:ImportCCTRefNos),TRN,FLAT,ICON('impcctp.jpg')
                       BUTTON,AT(68,366),USE(?Button:LinkedFaultCodes),TRN,FLAT,ICON('linkfaup.jpg')
                       CHECK('Not Available'),AT(144,106),USE(map:NotAvailable),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Available'),TIP('Not Available'),VALUE('1','0')
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:SkipCount          Byte(0)
Prog:SkipNumber         Long()
Prog:PercentText        String(100)
Prog:UserString         String(100)
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING(@s100),AT(0,3,161,10),USE(Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(0,32,161,10),USE(Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),HIDE,LEFT,ICON('pcancel.ico')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW16                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW16::Sort0:Locator StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
! -----------------------------------------
! DBH 07/10/2008 #10494
! Inserting: Import CCT Ref Nos
tmp:CCTImportFile      String(255),Static

CCTImportFile    File,Driver('BASIC'),Pre(cctimp),Name(tmp:CCTImportFile),Create,Bindable,Thread
Record              Record
Field1              String(1)
Field2              String(1)
CCTReference        String(30)
Field3              String(1)
ServiceActivity     String(30)
                    End
                End
! End: DBH 07/10/2008 #10494
! -----------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Prog:SkipCount = 0
    Prog:PercentText = '< 1% Completed'
    Prog:UserString = 'Working...'
    Prog:SkipNumber = 0
    If glo:WebJob
        Clarionet:OpenPushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Open(Prog:ProgressWindow)
        ?Prog:Cancel{prop:Hide} = 0
    End ! If glo:WebJob
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            Prog:PercentText = Format(Prog:PercentProgress,@n3) & '% Completed'
        Else
            Prog:PercentText = ''
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer

    If glo:WebJob
        If Prog:SkipCount < Prog:SkipNumber
            Prog:SkipCount += 1
            Exit
        Else
            Prog:SkipCount = 0
        End ! If Prog:SkipCount < 100
    End ! If glo:WebJob

    If glo:WebJob
        Clarionet:UpdatePushWindow(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    Prog:PercentText = 'Finished.'
    If glo:WebJob
        Clarionet:ClosePushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Close(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()
UseRelatedJobCode      Routine
    If map:UseRelatedJobCode
        Save_MANFAULT_ID = Access:MANFAULT.SaveFile()
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = map:Manufacturer
        maf:MainFault = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Found
            tmp:MainFaultNumber = maf:Field_Number
        Else ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End ! If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
        Access:MANFAULT.RestoreFile(Save_MANFAULT_ID)
        Select(?Sheet2,2)
        Brw16.ResetSort(1)
    Else
        Select(?Sheet2,1)
        Brw8.ResetSort(1)
    End ! If map:UseRelatedJobCode
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Part Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Part Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020362'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANFAUPA')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(map:Record,History::map:Record)
  SELF.AddHistoryField(?map:Manufacturer,1)
  SELF.AddHistoryField(?map:Field_Name,3)
  SELF.AddHistoryField(?map:Field_Number,2)
  SELF.AddHistoryField(?map:ScreenOrder,17)
  SELF.AddHistoryField(?map:MainFault,14)
  SELF.AddHistoryField(?map:UseRelatedJobCode,15)
  SELF.AddHistoryField(?map:KeyRepair,22)
  SELF.AddHistoryField(?map:CCTReferenceFaultCode,23)
  SELF.AddHistoryField(?map:Field_Type,4)
  SELF.AddHistoryField(?map:Compulsory,7)
  SELF.AddHistoryField(?map:CompulsoryForAdjustment,21)
  SELF.AddHistoryField(?map:RestrictLength,8)
  SELF.AddHistoryField(?map:LengthFrom,9)
  SELF.AddHistoryField(?map:LengthTo,10)
  SELF.AddHistoryField(?map:CopyFromJobFaultCode,18)
  SELF.AddHistoryField(?map:CopyJobFaultCode,19)
  SELF.AddHistoryField(?map:NAForAccessory,20)
  SELF.AddHistoryField(?map:NAForSW,24)
  SELF.AddHistoryField(?map:ForceFormat,11)
  SELF.AddHistoryField(?map:FieldFormat,12)
  SELF.AddHistoryField(?map:Lookup,5)
  SELF.AddHistoryField(?map:Force_Lookup,6)
  SELF.AddHistoryField(?map:NotAvailable,16)
  SELF.AddUpdateFile(Access:MANFAUPA)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MANFAULO.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Access:MANFAULT.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFAUPA
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:MANFPALO,SELF)
  BRW16.Init(?List:2,Queue:Browse:1.ViewPosition,BRW16::View:Browse,Queue:Browse:1,Relate:MANFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! What is the picture?
  Case map:DateType
      Of '@d5'
          tmp:DatePicture = 'DD/MM/YY'
      Of '@d6'
          tmp:DatePicture = 'DD/MM/YYYY'
      Of '@d1'
          tmp:DatePicture = 'MM/DD/YY'
      Of '@d2'
          tmp:DatePicture = 'MM/DD/YYYY'
      Of '@d13'
          tmp:DatePicture = 'MM/YY'
      Of '@d14'
          tmp:DatePicture = 'MM/YYYY'
      Of '@d15'
          tmp:DatePicture = 'YY/MM'
      Of '@d16'
          tmp:DatePicture = 'YYYY/MM'
      Of '@d9'
          tmp:DatePicture = 'YY/MM/DD'
      Of '@d10'
          tmp:DatePicture = 'YYYY/MM/DD'
      Of '@d11'
          tmp:DatePicture = 'YYMMDD'
      Of '@d12'
          tmp:DatePicture = 'YYYYMMDD'
  End !tmp:DatePicture
  Post(Event:Accepted,?map:Field_Type)
  
  ! Inserting (DBH 29/04/2008) # 9723 - New field to specify a CCT fault code
  If map:Manufacturer = 'NOKIA'
      ?map:CCTReferenceFaultCode{prop:Hide} = 0
  Else ! If map:Manufacturer = 'NOKIA'
      ?map:CCTReferenceFaultCode{prop:Hide} = 1
  End ! If map:Manufacturer = 'NOKIA'
  ! End (DBH 29/04/2008) #9723
  
  ?Sheet2{prop:Wizard} = 1
  ! Save Window Name
   AddToLog('Window','Open','UpdateMANFAUPA')
  ?tmp:DatePicture{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,mfp:Field_Key)
  BRW8.AddRange(mfp:Field_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,mfp:Field,1,BRW8)
  BRW8.AddField(mfp:Field,BRW8.Q.mfp:Field)
  BRW8.AddField(mfp:Description,BRW8.Q.mfp:Description)
  BRW8.AddField(mfp:RecordNumber,BRW8.Q.mfp:RecordNumber)
  BRW8.AddField(mfp:Manufacturer,BRW8.Q.mfp:Manufacturer)
  BRW8.AddField(mfp:Field_Number,BRW8.Q.mfp:Field_Number)
  BRW16.Q &= Queue:Browse:1
  BRW16.AddSortOrder(,mfo:RelatedFieldKey)
  BRW16.AddRange(mfo:Field_Number)
  BRW16.AddLocator(BRW16::Sort0:Locator)
  BRW16::Sort0:Locator.Init(,mfo:Field,1,BRW16)
  BRW16.AddField(mfo:Field,BRW16.Q.mfo:Field)
  BRW16.AddField(mfo:Description,BRW16.Q.mfo:Description)
  BRW16.AddField(mfo:RecordNumber,BRW16.Q.mfo:RecordNumber)
  BRW16.AddField(mfo:Manufacturer,BRW16.Q.mfo:Manufacturer)
  BRW16.AddField(mfo:RelatedPartCode,BRW16.Q.mfo:RelatedPartCode)
  BRW16.AddField(mfo:Field_Number,BRW16.Q.mfo:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?map:MainFault{Prop:Checked} = True
    UNHIDE(?map:UseRelatedJobCode)
  END
  IF ?map:MainFault{Prop:Checked} = False
    map:UseRelatedJobCode = 0
    HIDE(?map:UseRelatedJobCode)
  END
  IF ?map:CCTReferenceFaultCode{Prop:Checked} = True
    UNHIDE(?Button:ImportCCTRefNos)
  END
  IF ?map:CCTReferenceFaultCode{Prop:Checked} = False
    HIDE(?Button:ImportCCTRefNos)
  END
  IF ?map:Compulsory{Prop:Checked} = True
    ENABLE(?map:CompulsoryForAdjustment)
  END
  IF ?map:Compulsory{Prop:Checked} = False
    map:CompulsoryForAdjustment = 0
    DISABLE(?map:CompulsoryForAdjustment)
  END
  IF ?map:RestrictLength{Prop:Checked} = True
    ENABLE(?RestrictGroup)
  END
  IF ?map:RestrictLength{Prop:Checked} = False
    DISABLE(?RestrictGroup)
  END
  IF ?map:CopyFromJobFaultCode{Prop:Checked} = True
    ENABLE(?map:CopyJobFaultCode)
    ENABLE(?map:CopyJobFaultCode:Prompt)
  END
  IF ?map:CopyFromJobFaultCode{Prop:Checked} = False
    DISABLE(?map:CopyJobFaultCode)
    DISABLE(?map:CopyJobFaultCode:Prompt)
  END
  IF ?map:ForceFormat{Prop:Checked} = True
    ENABLE(?Group:FieldFormat)
  END
  IF ?map:ForceFormat{Prop:Checked} = False
    DISABLE(?Group:FieldFormat)
  END
  IF ?map:Lookup{Prop:Checked} = True
    UNHIDE(?map:Force_Lookup)
    UNHIDE(?Insert)
    UNHIDE(?Change)
    UNHIDE(?Delete)
    UNHIDE(?Button:CopyJobFaultCodes)
  END
  IF ?map:Lookup{Prop:Checked} = False
    HIDE(?map:Force_Lookup)
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
    HIDE(?Button:CopyJobFaultCodes)
  END
  IF ?map:NotAvailable{Prop:Checked} = True
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
  END
  IF ?map:NotAvailable{Prop:Checked} = False
    UNHIDE(?Insert)
    UNHIDE(?Change)
    UNHIDE(?Delete)
  END
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  BRW16.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW16.AskProcedure = 0
      CLEAR(BRW16.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  Do UseRelatedJobCode
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MANFAULO.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANFAUPA')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request  = Insertrecord
      If map:field_number = ''
          Case Missive('You have not entered a Field Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = 0
      End
  End
  If do_update# = 1 And map:lookup = 'YES'
      glo:select1  = map:manufacturer
      glo:select2  = map:field_number
      glo:select3  = map:field_name
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFPALO
    ReturnValue = GlobalResponse
  END
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
  End !If do_update# = 1 And maf:lookup = 'YES'
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020362'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020362'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020362'&'0')
      ***
    OF ?map:Field_Number
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW8.ApplyRange
      BRW8.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?map:MainFault
      IF ?map:MainFault{Prop:Checked} = True
        UNHIDE(?map:UseRelatedJobCode)
      END
      IF ?map:MainFault{Prop:Checked} = False
        map:UseRelatedJobCode = 0
        HIDE(?map:UseRelatedJobCode)
      END
      ThisWindow.Reset
    OF ?map:UseRelatedJobCode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?map:UseRelatedJobCode, Accepted)
      Do UseRelatedJobCode
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?map:UseRelatedJobCode, Accepted)
    OF ?map:CCTReferenceFaultCode
      IF ?map:CCTReferenceFaultCode{Prop:Checked} = True
        UNHIDE(?Button:ImportCCTRefNos)
      END
      IF ?map:CCTReferenceFaultCode{Prop:Checked} = False
        HIDE(?Button:ImportCCTRefNos)
      END
      ThisWindow.Reset
    OF ?map:Field_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?map:Field_Type, Accepted)
      Case map:Field_Type
          Of 'DATE'
              ?tmp:DatePicture{prop:Hide} = 0
              ?DatePicture{prop:Hide} = 0
              map:ForceFormat = 0
              ?map:ForceFormat{prop:Disable} = 1
              map:Lookup = 'NO'
              map:RestrictLength = 0
          Of 'STRING'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?map:ForceFormat{prop:Disable} = 0
          Of 'NUMBER'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?map:ForceFormat{prop:Disable} = 1
              map:ForceFormat = 0
      End !map:Field_Type
      Post(Event:Accepted,?map:ForceFormat)
      Post(Event:Accepted,?map:Lookup)
      Post(Event:Accepted,?map:RestrictLength)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?map:Field_Type, Accepted)
    OF ?map:Compulsory
      IF ?map:Compulsory{Prop:Checked} = True
        ENABLE(?map:CompulsoryForAdjustment)
      END
      IF ?map:Compulsory{Prop:Checked} = False
        map:CompulsoryForAdjustment = 0
        DISABLE(?map:CompulsoryForAdjustment)
      END
      ThisWindow.Reset
    OF ?map:RestrictLength
      IF ?map:RestrictLength{Prop:Checked} = True
        ENABLE(?RestrictGroup)
      END
      IF ?map:RestrictLength{Prop:Checked} = False
        DISABLE(?RestrictGroup)
      END
      ThisWindow.Reset
    OF ?map:CopyFromJobFaultCode
      IF ?map:CopyFromJobFaultCode{Prop:Checked} = True
        ENABLE(?map:CopyJobFaultCode)
        ENABLE(?map:CopyJobFaultCode:Prompt)
      END
      IF ?map:CopyFromJobFaultCode{Prop:Checked} = False
        DISABLE(?map:CopyJobFaultCode)
        DISABLE(?map:CopyJobFaultCode:Prompt)
      END
      ThisWindow.Reset
    OF ?map:ForceFormat
      IF ?map:ForceFormat{Prop:Checked} = True
        ENABLE(?Group:FieldFormat)
      END
      IF ?map:ForceFormat{Prop:Checked} = False
        DISABLE(?Group:FieldFormat)
      END
      ThisWindow.Reset
    OF ?Key
      ThisWindow.Update
      FormatKey
      ThisWindow.Reset
    OF ?map:Lookup
      IF ?map:Lookup{Prop:Checked} = True
        UNHIDE(?map:Force_Lookup)
        UNHIDE(?Insert)
        UNHIDE(?Change)
        UNHIDE(?Delete)
        UNHIDE(?Button:CopyJobFaultCodes)
      END
      IF ?map:Lookup{Prop:Checked} = False
        HIDE(?map:Force_Lookup)
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
        HIDE(?Button:CopyJobFaultCodes)
      END
      ThisWindow.Reset
    OF ?Button:CopyJobFaultCodes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CopyJobFaultCodes, Accepted)
      SaveRequest#      = GlobalRequest
      GlobalResponse    = RequestCancelled
      GlobalRequest     = SelectRecord
      Browse_Manufacturer_Fault_Coding
      If Globalresponse = RequestCompleted
          FieldNumber# = maf:Field_Number
          Beep(Beep:SystemQuestion);  Yield()
          Case Missive('Are you sure you want to add all the fault code lookup information from Fault Code: ' & Clip(maf:Field_Name) & '?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes')
              Of 2 ! Yes Button
                  Access:MANFAULO.Clearkey(mfo:Field_Key)
                  mfo:Manufacturer = map:Manufacturer
                  mfo:Field_Number = FieldNumber#
                  Set(mfo:Field_Key,mfo:Field_Key)
                  Loop
                      If Access:MANFAULO.Next()
                          Break
                      End ! If Access:MANFAULO.Next()
                      If mfo:Manufacturer <> map:Manufacturer
                          Break
                      End ! If mfo:Manufacturer <> map:Manufacturer
                      If mfo:Field_Number <> FieldNumber#
                          Break
                      End ! If mfo:Field_Number <> FieldNumber#
                      Access:MANFPALO.Clearkey(mfp:Field_Key)
                      mfp:Manufacturer = map:Manufacturer
                      mfp:Field_Number = map:Field_Number
                      mfp:Field = mfo:Field
                      If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                          ! Found
      
                      Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                          If Access:MANFPALO.PrimeRecord() = Level:Benign
                              mfp:Manufacturer = map:Manufacturer
                              mfp:Field_Number = map:Field_Number
                              mfp:Field = mfo:Field
                              mfp:Description = mfo:Description
                              If Access:MANFPALO.TryInsert() = Level:Benign
                                  ! Inserted
                              Else ! If Access:MANFPALO.TryInsert() = Level:Benign
                                  Access:MANFPALO.CancelAutoInc()
                              End ! If Access:MANFPALO.TryInsert() = Level:Benign
                          End ! If Access:MANFPALO.PrimeRecord() = Level:Benign
                      End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                  End ! Loop (MANFAULO)
                  Beep(Beep:SystemAsterisk);  Yield()
                  Case Missive('Done.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Of 1 ! No Button
          End ! Case Missive
      Else
          !Not Selected
      End
      Display()
      GlobalRequest     = SaveRequest#
      
      BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CopyJobFaultCodes, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Button:ImportCCTRefNos
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ImportCCTRefNos, Accepted)
          If SecurityCheck('IMPORT CCT REF NO')
              Beep(Beep:SystemHand);  Yield()
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !     Else ! If SecurityAccess('IMPORT CCT REF NO')
      
          tmp:SavePath = Path()
          tmp:CCTImportFile = ''
          If FileDialog('Choose File',tmp:CCTImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
              !Found
              SetPath(tmp:SavePath)
              Open(CCTImportFile)
              If Error()
                  Stop(Error())
              Else !If Error()
                  Count# = 0
                  Set(CCTImportFile,0)
                  Loop
                      Next(CCTImportFile)
                      If Error()
                          Break
                      End ! If Error()
                      Count# += 1
                  End ! Loop
      
                  Do Prog:ProgressSetup
                  Prog:TotalRecords = Count#
                  Prog:ShowPercentage = 1 !Show Percentage Figure
      
                  Set(CCTImportFile,0)
      
                  Accept
                      Case Event()
                          Of Event:Timer
                              Loop 25 Times
                                  !Inside Loop
                                  Next(CCTImportFile)
                                  If Error()
                                      Prog:Exit = 1
                                      Break
                                  End ! If Access:ORDERS.Next()
      
                                  Prog:RecordCount += 1
      
                                  ?Prog:UserString{Prop:Text} = 'Records Read: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                                  Do Prog:UpdateScreen
      
                                  Save_MANFPALO_ID = Access:MANFPALO.SaveFile()
                                  Access:MANFPALO.ClearKey(mfp:Field_Key)
                                  mfp:Manufacturer = map:Manufacturer
                                  mfp:Field_Number = map:Field_Number
                                  mfp:Field = Upper(cctimp:CCTReference)
                                  If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                                      !Found
                                      mfp:Description = Upper(cctimp:ServiceActivity)
                                  Else ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                                      !Error
                                      If Access:MANFPALO.PrimeRecord() = Level:Benign
                                          mfp:Manufacturer = map:Manufacturer
                                          mfp:FIeld_Number = map:Field_Number
                                          mfp:Field = Upper(cctimp:CCTReference)
                                          mfp:Description = UPper(cctimp:ServiceActivity)
                                          If Access:MANFPALO.TryInsert() = Level:Benign
                                              !Insert
                                          Else ! If Access:MANFPALO.TryInsert() = Level:Benign
                                              Access:MANFPALO.CancelAutoInc()
                                          End ! If Access:MANFPALO.TryInsert() = Level:Benign
                                      End ! If Access.MANFPALO.PrimeRecord() = Level:Benign
                                  End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                                  Access:MANFPALO.RestoreFile(Save_MANFPALO_ID)
      
                              End ! Loop 25 Times
                          Of Event:CloseWindow
                              Prog:Exit = 1
                              Prog:Cancelled = 1
                              Break
                          Of Event:Accepted
                              If Field() = ?Prog:Cancel
                                  Beep(Beep:SystemQuestion)  ;  Yield()
                                  Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                                 icon:Question,'&Yes|&No',2,2)
                                      Of 1 ! &Yes Button
                                          Prog:Exit = 1
                                          Prog:Cancelled = 1
                                          Break
                                      Of 2 ! &No Button
                                  End!Case Message
                              End ! If Field() = ?ProgressCancel
                      End ! Case Event()
                      If Prog:Exit
                          Break
                      End ! If Prog:Exit
                  End ! Accept
                  Do Prog:ProgressFinished
      
                  Beep(Beep:SystemAsterisk);  Yield()
                  Case Missive('Import Complete.','ServiceBase 3g',|
                                 'midea.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Close(CCTImportFile)
              End ! If Error()
      
          Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
              !Error
              SetPath(tmp:SavePath)
          End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
      BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ImportCCTRefNos, Accepted)
    OF ?Button:LinkedFaultCodes
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:LinkedFaultCodes, Accepted)
      save_map_id = Access:MANFAUPA.SaveFile()
      BrowseRelatedFaultCodes(map:Manufacturer,map:Field_Number,map:UseRelatedJobCode,0)
      Access:MANFAUPA.RestoreFile(save_map_id)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:LinkedFaultCodes, Accepted)
    OF ?map:NotAvailable
      IF ?map:NotAvailable{Prop:Checked} = True
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
      END
      IF ?map:NotAvailable{Prop:Checked} = False
        UNHIDE(?Insert)
        UNHIDE(?Change)
        UNHIDE(?Delete)
      END
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case tmp:DatePicture
      Of 'DD/MM/YY'
          map:DateType = '@d5'
      Of 'DD/MM/YYYY'
          map:DateType = '@d6'
      Of 'MM/DD/YY'
          map:DateType = '@d1'
      Of 'MM/DD/YYYY'
          map:DateType = '@d2'
      Of 'MM/YY'
          map:DateType = '@d13'
      Of 'MM/YYYY'
          map:DateType = '@d14'
      Of 'YY/MM'
          map:DateType = '@d15'
      Of 'YYYY/MM'
          map:DateType = '@d16'
      Of 'YY/MM/DD'
          map:DateType = '@d9'
      Of 'YYYY/MM/DD'
          map:DateType = '@d10'
      Of 'YYMMDD'
          map:DateType = '@d11'
      Of 'YYYYMMDD'
          map:DateType = '@d12'
  End !tmp:DatePicture
  ! Screen Order Check
  tmp:Manufacturer = map:Manufacturer
  tmp:FieldNumber = map:Field_Number
  tmp:ScreenOrder = map:ScreenOrder
  KeyRepair# = map:KeyRepair
  
  save_map_id = Access:MANFAUPA.SaveFile()
  Access:MANFAUPA.Clearkey(map:Field_Number_Key)
  map:Manufacturer = tmp:Manufacturer
  map:Field_Number = 0
  Set(map:Field_Number_Key,map:Field_Number_Key)
  Loop
      If Access:MANFAUPA.Next()
          Break
      End ! If Access:MANFAUPA.Next()
      If map:Manufacturer <> tmp:Manufacturer
          Break
      End ! If map:Manufacturer <> tmp:Manufacturer
      If map:Field_Number = tmp:FieldNumber
          Cycle
      End ! If map:Field_Number = tmp:FieldNumber
  
      If map:ScreenOrder = tmp:ScreenOrder
          Case Missive('The fault code "' & Clip(map:Field_Name) & '" has the same Screen Order Number.','ServiceBase 3g',|
                          'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          tmp:ScreenOrder = 0
          Error# = 1
          Break
      End ! If map:ScreenOrder = tmp:ScreenOrder
  
      If KeyRepair# And map:KeyRepair
          Case Missive('The fault code "' & Clip(map:Field_Name) & '" has already been marked as "Key Repair".','ServiceBase 3g',|
                          'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          KeyRepair# = 0
          Error# = 1
          Break
      End ! If KeyRepair# And map:KeyRepair
  End ! Loop
  Access:MANFAUPA.RestoreFile(save_map_id)
  
  If Error#
      map:ScreenOrder = tmp:ScreenOrder
      map:KeyRepair = KeyRepair#
      Cycle
  End ! If Error#
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?map:Field_Number
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW8.ApplyRange
      BRW8.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(event:accepted,?map:compulsory)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW8.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = map:Manufacturer
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = map:Field_Number
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW16.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = map:Manufacturer
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = map:Field_Number
  GET(SELF.Order.RangeList.List,3)
  Self.Order.RangeList.List.Right = tmp:MainFaultNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW16.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateMANFPALO PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mfp:Record  LIKE(mfp:RECORD),STATIC
QuickWindow          WINDOW('Update the MANFPALO File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Fault Code Lookup'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           TEXT,AT(244,112,124,10),USE(mfp:Manufacturer),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,SINGLE,READONLY
                           CHECK('Not Available'),AT(395,112),USE(mfp:NotAvailable),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Not Available'),TIP('Not Available'),VALUE('1','0')
                           ENTRY(@n2),AT(244,127,64,10),USE(mfp:Field_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           TEXT,AT(244,142,124,10),USE(mfp:Field),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,SINGLE
                           PROMPT('Description'),AT(168,155),USE(?MFP:Description:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(244,155,124,10),USE(mfp:Description),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),UPR,SINGLE
                           CHECK('Restrict Lookup To:'),AT(244,171),USE(mfp:RestrictLookup),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Restrict Lookup To')
                           OPTION('Job Type'),AT(372,183,124,60),USE(mfp:JobTypeAvailability),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Job Type Availability')
                             RADIO('Chargeable Job'),AT(384,195),USE(?Option2:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Warranty Job'),AT(384,211),USE(?Option2:Radio5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Both'),AT(384,227),USE(?Option2:Radio6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           END
                           CHECK('Force Job Fault Code'),AT(244,251),USE(mfp:ForceJobFaultCode),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force Job Fault Code'),TIP('Force Job Fault Code'),VALUE('1','0')
                           GROUP,AT(348,251,167,10),USE(?Group:ForceFaultCode)
                             PROMPT('Job Fault Code Number'),AT(351,251),USE(?mfp:ForceFaultCodeNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             SPIN(@n8),AT(451,251,64,10),USE(mfp:ForceFaultCodeNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Job Fault Code Number'),TIP('Job Fault Code Number'),REQ,UPR,RANGE(1,20),STEP(1)
                           END
                           GROUP,AT(164,264,328,34),USE(?Group:SetPartFaultCode),HIDE
                             CHECK('Set Part Fault Code'),AT(244,263),USE(mfp:SetPartFaultCode),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Job Fault Code'),TIP('Set Job Fault Code'),VALUE('1','0')
                             GROUP,AT(168,272,331,20),USE(?Group:PartFaultCode)
                               BUTTON,AT(472,272),USE(?Lookup:PartFaultCodeValue),FLAT,ICON('lookupp.jpg')
                               PROMPT('Fault Code Number'),AT(168,278),USE(?mfp:SelectPartFaultCode:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               SPIN(@n8),AT(244,278,64,10),USE(mfp:SelectPartFaultCode),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Select Job Fault Code'),TIP('Select Job Fault Code'),REQ,UPR
                               PROMPT('Part Fault Code Value'),AT(316,278),USE(?mfp:PartFaultCodeValue:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(404,278,64,10),USE(mfp:PartFaultCodeValue),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Part Fault Code Value'),TIP('Part Fault Code Value'),REQ,UPR
                             END
                           END
                           OPTION,AT(244,183,124,60),USE(mfp:RestrictLookupType),BOXED,MSG('Restrict Lookup Type')
                             RADIO('Part Used'),AT(256,195),USE(?mfp:RestrictLookupType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Part Correction'),AT(256,211),USE(?mfp:RestrictLookupType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Adjustment'),AT(256,227),USE(?mfp:RestrictLookupType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                           END
                         END
                       END
                       PROMPT('Manufacturer'),AT(168,112),USE(?MFP:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Field Number'),AT(168,127),USE(?MFP:Field_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Field'),AT(168,142),USE(?MFP:Field:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020363'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANFPALO')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mfp:Record,History::mfp:Record)
  SELF.AddHistoryField(?mfp:Manufacturer,2)
  SELF.AddHistoryField(?mfp:NotAvailable,8)
  SELF.AddHistoryField(?mfp:Field_Number,3)
  SELF.AddHistoryField(?mfp:Field,4)
  SELF.AddHistoryField(?mfp:Description,5)
  SELF.AddHistoryField(?mfp:RestrictLookup,6)
  SELF.AddHistoryField(?mfp:JobTypeAvailability,14)
  SELF.AddHistoryField(?mfp:ForceJobFaultCode,9)
  SELF.AddHistoryField(?mfp:ForceFaultCodeNumber,10)
  SELF.AddHistoryField(?mfp:SetPartFaultCode,11)
  SELF.AddHistoryField(?mfp:SelectPartFaultCode,12)
  SELF.AddHistoryField(?mfp:PartFaultCodeValue,13)
  SELF.AddHistoryField(?mfp:RestrictLookupType,7)
  SELF.AddUpdateFile(Access:MANFPALO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFPALO.Open
  Relate:MANFPALO_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFPALO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If map:MainFault
      ?Group:SetPartFaultCode{prop:Hide} = 0
  End ! If map:MainFault
  
  ! Save Window Name
   AddToLog('Window','Open','UpdateMANFPALO')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?mfp:RestrictLookup{Prop:Checked} = True
    ENABLE(?mfp:RestrictLookupType)
  END
  IF ?mfp:RestrictLookup{Prop:Checked} = False
    DISABLE(?mfp:RestrictLookupType)
  END
  IF ?mfp:ForceJobFaultCode{Prop:Checked} = True
    ENABLE(?Group:ForceFaultCode)
  END
  IF ?mfp:ForceJobFaultCode{Prop:Checked} = False
    DISABLE(?Group:ForceFaultCode)
  END
  IF ?mfp:SetPartFaultCode{Prop:Checked} = True
    ENABLE(?Group:PartFaultCode)
  END
  IF ?mfp:SetPartFaultCode{Prop:Checked} = False
    DISABLE(?Group:PartFaultCode)
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFPALO.Close
    Relate:MANFPALO_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMANFPALO')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mfp:Manufacturer = glo:select1
    mfp:Field_Number = glo:select2
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020363'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020363'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020363'&'0')
      ***
    OF ?mfp:RestrictLookup
      IF ?mfp:RestrictLookup{Prop:Checked} = True
        ENABLE(?mfp:RestrictLookupType)
      END
      IF ?mfp:RestrictLookup{Prop:Checked} = False
        DISABLE(?mfp:RestrictLookupType)
      END
      ThisWindow.Reset
    OF ?mfp:ForceJobFaultCode
      IF ?mfp:ForceJobFaultCode{Prop:Checked} = True
        ENABLE(?Group:ForceFaultCode)
      END
      IF ?mfp:ForceJobFaultCode{Prop:Checked} = False
        DISABLE(?Group:ForceFaultCode)
      END
      ThisWindow.Reset
    OF ?mfp:SetPartFaultCode
      IF ?mfp:SetPartFaultCode{Prop:Checked} = True
        ENABLE(?Group:PartFaultCode)
      END
      IF ?mfp:SetPartFaultCode{Prop:Checked} = False
        DISABLE(?Group:PartFaultCode)
      END
      ThisWindow.Reset
    OF ?Lookup:PartFaultCodeValue
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:PartFaultCodeValue, Accepted)
      Request# = GlobalRequest
      GlobalRequest = SelectRecord
      BrowsePartFaultCodeLookup_Alias(mfp:Manufacturer,mfp:SelectPartFaultCode,0,0)
      GlobalRequest = Request#
      If GlobalResponse = RequestCompleted
          mfp:PartFaultCodeValue = mfp_ali:Field
          Display()
      End ! If GlobalResponse = RequestCompleted
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:PartFaultCodeValue, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Manufacturer_Parts_Fault_Coding PROCEDURE      !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Available        STRING(3)
BRW1::View:Browse    VIEW(MANFAUPA)
                       PROJECT(map:Field_Number)
                       PROJECT(map:ScreenOrder)
                       PROJECT(map:Field_Name)
                       PROJECT(map:Field_Type)
                       PROJECT(map:Compulsory)
                       PROJECT(map:Lookup)
                       PROJECT(map:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
map:Field_Number       LIKE(map:Field_Number)         !List box control field - type derived from field
map:Field_Number_NormalFG LONG                        !Normal forground color
map:Field_Number_NormalBG LONG                        !Normal background color
map:Field_Number_SelectedFG LONG                      !Selected forground color
map:Field_Number_SelectedBG LONG                      !Selected background color
map:ScreenOrder        LIKE(map:ScreenOrder)          !List box control field - type derived from field
map:ScreenOrder_NormalFG LONG                         !Normal forground color
map:ScreenOrder_NormalBG LONG                         !Normal background color
map:ScreenOrder_SelectedFG LONG                       !Selected forground color
map:ScreenOrder_SelectedBG LONG                       !Selected background color
map:Field_Name         LIKE(map:Field_Name)           !List box control field - type derived from field
map:Field_Name_NormalFG LONG                          !Normal forground color
map:Field_Name_NormalBG LONG                          !Normal background color
map:Field_Name_SelectedFG LONG                        !Selected forground color
map:Field_Name_SelectedBG LONG                        !Selected background color
map:Field_Type         LIKE(map:Field_Type)           !List box control field - type derived from field
map:Field_Type_NormalFG LONG                          !Normal forground color
map:Field_Type_NormalBG LONG                          !Normal background color
map:Field_Type_SelectedFG LONG                        !Selected forground color
map:Field_Type_SelectedBG LONG                        !Selected background color
map:Compulsory         LIKE(map:Compulsory)           !List box control field - type derived from field
map:Compulsory_NormalFG LONG                          !Normal forground color
map:Compulsory_NormalBG LONG                          !Normal background color
map:Compulsory_SelectedFG LONG                        !Selected forground color
map:Compulsory_SelectedBG LONG                        !Selected background color
map:Lookup             LIKE(map:Lookup)               !List box control field - type derived from field
map:Lookup_NormalFG    LONG                           !Normal forground color
map:Lookup_NormalBG    LONG                           !Normal background color
map:Lookup_SelectedFG  LONG                           !Selected forground color
map:Lookup_SelectedBG  LONG                           !Selected background color
tmp:Available          LIKE(tmp:Available)            !List box control field - type derived from local data
tmp:Available_NormalFG LONG                           !Normal forground color
tmp:Available_NormalBG LONG                           !Normal background color
tmp:Available_SelectedFG LONG                         !Selected forground color
tmp:Available_SelectedBG LONG                         !Selected background color
map:Manufacturer       LIKE(map:Manufacturer)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Fault Code File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(168,110,344,180),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('32R(2)|M*~Field No~L@n2@48R(2)|M*~Screen Order~@n2@80L(2)|M*~Field Name~@s30@80L' &|
   '(2)|M*~Field Type~@s30@27L(2)|M*~Comp~@s3@32L(2)|M*~Lookup~@s3@12L(2)|M*~Avail.~' &|
   '@s3@'),FROM(Queue:Browse:1)
                       BUTTON,AT(448,298),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(168,298),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                       BUTTON,AT(236,298),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                       BUTTON,AT(304,298),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT('Tahoma',8,,FONT:regular),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n2),AT(168,98,64,10),USE(map:Field_Number),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Fault Code Field Number')
                         END
                         TAB('By Screen Order'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n8),AT(168,98,64,10),USE(map:ScreenOrder),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Screen Order')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BOX,AT(440,98,12,10),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Gray)
                       PROMPT('- Not Available'),AT(456,98),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020359'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manufacturer_Parts_Fault_Coding')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFAUPA.Open
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAUPA,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Manufacturer_Parts_Fault_Coding')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,map:ScreenOrderKey)
  BRW1.AddRange(map:Manufacturer,Relate:MANFAUPA,Relate:MANUFACT)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?map:ScreenOrder,map:ScreenOrder,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,map:Field_Number_Key)
  BRW1.AddRange(map:Manufacturer,Relate:MANFAUPA,Relate:MANUFACT)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?map:Field_Number,map:Field_Number,1,BRW1)
  BIND('tmp:Available',tmp:Available)
  BRW1.AddField(map:Field_Number,BRW1.Q.map:Field_Number)
  BRW1.AddField(map:ScreenOrder,BRW1.Q.map:ScreenOrder)
  BRW1.AddField(map:Field_Name,BRW1.Q.map:Field_Name)
  BRW1.AddField(map:Field_Type,BRW1.Q.map:Field_Type)
  BRW1.AddField(map:Compulsory,BRW1.Q.map:Compulsory)
  BRW1.AddField(map:Lookup,BRW1.Q.map:Lookup)
  BRW1.AddField(tmp:Available,BRW1.Q.tmp:Available)
  BRW1.AddField(map:Manufacturer,BRW1.Q.map:Manufacturer)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAUPA.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Manufacturer_Parts_Fault_Coding')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('MANUFACT FAULT CODES - INSERT',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            check_access('MANUFACT FAULT CODES - CHANGE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
        of deleterecord
            check_access('MANUFACT FAULT CODES - DELETE',x")
            if x" = false
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAUPA
    ReturnValue = GlobalResponse
  END
  End!do_udpate# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020359'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020359'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020359'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (map:NotAvailable = 1)
    tmp:Available = 'NO'
  ELSE
    tmp:Available = 'YES'
  END
  PARENT.SetQueueRecord
  SELF.Q.map:Field_Number_NormalFG = -1
  SELF.Q.map:Field_Number_NormalBG = -1
  SELF.Q.map:Field_Number_SelectedFG = -1
  SELF.Q.map:Field_Number_SelectedBG = -1
  SELF.Q.map:ScreenOrder_NormalFG = -1
  SELF.Q.map:ScreenOrder_NormalBG = -1
  SELF.Q.map:ScreenOrder_SelectedFG = -1
  SELF.Q.map:ScreenOrder_SelectedBG = -1
  SELF.Q.map:Field_Name_NormalFG = -1
  SELF.Q.map:Field_Name_NormalBG = -1
  SELF.Q.map:Field_Name_SelectedFG = -1
  SELF.Q.map:Field_Name_SelectedBG = -1
  SELF.Q.map:Field_Type_NormalFG = -1
  SELF.Q.map:Field_Type_NormalBG = -1
  SELF.Q.map:Field_Type_SelectedFG = -1
  SELF.Q.map:Field_Type_SelectedBG = -1
  SELF.Q.map:Compulsory_NormalFG = -1
  SELF.Q.map:Compulsory_NormalBG = -1
  SELF.Q.map:Compulsory_SelectedFG = -1
  SELF.Q.map:Compulsory_SelectedBG = -1
  SELF.Q.map:Lookup_NormalFG = -1
  SELF.Q.map:Lookup_NormalBG = -1
  SELF.Q.map:Lookup_SelectedFG = -1
  SELF.Q.map:Lookup_SelectedBG = -1
  SELF.Q.tmp:Available_NormalFG = -1
  SELF.Q.tmp:Available_NormalBG = -1
  SELF.Q.tmp:Available_SelectedFG = -1
  SELF.Q.tmp:Available_SelectedBG = -1
  SELF.Q.tmp:Available = tmp:Available                !Assign formula result to display queue
   
   
   IF (map:NotAvailable = 1)
     SELF.Q.map:Field_Number_NormalFG = 8421504
     SELF.Q.map:Field_Number_NormalBG = 16777215
     SELF.Q.map:Field_Number_SelectedFG = 16777215
     SELF.Q.map:Field_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.map:Field_Number_NormalFG = -1
     SELF.Q.map:Field_Number_NormalBG = -1
     SELF.Q.map:Field_Number_SelectedFG = -1
     SELF.Q.map:Field_Number_SelectedBG = -1
   END
   IF (map:NotAvailable = 1)
     SELF.Q.map:ScreenOrder_NormalFG = 8421504
     SELF.Q.map:ScreenOrder_NormalBG = 16777215
     SELF.Q.map:ScreenOrder_SelectedFG = 16777215
     SELF.Q.map:ScreenOrder_SelectedBG = 8421504
   ELSE
     SELF.Q.map:ScreenOrder_NormalFG = -1
     SELF.Q.map:ScreenOrder_NormalBG = -1
     SELF.Q.map:ScreenOrder_SelectedFG = -1
     SELF.Q.map:ScreenOrder_SelectedBG = -1
   END
   IF (map:NotAvailable = 1)
     SELF.Q.map:Field_Name_NormalFG = 8421504
     SELF.Q.map:Field_Name_NormalBG = 16777215
     SELF.Q.map:Field_Name_SelectedFG = 16777215
     SELF.Q.map:Field_Name_SelectedBG = 8421504
   ELSE
     SELF.Q.map:Field_Name_NormalFG = -1
     SELF.Q.map:Field_Name_NormalBG = -1
     SELF.Q.map:Field_Name_SelectedFG = -1
     SELF.Q.map:Field_Name_SelectedBG = -1
   END
   IF (map:NotAvailable = 1)
     SELF.Q.map:Field_Type_NormalFG = 8421504
     SELF.Q.map:Field_Type_NormalBG = 16777215
     SELF.Q.map:Field_Type_SelectedFG = 16777215
     SELF.Q.map:Field_Type_SelectedBG = 8421504
   ELSE
     SELF.Q.map:Field_Type_NormalFG = -1
     SELF.Q.map:Field_Type_NormalBG = -1
     SELF.Q.map:Field_Type_SelectedFG = -1
     SELF.Q.map:Field_Type_SelectedBG = -1
   END
   IF (map:NotAvailable = 1)
     SELF.Q.map:Compulsory_NormalFG = 8421504
     SELF.Q.map:Compulsory_NormalBG = 16777215
     SELF.Q.map:Compulsory_SelectedFG = 16777215
     SELF.Q.map:Compulsory_SelectedBG = 8421504
   ELSE
     SELF.Q.map:Compulsory_NormalFG = -1
     SELF.Q.map:Compulsory_NormalBG = -1
     SELF.Q.map:Compulsory_SelectedFG = -1
     SELF.Q.map:Compulsory_SelectedBG = -1
   END
   IF (map:NotAvailable = 1)
     SELF.Q.map:Lookup_NormalFG = 8421504
     SELF.Q.map:Lookup_NormalBG = 16777215
     SELF.Q.map:Lookup_SelectedFG = 16777215
     SELF.Q.map:Lookup_SelectedBG = 8421504
   ELSE
     SELF.Q.map:Lookup_NormalFG = -1
     SELF.Q.map:Lookup_NormalBG = -1
     SELF.Q.map:Lookup_SelectedFG = -1
     SELF.Q.map:Lookup_SelectedBG = -1
   END
   IF (map:NotAvailable = 1)
     SELF.Q.tmp:Available_NormalFG = 8421504
     SELF.Q.tmp:Available_NormalBG = 16777215
     SELF.Q.tmp:Available_SelectedFG = 16777215
     SELF.Q.tmp:Available_SelectedBG = 8421504
   ELSE
     SELF.Q.tmp:Available_NormalFG = -1
     SELF.Q.tmp:Available_NormalBG = -1
     SELF.Q.tmp:Available_SelectedFG = -1
     SELF.Q.tmp:Available_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?MAP:Field_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

UpdateCCTReferenceNumbers PROCEDURE                   !Generated from procedure template - Form

ActionMessage        CSTRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,162,192,94),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Model Number'),AT(248,194),USE(?mcc:ModelNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(308,194,124,10),USE(mcc:ModelNumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Model Number'),TIP('Model Number'),UPR,READONLY
                       PROMPT('CCT Ref No'),AT(248,212),USE(?mcc:CCTReferenceNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(308,212,124,10),USE(mcc:CCTReferenceNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('CCT Reference Number'),TIP('CCT Reference Number'),REQ,UPR
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend CCT Ref No'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020715'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateCCTReferenceNumbers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:MODELCCT)
  Relate:MODELCCT.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MODELCCT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateCCTReferenceNumbers')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MODELCCT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateCCTReferenceNumbers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020715'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020715'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020715'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BrowseCCTReferenceNumbers PROCEDURE (f:ModelNumber)   !Generated from procedure template - Browse

tmp:ModelNumber      STRING(30)
BRW9::View:Browse    VIEW(MODELCCT)
                       PROJECT(mcc:CCTReferenceNumber)
                       PROJECT(mcc:RecordNumber)
                       PROJECT(mcc:ModelNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mcc:CCTReferenceNumber LIKE(mcc:CCTReferenceNumber)   !List box control field - type derived from field
mcc:RecordNumber       LIKE(mcc:RecordNumber)         !Primary key field - type derived from field
mcc:ModelNumber        LIKE(mcc:ModelNumber)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,82,352,248),USE(?Panel5),FILL(09A6A7CH)
                       ENTRY(@s30),AT(272,88,124,10),USE(mcc:CCTReferenceNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('CCT Reference Number')
                       LIST,AT(272,102,144,222),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~CCT Reference Number~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(424,234),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                       BUTTON,AT(424,266),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                       BUTTON,AT(424,298),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse CCT Reference Numbers'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020714'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseCCTReferenceNumbers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:MODELCCT.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  tmp:ModelNumber = f:ModelNumber
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:MODELCCT,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseCCTReferenceNumbers')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.AddSortOrder(,mcc:CCTRefKey)
  BRW9.AddRange(mcc:ModelNumber,tmp:ModelNumber)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?mcc:CCTReferenceNumber,mcc:CCTReferenceNumber,1,BRW9)
  BRW9.AddField(mcc:CCTReferenceNumber,BRW9.Q.mcc:CCTReferenceNumber)
  BRW9.AddField(mcc:RecordNumber,BRW9.Q.mcc:RecordNumber)
  BRW9.AddField(mcc:ModelNumber,BRW9.Q.mcc:ModelNumber)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:MODELCCT.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseCCTReferenceNumbers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  DoUpdate# = True
  
  Case Request
  Of InsertRecord
      If SecurityCheck('CCT REF NO - INSERT')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          DoUpdate# = False
          Access:MODELCCT.CancelAutoInc()
      End ! If SecurityCheck('')
  Of ChangeRecord
      If SecurityCheck('CCT REF NO - CHANGE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          DoUpdate# = False
      End ! If SecurityCheck('')
  Of DeleteRecord
      If SecurityCheck('CCT REF NO - DELETE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          DoUpdate# = False
      End ! If SecurityCheck('')
  End ! Case Request
  If DoUpdate# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateCCTReferenceNumbers
    ReturnValue = GlobalResponse
  END
  End ! If DoUpdate# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020714'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020714'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020714'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateMODELNUM PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?mod:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
BRW7::View:Browse    VIEW(ACCESSOR)
                       PROJECT(acr:Accessory)
                       PROJECT(acr:Model_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
acr:Accessory          LIKE(acr:Accessory)            !List box control field - type derived from field
acr:Model_Number       LIKE(acr:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                       PROJECT(moc:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
moc:Model_Number       LIKE(moc:Model_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(PRODCODE)
                       PROJECT(prd:ProductCode)
                       PROJECT(prd:HandsetReplacementValue)
                       PROJECT(prd:RecordNumber)
                       PROJECT(prd:ModelNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
prd:ProductCode        LIKE(prd:ProductCode)          !List box control field - type derived from field
prd:HandsetReplacementValue LIKE(prd:HandsetReplacementValue) !List box control field - type derived from field
prd:RecordNumber       LIKE(prd:RecordNumber)         !Primary key field - type derived from field
prd:ModelNumber        LIKE(prd:ModelNumber)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(MODPROD)
                       PROJECT(mop:ProductCode)
                       PROJECT(mop:RecordNumber)
                       PROJECT(mop:ModelNumber)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
mop:ProductCode        LIKE(mop:ProductCode)          !List box control field - type derived from field
mop:RecordNumber       LIKE(mop:RecordNumber)         !Primary key field - type derived from field
mop:ModelNumber        LIKE(mop:ModelNumber)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::mod:Record  LIKE(mod:RECORD),STATIC
QuickWindow          WINDOW('Update the MODELNUM File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Model Number'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,212,310),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('General'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manufacturer'),AT(68,70),USE(?MOD:Manufacturer:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,70,124,10),USE(mod:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(68,82),USE(?MOD:Model_Number:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,82,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           CHECK('Specify Unit Type'),AT(144,94),USE(mod:Specify_Unit_Type),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           COMBO(@s30),AT(144,106,124,10),USE(mod:Unit_Type),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Product Type'),AT(68,118),USE(?MOD:Product_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,118,124,10),USE(mod:Product_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Exchange Unit Min Level'),AT(68,128,64,16),USE(?mod:ExchangeUnitMinLevel:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(144,130,64,10),USE(mod:ExchangeUnitMinLevel),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Unit Min Level'),TIP('Exchange Unit Min Level'),UPR
                           PROMPT('Unit Type'),AT(68,106),USE(?mod:unit_type:prompt),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. No Length From'),AT(68,144),USE(?MOD:ESN_Length:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s2),AT(156,144,15,10),USE(mod:ESN_Length_From),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('To'),AT(176,144),USE(?MOD:ESN_Length_To:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s2),AT(192,144,15,10),USE(mod:ESN_Length_To),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           GROUP,AT(68,152,152,20),USE(?Msn_Group)
                             PROMPT('M.S.N. Length From'),AT(68,160),USE(?MOD:MSN_Length:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s2),AT(156,160,15,10),USE(mod:MSN_Length_From),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('To'),AT(176,160),USE(?MOD:MSN_Length_To:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s2),AT(192,160,15,10),USE(mod:MSN_Length_To),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           END
                           GROUP,AT(64,240,200,30),USE(?nokia_group),HIDE
                             PROMPT('Nokia Unit Type'),AT(68,245),USE(?MOD:Nokia_Unit_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(144,245,124,10),USE(mod:Nokia_Unit_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('Sales Model'),AT(68,258),USE(?mod:SalesModel:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(68,366),USE(?Button:BrowseCCTRefNumbers),TRN,FLAT,ICON('brwcctp.jpg')
                             ENTRY(@s30),AT(144,258,124,10),USE(mod:SalesModel),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Sales Model'),TIP('Sales Model'),UPR
                           END
                           CHECK('Exclude Auto Rebook Process'),AT(144,272),USE(mod:ExcludeAutomaticRebookingProcess),TRN,FONT(,,,FONT:bold),MSG('Exclude Automatic Rebooking Process'),TIP('Exclude Automatic Rebooking Process'),VALUE('1','0')
                           CHECK('Handset Warranty 1 Year Only'),AT(144,284),USE(mod:OneYearWarrOnly),FONT(,,,FONT:bold),VALUE('Y','N')
                           CHECK('Active Model'),AT(144,297),USE(mod:Active),FONT(,,,FONT:bold),VALUE('Y','N')
                           PROMPT('Exchange Replacement Value'),AT(68,312,76,18),USE(?mod:ExchReplaceValue:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@n10.2),AT(144,312,64,10),USE(mod:ExchReplaceValue),RIGHT(2),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Replacement Value'),TIP('Exchange Replacement Value'),UPR
                           PROMPT('Replacement Value'),AT(68,180),USE(?mod:ReplacementValue:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(144,180,64,10),USE(mod:ReplacementValue),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Replacement Value'),TIP('Replacement Value'),UPR
                           PROMPT('Loan Selling Price'),AT(68,336),USE(?mod:LoanReplacementValue:Prompt),TRN,RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(144,336,64,10),USE(mod:LoanReplacementValue),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Loan Replacement Value'),TIP('Loan Replacement Value'),UPR
                           PROMPT('RRC Order Cap'),AT(212,326),USE(?mod:RRCOrderCap:Prompt)
                           ENTRY(@n-14),AT(212,336,56,10),USE(mod:RRCOrderCap),RIGHT(1),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Warranty Repair Limit'),AT(68,192,60,22),USE(?mod:WarrantyRepairLimit:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(144,194,64,10),USE(mod:WarrantyRepairLimit),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Warranty Repair Limit'),TIP('Warranty Repair Limit'),UPR
                           CHECK('Excluded From RRC Repair'),AT(144,208),USE(mod:ExcludedRRCRepair),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Excluded From RRC Repair'),TIP('Excluded From RRC Repair'),VALUE('1','0')
                           CHECK('Allow IMEI Characters'),AT(144,220),USE(mod:AllowIMEICharacters),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Allow IMEI Characters'),TIP('Allow IMEI Characters'),VALUE('1','0')
                           CHECK('Use Replenishment Process'),AT(144,232),USE(mod:UseReplenishmentProcess),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Replenishment Process'),TIP('Use Replenishment Process'),VALUE('1','0')
                           PROMPT('Handset'),AT(180,172),USE(?mod:LoanReplacementValue:Prompt:2),TRN,RIGHT,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(440,54,176,146),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('By Accessory'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(444,70,168,96),USE(?List),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('120L(2)~Accessory~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(444,170),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(548,170),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       SHEET,AT(440,202,176,160),USE(?Sheet2:2),COLOR(0D6E7EFH),SPREAD
                         TAB('By Colour'),USE(?ColourTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(444,222,168,106),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('120L(2)|M~Colour~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(444,332),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(548,332),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       SHEET,AT(280,202,156,160),USE(?Sheet5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Product Code'),USE(?Tab5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(284,222,148,106),USE(?List:4),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Product Code~@s30@'),FROM(Queue:Browse:3)
                           BUTTON,AT(284,332),USE(?Insert:4),TRN,FLAT,ICON('insertp.jpg')
                           BUTTON,AT(364,332),USE(?Delete:4),TRN,FLAT,ICON('deletep.jpg')
                         END
                       END
                       SHEET,AT(280,54,156,146),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('By Handset Part Number'),USE(?ProductCodeTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(284,70,148,96),USE(?List:3),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('86L(2)|M~Part No~@s30@56R(2)|M~Replace Value~L@n-14.2@'),FROM(Queue:Browse:2)
                           BUTTON,AT(284,170),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(368,170),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(140,366),USE(?btnFranchisesForExchanges),TRN,FLAT,ICON('franexcp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
TakeAccepted           PROCEDURE(),DERIVED
                     END

BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator IncrementalLocatorClass          !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
BRW21                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Hide_fields     Routine
    If mod:specify_unit_type <> 'YES'
        Disable(?mod:unit_type)
        Disable(?mod:unit_type:prompt)
    Else
        Enable(?mod:unit_type)
        Enable(?mod:unit_type:prompt)
    End
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Model Number'
  OF ChangeRecord
    ActionMessage = 'Changing A Model Number'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020380'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMODELNUM')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mod:Record,History::mod:Record)
  SELF.AddHistoryField(?mod:Manufacturer,2)
  SELF.AddHistoryField(?mod:Model_Number,1)
  SELF.AddHistoryField(?mod:Specify_Unit_Type,3)
  SELF.AddHistoryField(?mod:Unit_Type,5)
  SELF.AddHistoryField(?mod:Product_Type,4)
  SELF.AddHistoryField(?mod:ExchangeUnitMinLevel,11)
  SELF.AddHistoryField(?mod:ESN_Length_From,7)
  SELF.AddHistoryField(?mod:ESN_Length_To,8)
  SELF.AddHistoryField(?mod:MSN_Length_From,9)
  SELF.AddHistoryField(?mod:MSN_Length_To,10)
  SELF.AddHistoryField(?mod:Nokia_Unit_Type,6)
  SELF.AddHistoryField(?mod:SalesModel,18)
  SELF.AddHistoryField(?mod:ExcludeAutomaticRebookingProcess,19)
  SELF.AddHistoryField(?mod:OneYearWarrOnly,20)
  SELF.AddHistoryField(?mod:Active,21)
  SELF.AddHistoryField(?mod:ExchReplaceValue,22)
  SELF.AddHistoryField(?mod:ReplacementValue,12)
  SELF.AddHistoryField(?mod:LoanReplacementValue,14)
  SELF.AddHistoryField(?mod:RRCOrderCap,23)
  SELF.AddHistoryField(?mod:WarrantyRepairLimit,13)
  SELF.AddHistoryField(?mod:ExcludedRRCRepair,15)
  SELF.AddHistoryField(?mod:AllowIMEICharacters,16)
  SELF.AddHistoryField(?mod:UseReplenishmentProcess,17)
  SELF.AddUpdateFile(Access:MODELNUM)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCESSOR.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MODELNUM
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:ACCESSOR,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:MODELCOL,SELF)
  BRW15.Init(?List:3,Queue:Browse:2.ViewPosition,BRW15::View:Browse,Queue:Browse:2,Relate:PRODCODE,SELF)
  BRW21.Init(?List:4,Queue:Browse:3.ViewPosition,BRW21::View:Browse,Queue:Browse:3,Relate:MODPROD,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?ColourTab{prop:Text} = 'By ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
      ?List:2{Prop:Format} = '120L(2)|M~' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & '~@s30@'
  End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  !If ~man:ApplyMSNFormat And man:Use_MSN = 'YES'
  If man:Use_MSN = 'YES'  ! #13554 Incorrect as the format doesn't restrict length (DBH: 26/08/2015)
      ?MSN_Group{prop:Hide} = 0
  Else !man:ApplyMSNFormat And man:Use_MSN = 'YES'
      ?MSN_Group{prop:Hide} = 1
  End !man:ApplyMSNFormat And man:Use_MSN = 'YES'
  
  ! Inserting (DBH 21/02/2008) # 9717 - This tick box is not relevant if selected on the Manufacturer
  If man:UseReplenishmentProcess = 1
      ?mod:UseReplenishmentProcess{prop:Disable} = 1
  End ! If man:UseReplenishmentProcess = 1
  ! End (DBH 21/02/2008) #9717
  
  ! Insert --- Only show if the manufacturer hasn't got the option switched on (DBH: 05/05/2009) #10795
  if (man:ExcludeAutomaticRebookingProcess)
      ?mod:ExcludeAutomaticRebookingProcess{prop:disable} = 1
  end ! if (man:ExcludeAutomaticRebookingProcess)
  ! end --- (DBH: 05/05/2009) #10795
  ! Save Window Name
   AddToLog('Window','Open','UpdateMODELNUM')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,acr:Accesory_Key)
  BRW7.AddRange(acr:Model_Number,Relate:ACCESSOR,Relate:MODELNUM)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,acr:Accessory,1,BRW7)
  BRW7.AddField(acr:Accessory,BRW7.Q.acr:Accessory)
  BRW7.AddField(acr:Model_Number,BRW7.Q.acr:Model_Number)
  BRW10.Q &= Queue:Browse:1
  BRW10.AddSortOrder(,moc:Colour_Key)
  BRW10.AddRange(moc:Model_Number,Relate:MODELCOL,Relate:MODELNUM)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,moc:Colour,1,BRW10)
  BRW10.AddField(moc:Colour,BRW10.Q.moc:Colour)
  BRW10.AddField(moc:Record_Number,BRW10.Q.moc:Record_Number)
  BRW10.AddField(moc:Model_Number,BRW10.Q.moc:Model_Number)
  BRW15.Q &= Queue:Browse:2
  BRW15.AddSortOrder(,prd:ModelProductKey)
  BRW15.AddRange(prd:ModelNumber,Relate:PRODCODE,Relate:MODELNUM)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,prd:ProductCode,1,BRW15)
  BRW15.AddField(prd:ProductCode,BRW15.Q.prd:ProductCode)
  BRW15.AddField(prd:HandsetReplacementValue,BRW15.Q.prd:HandsetReplacementValue)
  BRW15.AddField(prd:RecordNumber,BRW15.Q.prd:RecordNumber)
  BRW15.AddField(prd:ModelNumber,BRW15.Q.prd:ModelNumber)
  BRW21.Q &= Queue:Browse:3
  BRW21.AddSortOrder(,mop:ProductCodeKey)
  BRW21.AddRange(mop:ModelNumber,mod:Model_Number)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,mop:ProductCode,1,BRW21)
  BRW21.AddField(mop:ProductCode,BRW21.Q.mop:ProductCode)
  BRW21.AddField(mop:RecordNumber,BRW21.Q.mop:RecordNumber)
  BRW21.AddField(mop:ModelNumber,BRW21.Q.mop:ModelNumber)
  FDCB8.Init(mod:Unit_Type,?mod:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(uni:Unit_Type_Key)
  FDCB8.AddField(uni:Unit_Type,FDCB8.Q.uni:Unit_Type)
  FDCB8.AddUpdateField(uni:Unit_Type,mod:Unit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  BRW7.AskProcedure = 1
  BRW10.AskProcedure = 2
  BRW15.AskProcedure = 3
  BRW21.AskProcedure = 4
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW15.AskProcedure = 0
      CLEAR(BRW15.AskProcedure, 1)
    END
  END
  BRW21.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW21.AskProcedure = 0
      CLEAR(BRW21.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCESSOR.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMODELNUM')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mod:Manufacturer = glo:select1
    mod:ESN_Length_To = 16
    mod:MSN_Length_To = 10
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_insert# = 1
  If request = Insertrecord
      Case number
          Of 1
              access:accessor.cancelautoinc()
  
              do_insert# = 0
              If mod:model_number = ''
                  Case Missive('You have not entered a Model Number.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If mod:model_number = ''
                  Pick_Accessories_Only
  
                  If Records(glo:Q_Accessory)
                      Loop x$ = 1 To Records(glo:Q_Accessory)
                          Get(glo:Q_Accessory,x$)
                          get(accessor,0)
                          if access:accessor.primerecord() = level:benign
                              acr:accessory    = glo:accessory_pointer
                              acr:model_number = mod:model_number
                              if access:accessor.tryinsert()
                                  access:accessor.cancelautoinc()
                              end
                          end!if access:accessor.primerecord() = level:benign
                      End!Loop x$ = 1 To Records(glo:Q_Accessory)
                  End!If Records(glo:Q_Accessory)
              End !If mod:model_number = ''
          Of 2
              access:modelcol.cancelautoinc()
              do_insert# = 0
              If mod:model_number = ''
                  Case Missive('You have not entered a Model Number.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If mod:model_number = ''
                  Pick_Colour
  
                  If Records(glo:Queue)
                      Loop x$ = 1 To Records(glo:Queue)
                          Get(glo:Queue,x$)
                          get(modelcol,0)
                          if access:modelcol.primerecord() = level:benign
                            moc:model_number  = mod:model_number
                            moc:colour        = glo:pointer
                            if access:modelcol.tryinsert()
                                access:modelcol.cancelautoinc()
                            end
                          end!if access:modelcol.primerecord() = level:benign
                      End!Loop x$ = 1 To Records(glo:Q_Accessory)
                  End!If Records(glo:Q_Accessory)
              End !If mod:model_number = ''
      End!Case number
  End !If request = Insertrecord
  
  If do_insert# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateACCESSOR
      UpdateMODELCOL
      UpdateProductCode
      UpdateNewProductCode
    END
    ReturnValue = GlobalResponse
  END
  End !do_insert# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020380'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020380'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020380'&'0')
      ***
    OF ?mod:Specify_Unit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Specify_Unit_Type, Accepted)
      Do Hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Specify_Unit_Type, Accepted)
    OF ?mod:Unit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Unit_Type, Accepted)
      If ~0{prop:acceptall}
          If mod:specify_unit_type = 'YES'
              access:unittype.clearkey(uni:unit_type_key)
              uni:unit_type = mod:unit_type
              if access:unittype.fetch(uni:unit_type_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browseunittype
                  if globalresponse = requestcompleted
                      mod:unit_type = uni:unit_type
                      display()
                  end
                  globalrequest     = saverequest#
              end !if access:unittype.fetch(uni:unit_type_key)
          End !If mod:specify_unit_type = 'YES'
      End !If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Unit_Type, Accepted)
    OF ?Button:BrowseCCTRefNumbers
      ThisWindow.Update
      BrowseCCTReferenceNumbers(mod:Model_Number)
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?btnFranchisesForExchanges
      ThisWindow.Update
      BrowseModelExchangeAccounts(mod:Manufacturer,mod:Model_Number)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! #12361 Update stock value (Bryan: 31/10/2011)
  Access:STOCK.Clearkey(sto:ExchangeModelKey)
  sto:Location = MainStoreLocation()
  sto:Manufacturer = mod:Manufacturer
  sto:ExchangeModelNumber = mod:Model_Number
  IF (Access:STOCK.Tryfetch(sto:ExchangeModelKey) = Level:Benign)
      IF (sto:ExchangeUnit = 'YES')
          sto:sale_Cost = mod:ExchReplaceValue
          Access:STOCK.TryUpdate()
      END
  END
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Hide_Fields
      If mod:manufacturer = 'NOKIA'
          Unhide(?nokia_group)
      Else!If mod:manufacturer = 'NOKIA'
          Hide(?nokia_group)
      End!If mod:manufacturer = 'NOKIA'
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW7.ResetSort(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB8.TakeAccepted PROCEDURE

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, TakeAccepted, ())
  If mod:specify_unit_type = 'YES'
  
  PARENT.TakeAccepted
  End
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, TakeAccepted, ())


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.DeleteControl=?Delete:2
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.DeleteControl=?Delete:3
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:4
    SELF.DeleteControl=?Delete:4
  END


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

