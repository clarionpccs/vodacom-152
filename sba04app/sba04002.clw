

   MEMBER('sba04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA04002.INC'),ONCE        !Local module procedure declarations
                     END


Insert_Consignment_Note_Number PROCEDURE (f_label_type,f_number1,f_number2,f_consignment_note_number,f_passednumber,func:ShowConsignNo) !Generated from procedure template - Window

FilesOpened          BYTE
consignment_note_number_temp STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Despatch Routine'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Consignment Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Insert the Consignment Note Number to Despatch this job.'),AT(248,180,184,16),USE(?Prompt2),CENTER,FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Consignment Note:'),AT(248,210),USE(?consignment_note_number_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,222,184,10),USE(consignment_note_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(244,258),USE(?Address_Label),SKIP,TRN,FLAT,LEFT,ICON('prnaddp.jpg')
                       BUTTON,AT(308,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(372,258),USE(?Cancel),TRN,FLAT,LEFT,FONT('c',,,),ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020071'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Insert_Consignment_Note_Number')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Insert_Consignment_Note_Number')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Insert_Consignment_Note_Number')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020071'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020071'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020071'&'0')
      ***
    OF ?Address_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
      Set(defaults)
      access:defaults.next()
      glo:select1 = f_number1
      glo:select2 = f_number2
      glo:select3 = f_label_type
      If f_label_type = 'SUB' Or f_label_type = 'TRA'
          Case def:label_printer_type
              Of 'TEC B-440 / B-442'
                  Address_Label_Multiple(f_number1,f_number2,f_label_type,Consignment_Note_Number_Temp,'')
              Of 'TEC B-452'
                  Address_Label_Multiple_B452
          End!Case def:themal_printer_type
      Else!If f_label_type = 'SUB' Or f_label_type = 'TRA'
          Case def:label_printer_type
              Of 'TEC B-440 / B-442'
                  Address_Label('DEL',Consignment_Note_Number_Temp)
              Of 'TEC B-452'
                  Address_Label_B452
          End!Case def:themal_printer_type
      End!If f_label_type = 'SUB' Or f_label_type = 'TRA'
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Address_Label, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If consignment_note_number_temp <> ''
          If (Sub(consignment_note_number_temp,1,Len(Clip(f_passednumber))) <> Clip(f_passednumber))
              If f_passednumber <> ''
                  Case Missive('Warning - Consolidation Error.'&|
                    '<13,10>'&|
                    '<13,10>Expected Consignment No: ' & Clip(f_PassedNumber) & '.'&|
                    '<13,10>Returned Consignment No: ' & Sub(consignment_note_number_temp,1,Len(Clip(f_passednumber))) & '.','ServiceBase 3g',|
                                 'mstop.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
      
                  Case Missive('A unit has already been despatch to this account today, but LABEL G has returned the wrong Consignment Number.'&|
                    '<13,10>DO NOT use the label that has just been produced.'&|
                    '<13,10>This despatch has been CANCELLED!','ServiceBase 3g',|
                                 'mstop.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
                  f_consignment_note_number   = ''
              Else!If f_passednumber <> 0
                  f_consignment_note_number   = consignment_note_number_temp
              End!If f_passednumber <> 0
              Post(event:closewindow)
          Else
              f_consignment_note_number   = consignment_note_number_temp
              Post(event:closewindow)
          End!If consignment_note_number_temp <> f_passednumber
      Else
          Select(?consignment_note_number_temp)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case Missive('Warning ! You have chosen to CANCEL the Despatch Procedure.'&|
        '<13,10>'&|
        '<13,10>Only continue if you are NOT doing to despatch this unit at this time.','ServiceBase 3g',|
                     'mexclam.jpg','\Cancel|/Continue') 
          Of 2 ! Continue Button
              f_consignment_note_number = ''
              Post(event:closewindow)
          Of 1 ! Cancel Button
      End ! Case Missive
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F8Key
              Post(Event:Accepted,?Address_Label)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ALIAS(AltT,AltO)
      If f_passednumber <> '' And func:ShowConsignNo
          consignment_note_number_temp    = f_passednumber
      End !tmp:PassedConsignNo <> '' And tmp:ShowConsignNo
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

DoNotDelete PROCEDURE                                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,RESIZE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('DoNotDelete')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXPCITY.Open
  Relate:EXPGEN.Open
  Relate:EXPLABG.Open
  Relate:JOBSE_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','DoNotDelete')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXPCITY.Close
    Relate:EXPGEN.Close
    Relate:EXPLABG.Close
    Relate:JOBSE_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','DoNotDelete')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

DespatchInvoice PROCEDURE (func:Type)                 !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CourierCost      REAL
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:SubTotal         REAL
tmp:VAT              REAL
tmp:Total            REAL
tmp:LabourVATRate    REAL
tmp:PartsVATRate     REAL
tmp:InvoiceNumber    STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Chargeable Repair'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,84,352,246),USE(?Sheet1),FONT(,,010101H,,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Job Details'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(168,131),USE(?Prompt1:2),FONT(,7,080FFFFH,FONT:bold)
                           STRING(@s15),AT(168,139),USE(job:Account_Number),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Name'),AT(168,151),USE(?Prompt1),FONT(,7,080FFFFH,FONT:bold)
                           STRING(@s30),AT(168,159),USE(sub:Company_Name),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Despatch Courier'),AT(316,145),USE(?job:Courier:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(388,145,124,10),USE(job:Courier),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,COLOR:Black),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           STRING(@s30),AT(168,179),USE(job:Charge_Type),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s30),AT(168,199),USE(job:Repair_Type),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Billing Details'),AT(168,215,136,84),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Labour Cost'),AT(176,239),USE(?Prompt5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Parts Cost'),AT(176,251),USE(?Prompt5:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(224,251),USE(tmp:PartsCost),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             PROMPT('Sub Total'),AT(176,263),USE(?Prompt5:4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(224,263),USE(tmp:SubTotal),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             PROMPT('V.A.T.'),AT(176,275),USE(?Prompt5:5),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(224,275),USE(tmp:VAT),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             PROMPT('Total'),AT(176,287),USE(?Prompt5:6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(224,287),USE(tmp:Total),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             PROMPT('Courier Cost'),AT(176,227),USE(?Prompt5:3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@n14.2),AT(224,227),USE(tmp:CourierCost),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                             STRING(@n14.2),AT(224,239),USE(tmp:LabourCost),RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           END
                           GROUP('Invoice Details'),AT(384,217,128,82),USE(?InvoiceDetails),BOXED,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Invoice Number:'),AT(388,241),USE(?InvoiceNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s8),AT(467,241),USE(job:Invoice_Number),FONT(,,COLOR:White,FONT:bold)
                             STRING(@d6b),AT(448,261),USE(job:Invoice_Date),FONT(,,COLOR:White,FONT:bold)
                             PROMPT('Invoice Date:'),AT(388,261),USE(?InvoiceNumber:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           PROMPT('Charge Type'),AT(168,171),USE(?Prompt1:3),FONT(,7,080FFFFH,FONT:bold)
                           PROMPT('Repair Type'),AT(168,191),USE(?Prompt1:4),FONT(,7,080FFFFH,FONT:bold)
                           BUTTON,AT(448,191),USE(?PrintInvoice),TRN,FLAT,LEFT,ICON('invoicep.jpg')
                         END
                       END
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(640,4,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Chargeable Repair'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020069'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DespatchInvoice')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:COURIER.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  Access:INVOICE.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','DespatchInvoice')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Case func:Type
      Of 0 !Single Despatch
          ?job:Courier{prop:Hide} = 0
          ?job:Courier:Prompt{prop:Hide} = 0
      Of 1 !Multiple Despatch
          ?job:Courier{prop:Hide} = 1
          ?job:Courier:Prompt{prop:Hide} = 1
  End !func:Type
  
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  
  If IsJobInvoiced(job:Invoice_Number)
      If glo:WebJob
          tmp:CourierCost = job:Invoice_Courier_Cost
          tmp:LabourCost  = jobe:InvRRCCLabourCost
          tmp:PartsCost   = jobe:InvRRCCPartsCost
          tmp:SubTotal    = job:Invoice_Courier_Cost + jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost
          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
          inv:Invoice_Number  = job:Invoice_Number
          If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Found
              tmp:VAT         = (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100) + |
                                  (jobe:InvRRCCLabourCost * inv:Vat_Rate_Labour/100) + |
                                  (jobe:InvRRCCPartsCost * inv:Vat_Rate_Parts/100)
          Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = Clarionet:Global.Param2
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              tmp:InvoiceNumber   = Clip(job:Invoice_Number) & '-' & tra:BranchIdentification
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      Else !If glo:WebJob
          tmp:CourierCost = job:Invoice_Courier_Cost
          tmp:LabourCost  = job:Invoice_Labour_Cost
          tmp:PartsCost   = job:Invoice_Parts_Cost
          tmp:SubTotal    = job:Invoice_Courier_Cost + job:Invoice_Labour_Cost + job:Invoice_Parts_Cost
          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
          inv:Invoice_Number  = job:Invoice_Number
          If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Found
              tmp:VAT         = (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100) + |
                                  (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                                  (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100)
          Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              tmp:InvoiceNumber   = Clip(job:Invoice_Number) & '-' & tra:BranchIdentification
          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
      End !If glo:WebJob
  
      ?InvoiceDetails{prop:Hide} = 0
  
  Else !job:Invoice_Number <> 0
      If glo:WebJob
          tmp:CourierCost = job:Courier_Cost
          tmp:LabourCost  = jobe:RRCCLabourCost
          tmp:PartsCost   = jobe:RRCCPartsCost
          tmp:SubTotal    = job:Courier_Cost + jobe:RRCCLabourCost + jobe:RRCCPartsCost
      Else !If glo:WebJob
          tmp:CourierCost = job:Courier_Cost
          tmp:LabourCost  = job:Labour_Cost
          tmp:PartsCost   = job:Parts_Cost
          tmp:SubTotal    = job:Courier_Cost + job:Labour_Cost + job:Parts_Cost
      End !If glo:WebJob
  
      tmp:InvoiceNumber   = 0
  
      If InvoiceSubAccounts(job:Account_Number)
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = sub:Labour_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:LabourVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
  
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = sub:Parts_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:PartsVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
      Else !If InvoiceSubAccounts(job:Account_Number)
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = tra:Labour_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:LabourVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
  
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = tra:Parts_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:PartsVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
  
      End !If InvoiceSubAccounts(job:Account_Number)
  
      If glo:WebJob
          tmp:VAT         = (job:Courier_Cost * tmp:LabourVATRate/100) + |
                                  (jobe:RRCCLabourCost * tmp:LabourVATRate/100) + |
                                  (jobe:RRCCPartsCost * tmp:PartsVATRate/100)
      Else !If glo:WebJob
          tmp:VAT         = (job:Courier_Cost * tmp:LabourVATRate/100) + |
                                  (job:Labour_Cost * tmp:LabourVATRate/100) + |
                                  (job:Parts_Cost * tmp:PartsVATRate/100)
      End !If glo:WebJob
  
      ?InvoiceDetails{prop:Hide} = 1
  End !job:Invoice_Number <> 0
  tmp:Total       = tmp:SubTotal + tmp:VAT
  
  FDCB4.Init(job:Courier,?job:Courier,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(cou:Courier_Key)
  FDCB4.AddField(cou:Courier,FDCB4.Q.cou:Courier)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','DespatchInvoice')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PrintInvoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
    !Do a double check to see if the invoice can still be created
    Error# = 0
    If job:Invoice_Number = ''
        !Because the invoice is created
        !whether it is printed or not. Bring up the message - 3308 (DBH: 26-09-2003)
        Case Missive('Are you sure you want to create an invoice?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
                    If CreateInvoice() = Level:Benign
                    Else !If CreateInvoice() = Level:Benign
                        Error# = 1
                    End !If CreateInvoice() = Level:Benign
                Else !If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
                    Error# = 1
                End !If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
            Of 1 ! No Button
                Error# = 1
        End!Case MessageEx
    End !job:Invoice_Number = ''
    If Error# = 0
        glo:Select1 = job:Invoice_Number
!        IF glo:webjob
!            Vodacom_Delivery_Note_Web('','')
!        ELSE
!            Single_Invoice('','')
!        END
        VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')  ! #11881 New Single Invoice (Bryan: 16/03/2011)
        glo:Select1 = ''
        ThisWindow.Update()
        If job:Invoice_Number <> 0
            ?InvoiceDetails{prop:Hide} = 0
        End !If job:Invoice_Number <> 0
    End !Error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020069'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020069'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020069'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

IsJobAlreadyInBatch  PROCEDURE  (func:JobNumber,func:HeadAccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_mulj_id         USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return IsTheJobAlreadyInBatch(func:JobNumber,func:HeadAccountNumber)
!    Save_mulj_ID = Access:MULDESPJ.SaveFile()
!    Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
!    mulj:JobNumber = func:JobNumber
!    Set(mulj:JobNumberOnlyKey,mulj:JobNumberOnlyKey)
!    Loop
!        If Access:MULDESPJ.NEXT()
!           Break
!        End !If
!        If mulj:JobNumber <> func:JobNumber      |
!            Then Break.  ! End If
!        Access:MULDESP.ClearKey(muld:RecordNumberKey)
!        muld:RecordNumber = mulj:RefNumber
!        If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
!            !Found
!            If muld:HeadAccountNumber = func:HeadAccountNumber
!                Return Level:Fatal
!            End !If muld:HeadAccountNumber = func:HeadAccountNumber
!        Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
!    End !Loop
!    Access:MULDESPJ.RestoreFile(Save_mulj_ID)
!    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchIMEIOk       PROCEDURE  (func:DespatchType,func:IMEINumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Case func:DespatchType
        Of 'JOB'
            If job:ESN <> func:IMEINumber
                Case Missive('The I.M.E.I. Number does not match the selected job.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End !If job:ESN <> tmp:IMEINumber
        Of 'EXC'
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = job:Exchange_Unit_Number
            If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If func:IMEINumber <> xch:ESN
                    Case Missive('The I.M.E.I. Number does not match the seleceted job''s Exchange Unit.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If tmp:IMEINumber <> xch:IMEI
            Else!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                Case Missive('Cannot find the unit attached to this job.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
        Of 'LOA'
            Access:LOAN.ClearKey(loa:Ref_Number_Key)
            loa:Ref_Number = job:Loan_Unit_Number
            If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If loa:ESN <> func:IMEINumber
                    Case Missive('The I.M.E.I. Number does not match the selected job''s Loan Unit.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If loa:ESN <> func:IMEINumber
            Else!If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                Case Missive('Cannot find the unit attached to this job.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End!If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchWebJob       PROCEDURE  (func:Type,func:Courier,func:WayBillNumber,func:Multiple) ! Declare Procedure
tmp:OldSecurityPackNo STRING(30)
save_cou_id          USHORT,AUTO
tmp:PrintDespatch    BYTE(0)
locAuditNotes        STRING(255)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Do not continue if job in use - 3939 (DBH: 29-03-2004)
    If JobInUse(job:Ref_Number,0)
        Return
    End !If JobInUse(job:Ref_Number,0)

    tmp:PrintDespatch = 0

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
        If InvoiceSubAccounts(job:Account_Number)
        End !If InvoiceSubAccounts(job:Account_Number)
            If tra:Use_Sub_Accounts = 'YES'
                If sub:Print_Despatch_Despatch = 'YES'
                    If sub:Despatch_Note_Per_Item = 'YES'
                        tmp:PrintDespatch = 1
                    End !If sub:Despatch_Note_Per_Item = 'YES'
!                            If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
!                                tmp:PrintSummaryDespatch = 1
!                            End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                End !If sub:Print_Despatch_Despatch = 'YES'
                
            Else !If tra:Use_Sub_Accounts = 'YES'
                If tra:Print_Despatch_Despatch = 'YES'
                    If tra:Despatch_Note_Per_Item = 'YES'
                        tmp:PrintDespatch = 1
                    End !If tra:Despatch_Note_Per_Item = 'YES'
!                            If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
!                                tmp:PrintSummaryDespatch = 1
!                            End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                End !If tra:Print_Despatch_Despatch = 'YES'
            End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple

        jobe:Despatched = 'YES'
        Case func:Type
            Of 'LOA'
                GetStatus(901,0,'LOA')
                AddToConsignmentHistory(job:Ref_Number,'RRC','CUSTOMER',job:Loan_Courier,func:WayBillNumber,'LOA')
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)

                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    wob:LoaWayBillNumber = func:WayBillNumber
                    wob:ReadyToDespatch = 0
                    Access:WEBJOB.Update()
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !let see if a new security pack number is entered
                tmp:OldSecurityPackNo = jobe:LoaSecurityPackNo

                !Get the courier to see if it's RAM - L911 (DBH: 11-08-2003)
                save_cou_id = Access:COURIER.SaveFile()
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = func:Courier
                If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Found

                Else !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Error
                End !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

                If cou:Courier_Type = 'RAM'
                    If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                        jobe:LoaSecurityPackNo = glo:Select21
                    Else !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                        jobe:LoaSecurityPackNo = InsertSecurityPackNumber()
                    End !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''

                    Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
                    waj:WayBillNumber = func:WayBillNumber
                    waj:JobNumber     = job:Ref_Number
                    If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                        !Found
                        waj:SecurityPackNumber  = jobe:LoaSecurityPackNo
                        Access:WAYBILLJ.TryUpdate()
                    Else!If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign

                End !If func:Courier = 'RAM'

                Access:COURIER.Restorefile(save_cou_id)

                locAuditNotes         = 'UNIT NO: ' & Clip(job:Loan_Unit_Number) & |
                                    '<13,10>COURIER: ' & Clip(func:Courier)
                If func:WayBillNumber <> ''
                    locAuditNotes       = Clip(locAuditNotes) & '<13,10>WAYBILL NO: ' & Clip(func:WayBillNumber)
                End !If func:WayBillNumber <> 0

                If tmp:OldSecurityPackNo <> jobe:LoaSecurityPackNo
                    locAuditNotes       = Clip(locAuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:LoaSecurityPackNo
                End !If tmp:OldSecurityPackNo <> jobe:SecurityPackNo

                IF (AddToAudit(job:ref_number,'LOA','DESPATCH FROM RRC',locAuditNotes))
                END ! IF

                If tmp:PrintDespatch and ~func:Multiple
                    glo:select1  = job:ref_number
                    If cou:CustomerCollection = 1
                        glo:Select2 = cou:NoOfDespatchNotes
                    Else!If cou:CustomerCollection = 1
                        glo:Select2 = 1
                    End!If cou:CustomerCollection = 1
                    Despatch_Note
                    glo:select1 = ''
                    glo:Select2 = ''
                End!If print_despatch# = 1

                !jobe:Despatched = ''
                !jobe:DespatchType = ''
            Of 'EXC'
                ! Inserting (DBH 10/09/2007) # 8731 - If job is VCP, then the exchange unit goes back to the PUP before the customer
                If job:Who_Booked = 'WEB'
                    GetStatus(468,0,'EXC') ! Despatched To PUP
                    AddToConsignmentHistory(job:Ref_Number,'RRC','PUP',job:Exchange_Courier,func:WayBillNumber,'EXC')
                    !TB13233 - J - 10/12/14 - add exchange location logging
                    UpdateExchLocation(job:Ref_number,Use:user_code,'IN-TRANSIT TO PUP')
                    !END TB13233 - J - 10/12/14 - add exchange location logging
                Else ! If job:Who_Booked = 'WEB'
                ! End (DBH 10/09/2007) #8731
                    GetStatus(901,0,'EXC')
                    AddToConsignmentHistory(job:Ref_Number,'RRC','CUSTOMER',job:Exchange_Courier,func:WayBillNumber,'EXC')
                    !TB13233 - J - 10/12/14 - add exchange location logging
                    UpdateExchLocation(job:Ref_number,Use:user_code,'DESPATCHED')
                    !END TB13233 - J - 10/12/14 - add exchange location logging
                End ! If job:Who_Booked = 'WEB'

                !JOB:exchange_Despatched = Today()
                access:users.clearkey(use:password_key)
                use:password = glo:password
                access:users.fetch(use:password_key)

                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    wob:ExcWayBillNumber = func:WayBillNumber
                    wob:ReadyToDespatch = 0
                    Access:WEBJOB.Update()
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                !let see if a new security pack number is entered
                tmp:OldSecurityPackNo = jobe:ExcSecurityPackNo

                !Get the courier to see if it's RAM - L911 (DBH: 11-08-2003)
                save_cou_id = Access:COURIER.SaveFile()
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = func:Courier
                If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Found

                Else !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Error
                End !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                
                If cou:Courier_Type = 'RAM'
                    If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                        jobe:ExcSecurityPackNo = glo:Select21
                    Else !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                        jobe:ExcSecurityPackNo = InsertSecurityPackNumber()
                    End !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                    Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
                    waj:WayBillNumber = func:WayBillNumber
                    waj:JobNumber     = job:Ref_Number
                    If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                        !Found
                        waj:SecurityPackNumber  = jobe:ExcSecurityPackNo
                        Access:WAYBILLJ.TryUpdate()
                    Else!If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                End !If func:Courier = 'RAM'

                Access:COURIER.Restorefile(save_cou_id)

                locAuditNotes         = 'UNIT NO: ' & Clip(job:Exchange_Unit_Number) & |
                                    '<13,10>COURIER: ' & Clip(func:Courier)
                If func:WayBillNumber <> ''
                    locAuditNotes       = Clip(locAuditNotes) & '<13,10>WAYBILL NO: ' & Clip(func:WayBillNumber)
                End !If func:WayBillNumber <> 0

                If tmp:OldSecurityPackNo <> jobe:ExcSecurityPackNo
                    locAuditNotes       = Clip(locAuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:ExcSecurityPackNo
                End !If tmp:OldSecurityPackNo <> jobe:SecurityPackNo

                IF (AddToAudit(job:ref_number,'EXC','DESPATCH FROM RRC',locAuditNotes))
                END ! IF


                ! Create CIM Xml Message - TrkBs: 6141 (DBH: 26-08-2005)
                If jobe:VSACustomer
                    CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)
                End ! If jobe:VSACustomer
                ! End   - Create CIM Xml Message - TrkBs: 6141 (DBH: 26-08-2005)

                !jobe:Despatched = ''
                !jobe:DespatchType = ''
                If tmp:PrintDespatch And ~func:Multiple
                    glo:select1  = job:ref_number
                    If cou:CustomerCollection = 1
                        glo:Select2 = cou:NoOfDespatchNotes
                    Else!If cou:CustomerCollection = 1
                        glo:Select2 = 1
                    End!If cou:CustomerCollection = 1
                    !Despatch_Note
                    If job:Warranty_Job <> 'YES'
                        Despatch_Note
                    Else !If job:Warranty_Job <> 'YES'
                        Warranty_Delivery_Note_Web
                    End !If job:Warranty_Job <> 'YES'
                    glo:select1 = ''
                    glo:Select2 = ''
                End!If print_despatch# = 1

                ! #12224 Wow, just realised that Exchange units
                ! are not updated when despatched from RRC. How old (Bryan: 15/08/2011)
                Relate:Exchange.Open()
                Relate:Exchhist.Open()
                access:exchange.clearkey(xch:ref_number_key)
                xch:ref_number    = job:exchange_unit_number
                If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                    xch:available    = 'DES'
                    xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                    Access:EXCHANGE.Update()

                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        exh:user = use:user_code
                        exh:status        = 'UNIT DESPATCHED ON JOB: ' & Clip(job:ref_number)
                        Access:EXCHHIST.Insert()
                    end!if access:exchhist.primerecord() = level:benign
                End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                Relate:Exchange.Close()
                Relate:Exchhist.Close()

            OF 'JOB'
                ! Inserting (DBH 07/07/2006) # 7149 - If PUP job, then change status to SENT TO PUP
                If job:Who_Booked = 'WEB'
                    GetStatus(Sub(GETINI('RRC','StatusSentToPUP',,Clip(Path()) & '\SB2KDEF.INI'),1,3),1,'JOB')
                Else ! If job:Who_Booked = 'WEB'
                ! End (DBH 07/07/2006) #7149
                    If job:paid = 'YES' or (job:Chargeable_Job = 'YES' And jobe:RRCCSubTotal = 0)
                        GetStatus(910,1,'JOB') !despatched paid
                    Else!If job:paid = 'YES'
                        GetStatus(905,1,'JOB') !despatched unpaid
                    End!If job:paid = 'YES'
                End ! If job:Who_Booked = 'WEB'

                ! Inserting (DBH 03/07/2006) # 7149 - If PUP job, then despatch back to PUP not customer
                If job:Who_Booked = 'WEB'
                    AddToConsignmentHistory(job:Ref_Number,'RRC','PUP',job:Courier,func:WayBillNumber,'JOB')
                Else ! If job:Who_Booked = 'WEB'
                ! End (DBH 03/07/2006) #7149
                    AddToConsignmentHistory(job:Ref_Number,'RRC','CUSTOMER',job:Courier,func:WayBillNumber,'JOB')
                End ! If job:Who_Booked = 'WEB'

                If job:Loan_Unit_Number <> 0
                    !If Loan unit attached, change status to Awaiting Return Loan
                    GetStatus(812,1,'LOA')

! Changing (DBH 31/07/2007) # 9254 - Don't print the collection note if the loan was from a VCP
!                    glo:Select1 = job:Ref_Number
!                    LoanCollectionNote
!                    glo:Select1 = ''
! to (DBH 31/07/2007) # 9254
                    PrintNote# = 1
                    Access:LOAN.ClearKey(loa:Ref_Number_Key)
                    loa:Ref_Number = job:Loan_Unit_Number
                    If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                        !Found
                        If loa:Location = 'VODACARE COLLECTION POINT'
                            PrintNote# = 0
                        End ! If loa:Location = 'VODACARE COLLECTION POINT'
                    Else ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                    If PrintNote# = 1
                        glo:Select1 = job:Ref_Number
                        LoanCollectionNote
                        glo:Select1 = ''
                    End ! If PrintNote# = 1
! End (DBH 31/07/2007) #9254

                End !If job:Loan_Unit_Number <> 0

                ! Inserting (DBH 03/07/2006) # 7149 - If PUP job, then despatch back to PUP, not Customer
                If job:who_booked = 'WEB'
                    LocationChange(Clip(GETINI('RRC','InTransitToPUPLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                Else ! If job:who_booked = 'WEB'
                ! End (DBH 03/07/2006) #7149
                    LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
                End ! If job:who_booked = 'WEB'
                

                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    wob:JobWayBillNumber = func:WayBillNumber
                    wob:DateJobDespatched = Today()
                    wob:ReadyToDespatch = 0
                    Access:WEBJOB.Update()
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                !let see if a new security pack number is entered
                tmp:OldSecurityPackNo = jobe:JobSecurityPackNo

                !Get the courier to see if it's RAM - L911 (DBH: 11-08-2003)
                save_cou_id = Access:COURIER.SaveFile()
                Access:COURIER.ClearKey(cou:Courier_Key)
                cou:Courier = func:Courier
                If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Found

                Else !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Error
                End !If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

                If cou:Courier_Type = 'RAM'
                    If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                        jobe:JobSecurityPackNo = glo:Select21
                    Else !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                        jobe:JobSecurityPackNo = InsertSecurityPackNumber()
                    End !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                    Access:WAYBILLJ.ClearKey(waj:JobNumberKey)
                    waj:WayBillNumber = func:WayBillNumber
                    waj:JobNumber     = job:Ref_Number
                    If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                        !Found
                        waj:SecurityPackNumber  = jobe:JobSecurityPackNo
                        Access:WAYBILLJ.TryUpdate()
                    Else!If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:WAYBILLJ.TryFetch(waj:JobNumberKey) = Level:Benign
                End !If func:Courier = 'RAM'

                Access:COURIER.Restorefile(save_cou_id)

                locAuditNotes         = '<13,10>COURIER: ' & Clip(func:Courier)
                If func:WayBillNumber <> ''
                    locAuditNotes       = Clip(locAuditNotes) & '<13,10>WAYBILL NO: ' & Clip(func:WayBillNumber)
                End !If func:WayBillNumber <> 0

                If tmp:OldSecurityPackNo <> jobe:JobSecurityPackNo
                    locAuditNotes       = Clip(locAuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:JobSecurityPackNo
                End !If tmp:OldSecurityPackNo <> jobe:SecurityPackNo

                IF (AddToAudit(job:ref_number,'JOB','DESPATCH FROM RRC',locAuditNotes))
                END ! IF

                ! Create CIM Xml Message - TrkBs: 6141 (DBH: 26-08-2005)
                If jobe:VSACustomer
                    CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)
                End ! If jobe:VSACustomer
                ! Create CIM Xml Message - TrkBs: 6141 (DBH: 26-08-2005)


                If tmp:PrintDespatch And ~func:Multiple
                    glo:select1  = job:ref_number
                    If cou:CustomerCollection = 1
                        glo:Select2 = cou:NoOfDespatchNotes
                    Else!If cou:CustomerCollection = 1
                        glo:Select2 = 1
                    End!If cou:CustomerCollection = 1
                    !Despatch_Note
                    If job:Warranty_Job <> 'YES'
                        Despatch_Note
                    Else !If job:Warranty_Job <> 'YES'
                        Warranty_Delivery_Note_Web
                    End !If job:Warranty_Job <> 'YES'
                    glo:select1 = ''
                    glo:Select2 = ''
                End!If print_despatch# = 1

                !jobe:Despatched = ''
                !jobe:DespatchType = ''

        End!Case tmp:despatchType
        Access:JOBS.Update()
        Access:JOBSE.TryUpdate()
    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
InsertSecurityPackNumber PROCEDURE                    !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SecurityPackNumber STRING(30)
window               WINDOW('Security Pack Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(244,164,192,90),USE(?Sheet1),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Security Pack Number'),AT(276,198),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(280,213,124,10),USE(tmp:SecurityPackNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Security Pack Number'),TIP('Security Pack Number'),UPR
                         END
                       END
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(640,4,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert Security Pack Number'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:SecurityPackNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020070'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('InsertSecurityPackNumber')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','InsertSecurityPackNumber')
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','InsertSecurityPackNumber')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020070'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020070'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020070'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:SecurityPackNumber = ''
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

DespatchTheJob       PROCEDURE  (STRING fWaybillNumber) ! Declare Procedure
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
PrintDespatchNote       BYTE,AUTO
SecurityPackNumber      STRING(30)
PrintCollectionNote     BYTE(0)
AuditNotes              STRING(255)
JobNumber               LONG()
WarrantyJob             BYTE(0)
NoOfDespatchNotes       LONG()
retError                BYTE(1)
PrintWaybill            BYTE(0)
WaybillAccount          STRING(30)
WaybillType             STRING(3)
jobSaveError            BYTE(0)
SaveGroup               GROUP,PRE(save)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
CurrentStatus               STRING(30)
StatusEndDate               DATE
StatusEndTime               TIME
CurrentStatusDate           DATE
ExchangeStatus              STRING(30)
ExchangeStatusDate          DATE
LoanStatus                  STRING(30)
LoanStatusDate              DATE
                        END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
        Relate:TRADEACC.Open()
        Relate:SUBTRACC.Open()
        Relate:WAYBILLS.Open()
        Relate:WAYBILLJ.Open()

        CLEAR(SaveGroup)
        
        PrintDespatchNote = 0        
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign)
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number = sub:Main_Account_Number
            IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign)
                IF (tra:Use_Sub_Accounts = 'YES')
                    IF (sub:Print_Despatch_Despatch = 'YES')
                        IF (sub:Despatch_Note_Per_Item = 'YES')
                            PrintDespatchNote = 1
                        END
                    END
                ELSE
                    IF (tra:Print_Despatch_Despatch = 'YES')
                        IF (tra:Despatch_Note_Per_Item = 'YES')
                            PrintDespatchNote = 1
                        END
                    END
                END
                
            END
        END
        
        Relate:TRADEACC.Close()
        Relate:SUBTRACC.Close()
        
        IF (cou:PrintWaybill)
            ! Save details for printing a waybill later
            PrintWaybill = 1
            IF (jobe:Sub_Sub_Account <> '')
                WaybillAccount = jobe:Sub_Sub_Account
                WaybillType = 'SUB'
            ELSE
                WaybillAccount = job:Account_Number
                WaybillType = 'CUS'
            END
        END
        
        IF (cou:Courier_Type = 'RAM')
            SecurityPackNumber = InsertSecurityPackNumber()
        END
        
        ! Double check the job is still not in use
        IF (JobInUse(job:Ref_Number,1))
            retError = 1
        ELSE ! IF (JobInUse(job:Ref_Number,1))
            
            CASE jobe:DespatchType
            OF 'LOA'
                ! Save Details Incase Of Errors
                save:LoanStatus = job:Loan_Status
                save:LoanStatusDate = wob:Loan_Status_Date

                SetTheJobStatus(901,'LOA')
                wob:LoawaybillNumber = fWaybillNumber
                wob:ReadyToDespatch = 0

                IF (cou:Courier_Type = 'RAM')
                    jobe:LoaSecurityPackNo = SecurityPackNumber
                END

                !TB12750 - changing these fields here means the despatch note (called below) does not print properly.
                !jobe:DespatchType = ''
                !jobe:Despatched = ''
            
                IF (Access:JOBS.TryUpdate())
                    jobSaveError = 1
                ELSE ! IF (Access:JOBS.TryUpdate())
                    IF (Access:WEBJOB.TryUpdate())
                        ! Webjob failed, revert jobs
                        job:Loan_Status = save:LoanStatus
                        IF (Access:JOBS.TryUpdate())
                        END

                        jobSaveError = 1
                    ELSE
                        IF (Access:JOBSE.TryUpdate())
                            ! JOBSE failed, revert jobs and webjobs
                            job:Loan_Status = save:LoanStatus
                            IF (Access:JOBS.TryUpdate())
                            END
                            wob:Loan_Status = save:LoanStatus
                            wob:Loan_Status_Date = save:LoanStatusDate
                            IF (Access:WEBJOB.TryUpdate())
                            END


                            jobSaveError = 1
                        ELSE
                            retError = 0
                        ! Save variables because they'll be lost once the printing starts
                            JobNumber = job:Ref_Number
                            IF (job:Warranty_Job = 'YES')
                                WarrantyJob = 1
                            END
                            IF (cou:CustomerCollection = 1)
                                NoOfDespatchNotes = cou:NoOfDespatchNotes
                            ELSE
                                NoOfDespatchNotes = 0
                            END

                            IF (cou:PrintWaybill)
                            
                                Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                                way:WaybillNumber = fWayBillNumber
                                IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                                    way:AccountNumber = Clarionet:Global.Param2
                                    way:WaybillID       = 4
                                    IF (job:Who_Booked = 'WEB') ! PUP Booked
                                        way:WaybillType = 9
                                        way:WaybillID = 21
                                    ELSE
                                        way:WaybillType = 2 ! Do not show in confirmation screen
                                    END
                                    way:FromAccount = Clarionet:Global.Param2
                                    way:ToAccount = job:Account_Number
                                    IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
                                        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                                            waj:WaybillNumber = fWaybillNumber
                                            waj:JobNumber = job:Ref_Number
                                            Relate:LOAN.Open()
                                            Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                            loa:Ref_Number  = job:Loan_Unit_Number
                                            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                !Found
                                                waj:IMEINumber  = loa:ESN
                                            Else ! If Access:LOANS.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                !Error
                                            End !If Access:LOANS.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                            Relate:LOAN.Close()
                                            waj:OrderNumber        = job:Order_Number
                                            waj:SecurityPackNumber = jobe:LoaSecurityPackNo
                                            waj:JobType = jobe:DespatchType
                                            IF (Access:WAYBILLJ.TryUpdate())
                                                Access:WAYBILLJ.CancelAutoInc()
                                            END
                                        END
                                    END
                                END ! IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)

                            END ! IF (cou:PrintWaybill)
                            AddToConsignmentHistory(job:Ref_Number,'RRC','CUSTOMER',job:Loan_Courier,fWaybillNumber,'LOA')
                        
                            AuditNotes = 'UNIT NO: ' & Clip(job:Loan_Unit_Number) & |
                                'COURIER: ' & Clip(cou:Courier)
                            IF (fWaybillNumber <> '')
                                AuditNotes = CLIP(AuditNotes) & '<13,10>WAYBILL NO: ' & Clip(fWaybillNumber)
                            END
                            IF (jobe:LoaSecurityPackNo <> '')
                                AuditNotes = CLIP(AuditNotes) & '<13,10>SECURITY PACK NO: ' & SecurityPackNumber
                            END
                        
                            IF (AddToAudit(job:Ref_Number,'LOA','DESPATCH FROM RRC',CLIP(AuditNotes)))
                            END

                            UpdateTheJobStatusHistory()
                        END
                    END
                END
            
            OF 'EXC'
                ! Save details
                save:ExchangeStatus = job:Exchange_Status
                save:ExchangeStatusDate = wob:Exchange_Status_Date

                IF (job:Who_Booked = 'WEB') ! PUP Booking
                    SetTheJobStatus(468,'EXC') ! Despatched To PUP
                    !TB13233 - J - 10/12/14 - add exchange location logging
                    UpdateExchLocation(job:Ref_number,Use:user_code,'IN-TRANSIT TO PUP')
                    !END TB13233 - J - 10/12/14 - add exchange location logging

                ELSE
                    SetTheJobStatus(901,'EXC') ! Despatched
                    !TB13233 - J - 10/12/14 - add exchange location logging
                    UpdateExchLocation(job:Ref_number,Use:user_code,'DESPATCHED')
                    !END TB13233 - J - 10/12/14 - add exchange location logging

                END
            
                wob:EXCWaybillNumber = fWaybillNumber
                wob:ReadyToDespatch = 0
            
                IF (cou:Courier_Type = 'RAM')
                    jobe:ExcSecurityPackNo = SecurityPackNumber
                END

                !TB12750 - changing these fields here means the despatch note (called below) does not print properly.
                !jobe:DespatchType = ''
                !jobe:Despatched = ''

            
                IF (Access:JOBS.TryUpdate())
                    jobSaveError = 1
                ELSE ! IF (Access:JOBS.TryUpdate())
                    IF (Access:WEBJOB.TryUpdate())
                        ! Webjob failed, revert jobs
                        job:Exchange_Status = save:ExchangeStatus
                        IF (Access:JOBS.TryUpdate())
                        END

                        jobSaveError = 1
                    ELSE
                        IF (Access:JOBSE.TryUpdate())
                            ! Jobse failed, revert jobs and webjobs
                            job:Exchange_Status = save:ExchangeStatus
                            IF (Access:JOBS.TryUpdate())
                            END
                            wob:Exchange_Status = save:ExchangeStatus
                            wob:Exchange_Status_Date = save:ExchangeStatusDate
                            IF (Access:WEBJOB.TryUpdate())
                            END

                            jobSaveError = 1
                        ELSE
                            retError = 0
                        ! Save variables because they'll be lost once the printing starts
                            JobNumber = job:Ref_Number
                            IF (job:Warranty_Job = 'YES')
                                WarrantyJob = 1
                            END
                            IF (cou:CustomerCollection = 1)
                                NoOfDespatchNotes = cou:NoOfDespatchNotes
                            ELSE
                                NoOfDespatchNotes = 0
                            END
                        
                            IF (cou:PrintWaybill)

                                Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                                way:WaybillNumber = fWayBillNumber
                                IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                                    way:AccountNumber = Clarionet:Global.Param2
                                    way:WaybillID       = 3
                                    IF (job:Who_Booked = 'WEB') ! PUP Booked
                                        way:WaybillType = 9
                                        way:WaybillID = 21
                                    ELSE
                                        way:WaybillType = 2 ! Do not show in confirmation screen
                                    END
                                    way:FromAccount = Clarionet:Global.Param2
                                    way:ToAccount = job:Account_Number
                                    IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
                                        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                                            waj:WaybillNumber = fWaybillNumber
                                            waj:JobNumber = job:Ref_Number
                                            Relate:EXCHANGE.Open()
                                            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                            xch:Ref_Number  = job:Exchange_Unit_Number
                                            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                !Found
                                                waj:IMEINumber  = xch:ESN
                                            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                !Error
                                            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                            Relate:EXCHANGE.Close()
                                            waj:OrderNumber        = job:Order_Number
                                            waj:SecurityPackNumber = jobe:ExcSecurityPackNo
                                            waj:JobType = jobe:DespatchType
                                            IF (Access:WAYBILLJ.TryUpdate())
                                                Access:WAYBILLJ.CancelAutoInc()
                                            END
                                        END
                                    END
                                END ! IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                        
                            END
                            AuditNotes = 'UNIT NO: ' & Clip(job:Exchange_Unit_Number) & |
                                '<13,10>COURIER: ' & Clip(cou:Courier)
                            IF (fWaybillNumber <> '')
                                AuditNotes = CLIP(AuditNotes) & '<13,10>WAYBILL NO: ' & Clip(fWaybillNumber)
                            END
                            IF (jobe:ExcSecurityPackNo <> '')
                                AuditNotes = CLIP(AuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:ExcSecurityPackNo
                            END

                            IF (AddToAudit(job:Ref_Number,'EXC','DESPATCH FROM RRC',CLIP(AuditNotes)))
                            END

                            job:Exchange_Courier                   = cou:courier
                            job:Exchange_Consignment_Number        = fWaybillNumber
                            job:Exchange_Despatched                = today()
                            job:Exchange_Despatched_User           = glo:userCode
                            job:Exchange_Despatch_Number           = ''
                            job:ExcService                         = ''
                            Access:jobs.update()
                            
                            UpdateTheJobStatusHistory()
                        
                            IF (job:Who_Booked = 'WEB')
                                AddToConsignmentHistory(job:Ref_Number,'RRC','PUP',job:Exchange_Courier,fWaybillNumber,'EXC')
                            ELSE
                                AddToConsignmentHistory(job:Ref_Number,'RRC','CUSTOMER',job:Exchange_Courier,fWaybillNumber,'EXC')
                            END
                        
! Create CIM Xml Message - TrkBs: 6141 (DBH: 26-08-2005)
                            If jobe:VSACustomer
                                CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)
                            End ! If jobe:VSACustomer
                        ! End   - Create CIM Xml Message - TrkBs: 6141 (DBH: 26-08-2005)

                                        ! #12224 Wow, just realised that Exchange units
                ! are not updated when despatched from RRC. How old (Bryan: 15/08/2011)
                            Relate:Exchange.Open()
                            Relate:Exchhist.Open()
                            access:exchange.clearkey(xch:ref_number_key)
                            xch:ref_number    = job:exchange_unit_number
                            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                                xch:available    = 'DES'
                                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                Access:EXCHANGE.Update()

                                get(exchhist,0)
                                if access:exchhist.primerecord() = level:benign
                                    exh:ref_number   = xch:ref_number
                                    exh:date          = today()
                                    exh:time          = clock()
                                    exh:user = glo:Usercode
                                    exh:status        = 'UNIT DESPATCHED ON JOB: ' & Clip(job:ref_number)
                                    Access:EXCHHIST.Insert()
                                end!if access:exchhist.primerecord() = level:benign
                            End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                            Relate:Exchange.Close()
                            Relate:Exchhist.Close()
                        END
                    END
                END
            
            
            OF 'JOB'
                ! Save details for errors
                save:PreviousStatus = job:PreviousStatus
                save:StatusUser = job:StatusUser
                save:CurrentStatus = job:Current_Status
                save:StatusEndDate = job:Status_End_Date
                save:StatusEndTime = job:Status_End_Time
                save:CurrentStatusDate = wob:Current_Status_Date

                IF (job:Who_Booked = 'WEB') ! PUP Booking
                    SetTheJobStatus(SUB(GETINI('RRC','StatusSentToPUP',,Clip(Path()) & '\SB2KDEF.INI'),1,3),'JOB')
                ELSE
                    IF (job:Paid = 'YES' OR (job:Chargeable_Job = 'YES' AND jobe:RRCCSubTotal = 0))
                        SetTheJobStatus(910,'JOB') ! Despatch Paid
                    ELSE
                        SetTheJobStatus(905,'JOB') ! Despatch Unpaid
                    END
                END
                IF (cou:Courier_Type = 'RAM')
                    jobe:JobSecurityPackNo = SecurityPackNumber
                END
        
                IF (job:Loan_Unit_Number > 0)
            ! Set the loan status to awaiting return
                    SetTheJobStatus(812,'LOA')
            
                    PrintCollectionNote = 1
                    Relate:LOAN.Open()
                    Access:LOAN.ClearKey(loa:Ref_Number_Key)
                    loa:Ref_Number = job:Loan_Unit_Number
                    If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                        !Found
                        If loa:Location = 'VODACARE COLLECTION POINT'
                            PrintCollectionNote = 0
                        End ! If loa:Location = 'VODACARE COLLECTION POINT'
                    Else ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign            
                    Relate:LOAN.Close()
                END
                wob:JobWaybillNumber = fWaybillNumber
                wob:DateJobDespatched = TODAY()
                wob:ReadyToDespatch = 0

                !TB12750 - changing these fields here means the despatch note (called below) does not print properly.
                !jobe:DespatchType = ''
                !jobe:Despatched = ''

        
                IF (Access:JOBS.TryUpdate())
                    jobSaveError = 1
                ELSE ! IF (Access:JOBS.TryUpdate())
                    IF (Access:WEBJOB.TryUpdate())
                        ! Update WEBJOB failed, revert jobs
                        job:PreviousStatus = save:PreviousStatus
                        job:StatusUser       = save:StatusUser
                        job:Current_Status = save:CurrentStatus
                        job:Status_End_Time = save:StatusEndTime
                        job:Status_End_Date = save:StatusEndDate
                        IF (Access:JOBS.TryUpdate())
                        END

                        jobSaveError = 1
                    ELSE
                        IF (Access:JOBSE.TryUpdate())
                            ! Update JOBSE failed, revert jobs and webjob
                            job:PreviousStatus = save:PreviousStatus
                            job:StatusUser       = save:StatusUser
                            job:Current_Status = save:CurrentStatus
                            job:Status_End_Time = save:StatusEndTime
                            job:Status_End_Date = save:StatusEndDate
                            IF (Access:JOBS.TryUpdate())
                            END

                            wob:Current_Status = save:CurrentStatus
                            wob:Current_Status_Date = save:CurrentStatusDate
                            IF (Access:WEBJOB.TryUpdate())
                            END

                            jobSaveError = 1
                        ELSE
                            retError = 0
                        ! Save variables because they'll be lost once the printing starts
                            JobNumber = job:Ref_Number
                            IF (job:Warranty_Job = 'YES')
                                WarrantyJob = 1
                            END
                            IF (cou:CustomerCollection = 1)
                                NoOfDespatchNotes = cou:NoOfDespatchNotes
                            ELSE
                                NoOfDespatchNotes = 0
                            END
                        
                        
                            IF (cou:PrintWaybill)
                                Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                                way:WaybillNumber = fWayBillNumber
                                IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                                    way:AccountNumber = Clarionet:Global.Param2
                                    way:WaybillID       = 2
                                    IF (job:Who_Booked = 'WEB') ! PUP Booked
                                        way:WaybillType = 9
                                        way:WaybillID = 21
                                    ELSE
                                        way:WaybillType = 2 ! Do not show in confirmation screen
                                    END
                                    way:FromAccount = Clarionet:Global.Param2
                                    way:ToAccount = job:Account_Number
                                    IF (Access:WAYBILLS.TryUpdate() = Level:Benign)
                                        IF (Access:WAYBILLJ.PrimeRecord() = Level:Benign)
                                            waj:WaybillNumber = fWaybillNumber
                                            waj:JobNumber = job:Ref_Number
                                            waj:IMEINumber         = job:ESN
                                            waj:OrderNumber        = job:Order_Number
                                            waj:SecurityPackNumber = jobe:JobSecurityPackNo
                                            waj:JobType = jobe:DespatchType
                                            IF (Access:WAYBILLJ.TryUpdate())
                                                Access:WAYBILLJ.CancelAutoInc()
                                            END
                                        END
                                    END
                                END ! IF (Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign)
                        ! Inserting (DBH 03/07/2006) # 7149 - If PUP job, then despatch back to PUP not customer
                                If job:Who_Booked = 'WEB'
                                    AddToConsignmentHistory(job:Ref_Number,'RRC','PUP',job:Courier,way:WaybillNumber,'JOB')
                            
                                Else ! If job:Who_Booked = 'WEB'
                            ! End (DBH 03/07/2006) #7149
                                    AddToConsignmentHistory(job:Ref_Number,'RRC','CUSTOMER',job:Courier,way:WaybillNumber,'JOB')
                            
                                End ! If job:Who_Booked = 'WEB'
                                
                        
                            END ! IF (cou:PrintWaybill)
                    
                            If job:who_booked = 'WEB'
                                LocationChange(Clip(GETINI('RRC','InTransitToPUPLocation',,CLIP(PATH())&'\SB2KDEF.INI')))
                            Else ! If job:who_booked = 'WEB'
                ! End (DBH 03/07/2006) #7149
                                LocationChange(Clip(GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')))
                            End ! If job:who_booked = 'WEB'
                            Access:JOBS.TryUpdate()  ! LocationChange (above) updates the job location as well. Not tidy, but for now, update the job again.

                            AuditNotes = 'COURIER: ' & Clip(cou:Courier)
                            IF (fWaybillNumber <> '')
                                AuditNotes = CLIP(AuditNotes) & '<13,10>WAYBILL NO: ' & Clip(fWaybillNumber)
                            END
                            IF (jobe:JobSecurityPackNo <> '')
                                AuditNotes = CLIP(AuditNotes) & '<13,10>SECURITY PACK NO: ' & SecurityPackNumber
                            END
                        
                            IF (AddToAudit(job:Ref_Number,'JOB','DESPATCH FROM RRC',CLIP(AuditNotes)))
                            END

                            UpdateTheJobStatusHistory()

                            If jobe:VSACustomer
                                CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)
                            End ! If jobe:VSACustomer

                            IF (PrintCollectionNote)
                                glo:Select1 = JobNumber
                                LoanCollectionNote
                                glo:Select1 = ''
                            END
                    
                        END !IF (Access:JOBSE.TryUpdate() = Level:Benign)
                    END !IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                END !IF (Access:JOBS.TryUpdate() = Level:Benign)
        
            END ! CASE
        
            IF (PrintDespatchNote AND retError = 0)
                glo:Select1 = JobNumber
                glo:Select2 = NoOfDespatchNotes
                IF (WarrantyJob)
                    Warranty_Delivery_Note_Web
                ELSE
                    Despatch_Note
                END
                glo:Select1 = ''
                glo:Select2 = ''
            END

            IF (PrintWaybill AND retError = 0)
                FREE(glo:Q_JobNumber)
                glo:Job_Number_Pointer = job:Ref_Number
                ADD(glo:Q_JobNumber)
            
                WaybillDespatch(Clip(Clarionet:Global.Param2),'TRA',|
                    WaybillAccount,WaybillType,|
                    fWaybillNumber,cou:Courier)
            END

            !TB12750 - keep these in place so this can be reprinted
            !!TB12750 - now that the despatch note has printed is the correctt time to blank fields
            !!refetch the jobse record
            !Access:Jobse.clearkey(jobe:RefNumberKey)
            !jobe:RefNumber = JobNumber
            !If access:jobse.fetch(jobe:RefNumberKey) = level:Benign
            !    jobe:DespatchType = ''
            !    jobe:Despatched = ''
            !    Access:jobse.update()
            !END

            Relate:WAYBILLS.Close()
            Relate:WAYBILLJ.Close()
        
        END ! IF (JobInUse(job:Ref_Number,1))

        IF (jobSaveError)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Error! The despatch process failed. '&|
                '|Ensure that no other user is editing the selected job, and try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
        END
        
        RETURN retError
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
