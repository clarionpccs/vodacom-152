

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04001.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
DespatchANC          PROCEDURE  (f_Courier,func:type) ! Declare Procedure
save_webjob_id       USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
! Changing (DBH 24/01/2007) # 8678 - Set the despatch status on completetion
!     if glo:webjob then
!        access:jobse.clearkey(jobe:RefNumberKey)
!        jobe:RefNumber = job:ref_number
!        access:jobse.fetch(jobe:refNumberKey)
!        jobe:despatched = 'REA'
!        jobe:DespatchType = func:type
!
!        save_WEBJOB_id = Access:WEBJOB.SaveFile()
!        Access:WEBJOB.Clearkey(wob:RefNumberKey)
!        wob:RefNumber   = job:Ref_Number
!        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!            !Found
!            wob:ReadyToDespatch = 1
!            Case func:Type
!                Of 'JOB'
!                    wob:DespatchCourier = job:Courier
!                Of 'EXC'
!                    wob:DespatchCourier = job:Exchange_Courier
!                Of 'LOA'
!                    wob:DespatchCourier = job:Loan_Courier
!                Else                                 
!                    wob:DespatchCourier = job:Courier
!            End !Case func:Type
!            Access:WEBJOB.TryUpdate()
!
!        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!            !Error
!        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!        Access:WEBJOB.RestoreFile(save_WEBJOB_id)
!    ELSE !not done over web
!        job:despatched = 'REA'
!        job:Despatch_Type = func:type
!     END
! to (DBH 24/01/2007) # 8678
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
    Access:WEBJOB.ClearKey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Found
    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        !Error
    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

    If glo:WebJob
        jobe:Despatched = 'REA'
        jobe:DespatchType = func:Type

        ! Inserting (DBH 24/01/2007) # 8678 - Save record
        Access:JOBSE.TryUpdate()
        ! End (DBH 24/01/2007) #8678

        wob:ReadyToDespatch = 1
        Case func:Type
        Of 'JOB'
            wob:DespatchCourier = job:Courier
        Of 'EXC'
            wob:DespatchCourier = job:Exchange_Courier
        Of 'LOA'
            wob:DespatchCourier = job:Loan_Courier
        Else
            wob:DespatchCourier = job:Courier
        End ! Case func:Type
        Access:WEBJOB.TryUpdate()
    Else ! If glo:WebJob
        job:Despatched = 'REA'
        job:Despatch_Type = func:Type
    End ! If glo:WebJob

    If func:Type = 'JOB'
        SetDespatchStatus()
    End ! If func:Type = 'JOB'

    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
! End (DBH 24/01/2007) #8678

    job:Current_Courier = f_courier

    Set(defaults)
    Access:defaults.next()
    access:courier.clearkey(cou:Courier_Key)
    cou:courier = f_courier
    if access:courier.tryfetch(cou:Courier_Key) = Level:Benign
        If cou:Courier_Type = 'ANC' Or cou:Courier_Type = 'ROYAL MAIL' Or |
                cou:Courier_Type = 'UPS' Or cou:Courier_Type = 'PARCELINE'
            If func:Type <> 'JOB'
                If job:Workshop <> 'YES'                
                    Return Today()                    
                End!If job:Workshop <> 'YES'
            End!If func:Type <> 'JOB'
            Case Today() % 7
                Of 6 !Saturday
                    If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                        Return Today() + 2
                    End!If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                    If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                        Return Today() + 1
                    End!If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                    IF def:Include_Saturday = 'YES' and def:Include_Sunday <> 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 2
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!IF def:Include_Saturday = 'YES' and def:Include_Sunday <> 'YES'
                    If def:Include_Saturday = 'YES' and def:Include_Sunday = 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 1
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!If def:Include_Saturday = 'YES' and def:Include_Sunday = 'YES'
                Of 0 !Sunday
                    If def:Include_Sunday = 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 1
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    Else!If def:Include_Sunday = 'YES'
                        Return Today() + 1
                    End!If def:Include_Sunday = 'YES'
                Of 5 !Friday
                    If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 3
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                    If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 2
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                    IF def:Include_Saturday = 'YES' 
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 1
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!IF def:Include_Saturday = 'YES' and def:Include_Sunday <> 'YES'
                Else
                    If clock() > cou:Last_Despatch_Time
                        Return Today() + 1
                    Else!If clock() > cou:Last_Despatch_Time
                        Return Today()
                    End!If clock() > cou:Last_Despatch_Time
            End!Case Today() % 7
        End!If cou:CourierType = 'ANC'
    end!if access:courier.tryfetch(cou:CourierKey) = Level:Benign

! Changing (DBH 24/01/2007) # 8678 - Not needed
!    if glo:webjob then
!        access:jobse.update()
!    END
! to (DBH 24/01/2007) # 8678

! End (DBH 24/01/2007) #8678

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
Despatch             PROCEDURE  (func:Type,func:ConsignmentNumber,func:Multiple) ! Declare Procedure
locAuditNotes        STRING(255)
tmp:PrintDespatch    BYTE(0)
tmp:PrintSummaryDespatch BYTE(0)
tmp:OldSecurityPackNo STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!(func:Type,func:ConsignmentNumber,func:Multiple)

    If JobInUse(job:Ref_Number,0)
        Return
    End !If JobInUse(job:Ref_Number,0)

    Case func:Type
        Of 'JOB'
            job:consignment_number  = func:ConsignmentNumber
            Access:COURIER.ClearKey(cou:Courier_Key)
            cou:Courier = job:Courier
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
              !Found one!
            END

            If Today() %7 = 6
                thedate#    = Today() + 2
            Else
                IF cou:Last_Despatch_Time <> '' AND cou:Courier_Type = 'TOTE'
                  IF CLOCK() > cou:Last_Despatch_Time
                    thedate# = TODAY()+1
                    If thedate# %7 = 6
                       thedate# = thedate# + 2
                    End
                  ELSE
                    thedate#    = Today()
                  END
                ELSE
                  thedate#    = Today()
                END
            End!If Today() %7 = 6

            job:date_despatched = thedate#
            job:despatch_number = dbt:Batch_Number
            job:despatched  = 'YES'
            access:users.clearkey(use:password_key)
            use:password    = glo:password
            access:users.fetch(use:password_key)
            job:despatch_user   = use:user_code

            Access:COURIER.ClearKey(cou:Courier_Key)
            cou:Courier = job:Courier
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
!                If cou:CustomerCollection = 1
!                    If cou:NewStatus <> ''
!                        GetStatus(Sub(cou:NewStatus,1,3),1,'JOB')
!                    End!If cou:NewStatus <> ''
!                Else!If cou:CustomerCollection = 1
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        If jobe:WebJob
                            stanum# = Sub(GETINI('RRC','StatusDespatchedToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3)
                            GetStatus(stanum#,0,'JOB')
                            AddToConsignmentHistory(job:Ref_Number,'ARC','RRC',job:Courier,job:Consignment_Number,'JOB')
                        Else
                            AddToConsignmentHistory(job:Ref_Number,'ARC','CUSTOMER',job:Courier,job:Consignment_Number,'JOB')
                            ! Change --- Check if the warranty & chargeable part has been paid (DBH: 01/02/2010) #11254
                            !If job:Paid = 'YES'
                            ! To --- (DBH: 01/02/2010) #11254
                            paid# = 1
                            if (job:chargeable_Job = 'YES' and job:Paid <> 'YES')
                                paid# = 0
                            end ! if (job:chargeable_Job = 'YES')
                            if (paid# = 1 and job:warranty_Job = 'YES' and job:paid_Warranty <> 'YES')
                                paid# = 0
                            end !if (paid# = 1 and job:warranty_Job = 'YES' and job:paid_Warranty <> 'YES')
                            if (paid# = 1)
                            ! end --- (DBH: 01/02/2010) #11254
                                GetStatus(910,1,'JOB') !Despatch Paid
                            Else!If job:Paid = 'YES'
                                GetStatus(905,1,'JOB') !Despatch Unpaid
                            End!If job:Paid = 'YES'

                            If job:Loan_Unit_Number <> 0
                                !If Loan unit attached, change status to Awaiting Return Loan
                                GetStatus(812,1,'LOA')
                            End !If job:Loan_Unit_Number <> 0

                        End !If jobe:WebJob
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                        ! Change --- Check if the warranty & chargeable part has been paid (DBH: 01/02/2010) #11254
                        !If job:Paid = 'YES'
                        ! To --- (DBH: 01/02/2010) #11254
                        paid# = 1
                        if (job:chargeable_Job = 'YES' and job:Paid <> 'YES')
                            paid# = 0
                        end ! if (job:chargeable_Job = 'YES')
                        if (paid# = 1 and job:warranty_Job = 'YES' and job:paid_Warranty <> 'YES')
                            paid# = 0
                        end !if (paid# = 1 and job:warranty_Job = 'YES' and job:paid_Warranty <> 'YES')
                        if (paid# = 1)
                            ! end --- (DBH: 01/02/2010) #11254
                            GetStatus(910,1,'JOB') !Despatch Paid
                        Else!If job:Paid = 'YES'
                            GetStatus(905,1,'JOB') !Despatch Unpaid
                        End!If job:Paid = 'YES'

                        If job:Loan_Unit_Number <> 0
                            !If Loan unit attached, change status to Awaiting Return Loan
                            GetStatus(812,1,'LOA')
                        End !If job:Loan_Unit_Number <> 0

                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                End!If cou:CustomerCollection = 1
            End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

            If def:RemoveWorkshopDespatch = 1
                job:Workshop = 'NO'
                LocationChange('DESPATCHED')

                !if it's not at the RRC, but the job was booked AT the RRC
                !then changed the location back to Intransit To RRC
                If ~glo:WebJob
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        If jobe:WebJob
                            !Remove the engineer for going back to the RRC
                            job:Engineer = ''
                            LocationChange(Clip(GETINI('RRC','InTransitRRC',,CLIP(PATH())&'\SB2KDEF.INI')))
                        End !If jobe:WebJob
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                End !If glo:WebJob

            End!If def:RemoveWorkshopDespatch = 1


            Access:JOBS.Update()



            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                !Check and write security pack number
                tmp:OldSecurityPackNo = jobe:JobSecurityPackNo

                If glo:Select20 = 'SECURITYPACKNO'
                    If glo:Select21 <> ''
                        jobe:JobSecurityPackNo = glo:Select21
                        Access:JOBSE.Update()
                    End !If glo:Select21 <> ''
                End !If glo:Select20 = 'SECURITYPACKNO'
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            locAuditNotes = 'CONSIGNMENT NUMBER: ' & Clip(job:Consignment_number)

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:JobSecurityPackNo <> tmp:OldSecurityPackNo and cou:Courier_Type = 'RAM'
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:JobSecurityPackNo
                End !If jobe:JobSecurityPackNo <> ''
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            IF (AddToAudit(job:Ref_Number,'JOB','JOB DESPATCHED VIA ' & Clip(job:courier),locAuditNotes))
            END ! IF

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)


            !Return Location
            access:locinter.clearkey(loi:location_key)
            loi:location    = job:location
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces  = 'YES'
                    loi:current_spaces  += 1
                    loi:location_available  = 'YES'
                    Access:LOCINTER.Update()
                End!If loi:allocate_spaces  = 'YES'
            End!If access:locinter.fetch(loi:location_key) = Level:Benign

            tmp:PrintDespatch = 0
!            If ~func:Multiple
            If InvoiceSubAccounts(job:Account_Number)
            End !If InvoiceSubAccounts(job:Account_Number)
                If tra:Use_Sub_Accounts = 'YES'
                    If sub:Print_Despatch_Despatch = 'YES'
                        If sub:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If sub:Despatch_Note_Per_Item = 'YES'
                        If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If sub:Print_Despatch_Despatch = 'YES'

                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:Print_Despatch_Despatch = 'YES'
                        If tra:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If tra:Despatch_Note_Per_Item = 'YES'
                        If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If tra:Print_Despatch_Despatch = 'YES'
                End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple

            If tmp:PrintDespatch And ~func:Multiple
                glo:select1  = job:ref_number
                If cou:CustomerCollection = 1
                    glo:Select2 = cou:NoOfDespatchNotes
                Else!If cou:CustomerCollection = 1
                    glo:Select2 = 1
                End!If cou:CustomerCollection = 1
                If job:Warranty_Job <> 'YES'
                    Despatch_Note
                Else !If job:Warranty_Job <> 'YES'
                    Warranty_Delivery_Note
                End !If job:Warranty_Job <> 'YES'
                glo:select1 = ''
                glo:Select2 = ''
            End!If print_despatch# = 1

            If job:Loan_Unit_Number <> 0
                !Always print a Loan Collection Note if there is a loan attached (apparantly)
                !Let's see how long it will be until this has to change
                If ~jobe:WebJob
                    glo:Select1 = job:Ref_Number
                    LoanCollectionNote
                    glo:Select1 = ''
                End !If ~jobe:WebJob
            End !If job:Loan_Unit_Number <> 0

            If tmp:PrintSummaryDespatch
                glo:select1  = job:Account_Number
                glo:select2  = func:ConsignmentNumber
                glo:select3  = dbt:batch_number
                glo:select4  = job:Courier
                SingleSummaryDespatchNote(job:Ref_Number,func:Type)
                glo:Select1 = ''
                glo:Select2 = ''
                glo:Select3 = ''
                glo:Select4 = ''
            End !If tmp:PrintSummaryDespatch
        Of 'EXC'
            job:exchange_consignment_number    = func:ConsignmentNumber
            job:exchange_despatched    = Today()
            job:exchange_despatch_number    = dbt:batch_number
            job:despatched      = ''
            access:users.clearkey(use:password_key)
            use:password        = glo:password
            access:users.tryfetch(use:password_key)
            job:exchange_despatched_user    = use:user_code
            If job:workshop    <> 'YES' and job:third_party_site = ''
                GetStatus(116,1,'JOB')
            End!If job:workshop    <> 'YES'
            
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                !Check and write security pack number
                tmp:OldSecurityPackNo = jobe:ExcSecurityPackNo

                If glo:Select20 = 'SECURITYPACKNO'
                    If glo:Select21 <> ''
                        jobe:ExcSecurityPackNo = glo:Select21
                        Access:JOBSE.Update()
                    End !If glo:Select21 <> ''
                End !If glo:Select20 = 'SECURITYPACKNO'
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


            !If Not being despatched by RRC, but job WAS booked by RRC,
            !then exchange unit needs to go back to RRC
            !Need to fill in waybill field, and change to default status
            If ~glo:WebJob and jobe:WebJob
                GetStatus(Sub(GETINI('RRC','ExchangeStatusDespatchToRRC',,CLIP(PATH())&'\SB2KDEF.INI'),1,3),1,'EXC')
                !TB13233 - J - 10/12/14 - add exchange location logging
                UpdateExchLocation(job:Ref_number,Use:user_code,'IN-TRANSIT TO RRC')
                !END TB13233 - J - 10/12/14 - add exchange location logging

                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                    wob:ExcWayBillNumber    = func:ConsignmentNumber
                    Access:WEBJOB.Update()
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                AddToConsignmentHistory(job:Ref_Number,'ARC','RRC',job:Exchange_Courier,func:ConsignmentNumber,'EXC')
            Else !If ~glo:WebJob and jobe:WebJob
                GetStatus(901,1,'EXC')
                AddToConsignmentHistory(job:Ref_Number,'ARC','CUSTOMER',job:Exchange_Courier,func:ConsignmentNumber,'EXC')
                !TB13233 - J - 10/12/14 - add exchange location logging
                UpdateExchLocation(job:Ref_number,Use:user_code,'DESPATCHED')
                !END TB13233 - J - 10/12/14 - add exchange location logging
            End !If ~glo:WebJob and jobe:WebJob

            Access:JOBS.Update()
            locAuditNotes = 'CONSIGNMENT NOTE NUMBER: ' & Clip(job:exchange_consignment_number)

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:ExcSecurityPackNo <> '' and cou:Courier_Type = 'RAM'
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:ExcSecurityPackNo
                End !If jobe:JobSecurityPackNo <> ''
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            IF (AddToAudit(job:Ref_Number,'EXC','EXCHANGE UNIT DESPATCHED VIA ' & CLip(job:exchange_courier),locAuditNotes))
            END ! IF

            If ~jobe:WebJob AND jobe:VSACustomer
                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                CID_XML(job:Mobile_Number,wob:HeadAccountNumber,2)
            End ! If ~jobe:WebJob AND jobe:VSACustomer


            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number    = job:exchange_unit_number
            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                xch:available    = 'DES'
                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                Access:EXCHANGE.Update()

                get(exchhist,0)
                if access:exchhist.primerecord() = level:benign
                    exh:ref_number   = xch:ref_number
                    exh:date          = today()
                    exh:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    exh:user = use:user_code
                    exh:status        = 'UNIT DESPATCHED ON JOB: ' & Clip(job:ref_number)
                    Access:EXCHHIST.Insert()
                end!if access:exchhist.primerecord() = level:benign
            End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign

            tmp:PrintDespatch = 0
!            If ~func:Multiple
            If InvoiceSubAccounts(job:Account_Number)

            End !If InvoiceSubAccounts(job:Account_Number)

                If tra:Use_Sub_Accounts = 'YES'
                    If sub:Print_Despatch_Despatch = 'YES'
                        If sub:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If sub:Despatch_Note_Per_Item = 'YES'
                        If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If sub:Print_Despatch_Despatch = 'YES'

                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:Print_Despatch_Despatch = 'YES'
                        If tra:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If tra:Despatch_Note_Per_Item = 'YES'
                        If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If tra:Print_Despatch_Despatch = 'YES'
                End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple
            If tmp:PrintDespatch And ~func:Multiple
                glo:select1  = job:ref_number
                If job:Warranty_Job <> 'YES'
                    Despatch_Note
                Else !If job:Warranty_Job <> 'YES'
                    Warranty_Delivery_Note
                End !If job:Warranty_Job <> 'YES'

                glo:select1  = ''
            End!If print_despatch# = 1

            If tmp:PrintSummaryDespatch
                glo:select1  = job:Account_Number
                glo:select2  = func:ConsignmentNumber
                glo:select3  = dbt:batch_number
                glo:select4  = job:Exchange_Courier
                SingleSummaryDespatchNote(job:Ref_Number,func:Type)
                glo:Select1 = ''
                glo:Select2 = ''
                glo:Select3 = ''
                glo:Select4 = ''
            End !If tmp:PrintSummaryDespatch

        Of 'LOA'
            job:loan_consignment_number    = func:ConsignmentNumber
            job:loan_despatched    = Today()
            job:loan_despatch_number    = dbt:batch_number
            job:despatched    = ''
            access:users.clearkey(use:password_key)
            use:password    = glo:password
            access:users.tryfetch(use:password_key)
            job:loan_despatched_user    = use:user_code
            If job:workshop <> 'YES' and job:third_party_site    = ''
                GetStatus(117,1,'JOB')
            End!If job:workshop <> 'YES' and job:third_party_site    = ''
            GetStatus(901,1,'LOA')

            AddToConsignmentHistory(job:Ref_Number,'ARC','CUSTOMER',job:Loan_Courier,func:ConsignmentNumber,'LOA')

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                !Check and write security pack number
                tmp:OldSecurityPackNo = jobe:LoaSecurityPackNo

                If glo:Select20 = 'SECURITYPACKNO'
                    If glo:Select21 <> ''
                        jobe:LoaSecurityPackNo = glo:Select21
                        Access:JOBSE.Update()
                    End !If glo:Select21 <> ''
                End !If glo:Select20 = 'SECURITYPACKNO'
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            access:jobs.update()

            locAuditNotes         = 'CONSIGNMENT NOTE NUMBER: ' & Clip(job:loan_consignment_number)
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                If jobe:LoaSecurityPackNo <> tmp:OldSecurityPackNo and cou:Courier_Type = 'RAM'
                    locAuditNotes   = Clip(locAuditNotes) & '<13,10>SECURITY PACK NO: ' & jobe:LoaSecurityPackNo
                End !If jobe:JobSecurityPackNo <> ''
            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

            IF (AddToAudit(job:Ref_Number,'LOA','LOAN UNIT DESPATCHED VIA ' & CLip(job:loan_courier),locAuditNotes))
            END ! IF


            access:loan.clearkey(loa:ref_number_key)
            loa:ref_number    = job:loan_unit_number
            If access:loan.tryfetch(loa:ref_number_key)    = Level:Benign
                loa:available    = 'DES'
                !Paul 02/06/2009 Log No 10684
                loa:StatusChangeDate = today()
                access:loan.update()
                get(loanhist,0)
                if access:loanhist.primerecord() = level:benign
                    loh:ref_number    = loa:ref_number
                    loh:date          = today()
                    loh:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    loh:user = use:user_code
                    loh:status        = 'UNIT DESPATCHED ON JOB: ' & CLip(job:ref_number)
                    access:loanhist.insert()
                end!if access:loanhist.primerecord() = level:benign

            End!    If access:loan.tryfetch(loa:ref_number_key)    = Level:Benign

            tmp:PrintDespatch = 0
!            If ~func:Multiple
            If InvoiceSubAccounts(job:Account_Number)

            End !If InvoiceSubAccounts(job:Account_Number)

                If tra:Use_Sub_Accounts = 'YES'
                    If sub:Print_Despatch_Despatch = 'YES'
                        If sub:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If sub:Despatch_Note_Per_Item = 'YES'
                        If sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If sub:Print_Despatch_Despatch = 'YES'

                Else !If tra:Use_Sub_Accounts = 'YES'
                    If tra:Print_Despatch_Despatch = 'YES'
                        If tra:Despatch_Note_Per_Item = 'YES'
                            tmp:PrintDespatch = 1
                        End !If tra:Despatch_Note_Per_Item = 'YES'
                        If tra:Summary_Despatch_Notes = 'YES' And tra:IndividualSummary
                            tmp:PrintSummaryDespatch = 1
                        End !sub:Summary_Despatch_Notes = 'YES' And sub:IndividualSummary
                    End !If tra:Print_Despatch_Despatch = 'YES'
                End !If tra:Use_Sub_Accounts = 'YES'
!            End !If ~func:Multiple
            If tmp:PrintDespatch And ~func:Multiple
                glo:select1  = job:ref_number
                Despatch_Note
                glo:select1  = ''
            End!If print_despatch# = 1

            If tmp:PrintSummaryDespatch
                glo:select1  = job:Account_Number
                glo:select2  = func:ConsignmentNumber
                glo:select3  = dbt:batch_number
                glo:select4  = job:Loan_Courier
                SingleSummaryDespatchNote(job:Ref_Number,func:Type)
                glo:Select1 = ''
                glo:Select2 = ''
                glo:Select3 = ''
                glo:Select4 = ''
            End !If tmp:PrintSummaryDespatch

    End !Case func:Type
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchAtClosing    PROCEDURE  (func:FailedAssessment) ! Declare Procedure
save_webjob_id       USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If job:despatched = 'YES'
        !Job Already Ready To Despatch
        Return Level:Benign
    Else !job:despatched = 'YES'
        If ToBeLoaned() = 1
            restock# = 0
            If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                Case Missive('A Loan Unit has been issued to this job but the Initial Transit Type has been setup so that an Exchane Unit is required.'&|
                  '<13,10>Do you wish to continue and mark this unit for DESPATCH back to the customer, or do you want to RESTOCK it?','ServiceBase 3g',|
                               'mquest.jpg','\Restock|/Despatch') 
                    Of 2 ! Despatch Button
                    Of 1 ! Restock Button
                        ForceDespatch()
                        restock# = 1
                End ! Case Missive
            End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
            If restock# = 0
                IF Clip(GETINI('DESPATCH','DoNotUseLoanAuthTable',,CLIP(PATH())&'\SB2KDEF.INI'))
! Changing (DBH 24/01/2007) # 8678 - Set despatch status based on account
!                    If glo:WebJob
!                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
!                        jobe:RefNumber  = job:Ref_Number
!                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                            !Found
!                            jobe:Despatched = 'REA'
!                            jobe:DespatchType = 'JOB'
!                            If Access:JOBSE.Tryupdate()
!                            End !If Access:JOBSE.Tryupdate()
!                            save_WEBJOB_id = Access:WEBJOB.SaveFile()
!                            Access:WEBJOB.Clearkey(wob:RefNumberKey)
!                            wob:RefNumber   = job:Ref_Number
!                            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                                !Found
!                                wob:ReadyToDespatch = 1
!                                wob:DespatchCourier = job:Courier
!                                Access:WEBJOB.TryUpdate()
!                            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                                !Error
!                            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                            Access:WEBJOB.RestoreFile(save_WEBJOB_id)
!                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                            !Error
!                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
!                    Else !If glo:WebJob
!                        job:despatched  = 'REA'
!                        job:Current_Courier = job:Courier
!                    End !If glo:WebJob
! to (DBH 24/01/2007) # 8678
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                    Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job:Ref_Number
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Found
                    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                    If glo:WebJob
                        jobe:Despatched = 'REA'
                        jobe:DespatchType = 'JOB'
                        Access:JOBSE.Tryupdate()

                        wob:ReadyToDespatch = 1
                        wob:DespatchCourier = job:Courier
                        Access:WEBJOB.TryUpdate()
                    Else ! If glo:WebJob
                        job:Despatched = 'REA'
                        job:Current_Courier = job:Courier
                    End ! If glo:WebJob

                    SetDespatchStatus()

                    Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)

! End (DBH 24/01/2007) #8678
                ELSE
                  job:despatched  = 'LAT'
                END
                job:despatch_Type = 'JOB'
                
            End!If restock# = 0
        Else!If ToBeLoaned() = 1
            If ToBeExchanged() And func:FailedAssessment = 0
                ForceDespatch()
            Else!If ToBeExchanged()
                If glo:WebJob
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        jobe:Despatched = 'REA'
                        jobe:DespatchType = 'JOB'
                        If Access:JOBSE.Tryupdate()
                        End !If Access:JOBSE.Tryupdate()
                        save_WEBJOB_id = Access:WEBJOB.SaveFile()
                        Access:WEBJOB.Clearkey(wob:RefNumberKey)
                        wob:RefNumber   = job:Ref_Number
                        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Found
                            wob:ReadyToDespatch = 1
                            wob:DespatchCourier = job:Courier
                            Access:WEBJOB.TryUpdate()

                            ! Inserting (DBH 24/01/2007) # 8678 - Set despatch status based on account settings
                            SetDespatchStatus()
                            ! End (DBH 24/01/2007) #8678

                        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Error
                        End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        Access:WEBJOB.RestoreFile(save_WEBJOB_id)

                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                Else !If glo:WebJob
                    job:date_despatched = DespatchANC(job:courier,'JOB')
                End !If glo:WebJob
                
                access:courier.clearkey(cou:courier_key)
                cou:courier = job:courier
                if access:courier.tryfetch(cou:courier_key) = Level:benign
                    If cou:despatchclose = 'YES'
                        Return Level:Fatal
                    Else!If cou:despatchclose = 'YES'
                        Return Level:Benign
                    End!If cou:despatchclose = 'YES'
                End!if access:courier.tryfetch(cou:courier_key) = Level:benign
            End!If ToBeExchanged()
        End!If ToBeLoaned() = 1

    End !job:despatched = 'YES'
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchSingle       PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:PassedConsignNo  STRING(30)
tmp:ConsignNo        STRING(30)
tmp:AccountNumber    STRING(30)
tmp:OldConsignNo     STRING(30)
sav:Path             STRING(255)
tmp:LabelError       STRING(255)
Account_Number2_Temp STRING(30)
tmp:Today            DATE
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:AUDIT.Open
   Relate:COURIER.Open
   Relate:DESBATCH.Open
   Relate:EXCHANGE.Open
   Relate:EXCHHIST.Open
   Relate:JOBS.Open
   Relate:JOBNOTES.Open
   Relate:LOAN.Open
   Relate:LOANHIST.Open
   Relate:LOCINTER.Open
   Relate:STATUS.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:USERS.Open
   Relate:JOBS_ALIAS.Open
   Relate:INVOICE.Open
   Relate:JOBPAYMT_ALIAS.Open
   Relate:VATCODE.Open
   Relate:WEBJOB.Open
   Relate:WAYBILLJ.Open
   Relate:WAYBCONF.Open
    cont# = 1
    Error# = 0
    !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',Entered Despatch Single code',clip(path())&'\WaybconfLog.csv')

    If JobInUse(job:Ref_Number,0)
        !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',Exiting because job is in use',clip(path())&'\WaybconfLog.csv')
        Return
    End !If JobInUse(job:Ref_Number,0)


    PrintWayBill# = 0
    If InvoiceSubAccounts(job:Account_Number)
        If job:Chargeable_Job = 'YES' and sub:Stop_Account = 'YES'
            Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '. '&|
              '<13,10>'&|
              '<13,10>It''s Trade Account is "on stop".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            Error# = 1
        End !If job:Chargeable_Job = 'YES' and sub:Stop_Account = 'YES'
        !Does this account required auto/manual invoice created?
        If sub:InvoiceAtDespatch and job:Despatch_Type = 'JOB'
            !Is it manual or automatic?
            Case sub:InvoiceType
                Of 0 !Manual
                    If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                        DespatchInvoice(0)
                    End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                Of 1 !Automatic
                    If job:Invoice_Number = ''
                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                            PrintInvoice# = 0
                            If job:Invoice_Number = ''
                                If CreateInvoice() = Level:Benign
                                    PrintInvoice# = 1
                                End !If CreateInvoice() = Level:Benign
                            Else !If job:Invoice_Number = ''
                                PrintInvoice# = 1
                            End !If job:Invoice_Number = ''
                            If PrintInvoice#
                                glo:Select1 = job:Invoice_Number
                                !Single_Invoice('','')
                                VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
                                glo:Select1 = ''
                            End !If PrintInvoice#
                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                    End !If job:Invoice_Number = ''
            End !Case sub:InvoiceType
        End !If sub:InvoiceAtDespatch

    Else !If InvoiceSubAccounts(job:Account_Number)
        If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
            Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '. '&|
              '<13,10>'&|
              '<13,10>It''s Trade Account is "on stop".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            Error# = 1
        End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
        !Does this account required auto/manual invoice created?
        If tra:InvoiceAtDespatch and job:Despatch_Type = 'JOB'
            !Is it manual or automatic?
            Case tra:InvoiceType
                Of 0 !Manual
                    If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                        DespatchInvoice(0)
                    End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                Of 1 !Automatic
                    If job:Invoice_Number = ''
                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                            PrintInvoice# = 0
                            If job:Invoice_Number = ''
                                If CreateInvoice() = Level:Benign
                                    PrintInvoice# = 1
                                End !If CreateInvoice() = Level:Benign
                            Else !If job:Invoice_Number = ''
                                PrintInvoice# = 1
                            End !If job:Invoice_Number = ''
                            If PrintInvoice#
                                glo:Select1 = job:Invoice_Number
                                !Single_Invoice('','')
                                VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
                                glo:Select1 = ''
                            End !If PrintInvoice#
                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                    End !If job:Invoice_Number = ''
            End !Case sub:InvoiceType
        End !If sub:InvoiceAtDespatch
    End !If InvoiceSubAccounts(job:Account_Number)

    !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',After deciding which invoice can be printed. Error is '&clip(Error#),clip(path())&'\WaybconfLog.csv')

    Access:COURIER.Clearkey(cou:Courier_Key)
    Case job:Despatch_Type
        Of 'JOB'
            cou:Courier = job:Courier
        Of 'EXC'
            cou:Courier = job:Exchange_Courier
        Of 'LOA'
            cou:Courier = job:Loan_Courier
    End !Case job:Despatch_Type
    If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Found
    Else! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
    

    If Error# = 0

        If job:Chargeable_Job = 'YES' and job:Despatch_Type = 'JOB'
            If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                If CheckPaid() = Level:Benign
                    glo:Select1 = job:Ref_Number
                    Browse_Payments
                    glo:Select1 = ''
                End !If CheckPaid() = Level:Benign
            End !If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        End !If job:Chargeable_Job = 'YES'

        If cou:PrintWaybill
            tmp:PassedConsignNo = NextWaybillNumber()
            !Do not allow Despatch to carry on if error - 3432 (DBH: 27-10-2003)
            If tmp:PassedConsignNo = 0
                Return
            End !If tmp:PassedConsignNo = 0
        Else !If cou:PrintWaybill
            If cou:AutoConsignmentNo 
                cou:LastConsignmentNo += 1
                Access:COURIER.Update()
                tmp:PassedConsignNo   = cou:LastConsignmentNo
            Else !If cou:AutoConsignmentNo     
                tmp:PassedConsignNo = ''
            End!Else !If cou:AutoConsignmentNo
        End !If cou:PrintWaybill


        Case cou:Courier_Type
            Of 'CITY LINK'
                glo:file_name   = 'C:\CITYOUT.TXT'
                Remove(expcity)
                access:expcity.open()
                access:expcity.usefile()
                epc:ref_number  = '|' & Left(Sub(Format(job:ref_number,@n_30),1,30))

                !Single Despatch Of City Link

                DespatchCityLink(0,job:Account_Number,'')

                access:expcity.insert()
                access:expcity.close()
                Copy(expcity,Clip(cou:export_path))
                tmp:ConsignNo   =   ''
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''

            Of 'LABEL G'
                !If the time is past the "Cut off" despatch time, advance the day by one
                tmp:Today = 0
                If cou:Last_Despatch_Time <> 0
                    If Clock() > cou:Last_Despatch_Time
                        tmp:Today = Today()
                        SetToday(Today() + 1)
                    End !If Clock() > cou:Last_Despatch_Time
                End !If cou:Last_Despatch_Time <> 0

                tmp:accountnumber   = job:account_number
                tmp:OldConsignNo    = ''
                tmp:OldConsignNo    = DespatchLabelG(0,'CHECKOLD',job:Account_Number,'','')

                glo:file_name   = Clip(cou:export_path) & '\CL' & Format(job:ref_number,@n06) & '.TXT'

                Remove(glo:file_name)

                !Nothing will be returned by this.
                Nothing# = DespatchLabelG(0,'EXPORT',job:Account_Number,tmp:OldConsignNo,'')

                sav:path    = Path()
                Setpath(cou:export_path)
                Run('LABELG.EXE ' & Clip(glo:file_name) & ' ' & Clip(COU:LabGOptions),1)
                Setpath(sav:path)

                tmp:LabelError = DespatchLabelG(0,'ERROR',job:Account_Number,'','')
                
                If tmp:labelerror <> 1
                    tmp:ConsignNo   =   ''
                    Setcursor()
                    Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:labelerror,0)
                    Setcursor(cursor:wait)
                    If tmp:ConsignNo  = ''
                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = job:despatch_type
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'DESPATCH ROUTINE CANCELLED'
                            access:audit.insert()
                        end!�if access:audit.primerecord() = level:benign
        
                    Else!If consignment_number"  = ''
                        Despatch(job:Despatch_Type,tmp:ConsignNo,0)                                      
                    End!If consignment_number"  = ''
                End!If label_error# = 0
                !If the date has been put forward, change it back.
                If tmp:Today <> 0
                    SetToday(tmp:Today)
                End !If tmp:Today <> 0
            Of 'UPS ALTERNATIVE'
                local:FileName   = 'C:\UPS.TXT'
                Remove(local:FileName)

                DespatchUPSAlt(0,job:Account_Number,'')
                Copy(local:FileName,Clip(cou:Export_Path))

                tmp:ConsignNo   =   ''
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''

            Of 'ANC'
                !Do Nothing
            Of 'SDS'
                If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDS_Thermal_Label(job:Ref_Number)
                Else !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDSLabelInd(job:Ref_Number)        
                End !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                setcursor(cursor:wait)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
            Of 'PARCELINE LASERNET'
                !Parceline
                tmp:ParcelLineName = Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXA'
                Remove(tmp:ParcelLineName)
                Open(ParcelLineExport)
                If Error()
                    Create(ParcelLineExport)
                    Open(ParcelLineExport)
                End !If Error()
                Clear(Parcel:Record)

                DespatchParceline(0,job:Account_Number,'')

                Add(ParcelLineExport)

                CLose(ParcelLineExport)
                RENAME(Clip(tmp:ParcelLineName),Clip(Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXT'))
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
            Of 'TOTE'
                tmp:PassedConsignNo = 'IN' & Clip(sub:Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2)
                ToteLabel(job:Ref_Number,tmp:PassedConsignNo)
                Insert_Consignment_Note_Number('DEL',job:Ref_Number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''

            Else
                If cou:PrintLabel
                    glo:Select1 = job:Ref_Number

                    Address_Label('DEL',tmp:PassedConsignNo)
                    glo:Select1 = ''
                End !If cou:PrintLabel

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber   = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found

                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                !Have finally setup a Courier Type of RAM - L911 (DBH: 11-08-2003)
                If cou:Courier_Type = 'RAM'
                    Case job:Despatch_Type
                        Of 'JOB'
                            If glo:Select20 = 'SECURITYPACKNO' and glo:Select21 <> ''
                                jobe:JobSecurityPackNo = glo:Select21
                            Else !If glo:Select20 = 'SECURITYPACKNO' and glo:Select21 <> ''
                                jobe:JobSecurityPackNo = InsertSecurityPackNumber()
                            End !If glo:Select20 = 'SECURITYPACKNO' and glo:Select21 <> ''
                            
                        Of 'EXC'
                            If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                                jobe:ExcSecurityPackNo = glo:Select21
                            Else !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                                jobe:ExcSecurityPackNo = InsertSecurityPackNumber()
                            End !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                            
                        Of 'LOA'
                            If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                                jobe:LoaSecurityPackNo = glo:Select21
                            Else !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                                jobe:LoaSecurityPackNo = InsertSecurityPackNumber()
                            End !If glo:Select20 = 'SECURITYPACKNO' And glo:Select21 <> ''
                            
                    End !Case job:Despatch_Type
                    Access:JOBSE.TryUpdate()
                End !If cou:Courier_Type = 'RAM'

                !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',After Courier type. Error is '&clip(Error#)&' Printwaybill is '&clip(cou:printWayBill),clip(path())&'\WaybconfLog.csv')

                If cou:PrintWayBill
                    tmp:ConsignNo = tmp:PassedConsignNo

                    Access:WEBJOB.Clearkey(wob:RefNumberKey)
                    wob:RefNumber   = job:Ref_Number
                    If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Found

                    Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                    !Already generated the waybill record. - L91! (DBH: 11-08-2003)
                    Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                    way:WaybillNumber   = tmp:ConsignNo
                    If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                        !Found
                        !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',After fetching waybill about to add details',clip(path())&'\WaybconfLog.csv')
                        way:WayBillType     = 1 !Created from RRC
                        way:AccountNumber   = wob:HeadAccountNumber !Will show under the "booking" accounts list
                        If glo:WebJob
                            way:FromAccount = Clarionet:Global.Param2
                            way:ToAccount   = job:Account_Number
                        Else !If glo:WebJob
                            
                            way:FromAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                            If jobe:WebJob
                                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                                wob:RefNumber   = job:Ref_Number
                                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                    !Found

                                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                    !Error
                                End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                                way:ToAccount = wob:HeadAccountNumber
                                Case job:Despatch_Type
                                    Of 'JOB'
                                        way:WaybillID   = 5 !ARC to RRC (JOB)
                                    Of 'EXC'
                                        way:WaybillID   = 6 !ARC to RRC (EXC)
                                End !Case job:Despatch_Type
                            Else !If jobe:WebJob
                                way:WayBillType = 2
                                way:ToAccount = job:Account_Number
                                Case job:Despatch_Type
                                    Of 'JOB'
                                        way:WaybillID   = 7 !ARC to Customer (JOB)
                                    Of 'EXC'
                                        way:WaybillID   = 8 !ARC to Customer (EXCH)
                                    OF 'LOA'
                                        way:WaybillID   = 9 !ARC to Customer (LOAN)
                                End !Case job:Despatch_Type

                            End !If jobe:WebJob
                            
                        End !If glo:WebJob

                        If Access:WAYBILLS.Update() = Level:Benign
                            !Update Successful
                            !Add the waybill jobs list
                            !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',Waybills updated about to create waybillj',clip(path())&'\WaybconfLog.csv')
                            If Access:WAYBILLJ.PrimeRecord() = Level:Benign
                                waj:WayBillNumber      = way:WayBillNumber
                                waj:JobNumber          = job:Ref_Number
                                Case job:Despatch_Type
                                    Of 'JOB'
                                        waj:IMEINumber         = job:ESN
                                        waj:SecurityPackNumber = jobe:JobSecurityPackNo
                                    Of 'EXC'
                                        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                        xch:Ref_Number  = job:Exchange_Unit_Number
                                        If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                            !Found

                                        Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                            !Error
                                        End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                        waj:IMEINumber         = xch:ESN
                                        waj:SecurityPackNumber = jobe:ExcSecurityPackNo
                                    Of 'LOA'
                                        Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                        loa:Ref_Number  = job:Loan_Unit_Number
                                        If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                            !Found

                                        Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                            !Error
                                        End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                        waj:IMEINumber         = loa:ESN
                                        waj:SecurityPackNumber = jobe:LoaSecurityPackNo
                                End !Case job:Despatch_Type
                                waj:JobType            = job:Despatch_Type
                                waj:OrderNumber        = job:Order_Number
                                
                                If Access:WAYBILLJ.TryInsert() = Level:Benign
                                    !Insert Successful

                                Else !If Access:WAYBILLJ.TryInsert() = Level:Benign
                                    !Insert Failed
                                    Access:WAYBILLJ.CancelAutoInc()
                                End !If Access:WAYBILLJ.TryInsert() = Level:Benign
                            End !If Access:WAYBILLJ.PrimeRecord() = Level:Benign

                            !do WaybConf      !TB13370 - J - 15/04/15 - adding a waybillconf record

                            Clear(glo:Q_JobNumber)
                            Free(glo:Q_JobNumber)
                            glo:Job_Number_Pointer = job:Ref_Number
                            Add(glo:q_JobNumber)

                            !If job was booked at RRC, then send it back to whoever
                            !booked the job, otherwise use whateverthe job is

                            PrintWayBill# = 1

                        Else !If Access:WAYBILLS.TryInsert() = Level:Benign
                            !Insert Failed
                            Case Missive('Failed to create a Waybill Entry.','ServiceBase 3g',|
                                           'mstop.jpg','/OK') 
                                Of 1 ! OK Button
                            End ! Case Missive
                            tmp:ConsignNo = ''
                            Access:WAYBILLS.CancelAutoInc()
                        End !If Access:WAYBILLS.TryInsert() = Level:Benign
                    End !If Access:WAYBILLS.PrimeRecord() = Level:Benign


                Else !If cou:PrintWayBill
                    Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                End !If cou:PrintWayBill

                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                    do AddWaybConf      !TB13370 - J - 15/04/15 - adding a waybillconf record
                End!If consignment_number"  = ''

        End!Case cou:courier_type
    End !If Error# = 0

    If PrintWayBill#
        If jobe:WebJob
            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Found
                WayBillDespatch('','DEF',|
                                wob:HeadAccountNumber,'TRA',|
                                tmp:ConsignNo,cou:Courier)

            Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
        Else !If jobe:WebJob
            WayBillDespatch('','DEF',|
                            job:Account_Number,'SUB',|
                            tmp:ConsignNo,cou:Courier)
        End !If jobe:WebJob
    End !PrintWayBill#
    !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',Leaving DespatchSingle Code',clip(path())&'\WaybconfLog.csv')
   Relate:AUDIT.Close
   Relate:COURIER.Close
   Relate:DESBATCH.Close
   Relate:EXCHANGE.Close
   Relate:EXCHHIST.Close
   Relate:JOBS.Close
   Relate:JOBNOTES.Close
   Relate:LOAN.Close
   Relate:LOANHIST.Close
   Relate:LOCINTER.Close
   Relate:STATUS.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:USERS.Close
   Relate:JOBS_ALIAS.Close
   Relate:INVOICE.Close
   Relate:JOBPAYMT_ALIAS.Close
   Relate:VATCODE.Close
   Relate:WEBJOB.Close
   Relate:WAYBILLJ.Close
   Relate:WAYBCONF.Close
AddWaybConf     Routine

    !TB13370 - J - 15/04/15 - adding a waybillconf record
    !does a record already exist for this waybill?
    Access:WaybConf.clearkey(WAC:KeyWaybillNo)
    WAC:WaybillNo        = way:WayBillNumber
    if access:WaybConf.fetch(WAC:KeyWaybillNo) then
        !OK - not found a record for this one - create a new one
        Access:WaybConf.primerecord()
    END

    WAC:AccountNumber    = way:AccountNumber
    WAC:WaybillNo        = way:WayBillNumber
    WAC:GenerateDate     = today()
    WAC:GenerateTime     = clock()
    WAC:ConfirmationSent = 0
    if Access:Waybconf.update() = level:Benign
        !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',Waybconf updated with no errors',clip(path())&'\WaybconfLog.csv')
    ELSE
        !Lineprint(format(today(),@d06)&','&Format(clock(),@t4)&',Waybconf did not update Error = '&clip(Error()),clip(path())&'\WaybconfLog.csv')
    END

    EXIT
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchLabelG       PROCEDURE  (func:Multiple,func:Type,func:AccountNumber,func:ConsNo,func:Virtual) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_cou_ali_id      USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:OldConsignNo     STRING(30)
tmp:LabelError       STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
OutFile FILE,DRIVER('ASCII'),PRE(OUF),NAME(local:FileName),CREATE,BINDABLE,THREAD
Record  Record
line1       String(255)
            End
        End

! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Case func:Type
        Of 'CHECKOLD'
            !Using the Jobs Alias see if another job has been despatched for this Courier
            !and same Trade Account today. Or "over the weekend".
            !If not, return Level:Fatal
            
            save_cou_ali_id = access:courier_alias.savefile()
            access:courier_alias.clearkey(cou_ali:courier_type_key)
            cou_ali:courier_type = 'LABEL G'
            set(cou_ali:courier_type_key,cou_ali:courier_type_key)
            loop
                if access:courier_alias.next()
                   break
                end !if
                if cou_ali:courier_type <> 'LABEL G'      |
                    then break.  ! end if
                tmp:OldConsignNo = ''

                save_job_ali_id = access:jobs_alias.savefile()
                access:jobs_alias.clearkey(job_ali:DateDespatchKey)
                job_ali:courier         = cou_ali:courier
                If Today() %7 = 6
                    thedate#    = Today() + 2
                Else
                    thedate#    = Today()                
                End!If Today() %7 = 6
                job_ali:date_despatched = thedate#
                set(job_ali:DateDespatchKey,job_ali:DateDespatchKey)
                loop
                    if access:jobs_alias.next()
                       break
                    end !if
                    if job_ali:courier         <> cou_ali:courier      |
                    or job_ali:date_despatched <> thedate#      |
                        then break.  ! end if
                    If job_ali:consignment_number = 'N/A'    
                        Cycle                        
                    End!If job_ali:consignment_number = 'N/A'
                    If job_ali:account_number   = func:AccountNumber and job_ali:postcode_Delivery = job:postcode_delivery
                        If job_ali:consignment_number <> ''
                            tmp:OldConsignNo = job_ali:consignment_number
                            Break
                        End!If job_ali:consignment_number <> ''
                    End!If job_ali:account_number   = job:account_number
                end !loop
                access:jobs_alias.restorefile(save_job_ali_id)
                If tmp:OldConsignNo <> ''
                Else!If tmp:OldConsignNo <> ''
                    save_job_ali_id = access:jobs_alias.savefile()
                    access:jobs_alias.clearkey(job_ali:DateDespLoaKey)
                    job_ali:loan_courier    = cou_ali:courier
                    If Today() %7 = 6
                        thedate#    = Today() + 2
                    Else
                        thedate#    = Today()                
                    End!If Today() %7 = 6            
                    job_ali:loan_despatched = thedate#
                    set(job_ali:DateDespLoaKey,job_ali:DateDespLoaKey)
                    loop
                        if access:jobs_alias.next()
                           break
                        end !if
                        if job_ali:loan_courier    <> cou_ali:courier      |
                        or job_ali:loan_despatched <> thedate#      |
                            then break.  ! end if
                        If job:loan_consignment_number = 'N/A'    
                            Cycle                        
                        End!If job:loan_consignment_number = 'N/A'
                        If job_ali:account_number   = func:AccountNumber and job_ali:postcode_Delivery = job:postcode_delivery
                             If job_ali:loan_consignment_number <> ''
                                tmp:OldConsignNo = job_ali:loan_consignment_number
                                Break
                            End!If job_ali:loan_consignment_number <> ''
                        End!If job_ali:account_number   = job:account_number
                    end !loop
                    access:jobs_alias.restorefile(save_job_ali_id)
                    IF tmp:OldConsignNo <> ''
                    Else!IF tmp:OldConsignNo <> ''
                        save_job_ali_id = access:jobs_alias.savefile()
                        access:jobs_alias.clearkey(job_ali:DateDespExcKey)
                        job_ali:exchange_courier    = cou_ali:courier
                        If Today() %7 = 6
                            thedate#    = Today() + 2
                        Else
                            thedate#    = Today()                
                        End!If Today() %7 = 6                
                        job_ali:exchange_despatched = thedate#
                        set(job_ali:DateDespExcKey,job_ali:DateDespExcKey)
                        loop
                            if access:jobs_alias.next()
                               break
                            end !if
                            if job_ali:exchange_courier    <> cou_ali:courier      |
                            or job_ali:exchange_despatched <> thedate#      |
                                then break.  ! end if
                            If job_ali:exchange_consignment_number = 'N/A'    
                                Cycle    
                            End!If job_ali:exchange_consignment_number = 'N/A'
                            If job_ali:account_number   = func:AccountNumber and job_ali:postcode_Delivery = job:postcode_delivery
                                If job_ali:exchange_consignment_number <> ''
                                    tmp:OldConsignNo = job_ali:exchange_consignment_number
                                    Break
                                End!If job_ali:exchange_consignment_number <> ''
                            End!If job_ali:account_number   = job:account_number
                        end !loop
                        access:jobs_alias.restorefile(save_job_ali_id)
                        If tmp:OldConsignNo <> ''
                        End!If tmp:OldConsignNo <> ''
                    End!IF tmp:OldConsignNo <> ''
                End!If tmp:OldConsignNo <> ''
            end !loop
            access:courier_alias.restorefile(save_cou_ali_id)
            Return tmp:OldConsignNo

        Of 'EXPORT'
            Remove(explabg)
            access:explabg.open()
            access:explabg.usefile()

            Clear(gen:record)
            epg:account_number  = Format(cou:account_number,@s8)
            If func:Multiple
                epg:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
                epg:customer_name     = '|' & Format(func:AccountNumber,@s30)
                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number = func:AccountNumber
                if access:subtracc.fetch(sub:account_number_key) = level:benign
                    access:tradeacc.clearkey(tra:account_number_key) 
                    tra:account_number = sub:main_account_number
                    if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        if tra:use_sub_accounts = 'YES' And func:Virtual <> 'TRA'
                            epc:contact_name = '|' & Left(sub:contact_name)
                            epc:address_line1 = '|' & Left(sub:company_name)
                            epc:address_line2 = '|' & Left(sub:address_line1)
                            epc:town = '|' & Left(sub:address_line2)
                            epc:county = '|' & Left(sub:address_line3)
                            epc:postcode = '|' & Left(sub:postcode)
                        else!if tra:use_sub_accounts = 'YES'
                            epc:contact_name = '|' & Left(tra:contact_name)
                            epc:address_line1 = '|' & Left(tra:company_name)
                            epc:address_line1 = '|' & Left(tra:address_line1)
                            epc:town = '|' & Left(tra:address_line2)
                            epc:county = '|' & Left(tra:address_line3)
                            epc:postcode = '|' & Left(tra:postcode)
                        end!if tra:use_sub_accounts = 'YES'
                    end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                end!if access:subtracc.fetch(sub:account_number_key) = level:benign
            Else!If multi# = 1
                epg:ref_number      = '|' & Format('J' & Clip(job:Ref_number) & '/DB' & Clip(dbt:batch_number) & '/' |
                                & Clip(job:order_number),@s30)
                If job:surname = ''                         
                    epg:contact_Name    = '|' & Format('N/A',@s30)
                Else!If job:surname = ''
                    epg:contact_Name    = '|' & Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & |
                                                CLip(job:surname),@s30)
                End!If job:surname = ''
                epg:customer_name   = '|' & Format(job:account_number,@s30)                                             
                epg:Address_Line1   = '|' & Format(job:company_name_delivery,@s30)              
                epg:Address_Line2   = '|' & Format(job:address_line1_delivery,@s30)
                epg:Address_Line3   = '|' & Format(job:address_line2_delivery,@s30)
                epg:Address_Line4   = '|' & Format(job:address_line3_delivery,@s30)
                epg:Postcode        = '|' & Format(job:postcode_delivery,@s4)
                
            End!If multi# = 1
            epg:city_service      = '|' & Format(cou:service,@s2)
            Case job:despatch_type    
                Of 'JOB'
                    If job:jobservice <> ''
                        epg:city_service      = '|' & Format(job:jobservice,@s2)
                    End!            If job:jobservice <> ''
                Of 'EXC'
                    If job:excservice <> ''
                        epg:city_service      = '|' & Format(job:excservice,@s2)            
                    End!If job:excservice <> ''
                Of 'LOA'
                    If job:loaservice <> ''
                        epg:city_service      = '|' & Format(job:loaservice,@s2)
                    End!If job:loaservice <> ''
            End!Case job:despatch_type
            access:jobnotes.clearkey(jbn:refNumberKey)
            jbn:RefNumber   = job:ref_number
            access:jobnotes.tryfetch(jbn:refnumberkey)
            epg:City_Instructions   = '|' & Format(jbn:delivery_text,@s30)
            epg:Pudamt          = '|' & Format(0.00,@s10)
            return# = 1
            If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
                If job:workshop = 'YES'
                    return# = 0
                Else!If job:workshop = 'YES'
                    If job:third_party_site <> ''
                        return# = 0
                    End!If job:third_party_site <> ''
                End!If job:workshop = 'YES'
            End!If job:despatch_type = 'EXC'
            If job:despatch_type = 'JOB'
                If job:loan_unit_number = ''
                    return# = 0
                End!If job:loan_unit_number = ''
            End!If job:despatch_type = 'JOB'
            
            If return# = 0
                epg:Return_it   = '|N'
            Else!If return# = 0
                epg:Return_it   = '|Y'
            End!If return# = 0
            
            epg:Saturday    = '|N'
            epg:dog         = '|' & Format('Mobile Phone Goods',@s30)
            epg:nol         = '|' & Format('01',@s8)
            !SolaceViewVars('Passed Old Cons No',tmp:OldConsignNo,,6)
            If func:ConsNo <> ''
                epg:JobNo       = '|' & Clip(Format(func:ConsNo,@s8))
            Else!   If tmp:OldConsignNo <> ''
                epg:JobNo       = '|        '       
            End!    If tmp:OldConsignNo <> ''
            epg:Weight      = '|00.00'
            access:explabg.insert()
            
            local:FileName   = Clip(Cou:export_Path) & '\SB' & Format(job:Ref_number,@n06) & '.TXT'
            Remove(local:FileName)
            Open(outfile)
            If Error()
                Create(outfile)
                Open(outfile)
            End!Open(outfile)
            ouf:line1   = 'Customer Name,' & CLip(epg:customer_name)
            Add(outfile)
            ouf:line1   = 'Date,' & Clip(Format(Today(),@d6))
            Add(outfile)
            ouf:line1   = 'No Passed,' & CLip(epg:JobNo)    
            add(outfile)
            Close(outfile)
            access:explabg.close()
            Return 0

        Of 'ERROR'
            access:explabg.open()
            access:explabg.usefile()
            Set(explabg)
            access:explabg.next()
            tmp:labelerror = 0
            Setcursor()

            Case Sub(epg:account_number,1,2)
                Of '01'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 01<13,10><13,10>Incorrect Format.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '02'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 02<13,10><13,10>Missing City Link Account Number.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '03'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 03<13,10><13,10>No Job Number.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '04'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 04<13,10><13,10>No Customer Name.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '05'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 05<13,10><13,10>No Contact Name.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '06'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 06<13,10><13,10>Invalid City Link Service Code.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '07'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 07<13,10><13,10>Service not available to destination.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '08'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 08<13,10><13,10>Description Of Goods Required.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '09'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 09<13,10><13,10>Number Of Labels Required.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Of '10'
                    tmp:labelerror = 1
                    Case Missive('Job Number: ' & Clip(job:Ref_Number) & ' - Export File Error: 10<13,10><13,10>Invalid Postcode.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                Else 
                    tmp:labelerror = epg:account_number              
            End!Case epg:account_number
            Setcursor(cursor:wait)
            local:FileName   = Clip(Cou:export_Path) & '\SB' & Format(job:Ref_number,@n06) & '.TXT'
            Open(outfile)
            ouf:line1   = 'Returned,' & CLip(epg:account_number)    
            Add(outfile)
            Close(outfile)
            
            access:explabg.close()
            Return tmp:LabelError

    End !Case func:Type
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchCityLink     PROCEDURE  (func:Multiple,func:AccountNumber,func:Virtual) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    epc:account_number    = Left(Sub(cou:account_number,1,8))
    dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
    If func:Multiple
        dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
        epc:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
        epc:customer_name     = '|' & Left(Sub(func:AccountNumber,1,30))        
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = func:AccountNumber
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                epc:contact_name      = '|N/A'

                if tra:use_sub_accounts = 'YES' and func:Virtual <> 'TRA'
                    epc:contact_name = '|' & Left(sub:contact_name)
                    epc:address_line1 = '|' & Left(sub:company_name)
                    epc:address_line2 = '|' & Left(sub:address_line1)
                    epc:town = '|' & Left(sub:address_line2)
                    epc:county = '|' & Left(sub:address_line3)
                    epc:postcode = '|' & Left(sub:postcode)
                else!if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(tra:contact_name)
                    epc:address_line1 = '|' & Left(tra:company_name)
                    epc:address_line2 = '|' & Left(tra:address_line1)
                    epc:town = '|' & Left(tra:address_line2)
                    epc:county = '|' & Left(tra:address_line3)
                    epc:postcode = '|' & Left(tra:postcode)
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If multiple_despatch_temp = 'YES'
        If job:surname = ''
            epc:contact_name      = '|N/A'            
        Else!If job:surname = ''
            epc:contact_name      = '|' & Left(Sub(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),1,30))
        End!
        epc:address_line1     = '|' & Left(Sub(job:company_name_delivery,1,30))
        epc:address_line2     = '|' & Left(Sub(job:address_line1_delivery,1,30))
        epc:town              = '|' & Left(Sub(job:address_line2_delivery,1,30))
        epc:county            = '|' & Left(Sub(job:address_line3_delivery,1,30))
        epc:postcode          = '|' & Left(Sub(job:postcode_delivery,1,8))
        epc:customer_name     = '|' & Left(Sub(job:account_number,1,30))        
    End!If multiple_despatch_temp = 'YES'
    epc:ref_number        = '|J' & Clip(Format(job:ref_number,@s9)) & '/DB' & Clip(Format(dbt:batch_number,@s9)) & |
                                    '/' & Clip(job:order_number)
    
    epc:nol               = '|' & '01'
    epc:city_service      = '|' & Left(Sub(cou:service,1,2))
    Case job:despatch_type    
        Of 'JOB'
            If job:jobservice <> ''
                epc:city_service      = '|' & Left(Sub(job:jobservice,1,2))                        
            End!            If job:jobservice <> ''
        Of 'EXC'
            If job:excservice <> ''
                epc:city_service      = '|' & Left(Sub(job:excservice,1,2))            
            End!If job:excservice <> ''
        Of 'LOA'
            If job:loaservice <> ''
                epc:city_service      = '|' & Left(Sub(job:loaservice,1,2))            
            End!If job:loaservice <> ''
    End!Case job:despatch_type
    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:ref_number
    access:jobnotes.tryfetch(jbn:REfNumberKey)
    epc:city_instructions = '|' & Left(Sub(jbn:delivery_text,1,30))
    epc:pudamt            = '|' & Left(Sub('0.00',1,4))
    
    return#    = 1
    If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
        If job:workshop = 'YES'
            return# = 0
        Else!If job:workshop = 'YES'
            If job:third_party_site <> ''
                return# = 0
            End!If job:third_party_site <> ''
        End!If job:workshop = 'YES'
    End!If job:despatch_type = 'EXC'
    If job:despatch_type = 'JOB'
        If job:loan_unit_number = ''
            return# = 0
        End!If job:loan_unit_number = ''
    End!If job:despatch_type = 'JOB'
    
    If return# = 0
        epc:Return_it    = '|N'
    Else!If return# = 0
        epc:Return_it    = '|Y'
    End!If return# = 0
    epc:saturday          = '|' & Left('N')
    epc:dog               = '|' & Left(Sub('Mobile Phone Goods',1,30))
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchParceLine    PROCEDURE  (func:Multiple,func:AccountNumber,func:Virtual) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:WorkstationName  STRING(255)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
     IF GetComputerName(JB:ComputerName,JB:ComputerLen).
     tmp:WorkstationName=JB:ComputerName
    Parcel:Labels  = 1
    If func:Multiple
        Parcel:OrderNo = 'DB' & dbt:Batch_Number
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = func:AccountNumber
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Use_Sub_Accounts = 'YES' And func:Virtual <> 'TRA'
                    Parcel:CompanyName  = sub:Company_Name
                    Parcel:AddressLine1 = sub:Address_Line1
                    Parcel:AddressLine2 = sub:Address_Line2
                    Parcel:Town         = sub:Address_Line3
                    Parcel:Postcode     = sub:Postcode
                Else !If tra:Use_Sub_Accounts = 'YES'
                    Parcel:CompanyName  = tra:Company_Name
                    Parcel:AddressLine1 = tra:Address_Line1
                    Parcel:AddressLine2 = tra:Address_Line2
                    Parcel:Town         = tra:Address_Line3
                    Parcel:Postcode     = tra:Postcode
                 
                End !If tra:Use_Sub_Accounts = 'YES'
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        Parcel:AccountCode  = func:AccountNumber
    Else !If multi# = 1
        Parcel:OrderNo = 'J' & job:Ref_Number & ' ' & job:Despatch_Type
        Parcel:Instructions = 'DB' & dbt:Batch_Number
        Parcel:CompanyName  = job:Company_Name_Delivery
        Parcel:AddressLine1 = job:Address_Line1_Delivery
        Parcel:AddressLine2 = job:Address_Line2_Delivery
        Parcel:Town         = job:Address_Line3_Delivery
        Parcel:Postcode     = job:Postcode_Delivery 
        Parcel:AccountCode  = job:Account_Number            
    End !If multi# = 1
    Parcel:Service      = cou:Service
    Parcel:Type         = 1
    Parcel:Workstation  = tmp:WorkstationName
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchMultiple     PROCEDURE  (func:Courier,func:AccountNumber,func:BatchNumber,func:LabelType,func:Type) ! Declare Procedure
save_muld_id         USHORT,AUTO
save_webjob_id       USHORT,AUTO
save_jobse_id        USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_muldesp_id      USHORT,AUTO
save_muldespj_id     USHORT,AUTO
save_jobs_id         USHORT,AUTO
tmp:PassedConsignNo  STRING(30)
tmp:DoDespatch       BYTE(0)
tmp:AccountNumber    STRING(30)
tmp:OldConsignNo     STRING(30)
sav:Path             STRING(255)
tmp:LabelError       STRING(255)
tmp:ConsignNo        STRING(30)
tmp:PrintSummaryNote BYTE(0)
tmp:Today            DATE
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:COURIER.Clearkey(cou:Courier_Key)
    cou:Courier = func:Courier
    If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Found
        !Get the next waybill number - L911 (DBH: 11-08-2003)
        If cou:PrintWaybill
            tmp:PassedConsignNo = NextWaybillNumber()
            !Do not continue if waybill error - 3432 (DBH: 27-10-2003)
            If tmp:PassedConsignNo = 0
                Return False
            End !If tmp:PassedConsignNo = 0
        Else !If cou:PrintWaybill
            If cou:AutoConsignmentNo 
                cou:LastConsignmentNo += 1
                Access:COURIER.Update()
                tmp:PassedConsignNo   = cou:LastConsignmentNo

            Else !If cou:AutoConsignmentNo     
                tmp:PassedConsignNo = ''
            End!Else !If cou:AutoConsignmentNo  
        End !If cou:PrintWaybill

        tmp:DoDespatch = 1
        Case cou:Courier_Type
            Of 'CITY LINK'
                glo:file_name   = 'C:\CITYOUT.TXT'
                Remove(expcity)
                access:expcity.open()
                access:expcity.usefile()
                multi# = 1

                DespatchCityLink(1,func:AccountNumber,func:Type)

                access:expcity.insert()
                access:expcity.close()
                Copy(expcity,Clip(cou:export_path))
                If error()
                    Stop(error())
                    tmp:DoDespatch = 0
                End!If error()
            Of 'LABEL G'
                !Is the despatch after the "cut off" time.
                !Move to the next day
                tmp:Today   = 0
                If cou:Last_Despatch_Time <> 0
                    If Clock() > cou:Last_Despatch_Time
                        tmp:Today = Today()
                        SetToday(Today() + 1)
                    End !If Clock() > cou:Last_Despatch_Time
                End !If cou:Last_Despatch_Time <> 0
                tmp:accountnumber   = func:AccountNumber
                tmp:OldConsignNo    = ''

                x# = DespatchLabelG(1,'CHECKOLD',func:AccountNumber,'',func:Type)

                glo:file_name   = Clip(cou:export_path) & '\CB' & Format(dbt:batch_number,@n06) & '.TXT'
                Remove(glo:file_name)


                x# = DespatchLabelG(1,'EXPORT',func:AccountNumber,'',func:Type)

                sav:path    = Path()
                Setpath(cou:export_path)
                Run('LABELG.EXE ' & Clip(glo:file_name) & ' ' & Clip(cou:labgOptions),1)
                Setpath(sav:path)


                tmp:LabelError = DespatchLabelG('1','ERROR',func:AccountNumber,'',func:Type)

                If tmp:labelerror = 0
                    tmp:DoDespatch = 1
                End!If tmp:labelerror = 0
                If tmp:Today <> 0
                    SetToday(tmp:Today)
                End !If tmp:Today <> 0
            Of 'SDS'
                !Fudge to make the label work. The Label needs the Global Job Number Queue, for it's job
                !number list, so I'll fill it in.
                Clear(glo:q_JobNumber)
                Free(glo:Q_JobNumber)
                Access:MULDESP.ClearKey(muld:RecordNumberKey)
                muld:RecordNumber = func:BatchNumber
                If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Found
                    Save_mulj_ID = Access:MULDESPJ.SaveFile()
                    Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                    mulj:RefNumber = muld:RecordNumber
                    Set(mulj:JobNumberKey,mulj:JobNumberKey)
                    Loop
                        If Access:MULDESPJ.NEXT()
                           Break
                        End !If
                        If mulj:RefNumber <> muld:RecordNumber      |
                            Then Break.  ! End If
                        glo:Job_Number_Pointer = mulj:JobNumber
                        Add(glo:q_JobNumber)
                    End !Loop
                    Access:MULDESPJ.RestoreFile(Save_mulj_ID)
                Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign

                If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDS_Thermal_Multiple(func:AccountNumber)
                Else !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDSLabel(func:AccountNumber)
                End !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                despatch# = 1
                Clear(glo:q_JobNumber)
                Free(glo:Q_JobNumber)

            Of 'PARCELINE LASERNET'
                tmp:ParcelLineName = Clip(cou:Export_Path) & '\DB' & Format(dbt:Batch_Number,@n06) &  '.TXA'
                Remove(tmp:ParcelLineName)
                Open(ParcelLineExport)
                If Error()
                    Create(ParcelLineExport)
                    Open(ParcelLineExport)
                End !If Error()
                Clear(Parcel:Record)
                multi# = 1

                DespatchParceline(1,func:AccountNumber,func:Type)

                Add(ParcelLineExport)

                CLose(ParcelLineExport)
                RENAME(Clip(tmp:ParcelLineName),Clip(Clip(cou:Export_Path) & '\DB' & Format(dbt:Batch_Number,@n06) &  '.TXT'))
            Else
                If cou:PrintLabel
                    Address_Label_Multiple(tra:Account_Number,sub:Account_Number,func:LabelType,tmp:PassedConsignNo,func:Type)
                End !If cou:PrintLabel
        End !Case cou:Courier_Type

        If tmp:DoDespatch = 1

            setcursor()
            If cou:PrintWayBill
                tmp:ConsignNo = tmp:PassedConsignNo
                !Is the "to" account an RRC. If so, create a waybill number entry
                !so they can use the waybill confirmation
                Access:WAYBILLS.Clearkey(way:WaybillNumberKey)
                way:WaybillNumber   = tmp:ConsignNo
                If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                    !Found
                    way:WayBillType     = 1 !Created from RRC
                    way:AccountNumber   = func:AccountNumber
                    If glo:WebJob
                        way:FromAccount = Clarionet:Global.Param2
                        way:ToAccount   = func:AccountNumber
                        way:WayBillID   = 10 !RRC to Customer (MULTI)
                        ! -----------------------------------------
                        ! DBH 20/11/2008 #10573
                        ! Inserting: Allow for multiple despatch back to PUP
                        Save_MULDESP_ID = Access:MULDESP.SaveFile()
                        Access:MULDESP.ClearKey(muld:RecordNumberKey)
                        muld:RecordNumber = func:BatchNumber
                        If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                            !Found
                            Save_MULDESPJ_ID = Access:MULDESPJ.SaveFile()
                            Access:MULDESPJ.Clearkey(mulj:JobNumberKey)
                            mulj:RefNumber = muld:RecordNumber
                            Set(mulj:JobNumberKey,mulj:JobNumberKey)
                            Loop ! Begin Loop
                                If Access:MULDESPJ.Next()
                                    Break
                                End ! If Access:MULDESPJ.Next()
                                Save_JOBS_ID = Access:JOBS.SaveFile()
                                Save_WEBJOB_ID = Access:WEBJOB.SaveFile()

                                Access:JOBS.ClearKey(job:Ref_Number_Key)
                                job:Ref_Number = mulj:JobNumber
                                If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                    !Found
                                Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                    !Error
                                End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                                If job:Who_Booked = 'WEB'
                                    way:WayBillType = 9
                                    way:WayBillID   = 22
                                End ! If job:Who_Booked = 'WEB'

                                Access:JOBS.RestoreFile(Save_JOBS_ID)
                                Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
                                Break
                            End ! Loop
                            Access:MULDESPJ.RestoreFile(Save_MULDESPJ_ID)

                        Else ! If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                            !Error
                        End ! If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign

                        Access:MULDESP.RestoreFile(Save_MULDESP_ID)

                        ! End: DBH 20/11/2008 #10573
                        ! -----------------------------------------

                    Else !If glo:WebJob
                        way:FromAccount = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                        way:ToAccount   = func:AccountNumber
! Changing (DBH 15/05/2007) # 9004 - To determine if sending to customer or RRC have to lookup one of the jobs on the batch
!! Changing (DBH 02/04/2007) # 8513 - Check for a remote account doesn't work for VCP jobs
!                        
!!                        If Global.RemoteAccount(func:AccountNumber,func:Type)
!! to (DBH 02/04/2007) # 8513
!                        Save_WEBJOB_ID = Access:WEBJOB.SaveFile()
!                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
!                        wob:RefNumber = job:Ref_Number
!                        If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!                            !Found
!                            way:AccountNumber = wob:HeadAccountNumber
!                            way:ToAccount = wob:HeadAccountNumber
!                        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!                            !Error
!                        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
!
!                        Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
!                        Save_JOBSE_ID = Access:JOBSE.SaveFile()
!                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
!                        jobe:RefNumber = job:Ref_Number
!                        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                            !Found
!                        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                            !Error
!                        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
!                        If jobe:WebJob
!! End (DBH 02/04/2007) #8513
!                            way:WaybillID = 12 !ARC to RRC (MULTI)
!                        Else
!                            way:WayBillType = 2
!                            way:WaybillID = 11 !ARC to Customer (MULTI)
!                        End !If RemoteAccount#
!                        Access:JOBSE.RestoreFile(Save_JOBSE_ID)
! to (DBH 15/05/2007) # 9004
                        Save_MULDESP_ID = Access:MULDESP.SaveFile()
                        Access:MULDESP.ClearKey(muld:RecordNumberKey)
                        muld:RecordNumber = func:BatchNumber
                        If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                            !Found
                            Save_MULDESPJ_ID = Access:MULDESPJ.SaveFile()
                            Access:MULDESPJ.Clearkey(mulj:JobNumberKey)
                            mulj:RefNumber = muld:RecordNumber
                            Set(mulj:JobNumberKey,mulj:JobNumberKey)
                            Loop ! Begin Loop
                                If Access:MULDESPJ.Next()
                                    Break
                                End ! If Access:MULDESPJ.Next()
                                Save_JOBSE_ID = Access:JOBSE.SaveFile()
                                Save_WEBJOB_ID = Access:WEBJOB.SaveFile()

                                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                jobe:RefNumber = mulj:JobNumber
                                If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                    !Found
                                Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                    !Error
                                End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                                If jobe:WebJob
                                    way:WaybillID = 12 !ARC to RRC (Multi)
                                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                                    wob:RefNumber = mulj:JobNumber
                                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                        !Found
                                        way:AccountNumber = wob:HeadAccountNumber
                                        way:ToAccount = wob:HeadAccountNumber
                                    Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                        !Error
                                    End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                Else ! If jobe:WebJob
                                    way:WayBillID = 11 !ARC to Customer (Multi)
                                    way:WaybillTYpe = 2 !Don't show in Waybill Confirmation
                                End ! If jobe:WebJob
                                

                                Access:JOBSE.RestoreFile(Save_JOBSE_ID)
                                Access:WEBJOB.RestoreFile(Save_WEBJOB_ID)
                                Break
                            End ! Loop
                            Access:MULDESPJ.RestoreFile(Save_MULDESPJ_ID)

                        Else ! If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                            !Error
                        End ! If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign

                        Access:MULDESP.RestoreFile(Save_MULDESP_ID)
! End (DBH 15/05/2007) #9004
                    End !If glo:WebJob
                    
                    If Access:WAYBILLS.Update() = Level:Benign
                        !Insert Successful
!                        !TB13370 - J - 15/04/15 - adding a waybillconf record
!                        Access:WaybConf.primerecord()
!                        WAC:AccountNumber    = way:AccountNumber
!                        WAC:WaybillNo        = way:WayBillNumber
!                        WAC:GenerateDate     = today()
!                        WAC:GenerateTime     = clock()
!                        WAC:ConfirmationSent = 0
!                        Access:Waybconf.update()


                    Else !If Access:WAYBILLS.TryInsert() = Level:Benign
                        !Insert Failed
                        tmp:ConsignNo = ''
                        Case Missive('Failed to create a Waybill Entry.','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                        Access:WAYBILLS.CancelAutoInc()
                    End !If Access:WAYBILLS.TryInsert() = Level:Benign
                Else ! If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign
                    !Error
                End !If Access:WAYBILLS.Tryfetch(way:WaybillNumberKey) = Level:Benign

            Else !If cou:PrintWayBill
                Insert_Consignment_Note_Number(func:LabelType,tra:account_number,|
                                                sub:account_number,tmp:ConsignNo,tmp:PassedConsignNo,1)
            End !If cou:PrintWayBill

            If tmp:consignno <> ''
                recordspercycle     = 25
                recordsprocessed    = 0
                percentprogress     = 0
                open(progresswindow)
                progress:thermometer    = 0
                ?progress:pcttext{prop:text} = '0% Completed'

                recordstoprocess    = Records(MULDESP)

                Access:MULDESP.ClearKey(muld:RecordNumberKey)
                muld:RecordNumber = func:BatchNumber
                If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Found

                    Clear(glo:Q_JobNumber)
                    Free(glo:Q_JobNumber)
                    Save_mulj_ID = Access:MULDESPJ.SaveFile()
                    Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                    mulj:RefNumber = func:BatchNumber
                    Set(mulj:JobNumberKey,mulj:JobNumberKey)
                    Loop
                        If Access:MULDESPJ.NEXT()
                           Break
                        End !If
                        If mulj:RefNumber <> func:BatchNumber      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        access:jobs.clearkey(job:ref_number_key)
                        job:ref_number  = mulj:JobNumber
                        If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                            !Do not process job if in use - 3939 (DBH: 26-03-2004)
                            If JobInUse(job:Ref_Number,1)
                                Return False
                            End !If JobInUse(job:Ref_Number,0)

                            Access:JOBSE.Clearkey(jobe:RefNumberKey)
                            jobe:RefNumber   = job:Ref_Number
                            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                !Found
                                !Fill in the globals to show that a security pack number
                                !has already been recorded
                                glo:Select20 = 'SECURITYPACKNO'
                                glo:Select21 = mulj:SecurityPackNumber

                            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                !Error
                            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                            !Add the waybill jobs list
                            If Access:WAYBILLJ.PrimeRecord() = Level:Benign
                                waj:WayBillNumber      = way:WayBillNumber
                                waj:JobNumber          = job:Ref_Number
                                If glo:WebJob
                                    Case jobe:DespatchType
                                        Of 'JOB'
                                            waj:IMEINumber         = job:ESN
                                        Of 'EXC'
                                            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                            xch:Ref_Number  = job:Exchange_Unit_Number
                                            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                !Found

                                            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                !Error
                                            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                            waj:IMEINumber         = xch:ESN
                                        Of 'LOA'
                                            Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                            loa:Ref_Number  = job:Loan_Unit_Number
                                            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                !Found

                                            Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                !Error
                                            End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                            waj:IMEINumber         = loa:ESN

                                    End !Case job:Despatch_Type
                                    waj:JobType            = jobe:DespatchType
                                Else !If glo:WebJob
                                    Case job:Despatch_Type
                                        Of 'JOB'
                                            waj:IMEINumber         = job:ESN
                                        Of 'EXC'
                                            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                            xch:Ref_Number  = job:Exchange_Unit_Number
                                            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                !Found

                                            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                !Error
                                            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                            waj:IMEINumber         = xch:ESN
                                        Of 'LOA'
                                            Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                            loa:Ref_Number  = job:Loan_Unit_Number
                                            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                !Found

                                            Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                !Error
                                            End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                            waj:IMEINumber         = loa:ESN
                                    End !Case job:Despatch_Type
                                    waj:JobType            = job:Despatch_Type
                                End !If glo:WebJob
                                
                                waj:OrderNumber        = job:Order_Number
                                waj:SecurityPackNumber = mulj:SecurityPackNumber
                                If Access:WAYBILLJ.TryInsert() = Level:Benign
                                    !Insert Successful

                                Else !If Access:WAYBILLJ.TryInsert() = Level:Benign
                                    !Insert Failed
                                    Access:WAYBILLJ.CancelAutoInc()
                                End !If Access:WAYBILLJ.TryInsert() = Level:Benign
                            End !If Access:WAYBILLJ.PrimeRecord() = Level:Benign

                            If glo:WebJob
                                DespatchWebJob(jobe:DespatchType,mulj:Courier,tmp:ConsignNo,1)
                            Else !If glo:WebJob
                                Despatch(job:Despatch_Type,tmp:ConsignNo,1)
                            End !If glo:WebJob
                            glo:Select20 = ''
                            glo:Select21 = ''
                        End!If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                        glo:Job_Number_Pointer = mulj:JobNumber
                        Add(glo:q_JobNumber)

                    End !Loop
                    Access:MULDESPJ.RestoreFile(Save_mulj_ID)

                    close(progresswindow)

                    !Check to see if any numbers added to queue.
                    !If not, then all the jobs were in use, so remove the waybill - 3939 (DBH: 26-03-2004)
                    If Records(glo:Q_JobNumber) = 0
                        Case Missive('There are no jobs available to despatch.','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                        If cou:PrintWayBill = True
                            Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
                            way:WayBillNumber = tmp:ConsignNo
                            If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                                !Found
                                Relate:WAYBILLS.Delete(0)
                            Else !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                                !Error
                            End !If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
                        End !If cou:PrintWayBill = True
                    Else !If Records(glo:Q_JobNumber) = 0

                        If func:Type = 'TRA'
                            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                            tra:Account_Number  = func:AccountNumber
                            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Found
                                If tra:Print_Despatch_Despatch = 'YES' And tra:Summary_Despatch_Notes = 'YES'
                                    tmp:PrintSummaryNote = 1
                                Else !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                                    tmp:PrintSummaryNote = 0
                                End !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'

                            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                            
                        Else !If func:Type = 'TRA'
                            If InvoiceSubAccounts(func:AccountNumber)
                                If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                                    tmp:PrintSummaryNote = 1
                                Else !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                                    tmp:PrintSummaryNote = 0
                                End !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                            Else !If InvoiceSubAccounts(func:AccountNumber)
                                If tra:Print_Despatch_Despatch = 'YES' And tra:Summary_Despatch_Notes = 'YES'
                                    tmp:PrintSummaryNote = 1
                                Else !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                                    tmp:PrintSummaryNote = 0
                                End !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                            End !If InvoiceSubAccounts(func:AccountNumber)
                        End !If func:Type = 'TRA'

                        If tmp:PrintSummaryNote
                            Multiple_Despatch_Note(func:AccountNumber, |
                                                tmp:ConsignNo,|
                                                dbt:Batch_Number,|
                                                func:Courier,|
                                                func:Type)
                        End!IF Print_summary# = 1

                        Set(DEFAULTS)
                        Access:DEFAULTS.Next()
                        If cou:PrintWayBill
                            If glo:WebJob
                                If Records(glo:Q_JobNumber) = 1
                                    WayBillDespatch(Clarionet:Global.Param2,'TRA',|
                                                    func:AccountNumber,'CUS',|
                                                    tmp:ConsignNo,func:Courier)
                                Else !If Records(glo:Q_JobNumber) = 1
                                    WayBillDespatch(Clarionet:Global.Param2,'TRA',|
                                                    func:AccountNumber,func:Type,|
                                                    tmp:ConsignNo,func:Courier)

                                End !If Records(glo:Q_JobNumber) = 1

                            Else !If glo:WebJob
                                WayBillDespatch('','DEF',|
                                                func:AccountNumber,func:Type,|
                                                tmp:ConsignNo,func:Courier)

                            End !If glo:WebJob

                        End !If Instring('VODACOM',def:User_Name,1,1)

                        Clear(glo:Q_JobNumber)
                        Free(glo:Q_JobNumber)

                        Relate:MULDESP.Delete(0)
                    End !If Records(glo:Q_JobNumber) = 0
                Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign

            End!If tmp:consignno <> ''
        End !If tmp:Despatch = 1
    Else! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:.Tryfetch(cou:Courier_Key) = Level:Benign

    Return True
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                tmp:cancel = 1
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchUPS          PROCEDURE  (func:Multiple,func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Company Name
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Fetch(sub:Account_Number_Key) = Level:Benign
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
            If tra:Use_Sub_Accounts = 'YES'
                gen:Line1   = Stripcomma(Clip(sub:Company_Name))
                If sub:Contact_Name <> ''            
                    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(sub:Contact_Name))                                              
                Else!If sub:Contact_Name <> ''
                    gen:Line1   = Clip(gen:Line1) & ',N/A'
                End!If sub:Contact_Name <> ''
            Else!If tra:Use_Sub_Accounts = 'YES'
                
            End!If tra:Use_Sub_Accounts = 'YES'
        End!If Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
    End!If Access:SUBTRACC.Fetch(sub:Account_Number_Key) = Level:Benign


    gen:Line1   = Stripcomma(Clip(job:Company_Name_Delivery))
    !Contact Name
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname))
    !Address
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Address_Line1_Delivery))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Address_LIne2_Delivery))
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Address_LIne3_Delivery))
    !Blank
    gen:Line1   = Clip(gen:Line1) & ','
    !Postcode
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(Job:Postcode_Delivery))
    !Telephone
    gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip(job:Telephone_Delivery))
    !Ref 1
    If multi# = 1
        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip('DB' & Format(dbt:Batch_Number,@s9)))    
    Else!If multi# = 1
        gen:Line1   = Clip(gen:Line1) & ',' & Stripcomma(Clip('J' & Format(job:Ref_Number,@s9) & '/DB' & Format(dbt:Batch_Number,@s9)) & '/' & Clip(job:Order_Number))
    End!If multi# = 1

    !Ref 2
    gen:Line1   = Clip(gen:line1) & ',' 
    !Legs
    gen:Line1   = Clip(gen:Line1) & ','
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DespatchUPSAlt       PROCEDURE  (func:Multiple,func:AccountNumber,func:Virtual) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
UPSFile     File,Driver('BASIC','/ALWAYSQUOTE=off'),Name(local:FileName),Pre(upsfil),Create,Bindable,Thread
Record          Record
CompanyName         String(35)
ContactName         String(35)
Address1            String(35)
Address2            String(35)
Address3            String(35)
City                String(35)
Country             String(35)
Postcode            String(9)
Telephone           String(15)
Description         String(35)
Ref1                String(35)
Ref2                String(35)
Legs                String(1)
                End
            End
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Open(UPSFILE)

    dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
    If func:Multiple
        dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
        upsfil:Description   = '.'
        upsfil:Ref1         = 'DB' & StripPoint(Clip(dbt:batch_number))
        upsfil:Ref2         = '.'
    Else
        upsfil:Description = 'J' & StripPoint(Clip(job:ref_number))
        upsfil:Ref1        = 'DB' & StripPoint(Clip(dbt:batch_number))
        upsfil:Ref2        = Clip(StripComma(job:order_number))
    End
    If func:Multiple
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = func:AccountNumber
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:use_sub_accounts = 'YES' and func:Virtual <> 'TRA'
                    upsfil:ContactName  = Left(StripComma(sub:contact_name))
                    upsfil:CompanyName  = Left(StripComma(sub:company_name))
                    upsfil:Address1     = Left(StripComma(sub:address_line1))
                    upsfil:Address2     = Left(StripComma(sub:address_line2))
                    upsfil:Address3     = Left(StripComma(sub:address_line3))
                    upsfil:City         = Left(StripComma(sub:address_line3))
                    upsfil:Country      = 'United Kingdom'
                    upsfil:Postcode = Left(StripComma(sub:postcode))
                    upsfil:Telephone    = Left(StripComma(sub:Telephone_Number))
                else!if tra:use_sub_accounts = 'YES'
                    upsfil:Contactname  = Left(StripComma(tra:contact_name))
                    upsfil:CompanyName  = Left(StripComma(tra:company_name))
                    upsfil:Address1     = Left(StripComma(tra:address_line1))
                    upsfil:Address2     = Left(StripComma(tra:address_line2))
                    upsfil:Address3     = Left(StripComma(tra:address_line3))
                    upsfil:City         = Left(StripComma(tra:address_line3))
                    upsfil:Country      = 'United Kingdom'
                    upsfil:Postcode     = Left(StripComma(tra:postcode))
                    upsfil:Telephone    = Left(StripComma(tra:Telephone_Number))
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If func:Multiple = 'YES'
        upsfil:ContactName  = Left(StripComma(Clip(job:title)) & ' ' & Clip(StripComma(job:initial)) & ' ' & Clip(StripComma(job:surname)))
        upsfil:CompanyName  = Left(StripComma(job:company_name_delivery))
        upsfil:Address1     = Left(StripComma(job:address_line1_delivery))
        upsfil:Address2     = Left(StripComma(job:address_line2_delivery))
        upsfil:Address3     = Left(StripComma(job:address_line3_delivery))
        upsfil:City         = Left(StripComma(job:address_line3_delivery))
        upsfil:Country      = 'United Kingdom'
        upsfil:Postcode     = Left(StripComma(job:postcode_delivery))
        upsfil:Telephone    = Left(StripComma(job:telephone_delivery))
    End!If func:Multiple = 'YES'
    If job:Despatch_Type = 'EXC' or job:Despatch_Type = 'LOA'
        upsfil:Legs = '3'
    Else!If job:Despatch_Type = 'EXC' or job:Desaptch_Type = 'LOA'
        upsfil:Legs = '1'
    End!If job:Despatch_Type = 'EXC' or job:Desaptch_Type = 'LOA'
    If upsfil:CompanyName = ''
        upsfil:CompanyName = '.'
    End!If upsfil:CompanyName = ''
    Close(UPSFILE)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
