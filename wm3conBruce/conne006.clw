

   MEMBER('connect.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('CONNE006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CONNE004.INC'),ONCE        !Req'd for module callout resolution
                     END


CheckForLabels       PROCEDURE                        ! Declare Procedure
  CODE
!A quick check
 relate:Labels.open
 set(Labels)
 if access:Labels.next() then
    !No labels don't do anything
 ELSE
    printLabels
 END
 relate:Labels.close
