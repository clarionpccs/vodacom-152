

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBK01001.INC'),ONCE        !Local module procedure declarations
                     END





Browse_Jobs PROCEDURE                                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::26:TAGFLAG         BYTE(0)
DASBRW::26:TAGMOUSE        BYTE(0)
DASBRW::26:TAGDISPSTATUS   BYTE(0)
DASBRW::26:QUEUE          QUEUE
Pointer20                     LIKE(glo:Pointer20)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
save_sub_id          USHORT,AUTO
save_jot_id          USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_aus_id          USHORT,AUTO
invoice_number_temp  LONG
tmp:accountnumber    STRING(15)
tmp:labelerror       STRING('0 {19}')
tmp:OldConsignNo     STRING(20)
save_job_ali_id      USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
sav:path             STRING(255)
tmp:ConsignNo        STRING(30)
despatch_type_temp   STRING(3)
status2_temp         STRING(30)
rea_temp             STRING('REA')
yes_temp             STRING('YES')
no_temp              STRING('NO')
save_lac_id          USHORT,AUTO
save_xca_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
blank_temp           STRING(1)
count_jobs_temp      REAL
save_xch_id          USHORT,AUTO
select_courier_temp  STRING('ALL')
Select_Trade_Account_Temp STRING('ALL')
Location_Temp        STRING(30)
LocalRequest         LONG
multiple_despatch_temp STRING(3)
FilesOpened          BYTE
Completed_Temp       STRING('ALL')
Completed2_Temp      STRING('ALL')
Invoiced_Temp        STRING(3)
Collected_Temp       STRING(3)
Paid_Temp            STRING(3)
status_temp          STRING(30)
model_unit_temp      STRING(60)
Overdue_Temp         STRING(1)
account_number_temp  STRING(15)
engineer_temp        STRING(3)
address_temp         STRING(90)
model_unit2_temp     STRING(60)
model_unit3_temp     STRING(60)
model_number_temp    STRING(30)
Overdue2_temp        STRING('NO {1}')
batch_number_temp    REAL(1)
courier_temp         STRING(30)
account_number2_temp STRING(15)
tag_temp             STRING(1)
tag_count_temp       REAL
despatch_batch_number_temp REAL
despatch_label_type_temp STRING(3)
despatch_label_number_temp STRING(30)
Warranty_Temp        STRING(1)
model_unit4_temp     STRING(60)
model_unit5_temp     STRING(60)
select_trade_account2_temp STRING('ALL')
account_number3_temp STRING(15)
select_model_number_temp STRING('ALL')
model_number2_temp   STRING(30)
Company_Name_Temp    STRING(30)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Postcode_Temp        STRING(10)
status3_temp         STRING(30)
status4_temp         STRING(30)
tmp:SelectLocation   BYTE(1)
tmp:JobLocation      STRING(30)
tmp:jobnumber        STRING(20)
tmp:branchID         STRING(20)
tmp:ExchangeText     STRING(60)
tmp:Address          STRING(255)
tmp:Location         STRING(30)
tmp:ModelUnit1       STRING(255)
tmp:ModelUnit2       STRING(255)
tmp:ModelUnit3       STRING(255)
tmp:StatusDate       DATE
tmp:ExchangeStatusDate DATE
tmp:LoanStatusDate   DATE
tmp:LocationStatus   STRING(30)
tmp:PrintDespatchNote BYTE(0)
tmp:FirstRecord      BYTE(0)
tmp:BatchNumber      LONG
tmp:BatchCreated     BYTE(0)
tmp:AccountSearchOrder BYTE(0)
tmp:GlobalIMEINumber STRING(30)
tmp:orderNumber      STRING(30)
del:JobNumber        LONG
tmp:BrowseFlag       BYTE(0)
tmp:LoanText         STRING(255)
tmp:DisableLocation  BYTE(0)
Tmp:Batch_No         STRING(30)
tmp:AllJobsType      BYTE(0)
tmp:Location2        STRING(30)
tmp:StatusSendToRRC  STRING(30)
tmp:RRCLocation      STRING(30)
tmp:JobDays          STRING(4)
tmp:JobStatusDays    STRING(4)
tmp:ExcStatusDays    STRING(30)
tmp:LoaStatusDays    STRING(4)
tmp:ExchangeText2    STRING(60)
tmp:LoanText2        STRING(60)
tmp:DespatchLocation STRING(30)
SMSErrorFlag         BYTE
Count                LONG
locAuditNotes        STRING(255)
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:date_booked)
                       PROJECT(job:Current_Status)
                       PROJECT(job:Location)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Surname)
                       PROJECT(job:Mobile_Number)
                       PROJECT(job:Postcode)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Current_Courier)
                       PROJECT(job:Loan_Status)
                       PROJECT(job:Exchange_Status)
                       PROJECT(job:Incoming_Consignment_Number)
                       PROJECT(job:Exchange_Unit_Number)
                       PROJECT(job:Loan_Unit_Number)
                       PROJECT(job:Address_Line1)
                       PROJECT(job:Date_Despatched)
                       PROJECT(job:time_booked)
                       PROJECT(job:Company_Name)
                       PROJECT(job:Address_Line2)
                       PROJECT(job:Address_Line3)
                       PROJECT(job:Engineer)
                       PROJECT(job:Completed)
                       PROJECT(job:Workshop)
                       PROJECT(job:Despatched)
                       JOIN(jobe:RefNumberKey,job:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Ref_Number_NormalFG LONG                          !Normal forground color
job:Ref_Number_NormalBG LONG                          !Normal background color
job:Ref_Number_SelectedFG LONG                        !Selected forground color
job:Ref_Number_SelectedBG LONG                        !Selected background color
tmp:jobnumber          LIKE(tmp:jobnumber)            !List box control field - type derived from local data
tmp:jobnumber_NormalFG LONG                           !Normal forground color
tmp:jobnumber_NormalBG LONG                           !Normal background color
tmp:jobnumber_SelectedFG LONG                         !Selected forground color
tmp:jobnumber_SelectedBG LONG                         !Selected background color
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Account_Number_NormalFG LONG                      !Normal forground color
job:Account_Number_NormalBG LONG                      !Normal background color
job:Account_Number_SelectedFG LONG                    !Selected forground color
job:Account_Number_SelectedBG LONG                    !Selected background color
model_unit_temp        LIKE(model_unit_temp)          !List box control field - type derived from local data
model_unit_temp_NormalFG LONG                         !Normal forground color
model_unit_temp_NormalBG LONG                         !Normal background color
model_unit_temp_SelectedFG LONG                       !Selected forground color
model_unit_temp_SelectedBG LONG                       !Selected background color
job:date_booked        LIKE(job:date_booked)          !List box control field - type derived from field
job:date_booked_NormalFG LONG                         !Normal forground color
job:date_booked_NormalBG LONG                         !Normal background color
job:date_booked_SelectedFG LONG                       !Selected forground color
job:date_booked_SelectedBG LONG                       !Selected background color
job:Current_Status     LIKE(job:Current_Status)       !List box control field - type derived from field
job:Current_Status_NormalFG LONG                      !Normal forground color
job:Current_Status_NormalBG LONG                      !Normal background color
job:Current_Status_SelectedFG LONG                    !Selected forground color
job:Current_Status_SelectedBG LONG                    !Selected background color
job:Location           LIKE(job:Location)             !List box control field - type derived from field
job:Location_NormalFG  LONG                           !Normal forground color
job:Location_NormalBG  LONG                           !Normal background color
job:Location_SelectedFG LONG                          !Selected forground color
job:Location_SelectedBG LONG                          !Selected background color
Overdue_Temp           LIKE(Overdue_Temp)             !List box control field - type derived from local data
Overdue_Temp_Icon      LONG                           !Entry's icon ID
Completed_Temp         LIKE(Completed_Temp)           !List box control field - type derived from local data
Completed_Temp_Icon    LONG                           !Entry's icon ID
Invoiced_Temp          LIKE(Invoiced_Temp)            !List box control field - type derived from local data
Invoiced_Temp_Icon     LONG                           !Entry's icon ID
Warranty_Temp          LIKE(Warranty_Temp)            !List box control field - type derived from local data
Warranty_Temp_Icon     LONG                           !Entry's icon ID
Collected_Temp         LIKE(Collected_Temp)           !List box control field - type derived from local data
Collected_Temp_Icon    LONG                           !Entry's icon ID
Paid_Temp              LIKE(Paid_Temp)                !List box control field - type derived from local data
Paid_Temp_Icon         LONG                           !Entry's icon ID
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:ESN_NormalFG       LONG                           !Normal forground color
job:ESN_NormalBG       LONG                           !Normal background color
job:ESN_SelectedFG     LONG                           !Selected forground color
job:ESN_SelectedBG     LONG                           !Selected background color
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:MSN_NormalFG       LONG                           !Normal forground color
job:MSN_NormalBG       LONG                           !Normal background color
job:MSN_SelectedFG     LONG                           !Selected forground color
job:MSN_SelectedBG     LONG                           !Selected background color
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Order_Number_NormalFG LONG                        !Normal forground color
job:Order_Number_NormalBG LONG                        !Normal background color
job:Order_Number_SelectedFG LONG                      !Selected forground color
job:Order_Number_SelectedBG LONG                      !Selected background color
job:Surname            LIKE(job:Surname)              !List box control field - type derived from field
job:Surname_NormalFG   LONG                           !Normal forground color
job:Surname_NormalBG   LONG                           !Normal background color
job:Surname_SelectedFG LONG                           !Selected forground color
job:Surname_SelectedBG LONG                           !Selected background color
job:Mobile_Number      LIKE(job:Mobile_Number)        !List box control field - type derived from field
job:Mobile_Number_NormalFG LONG                       !Normal forground color
job:Mobile_Number_NormalBG LONG                       !Normal background color
job:Mobile_Number_SelectedFG LONG                     !Selected forground color
job:Mobile_Number_SelectedBG LONG                     !Selected background color
job:Postcode           LIKE(job:Postcode)             !List box control field - type derived from field
job:Postcode_NormalFG  LONG                           !Normal forground color
job:Postcode_NormalBG  LONG                           !Normal background color
job:Postcode_SelectedFG LONG                          !Selected forground color
job:Postcode_SelectedBG LONG                          !Selected background color
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Model_Number_NormalFG LONG                        !Normal forground color
job:Model_Number_NormalBG LONG                        !Normal background color
job:Model_Number_SelectedFG LONG                      !Selected forground color
job:Model_Number_SelectedBG LONG                      !Selected background color
job:Unit_Type          LIKE(job:Unit_Type)            !List box control field - type derived from field
job:Unit_Type_NormalFG LONG                           !Normal forground color
job:Unit_Type_NormalBG LONG                           !Normal background color
job:Unit_Type_SelectedFG LONG                         !Selected forground color
job:Unit_Type_SelectedBG LONG                         !Selected background color
address_temp           LIKE(address_temp)             !List box control field - type derived from local data
address_temp_NormalFG  LONG                           !Normal forground color
address_temp_NormalBG  LONG                           !Normal background color
address_temp_SelectedFG LONG                          !Selected forground color
address_temp_SelectedBG LONG                          !Selected background color
job:Consignment_Number LIKE(job:Consignment_Number)   !List box control field - type derived from field
job:Consignment_Number_NormalFG LONG                  !Normal forground color
job:Consignment_Number_NormalBG LONG                  !Normal background color
job:Consignment_Number_SelectedFG LONG                !Selected forground color
job:Consignment_Number_SelectedBG LONG                !Selected background color
job:Current_Courier    LIKE(job:Current_Courier)      !List box control field - type derived from field
job:Current_Courier_NormalFG LONG                     !Normal forground color
job:Current_Courier_NormalBG LONG                     !Normal background color
job:Current_Courier_SelectedFG LONG                   !Selected forground color
job:Current_Courier_SelectedBG LONG                   !Selected background color
despatch_type_temp     LIKE(despatch_type_temp)       !List box control field - type derived from local data
despatch_type_temp_NormalFG LONG                      !Normal forground color
despatch_type_temp_NormalBG LONG                      !Normal background color
despatch_type_temp_SelectedFG LONG                    !Selected forground color
despatch_type_temp_SelectedBG LONG                    !Selected background color
status2_temp           LIKE(status2_temp)             !List box control field - type derived from local data
status2_temp_NormalFG  LONG                           !Normal forground color
status2_temp_NormalBG  LONG                           !Normal background color
status2_temp_SelectedFG LONG                          !Selected forground color
status2_temp_SelectedBG LONG                          !Selected background color
job:Loan_Status        LIKE(job:Loan_Status)          !List box control field - type derived from field
job:Loan_Status_NormalFG LONG                         !Normal forground color
job:Loan_Status_NormalBG LONG                         !Normal background color
job:Loan_Status_SelectedFG LONG                       !Selected forground color
job:Loan_Status_SelectedBG LONG                       !Selected background color
job:Exchange_Status    LIKE(job:Exchange_Status)      !List box control field - type derived from field
job:Exchange_Status_NormalFG LONG                     !Normal forground color
job:Exchange_Status_NormalBG LONG                     !Normal background color
job:Exchange_Status_SelectedFG LONG                   !Selected forground color
job:Exchange_Status_SelectedBG LONG                   !Selected background color
job:Incoming_Consignment_Number LIKE(job:Incoming_Consignment_Number) !List box control field - type derived from field
job:Incoming_Consignment_Number_NormalFG LONG         !Normal forground color
job:Incoming_Consignment_Number_NormalBG LONG         !Normal background color
job:Incoming_Consignment_Number_SelectedFG LONG       !Selected forground color
job:Incoming_Consignment_Number_SelectedBG LONG       !Selected background color
job:Exchange_Unit_Number LIKE(job:Exchange_Unit_Number) !List box control field - type derived from field
job:Exchange_Unit_Number_NormalFG LONG                !Normal forground color
job:Exchange_Unit_Number_NormalBG LONG                !Normal background color
job:Exchange_Unit_Number_SelectedFG LONG              !Selected forground color
job:Exchange_Unit_Number_SelectedBG LONG              !Selected background color
job:Loan_Unit_Number   LIKE(job:Loan_Unit_Number)     !List box control field - type derived from field
job:Loan_Unit_Number_NormalFG LONG                    !Normal forground color
job:Loan_Unit_Number_NormalBG LONG                    !Normal background color
job:Loan_Unit_Number_SelectedFG LONG                  !Selected forground color
job:Loan_Unit_Number_SelectedBG LONG                  !Selected background color
tmp:ExchangeText       LIKE(tmp:ExchangeText)         !List box control field - type derived from local data
tmp:ExchangeText_NormalFG LONG                        !Normal forground color
tmp:ExchangeText_NormalBG LONG                        !Normal background color
tmp:ExchangeText_SelectedFG LONG                      !Selected forground color
tmp:ExchangeText_SelectedBG LONG                      !Selected background color
job:Address_Line1      LIKE(job:Address_Line1)        !List box control field - type derived from field
job:Address_Line1_NormalFG LONG                       !Normal forground color
job:Address_Line1_NormalBG LONG                       !Normal background color
job:Address_Line1_SelectedFG LONG                     !Selected forground color
job:Address_Line1_SelectedBG LONG                     !Selected background color
tmp:BrowseFlag         LIKE(tmp:BrowseFlag)           !List box control field - type derived from local data
tmp:BrowseFlag_NormalFG LONG                          !Normal forground color
tmp:BrowseFlag_NormalBG LONG                          !Normal background color
tmp:BrowseFlag_SelectedFG LONG                        !Selected forground color
tmp:BrowseFlag_SelectedBG LONG                        !Selected background color
tmp:LoanText           LIKE(tmp:LoanText)             !List box control field - type derived from local data
tmp:LoanText_NormalFG  LONG                           !Normal forground color
tmp:LoanText_NormalBG  LONG                           !Normal background color
tmp:LoanText_SelectedFG LONG                          !Selected forground color
tmp:LoanText_SelectedBG LONG                          !Selected background color
job:Date_Despatched    LIKE(job:Date_Despatched)      !List box control field - type derived from field
job:Date_Despatched_NormalFG LONG                     !Normal forground color
job:Date_Despatched_NormalBG LONG                     !Normal background color
job:Date_Despatched_SelectedFG LONG                   !Selected forground color
job:Date_Despatched_SelectedBG LONG                   !Selected background color
Tmp:Batch_No           LIKE(Tmp:Batch_No)             !List box control field - type derived from local data
Tmp:Batch_No_NormalFG  LONG                           !Normal forground color
Tmp:Batch_No_NormalBG  LONG                           !Normal background color
Tmp:Batch_No_SelectedFG LONG                          !Selected forground color
Tmp:Batch_No_SelectedBG LONG                          !Selected background color
tmp:ExchangeStatusDate LIKE(tmp:ExchangeStatusDate)   !List box control field - type derived from local data
tmp:ExchangeStatusDate_NormalFG LONG                  !Normal forground color
tmp:ExchangeStatusDate_NormalBG LONG                  !Normal background color
tmp:ExchangeStatusDate_SelectedFG LONG                !Selected forground color
tmp:ExchangeStatusDate_SelectedBG LONG                !Selected background color
tmp:LoanStatusDate     LIKE(tmp:LoanStatusDate)       !List box control field - type derived from local data
tmp:LoanStatusDate_NormalFG LONG                      !Normal forground color
tmp:LoanStatusDate_NormalBG LONG                      !Normal background color
tmp:LoanStatusDate_SelectedFG LONG                    !Selected forground color
tmp:LoanStatusDate_SelectedBG LONG                    !Selected background color
tmp:StatusDate         LIKE(tmp:StatusDate)           !List box control field - type derived from local data
tmp:StatusDate_NormalFG LONG                          !Normal forground color
tmp:StatusDate_NormalBG LONG                          !Normal background color
tmp:StatusDate_SelectedFG LONG                        !Selected forground color
tmp:StatusDate_SelectedBG LONG                        !Selected background color
tmp:LoaStatusDays      LIKE(tmp:LoaStatusDays)        !List box control field - type derived from local data
tmp:LoaStatusDays_NormalFG LONG                       !Normal forground color
tmp:LoaStatusDays_NormalBG LONG                       !Normal background color
tmp:LoaStatusDays_SelectedFG LONG                     !Selected forground color
tmp:LoaStatusDays_SelectedBG LONG                     !Selected background color
tmp:ExcStatusDays      LIKE(tmp:ExcStatusDays)        !List box control field - type derived from local data
tmp:ExcStatusDays_NormalFG LONG                       !Normal forground color
tmp:ExcStatusDays_NormalBG LONG                       !Normal background color
tmp:ExcStatusDays_SelectedFG LONG                     !Selected forground color
tmp:ExcStatusDays_SelectedBG LONG                     !Selected background color
tmp:JobStatusDays      LIKE(tmp:JobStatusDays)        !List box control field - type derived from local data
tmp:JobStatusDays_NormalFG LONG                       !Normal forground color
tmp:JobStatusDays_NormalBG LONG                       !Normal background color
tmp:JobStatusDays_SelectedFG LONG                     !Selected forground color
tmp:JobStatusDays_SelectedBG LONG                     !Selected background color
tmp:JobDays            LIKE(tmp:JobDays)              !List box control field - type derived from local data
tmp:JobDays_NormalFG   LONG                           !Normal forground color
tmp:JobDays_NormalBG   LONG                           !Normal background color
tmp:JobDays_SelectedFG LONG                           !Selected forground color
tmp:JobDays_SelectedBG LONG                           !Selected background color
job:time_booked        LIKE(job:time_booked)          !List box control field - type derived from field
job:time_booked_NormalFG LONG                         !Normal forground color
job:time_booked_NormalBG LONG                         !Normal background color
job:time_booked_SelectedFG LONG                       !Selected forground color
job:time_booked_SelectedBG LONG                       !Selected background color
job:Company_Name       LIKE(job:Company_Name)         !Browse hot field - type derived from field
job:Address_Line2      LIKE(job:Address_Line2)        !Browse hot field - type derived from field
job:Address_Line3      LIKE(job:Address_Line3)        !Browse hot field - type derived from field
Postcode_Temp          LIKE(Postcode_Temp)            !Browse hot field - type derived from local data
Address_Line3_Temp     LIKE(Address_Line3_Temp)       !Browse hot field - type derived from local data
Address_Line2_Temp     LIKE(Address_Line2_Temp)       !Browse hot field - type derived from local data
Address_Line1_Temp     LIKE(Address_Line1_Temp)       !Browse hot field - type derived from local data
Company_Name_Temp      LIKE(Company_Name_Temp)        !Browse hot field - type derived from local data
tmp:Address            LIKE(tmp:Address)              !Browse hot field - type derived from local data
tmp:LoanText2          LIKE(tmp:LoanText2)            !Browse hot field - type derived from local data
tmp:ExchangeText2      LIKE(tmp:ExchangeText2)        !Browse hot field - type derived from local data
job:Engineer           LIKE(job:Engineer)             !Browse key field - type derived from field
job:Completed          LIKE(job:Completed)            !Browse key field - type derived from field
job:Workshop           LIKE(job:Workshop)             !Browse key field - type derived from field
job:Despatched         LIKE(job:Despatched)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,48)                !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Jobs File'),AT(0,0,680,429),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR,NOMERGE
                         MENU('Print Routines'),USE(?PrintRoutines),ICON('Printsm.gif')
                           ITEM('Job Receipt'),USE(?JobReceipt)
                           ITEM('Job Card'),USE(?JobCard)
                           ITEM('Job Label'),USE(?JobLabel)
                           ITEM,SEPARATOR
                           ITEM('Delivery Label'),USE(?DeliveryLabel)
                           ITEM('Collection Label'),USE(?CollectionLabel)
                           ITEM,SEPARATOR
                           ITEM('Letters'),USE(?Letters)
                           ITEM('Mail Merge'),USE(?MailMerge)
                           ITEM,SEPARATOR
                           ITEM('Despatch Note'),USE(?DespatchNote)
                           ITEM,SEPARATOR
                           ITEM('Exchange Unit Label'),USE(?Menu1ExchangeUnitLabel)
                           ITEM('Loan Unit Label'),USE(?Menu1LoanUnitLabel)
                           ITEM,SEPARATOR
                           ITEM('Estimate'),USE(?Menu1Estimate)
                           ITEM('Invoice'),USE(?Menu1Invoice)
                         END
                       END
                       PROMPT('Browse The Job File'),AT(8,11),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,9,640,13),USE(?PanelFalse),FILL(09A6A7CH)
                       GROUP('Status Details'),AT(528,236,144,144),USE(?Group4),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Job Status'),AT(532,246),USE(?Prompt26),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         STRING(@d6b),AT(608,246),USE(tmp:StatusDate),RIGHT,FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(532,254,124,10),USE(job:Current_Status),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         PROMPT('Job Days: '),AT(532,266),USE(?Prompt33),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s5),AT(564,266),USE(tmp:JobDays),FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Status Days:'),AT(596,266),USE(?Prompt34),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s4),AT(640,266),USE(tmp:JobStatusDays),FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Exchange Status'),AT(532,281),USE(?Prompt26:2),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         STRING(@d6b),AT(608,281),USE(tmp:ExchangeStatusDate),RIGHT,FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(532,290,124,10),USE(job:Exchange_Status),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         PROMPT('Status Days:'),AT(592,302),USE(?Prompt34:2),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s4),AT(636,302),USE(tmp:ExcStatusDays),FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s60),AT(532,312,136,8),USE(tmp:ExchangeText),TRN,LEFT,FONT('Tahoma',7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s60),AT(532,318,136,8),USE(tmp:ExchangeText2),TRN,LEFT,FONT('Tahoma',7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Loan Status'),AT(532,332),USE(?Prompt26:3),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         STRING(@d6b),AT(608,332),USE(tmp:LoanStatusDate),RIGHT,FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@s30),AT(532,342,124,10),USE(job:Loan_Status),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         PROMPT('Status Days:'),AT(596,354),USE(?Prompt34:3),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s4),AT(640,354),USE(tmp:LoaStatusDays),FONT('Tahoma',7,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s255),AT(532,364,136,8),USE(tmp:LoanText),TRN,LEFT,FONT('Tahoma',7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s255),AT(532,372,136,8),USE(tmp:LoanText2),TRN,LEFT,FONT('Tahoma',7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       BUTTON,AT(648,3),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,11),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(8,119,516,230),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),TIP('Right Click For Print Options'),ALRT(MouseLeft2),ALRT(EnterKey),FORMAT('0R(2)|FM*~Job No~L@s7@62L(2)|FM*~Job No~@s20@62L(2)|FM*~Account No~@s15@94L(2)|F' &|
   'M*~Model Number / Unit Type~@s60@42R(2)|FM*~Booked~@d6b@120L(2)|FM*~Status~@s30@' &|
   '70L(2)|FM*~Location~@s30@10CFI~O~@s1@10CFI~F~@s1@10CFI~C~@s1@10CFI~W~@s1@10CFI~D' &|
   '~@s1@10C|FI~P~@s1@#46#68L(2)|FM*~ESN/IMEI~@s16@#48#11L(2)FI~/~@s1@#53#68L(2)|FM*' &|
   '~MSN~@s15@#55#96L(2)|FM*~Order Number~@s30@60L(2)|FM*~Surname~@s30@60L(2)|FM*~Mo' &|
   'bile Number~@s15@46L(2)|FM*~Postcode~@s10@60L(2)|FM*~Model Number~@s30@60L(2)|FM' &|
   '*~Unit Type~@s30@220L(2)|FM*~Address~@s90@60L(2)|FM*~Consignment No~@s30@80L(2)|' &|
   'FM*~Courier~@s30@34L(2)|FM*~Type~@s3@124L(2)|FM*~Status~@s30@92L(2)|FM*~Loan Sta' &|
   'tus~@s30@92L(2)|FM*~Exchange Status~@s30@60L(2)|FM*~Consignment No~@s30@32L(2)|F' &|
   'M*~Exchange Unit Number~@s8@32L(2)|FM*~Loan Unit Number~@s8@120L(2)|FM*~tmp : Ex' &|
   'change Text~@s30@128L(2)|FM*~Location~@s30@4L(2)|FM*~Browse Flag~@n1@1020L(2)|FM' &|
   '*~Loan Text~@s255@40L(2)|FM*~Date Despatched~@d6b@60L(2)|FM*~Batch Number~@s30@4' &|
   '0L(2)|FM*~Changed~@d6@40L(2)|FM*~Changed~@d6@40L(2)|FM*~Changed~@d6@0L(2)|FM*~Lo' &|
   'an Status Days~@s4@0L(2)|FM*~Exchange Status Days~@s30@0L(2)|FM*~Job Status Days' &|
   '~@s4@0L(2)|FM*~Job Days~@s4@0L(2)|FM*~time booked~@t1@'),FROM(Queue:Browse:1)
                       BUTTON,AT(8,352),USE(?CreateNewJob),TRN,FLAT,LEFT,ICON('newjobp.jpg')
                       BUTTON,AT(80,352),USE(?Change:3),TRN,FLAT,LEFT,ICON('custserp.jpg')
                       TEXT,AT(344,55,140,56),USE(tmp:Address),SKIP,FONT('Arial',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       GROUP,AT(532,47,132,84),USE(?Button_Group)
                         BUTTON,AT(532,47),USE(?MarkLine),TRN,FLAT,ICON('marklinp.jpg')
                         BUTTON,AT(600,49),USE(?Print_Routines),TRN,FLAT,ICON('prnroutp.jpg')
                         BUTTON,AT(600,76),USE(?ChangeStatus),TRN,FLAT,ICON('chastap.jpg')
                         BUTTON,AT(532,76),USE(?Payment_Details),TRN,FLAT,ICON('paydetp.jpg')
                         BUTTON,AT(532,105),USE(?CancelJob),TRN,FLAT,ICON('canjobp.jpg')
                         BUTTON,AT(600,105),USE(?FailedDelivery),TRN,FLAT,ICON('faildelp.jpg')
                         BUTTON,AT(152,351),USE(?RapidUpdateButton:2),TRN,FLAT,ICON('engupdp.jpg')
                       END
                       BUTTON,AT(460,385),USE(?KeyColour),TRN,FLAT,LEFT,ICON('keyp.jpg')
                       SHEET,AT(4,27,672,354),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Job No'),USE(?Job_Number_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH,09A6A7CH,COLOR:White)
                           OPTION('Location'),AT(8,47,124,26),USE(tmp:AllJobsType),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All Jobs'),AT(12,57),USE(?tmp:AllJobsType:Radio1),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Individual'),AT(68,57),USE(?tmp:AllJobsType:Radio2),SKIP,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           ENTRY(@s30),AT(140,57,124,10),USE(tmp:Location2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Location'),TIP('Location'),UPR
                           BUTTON,AT(268,51),USE(?LookupLocation2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s8b),AT(8,91,60,10),USE(job:Ref_Number),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('Job Number'),AT(8,83),USE(?Prompt7),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('View'),AT(312,361,31,21),USE(?ViewButton),HIDE
                           BUTTON,AT(540,385),USE(?Count_Jobs),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By I.M.E.I. No'),USE(?Esn_tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Global I.M.E.I. Number Search'),AT(148,83),USE(?tmp:GlobalIMEINumber:Prompt),TRN,LEFT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(148,91,116,10),USE(tmp:GlobalIMEINumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Global I.M.E.I. Number'),TIP('Global I.M.E.I. Number'),UPR
                           ENTRY(@s20),AT(8,91,124,10),USE(job:ESN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,83),USE(?Prompt8),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:2),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By M.S.N.'),USE(?msn_tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,91,124,10),USE(job:MSN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(8,83),USE(?Prompt9),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:3),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By Account No'),USE(?Account_No_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,55,124,10),USE(tmp:orderNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Order Number'),TIP('Order Number'),UPR
                           OPTION('Search Order'),AT(244,73,92,32),USE(tmp:AccountSearchOrder),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('By Order Number'),AT(252,81),USE(?tmp:AccountSearchOrder:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('By Job Number'),AT(252,92),USE(?tmp:AccountSearchOrder:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                           END
                           ENTRY(@s15),AT(8,71,124,10),USE(account_number_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(132,67),USE(?Lookup_Account_number),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Global Order Number Search'),AT(8,47),USE(?Prompt4),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(160,71),USE(?Prompt4:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,95,76,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(88,95,64,10),USE(job:Ref_Number,,?job:Ref_Number:10),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Order Number'),AT(8,87),USE(?OrderNumber),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(88,87),USE(?JobNumber),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:4),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By Surname'),USE(?surname_tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,91,124,10),USE(job:Surname),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Surname'),AT(8,83),USE(?Prompt11),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(8,51),USE(?SearchByAddress),TRN,FLAT,LEFT,ICON('seraddp.jpg')
                           BUTTON,AT(536,385),USE(?Count_Jobs:5),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By Statuses'),USE(?status_type_tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(8,47,300,64),USE(?StatusSheet)
                             TAB('Job Status'),USE(?Tab14),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                               OPTION('Select Location'),AT(12,60,100,20),USE(tmp:SelectLocation),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                                 RADIO('All'),AT(20,68),USE(?tmp:SelectLocation:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(07D5564H),VALUE('1')
                                 RADIO('Individual'),AT(56,68),USE(?tmp:SelectLocation:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(07D5564H),VALUE('2')
                               END
                               ENTRY(@s30),AT(116,68,124,10),USE(tmp:JobLocation),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                               BUTTON,AT(244,65),USE(?Lookup_Location),SKIP,TRN,FLAT,HIDE,ICON('lookup2p.jpg')
                               PROMPT('Status Type'),AT(12,84),USE(?Prompt5),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(12,92,124,10),USE(status_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                               BUTTON,AT(140,89),USE(?Lookup_Job_Status),SKIP,TRN,FLAT,ICON('lookup2p.jpg')
                               ENTRY(@s8b),AT(172,92,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:2),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               PROMPT('Job Number'),AT(172,84),USE(?Prompt5:2),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                             TAB('Exchange Status'),USE(?Tab15),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                               PROMPT('Exchange Status'),AT(12,63),USE(?Prompt25),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(12,71,124,10),USE(status3_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                               BUTTON,AT(140,67),USE(?Lookup_Status3),SKIP,TRN,FLAT,ICON('lookup2p.jpg')
                               PROMPT('Job Number'),AT(12,84),USE(?Prompt25:2),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s8b),AT(12,92,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:7),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             END
                             TAB('Loan Status'),USE(?Tab16),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(07D5564H)
                               PROMPT('Loan Status'),AT(12,63),USE(?JOB:Ref_Number:Prompt:3),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(12,71,124,10),USE(status4_temp),FONT('Tahoma',,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey)
                               BUTTON,AT(140,67),USE(?Lookup_Status4),SKIP,TRN,FLAT,ICON('lookup2p.jpg')
                               PROMPT('Job Number'),AT(12,84),USE(?JOB:Ref_Number:Prompt:2),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s8b),AT(12,92,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:8),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             END
                           END
                           BUTTON,AT(536,153),USE(?ButtonSMSTaggedJobs),TRN,FLAT,ICON('smstagjp.jpg')
                           BUTTON,AT(228,351),USE(?ButtonTagFromCSV),TRN,FLAT,ICON('tagjobcp.jpg')
                           BUTTON,AT(536,385),USE(?Count_Jobs:6),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('Exch. Status'),USE(?Exchange_Status),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:15),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('Loan Status'),USE(?Loan_Status),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(540,385),USE(?Count_Jobs:8),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By Mobile No.'),USE(?mobile_tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(8,91,64,10),USE(job:Mobile_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Mobile Number'),AT(8,83),USE(?Prompt13),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:7),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By Model No'),USE(?Model_No_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,68,124,10),USE(model_number_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(136,65),USE(?Lookup_Model_Number),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model Number'),AT(8,60),USE(?Prompt6),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,91,124,10),USE(job:Unit_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Unit Type'),AT(8,83),USE(?Prompt15),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:9),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('By Engineer'),USE(?Engineer_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s3),AT(8,65,24,10),USE(engineer_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(36,60),USE(?Lookup_Engineer),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Engineer'),AT(8,57),USE(?Prompt16),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Job Type'),AT(136,73,160,28),USE(Completed2_Temp,,?Completed2_Temp:2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Incomplete'),AT(144,84),USE(?Completed_Temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('NO')
                             RADIO('All'),AT(264,84),USE(?Completed_Temp:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Completed'),AT(204,84),USE(?Completed_Temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES')
                           END
                           ENTRY(@s8b),AT(8,91,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:3),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Job Number'),AT(8,83),USE(?JobNumber:Prompt),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(536,385),USE(?Count_Jobs:10),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('Unalloc. Jobs'),USE(?Unallocated_Jobs_Tab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8b),AT(8,91,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:4),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Job Number'),AT(8,83),USE(?Prompt18),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('Unallocated Jobs Report'),AT(340,351,68,16),USE(?UnallocatedJobsReport),HIDE,LEFT,ICON(ICON:Print1)
                           BUTTON,AT(536,385),USE(?Count_Jobs:11),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                         TAB('Despatch'),USE(?Ready_To_Despatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Select Courier'),AT(8,44,92,20),USE(select_courier_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(16,52),USE(?select_courier_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Individual'),AT(44,52),USE(?select_courier_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                           END
                           ENTRY(@s30),AT(104,52,112,10),USE(courier_temp),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(220,49),USE(?Lookup_Courier),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           PROMPT('Job Number:'),AT(204,95),USE(?Prompt28),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Select Trade Account'),AT(8,67,92,20),USE(Select_Trade_Account_Temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('All'),AT(16,73),USE(?Select_Trade_Account_Temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                             RADIO('Individual'),AT(44,73),USE(?Select_Trade_Account_Temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IND')
                           END
                           ENTRY(@s15),AT(104,75,112,10),USE(account_number2_temp),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(220,71),USE(?Lookup_Account_Number:2),SKIP,TRN,FLAT,HIDE,ICON('lookupp.jpg')
                           STRING(@s8),AT(256,89,80,22),USE(job:Ref_Number,,?job:Ref_Number:9),TRN,LEFT,FONT('Tahoma',18,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP('Despatch'),AT(528,129,144,68),USE(?Button_Group2),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(604,139),USE(?Multiple_Despatch2),TRN,FLAT,LEFT,ICON('muldesp.jpg')
                             BUTTON,AT(536,139),USE(?Individual_Despatch),TRN,FLAT,LEFT,ICON('inddesp.jpg')
                             BUTTON,AT(536,167),USE(?Validate_Esn),TRN,FLAT,LEFT,ICON('valimeip.jpg')
                             BUTTON,AT(604,167),USE(?Change_Courier),TRN,FLAT,LEFT,ICON('chancoup.jpg')
                           END
                           BUTTON('sho&W tags'),AT(12,287,56,16),USE(?DASSHOWTAG),HIDE
                           ENTRY(@s8b),AT(8,97,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:6),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Job Number'),AT(8,89),USE(?Prompt23),TRN,FONT(,7,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON('&Rev tags'),AT(12,271,56,16),USE(?DASREVTAG),HIDE
                           BUTTON,AT(536,385),USE(?Count_Jobs:14),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                         END
                       END
                       GROUP,AT(300,355,224,21),USE(?TagButtons)
                         BUTTON,AT(307,351),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                         BUTTON,AT(383,351),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                         BUTTON,AT(455,351),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                       END
                       PROMPT('Address:'),AT(344,45),USE(?Prompt27),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       PROMPT('Date Booked:'),AT(8,387),USE(?Prompt35),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@d6b),AT(72,387),USE(job:date_booked),FONT(,10,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Time Booked:'),AT(8,399),USE(?Prompt35:2),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING(@t1),AT(72,399),USE(job:time_booked),FONT(,10,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort28:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(tmp:AllJobsType) = 0
BRW1::Sort29:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(tmp:AllJobsType) = 1
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 0
BRW1::Sort27:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 1
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 6 And Choice(?StatusSheet) = 1 And Upper(tmp:SelectLocation) = 1
BRW1::Sort25:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 6 And Choice(?StatusSheet) = 1 And Upper(tmp:SelectLocation) = 2
BRW1::Sort23:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 6 And Choice(?StatusSheet) = 2
BRW1::Sort24:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 6 And Choice(?StatusSheet) = 3
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 9
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 10
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 11 And Upper(completed2_temp) <> 'ALL'
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'ALL'
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 12
BRW1::Sort17:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'ALL' And Upper(Select_trade_account_temp) = 'ALL'
BRW1::Sort18:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'ALL' And Upper(select_trade_account_temp) = 'IND'
BRW1::Sort19:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'ALL'
BRW1::Sort20:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'IND'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort3:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 0
BRW1::Sort4:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 5
BRW1::Sort5:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 6 And Choice(?StatusSheet) = 1 And Upper(tmp:SelectLocation) = 1
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
WindowsDir      LPSTR(144)
SystemDir       WORD

ImportFile      FILE,DRIVER('BASIC'),PRE(IMP),NAME(filename3),CREATE,BINDABLE,THREAD
Record              Record
JobNumber       long        !Jobnumber   
Spare1          String(30)  !spare
Spare2          String(30)  !spare
Spare3          String(30)  !spare
                    End     !record
                End         !file definition
save_jpt_ali_id        ushort,auto
save_job_id   ushort,auto
TempFilePath         CSTRING(255)
    Map
LocalValidateAccessories    Procedure(),Byte
LocalValidateIMEI           Procedure(),Byte
LocalValidateAccountNumber  Procedure(),Byte
LocalFailedDeliveryAudit    Procedure(String    func:Type)
    End
!Save Entry Fields Incase Of Lookup
look:tmp:Location2                Like(tmp:Location2)
look:account_number_temp                Like(account_number_temp)
look:tmp:JobLocation                Like(tmp:JobLocation)
look:status_temp                Like(status_temp)
look:status3_temp                Like(status3_temp)
look:status4_temp                Like(status4_temp)
look:model_number_temp                Like(model_number_temp)
look:courier_temp                Like(courier_temp)
look:account_number2_temp                Like(account_number2_temp)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
DBH33:PopupText String(1000)
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::26:DASTAGONOFF Routine
  ! Before Embed Point: %DasTagBeforeTagOnOff) DESC(Tagging Before Tag On / Off) ARG(26)
  !Brw1.updatebuffer
  IF despatch_type_temp = 'HLD'
  
  Else!IF despatch_type_temp = 'HLD'
  
  ! After Embed Point: %DasTagBeforeTagOnOff) DESC(Tagging Before Tag On / Off) ARG(26)
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue20.Pointer20 = job:Ref_Number
   GET(glo:Queue20,glo:Queue20.Pointer20)
  IF ERRORCODE()
     glo:Queue20.Pointer20 = job:Ref_Number
     ADD(glo:Queue20,glo:Queue20.Pointer20)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue20)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 12
  ELSE
    Queue:Browse:1.tag_temp_Icon = 11
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
  ! Before Embed Point: %DasTagAfterTagOnOff) DESC(Tagging After Tag On / Off) ARG(26)
  End!IF despatch_type_temp = 'HLD'
  
  if tag_temp = '*'
      !TB12540 - Show contact history if there is a stick note
      !need to find one that has not been cancelled, and is valid for this request
      Access:conthist.clearkey(cht:KeyRefSticky)
      cht:Ref_Number = job:ref_number
      cht:SN_StickyNote = 'Y'
      Set(cht:KeyRefSticky,cht:KeyRefSticky)      
      Loop
          if access:Conthist.next() then break.
          IF cht:Ref_Number <> job:ref_number then break.
          if cht:SN_StickyNote <> 'Y' then break.
          if cht:SN_Completed <> 'Y' and cht:SN_Despatch = 'Y' then
              glo:select12 = job:ref_number
              Browse_Contact_History
              BREAK
          END
      END
  END !if tag_temp = '*'
  ! After Embed Point: %DasTagAfterTagOnOff) DESC(Tagging After Tag On / Off) ARG(26)
DASBRW::26:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue20)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue20.Pointer20 = job:Ref_Number
     ADD(glo:Queue20,glo:Queue20.Pointer20)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::26:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue20)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::26:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::26:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue20)
    GET(glo:Queue20,QR#)
    DASBRW::26:QUEUE = glo:Queue20
    ADD(DASBRW::26:QUEUE)
  END
  FREE(glo:Queue20)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::26:QUEUE.Pointer20 = job:Ref_Number
     GET(DASBRW::26:QUEUE,DASBRW::26:QUEUE.Pointer20)
    IF ERRORCODE()
       glo:Queue20.Pointer20 = job:Ref_Number
       ADD(glo:Queue20,glo:Queue20.Pointer20)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::26:DASSHOWTAG Routine
   CASE DASBRW::26:TAGDISPSTATUS
   OF 0
      DASBRW::26:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::26:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::26:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
TabChanging     Routine

    hide(?TagButtons)

    Case Choice(?CurrentTab)
        Of 1 !Job Number
            ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '94L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '112L(2)|FM*~Status~@s30@#26#' & |Status
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 2 !IMEI Number
            ?Browse:1{Prop:Format} =    '68L(2)|FM*~IMEI~@s16@#48#' & |IMEI Number
                                        '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '76L(2)|FM*~Order Number~@s30@#60#' & |Order Number
                                        '62L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 3 !MSN
            ?Browse:1{Prop:Format} =    '68L(2)|FM*~MSN~@s15@#55#' & |MSN Number
                                        '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '76L(2)|FM*~Order Number~@s30@#60#' & |Order Number
                                        '62L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 4 !Account Number
            ?Browse:1{Prop:Format} =    '76L(2)|FM*~Order Number~@s30@#60#' & |Order Number
                                        '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '40L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '112L(2)|FM*~Status~@s30@#26#' & |Status
                                        '40L(2)|FM*~Changed~@d6@#180#' & |Job Status Change
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 5 !Surname
            ?Browse:1{Prop:Format} =    '60L(2)|FM*~Surname~@s30@#65#' & |Surname
                                        '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '46L(2)|FM*~Postcode~@s10@#75#' & |Postcode
                                        '142L(2)|FM*~Address~@s90@#90#' & |Address
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 6 !Statuses
            unhide(?TagButtons)
            free(glo:Queue20)   !used for tags
            ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number was 70
                                        '58L(2)|FM*~Account No~@s15@#11#' & |Account Number Was 62
                                        '68L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type was 70
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '90L(2)|FM*~Status~@s30@#26#' & |Status  was 96
                                        '40L(2)|FM*~Changed~@d6@#180#' & |Job Status Change
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#11L(2)FI~�~@s1@#53#' ! Tag' !Icons
            Case Choice(?StatusSheet)
                Of 2 !Exchange Status
                    ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                                '58L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                                '68L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                                '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                                '90L(2)|FM*~Exchange Status~@s30@#120#' & |Exchange Status was 96
                                                '40L(2)|FM*~Changed~@d6@#170#' & |Exchange Status Change
                                                '70L(2)|FM*~Location~@s30@#31#' & |Location
                                                '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#11L(2)FI~�~@s1@#53#' ! Tag' !Icons

                Of 3 !Loan Status
                    ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                                '58L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                                '68L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                                '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                                '90L(2)|FM*~Loan Status~@s30@#115#' & |Loan Status  was 96
                                                '40L(2)|FM*~Changed~@d6@#175#' & |Loan Status Change
                                                '70L(2)|FM*~Location~@s30@#31#' & |Location
                                                '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#11L(2)FI~�~@s1@#53#' ! Tag' !Icons
            End ! Case Choice(?StatusSheet)
        Of 9 !Mobile Number
            ?Browse:1{Prop:Format} =    '60L(2)|FM*~Mobile Number~@s15@#70#' & |Mobile Number
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '82L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '106L(2)|FM*~Status~@s30@#26#' & |Status
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 10 !Model Number
            ?Browse:1{Prop:Format} =    '60L(2)|FM*~Unit Type~@s30@#85#' & |Unit Type
                                        '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '84L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '104L(2)|FM*~Status~@s30@#26#' & |Status
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 11!Engineer
            ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '62L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '104L(2)|FM*~Status~@s30@#26#' & |Status
                                        '40L(2)|FM*~Changed~@d6@#180#' & |Job Status Change
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons

        Of 12 !Unallocated Jobs
            ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '138L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '68L(2)|FM*~IMEI~@s16@#48#' & |IMEI Number
                                        '42R(2)|FM*~Booked~@d6b@#21#' & |Booked
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '70L(2)|FM*~Location~@s30@#31#' & |Location
                                        '10CFI~O~@s1@#36#10CFI~F~@s1@#38#10CFI~C~@s1@#40#10CFI~W~@s1@#42#10CFI~D~@s1@#44#10C|FI~P~@s1@#46#' !Icons
        Of 13 !Despatch
            unhide(?TagButtons)
            free(glo:Queue20)   !used for tags
            ?Browse:1{Prop:Format} =    '70L(2)|FM*~Job No~@s20@#6#' & |Job Number
                                        '62L(2)|FM*~Account No~@s15@#11#' & |Account Number
                                        '68L(2)|FM*~IMEI~@s16@#48#' & |IMEI Number
                                        '60L(2)|FM*~Batch Number~@s30@#165#' & |Batch Number
                                        '125L(2)|FM*~Model Number / Unit Type~@s60@#16#' & |Model Number / Unit Type
                                        '80L(2)|FM*~Courier~@s30@#100#' & |Courier
                                        '34L(2)|FM*~Type~@s3@#105#' & |Type
                                        '11L(2)FI~�~@s1@#53#' ! Tag




    End ! Choice(?CurrentTab)

!13 is despatch 6 is by stastus
Create_Invoice      Routine
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
    Set(defaults)
    access:defaults.next()
    invoice_number_temp = 0
    Case Missive('Are  you sure you want to print a single invoice?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes') 
        Of 2 ! Yes Button
            error# = 0
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = brw1.q.job:Ref_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                access:jobnotes.clearkey(jbn:RefNumberKey)
                jbn:RefNumber   = job:ref_number
                access:jobnotes.tryfetch(jbn:RefNumberKey)
                If job:bouncer <> ''
                    Case Missive('This job has been marked as a bouncer.'&|
                      '<13,10>'&|
                      '<13,10>You must authorise this job for invoicing before you can continue.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    error# = 1
                End!If job:bouncer <> ''
                If job:invoice_number <> '' and error# = 0
                    access:invoice.clearkey(inv:invoice_number_key)
                    inv:invoice_number = job:invoice_number
                    if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                        If inv:invoice_type = 'SIN'
                            glo:select1 = inv:invoice_number
                            !Single_Invoice('','')
                            VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
                            glo:select1 = ''
                        Else!End!If inv:invoice_type = 'SIN'
                            Case Missive('The selected job is part of a multiple invoice, or is a warranty job.','ServiceBase 3g',|
                                           'mstop.jpg','/OK') 
                                Of 1 ! OK Button
                            End ! Case Missive
                        End!If inv:invoice_type = 'SIN'
                    end

                Else!If job:invoice_number <> ''
                    error# = 0
                    If job:chargeable_job <> 'YES'
                        Case Missive('You are attempting to invoice a warranty only job.'&|
                          '<13,10>'&|
                          '<13,10>This can only be processed through the warranty procedures.','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If job:chargeable_job <> 'YES'
                    If job:date_completed <> '' And error# = 0
                        Set(defaults)
                        access:defaults.next()
                        If def:qa_required = 'YES'
                            If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
                                Case Missive('The selected job has not passed QA checks.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                error# = 1
                            End!If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
                        End!If def:qa_required = 'YES'
                    Else!If job:date_completed <> ''
                        Case Missive('The selected job has not been completed.','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                        error# = 1
                    End!If job:date_completed <> ''
                    Pricing_Routine('C',labour",parts",pass",claim",handling",exchange",RRCRAte",RRCParts")
                    If job:ignore_chargeable_charges = 'YES' And error# = 0
                        If job:sub_total = 0
                            Case Missive('You are attempting to invoice a job that has not been priced.','ServiceBase 3g',|
                                           'mstop.jpg','/OK') 
                                Of 1 ! OK Button
                            End ! Case Missive
                            error# = 1
                        End
                    Else!If job:ignore_chargeable_charges = 'YES'
                        If error# = 0
                            
                            If pass" = False
                                Case Missive('You are attempting to invoice a job for which a default charge structure has not been set-up.'&|
                                  '<13,10>'&|
                                  '<13,10>If you wish to proceed you must access the chargeable costs and tick "Ignore Default Charge" option.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                error# = 1
                            Else!If pass" = False
                                job:labour_cost = labour"
                                job:parts_cost  = parts"
                                job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
                            End!If pass" = False
                        End!If error# = 0
                    End!If job:ignore_chargeable_charges = 'YES'
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        If ~jobe:IgnoreRRCChaCosts
                            If pass" = True
                                jobe:RRCCLabourCost = RRCRate"
                                jobe:RRCCPartsCost  = RRCParts"
                                jobe:RRCCSubTotal   = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost
                                jobe:ClaimValue     = handling"
                            Else !If pass" = True
                                jobe:RRCCLabourCost = 0
                                jobe:RRCCPartsCost  = 0
                                jobe:RRCCSubTotal   = 0
                            End !If pass" = True
                            Access:JOBSE.Update()
                        End !If ~jobe:IgnoreRRCChaCosts
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                    If error# = 0
                        fetch_error# = 0
                        despatch# = 0
                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = job:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case Missive('Error! Cannot find sub account details.','ServiceBase 3g',|
                                           'mstop.jpg','/OK') 
                                Of 1 ! OK Button
                            End ! Case Missive
                        Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                            access:tradeacc.clearkey(tra:account_number_key) 
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case Missive('Error! Cannot find trade account details.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                fetch_error# = 1
                            Else!if access:tradeacc.fetch(tra:account_number_key)
                                If tra:use_sub_accounts = 'YES'
                                    If sub:despatch_invoiced_jobs = 'YES'
                                        If sub:despatch_paid_jobs = 'YES'
                                            If job:paid = 'YES'
                                                despatch# = 1
                                            Else
                                                despatch# = 2
                                            End
                                        Else
                                            despatch# = 1
                                        End!If sub:despatch_paid_jobs = 'YES' And job:paid = 'YES'
                                        
                                    End!If sub:despatch_invoiced_jobs = 'YES'
                                End!If tra:use_sub_accounts = 'YES'
                                if tra:invoice_sub_accounts = 'YES'
                                    vat_number" = sub:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:labour_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        Case Missive('Error! A V.A.T. rate has not been set-up for this sub account.','ServiceBase 3g',|
                                                       'mstop.jpg','/OK') 
                                            Of 1 ! OK Button
                                        End ! Case Missive
                                        fetch_error# = 1
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       labour_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:parts_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case Missive('Error! A V.A.T. rate has not been set-up for this sub account.','ServiceBase 3g',|
                                                       'mstop.jpg','/OK') 
                                            Of 1 ! OK Button
                                        End ! Case Missive
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       parts_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                else!if tra:use_sub_accounts = 'YES'
                                    If tra:despatch_invoiced_jobs = 'YES'
                                        If tra:despatch_paid_jobs = 'YES'
                                            If job:paid = 'YES'
                                                despatch# = 1
                                            Else
                                                despatch# = 2
                                            End!If job:paid = 'YES'
                                        Else
                                            despatch# = 1
                                        End!If tra:despatch_paid_jobs = 'YES' and job:paid = 'YES'
                                    End!If tra:despatch_invoiced_jobs = 'YES'
                                    vat_number" = tra:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:labour_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case Missive('Error! A V.A.T. rate has not been set-up for this trade account.','ServiceBase 3g',|
                                                       'mstop.jpg','/OK') 
                                            Of 1 ! OK Button
                                        End ! Case Missive
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       labour_rate$ = vat:vat_rate
                                    end
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:parts_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        Case Missive('Error! A V.A.T. rate has not been set-up for this trade account.','ServiceBase 3g',|
                                                       'mstop.jpg','/OK') 
                                            Of 1 ! OK Button
                                        End ! Case Missive
                                        fetch_error# = 1
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       parts_rate$ = vat:vat_rate
                                    end
                                end!if tra:use_sub_accounts = 'YES'
                            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                        If fetch_error# = 0
        !SAGE BIT
                            If def:use_sage = 'YES'
                                glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
                                Remove(paramss)
                                access:paramss.open()
                                access:paramss.usefile()
             !LABOUR
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Labour_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Labour_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:labour_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
            !PARTS
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Parts_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:parts_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:parts_cost * (parts_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
            !COURIER
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'

                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Courier_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Courier_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:courier_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
                                access:paramss.close()
                                Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
                                sage_error# = 0
                                access:paramss.open()
                                access:paramss.usefile()
                                Set(paramss,0)
                                If access:paramss.next()
                                    sage_error# = 1
                                Else!If access:paramss.next()
                                    If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                                        sage_error# = 1
                                    Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                                        invoice_number_temp = prm:invoice_no
                                        If invoice_number_temp = 0
                                            sage_error# = 1
                                        End!If invoice_number_temp = 0
                                    End!If prm:invoice_no = '-1'
                                End!If access:paramss.next()
                                access:paramss.close()
                            End!If def:use_sage = 'YES'
                            If sage_error# = 0
                                invoice_error# = 1
                                get(invoice,0)
                                if access:invoice.primerecord() = level:benign
                                    If def:use_sage = 'YES'
                                        inv:invoice_number = invoice_number_temp
                                    End!If def:use_sage = 'YES'
                                    inv:invoice_type       = 'SIN'
                                    inv:job_number         = job:ref_number
                                    inv:date_created       = Today()
                                    inv:account_number     = job:account_number
                                    If tra:invoice_sub_accounts = 'YES'
                                        inv:AccountType = 'SUB'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        inv:AccountType = 'MAI'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    inv:total              = job:sub_total
                                    inv:vat_rate_labour    = labour_rate$
                                    inv:vat_rate_parts     = parts_rate$
                                    inv:vat_rate_retail    = labour_rate$
                                    inv:vat_number         = def:vat_number
                                    INV:Courier_Paid       = job:courier_cost
                                    inv:parts_paid         = job:parts_cost
                                    inv:labour_paid        = job:labour_cost
                                    inv:invoice_vat_number = vat_number"
                                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                    jobe:RefNumber  = job:Ref_Number
                                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                        !Found
                                        inv:RRCVatRateLabour    = labour_rate$
                                        inv:RRCVatRateParts     = parts_Rate$
                                        inv:RRCVatRateRetail    = labour_rate$
                                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                        !Error
                                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                    invoice_error# = 0
                                    If access:invoice.insert()
                                        invoice_error# = 1
                                        access:invoice.cancelautoinc()
                                    End!If access:invoice.insert()
                                end!if access:invoice.primerecord() = level:benign
                                If Invoice_error# = 0
                                    Case despatch#
                                        Of 1
                                            If job:despatched = 'YES'
                                                If job:paid = 'YES'
                                                    GetStatus(910,0,'JOB') !Despatch Paid
                                                Else!If job:paid = 'YES'
                                                    GetStatus(905,0,'JOB') !Despatch Unpaid
                                                End!If job:paid = 'YES'
                                            Else!If job:despatched = 'YES'
                                                GetStatus(810,0,'JOB') !Ready To Despatch
                                                job:despatched = 'REA'
                                                job:despatch_type = 'JOB'
                                                job:current_courier = job:courier
                                            End!If job:despatched <> 'YES'
                                        Of 2
                                            GetStatus(803,0,'JOB') !Ready To Despatch

                                    End!Case despatch#

                                    job:invoice_number        = inv:invoice_number
                                    job:invoice_date          = Today()
                                    job:invoice_courier_cost  = job:courier_cost
                                    job:invoice_labour_cost   = job:labour_cost
                                    job:invoice_parts_cost    = job:parts_cost
                                    job:invoice_sub_total     = job:sub_total
                                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                    jobe:RefNumber  = job:Ref_Number
                                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                        !Found
                                        jobe:InvRRCCLabourCost  = jobe:RRCCLabourCost
                                        jobe:InvRRCCPartsCost   = jobe:RRCCPartsCost
                                        jobe:InvRRCCSubTotal    = jobe:RRCCSubTotal
                                        Access:JOBSE.TryUpdate()

                                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                        !Error
                                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                                    access:jobs.update()

                                    IF (AddToAudit(job:ref_number,'JOB','INVOICE CREATED','INVOICE NUMBER: ' & inv:invoice_number))
                                    END ! IF

                                    glo:select1 = inv:invoice_number
                                    !Single_Invoice('','')
                                    VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','')
                                    glo:select1 = ''
                                End!If Invoice_error# = 0
                            End!If def:use_sage = 'YES' and sage_error# = 1
                        End!If fetch_error# = 0
                    End!If error# = 0
                End!If job:invoice_number <> ''
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign

        Of 1 ! No Button
    End ! Case Missive
!Printing Routines
JobReceipt      Routine
    glo:select1 = brw1.q.job:ref_number
    Job_Receipt
    glo:Select1 = ''

JobCard         Routine
    glo:select1 = brw1.q.job:ref_number
    Job_Card
    glo:Select1 = ''
JobLabel        Routine
    glo:select1 = brw1.q.job:ref_number
    Set(defaults)
    access:defaults.next()
    Case def:label_printer_type
        Of 'TEC B-440 / B-442'
            If job:bouncer = 'B'
                Thermal_Labels('SE')
            Else!If job:bouncer = 'B'
                Thermal_Labels('')
            End!If job:bouncer = 'B'
            
        Of 'TEC B-452'
            Thermal_Labels_B452
    End!Case def:label_printer_type
    glo:select1 = ''

Letters         Routine
    glo:Select1 = brw1.q.job:ref_number
    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    Pick_Letter
    if globalresponse = requestcompleted
        glo:select2 = let:description
        Standard_Letter
        access:letters.clearkey(let:descriptionkey)
        let:description = glo:select2
        access:letters.tryfetch(LET:DescriptionKey)
        If let:usestatus = 'YES'
            GetStatus(Sub(let:status,1,3),0,'JOB') !Letter Status
            Access:jobs.update()

        End!If let:use_status = 'YES'

        IF (AddToAudit(job:ref_number,'JOB','LETTER SENT: ' & Clip(let:description),CHOOSE(let:usestatus = 'YES','PREVIOUS STATUS: ' & CLip(job:PreviousStatus) & |
                                      '<13,10>NEW STATUS: ' & Clip(job:current_status),'')))
        END ! IF


    end
    globalrequest     = saverequest#
    glo:select1 = ''
    glo:select2  = ''

MailMerge       Routine
    glo:select1 = brw1.q.job:ref_number
    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    Pickletters
    if globalresponse = requestcompleted
        glo:select2  = mrg:lettername
        ViewLetter
        access:mergelet.clearkey(MRg:LetterNameKey)
        mrg:LetterName  = glo:select2
        access:mergelet.fetch(mrg:LetterNameKey)
Compile('***',Debug=1)
    Message('glo:select2: ' & glo:select2 & |
        'mrg:usestatus: ' & mrg:usestatus,'Debug Message',icon:exclamation)
***
        If MRG:UseStatus = 'YES'
Compile('***',Debug=1)
    Message('Use New Status: ' & CLip(mrg:status),'Debug Message',icon:exclamation)
***
            GetStatus(Sub(mrg:status,1,3),0,'JOB') !Letter Status
            Access:jobs.update()

        End!If let:use_status = 'YES'

        IF (AddToAudit(job:ref_number,'JOB','LETTER SENT: ' & Clip(mrg:LetterName),CHOOSE(mrg:usestatus = 'YES','PREVIOUS STATUS: ' & CLip(job:PreviousStatus) & |
                                      '<13,10>NEW STATUS: ' & Clip(job:current_status),'')))
        END ! IF

    end
    globalrequest     = saverequest#
    glo:select1 = ''


ANCCollNote     Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = glo:select1
    If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
        error# = 0
        access:courier.clearkey(cou:courier_key)
        cou:courier = job:incoming_courier
        if access:courier.tryfetch(cou:courier_key) = Level:Benign
            If cou:courier_type <> 'ANC'
                error# = 1
            End!If job:courier_type = 'ANC'
        Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
            error# = 1
        End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
        If error# = 1
            Case Missive('This job has not been assigned to an ANC courier.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        Else!If error# = 1
            glo:file_name = 'C:\ANC.TXT'
            Remove(expgen)
            If access:expgen.open()
                Stop(error())
                
            
                access:expgen.usefile()
                Clear(gen:record)
                gen:line1   = job:ref_number
                gen:line1   = Sub(gen:line1,1,10) & job:company_name
                gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
                gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
                gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
                gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
                gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
                gen:line1   = Sub(gen:line1,1,175) & job:unit_type
                gen:line1   = Sub(gen:line1,1,195) & job:model_number
                gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
                access:expgen.insert()
                Close(expgen)
                access:expgen.close()
                cou:anccount += 1
                If cou:ANCCount = 999
                    cou:ANCCount = 1
                End
                access:courier.update()
                Set(Defaults)
                access:defaults.next()
                job:incoming_consignment_number = def:ANCCollectionNo
                access:jobs.update()

                IF (AddToAudit(job:ref_number,'JOB','ANC COLLECTION NOTE REPRINT','COLLECTION NOTE NUMBER: ' & Clip(def:ANCCollectionNo)))
                END ! IF


                DEF:ANCCollectionNo += 10
                access:defaults.update()

                sav:path    = path()
                setcursor(cursor:wait)
                setpath(Clip(cou:ancpath))
                Run('ancpaper.exe',1)
                Setpath(sav:path)
            End!If access:expgen.open()
            setcursor()


        End!If error# = 1
    End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
    glo:select1 = ''

DespatchNote        Routine
    glo:select1 = brw1.q.job:ref_number
    Despatch_Note
    glo:select1 = ''

ExchangeUnitLabel   Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = glo:select1
    If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
        If job:exchange_unit_number = ''
            Case Missive('There is no exchange unit attached to this job.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        Else!If job:exchange_unit_number = ''
            job_number# = glo:select1                                                    !A fudge becuase I can't be
            glo:select1 = job:exchange_unit_number                                       !bothered to change the prog.
            Exchange_Unit_Label
            glo:select1  = job_number#
        End!If job:exchange_unit_number = ''
    End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
    glo:select1 = ''

LoanUnitLabel       Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = glo:select1
    If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
        If job:loan_unit_number = ''
            Case Missive('There is no loan unit attached to this job.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        Else!If job:loan_unit_number = ''
            job_number# = glo:select1
            glo:select1  = job:loan_unit_number
            Loan_Unit_Label
            glo:select1  = job_number#
        End!If job:loan_unit_number = ''
    End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
    glo:select1 = ''

Estimate        Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = glo:select1
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        If job:estimate <> 'YES'
            Case Missive('The selected job has not been marked as an estimate.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        Else!If job:estimate <> 'YES'
            Estimate('')
        End!If job:estimate <> 'YES'
    end
    glo:Select1 = ''

Invoice         Routine
    Do Create_Invoice
refresh_job_status_tab      Routine
    Case tmp:SelectLocation
        Of 1
            job:current_status  = status_temp
            brw1.addrange(job:current_status,status_temp)
            Brw1.applyrange
        Of 2
            job:current_status = status_temp
            job:location    = tmp:joblocation
            brw1.addrange(job:location,tmp:joblocation)
            Brw1.applyrange
    End!Case Select_Courier
    Thiswindow.reset(1)
show_hide       Routine
    If select_trade_account_temp = 'IND'
        Unhide(?account_number2_temp)
        Unhide(?Lookup_account_number:2)
    Else
        Hide(?account_number2_temp)
        hide(?Lookup_account_number:2)
    End

    If select_courier_temp = 'IND'
        Unhide(?courier_temp)
        unhide(?lookup_Courier)
    Else
        Hide(?courier_temp)
        Hide(?lookup_courier)
    End

    If tmp:selectlocation = 2
        Unhide(?tmp:JobLocation)
        unhide(?Lookup_location)
    Else!If tmp:selectlocation = 1
        Hide(?tmp:jobLocation)
        hide(?lookup_location)
    End!If tmp:selectlocation = 1

    If tmp:AllJobsType = 1
        ?tmp:Location2{prop:Hide} = 0
        ?LookupLocation2{prop:Hide} = 0
    Else !It tmp:AllJobsType = 1
        ?tmp:Location2{prop:Hide} = 1
        ?LookupLocation2{prop:Hide} = 1
    End !It tmp:AllJobsType = 1
    Display()    
refresh_despatch_tab        Routine
    Case Select_Courier_Temp
        Of 'ALL'
            Case Select_Trade_Account_Temp
                Of 'ALL'
                    rea" = 'REA'
                    Brw1.addrange(job:despatched,rea")
                    Brw1.applyrange
                Of 'IND'
                    job:despatched = 'REA'
                    Brw1.addrange(job:account_number,account_number2_temp)
                    Brw1.applyrange    
            End!Case Select_Trade_Account_Temp
        Of 'IND'
            Case Select_Trade_Account_Temp
                Of 'ALL'
                    job:despatched = 'REA'
                    Brw1.addrange(job:current_courier,courier_temp)
                    Brw1.applyrange
                Of 'IND'
                    job:despatched = 'REA'
                    job:account_number    = account_number2_temp
                    Brw1.addrange(job:current_courier,courier_temp)
                    Brw1.applyrange
            End!Case Select_Trade_Account_Temp    
    End!Case Select_Courier
    Thiswindow.reset(1)
Update_City_Link    Routine
    epc:account_number    = Left(Sub(cou:account_number,1,8))
    dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
    If multiple_despatch_temp = 'YES'
        dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
        epc:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
    Else
        epc:ref_number        = '|J' & Clip(Format(job:ref_number,@s9)) & '/DB' & Clip(Format(dbt:batch_number,@s9)) & |
                                    '/' & Clip(job:order_number)
    End
    If multiple_despatch_temp = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(sub:contact_name)
                    epc:address_line1 = '|' & Left(sub:company_name)
                    epc:address_line2 = '|' & Left(sub:address_line1)
                    epc:town = '|' & Left(sub:address_line2)
                    epc:county = '|' & Left(sub:address_line3)
                    epc:postcode = '|' & Left(sub:postcode)
                else!if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(tra:contact_name)
                    epc:address_line1 = '|' & Left(tra:company_name)
                    epc:address_line1 = '|' & Left(tra:address_line1)
                    epc:town = '|' & Left(tra:address_line2)
                    epc:county = '|' & Left(tra:address_line3)
                    epc:postcode = '|' & Left(tra:postcode)
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If multiple_despatch_temp = 'YES'
        epc:contact_name      = '|' & Left(Sub(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),1,30))
        epc:address_line1     = '|' & Left(Sub(job:company_name_delivery,1,30))
        epc:address_line2     = '|' & Left(Sub(job:address_line1_delivery,1,30))
        epc:town              = '|' & Left(Sub(job:address_line2_delivery,1,30))
        epc:county            = '|' & Left(Sub(job:address_line3_delivery,1,30))
        epc:postcode          = '|' & Left(Sub(job:postcode_delivery,1,8))
    End!If multiple_despatch_temp = 'YES'
    epc:nol               = '|' & '01'
    epc:customer_name     = '|' & Left(Sub(job:account_number,1,30))
    epc:city_service      = '|' & Left(Sub(cou:service,1,2))
    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:ref_number
    access:jobnotes.tryfetch(jbn:REfNumberKey)
    epc:city_instructions = '|' & Left(Sub(jbn:delivery_text,1,30))
    epc:pudamt            = '|' & Left(Sub('0.00',1,4))
    If job:despatch_type = 'EXC' or job:despatch_type = 'LOA'
        epc:return_it     = '|' & Left('Y')
    Else
        epc:return_it     = '|' & Left('N')
    End
    epc:saturday          = '|' & Left('N')
    epc:dog               = '|' & Left(Sub('Mobile Phone Goods',1,30))
AddEngNotes             ROUTINE

    !notes added - so add them to the engineers notes field
    access:Jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
        !found the jobnotes
        If clip(jbn:Engineers_Notes) = '' then
            jbn:Engineers_Notes = clip(notes")
        Else
            jbn:Engineers_Notes = clip(jbn:Engineers_Notes) & '; ' & clip(notes")
        End !If clip(jbn:Engineers_Notes) = '' then

        access:Jobnotes.update()

    End !If access:jobnotes.fetch(jbn:RefNumberKey) = level:benign then
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Jobs','?Browse:1')        !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020524'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Jobs')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHACC.Open
  Relate:JOBBATCH.Open
  Relate:MULDESP.Open
  Relate:MULDESP_ALIAS.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WAYBCONF.Open
  Relate:WAYBILLS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:LOCINTER.UseFile
  Access:JOBACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MODELNUM.UseFile
  Access:EXCHANGE.UseFile
  Access:LOAN.UseFile
  Access:COURIER.UseFile
  Access:MULDESPJ.UseFile
  Access:INVOICE.UseFile
  Access:JOBNOTES.UseFile
  Access:CONTHIST.UseFile
  Access:JOBSWARR.UseFile
  SELF.FilesOpened = True
  Set(Defaults)
  access:defaults.next()
  
  If FullAccess(glo:PassAccount,glo:Password)
      ?ViewButton{prop:Hide} = 0
  End !FullAccess(glo:PassAccount,glo:Password)
  
  tmp:RRCLocation = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:StatusSendToRRC = GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  tmp:DisableLocation = GETINI('DISABLE','InternalLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  ! Change colour to "Despatch" if location = DESPATCH - TrkBs: 6545 (DBH: 19-10-2005)
  tmp:DespatchLocation = GETINI('RRC','DespatchToCustomer',,CLIP(PATH())&'\SB2KDEF.INI')
  
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Jobs')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbk01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?account_number_temp{Prop:Tip} AND ~?Lookup_Account_number{Prop:Tip}
     ?Lookup_Account_number{Prop:Tip} = 'Select ' & ?account_number_temp{Prop:Tip}
  END
  IF ?account_number_temp{Prop:Msg} AND ~?Lookup_Account_number{Prop:Msg}
     ?Lookup_Account_number{Prop:Msg} = 'Select ' & ?account_number_temp{Prop:Msg}
  END
  IF ?status_temp{Prop:Tip} AND ~?Lookup_Job_Status{Prop:Tip}
     ?Lookup_Job_Status{Prop:Tip} = 'Select ' & ?status_temp{Prop:Tip}
  END
  IF ?status_temp{Prop:Msg} AND ~?Lookup_Job_Status{Prop:Msg}
     ?Lookup_Job_Status{Prop:Msg} = 'Select ' & ?status_temp{Prop:Msg}
  END
  IF ?status3_temp{Prop:Tip} AND ~?Lookup_Status3{Prop:Tip}
     ?Lookup_Status3{Prop:Tip} = 'Select ' & ?status3_temp{Prop:Tip}
  END
  IF ?status3_temp{Prop:Msg} AND ~?Lookup_Status3{Prop:Msg}
     ?Lookup_Status3{Prop:Msg} = 'Select ' & ?status3_temp{Prop:Msg}
  END
  IF ?status4_temp{Prop:Tip} AND ~?Lookup_Status4{Prop:Tip}
     ?Lookup_Status4{Prop:Tip} = 'Select ' & ?status4_temp{Prop:Tip}
  END
  IF ?status4_temp{Prop:Msg} AND ~?Lookup_Status4{Prop:Msg}
     ?Lookup_Status4{Prop:Msg} = 'Select ' & ?status4_temp{Prop:Msg}
  END
  IF ?tmp:JobLocation{Prop:Tip} AND ~?Lookup_Location{Prop:Tip}
     ?Lookup_Location{Prop:Tip} = 'Select ' & ?tmp:JobLocation{Prop:Tip}
  END
  IF ?tmp:JobLocation{Prop:Msg} AND ~?Lookup_Location{Prop:Msg}
     ?Lookup_Location{Prop:Msg} = 'Select ' & ?tmp:JobLocation{Prop:Msg}
  END
  IF ?model_number_temp{Prop:Tip} AND ~?Lookup_Model_Number{Prop:Tip}
     ?Lookup_Model_Number{Prop:Tip} = 'Select ' & ?model_number_temp{Prop:Tip}
  END
  IF ?model_number_temp{Prop:Msg} AND ~?Lookup_Model_Number{Prop:Msg}
     ?Lookup_Model_Number{Prop:Msg} = 'Select ' & ?model_number_temp{Prop:Msg}
  END
  IF ?courier_temp{Prop:Tip} AND ~?Lookup_Courier{Prop:Tip}
     ?Lookup_Courier{Prop:Tip} = 'Select ' & ?courier_temp{Prop:Tip}
  END
  IF ?courier_temp{Prop:Msg} AND ~?Lookup_Courier{Prop:Msg}
     ?Lookup_Courier{Prop:Msg} = 'Select ' & ?courier_temp{Prop:Msg}
  END
  IF ?account_number2_temp{Prop:Tip} AND ~?Lookup_Account_Number:2{Prop:Tip}
     ?Lookup_Account_Number:2{Prop:Tip} = 'Select ' & ?account_number2_temp{Prop:Tip}
  END
  IF ?account_number2_temp{Prop:Msg} AND ~?Lookup_Account_Number:2{Prop:Msg}
     ?Lookup_Account_Number:2{Prop:Msg} = 'Select ' & ?account_number2_temp{Prop:Msg}
  END
  IF ?tmp:Location2{Prop:Tip} AND ~?LookupLocation2{Prop:Tip}
     ?LookupLocation2{Prop:Tip} = 'Select ' & ?tmp:Location2{Prop:Tip}
  END
  IF ?tmp:Location2{Prop:Msg} AND ~?LookupLocation2{Prop:Msg}
     ?LookupLocation2{Prop:Msg} = 'Select ' & ?tmp:Location2{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort28:Locator)
  BRW1::Sort28:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Location_Key)
  BRW1.AddRange(job:Location,tmp:Location2)
  BRW1.AddLocator(BRW1::Sort29:Locator)
  BRW1::Sort29:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ESN_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?job:ESN,job:ESN,1,BRW1)
  BRW1.AddSortOrder(,job:MSN_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?job:MSN,job:MSN,1,BRW1)
  BRW1.AddSortOrder(,job:AccOrdNoKey)
  BRW1.AddRange(job:Account_Number,account_number_temp)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?JOB:Order_Number,job:Order_Number,1,BRW1)
  BRW1.AddSortOrder(,job:AccountNumberKey)
  BRW1.AddRange(job:Account_Number,account_number_temp)
  BRW1.AddLocator(BRW1::Sort27:Locator)
  BRW1::Sort27:Locator.Init(?job:Ref_Number:10,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Surname_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?job:Surname,job:Surname,1,BRW1)
  BRW1.AddSortOrder(,job:By_Status)
  BRW1.AddRange(job:Current_Status,status_temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?JOB:Ref_Number:2,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:StatusLocKey)
  BRW1.AddRange(job:Location,tmp:JobLocation)
  BRW1.AddLocator(BRW1::Sort25:Locator)
  BRW1::Sort25:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ExcStatusKey)
  BRW1.AddRange(job:Exchange_Status,status3_temp)
  BRW1.AddLocator(BRW1::Sort23:Locator)
  BRW1::Sort23:Locator.Init(?JOB:Ref_Number:7,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:LoanStatusKey)
  BRW1.AddRange(job:Loan_Status,status4_temp)
  BRW1.AddLocator(BRW1::Sort24:Locator)
  BRW1::Sort24:Locator.Init(?JOB:Ref_Number:8,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:MobileNumberKey)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?job:Mobile_Number,job:Mobile_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Model_Unit_Key)
  BRW1.AddRange(job:Model_Number,model_number_temp)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?job:Unit_Type,job:Unit_Type,1,BRW1)
  BRW1.AddSortOrder(,job:EngCompKey)
  BRW1.AddRange(job:Engineer,engineer_temp)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?JOB:Ref_Number:3,job:Completed,1,BRW1)
  BRW1.SetFilter('(Upper(job:Completed) = Upper(Completed2_Temp))')
  BRW1.AddSortOrder(,job:Engineer_Key)
  BRW1.AddRange(job:Engineer,engineer_temp)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?JOB:Ref_Number:3,job:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,job:EngWorkKey)
  BRW1.AddRange(job:Workshop,yes_temp)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?JOB:Ref_Number:4,job:Ref_Number,1,BRW1)
  BRW1.SetFilter('(Upper(job:engineer) = '''')')
  BRW1.AddSortOrder(,job:ReadyToDespKey)
  BRW1.AddRange(job:Despatched,rea_temp)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ReadyToTradeKey)
  BRW1.AddRange(job:Account_Number,account_number2_temp)
  BRW1.AddLocator(BRW1::Sort18:Locator)
  BRW1::Sort18:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ReadyToCouKey)
  BRW1.AddRange(job:Current_Courier,courier_temp)
  BRW1.AddLocator(BRW1::Sort19:Locator)
  BRW1::Sort19:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ReadyToAllKey)
  BRW1.AddRange(job:Current_Courier,courier_temp)
  BRW1.AddLocator(BRW1::Sort20:Locator)
  BRW1::Sort20:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BIND('tmp:jobnumber',tmp:jobnumber)
  BIND('model_unit_temp',model_unit_temp)
  BIND('Overdue_Temp',Overdue_Temp)
  BIND('Completed_Temp',Completed_Temp)
  BIND('Invoiced_Temp',Invoiced_Temp)
  BIND('Warranty_Temp',Warranty_Temp)
  BIND('Collected_Temp',Collected_Temp)
  BIND('Paid_Temp',Paid_Temp)
  BIND('tag_temp',tag_temp)
  BIND('address_temp',address_temp)
  BIND('despatch_type_temp',despatch_type_temp)
  BIND('status2_temp',status2_temp)
  BIND('tmp:ExchangeText',tmp:ExchangeText)
  BIND('tmp:BrowseFlag',tmp:BrowseFlag)
  BIND('tmp:LoanText',tmp:LoanText)
  BIND('Tmp:Batch_No',Tmp:Batch_No)
  BIND('tmp:ExchangeStatusDate',tmp:ExchangeStatusDate)
  BIND('tmp:LoanStatusDate',tmp:LoanStatusDate)
  BIND('tmp:StatusDate',tmp:StatusDate)
  BIND('tmp:LoaStatusDays',tmp:LoaStatusDays)
  BIND('tmp:ExcStatusDays',tmp:ExcStatusDays)
  BIND('tmp:JobStatusDays',tmp:JobStatusDays)
  BIND('tmp:JobDays',tmp:JobDays)
  BIND('Postcode_Temp',Postcode_Temp)
  BIND('Address_Line3_Temp',Address_Line3_Temp)
  BIND('Address_Line2_Temp',Address_Line2_Temp)
  BIND('Address_Line1_Temp',Address_Line1_Temp)
  BIND('Company_Name_Temp',Company_Name_Temp)
  BIND('tmp:Address',tmp:Address)
  BIND('tmp:LoanText2',tmp:LoanText2)
  BIND('tmp:ExchangeText2',tmp:ExchangeText2)
  BIND('status_temp',status_temp)
  BIND('engineer_temp',engineer_temp)
  BIND('model_number_temp',model_number_temp)
  BIND('batch_number_temp',batch_number_temp)
  BIND('courier_temp',courier_temp)
  BIND('account_number2_temp',account_number2_temp)
  BIND('yes_temp',yes_temp)
  BIND('no_temp',no_temp)
  BIND('blank_temp',blank_temp)
  BIND('rea_temp',rea_temp)
  BIND('Completed2_Temp',Completed2_Temp)
  ?Browse:1{PROP:IconList,1} = '~Block_Blue.ico'
  ?Browse:1{PROP:IconList,2} = '~Block_Blue_R.ico'
  ?Browse:1{PROP:IconList,3} = '~Block_Blue_left.ico'
  ?Browse:1{PROP:IconList,4} = '~Block_Cyan.ico'
  ?Browse:1{PROP:IconList,5} = '~bl_y_lt.ico'
  ?Browse:1{PROP:IconList,6} = '~block_green.ico'
  ?Browse:1{PROP:IconList,7} = '~block_red.ico'
  ?Browse:1{PROP:IconList,8} = '~block_red_left.ico'
  ?Browse:1{PROP:IconList,9} = '~block_red_right.ico'
  ?Browse:1{PROP:IconList,10} = '~block_yellow.ico'
  ?Browse:1{PROP:IconList,11} = '~notick1.ico'
  ?Browse:1{PROP:IconList,12} = '~tick1.ico'
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(tmp:jobnumber,BRW1.Q.tmp:jobnumber)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(model_unit_temp,BRW1.Q.model_unit_temp)
  BRW1.AddField(job:date_booked,BRW1.Q.job:date_booked)
  BRW1.AddField(job:Current_Status,BRW1.Q.job:Current_Status)
  BRW1.AddField(job:Location,BRW1.Q.job:Location)
  BRW1.AddField(Overdue_Temp,BRW1.Q.Overdue_Temp)
  BRW1.AddField(Completed_Temp,BRW1.Q.Completed_Temp)
  BRW1.AddField(Invoiced_Temp,BRW1.Q.Invoiced_Temp)
  BRW1.AddField(Warranty_Temp,BRW1.Q.Warranty_Temp)
  BRW1.AddField(Collected_Temp,BRW1.Q.Collected_Temp)
  BRW1.AddField(Paid_Temp,BRW1.Q.Paid_Temp)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(job:MSN,BRW1.Q.job:MSN)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Surname,BRW1.Q.job:Surname)
  BRW1.AddField(job:Mobile_Number,BRW1.Q.job:Mobile_Number)
  BRW1.AddField(job:Postcode,BRW1.Q.job:Postcode)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:Unit_Type,BRW1.Q.job:Unit_Type)
  BRW1.AddField(address_temp,BRW1.Q.address_temp)
  BRW1.AddField(job:Consignment_Number,BRW1.Q.job:Consignment_Number)
  BRW1.AddField(job:Current_Courier,BRW1.Q.job:Current_Courier)
  BRW1.AddField(despatch_type_temp,BRW1.Q.despatch_type_temp)
  BRW1.AddField(status2_temp,BRW1.Q.status2_temp)
  BRW1.AddField(job:Loan_Status,BRW1.Q.job:Loan_Status)
  BRW1.AddField(job:Exchange_Status,BRW1.Q.job:Exchange_Status)
  BRW1.AddField(job:Incoming_Consignment_Number,BRW1.Q.job:Incoming_Consignment_Number)
  BRW1.AddField(job:Exchange_Unit_Number,BRW1.Q.job:Exchange_Unit_Number)
  BRW1.AddField(job:Loan_Unit_Number,BRW1.Q.job:Loan_Unit_Number)
  BRW1.AddField(tmp:ExchangeText,BRW1.Q.tmp:ExchangeText)
  BRW1.AddField(job:Address_Line1,BRW1.Q.job:Address_Line1)
  BRW1.AddField(tmp:BrowseFlag,BRW1.Q.tmp:BrowseFlag)
  BRW1.AddField(tmp:LoanText,BRW1.Q.tmp:LoanText)
  BRW1.AddField(job:Date_Despatched,BRW1.Q.job:Date_Despatched)
  BRW1.AddField(Tmp:Batch_No,BRW1.Q.Tmp:Batch_No)
  BRW1.AddField(tmp:ExchangeStatusDate,BRW1.Q.tmp:ExchangeStatusDate)
  BRW1.AddField(tmp:LoanStatusDate,BRW1.Q.tmp:LoanStatusDate)
  BRW1.AddField(tmp:StatusDate,BRW1.Q.tmp:StatusDate)
  BRW1.AddField(tmp:LoaStatusDays,BRW1.Q.tmp:LoaStatusDays)
  BRW1.AddField(tmp:ExcStatusDays,BRW1.Q.tmp:ExcStatusDays)
  BRW1.AddField(tmp:JobStatusDays,BRW1.Q.tmp:JobStatusDays)
  BRW1.AddField(tmp:JobDays,BRW1.Q.tmp:JobDays)
  BRW1.AddField(job:time_booked,BRW1.Q.job:time_booked)
  BRW1.AddField(job:Company_Name,BRW1.Q.job:Company_Name)
  BRW1.AddField(job:Address_Line2,BRW1.Q.job:Address_Line2)
  BRW1.AddField(job:Address_Line3,BRW1.Q.job:Address_Line3)
  BRW1.AddField(Postcode_Temp,BRW1.Q.Postcode_Temp)
  BRW1.AddField(Address_Line3_Temp,BRW1.Q.Address_Line3_Temp)
  BRW1.AddField(Address_Line2_Temp,BRW1.Q.Address_Line2_Temp)
  BRW1.AddField(Address_Line1_Temp,BRW1.Q.Address_Line1_Temp)
  BRW1.AddField(Company_Name_Temp,BRW1.Q.Company_Name_Temp)
  BRW1.AddField(tmp:Address,BRW1.Q.tmp:Address)
  BRW1.AddField(tmp:LoanText2,BRW1.Q.tmp:LoanText2)
  BRW1.AddField(tmp:ExchangeText2,BRW1.Q.tmp:ExchangeText2)
  BRW1.AddField(job:Engineer,BRW1.Q.job:Engineer)
  BRW1.AddField(job:Completed,BRW1.Q.job:Completed)
  BRW1.AddField(job:Workshop,BRW1.Q.job:Workshop)
  BRW1.AddField(job:Despatched,BRW1.Q.job:Despatched)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  BRW1.popup.additem('Job Reports','Popup2','',1)
  BRW1.popup.seticon('Popup2','Point.ico')
  BRW1.popup.additem('Job Receipt','SPopup21','Popup2',2)
  BRW1.popup.additemevent('SPopup21',event:accepted,?JobReceipt)
  BRW1.popup.seticon('SPopup21','receipt.ico')
  BRW1.popup.additem('Job Card','SPopup22','SPopup1',2)
  BRW1.popup.additemevent('SPopup22',event:accepted,?JobCard)
  BRW1.popup.seticon('SPopup22','jobcard.ico')
  BRW1.popup.additem('Job Label','SPopup23','SPopup2',2)
  BRW1.popup.additemevent('SPopup23',event:accepted,?JobLabel)
  BRW1.popup.seticon('SPopup23','label.ico')
  BRW1.popup.additem('-','SPopup24','SPopup3',2)
  BRW1.popup.additem('Letters','SPopup25','SPopup4',2)
  BRW1.popup.additemevent('SPopup25',event:accepted,?Letters)
  BRW1.popup.seticon('SPopup25','letter.ico')
  BRW1.popup.additem('Mail Merge','SPopup26','SPopup5',2)
  BRW1.popup.additemevent('SPopup26',event:accepted,?MailMerge)
  BRW1.popup.seticon('SPopup26','letter.ico')
  BRW1.popup.additem('Coll/Desp Reports','Popup3','SPopup6',1)
  BRW1.popup.seticon('Popup3','Point.ico')
  BRW1.popup.additem('Despatch Note','SPopup31','Popup3',2)
  BRW1.popup.additemevent('SPopup31',event:accepted,?DespatchNote)
  BRW1.popup.seticon('SPopup31','jobcard.ico')
  BRW1.popup.additem('-','SPopup32','SPopup1',2)
  BRW1.popup.additem('Delivery Label','SPopup33','SPopup2',2)
  BRW1.popup.additemevent('SPopup33',event:accepted,?DeliveryLabel)
  BRW1.popup.seticon('SPopup33','label.ico')
  BRW1.popup.additem('Collection Label','SPopup34','SPopup3',2)
  BRW1.popup.additemevent('SPopup34',event:accepted,?CollectionLabel)
  BRW1.popup.seticon('SPopup34','label.ico')
  BRW1.popup.additem('Loan/Exchange Reports','Popup4','SPopup4',1)
  BRW1.popup.seticon('Popup4','Point.ico')
  BRW1.popup.additem('Exchange Unit Label','SPopup41','Popup4',2)
  BRW1.popup.additemevent('SPopup41',event:accepted,?Menu1ExchangeUnitLabel)
  BRW1.popup.seticon('SPopup41','label.ico')
  BRW1.popup.additem('Loan Unit Label','SPopup42','SPopup1',2)
  BRW1.popup.additemevent('SPopup42',event:accepted,?Menu1LoanUnitLabel)
  BRW1.popup.seticon('SPopup42','label.ico')
  BRW1.popup.additem('Financial Reports','Popup5','SPopup2',1)
  BRW1.popup.seticon('Popup5','Point.ico')
  BRW1.popup.additem('Estimate','SPopup51','Popup5',2)
  BRW1.popup.additemevent('SPopup51',event:accepted,?Menu1Estimate)
  BRW1.popup.seticon('SPopup51','receipt.ico')
  BRW1.popup.additem('Invoice','SPopup52','SPopup1',2)
  BRW1.popup.additemevent('SPopup52',event:accepted,?Menu1Invoice)
  BRW1.popup.seticon('SPopup52','jobcard.ico')
  BRW1.popup.additem('-','Popup6','SPopup2',1)
  BRW1.popup.additem('Create New Job','Popup7','Popup6',1)
  BRW1.popup.additemevent('Popup7',event:accepted,?CreateNewJob)
  BRW1.popup.additem('Engineers Update','Popup8','Popup7',1)
  BRW1.popup.additemevent('Popup8',event:accepted,?RapidUpdateButton:2)
  BRW1.popup.additem('Amend Job','Popup9','Popup8',1)
  BRW1.popup.additemevent('Popup9',event:accepted,?Change:3)
  BRW1.popup.additem('-','EPopup90','SPopup2',1)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue20)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHACC.Close
    Relate:JOBBATCH.Close
    Relate:MULDESP.Close
    Relate:MULDESP_ALIAS.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WAYBCONF.Close
    Relate:WAYBILLS.Close
    Relate:WEBJOB.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Jobs','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','Browse_Jobs')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  BRW1.Popup.SetItemEnable('Popup2',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup21',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup22',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup23',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup24',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup25',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup26',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup3',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup31',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup32',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup33',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup34',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup4',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup41',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup42',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup5',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup51',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup52',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup6',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup7',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup8',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup9',CHOOSE(NOT(~Records(Queue:Browse:1))))
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
    do_update# = true

    If Field() = ?CreateNewJob
        check_access('JOBS - INSERT',x")
        if x" = false
            Case Missive('You do not have access to this option.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
            do_update# = false
        end
    End !Field() = ?CreateNewJob

    if do_update# = true
        If number = 9
            location_temp = ''
            !If you are deleting a job, save the location so you
            !can make a space available.
            If request = DeleteRecord
                !Save job number to delete WEBJOBS afterwards
                del:JobNumber   = brw1.q.job:Ref_Number
                If job:date_completed = ''
                    location_temp = job:location
                End!If job:date_completed = ''
            Else !If request = DeleteRecord
                If job:cancelled = 'YES'
                    request = Viewrecord
                End!If job:cancelled = 'YES'
            End!If request = DeleteRecord

        End!If number = 9
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Available_Locations
      PickSubAccounts
      Browse_Available_Locations
      PickJobStatus
      PickExchangeStatus
      PickLoanStatus
      Browse_Model_Numbers
      PickCouriers
      PickSubAccounts
    END
    ReturnValue = GlobalResponse
  END
  End !If do_update# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Xplore1.AddField(tmp:jobnumber,BRW1.Q.tmp:jobnumber)
  Xplore1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  Xplore1.AddField(model_unit_temp,BRW1.Q.model_unit_temp)
  Xplore1.AddField(job:date_booked,BRW1.Q.job:date_booked)
  Xplore1.AddField(job:Current_Status,BRW1.Q.job:Current_Status)
  Xplore1.AddField(job:Location,BRW1.Q.job:Location)
  Xplore1.AddField(Overdue_Temp,BRW1.Q.Overdue_Temp)
  Xplore1.AddField(Completed_Temp,BRW1.Q.Completed_Temp)
  Xplore1.AddField(Invoiced_Temp,BRW1.Q.Invoiced_Temp)
  Xplore1.AddField(Warranty_Temp,BRW1.Q.Warranty_Temp)
  Xplore1.AddField(Collected_Temp,BRW1.Q.Collected_Temp)
  Xplore1.AddField(Paid_Temp,BRW1.Q.Paid_Temp)
  Xplore1.AddField(job:ESN,BRW1.Q.job:ESN)
  Xplore1.AddField(tag_temp,BRW1.Q.tag_temp)
  Xplore1.AddField(job:MSN,BRW1.Q.job:MSN)
  Xplore1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  Xplore1.AddField(job:Surname,BRW1.Q.job:Surname)
  Xplore1.AddField(job:Mobile_Number,BRW1.Q.job:Mobile_Number)
  Xplore1.AddField(job:Postcode,BRW1.Q.job:Postcode)
  Xplore1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  Xplore1.AddField(job:Unit_Type,BRW1.Q.job:Unit_Type)
  Xplore1.AddField(address_temp,BRW1.Q.address_temp)
  Xplore1.AddField(job:Consignment_Number,BRW1.Q.job:Consignment_Number)
  Xplore1.AddField(job:Current_Courier,BRW1.Q.job:Current_Courier)
  Xplore1.AddField(despatch_type_temp,BRW1.Q.despatch_type_temp)
  Xplore1.AddField(status2_temp,BRW1.Q.status2_temp)
  Xplore1.AddField(job:Loan_Status,BRW1.Q.job:Loan_Status)
  Xplore1.AddField(job:Exchange_Status,BRW1.Q.job:Exchange_Status)
  Xplore1.AddField(job:Incoming_Consignment_Number,BRW1.Q.job:Incoming_Consignment_Number)
  Xplore1.AddField(job:Exchange_Unit_Number,BRW1.Q.job:Exchange_Unit_Number)
  Xplore1.AddField(job:Loan_Unit_Number,BRW1.Q.job:Loan_Unit_Number)
  Xplore1.AddField(tmp:ExchangeText,BRW1.Q.tmp:ExchangeText)
  Xplore1.AddField(job:Address_Line1,BRW1.Q.job:Address_Line1)
  Xplore1.AddField(tmp:BrowseFlag,BRW1.Q.tmp:BrowseFlag)
  Xplore1.AddField(tmp:LoanText,BRW1.Q.tmp:LoanText)
  Xplore1.AddField(job:Date_Despatched,BRW1.Q.job:Date_Despatched)
  Xplore1.AddField(Tmp:Batch_No,BRW1.Q.Tmp:Batch_No)
  Xplore1.AddField(tmp:ExchangeStatusDate,BRW1.Q.tmp:ExchangeStatusDate)
  Xplore1.AddField(tmp:LoanStatusDate,BRW1.Q.tmp:LoanStatusDate)
  Xplore1.AddField(tmp:StatusDate,BRW1.Q.tmp:StatusDate)
  Xplore1.AddField(tmp:LoaStatusDays,BRW1.Q.tmp:LoaStatusDays)
  Xplore1.AddField(tmp:ExcStatusDays,BRW1.Q.tmp:ExcStatusDays)
  Xplore1.AddField(tmp:JobStatusDays,BRW1.Q.tmp:JobStatusDays)
  Xplore1.AddField(tmp:JobDays,BRW1.Q.tmp:JobDays)
  Xplore1.AddField(job:time_booked,BRW1.Q.job:time_booked)
  Xplore1.AddField(job:Company_Name,BRW1.Q.job:Company_Name)
  Xplore1.AddField(job:Address_Line2,BRW1.Q.job:Address_Line2)
  Xplore1.AddField(job:Address_Line3,BRW1.Q.job:Address_Line3)
  Xplore1.AddField(Postcode_Temp,BRW1.Q.Postcode_Temp)
  Xplore1.AddField(Address_Line3_Temp,BRW1.Q.Address_Line3_Temp)
  Xplore1.AddField(Address_Line2_Temp,BRW1.Q.Address_Line2_Temp)
  Xplore1.AddField(Address_Line1_Temp,BRW1.Q.Address_Line1_Temp)
  Xplore1.AddField(Company_Name_Temp,BRW1.Q.Company_Name_Temp)
  Xplore1.AddField(tmp:Address,BRW1.Q.tmp:Address)
  Xplore1.AddField(tmp:LoanText2,BRW1.Q.tmp:LoanText2)
  Xplore1.AddField(tmp:ExchangeText2,BRW1.Q.tmp:ExchangeText2)
  Xplore1.AddField(job:Engineer,BRW1.Q.job:Engineer)
  Xplore1.AddField(job:Completed,BRW1.Q.job:Completed)
  Xplore1.AddField(job:Workshop,BRW1.Q.job:Workshop)
  Xplore1.AddField(job:Despatched,BRW1.Q.job:Despatched)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Change:3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change:3, Accepted)
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number  = brw1.q.job:ref_number
          If access:jobs.tryfetch(job:ref_number_key) = Level:benign
              GlobalRequest = ChangeRecord
              UpdateJobs
              if GlobalResponse = RequestCompleted
                  BRW1.ResetSort(1)
              end
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change:3, Accepted)
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
          !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
          IF LEN(CLIP(job:ESN)) = 18
            !Ericsson IMEI!
            Job:ESN = SUB(job:ESN,4,15)
            !ESN_Entry_Temp = Job:ESN
            UPDATE()
          ELSE
            !Job:ESN = ESN_Entry_Temp
          END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
    OF ?ButtonSMSTaggedJobs
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonSMSTaggedJobs, Accepted)
      !Send SMS to tagged jobs
      if records(glo:Queue20) = 0 then
          miss# = missive('You must tag some jobs for this to work','ServiceBase 3g','mstop.jpg','OK' )
          cycle
      END
      SMSErrorFlag = 0
      Loop count = 1 to records(glo:Queue20)
          get(Glo:Queue20,count)
      
          Access:jobs.clearkey(job:Ref_Number_Key)
          job:Ref_Number = glo:Queue20.Pointer20
          if access:jobs.fetch(job:Ref_Number_Key)
      
              !message('Job not found by ref number '&clip(glo:Queue20.Pointer20))
      
          ELSE
              
              Case choice(?StatusSheet)
                  of 1    !job status
                      SendSMSText('J','N','N')  !,job:Location,AutomaticSend,SpecialType)
                  of 2    !exchange status
                      !message('Doing the send smstext E N N from within browse jobs')
                      SendSMSText('E','N','N')  !,job:Location,AutomaticSend,SpecialType)
                  of 3    !loan status
                      SendSMSText('L','N','N')  !,job:Location,AutomaticSend,SpecialType)
              END !Case
      
              !did it work?
              if glo:ErrorText[1:5] = 'ERROR' then
      
                  miss# = missive('Unable to send SMS for job '&clip(job:Ref_number)&' as '&glo:ErrorText[ 6 : len(clip(Glo:ErrorText)) ],'ServiceBase 3g','mstop.jpg','OK' )
                  SMSErrorFlag = true
      
              ELSE
                  !message('No error on Prepare SMS text : '&clip(glo:ErrorText))
                  !miss# = missive('An SMS has been sent to '&clip(job:Initial)&' '&clip(job:Surname),'ServiceBase 3g','mwarn.jpg','OK' )
      
                  if choice(?StatusSheet) = 1 and job:Current_Status = '510 ESTIMATE READY' then
                      !Job:Current_Status = '520 ESTIMATE SENT
                      Access:Status.clearkey(sts:Status_Key)
                      sts:Status = '520 ESTIMATE SENT'
                      if access:Status.fetch(sts:Status_Key)
                          !message('Cannot find 520 estimate ready')
                      ELSE
                          SetTheJobStatus(sts:Ref_Number,'JOB')
                          IF (Access:JOBS.TryUpdate() = Level:Benign)
                              UpdateTheJobStatusHistory()
                              If AddToAudit(job:Ref_Number,'JOB','AUTOMATED STATUS CHANGE TO: 520 ESTIMATE SENT','PREVIOUS STATUS: 510 ESTIMATE READY<13,10>NEW STATUS: 520 ESTIMATE SENT ' &|
                                      '<13,10,13,10>REASON:<13,10>AUTOMATED SMS SENT')
      
                              End ! if addtoaudit
                          END !if jobs.update
      
                      END !if status found
                  END !if this is job and estimate ready
              END !if error shown
          END !if jobs record fetched
      END !loop through tagges jobs
      
      if SMSErrorFlag = true then
          miss# = missive('SMS messaging complete - some errors (already reported) occured during this process','ServiceBase 3g','mstop.jpg','OK' )
      ELSE
          miss# = missive('SMS Message(s) Submitted','ServiceBase 3g','mstop.jpg','OK' )
      END
      
      
      if choice(?StatusSheet) = 1 and status_temp = '510 ESTIMATE READY' then
          !this will have changed statuses
          BRW1.resetsort(1)
      END !if this was a job on estimate ready
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonSMSTaggedJobs, Accepted)
    OF ?ButtonTagFromCSV
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonTagFromCSV, Accepted)
      GetImportFilename
      
      if filename3 = '' then
          !do nothing - Cancel was used
          !message('Cancel pressed')
      ELSE
          !message('Filename returned '&clip(filename3))
      
          !exception file name
          glo:file_name4 = 'SMS_'&format(today(),@D12)&'_'&format(clock(),@t5)&'.csv'
      
          !defaults
          Count = 0
      
          !ImportFile      FILE,DRIVER('BASIC'),PRE(IMP),NAME(glo:file_name4),CREATE,BINDABLE,THREAD
          Open(ImportFile)
          Set(ImportFile,0)
          Loop
              next(ImportFile)
              if error() then break.
      
              !message('Got import file job number ='&clip(imp:JobNumber))
      !       imp:JobNumber defines the job - job:Ref_Number
      !       does this exist?       "Job not on servicebase"
              Access:jobs.clearkey(job:Ref_Number_Key)
              job:Ref_Number = imp:JobNumber
              if access:Jobs.fetch(job:Ref_Number_Key)
                  !error
      
                   Lineprint(clip(imp:jobNumber)&',Job not on ServiceBase',glo:file_name4)
                   Count += 1
              ELSE
      
                  !message('Looking at current status ='&clip(job:current_status))
                  !message('matching job location '&clip(job:location)& ' against '&clip(Tmp:DespatchLocation))
          !       despatched special case (901,902,905,810) "Job despatched"
                  if job:Current_Status[1] = '9' or job:location = tmp:DespatchLocation then
                      !message('Adding this to the exception file')
                      Lineprint(clip(imp:jobNumber)&',Job despatched',glo:file_name4)
                      Count += 1
                  ELSE
      
              !       Correct stauts         "Job not in correct status"
                      Case choice(?StatusSheet)
                          of 1    !job status
                              if job:Current_Status <>  status_temp
                                  Lineprint(clip(imp:jobNumber)&',Job not on selected status '&clip(Status_Temp),glo:file_name4)
                                  Count += 1
                              ELSE
                                  glo:Queue20.Pointer20 = job:Ref_Number
                                  Add(Glo:Queue20)
                              END
                          of 2    !exchange status
                              if job:Exchange_Status <> status3_temp
                                  Lineprint(clip(imp:jobNumber)&',Exchange not on selected status '&clip(Status3_Temp),glo:file_name4)
                                  Count += 1
                              ELSE
                                  glo:Queue20.Pointer20 = job:Ref_Number
                                  Add(Glo:Queue20)
                              END
                          of 3    !loan status
                              if job:Loan_Status <>  status4_temp
                                  Lineprint(clip(imp:jobNumber)&',Loan not on selected status '&clip(Status4_Temp),glo:file_name4)
                                  Count += 1
                              ELSE
                                  glo:Queue20.Pointer20 = job:Ref_Number
                                  Add(Glo:Queue20)
                              END
                      END !Case
                  END !if job despatched
              END !if jobs fetch worked
      
          END !loop through import file
      
          Close(ImportFile)
      
          if count > 0 then
      
              miss# = missive('Some jobs could not be tagged. An exception file has been created.','ServiceBase 3g','mexclam.jpg','OK')
      
              if glo:Webjob = 0 then
                  glo:file_name3 = clip(GETINI('EXCEPTION','SMS',,CLIP(Path()) & '\SB2KDEF.INI'))&'\'&clip(glo:file_name4)
                  copy(glo:file_name4,Glo:File_name3)
                  !message('File moved to '&clip(Glo:File_Name3))
              ELSE
                  flq:Filename = glo:file_name4
                  add(FileListQueue)
                  Return" = ClarioNET:SendFilesToClient(1,0)
              END
      
              remove(glo:file_name4)
              glo:file_name4 = ''
      
              !message('Got '&clip(count)&' exceptions in the exceptions file')
          END
      
          BRW1.resetsort(1)
      
      END
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonTagFromCSV, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?JobReceipt
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobReceipt, Accepted)
      Do JobReceipt
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobReceipt, Accepted)
    OF ?JobCard
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobCard, Accepted)
      Do JobCard
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobCard, Accepted)
    OF ?JobLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobLabel, Accepted)
      Do JobLabel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobLabel, Accepted)
    OF ?DeliveryLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeliveryLabel, Accepted)
      glo:Select1 = brw1.q.job:Ref_Number
      Address_Label('DEL','')
      glo:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeliveryLabel, Accepted)
    OF ?CollectionLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CollectionLabel, Accepted)
      glo:Select1 = brw1.q.job:Ref_Number
      Address_Label('COL','')
      glo:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CollectionLabel, Accepted)
    OF ?Letters
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Letters, Accepted)
      Do letters
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Letters, Accepted)
    OF ?MailMerge
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MailMerge, Accepted)
      Do MailMerge
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MailMerge, Accepted)
    OF ?DespatchNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DespatchNote, Accepted)
      Do DespatchNote
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DespatchNote, Accepted)
    OF ?Menu1ExchangeUnitLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1ExchangeUnitLabel, Accepted)
      Do ExchangeUnitLabel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1ExchangeUnitLabel, Accepted)
    OF ?Menu1LoanUnitLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1LoanUnitLabel, Accepted)
      Do LoanUnitLabel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1LoanUnitLabel, Accepted)
    OF ?Menu1Estimate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Estimate, Accepted)
      Do Estimate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Estimate, Accepted)
    OF ?Menu1Invoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Invoice, Accepted)
      DO Invoice
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Invoice, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020524'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020524'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020524'&'0')
      ***
    OF ?CreateNewJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateNewJob, Accepted)
      If SecurityCheck('JOBS - INSERT')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('JOBS - INSERT')
          glo:insert_global = 'YES'
          !glo:file_name   = 'J' & Format(CLock(),@n07) & '.DAT'
          GlobalRequest = InsertRecord
          Update_Jobs
          ! Inserting (DBH 14/07/2006) # 7975 - What reports are required to be printed?
          If GlobalResponse = 1
              ! Job has booked correctly (DBH: 14/07/2006)
              Loop x# = 1 To Records(glo:Queue)
                  Get(glo:Queue,x#)
                  Case glo:Pointer
                  Of 'THERMAL LABEL'
                      glo:Select1 = job:Ref_Number
                      If job:Bouncer = 'B'
                          Thermal_Labels('SE')
                      Else ! If job:Bouncer = 'B'
                          If ExchangeAccount(job:Account_Number)
                              Thermal_Labels('ER')
                          Else ! If ExchangeAccount(job:Account_Number)
                              Thermal_Labels('')
                          End ! If ExchangeAccount(job:Account_Number)
                      End ! If job:Bouncer = 'B'
                      glo:Select1 = ''
                  Of 'JOB CARD'
                      glo:Select1 = job:Ref_Number
                      Job_Card
                      glo:Select1 = ''
                  Of 'RETAINED ACCESSORIES LABEL'
                      glo:Select1 = job:Ref_Number
                      Job_Retained_Accessories_Label
                      glo:Select1 = ''
                  Of 'RECEIPT'
                      glo:Select1 = job:Ref_Number
                      Job_Receipt
                      glo:Select1 = ''
                  End ! Case glo:Pointer
              End ! Loop x# = 1 To Records(glo:Queue)
      
          End ! If GlobalReponse = 1
      End !SecurityCheck('JOBS - INSERT')
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateNewJob, Accepted)
    OF ?MarkLine
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MarkLine, Accepted)
      If SecurityCheck('MARK LINE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!If SecurityCheck('MARK LINE')
          Access:jobse.Clearkey(jobe:refnumberkey)
          jobe:refnumber  = brw1.q.job:ref_number
          If Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign
              !Found
              If jobe:jobmark = 1
                  jobe:jobmark = 0
              Else!If jobe:jobmark = 1
                  jobe:jobmark = 1
              End!If jobe:jobmark = 1
              access:jobse.update()
          Else! If Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign
              !Error
              If Access:jobse.Primerecord() = Level:Benign
                  jobe:jobmark = 1
                  jobe:refnumber  = brw1.q.job:ref_number
                  If Access:jobse.Tryinsert()
                      Access:jobse.Cancelautoinc()
                  End!If Access:jobse.Tryinsert()
              End!If Access:jobse.Primerecord() = Level:Benign
          End! If Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign
      End!If SecurityCheck('MARK LINE')
      BRW1.ResetQueue(reset:queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MarkLine, Accepted)
    OF ?Print_Routines
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
      check_access('PRINT ROUTINES',x")
      If x" = False
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!If x# = False
      
          Thiswindow.reset
          glo:select1  = job:ref_number
          Main_Print_Routines
          glo:select1 = ''
      End!If x# = False
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
    OF ?ChangeStatus
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeStatus, Accepted)
      If JobInUse(job:Ref_Number,1)
      
      Else ! If JobInUse(job:Ref_Number,1)
          check_access('CHANGE STATUS',x")
          If x" = False
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Else!If x" = False
      
              !TB12540 - Show contact history if there is a sticky note
              !need to find one that has not been cancelled, and is valid for this request
              Access:conthist.clearkey(cht:KeyRefSticky)
              cht:Ref_Number = job:ref_number
              cht:SN_StickyNote = 'Y'
              Set(cht:KeyRefSticky,cht:KeyRefSticky)      
              Loop
                  if access:Conthist.next() then break.
                  IF cht:Ref_Number <> job:ref_number then break.
                  if cht:SN_StickyNote <> 'Y' then break.
                  if cht:SN_Completed <> 'Y' and cht:SN_EngUpdate = 'Y' then
                      glo:select12 = job:ref_number
                      Browse_Contact_History
                      BREAK
                  END
              END
      
              Thiswindow.reset
              error# = 0
              clear(sts:record)
              access:status.clearkey(sts:status_key)
              sts:status = job:current_status
              if access:status.fetch(sts:status_key) = Level:Benign
                  If sts:ref_number = 799
                      check_access('JOBS - UNCANCEL',x")
                      If x" = False
                          Case Missive('This job has been cancelled and you do not have sufficient access to un-cancel it.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                          error# = 1
                      Else!If x" = False
                          Case Missive('This job has been cancelled. If you proceed it will be un-cancelled.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes') 
                              Of 2 ! Yes Button
                                  job:cancelled = 'NO'
                                  access:jobs.update()
                              Of 1 ! No Button
                                  error# = 1
                          End ! Case Missive
                      End!If x" = False
                  End!If sts:ref_number = 799
              End!if access:status.fetch(sts:status_key) = Level:Benign
              If error# = 0
                  Change_Status(job:ref_number)
              End!If error# = 0
          End!If x" = False
      End ! If JobInUse(job:Ref_Number,1)
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeStatus, Accepted)
    OF ?Payment_Details
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment_Details, Accepted)
      check_access('PAYMENT DETAILS',passed")
      if passed" = False
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!if passed" = False
      
          Thiswindow.reset(1)
          access:jobs_alias.clearkey(job_ali:ref_number_key)
          job_ali:ref_number = brw1.q.job:ref_number
          If access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
              continue# = 1
              If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                  continue# = 0
                  Case Missive('This job is marked as a warranty only job.'&|
                    '<13,10>'&|
                    '<13,10>It can only be marked as paid by reconciling the claim.','ServiceBase 3g',|
                                 'mstop.jpg','/OK') 
                      Of 1 ! OK Button
                  End ! Case Missive
              End!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
              If job_ali:date_completed = '' And continue# = 1
                  Case Missive('This job has not been completed. Are you sure you want to insert/amend a payment?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes') 
                      Of 2 ! Yes Button
                          continue# = 1
                      Of 1 ! No Button
                          continue# = 0
                  End ! Case Missive
              End!If job_ali:date_completed = ''
      
              If continue# = 1
      
                  Access:JOBS.ClearKey(job:ref_number_key)
                  job:ref_number = brw1.q.job:ref_number
                  if Access:JOBS.Fetch(job:ref_number_key) = Level:Benign
      
                      glo:select1 = job:ref_number
                      ! Before calling 'browse payments' make sure selected job record
                      ! is in the buffer
                      Browse_Payments()
      
                      access:jobs.clearkey(job:ref_number_key)
                      job:ref_number = glo:select1
                      if access:jobs.fetch(job:ref_number_key) = Level:Benign
                          ChkPaid()
                      End!if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      glo:select1 = ''
                  end
      
              End!If continue# = 1
          End!If access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
      
      
      End!if passed" = False
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment_Details, Accepted)
    OF ?CancelJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJob, Accepted)
      check_access('JOBS - CANCELLING',x")
      if x" = false
      
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      
      Else!if x" = false
          Case Missive('You are about to cancel the selected job. If you continue and entry will be made in the audit trail . You will be able to despatch this job back to the customer.'&|
            '<13,10>You cannot reverse this process!!'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
                  brw1.updatebuffer
                  error# = 0
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number  = brw1.q.job:ref_number
                  If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                      If job:consignment_number <> ''
                          Case Missive('Error! This job has already been despatched.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                          error# = 1
                      End!If job:consignment_number <> ''
                      If job:exchange_unit_number <> ''
                          Case Missive('Error! You cannot cancel a job that has an exchange unit attached.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                          error# = 1
                      End!If job:exchange_unit_number <> ''
                      If error# = 0
                          If job:loan_unit_number <> ''
                              Case Missive('Error! You cannot cancel a job that has a loan unit attached.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                              error# = 1
                          End!If job:loan_unit_number <> ''
                      End!If error# = 0
                  End!If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                  If error# = 0
                      !Complete Job
                      If job:date_completed = ''
                          job:date_completed  = Today()
                          job:time_completed  = Clock()
                          job:completed       = 'YES'
                          job:edi             = 'XXX'
      
                          !Update WEBJOB with completed info -  (DBH: 14-10-2003)
                          Access:WEBJOB.Clearkey(wob:RefNumberKey)
                          wob:RefNumber = job:Ref_Number
                          If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Found
                              wob:DateCompleted = job:Date_Completed
                              wob:TimeCompleted = job:Time_Completed
                              wob:Completed     = 'YES'
                              Access:WEBJOB.Update()
                          Else !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                            !Error
                          End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
      
                      End!If job:date_completed = ''
      
                      If job:consignment_number = ''
                          normal_despatch# = 1
                          restock# = 0
                          IF job:exchange_unit_number <> ''
                              normal_despatch# = 0
                              restock# = 1
                          Else!IF job:exchange_unit_number <> ''
                              If Sub(job:exchange_status,1,3) = '108'
                                  normal_despatch# = 0
                                  restock# = 1
                              End!If Sub(job:exchange_status,1,3) = '108'
                              access:trantype.clearkey(trt:transit_type_key)          !In case hasn't been attached yet!
                              trt:transit_type = job:transit_type
                              if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                  If trt:Exchange_Unit = 'YES'
                                      normal_despatch# = 0
                                      restock# = 1
                                  End!If trt:Exchange_Unit = 'YES'
                              end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                          End!!IF job:exchange_unit_number <> ''
      
                          If normal_despatch# = 0 and job:loan_unit_number <> ''
                              Case Missive('A loan unit has been issued to this job but the initial transit type states than an exchange unit is required.'&|
                                '<13,10>Do you wish to RESTOCK this unit, or mark it for DESPATCH back to the customer?','ServiceBase 3g',|
                                             'mquest.jpg','Despatch|Restock')
                                  Of 2 ! Restock Button
                                      normal_despatch# = 0
                                      restock# = 1
                                  Of 1 ! Despatch Button
                                      normal_despatch# = 1
                              End ! Case Missive
                          End!If normal_despatch# = 0 and job:loan_unit_number <> ''
      
                          If normal_despatch# = 1
      
                              access:courier.clearkey(cou:courier_key)
                              cou:courier = job:courier
                              if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                  If cou:courier_type = 'ANC'
                                      Case Today() % 7
                                          Of 6 !Saturday
                                              If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                                                  job:date_despatched = Today() + 2
                                              End!If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                                              If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                                                  job:date_despatched = Today() + 1
                                              End!If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                                              IF def:include_saturday = 'YES' and def:include_sunday <> 'YES'
                                                  If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today() + 2
                                                  Else!If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today()
                                                  End!If clock() > cou:last_despatch_time
                                              End!IF def:include_saturday = 'YES' and def:include_sunday <> 'YES'
                                              If def:include_saturday = 'YES' and def:include_sunday = 'YES'
                                                  If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today() + 1
                                                  Else!If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today()
                                                  End!If clock() > cou:last_despatch_time
                                              End!If def:include_saturday = 'YES' and def:include_sunday = 'YES'
                                          Of 0 !Sunday
                                              If def:include_sunday = 'YES'
                                                  If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today() + 1
                                                  Else!If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today()
                                                  End!If clock() > cou:last_despatch_time
                                              Else!If def:include_sunday = 'YES'
                                                  job:date_despatched = Today() + 1
                                              End!If def:include_sunday = 'YES'
                                          Of 5 !Friday
                                              If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                                                  If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today() + 3
                                                  Else!If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today()
                                                  End!If clock() > cou:last_despatch_time
                                              End!If def:include_saturday <> 'YES' And def:include_sunday <> 'YES'
                                              If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                                                  If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today() + 2
                                                  Else!If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today()
                                                  End!If clock() > cou:last_despatch_time
                                              End!If def:include_saturday <> 'YES' and def:include_sunday = 'YES'
                                              IF def:include_saturday = 'YES'
                                                  If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today() + 1
                                                  Else!If clock() > cou:last_despatch_time
                                                      job:date_despatched = Today()
                                                  End!If clock() > cou:last_despatch_time
                                              End!IF def:include_saturday = 'YES' and def:include_sunday <> 'YES'
                                          Else
                                              If clock() > cou:last_despatch_time
                                                  job:date_despatched = Today() + 1
                                              Else!If clock() > cou:last_despatch_time
                                                  job:date_despatched = Today()
                                              End!If clock() > cou:last_despatch_time
                                      End!Case Today() % 7
                                  End!If cou:courier_type = 'ANC'
                              end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
      
                              access:subtracc.clearkey(sub:account_number_key)
                              sub:account_number = job:account_number
                              if access:subtracc.fetch(sub:account_number_key) = level:benign
                                  access:tradeacc.clearkey(tra:account_number_key)
                                  tra:account_number = sub:main_account_number
                                  if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      If tra:IgnoreDespatch = 'YES'
                                          ignore_despatch# = 1
                                      End!If tra:IgnoreDespatch = 'YES'
                                  end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                              end!if access:subtracc.fetch(sub:account_number_key) = level:benign
                              job:despatched = 'REA'
                              job:despatch_type = 'JOB'
                              job:current_courier = job:courier
      
                          Else        !If normal_despatch# = 1
                              If restock# = 1
                                  !job:despatched = 'RTS'
                              Else!If restock# = 1
                                  job:despatched = 'YES'
                              End!If restock# = 1
      
                              job:date_despatched = Today()
                              job:consignment_number = 'N/A'
                              access:users.clearkey(use:password_key)
                              use:password    = glo:password
                              access:users.tryfetch(use:password_key)
                              job:despatch_user = use:user_code
                              get(desbatch,0)
                              if access:desbatch.primerecord() =  Level:Benign
                                  if access:desbatch.insert()
                                     access:desbatch.cancelautoinc()
                                  end
                              end!if access:desbatch.primerecord() =  Level:Benign
                              job:despatch_number = dbt:batch_number
                          End!If normal_despatch# = 1
                      End!If job:consignment_number = ''
                      GetStatus(799,1,'JOB')
                      job:cancelled       = 'YES'
                      job:Bouncer         = ''
      
      
      !                !TB13490 - J - 19/02/15 when the job is cancelled it MUST be removed from the warranty process
      !                job:edi             = 'XXX'
      !                Access:jobswarr.clearkey(jow:RefNumberKey)
      !                jow:RefNumber = job:Ref_number
      !                Set(jow:RefNumberKey,jow:RefNumberKey)
      !                Loop        !have to loop there may be more than one
      !                    if access:jobswarr.next() then break.
      !                    if jow:RefNumber <> job:Ref_number then break.
      !                    if jow:Status <> 'YES' then
      !                        !mark this as rejected rather than let it go through again
      !                        jow:Status = 'REJ'
      !                        jow:DateRejected = today()
      !                        jow:DateFinalRejection = today()
      !                        jow:RRCStatus = 'AAJ'
      !                        Access:jobswarr.update()
      !                        IF (AddToAudit(job:ref_number,'JOB','WARRANTY CLAIM CANCELLED','CLAIM MARKED AS REJECTED AS JOB IS CANCELLED')
      !                        END ! IF
      !                    END !if not already marked as paid!
      !                END !if jobswarr.fetched
      !                !END TB13490 
      
                      access:jobs.update()
      
                      IF (AddToAudit(job:ref_number,'JOB','JOB CANCELLED',CancelReason()))
                      END ! IF
      
      
                  End!If error# = 0
              Of 1 ! No Button
          End ! Case Missive
      End!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJob, Accepted)
    OF ?FailedDelivery
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailedDelivery, Accepted)
      If SecurityCheck('JOBS - FAILED DELIVERY')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else !SecurityCheck('FAILED DELIVERY')
          !Which type of delivery has failed
          brw1.UpdateViewRecord()
          Case Missive('Are you sure you want to mark the selected job as a "Failed Delivery"?'&|
            '<13,10>'&|
            '<13,10>You will not be able to undo this action!','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
      
                  !Which of the units attached to the job can be failed,
                  !i.e., already been desaptched.
      
                  Job# = 0
                  Exchange# = 0
                  Loan# = 0
                  If job:Consignment_Number <> ''
                      Job# = 1
                  End !job:Consignment_Number <> ''
                  If job:Exchange_Consignment_Number <> '' and job:Exchange_Unit_Number <> ''
                      Exchange# = 1
                  End !job:Exchange_Consignment_Number <> ''
                  If job:Loan_Consignment_Number <> '' And job:Loan_Unit_Number <> ''
                      Loan# = 1
                  End !job:Loan_Consignment_Number <> ''
      
                  !Work out which unit to fail
                  !Job Exchange or Loan
                  If Job# And Exchange#
                      Case Missive('Which unit do you want to mark as a "Failed Delivery"?'&|
                        '<13,10>'&|
                        '<13,10>Job Unit, or Exchange Unit?','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|Exchange|Job') 
                          Of 3 ! Job Button
                              Job# = 1
                              Exchange# = 0
                              Loan# = 0
                          Of 2 ! Exchange Button
                              Job# = 0
                              Exchange# = 1
                              Loan# = 0
                          Of 1 ! Cancel Button
                              Job# = 0
                              Exchange# = 0
                              Loan# = 0
                      End ! Case Missive
                  End !If Job# And Exchange#
      
                  If Job# And Loan#
                      Case Missive('Which unit do you want to mark as a "Failed Delivery"?'&|
                        '<13,10>'&|
                        '<13,10>Job Unit, or Loan Unit?','ServiceBase 3g',|
                                     'mquest.jpg','\Cancel|Loan|Job') 
                          Of 3 ! Job Button
                              Job# = 1
                              Exchange# = 0
                              Loan# = 0
                          Of 2 ! Loan Button
                              Job# = 0
                              Exchange# = 0
                              Loan# = 1
                          Of 1 ! Cancel Button
                              Job# = 0
                              Exchange# = 0
                              Loan# = 0
                      End ! Case Missive
                  End !Job# And Exchange# Or Job# And Loan#
      
                  If Job# Or Exchange# Or Loan#
                      !Get the JOBSE file and check if this job isn't already a failed delivery
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBSE.PrimeRecord() = Level:Benign
                              jobe:RefNumber  = job:Ref_Number
                              If Access:JOBSE.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBSE.TryInsert() = Level:Benign
                                  !Insert Failed
                              End!If Access:JOBSE.TryInsert() = Level:Benign
                          End !If Access:JOBSE.PrimeRecord() = Level:Benign
                      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      !Failed Delivery Types
                      !1 - Job Only
                      !2 - Exchange Only
                      !3 - Loan Only
                      !4 - Job And Exchange
                      !5 - Job And Loan
      
                      If Job#
                          If jobe:FailedDelivery = 1 Or jobe:FailedDelivery = 4 Or jobe:FailedDelivery = 5
                              Case Missive('This job has already been marked as a "Failed Delivery".','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                          Else
                              !Incase this job is already a failed delivery
                              Case jobe:FailedDelivery
                                  Of 2 !Already an Exchange
                                      jobe:FailedDelivery = 4
                                  Of 3 !Already a Loan
                                      jobe:FailedDelivery = 5
                                  Else
                                      jobe:FailedDelivery = 1
                              End !Case jobe:FailedDelivery
                              Access:JOBSE.Update()
                              LocalFailedDeliveryAudit('JOB')
                          End !if jobe:FailedDelivery = 1
                      End !If Job#
                      If Exchange#
                          If jobe:FailedDelivery = 2 Or jobe:FailedDelivery = 4
                              Case Missive('This exchange unit has already been marked as a "Failed Delivery".','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                          Else
                              !Incase this job is already a failed delivery
                              Case jobe:FailedDelivery
                                  Of 1 !Already a job
                                      jobe:FailedDelivery = 4
                                  Else
                                      jobe:FailedDelivery = 2
                              End !Case jobe:FailedDelivery
                              Access:JOBSE.Update()
                              LocalFailedDeliveryAudit('EXC')
                          End !if jobe:FailedDelivery = 1
                      End !If Job#
                      If Loan#
                          If jobe:FailedDelivery = 3 Or jobe:FailedDelivery = 5
                              Case Missive('This loan unit has already been marked as a "Failed Delivery".','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                          Else
                              !Incase this job is already a failed delivery
                              Case jobe:FailedDelivery
                                  Of 1 !Already a job
                                      jobe:FailedDelivery = 5
                                  Else
                                      jobe:FailedDelivery = 3
                              End !Case jobe:FailedDelivery
                              Access:JOBSE.Update()
                              LocalFailedDeliveryAudit('LOA')
                          End !if jobe:FailedDelivery = 1
                      End !If Job#
                  Else !If Job# Or Exchange# Or Loan#
                      Case Missive('The unit(s) attached to this job have not been despatched.','ServiceBase 3g',|
                                     'mstop.jpg','/OK') 
                          Of 1 ! OK Button
                      End ! Case Missive
                  End !If Job# Or Exchange# Or Loan#
      
              Of 1 ! No Button
          End ! Case Missive
      End !SecurityCheck('FAILED DELIVERY')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailedDelivery, Accepted)
    OF ?RapidUpdateButton:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidUpdateButton:2, Accepted)
      if (SecurityCheck('JOBS - CHANGE'))  ! #11682 Check access. (Bryan: 07/09/2010)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You do not have access to this option.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          Cycle
      End
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number  = brw1.q.job:ref_number
          If access:jobs.tryfetch(job:ref_number_key) = Level:benign
              GlobalRequest = ChangeRecord
              Update_Jobs_Rapid
      !        if GlobalResponse = RequestCompleted
                  BRW1.ResetSort(1)
      !        end
          Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              !Error
          End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?RapidUpdateButton:2, Accepted)
    OF ?KeyColour
      ThisWindow.Update
      Colour_Key
      ThisWindow.Reset
    OF ?tmp:AllJobsType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllJobsType, Accepted)
      BRW1.ResetSort(1)
      do Show_Hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllJobsType, Accepted)
    OF ?tmp:Location2
      IF tmp:Location2 OR ?tmp:Location2{Prop:Req}
        loi:Location = tmp:Location2
        !Save Lookup Field Incase Of error
        look:tmp:Location2        = tmp:Location2
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Location2 = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:Location2 = look:tmp:Location2
            SELECT(?tmp:Location2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation2
      ThisWindow.Update
      loi:Location = tmp:Location2
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Location2 = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location2)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location2)
    OF ?Count_Jobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
          Case tmp:AllJobsType
              Of 0
                  count# = 0
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  set(job:ref_number_key)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
      
              Of 1
                  count# = 0
                  setcursor(cursor:wait)
                  Save_job_ID = Access:JOBS.SaveFile()
                  Access:JOBS.ClearKey(job:Location_Key)
                  job:Location   = tmp:Location2
                  Set(job:Location_Key,job:Location_Key)
                  Loop
                      If Access:JOBS.NEXT()
                         Break
                      End !If
                      If job:Location   <> tmp:Location2      |
                          Then Break.  ! End If
                      count# += 1
                  End !Loop
                  Access:JOBS.RestoreFile(Save_job_ID)
                  setcursor()
          End !Case tmp:AllJobsType
      
          Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                         'midea.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs, Accepted)
    OF ?tmp:GlobalIMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:GlobalIMEINumber, Accepted)
      If (clip(tmp:GlobalIMEINumber) <> '')    ! ! #11974 Don't look for blanks (Bryan: 16/02/2011)
          !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
          IF LEN(CLIP(tmp:GlobalIMEINumber)) = 18
            !Ericsson IMEI!
            tmp:GlobalIMEINumber = SUB(tmp:GlobalIMEINumber,4,15)
            !ESN_Entry_Temp = Job:ESN
            UPDATE()
          ELSE
            !Job:ESN = ESN_Entry_Temp
          END
          If GetTempPathA(255,TempFilePath)
              If Sub(Clip(TempFilePath),-1,1) = '\'
                  glo:File_Name   = Clip(TempFilePath) & 'GLOBIMEI' & Clock() & '.TMP'
              Else !If Sub(Clip(TempFilePath),-1,1) = '\'
                  glo:File_Name   = Clip(TempFilePath) & '\GLOBIMEI' & Clock() & '.TMP'
              End !If Sub(Clip(TempFilePath),-1,1) = '\'
          End
      
          Remove(glo:File_Name)
          Access:ADDSEARCH.Open()
          Access:ADDSEARCH.UseFile()
      
          ! Inserting (DBH 23/11/2006) # 8405 - Call routine to show imeis
          BuildIMEISearch(tmp:GlobalIMEINumber)
          ! End (DBH 23/11/2006) #8405
      
          Access:ADDSEARCH.Close()
      
          !IMEISearch(tmp:GlobalIMEINumber)
          IMEIHistorySearch(tmp:GlobalIMEINumber)
      
          Remove(glo:File_Name)
      
      END ! If (clip(tmp:GlobalIMEINumber) <> '')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:GlobalIMEINumber, Accepted)
    OF ?Count_Jobs:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:2, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              count# = 0
              setcursor(cursor:wait)
      
              save_job_id = access:jobs.savefile()
              set(job:ref_number_key)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:2, Accepted)
    OF ?Count_Jobs:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:3, Accepted)
      Case Missive('Counting the jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          set(job:ref_number_key)
          loop
              if access:jobs.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case Missive('There are ' & count# & ' records in this browse.'&|
            '<13,10>','ServiceBase 3g',|
                         'midea.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:3, Accepted)
    OF ?tmp:orderNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:orderNumber, Accepted)
      If (clip(tmp:OrderNumber) <> '')
      
          If GetTempPathA(255,TempFilePath)
              If Sub(Clip(TempFilePath),-1,1) = '\'
                  glo:File_Name   = Clip(TempFilePath) & 'GLOBORD' & Clock() & '.TMP'
              Else !If Sub(Clip(TempFilePath),-1,1) = '\'
                  glo:File_Name   = Clip(TempFilePath) & '\GLOBORD' & Clock() & '.TMP'
              End !If Sub(Clip(TempFilePath),-1,1) = '\'
          End
      
          Prog.ProgressSetup(Records(SUBTRACC))
      
          Remove(glo:File_Name)
          Access:ADDSEARCH.Open()
          Access:ADDSEARCH.UseFile()
      
          Save_sub_ID = Access:SUBTRACC.SaveFile()
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          Set(sub:Account_Number_Key,sub:Account_Number_Key)
          Loop
              If Access:SUBTRACC.NEXT()
                 Break
              End !If
      
      
              If Prog.InsideLoop()
                Break
              End ! If Prog.InsideLoop()
      
              Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
              Access:JOBS_ALIAS.ClearKey(job_ali:AccOrdNoKey)
              job_ali:Account_Number = sub:Account_Number
              job_ali:Order_Number   = tmp:OrderNumber
              Set(job_ali:AccOrdNoKey,job_ali:AccOrdNoKey)
              Loop
                  If Access:JOBS_ALIAS.NEXT()
                     Break
                  End !If
                  If job_ali:Account_Number <> sub:Account_Number      |
                  Or job_ali:Order_Number   <> tmp:OrderNumber      |
                      Then Break.  ! End If
                  If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                      addtmp:AddressLine1  = job_ali:Address_Line1
                      addtmp:AddressLine2  = job_ali:Address_Line2
                      addtmp:AddressLine3  = job_ali:Address_Line3
                      addtmp:Postcode      = job_ali:Postcode
                      addtmp:JobNumber     = job_ali:Ref_Number
                      addtmp:FinalIMEI     = job_ali:ESN
                      addtmp:Surname       = job_ali:Surname
                      If Access:ADDSEARCH.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                          !Insert Failed
                      End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
                  End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
              End !Loop
              Access:JOBS.RestoreFile(Save_job_ID)
          End !Loop
          Access:SUBTRACC.RestoreFile(Save_sub_ID)
          Prog.ProgressFinish()
      
          Access:ADDSEARCH.Close()
      
          OrderSearch(tmp:OrderNumber)
      
          Remove(glo:File_Name)
      end ! If (clip(tmp:OrderNumber) <> '')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:orderNumber, Accepted)
    OF ?tmp:AccountSearchOrder
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountSearchOrder, Accepted)
      Case tmp:AccountSearchOrder
          Of 0
              ?JobNumber{prop:Hide} = 1
              ?job:Ref_Number:10{prop:Hide} = 1
              ?OrderNumber{prop:Hide} = 0
              ?job:Order_Number{prop:Hide} = 0
          Of 1
              ?JobNumber{prop:Hide} = 0
              ?job:Ref_Number:10{prop:Hide} = 0
              ?OrderNumber{prop:Hide} = 1
              ?job:Order_Number{prop:Hide} = 1
      End !tmp:AccountSearchOrder
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountSearchOrder, Accepted)
    OF ?account_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number_temp, Accepted)
      IF account_number_temp OR ?account_number_temp{Prop:Req}
        sub:Account_Number = account_number_temp
        !Save Lookup Field Incase Of error
        look:account_number_temp        = account_number_temp
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            account_number_temp = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            account_number_temp = look:account_number_temp
            SELECT(?account_number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number_temp, Accepted)
    OF ?Lookup_Account_number
      ThisWindow.Update
      sub:Account_Number = account_number_temp
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          account_number_temp = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?account_number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?account_number_temp)
    OF ?Count_Jobs:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:4, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
              
      
              count# = 0
              setcursor(cursor:wait)
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:accordnokey)
              job:account_number = account_number_temp
              set(job:accordnokey,job:accordnokey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:account_number <> account_number_temp      |
                      then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:4, Accepted)
    OF ?SearchByAddress
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SearchByAddress, Accepted)
      If GetTempPathA(255,TempFilePath)
          If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & 'GLOBADD' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & '\GLOBADD' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
      Remove(glo:File_Name)
      Access:ADDSEARCH.Open()
      Access:ADDSEARCH.UseFile()
      
      Count# = 0
      
      Setcursor(Cursor:Wait)
      Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
      Access:JOBS_ALIAS.ClearKey(job_ali:Surname_Key)
      job_ali:Surname = brw1.q.job:Surname
      Set(job_ali:Surname_Key,job_ali:Surname_Key)
      Loop
          If Access:JOBS_ALIAS.NEXT()
             Break
          End !If
          If job_ali:Surname <> brw1.q.job:Surname      |
              Then Break.  ! End If
          Count# += 1
          If Count# > 300
              Count# = Records(JOBS)
              Break
          End !If Count# > 300
      
      End !Loop
      Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
      Setcursor()
      
      Prog.ProgressSetup(Count#)
      
      Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
      Access:JOBS_ALIAS.ClearKey(job_ali:Surname_Key)
      job_ali:Surname = brw1.q.job:Surname
      Set(job_ali:Surname_Key,job_ali:Surname_Key)
      Loop
          If Access:JOBS_ALIAS.NEXT()
             Break
          End !If
          If job_ali:Surname <> brw1.q.job:Surname      |
              Then Break.  ! End If
      
          If Prog.InsideLoop()
            Break
          End ! If Prog.InsideLoop()
      
          If Access:ADDSEARCH.PrimeRecord() = Level:Benign
              addtmp:AddressLine1 = job_ali:Address_Line1
              addtmp:AddressLine2 = job_ali:Address_Line2
              addtmp:AddressLine3 = job_ali:Address_Line3
              addtmp:Postcode     = job_ali:Postcode
              addtmp:JobNumber    = job_ali:Ref_Number
              If Access:ADDSEARCH.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                  !Insert Failed
              End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
          End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
      End !Loop
      Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
      
      Prog.ProgressFinish()
      
      Access:ADDSEARCH.Close()
      
      AddressSearch(brw1.q.job:Surname)
      
      Remove(glo:File_Name)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SearchByAddress, Accepted)
    OF ?Count_Jobs:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:5, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
              count# = 0
              setcursor(cursor:wait)
      
              save_job_id = access:jobs.savefile()
              set(job:ref_number_key)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:5, Accepted)
    OF ?tmp:SelectLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectLocation, Accepted)
      DO refresh_job_status_tab
      BRW1.ResetSort(1)
      do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectLocation, Accepted)
    OF ?tmp:JobLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobLocation, Accepted)
      Do refresh_job_status_tab
      BRW1.ResetSort(1)
      IF tmp:JobLocation OR ?tmp:JobLocation{Prop:Req}
        loi:Location = tmp:JobLocation
        !Save Lookup Field Incase Of error
        look:tmp:JobLocation        = tmp:JobLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:JobLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:JobLocation = look:tmp:JobLocation
            SELECT(?tmp:JobLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobLocation, Accepted)
    OF ?Lookup_Location
      ThisWindow.Update
      loi:Location = tmp:JobLocation
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:JobLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:JobLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:JobLocation)
    OF ?status_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
      glo:select1    = 'JOB'
      sts:job   = 'YES'
      Do refresh_job_status_tab
      IF status_temp OR ?status_temp{Prop:Req}
        sts:Status = status_temp
        !Save Lookup Field Incase Of error
        look:status_temp        = status_temp
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            status_temp = sts:Status
          ELSE
            !Restore Lookup On Error
            status_temp = look:status_temp
            SELECT(?status_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      glo:select1    = ''
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
    OF ?Lookup_Job_Status
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Job_Status, Accepted)
      sts:job   = 'YES'
      glo:select1    = 'JOB'
      sts:Status = status_temp
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          status_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?status_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?status_temp)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Job_Status, Accepted)
    OF ?status3_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status3_temp, Accepted)
      glo:select1    = 'EXC'
      sts:exchange   = 'YES'
      IF status3_temp OR ?status3_temp{Prop:Req}
        sts:Status = status3_temp
        !Save Lookup Field Incase Of error
        look:status3_temp        = status3_temp
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            status3_temp = sts:Status
          ELSE
            !Restore Lookup On Error
            status3_temp = look:status3_temp
            SELECT(?status3_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status3_temp, Accepted)
    OF ?Lookup_Status3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status3, Accepted)
      glo:select1    = 'EXC'
      sts:exchange      = 'YES'
      sts:Status = status3_temp
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          status3_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?status3_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?status3_temp)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status3, Accepted)
    OF ?status4_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status4_temp, Accepted)
      glo:select1    = 'LOA'
      sts:loan      = 'YES'
      IF status4_temp OR ?status4_temp{Prop:Req}
        sts:Status = status4_temp
        !Save Lookup Field Incase Of error
        look:status4_temp        = status4_temp
        IF Access:STATUS.TryFetch(sts:LoanKey)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            status4_temp = sts:Status
          ELSE
            !Restore Lookup On Error
            status4_temp = look:status4_temp
            SELECT(?status4_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select1    = ''
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status4_temp, Accepted)
    OF ?Lookup_Status4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status4, Accepted)
      glo:select1    = 'LOA'
      sts:loan      = 'YES'
      sts:Status = status4_temp
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          status4_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?status4_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?status4_temp)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status4, Accepted)
    OF ?Count_Jobs:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:6, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
      
              count# = 0
              setcursor(cursor:wait)
      
              Case Choice(?StatusSheet)
                  Of 1 !Job
                      Case tmp:SelectLocation
                          Of 1
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:by_status)
                              job:current_status = status_temp
                              set(job:by_status,job:by_status)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:current_status <> status_temp      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  count# += 1
                              end !loop
                              access:jobs.restorefile(save_job_id)
      
                          Of 2
                              Save_job_ID = Access:JOBS.SaveFile()
                              Access:JOBS.ClearKey(job:StatusLocKey)
                              job:Current_Status = Status_Temp
                              job:Location       = tmp:JobLocation
                              Set(job:StatusLocKey,job:StatusLocKey)
                              Loop
                                  If Access:JOBS.NEXT()
                                     Break
                                  End !If
                                  !If job:Current_Status <> Status_Temp      | then OR next line
                                  IF job:Location       <> tmp:JobLocation      |
                                      Then Break.  ! End If
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                      Yield() ; yldcnt# = 0
                                  ENd !if
                                  Count# += 1
                              End !Loop
                              Access:JOBS.RestoreFile(Save_job_ID)
                      End !Case tmp:SelectLocation
      
                  Of 2 !Exchange
      
                      save_job_id = access:jobs.savefile()
                      job:Exchange_Status  = status3_temp
                      set(job:ExcStatusKey,job:ExcStatusKey)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          If job:exchange_status <> status3_temp
                              Break
                          End!If job:exchangestatus <> status3_temp
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          count# += 1
                      end !loop
                      access:jobs.restorefile(save_job_id)
                  Of 3 !Loan
      
                      save_job_id = access:jobs.savefile()
                      job:Loan_Status  = status4_temp
                      set(job:LoanStatuskey,job:LoanStatusKey)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          If job:Loan_Status <> status4_temp
                              Break
                          End!If job:LoanStatus <> statu4_temp
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          count# += 1
                      end !loop
                      access:jobs.restorefile(save_job_id)
              End ! Case Choice(?StatusSheet)
      
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:6, Accepted)
    OF ?Count_Jobs:15
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:15, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              count# = 0
              setcursor(cursor:wait)
      
              save_job_id = access:jobs.savefile()
              job:Exchange_Status  = status3_temp
              set(job:ExcStatusKey,job:ExcStatusKey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  If job:exchange_status <> status3_temp
                      Break
                  End!If job:exchangestatus <> status3_temp
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:15, Accepted)
    OF ?Count_Jobs:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:8, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
              count# = 0
              setcursor(cursor:wait)
      
              save_job_id = access:jobs.savefile()
              job:Loan_Status  = status4_temp
              set(job:LoanStatuskey,job:LoanStatusKey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  If job:Loan_Status <> status4_temp
                      Break
                  End!If job:LoanStatus <> statu4_temp
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:8, Accepted)
    OF ?Count_Jobs:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:7, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
      
              count# = 0
              setcursor(cursor:wait)
      
              save_job_id = access:jobs.savefile()
              set(job:ref_number_key)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:7, Accepted)
    OF ?model_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?model_number_temp, Accepted)
      BRW1.ResetQueue(1)
      IF model_number_temp OR ?model_number_temp{Prop:Req}
        mod:Model_Number = model_number_temp
        !Save Lookup Field Incase Of error
        look:model_number_temp        = model_number_temp
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            model_number_temp = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            model_number_temp = look:model_number_temp
            SELECT(?model_number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?model_number_temp, Accepted)
    OF ?Lookup_Model_Number
      ThisWindow.Update
      mod:Model_Number = model_number_temp
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          model_number_temp = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?model_number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?model_number_temp)
    OF ?Count_Jobs:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:9, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              count# = 0
              setcursor(cursor:wait)
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:model_unit_key)
              job:model_number = model_number_temp
              set(job:model_unit_key,job:model_unit_key)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:model_number <> model_number_temp      |
                      then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:9, Accepted)
    OF ?engineer_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?engineer_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?engineer_temp, Accepted)
    OF ?Lookup_Engineer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      Browse_Users_job_Assignment
      if globalresponse = requestcompleted
          engineer_temp = use:user_code
          display()
      end
      globalrequest     = saverequest#
      BRW1.ResetQueue(1)
      thiswindow.reset
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
    OF ?Completed2_Temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Completed2_Temp:2, Accepted)
      If Completed2_Temp = 'ALL'
          ?job:ref_Number:3{prop:Hide} = 1
          ?JobNumber:Prompt{prop:Hide} = 1
      Else !Completed2_Temp = 'ALL'
          ?job:ref_Number:3{prop:Hide} = 0
          ?JobNumber:Prompt{prop:Hide} = 0
      End !Completed2_Temp = 'ALL'
      
      Thiswindow.reset
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Completed2_Temp:2, Accepted)
    OF ?Count_Jobs:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:10, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
          count# = 0
          Case completed2_temp
              Of 'YES'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:EngCompKey)
                  job:engineer   = engineer_temp
                  job:completed  = 'YES'
                  set(job:EngCompKey,job:EngCompKey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:engineer   <> engineer_temp      |
                      or job:completed  <> 'YES'      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 'NO'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:EngCompKey)
                  job:engineer   = engineer_temp
                  job:completed  = 'NO'
                  set(job:EngCompKey,job:EngCompKey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:engineer   <> engineer_temp      |
                      or job:completed  <> 'NO'      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 'ALL'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:engineer_key)
                  job:engineer   = engineer_temp
                  set(job:engineer_key,job:engineer_key)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:engineer   <> engineer_temp |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
          End!Case completed2_temp
      
          Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                         'midea.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:10, Accepted)
    OF ?UnallocatedJobsReport
      ThisWindow.Update
      Unallocated_Criteria
      ThisWindow.Reset
    OF ?Count_Jobs:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:11, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              count# = 0
              setcursor(cursor:wait)
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:EngWorkKey)
              job:engineer   = ''
              job:workshop   = 'YES'
              set(job:EngWorkKey,job:EngWorkKey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:engineer   <> ''    |
                  or job:workshop   <> 'YES' |
                      then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  count# += 1
              end !loop
              access:jobs.restorefile(save_job_id)
              setcursor()
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:11, Accepted)
    OF ?select_courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?select_courier_temp, Accepted)
      Do DASBRW::26:DASUNTAGALL
      Do refresh_despatch_tab
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?select_courier_temp, Accepted)
    OF ?courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
      Do refresh_despatch_tab
      BRW1.ResetSort(1)
      IF courier_temp OR ?courier_temp{Prop:Req}
        cou:Courier = courier_temp
        !Save Lookup Field Incase Of error
        look:courier_temp        = courier_temp
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(8,SelectRecord) = RequestCompleted
            courier_temp = cou:Courier
          ELSE
            !Restore Lookup On Error
            courier_temp = look:courier_temp
            SELECT(?courier_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
    OF ?Lookup_Courier
      ThisWindow.Update
      cou:Courier = courier_temp
      
      IF SELF.RUN(8,Selectrecord)  = RequestCompleted
          courier_temp = cou:Courier
          Select(?+1)
      ELSE
          Select(?courier_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?courier_temp)
    OF ?Select_Trade_Account_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select_Trade_Account_Temp, Accepted)
      Do DASBRW::26:DASUNTAGALL
      Do refresh_despatch_tab
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select_Trade_Account_Temp, Accepted)
    OF ?account_number2_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number2_temp, Accepted)
      Do refresh_despatch_tab
      BRW1.ResetSort(1)
      IF account_number2_temp OR ?account_number2_temp{Prop:Req}
        sub:Account_Number = account_number2_temp
        !Save Lookup Field Incase Of error
        look:account_number2_temp        = account_number2_temp
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            account_number2_temp = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            account_number2_temp = look:account_number2_temp
            SELECT(?account_number2_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number2_temp, Accepted)
    OF ?Lookup_Account_Number:2
      ThisWindow.Update
      sub:Account_Number = account_number2_temp
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          account_number2_temp = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?account_number2_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?account_number2_temp)
    OF ?Multiple_Despatch2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multiple_Despatch2, Accepted)
      check_access('MULTIPLE DESPATCH',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      else!if x" = false
      !Has something been tagged already.
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
      
          If Select_Courier_Temp = 'IND' And Select_Trade_Account_Temp = 'IND'
              If Courier_Temp <> '' And Account_Number2_Temp <> ''
                  If Records(glo:Queue20)
                      Case Missive('Jobs have been tagged to be despatched.'&|
                        '<13,10>'&|
                        '<13,10>Do you want to automatically create a Multiple Despatch Batch from these jobs?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes') 
                          Of 2 ! Yes Button
                              If NormalDespatchCourier(Courier_Temp)
                                  Case Missive('The selected courier can only despatch jobs through "Despatch Procedures".','ServiceBase 3g',|
                                                 'mstop.jpg','/OK') 
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              Else !If NormalDespatchCourier(Courier_Temp)
                                  If cou:Courier_Type = 'TOTE'
                                      Case Missive('The selected courier can only be despatched individually.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK') 
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                  ELse !If cou:Courier_Type = 'TOTE'
                                  
                                      tmp:PrintDespatchNote = 0
                                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                                      sub:Account_Number  = Account_Number2_Temp
                                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                          !Found
                                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                          tra:Account_Number  = sub:Main_Account_Number
                                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Found
                                              If tra:Use_Sub_Accounts = 'YES'
                                                  If sub:Print_Despatch_Despatch = 'YES' And sub:Despatch_Note_Per_Item = 'YES'
                                                      tmp:PrintDespatchNote = 1
                                                  End !If sub:Print_Despatch_Despatch = 'YES'
                                              Else !If tra:Use_Sub_Accounts = 'YES'
                                                  If tra:Print_Despatch_Despatch = 'YES' And tra:Despatch_Note_Per_Item = 'YES'
                                                      tmp:PrintDespatchNote = 1
                                                  End !If tra:Print_Despatch_Despatch = 'YES'
                                              End !If tra:Use_Sub_Accounts = 'YES'
                                          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                          
                                      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
                                      If tmp:PrintDespatchNote
                                          Case Missive('Do you wish to print an individual despatch note for each job in the batch?','ServiceBase 3g',|
                                                         'mquest.jpg','\No|/Yes') 
                                              Of 2 ! Yes Button
                                              Of 1 ! No Button
                                                  tmp:PrintDespatchNote = 0
                                          End ! Case Missive
                                      End !If tmp:PrintDespatchNote
      
                                      tmp:FirstRecord = 1
                                      tmp:BatchNumber = 0
                                      tmp:BatchCreated = 0
                                      Loop x# = 1 To Records(glo:Queue20)
                                          Get(glo:Queue20,x#)
                                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                                          job:Ref_Number  = glo:Pointer20
                                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                              !Found
      
                                              If LocalValidateAccountNumber()
                                                  Cycle
                                              End !If LocalValidateAccountNumber()
      
                                              ! Inserting (DBH 24/01/2007) # 8678 - Has the job been paid/invoiced, and therefore can it be despatched?
                                              Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                              jobe:RefNumber = job:Ref_number
                                              If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                                  !Found
                                              Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                                  !Error
                                              End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      
                                              Access:WEBJOB.ClearKey(wob:RefNumberKey)
                                              wob:RefNumber = job:Ref_Number
                                              If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                                  !Found
                                              Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                                  !Error
                                              End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
                                              If CanJobBeDespatched() = 0
                                                  Cycle
                                              End ! If CanJobBeDespatched() = 0
                                              ! End (DBH 24/01/2007) #8678
      
      
                                              Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                                              mulj:JobNumber = job:Ref_Number
                                              If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                                  !Found
                                                  Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                                                    '<13,10>'&|
                                                    '<13,10>This job is part of a multiple despatch batch that has not been despatched.','ServiceBase 3g',|
                                                                 'mstop.jpg','/OK') 
                                                      Of 1 ! OK Button
                                                  End ! Case Missive
                                                  Cycle
                                              Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                                  !Error
                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                              End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
      
      
                                              !Only validate accessories when the job does not have an exchange unit attached - 3692 (DBH: 11-12-2003)
                                              If def:Force_Accessory_Check = 'YES' And job:Exchange_Unit_Number = ''
                                                  If LocalValidateAccessories()
                                                      Cycle
                                                  End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
                                              End !If def:Force_Accessory_Check = 'YES'
      
                                              If def:ValidateDesp = 'YES'
                                                  If LocalValidateIMEI()
                                                      Cycle
                                                  End !If LocalValidateIMEI()
                                              End !If def:ValidateDesp = 'YES'
      
                                              If tmp:FirstRecord = 1
                                                  If Access:MULDESP.PrimeRecord() = Level:Benign
                                                      muld:AccountNumber  = Account_Number2_Temp
                                                      muld:Courier        = Courier_temp
                                                      tmp:BatchNumber = muld:RecordNumber
                                                      tmp:FirstRecord = 0
                                                      tmp:BatchCreated = 1
                                                      muld:HeadAccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
                                                      If Access:MULDESP.TryInsert() = Level:Benign
                                                          !Insert Successful
                                                      Else !If Access:MULDESP.TryInsert() = Level:Benign
                                                          !Insert Failed
                                                      End !If Access:MULDESP.TryInsert() = Level:Benign
                                                  End !If Access:MULDESP.PrimeRecord() = Level:Benign
                                              End !If tmp:FirstRecord = 1
      
                                              If Access:MULDESPJ.PrimeRecord() = Level:Benign
                                                  mulj:RefNumber     = tmp:BatchNumber
                                                  mulj:JobNumber     = job:Ref_Number
                                                  Case job:Despatch_Type
                                                      Of 'JOB'
                                                          mulj:IMEINumber    = job:ESN
                                                          mulj:MSN           = job:MSN
                                                      Of 'EXC'
                                                          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                                          xch:Ref_Number  = job:Exchange_Unit_Number
                                                          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                              !Found
                                                              mulj:IMEINumber = xch:ESN
                                                              mulj:MSN        = xch:MSN
                                                          Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                              !Error
                                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                      Of 'LOA'
                                                          Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                                          loa:Ref_Number  = job:Loan_Unit_Number
                                                          If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                              !Found
                                                              mulj:IMEINumber = loa:ESN
                                                              mulj:MSN        = loa:MSN
                                                          Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                              !Error
                                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                  End !job:Despatch_Type
                                                  mulj:AccountNumber = Account_Number2_Temp
                                                  mulj:Courier       = Courier_Temp
      
                                                  Access:COURIER.Clearkey(cou:Courier_Key)
                                                  cou:Courier = courier_Temp
                                                  If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                                                      !Found
      
                                                  Else! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                                                      !Error
                                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  End! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                                                  
      
                                                  If tmp:PrintDespatchNote
                                                      glo:Select1 = job:Ref_Number
                                                      If cou:CustomerCollection = 1
                                                          glo:Select2 = cou:NoOfDespatchNotes
                                                      Else!If cou:CustomerCollection = 1
                                                          glo:Select2 = 1
                                                      End!If cou:CustomerCollection = 1
                                                      Despatch_Note
                                                      glo:Select1 = ''
                                                  End !If tmp:PrintDespatchNote
                                                  
                                                  If Access:MULDESPJ.TryInsert() = Level:Benign
                                                      !Insert Successful
                                                  Else !If Access:MULDESPJ.TryInsert() = Level:Benign
                                                      !Insert Failed
                                                  End !If Access:MULDESPJ.TryInsert() = Level:Benign
                                              End !If Access:MULDESPJ.PrimeRecord() = Level:Benign
      
                                          Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      End !Loop x# = 1 To Records(glo:Q_JobNumber)
                                      If tmp:BatchCreated = 1
                                          !Count how many jobs there are on the batch.
                                          !I could just add one to the record, but I can see
                                          !that being too inaccurate. So I'll physically count
                                          !all the records in the batch instead.
      
                                          CountBatch# = 0
                                          Save_mulj_ID = Access:MULDESPJ.SaveFile()
                                          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                                          mulj:RefNumber = tmp:BatchNumber
                                          Set(mulj:JobNumberKey,mulj:JobNumberKey)
                                          Loop
                                              If Access:MULDESPJ.NEXT()
                                                 Break
                                              End !If
                                              If mulj:RefNumber <> tmp:BatchNumber      |
                                                  Then Break.  ! End If
                                              CountBatch# += 1
                                          End !Loop
                                          Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
                                          Access:MULDESP.ClearKey(muld:RecordNumberKey)
                                          muld:RecordNumber = tmp:BatchNumber
                                          If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                                              !Found
                                              muld:BatchTotal = CountBatch#
                                              !Allocate a Batch Number
                                              !Count between 1 to 1000, (that should be enough)
                                              !and if I can't find a Batch with that batch number, then
                                              !assign that batch number to this batch.
                                              BatchNumber# = 0
                                              Loop BatchNumber# = 1 To 1000
                                                  Access:MULDESP_ALIAS.ClearKey(muld_ali:BatchNumberKey)
                                                  muld_ali:BatchNumber = BatchNumber#
                                                  If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                                                      !Found
                                                  Else!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                                                      !Error
                                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                                      muld:BatchNumber    = BatchNumber#
                                                      Break
                                                  End!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                                              End !BatchNumber# = 1 To 1000
      
                                              Access:MULDESP.Update()
                                          Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                                          Case Missive('A multiple despatch batch has been created for account number ' & Clip(Account_Number2_Temp) & ' and courier ' & Clip(Courier_Temp) & '.','ServiceBase 3g',|
                                                         'midea.jpg','/OK') 
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                      End !If tmp:BatchCreated = 1
                                  End !If cou:Courier_Type = 'TOTE'
                              End !If NormalDespatchCourier(Courier_Temp)
                          Of 1 ! No Button
                      End ! Case Missive
                  End !If Recods(glo:Q_JobNumber)
              End !If Courier_Temp <> '' And Account_Number2_Temp <> ''
          End !If Select_Courier_Temp = 'IND' And Select_Trade_Account_Temp = 'IND'
      
          MultipleBatchDespatch
      end!if x" = f
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multiple_Despatch2, Accepted)
    OF ?Individual_Despatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Individual_Despatch, Accepted)
      check_access('INDIVIDUAL DESPATCH',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      else!if x" = false
          If Records(glo:Queue20) = 0
              Case Missive('You must tag at least one job.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Else ! If Records(glo:Queue20) = 0
              Case Missive('You are about to despatch all the tagged jobs, and assign each one an INDIVIDUAL consignment number.'&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes') 
                  Of 2 ! Yes Button
      
                      Thiswindow.reset
                      Set(DEFAULTS)
                      Access:DEFAULTS.Next()
                      error# = 0
                      Loop x# = 1 To Records(glo:Queue20)
                          Get(glo:Queue20,x#)
                          Error# = 0
                          access:jobs.clearkey(job:ref_number_key)
                          job:ref_number  = glo:Pointer20
                          If access:jobs.fetch(job:ref_number_key) = Level:Benign
                              If JobInUse(job:Ref_Number,1)
                                  Cycle
                              End !If JobInUse(job:Ref_Number,1)
      
                              ! Inserting (DBH 24/01/2007) # 8678 - Has the job been paid/invoiced, and therefore can it be despatched?
                              Access:JOBSE.ClearKey(jobe:RefNumberKey)
                              jobe:RefNumber = job:Ref_Number
                              If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                  !Found
                              Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                  !Error
                              End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      
                              Access:WEBJOB.ClearKey(wob:RefNumberKey)
                              wob:RefNumber = job:Ref_Number
                              If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                  !Found
                              Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                  !Error
                              End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
                              If CanJobBeDespatched() = 0
                                  Cycle
                              End ! If CanJobBeDespatched() = 0
                              ! End (DBH 24/01/2007) #8678
      
                              !Has the job got a loan attached?
                              If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1 And job:Despatch_type = 'JOB'
                                  IF (jobe:WebJob <> 1)  ! #11817 Allow to despatch a unit back to the RRC if loan attached. (Bryan: 11/05/2011)
                                      If job:Loan_Unit_Number <> 0
                                          Case Missive('Cannot despatch!'&|
                                            '<13,10>'&|
                                            '<13,10>The loan unit attached to job ' & Clip(job:Ref_Number) & ' has not been returned.','ServiceBase 3g',|
                                                         'mstop.jpg','/OK') 
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      End !If job:Loan_Unit_Number <> 0
                                  END ! IF (jobe:WebJob <> 1)
                              End !If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
                              Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                              mulj:JobNumber = job:Ref_Number
                              If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                  !Found
                                  Case Missive('Cannot despatch!'&|
                                    '<13,10>'&|
                                    '<13,10>Job number ' & Clip(job:Ref_Number) & ' is part of a multiple batch that has not been despatched.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK') 
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Cycle
      
                              Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                              End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
      
                              If LocalValidateAccountNumber()
                                  Cycle
                              End !If LocalValidateAccountNumber()
      
                              access:courier.clearkey(cou:courier_key)
                              cou:courier = job:current_courier
                              if access:courier.fetch(cou:courier_key)
                                  Case Missive('Cannot despatch!'&|
                                    '<13,10>'&|
                                    '<13,10>There is no courier attached to job number ' & Clip(job:Ref_Number) & '.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK') 
                                      Of 1 ! OK Button
                                  End ! Case Missive
                                  Cycle
                              Else!if access:courier.fetch(cou:courier_key)
                                  If NormalDespatchCourier(job:Current_Courier)
                                      Case Missive('Job number ' & Clip(job:Ref_Number) & ' has been assigned to a "' & Clip(cou:Courier_Type) & '" courier.'&|
                                        '<13,10>'&|
                                        '<13,10>This should be despatched from "Despatch Procedures".','ServiceBase 3g',|
                                                     'mstop.jpg','/OK') 
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                      Cycle
                                  Else !If NormalDespatchCourier(job:Current_Courier)
                                      If cou:Courier_Type = 'SDS'
                                          Case Missive('You can only despatch SDS jobs using Multiple Despatch.','ServiceBase 3g',|
                                                         'mstop.jpg','/OK') 
                                              Of 1 ! OK Button
                                          End ! Case Missive
                                          Cycle
                                      End !If cou:Courier_Type = 'SDS'
                                  End !If NormalDespatchCourier(job:Current_Courier)
                              End!if access:courier.fetch(cou:courier_key)
      
                              !Only validate accessories when job does not have exchange unit.
                              !This should take care of failed assessment jobs, where there is no need to revalidate the accessories - 3692 (DBH: 11-12-2003)
                              If def:Force_Accessory_Check = 'YES' and job:Exchange_Unit_Number = ''
                                  If LocalValidateAccessories()
                                      Access:JOBS.Update()
                                      Cycle
                                  End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
                              End !If def:Force_Accessory_Check = 'YES'
      
                              If def:ValidateDesp = 'YES'
                                  If LocalValidateIMEI()
                                      Cycle
                                  End !If LocalValidateIMEI()
                              End !If def:ValidateDesp = 'YES'
      
                              If access:desbatch.primerecord() = Level:Benign
                                  If access:desbatch.tryinsert()
                                      access:desbatch.cancelautoinc()
                                  Else!If access:desbatch.tryinsert()
                                      DespatchSingle()
                                  End!If access:desbatch.tryinsert()
                              End!If access:desbatch.primerecord() = Level:Benign
                          End!If access:jobs.clearkey(job:ref_number_key) = Level:Benign
                      End!Loop x# = 1 To Records(glo:q_jobnumber)
                      Post(event:accepted,?Dasuntagall)
      
                  Of 1 ! No Button
              End ! Case Missive
          End ! If Records(glo:Queue20) = 0
      
      end!if x" = false
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Individual_Despatch, Accepted)
    OF ?Validate_Esn
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Esn, Accepted)
      check_access('VALIDATE ESN',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      else!if x" = false
          thiswindow.reset
          Case job:despatch_type
              Of 'JOB' Orof 'JHL'
                  Validate_Esn(job:esn,esn",validated")
                  If esn" <> ''
      
                      If validated" = False
                          Case Missive('You are attempting to despatch a unit that differs from the I.M.E.I. number issued by ServiceBase.'&|
                            '<13,10>Please inform you System Supervisor.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
      
                          IF (AddToAudit(JOB:REF_NUMBER,'JOB','I.M.E.I. NUMBER MISMATCH - JOB','THE I.M.E.I. AT POINT OF DESPATCH DIFFERED FROM THE REQUIRED SERIAL NUMBER. ' & |
                                                  '<13,10>REQUIRED NUMBER: ' & CLIP(JOB:ESN) & '<13,10>ACTUAL NUMBER: ' & CLIP(ESN")))
                          END ! IF
      
                      Else!If validated" = False
                          type# = 1 !Job
                      !Accessory Check
                      pass# = 1
                      exchange_required# = 0
                      loan_required# = 0
                      refurb# = 0
                      error# = 0
      
                      Case type#
                          Of 1
                              access:trantype.clearkey(trt:transit_type_key)
                              trt:transit_type = job:transit_type
                              if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                  If trt:exchange_unit = 'YES' or job:exchange_unit_number <> ''
                                      exchange_required# = 1
                                  End!If trt:exchange_unit = 'YES'
                                  If exchange_required# = 1 or loan_required# = 1
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = job:account_number
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          access:tradeacc.clearkey(tra:account_number_key)
                                          tra:account_number = sub:main_account_number
                                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                              If tra:refurbcharge = 'YES'
                                                  refurb# = 1
                                              End!If tra:refurbcharge = 'YES'
                                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
                                  End!If exchange_required# = 1 or loan_required# = 1
                              end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                          Of 2
                              exchange_required# = 1
                          Of 3
                              loan_required# = 1
                      End!If type# = 1
      
                      If refurb# = 1
                          BEEP(BEEP:SystemQuestion)  ;  YIELD()
                          CASE MESSAGE('This unit is authorised for Refubishment.'&|
                                  '||Is the Refurbishment complete?', |
                                  'ServiceBase 2000', ICON:Question, |
                                   BUTTON:Yes+BUTTON:No, BUTTON:No, 0)
                          Of BUTTON:Yes
                          Of BUTTON:No
                              pass# = 0
                              job:qa_rejected      = 'YES'
                              job:date_qa_rejected = Today()
                              job:time_qa_rejected = Clock()
                              If preliminary# = 1
                                  GetStatus(625,1,'JOB') !Electronic QA Rejected
      
                              Else
                                  GetStatus(615,1,'JOB') !Manual QA Rejected
      
                              End
                              access:jobs.update()
                              reason" = ''
                              notes" = ''
                              Qa_Failure_Reason(Reason",notes")                                          !Get the failure reason and write
      
                              IF (AddToAudit(job:ref_number,'JOB','QA REJECTION: ' & CLip(Reason"),'REFURBISHMENT NOT COMPLETE' & '<13,10>' & clip(notes")))
                              END ! IF
      
      
                              !Added by Paul - 03/09/2009 - log no 10785
                              If clip(notes") <> '' then
                                Do AddEngNotes
                              End !If clip(notes") <> '' then
                              !End Addition
      
                              beep(beep:systemasterisk)
                              glo:notes_global    = Clip(Reason")
                              If def:qa_failed_label = 'YES'                                      !Print the failure report/label
                                  glo:select1    = job:ref_number
                                  QA_Failed_Label
                                 glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  glo:select1 = job:ref_number
                                  QA_Failed
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
      
                          End !CASE
                      End!If tra:refurbcharge = 'YES'
                      If loan_required# = 1 And pass# = 1
                          found# = 0
                          save_lac_id = access:loanacc.savefile()
                          access:loanacc.clearkey(lac:ref_number_key)
                          lac:ref_number = job:loan_unit_number
                          set(lac:ref_number_key,lac:ref_number_key)
                          loop
                              if access:loanacc.next()
                                 break
                              end !if
                              if lac:ref_number <> job:loan_unit_number      |
                                  then break.  ! end if
                              found# = 1
                              Break
                          end !loop
                          access:loanacc.restorefile(save_lac_id)
                          If found# = 1
                              glo:select1 = job:ref_number
                              glo:select2 = ''
                              glo:select12 = job:model_number
                              Validate_Job_Accessories                                                    !Bring up the list of all accessories
                              error# = 0
                              save_lac_id = access:loanacc.savefile()
                              access:loanacc.clearkey(lac:ref_number_key)
                              lac:ref_number = job:loan_unit_number
                              set(lac:ref_number_key,lac:ref_number_key)
                              loop
                                  if access:loanacc.next()
                                     break
                                  end !if
                                  if lac:ref_number <> job:loan_unit_number      |
                                      then break.  ! end if
                                  Sort(glo:queue,glo:pointer)
                                  glo:pointer    = lac:accessory
                                  Get(glo:queue,glo:pointer)
                                  If Error()
                                      error# = 1
                                      Break
                                  End!If Error()
                              end !loop
                              access:loanacc.restorefile(save_lac_id)
      
                          End!If found# = 1
                      End!If loan_required# = 1 And pass# = 1
      
                      If exchange_required# = 0 and pass# = 1 and loan_required# = 0
                          found# = 0
                          save_jac_id = access:jobacc.savefile()                                          !Check to see if there are any
                          clear(jac:record, -1)                                                           !accessories attached to this job.
                          jac:ref_number = job:ref_number
                          set(jac:ref_number_key,jac:ref_number_key)
                          loop
                              next(jobacc)
                              if errorcode()                |
                                 or jac:ref_number <> job:Ref_number      |
                                 then break.  ! end if
      
                              found# = 1
                              Break
                          end !loop
                          access:jobacc.restorefile(save_jac_id)
                          If found# = 1                                                                   !Some accessories have been found.
                              glo:select1 = job:ref_number
                              glo:select2 = ''
                              glo:select12 = job:model_number
                              Validate_Job_Accessories                                                    !Bring up the list of all accessories
                              error# = 0                                                                  !for this model
                              setcursor(cursor:wait)
                              save_jac_id = access:jobacc.savefile()
                              access:jobacc.clearkey(jac:ref_number_key)
                              jac:ref_number = job:ref_number
                              set(jac:ref_number_key,jac:ref_number_key)
                              loop                                                                        !Loop through the accessories attached
                                  if access:jobacc.next()                                                 !to the job.
                                     break
                                  end !if
                                  if jac:ref_number <> job:ref_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
      
                                  Sort(glo:queue,glo:pointer)                                       !See if the accessories can be found in
                                  glo:pointer  = jac:accessory                                         !the tagged entries.
                                  Get(glo:queue,glo:pointer)
                                  If Error()
                                      error# = 1                                                          !If not, then it's a Missing Accessory
                                      Break
                                  End!If Error()
                              end !loop
                              access:jobacc.restorefile(save_jac_id)
                              setcursor()
                          End!If found# = 1
                      End!If exchange_required# = 0 and pass# = 1                                                                                                !Missing Accessory Error
                      If error# = 1
                          pass# = 0
                          If def:qa_required = 'YES'                                              !If QA after completion
                              BEEP(BEEP:SystemHand)  ;  YIELD()
                              CASE MESSAGE('This unit has a missing accessory.'&|
                                      '||It will now FAIL QA.', |
                                      'ServiceBase 2000', ICON:Hand, |
                                       BUTTON:OK, BUTTON:OK, 0)
                              Of BUTTON:OK
                              End !CASE
      
      
                              status_audit# = 1
      
                              Case type#
                                  Of 1!Job
                                      If preliminary# = 1
                                          GetStatus(625,1,'JOB') !Electronic QA Rejected
                                      Else!If preliminary# = 1
                                          GetStatus(615,1,'JOB') !Manual QA Rejected
                                      End!If preliminary# = 1
                                      job:despatch_Type = 'JHL'
                                  Of 2!Exchange
                                      If preliminary# = 1
                                          GetStatus(625,1,'EXC') !Electronic QA Rejected
                                      Else!If preliminary# = 1
                                          GetStatus(615,1,'EXC') !Manual QA Rejected
                                      End!If preliminary# = 1
      
                                      job:despatch_Type = 'EHL'
                                  Of 3!Loan
                                      If preliminary# = 1
                                          GetStatus(625,1,'LOA') !Electronic QA Rejected
                                      Else!If preliminary# = 1
                                          GetStatus(615,1,'LOA') !Manual QA Rejected
                                      End!If preliminary# = 1
      
                                      job:despatch_Type = 'LHL'
                              End!Case type#
      
                              access:jobs.update()
      
                              Case type#
                                  Of 1
                                      type" = 'JOB'
                                  Of 2
                                      type" = 'EXC'
                                  Of 3
                                      type" = 'LOA'
                              End!Case type#
      
                              IF (AddToAudit(job:ref_number,type",'MISSING ACCESSORY','UNIT HELD FROM DESPATCH'))
                              END ! IF
      
                          End!If def:qa_required = 'YES'
      
                          If def:qa_before_complete = 'YES'
                              BEEP(BEEP:SystemHand)  ;  YIELD()
                              CASE MESSAGE('This unit has a missing accessory.'&|
                                      '||It will now FAIL QA.', |
                                      'ServiceBase 2000', ICON:Hand, |
                                       BUTTON:OK, BUTTON:OK, 0)
                              Of BUTTON:OK
                              End !CASE
      
      
                              status_audit# = 1
      
                              Case type#
                                  Of 1!Job
                                      If preliminary# = 1
                                          GetStatus(625,1,'JOB') !Electronic QA Rejected
                                      Else!If preliminary# = 1
                                          GetStatus(615,1,'JOB') !Manual QA Rejected
                                      End!If preliminary# = 1
      
                                      job:despatch_Type = 'JHL'
                                  Of 2!Exchange
                                      If preliminary# = 1
                                          GetStatus(625,1,'EXC') !Electronic QA Rejected
                                      Else!If preliminary# = 1
                                          GetStatus(615,1,'EXC') !Manual QA Rejected
                                      End!If preliminary# = 1
                                      job:despatch_Type = 'EHL'
                                  Of 3!Loan
                                      If preliminary# = 1
                                          GetStatus(625,1,'LOA') !Electronic QA Rejected
                                      Else!If preliminary# = 1
                                          GetStatus(615,1,'LOA') !Manual QA Rejected
                                      End!If preliminary# = 1
                                      job:despatch_Type = 'LHL'
                              End!Case type#
                              access:jobs.update()
      
                              reason" = ''
                              notes" = ''
                              QA_failure_reason(reason",notes")
                                  locAuditNotes         = 'ACCESSORIES BOOKED IN:-'                   !booked out into the Audit Trail
                                  Case type#
                                      Of 3
                                          save_lac_id = access:loanacc.savefile()
                                          access:loanacc.clearkey(lac:ref_number_key)
                                          lac:ref_number = job:loan_unit_number
                                          set(lac:ref_number_key,lac:ref_number_key)
                                          loop
                                              if access:loanacc.next()
                                                 break
                                              end !if
                                              if lac:ref_number <> job:loan_unit_number      |
                                                  then break.  ! end if
                                              locAuditNotes = CLip(locAuditNotes) & '<13,10>' & Clip(lac:accessory)
                                          end !loop
                                      Else
                                          save_jac_id = access:jobacc.savefile()
                                          access:jobacc.clearkey(jac:ref_number_key)
                                          jac:ref_number = glo:select1
                                          set(jac:ref_number_key,jac:ref_number_key)
                                          loop
                                              if access:jobacc.next()
                                                 break
                                              end !if
                                              if jac:ref_number <> glo:select1      |
                                                  then break.  ! end if
                                              locAuditNotes = CLip(locAuditNotes) & '<13,10>' & Clip(jac:accessory)
                                          end !loop
                                          access:jobacc.restorefile(save_jac_id)
                                  End!Case type#
      
                                  !Added by Paul - 03/09/2009 - log no 10785
                                  If clip(notes") <> '' then
                                    Do AddEngNotes
                                  End !If clip(notes") <> '' then
                                  !End Addition
      
                                  locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                  Loop x# = 1 To Records(glo:queue)
                                      Get(glo:queue,x#)
                                      locAuditNotes = Clip(locAuditNotes) & '<13,10>' & CLip(glo:pointer) & '<13,10>' & clip(notes")
                                  End!Loop x# = 1 To Records(glo:queue)
                                  glo:notes_global    = Clip(locAuditNotes)
      
                                  Case type#
                                      Of 1
                                          type" = 'JOB'
                                      Of 2
                                          type" = 'EXC'
                                      Of 3
                                          type" = 'LOA'
                                  End!Case type#
      
                                  IF (AddToAudit(job:ref_number,type",'QA REJECTION: ' & Clip(reason"),locAuditNotes))
                                  END ! IF
      
                              beep(beep:systemasterisk)
                              If def:qa_failed_label = 'YES'                                      !Print the failure report/label
                                  Case type#
                                      Of 1!Job
                                          glo:select1    = job:ref_number
                                          QA_Failed_Label
                                      Of 2!Exchange
                                          glo:select1    = job:exchange_unit_number
                                          QA_Failed_Label_Exchange
                                      Of 3!Loan
                                          glo:select1    = job:loan_unit_number
                                          QA_Failed_Label_Loan
                                  End!Case type#
                                  glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  Case type#
                                      Of 1!Job
                                          glo:select1 = job:ref_number
                                          QA_Failed
                                      Of 2!Exchange
                                          glo:select1 = job:exchange_unit_number
                                          QA_Failed_Exchange
                                      Of 3!Loan
                                          glo:select1 = job:loan_unit_number
                                          QA_Failed_Loan
                                  End!Case type#
      
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
      
                          End!If def:qa_before_completed = 'YES'
      
                      Else!If error# = 1
                          mismatch# = 0
                          Case type#
                              Of 3
                                  Loop x# = 1 To Records(glo:queue)                                    !Check to see if more accessories were
                                      Get(glo:queue,x#)
                                      access:loanacc.clearkey(lac:ref_number_key)
                                      lac:ref_number = job:loan_unit_number
                                      lac:accessory  = glo:pointer
                                      if access:loanacc.tryfetch(lac:ref_number_key)
                                          mismatch# = 1
                                          Break
                                      End!if access:jobacc.tryfetch(jac:ref_number_key)
                                  End!Loop x# = 1 To Records(glo:queue)
      
                              Else
                                  Loop x# = 1 To Records(glo:queue)                                    !Check to see if more accessories were
                                      Get(glo:queue,x#)                                                !tagged than were on the job
                                      access:jobacc.clearkey(jac:ref_number_key)
                                      jac:ref_number = job:ref_number
                                      jac:accessory  = glo:pointer
                                      if access:jobacc.tryfetch(jac:ref_number_key)
                                          mismatch# = 1
                                          Break
                                      End!if access:jobacc.tryfetch(jac:ref_number_key)
                                  End!Loop x# = 1 To Records(glo:queue)
      
                          End!Case type#
                          If mismatch# = 1                                                        !If yes fail the QA
                              pass# = 0
                              BEEP(BEEP:SystemHand)  ;  YIELD()
                              CASE MESSAGE('There is a MISMATCH between the selected Accessories '&|
                                      'and the Accessories booked in with the job.'&|
                                      '||This job will now FAIL QA.'&|
                                      '||(The Failure Document/Label will now be printed)', |
                                      'ServiceBase 2000', ICON:Hand, |
                                       BUTTON:OK, BUTTON:OK, 0)
                              Of BUTTON:OK
                              End !CASE
      
                              job:qa_rejected      = 'YES'
                              job:date_qa_rejected = Today()
                              job:time_qa_rejected = Clock()
                              If preliminary# = 1
                                  status_audit# = 1
                                  Case type#
                                      Of 1 !Job
                                          GetStatus(625,1,'JOB') !Electronic QA Rejected
                                      Of 2 !Exchange
                                          GetStatus(625,1,'EXC') !Electronic QA Rejected
                                      Of 3 !Loan
                                          GetStatus(625,1,'LOA') !Electronic QA Rejected
                                  End!Case type#
                              Else
                                  status_number# = 615 !Manual QA Rejected
                                  status_audit# = 1
                                  Case type#
                                      Of 1 !Job
                                          GetStatus(615,1,'JOB') !Electronic QA Rejected
                                      Of 2 !Exchange
                                          GetStatus(615,1,'EXC') !Electronic QA Rejected
                                      Of 3 !Loan
                                          GetStatus(615,1,'LOA') !Electronic QA Rejected
                                  End!Case type#
                              End
                              access:jobs.update()
                              reason" = ''
                              notes" = ''
                              Qa_Failure_Reason(Reason",notes")                                          !Get the failure reason and write
      
                                  locAuditNotes         = 'ACCESSORIES BOOKED IN:-'
                                  Case type#
                                      Of 3
                                          save_lac_id = access:loanacc.savefile()
                                          access:loanacc.clearkey(lac:ref_number_key)
                                          lac:ref_number = job:loan_unit_number
                                          set(lac:ref_number_key,lac:ref_number_key)
                                          loop
                                              if access:loanacc.next()
                                                 break
                                              end !if
                                              if lac:ref_number <> job:loan_unit_number      |
                                                  then break.  ! end if
                                              locAuditNotes = CLip(locAuditNotes) & '<13,10>' & Clip(lac:accessory)
                                          end !loop
      
                                      Else
                                          save_jac_id = access:jobacc.savefile()
                                          access:jobacc.clearkey(jac:ref_number_key)
                                          jac:ref_number = glo:select1
                                          set(jac:ref_number_key,jac:ref_number_key)
                                          loop
                                              if access:jobacc.next()
                                                 break
                                              end !if
                                              if jac:ref_number <> glo:select1      |
                                                  then break.  ! end if
                                              locAuditNotes = CLip(locAuditNotes) & '<13,10>' & Clip(jac:accessory)
                                          end !loop
                                          access:jobacc.restorefile(save_jac_id)
      
                                  End!Case type#
      
                                  !Added by Paul - 03/09/2009 - log no 10785
                                  If clip(notes") <> '' then
                                    Do AddEngNotes
                                  End !If clip(notes") <> '' then
                                  !End Addition
      
                                  locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                  Loop x# = 1 To Records(glo:queue)
                                      Get(glo:queue,x#)
                                      locAuditNotes = Clip(locAuditNotes) & '<13,10>' & CLip(glo:pointer) & '<13,10>' & clip(notes")
                                  End!Loop x# = 1 To Records(glo:queue)
                                  glo:notes_global    = Clip(locAuditNotes)
      
                                 Case type#
                                      Of 1
                                          type" = 'JOB'
                                      Of 2
                                          type" = 'EXC'
                                      Of 3
                                          type" = 'LOA'
                                  End!Case type#
      
                                  IF (AddToAudit(job:ref_number,type",'QA REJECTION: ' & CLip(reason"),locAuditNotes))
                                  END ! IF
      
                              beep(beep:systemasterisk)
      
                              If def:qa_failed_label = 'YES'                                      !Print the failure report/label
                                  Case type#
                                      Of 1!Job
                                          glo:select1    = job:ref_number
                                          QA_Failed_Label
                                      Of 2!Exchange
                                          glo:select1    = job:exchange_unit_number
                                          QA_Failed_Label_Exchange
                                      Of 3!Loan
                                          glo:select1    = job:loan_unit_number
                                          QA_Failed_Label_Loan
                                  End!Case type#
                                  glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  Case type#
                                      Of 1!Job
                                          glo:select1 = job:ref_number
                                          QA_Failed
                                      Of 2!Exchange
                                          glo:select1 = job:exchange_unit_number
                                          QA_Failed_Exchange
                                      Of 3!Loan
                                          glo:select1 = job:loan_unit_number
                                          QA_Failed_Loan
                                  End!Case type#
      
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
      
                          Else!If mismatch                                                        !If everything ok allow the job to
                              job:despatch_type = 'JOB'                                           !be despatched although if it is a
                              access:jobs.update()                                                !QA before completion this bit
                          End!If mismatch# = 1                                                    !shouldn't matter
      
                      End!If error# = 1
                      If pass# = 1
                              locAuditNotes         = 'ACCESSORIES VALIDATED:-'                   !booked out into the Audit Trail
                              Case type#
                                  Of 3
                                      save_lac_id = access:loanacc.savefile()
                                      access:loanacc.clearkey(lac:ref_number_key)
                                      lac:ref_number = job:loan_unit_number
                                      set(lac:ref_number_key,lac:ref_number_key)
                                      loop
                                          if access:loanacc.next()
                                             break
                                          end !if
                                          if lac:ref_number <> job:loan_unit_number      |
                                              then break.  ! end if
                                          locAuditNotes = CLip(locAuditNotes) & '<13,10>' & Clip(lac:accessory)
                                      end !loop
                                      access:loanacc.restorefile(save_lac_id)
                                  Else!Case type#
                                      save_jac_id = access:jobacc.savefile()
                                      access:jobacc.clearkey(jac:ref_number_key)
                                      jac:ref_number = glo:select1
                                      set(jac:ref_number_key,jac:ref_number_key)
                                      loop
                                          if access:jobacc.next()
                                             break
                                          end !if
                                          if jac:ref_number <> glo:select1      |
                                              then break.  ! end if
                                          locAuditNotes = CLip(locAuditNotes) & '<13,10>' & Clip(jac:accessory)
                                      end !loop
                                      access:jobacc.restorefile(save_jac_id)
      
                              End!Case type#
      
                              Case type#
                                  Of 1
                                      type" = 'JOB'
                                  Of 2
                                      type" = 'EXC'
                                  Of 3
                                      type" = 'LOA'
                              End!Case type#
      
                              IF (AddToAudit(job:ref_number,type",'QA PASSED - ACCESSORIES VALIDATED',locAuditNotes))
                              END ! IF
      
                      End!If pass# = 1
      
                      glo:select1 = ''
                      glo:select2 = ''
                      glo:select12 = ''
      
      
                      End!If validated" = True
                  End!If esn" <> ''
              Of 'EXC' Orof 'EHL'
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = job:exchange_unit_number
                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                      Validate_Esn(xch:esn,esn",validated")
                      If esn" <> ''
                          If validated" = False
                              Case Missive('You are attempting to despatch a unit that differs from the I.M.E.I. number issued by ServiceBase.'&|
                                '<13,10>Please inform you System Supervisor.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
      
                              IF (AddToAudit(JOB:REF_NUMBER,'EXC','I.M.E.I. NUMBER MISMATCH - EXCHANGE','THE I.M.E.I. AT POINT OF DESPATCH DIFFERED FROM THE REQUIRED SERIAL NUMBER. ' & |
                                                      '<13,10>REQUIRED NUMBER: ' & CLIP(XCH:ESN) & '<13,10>ACTUAL NUMBER: ' & CLIP(ESN")))
                              END ! IF
      
                          Else!If validated" = False
                              found# = 0
                              save_xca_id = access:exchacc.savefile()
                              access:exchacc.clearkey(xca:ref_number_key)
                              xca:ref_number = job:exchange_unit_number
                              set(xca:ref_number_key,xca:ref_number_key)
                              loop
                                  if access:exchacc.next()
                                     break
                                  end !if
                                  if xca:ref_number <> job:exchange_unit_number      |
                                      then break.  ! end if
                                  found# = 1
                                  Break
                              end !loop
                              access:exchacc.restorefile(save_xca_id)
                              If found# = 1
                                  glo:select1 = job:exchange_unit_number
                                  glo:select2 = ''
                                  Validate_Exchange_Accessories
                                  If glo:select2 = 'FAIL'
                                      beep(beep:systemhand)  ;  yield()
                                      message('You are attempting to despatch a unit with a missing accessory.'&|
                                              '||The following will now be actioned:'&|
                                              '||1) The status of the job will be changed to: '&|
                                              '|    804 HELD - MISSING ACCESSORY.'&|
                                              '||2) An entry will be made in the Audit Trail.'&|
                                              '||You will NOT be able to despatch this unit all the accessories '&|
                                              'are present.', |
                                              'ServiceBase 2000', icon:hand)
                                      Status_Routine(804,job:current_status,end_date",end_time",a")
                                      job:status_end_date = end_date"
                                      job:status_end_time = end_time"
                                      job:despatch_Type = 'EHL'
                                      access:jobs.update()
      
                                      IF (AddToAudit(job:ref_number,'JOB','MISSING ACCESSORY ON EXCHANGE UNIT','UNIT HELD FROM DESPATCH'))
                                      END ! IF
      
                                      brw1.resetsort(1)
                                  Else!If glo:select1 = 'FAIL'
                                      job:despatch_type = 'EXC'
                                      access:jobs.update()
                                  End!If glo:select1 = 'FAIL'
                                  glo:select1 = ''
                                  glo:select2 = ''
                              End!If found# = 1
      
                          End!If validated" = True
                      End!If esn" <> ''
                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
              Of 'LOA' Orof 'LHL'
                  access:loan.clearkey(loa:ref_number_key)
                  loa:ref_number = job:loan_unit_number
                  if access:loan.fetch(loa:ref_number_key) = Level:Benign
                      Validate_Esn(loa:esn,esn",validated")
                      If esn" <> ''
                          If validated" = False
                              Case Missive('You are attempting to despatch a unit that differs from the I.M.E.I. number issued by ServiceBase.'&|
                                '<13,10>Please inform you System Supervisor.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
      
                              IF (AddToAudit(JOB:REF_NUMBER,'LOA','I.M.E.I. NUMBER MISMATCH - LOAN','THE I.M.E.I. AT POINT OF DESPATCH DIFFERED FROM THE REQUIRED SERIAL NUMBER. ' & |
                                                      '<13,10>REQUIRED NUMBER: ' & CLIP(loa:ESN) & '<13,10>ACTUAL NUMBER: ' & CLIP(ESN")))
                              END ! IF
      
                          Else!If validated" = False
                              found# = 0
                              save_lac_id = access:loanacc.savefile()
                              access:loanacc.clearkey(lac:ref_number_key)
                              lac:ref_number = job:loan_unit_number
                              set(lac:ref_number_key,lac:ref_number_key)
                              loop
                                  if access:loanacc.next()
                                     break
                                  end !if
                                  if lac:ref_number <> job:loan_unit_number      |
                                      then break.  ! end if
                                  found# = 1
                                  Break
                              end !loop
                              access:loanacc.restorefile(save_lac_id)
                              If found# = 1
                                  glo:select1 = job:loan_unit_number
                                  glo:select2 = ''
                                  Validate_Loan_Accessories
                                  If glo:select2 = 'FAIL'
                                      beep(beep:systemhand)  ;  yield()
                                      message('You are attempting to despatch a unit with a missing accessory.'&|
                                              '||The following will now be actioned:'&|
                                              '||1) The status of the job will be changed to: '&|
                                              '|    804 HELD - MISSING ACCESSORY.'&|
                                              '||2) An entry will be made in the Audit Trail.'&|
                                              '||You will NOT be able to despatch this unit all the accessories '&|
                                              'are present.', |
                                              'ServiceBase 2000', icon:hand)
                                      Status_Routine(804,job:current_status,end_date",end_time",a")
                                      job:status_end_date = end_date"
                                      job:status_end_time = end_time"
                                      job:despatch_Type = 'LHL'
                                      access:jobs.update()
      
                                      IF (AddToAudit(job:ref_number,'JOB','MISSING ACCESSORY ON EXCHANGE UNIT','UNIT HELD FROM DESPATCH'))
                                      END ! IF
      
                                      brw1.resetsort(1)
                                  Else!If glo:select1 = 'FAIL'
                                      job:despatch_type = 'LOA'
                                      access:jobs.update()
                                  End!If glo:select1 = 'FAIL'
                                  glo:select1 = ''
                                  glo:select2 = ''
                              End!If found# = 1
      
                          End!If validated" = True
                      End!If esn" <> ''
                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
          End!Case job:despatch_type
      end!if x" = false
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Esn, Accepted)
    OF ?Change_Courier
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change_Courier, Accepted)
      check_access('CHANGE COURIER',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      else!if x" = false
          If ~Records(glo:Queue20)
              Case Missive('You have not tagged any jobs.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Else !If ~Records(glo:Q_JobNumber)
              saverequest#      = globalrequest
              globalresponse    = requestcancelled
              globalrequest     = selectrecord
              pickcouriers
              if globalresponse = requestcompleted
                  Case Missive('This will change the courier of the tagged jobs to ' & Clip(cou:Courier) & '. '&|
                    '<13,10>'&|
                    '<13,10>Are you sure?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes') 
                      Of 2 ! Yes Button
                          Loop x# = 1 To Records(glo:Queue20)
                              Get(glo:Queue20,x#)
                              access:jobs.clearkey(job:ref_number_key)
                              job:ref_number  = glo:Pointer20
                              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                  Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                                  mulj:JobNumber = job:Ref_Number
                                  If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                      !Found
                                      Case Missive('Error! Job number ' & Clip(job:Ref_Number) & ' is part of a multiple batch.'&|
                                        '<13,10>You cannot change it''s courier until you remove it from it''s batch.','ServiceBase 3g',|
                                                     'mstop.jpg','/OK') 
                                          Of 1 ! OK Button
                                      End ! Case Missive
                                      Cycle
                                  Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                  Case job:despatch_type
                                      Of 'JOB'
                                          job:courier = cou:courier
                                          job:current_courier = job:courier
                                      Of 'EXC'
                                          job:exchange_courier = cou:courier
                                          job:current_courier = job:exchange_courier
                                      Of 'LOA'
                                          job:loan_courier = cou:courier
                                          job:current_courier = job:loan_courier
                                  End!Case job:despatch_type
                                  access:jobs.update()
                              End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:q_jobnumber)
                      Of 1 ! No Button
                  End ! Case Missive
      
              end
              globalrequest     = saverequest#
              Post(event:accepted,?dasuntagall)
          End !If ~Records(glo:Q_JobNumber)
      
      end!if x" = false
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change_Courier, Accepted)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Count_Jobs:14
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:14, Accepted)
      Case Missive('Counting jobs may take a very long time to complete.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              Case Select_Courier_Temp
                  Of 'ALL'
                      Case Select_Trade_Account_temp
                          Of 'ALL'
                              setcursor(cursor:wait)
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:ReadyToDespKey)
                              job:despatched = 'REA'
                              set(job:ReadyToDespKey,job:ReadyToDespKey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:despatched <> 'REA'      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  count# += 1
                              end !loop
                              access:jobs.restorefile(save_job_id)
                              setcursor()
                          Of 'IND'
                              setcursor(cursor:wait)
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:ReadyToTradeKey)
                              job:despatched     = 'REA'
                              job:account_number = account_number2_temp
                              set(job:ReadyToTradeKey,job:ReadyToTradeKey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:despatched     <> 'REA'      |
                                  or job:account_number <> account_number2_temp      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  count# += 1
                              end !loop
                              access:jobs.restorefile(save_job_id)
                              setcursor()
                      End!Case Select_Trade_Account_temp
                  Of 'IND'
                      Case Select_Trade_Account_temp
                          Of 'ALL'
                              setcursor(cursor:wait)
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:ReadyToCouKey)
                              job:despatched      = 'REA'
                              job:current_courier = courier_temp
                              set(job:ReadyToCouKey,job:ReadyToCouKey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:despatched      <> 'REA' |
                                  or job:current_courier <> courier_temp |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  count# += 1
                              end !loop
                              access:jobs.restorefile(save_job_id)
                              setcursor()
                          Of 'IND'
                              setcursor(cursor:wait)
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:ReadyToAllKey)
                              job:despatched      = 'REA'
                              job:account_number  = account_number2_temp
                              job:current_courier = courier_temp
                              set(job:ReadyToAllKey,job:ReadyToAllKey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:despatched      <> 'REA' |
                                  or job:account_number  <> account_number2_temp |
                                  or job:current_courier <> courier_temp      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
                                  count# += 1
                              end !loop
                              access:jobs.restorefile(save_job_id)
                              setcursor()
                      End!Case Select_Trade_Account_temp
              End!Case Select_Courier_Temp
      
              Case Missive('There are ' & Count# & ' jobs in this browse.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:14, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      Case KeyCode()
          Of MouseLeft2 Orof EnterKey
              Post(Event:Accepted,?RapidUpdateButton:2)
      End !Case KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  OF ?Job_Number_Tab
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Job_Number_Tab)
    BRW1.ResetQueue(1)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Job_Number_Tab)
  OF ?Ready_To_Despatch
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Ready_To_Despatch)
    Do show_hide
    Hide(?CreateNewJob)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Ready_To_Despatch)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 15
            DASBRW::26:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      !!50L(2)|FM*~Job No~@s20@#210#
      !
      !CASE CHOICE(?CurrentTab)
      !  OF 1
      !    If def:HideLocation
      !        
      !        if tmp:DisableLocation
      !            ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#116L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Location~@s30@#165#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !        else
      !            ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#116L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Status~@s30@#21#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !        end
      !    Else !If tmp:HideLocation
      !        ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#116L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Location~@s30@#165#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    End !If tmp:HideLocation
      !    
      !    ?Job_Number_Tab{PROP:TEXT} = 'By Job Number'
      !  OF 2
      !    ?Browse:1{PROP:FORMAT} ='68L(2)|FM*~IMEI No~@s16@#40#65L(2)|FM*~Job No~@s20@#210#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#96L(2)|FM*~Order Number~@s30@#50#80L(2)|FM*~Model Number / Unit Type~@s60@#85#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?Esn_tab{PROP:TEXT} = 'By I.M.E.I. No'
      !  OF 3
      !    ?Browse:1{PROP:FORMAT} ='68L(2)|FM*~MSN~@s15@#45#65L(2)|FM*~Job No~@s20@#210#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#96L(2)|FM*~Order Number~@s30@#50#80L(2)|FM*~Model Number / Unit Type~@s60@#85#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?msn_tab{PROP:TEXT} = 'By M.S.N.'
      !  OF 4
      !    ?Browse:1{PROP:FORMAT} ='96L(2)|FM*~Order Number~@s30@#50#65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Model Number / Unit Type~@s255@#185#42R(2)|FM*~Booked~@d6b@#16#100L(2)|FM*~Status~@s30@#21#48R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?Account_No_Tab{PROP:TEXT} = 'By Account No'
      !    Post(Event:Accepted,?tmp:AccountSearchOrder)
      !  OF 5 !Surname
      !    ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Surname~@s30@#55#65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#78L(2)|FM*~Model Number / Unit Type~@s255@#180#46L(2)|FM*~Postcode~@s30@#65#102L(2)|FM*~Address~@s30@#205#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?surname_tab{PROP:TEXT} = 'By Surname'
      !  OF 6
      !    ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#96L(2)|FM*~Model Number / Unit Type~@s60@#85#42R(2)|FM*~Booked~@d6b@#16#100L(2)|FM*~Status~@s30@#21#48R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?status_type_tab{PROP:TEXT} = 'By Statuses'
      !  OF 7
      !    ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#116L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#80L(2)|FM*~Exchange Status~@s30@#130#48R(2)|FM*~Changed~@d6b@#190#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?Exchange_Status{PROP:TEXT} = 'Exchange Status'
      !  OF 8
      !    ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#116L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#80L(2)|FM*~Loan Status~@s30@#125#48R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?Loan_Status{PROP:TEXT} = 'Loan Status'
      !  OF 9
      !    ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Mobile Number~@s15@#60#60L(2)|FM*~Account No~@s15@#6#65L(2)|FM*~Job No~@s20@#210#78L(2)|FM*~Model Number / Unit Type~@s255@#170#100L(2)|FM*~Status~@s30@#21#48R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?mobile_tab{PROP:TEXT} = 'By Mobile No.'
      !  OF 10
      !    ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Unit Type~@s30@#75#65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#74L(2)|FM*~Model Number / Unit Type~@s255@#11#104L(2)|FM*~Status~@s30@#21#48R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?Model_No_Tab{PROP:TEXT} = 'By Model No'
      !  OF 11
      !    ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#80L(2)|FM*~Model Number / Unit Type~@s60@#85#42R(2)|FM*~Booked~@d6b@#16#116L(2)|FM*~Status~@s30@#21#48R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    ?Engineer_Tab{PROP:TEXT} = 'By Engineer'
      !    Post(Event:Accepted,?Completed2_Temp:2)
      !  OF 12
      !    If def:HideLocation
      !        if tmp:DisableLocation
      !            ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#116L(2)|FM*~Model Number / Unit Type~@s60@#11#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Location~@s30@#135#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !        else
      !            ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#116L(2)|FM*~Model Number / Unit Type~@s60@#11#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Status~@s30@#21#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !        end
      !    Else !If def:HideLocation
      !        ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#116L(2)|FM*~Model Number / Unit Type~@s60@#11#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Location~@s30@#135#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
      !    End !If def:HideLocation
      !    
      !    ?Unallocated_Jobs_Tab{PROP:TEXT} = 'Unallocated Jobs'
      !  OF 13
      !    ?Browse:1{PROP:FORMAT} ='65L(2)|FM*~Job No~@s20@#210#60L(2)|FM*~Account No~@s15@#6#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Batch No~@s30@#230#92L(2)|FM*~Model Number / Unit Type~@s60@#100#80L(2)|FM*~Courier~@s30@#110#34L(2)|FM*~Type~@s3@#115#11L(2)FI@s1@#30#'
      !    ?Ready_To_Despatch{PROP:TEXT} = 'Despatch'
      !END
      !
      Do TabChanging
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    OF ?StatusSheet
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StatusSheet, NewSelection)
      Do TabChanging
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StatusSheet, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case Keycode()
          Of f4key
              thiswindow.reset
              Post(Event:accepted,?Validate_ESN)
          Of f5key
              brw1.resetsort(1)
          Of F8Key
              Case Choice(?CurrentTab)
                  Of 5
                      Post(Event:Accepted,?SearchByAddress)
              End !Case Choice(?CurrentTab)
      
      
      End!Case Keycode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(Event:NewSelection,?CurrentTab)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            !Do not validate accessories for jobs with an exchange unit on - 3692 (DBH: 11-12-2003)
            If job:Exchange_Unit_Number = ''
                If AccessoryCheck('JOB')
                    If AccessoryMismatch(job:Ref_Number,'JOB')
                        local:Errorcode = 1
                    End !If AccessoryMismatch(job:Ref_Number,'JOB')
                End !If AccessoryCheck('JOB')
            End !If job:Exchange_Unit_Number = ''
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)

            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    !Only show accessory attached/received by ARC - 4285 (DBH: 15-06-2004)
                    If ~glo:WebJob And ~jac:Attached
                        Cycle
                    End !If ~glo:WebJob And ~jac:Attached

                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            IF (AddToAudit(job:Ref_Number,'JOB','FAILED DESPATCH VALIDATION',locAuditNotes))
            END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
LocalValidateIMEI    Procedure()
local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            
        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
          '<13,10>'&|
          '<13,10>I.M.E.I. number mismatch.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
        GetStatus(850,1,job:Despatch_Type)

        IF (AddToAudit(job:ref_number,'JOB','FAILED DESPATCH VALIDATION','I.M.E.I. NUMBER MISMATCH'))
        END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccountNumber      Procedure()
    Code

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case Missive('The trade account attached to job number ' & Clip(job:Ref_Number) & ' is only used for "Batch Despatch".','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign
LocalFailedDeliveryAudit        Procedure(String  func:Type)
    Code
    GetStatus(899,1,func:Type)
    Access:JOBS.Update()

    If Access:AUDIT.PrimeRecord() = Level:Benign
        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = func:Type
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        aud:Action        = 'FAILED DELIVERY (' & Clip(func:Type) & ')'
        Access:AUDIT.Insert()
    End!If Access:AUDIT.PrimeRecord() = Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:Address{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:Address{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  If Records(Queue:Browse:1)
      Enable(?Button_Group)
      Enable(?Button_Group2)
  
  Else!If Records(Queue:Browse:1)
      Disable(?Button_Group)
      Disable(?Button_Group2)
  End!If Records(Queue:Browse:1)
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF Choice(?CurrentTab) = 1 And Upper(tmp:AllJobsType) = 0
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(tmp:AllJobsType) = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 0
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(7,Force)
  ELSIF CHOICE(?CurrentTab) = 6 And Choice(?StatusSheet) = 1 And Upper(tmp:SelectLocation) = 1
    RETURN SELF.SetSort(8,Force)
  ELSIF CHOICE(?CurrentTab) = 6 And Choice(?StatusSheet) = 1 And Upper(tmp:SelectLocation) = 2
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 6 And Choice(?StatusSheet) = 2
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 6 And Choice(?StatusSheet) = 3
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 9
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 10
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 11 And Upper(completed2_temp) <> 'ALL'
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'ALL'
    RETURN SELF.SetSort(15,Force)
  ELSIF Choice(?CurrentTab) = 12
    RETURN SELF.SetSort(16,Force)
  ELSIF Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'ALL' And Upper(Select_trade_account_temp) = 'ALL'
    RETURN SELF.SetSort(17,Force)
  ELSIF Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'ALL' And Upper(select_trade_account_temp) = 'IND'
    RETURN SELF.SetSort(18,Force)
  ELSIF Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'ALL'
    RETURN SELF.SetSort(19,Force)
  ELSIF Choice(?CurrentTab) = 13 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'IND'
    RETURN SELF.SetSort(20,Force)
  ELSE
    RETURN SELF.SetSort(21,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = job:Ref_Number
     GET(glo:Queue20,glo:Queue20.Pointer20)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  model_unit_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  address_temp = CLIP(job:Address_Line1) & ',  ' & CLIP(job:Address_Line2) & ',  ' & CLIP(job:Address_Line3)
  model_unit2_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit3_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit3_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit4_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit5_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  despatch_type_temp = job:Despatch_Type
  tmp:Location = job:Location
  tmp:ModelUnit3 = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  tmp:ModelUnit1 = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  tmp:ModelUnit3 = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  !Colour
  If job:Location = tmp:RRCLocation   
      tmp:BrowseFlag = 1        !AT RRC
  Else !job:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
      If job:Current_Status = tmp:StatusSendToRRC  
          tmp:BrowseFlag = 2           !Send to RRC
      Else !If job:Current_Status = GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI')
          If job:Location = tmp:DespatchLocation
              tmp:BrowseFlag = 3
          Else !If job:Date_Despatched <> ''
              tmp:BrowseFlag = 0
  
              ! DBH #10544 - Show a Purple Line if the job has been booked as a Liquid Repair
              Access:JOBSE.clearkey(jobe:RefNumberKey)
              jobe:RefNumber =job:Ref_Number
              if (access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign)
                  if (jobe:Booking48HourOption = 4)
                      tmp:BrowseFlag = 4
                  end
              end
          End !If job:Date_Despatched <> ''
      End !If job:Current_Status = GETINI('RRC','StatusSendToRRC',,CLIP(PATH())&'\SB2KDEF.INI')
  End !job:Location = GETINI('RRC','RRCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
  
  !Web Job Number
    access:webjob.clearkey(wob:refnumberkey)
    wob:refnumber = brw1.q.job:ref_number
    if access:webjob.fetch(wob:refnumberkey)
      tmp:jobnumber = brw1.q.job:ref_Number
    ELSE
      access:tradeacc.clearkey(tra:Account_Number_Key)
      tra:Account_Number = wob:HeadAccountNumber
      access:tradeacc.fetch(tra:Account_Number_Key)
      tmp:branchID = tra:BranchIdentification
      tmp:jobnumber = brw1.q.job:ref_number &'-'&clip(tmp:branchID)&wob:jobnumber
    END
  Case def:browse_option
      Of 'INV'
          tmp:address         = job:company_Name & '<13,10>' & |
                                job:address_line1 & '<13,10>' & |
                                job:address_line2 & '<13,10>' & |
                                job:address_line3 & '<13,10>' & |
                                job:postcode & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_number) & '<13,10>' & |
                                'Fax No: ' & Clip(job:fax_number)
      Of 'COL'
          tmp:address         = job:company_Name_collection & '<13,10>' & |
                                job:address_line1_collection & '<13,10>' & |
                                job:address_line2_collection & '<13,10>' & |
                                job:address_line3_collection & '<13,10>' & |
                                job:postcode_collection & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_collection)
      Of 'DEL'
          tmp:address         = job:company_Name_delivery & '<13,10>' & |
                                job:address_line1_delivery & '<13,10>' & |
                                job:address_line2_Delivery & '<13,10>' & |
                                job:address_line3_delivery & '<13,10>' & |
                                job:postcode_delivery & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_delivery)
      Else
          tmp:address         = job:company_Name & '<13,10>' & |
                                job:address_line1 & '<13,10>' & |
                                job:address_line2 & '<13,10>' & |
                                job:address_line3 & '<13,10>' & |
                                job:postcode & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_number) & '<13,10>' & |
                                'Fax No: ' & Clip(job:fax_number)
  
  End!Case def:browse_option
  
  tmp:ExchangeText = ''
  tmp:ExchangeText2 = ''
  If job:Exchange_Unit_Number <> ''
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number  = job:Exchange_Unit_Number
      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          tmp:ExchangeText = 'Exchange Issued: ' & Clip(xch:ESN)
          tmp:ExchangeText2 = 'Model No: ' & Clip(xch:Model_Number)
      Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
  End !job:Exchange_Unit_Number <> ''
  
  tmp:LoanText = ''
  tmp:LoanText2 = ''
  If job:Loan_Unit_Number <> ''
      Access:LOAN.Clearkey(loa:Ref_Number_Key)
      loa:Ref_Number  = job:Loan_Unit_Number
      If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
          !Found
          tmp:LoanText    = 'Loan Issued: ' & Clip(loa:ESN)
          tmp:LoanText2   = 'Model No: ' & Clip(loa:Model_Number)
      Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
  End !job:Loan_Unit_Number <> ''
  
  !Status Days
  Sat# = tra:IncludeSaturday
  Sun# = tra:IncludeSunday
  
  
  tmp:StatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = job:Ref_Number
  aus:Type        = 'JOB'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> job:Ref_Number      |
      Or aus:Type        <> 'JOB'      |
          Then Break.  ! End If
      tmp:StatusDate  =  aus:DateChanged
      
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:LoanStatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = job:Ref_Number
  aus:Type        = 'LOA'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> job:Ref_Number      |
      Or aus:Type        <> 'LOA'      |
          Then Break.  ! End If
      tmp:LoanStatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:ExchangeStatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = job:Ref_Number
  aus:Type        = 'EXC'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> job:Ref_Number      |
      Or aus:Type        <> 'EXC'      |
          Then Break.  ! End If
      tmp:ExchangeStatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:JobStatusDays = GetDays(Sat#,Sun#,tmp:StatusDate)
  tmp:ExcStatusDays = GetDays(Sat#,Sun#,tmp:ExchangeStatusDate)
  tmp:LoaStatusDays = GetDays(Sat#,Sun#,tmp:LoanStatusDate)
  
  If job:Date_Completed <> ''
      tmp:JobDays = GetDaysToDate(Sat#,Sun#,job:Date_Booked,job:date_Completed)
  Else !job:Date_Completed <> ''
      tmp:JobDays = GetDays(Sat#,Sun#,job:Date_Booked)
  End !job:Date_Completed <> ''
  
  If jobe:WebJob
      If wob:DateJobDespatched <> ''
          tmp:JobStatusDays = 'N/A'
      End !If wob:DateJobDespatched <> ''
  Else
      if job:Date_Despatched <> ''
          tmp:JobStatusDays = 'N/A'
      End !if job:Date_Despatched <> ''
  End !jobe:WebJob
  
  If job:Exchange_Despatched <> '' Or job:Exchange_Unit_Number = 0 Or Sub(job:Exchange_status,1,3) = '901'
      tmp:ExcStatusDays = 'N/A'
  End !job:Exchange_Despatched <> ''
  
  If job:Loan_Despatched <> '' Or job:Loan_unit_Number = 0 Or Sub(job:Loan_status,1,3) = '901'
      tmp:LoaStatusDays = 'N/A'
  End !job:Loan_Despatched <> ''
  
  !Batch Number
  IF CHOICE(?CurrentTab) = 16 !Despatch Tab
    Access:MuldespJ.ClearKey(mulj:JobNumberOnlyKey)
    mulj:JobNumber = job:Ref_Number
    IF Access:MuldespJ.Fetch(mulj:JobNumberOnlyKey)
      !Not Here!
      Tmp:Batch_No = ''
    ELSE
      Access:MulDesp.ClearKey(muld:RecordNumberKey)
      muld:RecordNumber = mulj:RefNumber
      IF Access:MulDesp.Fetch(muld:RecordNumberKey)
        !Not Here!
        Tmp:Batch_No = ''
      ELSE
        Tmp:Batch_No = muld:BatchNumber
      END
    END
  END
  PARENT.SetQueueRecord
  overdue2_temp = 'NO'
  turnaround# = 0
  status# = 0
  If job:turnaround_end_date <> ''
      If job:date_completed = '' And job:turnaround_end_date <> 90950
          If job:turnaround_end_date < Today()
              turnaround# = 1
          Else!If job:turnaround_end_date < Today()
              If job:turnaround_end_date = Today() And job:turnaround_end_time < Clock()
                  turnaround# = 1
              End!If job:turnaround_end_date = Today() And job:turnaround_end_time < Clock()
          End!If job:turnaround_end_date < Today()
      End!If job:date_completed = '' And job:turnaround_end_date <> 90950
  End!If job:turnaround_end_date <> ''
  If job:status_end_date <> ''
      If overdue2_temp = 'NO' And job:date_completed = '' And job:status_end_date <> 90950
          If job:status_end_date < Today()
              status# = 1
          Else!If job:status_end_date < Today()
              If job:status_end_date = Today() and job:status_end_time < Clock()
                  status# = 1
              End!If job:status_end_date = Today() and job:status_end_time < Clock()
          End!If job:status_end_date < Today()
      End!If overdue# = 0 And job:date_completed And job:status_end_date <> 90950
  End!If job:status_end_date <> ''
  If turnaround# = 1 And status# = 1
      overdue2_temp = 'BOT'
  End!If turnaround# = 1 And status# = 1
  If turnaround# = 0 and status# = 1
      overdue2_temp = 'STA'
  End!If turnaround# = 0 and status# = 1
  If turnaround# = 1 and status# = 0
      overdue2_temp = 'TUR'
  End!If turnaround# = 1 and status# = 0
  
  
  SELF.Q.job:Ref_Number_NormalFG = -1
  SELF.Q.job:Ref_Number_NormalBG = -1
  SELF.Q.job:Ref_Number_SelectedFG = -1
  SELF.Q.job:Ref_Number_SelectedBG = -1
  SELF.Q.tmp:jobnumber_NormalFG = -1
  SELF.Q.tmp:jobnumber_NormalBG = -1
  SELF.Q.tmp:jobnumber_SelectedFG = -1
  SELF.Q.tmp:jobnumber_SelectedBG = -1
  SELF.Q.job:Account_Number_NormalFG = -1
  SELF.Q.job:Account_Number_NormalBG = -1
  SELF.Q.job:Account_Number_SelectedFG = -1
  SELF.Q.job:Account_Number_SelectedBG = -1
  SELF.Q.model_unit_temp_NormalFG = -1
  SELF.Q.model_unit_temp_NormalBG = -1
  SELF.Q.model_unit_temp_SelectedFG = -1
  SELF.Q.model_unit_temp_SelectedBG = -1
  SELF.Q.job:date_booked_NormalFG = -1
  SELF.Q.job:date_booked_NormalBG = -1
  SELF.Q.job:date_booked_SelectedFG = -1
  SELF.Q.job:date_booked_SelectedBG = -1
  SELF.Q.job:Current_Status_NormalFG = -1
  SELF.Q.job:Current_Status_NormalBG = -1
  SELF.Q.job:Current_Status_SelectedFG = -1
  SELF.Q.job:Current_Status_SelectedBG = -1
  SELF.Q.job:Location_NormalFG = -1
  SELF.Q.job:Location_NormalBG = -1
  SELF.Q.job:Location_SelectedFG = -1
  SELF.Q.job:Location_SelectedBG = -1
  IF (overdue2_temp = 'BOT')
    SELF.Q.Overdue_Temp_Icon = 7
  ELSIF (overdue2_temp = 'STA')
    SELF.Q.Overdue_Temp_Icon = 8
  ELSIF (overdue2_temp = 'TUR')
    SELF.Q.Overdue_Temp_Icon = 9
  ELSE
    SELF.Q.Overdue_Temp_Icon = 11
  END
  IF (job:date_completed <> '')
    SELF.Q.Completed_Temp_Icon = 6
  ELSE
    SELF.Q.Completed_Temp_Icon = 11
  END
  IF (job:chargeable_job = 'YES' And job:invoice_number <> '')
    SELF.Q.Invoiced_Temp_Icon = 10
  ELSIF (job:chargeable_job = 'YES' and job:invoice_number = '')
    SELF.Q.Invoiced_Temp_Icon = 5
  ELSE
    SELF.Q.Invoiced_Temp_Icon = 11
  END
  IF (job:warranty_job = 'YES' And job:edi_batch_number <> '')
    SELF.Q.Warranty_Temp_Icon = 10
  ELSIF (job:warranty_job = 'YES' and job:edi_batch_number = '')
    SELF.Q.Warranty_Temp_Icon = 5
  ELSE
    SELF.Q.Warranty_Temp_Icon = 11
  END
  IF (job:Location = tmp:DespatchLocation)
    SELF.Q.Collected_Temp_Icon = 4
  ELSE
    SELF.Q.Collected_Temp_Icon = 11
  END
  IF (job:paid = 'YES' And job:invoice_number_warranty <> '')
    SELF.Q.Paid_Temp_Icon = 1
  ELSIF (job:paid = 'YES' And job:invoice_number_warranty = '')
    SELF.Q.Paid_Temp_Icon = 3
  ELSIF (job:paid <> 'YES' and job:invoice_number_warranty <> '')
    SELF.Q.Paid_Temp_Icon = 2
  ELSE
    SELF.Q.Paid_Temp_Icon = 11
  END
  SELF.Q.job:ESN_NormalFG = -1
  SELF.Q.job:ESN_NormalBG = -1
  SELF.Q.job:ESN_SelectedFG = -1
  SELF.Q.job:ESN_SelectedBG = -1
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 12
  ELSE
    SELF.Q.tag_temp_Icon = 11
  END
  SELF.Q.job:MSN_NormalFG = -1
  SELF.Q.job:MSN_NormalBG = -1
  SELF.Q.job:MSN_SelectedFG = -1
  SELF.Q.job:MSN_SelectedBG = -1
  SELF.Q.job:Order_Number_NormalFG = -1
  SELF.Q.job:Order_Number_NormalBG = -1
  SELF.Q.job:Order_Number_SelectedFG = -1
  SELF.Q.job:Order_Number_SelectedBG = -1
  SELF.Q.job:Surname_NormalFG = -1
  SELF.Q.job:Surname_NormalBG = -1
  SELF.Q.job:Surname_SelectedFG = -1
  SELF.Q.job:Surname_SelectedBG = -1
  SELF.Q.job:Mobile_Number_NormalFG = -1
  SELF.Q.job:Mobile_Number_NormalBG = -1
  SELF.Q.job:Mobile_Number_SelectedFG = -1
  SELF.Q.job:Mobile_Number_SelectedBG = -1
  SELF.Q.job:Postcode_NormalFG = -1
  SELF.Q.job:Postcode_NormalBG = -1
  SELF.Q.job:Postcode_SelectedFG = -1
  SELF.Q.job:Postcode_SelectedBG = -1
  SELF.Q.job:Model_Number_NormalFG = -1
  SELF.Q.job:Model_Number_NormalBG = -1
  SELF.Q.job:Model_Number_SelectedFG = -1
  SELF.Q.job:Model_Number_SelectedBG = -1
  SELF.Q.job:Unit_Type_NormalFG = -1
  SELF.Q.job:Unit_Type_NormalBG = -1
  SELF.Q.job:Unit_Type_SelectedFG = -1
  SELF.Q.job:Unit_Type_SelectedBG = -1
  SELF.Q.address_temp_NormalFG = -1
  SELF.Q.address_temp_NormalBG = -1
  SELF.Q.address_temp_SelectedFG = -1
  SELF.Q.address_temp_SelectedBG = -1
  SELF.Q.job:Consignment_Number_NormalFG = -1
  SELF.Q.job:Consignment_Number_NormalBG = -1
  SELF.Q.job:Consignment_Number_SelectedFG = -1
  SELF.Q.job:Consignment_Number_SelectedBG = -1
  SELF.Q.job:Current_Courier_NormalFG = -1
  SELF.Q.job:Current_Courier_NormalBG = -1
  SELF.Q.job:Current_Courier_SelectedFG = -1
  SELF.Q.job:Current_Courier_SelectedBG = -1
  SELF.Q.despatch_type_temp_NormalFG = -1
  SELF.Q.despatch_type_temp_NormalBG = -1
  SELF.Q.despatch_type_temp_SelectedFG = -1
  SELF.Q.despatch_type_temp_SelectedBG = -1
  SELF.Q.status2_temp_NormalFG = -1
  SELF.Q.status2_temp_NormalBG = -1
  SELF.Q.status2_temp_SelectedFG = -1
  SELF.Q.status2_temp_SelectedBG = -1
  SELF.Q.job:Loan_Status_NormalFG = -1
  SELF.Q.job:Loan_Status_NormalBG = -1
  SELF.Q.job:Loan_Status_SelectedFG = -1
  SELF.Q.job:Loan_Status_SelectedBG = -1
  SELF.Q.job:Exchange_Status_NormalFG = -1
  SELF.Q.job:Exchange_Status_NormalBG = -1
  SELF.Q.job:Exchange_Status_SelectedFG = -1
  SELF.Q.job:Exchange_Status_SelectedBG = -1
  SELF.Q.job:Incoming_Consignment_Number_NormalFG = -1
  SELF.Q.job:Incoming_Consignment_Number_NormalBG = -1
  SELF.Q.job:Incoming_Consignment_Number_SelectedFG = -1
  SELF.Q.job:Incoming_Consignment_Number_SelectedBG = -1
  SELF.Q.job:Exchange_Unit_Number_NormalFG = -1
  SELF.Q.job:Exchange_Unit_Number_NormalBG = -1
  SELF.Q.job:Exchange_Unit_Number_SelectedFG = -1
  SELF.Q.job:Exchange_Unit_Number_SelectedBG = -1
  SELF.Q.job:Loan_Unit_Number_NormalFG = -1
  SELF.Q.job:Loan_Unit_Number_NormalBG = -1
  SELF.Q.job:Loan_Unit_Number_SelectedFG = -1
  SELF.Q.job:Loan_Unit_Number_SelectedBG = -1
  SELF.Q.tmp:ExchangeText_NormalFG = -1
  SELF.Q.tmp:ExchangeText_NormalBG = -1
  SELF.Q.tmp:ExchangeText_SelectedFG = -1
  SELF.Q.tmp:ExchangeText_SelectedBG = -1
  SELF.Q.job:Address_Line1_NormalFG = -1
  SELF.Q.job:Address_Line1_NormalBG = -1
  SELF.Q.job:Address_Line1_SelectedFG = -1
  SELF.Q.job:Address_Line1_SelectedBG = -1
  SELF.Q.tmp:BrowseFlag_NormalFG = -1
  SELF.Q.tmp:BrowseFlag_NormalBG = -1
  SELF.Q.tmp:BrowseFlag_SelectedFG = -1
  SELF.Q.tmp:BrowseFlag_SelectedBG = -1
  SELF.Q.tmp:LoanText_NormalFG = -1
  SELF.Q.tmp:LoanText_NormalBG = -1
  SELF.Q.tmp:LoanText_SelectedFG = -1
  SELF.Q.tmp:LoanText_SelectedBG = -1
  SELF.Q.job:Date_Despatched_NormalFG = -1
  SELF.Q.job:Date_Despatched_NormalBG = -1
  SELF.Q.job:Date_Despatched_SelectedFG = -1
  SELF.Q.job:Date_Despatched_SelectedBG = -1
  SELF.Q.Tmp:Batch_No_NormalFG = -1
  SELF.Q.Tmp:Batch_No_NormalBG = -1
  SELF.Q.Tmp:Batch_No_SelectedFG = -1
  SELF.Q.Tmp:Batch_No_SelectedBG = -1
  SELF.Q.tmp:ExchangeStatusDate_NormalFG = -1
  SELF.Q.tmp:ExchangeStatusDate_NormalBG = -1
  SELF.Q.tmp:ExchangeStatusDate_SelectedFG = -1
  SELF.Q.tmp:ExchangeStatusDate_SelectedBG = -1
  SELF.Q.tmp:LoanStatusDate_NormalFG = -1
  SELF.Q.tmp:LoanStatusDate_NormalBG = -1
  SELF.Q.tmp:LoanStatusDate_SelectedFG = -1
  SELF.Q.tmp:LoanStatusDate_SelectedBG = -1
  SELF.Q.tmp:StatusDate_NormalFG = -1
  SELF.Q.tmp:StatusDate_NormalBG = -1
  SELF.Q.tmp:StatusDate_SelectedFG = -1
  SELF.Q.tmp:StatusDate_SelectedBG = -1
  SELF.Q.tmp:LoaStatusDays_NormalFG = -1
  SELF.Q.tmp:LoaStatusDays_NormalBG = -1
  SELF.Q.tmp:LoaStatusDays_SelectedFG = -1
  SELF.Q.tmp:LoaStatusDays_SelectedBG = -1
  SELF.Q.tmp:ExcStatusDays_NormalFG = -1
  SELF.Q.tmp:ExcStatusDays_NormalBG = -1
  SELF.Q.tmp:ExcStatusDays_SelectedFG = -1
  SELF.Q.tmp:ExcStatusDays_SelectedBG = -1
  SELF.Q.tmp:JobStatusDays_NormalFG = -1
  SELF.Q.tmp:JobStatusDays_NormalBG = -1
  SELF.Q.tmp:JobStatusDays_SelectedFG = -1
  SELF.Q.tmp:JobStatusDays_SelectedBG = -1
  SELF.Q.tmp:JobDays_NormalFG = -1
  SELF.Q.tmp:JobDays_NormalBG = -1
  SELF.Q.tmp:JobDays_SelectedFG = -1
  SELF.Q.tmp:JobDays_SelectedBG = -1
  SELF.Q.job:time_booked_NormalFG = -1
  SELF.Q.job:time_booked_NormalBG = -1
  SELF.Q.job:time_booked_SelectedFG = -1
  SELF.Q.job:time_booked_SelectedBG = -1
  SELF.Q.model_unit_temp = model_unit_temp            !Assign formula result to display queue
  SELF.Q.address_temp = address_temp                  !Assign formula result to display queue
  SELF.Q.despatch_type_temp = despatch_type_temp      !Assign formula result to display queue
   
   
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Ref_Number_NormalFG = 255
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Ref_Number_NormalFG = 32768
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Ref_Number_NormalFG = 16711680
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Ref_Number_NormalFG = 8388736
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Ref_Number_NormalFG = 0
     SELF.Q.job:Ref_Number_NormalBG = 65535
     SELF.Q.job:Ref_Number_SelectedFG = 65535
     SELF.Q.job:Ref_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Ref_Number_NormalFG = 0
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:jobnumber_NormalFG = 255
     SELF.Q.tmp:jobnumber_NormalBG = 16777215
     SELF.Q.tmp:jobnumber_SelectedFG = 16777215
     SELF.Q.tmp:jobnumber_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:jobnumber_NormalFG = 32768
     SELF.Q.tmp:jobnumber_NormalBG = 16777215
     SELF.Q.tmp:jobnumber_SelectedFG = 16777215
     SELF.Q.tmp:jobnumber_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:jobnumber_NormalFG = 16711680
     SELF.Q.tmp:jobnumber_NormalBG = 16777215
     SELF.Q.tmp:jobnumber_SelectedFG = 16777215
     SELF.Q.tmp:jobnumber_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:jobnumber_NormalFG = 8388736
     SELF.Q.tmp:jobnumber_NormalBG = 16777215
     SELF.Q.tmp:jobnumber_SelectedFG = 16777215
     SELF.Q.tmp:jobnumber_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:jobnumber_NormalFG = 0
     SELF.Q.tmp:jobnumber_NormalBG = 65535
     SELF.Q.tmp:jobnumber_SelectedFG = 65535
     SELF.Q.tmp:jobnumber_SelectedBG = 0
   ELSE
     SELF.Q.tmp:jobnumber_NormalFG = 0
     SELF.Q.tmp:jobnumber_NormalBG = 16777215
     SELF.Q.tmp:jobnumber_SelectedFG = 16777215
     SELF.Q.tmp:jobnumber_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Account_Number_NormalFG = 255
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Account_Number_NormalFG = 32768
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Account_Number_NormalFG = 16711680
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Account_Number_NormalFG = 8388736
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Account_Number_NormalFG = 0
     SELF.Q.job:Account_Number_NormalBG = 65535
     SELF.Q.job:Account_Number_SelectedFG = 65535
     SELF.Q.job:Account_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Account_Number_NormalFG = 0
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.model_unit_temp_NormalFG = 255
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.model_unit_temp_NormalFG = 32768
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.model_unit_temp_NormalFG = 16711680
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.model_unit_temp_NormalFG = 8388736
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.model_unit_temp_NormalFG = 0
     SELF.Q.model_unit_temp_NormalBG = 65535
     SELF.Q.model_unit_temp_SelectedFG = 65535
     SELF.Q.model_unit_temp_SelectedBG = 0
   ELSE
     SELF.Q.model_unit_temp_NormalFG = 0
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:date_booked_NormalFG = 255
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:date_booked_NormalFG = 32768
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:date_booked_NormalFG = 16711680
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:date_booked_NormalFG = 8388736
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:date_booked_NormalFG = 0
     SELF.Q.job:date_booked_NormalBG = 65535
     SELF.Q.job:date_booked_SelectedFG = 65535
     SELF.Q.job:date_booked_SelectedBG = 0
   ELSE
     SELF.Q.job:date_booked_NormalFG = 0
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Current_Status_NormalFG = 255
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Current_Status_NormalFG = 32768
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Current_Status_NormalFG = 16711680
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Current_Status_NormalFG = 8388736
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Current_Status_NormalFG = 0
     SELF.Q.job:Current_Status_NormalBG = 65535
     SELF.Q.job:Current_Status_SelectedFG = 65535
     SELF.Q.job:Current_Status_SelectedBG = 0
   ELSE
     SELF.Q.job:Current_Status_NormalFG = 0
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Location_NormalFG = 255
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Location_NormalFG = 32768
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Location_NormalFG = 16711680
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Location_NormalFG = 8388736
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Location_NormalFG = 0
     SELF.Q.job:Location_NormalBG = 65535
     SELF.Q.job:Location_SelectedFG = 65535
     SELF.Q.job:Location_SelectedBG = 0
   ELSE
     SELF.Q.job:Location_NormalFG = 0
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:ESN_NormalFG = 255
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:ESN_NormalFG = 32768
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:ESN_NormalFG = 16711680
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:ESN_NormalFG = 8388736
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:ESN_NormalFG = 0
     SELF.Q.job:ESN_NormalBG = 65535
     SELF.Q.job:ESN_SelectedFG = 65535
     SELF.Q.job:ESN_SelectedBG = 0
   ELSE
     SELF.Q.job:ESN_NormalFG = 0
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:MSN_NormalFG = 255
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:MSN_NormalFG = 32768
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:MSN_NormalFG = 16711680
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:MSN_NormalFG = 8388736
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:MSN_NormalFG = 0
     SELF.Q.job:MSN_NormalBG = 65535
     SELF.Q.job:MSN_SelectedFG = 65535
     SELF.Q.job:MSN_SelectedBG = 0
   ELSE
     SELF.Q.job:MSN_NormalFG = 0
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Order_Number_NormalFG = 255
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Order_Number_NormalFG = 32768
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Order_Number_NormalFG = 16711680
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Order_Number_NormalFG = 8388736
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Order_Number_NormalFG = 0
     SELF.Q.job:Order_Number_NormalBG = 65535
     SELF.Q.job:Order_Number_SelectedFG = 65535
     SELF.Q.job:Order_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Order_Number_NormalFG = 0
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Surname_NormalFG = 255
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Surname_NormalFG = 32768
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Surname_NormalFG = 16711680
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Surname_NormalFG = 8388736
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Surname_NormalFG = 0
     SELF.Q.job:Surname_NormalBG = 65535
     SELF.Q.job:Surname_SelectedFG = 65535
     SELF.Q.job:Surname_SelectedBG = 0
   ELSE
     SELF.Q.job:Surname_NormalFG = 0
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Mobile_Number_NormalFG = 255
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Mobile_Number_NormalFG = 32768
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Mobile_Number_NormalFG = 16711680
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Mobile_Number_NormalFG = 8388736
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Mobile_Number_NormalFG = 0
     SELF.Q.job:Mobile_Number_NormalBG = 65535
     SELF.Q.job:Mobile_Number_SelectedFG = 65535
     SELF.Q.job:Mobile_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Mobile_Number_NormalFG = 0
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Postcode_NormalFG = 255
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Postcode_NormalFG = 32768
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Postcode_NormalFG = 16711680
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Postcode_NormalFG = 8388736
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Postcode_NormalFG = 0
     SELF.Q.job:Postcode_NormalBG = 65535
     SELF.Q.job:Postcode_SelectedFG = 65535
     SELF.Q.job:Postcode_SelectedBG = 0
   ELSE
     SELF.Q.job:Postcode_NormalFG = 0
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Model_Number_NormalFG = 255
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Model_Number_NormalFG = 32768
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Model_Number_NormalFG = 16711680
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Model_Number_NormalFG = 8388736
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Model_Number_NormalFG = 0
     SELF.Q.job:Model_Number_NormalBG = 65535
     SELF.Q.job:Model_Number_SelectedFG = 65535
     SELF.Q.job:Model_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Model_Number_NormalFG = 0
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Unit_Type_NormalFG = 255
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Unit_Type_NormalFG = 32768
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Unit_Type_NormalFG = 16711680
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Unit_Type_NormalFG = 8388736
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Unit_Type_NormalFG = 0
     SELF.Q.job:Unit_Type_NormalBG = 65535
     SELF.Q.job:Unit_Type_SelectedFG = 65535
     SELF.Q.job:Unit_Type_SelectedBG = 0
   ELSE
     SELF.Q.job:Unit_Type_NormalFG = 0
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.address_temp_NormalFG = 255
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.address_temp_NormalFG = 32768
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.address_temp_NormalFG = 16711680
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.address_temp_NormalFG = 8388736
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.address_temp_NormalFG = 0
     SELF.Q.address_temp_NormalBG = 65535
     SELF.Q.address_temp_SelectedFG = 65535
     SELF.Q.address_temp_SelectedBG = 0
   ELSE
     SELF.Q.address_temp_NormalFG = 0
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Consignment_Number_NormalFG = 255
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Consignment_Number_NormalFG = 32768
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Consignment_Number_NormalFG = 16711680
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Consignment_Number_NormalFG = 8388736
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Consignment_Number_NormalFG = 0
     SELF.Q.job:Consignment_Number_NormalBG = 65535
     SELF.Q.job:Consignment_Number_SelectedFG = 65535
     SELF.Q.job:Consignment_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Consignment_Number_NormalFG = 0
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Current_Courier_NormalFG = 255
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Current_Courier_NormalFG = 32768
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Current_Courier_NormalFG = 16711680
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Current_Courier_NormalFG = 8388736
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Current_Courier_NormalFG = 0
     SELF.Q.job:Current_Courier_NormalBG = 65535
     SELF.Q.job:Current_Courier_SelectedFG = 65535
     SELF.Q.job:Current_Courier_SelectedBG = 0
   ELSE
     SELF.Q.job:Current_Courier_NormalFG = 0
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.despatch_type_temp_NormalFG = 255
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.despatch_type_temp_NormalFG = 32768
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.despatch_type_temp_NormalFG = 16711680
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.despatch_type_temp_NormalFG = 8388736
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.despatch_type_temp_NormalFG = 0
     SELF.Q.despatch_type_temp_NormalBG = 65535
     SELF.Q.despatch_type_temp_SelectedFG = 65535
     SELF.Q.despatch_type_temp_SelectedBG = 0
   ELSE
     SELF.Q.despatch_type_temp_NormalFG = 0
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.status2_temp_NormalFG = 255
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.status2_temp_NormalFG = 32768
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.status2_temp_NormalFG = 16711680
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.status2_temp_NormalFG = 8388736
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.status2_temp_NormalFG = 0
     SELF.Q.status2_temp_NormalBG = 65535
     SELF.Q.status2_temp_SelectedFG = 65535
     SELF.Q.status2_temp_SelectedBG = 0
   ELSE
     SELF.Q.status2_temp_NormalFG = 0
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Loan_Status_NormalFG = 255
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Loan_Status_NormalFG = 32768
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Loan_Status_NormalFG = 16711680
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Loan_Status_NormalFG = 8388736
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Loan_Status_NormalFG = 0
     SELF.Q.job:Loan_Status_NormalBG = 65535
     SELF.Q.job:Loan_Status_SelectedFG = 65535
     SELF.Q.job:Loan_Status_SelectedBG = 0
   ELSE
     SELF.Q.job:Loan_Status_NormalFG = 0
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Exchange_Status_NormalFG = 255
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Exchange_Status_NormalFG = 32768
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Exchange_Status_NormalFG = 16711680
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Exchange_Status_NormalFG = 8388736
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Exchange_Status_NormalFG = 0
     SELF.Q.job:Exchange_Status_NormalBG = 65535
     SELF.Q.job:Exchange_Status_SelectedFG = 65535
     SELF.Q.job:Exchange_Status_SelectedBG = 0
   ELSE
     SELF.Q.job:Exchange_Status_NormalFG = 0
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 255
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 32768
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 16711680
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 8388736
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 0
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 65535
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 65535
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 0
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 255
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 32768
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 16711680
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 8388736
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 0
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 65535
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 65535
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 0
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Loan_Unit_Number_NormalFG = 255
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Loan_Unit_Number_NormalFG = 32768
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Loan_Unit_Number_NormalFG = 16711680
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Loan_Unit_Number_NormalFG = 8388736
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Loan_Unit_Number_NormalFG = 0
     SELF.Q.job:Loan_Unit_Number_NormalBG = 65535
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 65535
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 0
   ELSE
     SELF.Q.job:Loan_Unit_Number_NormalFG = 0
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:ExchangeText_NormalFG = 255
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:ExchangeText_NormalFG = 32768
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:ExchangeText_NormalFG = 16711680
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:ExchangeText_NormalFG = 8388736
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:ExchangeText_NormalFG = 0
     SELF.Q.tmp:ExchangeText_NormalBG = 65535
     SELF.Q.tmp:ExchangeText_SelectedFG = 65535
     SELF.Q.tmp:ExchangeText_SelectedBG = 0
   ELSE
     SELF.Q.tmp:ExchangeText_NormalFG = 0
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Address_Line1_NormalFG = 255
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Address_Line1_NormalFG = 32768
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Address_Line1_NormalFG = 16711680
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Address_Line1_NormalFG = 8388736
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Address_Line1_NormalFG = 0
     SELF.Q.job:Address_Line1_NormalBG = 65535
     SELF.Q.job:Address_Line1_SelectedFG = 65535
     SELF.Q.job:Address_Line1_SelectedBG = 0
   ELSE
     SELF.Q.job:Address_Line1_NormalFG = 0
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:BrowseFlag_NormalFG = 255
     SELF.Q.tmp:BrowseFlag_NormalBG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedFG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:BrowseFlag_NormalFG = 32768
     SELF.Q.tmp:BrowseFlag_NormalBG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedFG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:BrowseFlag_NormalFG = 16711680
     SELF.Q.tmp:BrowseFlag_NormalBG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedFG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:BrowseFlag_NormalFG = 8388736
     SELF.Q.tmp:BrowseFlag_NormalBG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedFG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:BrowseFlag_NormalFG = 0
     SELF.Q.tmp:BrowseFlag_NormalBG = 65535
     SELF.Q.tmp:BrowseFlag_SelectedFG = 65535
     SELF.Q.tmp:BrowseFlag_SelectedBG = 0
   ELSE
     SELF.Q.tmp:BrowseFlag_NormalFG = 0
     SELF.Q.tmp:BrowseFlag_NormalBG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedFG = 16777215
     SELF.Q.tmp:BrowseFlag_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:LoanText_NormalFG = 255
     SELF.Q.tmp:LoanText_NormalBG = 16777215
     SELF.Q.tmp:LoanText_SelectedFG = 16777215
     SELF.Q.tmp:LoanText_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:LoanText_NormalFG = 32768
     SELF.Q.tmp:LoanText_NormalBG = 16777215
     SELF.Q.tmp:LoanText_SelectedFG = 16777215
     SELF.Q.tmp:LoanText_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:LoanText_NormalFG = 16711680
     SELF.Q.tmp:LoanText_NormalBG = 16777215
     SELF.Q.tmp:LoanText_SelectedFG = 16777215
     SELF.Q.tmp:LoanText_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:LoanText_NormalFG = 8388736
     SELF.Q.tmp:LoanText_NormalBG = 16777215
     SELF.Q.tmp:LoanText_SelectedFG = 16777215
     SELF.Q.tmp:LoanText_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:LoanText_NormalFG = 0
     SELF.Q.tmp:LoanText_NormalBG = 65535
     SELF.Q.tmp:LoanText_SelectedFG = 65535
     SELF.Q.tmp:LoanText_SelectedBG = 0
   ELSE
     SELF.Q.tmp:LoanText_NormalFG = 0
     SELF.Q.tmp:LoanText_NormalBG = 16777215
     SELF.Q.tmp:LoanText_SelectedFG = 16777215
     SELF.Q.tmp:LoanText_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:Date_Despatched_NormalFG = 255
     SELF.Q.job:Date_Despatched_NormalBG = 16777215
     SELF.Q.job:Date_Despatched_SelectedFG = 16777215
     SELF.Q.job:Date_Despatched_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:Date_Despatched_NormalFG = 32768
     SELF.Q.job:Date_Despatched_NormalBG = 16777215
     SELF.Q.job:Date_Despatched_SelectedFG = 16777215
     SELF.Q.job:Date_Despatched_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:Date_Despatched_NormalFG = 16711680
     SELF.Q.job:Date_Despatched_NormalBG = 16777215
     SELF.Q.job:Date_Despatched_SelectedFG = 16777215
     SELF.Q.job:Date_Despatched_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:Date_Despatched_NormalFG = 8388736
     SELF.Q.job:Date_Despatched_NormalBG = 16777215
     SELF.Q.job:Date_Despatched_SelectedFG = 16777215
     SELF.Q.job:Date_Despatched_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:Date_Despatched_NormalFG = 0
     SELF.Q.job:Date_Despatched_NormalBG = 65535
     SELF.Q.job:Date_Despatched_SelectedFG = 65535
     SELF.Q.job:Date_Despatched_SelectedBG = 0
   ELSE
     SELF.Q.job:Date_Despatched_NormalFG = 0
     SELF.Q.job:Date_Despatched_NormalBG = 16777215
     SELF.Q.job:Date_Despatched_SelectedFG = 16777215
     SELF.Q.job:Date_Despatched_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.Tmp:Batch_No_NormalFG = 255
     SELF.Q.Tmp:Batch_No_NormalBG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedFG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.Tmp:Batch_No_NormalFG = 32768
     SELF.Q.Tmp:Batch_No_NormalBG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedFG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.Tmp:Batch_No_NormalFG = 16711680
     SELF.Q.Tmp:Batch_No_NormalBG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedFG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.Tmp:Batch_No_NormalFG = 8388736
     SELF.Q.Tmp:Batch_No_NormalBG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedFG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.Tmp:Batch_No_NormalFG = 0
     SELF.Q.Tmp:Batch_No_NormalBG = 65535
     SELF.Q.Tmp:Batch_No_SelectedFG = 65535
     SELF.Q.Tmp:Batch_No_SelectedBG = 0
   ELSE
     SELF.Q.Tmp:Batch_No_NormalFG = 0
     SELF.Q.Tmp:Batch_No_NormalBG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedFG = 16777215
     SELF.Q.Tmp:Batch_No_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 255
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 32768
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 16711680
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 8388736
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 0
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 65535
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 65535
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 0
   ELSE
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 0
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:LoanStatusDate_NormalFG = 255
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:LoanStatusDate_NormalFG = 32768
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:LoanStatusDate_NormalFG = 16711680
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:LoanStatusDate_NormalFG = 8388736
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:LoanStatusDate_NormalFG = 0
     SELF.Q.tmp:LoanStatusDate_NormalBG = 65535
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 65535
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 0
   ELSE
     SELF.Q.tmp:LoanStatusDate_NormalFG = 0
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:StatusDate_NormalFG = 255
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:StatusDate_NormalFG = 32768
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:StatusDate_NormalFG = 16711680
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:StatusDate_NormalFG = 8388736
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:StatusDate_NormalFG = 0
     SELF.Q.tmp:StatusDate_NormalBG = 65535
     SELF.Q.tmp:StatusDate_SelectedFG = 65535
     SELF.Q.tmp:StatusDate_SelectedBG = 0
   ELSE
     SELF.Q.tmp:StatusDate_NormalFG = 0
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:LoaStatusDays_NormalFG = 255
     SELF.Q.tmp:LoaStatusDays_NormalBG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:LoaStatusDays_NormalFG = 32768
     SELF.Q.tmp:LoaStatusDays_NormalBG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:LoaStatusDays_NormalFG = 16711680
     SELF.Q.tmp:LoaStatusDays_NormalBG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:LoaStatusDays_NormalFG = 8388736
     SELF.Q.tmp:LoaStatusDays_NormalBG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:LoaStatusDays_NormalFG = 0
     SELF.Q.tmp:LoaStatusDays_NormalBG = 65535
     SELF.Q.tmp:LoaStatusDays_SelectedFG = 65535
     SELF.Q.tmp:LoaStatusDays_SelectedBG = 0
   ELSE
     SELF.Q.tmp:LoaStatusDays_NormalFG = 0
     SELF.Q.tmp:LoaStatusDays_NormalBG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:LoaStatusDays_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:ExcStatusDays_NormalFG = 255
     SELF.Q.tmp:ExcStatusDays_NormalBG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:ExcStatusDays_NormalFG = 32768
     SELF.Q.tmp:ExcStatusDays_NormalBG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:ExcStatusDays_NormalFG = 16711680
     SELF.Q.tmp:ExcStatusDays_NormalBG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:ExcStatusDays_NormalFG = 8388736
     SELF.Q.tmp:ExcStatusDays_NormalBG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:ExcStatusDays_NormalFG = 0
     SELF.Q.tmp:ExcStatusDays_NormalBG = 65535
     SELF.Q.tmp:ExcStatusDays_SelectedFG = 65535
     SELF.Q.tmp:ExcStatusDays_SelectedBG = 0
   ELSE
     SELF.Q.tmp:ExcStatusDays_NormalFG = 0
     SELF.Q.tmp:ExcStatusDays_NormalBG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:ExcStatusDays_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:JobStatusDays_NormalFG = 255
     SELF.Q.tmp:JobStatusDays_NormalBG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:JobStatusDays_NormalFG = 32768
     SELF.Q.tmp:JobStatusDays_NormalBG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:JobStatusDays_NormalFG = 16711680
     SELF.Q.tmp:JobStatusDays_NormalBG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:JobStatusDays_NormalFG = 8388736
     SELF.Q.tmp:JobStatusDays_NormalBG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:JobStatusDays_NormalFG = 0
     SELF.Q.tmp:JobStatusDays_NormalBG = 65535
     SELF.Q.tmp:JobStatusDays_SelectedFG = 65535
     SELF.Q.tmp:JobStatusDays_SelectedBG = 0
   ELSE
     SELF.Q.tmp:JobStatusDays_NormalFG = 0
     SELF.Q.tmp:JobStatusDays_NormalBG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedFG = 16777215
     SELF.Q.tmp:JobStatusDays_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.tmp:JobDays_NormalFG = 255
     SELF.Q.tmp:JobDays_NormalBG = 16777215
     SELF.Q.tmp:JobDays_SelectedFG = 16777215
     SELF.Q.tmp:JobDays_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.tmp:JobDays_NormalFG = 32768
     SELF.Q.tmp:JobDays_NormalBG = 16777215
     SELF.Q.tmp:JobDays_SelectedFG = 16777215
     SELF.Q.tmp:JobDays_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.tmp:JobDays_NormalFG = 16711680
     SELF.Q.tmp:JobDays_NormalBG = 16777215
     SELF.Q.tmp:JobDays_SelectedFG = 16777215
     SELF.Q.tmp:JobDays_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.tmp:JobDays_NormalFG = 8388736
     SELF.Q.tmp:JobDays_NormalBG = 16777215
     SELF.Q.tmp:JobDays_SelectedFG = 16777215
     SELF.Q.tmp:JobDays_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.tmp:JobDays_NormalFG = 0
     SELF.Q.tmp:JobDays_NormalBG = 65535
     SELF.Q.tmp:JobDays_SelectedFG = 65535
     SELF.Q.tmp:JobDays_SelectedBG = 0
   ELSE
     SELF.Q.tmp:JobDays_NormalFG = 0
     SELF.Q.tmp:JobDays_NormalBG = 16777215
     SELF.Q.tmp:JobDays_SelectedFG = 16777215
     SELF.Q.tmp:JobDays_SelectedBG = 0
   END
   IF (tmp:BrowseFlag = 1)
     SELF.Q.job:time_booked_NormalFG = 255
     SELF.Q.job:time_booked_NormalBG = 16777215
     SELF.Q.job:time_booked_SelectedFG = 16777215
     SELF.Q.job:time_booked_SelectedBG = 255
   ELSIF(tmp:BrowseFlag = 2)
     SELF.Q.job:time_booked_NormalFG = 32768
     SELF.Q.job:time_booked_NormalBG = 16777215
     SELF.Q.job:time_booked_SelectedFG = 16777215
     SELF.Q.job:time_booked_SelectedBG = 32768
   ELSIF(tmp:BrowseFlag = 3)
     SELF.Q.job:time_booked_NormalFG = 16711680
     SELF.Q.job:time_booked_NormalBG = 16777215
     SELF.Q.job:time_booked_SelectedFG = 16777215
     SELF.Q.job:time_booked_SelectedBG = 16711680
   ELSIF(tmp:BrowseFlag = 4)
     SELF.Q.job:time_booked_NormalFG = 8388736
     SELF.Q.job:time_booked_NormalBG = 16777215
     SELF.Q.job:time_booked_SelectedFG = 16777215
     SELF.Q.job:time_booked_SelectedBG = 8388736
   ELSIF(jobe:jobmark = 1)
     SELF.Q.job:time_booked_NormalFG = 0
     SELF.Q.job:time_booked_NormalBG = 65535
     SELF.Q.job:time_booked_SelectedFG = 65535
     SELF.Q.job:time_booked_SelectedBG = 0
   ELSE
     SELF.Q.job:time_booked_NormalFG = 0
     SELF.Q.job:time_booked_NormalBG = 16777215
     SELF.Q.job:time_booked_SelectedFG = 16777215
     SELF.Q.job:time_booked_SelectedBG = 0
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
    !More Code Added by Neil!
    !Note - but all jobs now have a webjob entry!
    IF CHOICE(?CurrentTab) = 16
      !Look up to see if it is a webjob!
      Access:WebJob.ClearKey(wob:RefNumberKey)
      wob:RefNumber = job:Ref_Number
      IF Access:WebJob.Fetch(wob:RefNumberKey)
        !Not here!
        RETURN Record:OK
      ELSE
        IF jobe:HubRepair = TRUE
          RETURN Record:OK
        ELSE
          RETURN RECORD:Filtered
        END
      END
    END
  
  
  
  
  
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue20.Pointer20 = job:Ref_Number
     GET(glo:Queue20,glo:Queue20.Pointer20)
    EXECUTE DASBRW::26:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !job:Ref_Number
  OF 6 !tmp:jobnumber
  OF 11 !job:Account_Number
  OF 16 !model_unit_temp
  OF 21 !job:date_booked
  OF 26 !job:Current_Status
  OF 31 !job:Location
  OF 36 !Overdue_Temp
  OF 38 !Completed_Temp
  OF 40 !Invoiced_Temp
  OF 42 !Warranty_Temp
  OF 44 !Collected_Temp
  OF 46 !Paid_Temp
  OF 48 !job:ESN
  OF 53 !tag_temp
  OF 55 !job:MSN
  OF 60 !job:Order_Number
  OF 65 !job:Surname
  OF 70 !job:Mobile_Number
  OF 75 !job:Postcode
  OF 80 !job:Model_Number
  OF 85 !job:Unit_Type
  OF 90 !address_temp
  OF 95 !job:Consignment_Number
  OF 100 !job:Current_Courier
  OF 105 !despatch_type_temp
  OF 110 !status2_temp
  OF 115 !job:Loan_Status
  OF 120 !job:Exchange_Status
  OF 125 !job:Incoming_Consignment_Number
  OF 130 !job:Exchange_Unit_Number
  OF 135 !job:Loan_Unit_Number
  OF 140 !tmp:ExchangeText
  OF 145 !job:Address_Line1
  OF 150 !tmp:BrowseFlag
  OF 155 !tmp:LoanText
  OF 160 !job:Date_Despatched
  OF 165 !Tmp:Batch_No
  OF 170 !tmp:ExchangeStatusDate
  OF 175 !tmp:LoanStatusDate
  OF 180 !tmp:StatusDate
  OF 185 !tmp:LoaStatusDays
  OF 190 !tmp:ExcStatusDays
  OF 195 !tmp:JobStatusDays
  OF 200 !tmp:JobDays
  OF 205 !job:time_booked
  OF 210 !job:Company_Name
  OF 211 !job:Address_Line2
  OF 212 !job:Address_Line3
  OF 213 !Postcode_Temp
  OF 214 !Address_Line3_Temp
  OF 215 !Address_Line2_Temp
  OF 216 !Address_Line1_Temp
  OF 217 !Company_Name_Temp
  OF 218 !tmp:Address
  OF 219 !tmp:LoanText2
  OF 220 !tmp:ExchangeText2
  OF 221 !job:Engineer
  OF 222 !job:Completed
  OF 223 !job:Workshop
  OF 224 !job:Despatched
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s7')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:jobnumber')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:jobnumber')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:jobnumber')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account No')                 !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account No')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('model_unit_temp')            !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_unit_temp')            !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_unit_temp')            !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:date_booked')            !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Booked')                     !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Booked')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Current_Status')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Status')                     !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Status')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Overdue_Temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Overdue_Temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Overdue_Temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Completed_Temp')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Completed_Temp')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Completed_Temp')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Invoiced_Temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Invoiced_Temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Invoiced_Temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Warranty_Temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Warranty_Temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Warranty_Temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Collected_Temp')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Collected_Temp')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Collected_Temp')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Paid_Temp')                  !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Paid_Temp')                  !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Paid_Temp')                  !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('ESN/IMEI')                   !Header
                PSTRING('@s16')                       !Picture
                PSTRING('ESN/IMEI')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tag_temp')                   !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tag_temp')                   !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tag_temp')                   !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:MSN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('MSN')                        !Header
                PSTRING('@s15')                       !Picture
                PSTRING('MSN')                        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Order_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Surname')                !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Surname')                    !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Surname')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Mobile_Number')          !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Mobile Number')              !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Mobile Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Postcode')               !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Postcode')                   !Header
                PSTRING('@s10')                       !Picture
                PSTRING('Postcode')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Unit_Type')              !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Unit Type')                  !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Unit Type')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('address_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('address_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('address_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Consignment_Number')     !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Consignment No')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Consignment No')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Current_Courier')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier')                    !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('despatch_type_temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('despatch_type_temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('despatch_type_temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('status2_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('status2_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('status2_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Loan_Status')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Loan Status')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Loan Status')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Exchange_Status')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Exchange Status')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Exchange Status')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Incoming_Consignment_Number') !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Consignment No')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Consignment No')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Exchange_Unit_Number')   !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Exchange Unit Number')       !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Exchange Unit Number')       !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Loan_Unit_Number')       !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Loan Unit Number')           !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Loan Unit Number')           !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ExchangeText')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExchangeText')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExchangeText')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Address_Line1')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:BrowseFlag')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:BrowseFlag')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:BrowseFlag')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LoanText')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LoanText')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LoanText')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Date_Despatched')        !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date Despatched')            !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date Despatched')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Tmp:Batch_No')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Tmp:Batch_No')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Tmp:Batch_No')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ExchangeStatusDate')     !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExchangeStatusDate')     !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExchangeStatusDate')     !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LoanStatusDate')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LoanStatusDate')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LoanStatusDate')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:StatusDate')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:StatusDate')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:StatusDate')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LoaStatusDays')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LoaStatusDays')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LoaStatusDays')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ExcStatusDays')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExcStatusDays')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExcStatusDays')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:JobStatusDays')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:JobStatusDays')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:JobStatusDays')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:JobDays')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:JobDays')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:JobDays')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:time_booked')            !Field Name
                SHORT(20)                             !Default Column Width
                PSTRING('time booked')                !Header
                PSTRING('@t1')                        !Picture
                PSTRING('time booked')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Company_Name')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Company Name')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Company Name')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Address_Line2')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('DELETE THIS')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('DELETE THIS')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Address_Line3')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('DELETE THIS')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('DELETE THIS')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Postcode_Temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Postcode_Temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Postcode_Temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Address_Line3_Temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Address_Line3_Temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Address_Line3_Temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Address_Line2_Temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Address_Line2_Temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Address_Line2_Temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Address_Line1_Temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Address_Line1_Temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Address_Line1_Temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Company_Name_Temp')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Company_Name_Temp')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Company_Name_Temp')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:Address')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Address')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Address')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LoanText2')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LoanText2')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LoanText2')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ExchangeText2')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExchangeText2')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExchangeText2')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Engineer')               !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Engineer')                   !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Engineer')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Completed')              !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Completed')                  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Completed')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Workshop')               !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Workshop')                   !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Workshop')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Despatched')             !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatched')                 !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatched')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                END
XpDim           SHORT(61)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 50
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Auto_Change_Courier PROCEDURE                         !Generated from procedure template - Window

tmp:FromCourier      STRING(30)
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:ToCourier        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:FromCourier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ToCourier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB3::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Auto Change Courier'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Auto Change Courier'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This facility is used for Automatically changing the Incoming and Outgoing Couri' &|
   'er of ALL the Trade / Sub Accounts where applicable.'),AT(238,144,204,32),USE(?Prompt1),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Change Courier From'),AT(238,188),USE(?Prompt2),FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Change Courier To'),AT(238,220),USE(?Prompt2:3),FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           COMBO(@s30),AT(318,200,124,10),USE(tmp:FromCourier),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           COMBO(@s30),AT(318,232,124,10),USE(tmp:ToCourier),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Courier'),AT(238,200),USE(?Prompt2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Courier'),AT(238,232),USE(?Prompt2:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(380,332),USE(?Button2),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020555'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Auto_Change_Courier')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Auto_Change_Courier')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB2.Init(tmp:FromCourier,?tmp:FromCourier,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(cou:Courier_Key)
  FDCB2.AddField(cou:Courier,FDCB2.Q.cou:Courier)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB3.Init(tmp:ToCourier,?tmp:ToCourier,Queue:FileDropCombo:1.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo:1,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo:1
  FDCB3.AddSortOrder(cou:Courier_Key)
  FDCB3.AddField(cou:Courier,FDCB3.Q.cou:Courier)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Auto_Change_Courier')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020555'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020555'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020555'&'0')
      ***
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      Case Missive('This routine will now go through the trade account and change every occurrence of the courier ' & Clip(tmp:FromCourier) & ' to the courier ' & Clip(tmp:ToCourier) & '.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              setcursor(cursor:wait)
              save_tra_id = access:tradeacc.savefile()
              set(tra:account_number_key)
              loop
                  if access:tradeacc.next()
                     break
                  end !if
                  If tra:courier_incoming = tmp:FromCourier
                      tra:courier_incoming    = tmp:ToCourier
                      access:tradeacc.update()
                  End!If tra:courier_incoming = tmp:FromCourier
                  If tra:courier_outgoing = tmp:FromCourier
                      tra:courier_outgoing    = tmp:ToCourier
                      access:tradeacc.update()
                  End!If tra:courier_outgoing = tmp:FromCourier
              end !loop
              access:tradeacc.restorefile(save_tra_id)
      
              save_sub_id = access:subtracc.savefile()
              set(sub:account_number_key)
              loop
                  if access:subtracc.next()
                     break
                  end !if
                  If sub:courier_incoming = tmp:FromCourier
                      sub:courier_incoming    = tmp:ToCourier
                      access:subtracc.update()
                  End!If tra:courier_incoming = tmp:FromCourier
                  If sub:courier_outgoing = tmp:FromCourier
                      sub:courier_outgoing    = tmp:ToCourier
                      access:subtracc.update()
                  End!If tra:courier_outgoing = tmp:FromCourier
              end !loop
              access:subtracc.restorefile(save_sub_id)
      
              setcursor()
              Case Missive('Process completed.','ServiceBase 3g',|
                             'midea.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Remove_Audit PROCEDURE                                !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Remove Audit Trail'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Remove Audit Trail'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,164,192,92),USE(?Panel2),FILL(09A6A7CH)
                       PROMPT('This routine is used to remove the Audit Trail from completed, invoiced jobs.'),AT(248,192,184,20),USE(?Prompt1),FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Only included jobs over'),AT(248,216),USE(?Prompt2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SPIN(@s30),AT(340,216,44,10),USE(GLO:Select1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       PROMPT('days old'),AT(388,216),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(300,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_job_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020559'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Remove_Audit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Remove_Audit')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Remove_Audit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020559'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020559'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020559'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Case Missive('Are you sure you want to remove the audit trail from all completed, invoiced jobs over ' & Clip(glo:Select1) & ' days old.'&|
        '<13,10>Warning! This may take some time, and you will not be able to recover any deleted information.','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|/Continue') 
          Of 2 ! Continue Button
      
          Prog.ProgressSetup(Records(JOBS))
          
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:date_booked_key)
          job:date_booked = ''
          set(job:date_booked_key,job:date_booked_key)
          loop
              if access:jobs.next()
                 break
              end !if
      
              If Prog.InsideLoop()
                  Break
              End ! If Prog.InsideLoop()
      
              if job:date_booked > (Today() - glo:select1)      |
                  then break.  ! end if
              if job:invoice_number = '' and job:chargeable_job = 'YES'
                  Cycle
              end
              If job:invoice_Number_warranty = '' And job:warranty_job = 'YES'
                  Cycle
              End!If job:invoice_Number_warranty = '' And job:warranty_job = 'YES'
              if job:date_completed = ''
                  Cycle
              end
              access:audit.clearkey(aud:ref_number_key)
              aud:ref_number = job:ref_number
              set(aud:ref_number_key,aud:ref_number_key)
              loop
                  if access:audit.next()
                     break
                  end !if
                  if aud:ref_number <> job:ref_number      |
                      then break.  ! end if
                  Delete(Audit)
                  deleted# += 1
                  Prog.ProgressText('Entries Deleted: ' & deleted#)
                  display()
              end !loop
          end !loop
          access:jobs.restorefile(save_job_id)
          Prog.ProgressFinish()
      
          Case Missive('Process Completed.','ServiceBase 3g',|
                         'midea.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      
          Of 1 ! Cancel Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
UpdateTURNARND PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::tur:Record  LIKE(tur:RECORD),STATIC
QuickWindow          WINDOW('Update the TURNARND File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Turnaround Time'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,164,192,90),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Turnaround Time'),AT(248,198),USE(?TUR:Turnaround_Time:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,198,116,10),USE(tur:Turnaround_Time),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Time'),AT(248,220),USE(?TUR:Days:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n3),AT(316,220,40,10),USE(tur:Days),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(0,9999),STEP(1)
                           PROMPT('Hours'),AT(360,212),USE(?TUR:Hours:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           SPIN(@n2),AT(360,220,40,10),USE(tur:Hours),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,RANGE(0,23),STEP(1)
                           PROMPT('Days'),AT(316,212),USE(?Prompt4),TRN,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job Turnaround Time'
  OF ChangeRecord
    ActionMessage = 'Changing A Job Turnaround Time'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020551'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTURNARND')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tur:Record,History::tur:Record)
  SELF.AddHistoryField(?tur:Turnaround_Time,1)
  SELF.AddHistoryField(?tur:Days,2)
  SELF.AddHistoryField(?tur:Hours,3)
  SELF.AddUpdateFile(Access:TURNARND)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TURNARND
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateTURNARND')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TURNARND.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateTURNARND')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020551'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020551'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020551'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Turnaround_Time PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                       PROJECT(tur:Days)
                       PROJECT(tur:Hours)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
tur:Days               LIKE(tur:Days)                 !List box control field - type derived from field
tur:Hours              LIKE(tur:Hours)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Job Turnaround File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Job Turnaround Time File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(248,114,188,210),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('125L(2)|M~Job Turnaround Time~@s30@24R(2)|M~Days~@n3@24R(2)|M~Hours~C(0)@n2@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,84,352,244),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Job Turnaround Time'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,100,124,10),USE(tur:Turnaround_Time),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,116),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,230),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,264),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,296),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,FONT('Tahoma',8,,),ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020530'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Turnaround_Time')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TURNARND,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Turnaround_Time')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,tur:Turnaround_Time_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?tur:Turnaround_Time,tur:Turnaround_Time,1,BRW1)
  BRW1.AddField(tur:Turnaround_Time,BRW1.Q.tur:Turnaround_Time)
  BRW1.AddField(tur:Days,BRW1.Q.tur:Days)
  BRW1.AddField(tur:Hours,BRW1.Q.tur:Hours)
  QuickWindow{PROP:MinWidth}=292
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TURNARND.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Turnaround_Time')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of insertrecord
          check_access('JOB TURNAROUND TIMES - INSERT',x")
          if x" = false
              Case Missive('You do not have access to this option','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of changerecord
          check_access('JOB TURNAROUND TIMES - CHANGE',x")
          if x" = false
              Case Missive('You do not have access to this option','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of deleterecord
          check_access('JOB TURNAROUND TIMES - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
  
              do_update# = false
          end
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTURNARND
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020530'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020530'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020530'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tur:Turnaround_Time
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tur:Turnaround_Time, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tur:Turnaround_Time, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?TUR:Turnaround_Time, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

Update_Payment_Types PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::pay:Record  LIKE(pay:RECORD),STATIC
QuickWindow          WINDOW('Update the PAYTYPES File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Payment Type'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Payment Type'),AT(248,201),USE(?PAY:Payment_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(308,201,124,10),USE(pay:Payment_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           CHECK('Credit Card'),AT(308,219,70,8),USE(pay:Credit_Card),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Payment Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Payment Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020552'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Payment_Types')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(pay:Record,History::pay:Record)
  SELF.AddHistoryField(?pay:Payment_Type,1)
  SELF.AddHistoryField(?pay:Credit_Card,2)
  SELF.AddUpdateFile(Access:PAYTYPES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PAYTYPES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PAYTYPES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_Payment_Types')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PAYTYPES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Payment_Types')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    pay:Credit_Card = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020552'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020552'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020552'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Payment_Types PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(PAYTYPES)
                       PROJECT(pay:Payment_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
pay:Payment_Type       LIKE(pay:Payment_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Payment Types File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Payment Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,114,148,210),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Payment Type~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,246),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Payment Type'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(pay:Payment_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,234),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,266),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,296),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020528'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Payment_Types')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:PAYTYPES.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PAYTYPES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Payment_Types')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,pay:Payment_Type_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?pay:Payment_Type,pay:Payment_Type,1,BRW1)
  BRW1.AddField(pay:Payment_Type,BRW1.Q.pay:Payment_Type)
  QuickWindow{PROP:MinWidth}=248
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PAYTYPES.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Payment_Types')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Payment_Types
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020528'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020528'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020528'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?pay:Payment_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pay:Payment_Type, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?pay:Payment_Type, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?PAY:Payment_Type, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

Update_QA_Reason PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::qar:Record  LIKE(qar:RECORD),STATIC
QuickWindow          WINDOW('Update The QA Reason File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend QA Reason'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,164,192,90),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('QA Reason'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(252,206,176,10),USE(qar:Reason),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting a QA Reason'
  OF ChangeRecord
    ActionMessage = 'Changing A QA Reason'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020553'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_QA_Reason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(qar:Record,History::qar:Record)
  SELF.AddHistoryField(?qar:Reason,2)
  SELF.AddUpdateFile(Access:QAREASON)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:QAREASON.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:QAREASON
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Update_QA_Reason')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:QAREASON.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_QA_Reason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020553'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020553'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020553'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_QA_Reason PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(QAREASON)
                       PROJECT(qar:Reason)
                       PROJECT(qar:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
qar:Reason             LIKE(qar:Reason)               !List box control field - type derived from field
qar:RecordNumber       LIKE(qar:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse QA Reason'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The QA Reason File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,116,148,208),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Reason~@s60@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,84,352,244),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Reason'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(264,100,124,10),USE(qar:Reason),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,116),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,236),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,266),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,296),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020529'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_QA_Reason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:QAREASON.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:QAREASON,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_QA_Reason')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,qar:ReasonKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,qar:Reason,1,BRW1)
  BRW1.AddField(qar:Reason,BRW1.Q.qar:Reason)
  BRW1.AddField(qar:RecordNumber,BRW1.Q.qar:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:QAREASON.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_QA_Reason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_QA_Reason
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020529'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020529'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020529'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?qar:Reason
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?qar:Reason, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?qar:Reason, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Reconcile_Claim_Value PROCEDURE (func:Manufacturer,func:BatchNumber,f_Ref_number,f_value_claimed,f_courier,f_labour,f_parts,f_total_jobs) !Generated from procedure template - Window

FilesOpened          BYTE
save_wpr_id          USHORT,AUTO
save_job_id          USHORT,AUTO
Reference_Number_Temp STRING(30)
Value_Claimed_Temp   REAL
Labour_Temp          REAL
Parts_Temp           REAL
total_jobs_temp      REAL
Difference_Temp      REAL
courier_temp         REAL
tmp:PerJob           REAL
tmp:ExchangeClaimed  REAL
tmp:LabourClaimed    REAL
tmp:PartsClaimed     REAL
tmp:ExchangeDifference REAL
tmp:LabourDifference REAL
tmp:PartsDifference  REAL
tmp:ExchangePerJob   REAL
tmp:LabourPerJob     REAL
tmp:PartsPerJob      REAL
tmp:RunningTotal     REAL
tmp:WarrantyPartsCount LONG
tmp:BatchExchangeCost REAL
tmp:BatchLabourCost  REAL
tmp:BatchPartsCost   REAL
tmp:BatchTotal       REAL
tmp:ExchangeJobs     LONG
tmp:LabourJobs       LONG
tmp:PartsJobs        LONG
tmp:PaidTotal        REAL
tmp:DifferenceTotal  REAL
tmp:LabourRunningTotal REAL
tmp:ExchangeRunningTotal REAL
tmp:PartsRunningTotal REAL
tmp:LabourAdjustment REAL
tmp:ExchangeAdjustment REAL
tmp:PartsAdjustment  REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:TotalVariance    REAL
window               WINDOW('Warranty Claim Financial Details'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Claim Financial Details'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Financial Details'),USE(?Tab1)
                           PROMPT('Total Jobs Claimed'),AT(68,120),USE(?total_jobs:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n6),AT(168,120,64,10),USE(total_jobs_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Reference Number'),AT(68,136),USE(?Select_Global1:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(168,136,124,10),USE(Reference_Number_Temp),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Value Claimed'),AT(68,256),USE(?Select_Global2:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(172,256,48,10),USE(tmp:BatchTotal),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           ENTRY(@n14.2),AT(236,256,48,10),USE(Value_Claimed_Temp),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           ENTRY(@n14.2),AT(304,256,48,10),USE(tmp:PaidTotal),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           ENTRY(@n-14.2),AT(380,256,48,10),USE(tmp:DifferenceTotal),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),READONLY
                           PROMPT('Total Variance (%)'),AT(480,248),USE(?tmp:TotalVariance:Prompt),TRN,HIDE,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(488,256,48,10),USE(tmp:TotalVariance),SKIP,HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Tota Variance'),TIP('Tota Variance'),UPR,READONLY
                           BUTTON,AT(68,330),USE(?Export_Claims),TRN,FLAT,LEFT,ICON('prnservp.jpg')
                           ENTRY(@s8),AT(436,236,48,10),USE(tmp:PartsJobs),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Difference'),TIP('Parts Difference'),UPR,READONLY
                           ENTRY(@n14.2),AT(172,204,48,10),USE(tmp:BatchExchangeCost),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Claimed'),TIP('Exchange Claimed'),UPR,READONLY
                           BUTTON,AT(296,156),USE(?Valuate_Claim:2),TRN,FLAT,LEFT,ICON('autovalp.jpg')
                           PROMPT('Unreconciled Batch Total'),AT(160,188,60,16),USE(?Prompt19),RIGHT,FONT(,7,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(380,204,48,10),USE(tmp:ExchangeDifference),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Difference'),TIP('Exchange Difference'),UPR
                           ENTRY(@n-14.2),AT(488,220,48,10),USE(tmp:LabourPerJob),SKIP,HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Per Job'),TIP('Labour Per Job'),UPR,READONLY
                           PROMPT('Claimed'),AT(256,194),USE(?Prompt12),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Paid'),AT(332,194),USE(?Prompt13),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Difference'),AT(392,194),USE(?Prompt11),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Affected Jobs'),AT(436,194),USE(?Prompt11:4),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Variance'),AT(508,194),USE(?Prompt11:2),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('% Removed Per Job'),AT(540,204),USE(?Prompt14),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14.2),AT(488,204,48,10),USE(tmp:ExchangePerJob),SKIP,HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Per Job'),TIP('Exchange Per Job'),UPR,READONLY
                           BUTTON,AT(548,332),USE(?LabourVariance),TRN,FLAT,LEFT,ICON('appvarp.jpg')
                           PROMPT('% Removed Per Job'),AT(540,220),USE(?Prompt14:3),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(228,156),USE(?Valuate_Claim),TRN,FLAT,LEFT,ICON('autovalp.jpg')
                           PROMPT('Exchange Cost (Less VAT)'),AT(68,204),USE(?courier_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(304,204,48,10),USE(courier_temp),DISABLE,RIGHT,FONT('Tahoma',,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           LINE,AT(368,212,0,45),USE(?Line2),COLOR(COLOR:Black)
                           ENTRY(@n14.2),AT(236,204,48,10),USE(tmp:ExchangeClaimed),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Claimed'),TIP('Exchange Claimed'),UPR,READONLY
                           PROMPT('Labour Value (Less VAT)'),AT(68,220),USE(?Select_Global3:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(172,220,48,10),USE(tmp:BatchLabourCost),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Claimed'),TIP('Labour Claimed'),UPR,READONLY
                           ENTRY(@n14.2),AT(304,220,48,10),USE(Labour_Temp),DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ
                           ENTRY(@n-14.2),AT(380,220,48,10),USE(tmp:LabourDifference),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Difference'),TIP('Labour Difference'),UPR
                           ENTRY(@s8),AT(436,204,48,10),USE(tmp:ExchangeJobs),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Exchange Difference'),TIP('Exchange Difference'),UPR,READONLY
                           ENTRY(@n-14.2),AT(488,236,48,10),USE(tmp:PartsPerJob),SKIP,HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Per Job'),TIP('Parts Per Job'),UPR,READONLY
                           PROMPT('% Removed Per Part'),AT(540,236),USE(?Prompt14:4),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(236,220,48,10),USE(tmp:LabourClaimed),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Claimed'),TIP('Labour Claimed'),UPR,READONLY
                           PROMPT('Parts Value (Less VAT)'),AT(68,236),USE(?Select_Global4:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n14.2),AT(172,236,48,10),USE(tmp:BatchPartsCost),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Claimed'),TIP('Parts Claimed'),UPR,READONLY
                           ENTRY(@n14.2),AT(304,236,48,10),USE(Parts_Temp),DISABLE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ
                           ENTRY(@n-14.2),AT(380,236,48,10),USE(tmp:PartsDifference),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Difference'),TIP('Parts Difference'),UPR
                           ENTRY(@s8),AT(436,220,48,10),USE(tmp:LabourJobs),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Labour Difference'),TIP('Labour Difference'),UPR,READONLY
                           ENTRY(@n14.2),AT(236,236,48,10),USE(tmp:PartsClaimed),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Parts Claimed'),TIP('Parts Claimed'),UPR,READONLY
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CountWarrantyParts      Routine
        Save_wpr_ID = Access:WARPARTS.SaveFile()
        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number  = glo:Pointer
        Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
        Loop
            If Access:WARPARTS.NEXT()
               Break
            End !If
            If wpr:Ref_Number  <> glo:Pointer      |
                Then Break.  ! End If
            tmp:WarrantyPartsCount += 1
        End !Loop
        Access:WARPARTS.RestoreFile(Save_wpr_ID)
DifferentCalculations       Routine
    tmp:ExchangeDifference  = tmp:ExchangeClaimed - Courier_Temp
    If tmp:ExchangeDifference <> 0
        ?tmp:ExchangePerJob{prop:Hide} = 0
        tmp:ExchangePerJob = Round((100 - (Courier_Temp * 100) / tmp:ExchangeClaimed),.01)

    Else !If tmp:ExchangeDifference > 0
        ?tmp:ExchangePerJob{prop:Hide} = 1
    End !If tmp:ExchangeDifference > 0

    tmp:LabourDifference    = tmp:LabourClaimed - Labour_Temp
    If tmp:LabourDifference <> 0
        ?tmp:LabourPerJob{prop:Hide} = 0
        tmp:LabourPerJob = Round((100 - (Labour_Temp * 100) / tmp:LabourClaimed),.01)
    Else !If tmp:LabourDifference > 0
        ?tmp:LabourPerJob{prop:Hide} = 1
    End !If tmp:LabourDifference > 0

    tmp:PartsDifference = tmp:PartsClaimed - Parts_Temp
    If tmp:PartsDifference <> 0
        ?tmp:PartsPerJob{prop:Hide} = 0
        tmp:PartsPerJob = Round((100 - (Parts_Temp * 100) / tmp:PartsClaimed),.01)
    Else !If tmp:PartsDifference > 0
        ?tmp:PartsPerJob{prop:Hide} = 1
    End !If tmp:PartsDifference > 0

    tmp:DifferenceTotal = tmp:ExchangeDifference + tmp:LabourDifference + tmp:PartsDifference
    tmp:PaidTotal   = Courier_Temp + Labour_Temp + Parts_Temp
    Value_Claimed_Temp  = tmp:ExchangeClaimed + tmp:LabourClaimed + tmp:PartsClaimed

    If tmp:PaidTotal < Value_Claimed_Temp
        tmp:TotalVariance = Round((tmp:PaidTotal / Value_Claimed_Temp) * 100,.01)
    Else !If tmp:PaidTotal > Value_Claimed_Temp
        tmp:TotalVariance = Round(100 - ((tmp:PaidTotal / Value_Claimed_Temp) * 100),.01)
    End !If tmp:PaidTotal > Value_Claimed_Temp
    If tmp:DifferenceTotal <> 0
        ?tmp:TotalVariance{prop:Hide} = 0
        ?tmp:TotalVariance:Prompt{prop:Hide} = 0
    Else !If tmp:DifferenceTotal <> 0
        ?tmp:TotalVariance{prop:Hide} = 1
        ?tmp:TotalVariance:Prompt{prop:Hide} = 1
    End !If tmp:DifferenceTotal <> 0
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020549'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Reconcile_Claim_Value')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Access:JOBSE.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  !Calculate Batch Total
  RecordCount# = 0
  Save_job_ID = Access:JOBS.SaveFile()
  Access:JOBS.ClearKey(job:EDI_Key)
  job:Manufacturer     = func:Manufacturer
  job:EDI              = 'YES'
  job:EDI_Batch_Number = func:BatchNumber
  Set(job:EDI_Key,job:EDI_Key)
  Loop
      If Access:JOBS.NEXT()
         Break
      End !If
      If job:Manufacturer     <> func:Manufacturer      |
      Or job:EDI              <> 'YES'      |
      Or job:EDI_Batch_Number <> func:BatchNumber      |
          Then Break.  ! End If
      RecordCount# += 1
      If RecordCount# = 1000
          RecordCount# = Records(JOBS)
          Break
      End !If RecordCount = 1000
  End !Loop
  Access:JOBS.RestoreFile(Save_job_ID)
  
  Prog.ProgressSetup(RecordCount#)
  
  Save_job_ID = Access:JOBS.SaveFile()
  Access:JOBS.ClearKey(job:EDI_Key)
  job:Manufacturer     = func:Manufacturer
  job:EDI              = 'YES'
  job:EDI_Batch_Number = func:BatchNumber
  Set(job:EDI_Key,job:EDI_Key)
  Loop
      If Access:JOBS.NEXT()
         Break
      End !If
      If job:Manufacturer     <> func:Manufacturer      |
      Or job:EDI              <> 'YES'      |
      Or job:EDI_Batch_Number <> func:BatchNumber      |
          Then Break.  ! End If
      If Prog.InsideLoop()
  
      End !If Prog.InsideLoop()
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
  
      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      !Do not reprice - L945 (DBH: 03-09-2003)
      !JobPricingRoutine
  
      If jobe:ConfirmClaimAdjustment
          tmp:BatchTotal += (jobe:ExchangeAdjustment + jobe:LabourAdjustment + jobe:PartsAdjustment)
          tmp:BatchExchangeCost += jobe:ExchangeAdjustment
          tmp:BatchLabourCost += jobe:LabourAdjustment
          tmp:BatchPartsCost += jobe:PartsAdjustment
      Else
          If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedAtRRC
              tmp:BatchTotal += jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
              tmp:BatchExchangeCost += job:Courier_Cost_Warranty
          Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedAtRRC
              tmp:BatchTotal += jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
          End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedAtRRC
          tmp:BatchLabourCost += jobe:ClaimValue
          tmp:BatchPartsCost += jobe:ClaimPartsCost
      End
  
  End !Loop
  Access:JOBS.RestoreFile(Save_job_ID)
  
  Prog.ProgressFinish()
  
  
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Reconcile_Claim_Value')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Reconcile_Claim_Value')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020549'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020549'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020549'&'0')
      ***
    OF ?Export_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export_Claims, Accepted)
      !Call the service history report
      !changed 22/11/02 unreferenced bug report
      
      !First I have to change the job:edi to something the
      !Pseudo service report will recognise
      Loop x# = 1 To Records(glo:queue)
          Get(glo:queue,x#)
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number = glo:pointer
          if access:jobs.fetch(job:ref_number_key) = Level:Benign
              SaveEDI" = job:EDI
              job:EDI = 'JJJ'
              access:jobs.update()
              !message('Outward - '&job:ref_number&' - ' &job:edi)
          END !if accessjobs
      END !loop
      
      !call the report
      ServiceHistoryReport2(Job:manufacturer,job:edi_batch_Number,1,0)
      
      !set the job edi back to what it was before
      Loop x# = 1 To Records(glo:queue)
          Get(glo:queue,x#)
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number = glo:pointer
          if access:jobs.fetch(job:ref_number_key) = Level:Benign
              job:EDI = SaveEDI"
              !message('Back - '&job:ref_number&' - ' &job:edi)
              access:jobs.update()
          END !if accessjobs
      END !loop
      
      
      
      !=====================================================!
      !!this is what it used to do - generate a war.csv file!
      !=====================================================!
      
      !Case MessageEx('Are you sure you want to export the claim you are about to Reconcile?','ServiceBase 2000',|
      !               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      !    Of 1 ! &Yes Button
      !        set(defaults)
      !        access:defaults.next()
      !        If def:exportpath <> ''
      !            glo:file_name = Clip(def:exportpath) & '\WARR.CSV'
      !        Else!If def:exportpath <> ''
      !            glo:file_name = 'C:\WARR.CSV'
      !        End!If def:exportpath <> ''
      !
      !        if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
      !                    file:keepdir + file:noerror + file:longname)
      !            !failed
      !        else!if not filedialog
      !            !found
      !            Remove(expgen)
      !            access:expgen.open()
      !            access:expgen.usefile()
      !
      !            Clear(gen:record)
      !            gen:line1   = 'Warranty Valuation'
      !            access:expgen.insert()
      !
      !            Clear(gen:record)
      !            gen:line1   = ''
      !            access:expgen.insert()
      !
      !            Clear(gen:record)
      !            gen:line1   = 'Job Number,Account Number,Model Number,E.S.N. / I.M.E.I.,M.S.N.,Charge Type,Repair Type,Courier Cost,Labour Cost,Parts Cost,Engineer,Date Booked,Date Completed,Batch Number'
      !            access:expgen.insert()
      !
      !            Clear(gen:record)
      !
      !            recordspercycle     = 25
      !            recordsprocessed    = 0
      !            percentprogress     = 0
      !            setcursor(cursor:wait)
      !            open(progresswindow)
      !            progress:thermometer    = 0
      !            ?progress:pcttext{prop:text} = '0% Completed'
      !
      !            recordstoprocess    = Records(glo:queue)
      !            value_claimed_temp = 0
      !            Loop x# = 1 To Records(glo:queue)
      !                Get(glo:queue,x#)
      !                Do GetNextRecord2
      !                access:jobs.clearkey(job:ref_number_key)
      !                job:ref_number = glo:pointer
      !                if access:jobs.fetch(job:ref_number_key) = Level:Benign
      !                    gen:line1   = CLip(job:ref_number)
      !                    gen:line1   = Clip(gen:line1) & ',' & CLip(job:account_number)
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(job:model_Number)
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(job:esn)
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(job:msn)
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(job:warranty_charge_type)
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(job:repair_type_warranty)
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:courier_cost_warranty,@n14.2))
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:labour_cost_warranty,@n14.2))
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(format(job:parts_cost_warranty,@n14.2))
      !                    gen:line1   = Clip(gen:line1) & ',' & CLip(job:engineer)
      !                    gen:line1   = Clip(gen:line1) & ',' & CLip(Format(job:date_booked,@d6))
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:date_completed,@d6))
      !                    gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:edi_batch_number))
      !                end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
      !                access:expgen.insert()
      !            End!Loop x# = 1 To Records(job_queue_global)
      !
      !            setcursor()
      !            close(progresswindow)
      !            access:expgen.close()
      !
      !        end!if not filedialog
      !
      !
      !
      !    Of 2 ! &No Button
      !End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export_Claims, Accepted)
    OF ?Valuate_Claim:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim:2, Accepted)
      Case Missive('This will auto valuate the tagged claims.'&|
        '<13,10>'&|
        '<13,10>Are you sure?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              Prog.ProgressSetup(Records(glo:Queue))
      
              labour_temp = 0
              parts_temp  = 0
              courier_temp     = 0
              tmp:WarrantyPartsCount = 0
      
              Loop x# = 1 To Records(glo:queue)
                  Get(glo:queue,x#)
      
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()
      
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = glo:pointer
                  if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                      
      
                      If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                          if jobe:ConfirmClaimAdjustment
                              labour_temp += jobe:LabourAdjustment
                          else
                              labour_temp += jobe:ClaimValue
                          end
                      Else !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                          if jobe:ConfirmClaimAdjustment
                              labour_temp += jobe:LabourAdjustment
                          else
                              labour_temp += job:labour_cost_warranty
                          end
                      End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
                      if jobe:ConfirmClaimAdjustment
                          parts_temp  += jobe:PartsAdjustment
                      else
                          parts_temp  += jobe:ClaimPartsCost
                      end
      
                      If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
                          If jobe:ConfirmClaimAdjustment
                              Courier_Temp    += jobe:ExchangeAdjustment
                          Else !If jobe:ConfirmAdjustment
                              Courier_Temp    += job:Courier_Cost_Warranty
                          End !If jobe:ConfirmAdjustment
                          
                      End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
      
                      Found# = 0
                      Save_wpr_ID = Access:WARPARTS.SaveFile()
                      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                      wpr:Ref_Number  = glo:Pointer
                      Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                      Loop
                          If Access:WARPARTS.NEXT()
                             Break
                          End !If
                          If wpr:Ref_Number  <> glo:Pointer      |
                              Then Break.  ! End If
                          tmp:WarrantyPartsCount += 1
                          Found# = 1
                      End !Loop
                      Access:WARPARTS.RestoreFile(Save_wpr_ID)
      
      
                  end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(job_queue_global)
      
              Prog.ProgressFinish()
      
              Display()
      
          Of 1 ! No Button
      End ! Case Missive
      
      Display()
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim:2, Accepted)
    OF ?tmp:ExchangeDifference
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeDifference, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeDifference, Accepted)
    OF ?LabourVariance
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LabourVariance, Accepted)
      Case Missive('Are you sure you want to apply the variance to the selected jobs?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              Prog.ProgressSetup(Records(glo:Queue))
      
              tmp:LabourRunningTotal = 0
              tmp:ExchangeRunningTotal = 0
              tmp:PartsRunningTotal = 0
              Labour_Temp = 0
      
              FinishedLabour# = 0
              FinishedParts# = 0
              FinishedExchange# = 0
              Loop x# = 1 To Records(glo:queue)
                  Get(glo:queue,x#)
      
                  If Prog.InsideLoop()
                      Break
                  End !If Prog.InsideLoop()
      
                  tmp:LabourAdjustment = 0
      
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = glo:pointer
                  if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
                          
                          If FinishedLabour# = 0
                              tmp:LabourAdjustment = Round(jobe:ClaimValue * (tmp:LabourPerJob/100),.01)
      
                              tmp:LabourRunningTotal += tmp:LabourAdjustment
      
                              If Abs(tmp:LabourRunningTotal) > Abs(tmp:LabourDifference)
      
                                  tmp:LabourAdjustment -= tmp:LabourRunningTotal - tmp:LabourDifference
      
                                  FinishedLabour# = 1
                              End !If tmp:RunningTotal > tmp:ExchangeDifference
      
                              IF Abs(tmp:LabourRunningTotal) = Abs(tmp:LabourDifference)
                                  FinishedLabour# = 1
                              End !IF tmp:RunningTotal = tmp:ExchangeDifference
      
                              jobe:LabourAdjustment = jobe:ClaimValue - tmp:LabourAdjustment
      
                              Labour_Temp += jobe:LabourAdjustment
      
                              jobe:ConfirmClaimAdjustment = 1
      
                          End !If Finished# = 0
      
                          If FinishedExchange# = 0
                              If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
                                  tmp:ExchangeAdjustment = Round(job:Courier_Cost_Warranty * (tmp:ExchangePerJob/100),.01)
      
                                  tmp:ExchangeRunningTotal += tmp:ExchangeAdjustment
      
                                  If Abs(tmp:ExchangeRunningTotal) > Abs(tmp:ExchangeDifference)
      
                                      tmp:ExchangeAdjustment -= tmp:ExchangeRunningTotal - tmp:ExchangeDifference
      
                                      FinishedExchange# = 1
                                  End !If tmp:RunningTotal > tmp:ExchangeDifference
      
                                  IF Abs(tmp:ExchangeRunningTotal) = Abs(tmp:ExchangeDifference)
                                      FinishedExchange# = 1
                                  End !IF tmp:RunningTotal = tmp:ExchangeDifference
      
                                  jobe:ExchangeAdjustment = job:Courier_Cost_Warranty - tmp:ExchangeAdjustment
      
                                  Courier_Temp += jobe:ExchangeAdjustment
      
                                  jobe:ConfirmClaimAdjustment = 1
                              End !If job:Exchange_Unit_Number <> 0 And jobe:ExchangedATRRC
                          End !If FinishedExchage# = 0
      
                          If FinishedParts# = 0
                              Save_wpr_ID = Access:WARPARTS.SaveFile()
                              Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                              wpr:Ref_Number  = job:Ref_Number
                              Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                              Loop
                                  If Access:WARPARTS.NEXT()
                                     Break
                                  End !If
                                  If wpr:Ref_Number  <> job:Ref_Number      |
                                      Then Break.  ! End If
                                  tmp:PartsAdjustment = Round(wpr:Purchase_Cost * (tmp:PartsPerJob/100),.01)
                                  tmp:PartsRunningTotal += tmp:PartsAdjustment
      
                                  If Abs(tmp:PartsRunningTotal) > Abs(tmp:PartsDifference)
                                      tmp:PartsAdjustment -= tmp:PartsRunningTotal - tmp:PartsDifference
                                      FinishedParts# = 1
                                  End !If tmp:PartsRunningTotal > tmp:PartsDifference
      
                                  If Abs(tmp:PartsRunningTotal) = Abs(tmp:PartsDifference)
                                      FinishedParts# = 1
                                  End !If tmp:PartsRunningTotal = tmp:PartsDifference
      
                                  wpr:CostAdjustment = wpr:Purchase_Cost - tmp:PartsAdjustment
                                  ACcess:WARPARTS.Update()
      
                              End !Loop
                              Access:WARPARTS.RestoreFile(Save_wpr_ID)
                          End !If FinishedParts# = 0
                          jobe:ConfirmClaimAdjustment = 1
                          Access:JOBSE.Update()
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                  end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(job_queue_global)
      
              Prog.ProgressFinish()
      
              Display()
      
          Of 1 ! No Button
      End ! Case Missive
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LabourVariance, Accepted)
    OF ?Valuate_Claim
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim, Accepted)
      Case Missive('Are you sure you want to valuate the tagged claims?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              !Changes made to stop repricing - L945 (DBH: 03-09-2003)
              value_claimed_temp  = 0
              tmp:ExchangeCLaimed = 0
              tmp:LabourClaimed   = 0
              tmp:PartsClaimed    = 0
              tmp:ExchangeJobs     = 0
              tmp:PartsJobs       = 0
              tmp:LabourJobs      = 0
      
              Prog.ProgressSetup(Records(glo:Queue))
      
              value_claimed_temp = 0
              Loop x# = 1 To Records(glo:queue)
                  Get(glo:queue,x#)
                  If Prog.InsideLoop()
                      !Break
                  End !If Prog.InsideLoop()
      
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = glo:pointer
                  if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      !Do not reprice - L945 (DBH: 03-09-2003)
      !                If rep# = 2
      !                    JobPricingRoutine
      !                    Access:JOBS.Update()
      !                    Access:JOBSE.Update()
      !                End!If rep# = 2
      
                      tmp:LabourJobs += 1
      
                      If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
                          tmp:ExchangeJobs += 1
                      End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedATRRC
      
                      If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                          If jobe:ConfirmClaimAdjustment
                              value_claimed_temp += (jobe:ExchangeAdjustment + jobe:LabourAdjustment + jobe:PartsAdjustment)
                              tmp:ExchangeCLaimed += jobe:ExchangeAdjustment
                              tmp:LabourClaimed += jobe:LabourAdjustment
                              tmp:PartsClaimed += jobe:PartsAdjustment
                          Else
                              If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedAtRRC
                                  Value_Claimed_Temp += jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
                                  tmp:ExchangeClaimed += job:Courier_Cost_Warranty
                              Else !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedAtRRC
                                  Value_Claimed_Temp += jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
                              End !If job:Exchange_Unit_Number <> 0 And ~jobe:ExchangedAtRRC
                              tmp:LabourClaimed += jobe:ClaimValue
                              tmp:PartsClaimed += jobe:ClaimPartsCost
                          End
                      Else !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                          if jobe:ConfirmClaimAdjustment
                              value_claimed_temp += (jobe:ExchangeAdjustment + jobe:LabourAdjustment + jobe:PartsAdjustment)
                          else
                              value_claimed_temp += (job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty)
                          end
                      End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
                      Found# = 0
                      Save_wpr_ID = Access:WARPARTS.SaveFile()
                      Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                      wpr:Ref_Number  = job:Ref_Number
                      Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                      Loop
                          If Access:WARPARTS.NEXT()
                             Break
                          End !If
                          If wpr:Ref_Number  <> job:Ref_Number      |
                              Then Break.  ! End If
                          Found# = 1
                          Break
                      End !Loop
                      Access:WARPARTS.RestoreFile(Save_wpr_ID)
      
                      If Found#
                          tmp:PartsJobs += 1
                      End !If Found#
                  end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(job_queue_global)
      
              Prog.ProgressFinish()
              Display(?value_claimed_temp)
      
              If tmp:ExchangeClaimed <> 0
                  ?Courier_Temp{prop:Disable} = 0
              Else !tmp:ExchangeClaimed
                  Courier_Temp = 0
                  ?Courier_Temp{prop:Disable} = 1
              End !tmp:ExchangeClaimed
      
              If tmp:PartsClaimed <> 0
                  ?Parts_Temp{prop:Disable} = 0
              Else !tmp:PartsClaimed <> 0
                  Parts_Temp = 0
                  ?Parts_Temp{prop:Disable} = 1
              End !tmp:PartsClaimed <> 0
      
              If tmp:LabourClaimed <> 0
                  ?Labour_Temp{prop:Disable} = 0
              Else !tmp:LabourClaimed <> 0
                  Labour_Temp = 0
                  ?Labour_Temp{prop:Disable} = 1
              End !tmp:LabourClaimed <> 0
              Display()
      
          Of 1 ! No Button
      End ! Case Missive
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim, Accepted)
    OF ?courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
    OF ?tmp:ExchangeClaimed
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeClaimed, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeClaimed, Accepted)
    OF ?Labour_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Labour_Temp, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Labour_Temp, Accepted)
    OF ?tmp:LabourDifference
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LabourDifference, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LabourDifference, Accepted)
    OF ?tmp:LabourClaimed
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LabourClaimed, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LabourClaimed, Accepted)
    OF ?Parts_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Parts_Temp, Accepted)
      tmp:WarrantyPartsCount = 0
      Loop x# = 1 To Records(glo:Queue)
          Get(glo:Queue,x#)
          Save_wpr_ID = Access:WARPARTS.SaveFile()
          Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
          wpr:Ref_Number  = glo:Pointer
          Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
          Loop
              If Access:WARPARTS.NEXT()
                 Break
              End !If
              If wpr:Ref_Number  <> glo:Pointer      |
                  Then Break.  ! End If
              tmp:WarrantyPartsCount += 1
          End !Loop
          Access:WARPARTS.RestoreFile(Save_wpr_ID)
      End !x# = 1 To Records(glo:Queue)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Parts_Temp, Accepted)
    OF ?tmp:PartsDifference
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PartsDifference, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PartsDifference, Accepted)
    OF ?tmp:PartsClaimed
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PartsClaimed, Accepted)
      Do DifferentCalculations
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PartsClaimed, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If reference_number_temp = ''
          Case Missive('You must insert a reference number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?reference_number_temp)
      Else!If reference_number_temp = ''
          Case Missive('You are about to reconcile the selected claim.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
              
                  f_ref_number  = reference_number_temp
                  f_value_claimed = value_claimed_temp
                  f_courier       = courier_temp
                  f_labour        = labour_temp
                  f_parts         = parts_temp
                  f_total_jobs    = total_jobs_temp
                  Post(event:Closewindow)
              Of 1 ! No Button
          End ! Case Missive
      End!If reference_number_temp = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      total_Jobs_temp = Records(glo:queue)
      Display(?total_Jobs_temp)
      Select(?Reference_Number_Temp)
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
