

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01006.INC'),ONCE        !Local module procedure declarations
                     END


CreateSecondYearInvoice PROCEDURE                     !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:tag              STRING(1)
tmp:CreateInvoice    BYTE(0)
save_job_id          USHORT,AUTO
save_man_id          USHORT,AUTO
save_ebt_id          USHORT,AUTO
tmp:CountJobs        LONG
tmp:CourierClaimed   REAL
tmp:LabourClaimed    REAL
tmp:PartsClaimed     REAL
tmp:InvoiceNumber    LONG
pos                  STRING(255)
BRW6::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:SecondYearAccount)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:SecondYearAccount  LIKE(tra:SecondYearAccount)    !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('8'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Create Second Year Warranty Invoice'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Second Year Warranty Accounts'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(228,102,204,224),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@0L(2)|M~Seco' &|
   'nd Year Warranty Account~@n1@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(250,159,50,13),USE(?DASREVTAG)
                           BUTTON('sho&W tags'),AT(242,183,70,13),USE(?DASSHOWTAG)
                           BUTTON,AT(448,142),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,172),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,204),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('Create Invoice'),AT(440,266),USE(tmp:CreateInvoice),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Create Invoice'),TIP('Create Invoice'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::7:QUEUE = glo:Queue
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020542'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CreateSecondYearInvoice')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EDIBATCH.Open
  Relate:VATCODE.Open
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','CreateSecondYearInvoice')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,tra:Account_Number_Key)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,tra:Account_Number,1,BRW6)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:tag,BRW6.Q.tmp:tag)
  BRW6.AddField(tra:Account_Number,BRW6.Q.tra:Account_Number)
  BRW6.AddField(tra:Company_Name,BRW6.Q.tra:Company_Name)
  BRW6.AddField(tra:SecondYearAccount,BRW6.Q.tra:SecondYearAccount)
  BRW6.AddField(tra:RecordNumber,BRW6.Q.tra:RecordNumber)
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EDIBATCH.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','CreateSecondYearInvoice')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020542'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020542'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020542'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      !Create an Invoice
      If tmp:CreateInvoice
          Case Missive('Are you sure you want to create a 2nd year warranty invoice for the selected accounts?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
                  tmp:InvoiceNumber = 0
                  If Access:INVOICE.PrimeRecord() = Level:Benign
                      If Access:INVOICE.TryInsert() = Level:Benign
                          !Insert Successful
                          tmp:InvoiceNumber = inv:Invoice_Number
                      Else !If Access:INVOICE.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:INVOICE.CancelAutoInc()
                      End !If Access:INVOICE.TryInsert() = Level:Benign
                  End !If Access:INVOICE.PrimeRecord() = Level:Benign
      
                  If tmp:InvoiceNumber <> 0
                      tmp:CountJobs = 0
      
                      Prog.ProgressSetup(Records(glo:Queue))
      
                      Loop x# = 1 To Records(glo:Queue)
                          Get(glo:Queue,x#)
      
                          If Prog.InsideLoop()
                              Case Missive('This routine cannot be cancelled.','ServiceBase 3g',|
                                             'mstop.jpg','/OK') 
                                  Of 1 ! OK Button
                              End ! Case Missive
                          End !If Prog.InsideLoop()
      
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = glo:Pointer
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Found
      
                          Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Error
                          End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
                          access:vatcode.clearkey(vat:vat_code_key)
                          vat:vat_code = tra:labour_vat_code
                          if access:vatcode.tryfetch(vat:vat_code_key)
                              labour$ = vat:vat_rate
                          end
                          access:vatcode.clearkey(vat:vat_code_key)
                          vat:vat_code = tra:parts_vat_code
                          if access:vatcode.tryfetch(vat:vat_code_key)
                              parts$ = vat:vat_rate
                          end
      
                          !Lets find all the manufacturers that have this account
                          !as their second year account.
                          Save_man_ID = Access:MANUFACT.SaveFile()
                          Set(man:Manufacturer_Key)
                          Loop
                              If Access:MANUFACT.NEXT()
                                 Break
                              End !If
                              If man:SecondYrTradeAccount = tra:Account_Number 
                                  !Lets find all Second Year Jobs for this Manufacturer
                                  Save_ebt_ID = Access:EDIBATCH.SaveFile()
                                  Access:EDIBATCH.ClearKey(ebt:Batch_Number_Key)
                                  ebt:Manufacturer = man:Manufacturer
                                  Set(ebt:Batch_Number_Key,ebt:Batch_Number_Key)
                                  Loop
                                      If Access:EDIBATCH.NEXT()
                                         Break
                                      End !If
                                      If ebt:Manufacturer <> man:Manufacturer      |
                                          Then Break.  ! End If
                                      If ~ebt:SecondYearWarranty
                                          Cycle
                                      End !If ~ebt:SecondYearWarranty
                                      If ebt:ApprovedDate = ''
                                          Cycle                    
                                      End !If ~ebt:ApprovedDate
      
                                      !Ok, have found a 2nd Year batch that has been approved.
                                      !Now, are their any jobs.
      
                                      Save_job_ID = Access:JOBS.SaveFile()
                                      Access:JOBS.ClearKey(job:EDI_Key)
                                      job:Manufacturer     = man:Manufacturer
                                      job:EDI              = 'YES'
                                      job:EDI_Batch_Number = ebt:Batch_Number
                                      Set(job:EDI_Key,job:EDI_Key)
                                      Loop
                                          If Access:JOBS.NEXT()
                                             Break
                                          End !If
                                          If job:Manufacturer     <> man:Manufacturer      |
                                          Or job:EDI              <> 'YES'      |
                                          Or job:EDI_Batch_Number <> ebt:Batch_Number      |
                                              Then Break.  ! End If
                                          !Found jobs on batch. Let's invoice them
                                          pos = Position(job:EDI_Key)
                                          job:edi = 'FIN'
                                          JOB:Invoice_Number_Warranty = inv:invoice_number
                                          JOB:WInvoice_Courier_Cost = JOB:Courier_Cost_Warranty
                                          JOB:WInvoice_Labour_Cost = JOB:Labour_Cost_Warranty
                                          JOB:WInvoice_Parts_Cost = JOB:Parts_Cost_Warranty
                                          JOB:WInvoice_Sub_Total = JOB:Sub_Total_Warranty
                                          JOB:Invoice_Date_Warranty = Today()
                                          job:paid_warranty = 'YES'
                                          If job:chargeable_job <> 'YES' Or (job:chargeable_job = 'YES' AND job:invoice_number <> '')
                                              If job:consignment_number <> ''
                                                  Getstatus(910,0,'JOB')
                                              Else!If job:consignment_number <> ''
                                                  If job:loan_unit_number <> ''
                                                      Getstatus(811,0,'JOB')
                                                  Else!If job:loan_unit_number <> ''
                                                      Getstatus(810,0,'JOB')
                                                  End!If job:loan_unit_number <> ''
                                              End!If job:consignment_number <> ''
                                          End
                                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                                          jobe:RefNumber    = job:Ref_Number
                                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                          !Found
                                              jobe:InvoiceClaimValue    = jobe:ClaimValue
                                              jobe:InvoiceHandlingFee   = jobe:HandlingFee
                                              jobe:InvoiceExchangeRate  = jobe:ExchangeRate
                                              jobe:InvRRCWLabourCost    = jobe:RRCWLabourCost
                                              jobe:InvRRCWPartsCost     = jobe:RRCWPartsCost
                                              jobe:InvRRCWPartsSale     = jobe:RRCWPartsSale
                                              jobe:InvRRCWSubTotal      = jobe:RRCWSubTotal
                                              jobe:InvClaimPartsCost    = jobe:ClaimPartsCost
                                              jobe:WarrantyClaimStatus  = 'RECONCILED'
                                              jobe:WarrantyStatusDate   = Today()
                                              Access:JOBSE.Update()
                                              tmp:CourierClaimed         += job:Courier_Cost_Warranty
                                              tmp:PartsClaimed           += jobe:ClaimPartsCost
                                              tmp:LabourClaimed          += jobe:ClaimValue
                                          Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                          !Error
                                          End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                                          access:jobs.update()
                                          Reset(job:EDI_Key,pos)
                                          If AddToAudit(job:Ref_Number,'JOB','2ND YEAR WARRANTY CLAIM RECONCILED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number))
      
                                          End ! If AddToAudit(job:Ref_Number,'JOB','2ND YEAR WARRANTY CLAIM RECONCILED','INVOICE NUMBER: ' & Clip(inv:Invoice_Number))
                                          tmp:CountJobs += 1
                                      End !Loop
                                      Access:JOBS.RestoreFile(Save_job_ID)
                                  End !Loop
                                  Access:EDIBATCH.RestoreFile(Save_ebt_ID)
                              End !If man:SecondYrTradeAccount = tra:Account_Number 
      
                          End !Loop
      
                          Prog.ProgressFinish()
      
                          Access:MANUFACT.RestoreFile(Save_man_ID)
                          Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                          inv:Invoice_Number  = tmp:InvoiceNumber
                          If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                              !Found
                              If tmp:CountJobs
                                  inv:date_created       = Today()
                                  inv:account_number     = tra:Account_Number
                                  inv:accounttype        = 'MAI'
                                  inv:total              = tmp:PartsClaimed + tmp:LabourClaimed + tmp:CourierClaimed
                                  inv:vat_rate_labour    = labour$
                                  inv:vat_rate_parts     = parts$
                                  inv:vat_rate_retail    = labour$
                                  inv:vat_number         = def:vat_number
                                  inv:invoice_vat_number = ''
                                  inv:currency           = ''
                                  !inv:batch_number       = batch_number_temp
                                  !inv:manufacturer       = manufacturer_temp
                                  !inv:claim_reference    = claim_reference_temp
                                  inv:total_claimed      = tmp:PartsClaimed + tmp:LabourClaimed + tmp:CourierClaimed
                                  inv:courier_paid       = tmp:CourierClaimed
                                  inv:labour_paid        = tmp:LabourClaimed
                                  inv:parts_paid         = tmp:PartsClaimed
                                  inv:reconciled_date    = Today()
                                  inv:invoice_type       = 'WAR'
                                  inv:jobs_count         = tmp:CountJobs
                                  Access:INVOICE.Update()
                                  Case Missive('Invoice created.','ServiceBase 3g',|
                                                 'midea.jpg','/OK') 
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              Else !If tmp:CountJobs
                                  !If there are no jobs to update, then you don't need the
                                  !invoice anymore
                                  Relate:Invoice.Delete(0)
                                  Case Missive('There are no jobs to invoice.','ServiceBase 3g',|
                                                 'mstop.jpg','/OK') 
                                      Of 1 ! OK Button
                                  End ! Case Missive
                              End !If tmp:CountJobs
                          Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                              !Error
                          End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                          
                      End !x# = 1 To Records(glo:Queue)
                  End !tmp:InvoiceNumber <> 0            
      
              Of 1 ! No Button
          End ! Case Missive
      End !tmp:CreateInvoice
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  If ~tra:SecondYearAccount
      Return Record:Filtered
  End !tra:SecondYearAccount
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
Data_Export_Wizard PROCEDURE
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'Data_Export_Wizard')
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled
EDIClaimsKey PROCEDURE                                !Generated from procedure template - Window

window               WINDOW('Ready For Warranty Claim Key'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,246),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('"M" Column Colour:'),AT(228,138),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(228,156,12,10),USE(?Panel6),FILL(COLOR:Lime)
                       PROMPT('- Marked Green'),AT(244,156),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(228,278,12,10),USE(?Panel6:4),FILL(08080FFH)
                       PROMPT('- POP Confirmed (when applicable)'),AT(244,278),USE(?Prompt4:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('- Marked Blue'),AT(244,172),USE(?Prompt4:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('- Marked Red'),AT(244,188),USE(?Prompt4:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(228,188,12,10),USE(?Panel6:3),FILL(COLOR:Red)
                       PROMPT('"O" Column colour:'),AT(228,210),USE(?Prompt6),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Line Colour:'),AT(228,264),USE(?Prompt6:2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(228,226,12,10),USE(?Panel8),FILL(COLOR:Aqua)
                       PROMPT('- Job Has Been "Ready For Claiming" For Over 48 HOURS'),AT(244,226),USE(?Prompt7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(228,244,12,10),USE(?Panel8:2),FILL(COLOR:Red)
                       PROMPT('- Job Has Been "Ready For Claiming" For Over 72 HOURS'),AT(244,244),USE(?Prompt7:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PANEL,AT(228,172,12,10),USE(?Panel6:2),FILL(COLOR:Blue)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Ready For Warranty Claim Key'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020645'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EDIClaimsKey')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','EDIClaimsKey')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','EDIClaimsKey')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020645'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020645'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020645'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
VettingOption PROCEDURE (f:Option,f:Reason)           !Generated from procedure template - Window

tmp:Reason           STRING(255)
tmp:Return           BYTE(0)
tmp:VettingReason    STRING(100)
FDB9::View:FileDrop  VIEW(VETTREAS)
                       PROJECT(vet:VettingReason)
                       PROJECT(vet:RecordNumber)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?tmp:VettingReason
vet:VettingReason      LIKE(vet:VettingReason)        !List box control field - type derived from field
vet:RecordNumber       LIKE(vet:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(168,88,344,36),USE(?GroupTip),BOXED,TRN
                       END
                       PANEL,AT(164,132,352,196),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Select Vetting Option'),AT(279,162),USE(?Prompt3),FONT(,12,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(476,92,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Vetting Options'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(184,200),USE(?Button:Pass),TRN,FLAT,ICON('passp.jpg')
                       BUTTON,AT(304,200),USE(?Button:Query),TRN,FLAT,ICON('queryb.jpg')
                       BUTTON,AT(427,200),USE(?Button:Fail),TRN,FLAT,ICON('fail10p.jpg')
                       GROUP,AT(248,230,176,92),USE(?Group:Reason),HIDE
                         PROMPT('Select Failure Reason'),AT(248,230),USE(?tmp:VettingReason:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         LIST,AT(248,240,176,10),USE(tmp:VettingReason),HIDE,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),FORMAT('400L(2)|M~Vetting Reason~@s100@'),DROP(10),FROM(Queue:FileDrop)
                         PROMPT('Enter Reason'),AT(248,252),USE(?tmp:Reason:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         TEXT,AT(248,262,177,60),USE(tmp:Reason),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Enter Reason'),TIP('Enter Reason'),REQ,UPR
                       END
                       BUTTON,AT(376,332),USE(?OK),TRN,FLAT,HIDE,ICON('okp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020671'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('VettingOption')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:VETTREAS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','VettingOption')
  ?tmp:VettingReason{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDB9.Init(?tmp:VettingReason,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:VETTREAS,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(vet:VettingReasonKey)
  FDB9.AddField(vet:VettingReason,FDB9.Q.vet:VettingReason)
  FDB9.AddField(vet:RecordNumber,FDB9.Q.vet:RecordNumber)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:VETTREAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','VettingOption')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Return = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020671'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020671'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020671'&'0')
      ***
    OF ?Button:Pass
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Pass, Accepted)
      f:Option = 1
      tmp:Return = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Pass, Accepted)
    OF ?Button:Query
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Query, Accepted)
      ! Inserting (DBH 25/06/2007) # 9061 - Don't show drop down reason for query
      ?tmp:VettingReason{prop:Hide} = 1
      ?tmp:VettingReason:Prompt{prop:Hide} = 1
      ! End (DBH 25/06/2007) #9061
      ?Group:Reason{prop:Hide} = 0
      ?OK{prop:Hide} = 0
      ?tmp:Reason:Prompt{prop:Text} = 'Enter Query Reason'
      f:Option = 2
      Select(?tmp:Reason)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Query, Accepted)
    OF ?Button:Fail
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Fail, Accepted)
      ! Inserting (DBH 25/06/2007) # 9061 - Show drop down for vetting failure reason
      ?tmp:VettingReason{prop:Hide} = 0
      ?tmp:VettingReason:Prompt{prop:Hide} = 0
      ! End (DBH 25/06/2007) #9061
      ?Group:Reason{prop:Hide} = 0
      ?OK{prop:Hide} = 0
      f:Option = 3
      ?tmp:Reason:Prompt{prop:Text} = 'Enter Free Text Failure Reason'
      Select(?tmp:Reason)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:Fail, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ! Inserting (DBH 25/06/2007) # 9061 - Add the drop down selection to the front of the reason
      If tmp:VettingReason{prop:Hide} = 0 And Clip(tmp:VettingReason) <> ''
          tmp:Reason = Clip(tmp:VettingReason) & '<13,10>' & Clip(tmp:Reason)
      End ! If tmp:VettingReasons{prop:Hide} = 0 And Clip(tmp:VettingReason) <> ''
      ! End (DBH 25/06/2007) #9061
      If Clip(tmp:Reason) = ''
          Select(?tmp:Reason)
          Cycle
      End ! If Clip(tmp:Reason) = ''
      f:Reason = tmp:Reason
      tmp:Return = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:VettingReason{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:VettingReason{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
MoveBatches PROCEDURE                                 !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_edibatch_id     USHORT,AUTO
save_jobswarr_id     USHORT,AUTO
tmp:Manufacturer     STRING(30)
tmp:FirstSecondYear  BYTE(0)
tmp:Tag              STRING(1)
tmp:DestinationBatchNumber LONG
BRW10::View:Browse   VIEW(EDIBATCH)
                       PROJECT(ebt:Batch_Number)
                       PROJECT(ebt:ApprovedDate)
                       PROJECT(ebt:Manufacturer)
                       PROJECT(ebt:SecondYearWarranty)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
ebt:Batch_Number       LIKE(ebt:Batch_Number)         !List box control field - type derived from field
ebt:ApprovedDate       LIKE(ebt:ApprovedDate)         !List box control field - type derived from field
ebt:Manufacturer       LIKE(ebt:Manufacturer)         !Browse key field - type derived from field
ebt:SecondYearWarranty LIKE(ebt:SecondYearWarranty)   !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK13::ebt:Manufacturer    LIKE(ebt:Manufacturer)
HK13::ebt:SecondYearWarranty LIKE(ebt:SecondYearWarranty)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDB12::View:FileDrop VIEW(EDIBATCH)
                       PROJECT(ebt:Batch_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?tmp:DestinationBatchNumber
ebt:Batch_Number       LIKE(ebt:Batch_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(168,88,344,36),USE(?GroupTip),BOXED,TRN
                       END
                       PANEL,AT(164,132,352,196),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Select Manufacturer'),AT(188,140),USE(?tmp:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(276,140,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                       BUTTON,AT(404,136),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       OPTION('Warranty Type'),AT(275,152,124,24),USE(tmp:FirstSecondYear),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('First Year'),AT(279,162),USE(?tmp:FirstSecondYear:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('Second Year'),AT(335,162),USE(?tmp:FirstSecondYear:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                       END
                       ENTRY(@p<<<<<#pB),AT(172,198,48,10),USE(ebt:Batch_Number),SKIP
                       GROUP('Tag The Batches You Want To Move'),AT(168,186,172,140),USE(?Group2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       GROUP('Select The Destination Batch'),AT(344,186,168,140),USE(?Group3),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       LIST,AT(172,210,96,112),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@34R(2)|M~Batch No~@p<<<<<<<<<<#pB@40R(2)|M~Approved~@d6b@'),FROM(Queue:Browse)
                       BUTTON('&Rev tags'),AT(183,231,1,1),USE(?DASREVTAG),HIDE
                       BUTTON,AT(268,236),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON('sho&W tags'),AT(179,244,1,1),USE(?DASSHOWTAG),HIDE
                       LIST,AT(420,232,84,10),USE(tmp:DestinationBatchNumber),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('24R(2)|M@p<<<<<<<<<<#pB@'),DROP(10),FROM(Queue:FileDrop)
                       PROMPT('Batch Number'),AT(360,232),USE(?Prompt5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(268,266),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON,AT(396,258),USE(?Button:MoveClaims),TRN,FLAT,ICON('movclamp.jpg')
                       BUTTON,AT(268,296),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       BUTTON,AT(476,92,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Move Batches'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:StartTime          Time,Auto
Prog:TimeSoFar          Time
Prog:TimePerRecord      Time
Prog:TotalTime          Time
Prog:TimeRemaining      String(30)
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW10                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator EntryLocatorClass                !Default Locator
FDB12                CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW10.UpdateBuffer
   glo:Queue.Pointer = ebt:Batch_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = ebt:Batch_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = ebt:Batch_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::11:QUEUE = glo:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(glo:Queue)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = ebt:Batch_Number
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = ebt:Batch_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
MoveClaims      Routine
Data
local:Batches       String(150)
BatchQueue          Queue,Pre()
local:BatchNumber       Long()
local:TotalJobs         Long()
                    End ! BatchQueue          Queue
local:StartTime     Time()
local:TimeSoFar     Time()
local:TimePerRecord Time()
local:TimeRemaining String(30)
local:TotalTime     Time()
Code
    Loop x# = 1 To Records(Glo:Queue)
        Get(Glo:Queue,x#)
        local:Batches = Clip(local:Batches) & ', ' & Clip(glo:Pointer)
    End ! Loop x# = 1 To Records(Glo:Queue)
    local:Batches = Sub(local:Batches,3,150)

    Case Missive('Are you sure you want to move batches:'&|
      '||' & Clip(local:Batches) & |
      '|To batch number: ' & CLip(tmp:DestinationBatchNumber),'ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes') 
        Of 2 ! Yes Button
        Of 1 ! No Button
            Exit
    End ! Case Missive

    Stream(JOBS)
    Stream(JOBSWARR)
    Stream(AUDIT)

    Free(BatchQueue)
    SetCursor(cursor:Wait)
    Loop x# = 1 To Records(glo:Queue)
        Get(glo:Queue,x#)
        Save_EDIBATCH_ID = Access:EDIBATCH.SaveFile()
        Access:EDIBATCH.ClearKey(ebt:BatchFirstSecondKey)
        ebt:Manufacturer = tmp:Manufacturer
        ebt:SecondYearWarranty = tmp:FirstSecondYear
        ebt:Batch_Number = glo:Pointer
        If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
            !Found
        Else ! If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign

        Count# = 0
        Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
        Access:JOBSWARR.Clearkey(jow:StatusManFirstKey)
        jow:Status = 'YES'
        jow:Manufacturer = tmp:Manufacturer
!        jow:BatchNumber = glo:Pointer
        jow:FirstSecondYear = tmp:FirstSecondYear
        Set(jow:StatusManFirstKey,jow:StatusManFirstKey)
        Loop ! Begin Loop
            If Access:JOBSWARR.Next()
                Break
            End ! If Access:JOBSWARR.Next()
            If jow:Status <> 'YES'
                Break
            End ! If jow:Status <> 'YES'
            If jow:Manufacturer <> tmp:Manufacturer
                Break
            End ! If jow:Manufacturer <> tmp:Manufacturer
!            If jow:BatchNumber <> glo:Pointer
!                Break
!            End ! If jow:BatchNumber <> glo:Pointer
            If jow:FirstSecondYear <> tmp:FirstSecondYear
                Break
            End ! If jow:FirstSecondYear <> tmp:FirstSecondYear
            Count# += 1
        End ! Loop
        Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)

        If Count# > 0
            local:BatchNumber = glo:Pointer
            local:TotalJobs = Count#
            Add(BatchQueue)
        End ! If Count# > 0

        Access:EDIBATCH.RestoreFile(Save_EDIBATCH_ID)
    End ! Loop x# = 1 To Records(glo:Queue)

    SetCursor()

    Loop x# = 1 To Records(BatchQueue)
        Get(BatchQueue,x#)
        Do Prog:ProgressSetup
        Prog:TotalRecords = local:TotalJobs
        Prog:ShowPercentage = 1 !Show Percentage Figure

        Access:EDIBATCH.ClearKey(ebt:BatchFirstSecondKey)
        ebt:Manufacturer = tmp:Manufacturer
        ebt:SecondYearWarranty = tmp:FirstSecondYear
        ebt:Batch_Number = local:BatchNumber
        If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
            !Found
        Else ! If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign

        Access:JOBSWARR.Clearkey(jow:StatusManFirstKey)
        jow:Status = 'YES'
        jow:Manufacturer = tmp:Manufacturer
!        jow:BatchNumber = local:BatchNumber
        jow:FirstSecondYear = tmp:FirstSecondYear
        Set(jow:StatusManFirstKey,jow:StatusManFirstKey)

        Accept
            Case Event()
                Of Event:Timer
                    Loop 25 Times
                        !Inside Loop
                        If Access:JOBSWARR.Next()
                            Prog:Exit = 1
                            Break
                        End ! If Access:JOBSWARR.Next()
                        If jow:Status <> 'YES'
                            Prog:Exit = 1
                            Break
                        End ! If jow:Status <> 'YES'
                        If jow:Manufacturer <> tmp:Manufacturer
                            Prog:Exit = 1
                            Break
                        End ! If jow:Manufacturer <> tmp:Manufacturer
!                        If jow:BatchNumber <> local:BatchNumber
!                            Prog:Exit = 1
!                            Break
!                        End ! If jow:BatchNumber <> glo:Pointer
                        If jow:FirstSecondYear <> tmp:FirstSecondYear
                            Prog:Exit = 1
                            Break
                        End ! If jow:FirstSecondYear <> tmp:FirstSecondYear

                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = jow:RefNumber
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                            If JobInUse(job:Ref_Number,1)
                                Cycle
                            End ! If JobInUse(job:Ref_Number,1)
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                            Cycle
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                        ! Don't move job if it's already there (DBH: 27/02/2007)
                        If job:EDI_Batch_Number = tmp:DestinationBatchNumber
                            Cycle
                        End ! If job:EDI_Batch_Number = tmp:DestinationBatchNumber

                        job:EDI_Batch_Number = tmp:DestinationBatchNumber
                        If Access:JOBS.Update() = Level:Benign
                            !jow:BatchNumber = job:EDI_Batch_Number
                            If Access:JOBSWARR.Update() = Level:Benign
                                If AddToAudit(job:Ref_Number,'JOB','JOB MOVED TO EDI BATCH NUMBER: ' & tmp:DestinationBatchNumber,'OLD BATCH NUMBER: ' & Clip(local:BatchNumber))
                                End ! If AddToAudit(job:Ref_Number,'JOB','JOB MOVED TO EDI BATCH NUMBER: ' & tmp:NewBatchNumber,'OLD BATCH NUMBER: ' & Clip(Batch_Number(Temp))
                            End ! If Access:JOBSWARR.Update() = Level:Benign
                        End ! If Access:JOBS.Update() = Level:Benign
                        Prog:RecordCount += 1
                        ?Prog:UserString{prop:Text} = 'Batch No: ' & local:BatchNumber & ' . Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
                        Do Prog:UpdateScreen
|
                        
                    End ! Loop 25 Times
                Of Event:CloseWindow
        !            Prog:Exit = 1
        !            Prog:Cancelled = 1
        !            Break
                Of Event:Accepted
                    If Field() = ?Prog:Cancel
                        Beep(Beep:SystemQuestion)  ;  Yield()
                        Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                       icon:Question,'&Yes|&No',2,2)
                            Of 1 ! &Yes Button
                                Prog:Exit = 1
                                Prog:Cancelled = 1
                                Break
                            Of 2 ! &No Button
                        End!Case Message
                    End ! If Field() = ?ProgressCancel
            End ! Case Event()
            If Prog:Exit
                Break
            End ! If Prog:Exit
        End ! Accept
        Do Prog:ProgressFinished
        If Prog:Cancelled = 1
            Break
        End ! If Prog:Cancelled = 1
    End ! Loop x# = 1 To Records(BatchQueue)


    Flush(JOBS)
    Flush(JOBSWARR)
    Flush(AUDIT)

    Case Missive('Move process completed.','ServiceBase 3g',|
                   'midea.jpg','/OK') 
        Of 1 ! OK Button
    End ! Case Missive

    BRW10.ResetSort(1)
    FDB12.ResetQueue(1)
    Post(Event:Accepted,?DasUnTagAll)
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020673'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MoveBatches')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EDIBATCH.Open
  Access:MANUFACT.UseFile
  Access:JOBS.UseFile
  Access:JOBSWARR.UseFile
  SELF.FilesOpened = True
  BRW10.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:EDIBATCH,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','MoveBatches')
  ?List{prop:vcr} = TRUE
  ?tmp:DestinationBatchNumber{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  BRW10.Q &= Queue:Browse
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,ebt:BatchFirstSecondKey)
  BRW10.AddRange(ebt:SecondYearWarranty)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(?ebt:Batch_Number,ebt:Batch_Number,1,BRW10)
  BIND('tmp:Tag',tmp:Tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(tmp:Tag,BRW10.Q.tmp:Tag)
  BRW10.AddField(ebt:Batch_Number,BRW10.Q.ebt:Batch_Number)
  BRW10.AddField(ebt:ApprovedDate,BRW10.Q.ebt:ApprovedDate)
  BRW10.AddField(ebt:Manufacturer,BRW10.Q.ebt:Manufacturer)
  BRW10.AddField(ebt:SecondYearWarranty,BRW10.Q.ebt:SecondYearWarranty)
  FDB12.Init(?tmp:DestinationBatchNumber,Queue:FileDrop.ViewPosition,FDB12::View:FileDrop,Queue:FileDrop,Relate:EDIBATCH,ThisWindow)
  BIND('tmp:FirstSecondYear',tmp:FirstSecondYear)
  FDB12.Q &= Queue:FileDrop
  FDB12.AddSortOrder(ebt:BatchFirstSecondKey)
  FDB12.AddRange(ebt:Manufacturer,tmp:Manufacturer)
  FDB12.SetFilter('Upper(ebt:SecondYearWarranty) = Upper(tmp:FirstSecondYear)')
  FDB12.AddField(ebt:Batch_Number,FDB12.Q.ebt:Batch_Number)
  ThisWindow.AddItem(FDB12.WindowComponent)
  FDB12.DefaultFill = 0
  BRW10.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW10.AskProcedure = 0
      CLEAR(BRW10.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EDIBATCH.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','MoveBatches')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseEDIManufacturers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      BRW10.ResetSort(1)
      FDB12.ResetQueue(1)
      Post(Event:Accepted,?DasUnTagAll)
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW10.ApplyRange
      BRW10.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?CallLookup
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:FirstSecondYear
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FirstSecondYear, Accepted)
      BRW10.ResetSort(1)
      FDB12.ResetQueue(1)
      Post(Event:Accepted,?DasUnTagAll)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW10.ApplyRange
      BRW10.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FirstSecondYear, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:MoveClaims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:MoveClaims, Accepted)
      If Records(Glo:Queue) = 0
          Case Missive('You have not tagged any batches.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If Records(Glo:Queue) = 0
      Save_EDIBATCH_ID = Access:EDIBATCH.SaveFile()
      Access:EDIBATCH.ClearKey(ebt:BatchFirstSecondKey)
      ebt:Manufacturer = tmp:Manufacturer
      ebt:Batch_NUmber = tmp:DestinationBatchNumber
      ebt:SecondYearWarranty = tmp:FirstSecondYear
      If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
          !Found
      Else ! If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
          !Error
          Case Missive('Make sure you have selected the correct batch number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:DestinationBatchNumber)
          Cycle
      End ! If Access:EDIBATCH.TryFetch(ebt:BatchFirstSecondKey) = Level:Benign
      Access:EDIBATCH.RestoreFile(Save_EDIBATCH_ID)
      
      Do MoveClaims
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:MoveClaims, Accepted)
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020673'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020673'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020673'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:DestinationBatchNumber{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:DestinationBatchNumber{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW10.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = tmp:Manufacturer
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:FirstSecondYear
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = ebt:Batch_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  Access:JOBSWARR.Clearkey(jow:StatusManFirstKey)
  jow:Manufacturer = ebt:Manufacturer
  jow:Status = 'YES'
  !jow:BatchNumber = ebt:Batch_Number
  jow:FirstSecondYear = ebt:SecondYearWarranty
  Set(jow:StatusManFirstKey,jow:StatusManFirstKey)
  Loop ! Begin Loop
      If Access:JOBSWARR.Next()
          Break
      End ! If Access:JOBSWARR.Next()
      If jow:Manufacturer <> ebt:Manufacturer Or |
          jow:Status <> 'YES' Or |
          jow:FirstSecondYear <> ebt:SecondYearWarranty
              Break
      End ! jow:FirstSecondYear <> ebt:SecondYearWarranty
      Found# = 1
      Break
  End ! Loop
  If Found# = 0
      Return Record:Filtered
  End ! If Found# = 0
  
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = ebt:Batch_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ValidateRecord, (),BYTE)
  RETURN ReturnValue


FDB12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(12, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  If ebt:ApprovedDate > 0
      Return Record:Filtered
  End ! If ebt:ApprovedDate > 0
  ! After Embed Point: %FileDropMethodCodeSection) DESC(FileDrop Method Executable Code Section) ARG(12, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BrowseVettingReasons PROCEDURE                        !Generated from procedure template - Browse

BRW9::View:Browse    VIEW(VETTREAS)
                       PROJECT(vet:VettingReason)
                       PROJECT(vet:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
vet:VettingReason      LIKE(vet:VettingReason)        !List box control field - type derived from field
vet:RecordNumber       LIKE(vet:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,244),USE(?Panel5),FILL(09A6A7CH)
                       ENTRY(@s100),AT(256,106,124,10),USE(vet:VettingReason),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Vetting Reason')
                       LIST,AT(256,120,172,204),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('400L(2)|M~Vetting Reason~@s100@'),FROM(Queue:Browse)
                       BUTTON,AT(444,154),USE(?Select),TRN,FLAT,ICON('selectp.jpg')
                       BUTTON,AT(444,234),USE(?Insert),TRN,FLAT,ICON('insertp.jpg')
                       BUTTON,AT(444,268),USE(?Change),TRN,FLAT,ICON('editp.jpg')
                       BUTTON,AT(444,300),USE(?Delete),TRN,FLAT,ICON('deletep.jpg')
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse Vetting Reasons'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020684'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseVettingReasons')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:VETTREAS.Open
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:VETTREAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseVettingReasons')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,vet:VettingReasonKey)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?vet:VettingReason,vet:VettingReason,1,BRW9)
  BRW9.AddField(vet:VettingReason,BRW9.Q.vet:VettingReason)
  BRW9.AddField(vet:RecordNumber,BRW9.Q.vet:RecordNumber)
  BRW9.AskProcedure = 1
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:VETTREAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseVettingReasons')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  DoUpdate# = True
  
  Case Request
  Of InsertRecord
      If SecurityCheck('VETTING REASONS - INSERT')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          DoUpdate# = False
          Access:VETTREAS.CancelAutoInc()
      End ! If SecurityCheck('')
  Of ChangeRecord
      If SecurityCheck('VETTING REASONS - CHANGE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          DoUpdate# = False
      End ! If SecurityCheck('')
  Of DeleteRecord
      If SecurityCheck('VETTING REASONS - DELETE')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          DoUpdate# = False
      End ! If SecurityCheck('')
  End ! Case Request
  If DoUpdate# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateVettingReasons
    ReturnValue = GlobalResponse
  END
  End ! If DoUpdate# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020684'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020684'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020684'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateVettingReasons PROCEDURE                        !Generated from procedure template - Form

ActionMessage        CSTRING(40)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PANEL,AT(244,164,192,90),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Vetting Reason'),AT(248,196),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Vetting Reason'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       TEXT,AT(248,212,184,10),USE(vet:VettingReason),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR,SINGLE,MSG('Vetting Reason')
                       BUTTON,AT(364,258),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(296,258),USE(?OK),TRN,FLAT,ICON('okp.jpg'),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020685'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateVettingReasons')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:VETTREAS)
  Relate:VETTREAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:VETTREAS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateVettingReasons')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:VETTREAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateVettingReasons')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020685'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020685'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020685'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

WarrantyProcess PROCEDURE                             !Generated from procedure template - Browse

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::14:TAGFLAG         BYTE(0)
DASBRW::14:TAGMOUSE        BYTE(0)
DASBRW::14:TAGDISPSTATUS   BYTE(0)
DASBRW::14:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_jobswarr_id     USHORT,AUTO
tmp:EDIFlag          STRING('NO {1}')
tmp:Manufacturer     STRING(30)
tmp:Tag              STRING(1)
tmp:FirstSecondYear  BYTE(2)
tmp:TempFilePath     CSTRING(255)
tmp:POPRequired      BYTE(0)
tmp:ClaimStatus      STRING(40)
tmp:ImportFile       STRING(255),STATIC
tmp:SavePath         STRING(255)
tmp:LimitByDate      BYTE(0)
tmp:LimitMonth       STRING(20)
tmp:LimitYear        STRING(4)
tmp:BrowseStartDate  DATE
tmp:BrowseEndDate    DATE
YearQueue            QUEUE,PRE(year)
TheYear              STRING(4)
                     END
tmp:AvailableYears   STRING(255)
tmp:DateReconciled   DATE
BRW9::View:Browse    VIEW(JOBSWARR)
                       PROJECT(jow:RefNumber)
                       PROJECT(jow:BranchID)
                       PROJECT(jow:RepairedAt)
                       PROJECT(jow:ClaimSubmitted)
                       PROJECT(jow:DateAccepted)
                       PROJECT(jow:Orig_Sub_Date)
                       PROJECT(jow:RecordNumber)
                       PROJECT(jow:Status)
                       PROJECT(jow:Manufacturer)
                       PROJECT(jow:FirstSecondYear)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
jow:RefNumber          LIKE(jow:RefNumber)            !List box control field - type derived from field
jow:BranchID           LIKE(jow:BranchID)             !List box control field - type derived from field
wob:JobNumber          LIKE(wob:JobNumber)            !List box control field - type derived from field
jow:RepairedAt         LIKE(jow:RepairedAt)           !List box control field - type derived from field
wob:HeadAccountNumber  LIKE(wob:HeadAccountNumber)    !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Date_Completed     LIKE(job:Date_Completed)       !List box control field - type derived from field
jow:ClaimSubmitted     LIKE(jow:ClaimSubmitted)       !List box control field - type derived from field
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
tmp:ClaimStatus        LIKE(tmp:ClaimStatus)          !List box control field - type derived from local data
tmp:DateReconciled     LIKE(tmp:DateReconciled)       !List box control field - type derived from local data
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
jobe:RecordNumber      LIKE(jobe:RecordNumber)        !List box control field - type derived from field
jow:DateAccepted       LIKE(jow:DateAccepted)         !List box control field - type derived from field
jow:Orig_Sub_Date      LIKE(jow:Orig_Sub_Date)        !List box control field - type derived from field
jow:RecordNumber       LIKE(jow:RecordNumber)         !Primary key field - type derived from field
jow:Status             LIKE(jow:Status)               !Browse key field - type derived from field
jow:Manufacturer       LIKE(jow:Manufacturer)         !Browse key field - type derived from field
jow:FirstSecondYear    LIKE(jow:FirstSecondYear)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK16::jow:FirstSecondYear LIKE(jow:FirstSecondYear)
HK16::jow:Manufacturer    LIKE(jow:Manufacturer)
HK16::jow:Status          LIKE(jow:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,679,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Procedures'),USE(?Procedures)
                           ITEM('Import Service History'),USE(?ProceduresImportMFTRSubmissions)
                           ITEM('Import MFTR Warranty Claim Status'),USE(?ProceduresImportMFTRRemittance)
                           ITEM,SEPARATOR
                           ITEM('Final Rejection Routine'),USE(?ProceduresFinalRejectionRoutine)
                         END
                         MENU('Utilities'),USE(?Utilities)
                           ITEM('Rebuild List'),USE(?UtilitiesRebuildList)
                         END
                       END
                       PROMPT('Warranty Process'),AT(8,11),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,9,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       CHECK('Limit By Date'),AT(244,81),USE(tmp:LimitByDate),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Limit By Date'),TIP('Limit By Date'),VALUE('1','0')
                       PROMPT('I.M.E.I. History'),AT(80,391),USE(?Prompt:IMEIHistory),TRN,HIDE,FONT(,12,COLOR:Yellow,)
                       BUTTON,AT(8,385),USE(?Button:PreviousUnitHistory),TRN,FLAT,HIDE,ICON('prevhisp.jpg')
                       ENTRY(@s8),AT(8,81,64,10),USE(jow:RefNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number')
                       OPTION('First / Second Year Warranty Type'),AT(240,45,204,25),USE(tmp:FirstSecondYear),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('Both'),AT(244,55),USE(?tmp:FirstSecondYear:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                         RADIO('First Year'),AT(292,55),USE(?tmp:FirstSecondYear:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                         RADIO('Second Year'),AT(356,55),USE(?tmp:FirstSecondYear:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                       END
                       SHEET,AT(4,29,672,352),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Browse Pending Claims'),USE(?Tab1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(604,45),USE(?Button:ValuateClaims),TRN,FLAT,ICON('valclamp.jpg')
                           BUTTON('&Rev tags'),AT(190,177,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(194,208,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(8,349),USE(?Button:PrintSHRPending),TRN,FLAT,ICON('prnservp.jpg')
                           BUTTON,AT(536,349),USE(?Button:POPConfirmed),TRN,FLAT,HIDE,ICON('popconfp.jpg')
                           BUTTON,AT(604,349),USE(?Button:PrintTechnicalReports),TRN,FLAT,ICON('prntechp.jpg')
                         END
                         TAB('Query Claims'),USE(?Tab2)
                           BUTTON,AT(8,347),USE(?Button:PrintSHRQuery),TRN,FLAT,ICON('prnservp.jpg')
                         END
                         TAB('Rejected Claims'),USE(?Tab3)
                           BUTTON,AT(604,347),USE(?Button:ResubmitClaim),TRN,FLAT,ICON('resubclp.jpg')
                           BUTTON,AT(8,347),USE(?Button:PrintSHRRejected),TRN,FLAT,ICON('prnservp.jpg')
                           BUTTON,AT(76,347),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                           BUTTON,AT(148,347),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                           BUTTON,AT(220,347),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           BUTTON,AT(536,347),USE(?Button:FinalRejection),TRN,FLAT,ICON('finrejp.jpg')
                         END
                         TAB('Accepted Claims'),USE(?Tab4)
                           PROMPT('Approved Date:'),AT(80,82),USE(?Prompt:ApprovedDate),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@d6b),AT(144,82),USE(jow:DateAccepted),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(8,347),USE(?Button:PrintSHRAccepted),TRN,FLAT,ICON('prnservp.jpg')
                           BUTTON,AT(604,347),USE(?Button:AutoReconcileJobs),TRN,FLAT,ICON('autorecp.jpg')
                           BUTTON,AT(536,347),USE(?Button:ReconcileJob),TRN,FLAT,ICON('recjobp.jpg')
                         END
                         TAB('Reconciled Claims'),USE(?Tab5)
                           BUTTON,AT(8,349),USE(?Button:PrintSHRREconciled),TRN,FLAT,ICON('prnservp.jpg')
                         END
                       END
                       BUTTON,AT(648,3),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,11),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Limit By Submitted Date'),AT(240,69,204,28),USE(?Group2),DISABLE,BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Month'),AT(312,77),USE(?Prompt5),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         LIST,AT(312,85,80,10),USE(tmp:LimitMonth),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('80L(2)|M@s20@'),DROP(12),FROM('01 January|02 February|03 March|04 April|05 May|06 June|07 July|08 August|09 September|10 October|11 November|12 December')
                         PROMPT('Year'),AT(396,77),USE(?Prompt6),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         LIST,AT(396,85,44,10),USE(tmp:LimitYear),VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('16L(2)|M@s4@'),DROP(10),FROM(YearQueue)
                       END
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(604,384),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(468,384),USE(?Button:AuditTrail),TRN,FLAT,ICON('auditp.jpg')
                       BUTTON,AT(536,384),USE(?Button:ViewJob),TRN,FLAT,ICON('viewjobp.jpg')
                       PROMPT('Manufacturer'),AT(8,53),USE(?tmp:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(80,53,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                       BUTTON,AT(208,49),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       LIST,AT(8,101,664,242),USE(?Browse:1),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@36R(2)|M~Job No~@s8@14C|M~ID~@s2@44L(2)|M~RRC Job No~@s8@36C(2)|M~Rep' &|
   'aired~@s3@52L(2)|M~Head Account~@s30@64L(2)|M~Account Number~@s15@44R(2)|M~Compl' &|
   'eted~@D6b@44R(2)|M~Submitted~@d6@92L(2)|M~Charge Type~@s30@92L(2)|M~Repair Type~' &|
   '@s30@72L(2)|M~Claim Status~@s40@48R(2)|M~Reconciled~@d6@0L(2)|M~I.M.E.I. Number~' &|
   '@s20@0R(2)|M~Job Number~L@s8@32R(2)|M~Record Number~L@s8@40L(2)|M~Date Accepted~' &|
   '@d6@40L(2)|M~Orig Sub Date~@d17@'),FROM(Queue:Browse)
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('pcancel.ico'),HIDE
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW9::Sort4:Locator  EntryLocatorClass                !Conditional Locator - tmp:FirstSecondYear = 2 And tmp:LimitByDate = 1
BRW9::Sort1:Locator  EntryLocatorClass                !Conditional Locator - tmp:FirstSecondYear <> 2 And tmp:LimitByDate = 1
BRW9::Sort2:Locator  EntryLocatorClass                !Conditional Locator - tmp:FirstSecondYear <> 2 And tmp:LimitByDate = 0
BRW9::Sort3:Locator  EntryLocatorClass                !Conditional Locator - tmp:FirstSecondYear = 2 And tmp:LimitByDate = 0
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
local       Class
InvoiceSetup    Procedure(*Real f:LabourVatRate,*Real f:PartsVatRate),Byte
InvoiceJob    Procedure(Long f:JobNumber,Real f:Lab,Real f:Par,Real f:Other,Real f:Sub,Real f:VAT,Real f:Total,String f:Invoice,String f:Reason),Byte
            End

cRollBack       CLASS
init                PROCEDURE()
revert              PROCEDURE(BYTE fjobs,<BYTE fwebjob>,<BYTE fjobse>)
kill                PROCEDURE()
jobEDI                  LIKE(job:EDI)
jobEDI_Batch_Number     LIKE(job:EDI_Batch_Number)
wobEDI                  LIKE(wob:EDI)
jobeWarrantyClaimStatus LIKE(jobe:WarrantyClaimStatus)
wobOracleExportNumber   LIKE(wob:OracleExportNumber)
                 END
! Inserting (DBH 12/06/2008) # 9792 - Import Service History Report to mark as "submitted"
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
FieldA              String(30)
FieldB              String(30)
FieldC              String(30)
FieldD              String(30)
FieldE              String(255)
FieldF              String(255)
FieldG              String(30)
FieldH              String(30)
FieldI              String(30)
JobNumber           String(30)
                    End
                End
! End (DBH 12/06/2008) #9792
ReconcileFile File,Driver('BASIC'),Pre(recfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
JobNumber               String(30)
IMEINumber              String(30)
InvoiceRefNumber        String(30)
Manufacturer            String(30)
Labour                  String(30)
Parts                   String(30)
OtherCosts              String(30)
SubTotal                String(30)
VAT                     String(30)
Total                   String(30)
                    End
                End ! ReconcileFile File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::14:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?Browse:1))
  BRW9.UpdateBuffer
   glo:Queue.Pointer = jow:RefNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = jow:RefNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse.tmp:Tag = tmp:Tag
  IF (tmp:Tag = '*')
    Queue:Browse.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::14:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = jow:RefNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::14:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::14:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::14:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::14:QUEUE = glo:Queue
    ADD(DASBRW::14:QUEUE)
  END
  FREE(glo:Queue)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::14:QUEUE.Pointer = jow:RefNumber
     GET(DASBRW::14:QUEUE,DASBRW::14:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = jow:RefNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::14:DASSHOWTAG Routine
   CASE DASBRW::14:TAGDISPSTATUS
   OF 0
      DASBRW::14:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::14:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::14:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
ImportMFTRRemittance        Routine
    Data
local:RejectionString   String(255)
    Code

    !J - 26/05/15 - TB13543 - Warrany jobs are still not getting right status in spite of this being looked at many times
    !I have changed all the tryupdate() to update() so that if something is going wrong we do get error messages.
    !I have also removed a savefile/restorefile pair for jobswarr. Getting short of ideas for anything more

        tmp:SavePath = Path()

        If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        !Found
            SetPath(tmp:SavePath)
        Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        !Error
            SetPath(tmp:SavePath)
            Exit
        End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)

        Open(ImportFile)
        If Error()
            Stop(Error())
            Exit
        End ! If Error()

        Count# = 0
        Set(ImportFile,0)
        Loop
            Next(ImportFile)
            If Error()
                Break
            End ! If Error()
            Count# += 1
        End ! Loop

        Do Prog:ProgressSetup
        Prog:TotalRecords = Count#
        Prog:ShowPercentage = 1 !Show Percentage Figure
        Set(ImportFile,0)
        Accept
            Case Event()
            Of Event:Timer
                Loop 25 Times
                !Inside Loop
                    Next(ImportFile)
                    If Error()
                        Prog:Exit = 1
                        Break
                    End ! If Access:IMPORTFILE.Next()

                    Do Prog:UpdateScreen

                    Prog:RecordCount += 1
                    ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                    !Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()   !commented J - 26/05/15 TB13543
                                                                     !is this and RestoreFile below sometimes negating the update() in the middle?
                    Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                    jow:RefNumber = impfil:FieldA
                    If Access:JOBSWARR.Fetch(jow:RefNumberKey) = Level:Benign        !J - 26/05/15 - TB13543 changed from try
                    !Found
                        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                        man:Manufacturer = jow:Manufacturer
                        If Access:MANUFACT.Fetch(man:Manufacturer_Key) = Level:Benign    !J - 26/05/15 - TB13543 changed from try
                        !Found
                            If Sub(CLip(man:EDI_Path),-1,1) <> '\'
                                man:EDI_Path = Clip(man:EDI_Path) & '\'
                            End ! If Sub(CLip(man:EDI_Path),-1,1) = '\'
                        Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Error
                        End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = jow:RefNumber
                        If Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign      !J - 26/05/15 - TB13543 - changed from tryfetch
                        !Found
                        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                            Cycle
                        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_number = jow:RefNumber
                        If Access:JOBS.Fetch(job:Ref_Number_Key) = Level:Benign     !J - 26/05/15 - TB13543 changed from try
                        !Found
                            If JobInUse(job:Ref_Number,0)
                                LinePrint(Format(TOday(),@d06) & ' ' & Format(Clock(),@t04) & ': Job in use, could not be updated: ' & Clip(impfil:FieldA),Clip(man:EDI_Path) & 'ImportMFTRRemittance.log')
                                Cycle
                            End ! If JobInUse(job:Ref_Number,0)
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        !Error
                            Cycle
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        If Access:WEBJOB.Fetch(wob:RefNumberKey) = Level:Benign         !J - 26/05/15 - TB13543 changed from try
                        !Found
                        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                            Cycle
                        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

                        local:RejectionString = ''

                        cRollBack.Init() ! ! #12471 Use manual rollback because of the ability for users to edit locked records (DBH: 26/01/2012)

                        If jow:Status = 'NO' Or jow:Status = 'QUE'
                            Case impfil:FieldD
                            Of 'Accepted'
                                job:EDI = 'YES'
                            ! Need to fill in batch number to verify that it has been accepted (DBH: 23/07/2008)
                                job:EDI_Batch_Number = 9999
                                IF (Access:JOBS.Update())
                                ! Failed To Update
                                    cRollback.Revert(0)
                                ELSE ! IF (Access:JOBS.Update())
                                    If wob:EDI <> 'PAY' And wob:EDI <> 'APP'
                                        wob:EDI         = 'APP'
                                    End ! If wob:EDI <> 'PAY' And wob:EDI <> 'APP'
                                    IF (Access:WEBJOB.Update())                 !J - 26/05/15 - TB13543 changed from try
                                    ! Failed To Update
                                        cRollBack.Revert(1)! Rollback Jobs
                                    ELSE ! IF (Access:WEBJOB.TryUpdate())
                                        jobe:WarrantyClaimStatus = 'ACCEPTED'
                                        IF (Access:JOBSE.Update())
                                        ! Failed TO Update
                                            cRollBack.Revert(1,1) ! Rollback Jobs & WebJob
                                        ELSE ! IF (Access:JOBSE.Update())
                                            jow:Status          = 'YES'
                                            jow:DateAccepted    = Today()
                                            jow:RRCStatus       = wob:EDI
                                            IF (Access:JOBSWARR.Update())               !J - 26/05/15 - TB13543 changed from try
                                            ! Failed TO Update
                                                cRollBack.Revert(1,1,1) ! Rollback Jobs & Webjob & Jobse
                                            ELSE
                                                If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM APPROVED','')

                                                End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTED','REASON: ' & Clip(glo:EDI_Reason))
                                            END
                                        END ! IF (Access:JOBSE.Update())
                                    END ! IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                                END ! IF (Access:JOBS.Update())

                            Of 'Rejected' Orof 'Query'
                                FoundCodes# = 0
                                start# = 1
                                Loop t# = 1 To Len(impfil:FieldE)
                                    If Sub(impfil:FieldE,t#,1) = ';'
                                        Access:MANREJR.ClearKey(mar:CodeKey)
                                        mar:MANRecordNumber = man:RecordNumber
                                        mar:CodeNumber = Sub(impfil:FieldE,start#,t#-start#)
                                        If Access:MANREJR.Fetch(mar:CodeKey) = Level:Benign         !J - 26/05/15 - TB13543 changed from try
                                        !Found
                                            FoundCodes# = 1
                                            local:RejectionString = Clip(local:RejectionString) & '<13,10>' & Clip(mar:CodeNumber) & ': ' & Clip(mar:Description)
                                        Else ! If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign
                                        !Error
                                        End ! If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign

                                        start# = t# + 1
                                        Cycle
                                    End ! If Sub(impfil:FieldE,t#,1) = ';'
                                End ! Loop t# = 1 To Len(impfil:FieldE)
                                Access:MANREJR.ClearKey(mar:CodeKey)
                                mar:MANRecordNumber = man:RecordNumber
                                mar:CodeNumber = Sub(impfil:FieldE,start#,t#-start#)
                                If Access:MANREJR.Fetch(mar:CodeKey) = Level:Benign         !J - 26/05/15 - TB13543 changed from try
                                !Found
                                    FoundCodes# = 1
                                    local:RejectionString = Clip(local:RejectionString) & '<13,10>' & Clip(mar:CodeNumber) & ': ' & Clip(mar:Description)
                                Else ! If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign
                                !Error
                                End ! If Access:MANREJR.TryFetch(mar:CodeKey) = Level:Benign

                                If FoundCodes# = 0
                                    local:RejectionString = Clip(Upper(impfil:FieldE))
                                End ! If local:RejectionString = ''
                                If impfil:FieldD = 'Rejected'
                                    job:EDI = 'EXC'
                                ! Need to fill in batch number to verify that it has been accepted (DBH: 23/07/2008)
                                    job:EDI_Batch_Number = 9999
                                    IF (Access:JOBS.Update() )              !J - 26/05/15 - TB13543 changed from try
                                        cRollBack.Revert(0)
                                    ELSE
                                        wob:OracleExportNumber = 0
                                        If wob:EDI <> 'PAY' And wob:EDI <> 'APP'
                                            wob:EDI = 'EXC'
                                        End ! If wob:EDI <> 'PAY' And wob:EDI <> 'APP'
                                        IF (Access:WEBJOB.Update())         !J - 26/05/15 - TB13543 changed from try
                                        ! Failed To Update
                                            cRollBack.Revert(1) ! Rollback Jobs
                                        ELSE ! IF (Access:WEBJOB.Update())
                                            jobe:WarrantyClaimStatus = 'REJECTED'
                                            IF (Access:JOBSE.Update())      !J - 26/05/15 - TB13543 changed from try
                                                cRollBack.Revert(1,1) ! Rollback Jobs & Webjob
                                            ELSE
                                                jow:Status = 'EXC'
                                                jow:RRCStatus = wob:EDI
                                                jow:DateRejected = Today()  ! #11505 Record date rejected (DBH: 21/07/2010)

                                                If Access:JOBSWARR.Update()         !J - 26/05/15 - TB13543 changed from try
                                                    cRollBack.Revert(1,1,1) ! Rollback Jobs & Webjob & Jobse
                                                ELSE
                                                    If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTED','REASON: ' & Clip(local:RejectionString))
                                                    End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTED','REASON: ' & Clip(glo:EDI_Reason))

                                            ! Inserting (DBH 12/09/2008) # 10398 - Set the number of resubmissions per manufacturer
                                                    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                                                    man:Manufacturer = job:Manufacturer
                                                    If Access:MANUFACT.Fetch(man:Manufacturer_Key) = Level:Benign       !J - 26/05/15 - TB13543 changed from try
                                                !Found
                                                        If man:LimitResubmissions
                                                ! End (DBH 12/09/2008) #10398
                                                            CountRejections# = 0
                                                            Access:AUDIT.Clearkey(aud:TypeActionKey)
                                                            aud:Ref_Number = job:Ref_Number
                                                            aud:Type = 'JOB'
                                                            aud:Action = 'WARRANTY CLAIM REJECTED'
                                                            Set(aud:TypeActionKey,aud:TypeActionKey)
                                                            Loop ! Begin Loop
                                                                If Access:AUDIT.Next()
                                                                    Break
                                                                End ! If Access:AUDIT.Next()
                                                                If aud:Ref_Number <> job:Ref_Number
                                                                    Break
                                                                End ! If aud:Ref_Number <> job:Ref_Number
                                                                If aud:Type <> 'JOB'
                                                                    Break
                                                                End ! If aud:Type <> 'JOB'
                                                                If aud:Action <> 'WARRANTY CLAIM REJECTED'
                                                                    Break
                                                                End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                                                                CountRejections# += 1
                                                            End ! Loop

                                                            If CountRejections# > man:TimesToResubmit
                                                        ! Job has been rejected 3 times. Make it a final rejectin (DBH: 16/06/2008)
                                                                job:EDI = 'REJ'
                                                                wob:EDI = 'REJ'
                                                                wob:OracleExportNumber = 0
                                                                If Access:JOBS.Update() = Level:Benign          !J - 26/05/15 - TB13543 changed from try
                                                                    If Access:WEBJOB.Update() = Level:Benign    !J - 26/05/15 - TB13543 changed from try
                                                                        jow:Status = 'REJ'
                                                                        jow:RRCStatus = wob:EDI
                                                                
                                                                        jow:DateFinalRejection = Today()    ! #11505 Record date of final rejection (DBH: 21/07/2010)
                                                                        If Access:JOBSWARR.Update() = Level:Benign      !J - 26/05/15 - TB13543 changed from try
                                                                            If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: JOB REJECTED ' & CountRejections# & ' TIMES')

                                                                            End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: ' & Clip(glo:EDI_Reason))

                                                                            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                                                                            jbn:RefNumber = job:Ref_Number
                                                                            If Access:JOBNOTES.Fetch(jbn:RefNumberKey) = Level:Benign       !J - 26/05/15 - TB13543 changed from try
                                                                        !Found
                                                                                jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>JOB REJECTED ' & CountRejections# & ' TIMES'
                                                                                Access:JOBNOTES.Update()
                                                                            Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                                                                        !Error
                                                                            End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

                                                                            Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                                                            jobe:RefNumber = job:Ref_Number
                                                                            If Access:JOBSE.Fetch(jobe:RefNumberKey) = Level:Benign         !J - 26/05/15 - TB13543 changed from try
                                                                        !Found
                                                                                jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                                                                                jobe:WarrantyStatusDate = Today()
                                                                                Access:JOBSE.Update()
                                                                            Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                                                        !Error
                                                                            End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign

                                                                        End ! If Access:JOBSWARR.TryUpdate()
                                                                    End ! If Access:WEBJOB.TryUpdate() = Level:Benign
                                                                End ! If Access:JOBS.TryUpdate() = Level:Benign
                                                            End ! If CountRejections# > 2
                                                        End ! If man:LimitResubmissions
                                                    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                                !Error
                                                    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                                END
                                                
                                            End ! If Access:JOBSWARR.TryUpdate() = Level:Benign
                                        END ! IF (Access:WEBJOB.Update())
                                    END
                                Else ! If impfil:FieldD = 'Rejected'
                                    job:EDI = 'QUE'
                                    job:EDI_Batch_Number = 9999
                                    IF (Access:JOBS.Update())       !J - 26/05/15 - TB13543 changed from try
                                        cRollback.Revert(0)
                                    ELSE
                                        jobe:WarrantyClaimStatus = 'QUERY'
                                        IF (Access:JOBSE.Update())      !J - 26/05/15 - TB13543 changed from try
                                            cRollBack.Revert(1) ! Rollback Jobs
                                        ELSE
                                            jow:Status = 'QUE'
                                            IF (Access:JOBSWARR.Update())           !J - 26/05/15 - TB13543 changed from try
                                                cRollBack.Revert(1,,1) ! Rollback Jobs And Jobse
                                            ELSE
                                                If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM QUERY','REASON: ' & Clip(local:RejectionString))

                                                End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM QUERY','REASON: ' & Clip(glo:EDI_Reason))
                                            END
                                        END
                                    END
                                End ! If impfil:FieldD = 'Rejected'
                            End ! Case
                        Else ! If jow:Status <> 'NO'
                            LinePrint(Format(TOday(),@d06) & ' ' & Format(Clock(),@t04) & ': Job not at the correct status: ' & Clip(impfil:FieldA),Clip(man:EDI_Path) & 'ImportMFTRRemittance.log')
                        End ! If jow:Status <> 'NO'
                    Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                        !Error
                        LinePrint(Format(TOday(),@d06) & ' ' & Format(Clock(),@t04) & ': Cannot find job number: ' & Clip(impfil:FieldA),Clip(man:EDI_Path) & 'ImportMFTRRemittance.log')
                    End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign

                    !Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)   !commented J - 26/05/15 TB13543
                                                                     !is this and SaveFile below sometimes negating the update() in the middle?
                End ! Loop 25 Times
            Of Event:CloseWindow
                Prog:Exit = 1
                Prog:Cancelled = 1
                Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                    Of 1 ! Yes
                        Prog:Exit = 1
                        Prog:Cancelled = 1
                        Break
                    Of 2 ! No
                    End ! Case Message
                End! If FIeld()
            End ! Case Event()
            If Prog:Exit
                Break
            End ! If Prog:Exit
        End ! Accept
        Do Prog:ProgressFinished
        Close(ImportFile)

        cRollBack.Kill()

! -------------------------------------
ReconcileJobs       Routine
Data
local:LabourVatRate     Real
local:PartsVatRate      Real
local:TotalJobs         Long()
local:Claimed           Real
local:TotalLabour       Real
local:TotalParts        Real
local:TotalOther        Real
local:Total             Real
local:InvoiceNumber     Long()
local:Errors            Byte()
Code
    If local.InvoiceSetup(local:LabourVatRate,local:PartsVatRate)
        Exit
    End ! If local.InvoiceSetup()

    tmp:SavePath = Path()
    If FileDialog('Choose Reconciliation File',tmp:ImportFile,'CSV Files|*.csv',file:KeepDir + file:NoError + File:LongName)
        !Found
        SetPath(tmp:SavePath)
    Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        !Error
        SetPath(tmp:SavePath)
        Exit
    End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)

    Open(ReconcileFile)
    If Error()
        Stop(Error())
        Exit
    End ! If Error()

    Count# = 0
    Set(ReconcileFile,0)
    Loop
        Next(ReconcileFile)
        If ERror()
            Break
        End ! If ERror()
        Access:JOBSWARR.ClearKey(jow:StatusManKey)
        jow:Status = 'YES'
        jow:Manufacturer = tmp:Manufacturer
        jow:RefNumber = recfil:JobNumber
        If Access:JOBSWARR.TryFetch(jow:StatusManKey) = Level:Benign
            !Found
            ! Double check that there are jobs on the CSV that will be updated (DBH: 19/06/2008)
        Else ! If Access:JOBSWARR.TryFetch(jow:StatusManKey) = Level:Benign
            !Error
            Cycle
        End ! If Access:JOBSWARR.TryFetch(jow:StatusManKey) = Level:Benign

        Count# += 1
    End ! Loop

    If Count# = 0
        Exit
    End ! If Count# = 0

    If Access:INVOICE.PrimeRecord() = Level:Benign
        inv:Job_Number = 0
        inv:Date_Created = Today()
        inv:Account_Number = man:Trade_Account
        inv:AccountType = 'MAI'
        inv:VAT_Rate_Labour = local:LabourVatRate
        inv:VAT_Rate_Parts = local:PartsVatRate
        inv:VAT_Rate_Retail = local:LabourVatRate
        inv:Vat_Number = def:Vat_Number
        inv:Invoice_Vat_Number = 0
        inv:Currency = ''
        inv:Manufacturer = tmp:Manufacturer
        inv:Claim_Reference = ''
        inv:Reconciled_Date = Today()
        inv:Invoice_Type = 'WAR'
        local:InvoiceNumber = inv:Invoice_Number
        If Access:INVOICE.TryInsert() = Level:Benign
            !Insert
        Else ! If Access:INVOICE.TryInsert() = Level:Benign
            Access:INVOICE.CancelAutoInc()
        End ! If Access:INVOICE.TryInsert() = Level:Benign
    End ! If Access.INVOICE.PrimeRecord() = Level:Benign

    Do Prog:ProgressSetup
    Prog:TotalRecords = Count#
    Prog:ShowPercentage = 1 !Show Percentage Figure
    Set(ReconcileFile,0)
    Accept
        Case Event()
        Of Event:Timer
            Loop 25 Times
                !Inside Loop
                Next(ReconcileFile)
                If Error()
                    Prog:Exit = 1
                    Break
                End ! If Access:RECONCILEFILE.Next()

                Access:JOBSWARR.ClearKey(jow:StatusManKey)
                jow:Status = 'YES'
                jow:Manufacturer = tmp:Manufacturer
                jow:RefNumber = recfil:JobNumber
                If Access:JOBSWARR.TryFetch(jow:StatusManKey) = Level:Benign
                    !Found

                Else ! If Access:JOBSWARR.TryFetch(jow:StatusManKey) = Level:Benign
                    !Error
                    LinePrint(Format(TOday(),@d06) & ' ' & Format(Clock(),@t04) & ': Job Number is not ready to be reconciled: ' & Clip(recfil:JobNumber),Clip(man:EDI_Path) & 'ReconcileJobs.log')
                    local:Errors = 1
                    Cycle
                End ! If Access:JOBSWARR.TryFetch(jow:StatusManKey) = Level:Benign

                If local.InvoiceJob(jow:RefNumber,recfil:Labour,recfil:Parts,recfil:OtherCosts,recfil:SubTotal,recfil:VAT,recfil:Total,recfil:InvoiceRefNumber,'')
                    local:Errors = 1
                Else ! If local.InvoiceJob(jow:RefNumber)
                    local:TotalJobs += 1
                    local:Claimed += jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
                    local:TotalLabour += jobe2:WLabourPaid
                    local:TotalParts += jobe2:WPartsPaid
                    local:TotalOther += jobe2:WOtherCosts
                    local:Total += jobe2:WTotal

                End ! If local.InvoiceJob(jow:RefNumber)


                Do Prog:UpdateScreen
                Prog:RecordCount += 1
                ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
            End ! Loop 25 Times
        Of Event:CloseWindow
!            Prog:Exit = 1
!            Prog:Cancelled = 1
!            Break
        Of Event:Accepted
            If Field() = ?Prog:Cancel
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                Of 1 ! Yes
                    Prog:Exit = 1
                    Prog:Cancelled = 1
                    Break
                Of 2 ! No
                End ! Case Message
            End! If FIeld()
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished

    Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
    inv:Invoice_Number = local:InvoiceNumber
    If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Found
        ! Reget the invoice (DBH: 19/06/2008)
        inv:Total = local:Claimed
        inv:Total_Claimed = local:Claimed
        inv:Courier_Paid = local:TotalOther
        inv:Labour_Paid = local:TotalLabour
        inv:Parts_Paid = local:TotalParts
        inv:Jobs_Count = local:TotalJobs
        If Access:INVOICE.TryUpdate() = Level:Benign

        End ! If Access:INVOICE.TryUpdate() = Level:Benign
    Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign


    If local:Errors
        Beep(Beep:SystemExclamation)  ;  Yield()
        Case Missive('' & Clip(local:TotalJobs) & ' Jobs Reconciled.'&|
            '|'&|
            '|Errors occured and can be viewed in the file: ' & Clip(man:EDI_Path) & 'ReconcileJobs.log','ServiceBase 3g',|
                       'mexclam.jpg','/&OK') 
            Of 1 ! &OK Button
        End!Case Message
    Else ! If local:Errors
        Beep(Beep:SystemAsterisk)  ;  Yield()
        Case Missive('' & Clip(local:TotalJobs) & ' Jobs Reconciled.','ServiceBase 3g',|
                       'midea.jpg','/&OK') 
            Of 1 ! &OK Button
        End!Case Message
    End ! If local:Errors
ReconcileSingleJob    Routine
Data
local:LabourVatRate     Real
local:PartsVatRate      Real
local:InvoiceNumber     Long
local:Labour            Real
local:Parts             Real
local:OtherCosts        Real
local:Sub               Real
local:VAT               Real
local:Total             Real
local:Reason            String(255)
Code
    brw9.UpdateViewRecord()

    If ReconcileJob(jow:RefNumber,local:Labour,local:Parts,local:OtherCosts,local:Sub,local:VAT,local:Total,local:Reason)
        Exit
    End ! If ReconcileJob(jow:RefNumber,local:Labour,local:Parts,local:OtherCosts,local:Sub,local:VAT,local:Total,local:Reason)
    If local.InvoiceSetup(local:LabourVatRate,local:PartsVatrate)
        Exit
    End ! If local.InvoiceSetup(local:LabourVatRate,local:PartsVatrate)

    If Access:INVOICE.PrimeRecord() = Level:Benign
        inv:Job_Number = 0
        inv:Date_Created = Today()
        inv:Account_Number = man:Trade_Account
        inv:AccountType = 'MAI'
        inv:VAT_Rate_Labour = local:LabourVatRate
        inv:VAT_Rate_Parts = local:PartsVatRate
        inv:VAT_Rate_Retail = local:LabourVatRate
        inv:Vat_Number = def:Vat_Number
        inv:Invoice_Vat_Number = 0
        inv:Currency = ''
        inv:Manufacturer = tmp:Manufacturer
        inv:Claim_Reference = ''
        inv:Reconciled_Date = Today()
        inv:Invoice_Type = 'WAR'
        local:InvoiceNumber = inv:Invoice_Number
        If Access:INVOICE.TryInsert() = Level:Benign
            !Insert
        Else ! If Access:INVOICE.TryInsert() = Level:Benign
            Access:INVOICE.CancelAutoInc()
        End ! If Access:INVOICE.TryInsert() = Level:Benign
    End ! If Access.INVOICE.PrimeRecord() = Level:Benign
    
    If local.InvoiceJob(jow:RefNumber,local:Labour,local:Parts,local:OtherCosts,local:Sub,local:VAT,local:Total,'',local:Reason)
        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = local:InvoiceNumber
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            Delete(INVOICE)
            Exit
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

    Else ! If local.InvoiceJob(jow:RefNumber)

        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = local:InvoiceNumber
        If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Found
            inv:Total = jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
            inv:Total_Claimed = jobe:ClaimValue + job:Courier_Cost_Warranty + jobe:ClaimPartsCost
            inv:Courier_Paid = jobe2:WOtherCosts
            inv:Labour_Paid = jobe2:WLabourPaid
            inv:Parts_Paid = jobe2:WPartsPaid
            inv:Jobs_Count = 1
            Access:INVOICE.TryUpdate()
        Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
        End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

    End ! If local.InvoiceJob(jow:RefNumber)

    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Missive('Job Reconciled.','ServiceBase 3g',|
                   'midea.jpg','/&OK')
        Of 1 ! &OK Button
    End!Case Message
RefreshBrowse       Routine
    tmp:POPRequired = 0
    ?Button:POPConfirmed{prop:Hide} = 1
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = tmp:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:POPRequired
            tmp:POPRequired = 1
            ?Button:POPConfirmed{prop:Hide} = 0
        End ! If man:POPRequired
    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer) = Level:Benign
        !Error
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer) = Level:Benign
    Post(Event:Accepted,?DASUNTAGALL)
    Brw9.ResetSort(1)
RefreshDate     ROutine
    If tmp:LimitByDate
        Case tmp:LimitMonth
        Of '01 January'
            tmp:BrowseStartDate = Date(1,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(1,31,tmp:LimitYear)
        Of '02 February'
            tmp:BrowseStartDate = Date(2,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(2,28,tmp:LimitYear)
        Of '03 March'
            tmp:BrowseStartDate = Date(3,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(3,31,tmp:LimitYear)
        Of '04 April'
            tmp:BrowseStartDate = Date(4,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(4,30,tmp:LimitYear)
        Of '05 May'
            tmp:BrowseStartDate = Date(5,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(5,31,tmp:LimitYear)
        Of '06 June'
            tmp:BrowseStartDate = Date(6,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(6,30,tmp:LimitYear)
        Of '07 July'
            tmp:BrowseStartDate = Date(7,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(7,31,tmp:LimitYear)
        Of '08 August'
            tmp:BrowseStartDate = Date(8,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(8,31,tmp:LimitYear)
        Of '09 September'
            tmp:BrowseStartDate = Date(9,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(9,30,tmp:LimitYear)
        Of '10 October'
            tmp:BrowseStartDate = Date(10,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(10,31,tmp:LimitYear)
        Of '11 November'
            tmp:BrowseStartDate = Date(11,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(11,30,tmp:LimitYear)
        Of '12 December'
            tmp:BrowseStartDate = Date(12,1,tmp:LimitYear)
            tmp:BrowseEndDate = Date(12,31,tmp:LimitYear)
        End ! Case tmp:LimitMonth
    Else
        tmp:BrowseStartDate = Date(1,1,2000)
        tmp:BrowseEndDate = Today()
    End ! If tmp:LimitByDate
    Brw9.ResetSort(1)
ValuateClaims       Routine
Data
tmp:EDI             String(3)
tmp:ClaimValue      Real()
Code
    Case Missive('This will valuate the pending claims in the browse. '&|
      '<13,10>'&|
      '<13,10>This may take a long time depending on the amount of claims. Are you sure?','ServiceBase 3g',|
                   'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
    
        tmp:ClaimValue = 0
    
        SetCursor(Cursor:Wait)
        Count# = 0

        Stream(JOBSWARR)
    
        Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
        Access:JOBSWARR.Clearkey(jow:StatusManFirstKey)
        jow:Status = 'NO'
        jow:Manufacturer = tmp:Manufacturer
        If tmp:FirstSecondYear <> 2
            jow:FirstSecondYear = tmp:FirstSecondYear
        End ! If tmp:FirstSecondYear <> 2
        Set(jow:StatusManFirstKey,jow:StatusManFirstKey)
        Loop ! Begin Loop
            If Access:JOBSWARR.Next()
                Break
            End ! If Access:JOBSWARR.Next()
            If jow:Status <> 'NO'
                Break
            End ! If jow:Status <> 'NO'
            If jow:Manufacturer <> tmp:Manufacturer
                Break
            End ! If jow:Manufacturer <> tmp:Manufacturer
            If tmp:FirstSecondYear <> 2
                If jow:FirstSecondYear <> tmp:FirstSecondYEar
                    Break
                End ! If jow:FirstSecondYear <> tmp:FirstSecondYEar
            End ! If tmp:FirstSecondYear <> 2
            Count# += 1
        End ! Loop
        Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
    
        SetCursor()
    
        Do Prog:ProgressSetup
        Prog:TotalRecords = Count#
        Prog:ShowPercentage = 1 !Show Percentage Figure
    
    
        Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
        Access:JOBSWARR.Clearkey(jow:StatusManFirstKey)
        jow:Status = 'NO'
        jow:Manufacturer = tmp:Manufacturer
        If tmp:FirstSecondYear <> 2
            jow:FirstSecondYear = tmp:FirstSecondYear
        End ! If tmp:FirstSecondYear <> 2
        Set(jow:StatusManFirstKey,jow:StatusManFirstKey)
    
        Accept
            Case Event()
                Of Event:Timer
                    Loop 25 Times
                        !Inside Loop
                        If Access:JOBSWARR.Next()
                            Prog:Exit = 1
                            Break
                        End ! If Access:JOBSWARR.Next()
                        If jow:Status <> 'NO'
                            Prog:Exit = 1
                            Break
                        End ! If jow:Status <> 'NO'
                        If jow:Manufacturer <> tmp:Manufacturer
                            Prog:Exit = 1
                            Break
                        End ! If jow:Manufacturer <> tmp:Manufacturer
                        If tmp:FirstSecondYear <> 2
                            If jow:FirstSecondYear <> tmp:FirstSecondYEar
                                Prog:Exit = 1
                                Break
                            End ! If jow:FirstSecondYear <> tmp:FirstSecondYEar
                        End ! If tmp:FirstSecondYear <> 2

                        ! Double check the EDI flag is correct (DBH: 13/02/2007)
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = jow:RefNumber
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                            tmp:EDI = job:EDI
                            job:EDI = PendingJob(job:Manufacturer)
                            If tmp:EDI <> job:EDI
                                If Access:JOBS.Update() = Level:Benign
    
                                End ! If Access:JOBS.Update() = Level:Benign
                            End ! If tmp:EDI <> job:EDI
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
    
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_Number
                        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                            !Found
                        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
    
                        If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                            if jobe:ConfirmClaimAdjustment
                                tmp:ClaimValue += Round((Round(jobe:SubTotalAdjustment,.01) + Round(job:courier_cost_warranty,.01)),.01)
                            else
                                tmp:ClaimValue += Round(Round(jobe:ClaimValue,.01) + Round(jobe:ClaimPartsCost,.01) + Round(job:courier_cost_warranty,.01),.01)
                            end
                        Else !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                            if jobe:ConfirmClaimAdjustment
                                tmp:ClaimValue += Round((Round(jobe:LabourAdjustment,.01) + Round(jobe:PartsAdjustment,.01) + Round(job:courier_cost_warranty,.01)),.01)
                            else
                                tmp:ClaimValue += Round((Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)),.01)
                            end
                        End !If GETINI('STOCK','UseVirtualPrice',,CLIP(PATH())&'\SB2KDEF.INI') = 1
    
                        Do Prog:UpdateScreen
    
                        Prog:RecordCount += 1
                        ?Prog:UserString{Prop:Text}= 'Records Found: ' & Prog:RecordCount
                    End ! Loop 25 Times
                Of Event:CloseWindow
  !                      Prog:Exit = 1
  !                      Prog:Cancelled = 1
  !                      Break
                Of Event:Accepted
                    If Field() = ?Prog:Cancel
                        Beep(Beep:SystemQuestion)  ;  Yield()
                        Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                       icon:Question,'&Yes|&No',2,2)
                            Of 1 ! &Yes Button
                                Prog:Exit = 1
                                Prog:Cancelled = 1
                                Break
                            Of 2 ! &No Button
                        End!Case Message
                    End ! If Field() = ?ProgressCancel
            End ! Case Event()
            If Prog:Exit
                Break
            End ! If Prog:Exit
        End ! Accept
        Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
        Do Prog:ProgressFinished
        Flush(JOBSWARR)
    
        Case Missive('Number of claims: ' & Count# & '.'&|
          '<13,10>'&|
          '<13,10>Value of claims: ' & Format(tmp:ClaimValue,@n14.2) & '.','ServiceBase 3g',|
                       'mquest.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Of 1 ! No Button
    End ! Case Missive
    BRW9.ResetSort(1)
    Select(?Browse:1)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020719'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('WarrantyProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:MANUFACT.UseFile
  Access:JOBS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBSE.UseFile
  Access:MANREJR.UseFile
  Access:JOBSE2.UseFile
  Access:JOBNOTES.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  SELF.FilesOpened = True
  Case Month(Today())
  Of 1
      tmp:LimitMonth = '01 January'
  Of 2
      tmp:LimitMonth = '02 February'
  Of 3
      tmp:LimitMonth = '03 March'
  Of 4
      tmp:LimitMonth = '04 April'
  Of 5
      tmp:LimitMonth = '05 May'
  Of 6
      tmp:LimitMonth = '06 June'
  Of 7
      tmp:LimitMonth = '07 July'
  Of 8
      tmp:LimitMonth = '08 August'
  Of 9
      tmp:LimitMonth = '09 September'
  Of 10
      tmp:LimitMonth = '10 October'
  Of 11
      tmp:LimitMonth = '11 November'
  Of 12
      tmp:LimitMonth = '12 December'
  End ! Case Month(Today())
  Loop x# = 2000 To Year(Today())
      year:TheYear = x#
      Add(YearQueue)
  End ! Loop x# = 2000 To Year(Today())
  tmp:LimitYear = Year(Today())
  tmp:BrowseStartDate = Date(1,1,2000)
  tmp:BrowseEndDate = Today()
  BRW9.Init(?Browse:1,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:JOBSWARR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','WarrantyProcess')
  ?tmp:LimitMonth{prop:vcr} = TRUE
  ?tmp:LimitYear{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:LimitMonth{prop:vcr} = False
  ?tmp:LimitYear{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,jow:ClaimStatusManKey)
  BRW9.AddRange(jow:ClaimSubmitted,tmp:BrowseStartDate,tmp:BrowseEndDate)
  BRW9.AddLocator(BRW9::Sort4:Locator)
  BRW9::Sort4:Locator.Init(?jow:RefNumber,jow:ClaimSubmitted,1,BRW9)
  BRW9.AddSortOrder(,jow:ClaimStatusManFirstKey)
  BRW9.AddRange(jow:ClaimSubmitted,tmp:BrowseStartDate,tmp:BrowseEndDate)
  BRW9.AddLocator(BRW9::Sort1:Locator)
  BRW9::Sort1:Locator.Init(?jow:RefNumber,jow:ClaimSubmitted,1,BRW9)
  BRW9.AddSortOrder(,jow:StatusManFirstKey)
  BRW9.AddRange(jow:FirstSecondYear)
  BRW9.AddLocator(BRW9::Sort2:Locator)
  BRW9::Sort2:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW9)
  BRW9.AddSortOrder(,jow:StatusManKey)
  BRW9.AddRange(jow:Manufacturer)
  BRW9.AddLocator(BRW9::Sort3:Locator)
  BRW9::Sort3:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW9)
  BRW9.AddSortOrder(,jow:ClaimStatusManKey)
  BRW9.AddRange(jow:ClaimSubmitted,tmp:BrowseStartDate,tmp:BrowseEndDate)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?jow:RefNumber,jow:ClaimSubmitted,1,BRW9)
  BIND('tmp:Tag',tmp:Tag)
  BIND('tmp:ClaimStatus',tmp:ClaimStatus)
  BIND('tmp:DateReconciled',tmp:DateReconciled)
  BIND('tmp:BrowseStartDate',tmp:BrowseStartDate)
  BIND('tmp:BrowseEndDate',tmp:BrowseEndDate)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(tmp:Tag,BRW9.Q.tmp:Tag)
  BRW9.AddField(jow:RefNumber,BRW9.Q.jow:RefNumber)
  BRW9.AddField(jow:BranchID,BRW9.Q.jow:BranchID)
  BRW9.AddField(wob:JobNumber,BRW9.Q.wob:JobNumber)
  BRW9.AddField(jow:RepairedAt,BRW9.Q.jow:RepairedAt)
  BRW9.AddField(wob:HeadAccountNumber,BRW9.Q.wob:HeadAccountNumber)
  BRW9.AddField(job:Account_Number,BRW9.Q.job:Account_Number)
  BRW9.AddField(job:Date_Completed,BRW9.Q.job:Date_Completed)
  BRW9.AddField(jow:ClaimSubmitted,BRW9.Q.jow:ClaimSubmitted)
  BRW9.AddField(job:Warranty_Charge_Type,BRW9.Q.job:Warranty_Charge_Type)
  BRW9.AddField(job:Repair_Type_Warranty,BRW9.Q.job:Repair_Type_Warranty)
  BRW9.AddField(tmp:ClaimStatus,BRW9.Q.tmp:ClaimStatus)
  BRW9.AddField(tmp:DateReconciled,BRW9.Q.tmp:DateReconciled)
  BRW9.AddField(job:ESN,BRW9.Q.job:ESN)
  BRW9.AddField(job:Ref_Number,BRW9.Q.job:Ref_Number)
  BRW9.AddField(jobe:RecordNumber,BRW9.Q.jobe:RecordNumber)
  BRW9.AddField(jow:DateAccepted,BRW9.Q.jow:DateAccepted)
  BRW9.AddField(jow:Orig_Sub_Date,BRW9.Q.jow:Orig_Sub_Date)
  BRW9.AddField(jow:RecordNumber,BRW9.Q.jow:RecordNumber)
  BRW9.AddField(jow:Status,BRW9.Q.jow:Status)
  BRW9.AddField(jow:Manufacturer,BRW9.Q.jow:Manufacturer)
  BRW9.AddField(jow:FirstSecondYear,BRW9.Q.jow:FirstSecondYear)
  IF ?tmp:LimitByDate{Prop:Checked} = True
    HIDE(?jow:RefNumber)
    ENABLE(?Group2)
  END
  IF ?tmp:LimitByDate{Prop:Checked} = False
    UNHIDE(?jow:RefNumber)
    DISABLE(?Group2)
  END
  BRW9.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW9.AskProcedure = 0
      CLEAR(BRW9.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Browse Pending Claims'
    ?Tab2{PROP:TEXT} = 'Query Claims'
    ?Tab3{PROP:TEXT} = 'Rejected Claims'
    ?Tab4{PROP:TEXT} = 'Accepted Claims'
    ?Tab5{PROP:TEXT} = 'Reconciled Claims'
    ?Browse:1{PROP:FORMAT} ='36R(2)|M~Job No~@s8@#3#14C|M~ID~@s2@#4#36C(2)|M~Repaired~@s3@#6#52L(2)|M~Head Account~@s30@#7#64L(2)|M~Account Number~@s15@#8#44R(2)|M~Completed~@D6b@#9#40L(2)|M~Orig Sub Date~@d17@#19#44R(2)|M~Submitted~@d6@#10#92L(2)|M~Charge Type~@s30@#11#92L(2)|M~Repair Type~@s30@#12#72L(2)|M~Claim Status~@s40@#13#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','WarrantyProcess')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseEDIManufacturers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:AuditTrail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
      Brw9.UpdateViewRecord()
      glo:Select12 = jow:RefNumber
      If glo:Select12 = 0
          Cycle
      End ! If glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ProceduresImportMFTRSubmissions
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresImportMFTRSubmissions, Accepted)
      tmp:SavePath = Path()
      
      If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Found
          SetPath(tmp:SavePath)
      Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Error
          SetPath(tmp:SavePath)
          Cycle
      End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
      
      Open(ImportFile)
      If Error()
          Stop(Error())
          Cycle
      End ! If Error()
      
      Count# = 0
      Set(ImportFile,0)
      Loop
          Next(ImportFile)
          If Error()
              Break
          End ! If Error()
          Count# += 1
      End ! Loop
      
      Do Prog:ProgressSetup
      Prog:TotalRecords = Count#
      Prog:ShowPercentage = 1 !Show Percentage Figure
      Set(ImportFile,0)
      Accept
          Case Event()
          Of Event:Timer
              Loop 25 Times
                  !Inside Loop
                  Next(ImportFile)
                  If Error()
                      Prog:Exit = 1
                      Break
                  End ! If Access:IMPORTFILE.Next()
      
                  Do Prog:UpdateScreen
      
                  Prog:RecordCount += 1
                  ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                  Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
                  Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                  jow:RefNumber = impfil:JobNumber
                  If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                      !Found
                      If jow:Status <> 'NO'
                          LinePrint('Job not at correct status to update: ' & Clip(impfil:JobNumber),'ImportError.log')
                      Else ! If jow:Status <> 'NO'
                          Access:JOBSE.ClearKey(jobe:RefNumberKey)
                          jobe:RefNumber = jow:RefNumber
                          If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                              !Found
                              If jobe:WarrantyClaimStatus <> 'SUBMITTED TO MFTR'
                                  jobe:WarrantyClaimStatus = 'SUBMITTED TO MFTR'
                                  Access:JOBSE.Update()
                                  If AddToAudit(jow:RefNumber,'JOB','WARRANTY CLAIM SUBMITTED TO MFTR','')
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM SUBMITTED TO MFTR','')
                              End ! If jobe:WarrantyClaimStatus <> 'ON TECHNICAL REPORT'
                          Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                          End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      
                      End ! If jow:Status <> 'NO'
                  Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                      !Error
                      LinePrint('Cannot Find Job Number: ' & Clip(impfil:JobNumber),'ImportError.log')
                  End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
      
                  Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
              End ! Loop 25 Times
          Of Event:CloseWindow
              Prog:Exit = 1
              Prog:Cancelled = 1
              Break
          Of Event:Accepted
              If Field() = ?Prog:Cancel
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                  Of 1 ! Yes
                      Prog:Exit = 1
                      Prog:Cancelled = 1
                      Break
                  Of 2 ! No
                  End ! Case Message
              End! If FIeld()
          End ! Case Event()
          If Prog:Exit
              Break
          End ! If Prog:Exit
      End ! Accept
      Do Prog:ProgressFinished
      Close(ImportFile)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresImportMFTRSubmissions, Accepted)
    OF ?ProceduresImportMFTRRemittance
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresImportMFTRRemittance, Accepted)
      Do ImportMFTRRemittance
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresImportMFTRRemittance, Accepted)
    OF ?ProceduresFinalRejectionRoutine
      ThisWindow.Update
      AutoFinalRejection
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresFinalRejectionRoutine, Accepted)
      Do RefreshBrowse
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProceduresFinalRejectionRoutine, Accepted)
    OF ?UtilitiesRebuildList
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UtilitiesRebuildList, Accepted)
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('Rebuild Process.'&|
              '|'&|
              '|Only use if instructed to do so.','ServiceBase',|
                         'mexclam.jpg','\&Cancel|/&Continue') 
          Of 2 ! &Continue Button
              Do Prog:ProgressSetup
              Prog:TotalRecords = Records(JOBSWARR)
              Prog:ShowPercentage = 1 !Show Percentage Figure
      
              Access:JOBSWARR.Clearkey(jow:RecordNumberKey)
              jow:RecordNumber = 1
              Set(jow:RecordNumberKey,jow:RecordNumberKey)
              Loop Until Access:JOBSWARR.Next()
                  Do Prog:UpdateScreen
      
                  Prog:RecordCount += 1
                  ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number = jow:RefNumber
                  If (Access:JOBS.TryFetch(job:Ref_Number_Key))
                      Access:JOBSWARR.DeleteRecord(0)
                  End
      
                  Access:WEBJOB.Clearkey(wob:RefNumberKey)
                  wob:RefNumber = jow:RefNumber
                  If (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                      Access:JOBSWARR.DeleteRecord(0)
                  End
      
                  Access:JOBSE.Clearkey(jobe:RefNumberKey)
                  jobe:RefNumber = jow:RefNumber
                  If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                      Cycle
                  end
      
                  if (jobe:WarrantyCLaimStatus = 'MFTR PAID')
                      if (jow:DateReconciled = 0)
                          Access:AUDIT.Clearkey(aud:TypeActionKey)
                          aud:Ref_Number = jow:RefNumber
                          aud:Type = 'JOB'
                          aud:Action = 'WARRANTY CLAIM RECONCILED'
                          Set(aud:TypeActionKey,aud:TypeActionKey)
                          Loop ! Begin Loop
                              If Access:AUDIT.Next()
                                  Break
                              End ! If Access:AUDIT.Next()
                              If aud:Ref_Number <> jow:RefNumber
                                  Break
                              End ! If aud:Ref_Number <> jow:RefNumber
                              If aud:Type <> 'JOB'
                                  Break
                              End ! If aud:Type = 'JOB'
                              If aud:Action <> 'WARRANTY CLAIM RECONCILED'
                                  Break
                              End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                              jow:DateReconciled = aud:Date
                              Access:JOBSWARR.TryUpdate()
                              Break
                          End ! Loop
      
                     End ! If jow:DateReconciled <> ''
                  End ! if (jobe:WarrantyCLaimStatus = 'MFTR PAID')
      
      
                  If (jow:RRCStatus = 'EXC')
                      if (jow:DateRejected = 0)
                          Access:AUDIT.Clearkey(aud:TypeActionKey)
                          aud:Ref_Number = jow:RefNumber
                          aud:Type = 'JOB'
                          aud:Action = 'WARRANTY CLAIM REJECTED'
                          Set(aud:TypeActionKey,aud:TypeActionKey)
                          Loop ! Begin Loop
                              If Access:AUDIT.Next()
                                  Break
                              End ! If Access:AUDIT.Next()
                              If aud:Ref_Number <> jow:RefNumber
                                  Break
                              End ! If aud:Ref_Number <> jow:RefNumber
                              If aud:Type <> 'JOB'
                                  Break
                              End ! If aud:Type = 'JOB'
                              If aud:Action <> 'WARRANTY CLAIM REJECTED'
                                  Break
                              End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                              jow:DateRejected = aud:Date
                              Access:JOBSWARR.TryUpdate()
                              break
                          End ! Loop
                      end ! if (jow:DateRejected = 0)
                  End ! If (jow:RRCStatus = 'EXC')
      
                  If (jow:RRCStatus = 'AAJ')
                      If (jow:DateFinalRejection = 0)
                          Access:AUDIT.Clearkey(aud:TypeActionKey)
                          aud:Ref_Number = jow:RefNumber
                          aud:Type = 'JOB'
                          aud:Action = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
                          Set(aud:TypeActionKey,aud:TypeActionKey)
                          Loop ! Begin Loop
                              If Access:AUDIT.Next()
                                  Break
                              End ! If Access:AUDIT.Next()
                              If aud:Ref_Number <> jow:RefNumber
                                  Break
                              End ! If aud:Ref_Number <> jow:RefNumber
                              If aud:Type <> 'JOB'
                                  Break
                              End ! If aud:Type = 'JOB'
                              If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
                                  Break
                              End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                              jow:DateFinalRejection = aud:Date
                              Access:JOBSWARR.TryUpdate()
                              break
                          End ! Loop
                      End ! If (jow:DateFinalRejection = 0)
                  End ! If (jow:RRCStatus = 'AAJ')
      
                  If (jow:RRCStatus = 'REJ')
                      If (jow:DateFinalRejection = 0)
                          Access:AUDIT.Clearkey(aud:TypeActionKey)
                          aud:Ref_Number = jow:RefNumber
                          aud:Type = 'JOB'
                          aud:Action = 'WARRANTY CLAIM FINAL REJECTION'
                          Set(aud:TypeActionKey,aud:TypeActionKey)
                          Loop ! Begin Loop
                              If Access:AUDIT.Next()
                                  Break
                              End ! If Access:AUDIT.Next()
                              If aud:Ref_Number <> jow:RefNumber
                                  Break
                              End ! If aud:Ref_Number <> jow:RefNumber
                              If aud:Type <> 'JOB'
                                  Break
                              End ! If aud:Type = 'JOB'
                              If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
                                  Break
                              End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                              jow:DateFinalRejection = aud:Date
                              Access:JOBSWARR.TryUpdate()
                              break
                          End ! Loop
                      End ! If (jow:DateFinalRejection = 0)
                  End ! If (jow:RRCStatus = 'REJ')
              End
              Do Prog:ProgressFinished
      
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Missive('Rebuild Process.'&|
                  '|'&|
                  '|Completed.','ServiceBase',|
                             'midea.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
      
          Of 1 ! &Cancel Button
          End!Case Message
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UtilitiesRebuildList, Accepted)
    OF ?tmp:LimitByDate
      IF ?tmp:LimitByDate{Prop:Checked} = True
        HIDE(?jow:RefNumber)
        ENABLE(?Group2)
      END
      IF ?tmp:LimitByDate{Prop:Checked} = False
        UNHIDE(?jow:RefNumber)
        DISABLE(?Group2)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LimitByDate, Accepted)
      Do RefreshDate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LimitByDate, Accepted)
    OF ?Button:PreviousUnitHistory
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PreviousUnitHistory, Accepted)
      Brw9.UpdateViewRecord()
      If jow:RefNumber > 0
          If GetTempPathA(255,tmp:TempFilePath)
              If Sub(tmp:TempFilePath,-1,1) = '\'
                  glo:FileName = Clip(tmp:TempFilePath) & 'JOBBOUNCER' & Clock() & '.TMP'
              Else !If Sub(TempFilePath,-1,1) = '\'
                  glo:FileName = Clip(tmp:TempFilePath) & '\JOBBOUNCER' & Clock() & '.TMP'
              End !If Sub(TempFilePath,-1,1) = '\'
          End
          x# = BrowseJobBouncers('V')
          Remove(glo:FileName)
      End ! If jow:RefNumber > 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PreviousUnitHistory, Accepted)
    OF ?tmp:FirstSecondYear
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FirstSecondYear, Accepted)
      Do RefreshBrowse
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW9.ApplyRange
      BRW9.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FirstSecondYear, Accepted)
    OF ?Button:ValuateClaims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ValuateClaims, Accepted)
      Do ValuateClaims
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ValuateClaims, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:PrintSHRPending
      ThisWindow.Update
      PrintTechnicalReport(tmp:Manufacturer,1,'NO')
      ThisWindow.Reset
    OF ?Button:PrintTechnicalReports
      ThisWindow.Update
      PrintTechnicalReport(tmp:Manufacturer)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PrintTechnicalReports, Accepted)
      BRW9.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:PrintTechnicalReports, Accepted)
    OF ?Button:PrintSHRQuery
      ThisWindow.Update
      PrintTechnicalReport(tmp:Manufacturer,1,'QUE')
      ThisWindow.Reset
    OF ?Button:ResubmitClaim
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ResubmitClaim, Accepted)
      If Records(glo:Queue) = 0
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('You not have tagged any jobs.','ServiceBase 3g',|
                         'mstop.jpg','&OK')
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If Records(glo:Queue) = 0
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('Are you sure you want to resubmit the tagged jobs?','ServiceBase 3g',|
                     'mquest.jpg','&No|&Yes')
          Of 2 ! &Yes Button
              glo:EDI_Reason = ''
              Get_EDI_Reason
              If Clip(glo:EDI_Reason) <> ''
                  Do Prog:ProgressSetup
                  Prog:TotalRecords = Records(GLO:QUEUE)
                  Prog:ShowPercentage = 1 !Show Percentage Figure
                  
                  x# = 0
                  Accept
                      Case Event()
                      Of Event:Timer
                          Loop 25 Times
                              !Inside Loop
                              x# += 1
                              Get(glo:Queue,x#)
                              If Error()
                                  Prog:Exit = 1
                                  Break
                              End ! If Access:GLO:QUEUE.Next()
      
                              Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                              jow:RefNumber = glo:Pointer
                              If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                  !Found
                                  Access:JOBS.ClearKey(job:Ref_Number_Key)
                                  job:Ref_Number = jow:RefNumber
                                  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                      !Found
                                      If JobInUse(job:Ref_Number,0)
                                          Beep(Beep:SystemHand)  ;  Yield()
                                          Case Missive('Cannot update Job Number ' & Clip(job:Ref_Number) & '.'&|
                                              '|'&|
                                              '|It is in use by another user.','ServiceBase 3g',|
                                                         'mstop.jpg','&OK')
                                              Of 1 ! &OK Button
                                          End!Case Message
                                          Cycle
                                      End ! If JobInUse(job:Ref_Number,0)
                                  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                      !Error
                                      Cycle
                                  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      
                              Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                                  !Error
                                  Cycle
                              End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
      
                              job:EDI = 'NO'
                              If Access:JOBS.TryUpdate() = Level:Benign
                                  jow:Status = 'NO'
                                  jow:Submitted += 1
                                  jow:ClaimSubmitted = Today()
                                  If Access:JOBSWARR.TryUpdate() = Level:Benign
                                      Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                                      jobe2:RefNumber = job:Ref_Number
                                      If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                          !Found
                                          jobe2:InPendingDate = Today()
                                          Access:JOBSE2.TryUpdate()
                                      Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                          !Error
                                          If Access:JOBSE2.PrimeRecord() = Level:Benign
                                              jobe2:RefNumber = job:Ref_Number
                                              jobe2:InPendingDate = Today()
                                              If Access:JOBSE2.TryInsert() = Level:Benign
                                                  !Insert
                                              Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                                                  Access:JOBSE2.CancelAutoInc()
                                              End ! If Access:JOBSE2.TryInsert() = Level:Benign
                                          End ! If Access.JOBSE2.PrimeRecord() = Level:Benign
                                      End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                  End ! If Access:JOBSWARR.TryUpdate() = Level:Benign
      
                                  Access:WEBJOB.ClearKey(wob:RefNumberKey)
                                  wob:RefNumber = job:Ref_Number
                                  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                      !Found
                                      wob:OracleExportNumber = 0
                                      wob:EDI = 'NO'
                                      Access:WEBJOB.TryUpdate()
                                  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                      !Error
                                  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: '& Clip(Glo:EDI_Reason))
      
                                  End!If Access:AUDIT.PrimeRecord() = Level:Benign
      
                                  Access:JOBNOTES.ClearKey(jbn:RefNumberKEy)
                                  jbn:RefNumber = job:Ref_Number
                                  If Access:JOBNOTES.TryFetch(jbn:RefNumberKEy) = Level:Benign
                                      !Found
                                      jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>' & Clip(glo:EDI_Reason)
                                      Access:JOBNOTES.Update()
                                  Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKEy) = Level:Benign
                                    !Error
                                  End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKEy) = Level:Benign
      
                                  Access:JOBSE.ClearKey(jobe:RefNumberKEy)
                                  jobe:RefNumber = job:Ref_Number
                                  If Access:JOBSE.TryFetch(jobe:RefNumberKEy) = Level:Benign
                                      !Found
                                      jobe:WarrantyClaimStatus = 'RESUBMITTED'
                                      jobe:WarrantyStatusDate = Today()
                                      Access:JOBSE.Update()
                                  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKEy) = Level:Benign
                                    !Error
                                  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKEy) = Level:Benign
                              End ! If Access:JOBS.TryUpdate() = Level:Benign
      
                              Do Prog:UpdateScreen
      
                              Prog:RecordCount += 1
                              ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
                          End ! Loop 25 Times
                      Of Event:CloseWindow
                          Prog:Exit = 1
                          Prog:Cancelled = 1
                          Break
                      Of Event:Accepted
                          If Field() = ?Prog:Cancel
                              Beep(Beep:SystemQuestion)  ;  Yield()
                              Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                              Of 1 ! Yes
                                  Prog:Exit = 1
                                  Prog:Cancelled = 1
                                  Break
                              Of 2 ! No
                              End ! Case Message
                          End! If FIeld()
                      End ! Case Event()
                      If Prog:Exit
                          Break
                      End ! If Prog:Exit
                  End ! Accept
                  Do Prog:ProgressFinished
      
              End ! If Clip(glo:EDI_Reason) <> ''
          Of 1 ! &No Button
      End!Case Message
      brw9.ResetSort(1)
      Post(Event:Accepted,?DASUNTAGALL)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ResubmitClaim, Accepted)
    OF ?Button:PrintSHRRejected
      ThisWindow.Update
      PrintTechnicalReport(tmp:Manufacturer,1,'EXC')
      ThisWindow.Reset
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::14:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button:FinalRejection
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FinalRejection, Accepted)
      brw9.UpdateViewRecord()
      
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = jow:RefNumber
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If JobInUse(job:Ref_Number,1)
              Cycle
          End ! If JobInUse(job:Ref_Number,1)
      Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
          Cycle
      End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      
      
      Case Missive('This procedure will mark the selected job as "Finally Rejected By Vetting".'&|
        '|Are you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
          Of 1 ! No Button
              Cycle
      End ! Case Missive
      
      glo:EDI_Reason = 'EXPIRED DATE RANGE'
      Get_EDI_Reason
      If Clip(glo:EDI_Reason) <> ''
              !Found
          Access:WEBJOB.ClearKey(wob:RefNumberKey)
          wob:RefNumber = job:Ref_Number
          If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
              !Found
          Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
              !Error
              Cycle
          End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
          job:EDI_Batch_Number = 0
          job:EDI = 'REJ'
          wob:EDI = 'REJ'
          wob:OracleExportNumber = 0
          
          If Sub(job:Current_Status,1,3) = 'JOB'
              GetStatus(910,0,'JOB')
          End ! If Sub(job:Current_Status,1,3) = 'JOB'
          If Access:JOBS.Update() = Level:Benign
              If Access:WEBJOB.Update() = Level:Benign
                  jow:Status = 'REJ'
                  ! Insert --- Update the RRC Status (DBH: 27/05/2009) #108383
                  jow:RRCStatus = wob:EDI
                  ! end --- (DBH: 27/05/2009) #108383
                  
                  jow:DateFinalRejection = Today()    ! #11505 Record date of final rejection (DBH: 21/07/2010)
      
                  If Access:JOBSWARR.Update() = Level:Benign
                      If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: ' & Clip(glo:EDI_Reason))
      
                      End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: ' & Clip(glo:EDI_Reason))
      
                      Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                      jbn:RefNumber = job:Ref_Number
                      If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                          !Found
                          jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>' & Clip(glo:EDI_Reason)
                          Access:JOBNOTES.Update()
                      Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                          !Error
                      End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
      
                      Access:JOBSE.ClearKey(jobe:RefNumberKey)
                      jobe:RefNumber = job:Ref_Number
                      If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                          !Found
                          jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                          jobe:WarrantyStatusDate = Today()
                          Access:JOBSE.Update()
                      Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      
                  End ! If Access:JOBSWARR.Update() = Level:Benign
              End ! If Access:WEBJOB.Update() = Level:Benign
      
          End ! If Access:JOBS.Update() = Level:Benign
      End ! If Clip(glo:EDI_Reason) <> ''
      Post(Event:Accepted,?DASUNTAGALL)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:FinalRejection, Accepted)
    OF ?Button:PrintSHRAccepted
      ThisWindow.Update
      PrintTechnicalReport(tmp:Manufacturer,1,'YES')
      ThisWindow.Reset
    OF ?Button:AutoReconcileJobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AutoReconcileJobs, Accepted)
      Do ReconcileJobs
      
      Do RefreshBrowse
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AutoReconcileJobs, Accepted)
    OF ?Button:ReconcileJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ReconcileJob, Accepted)
      Do ReconcileSingleJob
      Do RefreshBrowse
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ReconcileJob, Accepted)
    OF ?Button:PrintSHRREconciled
      ThisWindow.Update
      PrintTechnicalReport(tmp:Manufacturer,1,'FIN')
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020719'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020719'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020719'&'0')
      ***
    OF ?tmp:LimitMonth
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LimitMonth, Accepted)
      Do RefreshDate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LimitMonth, Accepted)
    OF ?tmp:LimitYear
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LimitYear, Accepted)
      If tmp:LimitYear < 2000
          tmp:LimitYear = 2000
      End ! If tmp:LimitYear < 2000
      If tmp:LimitYear > Year(Today())
          tmp:LimitYear = Year(Today())
      End ! If tmp:LimitYear > Year(Today())
      Display()
      Do RefreshDate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LimitYear, Accepted)
    OF ?Button:AuditTrail
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
      glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
    OF ?Button:ViewJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ViewJob, Accepted)
      Brw9.UpdateViewRecord()
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = jow:RefNumber
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Request# = GlobalRequest
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
          GlobalRequest = Request#
      End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      Do RefreshBrowse
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ViewJob, Accepted)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      Do RefreshBrowse
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW9.ApplyRange
      BRW9.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?CallLookup
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?CurrentTab
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?CurrentTab)
    Case Choice(?CurrentTab)
    Of 1
        tmp:EDIFlag = 'NO'
    Of 2
        tmp:EDIFlag = 'QUE'
    Of 3
        tmp:EDIFlag = 'EXC'
    Of 4
        tmp:EDIFlag = 'YES'
    Of 5
        tmp:EDIFlag = 'FIN'
    End ! Case Choice(?CurrentTab)
    Do RefreshBrowse
    CASE EVENT()
    OF EVENT:TabChanging
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, TabChanging)
      Post(Event:Accepted,?DASUNTAGALL)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, TabChanging)
    END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?CurrentTab)
  OF ?Browse:1
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
!Previous IMEI
    Found# = 0
    If Clip(job:ESN) <> '' And job:ESN <> 'N/A'
        Access:JOBS_ALIAS.Clearkey(job_ali:ESN_Key)
        job_ali:ESN = job:ESN
        Set(job_ali:ESN_Key,job_ali:ESN_Key)
        Loop ! Begin Loop
            If Access:JOBS_ALIAS.Next()
                Break
            End ! If Access:JOBS_ALIAS.Next()
            If job_ali:ESN <> job:ESN
                Break
            End ! If job_ali:ESN <> job:ESN
            If job_ali:Ref_Number <> job:Ref_Number
                Found# = 1
                Break
            End ! If job_ali:Ref_Number <> job:Ref_Number
        End ! Loop
    End ! If CLip(job:ESN) <> '' And job:ESN <> 'N/A'
    If Found# = 0
        If ?Button:PreviousUnitHistory{prop:Hide} = 0
            ?Button:PreviousUnitHistory{prop:Hide} = 1
            ?Prompt:IMEIHistory{prop:Hide} = 1
        End ! If ?Button:PreviousUnitHistory{prop:Hide} = 0
    Else ! If Found# = 0
        If ?Button:PreviousUnitHistory{prop:Hide} = 1
            ?Button:PreviousUnitHistory{prop:Hide} = 0
            ?Prompt:IMEIHistory{prop:Hide} = 0
        End ! If ?Button:PreviousUnitHistory{prop:Hide} = 1
    End ! If Found# = 0

    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If KeyCode() = MouseLeft2
          Post(Event:Accepted,?Button:ViewJob)
      End ! If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='36R(2)|M~Job No~@s8@#3#14C|M~ID~@s2@#4#36C(2)|M~Repaired~@s3@#6#52L(2)|M~Head Account~@s30@#7#64L(2)|M~Account Number~@s15@#8#44R(2)|M~Completed~@D6b@#9#40L(2)|M~Orig Sub Date~@d17@#19#44R(2)|M~Submitted~@d6@#10#92L(2)|M~Charge Type~@s30@#11#92L(2)|M~Repair Type~@s30@#12#72L(2)|M~Claim Status~@s40@#13#'
          ?Tab1{PROP:TEXT} = 'Browse Pending Claims'
        OF 2
          ?Browse:1{PROP:FORMAT} ='36R(2)|M~Job No~@s8@#3#14C|M~ID~@s2@#4#36C(2)|M~Repaired~@s3@#6#52L(2)|M~Head Account~@s30@#7#64L(2)|M~Account Number~@s15@#8#44R(2)|M~Completed~@D6b@#9#40L(2)|M~Orig Sub Date~@d17@#19#44R(2)|M~Submitted~@d6@#10#92L(2)|M~Charge Type~@s30@#11#92L(2)|M~Repair Type~@s30@#12#72L(2)|M~Claim Status~@s40@#13#'
          ?Tab2{PROP:TEXT} = 'Query Claims'
        OF 3
          ?Browse:1{PROP:FORMAT} ='11L(2)J@s1@#1#36R(2)|M~Job No~@s8@#3#14C|M~ID~@s2@#4#36C(2)|M~Repaired~@s3@#6#52L(2)|M~Head Account~@s30@#7#64L(2)|M~Account Number~@s15@#8#44R(2)|M~Completed~@D6b@#9#40L(2)|M~Orig Sub Date~@d17@#19#44R(2)|M~Submitted~@d6@#10#92L(2)|M~Charge Type~@s30@#11#92L(2)|M~Repair Type~@s30@#12#72L(2)|M~Claim Status~@s40@#13#'
          ?Tab3{PROP:TEXT} = 'Rejected Claims'
        OF 4
          ?Browse:1{PROP:FORMAT} ='36R(2)|M~Job No~@s8@#3#14C|M~ID~@s2@#4#36C(2)|M~Repaired~@s3@#6#52L(2)|M~Head Account~@s30@#7#64L(2)|M~Account Number~@s15@#8#44R(2)|M~Completed~@D6b@#9#40L(2)|M~Orig Sub Date~@d17@#19#44R(2)|M~Submitted~@d6@#10#92L(2)|M~Charge Type~@s30@#11#92L(2)|M~Repair Type~@s30@#12#72L(2)|M~Claim Status~@s40@#13#'
          ?Tab4{PROP:TEXT} = 'Accepted Claims'
        OF 5
          ?Browse:1{PROP:FORMAT} ='36R(2)|M~Job No~@s8@#3#14C|M~ID~@s2@#4#36C(2)|M~Repaired~@s3@#6#52L(2)|M~Head Account~@s30@#7#64L(2)|M~Account Number~@s15@#8#44R(2)|M~Completed~@D6b@#9#40L(2)|M~Orig Sub Date~@d17@#19#44R(2)|M~Submitted~@d6@#10#92L(2)|M~Charge Type~@s30@#11#92L(2)|M~Repair Type~@s30@#12#72L(2)|M~Claim Status~@s40@#13#48R(2)|M~Reconciled~@d6@#14#'
          ?Tab5{PROP:TEXT} = 'Reconciled Claims'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::14:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
local.InvoiceSetup    Procedure(*Real f:LabourVatRate,*Real f:PartsVatRate)
Code
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = tmp:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If Sub(Clip(man:EDI_Path),-1,1) <> '\'
           man:EDI_Path = Clip(man:EDI_Path) & '\'
        End ! Backslash Check
    Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        Return True
    End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = man:Trade_Account
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        !Error
        Case Missive('Error! A trade account has not been allocated to this manufacturer.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return True
    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = sub:Main_Account_Number
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Found
    Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        !Error
        Case Missive('Error! A trade account has not been allocated to this manufacturer.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        Return True
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

    If tra:Invoice_Sub_Accounts = 'YES'
        Access:VATCODE.ClearKey(vat:Vat_Code_Key)
        vat:Vat_Code = sub:Labour_Vat_Code
        If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            f:LabourVatRate = vat:Vat_Rate
        Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Error
            Case Missive('Error! V.A.T. rates have not been set-up for this trade account.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Return True
        End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

        Access:VATCODE.ClearKey(vat:Vat_Code_Key)
        vat:Vat_Code = sub:Parts_Vat_Code
        If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            f:PartsVatRate = vat:Vat_Rate
        Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Error
            Case Missive('Error! V.A.T. rates have not been set-up for this trade account.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Return True
        End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

    Else ! If tra:Invoice_Sub_Accounts = 'YES'
        Access:VATCODE.ClearKey(vat:Vat_Code_Key)
        vat:Vat_Code = tra:Labour_Vat_Code
        If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            f:LabourVatRate = vat:Vat_Rate
        Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Error
            Case Missive('Error! V.A.T. rates have not been set-up for this trade account.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Return True
        End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign

        Access:VATCODE.ClearKey(vat:Vat_Code_Key)
        vat:Vat_Code = tra:Parts_Vat_Code
        If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Found
            f:PartsVatRate = vat:Vat_Rate
        Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
            !Error
            Case Missive('Error! V.A.T. rates have not been set-up for this trade account.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Return True
        End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
    End ! If tra:Invoice_Sub_Accounts = 'YES'

    Return False
local.InvoiceJob    Procedure(Long f:JobNumber,Real f:Lab,Real f:Par,Real f:Other,Real f:Sub,Real f:VAT,Real f:Total,String f:Invoice,String f:Reason)
Code
    Access:JOBS.ClearKey(job:Ref_Number_Key)
    job:Ref_Number = f:JobNumber
    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Found
        If JobInUse(job:Ref_Number,0)
            LinePrint(Format(TOday(),@d06) & ' ' & Format(Clock(),@t04) & ': Job not reconciled. Job in use: ' & Clip(recfil:JobNumber),Clip(man:EDI_Path) & 'ReconcileJobs.log')
            Return True
        Else ! If JobInUse(job:Ref_Number,0)
            job:EDI = 'FIN'
            job:Invoice_Number_Warranty = inv:Invoice_Number
            job:WInvoice_Courier_Cost = job:Courier_Cost_Warranty
            job:WInvoice_Labour_Cost = job:Labour_Cost_Warranty
            job:WInvoice_Parts_Cost = job:Parts_Cost_Warranty
            job:WInvoice_Sub_Total = job:Sub_Total_Warranty
            job:Invoice_Date_Warranty = Today()
            job:Paid_Warranty = 'YES'
            If job:Chargeable_Job <> 'YES' Or (job:Chargeable_Job = 'YES' And job:Invoice_Number > 0)
                ! Do not change the status if exchange attached (DBH: 21/02/2007)
                If job:Exchange_Unit_Number = 0
! Delete --- Don't use this status, as consingment is set when returning from ARC (DBH: 14/01/2010) #11254
!                    If job:Consignment_Number <> ''
!                        GetStatus(910,0,'JOB')
!                    End ! If job:Consignment_Number <> ';
! end --- (DBH: 14/01/2010) #11254
                    if (sub(job:Current_Status,1,3) = '905')
                        GetStatus(910,0,'JOB')
                    end !if (sub(job:Current_Status,1,3) = '905')
                Else ! If job:Exchange_Unit_Number = 0
! Delete --- No other status changes (DBH: 02/02/2010) #11254
!                    If job:Loan_Unit_Number > 0
!                        GetStatus(811,0,'JOB')
!                    Else ! If job:Loan_Unit_Number > 0
!                        GetStatus(810,0,'JOB')
!                    End ! If job:Loan_Unit_Number > 0
! end --- (DBH: 02/02/2010) #11254
                End ! If job:Exchange_Unit_Number = 0
            End ! If job:Chargeable_Job <> 'YES' Or (job:Chargeable_Job = 'YES' And job:Invoice_Number > 0)
            If Access:JOBS.TryUpdate() = Level:Benign
                jow:Status = 'FIN'
                jow:DateReconciled = Today()

                If Access:JOBSWARR.TryUpdate() = Level:Benign
                    Access:JOBSE.ClearKey(jobe:RefNumberKey)
                    jobe:RefNumber = job:Ref_Number
                    If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        jobe:InvoiceClaimValue = jobe:ClaimValue
                        jobe:InvoiceHandlingFee = jobe:HandlingFee
                        jobe:InvoiceExchangeRate = jobe:ExchangeRate
                        jobe:InvClaimPartsCost = jobe:ClaimPartsCost
                        jobe:WarrantyClaimStatus = 'MFTR PAID'
                        jobe:WarrantyStatusDate = Today()
                        If Access:JOBSE.TryUpdate() = Level:Benign
                            Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                            jobe2:RefNumber = job:Ref_Number
                            If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                !Found
                                jobe2:WLabourPaid = f:Lab
                                jobe2:WPartsPaid = f:Par
                                jobe2:WOtherCosts = f:Other
                                jobe2:WSubTotal = f:Sub
                                jobe2:WVAT = f:Vat
                                jobe2:WTotal = f:Total
                                jobe2:WInvoiceRefNo = f:Invoice
                                jobe2:WarrantyReason = f:Reason
                                IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))
                                    ! #12363 Reget the Manufacturer Value. 
                                    ! This should cope with any existing records (DBH: 17/02/2012)
                                    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                                    man:Manufacturer = job:Manufacturer
                                    IF (Access:MANUFACT.Tryfetch(Man:Manufacturer_Key) = Level:Benign)
                                        jobe2:ThirdPartyHandlingFee = man:ThirdPartyHandlingFee
                                    END ! IF (Access:MANUFACT.Tryfetch(Man:Manufacturer_Key) = Level:Benign)
                                    jobe2:InvThirdPartyHandlingFee = jobe2:ThirdPartyHandlingFee  ! #12363 Save third party value (DBH: 17/02/2012)
                                END ! IF (_PreClaimThirdParty(job:Third_Party_Site,job:Manufacturer))
                                If Access:JOBSE2.TryUpdate() = LEvel:Benign
                                    If f:Reason <> ''
                                        If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RECONCILED','INVOICE NUMBER: ' & CLip(inv:invoice_number) & |
                                                                        '<13,10>MANUAL REASON: ' & Clip(jobe2:WarrantyReason) & |
                                                                        '<13,10>LABOUR PAID: ' & Format(jobe2:WLabourPaid,@n14.2) & |
                                                                        '<13,10>PARTS PAID: ' & Format(jobe2:WPartsPaid,@n14.2) & |
                                                                        '<13,10>OTHER COSTS: ' & Format(jobe2:WOtherCosts,@n14.2) & |
                                                                        '<13,10>VAT: ' & Format(jobe2:WVAT,@n14.2) & |
                                                                        '<13,10>TOTAL: ' & Format(jobe2:WTotal,@n14.2) )
                                        End ! 'BATCH NUMBER: ' & CLip(inv:batch_number))

                                    Else ! If f:Reason <> ''
                                        If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RECONCILED','INVOICE NUMBER: ' & CLip(inv:invoice_number) & |
                                                                        '<13,10>INV REF NO: ' & Clip(jobe2:WInvoiceRefNo) & |
                                                                        '<13,10>LABOUR PAID: ' & Format(jobe2:WLabourPaid,@n14.2) & |
                                                                        '<13,10>PARTS PAID: ' & Format(jobe2:WPartsPaid,@n14.2) & |
                                                                        '<13,10>OTHER COSTS: ' & Format(jobe2:WOtherCosts,@n14.2) & |
                                                                        '<13,10>VAT: ' & Format(jobe2:WVAT,@n14.2) & |
                                                                        '<13,10>TOTAL: ' & Format(jobe2:WTotal,@n14.2) )
                                        End ! 'BATCH NUMBER: ' & CLip(inv:batch_number))

                                    End ! If f:Reason <> ''
                                End ! If Access:JOBSE2.TryUpdate() = LEvel:Benign
                            Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                                !Error
                                
                            End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign

                        End ! If Access:JOBSE.TryUpdate() = Level:Benign
                    Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                End ! If Access:JOBSWARR.TryUpdate() = Level:Benign
            End ! If Access:JOBS.TryUpdate() = Level:Benign
        End ! If JobInUse(job:Ref_Number,0)
    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
        !Error

    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

    Return False
cRollBack.init    PROCEDURE()
    CODE
        ! Save data to revert incase of errors
        SELF.jobEDI = job:EDI
        SELF.jobEDI_Batch_Number = job:EDI_Batch_Number
        SELF.wobEDI = wob:EDI
        SELF.wobOracleExportNumber = wob:OracleExportNumber
        SELF.jobeWarrantyClaimStatus = jobe:WarrantyClaimStatus
        

cRollBack.kill  PROCEDURE()
    CODE
        SELF.jobEDI = ''
        SELF.jobEDI_Batch_Number = ''
        SELF.wobEDI = ''
        SELF.jobeWarrantyClaimStatus = ''
        SELF.wobOracleExportNumber = ''

cRollBack.revert    PROCEDURE(BYTE fjobs,<BYTE fwebjob>,<BYTE fjobse>)
    CODE
        ! Set which files to revert
        IF (fjobs = 1)
            job:EDI = SELF.jobEDI
            job:EDI_Batch_Number = SELF.jobEDI_Batch_Number
            Access:JOBS.TryUpdate()
        END
        IF (fwebjob = 1)
            wob:EDI = SELF.wobEDI
            wob:OracleExportNumber = SELF.wobOracleExportNumber
            Access:WEBJOB.TryUpdate()
        END
        IF (fjobse = 1)
            jobe:WarrantyClaimStatus = SELF.jobeWarrantyClaimStatus
            Access:JOBSE.TryUpdate()
        END
        LinePrint(Format(TOday(),@d06) & ' ' & Format(Clock(),@t04) & ': Job Failed To Update: ' & Clip(impfil:FieldA),Clip(man:EDI_Path) & 'ImportMFTRRemittance.log')
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        IF ?tmp:LimitMonth{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:LimitMonth{prop:Use} = DBHControl{prop:Use}
        IF ?tmp:LimitYear{prop:Feq} = DBHControl{prop:Feq}
            Cycle
        End ! IF ?tmp:LimitYear{prop:Use} = DBHControl{prop:Use}
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW9.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF tmp:FirstSecondYear = 2 And tmp:LimitByDate = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:EDIFlag
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
  ELSIF tmp:FirstSecondYear <> 2 And tmp:LimitByDate = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:EDIFlag
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FirstSecondYear
  ELSIF tmp:FirstSecondYear <> 2 And tmp:LimitByDate = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:EDIFlag
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:FirstSecondYear
  ELSIF tmp:FirstSecondYear = 2 And tmp:LimitByDate = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:EDIFlag
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:EDIFlag
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW9.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  ! Disable buttons if no records (DBH: 16/06/2008)
  If ~Records(Queue:Browse)
      ?Button:ValuateClaims{prop:Disable} = 1
      ?Button:PreviousUnitHistory{prop:Disable} = 1
      ?Button:POPConfirmed{prop:Disable} = 1
      ?Button:PrintTechnicalReports{prop:Disable} = 1
      ?Button:AuditTrail{prop:Disable} = 1
      ?Button:ViewJob{prop:Disable} = 1
      ?Button:PrintSHRQuery{prop:Disable} = 1
      ?Button:PrintSHRRejected{prop:Disable} = 1
      ?Button:ResubmitClaim{prop:Disable} = 1
      ?Button:FinalRejection{prop:Disable} = 1
      ?Button:PrintSHRAccepted{prop:Disable} = 1
      ?Button:ReconcileJob{prop:Disable} = 1
      ?Button:AutoReconcileJobs{prop:Disable} = 1
      ?Button:PrintSHRREconciled{prop:Disable} = 1
      ?Button:PrintSHRPending{prop:Disable} = 1
      ?DasTag{prop:Disable} = 1
      ?DasTagAll{prop:Disable} = 1
      ?DasUntagAll{prop:Disable} = 1
  Else ! If ~Records(Queue:Browse)
  
      ?Button:ValuateClaims{prop:Disable} = 0
      ?Button:PreviousUnitHistory{prop:Disable} = 0
      ?Button:POPConfirmed{prop:Disable} = 0
      ?Button:PrintTechnicalReports{prop:Disable} = 0
      ?Button:AuditTrail{prop:Disable} = 0
      ?Button:ViewJob{prop:Disable} = 0
      ?Button:PrintSHRQuery{prop:Disable} = 0
      ?Button:PrintSHRRejected{prop:Disable} = 0
      ?Button:ResubmitClaim{prop:Disable} = 0
      ?Button:FinalRejection{prop:Disable} = 0
      ?Button:PrintSHRAccepted{prop:Disable} = 0
      ?Button:ReconcileJob{prop:Disable} = 0
      ?Button:AutoReconcileJobs{prop:Disable} = 0
      ?Button:PrintSHRREconciled{prop:Disable} = 0
      ?Button:PrintSHRPending{prop:Disable} = 0
      ?DasTag{prop:Disable} = 0
      ?DasTagAll{prop:Disable} = 0
      ?DasUntagAll{prop:Disable} = 0
  End ! If ~Records(Queue:Browse)
  
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, ResetQueue, (BYTE ResetMode))


BRW9.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:FirstSecondYear = 2 And tmp:LimitByDate = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:FirstSecondYear <> 2 And tmp:LimitByDate = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF tmp:FirstSecondYear <> 2 And tmp:LimitByDate = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF tmp:FirstSecondYear = 2 And tmp:LimitByDate = 0
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jow:RefNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = jow:RefNumber
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = jow:RefNumber
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = jow:RefNumber
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  
  tmp:ClaimStatus = jobe:WarrantyClaimStatus
  
  ! Deleting (DBH 17/06/2008) # 9792 - Don't think this is necessary anymore
  !If tmp:ClaimStatus = 'PENDING'
  !    Found# = 0
  !    Access:AUDIT.Clearkey(aud:TypeActionKey)
  !    aud:Ref_Number = jow:RefNumber
  !    aud:Type = 'JOB'
  !    aud:Action = 'WARRANTY CLAIM'
  !    Set(aud:TypeActionKey,aud:TypeActionKey)
  !    Loop ! Begin Loop
  !        If Access:AUDIT.Next()
  !            Break
  !        End ! If Access:AUDIT.Next()
  !        If aud:Ref_Number <> jow:RefNumber
  !            Break
  !        End ! If aud:Ref_Number <> jow:RefNumber
  !        If aud:Type <> 'JOB'
  !            Break
  !        End ! If aud:Type <> 'JOB'
  !        If aud:Action <> 'WARRANTY CLAIM'
  !            Break
  !        End ! If aud:Action <> 'WARRANTY CLAIM'
  !        If Instring('WARRANTY EDI FILE CREATED',aud:Notes,1,1)
  !            Found# = 1
  !            Break
  !        End ! If Instring('WARRANTY EDI FILE CREATED',aud:Notes,1,1)
  !    End ! Loop
  !    If Found# = 1
  !        tmp:ClaimStatus = 'ON TECHNICAL REPORT'
  !    End ! If Found# = 1
  !End ! If tmp:ClaimStatus = 'PENDING'
  ! End (DBH 17/06/2008) #9792
  
  If tmp:ClaimStatus = 'RESUBMITTED' ! And jow:Submitted > 1
      Count# = 0
      Access:AUDIT.Clearkey(aud:TypeActionKey)
      aud:Ref_Number = jow:RefNumber
      aud:Type = 'JOB'
      aud:Action = 'WARRANTY CLAIM RESUBMITTED'
      Set(aud:TypeActionKey,aud:TypeActionKey)
      Loop ! Begin Loop
          If Access:AUDIT.Next()
              Break
          End ! If Access:AUDIT.Next()
          If aud:Ref_Number <> jow:RefNumber
              Break
          End ! If aud:Ref_Number <> jow:RefNumber
          If aud:Type <> 'JOB'
              Break
          End ! If aud:Type = 'JOB'
          If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
              Break
          End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
          Count# += 1
      End ! Loop
      If Count# > 1
          tmp:ClaimStatus = CLip(tmp:ClaimStatus) & ' x' & Count#
      End ! If Count# > 1
  End ! If tmp:HOClaimStatus = 'RESUBMITTED' And jow:Submitted > 1
  
  If tmp:ClaimStatus = 'REJECTED'
      Count# = 0
      Access:AUDIT.Clearkey(aud:TypeActionKey)
      aud:Ref_Number = jow:RefNumber
      aud:Type = 'JOB'
      aud:Action = 'WARRANTY CLAIM REJECTED'
      Set(aud:TypeActionKey,aud:TypeActionKey)
      Loop ! Begin Loop
          If Access:AUDIT.Next()
              Break
          End ! If Access:AUDIT.Next()
          If aud:Ref_Number <> jow:RefNumber
              Break
          End ! If aud:Ref_Number <> jow:RefNumber
          If aud:Type <> 'JOB'
              Break
          End ! If aud:Type = 'JOB'
          If aud:Action <> 'WARRANTY CLAIM REJECTED'
              Break
          End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
          Count# += 1
      End ! Loop
      If Count# > 1
          tmp:ClaimStatus = CLip(tmp:ClaimStatus) & ' x' & Count#
      End ! If Count# > 1
  End ! If tmp:ClaimStatus = 'REJECTED'
  
  If tmp:ClaimStatus = 'MFTR PAID'
  !    If jow:DateReconciled <> ''
          tmp:DateReconciled = jow:DateReconciled
  !    Else ! If jow:DateReconciled <> ''
  !      Access:AUDIT.Clearkey(aud:TypeActionKey)
  !      aud:Ref_Number = jow:RefNumber
  !      aud:Type = 'JOB'
  !      aud:Action = 'WARRANTY CLAIM RECONCILED'
  !      Set(aud:TypeActionKey,aud:TypeActionKey)
  !      Loop ! Begin Loop
  !          If Access:AUDIT.Next()
  !              Break
  !          End ! If Access:AUDIT.Next()
  !          If aud:Ref_Number <> jow:RefNumber
  !              Break
  !          End ! If aud:Ref_Number <> jow:RefNumber
  !          If aud:Type <> 'JOB'
  !              Break
  !          End ! If aud:Type = 'JOB'
  !          If aud:Action <> 'WARRANTY CLAIM RECONCILED'
  !              Break
  !          End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
  !          tmp:DateReconciled = aud:Date
  !          Break
  !      End ! Loop
  !
  !    End ! If jow:DateReconciled <> ''
  End ! If tmp:ClaimStatus = 'RECONCILED'
  PARENT.SetQueueRecord
  IF (tmp:Tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(9, SetQueueRecord, ())


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jow:RefNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::14:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
AutoFinalRejection   PROCEDURE                        ! Declare Procedure
save_jobswarr_id     USHORT,AUTO
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('pcancel.ico'),HIDE
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBSWARR.Open
   Relate:AUDIT.Open
   Relate:JOBS.Open
   Relate:WEBJOB.Open
   Relate:JOBSE.Open
   Relate:JOBSE2.Open
   Relate:MANUFACT.Open
   Relate:JOBSWARR_ALIAS.Open
    Count# = 0
    Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
    Access:JOBSWARR.Clearkey(jow:StatusManKey)
    jow:Status = 'EXC'
    Set(jow:StatusManKey,jow:StatusManKey)
    Loop ! Begin Loop
        If Access:JOBSWARR.Next()
            Break
        End ! If Access:JOBSWARR.Next()
        If jow:Status <> 'EXC'
            Break
        End ! If jow:Status <> 'EXC'
        Count# += 1
    End ! Loop
    Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)


    Do Prog:ProgressSetup
    Prog:TotalRecords = Count#
    Prog:ShowPercentage = 1 !Show Percentage Figure
    Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
    Access:JOBSWARR.Clearkey(jow:StatusManKey)
    jow:Status = 'EXC'
    Set(jow:StatusManKey,jow:StatusManKey)
    Accept
        Case Event()
        Of Event:Timer
            Loop 25 Times
                !Inside Loop
                If Access:JOBSWARR.Next()
                    Prog:Exit = 1
                    Break
                End ! If Access:JOBSWARR.Next()
                If jow:Status <> 'EXC'
                    Prog:Exit = 1
                    Break
                End ! If jow:Status <> 'EXC'

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = jow:RefNumber
                If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                    If JobInUse(job:Ref_Number,0)
                        Cycle
                    End ! If JobInUse(job:Ref_Number,0)
                Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign


                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                man:Manufacturer = jow:Manufacturer
                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                    !Found
                    If man:UseResubmissionLimit
                        Date# = 0
                        Access:AUDIT.Clearkey(aud:TypeActionKey)
                        aud:Ref_Number = jow:RefNumber
                        aud:Type = 'JOB'
                        aud:Action = 'WARRANTY CLAIM REJECTED'
                        aud:Date = Today()
                        Set(aud:TypeActionKey,aud:TypeActionKey)
                        Loop ! Begin Loop
                            If Access:AUDIT.Next()
                                Break
                            End ! If Access:AUDIT.Next()
                            If aud:Ref_Number <> jow:RefNumber
                                Break
                            End ! If aud:Ref_Number <> jow:RefNumber
                            If aud:Type <> 'JOB'
                                Break
                            End ! If aud:Type <> 'JOB'
                            If aud:Action <> 'WARRANTY CLAIM REJECTED'
                                Break
                            End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
                            Date# = aud:Date
                            Break
                        End ! Loop
                        If Date# > 0
                            If Date# < Today() - man:ResubmissionLimit

                                Access:JOBSWARR_ALIAS.ClearKey(jow_ali:RecordNumberKey)
                                jow_ali:RecordNumber = jow:RecordNumber
                                If Access:JOBSWARR_ALIAS.TryFetch(jow_ali:RecordNumberKey) = Level:Benign
                                    !Found
                                    jow_ali:Status = 'REJ'
                                    ! Insert --- Update the RRC Status (DBH: 27/05/2009) #108383
                                    jow_ali:RRCStatus ='REJ'
                                    ! end --- (DBH: 27/05/2009) #108383
                                    jow_ali:DateFinalRejection = Today()    ! #11505 Record date of final rejection (DBH: 21/07/2010)
                                    If Access:JOBSWARR_ALIAS.TryUpdate()
                                        Cycle
                                    End ! If Access:JOBSWARR_ALIAS.TryUpdate()
                                Else ! If Access:JOBSWARR_ALIAS.TryFetch(jow_ali:RecordNumberKey) = Level:Benign
                                    !Error
                                End ! If Access:JOBSWARR_ALIAS.TryFetch(jow_ali:RecordNumberKey) = Level:Benign

                                Access:WEBJOB.ClearKey(wob:RefNumberKey)
                                wob:RefNumber = job:Ref_Number
                                If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                    !Found
                                Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                    !Error
                                    Cycle
                                End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                                job:EDI_Batch_Number = 0
                                job:EDI = 'REJ'
                                wob:EDI = 'REJ'
                                wob:OracleExportNumber = 0
                                If Access:JOBS.TryUpdate() = Level:Benign
                                    If Access:WEBJOB.TryUpdate() = Level:Benign
                                        
                                        If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: EXPIRED DATE RANGE')

                                        End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM FINAL REJECTION','REASON: ' & Clip(glo:EDI_Reason))

                                        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                                        jbn:RefNumber = job:Ref_Number
                                        If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                                            !Found
                                            jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>EXPIRED DATE RANGE'
                                            Access:JOBNOTES.Update()
                                        Else ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign
                                            !Error
                                        End ! If Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign

                                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                                        jobe:RefNumber = job:Ref_Number
                                        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                            !Found
                                            jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                                            jobe:WarrantyStatusDate = Today()
                                            Access:JOBSE.Update()
                                        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                            !Error
                                        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                                    End ! If Access:WEBJOB.TryUpdate() = Level:Benign
                                End ! If Access:JOBS.TryUdate() = Level:Benign

                            End ! If Date# < Today() - man:ResubmissionLimit
                        End ! If Date# > 0
                    End ! If man:UseResubmissionLimit
                Else ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                    !Error
                    Cycle
                End ! If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign


                Do Prog:UpdateScreen

                Prog:RecordCount += 1
                ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
            End ! Loop 25 Times
        Of Event:CloseWindow
            Prog:Exit = 1
            Prog:Cancelled = 1
            Break
        Of Event:Accepted
            If Field() = ?Prog:Cancel
                Beep(Beep:SystemQuestion)  ;  Yield()
                Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                Of 1 ! Yes
                    Prog:Exit = 1
                    Prog:Cancelled = 1
                    Break
                Of 2 ! No
                End ! Case Message
            End! If FIeld()
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished
    Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
   Relate:JOBSWARR.Close
   Relate:AUDIT.Close
   Relate:JOBS.Close
   Relate:WEBJOB.Close
   Relate:JOBSE.Close
   Relate:JOBSE2.Close
   Relate:MANUFACT.Close
   Relate:JOBSWARR_ALIAS.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    If glo:WebJob
        Clarionet:OpenPushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Open(Prog:ProgressWindow)
        ?Prog:Cancel{prop:Hide} = 0
    End ! If glo:WebJob
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    If glo:WebJob
        Clarionet:UpdatePushWindow(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    If glo:WebJob
        Clarionet:ClosePushWindow(Prog:ProgressWindow)
    Else ! If glo:WebJob
        Close(Prog:ProgressWindow)
    End ! If glo:WebJob
    Display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ReconcileJob PROCEDURE (Long f:JobNumber,*Real f:Lab, *Real f:Par, *Real f:Other, *Real f:Sub, *Real f:VAT, *Real f:Total, *String f:Reason) !Generated from procedure template - Window

tmp:Return           BYTE(1)
tmp:JobNumber        LONG
tmp:IMEINumber       STRING(30)
tmp:Manufacturer     STRING(30)
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:OtherCosts       REAL
tmp:SubTotal         REAL
tmp:VAT              REAL
tmp:TotalPaid        REAL
tmp:Reason           STRING(255)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,244),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Job Number'),AT(220,120),USE(?tmp:JobNumber:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(332,120,64,10),USE(tmp:JobNumber),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Job Number'),TIP('Job Number'),UPR,READONLY
                       PROMPT('IMEI Number'),AT(220,134),USE(?tmp:IMEINumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(332,134,124,10),USE(tmp:IMEINumber),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('IMEI Number'),TIP('IMEI Number'),UPR,READONLY
                       PROMPT('Manufacturer'),AT(220,148),USE(?tmp:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s30),AT(332,148,124,10),USE(tmp:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR,READONLY
                       PROMPT('Labour Cost'),AT(220,170),USE(?tmp:LabourCost:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n-14.2),AT(332,170,64,10),USE(tmp:LabourCost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Labour Cost'),TIP('Labour Cost'),UPR
                       PROMPT('Parts Cost'),AT(220,184),USE(?tmp:PartsCost:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n-14.2),AT(332,184,64,10),USE(tmp:PartsCost),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Parts Cost'),TIP('Parts Cost'),UPR
                       PROMPT('Other Costs'),AT(220,198),USE(?tmp:OtherCosts:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n-14.2),AT(332,198,64,10),USE(tmp:OtherCosts),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Other Costs'),TIP('Other Costs'),UPR
                       PROMPT('Sub Total'),AT(220,214),USE(?tmp:SubTotal:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n-14.2),AT(332,214,64,10),USE(tmp:SubTotal),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Sub Total'),TIP('Sub Total'),UPR
                       PROMPT('V.A.T.'),AT(220,228),USE(?tmp:VAT:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n-14.2),AT(332,228,64,10),USE(tmp:VAT),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('V.A.T.'),TIP('V.A.T.'),UPR
                       PROMPT('Total Paid'),AT(220,244),USE(?tmp:TotalPaid:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@n-14.2),AT(332,244,64,10),USE(tmp:TotalPaid),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Total Paid'),TIP('Total Paid'),UPR,READONLY
                       PROMPT('Reason For Manual Reconciliation'),AT(220,260,108,24),USE(?tmp:Reason:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       TEXT,AT(332,260,124,42),USE(tmp:Reason),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Reason For Manual Reconciliation'),TIP('Reason For Manual Reconciliation'),UPR
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Manual Reconciliation'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(376,332),USE(?Button3),TRN,FLAT,ICON('recjobp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Return)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020723'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ReconcileJob')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBSWARR.UseFile
  Access:JOBSE2.UseFile
  SELF.FilesOpened = True
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = f:JobNumber
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
  tmp:JobNumber = job:Ref_Number
  tmp:IMEINUmber = job:ESN
  tmp:Manufacturer = job:Manufacturer
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','ReconcileJob')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReconcileJob')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeAccepted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  tmp:TotalPaid = tmp:SubTotal + tmp:VAT
  Display(?tmp:TotalPAid)
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020723'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020723'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020723'&'0')
      ***
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      If Clip(tmp:Reason) = ''
          Select(?tmp:Reason)
          Cycle
      End ! If Clip(tmp:Reason) = ''
      
      If tmp:TotalPaid = 0
          Beep(Beep:SystemExclamation)  ;  Yield()
          Case Missive('You have not entered any costs.'&|
              '|'&|
              '|Are you sure you want to reconcile this job?','ServiceBase 3g',|
                         'mexclam.jpg','\&No|/&Yes') 
              Of 2 ! &Yes Button
              Of 1 ! &No Button
                  Cycle
          End!Case Message
      Else
          If tmp:LabourCost = 0
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Missive('You have not entered a Labour Cost.'&|
                  '|'&|
                  '|Are you sure you want to reconcile this job?','ServiceBase 3g',|
                             'mexclam.jpg','\&No|/&Yes') 
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      Select(?tmp:LabourCost)
                      Cycle
              End!Case Message
          ElsIf tmp:PartsCost = 0
              Beep(Beep:SystemExclamation)  ;  Yield()
              Case Missive('You have not entered a Parts Cost.'&|
                  '|'&|
                  '|Are you sure you want to reconcile this job?','ServiceBase 3g',|
                             'mexclam.jpg','\&No|/&Yes') 
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      Select(?tmp:PartsCost)
                      Cycle
              End!Case Message
          Else
              Beep(Beep:SystemQuestion)  ;  Yield()
              Case Missive('Are you sure you want to reconcile this job?','ServiceBase 3g',|
                             'mquest.jpg','\&No|/&Yes') 
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      Cycle
              End!Case Message
          End
      End ! If tmp:TotalPaid = 0
      
      Beep(Beep:SystemExclamation)  ;  Yield()
      Case Missive('Confirm the entered costs:'&|
          '|Labour:' & Format(tmp:LabourCost,@n14.2) &|
          '|Parts: ' & Format(tmp:PartsCost,@n14.2) &|
          '|Other: ' & Format(tmp:OtherCosts,@n14.2) &|
          '|Total: ' & Format(tmp:TotalPaid,@n14.2),'ServiceBase 3g',|
                     'mexclam.jpg','\&Cancel|/Con&firm') 
          Of 2 ! Con&firm Button
          Of 1 ! &Cancel Button
              Cycle
      End!Case Message
      
      f:Lab = tmp:LabourCost
      f:Par = tmp:PartsCost
      f:Other = tmp:OtherCosts
      f:Sub = tmp:SubTotal
      f:Vat = tmp:VAT
      f:Total = tmp:TotalPaid
      f:Reason = tmp:Reason
      
      tmp:Return = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeAccepted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
