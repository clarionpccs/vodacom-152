

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01007.INC'),ONCE        !Local module procedure declarations
                     END



RRCWarrantyProcess PROCEDURE                          !Generated from procedure template - Browse

save_jobswarr_id     USHORT,AUTO
tmp:JobNumber        STRING(30)
tmp:BranchID         STRING(2)
tmp:CurrentStatus    STRING(40)
tmp:EDI              STRING('NO {1}')
tmp:SelectManufacturer BYTE(0)
tmp:Manufacturer     STRING(30)
tmp:FilterByDate     BYTE(0)
tmp:StartDate        DATE
tmp:EndDate          DATE
locSubmissions       LONG
locDateRejected      DATE
locRejectionReason   STRING(60)
locSavePath          CSTRING(255)
locDaysToFinalRejection STRING(20)
BRW8::View:Browse    VIEW(JOBSWARR)
                       PROJECT(jow:Manufacturer)
                       PROJECT(jow:RRCDateReconciled)
                       PROJECT(jow:DateAccepted)
                       PROJECT(jow:ClaimSubmitted)
                       PROJECT(jow:RefNumber)
                       PROJECT(jow:DateRejected)
                       PROJECT(jow:DateFinalRejection)
                       PROJECT(jow:Orig_Sub_Date)
                       PROJECT(jow:RecordNumber)
                       PROJECT(jow:BranchID)
                       PROJECT(jow:RepairedAt)
                       PROJECT(jow:RRCStatus)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:JobNumber          LIKE(tmp:JobNumber)            !List box control field - type derived from local data
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Company_Name       LIKE(job:Company_Name)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
jow:Manufacturer       LIKE(jow:Manufacturer)         !List box control field - type derived from field
tmp:CurrentStatus      LIKE(tmp:CurrentStatus)        !List box control field - type derived from local data
job:Location           LIKE(job:Location)             !List box control field - type derived from field
jow:RRCDateReconciled  LIKE(jow:RRCDateReconciled)    !List box control field - type derived from field
jow:DateAccepted       LIKE(jow:DateAccepted)         !List box control field - type derived from field
jow:ClaimSubmitted     LIKE(jow:ClaimSubmitted)       !List box control field - type derived from field
jow:RefNumber          LIKE(jow:RefNumber)            !List box control field - type derived from field
locSubmissions         LIKE(locSubmissions)           !List box control field - type derived from local data
locRejectionReason     LIKE(locRejectionReason)       !List box control field - type derived from local data
jow:DateRejected       LIKE(jow:DateRejected)         !List box control field - type derived from field
jow:DateFinalRejection LIKE(jow:DateFinalRejection)   !List box control field - type derived from field
locDaysToFinalRejection LIKE(locDaysToFinalRejection) !List box control field - type derived from local data
jow:Orig_Sub_Date      LIKE(jow:Orig_Sub_Date)        !List box control field - type derived from field
jow:RecordNumber       LIKE(jow:RecordNumber)         !Primary key field - type derived from field
jow:BranchID           LIKE(jow:BranchID)             !Browse key field - type derived from field
jow:RepairedAt         LIKE(jow:RepairedAt)           !Browse key field - type derived from field
jow:RRCStatus          LIKE(jow:RRCStatus)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK21::jow:BranchID        LIKE(jow:BranchID)
HK21::jow:Manufacturer    LIKE(jow:Manufacturer)
HK21::jow:RRCStatus       LIKE(jow:RRCStatus)
HK21::jow:RepairedAt      LIKE(jow:RepairedAt)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Warranty Claims'),AT(8,12),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,10,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       CHECK('Filter By Date'),AT(484,50),USE(tmp:FilterByDate),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Filter By Date'),TIP('Filter By Date'),VALUE('1','0')
                       GROUP,AT(556,50,116,28),USE(?Group:Dates)
                         PROMPT('Start Date'),AT(556,50),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(608,50,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),UPR
                         PROMPT('End Date'),AT(556,68),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(608,68,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       END
                       ENTRY(@s8),AT(8,74,64,10),USE(jow:RefNumber),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number')
                       CHECK('Select Manufacturer'),AT(8,54),USE(tmp:SelectManufacturer),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select Manufacturer'),TIP('Select Manufacturer'),VALUE('1','0')
                       ENTRY(@s30),AT(108,54,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                       BUTTON,AT(234,50),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                       SHEET,AT(4,30,672,350),USE(?CurrentTab),SPREAD
                         TAB('Pending Claims'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Approved Claims'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(604,350),USE(?Button:ClaimPaid),TRN,FLAT,ICON('clampayp.jpg')
                         END
                         TAB('Rejected Claims'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(604,350),USE(?Button:ProcessRejection),TRN,FLAT,ICON('prorejp.jpg')
                           BUTTON,AT(348,350),USE(?buttonImportResubmissions),TRN,FLAT,ICON('impresp.jpg')
                           BUTTON,AT(276,350),USE(?buttonImportAcceptedRejections),TRN,FLAT,ICON('impaccp.jpg')
                         END
                         TAB('Accepted Rejected Claims'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Final Rejection Claims'),USE(?Tab7),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Paid Claims'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       LIST,AT(8,88,664,256),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('60L(2)|M~Job Number~@s30@60L(2)|M~Account Number~@s15@100L(2)|M~Company Name~@s3' &|
   '0@80L(2)|M~I.M.E.I. Number~@s20@70L(2)|M~Manufacturer~@s30@110L(2)|M~Current Sta' &|
   'tus~@s40@80L(2)|M~Location~@s30@44R(2)|M~Reconciled~@d6@44R(2)|M~Approved~@d6@44' &|
   'R(2)|M~Submitted~@d6@32L(2)|M~Job Number~@s8@44R(2)|M~Submissions~@s8@240L(2)|M~' &|
   'Rejection Reason~@s60@44R(2)|M~Rejected~@d6b@50R(2)|M~Rejection~@d6b@60R(2)|M~Dy' &|
   's To Final Rej~@s5@40R(2)|M~Orig Sub Date~@d17@'),FROM(Queue:Browse)
                       BUTTON,AT(648,4),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,12),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(156,350),USE(?Button:AuditTrail),TRN,FLAT,ICON('auditp.jpg')
                       BUTTON,AT(604,384),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       BUTTON,AT(80,386),USE(?buttonExport),TRN,FLAT,ICON('exportp.jpg')
                       BUTTON,AT(8,386),USE(?Button:ValuateCLaims),TRN,FLAT,ICON('valclamp.jpg')
                       BUTTON,AT(80,350),USE(?Button:EngineeringDetails),TRN,FLAT,ICON('engdetp.jpg')
                       BUTTON,AT(8,350),USE(?Button:CustomerService),TRN,FLAT,ICON('custserp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       PROGRESS,USE(Prog.CNProgressThermometer),AT(4,14,156,12),RANGE(0,100)
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       STRING(@s60),AT(4,30,156,10),USE(Prog.CNPercentText),CENTER
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW8::Sort1:Locator  EntryLocatorClass                !Conditional Locator - tmp:SelectManufacturer = 1 And glo:WebJob = 1
BRW8::Sort6:Locator  EntryLocatorClass                !Conditional Locator - tmp:SelectManufacturer = 0 And glo:WebJob = 1
BRW8::Sort4:Locator  EntryLocatorClass                !Conditional Locator - tmp:SelectManufacturer = 1 And glo:WebJob = 0
BRW8::Sort5:Locator  EntryLocatorClass                !Conditional Locator - tmp:SelectManufacturer = 0 And glo:WebJob = 0
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
locImportFile           String(255),STATIC
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record              Record
JobNumber               String(20)
                    End
                End

locExportFile           String(255),STATIC
ExportFile    File,Driver('ASCII'),Pre(expfil),Name(locExportFile),Create,Bindable,Thread
Record              Record
Line                    String(1000)
                    End
                End
    Map
FilterBrowse        Procedure(String fEDI,Byte fDateFilter,Date fStartDate,Date fEndDate),Byte
    End
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Export      Routine
Data
locTempPath     Cstring(255)
locDestination      Cstring(255)
locSavePath     Cstring(255)
locFileName     CString(255)
Code

    if (glo:webJOb = 0)
        locSavePath = Path()
        If (filedialog ('Choose Directory',locDestination,'All Directories|*.*', |
                    file:save+file:keepdir + file:noerror + file:longname + file:directory))
            SetPath(locSavePath)
        Else
            SetPath(locSavePath)
            Exit
        End
        If Sub(locDestination,-1,1) <> '\'
            locDestination = Clip(locDestination) & '\'
        End !If Sub(tmp:TempFilePath,-1,1) <> '\'
    end !if (glo:webJOb = 0)

    If GetTempPathA(255,locTempPath)
        If Sub(locTempPath,-1,1) <> '\'
            locTempPath = Clip(locTempPath) & '\'
        End !If Sub(tmp:TempFilePath,-1,1) <> '\'
    End

    locExportFile = locTempPath & CLock() & '.TMP'

    Remove(locExportFile)

    Create(ExportFile)
    If Error()
        Stop(Error())
        Exit
    end

    Prog.ProgressSetup(500)

    Open(ExportFile)

    if (glo:WebJob = 1)
        if (tmp:SelectManufacturer = 1)
            Access:JOBSWARR.Clearkey(jow:RRCStatusManKey)
            jow:BranchID = tmp:BranchID
            jow:RepairedAt = 'RRC'
            jow:RRCSTatus = tmp:EDI
            jow:Manufacturer = tmp:Manufacturer
            Set(jow:RRCStatusManKey,jow:RRCStatusManKey)
        else ! if (tmp:SelectManufacturer = 1)
            Access:JOBSWARR.Clearkey(jow:RRCStatusKey)
            jow:BranchID = tmp:BranchID
            jow:RepairedAt = 'RRC'
            jow:RRCStatus = tmp:EDI
            Set(jow:RRCStatusKey,jow:RRCStatusKey)
        end ! if (tmp:SelectManufacturer = 1)
    else
        if (tmp:SelectManufacturer = 1)
            Access:JOBSWARR.Clearkey(jow:RepairedRRCStatusManKey)
            jow:RepairedAT = 'ARC'
            jow:RRCStatus = tmp:EDI
            jow:Manufacturer = tmp:Manufacturer
            Set(jow:RepairedRRCStatusManKey,jow:RepairedRRCStatusManKey)
        else ! if (tmp:SelectManufacturer = 1)
            Access:JOBSWARR.Clearkey(jow:RepairedRRCStatusKey)
            jow:RRCStatus = tmp:EDI
            jow:RepairedAt = 'ARC'
            Set(jow:RepairedRRCStatusKey,jow:RepairedRRCStatusKey)
        end ! if (tmp:SelectManufacturer = 1)
    end

    firstLine# = 1

    Loop Until Access:JOBSWARR.Next()
        if (glo:WebJob = 1)
            if (tmp:SelectManufacturer = 1)
                If (jow:BranchID <> tmp:BranchID  OR |
                    jow:RepairedAt <> 'RRC' OR |
                    jow:RRCSTatus <> tmp:EDI OR|
                    jow:Manufacturer <> tmp:Manufacturer)
                    Break
                End
            else ! if (tmp:SelectManufacturer = 1)
                If (jow:BranchID <> tmp:BranchID OR |
                    jow:RepairedAt <> 'RRC' OR |
                    jow:RRCStatus <> tmp:EDI)
                    Break
                end
            end ! if (tmp:SelectManufacturer = 1)
        else
            if (tmp:SelectManufacturer = 1)
                If (jow:RepairedAT <> 'ARC' OR |
                    jow:RRCStatus <> tmp:EDI OR |
                    jow:Manufacturer <> tmp:Manufacturer)
                    Break
                End
            else ! if (tmp:SelectManufacturer = 1)
                If (jow:RRCStatus <> tmp:EDI OR |
                    jow:RepairedAt <> 'ARC')
                    Break
                End
            end ! if (tmp:SelectManufacturer = 1)
        end

        If (Prog.InsideLoop())
            Break
        end

        if (FilterBrowse(tmp:EDI,tmp:FilterByDate,tmp:StartDate,tmp:EndDate))
            Cycle
        end

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = jow:RefNumber
        If (Access:JOBS.TryFetch(job:Ref_Number_Key))
            !Cycle
        end
        ACcess:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = jow:RefNumber
        If (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            !Cycle
        End

        if (firstLine# = 1)
            expfil:Line = 'Job Number,Account Number,Company Name,IMEI Number,Manufacturer,Current Status'
            Case tmp:EDI
            Of 'NO' ! Pending
                expfil:Line = Clip(expfil:Line) & ',Location,Submitted'
            of 'APP' ! Approved
                expfil:Line = Clip(expfil:Line) & ',Location,Approved'
            of 'EXC' ! Rejection
                expfil:Line = Clip(expfil:Line) & ',Rejected,Submissions,Days To Final Rejection,Rejection Reason,Location'
            of 'AAJ' orof 'REJ' ! Accepted / Final Rejection
                expfil:Line = Clip(expfil:Line) & ',Rejection,Rejection Reason,Location'
            of 'PAY' ! Paid
                expfil:Line = Clip(expfil:Line) & ',Location,Reconciled'
            End
            Add(ExportFile)
            firstLine# = 0
        end

        ! Job Number
        expfil:Line = '"' & jow:RefNumber
        ! Account Number
        expfil:Line = clip(expfil:Line) & '","' & clip(job:Account_Number)
        ! Company Name
        expfil:Line = clip(expfil:Line) & '","' & clip(job:Company_Name)
        ! IMEI Number
        expfil:Line = clip(expfil:Line) & '","''' & clip(job:ESN)
        ! Manufacturer
        expfil:Line = clip(expfil:Line) & '","' & clip(jow:Manufacturer)
        ! Current Status
        if (job:Exchange_Unit_Number > 0)
            expfil:Line = clip(expfil:Line) & '","E ' & clip(wob:Exchange_Status)
        else ! if (job:Exchange_Unit_Number > 0)
            expfil:Line = clip(expfil:Line) & '","J ' & clip(wob:Current_Status)
        end ! if (job:Exchange_Unit_Number > 0)
        

        Case tmp:EDI
        Of 'NO' ! Pending
            ! Location
            expfil:Line = clip(expfil:Line) & '","' & clip(job:Location)
            ! Submitted
            expfil:Line = Clip(expfil:Line) & '","' & format(jow:ClaimSubmitted,@d06b)
        Of 'APP' ! Accepted
            ! Location
            expfil:Line = clip(expfil:Line) & '","' & clip(job:Location)
            ! Accepted
            expfil:Line = Clip(expfil:Line) & '","' & format(jow:DateAccepted,@d06b)
        of 'EXC' !Rejected
            locRejectionReason = ''

            Do GetRejectionReason_EXC
            ! Rejected
            expfil:Line = Clip(expfil:Line) & '","' & format(jow:DateRejected,@d06b)
            ! Submissions
            expfil:Line = Clip(expfil:Line) & '","' & locSubmissions
            ! Days To Final Rejection
            expfil:Line = Clip(expfil:Line) & '","' & locDaysToFinalRejection
            ! Rejection Reason
            expfil:Line = Clip(expfil:Line) & '","' & locRejectionReason
            ! Location
            expfil:Line = Clip(expfil:Line) & '","' & clip(job:Location)
        Of 'AAJ' ! Rejection Aknowledged
            Do GetRejectionReason_AAJ
            ! Final Rejection
            expfil:Line = Clip(expfil:Line) & '","' & format(jow:DateFinalRejection,@d06b)
            ! Rejection Reason
            expfil:Line = Clip(expfil:Line) & '","' & locRejectionReason
            ! Location
            expfil:Line = Clip(expfil:Line) & '","' & clip(job:Location)
        of 'REJ' ! Final Reject
            
            Do GetRejectionReason_REJ

            ! Final Rejection
            expfil:Line = Clip(expfil:Line) & '","' & format(jow:DateFinalRejection,@d06b)
            ! Rejection Reason
            expfil:Line = Clip(expfil:Line) & '","' & locRejectionReason
            ! Location
            expfil:Line = Clip(expfil:Line) & '","' & clip(job:Location)
        Of 'PAY' ! Pay
            ! Location
            expfil:Line = clip(expfil:Line) & '","' & clip(job:Location)
            ! Submitted
            expfil:Line = Clip(expfil:Line) & '","' & format(jow:RRCDateReconciled,@d06b)

        End ! Case tmp:EDI

        expfil:Line = clip(expfil:Line) & '"'
        Add(ExportFile)
    end


    Prog.ProgressFinish()
    Close(ExportFile)

    Case tmp:EDI
    Of 'NO'
        locFileName = 'Pending Claims ' & Format(Today(),@d11) & '.csv'
    Of 'APP'
        locFileName = 'Approved Claims ' & Format(Today(),@d11) & '.csv'
    Of 'EXC'
        locFileName = 'Rejected Claims ' & Format(Today(),@d11) & '.csv'
    Of 'AAJ'
        locFileName = 'Accepted Rejected Claims ' & Format(Today(),@d11) & '.csv'
    Of 'REJ'
        locFileName = 'Final Rejection Claims ' & Format(Today(),@d11) & '.csv'
    Of 'PAY'
        locFileName = 'Paid Claims ' & Format(Today(),@d11) & '.csv'
    End !Case tmp:EDI

    Remove(locTempPath & locFileName)
    Rename(locExportFile,locTempPath & locFileName)

    ! Send Files To Client
    error# = 0
    If glo:WebJob
        Free(FileListQueue)
        flq:Filename = locTempPath & locFileName
        Add(FileListQueue)
        Beep(Beep:SystemAsterisk)  ;  Yield()
        If (ClarioNET:SendFilesToClient(1,0) <> 'DONE')
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error occurred downloading the report. Please check your download settings try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
            End!Case Message
            error# = 1
        End !If (ClarioNET:SendFilesToClient(1,0) <> 'DONE')
        Remove(locExportFile)
    Else
        Remove(locDestination & locFileName)
        Rename(locTempPath & locFileName,locDestination & locFileName)
    End ! If glo:WebJob

    if (error# <> 1)

        Beep(Beep:SystemAsterisk)  ;  Yield()
        Case Missive('Export Completed.<13,10>File: ' & clip(locFileName),'ServiceBase',|
                       'midea.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message

    end ! if (error# <> 1)
GetRejectionReason_EXC      Routine
    locDaysToFinalRejection = ''
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = jow:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    !Found
        If man:UseResubmissionLimit
            locDaysToFinalRejection = jow:DateRejected + man:ResubmissionLimit - Today()
        End ! If man:UseResubmissionLimit
    End

    

    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM REJECTED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM REJECTED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
            locRejectionReason = Clip(Sub(aud2:Notes,9,60))
            if (jow:DateRejected = 0)
                jow:DateRejected = aud:Date
                If man:UseResubmissionLimit
                    locDaysToFinalRejection = jow:DateRejected + man:ResubmissionLimit - Today()
                End ! If man:UseResubmissionLimit
            end

            break

        END ! IF

    End ! Loop

    Count# = 1
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM RESUBMITTED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM RESUBMITTED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'
        Count# += 1
    End ! Loop
    locSubmissions = Count#

GetRejectionReason_AAJ      Routine
    locRejectionReason = ''
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM REJECTION ACKNOWLEDGED'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
            locRejectionReason = Clip(Sub(aud2:Notes,9,60))
            if (jow:DateFinalRejection = 0)
                jow:DateFinalRejection = aud:Date
            end
            break

        END ! IF

    End ! Loop
GetRejectionReason_REJ      Routine
    locRejectionReason = ''
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number = jow:RefNumber
    aud:Type = 'JOB'
    aud:Action = 'WARRANTY CLAIM FINAL REJECTION'
    Set(aud:TypeActionKey,aud:TypeActionKey)
    Loop ! Begin Loop
        If Access:AUDIT.Next()
            Break
        End ! If Access:AUDIT.Next()
        If aud:Ref_Number <> jow:RefNumber
            Break
        End ! If aud:Ref_Number <> jow:RefNumber
        If aud:Type <> 'JOB'
            Break
        End ! If aud:Type = 'JOB'
        If aud:Action <> 'WARRANTY CLAIM FINAL REJECTION'
            Break
        End ! If aud:Action <> 'WARRANTY CLAIM REJECTED'

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
            locRejectionReason = Clip(Sub(aud2:Notes,9,60))
            if (jow:DateFinalRejection = 0)
                jow:DateFinalRejection = aud:Date
            end
            break

        END ! IF
    End ! Loop
ImportAcceptedRejections        Routine
    if (glo:WebJob = 0)
        locSavePath = Path()
        if (fileDialog('Choose File',locImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
            !Found
            setPath(locSavePath)

        else ! if (fileDialog)
            !Error
            setPath(locSavePath)
            Exit
        end ! if (fileDialog)
    else
        If ClarioNET:GetFilesFromClient(1,'TEST',1) = 1
            get(FileListQueue,1)
            locImportFile = flq:FileName
            if (NOT Exists(locImportFile))
                Exit
            end
        Else
            Exit
        end
    end

    Open(ImportFile)
    If (Error())
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Unable to open import file.'&|
            '|' & Clip(Error()) & ' : ' & Clip(FileError()) & '','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        exit
    end

    recCount# = 0
    set(ImportFile,0)
    Loop
        Next(ImportFile)
        If (Error())
            Break
        end
        recCount# += 1
    end

    Prog.ProgressSetup(recCount#)

    count# = 0

    set(ImportFile,0)
    loop
        next(ImportFile)
        if (Error())
            Break
        end

        If (Prog.InsideLoop())
            Break
        end


        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = impfil:JobNumber
        If (Access:JOBS.TryFetch(job:Ref_Number_Key))
            Cycle
        End

        If (JobInUse(job:Ref_Number,1))
            Cycle
        End

        Access:JOBSWARR.Clearkey(jow:RefNumberKey)
        jow:RefNumber = job:Ref_Number
        If (Access:JOBSWARR.TryFetcH(jow:RefNumberKey))
            Cycle
        End

        if (glo:webJob = 1 AND jow:BranchID <> tmp:BranchID)
            Cycle
        end

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            Cycle
        End

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
            Cycle
        End



        If (jow:RRCStatus <> 'EXC')
            Cycle
        End

        if (jobe:OBFProcessed = 3)
            jobe:OBFProcessed = 4
        else
            job:EDI = 'REJ'
            jow:Status = 'REJ'
        end

        wob:EDI = 'AAJ'
        jow:RRCStatus = 'AAJ'
        jow:DateFinalRejection = Today()
        jobe:WarrantyClaimStatus = 'FINAL REJECTION'
        jobe:WarrantyStatusDate = Today()
        If (Access:JOBS.TryUpdate() = Level:Benign)
            If (Access:JOBSE.TryUpdate() = Level:Benign)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                    If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                        If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTION ACKNOWLEDGED','')
                            count# += 1
                        End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
                    End ! If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                End ! IF (Access:WEBJOB.Update() = Level:Benign)
            End ! If (Access:JOBSE.Update() = Level:Benign)
        End

    End

    Prog.ProgressFinish()

    Close(ImportFile)
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Missive('File Imported.'&|
        '|Record(s) Updated: ' & Clip(count#) & '','ServiceBase',|
                   'midea.jpg','/&OK') 
    Of 1 ! &OK Button
    End!Case Message

    brw1.ResetSort(1)
        
        






ImportResubmissions        Routine
    if (glo:WebJob = 0)
        locSavePath = Path()
        if (fileDialog('Choose File',locImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
            !Found
            setPath(locSavePath)

        else ! if (fileDialog)
            !Error
            setPath(locSavePath)
            Exit
        end ! if (fileDialog)
    else
        If ClarioNET:GetFilesFromClient(1,'TEST',1) = 1
            get(FileListQueue,1)
            locImportFile = flq:FileName
            if (NOT Exists(locImportFile))
                Exit
            end
        Else
            Exit
        end
    end

    Open(ImportFile)
    If (Error())
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('Unable to open import file.'&|
            '|' & Clip(Error()) & ' : ' & Clip(FileError()) & '','ServiceBase',|
                       'mstop.jpg','/&OK') 
        Of 1 ! &OK Button
        End!Case Message
        exit
    end

    recCount# = 0
    set(ImportFile,0)
    Loop
        Next(ImportFile)
        If (Error())
            Break
        end
        recCount# += 1
    end

    Prog.ProgressSetup(recCount#)

    count# = 0

    set(ImportFile,0)
    loop
        next(ImportFile)
        if (Error())
            Break
        end

        If (Prog.InsideLoop())
            Break
        end


        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number = impfil:JobNumber
        If (Access:JOBS.TryFetch(job:Ref_Number_Key))
            Cycle
        End

        If (JobInUse(job:Ref_Number,1))
            Cycle
        End

        Access:JOBSWARR.Clearkey(jow:RefNumberKey)
        jow:RefNumber = job:Ref_Number
        If (Access:JOBSWARR.TryFetcH(jow:RefNumberKey))
            Cycle
        End

        if (glo:webJob = 1 AND jow:BranchID <> tmp:BranchID)
            Cycle
        end

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        If (Access:WEBJOB.TryFetch(wob:RefNumberKey))
            Cycle
        End

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If (Access:JOBSE.TryFetch(jobe:RefNumberKey))
            Cycle
        End

        If (jow:RRCStatus <> 'EXC')
            Cycle
        End

        if (jobe:OBFProcessed = 3)
            jobe:OBFProcessed = 0
        else
            job:EDI = 'NO'
            job:EDI_Batch_Number = ''
            jow:Status = 'NO'
            jow:Submitted += 1
            jow:ClaimSubmitted = Today()
        end

        wob:EDI = 'NO'
        jow:RRCStatus = 'NO'
        jobe:WarrantyClaimStatus = 'RESUBMITTED'
        jobe:WarrantyStatusDate = Today()
        If (Access:JOBS.TryUpdate() = Level:Benign)
            If (job:EDI = 'NO')
                Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                jobe2:RefNumber = job:Ref_Number
                If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
                    jobe2:InPendingDate = Today()
                    Access:JOBSE2.TryUpdate()
                else
                    If (Access:JOBSE2.PrimeRecord() = Level:Benign)
                        jobe2:RefNumber = job:Ref_Number
                        jobe2:InPendingDate = Today()
                        If (Access:JOBSE2.TryINsert())
                            Access:JOBSE2.CancelAutoInc()
                        end
                    End ! If (Access:JOBSE2.PrimeRecord() = Level:Benign)
                end ! If (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            End

            If (Access:JOBSE.TryUpdate() = Level:Benign)
                IF (Access:WEBJOB.TryUpdate() = Level:Benign)
                    If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                        If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','')
                            count# += 1
                        End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
                    End ! If (Access:JOBSWARR.TryUpdate() = Level:Benign)
                End ! IF (Access:WEBJOB.Update() = Level:Benign)
            End ! If (Access:JOBSE.Update() = Level:Benign)
        End

    End

    Prog.ProgressFinish()

    Close(ImportFile)
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Missive('File Imported.'&|
        '|Record(s) Updated: ' & Clip(count#) & '','ServiceBase',|
                   'midea.jpg','/&OK') 
    Of 1 ! &OK Button
    End!Case Message

    brw1.ResetSort(1)
        
        






! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020718'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('RRCWarrantyProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:MANUFACT.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = Clarionet:Global.Param2
  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:BranchID = tra:BranchIdentification
  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  tmp:StartDate = date(month(Today()),1,year(Today()))
  tmp:EndDate = TOday()
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:JOBSWARR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Inserting (DBH 30/06/2008) # 9792 - Don't show claim paid for ARC jobs
  If glo:webJob = 0
      ?Button:ClaimPaid{prop:Hide} = 1
  End ! If glo:webJob = 0
  ! End (DBH 30/06/2008) #9792
  ! Save Window Name
   AddToLog('Window','Open','RRCWarrantyProcess')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,jow:RRCStatusManKey)
  BRW1.AddRange(jow:Manufacturer)
  BRW1.AddLocator(BRW8::Sort1:Locator)
  BRW8::Sort1:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,jow:RRCStatusKey)
  BRW1.AddRange(jow:RRCStatus)
  BRW1.AddLocator(BRW8::Sort6:Locator)
  BRW8::Sort6:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,jow:RepairedRRCStatusManKey)
  BRW1.AddRange(jow:Manufacturer)
  BRW1.AddLocator(BRW8::Sort4:Locator)
  BRW8::Sort4:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,jow:RepairedRRCStatusKey)
  BRW1.AddRange(jow:RRCStatus)
  BRW1.AddLocator(BRW8::Sort5:Locator)
  BRW8::Sort5:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,jow:RRCStatusKey)
  BRW1.AddRange(jow:RRCStatus)
  BRW1.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?jow:RefNumber,jow:RefNumber,1,BRW1)
  BIND('tmp:JobNumber',tmp:JobNumber)
  BIND('tmp:CurrentStatus',tmp:CurrentStatus)
  BIND('locSubmissions',locSubmissions)
  BIND('locRejectionReason',locRejectionReason)
  BIND('locDaysToFinalRejection',locDaysToFinalRejection)
  BRW1.AddField(tmp:JobNumber,BRW1.Q.tmp:JobNumber)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Company_Name,BRW1.Q.job:Company_Name)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(jow:Manufacturer,BRW1.Q.jow:Manufacturer)
  BRW1.AddField(tmp:CurrentStatus,BRW1.Q.tmp:CurrentStatus)
  BRW1.AddField(job:Location,BRW1.Q.job:Location)
  BRW1.AddField(jow:RRCDateReconciled,BRW1.Q.jow:RRCDateReconciled)
  BRW1.AddField(jow:DateAccepted,BRW1.Q.jow:DateAccepted)
  BRW1.AddField(jow:ClaimSubmitted,BRW1.Q.jow:ClaimSubmitted)
  BRW1.AddField(jow:RefNumber,BRW1.Q.jow:RefNumber)
  BRW1.AddField(locSubmissions,BRW1.Q.locSubmissions)
  BRW1.AddField(locRejectionReason,BRW1.Q.locRejectionReason)
  BRW1.AddField(jow:DateRejected,BRW1.Q.jow:DateRejected)
  BRW1.AddField(jow:DateFinalRejection,BRW1.Q.jow:DateFinalRejection)
  BRW1.AddField(locDaysToFinalRejection,BRW1.Q.locDaysToFinalRejection)
  BRW1.AddField(jow:Orig_Sub_Date,BRW1.Q.jow:Orig_Sub_Date)
  BRW1.AddField(jow:RecordNumber,BRW1.Q.jow:RecordNumber)
  BRW1.AddField(jow:BranchID,BRW1.Q.jow:BranchID)
  BRW1.AddField(jow:RepairedAt,BRW1.Q.jow:RepairedAt)
  BRW1.AddField(jow:RRCStatus,BRW1.Q.jow:RRCStatus)
  IF ?tmp:FilterByDate{Prop:Checked} = True
    HIDE(?jow:RefNumber)
    ENABLE(?Group:Dates)
  END
  IF ?tmp:FilterByDate{Prop:Checked} = False
    UNHIDE(?jow:RefNumber)
    DISABLE(?Group:Dates)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = True
    ENABLE(?tmp:Manufacturer)
    ENABLE(?CallLookup)
  END
  IF ?tmp:SelectManufacturer{Prop:Checked} = False
    DISABLE(?tmp:Manufacturer)
    DISABLE(?CallLookup)
  END
  BRW1.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Pending'
    ?Tab2{PROP:TEXT} = 'Approved'
    ?Tab3{PROP:TEXT} = 'Rejected'
    ?Tab5{PROP:TEXT} = 'Accepted Rejected'
    ?Tab7{PROP:TEXT} = 'Final Rejection'
    ?Tab6{PROP:TEXT} = 'Paid'
    ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#80L(2)|M~Location~@s30@#7#44R(2)|M~Submitted~@d6@#10#40R(2)|M~Orig Sub Date~@d17@#17#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','RRCWarrantyProcess')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseEDIManufacturers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button:AuditTrail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
      Brw1.UpdateViewRecord()
      glo:Select12 = jow:RefNumber
      If glo:Select12 = 0
          Cycle
      End ! If glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:FilterByDate
      IF ?tmp:FilterByDate{Prop:Checked} = True
        HIDE(?jow:RefNumber)
        ENABLE(?Group:Dates)
      END
      IF ?tmp:FilterByDate{Prop:Checked} = False
        UNHIDE(?jow:RefNumber)
        DISABLE(?Group:Dates)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FilterByDate, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:FilterByDate, Accepted)
    OF ?tmp:StartDate
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StartDate, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StartDate, Accepted)
    OF ?tmp:EndDate
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:EndDate, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:EndDate, Accepted)
    OF ?tmp:SelectManufacturer
      IF ?tmp:SelectManufacturer{Prop:Checked} = True
        ENABLE(?tmp:Manufacturer)
        ENABLE(?CallLookup)
      END
      IF ?tmp:SelectManufacturer{Prop:Checked} = False
        DISABLE(?tmp:Manufacturer)
        DISABLE(?CallLookup)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectManufacturer, Accepted)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?CallLookup
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?Button:ClaimPaid
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ClaimPaid, Accepted)
      InsertBatchNumber(tmp:Manufacturer)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ClaimPaid, Accepted)
    OF ?Button:ProcessRejection
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ProcessRejection, Accepted)
      EDI# = 0
      Case Missive('Do you wish to RESUBMIT the claim, or acknowledge the REJECTion?','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|Reject|Resubmit')
          Of 3 ! Resubmit Button
      
              EDI# = 1
          Of 2 ! Reject Button
              EDI# = 2
          Of 1 ! Cancel Button
      End ! Case Missive
      
      If EDI#
          Loop   !makes edi_reason compulsory here
              Glo:EDI_Reason = ''
              Get_EDI_Reason   !writes to glo:EDI_Reason string 60
              if Clip(glo:edi_reason) = '' then
                  Case Missive('If you do not give a reason, this process cannot be completed.'&|
                    '<13,10>'&|
                    '<13,10>Do you want to RETURN and enter a reason, or CANCEL the process?','ServiceBase 3g',|
                                 'mquest.jpg','\Cancel|/Return')
                      Of 2 ! Return Button
                      Of 1 ! Cancel Button
                          break
                  End ! Case Missive
              ELSE
                  break
              END !If glo:edi_reason is blank
          End !loop
      
          If Clip(glo:edi_reason) <> ''
              Pending# = False
              Brw1.UpdateViewRecord()
              Access:WEBJOB.Clearkey(wob:RefNumberKey)
              wob:RefNumber   = jow:RefNumber
              If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Found
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = wob:RefNumber
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      !If job has been rejected via the OBF/2nd Year process, then
                      !jobe:OBFProcessed = 3. In this case do NOT change job:edi
                      !the job should not be visible in the normal warranty browses
      
                      Case EDI#
                          Of 1
                              If jobe:OBFProcessed = 3
                                  jobe:OBFProcessed = 0
                              Else !If jobe:OBFProcessed = 3
                                  job:EDI = 'NO'
                                  ! Inserting (DBH 10/04/2006) #7252 - The unit is being returned to the pending table
                                  Pending# = True
                                  ! End (DBH 10/04/2006) #7252
                                  job:EDI_Batch_Number = ''
      
                                  jow:Status = 'NO'
                                  jow:Submitted += 1
                                  jow:ClaimSubmitted = Today()
                                  Access:JOBSWARR.Update()
                              End !If jobe:OBFProcessed = 3
      
                              wob:EDI = 'NO'
                              jow:RRCStatus = 'NO'
                              Access:JOBSWARR.Update()
                              jobe:WarrantyCLaimStatus = 'RESUBMITTED'
                              jobe:WarrantyStatusDate = Today()
      
                          Of 2
                              If jobe:OBFProcessed = 3
                                  jobe:OBFProcessed = 4
                              Else !If jobe:OBFProcessed = 3
                                  job:EDI = 'REJ'
                                  jow:Status = 'REJ'
                                  Access:JOBSWARR.Update()
                              End !If jobe:OBFProcessed = 3
      
                              wob:EDI = 'AAJ'
                              jow:RRCStatus = 'AAJ'
                              jow:DateFinalRejection = Today()    ! #11505 Record date of final rejection (DBH: 21/07/2010)
                              Access:JOBSWARR.Update()
                              jobe:WarrantyClaimStatus = 'FINAL REJECTION'
                              jobe:WarrantyStatusDate = Today()
                      End !Case EDI#
                      If Access:JOBS.Update() = Level:Benign
                          Access:WEBJOB.Update()
                          Access:JOBSE.Update()
                          ! Inserting (DBH 10/04/2006) #7252 - Update the "in pending" date
                          If Pending# And job:EDI = 'NO'
                              Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
                              jobe2:RefNumber = job:Ref_Number
                              If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Found
                                  jobe2:InPendingDate = Today()
                                  Access:JOBSE2.TryUpdate()
                              Else ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                                  ! Error
                                  If Access:JOBSE2.PrimeRecord() = Level:Benign
                                      jobe2:RefNumber = job:Ref_Number
                                      jobe2:InPendingDate = Today()
                                      If Access:JOBSE2.TryInsert() = Level:Benign
                                          ! Insert Successful
      
                                      Else ! If Access:JOBSE2.TryInsert() = Level:Benign
                                          ! Insert Failed
                                          Access:JOBSE2.CancelAutoInc()
                                      End ! If Access:JOBSE2.TryInsert() = Level:Benign
                                  End !If Access:JOBSE2.PrimeRecord() = Level:Benign
                              End ! If Access:JOBSE2.Tryfetch(jobe2:RefNumberKey) = Level:Benign
                          End ! If Pending# And job:EDI = 'NO'
                          ! End (DBH 10/04/2006) #7252
      
                      ! End (DBH 10/04/2006) #7252
                          Case EDI#
                              Of 1
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
                              Of 2
                                  If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM REJECTION ACKNOWLEDGED','REASON: ' & Clip(Glo:EDI_Reason))
      
                                  End ! If AddToAudit(job:Ref_Number,'JOB','WARRANTY CLAIM RESUBMITTED','REASON: ' & Clip(Glo:EDI_Reason))
                          End !Case EDI#
      
                      End !If access:jobs.update() = Level:Benign
                      access:jobnotes.clearkey(jbn:RefNumberKey)
                  Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
              Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                  !Error
              End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
          END !If glo:edi reason is not blank
      End !EDI#
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ProcessRejection, Accepted)
    OF ?buttonImportResubmissions
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonImportResubmissions, Accepted)
      do ImportResubmissions
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonImportResubmissions, Accepted)
    OF ?buttonImportAcceptedRejections
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonImportAcceptedRejections, Accepted)
      do ImportAcceptedRejections
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonImportAcceptedRejections, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020718'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020718'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020718'&'0')
      ***
    OF ?Button:AuditTrail
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
      glo:Select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:AuditTrail, Accepted)
    OF ?buttonExport
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonExport, Accepted)
      Do Export
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonExport, Accepted)
    OF ?Button:ValuateCLaims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ValuateCLaims, Accepted)
      Case Missive('This will valuate the pending claims. This may take a long time depending on the amount of claims.'&|
        '<13,10>'&|
        '<13,10>Are  you sure you want to continue?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes')
          Of 2 ! Yes Button
      
          Count# = 0
          ClaimValue$ = 0
      
          SetCursor(Cursor:Wait)
          Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
          Access:JOBSWARR.Clearkey(jow:RRCStatusKey)
          jow:BranchID = tmp:BranchID
          jow:RepairedAT = 'RRC'
          jow:RRCStatus = tmp:EDI
          Set(jow:RRCStatusKey,jow:RRCStatusKey)
          Loop ! Begin Loop
              If Access:JOBSWARR.Next()
                  Break
              End ! If Access:JOBSWARR.Next()
              If jow:BranchID <> tmp:BranchID
                  Break
              End ! If jow:BranchID <> tmp:BranchID
              If jow:RepairedAt <> 'RRC'
                  Break
              End ! If jow:RepairedAt <> 'RRC'
              If jow:RRCStatus <> tmp:EDI
                  Break
              End ! If jow:RRCStatus <> tmp:EDI
              COunt# += 1
          End ! Loop
          Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
          SetCursor()
      
      
          Prog.ProgressSetup(Count#)
      
          Save_JOBSWARR_ID = Access:JOBSWARR.SaveFile()
          Access:JOBSWARR.Clearkey(jow:RRCStatusKey)
          jow:BranchID = tmp:BranchID
          jow:RepairedAT = 'RRC'
          jow:RRCStatus = tmp:EDI
          Set(jow:RRCStatusKey,jow:RRCStatusKey)
          Loop
              !Inside Loop
              If Access:JOBSWARR.Next()
                  Break
              End ! If Access:JOBSWARR.Next()
              If jow:BranchID <> tmp:BranchID
                  Break
              End ! If jow:BranchID <> tmp:BranchID
              If jow:RepairedAT <> 'RRC'
                  Break
              End ! If jow:RepairedAT <> 'RRC'
              If jow:RRCStatus <> tmp:EDI
                  Break
              End ! If jow:Status <> tmp:EDI
      
              Access:JOBSE.ClearKey(jobe:RefNumberKey)
              jobe:RefNumber = jow:RefNumber
              If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                  !Found
              Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                  !Error
                  Cycle
              End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      
      
              If (Prog.InsideLoop())
                break
              end
              Prog.ProgressText('Records Found: ' & Prog:RecordsProcessed & '/' & Prog:RecordsToProcess)
      
              ClaimValue$ += Round(jobe:RRCWLabourCost,.02) + Round(jobe:RRCWPartsCost,.02)
          End ! Loop 25 Times
          Prog.ProgressFinish()
          Access:JOBSWARR.RestoreFile(Save_JOBSWARR_ID)
          Case Missive('Number of claims: ' & Count# & '.'&|
            '<13,10>'&|
            '<13,10>Value of claims: ' & Format(ClaimValue$,@n14.2) & '.','ServiceBase 3g',|
                         'midea.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Of 1 ! No Button
      End ! Case Missive
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:ValuateCLaims, Accepted)
    OF ?Button:EngineeringDetails
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:EngineeringDetails, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.jow:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Request# = GlobalRequest
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
          GlobalRequest = Request#
      
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      brw1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:EngineeringDetails, Accepted)
    OF ?Button:CustomerService
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CustomerService, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.jow:RefNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          x# = Choice(?CurrentTab)
          Request# = GlobalRequest
          GlobalRequest = ChangeRecord
          UpdateJobs
          GlobalRequest = Request#
          Brw1.ResetSort(1)
      !    ! Fudge to make sure that it returns to the same tab if left from. (Poxy Clarionet) - TrkBs: 6636 (DBH: 02-11-2005)
      !    Select(?CurrentTab,x#)
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !brw1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button:CustomerService, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?CurrentTab
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?CurrentTab)
    Case Choice(?CurrentTab)
    Of 1 !Pending
        tmp:EDI = 'NO'
    Of 2 !Approved
        tmp:EDI = 'APP'
    Of 3 !Rejected
        tmp:EDI = 'EXC'
    Of 4 !Accepted Rejected / Final Rejection
        tmp:EDI = 'AAJ'
    of 5 !Final Rejection
        tmp:EDI = 'REJ'
    Of 6 !Paid
        tmp:EDI = 'PAY'
    End ! Case Choice(?CurrentTab)
    Brw1.ResetSort(1)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?CurrentTab)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#80L(2)|M~Location~@s30@#7#44R(2)|M~Submitted~@d6@#10#40R(2)|M~Orig Sub Date~@d17@#17#'
          ?Tab1{PROP:TEXT} = 'Pending'
        OF 2
          ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#80L(2)|M~Location~@s30@#7#44R(2)|M~Approved~@d6@#9#40R(2)|M~Orig Sub Date~@d17@#17#'
          ?Tab2{PROP:TEXT} = 'Approved'
        OF 3
          ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#44R(2)|M~Rejected~@d6b@#14#44R(2)|M~Submissions~@s8@#12#60R(2)|M~Dys To Final Rej~@s5@#16#240L(2)|M~Rejection Reason~@s60@#13#80L(2)|M~Location~@s30@#7#40R(2)|M~Orig Sub Date~@d17@#17#'
          ?Tab3{PROP:TEXT} = 'Rejected'
        OF 4
          ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#50R(2)|M~Rejection~@d6b@#15#240L(2)|M~Rejection Reason~@s60@#13#80L(2)|M~Location~@s30@#7#40R(2)|M~Orig Sub Date~@d17@#17#'
          ?Tab5{PROP:TEXT} = 'Accepted Rejected'
        OF 5
          ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#50R(2)|M~Rejection~@d6b@#15#240L(2)|M~Rejection Reason~@s60@#13#80L(2)|M~Location~@s30@#7#40R(2)|M~Orig Sub Date~@d17@#17#'
          ?Tab7{PROP:TEXT} = 'Final Rejection'
        OF 6
          ?List{PROP:FORMAT} ='60L(2)|M~Job Number~@s30@#1#60L(2)|M~Account Number~@s15@#2#100L(2)|M~Company Name~@s30@#3#80L(2)|M~I.M.E.I. Number~@s20@#4#70L(2)|M~Manufacturer~@s30@#5#110L(2)|M~Current Status~@s40@#6#80L(2)|M~Location~@s30@#7#44R(2)|M~Reconciled~@d6@#8#40R(2)|M~Orig Sub Date~@d17@#17#'
          ?Tab6{PROP:TEXT} = 'Paid'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
FilterBrowse        Procedure(String fEDI,Byte fDateFilter,Date fStartDate,Date fEndDate)
Code
    if (fDateFilter = 0)
        Return TRUE
    end
    case fEDI
    Of 'NO'
        if (jow:ClaimSubmitted < fStartDate Or jow:ClaimSubmitted > fEndDate)
            Return TRUE
        end
    Of 'APP'
        if (jow:DateAccepted < fStartDate Or jow:DateAccepted > fEndDate)
            Return TRUE
        end
    Of 'EXC'
        if (jow:DateRejected < fStartDate Or jow:DateRejected > fEndDate)
            Return TRUE
        end
    Of 'AAJ'  Orof 'REJ'
        if (jow:DateFinalRejection < fStartDate Or jow:DateFinalRejection > fEndDate)
            Return TRUE
        end
    Of 'PAY'
        if (jow:RRCDateReconciled < fStartDate Or jow:RRCDateReconciled > fEndDate)
            Return TRUE
        end
    else
        Return TRUE
    end
    Return FALSE
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF tmp:SelectManufacturer = 1 And glo:WebJob = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:BranchID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'RRC'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:EDI
     GET(SELF.Order.RangeList.List,4)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
  ELSIF tmp:SelectManufacturer = 1 And glo:WebJob = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 'ARC'
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:EDI
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:Manufacturer
  ELSIF tmp:SelectManufacturer = 0 And glo:WebJob = 0
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 'ARC'
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:EDI
  ELSIF tmp:SelectManufacturer = 0 And glo:WebJob = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:BranchID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'RRC'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:EDI
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:BranchID
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'RRC'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:EDI
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  ! Disable buttons if no records (DBH: 16/06/2008)
  If ~Records(Queue:Browse)
      ?Button:CustomerService{prop:Disable} = 1
      ?Button:engineeringDetails{prop:Disable} = 1
      ?Button:AuditTrail{prop:Disable} = 1
      ?Button:ValuateClaims{prop:Disable} = 1
      ?Button:ClaimPaid{prop:Disable}= 1
      ?Button:ProcessRejection{prop:Disable} = 1
      ?buttonExport{prop:Disable} = 1
  Else ! If ~Records(Queue:Browse)
      ?Button:CustomerService{prop:Disable} = 0
      ?Button:engineeringDetails{prop:Disable} = 0
      ?Button:AuditTrail{prop:Disable} = 0
      ?Button:ValuateClaims{prop:Disable} = 0
      ?Button:ClaimPaid{prop:Disable}= 0
      ?Button:ProcessRejection{prop:Disable} = 0
      ?buttonExport{prop:Disable} = 0
  End ! If ~Records(Queue:Browse)
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ResetQueue, (BYTE ResetMode))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:SelectManufacturer = 1 And glo:WebJob = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:SelectManufacturer = 0 And glo:WebJob = 1
    RETURN SELF.SetSort(2,Force)
  ELSIF tmp:SelectManufacturer = 1 And glo:WebJob = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF tmp:SelectManufacturer = 0 And glo:WebJob = 0
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())
  locRejectionReason = ''
  
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = jow:RefNumber
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  
  Access:WEBJOB.ClearKey(wob:RefNumberKey)
  wob:RefNumber = jow:RefNumber
  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = jow:RefNumber
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  Access:TRADEACC.ClearKey(tra:Account_Number_Key)
  tra:Account_Number = wob:HeadAccountNumber
  If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Found
  Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
  
  tmp:JobNumber = Clip(job:Ref_Number) & '-' & Clip(tra:BranchIdentification) & Clip(wob:JobNumber)
  
  If job:Exchange_unit_Number = 0
      tmp:CurrentStatus    = 'J '& WOB:Current_status
  Else ! If job:Exchange_unit_Number = 0
      tmp:CurrentStatus    = 'E '& WOB:Exchange_status
  End ! If job:Exchange_unit_Number = 0
  
  
  Case tmp:EDI
  of 'NO' ! Pending
  
  of 'EXC' ! Rejected
      do GetRejectionReason_EXC
  Of 'AAJ' ! Rejection Aknowledged
      do GetRejectionReason_AAJ
  of 'REJ' ! Final Reject
     Do GetRejectionReason_REJ
  End !Case tmp:EDI
  
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW8::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  if (FilterBrowse(tmp:EDI,tmp:FilterByDate,tmp:StartDate,tmp:EndDate) )
      Return Record:Filtered
  end
  
  !
  !if (tmp:FilterByDate = 0)
  !    Return Record:Filtered
  !end
  !case tmp:EDI
  !Of 'NO'
  !    if (tmp:FilterByDate)
  !        if (jow:ClaimSubmitted < tmp:StartDate Or jow:ClaimSubmitted > tmp:EndDate)
  !            Return Record:Filtered
  !        end
  !    end ! if (tmp:FilterByDate)
  !Of 'APP'
  !    if (tmp:FilterByDate)
  !        if (jow:DateAccepted < tmp:StartDate Or jow:DateAccepted > tmp:EndDate)
  !            Return Record:Filtered
  !        end
  !    end
  !Of 'EXC'
  !    if (tmp:FilterByDate)
  !        if (jow:DateRejected < tmp:StartDate Or jow:DateRejected > tmp:EndDate)
  !            Return Record:Filtered
  !        end
  !    end
  !Of 'AAJ'  Orof 'REJ'
  !    if (tmp:FilterByDate)
  !        if (jow:DateFinalRejection < tmp:StartDate Or jow:DateFinalRejection > tmp:EndDate)
  !            Return Record:Filtered
  !        end
  !    end
  !Of 'PAY'
  !    if (tmp:FilterByDate)
  !        if (jow:RRCDateReconciled < tmp:StartDate Or jow:RRCDateReconciled > tmp:EndDate)
  !            Return Record:Filtered
  !        end
  !    end
  !end
  !
  ! ! Insert --- Don't show final rejections (DBH: 27/05/2009) #10838
  !!if (jow:Status = 'REJ')
  !!    return record:Filtered
  !!end ! if (jow:Status = 'REJ')
  !! end --- (DBH: 27/05/2009) #108383
  ReturnValue = PARENT.ValidateRecord()
  BRW8::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(8, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Browse
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    Prog.SkipRecords += 1
    If Prog.SkipRecords < 20
        Prog.RecordsProcessed += 1
        Return 0
    Else
        Prog.SkipRecords = 0
    End
    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.NextRecord()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
SendSMSText          PROCEDURE  (SentJob,SentAuto,SentSpecial) ! Declare Procedure
LocalStatus          STRING(30)
LocalLocation        STRING(1)
localSMSType         STRING(1)
LocalKeyWord         STRING(20)
LocalKeyWordLength   LONG
LocalTextLength      LONG
LocalReplaceText     STRING(50)
LocalPosition        LONG
LocalSendingString   STRING(2000)
Save_Job_ID          USHORT
Save_Wob_ID          USHORT
StartPlace           LONG
EndPlace             LONG
Count                LONG
Tempwebjob           BYTE
ReturnJobType        STRING(10)
ReturnLocation       STRING(20)
TP_Address           STRING(100)
TP_Key               STRING(100)
TP_PathToExe         STRING(255)
TP_Logging           STRING(1)
TP_LogFolder         STRING(255)
TP_SaveOutFault      STRING(30)
TP_SaveGenericFault  STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
   Relate:JOBS.Open
   Relate:SMSText.Open
   Relate:WEBJOB.Open
   Relate:TRADEACC.Open
   Relate:JOBSE.Open
   Relate:SMSMAIL.Open
   Relate:JOBSE2.Open
   Relate:TRADEAC2.Open
   Relate:USERS_ALIAS.Open
   Relate:JOBOUTFL.Open
   Relate:MANFAULT.Open
   Relate:TRANTYPE.Open
!! DON'T FORGET THERE IS A SBONLINE VERSION OF THIS ROUTINE THAT NEEDS TO BE UDPATED TO MATCH !!

!(SentJob, SentAuto, SentSpecial)

!SentJob  J = Job:CurrentStatus, E = ExchangeStatus  L = LoanStatus  could be '2' for second loan?

!SentAuto Y means this is an automatic call  - N means this is a direct call so SMS must be sent

!sentSpecial used to define if this is :
!'B' = the Beyond Economic Repair SMS
!'C' =the "CSI - Customer Service Index" email - only set to if sts:Status[1:3] = '901' or sts:Status[1:3] = '902' or sts:Status[1:3] = '905' 
!'D' = the duplicate estimate reply sms
!'F' = the final estimate SMS (This can also be worked out if this is a normal job in estimate ready statys
!'R' = resend estimate - this may change to final if all resends are used up - (Status must be  "520 Estimate sent")

!glo:WebJob will tell me if this is at ARC or Franchise
!All VCP stuff is handled by Sue - she needs note of the datafiles to use
!jobs should be open on the calling routine
!glo:ErrorText is used for passing back the sms text line to send
!If an error is generated then it returns 'ERROR' plus the error thrown

    Glo:ErrorText = 'ERRORUndefined, untrapped, error occurred'
    Save_Job_ID = Access:Jobs.savefile()
    Save_Wob_ID = Access:Webjob.savefile()

    Do PreparationRoutine   !used so I can use the exit command

    access:jobs.restorefile(Save_Job_id)
    Access:Webjob.restorefile(Save_Wob_ID)

PreparationRoutine      Routine

    Case(SentJob)
        of 'J'
            LocalStatus = Job:Current_Status
            ReturnJobType = 'Job'
            !TB13018 - if an exchange is attached then do not send any job status messages
            if job:Exchange_Unit_Number > 0  then
                glo:ErrorText = 'ERRORThis handset has already been Exchanged.'
                EXIT
            END
        of 'E'
            LocalStatus = job:Exchange_Status
            ReturnJobType = 'Exchange'
        of 'L'
            LocalStatus = job:Loan_Status
            ReturnJobType = 'Loan'
        ELSE
            Glo:ErrorText = 'ERRORUnable to identify job status from '&clip(SentJob)
            EXIT
    END !Case sent job

    !will need jobe2:SMSAlertNumber
    Access:Jobse2.clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    if access:jobse2.fetch(jobe2:RefNumberKey)
        !error
        Glo:ErrorText = 'ERRORUnable to tracker Job extention 2 file from '&clip(Job:Ref_Number)
        EXIT
    END

    if jobe2:SMSNotification = false then
        Glo:ErrorText = 'ERRORJob is not set up to receive SMS notification'
        EXIT
    END

    if clip(jobe2:SMSAlertNumber)  = '' then
        Glo:ErrorText = 'ERRORJob has no SMS Alert number set up.'
        EXIT
    END

    !general setup and checking
    localSMSType = SentSpecial  !normal='N' etc - may be changed immeditately below

    !special cases of BER or Liquid Damage - job may be despatched but these still go through
    if sentSpecial = 'B' or SentSpecial = 'L' then
        !let it go through
    ELSE
        !check for status compatability
        !message('have '&LocalStatus[1:3])
        if LocalStatus[1:3] = '901' or LocalStatus[1:3] = '902' or LocalStatus[1:3] = '905' or LocalStatus[1:3] = '910'  or LocalStatus[1:3] = '916'then
            !this job has been despatched
            do CSITracking         !sets SentSpecial to 'C' if CSI due or X if already sent - or 'N' if not applicable? (one of special cases)
            if SentSpecial = 'X' then
                glo:ErrorText = 'ERRORThis handset has already received the CSI SMS.'
                EXIT
            END
        END !if one of the dispatched statuses

        !note the double if in the middle to let the code look easy
        if SentSpecial = 'C' then
            !it must be one of these
            if LocalStatus[1:3] = '901' or LocalStatus[1:3] = '902' or LocalStatus[1:3] = '905' or LocalStatus[1:3] = '910'  or LocalStatus[1:3] = '916' then
                !or job:Location = DespatchLocation then
                !this is OK
                localSMSType = 'C'
            ELSE
                glo:ErrorText = 'ERRORAttempt to send a CSI text to a handset not in a Despatched status'
                EXIT
            END !
        ELSE
            !It must not be one of these
            if LocalStatus[1:3] = '901' or LocalStatus[1:3] = '902' or LocalStatus[1:3] = '905' or LocalStatus[1:3] = '910'  or LocalStatus[1:3] = '916' then
                !or job:Location = DespatchLocation then
                !this job is despatched
                Glo:ErrorText = 'ERRORAttempt to send a non CSI text to a handset in a Despatched status'
                EXIT
            ELSE
                !this is ok - LocalSMStype stays as it was
            END !
        END

        if LocalStatus[1:3] = '520' then
            !this is an estimate
            localSMSType = 'E'
        END
    END !if sent bype was B or L

!    CASE glo:WebJob
!        of 0
!            LocalLocation = 'A'
!            ReturnLocation = 'ARC'
!        of 1
!            LocalLocation = 'F'
!            ReturnLocation = 'Franchise'
!        ELSE
!            Glo:ErrorText = 'ERRORUnable to identify job location from webjob = '&clip(glo:webjob)
!    END !Selecting

    !a more sophisticated way of finding where the job is now
    !set up Tempwebjob to show where the job is now - in transit to PUP is possible

    IF (SentJob = 'E' or SentJob = 'L')
        ! #13109 I don't see any need to lookup location for Exchange or Loan Statuses (DBH: 17/06/2013)
        Tempwebjob = glo:WebJob
    ELSE ! IF (SentJob = 'E' or SentJob = 'L')
        if instring('FRANCHISE',job:location,1,1) then
            Tempwebjob = 1
        ELSE
            if instring('ARC',Job:Location,1,1) then
                Tempwebjob = 0
            ELSE
                if instring('PUP',Job:location,1,1) then
                    Tempwebjob = 2
                ELSE
                    !something like despatched
                    if instring('FRANCHISE', job:Transit_type,1,1) then
                        Tempwebjob = 1
                    ELSE
                        If instring('ARC',job:transit_type,1,1) then
                            Tempwebjob = 0
                        ELSE
                            if instring('PUP',job:transit_Type,1,1) then
                                Tempwebjob = 2
                            ELSE
                                !I don't know
                                Tempwebjob = Glo:Webjob
                           END !if pup
                        END !if arc
                    END !if at franchise
                END !if location PUP
            END ! at ARC
        END !if at franchise
    END ! IFIF (SentJob = 'E' or SentJob = 'L')

    CASE Tempwebjob
        of 0
            LocalLocation = 'A'
            ReturnLocation = 'ARC'
        of 1
            LocalLocation = 'F'
            ReturnLocation = 'Franchise'
        of 2
            LocalLocation = 'V'
            ReturnLocation = 'PUP'
        ELSE
            Glo:ErrorText = 'ERROR|Unable to identify job location from Location, transit type: ' &clip(Job:Location)&' and '&clip(job:transit_type)
            EXIT
    END !Selecting


!sentSpecial used to define if this is :
!'C' =the "CSI - Customer Service Index" email - only set to if sts:Status[1:3] = '901' or sts:Status[1:3] = '902' or sts:Status[1:3] = '905' 
!'R' = resend estimate - this may change to final if all resends are used up - (Status must be  "520 Estimate sent")
!'D' = the duplicate estimate reply sms

    

    Case SentSpecial
        of 'C'   !Customer service index

            Access:SMSText.clearkey(SMT:Key_Location_CSI)
            SMT:Location = LocalLocation
            SMT:CSI = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_CSI)
                !error
                Glo:ErrorText = 'ERRORCSI system has not been set up for this location'
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)
            !message('Have seams notification of '&clip(SMT:SEAMS_Notification) & 'Record='&clip(SMT:Record_No))
            if SMT:SEAMS_Notification = 'Y' then do SEAMS_Notification.
            

        of 'F' !final estimate sms
            Access:SMSText.clearkey(SMT:Key_StatusType_Location_Trigger)
            SMT:Status_Type = SentJob
            SMT:Location = LocalLocation
            SMT:Trigger_Status = '520 ESTIMATE SENT'
            if access:SMSText.fetch(SMT:Key_StatusType_Location_Trigger)
                Glo:ErrorText = 'ERRORResent estimate system has not been set up for this location'
                EXIT
            END
            !This should only be called when we have reached the final one - before this should have been called on standard not special
            glo:ErrorText = clip(SMT:Final_Estimate_Text)
            !message('Have seams notification of '&clip(SMT:SEAMS_Notification) & 'Record='&clip(SMT:Record_No))
            if SMT:SEAMS_Notification = 'Y' then do SEAMS_Notification.
            

        of 'D' !duplicate estiimate replies received
            Access:SMSText.clearkey(SMT:Key_Location_Duplicate)
            SMT:Location = LocalLocation
            SMT:Duplicate_Estimate = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_Duplicate)
                Glo:ErrorText = 'ERRORDuplicate estimate reply system has not been set up for this location'
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)
            !message('Have seams notification of '&clip(SMT:SEAMS_Notification) & 'Record='&clip(SMT:Record_No))
            if SMT:SEAMS_Notification = 'Y' then do SEAMS_Notification.
            

        of 'B' !beyond economic repair
            Access:SMSText.clearkey(SMT:Key_Location_BER)
            SMT:Location = LocalLocation
            SMT:BER = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_BER)
                Glo:ErrorText = 'ERRORBER reply system has not been set up for this location'
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)
            !message('Have seams notification of '&clip(SMT:SEAMS_Notification) & 'Record='&clip(SMT:Record_No))
            if SMT:SEAMS_Notification = 'Y' then do SEAMS_Notification.


        of 'L' !liquid damage
            Access:SMSText.clearkey(SMT:Key_Location_Liquid)
            SMT:Location = LocalLocation
            SMT:LiquidDamage = 'YES'
            if access:SMSText.fetch(SMT:Key_Location_Liquid)
                Glo:ErrorText = 'ERRORLiquid Damage SMS has not been set up for this location'
                EXIT
            END
            glo:ErrorText = clip(SMT:SMSText)
            !message('Have seams notification of '&clip(SMT:SEAMS_Notification) & 'Record='&clip(SMT:Record_No))
            if SMT:SEAMS_Notification = 'Y' then do SEAMS_Notification.


        ELSE

            !normal status system call
            Access:SMSText.clearkey(SMT:Key_StatusType_Location_Trigger)
            SMT:Status_Type    = SentJob
            SMT:Location       = LocalLocation
            SMT:Trigger_Status = LocalStatus
            if access:SMSText.fetch(SMT:Key_StatusType_Location_Trigger)
                !error
                Glo:ErrorText = 'ERRORNo SMS text is set for the combination: '&clip(ReturnJobType)&', '&clip(ReturnLocation)&', '&clip(LocalStatus)
                EXIT
            END

            !message('Have seams notification of '&clip(SMT:SEAMS_Notification) & 'Record='&clip(SMT:Record_No))
            if SMT:SEAMS_Notification = 'Y' then do SEAMS_Notification.

            if SentAuto = 'Y'
                if SMT:Auto_SMS <> 'YES' then
                    Glo:ErrorText = 'ERRORCall for automatic SMS but the SMSText entry does not permit an automatic send.'
                    EXIT
                END !if not autom_SMS
            END !if sent auto

            glo:ErrorText = clip(SMT:SMSText)

    END !case SentSpecial


    !special case of the  Job:CurrentStatus = '520 ESTIMATE SENT'
    if sentJob = 'J' and LocalSMSType = 'E' 
        do EstimateTracking
        if LocalSMSType = 'X' then exit.
    END !if estimate sent

!Ready to send now =================================================================================
    !interpret the keyword bits
    !There may be more to come so I want to make this easy to expand - just repeat the code in this bit
    !#JobNo
    !#SiteName
    !#Cost
    !#SitePhone
    !#Date+30
    !Each bit intifies the key word to replace, and its length (LocalKeyWord, LocalKeyWordLength)
    !and the word(s) that will replace it (LocalReplaceText)

!will need the trade account open for this bit

    access:webjob.clearkey(wob:refnumberkey)
    wob:refnumber = job:ref_number
    if access:webjob.fetch(wob:refnumberkey)
        !error
    ELSE
        access:tradeacc.clearkey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        if access:tradeacc.fetch(tra:Account_Number_Key)
            !error
        END
        Access:TradeAc2.clearkey(TRA2:KeyAccountNumber)
        tra2:Account_Number = wob:HeadAccountNumber
        If access:TradeAc2.fetch(TRA2:KeyAccountNumber)
            !error
            tra2:coSMSname = ''
        END
    END


!Job number ====================================
    LocalKeyWord       = '#JobNo'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 6
        LocalReplaceText = job:ref_Number
        Do ReplaceKeyWord
    END !if #jobno in text

!Job number ====================================

!Site name =====================================
    LocalKeyWord       = '#SiteName'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 9
        If clip(tra2:coSMSname) = '' then
            LocalReplaceText = clip(tra:Company_Name)
        ELSE
            LocalReplaceText = clip(tra2:coSMSname)
        END !if Company SMS name existed
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Site name =====================================

!Site phone =====================================
    LocalKeyWord       = '#SitePhone'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 10
        LocalReplaceText = clip(tra:Telephone_Number)
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Site phone =====================================

!Date =====================================
    LocalKeyWord       = '#Date+30'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
        LocalKeyWordLength = 8
        LocalReplaceText = format(today()+30,@d06)
        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Date =====================================


!Estimate cost==================================
    LocalKeyWord       = '#Cost'
    if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then

        !TB13192 - change the keywordlength to 5 so that it formats properly
        LocalKeyWordLength = 5

        if LocalLocation = 'A'
            !ARC
            LocalReplaceText = left(format(job:Sub_Total_Estimate + |
                               (job:Courier_Cost_Estimate * (VatRate(job:Account_Number,'L') /100)) + |
                               (job:Parts_Cost_Estimate * (VatRate(job:Account_Number,'P') /100)) +  |
                               (job:Labour_Cost_Estimate * (VatRate(job:Account_Number,'L')/100)),@n9.2))
        ELSE
            !For RRC
            LocalReplaceText  = left(format(jobe:RRCESubTotal + |
                                (job:Courier_Cost_Estimate * (VatRate(job:Account_Number,'L') /100)) + |
                                (jobe:RRCEPartsCost * (VatRate(job:Account_Number,'P') /100)) +  |
                                (jobe:RRCELabourCost * (VatRate(job:Account_Number,'L')/100)),@n9.2))

        END !if at ARC

        !TB13192 - if the value of the estimate is zero do not send the SMS
        if deformat(LocalReplaceText) = 0 then
            Glo:ErrorText = 'ERROREstimate value returned zero for job number '&clip(Job:Ref_Number)
            EXIT
        END

        Do ReplaceKeyWord
    END !if instring(clip(LocalKeyword),clip(Glo:ErrorText),1,1) then
!Estimate cost==================================

    if len(Clip(Glo:ErrorText)) < 160 then

        LocalSendingString = clip(Glo:ErrorText)
        do SendString

    ELSE !if length less that 160
        !cut this up into bits less than 160 long

        !starting conditions
        StartPlace = 1
        EndPlace = 160  !first attempt we know this is over 160

        Loop !work back from the end place to find the first space

            EndPlace -= 1
            if EndPlace = StartPlace then
                !no space??
                !Glo:ErrorText = 'ERRORSpecified text is over 160 characters and contains no spaces in at least one block. Cannot split this'
                break
            END

            if glo:ErrorText[EndPlace] = ' ' then
                LocalSendingString = Glo:ErrorText[ StartPlace : EndPlace - 1 ]
                Do SendString

                if glo:Errortext[1:5] = 'ERROR' then exit. !no point in continueing if it errors

                !new stating place
                StartPlace = EndPlace + 1
                EndPlace = StartPlace + 159
                if EndPlace > Len(clip(Glo:ErrorText)) then
                    !Got the last bit
                    EndPlace = Len(clip(Glo:ErrorText))
                    LocalSendingString = Glo:ErrorText[ StartPlace : EndPlace ]
                    Do SendString
                    BREAK       !done the last bit
                END !if got the last bit
            END !if the end place is a space
        END !loop to find a space
    END  !if length less that 160

    !if we get here it has worked
    Glo:ErrorText = 'SUCCESS'

    exit


ReplaceKeyWord      Routine           !LocalReplaceText

    Loop

        LocalPosition = instring(clip(LocalKeyWord),clip(Glo:ErrorText),1,1)
        if LocalPosition = 0 then break. !not found in the text

        LocalTextLength = len(clip(glo:ErrorText))
        glo:ErrorText = glo:ErrorText[ 1 : LocalPosition - 1 ] & clip(LocalReplaceText) & glo:ErrorText[ LocalPosition + LocalKeyWordLength : LocalTextLength]

    END !loop to find the LocalKeyWord in the Glo:ErrorText

    Exit


CSITracking         Routine
    !message('Doing CSI tracking')
    count = 0
    Access:SMSMail.clearkey(sms:DateTimeInsertedKey)
    sms:RefNumber = Job:Ref_number
    sms:DateInserted = 0
    sms:TimeInserted = 0
    Set(sms:DateTimeInsertedKey,sms:DateTimeInsertedKey)
    loop
        if access:SMSMail.next() then break.
        !message('Got SMS record with no and type '&sms:RecordNumber&sms:SMSType)
        if sms:RefNumber <> Job:Ref_number then break.
        if sms:SMSType = 'C' then
            count =1
            break
        END
    END !loop
    !message('Break from loop with count ='&clip(Count))
    if Count = 1 then
        !message('Returning X')
        SentSpecial  = 'X'
    ELSE
        !message('Returning C')
        SentSpecial = 'C'
    END



EstimateTracking    Routine

    localSMSType = 'E'

    !if the customer has already received the maximum number or emails then send the final
    count = 0
    Access:SMSMail.clearkey(sms:DateTimeInsertedKey)
    sms:RefNumber = Job:Ref_number
    sms:DateInserted = 0
    sms:TimeInserted = 0
    Set(sms:DateTimeInsertedKey,sms:DateTimeInsertedKey)
    loop
        if access:SMSMail.next() then break.
        if sms:RefNumber <> Job:Ref_number then break.
        if sms:SMSType = 'E' then   count += 1.

    END !loop

    if count = SMT:Resend_Estimate_Days +1 then
        !it is time to send the last one
        glo:ErrorText = clip(SMT:Final_Estimate_Text)
            
    END
    if Count > SMT:Resend_Estimate_Days+1 then
        !don't print any
        Glo:ErrorText = 'ERRORThis handset has already received the final estimiate text'
        LocalSMSType = 'X'
    END

    !otherwise leave it as the selected text
    EXIT

SendString      Routine

    If Access:SMSMAIL.PrimeRecord() = Level:Benign
        sms:RefNumber       = job:Ref_Number
        sms:MSISDN          = jobe2:SMSAlertNumber
        sms:EmailAddress    = ''
        sms:SendToSMS       = 'T'
        sms:SendToEmail     = 'F'
        sms:DateInserted    = Today()
        sms:TimeInserted    = Clock()
        sms:SMSSent         = 'F'
        sms:SBUpdated       = 0
        sms:MSG             = clip(LocalSendingString)
        sms:SMSType         = localSMSType
        If Access:SMSMAIL.TryInsert() = Level:Benign
            ! Insert Successful
        Else ! If Access:SMSMAIL.TryInsert() = Level:Benign
            ! Insert Failed
            Access:SMSMAIL.CancelAutoInc()
            Glo:ErrorText = 'ERRORUnable to insert an SMS record - this attempt has failed'
        End ! If Access:SMSMAIL.TryInsert() = Level:Benign
    ELSE
        Glo:ErrorText = 'ERRORUnable to create an SMS record - this attempt has failed'
    End !If Access:SMSMAIL.PrimeRecord() = Level:Benign

    EXIT


SEAMS_Notification      Routine


    if GetIni('Touchpoint','Use','N',clip(Path())&'\SB2KDEF.INI') <> 'Y' then EXIT.
    
    !TB13267 - J - 19/12/14 - add defaults for the Touchpoint integration
    TP_Address    = GetIni('Touchpoint','Address'   ,'' ,    clip(Path())&'\SB2KDEF.INI')
    TP_Key        = GetIni('Touchpoint','Key'       ,'' ,    clip(Path())&'\SB2KDEF.INI')
    TP_PathToExe  = GetIni('Touchpoint','PathToExe' ,'' ,    clip(Path())&'\SB2KDEF.INI')
    TP_Logging    = GetIni('Touchpoint','Log'       ,'N',    clip(Path())&'\SB2KDEF.INI')
    TP_LogFolder  = GetIni('Touchpoint','LogFolder' ,'' ,    clip(Path())&'\SB2KDEF.INI')

    !error checking
    If clip(TP_Address) = '' then
        EXIT
    END
    if clip(TP_Key) = '' then
        EXIT
    END
    if clip(TP_PathToExe) = '' then
        EXIT
    END
    if TP_Logging = 'Y' and clip(TP_LogFolder) = '' then
        exit
    END
    if ~exists(clip(TP_PathToExe))&'\SeamsNotify.exe' then
        Exit
    END

    !gather the details into LocalSendingString
    !message('Processing')

    !For MII Dealer ID set up against the user
    Access:users_alias.clearkey(use_ali:User_Code_Key)
    use_ali:User_Code = job:who_booked
    If access:Users_alias.fetch(use_ali:User_Code_Key) then exit.

    !For tradeaccount
    access:webjob.clearkey(wob:refnumberkey)
    wob:refnumber = job:ref_number
    if access:webjob.fetch(wob:refnumberkey)
        !error
        EXIT
    END

    !For highest level out fault
    TP_SaveOutFault = 'NOT_SET'
    Access:JobOutFl.clearkey(joo:LevelKey)      !By jobnumber then level so the last one is the highest
    joo:JobNumber = job:ref_number
    joo:Level     = 0
    set(joo:LevelKey,joo:LevelKey)
    loop
        if access:JobOutFl.next() then break.
        if joo:JobNumber <> job:ref_number then break.
        TP_SaveOutFault = joo:Description
    END

    !for "Generic" in fault
    TP_SaveGenericFault = 'NOT_SET'
    Access:ManFault.clearkey(maf:Field_Number_Key)  !has manufacturer as first part
    maf:Manufacturer = job:Manufacturer
    maf:Field_Number = 0
    Set(maf:Field_Number_Key,maf:Field_Number_Key)
    Loop
        if access:Manfault.next() then break.
        if maf:Manufacturer <> job:Manufacturer then break.
        if maf:GenericFault then
            Case maf:Field_Number
            Of 1
                TP_SaveGenericFault  = job:Fault_Code1
            Of 2
                TP_SaveGenericFault  = job:Fault_Code2
            Of 3
                TP_SaveGenericFault  = job:Fault_Code3
            Of 4
                TP_SaveGenericFault  = job:Fault_Code4
            Of 5
                TP_SaveGenericFault  = job:Fault_Code5
            Of 6
                TP_SaveGenericFault  = job:Fault_Code6
            Of 7
                TP_SaveGenericFault  = job:Fault_Code7
            Of 8
                TP_SaveGenericFault  = job:Fault_Code8
            Of 9
                TP_SaveGenericFault  = job:Fault_Code9
            Of 10
                TP_SaveGenericFault  = job:Fault_Code10
            Of 11
                TP_SaveGenericFault  = job:Fault_Code11
            Of 12
                TP_SaveGenericFault  = job:Fault_Code12
            of 13
                TP_SaveGenericFault  = wob:FaultCode13
            of 14
                TP_SaveGenericFault  = wob:FaultCode14
            of 15
                TP_SaveGenericFault  = wob:FaultCode15
            of 16
                TP_SaveGenericFault  = wob:FaultCode16
            of 17
                TP_SaveGenericFault  = wob:FaultCode17
            of 18
                TP_SaveGenericFault  = wob:FaultCode18
            of 19
                TP_SaveGenericFault  = wob:FaultCode19
            of 20
                TP_SaveGenericFault  = wob:FaultCode20
            End !Case maf:Field_Number
            Break
        END !if generic fault
    END !loop to find generic fault

    !Jobse2 has been opened above so jobe2:SMSAlertNumber is known
    if clip(use_ali:RRC_MII_Dealer_ID) = '' then use_ali:RRC_MII_Dealer_ID = 'NOTSET'.

    !Trantype
    Access:TranType.clearkey(trt:Transit_Type_Key)
    trt:Transit_Type = job:Transit_type
    if access:TranType.fetch(trt:Transit_Type_Key)
        !error
    END
    if trt:SeamsTransactionCode = '' then trt:SeamsTransactionCode = job:Transit_type. !just in case


    LocalSendingString =' Address='                 &clip(TP_Address)               &|
                        ' Log='                     &TP_Logging                     &|
                        ' Folder='                  &clip(TP_LogFolder)             &|
                        ' AppKey='                  &clip(TP_Key)                   &|
                        ' MSISDN='                  &clip(jobe2:SMSAlertNumber)     &|
                        ' DealerID='                &clip(wob:HeadAccountNumber)    &|  !trade account thing
                        ' AgentID='                 &clip(use_ali:RRC_MII_Dealer_ID)&|  !MII Dealer ID setup against the user on the Insert/Amend User
                        ' TransactionType='         &clip(TP_SaveGenericFault)      &|  !Generic Fault Description selected on the job during booking
                        ' TransactionCode='         &clip(trt:SeamsTransactionCode) &|  !The Tranist Type selected at the point of booking will be used to identify the type of repair
                        ' TransactionDescription='  &clip(TP_SaveOutFault)          &|  !The Outfault of the job will be used in this field
                        ' SystemID='                &'SB-01'                        &|  !This will be hardcoded as SB-01
                        ' Latency='                 &'-1'                           &|  !This will be hardcoded as -1
                        ' DomainName='              &clip(wob:HeadAccountNumber)    &|  !Duplicate of DealerID
                        ' IMEI='                    &clip(Job:ESN)                  &|
                        ' JobNumber='               &clip(Job:Ref_number)           &|
                        ' Manufacturer='            &clip(Job:Manufacturer)         &|
                        ' Model='                   &clip(Job:model_number)
    !call the exe
    run(clip(TP_PathToExe)&'\SeamsNotify.exe'&clip(LocalSendingString) ,0)
    !message('DONE|'&clip(LocalSendingString))



    EXIT
   Relate:JOBS.Close
   Relate:SMSText.Close
   Relate:WEBJOB.Close
   Relate:TRADEACC.Close
   Relate:JOBSE.Close
   Relate:SMSMAIL.Close
   Relate:JOBSE2.Close
   Relate:TRADEAC2.Close
   Relate:USERS_ALIAS.Close
   Relate:JOBOUTFL.Close
   Relate:MANFAULT.Close
   Relate:TRANTYPE.Close
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
GetImportFilename PROCEDURE                           !Generated from procedure template - Window

LocalFilename        STRING(255)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       BUTTON,AT(409,142,28,24),USE(?ButtonHelp),TRN,FLAT,KEY(F1Key),ICON('F1Helpsw.jpg')
                       PANEL,AT(245,148,159,12),USE(?panelSellfoneTitle),FILL(09A6A7CH)
                       PROMPT('Select Import File'),AT(249,151),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(344,151),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(245,164,192,90),USE(?Panel3),FILL(09A6A7CH)
                       ENTRY(@s255),AT(257,206,124,10),USE(LocalFilename),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                       BUTTON,AT(393,201),USE(?LookupFile),FLAT,ICON('lookupp.jpg')
                       PANEL,AT(245,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(284,258),USE(?ButtonOK),FLAT,ICON('okp.jpg')
                       BUTTON,AT(365,258),USE(?ButtonCancel),TRN,FLAT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup10         SelectFileClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020775'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GetImportFilename')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','GetImportFilename')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup10.Init
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:LongName)
  FileLookup10.SetMask('CSV Files','*.csv')
  FileLookup10.WindowTitle='Import File'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','GetImportFilename')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ButtonOK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
      filename3 = LocalFilename
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    OF ?ButtonCancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
      filename3 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020775'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020775'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020775'&'0')
      ***
    OF ?LookupFile
      ThisWindow.Update
      LocalFilename = FileLookup10.Ask(1)
      DISPLAY
    OF ?ButtonOK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOK, Accepted)
    OF ?ButtonCancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonCancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
