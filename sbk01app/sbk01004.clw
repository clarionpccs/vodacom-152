

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01004.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
MultipleBatchDespatch PROCEDURE                       !Generated from procedure template - Window

tmp:CloseWindow      BYTE(0)
save_muld_id         USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_lac_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
tmp:BatchNumber      LONG
tmp:Error            BYTE(0)
tmp:AccountNumber    STRING(30)
tmp:Courier          STRING(30)
tmp:JobNumber        STRING(30)
tmp:IMEINumber       STRING(30)
tmp:ErrorMessage     STRING(255)
tmp:BeginBatch       BYTE(0)
tmp:PrintSummaryNote BYTE(0)
tmp:FirstRecord      BYTE(0)
tmp:PrintDespatchNote BYTE(0)
Despatch_Label_Type_Temp STRING(3)
tmp:DoDespatch       BYTE(0)
save_job_ali_id      USHORT,AUTO
sav:Path             STRING(255)
tmp:LabelError       STRING(30)
Account_Number2_Temp STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:ParcellineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
tmp:ConsignNo        STRING(30)
tmp:IndividualDespatch BYTE(0)
Courier_Temp         STRING(30)
tmp:PassedConsignNo  STRING(30)
local:FileName       STRING(255),STATIC
tmp:ReturnedBatchNumber LONG
tmp:CompanyName      STRING(30)
tmp:PrintDespatch    BYTE(0)
tmp:HeadAccountNumber STRING(30)
tmp:SecurityPackNumber STRING(30)
locAuditNotes        STRING(255)
BRW4::View:Browse    VIEW(MULDESP)
                       PROJECT(muld:AccountNumber)
                       PROJECT(muld:Courier)
                       PROJECT(muld:BatchNumber)
                       PROJECT(muld:BatchTotal)
                       PROJECT(muld:BatchType)
                       PROJECT(muld:RecordNumber)
                       PROJECT(muld:HeadAccountNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
muld:AccountNumber     LIKE(muld:AccountNumber)       !List box control field - type derived from field
tmp:CompanyName        LIKE(tmp:CompanyName)          !List box control field - type derived from local data
muld:Courier           LIKE(muld:Courier)             !List box control field - type derived from field
muld:BatchNumber       LIKE(muld:BatchNumber)         !List box control field - type derived from field
muld:BatchTotal        LIKE(muld:BatchTotal)          !List box control field - type derived from field
muld:BatchType         LIKE(muld:BatchType)           !List box control field - type derived from field
muld:RecordNumber      LIKE(muld:RecordNumber)        !Primary key field - type derived from field
muld:HeadAccountNumber LIKE(muld:HeadAccountNumber)   !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Multiple Batch Despatch'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Multiple Batch Despatch'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,288,552,74),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Input Batch'),USE(?Tab2)
                           PROMPT('Job Number'),AT(140,304),USE(?tmp:JobNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(232,304,64,10),USE(tmp:JobNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),REQ,UPR
                           PROMPT('Batch Number'),AT(368,304),USE(?TheBatchNumber),HIDE,FONT(,14,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(140,320),USE(?tmp:IMEINumber:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(232,320,124,10),USE(tmp:IMEINumber),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),REQ,UPR
                           PROMPT('Security Pack Number'),AT(140,336),USE(?tmp:SecurityPackNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(232,336,124,10),USE(tmp:SecurityPackNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Security Pack Number'),TIP('Security Pack Number'),UPR
                           BUTTON,AT(364,330),USE(?ProcessJob),TRN,FLAT,LEFT,ICON('projobp.jpg')
                         END
                       END
                       SHEET,AT(64,54,552,230),USE(?Sheet1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('Batches In Progress'),USE(?Tab1)
                           ENTRY(@s30),AT(140,78,124,10),USE(muld:AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Account Number'),TIP('Account Number'),UPR
                           GROUP,AT(544,126,56,152),USE(?BatchButtons)
                             BUTTON,AT(548,136),USE(?FinishCurrentBatch),TRN,FLAT,LEFT,ICON('finbatp.jpg')
                             BUTTON,AT(547,223),USE(?Delete),TRN,FLAT,LEFT,ICON('delbatp.jpg')
                             BUTTON,AT(547,255),USE(?ListJobsInBatch),TRN,FLAT,LEFT,ICON('lisbatp.jpg')
                           END
                           LIST,AT(140,94,400,186),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('68L(2)|M~Account Number~@s30@120L(2)|M~Company Name~@s30@91L(2)|M~Courier~@s30@4' &|
   '3L(2)|M~Batch No~@s4@64L(2)|M~Total In Batch~@s8@12L(2)|M~Batch Type~@s3@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(548,366),USE(?CloseDespatch),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW4::Sort1:Locator  EntryLocatorClass                !Conditional Locator - tmp:HeadAccountNumber = ''
BRW4::Sort2:Locator  EntryLocatorClass                !Conditional Locator - tmp:HeadAccountNumber <> ''
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    Map
LocalValidateAccessories    Procedure(),Byte
LocalValidateIMEI           Procedure(),Byte
LocalValidateAccountNumber  Procedure(),Byte
    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
UpdateBatch     Routine
Data
loc:DespatchType    String(3)
Code

    tmp:IndividualDespatch = 0

    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number  = tmp:JobNumber
    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign


        !TB12540 - Show contact history if there is a stick note
        !need to find one that has not been cancelled, and is valid for this request
        Access:conthist.clearkey(cht:KeyRefSticky)
        cht:Ref_Number = job:ref_number
        cht:SN_StickyNote = 'Y'
        Set(cht:KeyRefSticky,cht:KeyRefSticky)      
        Loop
            if access:Conthist.next() then break.
            IF cht:Ref_Number <> job:ref_number then break.
            if cht:SN_StickyNote <> 'Y' then break.
            if cht:SN_Completed <> 'Y' and cht:SN_Despatch = 'Y' then
                glo:select12 = job:ref_number
                Browse_Contact_History
                BREAK
            END
        END


        !Found
        Error# = 0

        If JobInUse(job:Ref_Number,1)
            Brw4.ResetSort(1)
            Select(?tmp:JobNumber)
            Exit
        End !If JobInUse(job:Ref_Number,1)


        !Is the job actually ready to despatch
        If Error# = 0
            If glo:webJob
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                If jobe:DespatchType <> 'JOB' And jobe:DespatchType <> 'EXC' and jobe:DespatchType <> 'LOA'
                    Error# = 1
                End !If job:Despatch_Type <> 'JOB' And job:Despatch_Type <> 'EXC' and job:Despatch_Type <> 'LOA'
            Else !If glo:webJob
                If job:Despatch_Type <> 'JOB' And job:Despatch_Type <> 'EXC' and job:Despatch_Type <> 'LOA'
                    Error# = 1
                End !If job:Despatch_Type <> 'JOB' And job:Despatch_Type <> 'EXC' and job:Despatch_Type <> 'LOA'
            End !If glo:webJob
        End !If Error# = 0

        ! Inserting (DBH 06/02/2008) # 9613 - Check if the delivery address (assume Sub Account address) is outside the region
        If Error# = 0 And glo:WebJob
            Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
                If HubOutOfRegion(Clarionet:Global.Param2,sub:Hub) = 1
                    If ReleasedForDespatch(job:Ref_Number) = 0
                        Error# = 1
                    End ! If ReleasedForDespatch(job:Ref_Number) = 0
                End ! If HubOutOfRegion(Clarionet:Global.Param2,jobe2:HubCustomer) = 1
            End ! If Access:SUBTRACC.Clearkey(sub:Account_Number_Key) = Level:Benign
            If Error# = 1
                Beep(Beep:SystemHand);  Yield()
                Case Missive('Cannot Despatch!'&|
                    '|'&|
                    '|The delivery address is outside your region.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                Exit
            End ! If Error# = 1
        End ! If Error# = 0
        ! End (DBH 06/02/2008) #9613

        If Error# = 0 and ~glo:WebJob
            Case job:Despatch_Type
                Of 'JOB'
                    If job:Consignment_Number <> ''
                        Error# = 1
                    End !If job:Consignment_Number <> ''
                Of 'EXC'
                    If job:Exchange_Consignment_Number <> ''
                        Error# = 1
                    End !If job:Exchange_Consignment_Number <> ''
                Of 'LOA'
                    If job:Loan_Consignment_Number <> ''
                        Error# = 1
                    End !If job:Loan_Consignment_Number <> ''
            End !Case job:Despatch_Type
        End !If tmp:Error = 0.
        If Error# = 1
            Case Missive('The selected job is not ready to despatch.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive
        End !If Error# = 1

        ! Inserting (DBH 24/01/2007) # 8678 - Has the job been paid/invoiced, and therefore can it be despatched?
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
        Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
        End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        Access:WEBJOB.ClearKey(wob:RefNumberKEy)
        wob:RefNumber = job:Ref_Number
        If Access:WEBJOB.TryFetch(wob:RefNumberKEy) = Level:Benign
            !Found
        Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKEy) = Level:Benign
            !Error
        End ! If Access:WEBJOB.TryFetch(wob:RefNumberKEy) = Level:Benign

        If CanJobBeDespatched() = 0
            Exit
        End ! If CanJobBeDespatched() = 0
        ! End (DBH 24/01/2007) #8678

        !Does a courier exist?
        Access:COURIER.ClearKey(cou:Courier_Key)

        IF glo:WebJob
            loc:DespatchType   = jobe:DespatchType
        Else
            loc:DespatchType   = job:Despatch_Type
        End !IF glo:WebJob

        Case loc:DespatchType
            Of 'JOB'
                If job:Courier = ''
                    Case Missive('The selected job does not have a courier assigned to it.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Error# = 1
                End !If job:Courier = ''
                cou:Courier = job:Courier

            Of 'EXC'
                If job:Exchange_Courier = ''
                    Case Missive('The selected job''s exchange unit does not have a courier assigned to it.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Error# = 1
                End !If job:Exchange_Courier = ''
                cou:Courier = job:Exchange_Courier
            Of 'LOA'
                If job:Loan_Courier = ''
                    Case Missive('The selected job''s loan unit does not have a courier assigned to it.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Error# = 1
                End !If job:Loan_Courier = ''
                cou:Courier = job:Loan_Courier
        End !Case job:Despatch_Type

        !Has the job got a loan attached?
        If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            If (glo:WebJob = 1 OR (glo:WebJob = 0 AND jobe:WebJob <> 1))
                ! #11817 Only stop despatch if RRC, or ARC back to customer. (Bryan: 11/05/2011)
                If job:Loan_Unit_Number <> 0 And loc:DespatchType = 'JOB' AND job:Who_Booked <> 'WEB' ! #12259 Allow to despatch if VCP job. (Bryan: 26/08/2011)
                    Case Missive('Cannot despatch!'&|
                      '<13,10>'&|
                      '<13,10>This loan unit attached to this job has not been returned.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Error# = 1
                End !If job:Loan_Unit_Number <> 0
            END ! If (glo:WebJob = 0 AND jobe:WebJob <> 1)
        End !If GETINI('DESPATCH','DoNotDespatchLoan',,CLIP(PATH())&'\SB2KDEF.INI') = 1


        !Should the courier be despatched from here?
        If Error# = 0
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Found
                If ~glo:webJob
                    !There are no fancy export routines for RRC Despatches... WE HOPE!
                    If cou:courier_Type = 'ANC' Or cou:Courier_Type = 'ROYAL MAIL' Or |
                        cou:Courier_Type = 'PARCELINE' or cou:Courier_Type = 'UPS'
                        Case Missive('The selected job''s courier can only be despatched from "Despatch Procedures".','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                        Error# = 1
                    End !
                    !If Tote, force Individual Despatch
                    If cou:Courier_Type = 'TOTE'
                        tmp:IndividualDespatch = 1
                    End !If cou:Courier_Type = 'TOTE'
                End !If ~glo:webJob
            Else!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
        End !If Error# = 0

        !Is this Job already attached to a batch?
        If Error# = 0
            If glo:WebJob
                If IsJobAlreadyInBatch(job:Ref_Number,Clarionet:Global.Param2)
                    Error# = 1
                End !If IsJobAlreadyInBatch(job:Ref_Number,Clarionet:Global.Param2)
            Else !If glo:WebJob
                If IsJobAlreadyInBatch(job:Ref_Number,GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
                    Error# = 1
                End !(job:Ref_Number,GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))
                
            End !If glo:WebJob
            
            If Error# = 1
                Case Missive('The selected job is already attached to a batch.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Error# = 1
            End !If Local.JobAlreadyInBatch(glo:WebJob)
        End !If Error# = 0

        !Is the job's account only used for Batch Despatch
        !Although I don't think any uses this feature.
        If Error# = 0
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            If Access:SUBTRACC.Fetch(sub:Account_Number_Key) = Level:Benign
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                If Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
                    If tra:Skip_Despatch = 'YES' And ~glo:WebJob
                        Case Missive('The selected job''s account can only be used for "Batch Despatch".','ServiceBase 3g',|
                                       'mstop.jpg','/OK') 
                            Of 1 ! OK Button
                        End ! Case Missive
                        Error# = 1
                    end!if tra:use_sub_accounts = 'YES'
                    If Error# = 0
                        If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                            If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                                Case Missive('The selected job''s trade account is on stop.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Error# = 1
                            End !If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                            If sub:UseCustDespAdd = 'YES'
                                tmp:IndividualDespatch = 1
                            End !If sub:UseCustDespAdd = 'YES'
                        Else !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                            If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                                Case Missive('The selected job''s trade account is on stop.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Error# = 1
                            End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                            If tra:UseCustDespAdd = 'YES'
                                tmp:IndividualDespatch = 1
                            End !If tra:UseCustDespAdd = 'YES'
                        End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                    End !If Error# = 0

                end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
            end!if access:subtracc.fetch(sub:account_number_key) = level:benign  
        End !If Error# = 0

        !If the IMEI number has been used, is it correct?
        If Error# = 0
            If ?tmp:IMEINumber{prop:hide} = 0
                !If the IMEI number is required, does it match the job/loan/exchange?
                If DespatchIMEIOK(loc:DespatchType,tmp:IMEINumber)
                    Error# = 1
                End !If DespatchIMEIOK(job:Despatch_Type,tmp:IMEINumber)
            End !If ?tmp:IMEINumber{prop:hide} = 0
        End !If Error# = 0

    Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        !Error
        Case Missive('The selected job number does not exist.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
        Error# = 1
    End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

    If def:Force_Accessory_Check = 'YES' And Error# = 0 and ~tmp:IndividualDespatch
        If LocalValidateAccessories()
            Access:JOBS.Update()
            !Failed Check
            Error# = 1
        End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
    End !If def:Force_Accessory_Check = 'YES'


    If Error# = 0
        If tmp:IndividualDespatch
            !If the job shouldn't be in a batch, then all the normal
            !web individual despatch process
            If glo:WebJob
                glo:Select1 = job:Ref_Number
                RemoteDespatch(job:Ref_Number,loc:DespatchType)
                glo:Select1 = ''
            Else !If glo:WebJob
                Error# = 0
                If def:Force_Accessory_Check = 'YES'
                    If LocalValidateAccessories()
                        Access:JOBS.Update()
                        !Failed Check
                        Error# = 1
                    End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
                End !If def:Force_Accessory_Check = 'YES'

                If Error# = 0
                    !Get the Courier. 
                    Access:COURIER.ClearKey(cou:Courier_Key)
                    If glo:WebJob
                        loc:DespatchType = jobe:DespatchType
                    Else !If glo:WebJob
                        loc:DespatchType = job:Despatch_Type
                    End !If glo:WebJob
                    Case loc:DespatchType
                        Of 'JOB'
                            cou:Courier = job:Courier
                        Of 'EXC'
                            cou:Courier = job:Exchange_Courier
                        Of 'LOA'
                            cou:Courier = job:Loan_Courier
                    End !Case job:Despatch_Type
                    If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                        !Found
                        If access:desbatch.primerecord() = Level:Benign
                            If access:desbatch.tryinsert()
                                access:desbatch.cancelautoinc()
                            Else!If access:desbatch.tryinsert()
                                If glo:WebJob
                                    
                                Else !If glo:WebJob
                                    !Another bodge. Too many places to change DespatchSingle,
                                    !so will pass Security Pack Number as a global. This means
                                    !that is will not ask again if you fill it in during multiple despatch
                                    !Have set SelectGlobal20 to avoid picking up some random value
                                    glo:Select20 = 'SECURITYPACKNO'
                                    glo:Select21 = tmp:SecurityPackNumber

                                    DespatchSingle()

                                    glo:Select20 = ''
                                    glo:Select21 = ''
                                End !If glo:WebJob
                                
                                ?TheBatchNumber{prop:Hide} = 1
                            End!If access:desbatch.tryinsert()
                        End!If access:desbatch.primerecord() = Level:Benign

                    Else!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

                End !If Error# = 0
            End !If glo:WebJob
        Else !If tmp:IndividualDespatch
            If job:Chargeable_Job = 'YES' and loc:DespatchType = 'JOB'
                If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                    If CheckPaid() = Level:Benign
                        glo:Select1 = job:Ref_Number
                        Browse_Payments
                        glo:Select1 = ''
                    End !If CheckPaid() = Level:Benign
                End !If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            End !If job:Chargeable_Job = 'YES'
            tmp:ReturnedBatchNumber = DespatchProcess(tmp:JobNumber,tmp:SecurityPackNumber)
            If tmp:ReturnedBatchNumber <> 0
                !Ok, can we do the invoicing stuff here?

                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_NUmber  = tmp:JobNumber
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                    tmp:PrintDespatch = 0

                    If InvoiceSubAccounts(job:Account_Number)
                        If sub:InvoiceAtDespatch and loc:DespatchType = 'JOB'
                            !Is it manual or automatic?
                            Case sub:InvoiceType
                                Of 0 !Manual
                                    If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                        DespatchInvoice(1)
                                    End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                Of 1 !Automatic
                                    PrintInvoice# = 0
                                    If job:Invoice_Number = ''
                                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                            PrintInvoice# = 0
                                            If job:Invoice_Number = ''
                                                If CreateInvoice() = Level:Benign
                                                    PrintInvoice# = 1
                                                End !If CreateInvoice() = Level:Benign
                                            Else !If job:Invoice_Number = ''
                                                PrintInvoice# = 1
                                            End !If job:Invoice_Number = ''
                                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                    Else
                                        PrintInvoice# = 1
                                    End !If job:Invoice_Number = ''
                                    If PrintInvoice#
                                        glo:Select1 = job:Invoice_Number
!                                        IF glo:webjob
!                                          Vodacom_Delivery_Note_Web('','')
!                                        ELSE
!                                          Single_Invoice('','')
!                                        END
                                        VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','') ! #11881 New Single Invoice (Bryan: 16/03/2011)
                                        glo:Select1 = ''
                                    End !If PrintInvoice#

                            End !Case sub:InvoiceType
                        End !If sub:InvoiceAtDespatch

                    Else !If InvoiceSubAccounts(job:Account_Number)
                        If tra:InvoiceAtDespatch and loc:DespatchType = 'JOB'
                            !Is it manual or automatic?
                            Case tra:InvoiceType
                                Of 0 !Manual
                                    If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                        DespatchInvoice(1)
                                    End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                Of 1 !Automatic
                                    If job:Invoice_Number = ''
                                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                            PrintInvoice# = 0
                                            If job:Invoice_Number = ''
                                                If CreateInvoice() = Level:Benign
                                                    PrintInvoice# = 1
                                                End !If CreateInvoice() = Level:Benign
                                            Else !If job:Invoice_Number = ''
                                                PrintInvoice# = 1
                                            End !If job:Invoice_Number = ''
                                            If PrintInvoice#
                                                glo:Select1 = job:Invoice_Number
!                                                IF glo:webjob
!                                                  Vodacom_Delivery_NOte_Web('','')
!                                                ELSE
!                                                  Single_Invoice('','')
!                                                END
                                                VodacomSingleInvoice(job:Invoice_Number,job:Ref_Number,'','') ! #11881 New Single Invoice (Bryan: 16/03/2011)
                                                glo:Select1 = ''
                                                
                                            End !If PrintInvoice#
                                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                                    End !If job:Invoice_Number = ''
                            End !Case sub:InvoiceType
                        End !If sub:InvoiceAtDespatch

                    End !If InvoiceSubAccounts(job:Account_Number)

                    !Only print despatch notes for warranty jobs...I hope so
                    If job:Warranty_Job = 'YES'
                        If tra:Use_Sub_Accounts = 'YES'
                            If sub:Print_Despatch_Despatch = 'YES'
                                If sub:Despatch_Note_Per_Item = 'YES'
                                    tmp:PrintDespatch = 1
                                End !If sub:Despatch_Note_Per_Item = 'YES'
                            End !If sub:Print_Despatch_Despatch = 'YES'
                            
                        Else !If tra:Use_Sub_Accounts = 'YES'
                            If tra:Print_Despatch_Despatch = 'YES'
                                If tra:Despatch_Note_Per_Item = 'YES'
                                    tmp:PrintDespatch = 1
                                End !If tra:Despatch_Note_Per_Item = 'YES'
                            End !If tra:Print_Despatch_Despatch = 'YES'
                        End !If tra:Use_Sub_Accounts = 'YES'
            !            End !If ~func:Multiple

                        If tmp:PrintDespatch
                            glo:select1  = job:ref_number
                            If cou:CustomerCollection = 1
                                glo:Select2 = cou:NoOfDespatchNotes
                            Else!If cou:CustomerCollection = 1
                                glo:Select2 = 1
                            End!If cou:CustomerCollection = 1
                            Despatch_Note
                            glo:select1 = ''
                            glo:Select2 = ''
                        End!If print_despatch# = 1
                    End !If job:Warranty_Job = 'YES'
                Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                ?TheBatchNumber{prop:Text} = 'Last Job Number On Batch: ' & tmp:JobNumber
                ?TheBatchNumber{prop:Hide} = 0
            Else
                ?TheBatchNumber{prop:Hide} = 1
            End !If tmp:ReturnedBatchNumber <> 0
        End !If tmp:IndividualDespatch
        
    End !If Error# = 0

    !Error Checking
    If Error# = 1
        ?TheBatchNumber{prop:Hide} = 1
    End !If Error# = 1

    Brw4.ResetSort(1)
    Select(?tmp:JobNumber)
ShowError       Routine
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020538'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MultipleBatchDespatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:LOANACC.Open
  Relate:MULDESP.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:SUBTRACC.UseFile
  Access:COURIER.UseFile
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBACC.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:JOBSE.UseFile
  Access:CONTHIST.UseFile
  SELF.FilesOpened = True
  If glo:WebJob
      tmp:HeadAccountNumber = Clarionet:Global.Param2
  Else !glo:WebJob
      tmp:HeadAccountNumber =  GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
  End !glo:WebJob
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:MULDESP,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:ValidateDesp = 'YES'
      ?tmp:IMEINumber{prop:Hide} = 0
      ?tmp:IMEINumber:Prompt{prop:Hide} = 0
  Else
      ?tmp:IMEINumber{prop:Hide} = 1
      ?tmp:IMEINumber:Prompt{prop:Hide} = 1
  End !def:ValidateDesp
  ! Save Window Name
   AddToLog('Window','Open','MultipleBatchDespatch')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,muld:AccountNumberKey)
  BRW4.AddLocator(BRW4::Sort1:Locator)
  BRW4::Sort1:Locator.Init(?muld:AccountNumber,muld:AccountNumber,1,BRW4)
  BRW4.AddSortOrder(,muld:HeadAccountKey)
  BRW4.AddRange(muld:HeadAccountNumber,tmp:HeadAccountNumber)
  BRW4.AddLocator(BRW4::Sort2:Locator)
  BRW4::Sort2:Locator.Init(?muld:AccountNumber,muld:AccountNumber,1,BRW4)
  BRW4.AddSortOrder(,muld:AccountNumberKey)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?muld:AccountNumber,muld:AccountNumber,1,BRW4)
  BIND('tmp:CompanyName',tmp:CompanyName)
  BRW4.AddField(muld:AccountNumber,BRW4.Q.muld:AccountNumber)
  BRW4.AddField(tmp:CompanyName,BRW4.Q.tmp:CompanyName)
  BRW4.AddField(muld:Courier,BRW4.Q.muld:Courier)
  BRW4.AddField(muld:BatchNumber,BRW4.Q.muld:BatchNumber)
  BRW4.AddField(muld:BatchTotal,BRW4.Q.muld:BatchTotal)
  BRW4.AddField(muld:BatchType,BRW4.Q.muld:BatchType)
  BRW4.AddField(muld:RecordNumber,BRW4.Q.muld:RecordNumber)
  BRW4.AddField(muld:HeadAccountNumber,BRW4.Q.muld:HeadAccountNumber)
  BRW4.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  Brw4.InsertControl = 0
  Brw4.ChangeControl = 0
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:LOANACC.Close
    Relate:MULDESP.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','MultipleBatchDespatch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMulDesp
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020538'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020538'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020538'&'0')
      ***
    OF ?ProcessJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessJob, Accepted)
      Do UpdateBatch
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessJob, Accepted)
    OF ?FinishCurrentBatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishCurrentBatch, Accepted)
      Case Missive('Are you sure you want to finish and despatch this batch?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
              Brw4.UpdateViewRecord()
      
              InUse# = 0
              If GETINI('MULTIPLEDESPATCH',muld:RecordNumber,,CLIP(PATH())&'\DESPATCH.LOG') = 'InUse'
                  Case Missive('The selected batch appears to be in use by another station. If you continue despatching, corruption may occur.'&|
                    '<13,10>If you are SURE this batch is not in use, click "Confirm". Otherwise click "Continue" and try again.','ServiceBase 3g',|
                                 'mexclam.jpg','/Continue|\Confirm') 
                      Of 2 ! Confirm Button
                          InUse# = 1
                      Of 1 ! Continue Button
                  End ! Case Missive
      
              End !If Sub(Format(Clock(),@t01),4,2) = GETINI('MULTIPLEDESPATCH',muld:BatchNUmber,,CLIP(PATH())&'\DESPATCH.LOG')
      
              If InUse# = 0
                  PUTINI('MULTIPLEDESPATCH',muld:RecordNumber,'InUse',CLIP(PATH()) & '\DESPATCH.LOG')
      
                  tmp:BatchNumber = muld:RecordNumber
      
                  tmp:PrintSummaryNote = 0
                  If muld:BatchType = 'TRA'
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = muld:AccountNumber
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          If tra:Print_Despatch_Despatch = 'YES'
                              If tra:Summary_Despatch_Notes = 'YES'
                                  tmp:PrintSummaryNote = 1
                              End !If tra:Summary_Despatch_Notes = 'YES'
                          End !If tra:Print_Despatch_Despatch = 'YES'
                          Despatch_Label_Type_Temp = 'TRA'
      
                      Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
                  Else !If muld:BatchType = 'TRA'
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = muld:AccountNumber
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Found
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = sub:Main_Account_Number
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Found
                              If tra:Use_Sub_Accounts = 'YES'
                                  If sub:Print_Despatch_Despatch  = 'YES'
                                      If sub:Summary_Despatch_Notes = 'YES'
                                          tmp:PrintSummaryNote = 1
                                      End !If sub:Summary_Despatch_Notes = 'YES'
                                  End !If sub:Print_Despatch_Despatch  = 'YES'
                                  Despatch_Label_Type_temp    = 'SUB'
                              Else !If tra:Use_Sub_Accounts = 'YES'
                                  If tra:Print_Despatch_Despatch = 'YES'
                                      If tra:Summary_Despatch_Notes = 'YES'
                                          tmp:PrintSummaryNote = 1
                                      End !If tra:Summary_Despatch_Notes = 'YES'
                                  End !If tra:Print_Despatch_Despatch = 'YES'
                                  Despatch_Label_Type_Temp = 'TRA'
      
                              End !If tra:Use_Sub_Accounts = 'YES'
                          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          
                      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  End !If muld:BatchType = 'TRA'
                  !Fill in old variables
                  Error# = 0
                  Courier_Temp = muld:Courier
                  Account_Number2_Temp = muld:AccountNumber
                  If access:desbatch.primerecord() = Level:Benign
                      If access:desbatch.tryinsert()
                          access:desbatch.cancelautoinc()
                      Else!If access:desbatch.tryinsert()
                          !Despatch procedure should work for web and non web
                          If DespatchMultiple(muld:Courier,muld:AccountNumber,tmp:BatchNumber,Despatch_Label_Type_Temp,muld:BatchType) = False
                              !Do not continue if an error occurs, i.e. jobs in use - 3939 (DBH: 26-03-2004)
                              Error# = 1
                          End !If DespatchMultiple(muld:Courier,muld:AccountNumber,tmp:BatchNumber,Despatch_Label_Type_Temp,muld:BatchType) = False
                      End!
                  End!If access:desbatch.tryinsert()
      
      
                  If Error# = 0
                      !Delete the records from the files.
                      !Relate delete didn't work, as usual, will do it manually
                      Save_mulj_ID = Access:MULDESPJ.SaveFile()
                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                      mulj:RefNumber = tmp:BatchNumber
                      Set(mulj:JobNumberKey,mulj:JobNumberKey)
                      Loop
                          If Access:MULDESPJ.NEXT()
                             Break
                          End !If
                          If mulj:RefNumber <> tmp:BatchNumber   |
                              Then Break.  ! End If
                          Delete(MULDESPJ)
                      End !Loop
                      Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
                      !Reget the record to make sure I'm deleteing the right thing
                      Access:MULDESP.ClearKey(muld:RecordNumberKey)
                      muld:RecordNumber = tmp:BatchNumber
                      If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                          !Found
                          
                          Delete(MULDESP)
                      Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                  End !If Error# = 0
                  PUTINI('MULTIPLEDESPATCH',muld:RecordNumber,'Free',CLIP(PATH()) & '\DESPATCH.LOG')
              End !If InUse# = 0
      
          Of 1 ! No Button
      End ! Case Missive
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishCurrentBatch, Accepted)
    OF ?ListJobsInBatch
      ThisWindow.Update
      ListJobsOnBatch(brw4.q.muld:RecordNumber)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ListJobsInBatch, Accepted)
      brw4.UpdateViewRecord
      !Count how many jobs there are on the batch.
      !I could just add one to the record, but I can see
      !that being too inaccurate. So I'll physically count
      !all the records in the batch instead.
      
      CountBatch# = 0
      Save_mulj_ID = Access:MULDESPJ.SaveFile()
      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
      mulj:RefNumber = muld:RecordNumber
      Set(mulj:JobNumberKey,mulj:JobNumberKey)
      Loop
          If Access:MULDESPJ.NEXT()
             Break
          End !If
          If mulj:RefNumber <> muld:RecordNumber      |
              Then Break.  ! End If
          CountBatch# += 1
      End !Loop
      Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
      muld:BatchTotal = CountBatch#
      Access:MULDESP.TryUpdate()
      
      Brw4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ListJobsInBatch, Accepted)
    OF ?CloseDespatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CloseDespatch, Accepted)
          tmp:CloseWindow = 1
          Post(Event:CloseWindow)
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CloseDespatch, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ~tmp:CloseWindow
          Cycle
      End !tmp:CloseWindow
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F10Key
              Post(Event:Accepted,?FinishCurrentBatch)
      
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
        IF GetComputerName(JB:ComputerName,JB:ComputerLen).
        tmp:WorkstationName=JB:ComputerName
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode             Byte
local:Despatch_Type         like(job:Despatch_Type)
    Code

    if glo:WebJob
        local:Despatch_Type = jobe:DespatchType 
    else
        local:Despatch_Type = job:Despatch_Type
    end

    local:ErrorCode  = 0
    Case local:Despatch_Type
        Of 'JOB'
            If job:Exchange_Unit_Number <> ''
                !If an exchange unit is attached, do not
                !do the accessory check
                Return Level:Benign
            Else !If job:Exchange_Unit_Number <> ''
                If AccessoryCheck('JOB')
                    If AccessoryMismatch(job:Ref_Number,'JOB')
                        local:Errorcode = 1
                    End !If AccessoryMismatch(job:Ref_Number,'JOB')
                End !If AccessoryCheck('JOB')
            End !If job:Exchange_Unit_Number <> ''
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,local:Despatch_Type)

            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If local:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    !Only show accessory attached/received by ARC - 4285 (DBH: 11-06-2004)
                    If ~glo:WebJob And ~jac:Attached
                        Cycle
                    End !If ~glo:WebJob And ~jac:Attached

                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            IF (AddToAudit(job:Ref_Number,'JOB',job:Ref_Number,locAuditNotes))
            END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
LocalValidateIMEI    Procedure()
local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            
        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
          '<13,10>'&|
          '<13,10>I.M.E.I. number mismatch.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
        GetStatus(850,1,job:Despatch_Type)

        IF (AddToAudit(job:ref_number,job:Despatch_Type,'FAILED DESPATCH VALIDATION','I.M.E.I. NUMBER MISMATCH'))
        END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccountNumber      Procedure()
    Code

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case Missive('The trade account attached to job number ' & Clip(job:Ref_Number) & ' is only used for "Batch Despatch".','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW4.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:HeadAccountNumber = ''
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:HeadAccountNumber <> ''
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, SetQueueRecord, ())
  tmp:CompanyName = ''
  If muld:BatchType = 'TRA'
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = muld:AccountNumber
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          tmp:CompanyName = tra:Company_Name
      Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          !Error
      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  Else !muld:BatchType = 'TRA'
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = muld:AccountNumber
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          tmp:CompanyName = sub:Company_Name
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch() = Level:Benign
  
  End !muld:BatchType = 'TRA'
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, SetQueueRecord, ())


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
UpdateMulDesp PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
BRW2::View:Browse    VIEW(MULDESPJ)
                       PROJECT(mulj:RecordNumber)
                       PROJECT(mulj:RefNumber)
                       PROJECT(mulj:JobNumber)
                       PROJECT(mulj:IMEINumber)
                       PROJECT(mulj:MSN)
                       PROJECT(mulj:AccountNumber)
                       PROJECT(mulj:Courier)
                       PROJECT(mulj:Current)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
mulj:RecordNumber      LIKE(mulj:RecordNumber)        !List box control field - type derived from field
mulj:RefNumber         LIKE(mulj:RefNumber)           !List box control field - type derived from field
mulj:JobNumber         LIKE(mulj:JobNumber)           !List box control field - type derived from field
mulj:IMEINumber        LIKE(mulj:IMEINumber)          !List box control field - type derived from field
mulj:MSN               LIKE(mulj:MSN)                 !List box control field - type derived from field
mulj:AccountNumber     LIKE(mulj:AccountNumber)       !List box control field - type derived from field
mulj:Courier           LIKE(mulj:Courier)             !List box control field - type derived from field
mulj:Current           LIKE(mulj:Current)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::muld:Record LIKE(muld:RECORD),STATIC
QuickWindow          WINDOW('Update the MULDESP File'),AT(,,200,138),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateMulDesp'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,192,112),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?muld:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(68,20,40,10),USE(muld:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Batch Number'),AT(8,34),USE(?muld:BatchNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(68,34,124,10),USE(muld:BatchNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Batch Number'),TIP('Batch Number'),UPR
                           PROMPT('Account Number'),AT(8,48),USE(?muld:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(68,48,124,10),USE(muld:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),UPR
                         END
                         TAB('MULDESPJ'),USE(?Tab:2)
                           LIST,AT(8,20,184,92),USE(?Browse:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('56L(2)|M~Record Number~L(2)@s8@40L(2)|M~RefNumber~L(2)@s8@44L(2)|M~Job Number~L(' &|
   '2)@s8@80L(2)|M~I.M.E.I. Number~L(2)@s30@80L(2)|M~M.S.N.~L(2)@s30@80L(2)|M~Accoun' &|
   'tNumber~L(2)@s30@80L(2)|M~Courier~L(2)@s30@32R(2)|M~Current~C(0)@n1@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(49,80,45,14),USE(?Insert:3),HIDE
                           BUTTON('&Change'),AT(98,80,45,14),USE(?Change:3),HIDE
                           BUTTON('&Delete'),AT(147,80,45,14),USE(?Delete:3),HIDE
                         END
                       END
                       BUTTON('OK'),AT(102,120,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(151,120,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(151,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW2::Sort0:StepClass StepLongClass                   !Default Step Manager
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMulDesp')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?muld:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(muld:Record,History::muld:Record)
  SELF.AddHistoryField(?muld:RecordNumber,1)
  SELF.AddHistoryField(?muld:BatchNumber,2)
  SELF.AddHistoryField(?muld:AccountNumber,3)
  SELF.AddUpdateFile(Access:MULDESP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MULDESP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MULDESP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:MULDESPJ,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateMulDesp')
  ?Browse:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW2.Q &= Queue:Browse:2
  BRW2.RetainRow = 0
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,mulj:JobNumberKey)
  BRW2.AddRange(mulj:RefNumber,Relate:MULDESPJ,Relate:MULDESP)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,mulj:JobNumber,1,BRW2)
  BRW2.AddField(mulj:RecordNumber,BRW2.Q.mulj:RecordNumber)
  BRW2.AddField(mulj:RefNumber,BRW2.Q.mulj:RefNumber)
  BRW2.AddField(mulj:JobNumber,BRW2.Q.mulj:JobNumber)
  BRW2.AddField(mulj:IMEINumber,BRW2.Q.mulj:IMEINumber)
  BRW2.AddField(mulj:MSN,BRW2.Q.mulj:MSN)
  BRW2.AddField(mulj:AccountNumber,BRW2.Q.mulj:AccountNumber)
  BRW2.AddField(mulj:Courier,BRW2.Q.mulj:Courier)
  BRW2.AddField(mulj:Current,BRW2.Q.mulj:Current)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW2.AskProcedure = 1
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  BRW2.AddToolbarTarget(Toolbar)
  BRW2.ToolbarItem.HelpButton = ?Help
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MULDESP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMulDesp')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMULDESPJ
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateMULDESPJ PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::mulj:Record LIKE(mulj:RECORD),STATIC
QuickWindow          WINDOW('Update the MULDESPJ File'),AT(,,204,152),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateMULDESPJ'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,196,126),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?mulj:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,20,40,10),USE(mulj:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('RefNumber'),AT(8,34),USE(?mulj:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,34,40,10),USE(mulj:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('RefNumber'),TIP('RefNumber'),UPR
                           PROMPT('Job Number'),AT(8,48),USE(?mulj:JobNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,48,40,10),USE(mulj:JobNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,62),USE(?mulj:IMEINumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,62,124,10),USE(mulj:IMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR
                           PROMPT('M.S.N.'),AT(8,76),USE(?mulj:MSN:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,76,124,10),USE(mulj:MSN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                           PROMPT('AccountNumber'),AT(8,90),USE(?mulj:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,90,124,10),USE(mulj:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('AccountNumber'),TIP('AccountNumber'),UPR
                           PROMPT('Courier'),AT(8,104),USE(?mulj:Courier:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,104,124,10),USE(mulj:Courier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Courier'),TIP('Courier'),UPR
                           CHECK('Current'),AT(72,118,70,8),USE(mulj:Current),MSG('Current'),TIP('Current'),VALUE('1','0')
                         END
                       END
                       BUTTON('OK'),AT(106,134,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(155,134,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(155,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a MULDESPJ Record'
  OF ChangeRecord
    ActionMessage = 'Changing a MULDESPJ Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMULDESPJ')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?mulj:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mulj:Record,History::mulj:Record)
  SELF.AddHistoryField(?mulj:RecordNumber,1)
  SELF.AddHistoryField(?mulj:RefNumber,2)
  SELF.AddHistoryField(?mulj:JobNumber,3)
  SELF.AddHistoryField(?mulj:IMEINumber,4)
  SELF.AddHistoryField(?mulj:MSN,5)
  SELF.AddHistoryField(?mulj:AccountNumber,6)
  SELF.AddHistoryField(?mulj:Courier,7)
  SELF.AddHistoryField(?mulj:Current,8)
  SELF.AddUpdateFile(Access:MULDESPJ)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MULDESPJ.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MULDESPJ
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateMULDESPJ')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MULDESPJ.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateMULDESPJ')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

ListJobsOnBatch PROCEDURE (func:RefNumber)            !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:TotalJobs        LONG
BRW1::View:Browse    VIEW(MULDESPJ)
                       PROJECT(mulj:JobNumber)
                       PROJECT(mulj:Courier)
                       PROJECT(mulj:RecordNumber)
                       PROJECT(mulj:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
mulj:JobNumber         LIKE(mulj:JobNumber)           !List box control field - type derived from field
mulj:Courier           LIKE(mulj:Courier)             !List box control field - type derived from field
mulj:RecordNumber      LIKE(mulj:RecordNumber)        !Primary key field - type derived from field
mulj:RefNumber         LIKE(mulj:RefNumber)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK7::mulj:RefNumber       LIKE(mulj:RefNumber)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse Jobs'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Job File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       LIST,AT(240,118,172,206),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('44L(2)|M~Job Number~@s8@120L(2)|M~Courier~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,84,352,244),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Job Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(240,102,64,10),USE(mulj:JobNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                           PROMPT('Total Jobs'),AT(448,224),USE(?tmp:TotalJobs:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(448,236,64,10),USE(tmp:TotalJobs),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Total Jobs'),TIP('Total Jobs'),UPR,READONLY
                           BUTTON,AT(448,252),USE(?Delete),TRN,FLAT,LEFT,ICON('remjobp.jpg')
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020537'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ListJobsOnBatch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MULDESP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MULDESPJ,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','ListJobsOnBatch')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,mulj:JobNumberKey)
  BRW1.AddRange(mulj:RefNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?mulj:JobNumber,mulj:JobNumber,1,BRW1)
  BRW1.AddField(mulj:JobNumber,BRW1.Q.mulj:JobNumber)
  BRW1.AddField(mulj:Courier,BRW1.Q.mulj:Courier)
  BRW1.AddField(mulj:RecordNumber,BRW1.Q.mulj:RecordNumber)
  BRW1.AddField(mulj:RefNumber,BRW1.Q.mulj:RefNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  brw1.InsertControl = 0
  brw1.ChangeControl = 0
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MULDESP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ListJobsOnBatch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMULDESPJ
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020537'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020537'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020537'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mulj:JobNumber
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = func:RefNumber
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW1.ResetFromView PROCEDURE

tmp:TotalJobs:Cnt    LONG
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:MULDESPJ.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:TotalJobs:Cnt += 1
  END
  tmp:TotalJobs = tmp:TotalJobs:Cnt
  PARENT.ResetFromView
  Relate:MULDESPJ.SetQuickScan(0)
  SETCURSOR()


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

SelectMULDESP PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MULDESP)
                       PROJECT(muld:RecordNumber)
                       PROJECT(muld:BatchNumber)
                       PROJECT(muld:AccountNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
muld:RecordNumber      LIKE(muld:RecordNumber)        !List box control field - type derived from field
muld:BatchNumber       LIKE(muld:BatchNumber)         !List box control field - type derived from field
muld:AccountNumber     LIKE(muld:AccountNumber)       !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a MULDESP Record'),AT(,,224,198),FONT('MS Sans Serif',8,,),IMM,HLP('SelectMULDESP'),SYSTEM,GRAY,RESIZE,MDI
                       LIST,AT(8,30,208,124),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('56L(2)|M~Record Number~L(2)@s8@80L(2)|M~Batch Number~L(2)@s30@80L(2)|M~Account N' &|
   'umber~L(2)@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(171,158,45,14),USE(?Select:2)
                       SHEET,AT(4,4,216,172),USE(?CurrentTab)
                         TAB('By Record Number'),USE(?Tab:2)
                         END
                         TAB('By Batch Number'),USE(?Tab:3)
                         END
                         TAB('By Account Number'),USE(?Tab:4)
                         END
                       END
                       BUTTON('Close'),AT(126,180,45,14),USE(?Close)
                       BUTTON('Help'),AT(175,180,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 3
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectMULDESP')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MULDESP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MULDESP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','SelectMULDESP')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,muld:BatchNumberKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,muld:BatchNumber,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,muld:AccountNumberKey)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,muld:AccountNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,muld:RecordNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,muld:RecordNumber,1,BRW1)
  BRW1.AddField(muld:RecordNumber,BRW1.Q.muld:RecordNumber)
  BRW1.AddField(muld:BatchNumber,BRW1.Q.muld:BatchNumber)
  BRW1.AddField(muld:AccountNumber,BRW1.Q.muld:AccountNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MULDESP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','SelectMULDESP')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Main PROCEDURE
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'Main')
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled
CourierMismatch PROCEDURE
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'CourierMismatch')
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled
DespatchProcess PROCEDURE (func:JobNumber,func:SecurityPackNumber) !Generated from procedure template - Window

save_muld_id         USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_jac_id          USHORT,AUTO
save_lac_id          USHORT,AUTO
tmp:Close            BYTE(0)
tmp:BatchAccountNumber STRING(30)
tmp:BatchCourier     STRING(30)
tmp:NumberInBatch    LONG
tmp:BatchAccountName STRING(30)
tmp:ExistingBatchNumber LONG
tmp:ConsignNo        STRING(30)
tmp:LabelError       STRING(30)
sav:path             STRING(255)
Account_Number2_Temp STRING(30)
tmp:AccountNumber    STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:ParcelLineName   STRING(255),STATIC
tmp:WorkStationName  STRING(255)
tmp:BatchNumber      LONG
tmp:PrintSummaryNote BYTE(0)
Despatch_Label_Type_Temp STRING(3)
Courier_Temp         STRING(30)
tmp:DoDespatch       BYTE(0)
tmp:PassedConsignNo  STRING(30)
local:FileName       STRING(255),STATIC
tmp:ReturnedBatchNumber LONG
tmp:ChargeableChargeType STRING(30)
tmp:WarrantyChargeType STRING(30)
tmp:HeadAccountNumber STRING(30)
tmp:CurrentAccount   STRING(60)
tmp:CurrentAccountNumber STRING(30)
locAuditNotes        STRING(255)
window               WINDOW('Despatch Process'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Despatch Process'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,246),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           GROUP('Existing Batch Details'),AT(376,198,136,108),USE(?Group1),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Account Number'),AT(381,207),USE(?Prompt1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(380,215,128,10),USE(tmp:BatchAccountNumber),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Account Name'),AT(380,226),USE(?Prompt2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(380,234,128,10),USE(tmp:BatchAccountName),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Courier'),AT(380,247),USE(?Prompt3),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(380,255,128,10),USE(tmp:BatchCourier),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Units in Batch'),AT(380,271),USE(?Prompt4),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s3),AT(380,282),USE(tmp:NumberInBatch),FONT(,20,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           END
                           PROMPT('StatusText'),AT(212,116,256,16),USE(?StatusText),CENTER,FONT(,14,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s60),AT(212,132,256,12),USE(tmp:CurrentAccount),CENTER,FONT(,8,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           GROUP('Current Job Details'),AT(376,144,136,52),USE(?Group2),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Chargeable Charge Type'),AT(380,152),USE(?ChargeableChargeType:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             STRING(@s30),AT(380,160,128,10),USE(tmp:ChargeableChargeType),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             STRING(@s30),AT(380,181,128,10),USE(tmp:WarrantyChargeType),HIDE,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             PROMPT('Warranty Charge Type'),AT(380,173),USE(?WarrantyChargeType:Prompt),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           END
                           BUTTON,AT(212,172),USE(?AddUnit),TRN,FLAT,LEFT,ICON('addbatp.jpg')
                           BUTTON,AT(212,208),USE(?CreateNewBatch),TRN,FLAT,LEFT,ICON('newbatp.jpg')
                           BUTTON,AT(212,244),USE(?ForceIndividualDespatch),TRN,FLAT,LEFT,ICON('sindesp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    Map
LocalValidateAccessories    Procedure(),Byte
LocalValidateIMEI           Procedure(),Byte
LocalValidateAccountNumber  Procedure(),Byte
    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnedBatchNumber)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020536'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DespatchProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:MULDESP.Open
  Relate:MULDESP_ALIAS.Open
  Relate:STATUS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:MULDESPJ.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBACC.UseFile
  Access:LOAN.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:EXCHHIST.UseFile
  Access:LOANHIST.UseFile
  Access:LOCINTER.UseFile
  Access:USERS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','DespatchProcess')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
    !Are we are the ARC or RRC? - L886 (DBH: 25-07-2003)
    If glo:WebJob
        tmp:CurrentAccountNumber    = Clarionet:Global.Param2
    Else !If glo:WebJob
        tmp:CurrentAccountNumber    = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
    End !If glo:WebJob

    !First thing. Get the job details
    Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
    job_ali:Ref_Number = func:JobNumber
    If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Found
        !Show Details
        If job_ali:Chargeable_Job = 'YES'
            ?ChargeableChargeType:Prompt{prop:Hide} = 0
            ?tmp:ChargeableChargeType{prop:Hide} = 0
            tmp:ChargeableChargeType = job_ali:Charge_Type
        End !If job_ali:Chargeable_Job = 'YES'
        If job_ali:Warranty_Job = 'YES'
            ?WarrantyChargeType:Prompt{prop:Hide} = 0
            ?tmp:WarrantyChargeType{prop:Hide} = 0
            tmp:WarrantyChargeType = job_ali:Warranty_Charge_Type
        End !If job_ali:Warranty_Job = 'YES'
    
        tmp:HeadAccountNumber   = job_ali:Account_Number

        Access:Jobse.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = func:JobNumber
        IF Access:Jobse.Fetch(jobe:RefNumberKey)
          !Error
        END


        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job_ali:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            tmp:CurrentAccount  = Clip(job_ali:Account_Number) & ' - ' & Clip(sub:Company_Name)
            If sub:Generic_Account
! Change --- Allow for ARC Generic Accounts (DBH: 25/03/2009) #10473
!If glo:WebJob
! To --- (DBH: 25/03/2009) #10473
                if (glo:WebJob = 1 or (glo:WebJob = 0 And jobe:webJob = 0))
! end --- (DBH: 25/03/2009) #10473
                    tmp:HeadAccountNumber = job_ali:Account_Number
                Else !If glo:WebJob
                    !Get the original account number of the job from WOBS
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job_ali:Ref_Number
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Found
                        tmp:HeadAccountNumber   = wob:HeadAccountNumber
                    Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                End !If glo:WebJob
            Else
                !Added by Paul (Bryan) on 09/09/09 log no 10937
                If glo:webjob <> 1 And jobe:webJob = 1
                    Access:WEBJOB.ClearKey(wob:RefNumberKey)
                    wob:RefNumber = job_ali:Ref_Number
                    If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Found
                        tmp:HeadAccountNumber   = wob:HeadAccountNumber
                    Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access
                End !If glo:webjob <> 1 And jobe:webJob = 1
            End !If sub:GenericAccount
        Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
        End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign


    
        !Does this job's details match an existing batch?
        Setcursor(Cursor:Wait)
        Virtual# = 0
        IF ~glo:Webjob
            Virtual# = 0
            tmp:ExistingBatchNumber = 0
            !Make sure the batch is for the current location - L886 (DBH: 25-07-2003)
            Save_muld_ID = Access:MULDESP.SaveFile()
            Access:MULDESP.ClearKey(muld:HeadAccountKey)
            muld:HeadAccountNumber = tmp:CurrentAccountNumber
            muld:AccountNumber     = tmp:HeadAccountNumber
            If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1 and ~glo:WebJob
                If ~sub:Generic_Account
                    If VirtualAccount(job_ali:Account_Number)
                        muld:Accountnumber = tra:Account_Number
                        Virtual# = 1
                    End !If VirtualAccount(job_ali:Account_Number)
                End !If ~sub:Generic_Account
            End !If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ELSE
            !RRC!
            IF jobe:Sub_Sub_Account = ''
                tmp:ExistingBatchNumber = 0
                !Make sure the batch is for the current location - L886 (DBH: 25-07-2003)
                Save_muld_ID = Access:MULDESP.SaveFile()
                Access:MULDESP.ClearKey(muld:HeadAccountKey)
                muld:HeadAccountNumber = tmp:CurrentAccountNumber
                muld:AccountNumber     = tmp:HeadAccountNumber

            ELSE
                tmp:HeadAccountNumber = jobe:Sub_Sub_Account
                tmp:ExistingBatchNumber = 0
                !Make sure the batch is for the current location - L886 (DBH: 25-07-2003)
                Save_muld_ID = Access:MULDESP.SaveFile()
                Access:MULDESP.ClearKey(muld:HeadAccountKey)
                muld:HeadAccountNumber = tmp:CurrentAccountNumber
                muld:AccountNumber     = tmp:HeadAccountNumber

            END
        END
        Set(muld:HeadAccountKey,muld:HeadAccountKey)
        Loop
            If Access:MULDESP.NEXT()
               Break
            End !If
            ! - L886 (DBH: 25-07-2003)
            If muld:HeadAccountNumber <> tmp:CurrentAccountNumber
                Break
            End !If muld:HeadAccountNumber <> tmp:CurrentAccountNumber
            If Virtual#
                If muld:AccountNumber <> tra:Account_Number      |
                    Then Break.  ! End If
                If muld:Courier = tra:Courier_Outgoing
                    tmp:ExistingBatchNumber = muld:RecordNumber
                End !If muld:Courier = tra:Courier_Outgoing
            Else !If Virtual#
                If muld:AccountNumber <> tmp:HeadAccountNumber      |
                    Then Break.  ! End If
    
                !Is it the job/exchange or loan being despatch?
                IF ~glo:webjob
                    Case job_ali:Despatch_Type
                        Of 'JOB'
                            If job_ali:Courier = muld:Courier
                                tmp:ExistingBatchNumber = muld:RecordNumber
                                Break
                            End !If job_ali:Courier = muld:Courier
                        Of 'EXC'
                            If job_ali:Exchange_Courier = muld:Courier
                                tmp:ExistingBatchNumber = muld:RecordNumber
                                Break
                            End !If job_Ali:Exchange_Courier = muld:Courier
                        Of 'LOA'
                            If job_ali:Loan_Courier = muld:Courier
                                tmp:ExistingBatchNumber = muld:RecordNumber
                                Break
                            End !If job_ali:Loan_Courier = muld:Courier
                    End !Case job_ali:Despatch_Type
                ELSE
                    Case jobe:DespatchType
                        Of 'JOB'
                            If job_ali:Courier = muld:Courier
                                tmp:ExistingBatchNumber = muld:RecordNumber
                                Break
                            End !If job_ali:Courier = muld:Courier
                        Of 'EXC'
                            If job_ali:Exchange_Courier = muld:Courier
                                tmp:ExistingBatchNumber = muld:RecordNumber
                                Break
                            End !If job_Ali:Exchange_Courier = muld:Courier
                        Of 'LOA'
                            If job_ali:Loan_Courier = muld:Courier
                                tmp:ExistingBatchNumber = muld:RecordNumber
                                Break
                            End !If job_ali:Loan_Courier = muld:Courier
                    End !Case job_ali:Despatch_Type
                End
            End !If Virtual#
        End !Loop
        Access:MULDESP.RestoreFile(Save_muld_ID)
        Setcursor()
    
        If tmp:ExistingBatchNumber = 0
            !No Existing Batch Found
            ?StatusText{prop:Text} = 'No Batch Found'
            ?StatusText{prop:FontColor} = color:Red
            ?AddUnit{prop:Disable} = 1
        Else !If tmp:ExistingBatchNumber = 0
            !Found a Batch, fill in it's details.
            Access:MULDESP.ClearKey(muld:RecordNumberKey)
            muld:RecordNumber = tmp:ExistingBatchNumber
            If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                !Found
                tmp:BatchAccountNumber  = muld:AccountNumber
                tmp:BatchCourier        = muld:Courier
                !Count the Jobs in the Batch
                tmp:NumberInBatch       = 0
                Save_mulj_ID = Access:MULDESPJ.SaveFile()
                Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                mulj:RefNumber = muld:RecordNumber
                Set(mulj:JobNumberKey,mulj:JobNumberKey)
                Loop
                    If Access:MULDESPJ.NEXT()
                       Break
                    End !If
                    If mulj:RefNumber <> muld:RecordNumber      |
                        Then Break.  ! End If
                    tmp:NumberInBatch   += 1
                End !Loop
                Access:MULDESPJ.RestoreFile(Save_mulj_ID)
    
                !Get the Account Name
                If muld:BatchType = 'TRA'
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = muld:AccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        tmp:BatchAccountName    = tra:Company_Name
                    Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                Else !If muld:BatchType = 'TRA'
                    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                    sub:Account_Number  = muld:AccountNumber
                    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                        !Found
                        tmp:BatchAccountName    = sub:Company_Name
                    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                End !If muld:BatchType = 'TRA'
    
                ?StatusText{prop:Text} = 'Existing Batch Found'
                ?StatusText{prop:FontColor} = color:Green
                ?tmp:NumberInBatch{prop:FontColor} = color:Green
            Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
        End !If tmp:ExistingBatchNumber = 0
    
    Else!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:MULDESP.Close
    Relate:MULDESP_ALIAS.Close
    Relate:STATUS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','DespatchProcess')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020536'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020536'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020536'&'0')
      ***
    OF ?AddUnit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddUnit, Accepted)
      !Add job to existing Batch
      !Reget Job
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = func:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Access:MULDESP.ClearKey(muld:RecordNumberKey)
          muld:RecordNumber = tmp:ExistingBatchNumber
          If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Found
      
              If Access:MULDESPJ.PrimeRecord() = Level:Benign
                  mulj:RefNumber     = tmp:ExistingBatchNumber
                  mulj:JobNumber     = job:Ref_Number
                  mulj:IMEINumber    = job:ESN
                  mulj:MSN           = job:MSN
                  mulj:AccountNumber = job:Account_Number
                  mulj:SecurityPackNumber = func:SecurityPackNumber
      
                  If glo:WebJob
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
                          Case jobe:DespatchType
                              Of 'JOB'
                                  mulj:Courier    = job:Courier
                              Of 'EXC'
                                  mulj:Courier    = job:Exchange_Courier
                              Of 'LOA'
                                  mulj:Courier    = job:Loan_Courier
                          End !Case job:Despatch_Type
      
                      Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                      End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                  Else !If glo:WebJob
                      Case job:Despatch_Type
                          Of 'JOB'
                              mulj:Courier    = job:Courier
                          Of 'EXC'
                              mulj:Courier    = job:Exchange_Courier
                          Of 'LOA'
                              mulj:Courier    = job:Loan_Courier
                      End !Case job:Despatch_Type
      
                  End !If glo:WebJob
                  If muld:BatchType = 'TRA'
                      mulj:AccountNumber = job:Account_Number
                      mulj:Courier        = muld:Courier
                  End !If muld:BatchType = 'TRA'
      
                  If Access:MULDESPJ.TryInsert() = Level:Benign
                      !Insert Successful
      
                      !Count how many jobs there are on the batch.
                      !I could just add one to the record, but I can see
                      !that being too inaccurate. So I'll physically count
                      !all the records in the batch instead.
      
                      CountBatch# = 0
                      Save_mulj_ID = Access:MULDESPJ.SaveFile()
                      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                      mulj:RefNumber = tmp:ExistingBatchNumber
                      Set(mulj:JobNumberKey,mulj:JobNumberKey)
                      Loop
                          If Access:MULDESPJ.NEXT()
                             Break
                          End !If
                          If mulj:RefNumber <> tmp:ExistingBatchNumber      |
                              Then Break.  ! End If
                          CountBatch# += 1
                      End !Loop
                      Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
                      muld:BatchTotal = CountBatch#
                      tmp:ReturnedBatchNumber = muld:BatchNumber
                      Access:MULDESP.Update()
      
                  Else !If Access:MULDESPJ.TryInsert() = Level:Benign
                      !Insert Failed
                  End !If Access:MULDESPJ.TryInsert() = Level:Benign
              End !If Access:MULDESPJ.PrimeRecord() = Level:Benign
          Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
      
          tmp:Close = 1
          Post(Event:CloseWindow)
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddUnit, Accepted)
    OF ?CreateNewBatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateNewBatch, Accepted)
      If tmp:ExistingBatchNumber <> 0
          !Create a new batch when one already exists:
          !Finish and despatch the current batch, i.e. the one that was found
          !and start a new batch for this job.
      
          Access:MULDESP.ClearKey(muld:RecordNumberKey)
          muld:RecordNumber = tmp:ExistingBatchNumber
          If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Found
      
              !The following code was copied/pasted from the other Multiple Despatch
              !screen.
              tmp:BatchNumber = muld:RecordNumber
      
              tmp:PrintSummaryNote = 0
      
              !Is this despatch for a Virtual Site?
              Case muld:BatchType
                  Of 'TRA'
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = muld:AccountNumber
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          If tra:Print_Despatch_Despatch = 'YES'
                              If tra:Summary_Despatch_Notes = 'YES'
                                  tmp:PrintSummaryNote = 1
                              End !If tra:Summary_Despatch_Notes = 'YES'
                          End !If tra:Print_Despatch_Despatch = 'YES'
                          Despatch_Label_Type_Temp = 'TRA'
      
                      Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
                  Else
                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                      sub:Account_Number  = muld:AccountNumber
                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Found
                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                          tra:Account_Number  = sub:Main_Account_Number
                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Found
                              If tra:Use_Sub_Accounts = 'YES'
                                  If sub:Print_Despatch_Despatch  = 'YES'
                                      If sub:Summary_Despatch_Notes = 'YES'
                                          tmp:PrintSummaryNote = 1
                                      End !If sub:Summary_Despatch_Notes = 'YES'
                                  End !If sub:Print_Despatch_Despatch  = 'YES'
                                  Despatch_Label_Type_temp    = 'SUB'
                              Else !If tra:Use_Sub_Accounts = 'YES'
                                  If tra:Print_Despatch_Despatch = 'YES'
                                      If tra:Summary_Despatch_Notes = 'YES'
                                          tmp:PrintSummaryNote = 1
                                      End !If tra:Summary_Despatch_Notes = 'YES'
                                  End !If tra:Print_Despatch_Despatch = 'YES'
                                  Despatch_Label_Type_Temp = 'TRA'
      
                              End !If tra:Use_Sub_Accounts = 'YES'
                          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          
                      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
              End !Case muld:BatchType.
      
              !Fill in old variables
              Courier_Temp = muld:Courier
              Account_Number2_Temp = muld:AccountNumber
              If access:desbatch.primerecord() = Level:Benign
                  If access:desbatch.tryinsert()
                      access:desbatch.cancelautoinc()
                  Else!If access:desbatch.tryinsert()
                      !Change to returning routine - 3939 (DBH: 26-03-2004)
                      If DespatchMultiple(muld:Courier,muld:AccountNumber,tmp:BatchNumber,Despatch_Label_Type_Temp,muld:BatchType) = False
      
                      End !If DespatchMultiple(muld:Courier,muld:AccountNumber,tmp:BatchNumber,Despatch_Label_Type_Temp,muld:BatchType) = False
                  End!
              End!If access:desbatch.tryinsert()
          Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
      
      End !tmp:ExistingBatchNumber = 0
      
      !Now that's all finished create a new batch,
      !for the original job.
      
      !Reget the live job.
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = func:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Access:JobSe.ClearKey(jobe:RefNumberKey)
          jobe:RefNumber = job:ref_number
          IF Access:JobSe.Fetch(jobe:RefNumberKey)
            !Error
          END
          If Access:MULDESP.PrimeRecord() = Level:Benign
              muld:BatchType  = 'SUB'
              IF jobe:Sub_Sub_Account = ''
                tmp:HeadAccountNumber   = job:Account_Number
              ELSE
                tmp:HeadAccountNumber   = jobe:Sub_Sub_Account
              END
      
              !Only worry about generic accounts for non web jobs
      
              Access:JOBSE.Clearkey(jobe:RefNumberKey)
              jobe:RefNumber    = job:Ref_Number
              if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                  ! Found
              else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                  ! Error
              end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
      
              ! Delete --- Check if the job was booked at the RRC (DBH: 25/03/2009) #10473
              !IF ~glo:WebJob
              ! end --- (DBH: 25/03/2009) #10473
              if (~glo:WebJob and jobe:WebJob)
                  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                  sub:Account_Number  = job:Account_Number
                  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      If sub:Generic_Account And ~glo:WebJob
                          Access:WEBJOB.ClearKey(wob:RefNumberKey)
                          wob:RefNumber = job:Ref_Number
                          If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                              !Found
                              tmp:HeadAccountNumber   = wob:HeadAccountNumber
                              muld:BatchType          = 'TRA'
      
                              !Job was original for a generic account, so batch
                              !it up to go back to the RRC
      
                          Else!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                      End !If sub:Generic_Account
                  Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              End
      !        MESSAGE(tmp:HeadAccountNumber)
              muld:AccountNumber = tmp:HeadAccountNumber
      
              If glo:WebJob
      ! Delete --- Move higher up (DBH: 25/03/2009) #10473
      !            Access:JOBSE.Clearkey(jobe:RefNumberKey)
      !            jobe:RefNumber  = job:Ref_Number
      !            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !
      ! end --- (DBH: 25/03/2009) #10473                !Found
                      Case jobe:DespatchType
                          Of 'JOB'
                              muld:Courier    = job:Courier
                          Of 'LOA'
                              muld:Courier    = job:Loan_Courier
                          Of 'EXC'
                              muld:Courier    = job:Exchange_Courier
                      End !Case job:Despatch_Type
      !            Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !                !Error
      !            End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
              Else !If glo:WebJob
                  Case job:Despatch_Type
                      Of 'JOB'
                          muld:Courier    = job:Courier
                      Of 'LOA'
                          muld:Courier    = job:Loan_Courier
                      Of 'EXC'
                          muld:Courier    = job:Exchange_Courier
                  End !Case job:Despatch_Type
      
              End !If glo:WebJob
      
              If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1 and ~glo:webjob
                  !Is this for a Virtual Site?
                  If ~sub:Generic_Account
                      If VirtualAccount(job:Account_Number)
                          muld:BatchType = 'TRA'
                          muld:AccountNumber = tra:Account_Number
                          muld:Courier        = tra:Courier_Outgoing
                      End !If VirualAccount(job:Account_Number)
                  End !If ~sub:Generic_Account
              End !If GETINI('DESPATCH','GroupVirtualBatches',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
              muld:BatchTotal         = 1
      
              !Allocate a Batch Number
              !Count between 1 to 1000, (that should be enough)
              !and if I can't find a Batch with that batch number, then
              !assign that batch number to this batch.
              BatchNumber# = 0
              Loop BatchNumber# = 1 To 1000
                  Access:MULDESP_ALIAS.ClearKey(muld_ali:BatchNumberKey)
                  muld_ali:BatchNumber = BatchNumber#
                  If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Found
                  Else!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      muld:BatchNumber    = BatchNumber#
                      tmp:ReturnedBatchNumber = muld:BatchNumber
                      Break
      
                  End!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
              End !BatchNumber# = 1 To 1000
              If glo:WebJob
                  muld:HeadAccountNumber      = Clarionet:Global.Param2
              Else !If glo:WebJob
                  muld:HeadAccountNumber    = GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI')
              End !If glo:WebJob
              
              If Access:MULDESP.TryInsert() = Level:Benign
                  !Insert Successful
                  If Access:MULDESPJ.PrimeRecord() = Level:Benign
                      mulj:RefNumber     = muld:RecordNumber
                      mulj:JobNumber     = job:Ref_Number
                      mulj:IMEINumber    = job:ESN
                      mulj:MSN           = job:MSN
                      mulj:AccountNumber = muld:AccountNumber
                      mulj:Courier       = muld:Courier
                      mulj:Current       = 1
                      mulj:SecurityPackNumber = func:SecurityPackNumber
                      If Access:MULDESPJ.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:MULDESPJ.TryInsert() = Level:Benign
                          !Insert Failed
                      End !If Access:MULDESPJ.TryInsert() = Level:Benign
                  End !If Access:MULDESPJ.PrimeRecord() = Level:Benign
              Else !If Access:MULDESP.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:MULDESP.TryInsert() = Level:Benign
          End !If Access:MULDESP.PrimeRecord() = Level:Benign
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      tmp:Close = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateNewBatch, Accepted)
    OF ?ForceIndividualDespatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ForceIndividualDespatch, Accepted)
      !Do not add the job to a batch, just
      !despatch it individually.
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      !Reget Job
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = func:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Error# = 0
          If def:Force_Accessory_Check = 'YES'
              If LocalValidateAccessories()
                  Access:JOBS.Update()
                  !Failed Check
                  Error# = 1
              End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
          End !If def:Force_Accessory_Check = 'YES'
      
          If def:ValidateDesp = 'YES'
              If LocalValidateIMEI()
                  !Failed Check
                  Error# = 1
              End !If LocalValidateIMEI()
          End !If def:ValidateDesp = 'YES'
      
          If Error# = 0
              !Get the Courier. Required by 'despsing.inc'
              Access:COURIER.ClearKey(cou:Courier_Key)
              Case job:Despatch_Type
                  Of 'JOB'
                      cou:Courier = job:Courier
                  Of 'EXC'
                      cou:Courier = job:Exchange_Courier
                  Of 'LOA'
                      cou:Courier = job:Loan_Courier
              End !Case job:Despatch_Type
              If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                  !Found
                  If access:desbatch.primerecord() = Level:Benign
                      If access:desbatch.tryinsert()
                          access:desbatch.cancelautoinc()
                      Else!If access:desbatch.tryinsert()
                          DespatchSingle()
                      End!If access:desbatch.tryinsert()
                  End!If access:desbatch.primerecord() = Level:Benign
      
              Else!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
      
          End !If Error# = 0
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      tmp:Close = 1
      tmp:ReturnedBatchNumber = 0
      Post(Event:CloseWindow)
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ForceIndividualDespatch, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Close = 0
      tmp:ReturnedBatchNumber = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If tmp:Close = 0
          Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
              Of 1 ! No Button
                  Cycle
          End ! Case Missive
      End !tmp:Close = 0
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F6Key
              !This button can be disabled. Only alert the key if enabled.
              If ?AddUnit{prop:Disable} = 0
                  Post(Event:Accepted,?AddUnit)
              End !If ?AddUnit{prop:Disable} = 0
          Of F7Key
              Post(Event:Accepted,?CreateNewBatch)
          Of F8Key
              Post(Event:Accepted,?ForceIndividualDespatch)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            If AccessoryCheck('JOB')
                If AccessoryMismatch(job:Ref_Number,'JOB')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'JOB')
            End !If AccessoryCheck('JOB')
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)

            locAuditNotes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            locAuditNotes         = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            IF (AddToAudit(job:Ref_Number,job:Despatch_Type,'FAILED DESPATCH VALIDATION',locAuditNotes))
            END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
LocalValidateIMEI    Procedure()
local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            
        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
          '<13,10>'&|
          '<13,10>I.M.E.I. Number mismatch.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
        GetStatus(850,1,job:Despatch_Type)

        IF (AddToAudit(job:ref_number,job:Despatch_Type,'FAILED DESPATCH VALIDATION','I.M.E.I. NUMBER MISMATCH'))
        END ! IF

        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccountNumber      Procedure()
    Code

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case Missive('The trade account attached to job number ' & Clip(job:Ref_Number) & ' is only used for "Batch Despatch".','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case Missive('Cannot despatch job number ' & Clip(job:Ref_Number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The trade account is on stop and it is not a warranty job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
SDSLabelInd PROCEDURE
  CODE
  GlobalErrors.ThrowMessage(Msg:ProcedureToDo,'SDSLabelInd')
  SETKEYCODE(0)
  GlobalResponse = RequestCancelled
AddressSearch PROCEDURE (func:Surname)                !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ADDSEARCH)
                       PROJECT(addtmp:AddressLine1)
                       PROJECT(addtmp:AddressLine2)
                       PROJECT(addtmp:AddressLine3)
                       PROJECT(addtmp:Postcode)
                       PROJECT(addtmp:JobNumber)
                       PROJECT(addtmp:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
addtmp:AddressLine1    LIKE(addtmp:AddressLine1)      !List box control field - type derived from field
addtmp:AddressLine2    LIKE(addtmp:AddressLine2)      !List box control field - type derived from field
addtmp:AddressLine3    LIKE(addtmp:AddressLine3)      !List box control field - type derived from field
addtmp:Postcode        LIKE(addtmp:Postcode)          !List box control field - type derived from field
addtmp:JobNumber       LIKE(addtmp:JobNumber)         !List box control field - type derived from field
addtmp:RecordNumber    LIKE(addtmp:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Address Search'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Address Search'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,54,552,20),USE(?Panel1),FILL(09A6A7CH)
                       PROMPT('Surname:'),AT(68,58),USE(?Prompt1),FONT(,12,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                       STRING(@s30),AT(128,58),USE(func:Surname),FONT(,12,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       LIST,AT(104,110,432,248),USE(?Browse:1),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),ALRT(EnterKey),ALRT(MouseLeft2),FORMAT('124L(2)|M~Address Line 1~@s30@80L(2)|M~Address Line 2~@s30@80L(2)|M~Address Line' &|
   ' 3~@s30@80L(2)|M~Postcode~@s18@32L(2)|M~Job Number~@s8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(548,188),USE(?Edit),TRN,FLAT,LEFT,ICON('editp.jpg')
                       SHEET,AT(64,78,552,284),USE(?CurrentTab),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6E7EFH),SPREAD
                         TAB('By Address'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(104,94,124,10),USE(addtmp:AddressLine1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Address Line 1'),TIP('Address Line 1'),UPR
                         END
                         TAB('By Postcode'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(104,94,124,10),USE(addtmp:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Postcode'),TIP('Postcode'),UPR
                         END
                       END
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020525'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AddressSearch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ADDSEARCH.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ADDSEARCH,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','AddressSearch')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,addtmp:PostcodeKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?addtmp:Postcode,addtmp:Postcode,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,addtmp:AddressLine1Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?addtmp:AddressLine1,addtmp:AddressLine1,1,BRW1)
  BRW1.AddField(addtmp:AddressLine1,BRW1.Q.addtmp:AddressLine1)
  BRW1.AddField(addtmp:AddressLine2,BRW1.Q.addtmp:AddressLine2)
  BRW1.AddField(addtmp:AddressLine3,BRW1.Q.addtmp:AddressLine3)
  BRW1.AddField(addtmp:Postcode,BRW1.Q.addtmp:Postcode)
  BRW1.AddField(addtmp:JobNumber,BRW1.Q.addtmp:JobNumber)
  BRW1.AddField(addtmp:RecordNumber,BRW1.Q.addtmp:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Address'
    ?Tab2{PROP:TEXT} = 'By Postcode'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Address Line 1~@s30@#1#80L(2)|M~Address Line 2~@s30@#2#80L(2)|M~Address Line 3~@s30@#3#80L(2)|M~Postcode~@s18@#4#32L(2)|M~Job Number~@s8@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ADDSEARCH.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','AddressSearch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020525'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020525'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020525'&'0')
      ***
    OF ?Edit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Edit, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = brw1.q.addtmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          UpdateJOBS
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Edit, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      Case KeyCode()
          Of MouseLeft2 Orof EnterKey
              Post(Event:Accepted,?Edit)
      End !KeyCode()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Address Line 1~@s30@#1#80L(2)|M~Address Line 2~@s30@#2#80L(2)|M~Address Line 3~@s30@#3#80L(2)|M~Postcode~@s18@#4#32L(2)|M~Job Number~@s8@#5#'
          ?Tab:2{PROP:TEXT} = 'By Address'
        OF 2
          ?Browse:1{PROP:FORMAT} ='80L(2)|M~Postcode~@s18@#4#124L(2)|M~Address Line 1~@s30@#1#80L(2)|M~Address Line 2~@s30@#2#80L(2)|M~Address Line 3~@s30@#3#32L(2)|M~Job Number~@s8@#5#'
          ?Tab2{PROP:TEXT} = 'By Postcode'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?addtmp:AddressLine1
      Select(?Browse:1)
    OF ?addtmp:Postcode
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

