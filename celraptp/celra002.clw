

   MEMBER('celraptp.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA003.INC'),ONCE        !Req'd for module callout resolution
                     END


Browse_Batch PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
save_jot_id          USHORT,AUTO
save_trb_id          USHORT,AUTO
tmp:JobNumber        LONG
tmp:BatchNumber      LONG
tmp:CompanyName      STRING(30)
tmp:ThirdPartyNumber LONG
queue:filter         QUEUE,PRE()
                     END
Company_Name_temp    STRING(30)
status_temp          STRING('OUT')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Company_Name_temp
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(TRDBATCH)
                       PROJECT(trb:Batch_Number)
                       PROJECT(trb:Date)
                       PROJECT(trb:Company_Name)
                       PROJECT(trb:Ref_Number)
                       PROJECT(trb:ESN)
                       PROJECT(trb:MSN)
                       PROJECT(trb:AuthorisationNo)
                       PROJECT(trb:RecordNumber)
                       PROJECT(trb:Status)
                       JOIN(jot:ThirdPartyKey,trb:RecordNumber)
                         PROJECT(jot:DateDespatched)
                         PROJECT(jot:DateIn)
                       END
                       JOIN(job:Ref_Number_Key,trb:Ref_Number)
                         PROJECT(job:Account_Number)
                         PROJECT(job:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
trb:Batch_Number       LIKE(trb:Batch_Number)         !List box control field - type derived from field
trb:Batch_Number_NormalFG LONG                        !Normal forground color
trb:Batch_Number_NormalBG LONG                        !Normal background color
trb:Batch_Number_SelectedFG LONG                      !Selected forground color
trb:Batch_Number_SelectedBG LONG                      !Selected background color
trb:Date               LIKE(trb:Date)                 !List box control field - type derived from field
trb:Date_NormalFG      LONG                           !Normal forground color
trb:Date_NormalBG      LONG                           !Normal background color
trb:Date_SelectedFG    LONG                           !Selected forground color
trb:Date_SelectedBG    LONG                           !Selected background color
trb:Company_Name       LIKE(trb:Company_Name)         !List box control field - type derived from field
trb:Company_Name_NormalFG LONG                        !Normal forground color
trb:Company_Name_NormalBG LONG                        !Normal background color
trb:Company_Name_SelectedFG LONG                      !Selected forground color
trb:Company_Name_SelectedBG LONG                      !Selected background color
trb:Ref_Number         LIKE(trb:Ref_Number)           !List box control field - type derived from field
trb:Ref_Number_NormalFG LONG                          !Normal forground color
trb:Ref_Number_NormalBG LONG                          !Normal background color
trb:Ref_Number_SelectedFG LONG                        !Selected forground color
trb:Ref_Number_SelectedBG LONG                        !Selected background color
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Account_Number_NormalFG LONG                      !Normal forground color
job:Account_Number_NormalBG LONG                      !Normal background color
job:Account_Number_SelectedFG LONG                    !Selected forground color
job:Account_Number_SelectedBG LONG                    !Selected background color
jot:DateDespatched     LIKE(jot:DateDespatched)       !List box control field - type derived from field
jot:DateDespatched_NormalFG LONG                      !Normal forground color
jot:DateDespatched_NormalBG LONG                      !Normal background color
jot:DateDespatched_SelectedFG LONG                    !Selected forground color
jot:DateDespatched_SelectedBG LONG                    !Selected background color
trb:ESN                LIKE(trb:ESN)                  !List box control field - type derived from field
trb:ESN_NormalFG       LONG                           !Normal forground color
trb:ESN_NormalBG       LONG                           !Normal background color
trb:ESN_SelectedFG     LONG                           !Selected forground color
trb:ESN_SelectedBG     LONG                           !Selected background color
trb:MSN                LIKE(trb:MSN)                  !List box control field - type derived from field
trb:MSN_NormalFG       LONG                           !Normal forground color
trb:MSN_NormalBG       LONG                           !Normal background color
trb:MSN_SelectedFG     LONG                           !Selected forground color
trb:MSN_SelectedBG     LONG                           !Selected background color
jot:DateIn             LIKE(jot:DateIn)               !List box control field - type derived from field
jot:DateIn_NormalFG    LONG                           !Normal forground color
jot:DateIn_NormalBG    LONG                           !Normal background color
jot:DateIn_SelectedFG  LONG                           !Selected forground color
jot:DateIn_SelectedBG  LONG                           !Selected background color
trb:AuthorisationNo    LIKE(trb:AuthorisationNo)      !List box control field - type derived from field
trb:AuthorisationNo_NormalFG LONG                     !Normal forground color
trb:AuthorisationNo_NormalBG LONG                     !Normal background color
trb:AuthorisationNo_SelectedFG LONG                   !Selected forground color
trb:AuthorisationNo_SelectedBG LONG                   !Selected background color
trb:RecordNumber       LIKE(trb:RecordNumber)         !Primary key field - type derived from field
trb:Status             LIKE(trb:Status)               !Browse key field - type derived from field
job:Ref_Number         LIKE(job:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK19::trb:Company_Name    LIKE(trb:Company_Name)
HK19::trb:Status          LIKE(trb:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
QuickWindow          WINDOW('Consignment Batch'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PROMPT('Browse The Third Party Batch File'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(8,384),USE(?Button:NMSPreAlert),TRN,FLAT,ICON('nmsprep.jpg')
                       PROMPT('- Exchanged'),AT(20,366),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       OPTION('Status'),AT(480,42,112,24),USE(status_temp),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         RADIO('All'),AT(564,50),USE(?status_temp:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ALL')
                         RADIO('Out'),AT(488,50),USE(?status_temp:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('OUT')
                         RADIO('In'),AT(528,50),USE(?status_temp:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IN')
                       END
                       LIST,AT(8,72,584,272),USE(?Browse:1),IMM,HVSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('37R(2)|M*~Batch No~L@s8@40R(2)|M*~Date~C(0)@d6@93L(2)|M*~Company Name~@s30@37R(2' &|
   ')|M*~Job No~@s8@65L(2)|M*~Account Number~@s15@44R(2)|M*~Despatched~@d6@69L(2)|M*' &|
   '~I.M.E.I. No~@s16@69L(2)|M*~M.S.N.~@s16@40R(2)|M*~Recvd~@d6@120L(2)|M*~Auth No~@' &|
   's30@'),FROM(Queue:Browse:1)
                       BUTTON,AT(604,156),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                       BUTTON,AT(604,96),USE(?Insert),TRN,FLAT,LEFT,ICON('newbatp.jpg'),DEFAULT
                       BUTTON,AT(604,126),USE(?Insert:2),TRN,FLAT,LEFT,ICON('addbat2p.jpg'),DEFAULT
                       BUTTON,AT(604,186),USE(?Allocate),TRN,FLAT,LEFT,ICON('allauthp.jpg')
                       BUTTON,AT(604,218),USE(?Print_Routines),TRN,FLAT,LEFT,ICON('prnroutp.jpg')
                       BUTTON,AT(608,282),USE(?Rapid_Job_Update),TRN,FLAT,LEFT,ICON('rapjobp.jpg')
                       BUTTON,AT(608,316),USE(?returns),TRN,FLAT,LEFT,ICON('retunitp.jpg')
                       BUTTON,AT(608,348),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                       BUTTON,AT(528,348),USE(?CountJobs),TRN,FLAT,LEFT,ICON('coujobp.jpg')
                       SHEET,AT(4,28,672,354),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Batch Number'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(8,58,64,10),USE(trb:Batch_Number),LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By 3rd Party Agent'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(8,47,124,10),USE(Company_Name_temp),IMM,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           ENTRY(@s8),AT(8,60,64,10),USE(trb:Batch_Number,,?TRB:Batch_Number:2),LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By I.M.E.I. Number'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(8,58,124,10),USE(trb:ESN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                         TAB('By Authorisation Number'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(8,58,124,10),USE(trb:AuthorisationNo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Authorisation Number for the Despatched Batch'),TIP('Authorisation Number for the Despatched Batch'),UPR
                         END
                         TAB('By Job Number'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(8,58,64,10),USE(trb:Ref_Number),LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                         END
                       END
                       BUTTON,AT(608,384),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(436,348),USE(?GenerateReport),TRN,FLAT,ICON('genrepp.jpg')
                       BOX,AT(8,366,8,8),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Red)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(status_temp) = 'ALL'
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(status_temp) = 'IN'
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(status_temp) = 'OUT'
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(status_temp) = 'ALL'
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(status_temp) = 'IN'
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(status_temp) = 'OUT'
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(status_temp) = 'ALL'
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(status_temp) = 'IN'
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(status_temp) = 'OUT'
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(status_temp) = 'ALL'
BRW1::Sort8:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(status_temp) = 'IN'
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(status_temp) = 'OUT'
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(status_temp) = 'ALL'
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(status_temp) = 'IN'
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(status_temp) = 'OUT'
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020579'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Batch')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Access:TRDPARTY.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRDBATCH,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Browse_Batch')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,trb:BatchOnlyKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Number_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Number_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Number_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?TRB:Batch_Number:2,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Company_Status_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?TRB:Batch_Number:2,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Company_Status_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?TRB:Batch_Number:2,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:ESN_Only_Key)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?TRB:ESN,trb:ESN,1,BRW1)
  BRW1.AddSortOrder(,trb:ESN_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?TRB:ESN,trb:ESN,1,BRW1)
  BRW1.AddSortOrder(,trb:ESN_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?TRB:ESN,trb:ESN,1,BRW1)
  BRW1.AddSortOrder(,trb:AuthorisationKey)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?trb:AuthorisationNo,trb:AuthorisationNo,1,BRW1)
  BRW1.AddSortOrder(,trb:StatusAuthKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?trb:AuthorisationNo,trb:AuthorisationNo,1,BRW1)
  BRW1.AddSortOrder(,trb:StatusAuthKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?trb:AuthorisationNo,trb:AuthorisationNo,1,BRW1)
  BRW1.AddSortOrder(,trb:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?trb:Ref_Number,trb:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:JobStatusKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?trb:Ref_Number,trb:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:JobStatusKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?trb:Ref_Number,trb:Ref_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,trb:Batch_Number_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?trb:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddField(trb:Batch_Number,BRW1.Q.trb:Batch_Number)
  BRW1.AddField(trb:Date,BRW1.Q.trb:Date)
  BRW1.AddField(trb:Company_Name,BRW1.Q.trb:Company_Name)
  BRW1.AddField(trb:Ref_Number,BRW1.Q.trb:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(jot:DateDespatched,BRW1.Q.jot:DateDespatched)
  BRW1.AddField(trb:ESN,BRW1.Q.trb:ESN)
  BRW1.AddField(trb:MSN,BRW1.Q.trb:MSN)
  BRW1.AddField(jot:DateIn,BRW1.Q.jot:DateIn)
  BRW1.AddField(trb:AuthorisationNo,BRW1.Q.trb:AuthorisationNo)
  BRW1.AddField(trb:RecordNumber,BRW1.Q.trb:RecordNumber)
  BRW1.AddField(trb:Status,BRW1.Q.trb:Status)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB6.Init(Company_Name_temp,?Company_Name_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder()
  FDCB6.AddField(trd:Company_Name,FDCB6.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By Batch Number'
    ?Tab2{PROP:TEXT} = 'By 3rd Party Agent'
    ?Tab3{PROP:TEXT} = 'By I.M.E.I. Number'
    ?Tab4{PROP:TEXT} = 'By Authorisation Number'
    ?Tab5{PROP:TEXT} = 'By Job Number'
    ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Batch')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of changerecord
          check_access('3RD PARTY DESPATCH - CHANGE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
      of deleterecord
          check_access('3RD PARTY DESPATCH - DELETE',x")
          if x" = false
              Case Missive('You do not have access to this option.','ServiceBase 3g',|
                             'mstop.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
              do_update# = false
          end
          tmp:jobnumber   = TRB:Ref_Number
          tmp:BatchNumber = TRB:Batch_Number
          tmp:CompanyName = TRB:Company_Name
          tmp:ThirdPartyNumber    = trb:RecordNumber
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTRDBATCH
    ReturnValue = GlobalResponse
  END
      If request  = Deleterecord And globalresponse = 1
  
          IF (AddToAudit(tmp:jobNumber,'JOB','DELETED FROM 3RD PARTY BATCH','BATCH NUMBER: ' & Clip(tmp:BatchNumber) & |
                                  '<13,10>COMPANY NAME: ' & Clip(tmp:CompanyName)))
          END ! IF
  
          !Delete the associated JOBTHIRD entry
          Save_jot_ID = Access:JOBTHIRD.SaveFile()
          Access:JOBTHIRD.ClearKey(jot:ThirdPartyKey)
          jot:ThirdPartyNumber = tmp:ThirdPartyNumber
          Set(jot:ThirdPartyKey,jot:ThirdPartyKey)
          Loop
              If Access:JOBTHIRD.NEXT()
                 Break
              End !If
              If jot:ThirdPartyNumber <> tmp:ThirdPartyNumber      |
                  Then Break.  ! End If
              Delete(JOBTHIRD)
          End !Loop
          Access:JOBTHIRD.RestoreFile(Save_jot_ID)
  
      End!If request  = Deleterecord And globalresponse = 1
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?trb:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
      IF LEN(CLIP(trb:ESN)) = 18
        !Ericsson IMEI!
        trb:ESN = SUB(trb:ESN,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020579'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020579'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020579'&'0')
      ***
    OF ?Button:NMSPreAlert
      ThisWindow.Update
      NMSPreAlertBooking
      ThisWindow.Reset
    OF ?status_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
      thiswindow.reset(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      Check_access('3RD PARTY DESPATCH',x")
      If x" = False
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!If x" = False
          Case Missive('Are you sure you want to create a new batch of items to despatch to a 3rd party?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
                  Despatch_Units('NEW')
              Of 1 ! No Button
          End ! Case Missive
      End!If x" = False
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Insert:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert:2, Accepted)
      Check_access('3RD PARTY DESPATCH',x")
      If x" = False
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!If x" = False
          Despatch_Units('ADD')
      End!If x" = False
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert:2, Accepted)
    OF ?Allocate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate, Accepted)
      check_access('3RD PARTY DESPATCH - ALLOCATE',x")
      if x" = false
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!if x" = false
          Allocate_Authorisation_Number
      end!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate, Accepted)
    OF ?Print_Routines
      ThisWindow.Update
      Third_Party_Print_Routine
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
    OF ?Rapid_Job_Update
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rapid_Job_Update, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = brw1.q.trb:ref_number
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
          Globalrequest = ChangeRecord
          Update_Jobs_Rapid
      Else!If access:jobs.fetch(job:ref_number_key) = Level:Benign
          Case Missive('Cannot find the selected job number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rapid_Job_Update, Accepted)
    OF ?returns
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?returns, Accepted)
      BRW1.ResetSort(1)
      Check_access('3RD PARTY DESPATCH - RETURNS',x")
      If x" = False
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
      Else!If x" = False
          Return_units(0)
      End!If x" = False
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?returns, Accepted)
    OF ?CountJobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CountJobs, Accepted)
      thiswindow.reset(1)
      count# = 0
      setcursor(cursor:wait)
      save_trb_id = access:trdbatch.savefile()
      Case status_temp
          Of 'ALL'
              Case Choice(?CurrentTab)
                  Of 1
                      set(trb:batchonlykey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
                  Of 2
                      access:trdbatch.clearkey(trb:batch_number_key)
                      trb:company_name = company_name_temp
                      set(trb:batch_number_key,trb:batch_number_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:company_name <> company_name_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 3
                      set(trb:esn_only_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
                  Of 4
                      set(trb:authorisationkey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
                  Of 5
                      set(trb:jobnumberkey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
              End!Case Choice(?CurrentTab)
          Else
              Case Choice(?Currenttab)
                  Of 1
                      access:trdbatch.clearkey(trb:batch_number_status_key)
                      trb:status       = status_temp
                      set(trb:batch_number_status_key,trb:batch_number_status_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status       <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
      
                  Of 2
                      access:trdbatch.clearkey(trb:batch_company_status_key)
                      trb:status       = status_temp
                      trb:company_name = company_name_temp
                      set(trb:batch_company_status_key,trb:batch_company_status_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status       <> status_temp      |
                          or trb:company_name <> company_name_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 3
                      access:trdbatch.clearkey(trb:esn_status_key)
                      trb:status = status_temp
                      set(trb:esn_status_key,trb:esn_status_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 4
                      access:trdbatch.clearkey(trb:statusauthkey)
                      trb:status          = status_temp
                      set(trb:statusauthkey,trb:statusauthkey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status          <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 5
                      access:trdbatch.clearkey(trb:jobstatuskey)
                      trb:status     = status_temp
                      set(trb:jobstatuskey,trb:jobstatuskey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status     <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
              End!Case Choice(?Currenttab)
      End!Case status_temp
      access:trdbatch.restorefile(save_trb_id)
      setcursor()
      
      Case Missive('There are ' & Count# & ' records.','ServiceBase 3g',|
                     'midea.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CountJobs, Accepted)
    OF ?Company_Name_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Company_Name_temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Company_Name_temp, Accepted)
    OF ?GenerateReport
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GenerateReport, Accepted)
      RUN('VODR0082.EXE')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?GenerateReport, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab:3{PROP:TEXT} = 'By Batch Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab2{PROP:TEXT} = 'By 3rd Party Agent'
        OF 3
          ?Browse:1{PROP:FORMAT} ='69L(2)|M*~I.M.E.I. No~@s16@#31#37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab3{PROP:TEXT} = 'By I.M.E.I. Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='120L(2)|M*~Auth No~@s30@#46#37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#'
          ?Tab4{PROP:TEXT} = 'By Authorisation Number'
        OF 5
          ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Job No~@s8@#16#37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab5{PROP:TEXT} = 'By Job Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Company_Name_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F3Key
              Post(Event:Accepted,?Insert)
          Of F4Key
              Post(Event:Accepted,?Insert:2)
          Of F5Key
              Post(Event:Accepted,?Allocate)
          Of F6Key
              Post(Event:Accepted,?Print_Routines)
          Of F7Key
              Post(Event:Accepted,?Rapid_Job_Update)
          Of F8Key
              Post(Event:Accepted,?Returns)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Company_Name_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Company_Name_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Company_Name_temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Company_Name_temp
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(15,Force)
  ELSE
    RETURN SELF.SetSort(16,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.trb:Batch_Number_NormalFG = -1
  SELF.Q.trb:Batch_Number_NormalBG = -1
  SELF.Q.trb:Batch_Number_SelectedFG = -1
  SELF.Q.trb:Batch_Number_SelectedBG = -1
  SELF.Q.trb:Date_NormalFG = -1
  SELF.Q.trb:Date_NormalBG = -1
  SELF.Q.trb:Date_SelectedFG = -1
  SELF.Q.trb:Date_SelectedBG = -1
  SELF.Q.trb:Company_Name_NormalFG = -1
  SELF.Q.trb:Company_Name_NormalBG = -1
  SELF.Q.trb:Company_Name_SelectedFG = -1
  SELF.Q.trb:Company_Name_SelectedBG = -1
  SELF.Q.trb:Ref_Number_NormalFG = -1
  SELF.Q.trb:Ref_Number_NormalBG = -1
  SELF.Q.trb:Ref_Number_SelectedFG = -1
  SELF.Q.trb:Ref_Number_SelectedBG = -1
  SELF.Q.job:Account_Number_NormalFG = -1
  SELF.Q.job:Account_Number_NormalBG = -1
  SELF.Q.job:Account_Number_SelectedFG = -1
  SELF.Q.job:Account_Number_SelectedBG = -1
  SELF.Q.jot:DateDespatched_NormalFG = -1
  SELF.Q.jot:DateDespatched_NormalBG = -1
  SELF.Q.jot:DateDespatched_SelectedFG = -1
  SELF.Q.jot:DateDespatched_SelectedBG = -1
  SELF.Q.trb:ESN_NormalFG = -1
  SELF.Q.trb:ESN_NormalBG = -1
  SELF.Q.trb:ESN_SelectedFG = -1
  SELF.Q.trb:ESN_SelectedBG = -1
  SELF.Q.trb:MSN_NormalFG = -1
  SELF.Q.trb:MSN_NormalBG = -1
  SELF.Q.trb:MSN_SelectedFG = -1
  SELF.Q.trb:MSN_SelectedBG = -1
  SELF.Q.jot:DateIn_NormalFG = -1
  SELF.Q.jot:DateIn_NormalBG = -1
  SELF.Q.jot:DateIn_SelectedFG = -1
  SELF.Q.jot:DateIn_SelectedBG = -1
  SELF.Q.trb:AuthorisationNo_NormalFG = -1
  SELF.Q.trb:AuthorisationNo_NormalBG = -1
  SELF.Q.trb:AuthorisationNo_SelectedFG = -1
  SELF.Q.trb:AuthorisationNo_SelectedBG = -1
   
   
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Batch_Number_NormalFG = 255
     SELF.Q.trb:Batch_Number_NormalBG = 16777215
     SELF.Q.trb:Batch_Number_SelectedFG = 16777215
     SELF.Q.trb:Batch_Number_SelectedBG = 255
   ELSE
     SELF.Q.trb:Batch_Number_NormalFG = -1
     SELF.Q.trb:Batch_Number_NormalBG = -1
     SELF.Q.trb:Batch_Number_SelectedFG = -1
     SELF.Q.trb:Batch_Number_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Date_NormalFG = 255
     SELF.Q.trb:Date_NormalBG = 16777215
     SELF.Q.trb:Date_SelectedFG = 16777215
     SELF.Q.trb:Date_SelectedBG = 255
   ELSE
     SELF.Q.trb:Date_NormalFG = -1
     SELF.Q.trb:Date_NormalBG = -1
     SELF.Q.trb:Date_SelectedFG = -1
     SELF.Q.trb:Date_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Company_Name_NormalFG = 255
     SELF.Q.trb:Company_Name_NormalBG = 16777215
     SELF.Q.trb:Company_Name_SelectedFG = 16777215
     SELF.Q.trb:Company_Name_SelectedBG = 255
   ELSE
     SELF.Q.trb:Company_Name_NormalFG = -1
     SELF.Q.trb:Company_Name_NormalBG = -1
     SELF.Q.trb:Company_Name_SelectedFG = -1
     SELF.Q.trb:Company_Name_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Ref_Number_NormalFG = 255
     SELF.Q.trb:Ref_Number_NormalBG = 16777215
     SELF.Q.trb:Ref_Number_SelectedFG = 16777215
     SELF.Q.trb:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.trb:Ref_Number_NormalFG = -1
     SELF.Q.trb:Ref_Number_NormalBG = -1
     SELF.Q.trb:Ref_Number_SelectedFG = -1
     SELF.Q.trb:Ref_Number_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.job:Account_Number_NormalFG = 255
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Account_Number_NormalFG = -1
     SELF.Q.job:Account_Number_NormalBG = -1
     SELF.Q.job:Account_Number_SelectedFG = -1
     SELF.Q.job:Account_Number_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.jot:DateDespatched_NormalFG = 255
     SELF.Q.jot:DateDespatched_NormalBG = 16777215
     SELF.Q.jot:DateDespatched_SelectedFG = 16777215
     SELF.Q.jot:DateDespatched_SelectedBG = 255
   ELSE
     SELF.Q.jot:DateDespatched_NormalFG = -1
     SELF.Q.jot:DateDespatched_NormalBG = -1
     SELF.Q.jot:DateDespatched_SelectedFG = -1
     SELF.Q.jot:DateDespatched_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:ESN_NormalFG = 255
     SELF.Q.trb:ESN_NormalBG = 16777215
     SELF.Q.trb:ESN_SelectedFG = 16777215
     SELF.Q.trb:ESN_SelectedBG = 255
   ELSE
     SELF.Q.trb:ESN_NormalFG = -1
     SELF.Q.trb:ESN_NormalBG = -1
     SELF.Q.trb:ESN_SelectedFG = -1
     SELF.Q.trb:ESN_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:MSN_NormalFG = 255
     SELF.Q.trb:MSN_NormalBG = 16777215
     SELF.Q.trb:MSN_SelectedFG = 16777215
     SELF.Q.trb:MSN_SelectedBG = 255
   ELSE
     SELF.Q.trb:MSN_NormalFG = -1
     SELF.Q.trb:MSN_NormalBG = -1
     SELF.Q.trb:MSN_SelectedFG = -1
     SELF.Q.trb:MSN_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.jot:DateIn_NormalFG = 255
     SELF.Q.jot:DateIn_NormalBG = 16777215
     SELF.Q.jot:DateIn_SelectedFG = 16777215
     SELF.Q.jot:DateIn_SelectedBG = 255
   ELSE
     SELF.Q.jot:DateIn_NormalFG = -1
     SELF.Q.jot:DateIn_NormalBG = -1
     SELF.Q.jot:DateIn_SelectedFG = -1
     SELF.Q.jot:DateIn_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:AuthorisationNo_NormalFG = 255
     SELF.Q.trb:AuthorisationNo_NormalBG = 16777215
     SELF.Q.trb:AuthorisationNo_SelectedFG = 16777215
     SELF.Q.trb:AuthorisationNo_SelectedBG = 255
   ELSE
     SELF.Q.trb:AuthorisationNo_NormalFG = -1
     SELF.Q.trb:AuthorisationNo_NormalBG = -1
     SELF.Q.trb:AuthorisationNo_SelectedFG = -1
     SELF.Q.trb:AuthorisationNo_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateTRDBATCH PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?trb:Company_Name
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
History::trb:Record  LIKE(trb:RECORD),STATIC
QuickWindow          WINDOW('Update The Batch File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Amend The Batch File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Batch Number'),AT(240,146),USE(?TRB:Batch_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n-14),AT(316,146,64,10),USE(trb:Batch_Number),LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('3rd Party Agent'),AT(240,162),USE(?TRB:Batch_Number:Prompt:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(316,162,124,10),USE(trb:Company_Name),IMM,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           PROMPT('Job Number'),AT(240,178),USE(?TRB:Ref_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@N_14),AT(316,178,64,10),USE(trb:Ref_Number),SKIP,LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(240,194),USE(?TRB:ESN:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(316,194,124,10),USE(trb:ESN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(240,210),USE(?trb:MSN:Prompt),TRN,HIDE,LEFT,FONT('Arial',8,COLOR:White,956,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,210,124,10),USE(trb:MSN),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                           PROMPT('Status'),AT(240,226),USE(?TRB:ESN:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Return Date'),AT(240,258),USE(?TRB:Date:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6b),AT(316,258,64,10),USE(trb:DateReturn),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Date The Unit Was Returned')
                           BUTTON,AT(384,254),USE(?LookupDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           OPTION('Status'),AT(316,222,124,28),USE(trb:Status),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Out or In')
                             RADIO('Out'),AT(336,235),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('OUT')
                             RADIO('In'),AT(400,235),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('IN')
                           END
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Insert a 3rd Party Batch'
  OF ChangeRecord
    ActionMessage = 'Changing A 3rd Party Batch'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020581'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTRDBATCH')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(trb:Record,History::trb:Record)
  SELF.AddHistoryField(?trb:Batch_Number,2)
  SELF.AddHistoryField(?trb:Company_Name,3)
  SELF.AddHistoryField(?trb:Ref_Number,4)
  SELF.AddHistoryField(?trb:ESN,5)
  SELF.AddHistoryField(?trb:MSN,14)
  SELF.AddHistoryField(?trb:DateReturn,11)
  SELF.AddHistoryField(?trb:Status,6)
  SELF.AddUpdateFile(Access:TRDBATCH)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Access:JOBTHIRD.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRDBATCH
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  !Msn?
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = trb:Ref_Number
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
      man:Manufacturer = job:Manufacturer
      If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:Use_MSN = 'YES'
              ?trb:MSN{Prop:Hide} = 0
              ?trb:MSN:Prompt{Prop:Hide}=0
          End !If man:Use_MSN = 'YES'
      Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
  Else!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  ! Save Window Name
   AddToLog('Window','Open','UpdateTRDBATCH')
  Bryan.CompFieldColour()
  ?TRB:DateReturn{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  FDCB8.Init(trb:Company_Name,?trb:Company_Name,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(trd:Company_Name_Key)
  FDCB8.AddField(trd:Company_Name,FDCB8.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateTRDBATCH')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020581'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020581'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020581'&'0')
      ***
    OF ?trb:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
      IF LEN(CLIP(trb:ESN)) = 18
        !Ericsson IMEI!
        trb:ESN = SUB(trb:ESN,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
    OF ?LookupDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          trb:DateReturn = TINCALENDARStyle1(trb:DateReturn)
          Display(?TRB:DateReturn)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
  Access:JobThird.ClearKey(jot:ThirdPartyKey)
  jot:ThirdPartyNumber = trb:RecordNumber
  IF Access:JobThird.Fetch(jot:ThirdPartyKey)
    !Error!
  ELSE
    IF trb:Status = 'IN'
      jot:DateIn = trb:DateReturn
      jot:InIMEI = trb:ESN
    ELSIF trb:Status = 'OUT'
      jot:OutIMEI = trb:ESN
      jot:DateOut = trb:DateReturn
    END
    Access:JobThird.Update()
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?trb:DateReturn
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Despatch_Units PROCEDURE (f_type)                     !Generated from procedure template - Window

! ================================================================
! Xml Export FILE structure
fileXmlExport   FILE,DRIVER('ASCII'),CREATE,BINDABLE,THREAD
Record              RECORD
recbuff                 STRING(256)
                    END
                END
! Xml Export Class Instance
objXmlExport XmlExport
! ================================================================
Company_Name_temp    STRING(30)
UnallocatedParts     BYTE
ExistingParts        BYTE
tmp:type             STRING(3)
tmp:OriginalIMEI     STRING(30)
tmp:OriginalMSN      STRING(30)
save_trb_ali_id      USHORT,AUTO
save_trb_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
Job_Number_Temp      STRING(20)
esn_temp             STRING(30)
batch_number_temp    LONG
tmp:beginbatch       BYTE(0)
tmp:recordcount      LONG
tmp:jobsqueue        QUEUE,PRE(jobque)
job_number           LONG
RecordNumber         LONG
                     END
DisplayString        STRING(255)
tmp:JobNumber        LONG
tmp:passfail         STRING(40)
tmp:addbatch         BYTE(0)
tmp:itemsinbatch     LONG
tmp:CurrentBatchNumber LONG
tmp:Exchanged        BYTE(0)
tmp:msn              STRING(30)
tmp:GotExchange      BYTE
tmp:False            BYTE(0)
locAuditNotes        STRING(255)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Company_Name_temp
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
trd:Account_Number     LIKE(trd:Account_Number)       !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                       PROJECT(trd:Account_Number)
                     END
BRW5::View:Browse    VIEW(JOBACC)
                       PROJECT(jac:Accessory)
                       PROJECT(jac:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jac:Accessory          LIKE(jac:Accessory)            !List box control field - type derived from field
jac:Ref_Number         LIKE(jac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Inserting A 3rd Party Batch'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Inserting A 3rd Party Batch'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('3rd Party Agent'),AT(68,122),USE(?company_name_temp:prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       COMBO(@s30),AT(144,122,124,10),USE(Company_Name_temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@120L(2)|M@s30@'),DROP(20,200),FROM(Queue:FileDropCombo)
                       LIST,AT(324,72,124,284),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Accessory~@s30@'),FROM(Queue:Browse)
                       LIST,AT(460,74,152,284),USE(?List2),VSCROLL,FONT('Tahoma',8,010101H,FONT:regular,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('56L(2)|M~Job Number~@s8@'),FROM(tmp:jobsqueue)
                       SHEET,AT(64,54,252,88),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('3rd Party Batch'),USE(?New_Tab),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Select a 3rd Party Agent and then tick ''Begin Batch''.'),AT(88,94,200,16),USE(?Prompt4),CENTER,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Begin Batch'),AT(156,78),USE(tmp:beginbatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                         END
                         TAB('Add To 3rd Party Batch'),USE(?Add_Tab),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Batch Number'),AT(68,86),USE(?batch_number_temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s4),AT(144,86,64,10),USE(batch_number_temp),LEFT(1),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Select a 3rd Party Agent and a Batch Number, then click ''Add To Batch'' to begin ' &|
   'adding to the selected Batch.'),AT(68,99,240,19),USE(?Prompt8),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Add To Batch'),AT(144,70),USE(tmp:addbatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Add to Existing Batch'),TIP('Add to Existing Batch'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(320,54,132,310),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('Accessories'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(64,144,252,220),USE(?Sheet2),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('E.S.N. / I.M.E.I.'),USE(?Tab2)
                           PROMPT('Insert the Job Number and I.M.E.I. Number of the Job being sent to the 3rd Party' &|
   ' Agent, and then select one of the three options below.'),AT(68,149,244,25),USE(?Prompt5),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Batch Number'),AT(68,176),USE(?Prompt6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(144,176),USE(tmp:CurrentBatchNumber),HIDE,LEFT,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(144,189),USE(tmp:itemsinbatch),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Batch Limit'),AT(68,200),USE(?Prompt10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING(@s8),AT(144,200),USE(trd:BatchLimit),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Total In Batch'),AT(68,189),USE(?Prompt6:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(68,213),USE(?Job_Number_Temp:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s20),AT(144,213,64,10),USE(Job_Number_Temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('I.M.E.I. Number'),AT(68,229),USE(?esn_temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,229,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('M.S.N.'),AT(68,245),USE(?tmp:msn:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(144,245,124,10),USE(tmp:msn),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.S.N.'),TIP('M.S.N.'),REQ,UPR
                           STRING(@s40),AT(68,261,244,13),USE(tmp:passfail),FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       SHEET,AT(456,56,160,308),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('Jobs Updated Successfully'),USE(?Tab4),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(548,366),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       GROUP,AT(68,328,272,20),USE(?Button_Group),DISABLE
                         BUTTON,AT(72,332),USE(?Exchange),TRN,FLAT,LEFT,ICON('exchp.jpg')
                         BUTTON,AT(160,332),USE(?accessories_retained),TRN,FLAT,LEFT,ICON('retainap.jpg')
                         BUTTON,AT(248,332),USE(?Despatched),TRN,FLAT,LEFT,ICON('desp2p.jpg')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ld                   CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
FinalValidation     Routine

    trb:exchanged = 'NO'
    error# = 0
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.tryfetch(job:ref_number_key)
        error# = 1
        Case Missive('Unable to find the selected job number.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    Else!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        If JobInUse(job:Ref_Number,1) = False
            If job:date_completed <> ''
                Case Missive('The selected job has been completed.','ServiceBase 3g',|
                               'mstop.jpg','/OK') 
                    Of 1 ! OK Button
                End ! Case Missive
                error# = 1
            End!If job:date_completed <> ''

            !Check the job has been through Waybill Confirmation
            !and is at the ARC Location - L921 (DBH: 13-08-2003)
            If Error# = 0
                Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                lot:RefNumber   = job:Ref_Number
                lot:NewLocation = Clip(GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI'))
                If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                    !Found
                Else !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
                    !Error
                    Case Missive('Error! This job is not at the ARC location. Please ensure that the waybill confirmation procedure has been completed.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                    Error# = 1
                End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
            End !If Error# = 0
            !Stuart 20/11/02 - put check in here to see if warranty / chargeable parts are on this job, and error if so.
            !J -    26/11/02 - ref VP126/L298/H124 - allow parts attached - but set sale price to zero
            If Error# = 0
                Access:WARPARTS.CLEARKEY(wpr:Part_Number_Key)
                wpr:Ref_Number = job:Ref_Number
                SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
                ExistingParts = 0
                UnallocatedParts = 0
                LOOP
                  IF Access:WARPARTS.NEXT() THEN BREAK.
                  IF wpr:Ref_Number <> job:Ref_Number THEN BREAK.
                  if wpr:PartAllocated = 0 then UnallocatedParts =1.
                  ExistingParts = 1
                END !LOOP

                if UnallocatedParts = 1 then
                    Case Missive('Error! Cannot despatch due to unallocated warranty parts being attached to the job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                   error# = 1
                ELSE
                   error# = 0
                    if existingParts = 1 then
                        !ref VP126/L298/H124 - allow parts attached - but set sale price to zero
                        Case Missive('Warning! There are warranty parts attached to this job.'&|
                          '<13,10>If you continue the selling price will be removed from all these parts.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                       'mquest.jpg','\No|/Yes') 
                            Of 2 ! Yes Button
                                !set both ARC and RRC sale prices to zero
                                Access:WARPARTS.CLEARKEY(wpr:Part_Number_Key)
                                wpr:Ref_Number = job:Ref_Number
                                SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
                                LOOP
                                    IF Access:WARPARTS.NEXT() THEN BREAK.
                                    IF wpr:Ref_Number <> job:Ref_Number THEN BREAK.
                                    IF wpr:Part_Number = 'EXCH' THEN cycle.
                                    wpr:RRCPurchaseCost = 0
                                    wpr:Purchase_Cost = 0
                                    access:warparts.update()
                                END !LOOP
                                error#=0
                            Of 1 ! No Button
                                error# = 1    !make it stop
                        End ! Case Missive
                    END !If existing parts

                END !If unallocated parts exist

                Access:PARTS.CLEARKEY(par:Part_Number_Key)
                par:Ref_Number = job:Ref_Number
                SET(par:Part_Number_Key,par:Part_Number_Key)
                UnallocatedParts = 0
                ExistingParts    = 0
                LOOP
                  IF Access:PARTS.NEXT() THEN BREAK.
                  IF par:Ref_Number <> job:Ref_Number THEN BREAK.
                  if par:PartAllocated = 0 then UnallocatedParts = 1.
                  ExistingParts  = 1
                END !LOOP

                if UnallocatedParts = 1
                    Case Missive('Error! Cannot despatch due to unallocated chargeable parts being attached to the job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                       error# = 1
                ELSE
                    error# = 0
                    if existingParts = 1
                        !ref VP126/L298/H124 - allow parts attached - but set sale price to zero
                        Case Missive('Warning! There are chargeable parts attached to this job.'&|
                          '<13,10>If you continue the selling price will be removed from all these parts.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                       'mstop.jpg','\No|/Yes') 
                            Of 2 ! Yes Button
                                !set both ARC and RRC sale prices to zero
                                Access:PARTS.CLEARKEY(par:Part_Number_Key)
                                par:Ref_Number = job:Ref_Number
                                SET(par:Part_Number_Key,par:Part_Number_Key)
                                LOOP
                                    IF Access:PARTS.NEXT() THEN BREAK.
                                    IF par:Ref_Number <> job:Ref_Number THEN BREAK.
                                    IF par:Part_Number = 'EXCH' THEN cycle.
                                    par:RRCSaleCost = 0
                                    par:Sale_Cost = 0
                                    access:parts.update()
                                END !LOOP
                                error#=0
                            Of 1 ! No Button
                                error# = 1    !make it stop
                        End ! Case Missive

                   END !If existing parts = 1
                END !If unallocated parts exist


                Access:ESTPARTS.CLEARKEY(epr:Part_Number_Key)
                epr:Ref_Number = job:Ref_Number
                SET(epr:Part_Number_Key,epr:Part_Number_Key)
                ExistingParts = 0
                UnallocatedParts = 0
                LOOP
                  IF Access:ESTPARTS.NEXT() THEN BREAK.
                  IF epr:Ref_Number <> job:Ref_Number THEN BREAK.
                  if epr:PartAllocated = 0 then UnallocatedParts =1.
                  ExistingParts = 1
                END !LOOP

                if UnallocatedParts = 1 then
                    Case Missive('Error! Cannot despatch due to unallocated estimate parts being attached to the job.','ServiceBase 3g',|
                                   'mstop.jpg','/OK') 
                        Of 1 ! OK Button
                    End ! Case Missive
                   error# = 1
                ELSE
                   error# = 0
                    if existingParts = 1 then
                        !ref VP126/L298/H124 - allow parts attached - but set sale price to zero
                        Case Missive('Warning! There are estimate parts attached to this job.'&|
                          '<13,10>If you continue the selling price will be removed from all these parts.'&|
                          '<13,10>'&|
                          '<13,10>Are you sure you want to continue?','ServiceBase 3g',|
                                       'mstop.jpg','\No|/Yes') 
                            Of 2 ! Yes Button
                                !set both ARC and RRC sale prices to zero
                                Access:ESTPARTS.CLEARKEY(epr:Part_Number_Key)
                                epr:Ref_Number = job:Ref_Number
                                SET(epr:Part_Number_Key,epr:Part_Number_Key)
                                LOOP
                                    IF Access:ESTPARTS.NEXT() THEN BREAK.
                                    IF epr:Ref_Number <> job:Ref_Number THEN BREAK.
                                    IF epr:Part_Number = 'EXCH' THEN cycle.
                                    epr:RRCSaleCost = 0
                                    epr:Sale_Cost = 0
                                    access:estparts.update()
                                END !LOOP
                                error#=0
                            Of 1 ! No Button
                                error# = 1    !make it stop
                        End ! Case Missive
                    END !If existing parts
                END !If unallocated parts exist


                !Check if the trade account is allow to despatch to third party
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = sub:Main_Account_Number
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        If tra:StopThirdParty
                            Case Missive('The trade account for this job does not allow units to be sent to third party.','ServiceBase 3g',|
                                           'mstop.jpg','/OK') 
                                Of 1 ! OK Button
                            End ! Case Missive
                            Error# = 1
                        End !If tra:StopThirdParty
                    Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    
                Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                
                If error# = 0
                    If job:esn <> esn_temp
                        Access:EXCHANGE.ClearKey(xch:Esn_Only_Key)
                        xch:esn = esn_temp
                        If Access:EXCHANGE.TryFetch(xch:Esn_Only_Key) = Level:Benign
                            Case Missive('Error! You have entered the I.M.E.I. number of the exchange unit attached to this job.','ServiceBase 3g',|
                                           'mstop.jpg','/OK') 
                                Of 1 ! OK Button
                            End ! Case Missive
                            Error# = 1
                        Else!if access:exchange.tryfetch(xch:esn_only_key) = Level:Benign
                            Access:Loan.clearkey(loa:esn_only_key)
                            loa:esn = esn_temp
                            if access:loan.tryfetch(loa:esn_only_key) = Level:Benign
                                Case Missive('Error! You have entered the I.M.E.I. number of the loan unit attached to this job.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Error# = 1
                            Else!if access:loan.tryfetch(loa:esn_only_key) = Level:Benign
                                Case Missive('Error! Incorrect I.M.E.I. number entered for this job number.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Error# = 1
                            End!if access:loan.tryfetch(loa:esn_only_key) = Level:Benign
                        End!if access:exchange.tryfetch(xch:esn_only_key) = Level:Benign
                    Else!If job:esn <> esn_temp
                        If ?tmp:msn{prop:hide} = 0 And job:msn <> tmp:msn
                            Access:EXCHANGE.Clearkey(xch:MSN_Only_Key)
                            xch:msn = tmp:msn
                            If Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign
                                Case Missive('Error! You have entered the M.S.N. of the exchange unit attached to this job.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                Error# = 1
                            Else !If Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign
                                Access:LOAN.Clearkey(loa:MSN_Only_Key)
                                loa:MSN = tmp:MSN
                                If Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign
                                    Case Missive('Error! You have entered the M.S.N. of the loan unit attached to this job.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK') 
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    Error# = 1
                                Else !If Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign
                                    Case Missive('Error! Incorrect M.S.N. entered for this job number.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK') 
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    Error# = 1
                                End !If Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign
                            End !If Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign
                        Else !If ?tmp:msn{prop:hide} = 0 And job:msn <> tmp:msn
                            access:trdmodel.clearkey(trm:model_number_key)
                            trm:company_name = company_name_temp
                            trm:model_number = job:model_number
                            if access:trdmodel.tryfetch(trm:model_number_key)
                                Case Missive('The selected 3rd party has not been authorised to repair this model number.','ServiceBase 3g',|
                                               'mstop.jpg','/OK') 
                                    Of 1 ! OK Button
                                End ! Case Missive
                                error# = 1
                            Else!if access:trdmodel.tryfetch(trm:model_number_key)
                                found# = 0
                                save_trb_ali_id = access:trdbatch_alias.savefile()
                                access:trdbatch_alias.clearkey(trb_ali:esn_only_key)
                                trb_ali:esn = esn_temp
                                set(trb_ali:esn_only_key,trb_ali:esn_only_key)
                                loop
                                    if access:trdbatch_alias.next()
                                       break
                                    end !if
                                    if trb_ali:esn <> esn_temp      |
                                        then break.  ! end if
                                    If trb_ali:DateReturn = ''
                                        Found# = 2
                                        Break
                                    End !If trb_ali:DateReturn = ''
                                    If trb_Ali:DateReturn <> ''
                                        found# = 1
                                    End !If trb_Ali:DateReturn <> ''
                                end !loop
                                access:trdbatch_alias.restorefile(save_trb_ali_id)

                                If found# = 1
                                    Case Missive('The selected I.M.E.I. number has been previously entered.'&|
                                      '<13,10>'&|
                                      '<13,10>Do you wish to continue, or enter the I.M.E.I. number?','ServiceBase 3g',|
                                                   'mquest.jpg','\Re-Enter|/Continue') 
                                        Of 2 ! Continue Button
                                        Of 1 ! Re-Enter Button
                                            error# = 1
                                    End ! Case Missive
                                End!If found# = 1

                                If found# = 2
                                    Case Missive('The selected I.M.E.I. number is already attached to a batch and the unit has not been marked as returned.','ServiceBase 3g',|
                                                   'mstop.jpg','/OK') 
                                        Of 1 ! OK Button
                                    End ! Case Missive
                                    Error# = 1
                                End !If found# = 2
                                If error# = 0
                                    Enable(?button_group)
                                    tmp:JobNumber = job:ref_number
                                    brw5.resetsort(1)
                                End!If found# = 1
                            End!if access:trdmodel.tryfetch(trm:model_number_key)
                        End !If ?tmp:msn{prop:hide} = 0 And job:msn <> tmp:msn

                    End!If job:esn <> esn_temp

                End!If error# = 0
            End !If Error# = 0
            If error# = 1
                Select(?esn_temp)
                tmp:passfail = 'Job Number ' & Clip(job_number_temp) & ' Updated Failed'
                ?tmp:passfail{prop:fontcolor} = color:red
                disable(?button_group)
            End!If error# = 1
        End !If Errorcode() = 43
    End!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
       


Despatch        Routine
    DATA
kPreClaim       BYTE(0)
    CODE
    Access:WEBJOB.CLearkey(wob:RefNumberKey)
    wob:RefNumber = job:Ref_Number
    IF (Access:WEBJOB.Tryfetch(wob:RefNumberKey))
    END

    IF (job:Warranty_Job = 'YES')

        IF (_PreClaimThirdParty(Company_Name_temp,job:Manufacturer))
            kPreClaim = 1
            glo:ErrorText = ''
            CompulsoryFieldCheck('T')
            IF (glo:ErrorText <> '')
                RELEASE(JOBS)
                glo:ErrorText = 'You cannot Despatch this unit because of the following error(s): <13,10>' & Clip(glo:ErrorText)
                Error_Text
                glo:errortext = ''
                
                EXIT
            END
        END
    END
    If Access:TRDBATCH.Primerecord() = Level:Benign
!        Stop('After Prime: ' & trb:RecordNumber & '<13,10>Job Number: ' & job:Ref_Number)
        job:Workshop = 'NO'
        job:Third_Party_Site    = Company_Name_Temp
        !Remove the engineer from the job, so he can
        !be reallocated when the unit comes back
        job:Engineer = ''
        GetStatus(410,1,'JOB') !Despatched To 3rd Party
        If tmp:Exchanged = 1
            trb:Exchanged = 'YES'
            If job:Exchange_Unit_Number = ''
                GetStatus(108,1,'EXC') !Exchange Unit Required
            End !If job:Exchange_Unit_Number <> ''
        Else !If tmp:Exchanged = 1
            trb:Exchanged = 'NO'
        End !If tmp:Exchanged = 1

        Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey)
            !If A Jobnotes entry doesn't not already exist, create one.
            If Access:JOBNOTES.Primerecord() = Level:Benign
                jbn:RefNumber   = job:Ref_Number
                If Access:JOBNOTES.Tryinsert()
                    Access:JOBNOTES.Cancelautoinc()
                End!If Access:JOBNOTES.Tryinsert()
                Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                jbn:RefNumber   = job:Ref_Number
                If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    !Found
                End! If Access:.Tryfetch(jbn:RefNumberKey) = Level:Benign
          End!If Access:JOBNOTES.Primerecord() = Level:Benign
        End! If Access:.Tryfetch(jbn:RefNumberKey) = Level:Benign
        Clear(glo:Queue)
        Free(glo:Queue)
        Case tmp:type
            Of 'DES'
                If Records(Queue:Browse)
                    PickJobAccessories(job:Ref_Number)
                    If Records(Queue:Browse) <> Records(glo:Queue)
                        If jbn:Engineers_Notes = ''
                            jbn:Engineers_Notes = '<13,10>' & Records(glo:Queue) & ' ACCESSORIES DESPATCHED WITH UNIT'
                        Else!If job:engineers_notes = ''
                            jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>' & Records(glo:Queue) & ' ACCESSORIES DESPATCHED WITH UNIT'
                        End!If job:engineers_notes = ''

                    Else !If Records(Queue:Browse) <> Records(glo:Queue)
                        If jbn:Engineers_Notes = ''
                            jbn:Engineers_Notes = '<13,10>ACCESSORIES DESPATCHED WITH UNIT'
                        Else!If job:engineers_notes = ''
                            jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>ACCESSORIES DESPATCHED WITH UNIT'
                        End!If job:engineers_notes = ''

                    End !If Records(Queue:Browse) <> Records(glo:Queue)
                End !If Records(Queue:Browse)
                
            Of 'RET'
                If jbn:Engineers_Notes = ''
                    jbn:Engineers_Notes = '<13,10>ACCESSORIES RETAINED'
                Else!If job:engineers_notes = ''
                    jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>ACCESSORIES RETAINED'
                End!If job:engineers_notes = ''
        End!Case tmp:type

        access:JOBNOTES.Update()

        !Change to 3rd Party Location
        LocationChange(Clip(GETINI('RRC','RTMLocation',,CLIP(PATH())&'\SB2KDEF.INI')))

        AddToConsignmentHistory(job:Ref_Number,'ARC','3RD PARTY',job:Manufacturer,'BATCH: ' & Clip(tmp:CurrentBatchNumber),'JOB')

        !Remove the engineer
        job:Engineer = ''

        IF (kPreClaim)
            ! #12363 Add to the warranty claims browse (DBH: 13/02/2012)
            job:EDI = PendingJob(job:Manufacturer)
        END
        
        If Access:JOBS.TryUpdate() = Level:Benign
            ! #12363 Update Warranty Status (DBH: 29/02/2012)
            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber =job:Ref_Number
            IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                 If jobe:WarrantyClaimStatus = '' and job:EDI <> 'XXX'
                     jobe:WarrantyClaimStatus = 'PENDING'
                     Access:JOBSE.TryUpdate()
                 End !If jobe:WarrantyStatus = ''
            END ! IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)


            RELEASE(JOBS)
            tmp:OriginalIMEI    = job:Esn
            tmp:OriginalMSN     = job:MSN

            !See if a previous entry exists in the JOBTHIRD file for this job
            !If so, then take the Original IMEI Number. So every entry has the same
            !Original IMEI Number.
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT() = Level:Benign
                If jot:RefNumber = job:Ref_Number
                    error# = 0
                    tmp:OriginalIMEI    = jot:OriginalIMEI
                    tmp:OriginalMSN     = jot:OriginalMSN
                End!If jot:RefNumber = job:RefNumber
            End!If Access:JOBTHIRD.NEXT() = Level:Benign

            Access:JOBTHIRD.ClearKey(jot:ThirdPartyKey)
            jot:ThirdPartyNumber = trb:RecordNumber
            If Access:JOBTHIRD.TryFetch(jot:ThirdPartyKey) = Level:Benign
               ASSERT(0,'<13,10>About to create a duplicate JOBTHIRD value '&|
                        '<13,10>(Job Number: ' & Clip(job:Ref_Number) &')'& |
                        '<13,10>(Record Number: ' & Clip(trb:RecordNumber) & ')<13,10>')
            End

            !Create a new entry in the JOBTHIRD file for this despatch
            If Access:JOBTHIRD.Primerecord() = Level:Benign
                jot:RefNumber       = job:Ref_Number
                jot:OriginalIMEI    = tmp:OriginalIMEI
                jot:OriginalMSN     = tmp:OriginalMSN
                jot:OutIMEI         = ESN_Temp
                jot:OutMSN          = job:MSN
                jot:InIMEI          = ''
                jot:DateOut         = Today()
                jot:DateIn          = ''
                jot:ThirdPartyNumber    = trb:RecordNumber
                If Access:JOBTHIRD.Tryinsert()
                    Access:JOBTHIRD.Cancelautoinc()
                End!If Access:JOBTHIRD.Tryinsert()
            End!If Access:JOBTHIRD.Primerecord() = Level:Benign

            !Create an entry in the AUDIT trail
                locAuditNotes         = '3RD PARTY AGENT: ' & Clip(Company_Name_Temp) & '<13,10>BATCH NUMBER: ' & Clip(tmp:CurrentBatchNumber)
                If trb:Exchanged = 'YES'
                    locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>EXCHANGE UNIT REQUIRED'
                End!If trb:exchanged = 'YES'

                !Check if there are any accessories attached to this job
                !If so, make an entry in the AUDIT trail listing them.
                locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES RETAINED WITH UNIT:'
                acc_count# = 0
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    ! Insert --- Only show accessories that were sent to the ARC (DBH: 07/09/2009) #11056
                    if (jac:Attached <> 1)
                        Cycle
                    end ! if (jac:Attached <> 1)
                    ! end --- (DBH: 07/09/2009) #11056
                        Case tmp:Type
                            Of 'RET'
                                locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:accessory)
                                acc_count# += 1

                            Of 'DES'
                                Sort(glo:Queue,glo:Pointer)
                                glo:Pointer = jac:Accessory
                                Get(glo:Queue,glo:Pointer)
                                If Error()
                                    locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                                    acc_count# += 1

                                End !If ~Error()>
                        End!Case tmp:Type

                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)

                If tmp:Type = 'DES'

                    locAuditNotes = Clip(locAuditNotes) & '<13,10,13,10>ACCESSORIES DESPATCHED WITH UNIT:'
                    Save_jac_ID = Access:JOBACC.SaveFile()
                    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                    jac:Ref_Number = job:Ref_Number
                    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                    Loop
                        If Access:JOBACC.NEXT()
                           Break
                        End !If
                        If jac:Ref_Number <> job:Ref_Number      |
                            Then Break.  ! End If
                        ! Insert --- Only show accessories that were sent to the ARC (DBH: 07/09/2009) #11056
                        if (jac:Attached <> 1)
                            Cycle
                        end ! if (jac:Attached <> 1)
                        ! end --- (DBH: 07/09/2009) #11056
                        Sort(glo:Queue,glo:Pointer)
                        glo:Pointer = jac:Accessory
                        Get(glo:Queue,glo:Pointer)
                        If ~Error()
                            locAuditNotes = Clip(locAuditNotes) & '<13,10>' & Clip(jac:Accessory)
                        End !If ~Error()>

                    End !Loop
                    Access:JOBACC.RestoreFile(Save_jac_ID)
                End !If tmp:Type = 'DES'

                IF (AddToAudit(job:Ref_Number,'JOB','3RD PARTY AGENT: UNIT SENT',locAuditNotes))
                END ! IF

            trb:Batch_Number    = tmp:CurrentBatchNumber
            trb:Ref_Number      = job_number_temp
            trb:Status          = 'OUT'
            trb:Company_Name    = Company_Name_Temp
            trb:ESN             = esn_temp
            trb:MSN             = job:MSN
            trb:Date            = Today()
            trb:Time            = Clock()
            If Access:TRDBATCH.Tryinsert()
                Access:TRDBATCH.Cancelautoinc()
            End!If Access:TRDBATCH.Tryinsert()
            !Write the entry into the Trade Batch File

            tmp:passfail = 'Job Number ' & Clip(job_number_temp) & ' Updated Successfully'
            ?tmp:passfail{prop:fontcolor} = 080FFFFH
            tmp:recordcount += 1
            jobque:job_number = job_number_temp
            jobque:RecordNumber = tmp:recordcount
            Add(tmp:jobsqueue)
            Sort(tmp:jobsqueue,-jobque:RecordNumber)

            !Print 3rd Party Despatch Note
            Set(DEFAULTS)
            access:DEFAULTS.next()
            Set(DEFAULT2)
            Access:DEFAULT2.Next()
            glo:select1 = job_number_temp
            glo:select2  = trb:batch_number
            glo:select3  = Company_Name_Temp
            If DEF:ThirdPartyNote = 'YES'
                Third_Party_Single_Despatch(tmp:type)
            End!If DEF:ThirdPartyNote = 'YES'
            If de2:PrintRetainedAccLabel
                If acc_count# <> 0
                    Retained_Accessories_Label
                End!If acc_count# <> 0
            End !If de2:PrintRetainedAccLabel
            glo:select1  = ''
            glo:select2  = ''
            glo:select3  = ''
            Clear(glo:Queue)
            Free(glo:Queue)

            Disable(?button_group)
            job_number_temp = ''
            esn_temp = ''
            tmp:msn = ''
            ?tmp:msn{prop:hide} = 1
            ?tmp:msn:prompt{prop:hide} = 1
            Select(?job_number_temp)
            tmp:jobnumber = ''
            brw5.resetsort(1)
            ThisWindow.UPDATE()
            Display()


            
        End!If access:jobs.tryupdate() = Level:Benign
        Do count_batch
    End!If Access:TRDBATCH.Primerecord() = Level:Benign
Count_Batch         Routine
    Setcursor(cursor:wait)
    tmp:ItemsInBatch = 0
    Save_trb_ali_Id = Access:TRDBATCH_ALIAS.Savefile()
    Access:TRDBATCH_ALIAS.Clearkey(trb_ali:Company_Batch_Esn_Key)
    trb_ali:Company_Name = Company_Name_Temp
    trb_ali:Batch_Number = tmp:CurrentBatchNumber
    Set(trb_ali:Company_Batch_Esn_Key,trb_ali:Company_Batch_Esn_Key)
    Loop
        If Access:TRDBATCH_ALIAS.Next()
           Break
        End !if
        If trb_ali:Company_Name <> trb:Company_Name      |
        Or trb_ali:Batch_Number <> tmp:CurrentBatchNumber      |
            then Break.  ! end if
        tmp:ItemsInBatch += 1
    end !loop
    Access:TRDBATCH_ALIAS.Restorefile(Save_trb_ali_Id)
    Setcursor()
    If tmp:ItemsInBatch > trd:BatchLimit and trd:BatchLimit <> 0
        Case Missive('You have exceed the batch limit for this third party repairer.'&|
          '<13,10>'&|
          '<13,10>Do you want to continue to add jobs to this batch?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
            Of 1 ! No Button
                Post(event:accepted,?finish)
        End ! Case Missive
    End!If tmp:itemsinbatch => trd:batchlimit
XMLExport       Routine
Data
local:Seq        Long()
local:ToSendFolder  CString(255)
local:SentFolder    CString(255)
local:ReceivedFolder CString(255)
local:ProcessedFolder CString(255)
local:XMLFileName   CString(255)
Code
    If Records(tmp:JobsQueue) = 0
        Exit
    End ! If Records(tmp:JobsQueue) = 0

    Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
    trd:Company_Name = Company_Name_Temp
    If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Found
        If trd:LHubAccount = 0
            Exit
        End ! If trd:LHubAccount = 0
    Else ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        !Error
    End ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

    local:ToSendFolder = GETINI('NMSFTP','XMLFolder',,Clip(Path()) & '\SB2KDEF.INI')
    If Sub(Clip(local:ToSendFolder),-1,1) <> '\'
        local:ToSendFolder = Clip(local:ToSendFolder) & '\'
    End ! If Sub(Clip(local:ToSendFolder),-1,1) <> '\'

    local:SentFolder = Clip(local:ToSendFolder) & 'Sent'
    If ~Exists(local:SentFolder)
        x# = MkDir(local:SentFolder)
    End ! If ~Exists(local:SentFolder)

    If ~Exists(local:SentFolder)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('A problem has occured accessing the NMS XML Folder.','ServiceBase 3g',|
                       'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
        End!Case Message
        Exit
    End ! If ~Exists(local:ToSendFolder)

    local:ReceivedFolder = Clip(local:ToSendFolder) & 'Received'
    If ~Exists(local:ReceivedFolder)
        x# = MkDir(local:ReceivedFolder)
    End ! If ~Exists(local:SentFolder)

    If ~Exists(local:ReceivedFolder)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('A problem has occured accessing the NMS XML Folder.','ServiceBase 3g',|
                       'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
        End!Case Message
        Exit
    End ! If ~Exists(local:ToSendFolder)

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Processed'
    If ~Exists(local:ProcessedFolder)
        x# = MkDir(local:ProcessedFolder)
    End ! If ~Exists(local:SentFolder)

    If ~Exists(local:ProcessedFolder)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('A problem has occured accessing the NMS XML Folder.','ServiceBase 3g',|
                       'mstop.jpg','/&OK')
            Of 1 ! &OK Button
        End!Case Message
        Exit
    End ! If ~Exists(local:ToSendFolder)

    local:ProcessedFolder = Clip(local:ToSendFolder) & 'Bad'
    If ~Exists(local:ProcessedFolder)
        x# = MkDir(local:ProcessedFolder)
    End ! If ~Exists(local:SentFolder)

    If ~Exists(local:ProcessedFolder)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('A problem has occured accessing the NMS XML Folder.','ServiceBase 3g',|
                       'mstop.jpg','/&OK')
            Of 1 ! &OK Button
        End!Case Message
        Exit
    End ! If ~Exists(local:ToSendFolder)

    local:ToSendFolder = Clip(local:ToSendFolder) & 'ToSend'
    If ~Exists(local:ToSendFolder)
        x# = MkDir(local:ToSendFolder)
    End ! If ~Exists(local:ToSendFolder)
    If ~Exists(local:ToSendFolder)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Missive('A problem has occured accessing the NMS XML Folder.','ServiceBase 3g',|
                       'mstop.jpg','/&OK') 
            Of 1 ! &OK Button
        End!Case Message
        Exit
    End ! If ~Exists(local:ToSendFolder)

    local:XMLFileName =  'RR_' & Batch_Number_Temp & Format(Today(),@d12) & Clock() & '.xml'


    If objXmlExport.FOpen(local:ToSendFolder & '\' & local:XMLFileName,1) = Level:Benign
        objXmlExport.OpenTag('ShipmentOrderRequest','Version="4.0"')
        objXmlExport.OpenTag('Authentication')
            objXmlExport.WriteTag('SiteId',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('UserId',GETINI('NMSFTP','UserID',,Clip(Path()) & '\SB2KDEF.INI'))
            objXmlExport.WriteTag('ToolType','CT')
            objXmlExport.WriteTag('AccessKey',GETINI('NMSFTP','AccessKey',,Clip(Path()) & '\SB2KDEF.INI'))
        objXmlExport.CloseTag('Authentication')
        objXmlExport.WriteTag('ClientReferenceNumber',Clip(tmp:CurrentBatchNumber) & '-' & Today() & Clock())
        objXmlExport.WriteTag('ShipmentType','001')
        objXmlExport.WriteTag('SourceSiteId',trd:ASCID)

        local:Seq = 0

        Loop x# = 1 To Records(tmp:JobsQueue)
            Get(tmp:JobsQueue,x#)
            Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = jobque:Job_Number
            If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Found
            Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

            Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = job:Ref_Number
            If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Found
            Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                !Error
                Cycle
            End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign

            local:Seq += 1
            objXmlExport.OpenTag('Booking')
                objXmlExport.WriteTag('SequenceNo',local:Seq)
                objXmlExport.WriteTag('BookingType','001')
                objXmlExport.WriteTag('ItemType','PHONE')
                objXmlExport.WriteTag('ClientBookingNumber',job:Ref_Number)
                objXmlExport.WriteTag('FaultSymptomCode',Clip(job:Fault_Code1))
                objXmlExport.WriteTag('ExistenceOfSymptom',Clip(wob:FaultCode15))
                objXmlExport.WriteTag('OrderType','REPAIR')
                objXmlExport.WriteTag('ProductCode',Clip(job:ProductCode))
                objXmlExport.WriteTag('OriginalSerialNumber',Clip(job:ESN))
                objXmlExport.WriteTag('ClientBookingTimestamp',Format(Today(),@d05b) & ' ' & Format(Clock(),@t04b))
                If job:Warranty_Job = 'YES'
                    objXmlExport.WriteTag('WarrantyOption','001')
                Else ! If job:Warranty_Job = 'YES'
                    objXmlExport.WriteTag('WarrantyOption','002')
                End ! If job:Warranty_Job = 'YES'
                objXmlExport.WriteTag('WarrantyDatabaseDate',Format(job:DOP,@d05b) & ' ' & Format(Clock(),@t04b))
                objXmlExport.WriteTag('OwnershipCode','001')
                Found# = 0
                If job:Warranty_Job = 'YES'
                    If Instring('SOFTWARE',Upper(job:Repair_Type_Warranty),1,1)
                        objXmlExport.WriteTag('TransactionType','002')
                        Found# = 1
                    End ! If Instring('SOFTWARE',Upper(job:Warranty_Repair_Type),1,1)
                Else ! If job:Warranty_Job = 'YES'
                    If Instring('SOFTWARE',Upper(job:Repair_Type),1,1)
                        objXmlExport.WriteTag('TransactionType','002')
                        Found# = 1
                    End ! If Instring('SOFTWARE',Upper(job:Repair_Type),1,1)
                End ! If job:Warranty_Job = 'YES'
                If Found# = 0
                    objXmlExport.WriteTag('TransactionType','001')
                End ! If Found# = 0
            objXmlExport.CloseTag('Booking')
        End ! Loop x# = 1 To Records(tmp:JobsQueue)
        objXmlExport.CloseTag('ShipmentOrderRequest')
        objXMLExport.FClose()

    End ! If objXmlExport.FOpen(Filename.xml,1) = Level:Benign

    

! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020583'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Despatch_Units')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Relate:TRDBATCH_ALIAS.Open
  Relate:WEBJOB.Open
  Access:TRDBATCH.UseFile
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:TRDPARTY.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBTHIRD.UseFile
  Access:MANUFACT.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  Access:ESTPARTS.UseFile
  Access:LOCATLOG.UseFile
  Access:MANFAUPA.UseFile
  Access:MANFAULT.UseFile
  Access:JOBOUTFL.UseFile
  Access:MANFAULO.UseFile
  Access:MANFPALO.UseFile
  Access:STOCK.UseFile
  Access:JOBSE.UseFile
  Access:CONTHIST.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:JOBACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Despatch_Units')
  ?List{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,jac:Ref_Number_Key)
  BRW5.AddRange(jac:Ref_Number,tmp:JobNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,jac:Accessory,1,BRW5)
  BRW5.AddField(jac:Accessory,BRW5.Q.jac:Accessory)
  BRW5.AddField(jac:Ref_Number,BRW5.Q.jac:Ref_Number)
  IF ?tmp:beginbatch{Prop:Checked} = True
    UNHIDE(?tmp:CurrentBatchNumber)
    ENABLE(?Sheet3)
    ENABLE(?Sheet2)
    ENABLE(?Sheet4)
    DISABLE(?Sheet1)
    DISABLE(?company_name_temp:prompt)
    DISABLE(?Company_Name_temp)
  END
  IF ?tmp:beginbatch{Prop:Checked} = False
    DISABLE(?Sheet2)
    DISABLE(?Sheet3)
    DISABLE(?Sheet4)
  END
  IF ?tmp:addbatch{Prop:Checked} = True
    UNHIDE(?tmp:CurrentBatchNumber)
    ENABLE(?Sheet3)
    ENABLE(?Sheet2)
    ENABLE(?Sheet4)
    DISABLE(?Sheet1)
    DISABLE(?company_name_temp:prompt)
    DISABLE(?Company_Name_temp)
  END
  IF ?tmp:addbatch{Prop:Checked} = False
    DISABLE(?Sheet3)
    DISABLE(?Sheet4)
    DISABLE(?Sheet2)
  END
  ld.Init(Company_Name_temp,?Company_Name_temp,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  ld.Q &= Queue:FileDropCombo
  ld.AddSortOrder(trd:DeactivateCompanyKey)
  ld.AddRange(trd:Deactivate,tmp:False)
  ld.AddField(trd:Company_Name,ld.Q.trd:Company_Name)
  ld.AddField(trd:Account_Number,ld.Q.trd:Account_Number)
  ThisWindow.AddItem(ld.WindowComponent)
  ld.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW5.AskProcedure = 0
      CLEAR(BRW5.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
    Relate:TRDBATCH_ALIAS.Close
    Relate:WEBJOB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Despatch_Units')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ================================================================
  ! Initialise Xml Export Object
    objXmlExport.FInit(fileXmlExport)
  ! ================================================================
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:beginbatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:beginbatch, Accepted)
      IF tmp:beginbatch = 1
          Case Missive('You are about a create a new batch.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
      
                  Access:TRDPARTY.Clearkey(trd:Company_Name_Key)
                  trd:Company_Name    = Company_Name_Temp
                  If Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign
                      !Found
                      trd:Batch_Number += 1
                      tmp:CurrentBatchNumber = trd:Batch_Number
                      Do count_batch
                      Access:TRDPARTY.Update()
                  Else! If Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign
                      !Error
                      Case Missive('Cannot create a new batch number for this site.','ServiceBase 3g',|
                                     'mstop.jpg','/OK') 
                          Of 1 ! OK Button
                      End ! Case Missive
                      Post(Event:CloseWindow)
                  End! If Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign
      
      
              Of 1 ! No Button
                  tmp:beginbatch = 0
          End ! Case Missive
      End!IF tmp:beginbatch = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:beginbatch, Accepted)
    OF ?tmp:addbatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:addbatch, Accepted)
      If tmp:AddBatch = 1
          Case Missive('You are about a add to an existing batch.'&|
            '<13,10>'&|
            '<13,10>Are you sure?','ServiceBase 3g',|
                         'mquest.jpg','\No|/Yes') 
              Of 2 ! Yes Button
                  Error# = 0
                  Access:TRDBATCH.Clearkey(trb:Batch_Number_Key)
                  trb:Company_Name    = Company_Name_Temp
                  trb:Batch_Number    = Batch_Number_Temp
                  If Access:TRDBATCH.Tryfetch(trb:Batch_Number_Key) = Level:Benign
                      !Found
                      If trb:AuthorisationNo <> ''
                          Error# = 1
                          Case Missive('Cannot add to the selected batch.'&|
                            '<13,10>'&|
                            '<13,10>It has already been authorised.','ServiceBase 3g',|
                                         'mstop.jpg','/OK') 
                              Of 1 ! OK Button
                          End ! Case Missive
                      End!If trb:authorisationNo <> ''
      
                  Else! If Access:TRDBATCH.Tryfetch(trb:Batch_Number_Key) = Level:Benign
                      !Error
                      Case Missive('The selected batch does not exist.','ServiceBase 3g',|
                                     'mstop.jpg','/OK') 
                          Of 1 ! OK Button
                      End ! Case Missive
                      error# = 1
      
                  End! If Access:TRDBATCH.Tryfetch(trb:Batch_Number_Key) = Level:Benign
      
      
                  If error# = 1
                      Select(?Batch_Number_Temp)
                      tmp:AddBatch = 0
                  Else!f error# = 1
                      tmp:CurrentBatchNumber  = Batch_Number_Temp
      
                      Do Count_Batch
                  End!If error# = 1
      
              Of 1 ! No Button
                  tmp:AddBatch = 0
          End ! Case Missive
      End!IF tmp:beginbatch = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:addbatch, Accepted)
    OF ?Job_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Accepted)
            !TB12540 - Show contact history if there is a stick note
            !need to find one that has not been cancelled, and is valid for this request
            if glo:webjob = 0
                Access:conthist.clearkey(cht:KeyRefSticky)
                cht:Ref_Number    = deformat(Job_Number_Temp)         !was job:ref_number - TB12540 bounced 20/11/12
                cht:SN_StickyNote = 'Y'
                Set(cht:KeyRefSticky,cht:KeyRefSticky)      
                Loop
                    if access:Conthist.next() then break.
                    IF cht:Ref_Number <>  deformat(Job_Number_Temp) then break.
                    if cht:SN_StickyNote <> 'Y' then break.
                    if cht:SN_Completed <> 'Y' and cht:SN_Despatch = 'Y' then
                        glo:select12 =  Job_Number_Temp               !was job:ref_number
                        !Browse_Contact_History
                        Browse_Contact_hist2
                        BREAK
                    END
                END
            END !if glo:webjob is zero
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020583'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020583'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020583'&'0')
      ***
    OF ?tmp:beginbatch
      IF ?tmp:beginbatch{Prop:Checked} = True
        UNHIDE(?tmp:CurrentBatchNumber)
        ENABLE(?Sheet3)
        ENABLE(?Sheet2)
        ENABLE(?Sheet4)
        DISABLE(?Sheet1)
        DISABLE(?company_name_temp:prompt)
        DISABLE(?Company_Name_temp)
      END
      IF ?tmp:beginbatch{Prop:Checked} = False
        DISABLE(?Sheet2)
        DISABLE(?Sheet3)
        DISABLE(?Sheet4)
      END
      ThisWindow.Reset
    OF ?tmp:addbatch
      IF ?tmp:addbatch{Prop:Checked} = True
        UNHIDE(?tmp:CurrentBatchNumber)
        ENABLE(?Sheet3)
        ENABLE(?Sheet2)
        ENABLE(?Sheet4)
        DISABLE(?Sheet1)
        DISABLE(?company_name_temp:prompt)
        DISABLE(?Company_Name_temp)
      END
      IF ?tmp:addbatch{Prop:Checked} = False
        DISABLE(?Sheet3)
        DISABLE(?Sheet4)
        DISABLE(?Sheet2)
      END
      ThisWindow.Reset
    OF ?esn_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Accepted)
      IF LEN(CLIP(ESN_Temp)) = 18
        !Ericsson IMEI!
        ESN_Temp = SUB(ESN_Temp,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      Do FinalValidation
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Accepted)
    OF ?tmp:msn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Accepted)
      !Get Manufactuerere
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number = Job_Number_Temp
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          IF LEN(CLIP(tmp:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
            !Ericsson MSN!
            tmp:MSN = SUB(tmp:MSN,2,10)
            UPDATE()
          END
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      Do FinalValidation
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      Do XMLExport
      
      setcursor(cursor:wait)
      save_trb_id = access:trdbatch.savefile()
      access:trdbatch.clearkey(trb:batch_number_key)
      trb:company_name = ''
      trb:batch_number = 0
      set(trb:batch_number_key,trb:batch_number_key)
      loop
          if access:trdbatch.next()
             break
          end !if
          if trb:company_name <> ''      |
          or trb:batch_number <> 0      |
              then break.  ! end if
          Delete(trdbatch)
      end !loop
      access:trdbatch.restorefile(save_trb_id)
      setcursor()
      
      POst(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    OF ?Exchange
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Exchange, Accepted)
    If job:Exchange_Unit_Number <> ''
        Case Missive('This job already has an exchange unit attached. '&|
          '<13,10>'&|
          '<13,10>Please select a different option.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    End!If job:exchange_unit_number <> ''

    tmp:Exchanged = 1
    tmp:Type = 'DES'
    Do Despatch


      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Exchange, Accepted)
    OF ?accessories_retained
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessories_retained, Accepted)
    tmp:Exchanged = 0
    tmp:type    = 'RET'
    Do Despatch
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessories_retained, Accepted)
    OF ?Despatched
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Despatched, Accepted)
    tmp:Exchanged = 0
    tmp:Type = 'DES'
    Do Despatch
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Despatched, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?Job_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Selected)
      ?Job_Number_Temp{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Selected)
    OF ?tmp:msn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Selected)
      ?tmp:MSN{Prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              If ?Button_Group{prop:Disable} = 0
                  Post(Event:Accepted,?Exchange)
              End !If ?Exchange{prop:Hide} = 0
          Of F6Key
              If ?Button_Group{prop:Disable} = 0
                  Post(Event:Accepted,?Accessories_Retained)
              End !If ?Accessories_Retained{prop:Hide} = 0
          Of F7Key
              If ?Button_Group{prop:Disable} = 0
                  Post(Event:Accepted,?Despatched)
              End !If ?Despatched{prop:Hide} = 0
          Of F10Key
              Post(Event:Accepted,?Finish)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case f_Type
          Of 'NEW'
              Unhide(?new_tab)
              Post(Event:Accepted,?tmp:beginbatch)
          Of 'ADD'
              Unhide(?add_tab)
              Post(Event:accepted,?tmp:addbatch)
      End!Case f_Type
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(5, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  ! Insert --- Only show accessories that were sent to the ARC (DBH: 07/09/2009) #11056
  if (jac:Attached <> 1)
      return Record:Filtered
  end ! if (jac:Attached <> 1)
  ! end --- (DBH: 07/09/2009) #11056
  BRW5::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(5, ValidateRecord, (),BYTE)
  RETURN ReturnValue

Return_Units PROCEDURE (f:NMSNumber)                  !Generated from procedure template - Window

Local                CLASS
ExchangeUnitExist    Procedure(),Byte
                     END
ESN_temp             STRING(30)
tmp:IMEIChanged      BYTE(0)
tmp:MSNChanged       BYTE(0)
save_aud_id          USHORT,AUTO
save_exh_id          USHORT,AUTO
tmp:JobQueue         QUEUE,PRE(jobque)
JobNumber            LONG
Exchanged            STRING(3)
RecordNumber         LONG
BatchRecordNumber    LONG
                     END
tmp:JobNumber        LONG
tmp:ErrorText        STRING(60)
tmp:location         STRING(30)
tmp:status           STRING(30)
tmp:MSN              STRING(30)
tmp:Cost             REAL
tmp:Charge_t         STRING(30)
tmp:warranty_t       STRING(30)
tmp:FaultCode_t      STRING(30)
tmp:FaultCodeW_T     STRING(30)
tmp:COverwriteRepairType BYTE
tmp:WOverwriteRepairType BYTE
tmp:Network          STRING(30)
tmp:HubRepair        BYTE(0)
tmp:HubRepairDate    DATE
tmp:HubRepairTime    TIME
tmp:Claim            REAL
tmp:Handling         REAL
tmp:Exchange         REAL
tmp:IgnoreClaimCosts BYTE(0)
tmp:RRCCLabourCost   REAL
tmp:RRCCPartsCost    REAL
tmp:RRCWPartsCost    REAL
tmp:RRCWLabourCost   REAL
tmp:RRCELabourCost   REAL
tmp:RRCEPartsCost    REAL
tmp:OBFvalidated     BYTE
tmp:OBFvalidateDate  DATE
tmp:OBFvalidateTime  TIME
tmp:Despatched       STRING(3)
tmp:DespatchType     STRING(3)
tmp:CostVAT          REAL
tmp:InvoiceNumber    STRING(30)
tmp:ChaChargeType    STRING(30)
tmp:ChaRepairType    STRING(30)
tmp:WarChargeType    STRING(30)
tmp:WarRepairType    STRING(30)
tmp:3rdPartyMarkup   REAL
tmp:3rdPartyTotal    REAL
tmp:ReplaceOld       LONG
ExchangeHistoryQueue QUEUE,PRE(exhque)
Date                 DATE
Time                 TIME
User                 STRING(3)
Status               STRING(60)
                     END
tmp:WaybillNumber    STRING(30)
tmp:RepairType       STRING(30)
tmp:RejectedAmount   REAL
tmp:RejectedReason   STRING(255)
tmp:ThirdPartyInvoiceDate DATE
tmp:Manufacturer     STRING(30)
tmp:ModelNumber      STRING(30)
tmp:NewThirdPartySite STRING(30)
locAuditNotes        STRING(255)
window               WINDOW('Returning 3rd Party Repairs'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PROMPT('Returning 3rd Party Repairs'),AT(8,10),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,8,640,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,10),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(4,32,672,350),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Returns'),USE(?Tab1)
                           PROMPT('Insert the Job Number and  I.M.E.I. Number of the Unit being Returned.'),AT(8,38),USE(?Prompt3),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Jobs Updated'),AT(508,38),USE(?Prompt18),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Status'),AT(8,52),USE(?tmp:status:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(104,52,124,10),USE(tmp:status),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Status'),TIP('Status'),UPR
                           BUTTON,AT(232,48),USE(?LookupStatus),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           LIST,AT(508,52,144,282),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('69L(2)|M~Job Number~@s8@12L(2)|M~Exchanged~@s3@'),FROM(tmp:JobQueue)
                           PROMPT('Waybill Number'),AT(8,68),USE(?tmp:WaybillNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(104,68,124,10),USE(tmp:WaybillNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Waybill Number'),TIP('Waybill Number'),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,84),USE(?ESN_Temp:Prompt:2),TRN,HIDE,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Job Number'),AT(8,84),USE(?tmp:JobNumber:Prompt),FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(104,84,124,10),USE(ESN_temp,,?ESN_temp:2),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           ENTRY(@s8),AT(104,84,64,10),USE(tmp:JobNumber),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           GROUP,AT(4,92,256,248),USE(?JobDetailsGroup),HIDE
                             PROMPT('New Third Party Site'),AT(8,98),USE(?Prompt:NewThirdPartySite),HIDE,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,98,124,10),USE(tmp:NewThirdPartySite),HIDE,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             BUTTON,AT(232,94),USE(?LookupThirdParty),TRN,FLAT,HIDE,ICON('lookupp.jpg')
                             PROMPT('Job Number'),AT(8,114),USE(?tmp:JobNumber:Prompt:2),HIDE,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s8),AT(104,114,64,10),USE(tmp:JobNumber,,?tmp:JobNumber:2),SKIP,HIDE,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                             PROMPT('I.M.E.I. Number'),AT(8,114),USE(?ESN_Temp:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,114,124,10),USE(ESN_temp),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                             PROMPT('M.S.N.'),AT(8,128),USE(?tmp:MSN:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,130,124,10),USE(tmp:MSN),HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                             PROMPT('Manufacturer'),AT(8,146),USE(?tmp:Manufacturer:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,146,124,10),USE(tmp:Manufacturer),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Manufacturer'),TIP('Manufacturer'),UPR
                             BUTTON,AT(232,142),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Model Number'),AT(8,168),USE(?tmp:ModelNumber:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,168,124,10),USE(tmp:ModelNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Model Number'),TIP('Model Number'),UPR
                             BUTTON,AT(232,164),USE(?CallLookup:3),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Repair Type'),AT(8,188),USE(?tmp:RepairType:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,188,124,10),USE(tmp:RepairType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Third Party Repair Type'),TIP('Third Party Repair Type'),UPR
                             BUTTON,AT(232,184),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('Rejected Amount'),AT(8,206),USE(?tmp:RejectedAmount:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n-14.2),AT(104,206,64,10),USE(tmp:RejectedAmount),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Rejected Amount'),TIP('Rejected Amount'),UPR
                             PROMPT('Rejected Reason'),AT(8,220),USE(?tmp:RejectedReason:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             TEXT,AT(104,220,124,34),USE(tmp:RejectedReason),VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('Rejected Reason'),TIP('Rejected Reason'),UPR
                             PROMPT('3rd Party Charge'),AT(8,258),USE(?tmp:Cost:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(104,258,64,10),USE(tmp:Cost),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Cost'),TIP('Cost'),REQ,UPR
                             PROMPT('3rd Party V.A.T.'),AT(8,270),USE(?tmp:CostVAT:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(104,270,64,10),USE(tmp:CostVAT),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('V.A.T.'),TIP('V.A.T.'),UPR,READONLY
                             PROMPT('3rd Party Total'),AT(8,282),USE(?tmp:3rdPartyTotal:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(104,282,64,10),USE(tmp:3rdPartyTotal),SKIP,RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('3rd Party Total'),TIP('3rd Party Total'),UPR,READONLY
                             PROMPT('3rd Party Invoice No'),AT(8,298),USE(?tmp:InvoiceNumber:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@s30),AT(104,298,124,10),USE(tmp:InvoiceNumber),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('3rd Party Invoice No'),TIP('3rd Party Invoice No'),REQ,UPR
                             PROMPT('3rd Party Invoice Date'),AT(8,314),USE(?tmp:ThirdPartyInvoiceDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@d6),AT(104,314,64,10),USE(tmp:ThirdPartyInvoiceDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('3rd Party Invoice Date'),TIP('3rd Party Invoice Date'),REQ,UPR
                             BUTTON,AT(172,310),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                             PROMPT('3rd Party Markup'),AT(8,330),USE(?tmp:3rdPartyMarkup:Prompt),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             ENTRY(@n14.2),AT(104,330,64,10),USE(tmp:3rdPartyMarkup),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('3rd Party Markup'),TIP('3rd Party Markup'),UPR
                             BUTTON,AT(276,198),USE(?ViewJob),TRN,FLAT,LEFT,ICON('viewjobp.jpg')
                             GROUP('Warranty Types'),AT(276,134,212,38),USE(?WarrantyGroup),BOXED,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               PROMPT('Charge Type:'),AT(280,144),USE(?tmp:WarChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(352,144,124,10),USE(tmp:WarChargeType),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Charge Type'),TIP('Charge Type'),UPR,READONLY
                               PROMPT('Repair Type:'),AT(280,158),USE(?tmp:WarRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(352,158,124,10),USE(tmp:WarRepairType),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Repair Type'),TIP('Repair Type'),UPR,READONLY
                             END
                             GROUP('Chargeable Types'),AT(276,94,212,38),USE(?ChargeableGroup),BOXED,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(352,104,124,10),USE(tmp:ChaChargeType),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Charge Type'),TIP('Charge Type'),UPR,READONLY
                               PROMPT('Charge Type:'),AT(280,104),USE(?tmp:ChaChargeType:Prompt),TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               ENTRY(@s30),AT(352,118,124,10),USE(tmp:ChaRepairType),SKIP,TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Repair Type'),TIP('Repair Type'),UPR,READONLY
                               PROMPT('Repair Type:'),AT(280,118),USE(?tmp:ChaRepairType:Prompt),TRN,LEFT,FONT('Tahoma',8,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             END
                           END
                           BUTTON,AT(276,314),USE(?NextButton),TRN,FLAT,LEFT,ICON('nextp.jpg')
                           STRING(@s60),AT(8,356,200,12),USE(tmp:ErrorText),FONT(,10,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(608,384),USE(?Cancel),TRN,FLAT,LEFT,ICON('stopp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tmp:status                Like(tmp:status)
look:tmp:NewThirdPartySite                Like(tmp:NewThirdPartySite)
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:ModelNumber                Like(tmp:ModelNumber)
look:tmp:RepairType                Like(tmp:RepairType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CheckHubTicked               Routine
   ! Workaround to make sure hub Repair is still ticked (L831)
   Access:JOBSE.Clearkey(jobe:RefNumberKey)
   jobe:RefNumber  = job:Ref_Number
   If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
       ! Found
       If ~jobe:HubRepair
           jobe:HubRepair = 1
           Access:JOBSE.Update()
       End ! If ~jobe:HubRepair

        ! Just incase the dates also aren't filled in, go through the
        ! audit trail and try and find the last time the hub repair
        ! was ticked
       If jobe:HubRepairDate = 0
           Save_aud_ID = Access:AUDIT.SaveFile()
           Access:AUDIT.ClearKey(aud:Action_Key)
           aud:Ref_Number = job:Ref_Number
           aud:Action     = 'JOB UPDATED'
           Set(aud:Action_Key, aud:Action_Key)
           Loop
               If Access:AUDIT.NEXT()
                   Break
               End ! If
               If aud:Ref_Number <> job:Ref_Number    |
                   Or aud:Action     <> 'JOB UPDATED' |
                   Then Break   ! End If
               End ! If
                Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                aud2:AUDRecordNumber = aud:Record_Number
                IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                   If Instring('HUB REPAIR SELECTED', aud2:Notes, 1, 1)
                       jobe:HubRepairDate = aud:Date
                       jobe:HubRepairTime = aud:Time
                       Access:JOBSE.Update
                       Break
                   End ! If Instring('HUB REPAIR SELECTED',aud2:Notes,1,1)

                END ! IF
           End ! Loop
           Access:AUDIT.RestoreFile(Save_aud_ID)
       End ! If jobe:HubRepairDate = 0
   Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
   ! Error
   End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
FinalRoutine                 Routine


    Access:TRDBATCH.Clearkey(trb:JobStatusKey)
    trb:Status     = 'OUT'
    trb:Ref_Number = tmp:JobNumber
    If Access:TRDBATCH.Tryfetch(trb:JobStatusKey) = Level:Benign
        ! Found

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = tmp:JobNumber
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Found
            Error#   = 0
            If JobInUse(job:Ref_Number,1) = 1
                Error# = 1
            End ! If JobInUse(job:Ref_Number,1)

            tmp:IMEIChanged = 0
            tmp:MSNChanged  = 0
            tmp:ReplaceOld  = 0

            If Error# = 0
                If AccessoryCheck('JOB')
                    If AccessoryMismatch(job:Ref_Number,'JOB')
                        Error# = 1
                    End ! If AccessoryMismatch(job:Ref_Number,'JOB')
                End ! If AccessoryCheck('JOB')

            End ! If Error# = 0

            If Error# = 0
                ! Save which accessories where actually returned - TrkBs: 6264 (DBH: 09-09-2005)

                ! Delete any accessories that have already been recorded. I don't see how the same batch would be returned twice - TrkBs: 6264 (DBH: 09-09-2005)
                Access:TRDACC.ClearKey(trr:AccessoryKey)
                trr:RefNumber = trb:RecordNumber
                Set(trr:AccessoryKey,trr:AccessoryKey)
                Loop
                    If Access:TRDACC.NEXT()
                       Break
                    End !If
                    If trr:RefNumber <> trb:RecordNumber      |
                        Then Break.  ! End If
                    Relate:TRDACC.Delete(0)
                End !Loop

                ! The temp queue holds the accessories that were tagged - TrkBs: 6264 (DBH: 09-09-2005)
                Loop acc# = 1 To Records(glo:Queue)
                    Get(glo:Queue,acc#)
                    If Access:TRDACC.PrimeRecord() = Level:Benign
                        trr:RefNumber = trb:RecordNumber
                        trr:Accessory = glo:Pointer
                        If Access:TRDACC.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:TRDACC.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:TRDACC.CancelAutoInc()
                        End ! If Access:TRDACC.TryInsert() = Level:Benign
                    End !If Access:TRDACC.PrimeRecord() = Level:Benign
                End ! Loop acc# = 1 To Records(glo:Queue)

                ! Has the ESN changed from despatch
                If ESN_Temp <> trb:ESN
                    ! Is MSN required for this manufacturer
                    If MSNRequired(job:Manufacturer) = Level:Benign
                        If ?tmp:MSN{prop:Hide} = 0
                            ! MSN Is visible, so this question has already been asked
                            If tmp:MSN <> trb:MSN
                                Case Local.ExchangeUnitExist()
                                Of 1
                                    Error#   = 1
                                    ESN_Temp = ''
                                    Select(?ESN_Temp)
                                Of 2
                                    tmp:MSNChanged  = 1
                                    tmp:IMEIChanged = 1
                                    tmp:ReplaceOld  = xch:Ref_Number
                                Of 3
                                    Case Missive('The I.M.E.I. number and the M.S.N. are different to the unit that was initial despatched.' &       |
                                                 '<13,10>Do you wish to CHANGE the I.M.E.I. number and M.S.N., or RE-ENTER them?', 'ServiceBase 3g', |
                                                 'mquest.jpg', '\Re-Enter|/Change')
                                    Of 2 ! Change Button
                                        tmp:MSNChanged  = 1
                                        tmp:IMEIChanged = 1
                                    Of 1 ! Re-Enter Button
                                        Error#   = 1
                                        ESN_Temp = ''
                                        Select(?ESN_Temp)
                                    End ! Case Missive
                                End ! Case Local.ExchangeUnitExist()
                            Else ! If tmp:MSN <> trb:MSN
                                Case Missive('The I.M.E.I. number of the unit is different to the unit that was initially despatched.' &|
                                             '<13,10>(Note: The M.S.N. is the same)' &|
                                             '<13,10>' &|
                                             '<13,10>Do you want to CHANGE the I.M.E.I. number or RE-ENTER it?', 'ServiceBase 3g', |
                                             'mquest.jpg', '\Re-Enter|/Change')
                                Of 2 ! Change Button
                                    tmp:IMEIChanged = 1
                                Of 1 ! Re-Enter Button
                                    Error#   = 1
                                    ESN_Temp = ''
                                    Select(?ESN_Temp)
                                    ?tmp:MSN{prop:Hide}        = 1
                                    ?tmp:MSN:Prompt{prop:Hide} = 1

                                End ! Case Missive
                            End ! If tmp:MSN <> trb:MSN
                        Else ! If ?tmp:MSN{prop:Hide} = 0
                            Case Missive('The I.M.E.I. number is different to the unit that was initially despatched. ' &|
                                         '<13,10>' &|
                                         '<13,10>Do you wish to CHANGE the I.M.E.I. number and M.S.N., or RE-ENTER them?', 'ServiceBase 3g', |
                                         'mquest.jpg', '\Re-Enter|/Change')
                            Of 2 ! Change Button
                                ! Ok, show the MSN for them to change
                                tmp:IMEIChanged = 1
                                ?tmp:MSN{prop:Hide}        = 0
                                ?tmp:MSN:Prompt{prop:Hide} = 0
                                tmp:MSN                    = trb:MSN
                                Select(?tmp:MSN)
                                Post(Event:Selected, ?tmp:MSN)
                                Error# = 1
                            Of 1 ! Re-Enter Button
                                Error#                     = 1
                                ?tmp:MSN{prop:Hide}        = 1
                                ?tmp:MSN:Prompt{prop:Hide} = 1
                                ESN_Temp                   = ''
                                Select(?ESN_Temp)
                            End ! Case Missive
                        End ! If ?tmp:MSN{prop:Hide} = 0
                    Else ! If MSNRequired(job:Manufacturer)
                        Case Local.ExchangeUnitExist()
                        Of 1
                            Do Fail_Update
                            Error#   = 1
                            ESN_Temp = ''
                            Select(?ESN_Temp)
                            ?tmp:MSN{prop:Touched} = 1
                        Of 2
                            tmp:IMEIChanged = 1
                            tmp:ReplaceOld  = xch:Ref_Number
                        Of 3
                            Case Missive('The I.M.E.I. number of the unit is different to the unit that was initially despatched.' &|
                                         '<13,10>(Note: The M.S.N. is the same)' &|
                                         '<13,10>' &|
                                         '<13,10>Do you want to CHANGE the I.M.E.I. number or RE-ENTER it?', 'ServiceBase 3g', |
                                         'mquest.jpg', '\Re-Enter|/Change')
                            Of 2 ! Change Button
                                tmp:IMEIChanged = 1
                            Of 1 ! Re-Enter Button
                                Do Fail_Update
                                Error#   = 1
                                ESN_Temp = ''
                                Select(?ESN_Temp)
                                ?tmp:MSN{Prop:Touched} = 1
                            End ! Case Missive
                        End ! Case Local.ExchangeUnitExist()
                    End ! If MSNRequired(job:Manufacturer)
                Else ! !If ESN_Temp <> trb:ESN
                    tmp:MSN = trb:MSN
                End ! If ESN_Temp <> trb:ESN
            End ! If Error# = 0

            If Error# = 0
                Access:LOCINTER.Clearkey(loi:Location_Key)
                loi:Location    = tmp:Location
                If Access:LOCINTER.Tryfetch(loi:Location_Key) = Level:Benign and tmp:Location <> ''
                    If loi:Allocate_Spaces = 'YES'
                        If loi:Current_Spaces < 1
                            Case Missive('There are no available locations. Please pick another.', 'ServiceBase 3g', |
                                         'mstop.jpg', '/OK')
                            Of 1 ! OK Button
                            End ! Case Missive
                            error# = 1
                            Do fail_update
                        End! If loi:current_spaces < 1
                    End! If loi:allocate_spaces = 'YES'
                End! If access:locinter(loi:location_key) = Level:Benign
            End ! If Error# = 0

            IF error# = 0
                ! Exchange Unit attached, or there will be an exchange unit at some time
                No_Audit#      = 0
                ExchangeFound# = 0
                If trb:Exchanged = 'YES' and job:Exchange_Unit_Number = ''
                     ! An Exchange Unit hasn't been attached yet, but
                     ! should be at some point. There should already be an entry in the
                     ! Exchange Stock for this. Now I've got to try and find it.
                    Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
                    xch_ali:ESN = job:ESN
                    If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
                        ExchangeFound# = 1
                        If xch_ali:audit_number <> ''
                            no_audit# = 0
                        Else! If xch_ali:audit_number <> ''
                            ! Get Exchange Unit and mark as 'Return To Stock'
                            no_audit# = 1
                        End! If xch_ali:audit_number <> ''
                    End ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                End ! If trb:Exchanged = 'YES' and job:Exchange_Unit_Number = ''
                If (job:exchange_unit_number <> '' Or trb:exchanged = 'YES') And ExchangeFound# = 0
                    ! Get Exchange Unit to see if it has an audit number attached
                    print_label# = 0
                    access:exchange_alias.clearkey(xch_ali:ref_number_key)
                    xch_ali:ref_number = job:exchange_unit_number
                    if access:exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign and job:exchange_unit_number <> ''
                        ExchangeFound# = 1
                        If xch_ali:audit_number <> ''
                            no_audit# = 0
                        Else! If xch_ali:audit_number <> ''
                            ! Get Exchange Unit and mark as 'Return To Stock'
                            no_audit# = 1
                        End! If xch_ali:audit_number <> ''
                    Else! if access:exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
                        no_audit# = 1
                    end! if access:exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
                End! If job:exchange_unit_number <> '' or trb:exchanged = 'YES'

                If ExchangeFound# = 1
                    If no_audit# = 0
                        access:excaudit.clearkey(exa:audit_number_key)
                        exa:stock_type   = xch_ali:stock_type
                        exa:audit_number = xch_ali:audit_number
                        If access:excaudit.tryfetch(exa:audit_number_key) = Level:benign
                            ! Remove Stock Unit From Audit Number
                            access:exchange.clearkey(xch:ref_number_key)
                            xch:ref_number  = exa:stock_unit_number
                            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                                get(exchhist, 0)
                                if access:exchhist.primerecord() = level:benign
                                    exh:ref_number = xch:ref_number
                                    exh:date       = today()
                                    exh:time       = clock()
                                    access:users.clearkey(use:password_key)
                                    use:password        = glo:password
                                    access:users.fetch(use:password_key)
                                    exh:user   = use:user_code
                                    exh:status = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
                                    access:exchhist.insert()
                                end! if access:exchhist.primerecord() = level:benign
                                xch:audit_number    = ''
                                access:exchange.update()
                            End! If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                            ! Get Replacement Unit
                            access:exchange.clearkey(xch:ref_number_key)
                            xch:ref_number  = exa:replacement_unit_number
                            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                                get(exchhist, 0)
                                if access:exchhist.primerecord() = level:benign
                                    exh:ref_number = xch:ref_number
                                    exh:date       = today()
                                    exh:time       = clock()
                                    access:users.clearkey(use:password_key)
                                    use:password        = glo:password
                                    access:users.fetch(use:password_key)
                                    exh:user   = use:user_code
                                    exh:status = 'UNIT RETURNED. (AUDIT:' & |
                                    Clip(exa:audit_number)
                                    If tmp:IMEIChanged
                                        exh:status  = CLip(exh:status) & ' OLD I.M.E.I.: ' & Clip(xch:esn)
                                    End ! If tmp:ESNChanged
                                    If tmp:MSNChanged
                                        exh:Status  = Clip(exh:Status) & ' OLD M.S.N.: ' & Clip(xch:MSN)
                                    End ! If tmp:MSNChanged
                                    access:exchhist.insert()
                                end! if access:exchhist.primerecord() = level:benign
                                xch:audit_number = exa:audit_number
                                xch:available    = 'RTS'
                                xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                                glo:select1      = xch:Ref_number
                                IF tmp:IMEIChanged
                                    xch:ESN         = ESN_Temp
                                END ! IF tmp:IMEIChanged
                                If tmp:MSNChanged
                                    xch:MSN         = tmp:MSN
                                End ! If tmp:MSNChanged
                                access:exchange.update()
                                print_label# = 1
                            End! If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        End! If access:excaudit.tryfetch(exa:audit_number_key) = Level:benign
                    Else! If no_audit# = 0
                        ! Get The Incoming Customer Unit and mark
                        Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                        xch:ESN = job:esn
                        If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:benign
                            get(exchhist, 0)
                            if access:exchhist.primerecord() = level:benign
                                exh:ref_number = xch:ref_number
                                exh:date       = today()
                                exh:time       = clock()
                                access:users.clearkey(use:password_key)
                                use:password        = glo:password
                                access:users.fetch(use:password_key)
                                exh:user   = use:user_code
                                exh:status = 'UNIT RETURNED.'
                                If tmp:IMEIChanged
                                    exh:status      = Clip(exh:status) & ' OLD I.M.E.I. : ' & Clip(xch:esn)
                                End! If esn_change# = 1
                                If tmp:MSNChanged
                                    exh:Status      = Clip(exh:Status) & ' OLD M.S.N. : ' & Clip(xch:msn)
                                End ! If tmp:MSNChanged
                                access:exchhist.insert()
                            end! if access:exchhist.primerecord() = level:benign
                            xch:available = 'RTS'
                            xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
                            glo:select1   = xch:ref_number
                            If tmp:IMEIChanged
                                xch:esn = esn_temp
                            End ! If tmp:IMEIChanged
                            If tmp:MSNChanged
                                xch:MSN = tmp:MSN
                            End ! If tmp:MSNChanged
                            access:exchange.update()
                            print_label# = 1

                            If tmp:ReplaceOld <> 0
                                 ! Get the old unit, save it's notes then
                                 ! add the notes to the new unit.
                                 ! Write an entry in the audit trail - L883 (DBH: 11-09-2003)
                                Access:EXCHANGE_ALIAS.Clearkey(xch_ali:Ref_Number_Key)
                                xch_ali:Ref_Number  = tmp:ReplaceOld
                                If Access:EXCHANGE_ALIAS.Tryfetch(xch_ali:Ref_Number_Key) = Level:Benign
                                    ! Found
                                    Clear(ExchangeHistoryQueue)
                                    Free(ExchangeHistoryQueue)
                                    Save_exh_ID = Access:EXCHHIST.SaveFile()
                                    Access:EXCHHIST.ClearKey(exh:Ref_Number_Key)
                                    exh:Ref_Number = xch_ali:Ref_Number
                                    Set(exh:Ref_Number_Key, exh:Ref_Number_Key)
                                    Loop
                                        If Access:EXCHHIST.NEXT()
                                            Break
                                        End ! If
                                        If exh:Ref_Number <> xch_ali:Ref_Number      |
                                            Then Break   ! End If
                                        End ! If
                                        exhque:Date   = exh:Date
                                        exhque:Time   = exh:Time
                                        exhque:User   = exh:User
                                        exhque:Status = exh:Status
                                        Add(ExchangeHistoryQueue)
                                    End ! Loop
                                    Access:EXCHHIST.RestoreFile(Save_exh_ID)

                                    Loop x# = 1 To Records(ExchangeHistoryQueue)
                                        Get(ExchangeHistoryQueue, x#)
                                        if access:exchhist.primerecord() = level:benign
                                            exh:ref_number = xch:ref_number
                                            exh:date       = exhque:Date
                                            exh:time       = exhque:Time
                                            exh:user       = exhque:User
                                            exh:status     = exhque:Status
                                            access:exchhist.insert()
                                        end! if access:exchhist.primerecord() = level:benign
                                    End ! Loop x# = 1 To Records(ExchangeHistoryQueue)
                                    if access:exchhist.primerecord() = level:benign
                                        exh:ref_number = xch:ref_number
                                        exh:date       = today()
                                        exh:time       = clock()
                                        access:users.clearkey(use:password_key)
                                        use:password        = glo:password
                                        access:users.fetch(use:password_key)
                                        exh:user   = use:user_code
                                        exh:status = 'REPLACED UNIT: ' & tmp:ReplaceOld
                                        access:exchhist.insert()
                                    end! if access:exchhist.primerecord() = level:benign
                                    ! Now Delete The Original Unit
                                    Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                    xch:Ref_Number  = tmp:ReplaceOld
                                    If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                        ! Found
                                        Relate:EXCHANGE.Delete(0)
                                    Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                    ! Error
                                    End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                Else ! If Access:EXCHANGE_ALIAS.Tryfetch(xch_ali:Ref_Number_Key) = Level:Benign
                                ! Error
                                End ! If Access:EXCHANGE_ALIAS.Tryfetch(xch_ali:Ref_Number_Key) = Level:Benign
                            End ! If tmp:ReplaceOld <> 0

                        End ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:benign
                    End! If no_audit# = 1
                    If print_label# = 1
                        Set(defaults)
                        access:defaults.next()

                        If GETINI('PRINTING', 'ThirdPartyReturnsLabel',, CLIP(PATH()) & '\SB2KDEF.INI') = 1
                            glo:select1 = job:ref_number
                            Thermal_Labels('3RD')
                            glo:select1 = ''
                        End! If def:use_job_label = 'YES'
                    End! If print_label# = 1
                    job:workshop  = 'YES'
                    job:date_paid = Today()             ! Use this field to Date Returned
                    GetStatus(Sub(tmp:status, 1, 3), 1, 'JOB')

                    locAuditNotes         = '3RD PARTY AGENT: ' & Clip(trb:company_name) & |
                    '<13,10>BATCH NUMBER: ' & Clip(trb:batch_number)
                    If tmp:location <> ''
                        locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOCATION: ' & Clip(tmp:location)
                    End! If tmp:location <> ''

                    If tmp:IMEIChanged
                        locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>NEW I.M.E.I. NO.: ' & Clip(esn_temp) & |
                        '<13,10>PREVIOUS I.M.E.I. NO.: ' & Clip(job:esn)
                    End! If esn_change# = 1
                    If tmp:MSNChanged
                        locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>NEW M.S.N.: ' & Clip(tmp:MSN) & |
                        '<13,10>PREVIOUS M.S.N.: ' & Clip(job:MSN)
                    End ! If tmp:MSNChanged

                    IF (AddToAudit(job:ref_number,'JOB','3RD PARTY AGENT: UNIT RETURNED',locAuditNotes))
                    END ! IF

                    If tmp:location <> ''
                        access:locinter.clearkey(loi:location_key)
                        loi:location    = tmp:location
                        If access:locinter.tryfetch(loi:location_key) = Level:Benign
                            If loi:allocate_spaces = 'YES'
                                loi:current_spaces -= 1
                                If loi:current_spaces < 1
                                    loi:current_spaces     = 0
                                    loi:location_available = 'NO'
                                End! If loi:current_spaces < 1
                                access:locinter.update()
                            End! If loi:allocate_spaces = 'YES'
                        End! If access:locinter(loi:location_key) = Level:Benign
                        job:location    = tmp:location
                    End! If tmp:location <> ''

                    Do ThirdParty

                    If tmp:IMEIChanged
                        ! #11609 Record IMEI Change in the OutFaults List (DBH: 12/08/2010)
                        if (Access:JOBOUTFL.PrimeRecord() = Level:Benign)
                            joo:JobNumber = job:Ref_Number
                            joo:FaultCode = 0
                            joo:Description = 'IMEI ' & clip(job:ESN) & ' EXCHANGED FOR ' & (ESN_Temp)
                            joo:Level = 0
                            If (Access:JOBOUTFL.TryInsert())
                                Access:JOBOUTFL.CancelAutoInc()
                            End
                        end ! if (Access:JOBOUTFL.PrimeRecord() = Level:Benign)

                        job:esn = esn_temp
                    End! If esn_change# = 1

                    If tmp:MSNChanged
                        job:MSN = tmp:MSN
                    End ! If tmp:MSNChanged

                    ! Write 3rd party Cost
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        ! Found
                        jobe:ARC3rdPartyCost          = tmp:Cost
                        jobe:ARC3rdPartyVAT           = tmp:CostVAT
                        jobe:ARC3rdPartyInvoiceNumber = tmp:InvoiceNumber
                        jobe:ARC3rdPartyMarkup        = tmp:3rdPartyMarkup
                        ! Start - New fields - TrkBs: 5110 (DBH: 19-05-2005)
                        jobe:ARC3rdPartyInvoiceDate     = tmp:ThirdPartyInvoiceDate
                        jobe:ARC3rdPartyWaybillNo       = tmp:WaybillNumber
                        jobe:ARC3rdPartyRepairType      = tmp:RepairType
                        jobe:ARC3rdPartyRejectedReason  = tmp:RejectedReason
                        jobe:ARC3rdPartyRejectedAmount  = tmp:RejectedAmount
                        ! End   - New fields - TrkBs: 5110 (DBH: 19-05-2005)
                        Access:JOBSE.TryUpdate()
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Error
                    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                    ! Change to back to at ARC
                    LocationChange(Clip(GETINI('RRC', 'ARCLocation',, CLIP(PATH()) & '\SB2KDEF.INI')))

                    AddToConsignmentHistory(job:Ref_Number, '3RD PARTY', 'ARC', job:Manufacturer, 'BATCH: ' & Clip(trb:Batch_Number), 'JOB')

                    access:jobs.update()

                    Do CheckHubTicked

                    ! Check to see if accessories where retained or kept
                    glo:select2 = 'DES'
                    setcursor(cursor:wait)
                    save_aud_id = access:audit.savefile()
                    access:audit.clearkey(aud:action_key)
                    aud:ref_number = job:ref_number
                    aud:action     = '3RD PARTY AGENT: UNIT SENT'
                    set(aud:action_key, aud:action_key)
                    loop
                        if access:audit.next()
                            break
                        end ! if
                        if aud:ref_number <> job:ref_number                   |
                            or aud:action     <> '3RD PARTY AGENT: UNIT SENT' |
                            then break   ! end if
                        end ! if
                        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                        aud2:AUDRecordNumber = aud:Record_Number
                        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                            If Instring('ACCESSORIES RETAINED', aud2:notes, 1, 1)
                                glo:select2 = 'RET'
                                Break
                            End! If Instring('ACCESSORIES RETAINED',aud:notes,1,1)
                        END ! IF

                    end ! loop
                    access:audit.restorefile(save_aud_id)
                    setcursor()
                    DO pass_update
    ! _____________________________________________________________________

                Else! If ExchangeFound# = 1
                    ! Unit Not Exchanged

                        locAuditNotes         = '3RD PARTY AGENT: ' & Clip(trb:company_name) & |
                        '<13,10>BATCH NUMBER: ' & Clip(trb:batch_number)
                        If tmp:location <> ''
                            locAuditNotes   = Clip(locAuditNotes) & '<13,10>LOCATION: ' & Clip(tmp:location)
                        End! If tmp:location <> ''
                        If tmp:IMEIChanged
                            locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>NEW I.M.E.I. NO.: ' & Clip(esn_temp) & |
                            '<13,10>PREVIOUS I.M.E.I. NO.: ' & Clip(job:esn)
                        End! If esn_change# = 1
                        If tmp:MSNChanged
                            locAuditNotes       = Clip(locAuditNotes) & '<13,10,13,10>NEW M.S.N.: ' & Clip(tmp:MSN) & |
                            '<13,10>PREVIOUS M.S.N.: ' & Clip(job:MSN)
                        End ! If tmp:MSNChanged

                        IF (AddToAudit(job:ref_number,'JOB','3RD PARTY AGENT: UNIT RETURNED',locAuditNotes))
                        END ! IF


                    Do ThirdParty

                    If tmp:IMEIChanged
                        ! #11609 Record IMEI Change in the OutFaults List (DBH: 12/08/2010)
                        if (Access:JOBOUTFL.PrimeRecord() = Level:Benign)
                            joo:JobNumber = job:Ref_Number
                            joo:FaultCode = 0
                            joo:Description = 'IMEI ' & clip(job:ESN) & ' EXCHANGED FOR ' & (ESN_Temp)
                            joo:Level = 0
                            If (Access:JOBOUTFL.TryInsert())
                                Access:JOBOUTFL.CancelAutoInc()
                            End
                        end ! if (Access:JOBOUTFL.PrimeRecord() = Level:Benign)

                        job:esn = esn_temp
                    End! If esn_change# = 1
                    If tmp:MSNChanged
                        job:MSN = tmp:MSN
                    End ! If tmp:MSNChanged
                    job:workshop  = 'YES'
                    job:date_paid = Today()             ! Use this field to Date Returned
                    GetStatus(Sub(tmp:status, 1, 3), 1, 'JOB') ! QA Required

                    ! Write 3rd party Cost
                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        ! Found
                        jobe:ARC3rdPartyCost          = tmp:Cost
                        jobe:ARC3rdPartyVAT           = tmp:CostVAT
                        jobe:ARC3rdPartyInvoiceNumber = tmp:InvoiceNumber
                        jobe:ARC3rdPartyMarkUp        = tmp:3rdPartyMarkup
                        ! Start - New fields - TrkBs: 5110 (DBH: 19-05-2005)
                        jobe:ARC3rdPartyInvoiceDate     = tmp:ThirdPartyInvoiceDate
                        jobe:ARC3rdPartyWaybillNo       = tmp:WaybillNumber
                        jobe:ARC3rdPartyRepairType      = tmp:RepairType
                        jobe:ARC3rdPartyRejectedReason  = tmp:RejectedReason
                        jobe:ARC3rdPartyRejectedAmount  = tmp:RejectedAmount
                        ! End   - New fields - TrkBs: 5110 (DBH: 19-05-2005)
                        Access:JOBSE.TryUpdate()
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Error
                    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                    ! Change to back to at ARC
                    LocationChange(Clip(GETINI('RRC', 'ARCLocation',, CLIP(PATH()) & '\SB2KDEF.INI')))
                    AddToConsignmentHistory(job:Ref_Number, '3RD PARTY', 'ARC', job:Manufacturer, 'BATCH: ' & Clip(trb:Batch_Number), 'JOB')
                    access:jobs.update()

                    Do CheckHubTicked


                    If tmp:location <> ''
                        access:locinter.clearkey(loi:location_key)
                        loi:location    = tmp:location
                        If access:locinter.tryfetch(loi:location_key) = Level:Benign
                            If loi:allocate_spaces = 'YES'
                                loi:current_spaces -= 1
                                If loi:current_spaces < 1
                                    loi:current_spaces     = 0
                                    loi:location_available = 'NO'
                                End! If loi:current_spaces < 1
                                access:locinter.update()
                            End! If loi:allocate_spaces = 'YES'
                        End! If access:locinter(loi:location_key) = Level:Benign
                        job:location    = tmp:location
                    End! If tmp:location <> ''
                    access:jobs.update()

                    ! Check to see if accessories where retained or kept
                    glo:select2 = 'DES'
                    setcursor(cursor:wait)
                    save_aud_id = access:audit.savefile()
                    access:audit.clearkey(aud:action_key)
                    aud:ref_number = job:ref_number
                    aud:action     = '3RD PARTY AGENT: UNIT SENT'
                    set(aud:action_key, aud:action_key)
                    loop
                        if access:audit.next()
                            break
                        end ! if
                        if aud:ref_number <> job:ref_number                   |
                            or aud:action     <> '3RD PARTY AGENT: UNIT SENT' |
                            then break   ! end if
                        end ! if
                        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
                        aud2:AUDRecordNumber = aud:Record_Number
                        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
                            If Instring('ACCESSORIES RETAINED', aud2:notes, 1, 1)
                                glo:select2 = 'RET'
                                Break
                            End! If Instring('ACCESSORIES RETAINED',aud:notes,1,1)
                        END ! IF

                    end ! loop
                    access:audit.restorefile(save_aud_id)
                    setcursor()

                    IF GETINI('PRINTING', 'ThirdPartyReturnsLabel',, CLIP(PATH()) & '\SB2KDEF.INI') = 1
                        glo:select1 = job:ref_number
                        Thermal_Labels('3RD')
                    END! IF def:use_job_label = 'YES'
                    glo:select1 = ''
                    glo:select2 = ''
                    Do pass_update
                End! If job:exchange_unit_number <> '' or trb:exchanged = 'YES'
            END ! If Error# = 0
        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        ! Error
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
    Else ! If Access:TRDBATCH.Tryfetch(trb:JobStatusKey) = Level:Benign
        ! Error
        Case Missive('Error! The selected job has not been despatched to a third party repairer.', 'ServiceBase 3g', |
                     'mstop.jpg', '/OK')
        Of 1 ! OK Button
        End ! Case Missive
        error# = 1
        Do fail_update
    End ! If Access:TRDBATCH.Tryfetch(trb:JobStatusKey) = Level:Benign
pass_update     Routine
    jobque:RecordNumber += 1
    jobque:JobNumber    = tmp:JobNumber
    If job:exchange_unit_number
        jobque:Exchanged = 'YES'
    Else!If job:exchange_unit_number
        jobque:Exchanged = 'NO'
    End!If job:exchange_unit_number
    jobque:BatchRecordNumber = trb:RecordNumber
    Add(tmp:JobQueue)
    Sort(tmp:JobQueue,-jobque:RecordNumber)
    trb:status = 'IN'
    trb:datereturn  = Today()
    trb:TimeReturn  = Clock()
    Access:USERS.Clearkey(use:password_key)
    use:Password = glo:Password
    If Access:USERS.TryFetch(use:password_key) = Level:Benign
        !Found
        trb:ReturnUser = use:User_Code
    Else ! Access:USERS.TryFetch(use:password_key)
        !Error
    End ! Access:USERS.TryFetch(use:password_key)
    trb:ReturnWaybillNo         = tmp:WaybillNumber
    trb:ReturnRepairType        = tmp:RepairType
    trb:ReturnRejectedAmount    = tmp:RejectedAmount
    trb:ReturnRejectedReason    = tmp:RejectedReason
    trb:ThirdPartyInvoiceNO     = tmp:InvoiceNumber
    trb:ThirdPartyInvoiceDate   = tmp:ThirdPartyInvoiceDate
    trb:ThirdPartyInvoiceCharge = tmp:Cost
    trb:ThirdPartyVAT           = tmp:CostVAT
    trb:ThirdPartyMarkup        = tmp:3rdPartyMarkup
    trb:Manufacturer            = tmp:Manufacturer
    trb:ModelNumber             = tmp:ModelNumber

    ! Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)
    If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        trb:BatchRunNotPrinted      = False
    Else ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
        trb:BatchRunNotPrinted      = True
    End ! If GETINI('XML','CreateOracle',,Clip(Path()) & '\SB2KDEF.INI') <> 1
    ! End   - Check if "Use Oracle" default is set. If not, then mark as printed - TrkBs: 5110 (DBH: 18-08-2005)

    ! -----------------------------------------
    ! DBH 12/11/2008 #10394
    ! Inserting: Update NMS PreAlert List
    If f:NMSNumber = 1 And tmp:NewThirdPartySite <> ''
        trb:NewThirdPartySite = tmp:NewThirdPartySite
        Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
        trd:Company_Name = tmp:NewThirdPartySite
        If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            !Found                              \
            trb:NewThirdPartySiteID = trd:ASCID
        Else ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            !Error
        End ! If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
        nms:Processed = 1
        Access:NMSPRE.Update()
    End ! If f:NMSRefNumber = 1 ANd tmp:NewThirdPartySite <> ''
    ! End: DBH 12/11/2008 #10394
    ! -----------------------------------------
    trb:EVO_Status = 'R'        !jc 24/1/13 - tb12777 - marks this as ready to send to EVO

    access:trdbatch.update()

    !Evo_Export('THIRD',trb:RecordNumber,'N')    !JC 28/01/13 - exports the EVO stuff immeditatly but gives no errors if it fails
    !Cannot do this here - have to wait until repbatch is run to group into Purchase order and create trb:PurchaseOrderNumber

    tmp:ErrorText   = 'Job Number: ' & Clip(tmp:JobNumber) & ' Updated Successfully.'
    tmp:jobNumber    = ''
    esn_temp        = ''
    tmp:MSN         = ''
    tmp:cost        = ''
    tmp:CostVAT     = ''
    tmp:3rdPartyMarkup = ''
    tmp:3rdPartyTotal = ''
    tmp:InvoiceNumber   = ''
    tmp:ThirdPartyInvoiceDate = ''
    ?tmp:MSN{prop:Hide} = 1
    ?tmp:MSN:Prompt{prop:Hide} = 1
    !reset the imei changed flag for the next set
    tmp:IMEIChanged = 0
    tmp:MSNChanged  = 0


    Display()
    Select(?tmp:JobNumber)

fail_update     Routine
    tmp:ErrorText   = 'Job Number: ' & Clip(tmp:JobNumber) & ' Update Failed.'
    Display()

ThirdParty      Routine
    access:jobthird.clearkey(jot:RefNumberKey)
    jot:RefNumber   = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefnumberKey)
    If access:jobthird.previous()
        get(jobthird,0)
        if access:jobthird.primerecord() = Level:benign
            jot:refnumber    = job:Ref_Number
            jot:originalimei = job:esn
            jot:OriginalMSN  = job:MSN
            jot:outimei      = job:esn
            jot:OutMSN       = job:MSN
            jot:inimei       = esn_temp
            jot:inMSN        = tmp:MSN
            jot:dateout      = job:ThirdPartyDateDesp
            jot:datedespatched = job:ThirdPartyDateDesp
            jot:datein       = Today()
            if access:jobthird.insert()
                access:jobthird.cancelautoinc()
            end
        End!if access:jobthird.primerecord() = Level:benign
    Else!If access:jobthird.previous()
        If jot:RefNumber <> job:Ref_Number
            jot:refnumber    = job:Ref_Number
            jot:originalimei = job:esn
            jot:OriginalMSN  = job:MSN
            jot:outimei      = job:esn
            jot:OutMSN       = job:MSN
            jot:inimei       = esn_temp
            jot:inMSN        = tmp:MSN
            jot:dateout      = job:ThirdPartyDateDesp
            jot:datedespatched = job:ThirdPartyDateDesp
            jot:datein       = Today()
            if access:jobthird.insert()
                access:jobthird.cancelautoinc()
            end
        Else!If job:RefNumber <> jot:RefNumber
            jot:inimei  = esn_temp
            jot:inMSN   = tmp:MSN
            jot:datein  = Today()
            access:jobthird.update()
        End!If job:RefNumber <> jot:RefNumber
    End!If access:jobthird.previous()
Refresh_Types Routine
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020584'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Return_Units')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?WindowTitle
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:NMSPRE.Open
  Relate:STAHEAD.Open
  Relate:TRDREPTY.Open
  Relate:USERS_ALIAS.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:TRDBATCH.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:EXCHHIST.UseFile
  Access:JOBTHIRD.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  Access:TRDPARTY.UseFile
  Access:MODELNUM.UseFile
  Access:TRDACC.UseFile
  Access:JOBOUTFL.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  access:status.clearkey(sts:ref_number_only_key)
  sts:ref_number = 605
  if access:status.tryfetch(sts:ref_number_only_key) = Level:Benign
      tmp:status  = sts:status
  End!if access:status.tryfetch(sts:ref_number_only_key)
  OPEN(window)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  If f:NMSNumber = 1
      ?tmp:JobNumber{prop:Hide} = 1
      ?tmp:JobNumber:Prompt{prop:Hide} = 1
      ?tmp:JobNumber:2{prop:Hide} = 0
      ?tmp:JobNumber:Prompt:2{prop:Hide} = 0
      ?ESN_Temp{prop:Hide} = 1
      ?ESN_Temp:Prompt{prop:Hide} = 1
      ?ESN_Temp:2{prop:Hide} = 0
      ?ESN_Temp:Prompt:2{prop:Hide} = 0
      ?tmp:NewThirdPartySite{prop:Hide} = 0
      ?Prompt:NewThirdPartySite{prop:Hide} = 0
  
      Select(?ESN_Temp:2)
  !
  !    Access:NMSPRE.ClearKey(nms:RecordNumberKey)
  !    nms:RecordNumber = f:NMSNumber
  !    If Access:NMSPRE.TryFetch(nms:RecordNumberKey) = Level:Benign
  !        !Found
  !        tmp:JobNumber = nms:JobNumber
  !        tmp:WaybillNumber = nms:WaybillNumber
  !        ?tmp:NewThirdPartySite{prop:Hide} = 0
  !        ?Prompt:NewThirdPartySite{prop:Hide} = 0
  !        ?LookupThirdParty{prop:Hide} = 0
  !        Access:TRDPARTY.ClearKey(trd:ASCIDKey)
  !        trd:ASCID = nms:ThirdPartyID
  !        If Access:TRDPARTY.TryFetch(trd:ASCIDKey) = Level:Benign
  !            !Found
  !            tmp:NewThirdPartySite = trd:Company_Name
  !        Else ! If Access:TRDPARTY.TryFetch(trd:ASCIIDKey) = Level:Benign
  !            !Error
  !        End ! If Access:TRDPARTY.TryFetch(trd:ASCIIDKey) = Level:Benign
  !
  !        Post(Event:Accepted,?tmp:JobNumber)
  !    Else ! If Access:NMSPRE.TryFetch(nms:RecordNumberKey) = Level:Benign
  !        !Error
  !    End ! If Access:NMSPRE.TryFetch(nms:RecordNumberKey) = Level:Benign
  
  End ! If f:NMSNumber > 0
  ! Save Window Name
   AddToLog('Window','Open','Return_Units')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:ThirdPartyInvoiceDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:status{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?tmp:status{Prop:Tip}
  END
  IF ?tmp:status{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?tmp:status{Prop:Msg}
  END
  IF ?tmp:RepairType{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:RepairType{Prop:Tip}
  END
  IF ?tmp:RepairType{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:RepairType{Prop:Msg}
  END
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:ModelNumber{Prop:Tip} AND ~?CallLookup:3{Prop:Tip}
     ?CallLookup:3{Prop:Tip} = 'Select ' & ?tmp:ModelNumber{Prop:Tip}
  END
  IF ?tmp:ModelNumber{Prop:Msg} AND ~?CallLookup:3{Prop:Msg}
     ?CallLookup:3{Prop:Msg} = 'Select ' & ?tmp:ModelNumber{Prop:Msg}
  END
  IF ?tmp:NewThirdPartySite{Prop:Tip} AND ~?LookupThirdParty{Prop:Tip}
     ?LookupThirdParty{Prop:Tip} = 'Select ' & ?tmp:NewThirdPartySite{Prop:Tip}
  END
  IF ?tmp:NewThirdPartySite{Prop:Msg} AND ~?LookupThirdParty{Prop:Msg}
     ?LookupThirdParty{Prop:Msg} = 'Select ' & ?tmp:NewThirdPartySite{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:NMSPRE.Close
    Relate:STAHEAD.Close
    Relate:TRDREPTY.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Return_Units')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickJobStatus
      Browse_3rdParty
      Browse_Manufacturers
      SelectModelKnownMake
      BrowseThirdPartyRepairTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?NextButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NextButton, Accepted)
      ! Force Charge/Invoice and IMEI - TrkBs: 6264 (DBH: 08-09-2005)
      If ESN_temp = ''
          Case Missive('You must insert a IMEI number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?ESN_Temp)
          Cycle
      End ! If ESN_temp = ''
      
      If tmp:InvoiceNumber = ''
          Case Missive('You must insert an invoice number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:InvoiceNumber)
          Cycle
      End !tmp:InvoiceNumber = ''
      
      If tmp:ThirdPartyInvoiceDate = 0
          Case Missive('You must insert an Invoice Date.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:ThirdPartyInvoiceDate)
          Cycle
      End ! If tmp:ThirdPartyInvoiceDate = 0
      
      If tmp:Cost > 999.99
          Case Missive('The payment entered is ' & Format(tmp:Cost,@n14.2) & '.'&|
            '<13,10>'&|
            '<13,10>Please confirm this is correct?','ServiceBase 3g',|
                         'mquest.jpg','\Re-Enter|/Confirm') 
              Of 2 ! Confirm Button
              Of 1 ! Re-Enter Button
                  Select(?tmp:Cost)
                  Cycle
          End ! Case Missive
      End !If tmp:Cost > 999.99
      
      Do FinalRoutine
      
      if tmp:IMEIChanged = 1      !was Error# = 1 but that didn't work
        !we want to keep the details so they can be reentered
      ELSE
        ?JobDetailsGroup{prop:Hide} = True
      END
      
      if ~0{prop:AcceptAll}
          select(?tmp:JobNumber)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NextButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020584'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020584'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020584'&'0')
      ***
    OF ?tmp:status
      IF tmp:status OR ?tmp:status{Prop:Req}
        sts:Status = tmp:status
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:tmp:status        = tmp:status
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:status = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:status = look:tmp:status
            SELECT(?tmp:status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = tmp:status
      GLO:Select1 = 'JOB'
      sts:Job = 'YES'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:status = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:status)
    OF ?ESN_temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp:2, Accepted)
      If f:NMSNumber = 1
          Access:NMSPRE.ClearKey(nms:ProcessedIMEIKey)
          nms:Processed = 0
          nms:IMEINumber = ESN_Temp
          If Access:NMSPRE.TryFetch(nms:ProcessedIMEIKey) = Level:Benign
              !Found
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number = nms:JobNumber
              If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  !Found
                  tmp:JobNumber = job:Ref_Number
                  If job:Chargeable_Job = 'YES'
                      ?ChargeableGroup{prop:Hide} = 0
                      tmp:ChaChargeType = job:Charge_Type
                      tmp:ChaRepairType = job:Repair_Type
                  Else !If job:Chargeable_Job = 'YES'
                      ?ChargeableGroup{prop:Hide} = 1
                      tmp:ChaChargeType = ''
                      tmp:ChaRepairType = ''
                  End !If job:Chargeable_Job = 'YES'
                  If job:Warranty_Job = 'YES'
                      ?WarrantyGroup{prop:Hide} = 0
                      tmp:WarChargeType = job:Warranty_Charge_Type
                      tmp:WarRepairType = job:Repair_Type_Warranty
                  Else !If job:Chargeable_Job = 'YES'
                      ?WarrantyGroup{prop:Hide} = 1
                      tmp:WarChargeType = ''
                      tmp:WarRepairType = ''
                  End !If job:Chargeable_Job = 'YES'
                  ?JobDetailsGroup{PROP:Hide} = False
                  tmp:Manufacturer = job:Manufacturer
                  tmp:ModelNumber = job:Model_Number
      
                  Case nms:WarrantyOption
                  Of '001'
                      If job:Warranty_Job <> 'YES'
                          Beep(Beep:SystemAsterisk)  ;  Yield()
                          Case Missive('Warning! The selected job has been marked by NMS as a "Warranty Job". In ServiceBase is it a Chargeable Job.','ServiceBase 3g',|
                                         'midea.jpg','/&OK') 
                              Of 1 ! &OK Button
                          End!Case Message
                      End ! If job:Warranty_Job <> 'YES'
                  Of '002'
                      If job:Chargeable_Job <> 'YES'
                          Beep(Beep:SystemAsterisk)  ;  Yield()
                          Case Missive('Warning! The selected job has been marked by NMS as a "Chargrable Job". In ServiceBase is it a Warranty Job.','ServiceBase 3g',|
                                         'midea.jpg','/&OK') 
                              Of 1 ! &OK Button
                          End!Case Message
                      End ! If job:Chargeable_Job <> 'YES'
                  End ! Case nms:WarrantyOption
      
                  Access:TRDPARTY.ClearKey(trd:ASCIDKey)
                  trd:ASCID = nms:ThirdPartyID
                  If Access:TRDPARTY.TryFetch(trd:ASCIDKey) = Level:Benign
                      !Found
                      tmp:NewThirdPartySite = trd:Company_Name
                  Else ! If Access:TRDPARTY.TryFetch(trd:ASCIIDKey) = Level:Benign
                      !Error
                  End ! If Access:TRDPARTY.TryFetch(trd:ASCIIDKey) = Level:Benign
      
      
                  Select(?tmp:NewThirdPartySite)
              Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                  !Error
                  ?ChargeableGroup{prop:Hide} = 1
                  ?WarrantyGroup{prop:Hide} = 1
                  ?JobDetailsGroup{Prop:Hide} = 1
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Missive('Unable to find the job that corresponds to the selected I.M.E.I. Number.','ServiceBase 3g',|
                                 'mstop.jpg','/&OK') 
                      Of 1 ! &OK Button
                  End!Case Message
                  Select(?ESN_Temp:2)
              End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      
          Else ! If Access:NMSPRE.TryFetch(nms:ProcessedIMEIKey) = Level:Benign
              !Error
              ?ChargeableGroup{prop:Hide} = 1
              ?WarrantyGroup{prop:Hide} = 1
              ?JobDetailsGroup{Prop:Hide} = 1
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Unable to find the selected I.M.E.I. Number.','ServiceBase 3g',|
                             'mstop.jpg','/&OK') 
                  Of 1 ! &OK Button
              End!Case Message
              Select(?ESN_Temp:2)
          End ! If Access:NMSPRE.TryFetch(nms:ProcessedIMEIKey) = Level:Benign
      
      End ! If f:NMSNumber = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp:2, Accepted)
    OF ?tmp:JobNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobNumber, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If job:Chargeable_Job = 'YES'
              ?ChargeableGroup{prop:Hide} = 0
              tmp:ChaChargeType = job:Charge_Type
              tmp:ChaRepairType = job:Repair_Type
          Else !If job:Chargeable_Job = 'YES'
              ?ChargeableGroup{prop:Hide} = 1
              tmp:ChaChargeType = ''
              tmp:ChaRepairType = ''
          End !If job:Chargeable_Job = 'YES'
          If job:Warranty_Job = 'YES'
              ?WarrantyGroup{prop:Hide} = 0
              tmp:WarChargeType = job:Warranty_Charge_Type
              tmp:WarRepairType = job:Repair_Type_Warranty
          Else !If job:Chargeable_Job = 'YES'
              ?WarrantyGroup{prop:Hide} = 1
              tmp:WarChargeType = ''
              tmp:WarRepairType = ''
          End !If job:Chargeable_Job = 'YES'
          ?JobDetailsGroup{PROP:Hide} = False
          tmp:Manufacturer = job:Manufacturer
          tmp:ModelNumber = job:Model_Number
          Select(?ESN_temp)
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          ?ChargeableGroup{prop:Hide} = 1
          ?WarrantyGroup{prop:Hide} = 1
          ?JobDetailsGroup{PROP:Hide} = True
          Case Missive('Error! Unable to find the selected job number.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Select(?tmp:JobNumber)
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobNumber, Accepted)
    OF ?tmp:NewThirdPartySite
      IF tmp:NewThirdPartySite OR ?tmp:NewThirdPartySite{Prop:Req}
        trd:Company_Name = tmp:NewThirdPartySite
        !Save Lookup Field Incase Of error
        look:tmp:NewThirdPartySite        = tmp:NewThirdPartySite
        IF Access:TRDPARTY.TryFetch(trd:Company_Name_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:NewThirdPartySite = trd:Company_Name
          ELSE
            !Restore Lookup On Error
            tmp:NewThirdPartySite = look:tmp:NewThirdPartySite
            SELECT(?tmp:NewThirdPartySite)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupThirdParty
      ThisWindow.Update
      trd:Company_Name = tmp:NewThirdPartySite
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:NewThirdPartySite = trd:Company_Name
          Select(?+1)
      ELSE
          Select(?tmp:NewThirdPartySite)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:NewThirdPartySite)
    OF ?ESN_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Accepted)
      IF LEN(CLIP(ESN_temp)) = 18
          ! Ericsson IMEI!
          ESN_temp = SUB(ESN_temp, 4, 15)
          ! ESN_Entry_Temp = Job:ESN
          UPDATE()
          DISPLAY()
      ELSE
        ! Job:ESN = ESN_Entry_Temp
      END ! IF
      
      ! Limit the IMEI to 15 characters - TrkBs: 5110 (DBH: 20-05-2005)
      If Len(Clip(ESN_temp)) <> 15 And Clip(ESN_temp) <> ''
          Case Missive('The I.M.E.I. number must be 15 characters in length.','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          ESN_Temp = ''
          Select(?ESN_temp)
          Cycle
      End ! Len(Clip(ESN_temp)) <> 15
      ! End   - Limit the IMEI to 15 characters - TrkBs: 5110 (DBH: 20-05-2005)
      
      ! Validate the Model Number against the IMEI Number - TrkBs: 5110 (DBH: 23-05-2005)
      tmp:ModelNumber = IMEIModelRoutine(ESN_temp,tmp:ModelNumber)
      
      Access:MODELNUM.ClearKey(mod:Model_Number_Key)
      mod:Model_Number = tmp:ModelNumber
      If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
          !Found
          ! Double check the that manufacter is correct too - TrkBs: 5110 (DBH: 24-05-2005)
          tmp:Manufacturer = mod:Manufacturer
      Else !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
          !Error
      End !If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      ! End   - Validate the Model Number against the IMEI Number - TrkBs: 5110 (DBH: 23-05-2005)
      Display()
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Accepted)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
      ! Get Manufactuerere
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Found
          IF LEN(CLIP(tmp:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
              ! Ericsson MSN!
              tmp:MSN = SUB(tmp:MSN, 2, 10)
              UPDATE()
          END ! IF
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Error
          ! Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      !Do FinalRoutine
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
    OF ?tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:ModelNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ModelNumber, Accepted)
      IF tmp:ModelNumber OR ?tmp:ModelNumber{Prop:Req}
        mod:Model_Number = tmp:ModelNumber
        mod:Manufacturer = tmp:Manufacturer
        GLO:Select2 = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:ModelNumber        = tmp:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            tmp:ModelNumber = mod:Model_Number
          ELSE
            CLEAR(mod:Manufacturer)
            CLEAR(GLO:Select2)
            !Restore Lookup On Error
            tmp:ModelNumber = look:tmp:ModelNumber
            SELECT(?tmp:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! Validate the Model Number against the IMEI Number - TrkBs: 5110 (DBH: 23-05-2005)
      tmp:ModelNumber = IMEIModelRoutine(ESN_temp,tmp:ModelNumber)
      ! End   - Validate the Model Number against the IMEI Number - TrkBs: 5110 (DBH: 23-05-2005)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ModelNumber, Accepted)
    OF ?CallLookup:3
      ThisWindow.Update
      mod:Model_Number = tmp:ModelNumber
      GLO:Select2 = tmp:Manufacturer
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          tmp:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ModelNumber)
    OF ?tmp:RepairType
      IF tmp:RepairType OR ?tmp:RepairType{Prop:Req}
        try:RepairType = tmp:RepairType
        !Save Lookup Field Incase Of error
        look:tmp:RepairType        = tmp:RepairType
        IF Access:TRDREPTY.TryFetch(try:RepairTypeKey)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tmp:RepairType = try:RepairType
          ELSE
            !Restore Lookup On Error
            tmp:RepairType = look:tmp:RepairType
            SELECT(?tmp:RepairType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      try:RepairType = tmp:RepairType
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tmp:RepairType = try:RepairType
          Select(?+1)
      ELSE
          Select(?tmp:RepairType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:RepairType)
    OF ?tmp:Cost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Cost, Accepted)
          !3rd party defaults
          Access:TRDBATCH.Clearkey(trb:JobStatusKey)
          trb:Status  = 'OUT'
          trb:Ref_Number  = tmp:JobNumber
          If Access:TRDBATCH.Tryfetch(trb:JobStatusKey) = Level:Benign
              Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
              trd:Company_Name = trb:Company_Name
              If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                  !Found
                  tmp:3rdPartyMarkup  = trd:Markup
                  tmp:CostVAT         = Round(tmp:Cost * trd:VatRate/100,.01)
                  tmp:3rdPartyTotal   = tmp:Cost + tmp:CostVAT
              Else!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
          End !If Access:TRDBATCH.Tryfetch(trb:JobStatusKey) = Level:Benign
      !    If tmp:Cost <> 0
      !        ?tmp:InvoiceNumber{prop:Req} = 1
      !    Else
      !        ?tmp:InvoiceNumber{prop:Req} = 0
      !    End !If tmp:Cost <> 0
      !    Bryan.CompFieldColour()
          Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Cost, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:ThirdPartyInvoiceDate = TINCALENDARStyle1(tmp:ThirdPartyInvoiceDate)
          Display(?tmp:ThirdPartyInvoiceDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ViewJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewJob, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          GlobalRequest = ChangeRecord
          Update_Jobs_Rapid
          Post(Event:Accepted,?tmp:JobNumber)
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewJob, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Stop# = 1
      Case Missive('Are you sure you want to stop the return process?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
              ! Change wording, and only show message if jobs returned - TrkBs: 6394 (DBH: 29-09-2005)
              If Records(tmp:JobQueue)
                  Case Missive('Select Option:'&|
                    '|- Complete:   Print a returns note for all returned jobs, over a selected date range.'&|
                    '|- Current:   Print a returns note for the current jobs processed in this session.'&|
                    '|- No Print:   Do not print a returns note.','ServiceBase 3g',|
                                 'mquest.jpg','\No Print|/Current|Complete') 
                      Of 3 ! Complete Button
                          Third_Party_Report_Criteria
                      Of 2 ! Current Button
                          Free(glo:Queue)
                          Loop x# = 1 To Records(tmp:JobQueue)
                              Get(tmp:JobQueue,x#)
                              glo:Pointer = jobque:BatchRecordNumber
                              Add(glo:Queue)
                          End !Loop x# = 1 To Records(tmp:JobQueue)
                          Third_Party_Returns_Report(1)
                          Free(glo:Queue)
                      Of 1 ! No Print Button
                  End ! Case Missive
              End ! If Records(tmp:JobQueue)
          Of 1 ! No Button
              Stop# = 0
      End ! Case Missive
      If Stop#
          Post(event:CloseWindow)
      End !Stop#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:ThirdPartyInvoiceDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?ESN_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Selected)
      !?ESN_temp{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Selected)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Selected)
      ?tmp:MSN{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Selected)
    OF ?tmp:Cost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Cost, Selected)
      ?tmp:Cost{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Cost, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Local.ExchangeUnitExist        Procedure()
Code
    If job:Exchange_Unit_Number <> 0
        Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
        xch:ESN = ESN_Temp
        If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
            !Found
            Case Missive('The selected I.M.E.I. number already exists in the exchange database.'&|
              '<13,10>Do you wish to RE-ENTER the I.M.E.I. number or REPLACE the existing unit in the exchange database?','ServiceBase 3g',|
                           'mquest.jpg','Replace|Re-Enter') 
                Of 2 ! Re-Enter Button
                    Return 1
                Of 1 ! Replace Button
                    !Copy the notes from the old unit, deleted it and use the new one - L883 (DBH: 11-09-2003)
                    Return 2
            End ! Case Missive
        Else ! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
            !Error
        End !If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
    End !If job:Exchange_Unit_Number <> 0
    Return 3
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Allocate_Authorisation_Number PROCEDURE               !Generated from procedure template - Window

tmp:BatchNo          LONG
save_trb_id          USHORT,AUTO
tmp:AuthorisationNo  STRING(30)
tmp:CompanyName      STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:CompanyName
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
window               WINDOW('Allocate Authorisation Number'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Allocate Authorisation Number'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('sd'),USE(?Tab1)
                           PROMPT('The Authorisation Number will be allocated to ALL jobs in the selected Batch'),AT(238,154,204,20),USE(?Prompt1),CENTER,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(316,184,124,10),USE(tmp:CompanyName),IMM,VSCROLL,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Batch Number'),AT(240,204),USE(?tmp:BatchNumber:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('3rd Party Agent'),AT(240,184),USE(?tmp:BatchNumber:Prompt:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(316,204,64,10),USE(tmp:BatchNo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Authorisation No'),AT(240,224),USE(?tmp:AuthorisationNo:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,224,124,10),USE(tmp:AuthorisationNo),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Authorisation Number for the Despatched Batch'),TIP('Authorisation Number for the Despatched Batch'),REQ,UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,ICON('allocatp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020582'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Allocate_Authorisation_Number')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRDPARTY.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Allocate_Authorisation_Number')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB4.Init(tmp:CompanyName,?tmp:CompanyName,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder()
  FDCB4.AddField(trd:Company_Name,FDCB4.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDPARTY.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Allocate_Authorisation_Number')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020582'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020582'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020582'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Case Missive('Are you sure you want to allocate the selected authorisation number to batch ' & tmp:BatchNo & '?','ServiceBase 3g',|
                     'mquest.jpg','\No|/Yes') 
          Of 2 ! Yes Button
          
      
              count# = 0
              continue# = 0
              setcursor(cursor:wait)
              save_trb_id = access:trdbatch.savefile()
              access:trdbatch.clearkey(trb:batch_number_key)
              trb:company_name = tmp:companyName
              trb:batch_number = tmp:BatchNo
              set(trb:batch_number_key,trb:batch_number_key)
              loop
                  if access:trdbatch.next()
                     break
                  end !if
                  if trb:company_name <> tmp:CompanyName      |
                  or trb:batch_number <> tmp:BatchNo      |
                      then break.  ! end if
                  If trb:authorisationNo <> '' And continue# = 0
                      setcursor()
                      Case Missive('This batch has already been allocated an authorisation number of ' & trb:AuthorisationNo & '.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure you want to overwrite this?','ServiceBase 3g',|
                                     'mquest.jpg','\No|/Yes') 
                          Of 2 ! Yes Button
                              continue# = 1
                          Of 1 ! No Button
                              count# = 2
                              Break
                      End ! Case Missive
                      setcursor(cursor:wait)
                  End!If trb:authorisationNo <> '' And continue# = 0
                  count# = 1
                  trb:authorisationNo = tmp:AuthorisationNo
                  access:trdbatch.update()
              end !loop
              access:trdbatch.restorefile(save_trb_id)
              setcursor()
              Case count#
                  Of 0
                      Case Missive('Cannot find the selected batch number.','ServiceBase 3g',|
                                     'mstop.jpg','/OK') 
                          Of 1 ! OK Button
                      End ! Case Missive
                  Of 1
                      Case Missive('Allocation complete.','ServiceBase 3g',|
                                     'midea.jpg','/OK') 
                          Of 1 ! OK Button
                      End ! Case Missive
              End!IF count# = 0
          Of 1 ! No Button
      End ! Case Missive
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Third_Party_Print_Routine PROCEDURE                   !Generated from procedure template - Window

tmp:ReportType       BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Print Routines'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Print Routines'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           OPTION('Reports'),AT(288,188,96,44),USE(tmp:ReportType),COLOR(0D6E7EFH)
                             RADIO('Consignment Note'),AT(297,191),USE(?Option1:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Returns Note'),AT(297,202),USE(?Option1:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                             RADIO('Failed Returns'),AT(297,214),USE(?Option1:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('3')
                           END
                         END
                       END
                       BUTTON,AT(368,258),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       BUTTON,AT(300,258),USE(?Print),TRN,FLAT,LEFT,ICON('printp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020585'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Third_Party_Print_Routine')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Third_Party_Print_Routine')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','Third_Party_Print_Routine')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020585'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020585'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020585'&'0')
      ***
    OF ?Print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
      Case tmp:ReportType
          Of 1
              Third_Party_Consignment_Criteria
          Of 2
              Third_Party_Report_Criteria
          Of 3
              Third_Party_Returns_Criteria
      End!Case tmp:ReportType
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseAvailLocation PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
Current_Spaces_Temp  STRING(10)
yes_temp             STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOCINTER)
                       PROJECT(loi:Location)
                       PROJECT(loi:Location_Available)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
loi:Location           LIKE(loi:Location)             !List box control field - type derived from field
Current_Spaces_Temp    LIKE(Current_Spaces_Temp)      !List box control field - type derived from local data
loi:Location_Available LIKE(loi:Location_Available)   !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Available Internal Locations File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Available Internal Location File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(240,112,204,212),USE(?Browse:1),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('127L(2)|M~Location~@s30@40L(2)|M~Current Spaces~@s10@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Location'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(240,98,124,10),USE(loi:Location),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020578'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseAvailLocation')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCINTER.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCINTER,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','BrowseAvailLocation')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,loi:Location_Available_Key)
  BRW1.AddRange(loi:Location_Available,yes_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?loi:Location,loi:Location,1,BRW1)
  BIND('Current_Spaces_Temp',Current_Spaces_Temp)
  BRW1.AddField(loi:Location,BRW1.Q.loi:Location)
  BRW1.AddField(Current_Spaces_Temp,BRW1.Q.Current_Spaces_Temp)
  BRW1.AddField(loi:Location_Available,BRW1.Q.loi:Location_Available)
  QuickWindow{PROP:MinWidth}=295
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCINTER.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseAvailLocation')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020578'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020578'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020578'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?loi:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loi:Location, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loi:Location, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (loi:Allocate_Spaces = 'YES')
    Current_Spaces_Temp = loi:Current_Spaces
  ELSE
    Current_Spaces_Temp = 'N/A'
  END
  PARENT.SetQueueRecord
  SELF.Q.Current_Spaces_Temp = Current_Spaces_Temp    !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?LOI:Location, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

PickJobAccessories PROCEDURE (func:JobNumber)         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:JobNumber        LONG
tmp:Tag              STRING(1)
tmp:Close            BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBACC)
                       PROJECT(jac:Accessory)
                       PROJECT(jac:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tmp:Tag                LIKE(tmp:Tag)                  !List box control field - type derived from local data
tmp:Tag_Icon           LONG                           !Entry's icon ID
jac:Accessory          LIKE(jac:Accessory)            !List box control field - type derived from field
jac:Ref_Number         LIKE(jac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Accessories'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Accessory File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,114,148,208),USE(?Browse:1),IMM,HSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@80L(2)|M~Accessory~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('By Accessory'),USE(?Tab:2)
                           PROMPT('Select Accessories To Despatch'),AT(264,86),USE(?Prompt1),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,100,124,10),USE(jac:Accessory),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON('&Rev tags'),AT(204,148,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(204,175,1,1),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(448,162),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(448,194),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(448,226),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(448,130),USE(?Select),TRN,FLAT,LEFT,ICON('selectp.jpg')
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,HIDE,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = jac:Accessory
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = jac:Accessory
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:Tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:Tag = ''
  END
    Queue:Browse:1.tmp:Tag = tmp:Tag
  IF (tmp:tag = '*')
    Queue:Browse:1.tmp:Tag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:Tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = jac:Accessory
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::8:QUEUE = glo:Queue
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer = jac:Accessory
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = jac:Accessory
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020580'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PickJobAccessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBACC.Open
  SELF.FilesOpened = True
  tmp:JobNumber   = func:JobNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','PickJobAccessories')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,jac:Ref_Number_Key)
  BRW1.AddRange(jac:Ref_Number,tmp:JobNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?jac:Accessory,jac:Accessory,1,BRW1)
  BIND('tmp:Tag',tmp:Tag)
  BIND('tmp:JobNumber',tmp:JobNumber)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tmp:Tag,BRW1.Q.tmp:Tag)
  BRW1.AddField(jac:Accessory,BRW1.Q.jac:Accessory)
  BRW1.AddField(jac:Ref_Number,BRW1.Q.jac:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:JOBACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','PickJobAccessories')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020580'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020580'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020580'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Select
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
      ! Only allowed to close the window via Select - TrkBs: 6264 (DBH: 12-10-2005)
      tmp:Close = 1
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?jac:Accessory
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If tmp:Close <> 1
          Cycle
      End ! If tmp:Close <> 1
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?Dastag)
          Of F6Key
              Post(Event:Accepted,?Dastagall)
          Of F7Key
              Post(Event:Accepted,?DasUntagall)
          Of F10Key
              Post(Event:Accepted,?Select)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::8:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jac:Accessory
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:Tag = ''
    ELSE
      tmp:Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:Tag_Icon = 2
  ELSE
    SELF.Q.tmp:Tag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  ! Insert --- Only show accessories that were sent to the ARC (DBH: 07/09/2009) #11056
  if (jac:Attached <> 1)
      return Record:Filtered
  end ! if (jac:Attached <> 1)
  ! end --- (DBH: 07/09/2009) #11056
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = jac:Accessory
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Xfiles PROCEDURE                                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Xfiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTV.Open
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! Save Window Name
   AddToLog('Window','Open','Xfiles')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTV.Close
    Relate:SRNTEXT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Xfiles')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Startup              PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    if ClarioNET:Global.Param2 ='' then
        !this was loaded directly
        GLO:webJob = false
    Else
        Glo:WebJob = true
        glo:password = clip(ClarioNET:GLobal.Param1)
    END
    If glo:WebJob
        !Look for a redirection file
        !set path and update procedure
        g_path = GETINI('global','datapath','xx7fail8zz', clip(path()) & '\celweb.ini')
        !message('g_path is ' & clip(g_path))
        if clip(g_path)='xx7fail8zz' then
            !not using an ini file - let it go onto own checks
        ELSE
            setpath (clip(g_path))
        end
    End ! If glo:WebJob
   Relate:DEFAULTS.Open
   Relate:DEFAULTV.Open
   Relate:LOGGED.Open
   Relate:TRADEACC.Open
   Relate:USERS.Open
    ! Close Splash Window (DBH: 24-05-2005)
    PUTINI('STARTING','Run',True,)
    error# = 1
    if GLO:webJob then
        !glo:Password was set on enty to the passed variable - LogonUserPassword
        access:users.clearkey(use:password_key)
        use:password = glo:Password
        if access:users.fetch(use:password_key) = level:benign then error#=0.
        If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() then error#=1.
        
        !check the traders details
        a#=instring(',',ClarioNET:Global.Param2,1,1)
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = clip(ClarioNET:Global.Param2[1:a#-1])
        if not access:tradeacc.fetch(tra:account_number_key)
            If Clip(tra:password)<> ClarioNET:Global.Param2[a#+1:len(clip(ClarioNET:Global.Param2))]
                halt(0,'Incorrect Trade details')
            END
            !glo:Default_Site_Location = tra:SiteLocation
        end
        !Send back the version number to the client
        set(defaultv)
        if access:defaultv.next()
            !error  - this is handled elsewhere ignored here
        ELSE
            !pass current version number back to client
            ClarioNET:CallClientProcedure('VERSIONSAVE',defv:VersionNumber)
        END

    ELSE
        Loop x# = 1 To Len(Clip(Command()))
            If Sub(Command(),x#,1) = '%'
                glo:Password = Clip(Sub(Command(),x#+1,30))
                Access:USERS.Clearkey(use:Password_Key)
                use:Password    = glo:Password
                If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    !Found
                    error# = 0
                    If use:RenewPassword <> 0 
                        If use:PasswordLastChanged = ''
                            use:PasswordLastChanged = Today()
                            Access:USERS.Update()
                        Else !If use:PasswordLastChanged = ''
                            If use:PasswordLastChanged + use:RenewPassword < Today() Or use:PasswordLastChanged > Today() 
                                error#=1
                            End
                        End
                    End !If use:RenewPassword <> 0
                Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                
                Break
            End!If Sub(Command(),x#,1) = '%'
        End!Loop x# = 1 To Len(Comman())
    END !if GLO:WebJob

    If error# = 1
        Do Login
    End

    Browse_Batch

    Do Log_out

   Relate:DEFAULTS.Close
   Relate:DEFAULTV.Close
   Relate:LOGGED.Close
   Relate:TRADEACC.Close
   Relate:USERS.Close
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
    End
Log_out      Routine
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
