

                    MEMBER('Debugging.clw')

JobNumber           LONG

Window              WINDOW('Debugging'),AT(,,186,141),CENTER,GRAY,SYSTEM,ICON('clarion.ico'), |
                        FONT('Segoe UI',9),COLOR(COLOR:White),DOUBLE
                        PROMPT('Job Number'),AT(5,2),USE(?PROMPT1)
                        ENTRY(@s10),AT(5,13,49),USE(JobNumber)
                        GROUP('Uncomplete Job'),AT(5,30,84,49),USE(?GROUP1),BOXED
                            PROMPT('Blank Date Completed'),AT(11,45,74,9),USE(?PROMPT2),FONT(,8)
                            BUTTON('Un-complete Job'),AT(9,58),USE(?btnUncompleteJob)
                        END
                        GROUP('Move Into RRC Control'),AT(94,30,80,49),USE(?Group2),BOXED
                            PROMPT('Untick HubRepair and Change Job Location'),AT(98,41,71,17), |
                                USE(?PROMPT3),FONT(,8)
                            BUTTON('Move Job'),AT(107,58),USE(?btnMoveIntoControl)
                        END
                        GROUP('Exchange Units'),AT(5,81,85,51),USE(?GROUP3),BOXED
                            PROMPT('Units incorrectly made available'),AT(9,92,71,17),USE(?PROMPT4)
                            BUTTON('Count Units'),AT(14,111,71),USE(?btnCountExchange)
                        END
                    END

                    MAP
CountExchange       PROCEDURE()
MoveIntoControl         PROCEDURE()
UncompleteJob           PROCEDURE()
                    END



Main                PROCEDURE()
    CODE
!region CODE
        OPEN(Window)
        
        ACCEPT
            CASE EVENT()
            OF EVENT:Accepted
                CASE FIELD()
                OF ?btnUncompleteJob
                    UncompleteJob()
                OF ?btnMoveIntoControl
                    MoveIntoControl()
                OF ?btnCountExchange
                    CountExchange()
                END ! CASE
            END ! CASE
        END !  ACCEPT
        
        CLOSE(Window)
!endregion
!region PROCEDURE        
MoveIntoControl     PROCEDURE()        
    CODE
        Relate:JOBS.Open()
        Relate:JOBSE.Open()
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = JobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            job:Location = 'AT FRANCHISE'
            IF (Access:JOBS.TryUpdate() = Level:Benign)
                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
                    jobe:HubRepair = 0
                    Access:JOBSE.TryUpdate()
                    BEEP(BEEP:SystemAsterisk)  ;  YIELD()
                    CASE MESSAGE('Done','Debugging',|
                        ICON:Asterisk,'&OK',1) 
                    OF 1 ! &OK Button
                    END!CASE MESSAGE
                    SELECT(?JobNumber)
                END ! IF
            END ! IF
        END ! IF
        
        Relate:JOBSE.Close()
        Relate:JOBS.Close()
        
UncompleteJob       PROCEDURE()
    CODE
        Relate:JOBS.Open()
        Relate:WEBJOB.Open()
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = JobNumber
        IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
            job:Date_Completed = ''
            IF (Access:JOBS.TryUpdate() = Level:Benign)
                Access:WEBJOB.ClearKey(wob:RefNumberKEy)
                wob:RefNumber = job:Ref_Number
                IF (Access:WEBJOB.TryFetch(wob:RefNumberKEy) = Level:Benign)
                    wob:DateCompleted = ''
                    Access:WEBJOB.TryUpdate()
                    BEEP(BEEP:SystemAsterisk)  ;  YIELD()
                    CASE MESSAGE('Done','Debugging',|
                        ICON:Asterisk,'&OK',1) 
                    OF 1 ! &OK Button
                    END!CASE MESSAGE
                    SELECT(?JobNumber)
                END ! IF
            END ! IF
        END ! IF
        
        Relate:WEBJOB.Close()
        Relate:JOBS.Close()

CountExchange       PROCEDURE()
countRecs               LONG
    CODE
        Relate:EXCHHIST.Open()
        Relate:EXCHANGE.Open()
        Access:EXCHHIST.ClearKey(exh:DateStatusKey)
        exh:Date = TODAY()
        SET(exh:DateStatusKey,exh:DateStatusKey)
        LOOP UNTIL Access:EXCHHIST.Next() <> Level:Benign
            IF (exh:Date <> TODAY())
                BREAK
            END ! IF
            IF (exh:User <> 'LBT')
                CYCLE
            END ! IF
            IF (exh:Notes <> 'VDC5758764')
                CYCLE
            END ! IF
            
            Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = exh:Ref_Number
            IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key))
                CYCLE
            END ! IF
            IF (xch:Location <> 'RETURN TO MANUFACTURER')
                CYCLE
            END ! IF
            IF (xch:Stock_Type <> 'DESCO SCRAPS')
                CYCLE
            END ! IF
            countRecs += 1
        END ! LOOP
        
        Relate:EXCHANGE.Close()
        Relate:EXCHHIST.Close()
        
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        CASE MESSAGE('Exchange Units Found: ' & CLIP(countRecs) & '','Debugging',|
            ICON:Asterisk,'&OK',1) 
        OF 1 ! &OK Button
        END!CASE MESSAGE
!endregion        