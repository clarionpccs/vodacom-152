
-- Add paths that are effective for all configurations
-- eg *.exe = exe
-- The redirection system has an order of precedence where a line has priority over later lines

[Debug]
*.obj = obj\debug
*.res = obj\debug
*.rsc = obj\debug
*.lib = obj\debug
*.FileList.xml = obj\debug
*.map = map\debug

[Release]
*.obj = obj\release
*.res = obj\release
*.rsc = obj\release
*.lib = obj\release
*.FileList.xml = obj\release
*.map = map\release

[Common]
-- Add paths that are effective for all configurations
-- eg *.exe = exe

*.ico=.;..\BUTTONS;
*.jpg=.;..\BUTTONS;
*.gif=.;..\BUTTONS;
*.lib=.;..\LIB;
*.dct=.;..\..\;
*.inc=.;..\INC;
*.clw=.;..\INC;
*.c=.;..\INC;

{include %REDDIR%\%REDNAME% }
