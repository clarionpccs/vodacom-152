

   MEMBER('BouncerCheck.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('BOUNCERCHECK001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Main PROCEDURE 

                    MAP
CountJobBouncers    PROCEDURE(<NetWebServerWorker p_web>),LONG
                    END ! MAP
locJobNumber         LONG                                  !
locTotalBouncers     LONG                                  !
QuickWindow          WINDOW('Window'),AT(,,403,199),FONT('Segoe UI',9,COLOR:Black,FONT:regular,CHARSET:ANSI),DOUBLE, |
  CENTER,GRAY,IMM,HLP('Main'),SYSTEM
                       PROMPT('Job Number:'),AT(5,5),USE(?locJobNumber:Prompt)
                       ENTRY(@n_10),AT(59,6,60,10),USE(locJobNumber),RIGHT(2)
                       LIST,AT(57,32,78,70),USE(?LIST1),VSCROLL,FLAT,FORMAT('160L(2)|M~Bouncer Job Number~@s40@'), |
  FROM(glo:Queue20)
                       PROMPT('Total Bouncers:'),AT(5,20),USE(?locTotalBouncers:Prompt)
                       ENTRY(@n_10),AT(59,20,60,10),USE(locTotalBouncers),RIGHT(2),FLAT,SKIP,TRN
                       PROMPT('Manufacturer'),AT(221,10),USE(?man:Manufacturer:Prompt),FONT('Segoe UI',9,,FONT:regular, |
  CHARSET:ANSI),LEFT,TRN
                       ENTRY(@s30),AT(293,9,102,10),USE(man:Manufacturer),FONT('Segoe UI',9,,FONT:regular,CHARSET:ANSI), |
  LEFT,UPR,MSG('Manufacturer'),READONLY,TIP('Manufacturer')
                       PROMPT('Bouncer Period:'),AT(222,47),USE(?man:BouncerPeriod:Prompt)
                       PROMPT('Bouncer Period IMEI:'),AT(225,166),USE(?man:BouncerPeriodIMEI:Prompt)
                       LIST,AT(2,107,187,87),USE(?LIST2),VSCROLL,FLAT,FORMAT('160L(2)|M~Failures~@s40@'),FROM(glo:Queue19)
                       CHECK('Use Bouncer Rules'),AT(293,25),USE(man:UseBouncerRules),VALUE('1','0')
                       CHECK('Do Not Bounce Warranty'),AT(295,183,117,10),USE(man:DoNotBounceWarranty),VALUE('1', |
  '0')
                       BUTTON('Save'),AT(206,149,75),USE(?buttonSave),HIDE
                       BUTTON('Refresh'),AT(123,5),USE(?buttonRefresh)
                       GROUP,AT(293,38,99,139),USE(?groupBouncer),TRN
                         ENTRY(@n-14),AT(298,45,60,10),USE(man:BouncerPeriod),RIGHT(1)
                         OPTION('Bouncer Type'),AT(295,60,107,47),USE(man:BouncerType),BOXED
                           RADIO('Date Booked'),AT(302,68),USE(?OPTION1:RADIO1),VALUE('0')
                           RADIO('Date Completed'),AT(302,78,77,10),USE(?OPTION1:RADIO1:2),VALUE('1')
                           RADIO('Date Despatched'),AT(302,90,72,10),USE(?OPTION1:RADIO1:3),VALUE('2')
                         END
                         CHECK('Bounce In Fault'),AT(302,110,79,10),USE(man:BouncerInFault),VALUE('1','0')
                         CHECK('Bounce Out Fault'),AT(302,125,79,10),USE(man:BouncerOutFault),VALUE('1','0')
                         CHECK('Bounce Same Spares'),AT(302,140,79,10),USE(man:BouncerSpares),VALUE('1','0')
                         ENTRY(@n-14),AT(301,165,60,10),USE(man:BouncerPeriodIMEI),RIGHT(1)
                         CHECK('Off (AND) On (OR)'),AT(302,154),USE(man:BounceRulesType),MSG('Bounce Rules Type')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?locJobNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:JOBS.SetOpenRelated()
  Relate:JOBS.Open                                         ! File JOBS used by this procedure, so make sure it's RelationManager is open
  Access:MANUFACT.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Main',QuickWindow)                         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  IF ?man:UseBouncerRules{Prop:Checked}
    ENABLE(?groupBouncer)
  END
  IF NOT ?man:UseBouncerRules{PROP:Checked}
    DISABLE(?groupBouncer)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  IF SELF.Opened
    INIMgr.Update('Main',QuickWindow)                      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?locJobNumber
      FREE(glo:Queue20)
      FREE(glo:Queue19)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number = locJobNumber
      IF (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
          locTotalBouncers = CountJobBouncers()
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
              
          ELSE
          END
          
      ELSE
          locTotalBouncers = 0
      
      END
      DISPLAY()
      Select(locJobNumber)
      POST(EVENT:Accepted,man:UseBouncerRules)
    OF ?man:UseBouncerRules
      IF ?man:UseBouncerRules{PROP:Checked}
        ENABLE(?groupBouncer)
      END
      IF NOT ?man:UseBouncerRules{PROP:Checked}
        DISABLE(?groupBouncer)
      END
      ThisWindow.Reset
    OF ?buttonSave
      ThisWindow.Update
      IF (man:Manufacturer <> '')
              Access:MANUFACT.TryUpdate()
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Message('Done.','Saved',|
                  Icon:Asterisk,'&OK',1) 
              Of 1 ! &OK Button
              End!Case Message
          
      END
    OF ?buttonRefresh
      ThisWindow.Update
      Post(EVENT:Accepted,locJobNumber)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

CountJobBouncers    PROCEDURE(<NetWebServerWorker p_web>)
tmp:DateBooked          DATE
tmp:count               LONG
save_job2_id            USHORT,AUTO
save_joo_id             USHORT,AUTO
save_mfo_id             USHORT,AUTO
save_wpr_id             USHORT,AUTO
save_par_id             USHORT,AUTO
save_wob_id             USHORT,AUTO
tmp:IMEI                STRING(30)
tmp:Manufacturer        STRING(30)
tmp:IgnoreChargeable    BYTE(0)
tmp:IgnoreWarranty      BYTE(0)
locBouncerType          BYTE
locBouncerTime          LONG

save_chartype_id        USHORT,AUTO
save_reptydef_id        USHORT,AUTO
locFailedInFault        BYTE(0)
locFailedOutFault       BYTE(0)
locFailedParts          BYTE(0)

i                       LONG
recs                    LONG
                    MAP
OutFaultExcluded        Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob,STRING local:Manufacturer),Byte
BouncerBit              Procedure(BYTE fType),Byte

                    END

CODE

        !Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
        !From that count how many times the IMEI number has been booked in before
    tmp:IgnoreChargeable = GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI')
    tmp:IgnoreWarranty  = GETINI('BOUNCER','IgnoreWarranty',,CLIP(PATH())&'\SB2KDEF.INI')

        !Write failures into queue for testing (debug only)
   FREE(glo:Queue19)
   CLEAR(glo:Queue19)

    DO OpenFiles
        
    IF NOT p_web &= NULL
            ! Clear Q for SBOnline
        recs = RECORDS(qBouncedJobs)
        LOOP i = recs TO 1 BY -1
            GET(qBouncedJobs,i)
            IF (qBouncedJobs.SessionID = p_web.SessionID)
                DELETE(qBouncedJobs)
            END ! IF
        END ! LOOP
            ! #13487 Get the job record incase SBOnline isn't in the correct record. (DBH: 13/04/2015)
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = p_web.GSV('job:Ref_Number')
        Access:JOBS.Tryfetch(job:Ref_Number_Key)
    END ! IF           

    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
    END


    CheckBouncer# = 1
    If job:Chargeable_Job = 'YES'
        If tmp:IgnoreChargeable
            CheckBouncer# = 0
           glo:Pointer19 = 'Ignore Chargeable & Chargeable Job';ADD(glo:Queue19)
                
        Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI;ADD(glo:Queue19) = 1
            Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
            cha:Charge_Type = job:Charge_Type
            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                    !Found
                If cha:ExcludeFromBouncer
                    CheckBouncer# = 0
                   glo:Pointer19 = 'Charge Type Excluded';ADD(glo:Queue19)
                Else !If cha:ExcludeFromBouncer
                    Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                    rtd:Manufacturer = job:Manufacturer
                    rtd:Chargeable   = 'YES'
                    rtd:Repair_Type  = job:Repair_Type
                    If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                            !Found
                        If rtd:ExcludeFromBouncer
                           glo:Pointer19 = 'Repair Type Excluded';ADD(glo:Queue19)
                            CheckBouncer# = 0
                        End !If rtd:ExcludeFromBouncer
                    Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                    End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                End !If cha:ExcludeFromBouncer
            Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
            End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
        End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI;ADD(glo:Queue19) = 1
    End !If func:CJob = 'YES'

        !Is the job's outfault excluded
    If CheckBouncer#
        If OutFaultExcluded(job:Ref_Number,job:Warranty_job,job:Chargeable_Job,job:Manufacturer)
           glo:Pointer19 = 'Out Fault Excluded';ADD(glo:Queue19)

            CheckBouncer# = 0
        End !If Local.OutFaultExcluded()
    End !If CheckBouncer#

        !Is the job's infault excluded
    If CheckBouncer#
        Access:MANFAULT.ClearKey(maf:InFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:InFault      = 1
        If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            Case maf:Field_Number
            Of 1
                mfo:Field        = job:Fault_Code1
            Of 2
                mfo:Field        = job:Fault_Code2
            Of 3
                mfo:Field        = job:Fault_Code3
            Of 4
                mfo:Field        = job:Fault_Code4
            Of 5
                mfo:Field        = job:Fault_Code5
            Of 6
                mfo:Field        = job:Fault_Code6
            Of 7
                mfo:Field        = job:Fault_Code7
            Of 8
                mfo:Field        = job:Fault_Code8
            Of 9
                mfo:Field        = job:Fault_Code9
            Of 10
                mfo:Field        = job:Fault_Code10
            Of 11
                mfo:Field        = job:Fault_Code11
            Of 12
                mfo:Field        = job:Fault_Code12
            End !Case maf:Field_Number
            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Found
                If mfo:ExcludeFromBouncer
                   glo:Pointer19 = 'In Fault Excluded';ADD(glo:Queue19)
                    CheckBouncer# = 0
                End !If mfo:ExcludeBouncer
            Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
            End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

        Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
        End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
    End !If CheckBouncer#

        !Lets write the bouncers into a memory queue
    Free(glo:Queue20)
    Clear(glo:Queue20)

    If CheckBouncer#
        If job:Warranty_Job = 'YES'
            If tmp:IgnoreWarranty
                CheckBouncer# = 0
               glo:Pointer19 = 'Ignore Warranty & Warranty Job';ADD(glo:Queue19)
            Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI;ADD(glo:Queue19) = 1

                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                cha:Charge_Type = job:Warranty_Charge_Type
                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                        !Found
                    If cha:ExcludeFromBouncer
                        CheckBouncer# = 0
                       glo:Pointer19 = 'Warranty Charge Type Excluded';ADD(glo:Queue19)

                    Else !If cha:ExcludeFromBouncer
                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                        rtd:Manufacturer = job:Manufacturer
                        rtd:Warranty     = 'YES'
                        rtd:Repair_Type  = job:Repair_Type_Warranty
                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                !Found
                            If rtd:ExcludeFromBouncer
                                CheckBouncer# = 0
                               glo:Pointer19 = 'Warranty Repair Type Type Excluded';ADD(glo:Queue19)

                            End !If rtd:ExcludeFromBouncer
                        Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                        End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                    End !If cha:ExcludeFromBouncer
                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign

                    ! #11887 This is warranty. Check if the manufacturer is excluded. (Bryan: 24/05/2011)
                IF (CheckBouncer#)
                    IF (man:DoNotBounceWarranty = 1)
                        CheckBouncer# = 0
                       glo:Pointer19 = 'Manufacturer: Do not bouncer Warranty jobs';ADD(glo:Queue19)

                    END
                END ! IF (CheckBouncer#)

            End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI;ADD(glo:Queue19) = 1
        End !If func:CJob = 'YES'
    End !If CheckBouncer#

    If CheckBouncer#
        tmp:count    = 0
        locFailedParts = 0
        locFailedInFault = 0
        locFailedOutFault = 0

        If (job:ESN <> 'N/A' And job:ESN <> '')
            Set(defaults)
            Access:Defaults.Next()

            access:jobs2_alias.clearkey(job2:esn_key)
            job2:esn = job:ESN
            set(job2:esn_key,job2:esn_key)
            loop
                if access:jobs2_alias.next()
                    break
                end !if
                if job2:esn <> job:ESN      |
                    then break.  ! end if

                If job2:ref_number = job:Ref_Number
                    CYCLE
                END

                If job2:Cancelled = 'YES'
                   glo:Pointer19 = '# ' & job2:Ref_Number & ' - Cancelled';ADD(glo:Queue19)

                    Cycle
                End !If job2:Cancelled = 'YES'

                If job2:Exchange_Unit_Number <> ''
                   glo:Pointer19 = '# ' & job2:Ref_Number & ' - Exchanged';ADD(glo:Queue19)

                    Cycle
                End !If job2:Exchange_Unit_Number <> ''

                If job2:Chargeable_Job = 'YES'
                    If tmp:IgnoreChargeable
                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - Ignore Chargeable';ADD(glo:Queue19)

                        Cycle
                    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI;ADD(glo:Queue19) = 1

                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                    cha:Charge_Type = job2:Charge_Type
                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                            !Found
                        If cha:ExcludeFromBouncer
                           glo:Pointer19 = '# ' & job2:Ref_Number & ' - Char Type Excluded';ADD(glo:Queue19)

                            Cycle
                        Else !If cha:ExcludeFromBouncer
                            If job2:Repair_Type <> ''
                                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                                rtd:Manufacturer = job:Manufacturer
                                rtd:Chargeable   = 'YES'
                                rtd:Repair_Type  = job2:Repair_Type
                                If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                                        !Found
                                    If rtd:ExcludeFromBouncer
                                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - Rep Type Excluded';ADD(glo:Queue19)

                                        Cycle
                                    End !If rtd:ExcludeFromBouncer
                                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign

                            End !If job2:Repair_Type <> ''
                        End !If cha:ExcludeFromBouncer
                    Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                    End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                End !If func:CJob = 'YES'


                If job2:Warranty_Job = 'YES'
                    If tmp:IgnoreWarranty
                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - Ignore Warranty';ADD(glo:Queue19)

                        Cycle
                    End !If tmp:IgnoreWarranty

                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
                    cha:Charge_Type = job2:Warranty_Charge_Type
                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                            !Found
                        If cha:ExcludeFromBouncer
                           glo:Pointer19 = '# ' & job2:Ref_Number & ' - W Char Type Excluded';ADD(glo:Queue19)

                            Cycle
                        Else !If cha:ExcludeFromBouncer
                            If job2:Repair_Type_Warranty <> ''
                                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                                rtd:Manufacturer = job:Manufacturer
                                rtd:Warranty     = 'YES'
                                rtd:Repair_Type  = job2:Repair_Type_Warranty
                                If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                                        !Found
                                    If rtd:ExcludeFromBouncer
                                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - W Rep Type Excluded';ADD(glo:Queue19)

                                        Cycle
                                    End !If rtd:ExcludeFromBouncer
                                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
                            End !If job2:Repair_Type_Warranty <> ''
                        End !If cha:ExcludeFromBouncer
                    Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                    End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
                End !If func:CJob = 'YES'

                IF (man:UseBouncerRules = 1 AND job:Warranty_Job = 'YES' AND man:BouncerPeriodIMEI > 0)
                        ! #11887 Manufacturer Bouncer Rules Apply.
                        ! Bouncer IMEI period set. So don't get faults, just check date (Bryan: 24/05/2011)
                    IF (BouncerBit(1) = 1)
                            ! Job is a bouncer based on the IMEI date. No need to carry on
                        CYCLE
                    END

                END

                    ! Is Outfault Excluded
                If OutFaultExcluded(job2:Ref_Number,job2:Warranty_job,job2:Chargeable_Job,job2:Manufacturer)
                   glo:Pointer19 = '# ' & job2:Ref_Number & ' - Out Fault Excluded';ADD(glo:Queue19)

                    Cycle
                End !If Local.OutFaultExcluded()

                    !Has job got the same In Fault
					
!J - 17/12/14 This seems to be inverted - try it the other way round
!                    checkInFault# = 1		
!                    IF (man:UseBouncerRules = 1 AND man:BouncerInFault = 0 AND job:Warranty_Job = 'YES;ADD(glo:Queue19)
!                        ! #11887 Manufacturer Bouncer Rules Apply.
!                        ! Should In Fault be compared (Bryan: 24/05/2011)
!                        checkInFault# = 0
!                    END
					
                checkInFault# = 0		
                IF (man:UseBouncerRules = 1 AND man:BouncerInFault = 1 AND job:Warranty_Job = 'YES')
                        ! #11887 Manufacturer Bouncer Rules Apply.
                        ! Should In Fault be compared (Bryan: 24/05/2011)
                    checkInFault# = 1
                END
!END of change 17/12/14					
					
					
					!TB13231 - J - 12/12/14 - change bouncer definition to be on Bouncer fault not InFault
					!man:BouncerInFault now means check on bouncer fault not in fault
                Access:MANFAULT.ClearKey(maf:BouncerFaultKey)
                maf:Manufacturer = job:Manufacturer
                maf:BouncerFault      = 1
                If Access:MANFAULT.TryFetch(maf:BouncerFaultKey) = Level:Benign
                        !Found - use this one
                ELSE
						!Bouncer fault not found - try for the InFault
                    Access:MANFAULT.ClearKey(maf:InFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:InFault      = 1
                    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
							!Ok to use this one
                    ELSE
							!no bouncer fault and no infault
                        cycle
                    END
                END	
					
                IF (checkInFault# = 1)
                    locFailedInFault = 1 ! In Fault Being Checked
                    inFaultsMatch# = 1
                    Case maf:Field_Number
                    Of 1
                        If job2:Fault_Code1 <> job:Fault_Code1
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 2
                        If job2:Fault_Code2 <> job:Fault_Code2
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 3
                        If job2:Fault_Code3 <> job:Fault_Code3
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 4
                        If job2:Fault_Code4 <> job:Fault_Code4
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 5
                        If job2:Fault_Code5 <> job:Fault_Code5
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 6
                        If job2:Fault_Code6 <> job:Fault_Code6
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 7
                        If job2:Fault_Code7 <> job:Fault_Code7
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 8
                        If job2:Fault_Code8 <> job:Fault_Code8
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 9
                        If job2:Fault_Code9 <> job:Fault_Code9
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 10
                        If job2:Fault_Code10 <> job:Fault_Code10
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 11
                        If job2:Fault_Code11 <> job:Fault_Code11
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1

                    Of 12
                        If job2:Fault_Code12 <> job:Fault_Code12
                            inFaultsMatch# = 0
                        End !If job2:Fault_Code1 <> job:Fault_Code1
                    End !Case maf:Field_Number
                    IF (inFaultsMatch# = 0)
                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - Bounce Fault Don''t Match';ADD(glo:Queue19)
                        IF (man:BounceRulesType = 0)
                                    ! #12161 Only count bouncer if ALL the rules are met.
                                    ! This rule failed, so job is not a bouncer (Bryan: 29/06/2011)
                            CYCLE
                        ELSE
                            locFailedInFault = 2
                        END
                    END

                END ! IF (checkInFault# = 1)

                        !Is the infault excluded
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = job:Manufacturer
                mfo:Field_Number = maf:Field_Number
                Case maf:Field_Number
                Of 1
                    mfo:Field        = job2:Fault_Code1
                Of 2
                    mfo:Field        = job2:Fault_Code2
                Of 3
                    mfo:Field        = job2:Fault_Code3
                Of 4
                    mfo:Field        = job2:Fault_Code4
                Of 5
                    mfo:Field        = job2:Fault_Code5
                Of 6
                    mfo:Field        = job2:Fault_Code6
                Of 7
                    mfo:Field        = job2:Fault_Code7
                Of 8
                    mfo:Field        = job2:Fault_Code8
                Of 9
                    mfo:Field        = job2:Fault_Code9
                Of 10
                    mfo:Field        = job2:Fault_Code10
                Of 11
                    mfo:Field        = job2:Fault_Code11
                Of 12
                    mfo:Field        = job2:Fault_Code12
                End !Case maf:Field_Number
                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            !Found
                    If mfo:ExcludeFromBouncer
                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - In Fault Excluded';ADD(glo:Queue19)

                        Cycle
                    End !If mfo:ExcludeBouncer
                Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
                End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

!!TB13231 - J - 15/12/14 changing to use the maf:BouncerFault not the in Fault this is moved to near the if statement
!                    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!						 !now !If Access:MANFAULT.TryFetch(maf:BounceFaultKey) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>;ADD(glo:Queue19)
!                    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign

                    !Are the outfaults excluded
                IF (man:UseBouncerRules = 1 AND man:BouncerOutFault = 1 AND job:Warranty_Job = 'YES');ADD(glo:Queue19)
                        ! #11887 Manufacturer Bouncer Rules Apply.
                        ! Only Bounce If Same Out Fault (Bryan: 24/05/2011)
                    locFailedOutFault = 1 ! Out Fault Being Checked
                    outFaultsMatch# = 1
                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                    maf:Manufacturer = job:Manufacturer
                    maf:MainFault    = 1		!J - checking for outfault bouncer
                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                        Case maf:Field_Number
                        Of 1
                            If job2:Fault_Code1 <> job:Fault_Code1
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1
                        Of 2
                            If job2:Fault_Code2 <> job:Fault_Code2
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 3
                            If job2:Fault_Code3 <> job:Fault_Code3
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 4
                            If job2:Fault_Code4 <> job:Fault_Code4
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 5
                            If job2:Fault_Code5 <> job:Fault_Code5
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 6
                            If job2:Fault_Code6 <> job:Fault_Code6
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 7
                            If job2:Fault_Code7 <> job:Fault_Code7
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 8
                            If job2:Fault_Code8 <> job:Fault_Code8
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 9
                            If job2:Fault_Code9 <> job:Fault_Code9
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 10
                            If job2:Fault_Code10 <> job:Fault_Code10
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 11
                            If job2:Fault_Code11 <> job:Fault_Code11
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1

                        Of 12
                            If job2:Fault_Code12 <> job:Fault_Code12
                                outFaultsMatch# = 0
                            End !If job2:Fault_Code1 <> job:Fault_Code1
                        End !Case maf:Field_Number
                        IF (outFaultsMatch# = 0)
                           glo:Pointer19 = '# ' & job2:Ref_Number & ' - Outfault Doesn''t Match';ADD(glo:Queue19)
                            IF (man:BounceRulesType = 0)
                                    ! #12161 Only count bouncer if ALL the rules are met.
                                    ! This rule failed, so job is not a bouncer (Bryan: 29/06/2011)
                                CYCLE
                            ELSE
                                locFailedOutFault = 2
                            END
                        ELSE
                           glo:Pointer19 = 'OutFault: ' & EVALUATE('job:Fault_Code' & maf:Field_Number) & '   # ' & job2:Ref_Number & ' - Outfault:' & EVALUATE('job2:Fault_Code' & maf:Field_Number);ADD(glo:Queue19)
                        END

                    END
                END

                IF (man:UseBouncerRules = 1 AND man:BouncerSpares = 1 AND job:Warranty_Job = 'YES')
                        ! #11887 Manufacturer Bouncer Rules Apply.
                        ! Only Bounce If Spares Are The Same (Bryan: 24/05/2011)
                    locFailedParts = 1 ! Parts Being Checked
                    usedSameParts# = 0
                    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                    wpr:Ref_Number = job:Ref_Number
                    SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    LOOP UNTIL ACCESS:WARPARTS.Next()
                        IF (wpr:Ref_Number <> job:Ref_Number)
                            BREAK
                        END
                        IF (wpr:Part_Number = 'ADJUSTMENT')
                                ! #12187 Don't count adjustments as bouncers. (Bryan: 14/07/2011)
                            CYCLE
                        END
                        Access:WARPARTS_ALIAS.Clearkey(war_ali:Part_Number_Key)
                        war_ali:Ref_Number = job2:Ref_Number
                        war_ali:Part_Number = wpr:Part_Number
                        IF (Access:WARPARTS_ALIAS.TryFetch(war_ali:Part_Number_Key) = Level:Benign)
                            usedSameParts# = 1
                            BREAK
                        END
                    END
                    IF (usedSameParts# = 0)
                       glo:Pointer19 = '# ' & job2:Ref_Number & ' - Spares Don''t Match';ADD(glo:Queue19)
                        IF (man:BounceRulesType = 0)
                                ! #12161 Only count bouncer if ALL the rules are met.
                                ! This rule failed, so job is not a bouncer (Bryan: 29/06/2011)
                            CYCLE
                        ELSE
                            locFailedParts = 2
                        END

                    END

                END

                IF (man:BounceRulesType = 1)
                    IF (locFailedInFault <> 1 AND locFailedOutFault <> 1 AND locFailedParts <> 1)
                            ! Didn't match any of the criteria, so not a bouncer
                        CYCLE
                    END

                END

                IF (BouncerBit(0))
                        ! Call normal bouncer rules based on default dates
                END

            end !loop


        End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
    End !If CheckBouncer#
    DO CloseFiles
    IF NOT p_web &= NULL
       glo:Pointer19 = 'Records In Q = ' & RECORDS(qBouncedJobs)
    ELSE
       glo:Pointer19 = 'Records In Q = ' & RECORDS(glo:Queue20)
    END ! IF
        
    Return tmp:count
! Routines
OpenFiles           ROUTINE
    Relate:MANUFACT.Open()
    Relate:CHARTYPE.Open()
    Relate:REPTYDEF.Open()
    Relate:MANFAULT.Open()
    Relate:JOBS2_ALIAS.Open()
    Relate:MANFAULO.Open()
    Relate:WEBJOB.Open()
    Relate:JOBOUTFL.Open()
    Relate:WARPARTS.Open()
    Relate:MANFAUPA.Open()
    Relate:PARTS.Open()
    Relate:DEFAULTS.Open()
    Relate:WARPARTS_ALIAS.Open()
CloseFiles          ROUTINE
    Relate:MANUFACT.Close()
    Relate:CHARTYPE.Close()
    Relate:REPTYDEF.Close()
    Relate:MANFAULT.Close()
    Relate:JOBS2_ALIAS.Close()
    Relate:MANFAULO.Close()
    Relate:WEBJOB.Close()
    Relate:JOBOUTFL.Close()
    Relate:WARPARTS.Close()
    Relate:MANFAUPA.Close()
    Relate:PARTS.Close()
    Relate:DEFAULTS.Close()
    Relate:WARPARTS_ALIAS.Close()
! Local Procedures
BouncerBit          Procedure(BYTE fType)
CODE
    locBouncerType = GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')

    locBouncerTime = def:BouncerTime
    IF (man:UseBouncerRules = 1 AND job:Warranty_Job = 'YES')
            ! #11887 Manufacturer Bouncer Rules Apply.
            ! Use Manufacturer Bouncer Type (Bryan: 24/05/2011)
        locBouncerType = man:BouncerType
        locBouncerTime = man:BouncerPeriod
    END

    IF (fType = 1) ! User Manufacturer IMEI time
        locBouncerTime = man:BouncerPeriodIMEI
    END


    CASE locBouncerType
    Of 2 !Despatched Date
        If job2:Date_Despatched <> ''
            If job2:date_despatched + locBouncerTime > job:Date_Booked And job2:date_despatched <= job:Date_Booked
                tmp:count += 1

                IF NOT p_web &= NULL
                        ! #13231 Add to Q For SBOnline to use (DBH: 12/01/2015)
                    qBouncedJobs.SessionID = p_web.SessionID
                    qBouncedJobs.JobNumber = job2:Ref_Number
                    ADD(qBouncedJobs)
                ELSE
                    GLO:Pointer20 = job2:Ref_Number
                    ADd(glo:Queue20)                        
                END ! IF
                    
                Return 1
            End!If job2:date_booked + man:warranty_period < Today()

        Else !If job2:Date_Despatched <> ''

                !Need to get the webjob record for the bouncer job
                !Will save the file, and then restore it afterwards.
                !Therefore, no records should be lost.

            Save_wob_ID = Access:WEBJOB.SaveFile()

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job2:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Found

            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    !Error
            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign


            If wob:DateJobDespatched <> ''
                If wob:DateJobDespatched + locBouncerTime > job:Date_Booked And wob:DateJobDespatched <= job:Date_Booked

                    tmp:count += 1
                    IF NOT p_web &= NULL
                        ! #13231 Add to Q For SBOnline to use (DBH: 12/01/2015)
                        qBouncedJobs.SessionID = p_web.SessionID
                        qBouncedJobs.JobNumber = job2:Ref_Number
                        ADD(qBouncedJobs)
                    ELSE
                        GLO:Pointer20 = job2:Ref_Number
                        ADd(glo:Queue20)                        
                    END ! IF
                    Access:WEBJOB.RestoreFile(Save_wob_ID)
                    Return 1
                End!If job2:date_booked + man:warranty_period < Today()

            End !If wob:JobDateDespatched <> ''

            Access:WEBJOB.RestoreFile(Save_wob_ID)

        End !If job2:Date_Despatched <> ''
    Of 1 !Completed Date
        If job2:date_completed + locBouncerTime > job:Date_Booked And job2:date_completed <= job:Date_Booked
            tmp:count += 1
            IF NOT p_web &= NULL
                        ! #13231 Add to Q For SBOnline to use (DBH: 12/01/2015)
                qBouncedJobs.SessionID = p_web.SessionID
                qBouncedJobs.JobNumber = job2:Ref_Number
                ADD(qBouncedJobs)
            ELSE
                GLO:Pointer20 = job2:Ref_Number
                ADd(glo:Queue20)                        
            END ! IF
            Return 1
        End!If job2:date_booked + man:warranty_period < Today()
    Else !Booking Date
        If job2:date_booked + locBouncerTime > job:Date_Booked And job2:date_booked <= job:Date_Booked
            tmp:count += 1
            IF NOT p_web &= NULL
                        ! #13231 Add to Q For SBOnline to use (DBH: 12/01/2015)
                qBouncedJobs.SessionID = p_web.SessionID
                qBouncedJobs.JobNumber = job2:Ref_Number
                ADD(qBouncedJobs)
            ELSE
                GLO:Pointer20 = job2:Ref_Number
                ADd(glo:Queue20)                        
            END ! IF
            Return 1
        End!If job2:date_booked + man:warranty_period < Today()
    End !Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI;ADD(glo:Queue19)
    Return 0
OutFaultExcluded    Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob,STRING local:Manufacturer)
local:FaultCode         String(30)
Code
        !Loop through outfaults
    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
    joo:JobNumber = local:JobNumber
    Set(joo:JobNumberKey,joo:JobNumberKey)
    Loop
        If Access:JOBOUTFL.NEXT()
            Break
        End !If
        If joo:JobNumber <> local:JobNumber      |
            Then Break.  ! End If
            !Which is the main out fault
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = local:Manufacturer
        maf:MainFault    = 1
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Lookup the Fault Code lookup to see if it's excluded
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = local:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = joo:FaultCode
            Set(mfo:Field_Key,mfo:Field_Key)
            Loop
                If Access:MANFAULO.NEXT()
                    Break
                End !If
                If mfo:Manufacturer <> local:Manufacturer      |
                    Or mfo:Field_Number <> maf:Field_Number      |
                    Or mfo:Field        <> joo:FaultCode      |
                    Then Break.  ! End If
                If Clip(mfo:Description) = Clip(joo:Description)
                        !Make sure the descriptions match in case of duplicates
                    If mfo:ExcludeFromBouncer
                        Return Level:Fatal
                    End !If mfo:ExcludeFromBoucer
                    Break
                End !If Clip(mfo:Description) = Clip(joo:Description)
            End !Loop


        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

    End !Loop


        !Is an outfault records on parts for this manufacturer
    Access:MANFAUPA.ClearKey(map:MainFaultKey)
                    map:Manufacturer = local:Manufacturer
                    map:MainFault    = 1
                        If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                            !Found
                            !Loop through the parts as see if any of the faults codes are excluded
                            If local:WarrantyJob = 'YES'
                                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                                wpr:Ref_Number  = local:JobNumber
                                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                                Loop
                                    If Access:WARPARTS.NEXT()
                                        Break
                                    End !If
                                    If wpr:Ref_Number  <> local:JobNumber      |
                                        Then Break.  ! End If
                                    !Which is the main out fault
                                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                                    maf:Manufacturer = local:Manufacturer
                                    maf:MainFault    = 1
                                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                                        !Lookup the Fault Code lookup to see if it's excluded

                                        !Work out which part fault code is the outfault
                                        !and use that for the lookup
                                        Case map:Field_Number
                                        Of 1
                                            local:FaultCode        = wpr:Fault_Code1
                                        Of 2
                                            local:FaultCode        = wpr:Fault_Code2
                                        Of 3
                                            local:FaultCode        = wpr:Fault_Code3
                                        Of 4
                                            local:FaultCode        = wpr:Fault_Code4
                                        Of 5
                                            local:FaultCode        = wpr:Fault_Code5
                                        Of 6
                                            local:FaultCode        = wpr:Fault_Code6
                                        Of 7
                                            local:FaultCode        = wpr:Fault_Code7
                                        Of 8
                                            local:FaultCode        = wpr:Fault_Code8
                                        Of 9
                                            local:FaultCode        = wpr:Fault_Code9
                                        Of 10
                                            local:FaultCode        = wpr:Fault_Code10
                                        Of 11
                                            local:FaultCode        = wpr:Fault_Code11
                                        Of 12
                                            local:FaultCode        = wpr:Fault_Code12
                                        End !Case map:Field_Number


                                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                                        mfo:Manufacturer = local:Manufacturer
                                        mfo:Field_Number = maf:Field_Number
                                        mfo:Field        = local:FaultCode
                                        Set(mfo:Field_Key,mfo:Field_Key)
                                        Loop
                                            If Access:MANFAULO.NEXT()
                                                Break
                                            End !If
                                            If mfo:Manufacturer <> local:Manufacturer      |
                                                Or mfo:Field_Number <> maf:Field_Number      |
                                                Or mfo:Field        <> local:FaultCode      |
                                                Then Break.  ! End If
                                            !This fault relates to a specific part fault code number?
                                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                                If mfo:RelatedPartCode <> maf:Field_Number
                                                    Cycle
                                                End !If mfo:RelatedPartCode <> maf:Field_Number
                                            End !If mfo:RelatedPartCode <> 0
                                            IF mfo:ExcludeFromBouncer
                                                Return Level:Fatal
                                            End !IF mfo:ExcludeFromBouncer
                                        End !Loop


                                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                                        !Error
                                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                                End !Loop

                            End !If job:Warranty_Job = 'YES'

                            If local:ChargeableJob = 'YES'
                                !Loop through the parts as see if any of the faults codes are excluded

                                Access:PARTS.ClearKey(par:Part_Number_Key)
                                par:Ref_Number  = local:JobNumber
                                Set(par:Part_Number_Key,par:Part_Number_Key)
                                Loop
                                    If Access:PARTS.NEXT()
                                        Break
                                    End !If
                                    If par:Ref_Number  <> local:JobNumber      |
                                        Then Break.  ! End If
                                    !Which is the main out fault
                                    Access:MANFAULT.ClearKey(maf:MainFaultKey)
                                    maf:Manufacturer = local:Manufacturer
                                    maf:MainFault    = 1
                                    If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                                        !Lookup the Fault Code lookup to see if it's excluded

                                        !Work out which part fault code is the outfault
                                        !and use that for the lookup
                                        Case map:Field_Number
                                        Of 1
                                            local:FaultCode        = par:Fault_Code1
                                        Of 2
                                            local:FaultCode        = par:Fault_Code2
                                        Of 3
                                            local:FaultCode        = par:Fault_Code3
                                        Of 4
                                            local:FaultCode        = par:Fault_Code4
                                        Of 5
                                            local:FaultCode        = par:Fault_Code5
                                        Of 6
                                            local:FaultCode        = par:Fault_Code6
                                        Of 7
                                            local:FaultCode        = par:Fault_Code7
                                        Of 8
                                            local:FaultCode        = par:Fault_Code8
                                        Of 9
                                            local:FaultCode        = par:Fault_Code9
                                        Of 10
                                            local:FaultCode        = par:Fault_Code10
                                        Of 11
                                            local:FaultCode        = par:Fault_Code11
                                        Of 12
                                            local:FaultCode        = par:Fault_Code12
                                        End !Case map:Field_Number


                                        Access:MANFAULO.ClearKey(mfo:Field_Key)
                                        mfo:Manufacturer = local:Manufacturer
                                        mfo:Field_Number = maf:Field_Number
                                        mfo:Field        = local:FaultCode
                                        Set(mfo:Field_Key,mfo:Field_Key)
                                        Loop
                                            If Access:MANFAULO.NEXT()
                                                Break
                                            End !If
                                            If mfo:Manufacturer <> local:Manufacturer      |
                                                Or mfo:Field_Number <> maf:Field_Number      |
                                                Or mfo:Field        <> local:FaultCode      |
                                                Then Break.  ! End If
                                            !This fault relates to a specific part fault code number?
                                            If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
                                                If mfo:RelatedPartCode <> maf:Field_Number
                                                    Cycle
                                                End !If mfo:RelatedPartCode <> maf:Field_Number
                                            End !If mfo:RelatedPartCode <> 0
                                            IF mfo:ExcludeFromBouncer
                                                Return Level:Fatal
                                            End !IF mfo:ExcludeFromBouncer
                                        End !Loop


                                    Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                                        !Error
                                    End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

                                End !Loop

                            End !If job:Chargeable_Job = 'YES'
                        Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                            !Error
                        End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign

                        Return Level:Benign

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)

  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)

  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE)

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

