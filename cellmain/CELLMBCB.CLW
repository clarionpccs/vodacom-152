  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBCB:DctInit    PROCEDURE
CELLMBCB:DctKill    PROCEDURE
CELLMBCB:FilesInit  PROCEDURE
  END

Hide:Access:STATCRIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STATCRIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGDESNO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:LOGDESNO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CPNDPRTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CPNDPRTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGSALCD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGSALCD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFSTOCK CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:DEFSTOCK CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBLOHIS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBLOHIS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORACLEEX CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORACLEEX CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOAN     CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOAN     CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:NOTESCON CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:NOTESCON CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:VATCODE  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:VATCODE  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SMSMAIL  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:SMSMAIL  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOANHIST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOANHIST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TURNARND CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TURNARND CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDJOBS  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORDJOBS  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MERGETXT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MERGETXT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MERGELET CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MERGELET CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDSTOCK CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ORDSTOCK CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXCHHIST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXCHHIST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ACCESSOR CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ACCESSOR CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBACCAD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUBACCAD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBCB:DctInit PROCEDURE
  CODE
  Relate:STATCRIT &= Hide:Relate:STATCRIT
  Relate:LOGDESNO &= Hide:Relate:LOGDESNO
  Relate:CPNDPRTS &= Hide:Relate:CPNDPRTS
  Relate:LOGSALCD &= Hide:Relate:LOGSALCD
  Relate:DEFSTOCK &= Hide:Relate:DEFSTOCK
  Relate:JOBLOHIS &= Hide:Relate:JOBLOHIS
  Relate:ORACLEEX &= Hide:Relate:ORACLEEX
  Relate:LOAN &= Hide:Relate:LOAN
  Relate:NOTESCON &= Hide:Relate:NOTESCON
  Relate:VATCODE &= Hide:Relate:VATCODE
  Relate:SMSMAIL &= Hide:Relate:SMSMAIL
  Relate:LOANHIST &= Hide:Relate:LOANHIST
  Relate:TURNARND &= Hide:Relate:TURNARND
  Relate:ORDJOBS &= Hide:Relate:ORDJOBS
  Relate:MERGETXT &= Hide:Relate:MERGETXT
  Relate:MERGELET &= Hide:Relate:MERGELET
  Relate:ORDSTOCK &= Hide:Relate:ORDSTOCK
  Relate:EXCHHIST &= Hide:Relate:EXCHHIST
  Relate:ACCESSOR &= Hide:Relate:ACCESSOR
  Relate:SUBACCAD &= Hide:Relate:SUBACCAD

CELLMBCB:FilesInit PROCEDURE
  CODE
  Hide:Relate:STATCRIT.Init
  Hide:Relate:LOGDESNO.Init
  Hide:Relate:CPNDPRTS.Init
  Hide:Relate:LOGSALCD.Init
  Hide:Relate:DEFSTOCK.Init
  Hide:Relate:JOBLOHIS.Init
  Hide:Relate:ORACLEEX.Init
  Hide:Relate:LOAN.Init
  Hide:Relate:NOTESCON.Init
  Hide:Relate:VATCODE.Init
  Hide:Relate:SMSMAIL.Init
  Hide:Relate:LOANHIST.Init
  Hide:Relate:TURNARND.Init
  Hide:Relate:ORDJOBS.Init
  Hide:Relate:MERGETXT.Init
  Hide:Relate:MERGELET.Init
  Hide:Relate:ORDSTOCK.Init
  Hide:Relate:EXCHHIST.Init
  Hide:Relate:ACCESSOR.Init
  Hide:Relate:SUBACCAD.Init


CELLMBCB:DctKill PROCEDURE
  CODE
  Hide:Relate:STATCRIT.Kill
  Hide:Relate:LOGDESNO.Kill
  Hide:Relate:CPNDPRTS.Kill
  Hide:Relate:LOGSALCD.Kill
  Hide:Relate:DEFSTOCK.Kill
  Hide:Relate:JOBLOHIS.Kill
  Hide:Relate:ORACLEEX.Kill
  Hide:Relate:LOAN.Kill
  Hide:Relate:NOTESCON.Kill
  Hide:Relate:VATCODE.Kill
  Hide:Relate:SMSMAIL.Kill
  Hide:Relate:LOANHIST.Kill
  Hide:Relate:TURNARND.Kill
  Hide:Relate:ORDJOBS.Kill
  Hide:Relate:MERGETXT.Kill
  Hide:Relate:MERGELET.Kill
  Hide:Relate:ORDSTOCK.Kill
  Hide:Relate:EXCHHIST.Kill
  Hide:Relate:ACCESSOR.Kill
  Hide:Relate:SUBACCAD.Kill


Hide:Access:STATCRIT.Init PROCEDURE
  CODE
  SELF.Init(STATCRIT,GlobalErrors)
  SELF.Buffer &= stac:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stac:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(stac:DescriptionKey,'By Description',0)
  SELF.AddKey(stac:DescriptionOptionKey,'By FieldValue',0)
  SELF.AddKey(stac:LocationDescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:STATCRIT &= SELF


Hide:Relate:STATCRIT.Init PROCEDURE
  CODE
  Hide:Access:STATCRIT.Init
  SELF.Init(Access:STATCRIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STATREP)


Hide:Access:STATCRIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STATCRIT &= NULL


Hide:Relate:STATCRIT.Kill PROCEDURE

  CODE
  Hide:Access:STATCRIT.Kill
  PARENT.Kill
  Relate:STATCRIT &= NULL


Hide:Access:LOGDESNO.Init PROCEDURE
  CODE
  SELF.Init(LOGDESNO,GlobalErrors)
  SELF.Buffer &= ldes:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ldes:DespatchNoKey,'By Despatch Number',1)
  SELF.LazyOpen = False
  Access:LOGDESNO &= SELF


Hide:Relate:LOGDESNO.Init PROCEDURE
  CODE
  Hide:Access:LOGDESNO.Init
  SELF.Init(Access:LOGDESNO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGSTHIS)


Hide:Access:LOGDESNO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGDESNO &= NULL


Hide:Access:LOGDESNO.PrimeFields PROCEDURE

  CODE
  ldes:Date = Today()
  ldes:Time = clock()
  PARENT.PrimeFields


Hide:Relate:LOGDESNO.Kill PROCEDURE

  CODE
  Hide:Access:LOGDESNO.Kill
  PARENT.Kill
  Relate:LOGDESNO &= NULL


Hide:Access:CPNDPRTS.Init PROCEDURE
  CODE
  SELF.Init(CPNDPRTS,GlobalErrors)
  SELF.Buffer &= tmppen:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tmppen:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(tmppen:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:CPNDPRTS &= SELF


Hide:Relate:CPNDPRTS.Init PROCEDURE
  CODE
  Hide:Access:CPNDPRTS.Init
  SELF.Init(Access:CPNDPRTS,1)


Hide:Access:CPNDPRTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CPNDPRTS &= NULL


Hide:Relate:CPNDPRTS.Kill PROCEDURE

  CODE
  Hide:Access:CPNDPRTS.Kill
  PARENT.Kill
  Relate:CPNDPRTS &= NULL


Hide:Access:LOGSALCD.Init PROCEDURE
  CODE
  SELF.Init(LOGSALCD,GlobalErrors)
  SELF.Buffer &= logsal:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(logsal:RefNumberKey,'By Ref Number',1)
  SELF.AddKey(logsal:SalesCodeKey,'By Sales Code',0)
  SELF.LazyOpen = False
  Access:LOGSALCD &= SELF


Hide:Relate:LOGSALCD.Init PROCEDURE
  CODE
  Hide:Access:LOGSALCD.Init
  SELF.Init(Access:LOGSALCD,1)


Hide:Access:LOGSALCD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGSALCD &= NULL


Hide:Relate:LOGSALCD.Kill PROCEDURE

  CODE
  Hide:Access:LOGSALCD.Kill
  PARENT.Kill
  Relate:LOGSALCD &= NULL


Hide:Access:DEFSTOCK.Init PROCEDURE
  CODE
  SELF.Init(DEFSTOCK,GlobalErrors)
  SELF.Buffer &= dst:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dst:record_number_key,'dst:record_number_key',1)
  SELF.LazyOpen = False
  Access:DEFSTOCK &= SELF


Hide:Relate:DEFSTOCK.Init PROCEDURE
  CODE
  Hide:Access:DEFSTOCK.Init
  SELF.Init(Access:DEFSTOCK,1)


Hide:Access:DEFSTOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFSTOCK &= NULL


Hide:Access:DEFSTOCK.PrimeFields PROCEDURE

  CODE
  dst:Use_Site_Location = 'NO'
  PARENT.PrimeFields


Hide:Relate:DEFSTOCK.Kill PROCEDURE

  CODE
  Hide:Access:DEFSTOCK.Kill
  PARENT.Kill
  Relate:DEFSTOCK &= NULL


Hide:Access:JOBLOHIS.Init PROCEDURE
  CODE
  SELF.Init(JOBLOHIS,GlobalErrors)
  SELF.Buffer &= jlh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jlh:Ref_Number_Key,'By Date',0)
  SELF.AddKey(jlh:record_number_key,'jlh:record_number_key',1)
  SELF.LazyOpen = False
  Access:JOBLOHIS &= SELF


Hide:Relate:JOBLOHIS.Init PROCEDURE
  CODE
  Hide:Access:JOBLOHIS.Init
  SELF.Init(Access:JOBLOHIS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBLOHIS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBLOHIS &= NULL


Hide:Relate:JOBLOHIS.Kill PROCEDURE

  CODE
  Hide:Access:JOBLOHIS.Kill
  PARENT.Kill
  Relate:JOBLOHIS &= NULL


Hide:Access:ORACLEEX.Init PROCEDURE
  CODE
  SELF.Init(ORACLEEX,GlobalErrors)
  SELF.Buffer &= ora:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ora:RecordNumberKey,'By Record Number',1)
  SELF.LazyOpen = False
  Access:ORACLEEX &= SELF


Hide:Relate:ORACLEEX.Init PROCEDURE
  CODE
  Hide:Access:ORACLEEX.Init
  SELF.Init(Access:ORACLEEX,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ORACLECN,RI:CASCADE,RI:CASCADE,orac:OracleNumberKey)
  SELF.AddRelationLink(ora:RecordNumber,orac:OracleNumber)


Hide:Access:ORACLEEX.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORACLEEX &= NULL


Hide:Access:ORACLEEX.PrimeFields PROCEDURE

  CODE
  ora:TheDate = Today()
  ora:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:ORACLEEX.Kill PROCEDURE

  CODE
  Hide:Access:ORACLEEX.Kill
  PARENT.Kill
  Relate:ORACLEEX &= NULL


Hide:Access:LOAN.Init PROCEDURE
  CODE
  SELF.Init(LOAN,GlobalErrors)
  SELF.Buffer &= loa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(loa:Ref_Number_Key,'By Loan Unit Number',1)
  SELF.AddKey(loa:ESN_Only_Key,'loa:ESN_Only_Key',0)
  SELF.AddKey(loa:MSN_Only_Key,'loa:MSN_Only_Key',0)
  SELF.AddKey(loa:Ref_Number_Stock_Key,'By Ref Number',0)
  SELF.AddKey(loa:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(loa:ESN_Key,'By E.S.N. / I.M.E.I. Number',0)
  SELF.AddKey(loa:MSN_Key,'By M.S.N. Number',0)
  SELF.AddKey(loa:ESN_Available_Key,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(loa:MSN_Available_Key,'By M.S.N.',0)
  SELF.AddKey(loa:Ref_Available_Key,'By Loan Unit Number',0)
  SELF.AddKey(loa:Model_Available_Key,'By Model Number',0)
  SELF.AddKey(loa:Stock_Type_Key,'By Stock Type',0)
  SELF.AddKey(loa:ModelRefNoKey,'By Unit Number',0)
  SELF.AddKey(loa:AvailIMEIOnlyKey,'By I.M.E.I. Number',0)
  SELF.AddKey(loa:AvailMSNOnlyKey,'By M.S.N',0)
  SELF.AddKey(loa:AvailRefOnlyKey,'By Unit Number',0)
  SELF.AddKey(loa:AvailModOnlyKey,'By Unit Number',0)
  SELF.AddKey(loa:DateBookedKey,'By Date Booked',0)
  SELF.AddKey(loa:LocStockAvailIMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(loa:LocStockIMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(loa:LocIMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(loa:LocStockAvailRefKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocStockRefKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocRefKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocStockAvailModelKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocStockModelKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocModelKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocStockAvailMSNKey,'By M.S.N.',0)
  SELF.AddKey(loa:LocStockMSNKey,'By Loan Unit Number',0)
  SELF.AddKey(loa:LocMSNKey,'By M.S.N.',0)
  SELF.AddKey(loa:AvailLocIMEI,'loa:AvailLocIMEI',0)
  SELF.AddKey(loa:AvailLocMSN,'loa:AvailLocMSN',0)
  SELF.AddKey(loa:AvailLocRef,'loa:AvailLocRef',0)
  SELF.AddKey(loa:AvailLocModel,'loa:AvailLocModel',0)
  SELF.AddKey(loa:InTransitLocationKey,'By IMEI Number',0)
  SELF.AddKey(loa:InTransitKey,'By IMEI Number',0)
  SELF.AddKey(loa:StatusChangeDateKey,'By Status Change Date',0)
  SELF.AddKey(loa:LocStatusChangeDateKey,'By Status Change Date',0)
  SELF.LazyOpen = False
  Access:LOAN &= SELF


Hide:Relate:LOAN.Init PROCEDURE
  CODE
  Hide:Access:LOAN.Init
  SELF.Init(Access:LOAN,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOANAUI,RI:CASCADE,RI:CASCADE,lau:Ref_Number_Key)
  SELF.AddRelationLink(loa:Ref_Number,lau:Ref_Number)
  SELF.AddRelation(Relate:STOCKTYP)
  SELF.AddRelation(Relate:LOANHIST,RI:CASCADE,RI:CASCADE,loh:Ref_Number_Key)
  SELF.AddRelationLink(loa:Ref_Number,loh:Ref_Number)
  SELF.AddRelation(Relate:LOANACC,RI:CASCADE,RI:CASCADE,lac:Ref_Number_Key)
  SELF.AddRelationLink(loa:Ref_Number,lac:Ref_Number)


Hide:Access:LOAN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOAN &= NULL


Hide:Relate:LOAN.Kill PROCEDURE

  CODE
  Hide:Access:LOAN.Kill
  PARENT.Kill
  Relate:LOAN &= NULL


Hide:Access:NOTESCON.Init PROCEDURE
  CODE
  SELF.Init(NOTESCON,GlobalErrors)
  SELF.Buffer &= noc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(noc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(noc:Notes_Key,'By Notes',0)
  SELF.LazyOpen = False
  Access:NOTESCON &= SELF


Hide:Relate:NOTESCON.Init PROCEDURE
  CODE
  Hide:Access:NOTESCON.Init
  SELF.Init(Access:NOTESCON,1)


Hide:Access:NOTESCON.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:NOTESCON &= NULL


Hide:Relate:NOTESCON.Kill PROCEDURE

  CODE
  Hide:Access:NOTESCON.Kill
  PARENT.Kill
  Relate:NOTESCON &= NULL


Hide:Access:VATCODE.Init PROCEDURE
  CODE
  SELF.Init(VATCODE,GlobalErrors)
  SELF.Buffer &= vat:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(vat:Vat_code_Key,'By V.A.T. Code',0)
  SELF.LazyOpen = False
  Access:VATCODE &= SELF


Hide:Relate:VATCODE.Init PROCEDURE
  CODE
  Hide:Access:VATCODE.Init
  SELF.Init(Access:VATCODE,1)


Hide:Access:VATCODE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:VATCODE &= NULL


Hide:Relate:VATCODE.Kill PROCEDURE

  CODE
  Hide:Access:VATCODE.Kill
  PARENT.Kill
  Relate:VATCODE &= NULL


Hide:Access:SMSMAIL.Init PROCEDURE
  CODE
  SELF.Init(SMSMAIL,GlobalErrors)
  SELF.Buffer &= sms:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sms:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sms:SMSSentKey,'SMS Sent Key',0)
  SELF.AddKey(sms:EmailSentKey,'By Email Sent',0)
  SELF.AddKey(sms:SMSSBUpdateKey,'sms:SMSSBUpdateKey',0)
  SELF.AddKey(sms:EmailSBUpdateKey,'sms:EmailSBUpdateKey',0)
  SELF.AddKey(sms:DateTimeInsertedKey,'By Date Time',0)
  SELF.AddKey(sms:KeyMSISDN_DateDec,'sms:KeyMSISDN_DateDec',0)
  SELF.AddKey(sms:KeyRefDateTimeDec,'sms:KeyRefDateTimeDec',0)
  SELF.LazyOpen = False
  Access:SMSMAIL &= SELF


Hide:Relate:SMSMAIL.Init PROCEDURE
  CODE
  Hide:Access:SMSMAIL.Init
  SELF.Init(Access:SMSMAIL,1)


Hide:Access:SMSMAIL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SMSMAIL &= NULL


Hide:Access:SMSMAIL.PrimeFields PROCEDURE

  CODE
  sms:SendToSMS = 'F'
  sms:SendToEmail = 'F'
  sms:SMSSent = 'F'
  sms:EmailSent = 'F'
  sms:SBUpdated = 0
  sms:SMSType = 'N'
  PARENT.PrimeFields


Hide:Relate:SMSMAIL.Kill PROCEDURE

  CODE
  Hide:Access:SMSMAIL.Kill
  PARENT.Kill
  Relate:SMSMAIL &= NULL


Hide:Access:LOANHIST.Init PROCEDURE
  CODE
  SELF.Init(LOANHIST,GlobalErrors)
  SELF.Buffer &= loh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(loh:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(loh:Ref_Number_Key,'By Ref Number',0)
  SELF.LazyOpen = False
  Access:LOANHIST &= SELF


Hide:Relate:LOANHIST.Init PROCEDURE
  CODE
  Hide:Access:LOANHIST.Init
  SELF.Init(Access:LOANHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOAN)


Hide:Access:LOANHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOANHIST &= NULL


Hide:Relate:LOANHIST.Kill PROCEDURE

  CODE
  Hide:Access:LOANHIST.Kill
  PARENT.Kill
  Relate:LOANHIST &= NULL


Hide:Access:TURNARND.Init PROCEDURE
  CODE
  SELF.Init(TURNARND,GlobalErrors)
  SELF.Buffer &= tur:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tur:Turnaround_Time_Key,'By Turnaround Time',0)
  SELF.LazyOpen = False
  Access:TURNARND &= SELF


Hide:Relate:TURNARND.Init PROCEDURE
  CODE
  Hide:Access:TURNARND.Init
  SELF.Init(Access:TURNARND,1)


Hide:Access:TURNARND.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TURNARND &= NULL


Hide:Relate:TURNARND.Kill PROCEDURE

  CODE
  Hide:Access:TURNARND.Kill
  PARENT.Kill
  Relate:TURNARND &= NULL


Hide:Access:ORDJOBS.Init PROCEDURE
  CODE
  SELF.Init(ORDJOBS,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= orjtmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(orjtmp:RecordNumberKey,'Record Number',1)
  SELF.AddKey(orjtmp:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:ORDJOBS &= SELF


Hide:Relate:ORDJOBS.Init PROCEDURE
  CODE
  Hide:Access:ORDJOBS.Init
  SELF.Init(Access:ORDJOBS,1)


Hide:Access:ORDJOBS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDJOBS &= NULL


Hide:Access:ORDJOBS.PrimeFields PROCEDURE

  CODE
  orjtmp:CharWarr = 'C'
  PARENT.PrimeFields


Hide:Relate:ORDJOBS.Kill PROCEDURE

  CODE
  Hide:Access:ORDJOBS.Kill
  PARENT.Kill
  Relate:ORDJOBS &= NULL


Hide:Access:MERGETXT.Init PROCEDURE
  CODE
  SELF.Init(MERGETXT,GlobalErrors)
  SELF.Buffer &= mrt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mrt:RefNumberKey,'By Ref Number',0)
  SELF.LazyOpen = False
  Access:MERGETXT &= SELF


Hide:Relate:MERGETXT.Init PROCEDURE
  CODE
  Hide:Access:MERGETXT.Init
  SELF.Init(Access:MERGETXT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MERGELET)


Hide:Access:MERGETXT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MERGETXT &= NULL


Hide:Relate:MERGETXT.Kill PROCEDURE

  CODE
  Hide:Access:MERGETXT.Kill
  PARENT.Kill
  Relate:MERGETXT &= NULL


Hide:Access:MERGELET.Init PROCEDURE
  CODE
  SELF.Init(MERGELET,GlobalErrors)
  SELF.Buffer &= mrg:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mrg:RefNumberKey,'By Ref Number',1)
  SELF.AddKey(mrg:LetterNameKey,'By Letter Name',0)
  SELF.LazyOpen = False
  Access:MERGELET &= SELF


Hide:Relate:MERGELET.Init PROCEDURE
  CODE
  Hide:Access:MERGELET.Init
  SELF.Init(Access:MERGELET,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MERGETXT,RI:CASCADE,RI:CASCADE,mrt:RefNumberKey)
  SELF.AddRelationLink(mrg:RefNumber,mrt:RefNumber)


Hide:Access:MERGELET.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MERGELET &= NULL


Hide:Access:MERGELET.PrimeFields PROCEDURE

  CODE
  mrg:UseStatus = 'NO'
  PARENT.PrimeFields


Hide:Relate:MERGELET.Kill PROCEDURE

  CODE
  Hide:Access:MERGELET.Kill
  PARENT.Kill
  Relate:MERGELET &= NULL


Hide:Access:ORDSTOCK.Init PROCEDURE
  CODE
  SELF.Init(ORDSTOCK,GlobalErrors)
  SELF.FileName &= glo:File_name2
  SELF.Buffer &= orstmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(orstmp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(orstmp:LocationKey,'By Location',0)
  SELF.LazyOpen = False
  Access:ORDSTOCK &= SELF


Hide:Relate:ORDSTOCK.Init PROCEDURE
  CODE
  Hide:Access:ORDSTOCK.Init
  SELF.Init(Access:ORDSTOCK,1)


Hide:Access:ORDSTOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDSTOCK &= NULL


Hide:Relate:ORDSTOCK.Kill PROCEDURE

  CODE
  Hide:Access:ORDSTOCK.Kill
  PARENT.Kill
  Relate:ORDSTOCK &= NULL


Hide:Access:EXCHHIST.Init PROCEDURE
  CODE
  SELF.Init(EXCHHIST,GlobalErrors)
  SELF.Buffer &= exh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(exh:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(exh:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(exh:DateStatusKey,'By Ref Number',0)
  SELF.LazyOpen = False
  Access:EXCHHIST &= SELF


Hide:Relate:EXCHHIST.Init PROCEDURE
  CODE
  Hide:Access:EXCHHIST.Init
  SELF.Init(Access:EXCHHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:EXCHANGE)


Hide:Access:EXCHHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXCHHIST &= NULL


Hide:Relate:EXCHHIST.Kill PROCEDURE

  CODE
  Hide:Access:EXCHHIST.Kill
  PARENT.Kill
  Relate:EXCHHIST &= NULL


Hide:Access:ACCESSOR.Init PROCEDURE
  CODE
  SELF.Init(ACCESSOR,GlobalErrors)
  SELF.Buffer &= acr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(acr:Accesory_Key,'By Accessory',0)
  SELF.AddKey(acr:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(acr:AccessOnlyKey,'By Accessory',0)
  SELF.LazyOpen = False
  Access:ACCESSOR &= SELF


Hide:Relate:ACCESSOR.Init PROCEDURE
  CODE
  Hide:Access:ACCESSOR.Init
  SELF.Init(Access:ACCESSOR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ACCESDEF)
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:ACCESSOR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ACCESSOR &= NULL


Hide:Relate:ACCESSOR.Kill PROCEDURE

  CODE
  Hide:Access:ACCESSOR.Kill
  PARENT.Kill
  Relate:ACCESSOR &= NULL


Hide:Access:SUBACCAD.Init PROCEDURE
  CODE
  SELF.Init(SUBACCAD,GlobalErrors)
  SELF.Buffer &= sua:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sua:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sua:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(sua:CompanyNameKey,'By Company Name',0)
  SELF.AddKey(sua:AccountNumberOnlyKey,'By Account Number',0)
  SELF.LazyOpen = False
  Access:SUBACCAD &= SELF


Hide:Relate:SUBACCAD.Init PROCEDURE
  CODE
  Hide:Access:SUBACCAD.Init
  SELF.Init(Access:SUBACCAD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUBTRACC)


Hide:Access:SUBACCAD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBACCAD &= NULL


Hide:Relate:SUBACCAD.Kill PROCEDURE

  CODE
  Hide:Access:SUBACCAD.Kill
  PARENT.Kill
  Relate:SUBACCAD &= NULL

