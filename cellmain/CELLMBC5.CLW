  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBC5:DctInit    PROCEDURE
CELLMBC5:DctKill    PROCEDURE
CELLMBC5:FilesInit  PROCEDURE
  END

Hide:Access:CONSIGN  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CONSIGN  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:HANDOJOB CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:HANDOJOB CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBEXACC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBEXACC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODELCOL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODELCOL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COMMCAT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:COMMCAT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MESSAGES CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MESSAGES CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGEXHE  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGEXHE  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFCRC   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:DEFCRC   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PAYTYPES CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PAYTYPES CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COMMONWP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COMMONWP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOGEXCH  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOGEXCH  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:QAREASON CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:QAREASON CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COMMONCP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COMMONCP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ADDSEARCH CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ADDSEARCH CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSSL   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSSL   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CONTHIST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:CONTHIST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CONTACTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CONTACTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPJOBS  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPJOBS  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPAUDIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPAUDIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBBATCH CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBBATCH CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBC5:DctInit PROCEDURE
  CODE
  Relate:CONSIGN &= Hide:Relate:CONSIGN
  Relate:HANDOJOB &= Hide:Relate:HANDOJOB
  Relate:JOBEXACC &= Hide:Relate:JOBEXACC
  Relate:MODELCOL &= Hide:Relate:MODELCOL
  Relate:COMMCAT &= Hide:Relate:COMMCAT
  Relate:MESSAGES &= Hide:Relate:MESSAGES
  Relate:LOGEXHE &= Hide:Relate:LOGEXHE
  Relate:DEFCRC &= Hide:Relate:DEFCRC
  Relate:PAYTYPES &= Hide:Relate:PAYTYPES
  Relate:COMMONWP &= Hide:Relate:COMMONWP
  Relate:LOGEXCH &= Hide:Relate:LOGEXCH
  Relate:QAREASON &= Hide:Relate:QAREASON
  Relate:COMMONCP &= Hide:Relate:COMMONCP
  Relate:ADDSEARCH &= Hide:Relate:ADDSEARCH
  Relate:JOBSSL &= Hide:Relate:JOBSSL
  Relate:CONTHIST &= Hide:Relate:CONTHIST
  Relate:CONTACTS &= Hide:Relate:CONTACTS
  Relate:EXPJOBS &= Hide:Relate:EXPJOBS
  Relate:EXPAUDIT &= Hide:Relate:EXPAUDIT
  Relate:JOBBATCH &= Hide:Relate:JOBBATCH

CELLMBC5:FilesInit PROCEDURE
  CODE
  Hide:Relate:CONSIGN.Init
  Hide:Relate:HANDOJOB.Init
  Hide:Relate:JOBEXACC.Init
  Hide:Relate:MODELCOL.Init
  Hide:Relate:COMMCAT.Init
  Hide:Relate:MESSAGES.Init
  Hide:Relate:LOGEXHE.Init
  Hide:Relate:DEFCRC.Init
  Hide:Relate:PAYTYPES.Init
  Hide:Relate:COMMONWP.Init
  Hide:Relate:LOGEXCH.Init
  Hide:Relate:QAREASON.Init
  Hide:Relate:COMMONCP.Init
  Hide:Relate:ADDSEARCH.Init
  Hide:Relate:JOBSSL.Init
  Hide:Relate:CONTHIST.Init
  Hide:Relate:CONTACTS.Init
  Hide:Relate:EXPJOBS.Init
  Hide:Relate:EXPAUDIT.Init
  Hide:Relate:JOBBATCH.Init


CELLMBC5:DctKill PROCEDURE
  CODE
  Hide:Relate:CONSIGN.Kill
  Hide:Relate:HANDOJOB.Kill
  Hide:Relate:JOBEXACC.Kill
  Hide:Relate:MODELCOL.Kill
  Hide:Relate:COMMCAT.Kill
  Hide:Relate:MESSAGES.Kill
  Hide:Relate:LOGEXHE.Kill
  Hide:Relate:DEFCRC.Kill
  Hide:Relate:PAYTYPES.Kill
  Hide:Relate:COMMONWP.Kill
  Hide:Relate:LOGEXCH.Kill
  Hide:Relate:QAREASON.Kill
  Hide:Relate:COMMONCP.Kill
  Hide:Relate:ADDSEARCH.Kill
  Hide:Relate:JOBSSL.Kill
  Hide:Relate:CONTHIST.Kill
  Hide:Relate:CONTACTS.Kill
  Hide:Relate:EXPJOBS.Kill
  Hide:Relate:EXPAUDIT.Kill
  Hide:Relate:JOBBATCH.Kill


Hide:Access:CONSIGN.Init PROCEDURE
  CODE
  SELF.Init(CONSIGN,GlobalErrors)
  SELF.Buffer &= cns:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cns:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(cns:Consignment_Number_Key,'By Consignment Number',0)
  SELF.LazyOpen = False
  Access:CONSIGN &= SELF


Hide:Relate:CONSIGN.Init PROCEDURE
  CODE
  Hide:Access:CONSIGN.Init
  SELF.Init(Access:CONSIGN,1)


Hide:Access:CONSIGN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONSIGN &= NULL


Hide:Relate:CONSIGN.Kill PROCEDURE

  CODE
  Hide:Access:CONSIGN.Kill
  PARENT.Kill
  Relate:CONSIGN &= NULL


Hide:Access:HANDOJOB.Init PROCEDURE
  CODE
  SELF.Init(HANDOJOB,GlobalErrors)
  SELF.Buffer &= haj:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(haj:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(haj:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:HANDOJOB &= SELF


Hide:Relate:HANDOJOB.Init PROCEDURE
  CODE
  Hide:Access:HANDOJOB.Init
  SELF.Init(Access:HANDOJOB,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:HANDOVER)


Hide:Access:HANDOJOB.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:HANDOJOB &= NULL


Hide:Relate:HANDOJOB.Kill PROCEDURE

  CODE
  Hide:Access:HANDOJOB.Kill
  PARENT.Kill
  Relate:HANDOJOB &= NULL


Hide:Access:JOBEXACC.Init PROCEDURE
  CODE
  SELF.Init(JOBEXACC,GlobalErrors)
  SELF.Buffer &= jea:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jea:Record_Number_Key,'jea:Record_Number_Key',1)
  SELF.AddKey(jea:Part_Number_Key,'jea:Part_Number_Key',0)
  SELF.LazyOpen = False
  Access:JOBEXACC &= SELF


Hide:Relate:JOBEXACC.Init PROCEDURE
  CODE
  Hide:Access:JOBEXACC.Init
  SELF.Init(Access:JOBEXACC,1)


Hide:Access:JOBEXACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBEXACC &= NULL


Hide:Relate:JOBEXACC.Kill PROCEDURE

  CODE
  Hide:Access:JOBEXACC.Kill
  PARENT.Kill
  Relate:JOBEXACC &= NULL


Hide:Access:MODELCOL.Init PROCEDURE
  CODE
  SELF.Init(MODELCOL,GlobalErrors)
  SELF.Buffer &= moc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(moc:Record_Number_Key,'moc:Record_Number_Key',1)
  SELF.AddKey(moc:Colour_Key,'By Colour',0)
  SELF.LazyOpen = False
  Access:MODELCOL &= SELF


Hide:Relate:MODELCOL.Init PROCEDURE
  CODE
  Hide:Access:MODELCOL.Init
  SELF.Init(Access:MODELCOL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODELCOL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODELCOL &= NULL


Hide:Relate:MODELCOL.Kill PROCEDURE

  CODE
  Hide:Access:MODELCOL.Kill
  PARENT.Kill
  Relate:MODELCOL &= NULL


Hide:Access:COMMCAT.Init PROCEDURE
  CODE
  SELF.Init(COMMCAT,GlobalErrors)
  SELF.Buffer &= cmc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cmc:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(cmc:Category_Key,'By Category',0)
  SELF.LazyOpen = False
  Access:COMMCAT &= SELF


Hide:Relate:COMMCAT.Init PROCEDURE
  CODE
  Hide:Access:COMMCAT.Init
  SELF.Init(Access:COMMCAT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COMMONFA,RI:CASCADE,RI:RESTRICT,com:Description_Key)
  SELF.AddRelationLink(cmc:Model_Number,com:Model_Number)
  SELF.AddRelationLink(cmc:Category,com:Category)


Hide:Access:COMMCAT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COMMCAT &= NULL


Hide:Relate:COMMCAT.Kill PROCEDURE

  CODE
  Hide:Access:COMMCAT.Kill
  PARENT.Kill
  Relate:COMMCAT &= NULL


Hide:Access:MESSAGES.Init PROCEDURE
  CODE
  SELF.Init(MESSAGES,GlobalErrors)
  SELF.Buffer &= mes:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mes:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(mes:Read_Key,'mes:Read_Key',0)
  SELF.AddKey(mes:Who_To_Key,'mes:Who_To_Key',0)
  SELF.AddKey(mes:Who_From_Key,'mes:Who_From_Key',0)
  SELF.AddKey(mes:Read_Only_Key,'mes:Read_Only_Key',0)
  SELF.AddKey(mes:Date_Only_Key,'mes:Date_Only_Key',0)
  SELF.LazyOpen = False
  Access:MESSAGES &= SELF


Hide:Relate:MESSAGES.Init PROCEDURE
  CODE
  Hide:Access:MESSAGES.Init
  SELF.Init(Access:MESSAGES,1)


Hide:Access:MESSAGES.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MESSAGES &= NULL


Hide:Access:MESSAGES.PrimeFields PROCEDURE

  CODE
  mes:Read = 'NO'
  PARENT.PrimeFields


Hide:Relate:MESSAGES.Kill PROCEDURE

  CODE
  Hide:Access:MESSAGES.Kill
  PARENT.Kill
  Relate:MESSAGES &= NULL


Hide:Access:LOGEXHE.Init PROCEDURE
  CODE
  SELF.Init(LOGEXHE,GlobalErrors)
  SELF.Buffer &= log1:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(log1:Batch_Number_Key,'log1:Batch_Number_Key',1)
  SELF.AddKey(log1:Processed_Key,'log1:Processed_Key',0)
  SELF.LazyOpen = False
  Access:LOGEXHE &= SELF


Hide:Relate:LOGEXHE.Init PROCEDURE
  CODE
  Hide:Access:LOGEXHE.Init
  SELF.Init(Access:LOGEXHE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGEXCH,RI:CASCADE,RI:CASCADE,xch1:Batch_Number_Key)
  SELF.AddRelationLink(log1:Batch_No,xch1:Batch_Number)


Hide:Access:LOGEXHE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGEXHE &= NULL


Hide:Relate:LOGEXHE.Kill PROCEDURE

  CODE
  Hide:Access:LOGEXHE.Kill
  PARENT.Kill
  Relate:LOGEXHE &= NULL


Hide:Access:DEFCRC.Init PROCEDURE
  CODE
  SELF.Init(DEFCRC,GlobalErrors)
  SELF.Buffer &= dfc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dfc:RecordNumberKey,'By Record Number',1)
  SELF.LazyOpen = False
  Access:DEFCRC &= SELF


Hide:Relate:DEFCRC.Init PROCEDURE
  CODE
  Hide:Access:DEFCRC.Init
  SELF.Init(Access:DEFCRC,1)


Hide:Access:DEFCRC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFCRC &= NULL


Hide:Relate:DEFCRC.Kill PROCEDURE

  CODE
  Hide:Access:DEFCRC.Kill
  PARENT.Kill
  Relate:DEFCRC &= NULL


Hide:Access:PAYTYPES.Init PROCEDURE
  CODE
  SELF.Init(PAYTYPES,GlobalErrors)
  SELF.Buffer &= pay:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(pay:Payment_Type_Key,'By Payment Type',0)
  SELF.LazyOpen = False
  Access:PAYTYPES &= SELF


Hide:Relate:PAYTYPES.Init PROCEDURE
  CODE
  Hide:Access:PAYTYPES.Init
  SELF.Init(Access:PAYTYPES,1)


Hide:Access:PAYTYPES.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PAYTYPES &= NULL


Hide:Relate:PAYTYPES.Kill PROCEDURE

  CODE
  Hide:Access:PAYTYPES.Kill
  PARENT.Kill
  Relate:PAYTYPES &= NULL


Hide:Access:COMMONWP.Init PROCEDURE
  CODE
  SELF.Init(COMMONWP,GlobalErrors)
  SELF.Buffer &= cwp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cwp:RecordNumberKey,'cwp:RecordNumberKey',1)
  SELF.AddKey(cwp:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(cwp:Description_Key,'By Description',0)
  SELF.AddKey(cwp:RefPartNumberKey,'cwp:RefPartNumberKey',0)
  SELF.AddKey(cwp:PartNumberKey,'By Part Number',0)
  SELF.LazyOpen = False
  Access:COMMONWP &= SELF


Hide:Relate:COMMONWP.Init PROCEDURE
  CODE
  Hide:Access:COMMONWP.Init
  SELF.Init(Access:COMMONWP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COMMONFA)


Hide:Access:COMMONWP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COMMONWP &= NULL


Hide:Access:COMMONWP.PrimeFields PROCEDURE

  CODE
  cwp:Quantity = 1
  PARENT.PrimeFields


Hide:Relate:COMMONWP.Kill PROCEDURE

  CODE
  Hide:Access:COMMONWP.Kill
  PARENT.Kill
  Relate:COMMONWP &= NULL


Hide:Access:LOGEXCH.Init PROCEDURE
  CODE
  SELF.Init(LOGEXCH,GlobalErrors)
  SELF.Buffer &= xch1:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(xch1:Batch_Number_Key,'xch1:Batch_Number_Key',0)
  SELF.AddKey(xch1:Ref_Number_Key,'By Loan Unit Number',1)
  SELF.AddKey(xch1:ESN_Only_Key,'xch1:ESN_Only_Key',0)
  SELF.AddKey(xch1:MSN_Only_Key,'xch1:MSN_Only_Key',0)
  SELF.AddKey(xch1:Ref_Number_Stock_Key,'By Ref Number',0)
  SELF.AddKey(xch1:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(xch1:ESN_Key,'By E.S.N. / I.M.E.I. Number',0)
  SELF.AddKey(xch1:MSN_Key,'By M.S.N. Number',0)
  SELF.AddKey(xch1:ESN_Available_Key,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(xch1:MSN_Available_Key,'By M.S.N.',0)
  SELF.AddKey(xch1:Ref_Available_Key,'By Exchange Unit Number',0)
  SELF.AddKey(xch1:Model_Available_Key,'By Model Number',0)
  SELF.AddKey(xch1:Stock_Type_Key,'By Stock Type',0)
  SELF.AddKey(xch1:ModelRefNoKey,'By Unit Number',0)
  SELF.AddKey(xch1:AvailIMEIOnlyKey,'By I.M.E.I. Number',0)
  SELF.AddKey(xch1:AvailMSNOnlyKey,'By M.S.N.',0)
  SELF.AddKey(xch1:AvailRefOnlyKey,'By Unit Number',0)
  SELF.AddKey(xch1:AvailModOnlyKey,'By Unit Number',0)
  SELF.LazyOpen = False
  Access:LOGEXCH &= SELF


Hide:Relate:LOGEXCH.Init PROCEDURE
  CODE
  Hide:Access:LOGEXCH.Init
  SELF.Init(Access:LOGEXCH,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOGEXHE)


Hide:Access:LOGEXCH.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:LOGEXCH &= NULL


Hide:Relate:LOGEXCH.Kill PROCEDURE

  CODE
  Hide:Access:LOGEXCH.Kill
  PARENT.Kill
  Relate:LOGEXCH &= NULL


Hide:Access:QAREASON.Init PROCEDURE
  CODE
  SELF.Init(QAREASON,GlobalErrors)
  SELF.Buffer &= qar:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(qar:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(qar:ReasonKey,'By Reason',0)
  SELF.LazyOpen = False
  Access:QAREASON &= SELF


Hide:Relate:QAREASON.Init PROCEDURE
  CODE
  Hide:Access:QAREASON.Init
  SELF.Init(Access:QAREASON,1)


Hide:Access:QAREASON.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:QAREASON &= NULL


Hide:Relate:QAREASON.Kill PROCEDURE

  CODE
  Hide:Access:QAREASON.Kill
  PARENT.Kill
  Relate:QAREASON &= NULL


Hide:Access:COMMONCP.Init PROCEDURE
  CODE
  SELF.Init(COMMONCP,GlobalErrors)
  SELF.Buffer &= ccp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ccp:RecordNumberKey,'ccp:RecordNumberKey',1)
  SELF.AddKey(ccp:Ref_Number_Key,'By Ref Number',0)
  SELF.AddKey(ccp:Description_Key,'By Description',0)
  SELF.AddKey(ccp:RefPartNumberKey,'ccp:RefPartNumberKey',0)
  SELF.AddKey(ccp:PartNumberKey,'By Part Number',0)
  SELF.LazyOpen = False
  Access:COMMONCP &= SELF


Hide:Relate:COMMONCP.Init PROCEDURE
  CODE
  Hide:Access:COMMONCP.Init
  SELF.Init(Access:COMMONCP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COMMONFA)


Hide:Access:COMMONCP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COMMONCP &= NULL


Hide:Access:COMMONCP.PrimeFields PROCEDURE

  CODE
  ccp:Quantity = 1
  PARENT.PrimeFields


Hide:Relate:COMMONCP.Kill PROCEDURE

  CODE
  Hide:Access:COMMONCP.Kill
  PARENT.Kill
  Relate:COMMONCP &= NULL


Hide:Access:ADDSEARCH.Init PROCEDURE
  CODE
  SELF.Init(ADDSEARCH,GlobalErrors)
  SELF.FileName &= glo:File_Name
  SELF.Buffer &= addtmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(addtmp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(addtmp:AddressLine1Key,'By Address',0)
  SELF.AddKey(addtmp:PostcodeKey,'By Postcode',0)
  SELF.AddKey(addtmp:IncomingIMEIKey,'By Incoming I.M.E.I. No',0)
  SELF.AddKey(addtmp:ExchangedIMEIKey,'By Exchanged I.M.E.I. No',0)
  SELF.AddKey(addtmp:FinalIMEIKey,'By Final I.M.E.I. No',0)
  SELF.AddKey(addtmp:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:ADDSEARCH &= SELF


Hide:Relate:ADDSEARCH.Init PROCEDURE
  CODE
  Hide:Access:ADDSEARCH.Init
  SELF.Init(Access:ADDSEARCH,1)


Hide:Access:ADDSEARCH.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ADDSEARCH &= NULL


Hide:Relate:ADDSEARCH.Kill PROCEDURE

  CODE
  Hide:Access:ADDSEARCH.Kill
  PARENT.Kill
  Relate:ADDSEARCH &= NULL


Hide:Access:JOBSSL.Init PROCEDURE
  CODE
  SELF.Init(JOBSSL,GlobalErrors)
  SELF.Buffer &= jsl:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jsl:RecordNumberKey,'jsl:RecordNumberKey',1)
  SELF.AddKey(jsl:RefNumberKey,'jsl:RefNumberKey',0)
  SELF.AddKey(jsl:SLNumberKey,'jsl:SLNumberKey',0)
  SELF.LazyOpen = False
  Access:JOBSSL &= SELF


Hide:Relate:JOBSSL.Init PROCEDURE
  CODE
  Hide:Access:JOBSSL.Init
  SELF.Init(Access:JOBSSL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSSL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSSL &= NULL


Hide:Relate:JOBSSL.Kill PROCEDURE

  CODE
  Hide:Access:JOBSSL.Kill
  PARENT.Kill
  Relate:JOBSSL &= NULL


Hide:Access:CONTHIST.Init PROCEDURE
  CODE
  SELF.Init(CONTHIST,GlobalErrors)
  SELF.Buffer &= cht:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cht:Ref_Number_Key,'By Date',0)
  SELF.AddKey(cht:Action_Key,'By Action',0)
  SELF.AddKey(cht:User_Key,'By User',0)
  SELF.AddKey(cht:Record_Number_Key,'cht:Record_Number_Key',1)
  SELF.AddKey(cht:KeyRefSticky,'cht:KeyRefSticky',0)
  SELF.LazyOpen = False
  Access:CONTHIST &= SELF


Hide:Relate:CONTHIST.Init PROCEDURE
  CODE
  Hide:Access:CONTHIST.Init
  SELF.Init(Access:CONTHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:CONTHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONTHIST &= NULL


Hide:Access:CONTHIST.PrimeFields PROCEDURE

  CODE
  cht:SystemHistory = 0
  PARENT.PrimeFields


Hide:Relate:CONTHIST.Kill PROCEDURE

  CODE
  Hide:Access:CONTHIST.Kill
  PARENT.Kill
  Relate:CONTHIST &= NULL


Hide:Access:CONTACTS.Init PROCEDURE
  CODE
  SELF.Init(CONTACTS,GlobalErrors)
  SELF.Buffer &= con:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(con:Name_Key,'By Name',0)
  SELF.LazyOpen = False
  Access:CONTACTS &= SELF


Hide:Relate:CONTACTS.Init PROCEDURE
  CODE
  Hide:Access:CONTACTS.Init
  SELF.Init(Access:CONTACTS,1)


Hide:Access:CONTACTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONTACTS &= NULL


Hide:Relate:CONTACTS.Kill PROCEDURE

  CODE
  Hide:Access:CONTACTS.Kill
  PARENT.Kill
  Relate:CONTACTS &= NULL


Hide:Access:EXPJOBS.Init PROCEDURE
  CODE
  SELF.Init(EXPJOBS,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= expjob:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPJOBS &= SELF


Hide:Relate:EXPJOBS.Init PROCEDURE
  CODE
  Hide:Access:EXPJOBS.Init
  SELF.Init(Access:EXPJOBS,1)


Hide:Access:EXPJOBS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPJOBS &= NULL


Hide:Relate:EXPJOBS.Kill PROCEDURE

  CODE
  Hide:Access:EXPJOBS.Kill
  PARENT.Kill
  Relate:EXPJOBS &= NULL


Hide:Access:EXPAUDIT.Init PROCEDURE
  CODE
  SELF.Init(EXPAUDIT,GlobalErrors)
  SELF.FileName &= glo:file_name4
  SELF.Buffer &= expaud:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPAUDIT &= SELF


Hide:Relate:EXPAUDIT.Init PROCEDURE
  CODE
  Hide:Access:EXPAUDIT.Init
  SELF.Init(Access:EXPAUDIT,1)


Hide:Access:EXPAUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPAUDIT &= NULL


Hide:Relate:EXPAUDIT.Kill PROCEDURE

  CODE
  Hide:Access:EXPAUDIT.Kill
  PARENT.Kill
  Relate:EXPAUDIT &= NULL


Hide:Access:JOBBATCH.Init PROCEDURE
  CODE
  SELF.Init(JOBBATCH,GlobalErrors)
  SELF.Buffer &= jbt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jbt:Batch_Number_Key,'By Batch Number',1)
  SELF.LazyOpen = False
  Access:JOBBATCH &= SELF


Hide:Relate:JOBBATCH.Init PROCEDURE
  CODE
  Hide:Access:JOBBATCH.Init
  SELF.Init(Access:JOBBATCH,1)


Hide:Access:JOBBATCH.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBBATCH &= NULL


Hide:Access:JOBBATCH.PrimeFields PROCEDURE

  CODE
  jbt:Date = Today()
  jbt:Time = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBBATCH.Kill PROCEDURE

  CODE
  Hide:Access:JOBBATCH.Kill
  PARENT.Kill
  Relate:JOBBATCH &= NULL

