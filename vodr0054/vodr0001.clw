

   MEMBER('vodr0054.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


ManufacturerWIPReport PROCEDURE                       !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
ManufacturerValue             LIKE(glo:ManufacturerValue)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Param_Group          GROUP,PRE()
LOC:Manufacturer     STRING(30)
LOC:StartDate        DATE
LOC:EndDate          DATE
                     END
LOCal_Group          GROUP,PRE(LOC)
RepairCentreType     STRING(3)
ARCLocation          STRING(30)
ARCDateBooked        DATE
RRCLocation          STRING(30)
StatusReceivedAtRRC  STRING(30)
Account_Name         STRING(30)
ApplicationName      STRING('ServiceBase')
CommentText          STRING(255)
DesktopPath          STRING(255)
ExchNumber           LONG
FileName             STRING(255)
JobNumber            LONG
JobsExtendedInSync   BYTE
LoanNumber           LONG
Path                 STRING(255)
ProgramName          STRING(50)
SectionName          STRING(50)
Text                 STRING(255)
UserName             STRING(20)
UserCode             STRING(3)
Version              STRING('3.1.0000')
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
ProgressBar_Group    GROUP,PRE()
Progress:Text        STRING(255)
RecordCount          LONG
                     END
LOC:ModelNumber      STRING(30)
ModelQueue           QUEUE,PRE(mq)
Manufacturer         STRING(30)
ModelNumber          STRING(30)
Booked               LONG
Incomplete           LONG
Complete             LONG
ThirdParty           LONG
Exchanged            LONG
                     END
ExchangeTypeQueue    QUEUE,PRE(exq)
ExchangeType         STRING(30)
Exchanged            LONG(0)
                     END
Misc_Group           GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
ManufacturerChange   BYTE
ManufacturerCount    LONG
ManufacturerTag      STRING(1)
ManufacturerValue    STRING(30)
Result               BYTE
SkipBlankManufacturerSummaryLines BYTE
WEBJOB_OK            LONG
                     END
RecordCount_Group    GROUP,PRE()
RecordCount_Complete LONG
RecordCount_Exchanged LONG
RecordCount_Incomplete LONG
RecordCount_JobsBooked LONG
RecordCount_ThirdParty LONG
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
Excel_Group          GROUP,PRE()
Excel                CSTRING(20)
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(2)
HeadLastCol          STRING('G {1}')
DataLastCol          STRING('Z {1}')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(10)
DataHeaderRow        LONG(12)
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
InvoiceSubAccounts   BYTE
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
                     END
Job_Queue            QUEUE,PRE(jobQ)
AccountNumber        STRING(15)
Manufacturer         STRING(30)
JobNumber            LONG
                     END
BRW6::View:Browse    VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
ManufacturerBrowseQueue QUEUE                         !Queue declaration for browse/combo box using ?ManufacturerList
ManufacturerTag        LIKE(ManufacturerTag)          !List box control field - type derived from local data
ManufacturerTag_Icon   LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Manufacturer''s WIP Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('Start Job Booking Date '),AT(245,110),USE(?String4),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           LIST,AT(72,168,192,154),USE(?ManufacturerList),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Manufacturer~@s30@'),FROM(ManufacturerBrowseQueue)
                           LIST,AT(416,168,192,154),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                           ENTRY(@D6),AT(341,112,64,10),USE(LOC:StartDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest Booking date to show on report'),REQ
                           BUTTON,AT(413,108),USE(?PopCalendar:1),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           STRING('End Job Booking Date '),AT(245,134),USE(?String5),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(341,134,64,10),USE(LOC:EndDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest Booking date to show on report'),REQ
                           BUTTON,AT(413,130),USE(?PopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON('&Rev tags'),AT(544,260,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON,AT(68,330),USE(?ManufacturerDASTAG),TRN,FLAT,LEFT,FONT(,,COLOR:White,,CHARSET:ANSI),ICON('tagitemp.jpg')
                           BUTTON,AT(136,330),USE(?ManufacturerDASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(204,330),USE(?ManufacturerDASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON,AT(412,328),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(480,328),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(548,328),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON('&Rev tags'),AT(160,230,56,16),USE(?ManufacturerDASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(524,280,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON('sho&W tags'),AT(160,250,56,16),USE(?ManufacturerDASSHOWTAG),HIDE
                           CHECK(' Skip Blank Manufacturer Summary Lines'),AT(72,152,176,16),USE(SkipBlankManufacturerSummaryLines),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('If a manufacturer''s Model has no Jobs <13,10>    booked against it.<13,10,13,10>Tick the box t' &|
   'o stop it from being printed in <13,10>    the Summary Sheet.<13,10>Or leave unticked to p' &|
   'rint it anyway.')
                           CHECK('All Repair Centres'),AT(416,156),USE(DoAll),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(556,110,,10),USE(excel:Visible),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ManufacturerBRW      CLASS(BrowseClass)               !Browse using ?ManufacturerList
Q                      &ManufacturerBrowseQueue       !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
TradeAccBRW          CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
!-----------------------------------------------
    MAP
DateToString        PROCEDURE( DATE ), STRING
GetDeliveryDateAtARC        PROCEDURE( LONG ), BYTE
GetModelNumber         PROCEDURE( STRING, STRING ),BYTE
GetHeadAccount PROCEDURE( STRING )
GetSubAccount PROCEDURE( STRING )
LoadEXCHANGE            PROCEDURE( LONG ), LONG ! BOOL
IsManufacturerRequired       PROCEDURE( STRING ), LONG ! BOOL
IsModelNumberRequired       PROCEDURE( STRING ), LONG ! BOOL
IsTradeAccRequired      PROCEDURE( STRING ), LONG ! BOOL
LoadJOBS            PROCEDURE( LONG ), LONG ! BOOL
LoadJOBSE           PROCEDURE( LONG ), LONG, PROC ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadUSERS            PROCEDURE( STRING ),LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
StartNextMonth            PROCEDURE(), DATE
StartThisMonth  PROCEDURE(), DATE
StartThisWeek       PROCEDURE(), DATE
UpdateExchangeType      PROCEDURE( LONG )
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug      PROCEDURE( STRING )
    END !MAP
!-----------------------------------------------
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(ManufacturerBrowseQueue,CHOICE(?ManufacturerList))
  ManufacturerBRW.UpdateBuffer
   ManufacturerQueue.ManufacturerValue = man:Manufacturer
   GET(ManufacturerQueue,ManufacturerQueue.ManufacturerValue)
  IF ERRORCODE()
     ManufacturerQueue.ManufacturerValue = man:Manufacturer
     ADD(ManufacturerQueue,ManufacturerQueue.ManufacturerValue)
    ManufacturerTag = '*'
  ELSE
    DELETE(ManufacturerQueue)
    ManufacturerTag = ''
  END
    ManufacturerBrowseQueue.ManufacturerTag = ManufacturerTag
  IF (ManufacturerTag = '*')
    ManufacturerBrowseQueue.ManufacturerTag_Icon = 2
  ELSE
    ManufacturerBrowseQueue.ManufacturerTag_Icon = 1
  END
  PUT(ManufacturerBrowseQueue)
  SELECT(?ManufacturerList,CHOICE(?ManufacturerList))
  ManufacturerCount = RECORDS(ManufacturerQueue) !DAS Taging
  ManufacturerChange = 1  !DAS Taging
      DO SetButtons
DASBRW::5:DASTAGALL Routine
  ?ManufacturerList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  ManufacturerBRW.Reset
  FREE(ManufacturerQueue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     ManufacturerQueue.ManufacturerValue = man:Manufacturer
     ADD(ManufacturerQueue,ManufacturerQueue.ManufacturerValue)
  END
  SETCURSOR
  ManufacturerBRW.ResetSort(1)
  SELECT(?ManufacturerList,CHOICE(?ManufacturerList))
  ManufacturerCount = RECORDS(ManufacturerQueue) !DAS Taging
  ManufacturerChange = 1  !DAS Taging
      DO SetButtons
DASBRW::5:DASUNTAGALL Routine
  ?ManufacturerList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(ManufacturerQueue)
  ManufacturerBRW.Reset
  SETCURSOR
  ManufacturerBRW.ResetSort(1)
  SELECT(?ManufacturerList,CHOICE(?ManufacturerList))
  ManufacturerCount = RECORDS(ManufacturerQueue) !DAS Taging
  ManufacturerChange = 1  !DAS Taging
      DO SetButtons
DASBRW::5:DASREVTAGALL Routine
  ?ManufacturerList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(ManufacturerQueue)
    GET(ManufacturerQueue,QR#)
    DASBRW::5:QUEUE = ManufacturerQueue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(ManufacturerQueue)
  ManufacturerBRW.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.ManufacturerValue = man:Manufacturer
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.ManufacturerValue)
    IF ERRORCODE()
       ManufacturerQueue.ManufacturerValue = man:Manufacturer
       ADD(ManufacturerQueue,ManufacturerQueue.ManufacturerValue)
    END
  END
  SETCURSOR
  ManufacturerBRW.ResetSort(1)
  SELECT(?ManufacturerList,CHOICE(?ManufacturerList))
  ManufacturerCount = RECORDS(ManufacturerQueue) !DAS Taging
  ManufacturerChange = 1  !DAS Taging
      DO SetButtons
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?ManufacturerDASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?ManufacturerDASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?ManufacturerDASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?ManufacturerDASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?ManufacturerDASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?ManufacturerDASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?ManufacturerDASSHOWTAG{PROP:Text} = 'Show All'
      ?ManufacturerDASSHOWTAG{PROP:Msg}  = 'Show All'
      ?ManufacturerDASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?ManufacturerDASSHOWTAG{PROP:Text})
   ManufacturerBRW.ResetSort(1)
   SELECT(?ManufacturerList,CHOICE(?ManufacturerList))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  TradeAccBRW.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  TradeAccBRW.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  TradeAccBRW.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  TradeAccBRW.Reset
  SETCURSOR
  TradeAccBRW.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::8:QUEUE = glo:Queue2
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue2)
  TradeAccBRW.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  TradeAccBRW.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   TradeAccBRW.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
OKButton_Pressed                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        debug:Active  = true

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !-----------------------------------------------


        !-----------------------------------------------------------------
        DO ProgressBar_Setup
        DO XL_Setup
        !-----------------------------------------------------------------
        IF RECORDS(ManufacturerQueue) = 0
            Case Missive('You must select at least one manufacturer.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            EXIT
        END !IF
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        ! 29 Oct 2002 John
        ! MICHALAN: What about the jobs that were booked in at the RRC but sent to the ARC?
        ! The RRC branch number should appear in this column.
        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! DespatchToCustomer     = DESPATCHED
        !
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        LOC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !-----------------------------------------------------------------

        !SELECT(?CancelButton)
        !DISABLE(?OKButton)

        !------------------------------------------
        !DO FillJobsQueue
        DO LoadModelNumbers
        !MESSAGE(ModelQueue)
        !------------------------------------------

        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)
        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.  ! Is this a Vodacom exclusive ?

            IF tra_ali:Account_Number = LocalHeadAccount 
                LOC:RepairCentreType = 'ARC'
            ELSIF tra_ali:RemoteRepairCentre =  0
                CYCLE
            ELSE
                LOC:RepairCentreType = 'RRC'
            END !IF

            IF DoAll <> 'Y' THEN
                glo:Queue2.Pointer2 = tra_ali:RecordNumber
                GET(glo:Queue2,glo:Queue2.Pointer2)
                IF ERROR() THEN CYCLE.
            END !IF
            !-------------------------------------------------------------
            LOC:FileName = ''
                DO ExportSetup
            IF LOC:FileName = '' THEN CYCLE.

            DO ExportBody
            DO ExportFinalize
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise

        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        FREE(HeadAccount_Queue)
        CLEAR(HeadAccount_Queue)

        FREE(SubAccount_Queue)
        CLEAR(SubAccount_Queue)

!        FREE(ModelQueue)
!        CLEAR(ModelQueue)

        FREE(ExchangeTypeQueue)
        CLEAR(ExchangeTypeQueue)

!        FREE(HeaderQueue)
!        CLEAR(HeaderQueue)

        CLEAR(RecordCount_Group)
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''

        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)
           !-----------------------------------------------------------------
           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader

        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 22
        Excel{'ActiveSheet.Columns("B:G").ColumnWidth'} = 15
        !-----------------------------------------------------------------
        ! Totals
        Excel{'Range("A11:G11").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A11").Select'}
            DO XL_SetTitle
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Totals'
        !
        Excel{'Range("A12:G16").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A12").Select'}
            Excel{'ActiveCell.Formula'} = 'Jobs Booked'

        Excel{'Range("A13").Select'}
            Excel{'ActiveCell.Formula'} = 'Incomplete'

        Excel{'Range("A14").Select'}
            Excel{'ActiveCell.Formula'} = 'Complete'

        Excel{'Range("A15").Select'}
            Excel{'ActiveCell.Formula'} = '3rd Party'

        Excel{'Range("A16").Select'}
            Excel{'ActiveCell.Formula'} = 'Exchanged'

        Excel{'Range("A17:G17").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        Excel{'Range("A19:G19").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A19").Select'}
            Excel{'ActiveCell.Formula'} = 'Summary'
            DO XL_SetBold
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO ClearModelNumbers

        DO InitColumns

        LOC:Text = CLIP(LOC:ProgramName) ! 'Detailed' ! 'Jobs Booked'
        DO CreateSubSheet
        !------------------------------------------
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:Date_Booked_Key)
        job:date_booked = LOC:StartDate
        SET(job:Date_Booked_Key, job:Date_Booked_Key)
        !------------------------------------------
        Progress:Text    = 'Checking Jobs For ' & CLIP(tra_ali:Account_Number) ! LOC:SectionName
        RecordsToProcess = RECORDS(JOBS)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:date_booked > LOC:EndDate
                BREAK
            END !IF
            !-------------------------------------------------------------
            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT

!CreateDataSheet                                    ROUTINE
!    DATA
!ItemPosition LONG
!    CODE
!        !-----------------------------------------------------------------
!        IF CancelPressed
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        DO ClearModelNumbers
!
!        DO InitColumns
!
!        LOC:Text = CLIP(LOC:ProgramName) ! 'Detailed' ! 'Jobs Booked'
!        DO CreateSubSheet
!        !------------------------------------------
!        CLEAR(Job_Queue)
!            jobQ:AccountNumber = tra_ali:Account_Number
!        GET(Job_Queue, +jobQ:AccountNumber)
!        ItemPosition = POSITION(Job_Queue)
!        !------------------------------------------
!        Progress:Text    = 'Checking Jobs For ' & CLIP(tra_ali:Account_Number) ! LOC:SectionName
!        RecordsToProcess = 1000 ! RECORDS(JOBS)
!
!        DO ProgressBar_LoopPre
!        !-----------------------------------------------------------------
!        LOOP WHILE jobQ:AccountNumber = tra_ali:Account_Number
!            !-------------------------------------------------------------
!            DO ProgressBar_Loop
!
!            IF CancelPressed
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            IF NOT LoadJOBS(jobQ:JobNumber)
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            DO WriteColumns
!            ItemPosition += 1
!            GET(Job_Queue, ItemPosition)
!            !GET(Job_Queue, +jobQ:AccountNumber)
!            IF ERRORCODE() <> 0
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        IF CancelPressed
!            EXIT
!        END !IF
!
!        DO ProgressBar_LoopPost
!        !-----------------------------------------------------------------
!        DO WriteDataSummary
!        !-----------------------------------------------------------------
!    EXIT
!
!
CreateSubSheet                                              ROUTINE
    DATA
temp LIKE(LOC:Text)
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet

        LOC:SectionName   = CLIP(LOC:Text)
        LOC:CommentText   = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        DO CreateSectionHeader
        DO SetColumns
        !-----------------------------------------------------------------
        DO XL_SetWorksheetLandscape

        LOC:Text = '$11:$11'
        DO XL_PrintTitleRows
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:SectionName)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & CLIP(sheet:TempLastCol) & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'

        DO XL_ColRight
            Excel{'ActiveCell.Formula'}         = CLIP(LOC:Text)
            DO XL_SetBold
            DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight

        DO XL_ColRight
            Excel{'ActiveCell.Formula'}         = 0
            DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}                = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'}              = 16

        Excel{'Range("A1:' & CLIP(sheet:TempLastCol) & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:TempLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}                = 'Criteria'
            DO XL_SetBold

        Excel{'Range("D' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Start  Job Booking Date'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(LOC:StartDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'End  Job Booking Date'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(LOC:EndDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Skip Blanks'
                DO XL_ColRight
                    DO XL_HorizontalAlignmentLeft
                    IF SkipBlankManufacturerSummaryLines
                        Excel{'ActiveCell.Formula'}    = 'YES'
                    ELSE
                        Excel{'ActiveCell.Formula'}    = 'NO'
                    END !IF

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}            = 'Created By'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}        = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}            = 'Date Created'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(TODAY())
        !-----------------------------------------------
        Excel{'Range("A4:' & CLIP(sheet:TempLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2

        sheet:HeadSummaryRow = sheet:DataSectionRow 
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        !-----------------------------------------------
        ! 15 Sep 2002 John, 5) Amend the Job Number to read "SB Job Number"
        !
        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 15 Sep 2002 John, 6) Add two new columns next to "SB Job Number":
        !   a. Franchise Account Number, This should show the Franchise Account Number
        !
        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !
        !   b. Franchise Job Number, This should show the Franchise Job Number
        !
        head:ColumnName       = 'Franchise Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------


        !-----------------------------------------------
        ! 29 Oct 2002 John
        ! MICHALAN: For the RRC report, include a column indicating the jobs current location - whether at the ARC or the RRC.
        IF Loc:RepairCentreType = 'RRC'
            head:ColumnName       = 'Current Location'
                head:ColumnWidth  = 15.00
                head:NumberFormat = chr(64) ! Text (AT) Sign
                ADD(HeaderQueue)
        END !IF
        !-----------------------------------------------


!        DO SetColumn_Manufacturer         ! Make
        head:ColumnName       = 'Make'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_ModelNumber          ! Model
        head:ColumnName       = 'Model Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_UnitType             ! Unit Type
        head:ColumnName       = 'Unit Type'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_ESN                  ! IMEI
        head:ColumnName       = 'IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

!        DO SetColumn_MSN                  ! MSN
        head:ColumnName       = 'MSN'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

!        DO SetColumn_HeadAccountNumber    ! Head Account No.
        head:ColumnName       = 'Head Account Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_HeadAccountName      ! Head Account Name (Trade Account)
        head:ColumnName       = 'Head Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_SubAccountNumber     ! Sub Account No.  (Sub Account Name)
        head:ColumnName       = 'Sub Account Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_SubAccountName       ! Account (Sub Account Name)
        head:ColumnName       = 'Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_DateBooked           ! Date Booked
        head:ColumnName       = 'Date Booked'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_Complete             ! Complete (YES/NO)
        head:ColumnName       = 'Complete'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_DateCompleted        ! Date Completed
        head:ColumnName       = 'Date Completed'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_Exchanged            ! Exchanged (YES/NO)
        head:ColumnName       = 'Exchanged'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_ExchangeType         ! Status_Type
        head:ColumnName       = 'Exchange Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_ExchangeIMEI         ! Exchange IMEI
        head:ColumnName       = 'Exchange IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

!        DO SetColumn_ExchangeDate         ! Exchange Date
        head:ColumnName       = 'Exchange Date'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_ThirdParty           ! ThirdParty (YES/NO)
        head:ColumnName       = 'Third Party'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_3rdPartySite         ! 3rd Party Site
        head:ColumnName       = '3rd Party Site'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_DateDespatched       ! Date Sent
        head:ColumnName       = 'Date Sent'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_DateReturned         ! Date Received Back
        head:ColumnName       = 'Date Received Back'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        DO SetColumn_Engineer             ! Engineer
        head:ColumnName       = 'Engineer User Code'
            head:ColumnWidth  = 12.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 15 Sep 2002 John, Reinstating job type as it makes it easier to read
        head:ColumnName       = 'Job Type'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

!        DO SetColumn_ChargeableChargeType ! Chargeable Job Type
        head:ColumnName       = 'Chargeable Charge Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_ChargeableRepairType ! Chargeable Repair Type
        head:ColumnName       = 'Chargeable Repair Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_WarrantyChargeType   ! Warranty Job Type
        head:ColumnName       = 'Warranty Charge Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

!        DO SetColumn_WarrantyRepairType   ! Warranty Repair Type
        head:ColumnName       = 'Warranty Repair Type'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & CLIP(sheet:DataLastCol) & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}

        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                  ROUTINE
    DATA
    CODE
        !---------------------------------------------------
        IF NOT LoadWEBJOB(job:Ref_Number)
            ! ERROR

            EXIT
        END !IF
        !---------------------------------------------------
        IF     LOC:RepairCentreType = 'RRC'
            !-----------------------------------------------
            ! RRC -> RRC
            !-----------------------------------------------
            IF wob:HeadAccountNumber <> tra_ali:Account_Number
                EXIT
            END !IF

            IF GetDeliveryDateAtARC(job:Ref_Number)
                !-------------------------------------------
                ! RRC -> ARC  <====================(1)
                !-------------------------------------------
                EXIT
            END !IF

            DO WriteColumnsAll

            EXIT
            !-----------------------------------------------
        END !IF
        !---------------------------------------------------
        ! LOC:RepairCentreType = 'ARC'
        !---------------------------------------------------
        IF wob:HeadAccountNumber = LocalHeadAccount
            !-----------------------------------------------
            ! ARC -> ARC
            !-----------------------------------------------
            ! NULL
            !-----------------------------------------------
        ELSIF GetDeliveryDateAtARC(job:Ref_Number) = TRUE
            !-----------------------------------------------
            ! RRC -> ARC  <====================(1)
            !-----------------------------------------------
            ! NULL
            !-----------------------------------------------
        ELSE
            !-----------------------------------------------
            ! RRC -> RRC
            !-----------------------------------------------
            EXIT
        END
        !---------------------------------------------------
        DO WriteColumnsAll
        !---------------------------------------------------
    EXIT
!-----------------------------------------------
!WriteColumns                                  ROUTINE
!    DATA
!    CODE
!        !-------------------------------------------------
!        mq:ModelNumber = job:Model_Number
!        GET(ModelQueue, +mq:ModelNumber)
!        IF ERRORCODE()
!            ! Not in chosen list - ignore
!            !
!            WriteDebug('WriteColumns job:Model_Number(' & CLIP(job:Model_Number) & ') Not in ModelQueue')
!
!            EXIT
!        END !IF
!        !====================================================
!        RecordCount += 1
!        !-------------------------------------------------
!        mq:Booked += 1
!
!        IF job:Date_Completed <> ''
!            RecordCount_Complete   += 1
!            mq:Complete            += 1
!        ELSE
!            RecordCount_Incomplete += 1
!            mq:Incomplete          += 1
!        END !IF
!
!        IF (job:Third_Party_Site <> '') AND (job:Workshop <> 'YES')
!            RecordCount_ThirdParty += 1
!            mq:ThirdParty          += 1
!        END !IF
!
!        IF job:Exchange_Unit_Number <> ''
!            RecordCount_Exchanged += 1
!            mq:Exchanged          += 1
!        END !IF
!
!        PUT(ModelQueue, +mq:ModelNumber)
!        !-------------------------------------------------
!        IF job:Exchange_Unit_Number <> 0
!            LOC:ExchNumber = job:Exchange_Unit_Number
!            DO GetExchangeTypeDetailsFromQueue
!
!            exq:Exchanged += 1
!            PUT(ExchangeTypeQueue, +exq:ExchangeType)
!        END !IF
!        !-------------------------------------------------
!        LOC:SubAccountNumber = job:Account_Number
!        DO GetSubAccountDetailsFromQueue
!        !-----------------------------------------------
!        DO SyncJOBTHIRD
!
!        WEBJOB_OK = LoadWEBJOB(job:Ref_Number)
!        !-----------------------------------------------
!        Excel{'ActiveWorkBook.Sheets("Detailed").Select'} ! Jobs Booked").Select'}
!
!        RecordCount_JobsBooked += 1
!        !-----------------------------------------------
!        DO WriteColumnsAll
!!        DO WriteColumns_Incomplete
!!        DO WriteColumns_Complete
!!        DO WriteColumns_ThirdParty
!!        DO WriteColumns_Exchanged
!        !-----------------------------------------------
!    EXIT
WriteColumnsAll                               ROUTINE
    DATA
TRADEACC_ALIAS_ID USHORT
    CODE
        !-----------------------------------------------
        IF CLIP(job:Model_Number) = ''
            EXIT
        END !IF
        !-----------------------------------------------
        GetSubAccount(job:Account_Number)
        GetHeadAccount(wob:HeadAccountNumber)
        !-----------------------------------------------
        IF ~GetModelNumber(job:Model_Number, job:Manufacturer)
          EXIT
        END

        RecordCount_JobsBooked     += 1
        mq:Booked                  += 1

        IF job:Date_Completed <> ''
            RecordCount_Complete   += 1
            mq:Complete            += 1
        ELSE
            RecordCount_Incomplete += 1
            mq:Incomplete          += 1
        END !IF

        IF (job:Third_Party_Site <> '') AND (job:Workshop <> 'YES')
            RecordCount_ThirdParty += 1
            mq:ThirdParty          += 1
        END !IF

        IF job:Exchange_Unit_Number <> ''
            !-------------------------------------------
            UpdateExchangeType(job:Exchange_Unit_Number)

            exq:Exchanged          += 1
            PUT(ExchangeTypeQueue, +exq:ExchangeType)
            !-------------------------------------------
            RecordCount_Exchanged  += 1
            mq:Exchanged           += 1
            !-------------------------------------------
        END !IF

        PUT(ModelQueue, +mq:ModelNumber)
        !===============================================
        RecordCount += 1
        !-----------------------------------------------
        WriteColumn( job:Ref_Number, True                 ) ! Job Number

        !-----------------------------------------------
        ! 15 Sep 2002 John, 6) Add two new columns next to "SB Job Number":
        !   a. Franchise Account Number, This should show the Franchise Account Number
        !
        LoadJOBSE( job:Ref_Number                         )

        IF jobe:WebJob
            IF jobe:HubRepair
                TRADEACC_ALIAS_ID = Access:TRADEACC_ALIAS.SaveFile()
                Access:TRADEACC_ALIAS.ClearKey(tra_ali:Account_Number_Key)
                tra_ali:Account_Number = wob:HeadAccountNumber
                IF Access:TRADEACC_ALIAS.Fetch(tra_ali:Account_Number_Key)
                    WriteColumn( haQ:BranchIdentification ) ! Franchise Account Number
                ELSE
                    WriteColumn( tra_ali:BranchIdentification ) ! RRC Branch Number
                END
                Access:TRADEACC_ALIAS.RestoreFile(TRADEACC_ALIAS_ID)
            ELSE
                WriteColumn( haQ:BranchIdentification     ) ! Franchise Account Number
            END
        ELSE
            WriteColumn( haQ:BranchIdentification         ) ! Franchise Account Number
        END
        !
        !   b. Franchise Job Number, This should show the Franchise Job Number
        !
        !IF WEBJOB_OK
            WriteColumn( wob:JobNumber                    ) ! Franchise Job Number
        !ELSE
!            WriteColumn( ''                               ) ! Franchise Job Number
        !END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 29 Oct 2002 John
        ! MICHALAN: For the RRC report, include a column indicating the jobs current location - whether at the ARC or the RRC.
        IF Loc:RepairCentreType = 'RRC'
            WriteColumn( job:Location                     ) ! Location
        END !IF
        !-----------------------------------------------

        WriteColumn( job:Manufacturer                     ) ! Make
        WriteColumn( job:Model_Number                     ) ! Model
        WriteColumn( job:Unit_Type                        ) ! Unit Type

        WriteColumn( '''' & job:ESN                       ) ! IMEI
        WriteColumn( job:MSN                              ) ! MSN

        WriteColumn( haQ:AccountNumber                    ) ! Head Account No.
        WriteColumn( haQ:AccountName                      ) ! Head Account Name
        WriteColumn( saQ:AccountName                      ) ! sub Account No.
        WriteColumn( saQ:AccountName                      ) ! Sub Account Name

        WriteColumn( DateToString(job:date_booked)        ) ! Date Booked

        !-----------------------------------------------
        ! WriteColumn_Complete                              ! Complete (YES/NO)
        IF  job:Date_Completed > 0
            WriteColumn( 'YES' )
            WriteColumn( DateToString(job:Date_Completed) ) ! Date Completed
        ELSE
            WriteColumn( 'NO' )
            WriteColumn( '' )
        END !IF
        !-----------------------------------------------

        !-----------------------------------------------
        IF job:Exchange_Unit_Number <> ''       
            WriteColumn( 'YES'                            ) ! Exchanged (YES/NO)

            WriteColumn( exq:ExchangeType)                   ! Status_Type
            IF Xch:ESN <> ''
              WriteColumn( xch:ESN                          ) ! Exchange IMEI
            ELSE
              WriteColumn ('IMEI NOT FOUND')
            END
            WriteColumn( job:Exchange_Despatched          ) ! Exchange Date
        ELSE
            WriteColumn( 'NO' )

            WriteColumn( '' )
            WriteColumn( '' )
            WriteColumn( '' )
        END !IF
        !-----------------------------------------------

        !-----------------------------------------------
        IF  (job:Third_Party_Site <> '') AND (job:Workshop <> 'YES')
            WriteColumn( 'YES'                            ) ! ThirdParty (YES/NO)
            WriteColumn( job:Third_Party_Site             ) ! 3rd Party Site
            DO SyncJOBTHIRD
            WriteColumn( DateToString(jot:DateOut)        ) ! Date Sent
            WriteColumn( DateToString(jot:DateIn)         ) ! Date Received Back
        ELSE
            WriteColumn( 'NO'                             ) ! ThirdParty (YES/NO)

            WriteColumn( ''                               )
            WriteColumn( ''                               )
            WriteColumn( ''                               )
        END !IF
        !-----------------------------------------------

        WriteColumn( job:Engineer                         ) ! Engineer

        !-----------------------------------------------
        ! 15 Sep 2002 John, Reinstating job type as it makes it easier to read
        DO WriteColumn_JobType                   ! Chargeable Job Type
        !-----------------------------------------------

        !-----------------------------------------------
        IF  (job:Chargeable_Job = 'YES') 
            WriteColumn( job:Charge_Type                  ) ! Chargeable Job Type
            WriteColumn( job:Repair_Type                  ) ! Chargeable Repair Type
        ELSE
            WriteColumn( ''                               )
            WriteColumn( ''                               )
        END !IF
        !-----------------------------------------------
        IF  (job:Warranty_Job = 'YES')
            WriteColumn( job:Warranty_Charge_Type         ) ! Warranty Job Type
            WriteColumn( job:Repair_Type_Warranty         ) ! Warranty Repair Type
        ELSE
            WriteColumn( ''                               )
            WriteColumn( ''                               )
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_JobNumber                   ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = job:Ref_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_FranchiseAccountNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = haQ:BranchIdentification

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = haQ:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_FranchiseJobNumber      ROUTINE
    DATA
Temp STRING(10)
    CODE
        !-----------------------------------------------
        IF WEBJOB_OK
            Temp = wob:JobNumber
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(Temp)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = CLIP(Temp)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_3rdPartyDateSent                  ROUTINE ! Date Sent
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF (job:Third_Party_Site = '')
                clip:Value = CLIP(clip:Value) & '<09>'
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(jot:DateOut, @D8))
            END !IF
            

            EXIT
        END !IF
        !-----------------------------------------------
        IF (job:Third_Party_Site = '')
            ! null
        ELSE
            Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(jot:DateOut, @D8))
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_3rdPartyDateReceivedBack                     ROUTINE ! Date Received Back
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF (job:Third_Party_Site = '')
                clip:Value = CLIP(clip:Value) & '<09>'
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(jot:DateIn, @D8))
            END !IF
            

            EXIT
        END !IF
        !-----------------------------------------------
        IF (job:Third_Party_Site = '')
            ! null
        ELSE
            Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(jot:DateIn, @D8))
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_3rdPartySite                      ROUTINE ! 3rd Party Repairer
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Third_Party_Site

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Third_Party_Site

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ChargeableChargeType                    ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Charge_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Charge_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ChargeableRepairType            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Repair_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Repair_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Complete                                ROUTINE ! Complete (YES/NO)
    DATA
Temp STRING(3)
    CODE
        !-----------------------------------------------
        ! Manufacturer report - SBR_0228 JOB=1865 10 Jul 2002 John
        !
        ! The report records the incorrect information in the date completed
        !   column on the detailed side of the report.
        !
        ! When a third party is used the report should record it as yes or no.
        !
        ! If you look at the attached sheet you will see third party being used
        !   but "no" being recorded when sent.
        !
        IF  job:Date_Completed > 0
            temp = 'YES'
        ELSE
            temp = 'NO'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ConsignmentNumber           ROUTINE ! Consignment Number
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Consignment_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Consignment_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_CurrentStatus_Job                       ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Current_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Current_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_CurrentStatus_Exchange                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Exchange_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Exchange_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_CurrentStatus_Loan                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Loan_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Loan_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DateBooked                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:date_booked, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:date_booked, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_DateCompleted                    ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:Date_Completed, @D8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = LEFT(FORMAT(job:Date_Completed, @D8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Engineer                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Engineer

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Engineer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_EngineerSkillLevel                  ROUTINE ! Eng. Skill Level
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & 'uq:SkillLevel'

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = 'uq:SkillLevel'

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ESN                         ROUTINE ! IMEI
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_EstimateStatus                    ROUTINE ! Set values for data sheet column header cell
    DATA
Temp STRING('ACCEPTED')
    CODE
        !-----------------------------------------------
        IF    job:Estimate_Accepted = 'YES'
            Temp = 'ACCEPTED'

        ELSIF job:Estimate_Rejected = 'YES'
            Temp = 'REJECTED'

        ELSIF job:Estimate = 'YES'
            Temp = 'REQUIRED'

        ELSE
            Temp = 'N/A'

        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ExchangeDate                            ROUTINE ! Exchange Date
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LEFT(FORMAT(job:Exchange_Despatched, @d8))

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = LEFT(FORMAT(job:Exchange_Despatched, @d8))

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Exchanged                           ROUTINE ! Exchanged (YES/NO)
    DATA
Temp STRING(3)
    CODE
        !-----------------------------------------------
        IF job:Exchange_Unit_Number <> ''
            temp = 'YES'
        ELSE
            temp = 'NO'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ExchangeIMEI                        ROUTINE ! Exchange IMEI
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF job:Exchange_Unit_Number = 0
                clip:Value = CLIP(clip:Value) & '<09>'
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & xch:ESN
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        IF job:Exchange_Unit_Number = 0
            ! NULL
        ELSE
            Excel{'ActiveCell.Formula'}      = xch:ESN
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ExchangeType                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF job:Exchange_Unit_Number = 0
                clip:Value = CLIP(clip:Value) & '<09>'
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & exq:ExchangeType
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        IF job:Exchange_Unit_Number = 0
            ! NULL
        ELSE
            Excel{'ActiveCell.Formula'}      = exq:ExchangeType
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_HeadAccountName                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:HeadAccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:HeadAccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_HeadAccountNumber              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:HeadAccountNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:HeadAccountNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

! [Priority 3600]
WriteColumn_IncomingCourier             ROUTINE  ! Incoming Courier
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Incoming_Courier

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Incoming_Courier

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_JobFaultDescription                       ROUTINE ! Fault Description
    DATA
    CODE
        !-----------------------------------------------
        DO SyncJOBNOTES
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF Result
                clip:Value = CLIP(clip:Value) & '<09>' & jbn:Fault_Description
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>'
            END !IF
            

            EXIT
        END !IF
        !-----------------------------------------------
        IF Result
            Excel{'ActiveCell.Formula'}  = jbn:Fault_Description
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_JobSkillLevel                       ROUTINE ! Job Skill Level
    DATA
jobLevel LONG
    CODE
        !-----------------------------------------------
        ! Job Skill Level
        !
        jobLevel = 1

        IF LOC:JobsExtendedInSync = True
            IF jobe:SkillLevel < 1
                jobLevel = 1
            ELSIF jobe:SkillLevel > 10
                jobLevel = 10
            ELSE
                jobLevel = jobe:SkillLevel
            END !IF

        END !IF
        !-----------------------------------------------
        ! jobLevel in range 1 .. 10
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & JobLevel

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = JobLevel

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_JobType                     ROUTINE ! Write value to data cell
    DATA
Temp STRING('Chargeable/Warranty')
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            IF job:Warranty_Job = 'YES'
                Temp = 'Chargeable/Warranty'
            ELSE
                Temp = 'Chargeable'
            END !IF

        ELSIF job:Warranty_Job   = 'YES'
            Temp = 'Warranty'

        ELSE
            Temp = 'Unknown'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Manufacturer                ROUTINE ! Make
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Manufacturer

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Manufacturer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ModelNumber                 ROUTINE ! Model
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_MSN                         ROUTINE ! MSN
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:MSN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:MSN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_SubAccountName                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:AccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:AccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_SubAccountNumber              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:AccountNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:AccountNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_ThirdParty                          ROUTINE ! ThirdParty (YES/NO)
    DATA
Temp STRING(3)
    CODE
        !-----------------------------------------------
        IF  (job:Third_Party_Site <> '') AND (job:Workshop <> 'YES')
            Temp = 'YES'
        ELSE
            Temp = 'NO'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_TransitType                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Transit_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Transit_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_UnitType                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Unit_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Unit_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_UserForename                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & 'uq:Forename'

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = 'uq:Forename'

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_UserSurname                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & 'uq:Surname'

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = 'uq:Surname'

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_WarrantyChargeType                  ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Warranty_Charge_Type

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Warranty_Charge_Type

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_WarrantyRepairType              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Repair_Type_Warranty

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Repair_Type_Warranty

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_WhoBooked                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Who_Booked

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Who_Booked

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                             ROUTINE !
    DATA
ResultsCount LONG
    CODE
        !-----------------------------------------------------------------
        ! summary details (JobsBooked)
        !
        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'} = RecordCount
            !Excel{'ActiveCell.Formula'} = RecordCount_JobsBooked

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'} = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}  = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount             & ')'
            !Excel{'ActiveCell.Formula'}  = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount_JobsBooked & ')'

        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
            DO XL_DataAutoFilter

        !RecordCount += 1

        DO ProgressBar_Loop

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT

WriteHeadSummary                             ROUTINE !
    DATA
ResultsCount LONG
    CODE
        !-----------------------------------------------------------------
        ! Totals
        !
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Summary").Select'}
        Excel{'Range("C12").Select'}

        Excel{'ActiveCell.Formula'} = RecordCount_JobsBooked
            DO XL_RowDown

        Excel{'ActiveCell.Formula'} = RecordCount_Incomplete
            DO XL_RowDown

        Excel{'ActiveCell.Formula'} = RecordCount_Complete
            DO XL_RowDown

        Excel{'ActiveCell.Formula'} = RecordCount_ThirdParty
            DO XL_RowDown

        Excel{'ActiveCell.Formula'} = RecordCount_Exchanged
            DO XL_RowDown

        DO XL_ColFirst
        !-----------------------------------------------------------------
        ! Summary
        !
        Excel{'Range("A21").Select'}

!        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        DO WriteSumary_ExchangeType
        DO WriteSumary_Manufacturer
        !-----------------------------------------------------------------
    EXIT

WriteSumary_ExchangeType                        ROUTINE
    DATA
CurrentRow   LONG
ResultsCount LONG
QueueIndex   LONG
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}

        Excel{'ActiveCell.Formula'}                    = 'Available Exchange Units'

        CurrentRow = Excel{'ActiveCell.Row'}
        Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:HeadLastCol) & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetTitle
            DO XL_SetBorder

        DO XL_RowDown
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(ExchangeTypeQueue)

        IF ResultsCount = 0
            Excel{'ActiveCell.Formula'} = 'No Exchange Types Found'
            DO XL_RowDown

            CurrentRow = Excel{'ActiveCell.Row'}
            Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:HeadLastCol) & CurrentRow & '").Select'}
                DO XL_SetTitle
                DO XL_SetBorder

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Exchange Type Summary'
        RecordsToProcess = ResultsCount

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        SORT(ExchangeTypeQueue, +exq:ExchangeType)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(ExchangeTypeQueue, QueueIndex)
            Excel{'ActiveCell.NumberFormat'}               = CHR(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  2).NumberFormat'} = '#,##0'

            Excel{'ActiveCell.Formula'}                    = exq:ExchangeType
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = exq:Exchanged

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        Excel{'ActiveCell.Formula'} = 'Total'
        Excel{'ActiveCell.Offset(0, 2).FormulaR1C1'} = '=SUM(r' & CurrentRow+1 & 'c3:r[-1]c3)'

        CurrentRow = Excel{'ActiveCell.Row'}
        Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:HeadLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        DO XL_ColFirst

        DO XL_RowDown
        DO XL_RowDown
        !-----------------------------------------------------------------
    EXIT
WriteSumary_Manufacturer                        ROUTINE
    DATA
CurrentRow          LONG
FirstRow            LONG
LastRow             LONG
ResultsCount        LONG
QueueIndex          LONG
LastManufacturer    LIKE(mq:Manufacturer)
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}

        Excel{'ActiveCell.Formula'}      = 'Manufacturer'
            DO XL_SetBold

        CurrentRow = Excel{'ActiveCell.Row'}
        Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:HeadLastCol) & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        DO XL_RowDown
            CurrentRow = Excel{'ActiveCell.Row'}
            Excel{'Range("A' & CurrentRow & ':' & CLIP(sheet:HeadLastCol) & CurrentRow & '").Select'}
                DO XL_SetTitle
                DO XL_SetBorder

        Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Manufacturer'
        Excel{'ActiveCell.Offset(0, 1).Formula'}      = 'Model Number'
        Excel{'ActiveCell.Offset(0, 2).Formula'}      = 'Jobs Booked'
        Excel{'ActiveCell.Offset(0, 3).Formula'}      = 'Incomplete'
        Excel{'ActiveCell.Offset(0, 4).Formula'}      = 'Complete'
        Excel{'ActiveCell.Offset(0, 5).Formula'}      = '3rd Party'
        Excel{'ActiveCell.Offset(0, 6).Formula'}      = 'Exchanged'

        DO XL_RowDown
        FirstRow = Excel{'ActiveCell.Row'}
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(ModelQueue)

        IF ResultsCount = 0
            Excel{'ActiveCell.Formula'} = 'No Jobs Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Manufacturer Summary'
        RecordsToProcess = ResultsCount
        RecordsProcessed = 0
        RecordCount      = 0

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        CurrentRow = Excel{'ActiveCell.Row'}
        LastManufacturer = ''

        SORT(ModelQueue, +mq:Manufacturer, +mq:ModelNumber)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(ModelQueue, QueueIndex)
            IF (mq:Booked = 0) AND (SkipBlankManufacturerSummaryLines = True)
                CYCLE
            END !IF

            Excel{'ActiveCell.NumberFormat'}               = CHR(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  1).NumberFormat'} = CHR(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  2).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  3).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  4).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  5).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  6).NumberFormat'} = '#,##0'

!            IF LastManufacturer <> mq:Manufacturer
!                LastManufacturer = mq:Manufacturer
!            ELSE
!                Excel{'Selection.Font.ColorIndex'} = 2 ! WHITE
!            END !IF

            Excel{'ActiveCell.Formula'}                    = mq:Manufacturer
            Excel{'ActiveCell.Offset(0,  1).Formula'}      = mq:ModelNumber
            Excel{'ActiveCell.Offset(0,  2).Formula'}      = mq:Booked
            Excel{'ActiveCell.Offset(0,  3).Formula'}      = mq:Incomplete
            Excel{'ActiveCell.Offset(0,  4).Formula'}      = mq:Complete
            Excel{'ActiveCell.Offset(0,  5).Formula'}      = mq:ThirdParty
            Excel{'ActiveCell.Offset(0,  6).Formula'}      = mq:Exchanged

            DO XL_RowDown
        END !LOOP

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        LastRow = Excel{'ActiveCell.Row'}

        IF FirstRow = LastRow
            Excel{'ActiveCell.Formula'} = 'No Non Blank Manufacturers Found'

            DO XL_RowDown
            DO XL_RowDown

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'Selection.AutoFilter'}

        Excel{'Range("A' & LastRow & ':' & CLIP(sheet:HeadLastCol) & LastRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & LastRow & '").Select'}
            Excel{'ActiveCell.Offset(0,  0).Formula'} = 'Total'
            Excel{'ActiveCell.Offset(0,  2).Formula'} = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow-1 & ')'
            Excel{'ActiveCell.Offset(0,  3).Formula'} = '=SUBTOTAL(9, D' & FirstRow & ':D' & LastRow-1 & ')'
            Excel{'ActiveCell.Offset(0,  4).Formula'} = '=SUBTOTAL(9, E' & FirstRow & ':E' & LastRow-1 & ')'
            Excel{'ActiveCell.Offset(0,  5).Formula'} = '=SUBTOTAL(9, F' & FirstRow & ':F' & LastRow-1 & ')'
            Excel{'ActiveCell.Offset(0,  6).Formula'} = '=SUBTOTAL(9, G' & FirstRow & ':G' & LastRow-1 & ')'

        Excel{'Range("A' & LastRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
FillJobsQueue                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:Date_Booked_Key)
        job:date_booked = LOC:StartDate
        SET(job:Date_Booked_Key, job:Date_Booked_Key)
        !------------------------------------------
        Progress:Text    = 'Finding Jobs' !& CLIP(tra_ali:Account_Number) ! LOC:SectionName
        RecordsToProcess = 1000 * (LOC:EndDate - LOC:StartDate + 1) !RECORDS(JOBS)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP WHILE Access:JOBS.Next() = Level:Benign
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:date_booked > LOC:EndDate
                BREAK
            END !IF

!            IF NOT IsManufacturerRequired(job:Manufacturer)
!                CYCLE
!            END !IF
!
!            IF NOT LoadWEBJOB(job:Ref_Number)
!                CYCLE
!!            ELSIF NOT IsTradeAccRequired(wob:HeadAccountNumber)
!!                CYCLE
!            END !IF
!
!            IF NOT IsModelNumberRequired(job:Model_Number)
!                CYCLE
!            END !IF
            !-------------------------------------------------------------
            RecordCount += 1

            CLEAR(Job_Queue)
                jobQ:AccountNumber = wob:HeadAccountNumber
                jobQ:Manufacturer  = job:Manufacturer
                jobQ:JobNumber     = job:Ref_Number
            ADD(Job_Queue)
            !-------------------------------------------------------------
        END !LOOP

        SORT(Job_Queue, +jobQ:AccountNumber, +jobQ:JobNumber)
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
    EXIT
SyncJOBNOTES                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Synchronise JOBNOTES (Notes Attached To Jobs) to current JOBS file
        !
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number

        IF Access:JOBSE.TryFetch(jbn:RefNumberKey)
            Result = False
        ELSE
            Result = True
        END !IF
        !-----------------------------------------------
    EXIT
SyncJOBTHIRD                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Synchronise JOBNOTES (Notes Attached To Jobs) to current JOBS file
        !
        IF (job:Third_Party_Site = '')
            Result = False

            EXIT
        END !IF
        !-----------------------------------------------
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number

        IF Access:JOBTHIRD.TryFetch(jot:RefNumberKey)
            Result = False
        ELSE
            Result = True
        END !IF
        !-----------------------------------------------
    EXIT
LoadModelNumbers         ROUTINE
    DATA
    CODE
        !----------------------------------------------------
        IF DoAll <> 'Y' THEN
            SORT(ManufacturerQueue, +ManufacturerQueue.ManufacturerValue)
        END !IF
        !----------------------------------------------------
        ! Model_Number_Key           KEY(mod:Model_Number),NOCASE,PRIMARY
        ! Manufacturer_Key           KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE
        ! Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE
        !
        CLEAR(MODELNUM)
        !mod:Manufacturer = '' ! LOC:Manufacturer
        SET(mod:Manufacturer_Key, mod:Manufacturer_Key)
        !----------------------------------------------------
        RecordsProcessed = 0
        RecordsToProcess = RECORDS(MODELNUM)

        SETTARGET(ProgressWindow)
        ?Progress:UserString{PROP:Text} = 'Loading Model Numbers'
        SETTARGET()
        !----------------------------------------------------
        LOOP UNTIL Access:MODELNUM.Next()
            !------------------------------------------------
            IF DoAll <> 'Y' THEN
                ManufacturerQueue.ManufacturerValue = CLIP(mod:Manufacturer)

                GET(ManufacturerQueue, +ManufacturerQueue.ManufacturerValue)
                IF ERRORCODE()
                    WriteDebug('LoadModelNumbers(ERROR ' & ERRORCODE() & '-"' & CLIP(ERROR()) & '"), Not in list')

                    CYCLE
                END !IF
            END !IF
            !================================================
            RecordsProcessed += 1

            IF GetModelNumber(mod:Model_Number, mod:Manufacturer)
              !ok
            ELSE
             CLEAR(ModelQueue)
             mq:ModelNumber  = mod:Model_Number
             mq:Manufacturer = mod:Manufacturer
             ADD(ModelQueue, +mq:ModelNumber)
           END
!            mq:Manufacturer = mod:Manufacturer
!            mq:ModelNumber  = mod:Model_Number
!            mq:Booked       = 0
!            mq:Incomplete   = 0
!            mq:Complete     = 0
!            mq:ThirdParty   = 0
!            mq:Exchanged    = 0
!
!            ADD(ModelQueue)

            WriteDebug('LoadModelNumbers(ADD "' & CLIP(mod:Manufacturer) & '", "' & CLIP(mod:Model_Number) & '")')
            !------------------------------------------------
        END ! LOOP UNTIL Access:MODELNUM.Next()
        !----------------------------------------------------
        SORT(ModelQueue, +mq:ModelNumber)

        WriteDebug('LoadModelNumbers(RECORDS(' & RECORDS(ModelQueue) & '")')
        !----------------------------------------------------
    EXIT
ClearModelNumbers         ROUTINE
    DATA
    CODE
        !----------------------------------------------------
        RecordsProcessed = 0
        RecordsToProcess = RECORDS(MODELNUM)

        SETTARGET(ProgressWindow)
        ?Progress:UserString{PROP:Text} = 'Clearing Model Numbers'
        SETTARGET()
        !----------------------------------------------------
        LOOP x# = 1 TO RECORDS(ModelQueue)
            !------------------------------------------------
            RecordsProcessed += 1

            GET(ModelQueue, x#)
                mq:Booked       = 0
                mq:Incomplete   = 0
                mq:Complete     = 0
                mq:ThirdParty   = 0
                mq:Exchanged    = 0
            PUT(ModelQueue)
            !------------------------------------------------
        END !LOOP
        !----------------------------------------------------
        SORT(ModelQueue, +mq:ModelNumber)
        !----------------------------------------------------
    EXIT
!-----------------------------------------------
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Manufacturer WIP Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & CLIP(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VOD_0228 ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0054 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
Get_UserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
SetButtons                                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RECORDS(ManufacturerQueue) = 0
            ?OkButton{Prop:Hide} = True
        ELSE
            ?OkButton{Prop:Hide} = False
        END !IF
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A10").Select'}
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0 ! Default/error value
        OperatingSystem     = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
        !-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020612'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ManufacturerWIPReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBTHIRD.UseFile
  Access:MODELNUM.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  ManufacturerBRW.Init(?ManufacturerList,ManufacturerBrowseQueue.ViewPosition,BRW6::View:Browse,ManufacturerBrowseQueue,Relate:MANUFACT,SELF)
  TradeAccBRW.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
        LOC:ProgramName       = 'Manufacturer WIP Report' !             Job=150         Cust=N02
        MainWindow{PROP:Text} = CLIP(LOC:ProgramName)
        ?Tab1{PROP:Text}      = CLIP(LOC:ProgramName) & ' Criteria'
        excel:Visible    = False
  
        DO Get_UserName
  
        LOC:StartDate = TODAY() ! 6 Dec 2001 John DATE(01,01,1990)
        LOC:EndDate   = TODAY()
  
        IF GUIMode = 1 THEN
           MainWindow{PROP:ICONIZE} = TRUE
           LocalTimeOut = 500
        END !IF
        LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  
        DISPLAY
  ?ManufacturerList{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ManufacturerBRW.Q &= ManufacturerBrowseQueue
  ManufacturerBRW.AddSortOrder(,man:Manufacturer_Key)
  ManufacturerBRW.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,man:Manufacturer,1,ManufacturerBRW)
  BIND('ManufacturerTag',ManufacturerTag)
  ?ManufacturerList{PROP:IconList,1} = '~NoTick1.ico'
  ?ManufacturerList{PROP:IconList,2} = '~Tick1.ico'
  ManufacturerBRW.AddField(ManufacturerTag,ManufacturerBRW.Q.ManufacturerTag)
  ManufacturerBRW.AddField(man:Manufacturer,ManufacturerBRW.Q.man:Manufacturer)
  ManufacturerBRW.AddField(man:RecordNumber,ManufacturerBRW.Q.man:RecordNumber)
  TradeAccBRW.Q &= Queue:Browse
  TradeAccBRW.AddSortOrder(,tra:Company_Name_Key)
  TradeAccBRW.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,tra:Company_Name,1,TradeAccBRW)
  TradeAccBRW.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND tra:Account_Number <<> ''XXXRRC'')')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  TradeAccBRW.AddField(LocalTag,TradeAccBRW.Q.LocalTag)
  TradeAccBRW.AddField(tra:Account_Number,TradeAccBRW.Q.tra:Account_Number)
  TradeAccBRW.AddField(tra:Company_Name,TradeAccBRW.Q.tra:Company_Name)
  TradeAccBRW.AddField(tra:RecordNumber,TradeAccBRW.Q.tra:RecordNumber)
  ! 13 Sep 2002 John 4. Manufacturers should be untagged by default
  DO DASBRW::5:DASUNTAGALL ! DASBRW::5:DASTAGALL
  ManufacturerBRW.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?ManufacturerDASSHOWTAG{PROP:Text} = 'Show All'
  ?ManufacturerDASSHOWTAG{PROP:Msg}  = 'Show All'
  ?ManufacturerDASSHOWTAG{PROP:Tip}  = 'Show All'
  ManufacturerCount = RECORDS(ManufacturerQueue) !DAS Taging
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?ManufacturerList{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
      DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020612'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020612'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020612'&'0')
      ***
    OF ?PopCalendar:1
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ManufacturerDASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ManufacturerDASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ManufacturerDASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ManufacturerDASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ManufacturerDASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?ManufacturerList
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?ManufacturerDASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(TradeAccBRW) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?ManufacturerDASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?ManufacturerList
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?ManufacturerList{PROPLIST:MouseDownRow} > 0) 
        CASE ?ManufacturerList{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?ManufacturerDASTAG)
               ?ManufacturerList{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

        !-----------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
    SentToARC# = False
    LOC:ARCDateBooked = ''
    Access:LOCATLOG.Clearkey(lot:DateKey)
    lot:RefNumber = in:JOBNumber
    Set(lot:DateKey,lot:DateKey)
    Loop 
        If Access:LOCATLOG.Next()
            Break
        End !
        If lot:RefNumber <> in:JobNumber 
            Break
        End !
        If lot:NewLocation = loc:ARCLocation
            SentToARC# = True
            loc:ARCDateBooked = lot:TheDate
            !MESSAGE('TRUE CONDITION MET')
        Break
        End!    
      
    End !Loop

    Return SentToARC#



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        haq:AccountNumber = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF (haq:AccountNumber = IN:AccountNumber)
                RETURN
            END !IF - ADD
            ! Partial Match

        OF 30 ! NOT Found
            ! Not in queue - ADD
        ELSE
            CancelPressed = True

            RETURN
        END !IF
        !-----------------------------------------------------------------
        CLEAR(HeadAccount_Queue)
            haq:AccountNumber            = IN:AccountNumber

            IF LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = tra:Company_Name
                haq:BranchIdentification = tra:BranchIdentification

                IF tra:Invoice_Sub_Accounts = 'YES'
                    haQ:InvoiceSubAccounts = True
                ELSE
                    haQ:InvoiceSubAccounts = False
                END !IF
            ELSE
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'
            END !IF

        ADD(HeadAccount_Queue, +haq:AccountNumber)
        !-----------------------------------------------------------------
GetModelNumber         PROCEDURE( IN:ModelNumber, IN:Manufacturer )
    CODE
        !----------------------------------------------------
        WriteDebug('GetModelNumber("' & CLIP(IN:ModelNumber) & '", "' & CLIP(IN:Manufacturer) & '")')
        SORT(ModelQueue, mq:ModelNumber, mq:manufacturer)
        CLEAR(ModelQueue)
            mq:ModelNumber  = IN:ModelNumber
            mq:Manufacturer = IN:Manufacturer
        GET(ModelQueue, mq:ModelNumber, mq:manufacturer)
        CASE ERRORCODE()
        OF 00 ! Found
            IF CLIP(mq:ModelNumber) = CLIP(IN:ModelNumber)
                WriteDebug('GetModelNumber(OK)')
                RETURN True
            END !IF

            WriteDebug('GetModelNumber(Partial Match "' & CLIP(mq:ModelNumber) & '" <> "' & CLIP(IN:ModelNumber) & '")')

        OF 30 ! NOT Found
            ! NULL
            WriteDebug('GetModelNumber(NOT FOUND)')
            RETURN False
        ELSE
            WriteDebug('GetModelNumber(Error(' & ERRORCODE() & ', "' & CLIP(ERROR()) & '"')
            CancelPressed = True

            RETURN FALSE
        END !CASE
        !----------------------------------------------------
        WriteDebug('GetModelNumber(ADD)')


        !----------------------------------------------------

GetSubAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF (saq:AccountNumber = IN:AccountNumber)
                RETURN
            END !IF
            ! Partial Match

        OF 30 ! NOT Found
            ! Not in queue - ADD

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        CLEAR(SubAccount_Queue)
            saQ:AccountNumber            = IN:AccountNumber

            IF LoadSUBTRACC(IN:AccountNumber)
                GetHeadAccount(sub:Main_Account_Number)

                saQ:AccountName          = sub:Company_Name
                saQ:HeadAccountNumber    = sub:Main_Account_Number
                saQ:HeadAccountName      = haQ:AccountName
                saQ:BranchIdentification = haQ:BranchIdentification
            END !IF
        ADD(SubAccount_Queue, +saq:AccountNumber)
        !-----------------------------------------------------------------
IsManufacturerRequired       PROCEDURE( IN:Manufacturer )! LONG ! BOOL ! For use during LoabJobNumbers
    CODE
        !----------------------------------------------------
        ! For use during LoabJobNumbers
        !
        CLEAR(ManufacturerQueue)
            manq:ManufacturerValue = IN:Manufacturer
        GET(ManufacturerQueue, +manq:ManufacturerValue)

        CASE ERRORCODE()
        OF 00
            IF (manq:ManufacturerValue = IN:Manufacturer)
                RETURN True
            END !IF
            ! Partial Match
        OF 30
            ! NULL
        ELSE
            CancelPressed = True
        END !CASE
        !----------------------------------------------------
        RETURN False
        !----------------------------------------------------
IsModelNumberRequired       PROCEDURE( IN:ModelNumber )! LONG ! BOOL ! For use during LoabJobNumbers
    CODE
        !----------------------------------------------------
        ! For use during LoabJobNumbers
        !
        CLEAR(ModelQueue)
            mq:ModelNumber = IN:ModelNumber
        GET(ModelQueue, +mq:ModelNumber)

        CASE ERRORCODE()
        OF 00
            IF (mq:ModelNumber = IN:ModelNumber)
                RETURN True
            END !IF
            ! Partial Match
        OF 30
            ! NULL
        ELSE
            CancelPressed = True
        END !CASE
        !----------------------------------------------------
        RETURN False
        !----------------------------------------------------
IsTradeAccRequired      PROCEDURE(IN:HeadAccountNumber)! LONG ! BOOL ! For use during LoabJobNumbers
    CODE
        !----------------------------------------------------
        ! For use during LoabJobNumbers
        !----------------------------------------------------
        IF DoAll = 'Y' THEN
            RETURN True
        END !IF
        !----------------------------------------------------   TradeAccBRW
        CLEAR(Queue:Browse)
            Queue:Browse.tra:Account_Number = IN:HeadAccountNumber
        GET(Queue:Browse, +Queue:Browse.tra:Account_Number)

        CASE ERRORCODE()
        OF 00
            IF (Queue:Browse.tra:Account_Number = IN:HeadAccountNumber)
                RETURN True
            END !IF
            ! Partial Match
        OF 30
            ! NULL
        ELSE
            CancelPressed = True
        END !CASE
        !----------------------------------------------------
        RETURN False
        !----------------------------------------------------
LoadEXCHANGE            PROCEDURE( IN:ExchNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = IN:ExchNumber

        IF NOT Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            RETURN False
        END !IF

        IF NOT xch:Ref_Number = IN:ExchNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBS            PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = IN:JobNumber

        IF NOT Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
            RETURN False
        END !IF

        IF NOT job:Ref_Number = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBSE            PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = IN:JobNumber

        IF Access:JOBSE.Fetch(jobe:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadUSERS            PROCEDURE( IN:UserCode )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = IN:UserCode
        IF Access:USERS.Fetch(use:User_Code_Key)
            RETURN False
        END

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
NumberToColumn PROCEDURE(IN:Long)!STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
StartNextMonth            PROCEDURE()! DATE
Temp DATE
mmm  LONG
yyyy LONG
    CODE
        !-----------------------------------------------
        Temp = TODAY()
        mmm  = MONTH(Temp) + 1
        yyyy = YEAR(Temp)

        RETURN DATE(mmm, 1, yyyy)
        !-----------------------------------------------
StartThisMonth  PROCEDURE()! DATE
Temp DATE
mmm  LONG
yyyy LONG
    CODE
        !-----------------------------------------------
        Temp = TODAY()
        yyyy = YEAR(Temp)
        mmm  = MONTH(Temp)

        RETURN DATE(mmm, 1, yyyy)
        !-----------------------------------------------
StartThisWeek       PROCEDURE()! DATE
ReturnValue DATE
    CODE
        !-----------------------------------------------
        ReturnValue = TODAY()

        ReturnValue = ReturnValue - (ReturnValue % 7)

        RETURN ReturnValue
        !-----------------------------------------------
UpdateExchangeType      PROCEDURE( IN:ExchangeNumber )
    CODE
        !-----------------------------------------------------------------
        ! on entry LOC:SubAccountNumber = Account Number to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        IF CancelPressed
            RETURN
        END !IF
        !-----------------------------------------------------------------
        IF LoadEXCHANGE(IN:ExchangeNumber) !  LOC:ExchNumber)
            exq:ExchangeType = xch:Stock_Type
        ELSE
            exq:ExchangeType = '*UNKNOWN*'
        END ! IF

        GET(ExchangeTypeQueue, +exq:ExchangeType)
        CASE ERRORCODE()
        OF 00
            IF exq:ExchangeType = xch:Stock_Type
                RETURN
            END !IF
            IF  exq:ExchangeType = '*UNKNOWN*'
              RETURN
            END
        OF 30
            ! NULL
        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        ! Not in queue - ADD
        ADD(ExchangeTypeQueue, +exq:ExchangeType)
        !-----------------------------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

ManufacturerBRW.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     ManufacturerQueue.ManufacturerValue = man:Manufacturer
     GET(ManufacturerQueue,ManufacturerQueue.ManufacturerValue)
    IF ERRORCODE()
      ManufacturerTag = ''
    ELSE
      ManufacturerTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (ManufacturerTag = '*')
    SELF.Q.ManufacturerTag_Icon = 2
  ELSE
    SELF.Q.ManufacturerTag_Icon = 1
  END


ManufacturerBRW.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


ManufacturerBRW.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


ManufacturerBRW.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     ManufacturerQueue.ManufacturerValue = man:Manufacturer
     GET(ManufacturerQueue,ManufacturerQueue.ManufacturerValue)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


TradeAccBRW.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


TradeAccBRW.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


TradeAccBRW.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


TradeAccBRW.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

