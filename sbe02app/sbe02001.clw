

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02001.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Job_Loan_History PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBLOHIS)
                       PROJECT(jlh:Date)
                       PROJECT(jlh:Time)
                       PROJECT(jlh:User)
                       PROJECT(jlh:Loan_Unit_Number)
                       PROJECT(jlh:Status)
                       PROJECT(jlh:record_number)
                       PROJECT(jlh:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jlh:Date               LIKE(jlh:Date)                 !List box control field - type derived from field
jlh:Time               LIKE(jlh:Time)                 !List box control field - type derived from field
jlh:User               LIKE(jlh:User)                 !List box control field - type derived from field
jlh:Loan_Unit_Number   LIKE(jlh:Loan_Unit_Number)     !List box control field - type derived from field
jlh:Status             LIKE(jlh:Status)               !List box control field - type derived from field
jlh:record_number      LIKE(jlh:record_number)        !Primary key field - type derived from field
jlh:Ref_Number         LIKE(jlh:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Loan History File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,100,344,226),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48R(2)|M~Date~C(0)@d6b@24R(2)|M~Time~C(0)@T1@20C|M~User~@s3@68L(2)|M~Loan Unit N' &|
   'umber~@n012@80L(2)|M~Status~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Loan History File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020280'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Job_Loan_History')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBLOHIS.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBLOHIS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Job_Loan_History')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,jlh:Ref_Number_Key)
  BRW1.AddRange(jlh:Ref_Number,Relate:JOBLOHIS,Relate:JOBS)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,jlh:Date,1,BRW1)
  BRW1.AddField(jlh:Date,BRW1.Q.jlh:Date)
  BRW1.AddField(jlh:Time,BRW1.Q.jlh:Time)
  BRW1.AddField(jlh:User,BRW1.Q.jlh:User)
  BRW1.AddField(jlh:Loan_Unit_Number,BRW1.Q.jlh:Loan_Unit_Number)
  BRW1.AddField(jlh:Status,BRW1.Q.jlh:Status)
  BRW1.AddField(jlh:record_number,BRW1.Q.jlh:record_number)
  BRW1.AddField(jlh:Ref_Number,BRW1.Q.jlh:Ref_Number)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBLOHIS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Job_Loan_History')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020280'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020280'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020280'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

Browse_Exchange_History PROCEDURE                     !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(EXCHHIST)
                       PROJECT(exh:Date)
                       PROJECT(exh:Time)
                       PROJECT(exh:User)
                       PROJECT(exh:Status)
                       PROJECT(exh:Notes)
                       PROJECT(exh:record_number)
                       PROJECT(exh:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
exh:Date               LIKE(exh:Date)                 !List box control field - type derived from field
exh:Time               LIKE(exh:Time)                 !List box control field - type derived from field
exh:User               LIKE(exh:User)                 !List box control field - type derived from field
exh:Status             LIKE(exh:Status)               !List box control field - type derived from field
exh:Notes              LIKE(exh:Notes)                !List box control field - type derived from field
exh:record_number      LIKE(exh:record_number)        !Primary key field - type derived from field
exh:Ref_Number         LIKE(exh:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Exchange Unit History File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,104,344,138),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48R(2)|M~Date~L@d6b@24R(2)|~Time~@T1@22L(2)|~User~@s3@200L(2)|~Status~@s60@0L(2)' &|
   '|~Notes~@s255@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Exchange Unit History File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(168,246,344,80),USE(exh:Notes),SKIP,VSCROLL,READONLY
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020266'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Exchange_History')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHHIST.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCHHIST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Exchange_History')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,exh:Ref_Number_Key)
  BRW1.AddRange(exh:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,exh:Date,1,BRW1)
  BRW1.AddField(exh:Date,BRW1.Q.exh:Date)
  BRW1.AddField(exh:Time,BRW1.Q.exh:Time)
  BRW1.AddField(exh:User,BRW1.Q.exh:User)
  BRW1.AddField(exh:Status,BRW1.Q.exh:Status)
  BRW1.AddField(exh:Notes,BRW1.Q.exh:Notes)
  BRW1.AddField(exh:record_number,BRW1.Q.exh:record_number)
  BRW1.AddField(exh:Ref_Number,BRW1.Q.exh:Ref_Number)
  QuickWindow{PROP:MinWidth}=400
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHHIST.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Exchange_History')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020266'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020266'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020266'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  if glo:Webjob then
      if exh:Notes[1:6] = 'HIDDEN' then
          return(Record:filtered)
      END
  END
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

Update_Exchange PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
save_loan_alias_id   USHORT,AUTO
save_moc_id          USHORT,AUTO
audit_number_temp    REAL
esn_temp             STRING(16)
print_label_temp     STRING('NO {1}')
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
status_temp          STRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW7::View:Browse    VIEW(EXCHACC)
                       PROJECT(xca:Accessory)
                       PROJECT(xca:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
xca:Accessory          LIKE(xca:Accessory)            !List box control field - type derived from field
xca:Ref_Number         LIKE(xca:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB9::View:FileDrop  VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                       PROJECT(exa:Record_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?xch:Audit_Number
exa:Audit_Number       LIKE(exa:Audit_Number)         !List box control field - type derived from field
exa:Record_Number      LIKE(exa:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::xch:Record  LIKE(xch:RECORD),STATIC
QuickWindow          WINDOW('Update the Exchange File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Exchange Unit No'),AT(68,90),USE(?LOA:Ref_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@p<<<<<<<#pb),AT(140,90,64,10),USE(xch:Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Unit Number')
                           ENTRY(@d6b),AT(212,90,52,10),USE(xch:Date_Booked),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Status'),AT(68,104),USE(?status_temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(140,104,124,10),USE(status_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Accessories'),AT(372,112),USE(?Prompt14),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(68,126),USE(?LOA:ESN:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,126,124,10),USE(xch:ESN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           ENTRY(@s30),AT(372,124,125,10),USE(xca:Accessory),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('M.S.N.'),AT(68,140),USE(?xch:MSN:Prompt),TRN,HIDE,FONT(,8,COLOR:White,FONT:bold)
                           ENTRY(@s30),AT(140,140,124,10),USE(xch:MSN),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(372,140,148,140),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)@s30@'),FROM(Queue:Browse)
                           PROMPT('Model Number'),AT(68,162),USE(?XCH:Model_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,162,124,10),USE(xch:Model_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(264,158),USE(?LookupModelNumber),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Manufacturer'),AT(68,176),USE(?LOA:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           PROMPT('Colour'),AT(68,198),USE(?XCH:Colour:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,198,124,10),USE(xch:Colour),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(264,194),USE(?LookupModelColour),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(528,176),USE(?Button4),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           ENTRY(@s30),AT(140,176,124,10),USE(xch:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Location'),AT(68,220),USE(?XCH:Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,220,124,10),USE(xch:Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(264,216),USE(?LookupLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Shelf Location'),AT(68,240),USE(?XCH:Shelf_Location:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,240,124,10),USE(xch:Shelf_Location),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(264,236),USE(?LookupShelfLocation),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(528,220),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           PROMPT('Stock Type'),AT(68,260),USE(?XCH:Stock_Type:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(140,260,124,10),USE(xch:Stock_Type),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(264,256),USE(?LookupStockType),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Audit Number'),AT(68,282),USE(?XCH:Stock_Type:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           LIST,AT(140,282,64,10),USE(xch:Audit_Number),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('32L(2)|M@s8@'),DROP(10,64),FROM(Queue:FileDrop)
                           BUTTON,AT(68,306),USE(?Button5),TRN,FLAT,LEFT,ICON('unihistp.jpg')
                           BUTTON,AT(140,306),USE(?Print_Label),TRN,FLAT,LEFT,ICON('prnlabp.jpg')
                           BUTTON,AT(212,276),USE(?New_Audit_Number),TRN,FLAT,LEFT,ICON('newaudp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Exchange Unit'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  IncrementalLocatorClass          !Default Locator
FDB9                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_xch_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:xch:Model_Number                Like(xch:Model_Number)
look:xch:Colour                Like(xch:Colour)
look:xch:Location                Like(xch:Location)
look:xch:Shelf_Location                Like(xch:Shelf_Location)
look:xch:Stock_Type                Like(xch:Stock_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ValidateMSN     Routine
    If ?xch:MSN{prop:Hide} = 0 and xch:MSN <> ''
        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
        mod:Model_Number    = xch:Model_Number
        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Found
            Access:MANUFACT.Clearkey(man:Manufacturer_Key)
            man:Manufacturer    = mod:Manufacturer
            If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Found
                If man:ApplyMSNFormat
                    If CheckFaultFormat(xch:MSN,man:MSNFormat)
                        Case Missive('The M.S.N. has failed format validation.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        xch:MSN = ''
                        Select(?xch:MSN)
                    End !If CheckFieldFormat(xch:MSN,man:MSNFormat)
                End !If man:ApplyMSNFormat
            Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                !Error
            End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        Else ! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Error
        End !If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
    End !If ?xch:MSN{prop:Hide} = 0
    Display()
msn_check       Routine
    Hide(?xch:msn)
    Hide(?xch:msn:prompt)
    access:modelnum.clearkey(mod:model_number_key)
    mod:model_number    = xch:model_number
    If access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
        access:manufact.clearkey(man:manufacturer_key)
        man:manufacturer    = mod:manufacturer
        If access:manufact.fetch(man:manufacturer_key) = Level:Benign
            If man:use_msn = 'YES'
                Unhide(?xch:msn)
                Unhide(?xch:msn:prompt)
            End
        End!If access:manufact.fetch(man:manufacturer_key)
    End!If access:modelnum.tryfetch(mod:model_number_key) = Level:Benign
    Display()
Fill_Lists      Routine
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Exchange Unit'
  OF ChangeRecord
    ActionMessage = 'Changing An Exchange Unit'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020274'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Exchange')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOA:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(xch:Record,History::xch:Record)
  SELF.AddHistoryField(?xch:Ref_Number,1)
  SELF.AddHistoryField(?xch:Date_Booked,9)
  SELF.AddHistoryField(?xch:ESN,4)
  SELF.AddHistoryField(?xch:MSN,5)
  SELF.AddHistoryField(?xch:Model_Number,2)
  SELF.AddHistoryField(?xch:Colour,6)
  SELF.AddHistoryField(?xch:Manufacturer,3)
  SELF.AddHistoryField(?xch:Location,7)
  SELF.AddHistoryField(?xch:Shelf_Location,8)
  SELF.AddHistoryField(?xch:Stock_Type,13)
  SELF.AddHistoryField(?xch:Audit_Number,14)
  SELF.AddUpdateFile(Access:EXCHANGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:ACCESSOR.Open
  Relate:DEFAULTS.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:USERS_ALIAS.Open
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:MODELNUM.UseFile
  Access:MODELCOL.UseFile
  Access:LOCATION.UseFile
  Access:LOCSHELF.UseFile
  Access:STOCKTYP.UseFile
  Access:USERS.UseFile
  Access:EXCHHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:EXCHANGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:EXCHACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Status
  status_Temp = GetExchangeStatus(xch:Available,xch:Job_Number)
  If ThisWindow.Request <> InsertRecord
      ?xch:Audit_Number{prop:Disable} = 1
  End !ThisWindow.Request <> InsertRecord
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly or glo:WebJob
          ?xch:Location{prop:Disable} = 1
          ?LookupLocation{prop:Disable} = 1
      End !If use:StockFromLocationOnly
      If ThisWindow.Request = InsertRecord
          xch:Location    = use:Location
      End !If ThisWindow.Request = InsertRecord
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  ! Save Window Name
   AddToLog('Window','Open','Update_Exchange')
  ?List{prop:vcr} = TRUE
  ?xch:Audit_Number{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?xch:Model_Number{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?xch:Model_Number{Prop:Tip}
  END
  IF ?xch:Model_Number{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?xch:Model_Number{Prop:Msg}
  END
  IF ?xch:Colour{Prop:Tip} AND ~?LookupModelColour{Prop:Tip}
     ?LookupModelColour{Prop:Tip} = 'Select ' & ?xch:Colour{Prop:Tip}
  END
  IF ?xch:Colour{Prop:Msg} AND ~?LookupModelColour{Prop:Msg}
     ?LookupModelColour{Prop:Msg} = 'Select ' & ?xch:Colour{Prop:Msg}
  END
  IF ?xch:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?xch:Location{Prop:Tip}
  END
  IF ?xch:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?xch:Location{Prop:Msg}
  END
  IF ?xch:Shelf_Location{Prop:Tip} AND ~?LookupShelfLocation{Prop:Tip}
     ?LookupShelfLocation{Prop:Tip} = 'Select ' & ?xch:Shelf_Location{Prop:Tip}
  END
  IF ?xch:Shelf_Location{Prop:Msg} AND ~?LookupShelfLocation{Prop:Msg}
     ?LookupShelfLocation{Prop:Msg} = 'Select ' & ?xch:Shelf_Location{Prop:Msg}
  END
  IF ?xch:Stock_Type{Prop:Tip} AND ~?LookupStockType{Prop:Tip}
     ?LookupStockType{Prop:Tip} = 'Select ' & ?xch:Stock_Type{Prop:Tip}
  END
  IF ?xch:Stock_Type{Prop:Msg} AND ~?LookupStockType{Prop:Msg}
     ?LookupStockType{Prop:Msg} = 'Select ' & ?xch:Stock_Type{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,xca:Ref_Number_Key)
  BRW7.AddRange(xca:Ref_Number,xch:Ref_Number)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(?xca:Accessory,xca:Accessory,1,BRW7)
  BRW7.AddField(xca:Accessory,BRW7.Q.xca:Accessory)
  BRW7.AddField(xca:Ref_Number,BRW7.Q.xca:Ref_Number)
  BRW7.AskProcedure = 6
  FDB9.Init(?xch:Audit_Number,Queue:FileDrop.ViewPosition,FDB9::View:FileDrop,Queue:FileDrop,Relate:EXCAUDIT,ThisWindow)
  FDB9.Q &= Queue:FileDrop
  FDB9.AddSortOrder(exa:TypeStockNumber)
  FDB9.AddRange(exa:Stock_Type,xch:Stock_Type)
  FDB9.AddField(exa:Audit_Number,FDB9.Q.exa:Audit_Number)
  FDB9.AddField(exa:Record_Number,FDB9.Q.exa:Record_Number)
  FDB9.AddUpdateField(exa:Audit_Number,xch:Audit_Number)
  ThisWindow.AddItem(FDB9.WindowComponent)
  FDB9.DefaultFill = 0
  if self.Request = InsertRecord
      FDB9.SetFilter('Upper(exa:stock_unit_number) = 0')
  end
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Update_Exchange')
  If print_label_temp <> 'NO' And thiswindow.request = insertrecord
      glo:select1 = xch:ref_number
      case print_label_temp
          of '1'
              exchange_unit_label
          of '2'
              exchange_unit_label_b452
      end!case def:label_printer_type
      glo:select1 = ''
  End!If print_label_temp = 'YES' And thiswindow.request = insertrecord
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    xch:Date_Booked = Today()
    xch:Available = 'AVL'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Model_Numbers
      Pick_Model_Colour
      PickSiteLocations
      BrowseShelfLocations
      Select_Stock_Type
      Update_Exchange_Accessory
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = xch:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?xch:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:ESN, Accepted)
      xch:Model_Number = IMEIModelRoutine(xch:ESN,xch:Model_Number)
      
      IF (xch:Model_Number <> '')
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number = xch:Model_Number
          IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = LeveL:Benign)
              xch:Manufacturer = mod:Manufacturer
          END ! IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = LeveL:Benign)
      
          Post(Event:Accepted,?xch:Model_Number)
      
          DO MSN_Check
      
          IF (?xch:MSN{prop:Hide} = 0)
              Select(?xch:MSN)
          ELSE
              If (xch:Colour <> '' And ThisWindow.Request = InsertRecord)
                  Select(?xch:Location)
              ELSE ! If (xch:Colour <> '' And ThisWindow.Request = InsertRecord)
                  Select(?xch:Colour)
              END ! If (xch:Colour <> '' And ThisWindow.Request = InsertRecord)
           END
      END ! IF (xch:Model_Number <> '')
      ! #11897 Was still using the old routine for imei/model correlation (Bryan: 19/01/2011)
      !Esn_Model_Routine(xch:esn,xch:model_number,model",pass")
      !If pass" = 1
      !    xch:model_number = model"
      !
      !    access:modelnum.clearkey(mod:model_number_key)
      !    mod:model_number = xch:model_number
      !    if access:modelnum.fetch(mod:model_number_key) = Level:Benign
      !        xch:manufacturer = mod:manufacturer
      !    end
      !
      !    Post(event:accepted,?xch:model_number)
      !    Do msn_check
      !    If ?xch:msn{prop:hide} = 0
      !        Select(?xch:msn)
      !    Else!If ?xsn:msn{prop:hide} = 0
      !        If xch:colour <> '' and thiswindow.request  = Insertrecord
      !            Select(?xch:location)
      !        Else!If xch:colour <> '' and thiswindow.request  = Insertrecord
      !            Select(?xch:colour)
      !        End!If xch:colour <> '' and thiswindow.request  = Insertrecord
      !    End!If ?xsn:msn{prop:hide} = 0
      !End!If fill_in# = 1
      
      ! Inserting (DBH 01/06/2006) #6972 - Is the format of the IMEI Valid?
      If IsIMEIValid(xch:ESN,xch:Model_Number,1) = False
          xch:ESN = ''
          Display()
          Cycle
      End ! If IsIMEIValid(xch:ESN,xch:Model_Number,1) = False
      ! End (DBH 01/06/2006) #6972
      Display()
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:ESN, Accepted)
    OF ?xch:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Accepted)
      If xch:model_number <> '' And thiswindow.request = Insertrecord
          If xch:colour <> ''
              Select(?xch:location)
          Else!If xch:colour <> ''
              Select(?xch:colour)
          End!If xch:colour <> ''
      End!If xch:model_number <> ''
      
      Do ValidateMSN
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:MSN, Accepted)
    OF ?xch:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, Accepted)
      IF xch:Model_Number OR ?xch:Model_Number{Prop:Req}
        mod:Model_Number = xch:Model_Number
        !Save Lookup Field Incase Of error
        look:xch:Model_Number        = xch:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            xch:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            xch:Model_Number = look:xch:Model_Number
            SELECT(?xch:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      xch:manufacturer  = mod:manufacturer
      count# = 0
      colour" = ''
      save_moc_id = access:modelcol.savefile()
      access:modelcol.clearkey(moc:colour_key)
      moc:model_number = xch:model_number
      set(moc:colour_key,moc:colour_key)
      loop
          if access:modelcol.next()
             break
          end !if
          if moc:model_number <> xch:model_number      |
              then break.  ! end if
          count# += 1
          colour" = moc:colour
          If count# > 1
              Break
          End!If count# > 1
      end !loop
      access:modelcol.restorefile(save_moc_id)
      If count# = 1
          xch:colour  = colour"
      End!If count# = 1
      Do msn_check
      Do ValidateMSN
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, Accepted)
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = xch:Model_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          xch:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?xch:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Model_Number)
    OF ?xch:Colour
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, Accepted)
      glo:select12   = xch:model_number
      moc:model_number  = xch:model_number
      IF xch:Colour OR ?xch:Colour{Prop:Req}
        moc:Colour = xch:Colour
        moc:Model_Number = xch:Model_Number
        !Save Lookup Field Incase Of error
        look:xch:Colour        = xch:Colour
        IF Access:MODELCOL.TryFetch(moc:Colour_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            xch:Colour = moc:Colour
          ELSE
            CLEAR(moc:Model_Number)
            !Restore Lookup On Error
            xch:Colour = look:xch:Colour
            SELECT(?xch:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select12   = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, Accepted)
    OF ?LookupModelColour
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelColour, Accepted)
      glo:select12   = xch:model_number
      moc:Colour = xch:Colour
      moc:Model_Number = xch:Model_Number
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          xch:Colour = moc:Colour
          Select(?+1)
      ELSE
          Select(?xch:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Colour)
      glo:select12   = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupModelColour, Accepted)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      glo:select1 = xch:model_number
      Pick_Accessories
      IF Records(glo:Q_Accessory) <> ''
          Loop x# = 1 To Records(glo:Q_Accessory)
              get(glo:Q_Accessory,x#)
              get(exchacc,0)
              if access:exchacc.primerecord() = level:benign
                  xca:ref_number  = xch:ref_number
                  xca:accessory   = glo:accessory_pointer
                  access:exchacc.tryinsert()
              end!if access:exchacc.primerecord() = level:benign
          End
          BRW7.ResetSort(1)
      End
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?xch:Location
      IF xch:Location OR ?xch:Location{Prop:Req}
        loc:Location = xch:Location
        !Save Lookup Field Incase Of error
        look:xch:Location        = xch:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            xch:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            xch:Location = look:xch:Location
            SELECT(?xch:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loc:Location = xch:Location
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          xch:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?xch:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Location)
    OF ?xch:Shelf_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, Accepted)
      glo:select2    = xch:location
      los:site_location = xch:location
      IF xch:Shelf_Location OR ?xch:Shelf_Location{Prop:Req}
        los:Shelf_Location = xch:Shelf_Location
        los:Site_Location = xch:Location
        GLO:Select1 = xch:Location
        !Save Lookup Field Incase Of error
        look:xch:Shelf_Location        = xch:Shelf_Location
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            xch:Shelf_Location = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            xch:Shelf_Location = look:xch:Shelf_Location
            SELECT(?xch:Shelf_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select2    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, Accepted)
    OF ?LookupShelfLocation
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupShelfLocation, Accepted)
      glo:select2    = xch:location
      los:Shelf_Location = xch:Shelf_Location
      GLO:Select1 = xch:Location
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          xch:Shelf_Location = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?xch:Shelf_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Shelf_Location)
      glo:select2    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupShelfLocation, Accepted)
    OF ?xch:Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Stock_Type, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange  = 'YES'
      IF xch:Stock_Type OR ?xch:Stock_Type{Prop:Req}
        stp:Stock_Type = xch:Stock_Type
        !Save Lookup Field Incase Of error
        look:xch:Stock_Type        = xch:Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            xch:Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            xch:Stock_Type = look:xch:Stock_Type
            SELECT(?xch:Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Stock_Type, Accepted)
    OF ?LookupStockType
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupStockType, Accepted)
      glo:select1    = 'EXC'
      stp:Stock_Type = xch:Stock_Type
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          xch:Stock_Type = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?xch:Stock_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?xch:Stock_Type)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupStockType, Accepted)
    OF ?Button5
      ThisWindow.Update
      Browse_Exchange_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?Print_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Label, Accepted)
      If thiswindow.request = Insertrecord
          Case Missive('Cannot print a label until this record has been saved.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If thiswindow.request = Insertrecord
          glo:select1 = xch:ref_number
          set(defaults)
          access:defaults.next()
          case def:label_printer_type
              of 'TEC B-440 / B-442'
                  exchange_unit_label
              of 'TEC B-452'
                  exchange_unit_label_b452
          end!case def:label_printer_type
          glo:select1 = ''
      End!If thiswindow.request = Insertrecord
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Label, Accepted)
    OF ?New_Audit_Number
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?New_Audit_Number, Accepted)
      If SecurityCheck('EXCHANGE - NEW AUDIT NOS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
        do_update# = false
      Else!if x" = false
          If xch:stock_type = ''
              Case Missive('You must insert a Stock Type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
          Else!If xch:stock_type = ''
              Add_Exchange_Audit_Numbers(xch:stock_type)
              Do fill_lists
          End!If xch:stock_type = ''
      end!if x" = false
      FDB9.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?New_Audit_Number, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020274'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020274'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020274'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Validation
  If ESN_Temp <> xch:ESN
      Found# = 0
      Access:EXCHANGE_ALIAS.Clearkey(xch_ali:ESN_Only_Key)
      xch_ali:ESN = xch:ESN
      Set(xch_ali:ESN_Only_Key,xch_ali:ESN_Only_Key)
      Loop ! Begin Loop
          If Access:EXCHANGE_ALIAS.Next()
              Break
          End ! If Access:EXCHANGE_ALIAS.Next()
          If xch_ali:ESN <> xch:ESN
              Break
          End ! If xch_ali:ESN <> xch:ESN
          If xch_ali:Ref_Number <> xch:Ref_Number
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected I.M.E.I. Number already exists in Exchange Stock.','ServiceBase 3g',|
                             'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
              Found# = 1
              Break
          End ! If xch_ali:Ref_Number <> xch:Ref_Number
      End ! Loop
      If Found# = 1
          Cycle
      End ! If Found# = 1
  
      Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
      job_ali:ESN = xch:ESN
      If Access:JOBS_ALIAS.TryFetch(job_ali:ESN_Key) = Level:Benign
          !Found
          If job_ali:Date_Completed = ''
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('You are attempting to insert an Exchange Unit with the same I.M.E.I. Number as job number ' & Clip(job_ali:Ref_Number) & '.'&|
                  '|You must use Rapid Job Update if you wish to return this unit this Exchange Stock.','ServiceBase 3g',|
                             'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
              Cycle
          End ! If job_ali:Date_Completed = ''
      Else ! If Access:JOBS_ALIAS.TryFetch(job_ali:ESN_Key) = Level:Benign
          !Error
      End ! If Access:JOBS_ALIAS.TryFetch(job_ali:ESN_Key) = Level:Benign
  
      ! Inserting (DBH 21/07/2008) # 10130 - Allow to insert unit if already exists in Loan Stock
      Access:LOAN.ClearKey(loa:ESN_Only_Key)
      loa:ESN = xch:ESN
      If Access:LOAN.TryFetch(loa:ESN_Only_Key) = Level:Benign
          !Found
          If loa:Available = 'NOA'
              Beep(Beep:SystemQuestion)  ;  Yield()
              Case Missive('The selected I.M.E.I. Number already exists in Loan Stock but is "Not Available"'&|
                  '|'&|
                  '|Are you sure you want to continue and add this unit to Exchange Stock?','ServiceBase 3g',|
                             'mquest.jpg','\&No|/&Yes')
                  Of 2 ! &Yes Button
                  Of 1 ! &No Button
                      Cycle
              End!Case Message
          Else ! If loa:Available = 'NOA'
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected I.M.E.I. Number exists in Loan Stock but it is still "Available"','ServiceBase 3g',|
                             'mstop.jpg','/&OK')
                  Of 1 ! &OK Button
              End!Case Message
              Cycle
          End ! If loa:Available = 'NOA'
      Else ! If Access:LOAN.TryFetch(loa:ESN_Key) = Level:Benign
          !Error
      End ! If Access:LOAN.TryFetch(loa:ESN_Key) = Level:Benign
      ! End (DBH 21/07/2008) #10130
  
  
      Access:MODELNUM.ClearKey(mod:Model_Number_Key)
      mod:Model_Number = xch:Model_Number
      If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
          !Found
          If Len(Clip(xch:esn)) < mod:esn_length_from Or Len(Clip(xch:esn)) > mod:esn_length_to
              Case Missive('The I.M.E.I. Number entered should be between ' & Clip(mod:ESN_Length_From) & ' and ' & CLip(mod:ESN_Length_To) & ' characters in length.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
          If man:use_msn = 'YES'
              If len(Clip(xch:msn)) < mod:msn_length_from Or len(Clip(xch:msn)) > mod:msn_length_to
                  Case Missive('The M.S.N. entered should be between ' & clip(mod:MSN_Length_From) & ' and ' & Clip(mod:MSN_Length_To) & ' characters in length.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
                  Cycle
              End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
          End!If man:use_msn = 'YES'
      Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
          !Error
      End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
  End ! If ESN_Temp <> xch:ESN
  !Audit Number Check
  If thiswindow.request <> Insertrecord
      If audit_number_temp <> xch:audit_number
          If xch:audit_number <> ''
              Case Missive('Are you sure you want to change the Audit Number of this Exchange Unit?','ServiceBase 3g',|
                             'mquest.jpg','\No|/Yes')
                  Of 2 ! Yes Button
                      access:excaudit.clearkey(exa:audit_number_key)
                      exa:stock_type   = xch:stock_type
                      exa:audit_number = xch:audit_number
                      if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                          !This should never happen, because used audit numbers aren't shown in the list
                      Else!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                          get(exchhist,0)
                          if access:exchhist.primerecord() = level:benign
                              exh:ref_number   = xch:ref_number
                              exh:date          = today()
                              exh:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              exh:user = use:user_code
                              exh:status        = 'AUDIT NUMBER CHANGED TO' & Clip(xch:audit_number)
                              access:exchhist.insert()
                          end!if access:exchhist.primerecord() = level:benign
                      end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                  Of 1 ! No Button
                      xch:audit_number    = audit_number_temp
              End ! Case Missive
          Else!If xch:audit_number <> ''
  
          End!If xch:audit_number <> ''
      End!If audit_number_temp = xch:audit_number
  End!If thiswindow.request <> Insertrecord
  
  access:excaudit.clearkey(exa:audit_number_key)
  exa:stock_type   = xch:stock_type
  exa:audit_number = xch:audit_number
  if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
      If exa:stock_unit_number = 0
          exa:stock_unit_number = xch:ref_number
          access:excaudit.update()
          get(exchhist,0)
          if access:exchhist.primerecord() = level:benign
              exh:ref_number   = xch:ref_number
              exh:date          = today()
              exh:time          = clock()
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              exh:user = use:user_code
              exh:status        = 'ASSIGNED AUDIT NUMBER: ' & Clip(xch:audit_number)
              access:exchhist.insert()
          end!if access:exchhist.primerecord() = level:benign
      End!If exa:stock_unit_number = ''
  end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
  
  
  ReturnValue = PARENT.TakeCompleted()
  If thiswindow.request = Insertrecord
      Set(defaults)
      access:defaults.next()
      If def:use_loan_exchange_label = 'YES'
          case def:label_printer_type
              of 'TEC B-440 / B-442'
                  print_label_temp = '1'
              of 'TEC B-452'
                  print_label_temp = '2'
          end!case def:label_printer_type
      Else
          Print_label_temp = 'NO'
      End!If def:use_loan_exchange_label = 'YES'
  
      !Add initial entry
      if access:exchhist.primerecord() = Level:Benign
          exh:ref_number      = xch:ref_number
          exh:date            = Today()
          exh:time            = Clock()
          access:users.clearkey(use:password_key)
          use:password        = glo:password
          access:users.fetch(use:password_key)
          exh:user            = use:user_code
          exh:status          = 'INITIAL ENTRY'
          access:exchhist.insert()
      end!if access:exchhist.primerecord() = Level:Benign
  
  Else
      print_label_temp = 'NO'
  End!If thiswindow.request = Insertrecord
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?xch:Model_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, AlertKey)
      Post(Event:Accepted,?LookupModelNumber)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Model_Number, AlertKey)
    END
  OF ?xch:Colour
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, AlertKey)
      Post(event:accepted,?LookupModelColour)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Colour, AlertKey)
    END
  OF ?xch:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Location, AlertKey)
      Post(event:accepted,?LookupLocation)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Location, AlertKey)
    END
  OF ?xch:Shelf_Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, AlertKey)
      Post(Event:accepted,?LookupShelfLocation)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xch:Shelf_Location, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?xca:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xca:Accessory, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xca:Accessory, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do msn_check
      esn_temp = xch:esn
      audit_number_temp   = xch:audit_number
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Select_Stock_Type PROCEDURE                           !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
yes_temp             STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                       PROJECT(stp:Use_Loan)
                       PROJECT(stp:Use_Exchange)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
stp:Use_Loan           LIKE(stp:Use_Loan)             !Browse key field - type derived from field
stp:Use_Exchange       LIKE(stp:Use_Exchange)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select A Stock Type'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Stock Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Stock Type'),USE(?Tab:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(264,114,148,210),USE(?Browse:1),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Stock Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           ENTRY(@s30),AT(264,98,124,10),USE(stp:Stock_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'LOA' 
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'EXC'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020291'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Stock_Type')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCKTYP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Select_Stock_Type')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,stp:Use_Loan_Key)
  BRW1.AddRange(stp:Use_Loan,yes_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STP:Stock_Type,stp:Stock_Type,1,BRW1)
  BRW1.AddSortOrder(,stp:Use_Exchange_Key)
  BRW1.AddRange(stp:Use_Exchange,yes_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?STP:Stock_Type,stp:Stock_Type,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stp:Stock_Type_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?stp:Stock_Type,stp:Stock_Type,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(stp:Stock_Type,BRW1.Q.stp:Stock_Type)
  BRW1.AddField(stp:Use_Loan,BRW1.Q.stp:Use_Loan)
  BRW1.AddField(stp:Use_Exchange,BRW1.Q.stp:Use_Exchange)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCKTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Select_Stock_Type')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020291'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020291'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020291'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'LOA' 
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(glo:select1) = 'EXC'
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Job_Exchange_History PROCEDURE                 !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBEXHIS)
                       PROJECT(jxh:Date)
                       PROJECT(jxh:Time)
                       PROJECT(jxh:User)
                       PROJECT(jxh:Loan_Unit_Number)
                       PROJECT(jxh:Status)
                       PROJECT(jxh:record_number)
                       PROJECT(jxh:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jxh:Date               LIKE(jxh:Date)                 !List box control field - type derived from field
jxh:Time               LIKE(jxh:Time)                 !List box control field - type derived from field
jxh:User               LIKE(jxh:User)                 !List box control field - type derived from field
jxh:Loan_Unit_Number   LIKE(jxh:Loan_Unit_Number)     !List box control field - type derived from field
jxh:Status             LIKE(jxh:Status)               !List box control field - type derived from field
jxh:record_number      LIKE(jxh:record_number)        !Primary key field - type derived from field
jxh:Ref_Number         LIKE(jxh:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Exchange History'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,100,344,228),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48R(2)|M~Date~C(0)@d6b@24R(2)|M~Time~C(0)@T1@20C|M~User~@s3@64L(2)|M~Exchange Un' &|
   'it No~@n012@80L(2)|M~Status~@s60@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Exchange History File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020267'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Job_Exchange_History')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBEXHIS.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBEXHIS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Job_Exchange_History')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,jxh:Ref_Number_Key)
  BRW1.AddRange(jxh:Ref_Number,Relate:JOBEXHIS,Relate:JOBS)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,jxh:Date,1,BRW1)
  BRW1.AddField(jxh:Date,BRW1.Q.jxh:Date)
  BRW1.AddField(jxh:Time,BRW1.Q.jxh:Time)
  BRW1.AddField(jxh:User,BRW1.Q.jxh:User)
  BRW1.AddField(jxh:Loan_Unit_Number,BRW1.Q.jxh:Loan_Unit_Number)
  BRW1.AddField(jxh:Status,BRW1.Q.jxh:Status)
  BRW1.AddField(jxh:record_number,BRW1.Q.jxh:record_number)
  BRW1.AddField(jxh:Ref_Number,BRW1.Q.jxh:Ref_Number)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBEXHIS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Job_Exchange_History')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020267'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020267'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020267'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

Browse_Exchange_Accessory PROCEDURE (f_ref_number)    !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ref_number_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(EXCHACC)
                       PROJECT(xca:Accessory)
                       PROJECT(xca:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
xca:Accessory          LIKE(xca:Accessory)            !List box control field - type derived from field
GLO:Select1            STRING(1)                      !Browse hot field - unable to determine correct data type
xca:Ref_Number         LIKE(xca:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Exchange Accessory File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Accessory~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Accessory'),USE(?Tab:2)
                           ENTRY(@s30),AT(264,98,124,10),USE(xca:Accessory),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Exchange Accessory File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020264'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Exchange_Accessory')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  ref_number_temp = f_ref_number
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCHACC.Open
  Access:EXCHANGE.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCHACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Exchange_Accessory')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,xca:Ref_Number_Key)
  BRW1.AddRange(xca:Ref_Number,ref_number_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?xca:Accessory,xca:Accessory,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(xca:Accessory,BRW1.Q.xca:Accessory)
  BRW1.AddField(GLO:Select1,BRW1.Q.GLO:Select1)
  BRW1.AddField(xca:Ref_Number,BRW1.Q.xca:Ref_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Exchange_Accessory')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020264'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020264'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020264'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?xca:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xca:Accessory, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?xca:Accessory, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ?WindowTitle{Prop:text} = 'Accessories For Exchange Unit: ' & f_ref_number
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Loan_Accessories PROCEDURE (f_ref_number)      !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ref_number_temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOANACC)
                       PROJECT(lac:Accessory)
                       PROJECT(lac:Accessory_Status)
                       PROJECT(lac:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
lac:Accessory          LIKE(lac:Accessory)            !List box control field - type derived from field
lac:Accessory_NormalFG LONG                           !Normal forground color
lac:Accessory_NormalBG LONG                           !Normal background color
lac:Accessory_SelectedFG LONG                         !Selected forground color
lac:Accessory_SelectedBG LONG                         !Selected background color
GLO:Select1            STRING(1)                      !Browse hot field - unable to determine correct data type
lac:Accessory_Status   LIKE(lac:Accessory_Status)     !Browse hot field - type derived from field
lac:Ref_Number         LIKE(lac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Loan Accessory File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M*~Accessory~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Accessory'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(lac:Accessory),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Issue_Button),TRN,FLAT,LEFT,ICON('issaccp.jpg')
                           BUTTON,AT(448,148),USE(?Return_Button),TRN,FLAT,LEFT,ICON('retaccp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Loan Accessory File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020282'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Loan_Accessories')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  ref_number_temp = f_ref_number
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:LOANACC.Open
  Access:JOBS.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOANACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Loan_Accessories')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,lac:Ref_Number_Key)
  BRW1.AddRange(lac:Ref_Number,ref_number_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?lac:Accessory,lac:Accessory,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(lac:Accessory,BRW1.Q.lac:Accessory)
  BRW1.AddField(GLO:Select1,BRW1.Q.GLO:Select1)
  BRW1.AddField(lac:Accessory_Status,BRW1.Q.lac:Accessory_Status)
  BRW1.AddField(lac:Ref_Number,BRW1.Q.lac:Ref_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:LOANACC.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Loan_Accessories')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Issue_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Issue_Button, Accepted)
      BRW1.UpdateBuffer()
      
      RefNo#    = lac:Ref_Number
      Accessor" = lac:Accessory
      
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = RefNo#
      lac:Accessory  = Accessor"
      if not Access:LOANACC.Fetch(lac:Ref_Number_Key)
          if lac:Accessory_Status <> 'ISSUE'
              lac:Accessory_Status = 'ISSUE'
              if not Access:LOANACC.TryUpdate()
      
                  IF (AddToAudit(job:ref_number,'LOA','ACCESSORY ISSUED','ACCESSORY ISSUED : ' & clip(lac:Accessory)))
                  END ! IF
      
              end
          end
      end
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Issue_Button, Accepted)
    OF ?Return_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Return_Button, Accepted)
      BRW1.UpdateBuffer()
      
      RefNo#    = lac:Ref_Number
      Accessor" = lac:Accessory
      
      Access:LOANACC.ClearKey(lac:Ref_Number_Key)
      lac:Ref_Number = RefNo#
      lac:Accessory  = Accessor"
      if not Access:LOANACC.Fetch(lac:Ref_Number_Key)
          if lac:Accessory_Status <> 'RETURN'
              lac:Accessory_Status = 'RETURN'
              if not Access:LOANACC.TryUpdate()
      
                  IF (AddToAudit(job:ref_number,'LOA','ACCESSORY RETURNED','ACCESSORY RETURNED : ' & clip(lac:Accessory)))
                  END ! IF
      
              end
          end
      end
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Return_Button, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020282'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020282'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020282'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?lac:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lac:Accessory, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lac:Accessory, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      QuickWindow{Prop:text} = 'Accessories For Loan Unit: ' & f_ref_number
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (lac:Accessory_Status = 'ISSUE')
    SELF.Q.lac:Accessory_NormalFG = 32768
    SELF.Q.lac:Accessory_NormalBG = -1
    SELF.Q.lac:Accessory_SelectedFG = -1
    SELF.Q.lac:Accessory_SelectedBG = 32768
  ELSIF (lac:Accessory_Status = 'RETURN')
    SELF.Q.lac:Accessory_NormalFG = 16711680
    SELF.Q.lac:Accessory_NormalBG = -1
    SELF.Q.lac:Accessory_SelectedFG = -1
    SELF.Q.lac:Accessory_SelectedBG = 16711680
  ELSE
    SELF.Q.lac:Accessory_NormalFG = -1
    SELF.Q.lac:Accessory_NormalBG = -1
    SELF.Q.lac:Accessory_SelectedFG = -1
    SELF.Q.lac:Accessory_SelectedBG = -1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Browse_Loan_History PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOANHIST)
                       PROJECT(loh:Date)
                       PROJECT(loh:Time)
                       PROJECT(loh:User)
                       PROJECT(loh:Status)
                       PROJECT(loh:Notes)
                       PROJECT(loh:record_number)
                       PROJECT(loh:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
loh:Date               LIKE(loh:Date)                 !List box control field - type derived from field
loh:Time               LIKE(loh:Time)                 !List box control field - type derived from field
loh:User               LIKE(loh:User)                 !List box control field - type derived from field
loh:Status             LIKE(loh:Status)               !List box control field - type derived from field
loh:Notes              LIKE(loh:Notes)                !List box control field - type derived from field
loh:record_number      LIKE(loh:record_number)        !Primary key field - type derived from field
loh:Ref_Number         LIKE(loh:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Loan Unit History File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,102,344,146),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('48R(2)|M~Date~L@d6b@24R(2)|M~Time~@T1@22L(2)|M~User~@s3@240L(2)|M~Status~@s60@0L' &|
   '(2)|M~Notes~@s255@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Loan Unit History File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Date'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(168,252,344,74),USE(loh:Notes),SKIP,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),UPR,READONLY
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020283'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Loan_History')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOANHIST.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOANHIST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Browse_Loan_History')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,loh:Ref_Number_Key)
  BRW1.AddRange(loh:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,loh:Date,1,BRW1)
  BRW1.AddField(loh:Date,BRW1.Q.loh:Date)
  BRW1.AddField(loh:Time,BRW1.Q.loh:Time)
  BRW1.AddField(loh:User,BRW1.Q.loh:User)
  BRW1.AddField(loh:Status,BRW1.Q.loh:Status)
  BRW1.AddField(loh:Notes,BRW1.Q.loh:Notes)
  BRW1.AddField(loh:record_number,BRW1.Q.loh:record_number)
  BRW1.AddField(loh:Ref_Number,BRW1.Q.loh:Ref_Number)
  QuickWindow{PROP:MinWidth}=40
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOANHIST.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Browse_Loan_History')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020283'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020283'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020283'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

UpdateLOAN PROCEDURE                                  !Generated from procedure template - Window

CurrentTab           STRING(80)
save_loan_alias_id   USHORT,AUTO
print_label_temp     STRING(3)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
status_temp          STRING(10)
yes_temp             STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW7::View:Browse    VIEW(LOANACC)
                       PROJECT(lac:Accessory)
                       PROJECT(lac:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
lac:Accessory          LIKE(lac:Accessory)            !List box control field - type derived from field
lac:Ref_Number         LIKE(lac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::loa:Record  LIKE(loa:RECORD),STATIC
QuickWindow          WINDOW('Update the Loan File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,552,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Loan Unit Number'),AT(68,75),USE(?LOA:Ref_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@p<<<<<<<#pb),AT(148,75,64,10),USE(loa:Ref_Number),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(68,115),USE(?LOA:ESN:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(148,115,124,10),USE(loa:ESN),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                           ENTRY(@s30),AT(364,99,125,10),USE(lac:Accessory),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           PROMPT('M.S.N.'),AT(68,137),USE(?loa:MSN:Prompt),TRN,HIDE,FONT(,8,COLOR:White,FONT:bold)
                           ENTRY(@s30),AT(148,137,124,10),USE(loa:MSN),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(364,115,148,140),USE(?List),IMM,VSCROLL,COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)~Accessory~@s30@'),FROM(Queue:Browse)
                           PROMPT('Model Number'),AT(68,155),USE(?loa:model_number:prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(148,155,124,10),USE(loa:Model_Number),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ
                           BUTTON,AT(276,153),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(520,139),USE(?Button4),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           PROMPT('Manufacturer'),AT(68,177),USE(?LOA:Manufacturer:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(148,177,124,10),USE(loa:Manufacturer),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Colour'),AT(68,195),USE(?LOA:Manufacturer:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(148,195,124,10),USE(loa:Colour),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(276,190),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Location'),AT(68,214),USE(?loa:location:prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(148,214,124,10),USE(loa:Location),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(276,211),USE(?buttonLookupLocation),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(148,234,124,10),USE(loa:Shelf_Location),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(276,230),USE(?CallLookup:4),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Shelf Location'),AT(68,235),USE(?Prompt8),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(520,195),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           PROMPT('Stock Type'),AT(68,254),USE(?Prompt10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(148,254,124,10),USE(loa:Stock_Type),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(276,250),USE(?CallLookup:5),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(68,283),USE(?Button5),TRN,FLAT,LEFT,ICON('unihistp.jpg')
                           BUTTON,AT(136,283),USE(?print_Label),TRN,FLAT,LEFT,ICON('prnlabp.jpg')
                           ENTRY(@d6b),AT(220,75,52,10),USE(loa:Date_Booked),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Job Number'),AT(220,89),USE(?loa:job_number:prompt),HIDE,FONT(,7,COLOR:White,FONT:bold)
                           PROMPT('Accessories'),AT(365,86),USE(?Prompt14),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Status'),AT(67,97),USE(?status_temp:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s10),AT(148,97,64,10),USE(status_temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@p<<<<<<<#pb),AT(220,97,52,10),USE(loa:Job_Number),SKIP,HIDE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:Silver),UPR,READONLY
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Loan Unit'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  IncrementalLocatorClass          !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_loa_ali_id   ushort,auto
save_esn_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:loa:Model_Number                Like(loa:Model_Number)
look:loa:Colour                Like(loa:Colour)
look:loa:Location                Like(loa:Location)
look:loa:Shelf_Location                Like(loa:Shelf_Location)
look:loa:Stock_Type                Like(loa:Stock_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
msn_check       Routine
    Hide(?loa:msn)
    Hide(?loa:msn:prompt)
    access:manufact.clearkey(man:manufacturer_key)
    man:manufacturer    = loa:manufacturer
    If access:manufact.fetch(man:manufacturer_key) = Level:Benign
        If man:use_msn = 'YES'
            Unhide(?loa:msn)
            Unhide(?loa:msn:prompt)
        End
    End!If access:manufact.fetch(man:manufacturer_key)
    Display()
Check_Esn       Routine
    error# = 0

    setcursor(cursor:wait)

    save_loa_ali_id = access:loan_alias.savefile()
    access:loan_alias.clearkey(loa_ali:esn_only_key)
    loa_ali:esn = loa:esn
    set(loa_ali:esn_only_key,loa_ali:esn_only_key)
    loop
        if access:loan_alias.next()
           break
        end !if
        if loa_ali:esn <> loa:esn      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If loa_ali:ref_number <> loa:ref_number
            Case Missive('This I.M.E.I. Number has already been entered as an Exchange Stock Item.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            error# = 1
        End!@If loa_ali:ref_number <> loa:ref_number
    end !loop
    access:loan_alias.restorefile(save_loa_ali_id)
    setcursor()

    If error# = 0
        access:jobs.clearkey(job:esn_key)
        job:esn = loa:esn
        if access:jobs.fetch(job:esn_key) = Level:Benign
           If job:date_completed = ''
                Case Missive('This I.M.E.I. Number is already attached to job number ' & CLip(job:Ref_Number) & '.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0
    access:exchange.clearkey(xch:esn_only_key)
    xch:esn = loa:esn
    if access:exchange.fetch(xch:esn_only_key) = Level:Benign
        Case Missive('This I.M.E.I. Number has already been entered as an Exchange Stock Item.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
        error# = 1
    End!IF error# = 0
    If error# = 0
        access:jobs_alias.clearkey(job_ali:esn_key)
        job_ali:esn = loa:esn
        if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
           If job_ali:date_completed = ''
                Case Missive('This I.M.E.I. Number is already attached to job number ' & Clip(job_ali:Ref_Number) & '.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0

    If error# = 0
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = loa:model_number
        if access:modelnum.fetch(mod:model_number_key) = Level:benign
            If Len(Clip(loa:esn)) < mod:esn_length_from Or Len(Clip(loa:esn)) > mod:esn_length_to
                Case Missive('The I.M.E.I. Number entered should be between ' & Clip(mod:ESN_Length_From) & ' and ' & Clip(mod:ESN_Length_To) & ' characters in length.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                error# = 1
            End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
            If man:use_msn = 'YES'
                If len(Clip(loa:msn)) < mod:msn_length_from Or len(Clip(loa:msn)) > mod:msn_length_to
                    Case Missive('The M.S.N. entered should be between ' & Clip(mod:MSN_Length_From) & ' and ' & Clip(mod:MSN_Length_To) & ' characters in length.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                    error# = 1
                End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
            End!If man:use_msn = 'YES'
        end !if access:modelnum.fetch(mod:model_number_key) = Level:benign
    End!IF error# = 0
    If error# = 1
        loa:esn = ''
        Select(?loa:esn)
    End
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Loan Unit'
  OF ChangeRecord
    ActionMessage = 'Changing A Loan Unit'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020286'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateLOAN')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOA:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(loa:Record,History::loa:Record)
  SELF.AddHistoryField(?loa:Ref_Number,1)
  SELF.AddHistoryField(?loa:ESN,4)
  SELF.AddHistoryField(?loa:MSN,5)
  SELF.AddHistoryField(?loa:Model_Number,2)
  SELF.AddHistoryField(?loa:Manufacturer,3)
  SELF.AddHistoryField(?loa:Colour,6)
  SELF.AddHistoryField(?loa:Location,7)
  SELF.AddHistoryField(?loa:Shelf_Location,8)
  SELF.AddHistoryField(?loa:Stock_Type,13)
  SELF.AddHistoryField(?loa:Date_Booked,9)
  SELF.AddHistoryField(?loa:Job_Number,12)
  SELF.AddUpdateFile(Access:LOAN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCESSOR.Open
  Relate:DEFAULTS.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:LOAN_ALIAS.Open
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:USERS.UseFile
  Access:LOANHIST.UseFile
  Access:MODELNUM.UseFile
  Access:MODELCOL.UseFile
  Access:LOCATION.UseFile
  Access:LOCSHELF.UseFile
  Access:STOCKTYP.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOAN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:LOANACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !Status
  CASE (loa:Available)
  OF 'AVL'
    status_temp = 'AVAILABLE'
  OF 'LOA'
    status_temp = 'LOANED'
  OF 'LOS'
      Status_Temp = 'LOAN LOST'
  OF 'INC'
    status_temp = 'INCOMING TRANSIT'
  OF 'FAU'
    status_temp = 'FAULTY'
   Of 'NOA'
      Status_Temp = 'NOT AVAILABLE'
  OF 'REP'
    status_temp = 'IN REPAIR'
  OF 'SUS'
    status_temp = 'SUSPENDED'
  OF 'DES'
    status_temp = 'DESPATCHED'
  OF 'QA1'
    status_temp = 'ELECTRONIC QA REQUIRED'
  OF 'QA2'
    status_temp = 'MANUAL QA REQUIRED'
  OF 'QAF'
    status_temp = 'QA FAILED'
  OF 'RTS'
    status_temp = 'RETURN TO STOCK'
  ELSE
    status_temp = 'IN REPAIR'
  END
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly or glo:WebJob
          ?loa:Location{prop:Disable} = 1
          ?buttonLookupLocation{prop:Disable} = 1
      End !If use:StockFromLocationOnly
      If ThisWindow.Request = InsertRecord
          loa:Location    = use:Location
      End !If ThisWindow.Request = InsertRecord
  Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Error
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  ! Save Window Name
   AddToLog('Window','Open','UpdateLOAN')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?loa:Model_Number{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?loa:Model_Number{Prop:Tip}
  END
  IF ?loa:Model_Number{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?loa:Model_Number{Prop:Msg}
  END
  IF ?loa:Colour{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?loa:Colour{Prop:Tip}
  END
  IF ?loa:Colour{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?loa:Colour{Prop:Msg}
  END
  IF ?loa:Location{Prop:Tip} AND ~?buttonLookupLocation{Prop:Tip}
     ?buttonLookupLocation{Prop:Tip} = 'Select ' & ?loa:Location{Prop:Tip}
  END
  IF ?loa:Location{Prop:Msg} AND ~?buttonLookupLocation{Prop:Msg}
     ?buttonLookupLocation{Prop:Msg} = 'Select ' & ?loa:Location{Prop:Msg}
  END
  IF ?loa:Shelf_Location{Prop:Tip} AND ~?CallLookup:4{Prop:Tip}
     ?CallLookup:4{Prop:Tip} = 'Select ' & ?loa:Shelf_Location{Prop:Tip}
  END
  IF ?loa:Shelf_Location{Prop:Msg} AND ~?CallLookup:4{Prop:Msg}
     ?CallLookup:4{Prop:Msg} = 'Select ' & ?loa:Shelf_Location{Prop:Msg}
  END
  IF ?loa:Stock_Type{Prop:Tip} AND ~?CallLookup:5{Prop:Tip}
     ?CallLookup:5{Prop:Tip} = 'Select ' & ?loa:Stock_Type{Prop:Tip}
  END
  IF ?loa:Stock_Type{Prop:Msg} AND ~?CallLookup:5{Prop:Msg}
     ?CallLookup:5{Prop:Msg} = 'Select ' & ?loa:Stock_Type{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,lac:Ref_Number_Key)
  BRW7.AddRange(lac:Ref_Number,loa:Ref_Number)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(?lac:Accessory,lac:Accessory,1,BRW7)
  BRW7.AddField(lac:Accessory,BRW7.Q.lac:Accessory)
  BRW7.AddField(lac:Ref_Number,BRW7.Q.lac:Ref_Number)
  BRW7.AskProcedure = 6
  BRW7.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCESSOR.Close
    Relate:DEFAULTS.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:LOAN_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateLOAN')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    loa:Date_Booked = Today()
    loa:Available = 'AVL'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Model_Numbers
      Pick_Model_Colour
      PickSiteLocations
      BrowseShelfLocations
      Select_Stock_Type
      Update_Loan_Accessory
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = loa:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?loa:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Accepted)
      loa:Model_Number = IMEIModelRoutine(loa:ESN,loa:Model_Number)
      
      IF (loa:Model_Number <> '')
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number = loa:Model_Number
          IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
              loa:Manufacturer = mod:Manufacturer
          END ! IF (Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign)
          select(?loa:Colour)
          DO MSN_Check
      END ! IF (loa:Model_Number <> '')
      
      ! #11897 Was still using the old routine for imei/model correlation (Bryan: 19/01/2011)
      !Esn_Model_Routine(loa:esn,loa:model_number,model",pass")
      !If pass" = 1
      !    loa:model_number = model"
      !    access:modelnum.clearkey(mod:model_number_key)
      !    mod:model_number = loa:model_number
      !    if access:modelnum.fetch(mod:model_number_key) = Level:Benign
      !        loa:manufacturer = mod:manufacturer
      !    end
      !    Select(?loa:colour)
      !    Do msn_check
      !End!If fill_in# = 1
      ! Inserting (DBH 01/06/2006) #6972 - Check the validity of the imei number
      If IsIMEIValid(loa:ESN,loa:Model_Number,1) = False
          loa:ESN = ''
          Display()
          Cycle
      End ! If IsIMEIValud(loa:ESN,loa:Model_Number,1) = False
      ! End (DBH 01/06/2006) #6972
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:ESN, Accepted)
    OF ?loa:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:Model_Number, Accepted)
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = loa:model_number
      If access:modelnum.fetch(mod:model_number_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browsemodelnum
          if globalresponse = requestcompleted
              loa:model_number = mod:model_number
              loa:manufacturer = mod:manufacturer
          else
              xch:model_number = ''
          end
          display(?loa:model_number)
          globalrequest     = saverequest#
      Else
          loa:manufacturer    = mod:manufacturer
      End!If access:modelnum.fetch(mod:model_number_key)
      Do msn_check
      IF loa:Model_Number OR ?loa:Model_Number{Prop:Req}
        mod:Model_Number = loa:Model_Number
        !Save Lookup Field Incase Of error
        look:loa:Model_Number        = loa:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            loa:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            loa:Model_Number = look:loa:Model_Number
            SELECT(?loa:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loa:Model_Number, Accepted)
    OF ?CallLookup
      ThisWindow.Update
      mod:Model_Number = loa:Model_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          loa:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?loa:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?loa:Model_Number)
    OF ?Button4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      glo:select1 = loa:model_number
      Pick_Accessories
      IF Records(glo:Q_Accessory) <> ''
          Loop x# = 1 To Records(glo:Q_Accessory)
              Get(glo:Q_Accessory,x#)
              get(loanacc,0)
              if access:loanacc.primerecord() = level:benign
                  lac:ref_number  = loa:ref_number
                  lac:accessory   = glo:accessory_pointer
                  access:loanacc.tryinsert()
              end!if access:loanacc.primerecord() = level:benign
          End
          BRW7.ResetSort(1)
      End
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?loa:Colour
      IF loa:Colour OR ?loa:Colour{Prop:Req}
        moc:Colour = loa:Colour
        moc:Model_Number = loa:Model_Number
        GLO:Select12 = loa:Model_Number
        !Save Lookup Field Incase Of error
        look:loa:Colour        = loa:Colour
        IF Access:MODELCOL.TryFetch(moc:Colour_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            loa:Colour = moc:Colour
          ELSE
            CLEAR(moc:Model_Number)
            CLEAR(GLO:Select12)
            !Restore Lookup On Error
            loa:Colour = look:loa:Colour
            SELECT(?loa:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      moc:Colour = loa:Colour
      moc:Model_Number = loa:Model_Number
      GLO:Select12 = loa:Model_Number
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          loa:Colour = moc:Colour
          Select(?+1)
      ELSE
          Select(?loa:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?loa:Colour)
    OF ?loa:Location
      IF loa:Location OR ?loa:Location{Prop:Req}
        loc:Location = loa:Location
        !Save Lookup Field Incase Of error
        look:loa:Location        = loa:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            loa:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            loa:Location = look:loa:Location
            SELECT(?loa:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?buttonLookupLocation
      ThisWindow.Update
      loc:Location = loa:Location
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          loa:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?loa:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?loa:Location)
    OF ?loa:Shelf_Location
      IF loa:Shelf_Location OR ?loa:Shelf_Location{Prop:Req}
        los:Shelf_Location = loa:Shelf_Location
        los:Site_Location = loa:Location
        GLO:Select1 = loa:Location
        GLO:Select2 = loa:Location
        !Save Lookup Field Incase Of error
        look:loa:Shelf_Location        = loa:Shelf_Location
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            loa:Shelf_Location = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            CLEAR(GLO:Select1)
            CLEAR(GLO:Select2)
            !Restore Lookup On Error
            loa:Shelf_Location = look:loa:Shelf_Location
            SELECT(?loa:Shelf_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update
      los:Shelf_Location = loa:Shelf_Location
      los:Site_Location = loa:Location
      GLO:Select1 = loa:Location
      GLO:Select2 = loa:LOcation
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          loa:Shelf_Location = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?loa:Shelf_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?loa:Shelf_Location)
    OF ?loa:Stock_Type
      IF loa:Stock_Type OR ?loa:Stock_Type{Prop:Req}
        stp:Stock_Type = loa:Stock_Type
        GLO:Select1 = 'LOA'
        stp:Use_Loan = 'YES'
        !Save Lookup Field Incase Of error
        look:loa:Stock_Type        = loa:Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            loa:Stock_Type = stp:Stock_Type
          ELSE
            CLEAR(GLO:Select1)
            CLEAR(stp:Use_Loan)
            !Restore Lookup On Error
            loa:Stock_Type = look:loa:Stock_Type
            SELECT(?loa:Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update
      stp:Stock_Type = loa:Stock_Type
      GLO:Select1 = 'LOA'
      stp:Use_Loan = 'YES'
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          loa:Stock_Type = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?loa:Stock_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?loa:Stock_Type)
    OF ?Button5
      ThisWindow.Update
      Browse_Loan_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
      glo:select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button5, Accepted)
    OF ?print_Label
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print_Label, Accepted)
      If thiswindow.request = Insertrecord
          Case Missive('Cannot print a label until the record has been saved.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else!If thiswindow.request = Insertrecord
          glo:select1 = loa:ref_number
          set(defaults)
          access:defaults.next()
          case def:label_printer_type
              of 'TEC B-440 / B-442'
                  loan_unit_label
              of 'TEC B-452'
                  loan_unit_label_b452
          end!case def:label_printer_type
          glo:select1 = ''
      End!If thiswindow.request = Insertrecord
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print_Label, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020286'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020286'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020286'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Validation
  Found# = 0
  Save_LOAN_ALIAS_ID = Access:LOAN_ALIAS.SaveFile()
  Access:LOAN_ALIAS.Clearkey(loa_ali:ESN_Only_Key)
  loa_ali:ESN = loa:ESN
  Set(loa_ali:ESN_Only_Key,loa_ali:ESN_Only_Key)
  Loop ! Begin Loop
      If Access:LOAN_ALIAS.Next()
          Break
      End ! If Access:LOAN_ALIAS.Next()
      If loa_ali:ESN <> loa:ESN
          Break
      End ! If loa_ali:ESN <> loa:ESN
      If loa_ali:Ref_Number <> loa:Ref_Number
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected I.M.E.I. Number already exists in Loan Stock.','ServiceBase 3g',|
                         'mstop.jpg','/&OK')
              Of 1 ! &OK Button
          End!Case Message
          Found# = 1
          Break
      End ! If loa_ali:Ref_Number <> loa:Ref_Number
  End ! Loop
  Access:LOAN_ALIAS.RestoreFile(Save_LOAN_ALIAS_ID)
  If Found# = 1
      Cycle
  End ! If Found# = 1
  
  Access:JOBS.ClearKey(job:ESN_Key)
  job:ESN = loa:ESN
  If Access:JOBS.TryFetch(job:ESN_Key) = Level:Benign
      !Found
      If job:Date_Completed = ''
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected I.M.E.I. Number is already attached to job number ' & Clip(job:Ref_Number) & '.','ServiceBase 3g',|
                         'mstop.jpg','/&OK')
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If job:Date_Completed = ''
  Else ! If Access:JOBS.TryFetch(job:ESN_Key) = Level:Benign
      !Error
  End ! If Access:JOBS.TryFetch(job:ESN_Key) = Level:Benign
  
  ! Inserting (DBH 21/07/2008) # 10130 - Allow to add exchange unit, only if not available
  Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
  xch:ESN = loa:ESN
  If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !Found
      If xch:Available <> 'NOA'
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('The selected I.M.E.I. Number exists in Exchange Stock but it is still "Available"','ServiceBase 3g',|
                         'mstop.jpg','/&OK')
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If xch:Available <> 'NOA'
  
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Missive('The selected I.M.E.I. Number already exists in Exchange Stock but is "Not Available"'&|
          '|'&|
          '|Are you sure you want to continue and add this unit to Loan Stock?','ServiceBase 3g',|
                     'mquest.jpg','\&No|/&Yes')
          Of 2 ! &Yes Button
          Of 1 ! &No Button
              Cycle
      End!Case Message
  Else ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
      !Error
  End ! If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
  ! End (DBH 21/07/2008) #10130
  
  
  Access:MODELNUM.ClearKey(mod:Model_Number_Key)
  mod:Model_Number = loa:Model_Number
  If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !Found
      If Len(Clip(loa:esn)) < mod:esn_length_from Or Len(Clip(loa:esn)) > mod:esn_length_to
          Case Missive('The I.M.E.I. Number entered should be between ' & Clip(mod:ESN_Length_From) & ' and ' & Clip(mod:ESN_Length_To) & ' characters in length.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
      If man:use_msn = 'YES'
          If len(Clip(loa:msn)) < mod:msn_length_from Or len(Clip(loa:msn)) > mod:msn_length_to
              Case Missive('The M.S.N. entered should be between ' & Clip(mod:MSN_Length_From) & ' and ' & Clip(mod:MSN_Length_To) & ' characters in length.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Cycle
          End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
      End!If man:use_msn = 'YES'
  Else ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !Error
  End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
  
  
  
  ReturnValue = PARENT.TakeCompleted()
  If thiswindow.request = Insertrecord
      Set(defaults)
      access:defaults.next()
      If def:use_loan_exchange_label = 'YES'
          Print_label_temp = 'YES'
      Else
          Print_label_temp = 'NO'
      End!If def:use_loan_exchange_label = 'YES'
  
      !Add initial entry
      If Access:LOANHIST.PrimeRecord() = Level:Benign
          loh:Ref_Number    = loa:Ref_Number
          loh:Date          = Today()
          loh:Time          = Clock()
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
  
          Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
          End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          loh:User          = use:User_Code
          loh:Status        = 'INITIAL ENTRY'
          If Access:LOANHIST.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:LOANHIST.TryInsert() = Level:Benign
              !Insert Failed
              Access:LOANHIST.CancelAutoInc()
          End !If Access:LOANHIST.TryInsert() = Level:Benign
      End !If Access:LOANHIST.PrimeRecord() = Level:Benign
  Else
      print_label_temp = 'NO'
  End!If thiswindow.request = Insertrecord
  
  If print_label_temp = 'YES' And thiswindow.request = insertrecord
      glo:select1 = loa:ref_number
      set(defaults)
      access:defaults.next()
      case def:label_printer_type
          of 'TEC B-440 / B-442'
              loan_Unit_label
          of 'TEC B-452'
              loan_unit_label_b452
      end!case def:label_printer_type
      glo:select1 = ''
  End!If print_label_temp = 'YES' And thiswindow.request = insertrecord
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?lac:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lac:Accessory, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lac:Accessory, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do msn_check
      If loa:job_number <> ''
          Unhide(?loa:job_number)
          Unhide(?loa:job_number:prompt)
      Else!If xch:job_number <> ''
          Hide(?loa:job_number)
          Hide(?loa:job_number:prompt)
      End!If xch:job_number <> ''
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

UpdateSUBTRACC PROCEDURE                              !Generated from procedure template - Window

WinPrint             CLASS(CWin32File)
                     END
WinName              STRING(255),STATIC
CurrentTab           STRING(80)
save_sub_ali_id      USHORT,AUTO
Discount_Rate_Labour_Temp REAL
Discount_Rate_Parts_Temp REAL
VAT_Rate_Labour_Temp REAL
VAT_Rate_Parts_Temp  REAL
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
Discount_Rate_Retail_Temp REAL
VAT_Rate_Retail_Temp REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:YES              STRING('YES')
tmp:TradeState       LONG
tmp:SaveState        LONG
tmp:SaveState2       LONG
SaveDataGroup        GROUP,PRE(sav)
HeadRecordNumber     LONG
RecordNumber         LONG
MainAccountNumber    STRING(30)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Branch               STRING(30)
Postcode             STRING(30)
AddressLine1         STRING(30)
AddressLine2         STRING(30)
AddressLine3         STRING(30)
TelephoneNumber      STRING(30)
FaxNumber            STRING(30)
EmailAddress         STRING(255)
ContactName          STRING(30)
OutgoingCourier      STRING(30)
                     END
AccountQueue         QUEUE,PRE(accque)
RecordNumber         LONG
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?sub:Labour_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?sub:Parts_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:7 QUEUE                           !Queue declaration for browse/combo box using ?sub:Retail_VAT_Code
vat:VAT_Code           LIKE(vat:VAT_Code)             !List box control field - type derived from field
vat:VAT_Rate           LIKE(vat:VAT_Rate)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sub:DespatchedJobStatus
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?sub:InvoicedJobStatus
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB14::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB15::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB26::View:FileDropCombo VIEW(VATCODE)
                       PROJECT(vat:VAT_Code)
                       PROJECT(vat:VAT_Rate)
                     END
FDCB24::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB27::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
BRW8::View:Browse    VIEW(SUBACCAD)
                       PROJECT(sua:AccountNumber)
                       PROJECT(sua:CompanyName)
                       PROJECT(sua:Postcode)
                       PROJECT(sua:RecordNumber)
                       PROJECT(sua:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sua:AccountNumber      LIKE(sua:AccountNumber)        !List box control field - type derived from field
sua:CompanyName        LIKE(sua:CompanyName)          !List box control field - type derived from field
sua:Postcode           LIKE(sua:Postcode)             !List box control field - type derived from field
sua:RecordNumber       LIKE(sua:RecordNumber)         !Primary key field - type derived from field
sua:RefNumber          LIKE(sua:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::sub:Record  LIKE(sub:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Window Name'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Sub Account'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,256,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Sub Account &Details'),USE(?Tab1)
                           PROMPT('Main Account Number'),AT(68,59),USE(?SUB:Main_Account_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(156,59,67,10),USE(sub:Main_Account_Number),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,READONLY
                           PROMPT('Account Number'),AT(68,78),USE(?SUB:Account_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(156,78,67,10),USE(sub:Account_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('VCP Dealer ID:'),AT(228,68),USE(?sub:DealerID:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s30),AT(228,78,88,10),USE(sub:DealerID),SKIP,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Dealer ID'),TIP('Dealer ID'),UPR,READONLY
                           BUTTON,AT(256,318),USE(?ReplicateFrom),TRN,FLAT,HIDE,LEFT,ICON('repfromp.jpg')
                           PROMPT('Company Name'),AT(68,96),USE(?SUB:Company_Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s40),AT(156,96,136,10),USE(sub:Company_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Branch'),AT(68,110),USE(?SUB:Branch:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,110,136,10),USE(sub:Branch),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(68,170),USE(?SUB:Postcode:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(156,170,67,10),USE(sub:Postcode),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Hub'),AT(68,184),USE(?sub:Hub:Prompt),LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(156,184,67,10),USE(sub:Hub),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Hub'),TIP('Hub'),UPR,READONLY
                           PROMPT('Address'),AT(68,126),USE(?SUB:Address_Line1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,126,136,10),USE(sub:Address_Line1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(156,138,136,10),USE(sub:Address_Line2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Suburb'),AT(68,156),USE(?SUB:Address_Line1:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,156,124,10),USE(sub:Address_Line3),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(284,152),USE(?LookupSuburb),TRN,FLAT,ICON('lookupp.jpg')
                           STRING('Telephone Number'),AT(68,198),USE(?String1),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           STRING('Fax Number'),AT(212,200),USE(?String3),TRN,FONT(,8,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(156,198,52,11),USE(sub:Telephone_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s15),AT(260,198,52,11),USE(sub:Fax_Number),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Email Address'),AT(68,212),USE(?sub:EmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(156,212,136,10),USE(sub:EmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Contact Name'),AT(68,228),USE(?SUB:Contact_Name:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,228,136,10),USE(sub:Contact_Name),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           CHECK('Override Head Account V.A.T. No'),AT(156,246),USE(sub:OverrideHeadVATNo),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Override Head Account V.A.T. No'),TIP('Override Head Account V.A.T. No'),VALUE('1','0')
                           PROMPT('V.A.T. Number'),AT(68,259),USE(?sub:VAT_Number:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,259,136,10),USE(sub:VAT_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Courier - Incoming'),AT(68,275),USE(?sub:Courier_Incoming:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,275,124,10),USE(sub:Courier_Incoming),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(DownKey),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),UPR
                           BUTTON,AT(280,271),USE(?LookupIncomingCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Courier - Outgoing'),AT(68,294),USE(?sub:Courier_Outgoing:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(156,294,124,10),USE(sub:Courier_Outgoing),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(280,291),USE(?LookupOutgoingCourier),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           STRING('Generic Account'),AT(68,326),USE(?String4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK,AT(148,326),USE(sub:Generic_Account),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                         END
                       END
                       PANEL,AT(324,54,292,12),USE(?Panel3),FILL(09A6A7CH)
                       PROMPT('Replicated From'),AT(456,56,156,8),USE(?ReplicatedFrom),RIGHT,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       SHEET,AT(324,68,292,296),USE(?Sheet2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('General'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Use Address As Delivery Address'),AT(328,84),USE(sub:Use_Delivery_Address),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Use Address As Collection Address'),AT(328,97),USE(sub:Use_Collection_Address),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Record End User Address'),AT(328,108),USE(sub:Use_Customer_Address),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Do not use the Trade Account''s Address<13,10>as the Job''s "Customer Address".'),VALUE('YES','NO')
                           CHECK('Invoice End User Address'),AT(328,121),USE(sub:Invoice_Customer_Address),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Do not use the Trade Account''s Address as <13,10>the Invoice Address. Use the Job''s "' &|
   'Customer Address".'),VALUE('YES','NO')
                           CHECK('Produce Multiple Invoices'),AT(328,161),USE(sub:MultiInvoice),HIDE,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Multiple Invoice Account?'),TIP('Multiple Invoice Account?'),VALUE('YES','NO')
                           CHECK('Auto Send Status Emails'),AT(328,180),USE(sub:AutoSendStatusEmails),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Auto Send Status Emails'),TIP('Auto Send Status Emails'),VALUE('1','0')
                         END
                         TAB('Service'),USE(?ServiceDefaults),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SHEET,AT(328,86,284,226),USE(?Sheet3),LEFT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD,DOWN
                             TAB('General'),USE(?Tab8),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               OPTION('Invoice Type'),AT(348,91,136,65),USE(sub:MultiInvoice,,?SUB:MultiInvoice:2),BOXED,TRN,HIDE,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Type (SIN - Single / SIM - Single (Summary) / MUL - Multiple / BAT - Batch)')
                                 RADIO('Single Invoice (No Summary)'),AT(356,102),USE(?SUB:MultiIn:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('SIN')
                                 RADIO('Single Invoice (Summary)'),AT(356,115),USE(?SUB:MultiIn:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('SIM')
                                 RADIO('Multiple Invoice'),AT(356,129),USE(?SUB:MultiIn:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('MUL')
                                 RADIO('Batch Invoice'),AT(356,139),USE(?SUB:MultiIn:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('BAT')
                               END
                               CHECK('Exclude From Bouncer Table'),AT(488,94),USE(sub:ExcludeBouncer),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Exclude From Bouncer'),TIP('Exclude From Bouncer'),VALUE('1','0')
                               CHECK('Exclude From TAT Report'),AT(488,107),USE(sub:ExcludeFromTATReport),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Exclude From TAT Report'),TIP('Exclude From TAT Report'),VALUE('1','0')
                               CHECK('Refurbishment Account'),AT(488,121),USE(sub:RefurbishmentAccount),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Refurbishment Account'),TIP('Refurbishment Account'),VALUE('1','0')
                               CHECK('First Copy Un-priced'),AT(448,158),USE(sub:Price_First_Copy_Only),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Display pricing details on first copy of despatch note')
                               PROMPT('No. Of Despatch Note Copies'),AT(348,174,,10),USE(?sub:Num_Despatch_Note_Copies:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@s8),AT(464,174,64,10),USE(sub:Num_Despatch_Note_Copies),HIDE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Number of despatch notes to print'),TIP('Number of despatch notes to print'),UPR
                               CHECK('Price Despatch Notes'),AT(348,158),USE(sub:PriceDespatchNotes),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Price Despatch Notes'),TIP('Price Despatch Notes'),VALUE('1','0')
                               CHECK('Charge In Euros'),AT(348,187),USE(sub:EuroApplies),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0'),MSG('Apply Euro')
                               CHECK('Zero Value Chargeable Parts'),AT(348,201),USE(sub:ZeroChargeable),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Don''t show Chargeable Costs'),TIP('Don''t show Chargeable Costs'),VALUE('YES','NO')
                               CHECK('Stop Account'),AT(348,211),USE(sub:Stop_Account),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                               CHECK('Override Head Account Mandatory Mobile Check'),AT(348,225),USE(sub:OverrideHeadMobile),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Override Head Account Mandatory Mobile Check'),TIP('Override Head Account Mandatory Mobile Check'),VALUE('1','0')
                               CHECK('Override Mandatory Mobile Number'),AT(364,234),USE(sub:OverrideMobileDefault),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Override Mandatory Mobile Number'),TIP('Override Mandatory Mobile Number'),VALUE('1','0')
                               CHECK('Allow End User Name'),AT(348,246),USE(sub:Force_Customer_Name),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                               CHECK('Force Order Number'),AT(348,257),USE(sub:ForceOrderNumber),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force Order Number'),TIP('Force Order Number'),VALUE('1','0')
                               GROUP('Allow Address Manipulation'),AT(348,265,124,44),USE(?AddressManipulationGroup),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                                 CHECK('Invoice Address'),AT(356,274),USE(sub:ChangeInvAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Address'),TIP('Invoice Address'),VALUE('1','0')
                                 CHECK('Collection Address'),AT(356,283),USE(sub:ChangeCollAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Collection Address'),TIP('Collection Address'),VALUE('1','0')
                                 CHECK('Delivery Address'),AT(356,294),USE(sub:ChangeDelAddress),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Delivery Address'),TIP('Delivery Address'),VALUE('1','0')
                               END
                             END
                             TAB('Financial'),USE(?Tab9),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                               PROMPT('V.A.T. Code Labour'),AT(348,90),USE(?Prompt14),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               COMBO(@s2),AT(428,90,64,10),USE(sub:Labour_VAT_Code),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('16L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10),FROM(Queue:FileDropCombo:1)
                               ENTRY(@n6.2b),AT(496,90,64,10),USE(VAT_Rate_Labour_Temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                               PROMPT('V.A.T. Code Parts'),AT(348,102),USE(?Prompt17:4),TRN,FONT(,8,COLOR:White,FONT:bold)
                               COMBO(@s2),AT(428,102,64,10),USE(sub:Parts_VAT_Code),IMM,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('13L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10),FROM(Queue:FileDropCombo:5)
                               ENTRY(@n6.2b),AT(496,102,64,10),USE(VAT_Rate_Parts_Temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                               PROMPT('V.A.T. Code Retail'),AT(348,115),USE(?Prompt17:5),TRN,FONT(,8,COLOR:White,FONT:bold)
                               COMBO(@s2),AT(428,115,64,10),USE(sub:Retail_VAT_Code),IMM,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('14L(2)|M@s2@24L(2)|M@n6.2@'),DROP(10),FROM(Queue:FileDropCombo:7)
                               ENTRY(@n6.2b),AT(496,115,64,10),USE(VAT_Rate_Retail_Temp),SKIP,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                               PROMPT('Default Courier Cost'),AT(348,131),USE(?SUB:Courier_Cost:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                               ENTRY(@n14.2),AT(428,131,64,10),USE(sub:Courier_Cost),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                               CHECK('Force End User Name'),AT(348,147),USE(sub:ForceEndUserName),DISABLE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force End User Name'),TIP('Force End User Name'),VALUE('1','0')
                               CHECK('Force Estimate'),AT(348,166),USE(sub:ForceEstimate),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Force Estimate'),TIP('Force Estimate'),VALUE('1','0')
                               PROMPT('Estimate If Over'),AT(448,158),USE(?sub:EstimateIfOver:Prompt),TRN,FONT(,7,COLOR:White,FONT:bold)
                               ENTRY(@n14.2),AT(448,166,64,10),USE(sub:EstimateIfOver),RIGHT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Estimate If Over'),TIP('Estimate If Over'),UPR
                               OPTION('Invoice Print Type'),AT(484,179,116,20),USE(sub:InvoiceType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Type')
                                 RADIO('Manual'),AT(492,187),USE(?sub:InvoiceType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                                 RADIO('Automatic'),AT(544,187),USE(?sub:InvoiceType:Radio4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                               END
                               OPTION('Invoice Print Type'),AT(484,198,116,20),USE(sub:InvoiceTypeComplete),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice Type')
                                 RADIO('Manual'),AT(492,206),USE(?sub:InvoiceType:Radio3:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                                 RADIO('Automatic'),AT(544,206),USE(?sub:InvoiceType:Radio4:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                               END
                               CHECK('Print Invoice At Despatch'),AT(348,187),USE(sub:InvoiceAtDespatch),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Print Invoice At Despatch'),TIP('Print Invoice At Despatch'),VALUE('1','0')
                               CHECK('Print Invoice At Completion'),AT(348,206),USE(sub:InvoiceAtCompletion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Invoice At Completion'),TIP('Invoice At Completion'),VALUE('1','0')
                             END
                           END
                         END
                         TAB('Retail'),USE(?RetailDefaults),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION('Retail Sales Payment Type'),AT(328,84,144,28),USE(sub:Retail_Payment_Type),BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Account'),AT(336,97),USE(?SUB:Retail_Payment_Type:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('ACC')
                             RADIO('Cash'),AT(416,97),USE(?SUB:Retail_Payment_Type:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('CAS')
                           END
                           OPTION('Retail Sales Price Structure'),AT(328,116,144,28),USE(sub:Retail_Price_Structure),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Trade'),AT(336,129),USE(?sub:Retail_Price_Structure:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('TRA')
                             RADIO('Purchase'),AT(416,128),USE(?sub:Retail_Price_Structure:Radio3),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('PUR')
                           END
                           CHECK('Print Retail Picking Note'),AT(328,148),USE(sub:Print_Retail_Picking_Note),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Print Retail Despatch Note'),AT(328,161),USE(sub:Print_Retail_Despatch_Note),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           PROMPT('Line 500 Account Number'),AT(328,176),USE(?sub:Line500AccountNumber:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(436,176,124,10),USE(sub:Line500AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Line 500 Account Number'),TIP('Line 500 Account Number'),UPR
                         END
                         TAB('Despatch'),USE(?Tab6),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Use End User For Despatch (Disable Multiple Despatch)'),AT(328,84),USE(sub:UseCustDespAdd),RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Customer Address As Despatch Address'),TIP('Do not use the Trade Account''s Address as the<13,10>Despatch Address. Use the Job''s "' &|
   'Customer Address".'),VALUE('YES','NO')
                           CHECK('Print Service Despatch Notes'),AT(328,97),USE(sub:Print_Despatch_Notes),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Print Despatch Note On Completion'),AT(336,108),USE(sub:Print_Despatch_Complete),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO'),MSG('Print Despatch Note At Completion')
                           CHECK('Print Despatch Note On Despatch'),AT(336,116),USE(sub:Print_Despatch_Despatch),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO'),MSG('Print Despatch Note At Despatch')
                           CHECK('Hide Address On Despatch Note'),AT(336,124),USE(sub:HideDespAdd),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Hide Address On Despatch Note'),TIP('Hide Address On Despatch Note'),VALUE('1','0')
                           GROUP('Despatch Note Type'),AT(328,136,176,49),USE(?despatch_note_type),DISABLE,BOXED,TRN,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Despatch Note Per Item'),AT(336,147),USE(sub:Despatch_Note_Per_Item,,?SUB:Despatch_Note_Per_Item:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                             CHECK('Multiple Despatch Note Summary'),AT(336,160),USE(sub:Summary_Despatch_Notes),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                             CHECK('Print Summary Note For Individual Desp'),AT(336,171),USE(sub:IndividualSummary),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Print Individual Summary Report At Despatch'),TIP('Print Individual Summary Report At Despatch'),VALUE('1','0')
                           END
                           CHECK('Desp. Paid Jobs Only'),AT(328,188),USE(sub:Despatch_Paid_Jobs),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Set Compl. Status'),AT(436,188),USE(sub:SetDespatchJobStatus),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Completed Status'),TIP('Set Completed Status'),VALUE('1','0')
                           COMBO(@s30),AT(520,188,92,10),USE(sub:DespatchedJobStatus),IMM,HIDE,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo),MSG('Completed Status')
                           CHECK('Desp. Invoiced Jobs Only'),AT(328,202),USE(sub:Despatch_Invoiced_Jobs),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Set Compl. Status'),AT(436,202),USE(sub:SetInvoicedJobStatus),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Set Completed Status'),TIP('Set Completed Status'),VALUE('1','0')
                           COMBO(@s30),AT(520,202,92,10),USE(sub:InvoicedJobStatus),IMM,HIDE,VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2),MSG('Completed Status')
                           CHECK('Use Trade Contact No on Despatch Notes'),AT(328,216),USE(sub:UseTradeContactNo),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Trade Contact Name'),TIP('Use Trade Contact Name'),VALUE('1','0')
                           CHECK('Use Alternative Contact Nos On Despatch Note'),AT(328,230),USE(sub:UseDespatchDetails),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Alternative Contact Nos On Despatch Note#'),TIP('Use Alternative Contact Nos On Despatch Note#'),VALUE('1','0')
                           GROUP('Alternative Contact Numbers'),AT(328,240,252,54),USE(?AlternativeNumbers),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             PROMPT('Telephone Number'),AT(332,253),USE(?sub:AltTelephoneNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(412,253,124,10),USE(sub:AltTelephoneNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Telephone Number'),TIP('Telephone Number'),UPR
                             PROMPT('Fax Number'),AT(332,266),USE(?sub:AltFaxNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s30),AT(412,266,124,10),USE(sub:AltFaxNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fax Number'),TIP('Fax Number'),UPR
                             PROMPT('Email Address'),AT(332,280),USE(?sub:AltEmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s255),AT(412,280,164,10),USE(sub:AltEmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                           END
                           CHECK('Use Alternative Delivery Addresses'),AT(328,298),USE(sub:UseAlternativeAdd),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Use Alternative Delivery Addresses'),TIP('Use Alternative Delivery Addresses'),VALUE('1','0')
                         END
                         TAB('Delivery Addresses'),USE(?Tab7),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(456,86),USE(?sua:AccountNumber:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s15),AT(328,86,124,10),USE(sua:AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           LIST,AT(328,100,284,160),USE(?List),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@60L(2)|M~Postcode~@s15@'),FROM(Queue:Browse)
                           BUTTON,AT(412,264),USE(?Insert),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(480,264),USE(?Change),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(548,264),USE(?Delete),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                         TAB('VCP Defaults'),USE(?Tab10),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(328,206,212,38),USE(?groupVCPFee)
                             CHECK('Print OOW VCP Fee'),AT(415,206),USE(sub:PrintOOWVCPFee),TRN,FONT(,,,FONT:bold),MSG('Print OOW VCP Fee'),TIP('Print OOW VCP Fee'),VALUE('1','0')
                             PROMPT('OOW VCP Fee Label'),AT(328,219),USE(?sub:OOWVCPFeeLabel:Prompt),TRN,FONT(,,,FONT:bold)
                             ENTRY(@s30),AT(416,219,124,10),USE(sub:OOWVCPFeeLabel),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('OOW VCP Fee Label'),TIP('OOW VCP Fee Label'),UPR
                             PROMPT('OOW VCP Fee Amount'),AT(328,233),USE(?sub:OOWVCPFeeAmount:Prompt),TRN,FONT(,,,FONT:bold)
                             ENTRY(@n14.2),AT(416,233,64,10),USE(sub:OOWVCPFeeAmount),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('OOW VCP Fee Amount'),TIP('OOW VCP Fee Amount'),UPR
                           END
                           PROMPT('Password'),AT(328,84),USE(?SUB:Password:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s20),AT(416,84,124,10),USE(sub:Password),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,PASSWORD
                           CHECK('Allow VCP Job Booking'),AT(416,104),USE(sub:SIDJobBooking),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('SID Job Booking'),TIP('SID Job Booking'),VALUE('1','0')
                           CHECK('Allow VCP Job Enquiry'),AT(416,118),USE(sub:SIDJobEnquiry),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('SID Job Enquiry'),TIP('SID Job Enquiry'),VALUE('1','0')
                           CHECK('Allow VCP Loan Units'),AT(416,132),USE(sub:AllowVCPLoanUnits),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Allow VCP Loan Units'),TIP('Allow VCP Loan Units'),VALUE('1','0')
                           CHECK('Allow to change Siebel Customer Info'),AT(416,146),USE(sub:AllowChangeSiebelInfo),FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('1','0')
                           PROMPT('Region'),AT(328,168),USE(?sub:Region:Prompt),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(416,168,124,10),USE(sub:Region),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Region'),TIP('Region'),UPR
                           BUTTON,AT(544,164),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('VCP Waybill Prefix'),AT(328,188),USE(?sub:VCPWaybillPrefix:Prompt),TRN,FONT(,,,FONT:bold)
                           ENTRY(@s30),AT(416,188,124,10),USE(sub:VCPWaybillPrefix),FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White),MSG('VCP Waybill Prefix'),TIP('VCP Waybill Prefix'),UPR
                           CHECK('Print Warranty VCP Fee'),AT(416,250),USE(sub:PrintWarrantyVCPFee),TRN,FONT(,,,FONT:bold),MSG('Print Warranty VCP Fee'),TIP('Print Warranty VCP Fee'),VALUE('1','0')
                           PROMPT('Warranty VCP Fee Amount'),AT(328,262,84,20),USE(?sub:WarrantyVCPFeeAmount:Prompt),TRN,FONT(,,,FONT:bold)
                           ENTRY(@n14.2),AT(416,262,64,10),USE(sub:WarrantyVCPFeeAmount),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),MSG('Warranty VCP Fee Amount'),TIP('Warranty VCP Fee Amount'),UPR
                         END
                       END
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB14               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB15               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB26               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:7         !Reference to browse queue type
                     END

FDCB24               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB27               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  EntryLocatorClass                !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:sub:Courier_Incoming                Like(sub:Courier_Incoming)
look:sub:Courier_Outgoing                Like(sub:Courier_Outgoing)
look:sub:Region                Like(sub:Region)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
CheckOverrideVatNumber      Routine
    !Enable/disable the VAT number depending on the tick box - TrkBs: 5364 (DBH: 17-03-2005)
    Case sub:OverrideHeadVatNo
        Of 1
            ?sub:Vat_Number{prop:ReadOnly} = False
            ?sub:Vat_Number{prop:Skip} = False
        Of 0
            ?sub:Vat_Number{prop:ReadOnly} = True
            ?sub:Vat_Number{prop:Skip} = True
            !Fill in the Head Vat Number if not used - TrkBs: 5364 (DBH: 17-03-2005)
            sub:Vat_Number = tra:Vat_Number
            Display()
    End ! sub:OverrideHeadVatNo
    !Bryan.CompFieldColour()
Fill_Rates      Routine

    !Check Main Account!
    If glo:Select2 = 'FINANCIAL'
        ?sub:Labour_VAT_Code{PROP:ReQ} = TRUE
        ?sub:Retail_VAT_Code{PROP:ReQ} = TRUE
        ?sub:Parts_VAT_Code{PROP:ReQ} = TRUE
    END
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = sub:labour_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_rate_labour_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = sub:parts_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_rate_parts_temp  = vat:vat_rate
    end
    access:vatcode.clearkey(vat:vat_code_key)
    vat:vat_code = sub:retail_vat_code
    if access:vatcode.fetch(vat:vat_code_key) = Level:Benign
       vat_rate_retail_temp  = vat:vat_rate
    end

    Display()
ForceDealerID       Routine
    if (sub:SIDJobBooking = 1 Or sub:SIDJobEnquiry = 1 Or sub:AllowVCPLoanUnits = 1)
        ! #11815 Only force if it is a VCP account (Bryan: 29/11/2010)
        if (?sub:DealerID{prop:Req} = 0 and ?sub:DealerID{prop:ReadOnly} <> 1)
            ?sub:DealerID{prop:Req} = 1
            Bryan.CompFieldColour()
        end
    Else
        if (?sub:DealerID{prop:Req} = 1)
            ?sub:DealerID{prop:Req} = 0
            Bryan.CompFieldColour()
        end

    end ! if (sub:SIDJobBooking = 1 Or sub:SIDJobEnquiry = 1 Or sub:AllowVCPLoanUnits = 1)
ReplicatedFrom      Routine
    If sub:ReplicateAccount <> ''
        ?ReplicatedFrom{prop:Text} = 'Account Replicated From: ' & Clip(sub:ReplicateAccount)
        ?ReplicatedFrom{prop:Hide} = 0
    Else !If tra:ReplicateAccount <> ''
        ?ReplicatedFrom{prop:Hide} = 1
    End !If tra:ReplicateAccount <> ''
    Display()
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020305'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSUBTRACC')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sub:Record,History::sub:Record)
  SELF.AddHistoryField(?sub:Main_Account_Number,2)
  SELF.AddHistoryField(?sub:Account_Number,3)
  SELF.AddHistoryField(?sub:DealerID,126)
  SELF.AddHistoryField(?sub:Company_Name,5)
  SELF.AddHistoryField(?sub:Branch,12)
  SELF.AddHistoryField(?sub:Postcode,4)
  SELF.AddHistoryField(?sub:Hub,118)
  SELF.AddHistoryField(?sub:Address_Line1,6)
  SELF.AddHistoryField(?sub:Address_Line2,7)
  SELF.AddHistoryField(?sub:Address_Line3,8)
  SELF.AddHistoryField(?sub:Telephone_Number,9)
  SELF.AddHistoryField(?sub:Fax_Number,10)
  SELF.AddHistoryField(?sub:EmailAddress,11)
  SELF.AddHistoryField(?sub:Contact_Name,13)
  SELF.AddHistoryField(?sub:OverrideHeadVATNo,110)
  SELF.AddHistoryField(?sub:VAT_Number,31)
  SELF.AddHistoryField(?sub:Courier_Incoming,29)
  SELF.AddHistoryField(?sub:Courier_Outgoing,30)
  SELF.AddHistoryField(?sub:Generic_Account,93)
  SELF.AddHistoryField(?sub:Use_Delivery_Address,26)
  SELF.AddHistoryField(?sub:Use_Collection_Address,27)
  SELF.AddHistoryField(?sub:Use_Customer_Address,48)
  SELF.AddHistoryField(?sub:Invoice_Customer_Address,49)
  SELF.AddHistoryField(?sub:MultiInvoice,51)
  SELF.AddHistoryField(?sub:AutoSendStatusEmails,74)
  SELF.AddHistoryField(?SUB:MultiInvoice:2,51)
  SELF.AddHistoryField(?sub:ExcludeBouncer,104)
  SELF.AddHistoryField(?sub:ExcludeFromTATReport,109)
  SELF.AddHistoryField(?sub:RefurbishmentAccount,119)
  SELF.AddHistoryField(?sub:Price_First_Copy_Only,36)
  SELF.AddHistoryField(?sub:Num_Despatch_Note_Copies,37)
  SELF.AddHistoryField(?sub:PriceDespatchNotes,35)
  SELF.AddHistoryField(?sub:EuroApplies,57)
  SELF.AddHistoryField(?sub:ZeroChargeable,50)
  SELF.AddHistoryField(?sub:Stop_Account,24)
  SELF.AddHistoryField(?sub:OverrideHeadMobile,112)
  SELF.AddHistoryField(?sub:OverrideMobileDefault,113)
  SELF.AddHistoryField(?sub:Force_Customer_Name,95)
  SELF.AddHistoryField(?sub:ForceOrderNumber,59)
  SELF.AddHistoryField(?sub:ChangeInvAddress,78)
  SELF.AddHistoryField(?sub:ChangeCollAddress,79)
  SELF.AddHistoryField(?sub:ChangeDelAddress,80)
  SELF.AddHistoryField(?sub:Labour_VAT_Code,18)
  SELF.AddHistoryField(?sub:Parts_VAT_Code,20)
  SELF.AddHistoryField(?sub:Retail_VAT_Code,19)
  SELF.AddHistoryField(?sub:Courier_Cost,44)
  SELF.AddHistoryField(?sub:ForceEndUserName,77)
  SELF.AddHistoryField(?sub:ForceEstimate,89)
  SELF.AddHistoryField(?sub:EstimateIfOver,90)
  SELF.AddHistoryField(?sub:InvoiceType,63)
  SELF.AddHistoryField(?sub:InvoiceTypeComplete,92)
  SELF.AddHistoryField(?sub:InvoiceAtDespatch,62)
  SELF.AddHistoryField(?sub:InvoiceAtCompletion,91)
  SELF.AddHistoryField(?sub:Retail_Payment_Type,46)
  SELF.AddHistoryField(?sub:Retail_Price_Structure,47)
  SELF.AddHistoryField(?sub:Print_Retail_Picking_Note,41)
  SELF.AddHistoryField(?sub:Print_Retail_Despatch_Note,40)
  SELF.AddHistoryField(?sub:Line500AccountNumber,111)
  SELF.AddHistoryField(?sub:UseCustDespAdd,28)
  SELF.AddHistoryField(?sub:Print_Despatch_Notes,34)
  SELF.AddHistoryField(?sub:Print_Despatch_Complete,38)
  SELF.AddHistoryField(?sub:Print_Despatch_Despatch,39)
  SELF.AddHistoryField(?sub:HideDespAdd,56)
  SELF.AddHistoryField(?SUB:Despatch_Note_Per_Item:2,42)
  SELF.AddHistoryField(?sub:Summary_Despatch_Notes,43)
  SELF.AddHistoryField(?sub:IndividualSummary,64)
  SELF.AddHistoryField(?sub:Despatch_Paid_Jobs,33)
  SELF.AddHistoryField(?sub:SetDespatchJobStatus,84)
  SELF.AddHistoryField(?sub:DespatchedJobStatus,86)
  SELF.AddHistoryField(?sub:Despatch_Invoiced_Jobs,32)
  SELF.AddHistoryField(?sub:SetInvoicedJobStatus,83)
  SELF.AddHistoryField(?sub:InvoicedJobStatus,85)
  SELF.AddHistoryField(?sub:UseTradeContactNo,65)
  SELF.AddHistoryField(?sub:UseDespatchDetails,69)
  SELF.AddHistoryField(?sub:AltTelephoneNumber,70)
  SELF.AddHistoryField(?sub:AltFaxNumber,71)
  SELF.AddHistoryField(?sub:AltEmailAddress,72)
  SELF.AddHistoryField(?sub:UseAlternativeAdd,73)
  SELF.AddHistoryField(?sub:PrintOOWVCPFee,121)
  SELF.AddHistoryField(?sub:OOWVCPFeeLabel,122)
  SELF.AddHistoryField(?sub:OOWVCPFeeAmount,123)
  SELF.AddHistoryField(?sub:Password,45)
  SELF.AddHistoryField(?sub:SIDJobBooking,114)
  SELF.AddHistoryField(?sub:SIDJobEnquiry,115)
  SELF.AddHistoryField(?sub:AllowVCPLoanUnits,117)
  SELF.AddHistoryField(?sub:AllowChangeSiebelInfo,127)
  SELF.AddHistoryField(?sub:Region,116)
  SELF.AddHistoryField(?sub:VCPWaybillPrefix,120)
  SELF.AddHistoryField(?sub:PrintWarrantyVCPFee,124)
  SELF.AddHistoryField(?sub:WarrantyVCPFeeAmount,125)
  SELF.AddUpdateFile(Access:SUBTRACC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:REGIONS.Open
  Relate:STATUS.Open
  Relate:STOCKTYP.Open
  Relate:SUBACCAD_ALIAS.Open
  Relate:SUBCHRGE_ALIAS.Open
  Relate:SUBEMAIL_ALIAS.Open
  Relate:SUBTRACC_ALIAS.Open
  Relate:SUBURB.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:USERS.UseFile
  Access:TRAHUBS.UseFile
  Access:TRAHUBAC.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBTRACC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:SUBACCAD,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  If thiswindow.request = InsertRecord
      ?ReplicateFrom{prop:Hide} = 0
  End !thiswindow.request = InsertRecord
  Do ReplicatedFrom
  Do CheckOverrideVatNumber
  !Security Check
  If SecurityCheck('OVERRIDE HEAD ACCOUNT VAT NO')
      ?sub:OverrideHeadVatNo{prop:Disable} = True
  Else ! SecurityCheck('OVERRIDE HEAD ACCOUNT VAT NO')
      ?sub:OverrideHeadVatNo{prop:Disable} = False
  End ! SecurityCheck('OVERRIDE HEAD ACCOUNT VAT NO')
  
  If SecurityCheck('ALLOW VCP LOAN UNITS')
      ?sub:AllowVCPLoanUnits{prop:Disable} = 1
  End ! If SecurityCheck('ALLOW VCP LOAN UNITS')
  
  ! #11815 New Dealer ID Field (Bryan: 29/11/2010)
  If (SecurityCheck('AMEND VCP DEALER ID') = 0)
      ?sub:DealerID{prop:ReadOnly} = 0
      ?sub:DealerID{prop:Skip} = 0
      Do ForceDealerID
  End
  ! Inserting (DBH 04/08/2006) # 6643 - Find suburb? Was the postcode manually entered
  If ThisWindow.Request = ChangeRecord
      ! If edited the record and cannot find the suburb, assume that the postcode was manually entered. (DBH: 04/08/2006)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = sub:Address_Line3
      If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Found
          ?sub:Postcode{prop:Skip} = True
          ?sub:Postcode{prop:ReadOnly} = True
          ?sub:Hub{prop:Skip} = True
          ?sub:Hub{prop:Skip} = True
          ! Inserting (DBH 30/01/2008) # 9613 - Might as well autofill in the information
          sub:Postcode = sur:Postcode
          sub:Hub = sur:Hub
          ! End (DBH 30/01/2008) #9613
      Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Error
          ?sub:Postcode{prop:Skip} = False
          ?sub:Postcode{prop:ReadOnly} = False
          ?sub:Hub{prop:Skip} = False
          ?sub:Hub{prop:ReadOnly} = False
      End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
  End ! If ThisWindow.Request = ChangeRecord
  ! End (DBH 04/08/2006) #6643
  ! Save Window Name
   AddToLog('Window','Open','UpdateSUBTRACC')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?sub:Courier_Incoming{Prop:Tip} AND ~?LookupIncomingCourier{Prop:Tip}
     ?LookupIncomingCourier{Prop:Tip} = 'Select ' & ?sub:Courier_Incoming{Prop:Tip}
  END
  IF ?sub:Courier_Incoming{Prop:Msg} AND ~?LookupIncomingCourier{Prop:Msg}
     ?LookupIncomingCourier{Prop:Msg} = 'Select ' & ?sub:Courier_Incoming{Prop:Msg}
  END
  IF ?sub:Courier_Outgoing{Prop:Tip} AND ~?LookupOutgoingCourier{Prop:Tip}
     ?LookupOutgoingCourier{Prop:Tip} = 'Select ' & ?sub:Courier_Outgoing{Prop:Tip}
  END
  IF ?sub:Courier_Outgoing{Prop:Msg} AND ~?LookupOutgoingCourier{Prop:Msg}
     ?LookupOutgoingCourier{Prop:Msg} = 'Select ' & ?sub:Courier_Outgoing{Prop:Msg}
  END
  IF ?sub:Region{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?sub:Region{Prop:Tip}
  END
  IF ?sub:Region{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?sub:Region{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,sua:AccountNumberKey)
  BRW8.AddRange(sua:RefNumber,sub:RecordNumber)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?sua:AccountNumber,sua:AccountNumber,1,BRW8)
  BRW8.AddField(sua:AccountNumber,BRW8.Q.sua:AccountNumber)
  BRW8.AddField(sua:CompanyName,BRW8.Q.sua:CompanyName)
  BRW8.AddField(sua:Postcode,BRW8.Q.sua:Postcode)
  BRW8.AddField(sua:RecordNumber,BRW8.Q.sua:RecordNumber)
  BRW8.AddField(sua:RefNumber,BRW8.Q.sua:RefNumber)
  IF ?sub:PriceDespatchNotes{Prop:Checked} = True
    UNHIDE(?sub:Price_First_Copy_Only)
  END
  IF ?sub:PriceDespatchNotes{Prop:Checked} = False
    HIDE(?sub:Price_First_Copy_Only)
  END
  IF ?sub:OverrideHeadMobile{Prop:Checked} = True
    UNHIDE(?sub:OverrideMobileDefault)
  END
  IF ?sub:OverrideHeadMobile{Prop:Checked} = False
    HIDE(?sub:OverrideMobileDefault)
  END
  IF ?sub:Force_Customer_Name{Prop:Checked} = True
    ENABLE(?sub:ForceEndUserName)
  END
  IF ?sub:Force_Customer_Name{Prop:Checked} = False
    DISABLE(?sub:ForceEndUserName)
  END
  IF ?sub:ForceEstimate{Prop:Checked} = True
    UNHIDE(?sub:EstimateIfOver:Prompt)
    UNHIDE(?sub:EstimateIfOver)
  END
  IF ?sub:ForceEstimate{Prop:Checked} = False
    HIDE(?sub:EstimateIfOver:Prompt)
    HIDE(?sub:EstimateIfOver)
  END
  IF ?sub:InvoiceAtDespatch{Prop:Checked} = True
    UNHIDE(?sub:InvoiceType)
  END
  IF ?sub:InvoiceAtDespatch{Prop:Checked} = False
    HIDE(?sub:InvoiceType)
  END
  IF ?sub:InvoiceAtCompletion{Prop:Checked} = True
    UNHIDE(?sub:InvoiceTypeComplete)
  END
  IF ?sub:InvoiceAtCompletion{Prop:Checked} = False
    HIDE(?sub:InvoiceTypeComplete)
  END
  IF ?sub:Print_Despatch_Notes{Prop:Checked} = True
    ENABLE(?sub:Print_Despatch_Complete)
    ENABLE(?sub:Print_Despatch_Despatch)
    ENABLE(?sub:HideDespAdd)
    ENABLE(?sub:PriceDespatchNotes)
    ENABLE(?sub:Price_First_Copy_Only)
    ENABLE(?sub:Num_Despatch_Note_Copies)
  END
  IF ?sub:Print_Despatch_Notes{Prop:Checked} = False
    sub:Summary_Despatch_Notes = 'NO'
    sub:Despatch_Note_Per_Item = 'NO'
    sub:Print_Despatch_Complete = 'NO'
    sub:Print_Despatch_Despatch = 'NO'
    DISABLE(?sub:Print_Despatch_Complete)
    DISABLE(?sub:Print_Despatch_Despatch)
    DISABLE(?sub:HideDespAdd)
    DISABLE(?sub:PriceDespatchNotes)
    DISABLE(?sub:Price_First_Copy_Only)
    DISABLE(?sub:Num_Despatch_Note_Copies)
  END
  IF ?sub:Print_Despatch_Despatch{Prop:Checked} = True
    ENABLE(?despatch_note_type)
  END
  IF ?sub:Print_Despatch_Despatch{Prop:Checked} = False
    DISABLE(?despatch_note_type)
  END
  IF ?sub:Summary_Despatch_Notes{Prop:Checked} = True
    UNHIDE(?sub:IndividualSummary)
  END
  IF ?sub:Summary_Despatch_Notes{Prop:Checked} = False
    DISABLE(?sub:IndividualSummary)
  END
  IF ?sub:Despatch_Paid_Jobs{Prop:Checked} = True
    UNHIDE(?sub:SetDespatchJobStatus)
  END
  IF ?sub:Despatch_Paid_Jobs{Prop:Checked} = False
    sub:SetDespatchJobStatus = 0
    HIDE(?sub:SetDespatchJobStatus)
  END
  IF ?sub:SetDespatchJobStatus{Prop:Checked} = True
    UNHIDE(?sub:DespatchedJobStatus)
  END
  IF ?sub:SetDespatchJobStatus{Prop:Checked} = False
    HIDE(?sub:DespatchedJobStatus)
  END
  IF ?sub:Despatch_Invoiced_Jobs{Prop:Checked} = True
    UNHIDE(?sub:SetInvoicedJobStatus)
  END
  IF ?sub:Despatch_Invoiced_Jobs{Prop:Checked} = False
    sub:SetInvoicedJobStatus = 0
    HIDE(?sub:SetInvoicedJobStatus)
  END
  IF ?sub:SetInvoicedJobStatus{Prop:Checked} = True
    UNHIDE(?sub:InvoicedJobStatus)
  END
  IF ?sub:SetInvoicedJobStatus{Prop:Checked} = False
    HIDE(?sub:InvoicedJobStatus)
  END
  IF ?sub:UseDespatchDetails{Prop:Checked} = True
    ENABLE(?AlternativeNumbers)
  END
  IF ?sub:UseDespatchDetails{Prop:Checked} = False
    DISABLE(?AlternativeNumbers)
  END
  IF ?sub:UseAlternativeAdd{Prop:Checked} = True
    UNHIDE(?Tab7)
  END
  IF ?sub:UseAlternativeAdd{Prop:Checked} = False
    HIDE(?Tab7)
  END
  IF ?sub:PrintOOWVCPFee{Prop:Checked} = True
    ENABLE(?sub:OOWVCPFeeLabel)
    ENABLE(?sub:OOWVCPFeeAmount)
  END
  IF ?sub:PrintOOWVCPFee{Prop:Checked} = False
    DISABLE(?sub:OOWVCPFeeLabel)
    DISABLE(?sub:OOWVCPFeeAmount)
  END
  IF ?sub:PrintWarrantyVCPFee{Prop:Checked} = True
    ENABLE(?sub:WarrantyVCPFeeAmount)
  END
  IF ?sub:PrintWarrantyVCPFee{Prop:Checked} = False
    DISABLE(?sub:WarrantyVCPFeeAmount)
  END
  FDCB14.Init(sub:Labour_VAT_Code,?sub:Labour_VAT_Code,Queue:FileDropCombo:1.ViewPosition,FDCB14::View:FileDropCombo,Queue:FileDropCombo:1,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB14.Q &= Queue:FileDropCombo:1
  FDCB14.AddSortOrder(vat:Vat_code_Key)
  FDCB14.AddField(vat:VAT_Code,FDCB14.Q.vat:VAT_Code)
  FDCB14.AddField(vat:VAT_Rate,FDCB14.Q.vat:VAT_Rate)
  FDCB14.AddUpdateField(vat:VAT_Code,sub:Labour_VAT_Code)
  ThisWindow.AddItem(FDCB14.WindowComponent)
  FDCB14.DefaultFill = 0
  FDCB15.Init(sub:Parts_VAT_Code,?sub:Parts_VAT_Code,Queue:FileDropCombo:5.ViewPosition,FDCB15::View:FileDropCombo,Queue:FileDropCombo:5,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB15.Q &= Queue:FileDropCombo:5
  FDCB15.AddSortOrder(vat:Vat_code_Key)
  FDCB15.AddField(vat:VAT_Code,FDCB15.Q.vat:VAT_Code)
  FDCB15.AddField(vat:VAT_Rate,FDCB15.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB15.WindowComponent)
  FDCB15.DefaultFill = 0
  FDCB26.Init(sub:Retail_VAT_Code,?sub:Retail_VAT_Code,Queue:FileDropCombo:7.ViewPosition,FDCB26::View:FileDropCombo,Queue:FileDropCombo:7,Relate:VATCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB26.Q &= Queue:FileDropCombo:7
  FDCB26.AddSortOrder(vat:Vat_code_Key)
  FDCB26.AddField(vat:VAT_Code,FDCB26.Q.vat:VAT_Code)
  FDCB26.AddField(vat:VAT_Rate,FDCB26.Q.vat:VAT_Rate)
  ThisWindow.AddItem(FDCB26.WindowComponent)
  FDCB26.DefaultFill = 0
  FDCB24.Init(sub:DespatchedJobStatus,?sub:DespatchedJobStatus,Queue:FileDropCombo.ViewPosition,FDCB24::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB24.Q &= Queue:FileDropCombo
  FDCB24.AddSortOrder(sts:JobKey)
  FDCB24.AddRange(sts:Job,tmp:YES)
  FDCB24.AddField(sts:Status,FDCB24.Q.sts:Status)
  FDCB24.AddField(sts:Ref_Number,FDCB24.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB24.WindowComponent)
  FDCB24.DefaultFill = 0
  FDCB27.Init(sub:InvoicedJobStatus,?sub:InvoicedJobStatus,Queue:FileDropCombo:2.ViewPosition,FDCB27::View:FileDropCombo,Queue:FileDropCombo:2,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB27.Q &= Queue:FileDropCombo:2
  FDCB27.AddSortOrder(sts:JobKey)
  FDCB27.AddRange(sts:Job,tmp:YES)
  FDCB27.AddField(sts:Status,FDCB27.Q.sts:Status)
  FDCB27.AddField(sts:Ref_Number,FDCB27.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB27.WindowComponent)
  FDCB27.DefaultFill = 0
  BRW8.AskProcedure = 4
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:REGIONS.Close
    Relate:STATUS.Close
    Relate:STOCKTYP.Close
    Relate:SUBACCAD_ALIAS.Close
    Relate:SUBCHRGE_ALIAS.Close
    Relate:SUBEMAIL_ALIAS.Close
    Relate:SUBTRACC_ALIAS.Close
    Relate:SUBURB.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSUBTRACC')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    sub:Stop_Account = 'NO'
    sub:Allow_Cash_Sales = 'NO'
    sub:Main_Account_Number = glo:Select12
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickCouriers
      PickCouriers
      BrowseRegions
      UpdateSubAddresses
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020305'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020305'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020305'&'0')
      ***
    OF ?ReplicateFrom
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateFrom, Accepted)
      Case Missive('Do you wish to replicate this account from the DEFAULT account ' & Clip(GETINI('TRADE','DefaultSubAccount',,CLIP(PATH())&'\SB2KDEF.INI')) & ', or SELECT another Sub Account?','ServiceBase 3g',|
                     'mquest.jpg','\Cancel|Select|Default')
          Of 3 ! Default Button
      
              Access:SUBTRACC_ALIAS.Clearkey(sub_ali:Account_Number_Key)
              sub_ali:Account_Number  = GETINI('TRADE','DefaultSubAccount',,CLIP(PATH())&'\SB2KDEF.INI')
              If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key) = Level:Benign
                  !Found
                  RecordNumber$           = sub:RecordNumber
                  AccountNumber"          = sub:Account_Number
                  !Restore the originating Main Account Number - 4403 (DBH: 19-07-2004)
                  MainAccount"            = sub:Main_Account_Number
                  sub:Record              :=: sub_ali:Record
                  sub:RecordNumber        = RecordNumber$
                  sub:ReplicateAccount    = sub_ali:Account_Number
                  sub:Account_Number       = AccountNumber"
                  sub:Main_Account_Number = MainAccount"
                  ReplicateSubChildren()
                  !Refresh check boxes
                  Loop x# = FirstField() To LastField()
                      If x#{prop:type} = create:Check
                          Post(Event:Accepted,x#)
                      End !If x#{prop:type} = create:Check
                  End !Loop x# = FirstField() To LastField()
                  Display()
      
              Else! If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key) = Level:Benign
      
          Of 2 ! Select Button
      
              SaveRequest#      = GlobalRequest
              GlobalResponse    = RequestCancelled
              GlobalRequest     = SelectRecord
              PickSubAccountsAlias
              If Globalresponse = RequestCompleted
      
                  If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key) = Level:Benign
                      !Found
                      RecordNumber$           = sub:RecordNumber
                      AccountNumber"          = sub:Account_Number
                      !Restore the originating Main Account Number - 4403 (DBH: 19-07-2004)
                      MainAccount"            = sub:Main_Account_Number
                      sub:Record              :=: sub_ali:Record
                      sub:Main_Account_Number = MainAccount"
                      sub:RecordNumber        = RecordNumber$
                      sub:ReplicateAccount    = sub_ali:Account_Number
                      sub:Account_Number       = AccountNumber"
                      ReplicateSubChildren()
                      !Refresh check boxes
                      Loop x# = FirstField() To LastField()
                          If x#{prop:type} = create:Check
                              Post(Event:Accepted,x#)
                          End !If x#{prop:type} = create:Check
                      End !Loop x# = FirstField() To LastField()
                      Display()
      
                  Else ! If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:TRADE_ALIAS.Tryfetch(tra_ali:Account_Number_Key) = Level:Benign
              End
      
              GlobalRequest     = SaveRequest#
      
          Of 1 ! Cancel Button
      End ! Case Missive
      Do ReplicatedFrom
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReplicateFrom, Accepted)
    OF ?sub:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          postcode_routine(sub:postcode,sub:address_line1,sub:address_line2,sub:address_line3)
          Select(?sub:address_line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Postcode, Accepted)
    OF ?sub:Address_Line3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Address_Line3, Accepted)
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = sub:Address_Line3
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              sub:Postcode = sur:Postcode
              sub:Hub = sur:Hub
              ?sub:Postcode{prop:Skip} = True
              ?sub:Postcode{prop:ReadOnly} = True
              ! Inserting (DBH 30/01/2008) # 9613 - Add HUB field
              ?sub:Hub{prop:Skip} = True
              ?sub:Hub{prop:ReadOnly} = True
              ! End (DBH 30/01/2008) #9613
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Case Missive('Cannot find the select Suburb. '&|
                '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the postcode?','ServiceBase 3g',|
                             'mexclam.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      sub:Address_Line3 = ''
                      sub:Postcode = ''
                      sub:Hub = ''
                      ?sub:Postcode{prop:Skip} = True
                      ?sub:Postcode{prop:ReadOnly} = True
                      ?sub:Hub{prop:Skip} = True
                      ?sub:Hub{prop:ReadOnly} = True
                      Bryan.CompFieldColour()
                      Display()
                      Select(?sub:Address_Line3)
                  Of 2 ! Pick Button
                      sub:Address_Line3 = ''
                      sub:Postcode = ''
                      sub:Hub = ''
                      Post(Event:Accepted,?LookupSuburb)
                  Of 1 ! Continue Button
                      ?sub:Postcode{prop:Skip} = False
                      ?sub:Postcode{prop:ReadOnly} = False
                      ?sub:Hub{prop:Skip} = False
                      ?sub:Hub{prop:ReadOnly} = False
                      Bryan.CompFieldColour()
                      Select(?sub:Postcode)
              End ! Case Missive
           End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          Access:TRAHUBS.Clearkey(trh:HubKey)
          trh:TRADEACCAccountNumber = sub:Main_Account_Number
          trh:Hub = sub:Hub
          If Access:TRAHUBS.TryFetch(trh:HubKey)
              Case Missive('Warning! The Hub you have selected above has NOT been allocated to this Franchise.'&|
                '|If you continue then you may not be able to despatch items for this account.','ServiceBase 3g',|
                             'mstop.jpg','/Re-enter|\Ignore')
                  Of 2 ! Ignore Button
                  Of 1 ! Re-enter Button
                      sub:Address_Line3 = ''
                      sub:Postcode = ''
                      sub:Hub = ''
                      Display()
                      Select(?sub:Address_Line3)
              End ! Case Missive
          End ! If Access:TRAHUBS.TryFetch(trh:HubKey) = Level:Benign
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Address_Line3, Accepted)
    OF ?LookupSuburb
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSuburb, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              sub:Address_Line3 = sur:Suburb
              sub:Postcode = sur:Postcode
              sub:Hub = sur:Hub
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      ?sub:Postcode{prop:Skip} = True
      ?sub:Postcode{prop:ReadOnly} = True
      ?sub:Hub{prop:Skip} = True
      ?sub:Hub{prop:ReadOnly} = True
      Bryan.CompFieldColour()
      Display()
      Access:TRAHUBS.Clearkey(trh:HubKey)
      trh:TRADEACCAccountNumber = sub:Main_Account_Number
      trh:Hub = sub:Hub
      If Access:TRAHUBS.TryFetch(trh:HubKey)
          Case Missive('Warning! The Hub you have selected above has NOT been allocated to this Franchise.'&|
            '|If you continue then you may not be able to despatch items for this account.','ServiceBase 3g',|
                         'mstop.jpg','/Re-enter|\Ignore')
              Of 2 ! Ignore Button
              Of 1 ! Re-enter Button
                  sub:Address_Line3 = ''
                  sub:Postcode = ''
                  sub:Hub = ''
                  Display()
                  Select(?sub:Address_Line3)
          End ! Case Missive
      End ! If Access:TRAHUBS.TryFetch(trh:HubKey) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSuburb, Accepted)
    OF ?sub:Telephone_Number
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Telephone_Number, Accepted)
      
          temp_string = Clip(left(sub:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sub:Telephone_Number    = temp_string
          Display(?sub:Telephone_Number)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Telephone_Number, Accepted)
    OF ?sub:Fax_Number
      ! Before Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Fax_Number, Accepted)
      
          temp_string = Clip(left(sub:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          sub:Fax_Number    = temp_string
          Display(?sub:Fax_Number)
      ! After Embed Point: %ControlPostEventHandling) DESC(Legacy: Control Event Handling, after generated code) ARG(?sub:Fax_Number, Accepted)
    OF ?sub:OverrideHeadVATNo
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:OverrideHeadVATNo, Accepted)
      Do CheckOverrideVatNumber
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:OverrideHeadVATNo, Accepted)
    OF ?sub:Courier_Incoming
      IF sub:Courier_Incoming OR ?sub:Courier_Incoming{Prop:Req}
        cou:Courier = sub:Courier_Incoming
        !Save Lookup Field Incase Of error
        look:sub:Courier_Incoming        = sub:Courier_Incoming
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sub:Courier_Incoming = cou:Courier
          ELSE
            !Restore Lookup On Error
            sub:Courier_Incoming = look:sub:Courier_Incoming
            SELECT(?sub:Courier_Incoming)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupIncomingCourier
      ThisWindow.Update
      cou:Courier = sub:Courier_Incoming
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sub:Courier_Incoming = cou:Courier
          Select(?+1)
      ELSE
          Select(?sub:Courier_Incoming)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sub:Courier_Incoming)
    OF ?sub:Courier_Outgoing
      IF sub:Courier_Outgoing OR ?sub:Courier_Outgoing{Prop:Req}
        cou:Courier = sub:Courier_Outgoing
        !Save Lookup Field Incase Of error
        look:sub:Courier_Outgoing        = sub:Courier_Outgoing
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sub:Courier_Outgoing = cou:Courier
          ELSE
            !Restore Lookup On Error
            sub:Courier_Outgoing = look:sub:Courier_Outgoing
            SELECT(?sub:Courier_Outgoing)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupOutgoingCourier
      ThisWindow.Update
      cou:Courier = sub:Courier_Outgoing
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sub:Courier_Outgoing = cou:Courier
          Select(?+1)
      ELSE
          Select(?sub:Courier_Outgoing)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sub:Courier_Outgoing)
    OF ?sub:PriceDespatchNotes
      IF ?sub:PriceDespatchNotes{Prop:Checked} = True
        UNHIDE(?sub:Price_First_Copy_Only)
      END
      IF ?sub:PriceDespatchNotes{Prop:Checked} = False
        HIDE(?sub:Price_First_Copy_Only)
      END
      ThisWindow.Reset
    OF ?sub:OverrideHeadMobile
      IF ?sub:OverrideHeadMobile{Prop:Checked} = True
        UNHIDE(?sub:OverrideMobileDefault)
      END
      IF ?sub:OverrideHeadMobile{Prop:Checked} = False
        HIDE(?sub:OverrideMobileDefault)
      END
      ThisWindow.Reset
    OF ?sub:Force_Customer_Name
      IF ?sub:Force_Customer_Name{Prop:Checked} = True
        ENABLE(?sub:ForceEndUserName)
      END
      IF ?sub:Force_Customer_Name{Prop:Checked} = False
        DISABLE(?sub:ForceEndUserName)
      END
      ThisWindow.Reset
    OF ?sub:Labour_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Labour_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Labour_VAT_Code, Accepted)
    OF ?sub:Parts_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Parts_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Parts_VAT_Code, Accepted)
    OF ?sub:Retail_VAT_Code
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Retail_VAT_Code, Accepted)
      Do Fill_Rates
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Retail_VAT_Code, Accepted)
    OF ?sub:ForceEstimate
      IF ?sub:ForceEstimate{Prop:Checked} = True
        UNHIDE(?sub:EstimateIfOver:Prompt)
        UNHIDE(?sub:EstimateIfOver)
      END
      IF ?sub:ForceEstimate{Prop:Checked} = False
        HIDE(?sub:EstimateIfOver:Prompt)
        HIDE(?sub:EstimateIfOver)
      END
      ThisWindow.Reset
    OF ?sub:InvoiceAtDespatch
      IF ?sub:InvoiceAtDespatch{Prop:Checked} = True
        UNHIDE(?sub:InvoiceType)
      END
      IF ?sub:InvoiceAtDespatch{Prop:Checked} = False
        HIDE(?sub:InvoiceType)
      END
      ThisWindow.Reset
    OF ?sub:InvoiceAtCompletion
      IF ?sub:InvoiceAtCompletion{Prop:Checked} = True
        UNHIDE(?sub:InvoiceTypeComplete)
      END
      IF ?sub:InvoiceAtCompletion{Prop:Checked} = False
        HIDE(?sub:InvoiceTypeComplete)
      END
      ThisWindow.Reset
    OF ?sub:Print_Despatch_Notes
      IF ?sub:Print_Despatch_Notes{Prop:Checked} = True
        ENABLE(?sub:Print_Despatch_Complete)
        ENABLE(?sub:Print_Despatch_Despatch)
        ENABLE(?sub:HideDespAdd)
        ENABLE(?sub:PriceDespatchNotes)
        ENABLE(?sub:Price_First_Copy_Only)
        ENABLE(?sub:Num_Despatch_Note_Copies)
      END
      IF ?sub:Print_Despatch_Notes{Prop:Checked} = False
        sub:Summary_Despatch_Notes = 'NO'
        sub:Despatch_Note_Per_Item = 'NO'
        sub:Print_Despatch_Complete = 'NO'
        sub:Print_Despatch_Despatch = 'NO'
        DISABLE(?sub:Print_Despatch_Complete)
        DISABLE(?sub:Print_Despatch_Despatch)
        DISABLE(?sub:HideDespAdd)
        DISABLE(?sub:PriceDespatchNotes)
        DISABLE(?sub:Price_First_Copy_Only)
        DISABLE(?sub:Num_Despatch_Note_Copies)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Print_Despatch_Notes, Accepted)
      Post(Event:Accepted,?sub:Print_Despatch_Despatch)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Print_Despatch_Notes, Accepted)
    OF ?sub:Print_Despatch_Despatch
      IF ?sub:Print_Despatch_Despatch{Prop:Checked} = True
        ENABLE(?despatch_note_type)
      END
      IF ?sub:Print_Despatch_Despatch{Prop:Checked} = False
        DISABLE(?despatch_note_type)
      END
      ThisWindow.Reset
    OF ?sub:Summary_Despatch_Notes
      IF ?sub:Summary_Despatch_Notes{Prop:Checked} = True
        UNHIDE(?sub:IndividualSummary)
      END
      IF ?sub:Summary_Despatch_Notes{Prop:Checked} = False
        DISABLE(?sub:IndividualSummary)
      END
      ThisWindow.Reset
    OF ?sub:Despatch_Paid_Jobs
      IF ?sub:Despatch_Paid_Jobs{Prop:Checked} = True
        UNHIDE(?sub:SetDespatchJobStatus)
      END
      IF ?sub:Despatch_Paid_Jobs{Prop:Checked} = False
        sub:SetDespatchJobStatus = 0
        HIDE(?sub:SetDespatchJobStatus)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Despatch_Paid_Jobs, Accepted)
      Post(Event:Accepted,?sub:SetDespatchJobStatus)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Despatch_Paid_Jobs, Accepted)
    OF ?sub:SetDespatchJobStatus
      IF ?sub:SetDespatchJobStatus{Prop:Checked} = True
        UNHIDE(?sub:DespatchedJobStatus)
      END
      IF ?sub:SetDespatchJobStatus{Prop:Checked} = False
        HIDE(?sub:DespatchedJobStatus)
      END
      ThisWindow.Reset
    OF ?sub:Despatch_Invoiced_Jobs
      IF ?sub:Despatch_Invoiced_Jobs{Prop:Checked} = True
        UNHIDE(?sub:SetInvoicedJobStatus)
      END
      IF ?sub:Despatch_Invoiced_Jobs{Prop:Checked} = False
        sub:SetInvoicedJobStatus = 0
        HIDE(?sub:SetInvoicedJobStatus)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Despatch_Invoiced_Jobs, Accepted)
      Post(Event:Accepted,?sub:SetInvoicedJobStatus)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Despatch_Invoiced_Jobs, Accepted)
    OF ?sub:SetInvoicedJobStatus
      IF ?sub:SetInvoicedJobStatus{Prop:Checked} = True
        UNHIDE(?sub:InvoicedJobStatus)
      END
      IF ?sub:SetInvoicedJobStatus{Prop:Checked} = False
        HIDE(?sub:InvoicedJobStatus)
      END
      ThisWindow.Reset
    OF ?sub:UseDespatchDetails
      IF ?sub:UseDespatchDetails{Prop:Checked} = True
        ENABLE(?AlternativeNumbers)
      END
      IF ?sub:UseDespatchDetails{Prop:Checked} = False
        DISABLE(?AlternativeNumbers)
      END
      ThisWindow.Reset
    OF ?sub:UseAlternativeAdd
      IF ?sub:UseAlternativeAdd{Prop:Checked} = True
        UNHIDE(?Tab7)
      END
      IF ?sub:UseAlternativeAdd{Prop:Checked} = False
        HIDE(?Tab7)
      END
      ThisWindow.Reset
    OF ?sub:PrintOOWVCPFee
      IF ?sub:PrintOOWVCPFee{Prop:Checked} = True
        ENABLE(?sub:OOWVCPFeeLabel)
        ENABLE(?sub:OOWVCPFeeAmount)
      END
      IF ?sub:PrintOOWVCPFee{Prop:Checked} = False
        DISABLE(?sub:OOWVCPFeeLabel)
        DISABLE(?sub:OOWVCPFeeAmount)
      END
      ThisWindow.Reset
    OF ?sub:SIDJobBooking
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:SIDJobBooking, Accepted)
      Do ForceDealerID
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:SIDJobBooking, Accepted)
    OF ?sub:SIDJobEnquiry
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:SIDJobEnquiry, Accepted)
      Do ForceDealerID
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:SIDJobEnquiry, Accepted)
    OF ?sub:AllowVCPLoanUnits
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:AllowVCPLoanUnits, Accepted)
      ! Inserting (DBH 22/01/2008) # 9472 - If using VCP Loan Units then add company name as Stock Type
      If ~0{prop:AcceptAll}
          If sub:AllowVCPLoanUnits = 1
              Access:STOCKTYP.Clearkey(stp:Use_Loan_Key)
              stp:Use_Loan = 'YES'
              stp:Stock_Type = sub:Company_Name
              If Access:STOCKTYP.TryFetch(stp:Use_Loan_Key)
                  If Access:STOCKTYP.PrimeRecord() = Level:Benign
                      stp:Use_Loan = 'YES'
                      stp:Use_Exchange = 'NO'
                      stp:Stock_Type = Upper(sub:Company_Name)
                      stp:Available = 1
                      If Access:STOCKTYP.TryInsert() = Level:Benign
      
                      End ! If Access:STOCKTYP.TryInsert() = Level:Benign
                  End ! If Access:STOCKTYP.PrimeRecord() = Level:Benign
              End ! If Access:STOCKTYP.TryFetch(stp:Use_Loan_Key)
          Else ! If sub:AllowVCPLoanUnits = 1
              Access:STOCKTYP.Clearkey(stp:Use_Loan_Key)
              stp:Use_Loan = 'YES'
              stp:Stock_Type = sub:Company_Name
              If Access:STOCKTYP.TryFetch(stp:Use_Loan_Key) = Level:Benign
                  Relate:STOCKTYP.Delete(0)
              End ! If Access:STOCKTYP.TryFetch(stp:Use_Loan_Key) = Level:Benign
          End ! If sub:AllowVCPLoanUnits = 1
      End ! If ~0{prop:AcceptAll}
      ! End (DBH 22/01/2008) #9472
      Do ForceDealerID
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:AllowVCPLoanUnits, Accepted)
    OF ?sub:Region
      IF sub:Region OR ?sub:Region{Prop:Req}
        reg:Region = sub:Region
        !Save Lookup Field Incase Of error
        look:sub:Region        = sub:Region
        IF Access:REGIONS.TryFetch(reg:AccountNumberKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            sub:Region = reg:Region
          ELSE
            !Restore Lookup On Error
            sub:Region = look:sub:Region
            SELECT(?sub:Region)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      reg:Region = sub:Region
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          sub:Region = reg:Region
          Select(?+1)
      ELSE
          Select(?sub:Region)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sub:Region)
    OF ?sub:PrintWarrantyVCPFee
      IF ?sub:PrintWarrantyVCPFee{Prop:Checked} = True
        ENABLE(?sub:WarrantyVCPFeeAmount)
      END
      IF ?sub:PrintWarrantyVCPFee{Prop:Checked} = False
        DISABLE(?sub:WarrantyVCPFeeAmount)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sub:Print_Despatch_Notes = 'YES'
      If sub:Print_Despatch_Despatch = 'YES'
          If sub:Despatch_Note_Per_Item <> 'YES' And sub:Summary_Despatch_Notes <> 'YES'
              Case Missive('You have selected to print a Despatch Note at Despatch but have not selected a Despatch Note Type.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              Select(?sub:Print_Despatch_Notes)
              Cycle
          End !If sub:Despatch_Note_Per_Item <> 'YES' And sub:Summary_Despatch_Notes <> 'YES'
      End !If sub:Print_Despatch_Despatch = 'YES'
  End !sub:Print_Despatch_Notes = 'YES'
  !Account Changes
  If ThisWindow.Request <> InsertRecord
      !Has this account, been used anywhere else?
      Found# = 0
      Save_sub_ali_ID = Access:SUBTRACC_ALIAS.SaveFile()
      Access:SUBTRACC_ALIAS.ClearKey(sub_ali:ReplicateFromKey)
      sub_ali:ReplicateAccount = sub:Account_Number
      Set(sub_ali:ReplicateFromKey,sub_ali:ReplicateFromKey)
      Loop
          If Access:SUBTRACC_ALIAS.NEXT()
             Break
          End !If
          If sub_ali:ReplicateAccount <> sub:Account_Number      |
              Then Break.  ! End If
          Found# = 1
          Break
      End !Loop
      Access:SUBTRACC_ALIAS.RestoreFile(Save_sub_ali_ID)
  
      Access:SUBTRACC_ALIAS.Clearkey(sub_ali:RecordNumberKey)
      sub_ali:RecordNumber    = sub:RecordNumber
      If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:RecordNumberKey) = Level:Benign
          !Found
          If sub:Record <> sub_ali:Record
              !TB13335 - J - 21/07/14 - Log whoever it is that is changing names of trade accounts
              if sub_ali:Company_Name <> sub:Company_Name
                  !someone has just changed this - log it
                  WinName = clip(path())&'\SubAccNameChange.log'
                  WinPrint.init(WinName,append_mode)
                  WinPrint.write('Company name changed from: '&clip(sub_ali:Company_Name)&' To: '&clip(sub:Company_Name)&' On: '&format(today(),@d06)& ' At: ' & format(clock(),@t04) & ' by ' &clip(glo:UserCode)&'<13,10>')
                  WinPrint.close()
              END
              !TB13335 end
              If Found# = 1
                  Case Missive('This details of this account have changed.'&|
                    '<13,10>This Sub Account has been used to replicate other account defaults from.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish to propogate these changes made to those other accounts?','ServiceBase 3g',|
                                 'mquest.jpg','\No|/Yes')
                      Of 2 ! Yes Button
                          Case Missive('Warning! Any previous information, apart from the address and account number, will be overwritten from this account''s details.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure?','ServiceBase 3g',|
                                         'mquest.jpg','\No|/Yes')
                              Of 2 ! Yes Button
                                  Clear(AccountQueue)
                                  Free(AccountQueue)
                                  !Write the accounts to update, to a queue
  
                                  Save_sub_ali_ID = Access:SUBTRACC_ALIAS.SaveFile()
                                  Access:SUBTRACC_ALIAS.ClearKey(sub_ali:ReplicateFromKey)
                                  sub_ali:ReplicateAccount = sub:Account_Number
                                  Set(sub_ali:ReplicateFromKey,sub_ali:ReplicateFromKey)
                                  Loop
                                      If Access:SUBTRACC_ALIAS.NEXT()
                                         Break
                                      End !If
                                      If sub_ali:ReplicateAccount <> sub:Account_Number      |
                                          Then Break.  ! End If
                                      accque:RecordNumber = sub_ali:RecordNumber
                                      Add(AccountQueue)
  
                                  End !Loop
                                  Access:SUBTRACC_ALIAS.RestoreFile(Save_sub_ali_ID)
  
                                  sav:HeadRecordNumber    = sub:RecordNumber
                                  !Save the record
                                  Access:SUBTRACC.Update()
  
                                  tmp:SaveState           = Getstate(SUBTRACC)
  
  
                                  !Swap it round and get the alias file
  
                                  Access:SUBTRACC_ALIAS.Clearkey(sub_ali:RecordNumberKey)
                                  sub_ali:RecordNumber   = sav:HeadRecordNumber
                                  If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:RecordNumberKey) = Level:Benign
                                      !Found
  
                                      Loop x# = 1 To Records(AccountQueue)
                                          Get(AccountQueue,x#)
  
                                          Access:SUBTRACC.Clearkey(sub:RecordNumberKey)
                                          sub:RecordNumber    = accque:RecordNumber
                                          If Access:SUBTRACC.Tryfetch(sub:RecordNumberKey) = Level:Benign
                                              !Found
                                              sav:RecordNumber        = sub:RecordNumber
                                              sav:AccountNumber       = sub:Account_Number
                                              sav:CompanyName         = sub:Company_Name
                                              sav:Postcode            = sub:Postcode
                                              sav:AddressLine1        = sub:Address_Line1
                                              sav:AddressLine2        = sub:Address_Line2
                                              sav:AddressLine3        = sub:Address_Line3
                                              sav:TelephoneNumber     = sub:Telephone_Number
                                              sav:FaxNumber           = sub:Fax_Number
                                              sav:EmailAddress        = sub:EmailAddress
                                              sav:ContactName         = sub:Contact_Name
                                              sav:OutgoingCourier     = sub:Courier_Outgoing
                                              sav:Branch              = sub:Account_Number
                                              sav:MainAccountNumber   = sub:Main_Account_Number
  
                                              sub:Record :=: sub_ali:Record
  
                                              sub:RecordNumber        = sav:RecordNumber
                                              sub:Account_Number      = sav:AccountNumber
                                              sub:Company_Name        = sav:CompanyName
                                              sub:Postcode            = sav:Postcode
                                              sub:Address_Line1       = sav:AddressLine1
                                              sub:Address_Line2       = sav:AddressLine2
                                              sub:Address_Line3       = sav:AddressLine3
                                              sub:Telephone_Number    = sav:TelephoneNumber
                                              sub:Fax_Number          = sav:FaxNumber
                                              sub:EmailAddress        = sav:EmailAddress
                                              sub:Contact_Name        = sav:ContactName
                                              sub:Courier_Outgoing    = sav:OutgoingCourier
                                              sub:ReplicateAccount    = sub_ali:Account_Number
                                              sub:Branch              = sav:Branch
                                              sub:Main_Account_Number = sav:MainAccountNumber
  
                                              Access:SUBTRACC.TryUpdate()
  
                                              ReplicateSubChildren()
  
                                          Else ! If Access:SUBTRACC.Tryfetch(sub:RecordNumberKey) = Level:Benign
                                              !Error
                                          End !If Access:SUBTRACC.Tryfetch(sub:RecordNumberKey) = Level:Benign
                                      End !Loop x# = 1 To Records(AccountQueue)
                                  Else ! If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:RecordNumberKey) = Level:Benign
                                      !Error
                                  End !If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:RecordNumberKey) = Level:Benign
  
                                  RestoreState(SUBTRACC,tmp:SaveState)
                                  FreeState(SUBTRACC,tmp:SaveState)
                                  Case Missive('Accounts updated.','ServiceBase 3g',|
                                                 'midea.jpg','/OK')
                                      Of 1 ! OK Button
                                  End ! Case Missive
  
                              Of 1 ! No Button
                          End ! Case Missive
                      Of 1 ! No Button
                  End ! Case Missive
              End !If Found# = 1
          End !If sub:Record <> sub_ali:Record
      Else ! If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:RecordNumberKey) = Level:Benign
          !Error
      End !If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:RecordNumberKey) = Level:Benign
  End !ThisWindow.Request <> InsertRecord
  
  !TB13482 - J - 26/03/15 - if this is not a generic account make sure it is not in TraHubAc
  if sub:Generic_Account = 0
      !reuse the AccountQueue to hold the record number
      clear(AccountQueue)
      Free(AccountQueue)
      !identify problems
      Access:TraHubAc.clearkey(TRA1:RecordNoKey)
      set(TRA1:RecordNoKey,TRA1:RecordNoKey)
      loop
          if access:TraHubAc.next() then break.
          If TRA1:SubAcc = sub:Account_Number then
              AccountQueue.accque:RecordNumber = TRA1:RecordNo
              Add(AccountQueue)
          END
      END
      !Remove problems
      Loop QCounter# = 1 to records(AccountQueue)
          get(AccountQueue,Qcounter#)
          Access:TraHubAc.clearkey(TRA1:RecordNoKey)
          TRA1:RecordNo = AccountQueue.accque:RecordNumber
          if Access:TraHubAc.fetch(TRA1:RecordNoKey) = level:Benign
              Relate:TraHubAc.delete(0)
          END
      END
  END !if not sub:Generic_Account
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?sub:Courier_Incoming
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Incoming, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Incoming, AlertKey)
    END
  OF ?sub:Courier_Outgoing
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Outgoing, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sub:Courier_Outgoing, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Fill_Rates
      If glo:Select2 = 'FINANCIAL'
          ?ServiceDefaults{prop:Hide} = 0
          ?RetailDefaults{prop:Hide} = 0
      End !glo:Select2 = 'FINANCIAL'
      Post(Event:accepted,?sub:print_despatch_notes)
      
      If SecurityCheck('AMEND MAIN ACCOUNT NUMBER')
          ?sub:main_account_number{prop:readonly} = 0
      end
      
      ! Insert --- Add Access Level (DBH: 12/08/2009) #11005
      if (securityCheck('OOW VCP FEE'))
          ?groupVCPFee{prop:disable} = 1
      end ! if (securityCheck('OOW VCP FEE'))
      ! end --- (DBH: 12/08/2009) #11005
      !Autowrite Account
      If ThisWindow.Request = InsertRecord
          If GETINI('TRADE','AutowriteAccount',,CLIP(PATH())&'\SB2KDEF.INI') = 1
              sub:Account_Number = Clip(sub:Main_Account_Number) & '-'
              Display()
              Select(?sub:Account_Number,Len(Clip(sub:Account_Number))+1)
          End !If GETINI('TRADE','AutowriteAccount',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      End !ThisWindow.Request = InsertRecord
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB14.ResetQueue(1)
      FDCB15.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

