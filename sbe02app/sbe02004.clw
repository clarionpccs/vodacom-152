

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE02004.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTradeRecipients PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::tre:Record  LIKE(tre:RECORD),STATIC
QuickWindow          WINDOW('Update Trade Recipient Type'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Trade Recipient'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Recipient Type'),AT(225,154),USE(?tre:RecipientType:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(301,154,124,10),USE(tre:RecipientType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Recipient Type'),TIP('Recipient Type'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(429,150),USE(?LookupRecipientTypes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Contact Name'),AT(225,174),USE(?tre:ContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s60),AT(301,174,124,10),USE(tre:ContactName),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Contact Name'),TIP('Contact Name'),REQ,UPR
                           PROMPT('Email Address'),AT(225,194),USE(?tre:EmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s255),AT(301,194,124,10),USE(tre:EmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address'),REQ
                           CHECK('Send Status Emails'),AT(301,214),USE(tre:SendStatusEmails),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Send Status Emails')
                           CHECK('Send Report Emails'),AT(301,230),USE(tre:SendReportEmails),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Send Report Emails')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:tre:RecipientType                Like(tre:RecipientType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Trade Recipient'
  OF ChangeRecord
    ActionMessage = 'Changing A Trade Recipient'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020301'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTradeRecipients')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tre:Record,History::tre:Record)
  SELF.AddHistoryField(?tre:RecipientType,3)
  SELF.AddHistoryField(?tre:ContactName,4)
  SELF.AddHistoryField(?tre:EmailAddress,5)
  SELF.AddHistoryField(?tre:SendStatusEmails,6)
  SELF.AddHistoryField(?tre:SendReportEmails,7)
  SELF.AddUpdateFile(Access:TRAEMAIL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RECIPTYP.Open
  Relate:TRAEMAIL.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRAEMAIL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateTradeRecipients')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tre:RecipientType{Prop:Tip} AND ~?LookupRecipientTypes{Prop:Tip}
     ?LookupRecipientTypes{Prop:Tip} = 'Select ' & ?tre:RecipientType{Prop:Tip}
  END
  IF ?tre:RecipientType{Prop:Msg} AND ~?LookupRecipientTypes{Prop:Msg}
     ?LookupRecipientTypes{Prop:Msg} = 'Select ' & ?tre:RecipientType{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
    Relate:TRAEMAIL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateTradeRecipients')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickRecipientTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020301'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020301'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020301'&'0')
      ***
    OF ?tre:RecipientType
      IF tre:RecipientType OR ?tre:RecipientType{Prop:Req}
        rec:RecipientType = tre:RecipientType
        !Save Lookup Field Incase Of error
        look:tre:RecipientType        = tre:RecipientType
        IF Access:RECIPTYP.TryFetch(rec:RecipientTypeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tre:RecipientType = rec:RecipientType
          ELSE
            !Restore Lookup On Error
            tre:RecipientType = look:tre:RecipientType
            SELECT(?tre:RecipientType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupRecipientTypes
      ThisWindow.Update
      rec:RecipientType = tre:RecipientType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tre:RecipientType = rec:RecipientType
          Select(?+1)
      ELSE
          Select(?tre:RecipientType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tre:RecipientType)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseRecipientTypes PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(RECIPTYP)
                       PROJECT(rec:RecipientType)
                       PROJECT(rec:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
rec:RecipientType      LIKE(rec:RecipientType)        !List box control field - type derived from field
rec:RecordNumber       LIKE(rec:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Recipient Types'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Recipient Type File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Recipient Type~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Recipient Type'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(264,98,124,10),USE(rec:RecipientType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Recipient Type'),TIP('Recipient Type'),UPR
                           BUTTON,AT(448,183),USE(?Insert:2),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,218),USE(?Change:2),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,250),USE(?Delete:2),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020289'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseRecipientTypes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RECIPTYP.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RECIPTYP,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseRecipientTypes')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,rec:RecipientTypeKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?rec:RecipientType,rec:RecipientType,1,BRW1)
  BRW1.AddField(rec:RecipientType,BRW1.Q.rec:RecipientType)
  BRW1.AddField(rec:RecordNumber,BRW1.Q.rec:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseRecipientTypes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateRecipientTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020289'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020289'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020289'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateRecipientTypes PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::rec:Record  LIKE(rec:RECORD),STATIC
QuickWindow          WINDOW('Update Recipient Types'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Recipient Type'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Recipient Type'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(248,204,184,10),USE(rec:RecipientType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Recipient Type'),TIP('Recipient Type'),REQ,UPR
                         END
                       END
                       BUTTON,AT(300,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Recipient Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Recipient Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020290'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRecipientTypes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rec:Record,History::rec:Record)
  SELF.AddHistoryField(?rec:RecipientType,2)
  SELF.AddUpdateFile(Access:RECIPTYP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RECIPTYP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RECIPTYP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateRecipientTypes')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateRecipientTypes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020290'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020290'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020290'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateSubRecipients PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::sue:Record  LIKE(sue:RECORD),STATIC
QuickWindow          WINDOW('Update Sub Account Recipient Type'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Sub Account Recipient Type'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Recipient Type'),AT(225,162),USE(?tre:RecipientType:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(301,162,124,10),USE(sue:RecipientType),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Recipient Type'),TIP('Recipient Type'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(429,158),USE(?LookupRecipientTypes),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Contact Name'),AT(225,182),USE(?tre:ContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s60),AT(301,182,124,10),USE(sue:ContactName),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Contact Name'),TIP('Contact Name'),REQ,UPR
                           PROMPT('Email Address'),AT(225,202),USE(?tre:EmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s255),AT(301,202,124,10),USE(sue:EmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address'),REQ
                           CHECK('Send Status Emails'),AT(301,222),USE(sue:SendStatusEmails),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Send Status Emails')
                           CHECK('Send Report Emails'),AT(301,234),USE(sue:SendReportEmails),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),VALUE('1','0'),MSG('Send Report Emails')
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:sue:RecipientType                Like(sue:RecipientType)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account Recipient'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account Recipient'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020304'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSubRecipients')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sue:Record,History::sue:Record)
  SELF.AddHistoryField(?sue:RecipientType,3)
  SELF.AddHistoryField(?sue:ContactName,4)
  SELF.AddHistoryField(?sue:EmailAddress,5)
  SELF.AddHistoryField(?sue:SendStatusEmails,6)
  SELF.AddHistoryField(?sue:SendReportEmails,7)
  SELF.AddUpdateFile(Access:SUBEMAIL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RECIPTYP.Open
  Relate:SUBEMAIL.Open
  Access:TRAEMAIL.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBEMAIL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateSubRecipients')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?sue:RecipientType{Prop:Tip} AND ~?LookupRecipientTypes{Prop:Tip}
     ?LookupRecipientTypes{Prop:Tip} = 'Select ' & ?sue:RecipientType{Prop:Tip}
  END
  IF ?sue:RecipientType{Prop:Msg} AND ~?LookupRecipientTypes{Prop:Msg}
     ?LookupRecipientTypes{Prop:Msg} = 'Select ' & ?sue:RecipientType{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
    Relate:SUBEMAIL.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSubRecipients')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickRecipientTypes
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020304'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020304'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020304'&'0')
      ***
    OF ?sue:RecipientType
      IF sue:RecipientType OR ?sue:RecipientType{Prop:Req}
        rec:RecipientType = sue:RecipientType
        !Save Lookup Field Incase Of error
        look:sue:RecipientType        = sue:RecipientType
        IF Access:RECIPTYP.TryFetch(rec:RecipientTypeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sue:RecipientType = rec:RecipientType
          ELSE
            !Restore Lookup On Error
            sue:RecipientType = look:sue:RecipientType
            SELECT(?sue:RecipientType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupRecipientTypes
      ThisWindow.Update
      rec:RecipientType = sue:RecipientType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sue:RecipientType = rec:RecipientType
          Select(?+1)
      ELSE
          Select(?sue:RecipientType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sue:RecipientType)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

NotAvailableReason PROCEDURE                          !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Reason           STRING(60)
tmp:Close            BYTE(0)
window               WINDOW('Not Available Reason'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Not Available Reason'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Reason For Making Using "Not Available"'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s60),AT(248,206,184,10),USE(tmp:Reason),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Reason'),TIP('Reason'),REQ,UPR
                         END
                       END
                       BUTTON,AT(368,258),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:Reason)


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020271'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('NotAvailableReason')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','NotAvailableReason')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  ! Save Window Name
   AddToLog('Window','Close','NotAvailableReason')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020271'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020271'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020271'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If tmp:Reason = ''
          Select(?tmp:Reason)
      Else !tmp:Reason = ''
          tmp:Close = 1
          Post(Event:CloseWindow)
      End !tmp:Reason = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      If ~tmp:Close
          Cycle
      End !tmp:Close
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
ReplicateToHeadAccounts PROCEDURE (func:HeadAccNo,func:SubAccNo) !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_sue_ali_id      USHORT,AUTO
save_suc_ali_id      USHORT,AUTO
save_sua_ali_id      USHORT,AUTO
tmp:SubAccount       STRING(30)
tmp:tag              STRING(1)
tmp:HeadAccount      STRING(30)
tmp:NewAccount       STRING(30)
BRW4::View:Browse    VIEW(TRADEACC_ALIAS)
                       PROJECT(tra_ali:Account_Number)
                       PROJECT(tra_ali:Company_Name)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
tra_ali:Account_Number LIKE(tra_ali:Account_Number)   !List box control field - type derived from field
tra_ali:Company_Name   LIKE(tra_ali:Company_Name)     !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Replicate Sub Account'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Replicating Sub Account: '),AT(236,110),USE(?Prompt1),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           STRING(@s30),AT(336,110,108,12),USE(tmp:SubAccount),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Select which Head Accounts you wish to replicate the above sub account to.'),AT(198,126),USE(?Prompt2),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Note: The new sub accounts will have their account number prefixed with the head' &|
   ' account''s number'),AT(178,140,324,18),USE(?Prompt3),CENTER,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(252,162,168,162),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@66L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(311,220,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(311,240,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON,AT(424,188),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(424,218),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(424,248),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Replicate Sub Account'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,332),USE(?Finish),TRN,FLAT,LEFT,ICON('finishp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW4.UpdateBuffer
   glo:Queue.Pointer = tra_ali:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra_ali:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra_ali:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::5:QUEUE = glo:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = tra_ali:RecordNumber
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra_ali:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020296'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ReplicateToHeadAccounts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SUBACCAD.Open
  Relate:SUBACCAD_ALIAS.Open
  Relate:SUBCHRGE_ALIAS.Open
  Relate:SUBEMAIL_ALIAS.Open
  Relate:SUBTRACC_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  Access:SUBTRACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:SUBEMAIL.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:TRADEACC_ALIAS,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  tmp:SubAccount  = func:SubAccNo
  tmp:HeadAccount = func:HeadAccNo
  ! Save Window Name
   AddToLog('Window','Open','ReplicateToHeadAccounts')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,tra_ali:Account_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,tra_ali:Account_Number,1,BRW4)
  BRW4.SetFilter('(Upper(tra_ali:Account_Number) <<> Upper(tmp:HeadAccount) And Upper(tra_ali:Use_Sub_Accounts) = ''YES'')')
  BIND('tmp:tag',tmp:tag)
  BIND('tmp:HeadAccount',tmp:HeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:tag,BRW4.Q.tmp:tag)
  BRW4.AddField(tra_ali:Account_Number,BRW4.Q.tra_ali:Account_Number)
  BRW4.AddField(tra_ali:Company_Name,BRW4.Q.tra_ali:Company_Name)
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:SUBACCAD.Close
    Relate:SUBACCAD_ALIAS.Close
    Relate:SUBCHRGE_ALIAS.Close
    Relate:SUBEMAIL_ALIAS.Close
    Relate:SUBTRACC_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ReplicateToHeadAccounts')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020296'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020296'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020296'&'0')
      ***
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      If ~Records(glo:Queue)
          Case Missive('You must tag at least one account.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
      Else !Records(glo:Queue)
          tmp:NewAccount = tmp:SubAccount
          Access:SUBTRACC_ALIAS.Clearkey(sub_ali:Account_Number_Key)
          sub_ali:Account_Number  = tmp:SubAccount
          If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key) = Level:Benign
              !Found
              If Sub(sub_ali:Account_Number,1,Len(Clip(sub_ali:Main_Account_Number))) = sub_Ali:Main_Account_Number
                  tmp:NewAccount = Sub(sub_ali:Account_Number,Len(Clip(sub_ali:Main_Account_Number)) + 1,15)
              End !If Sub(sub_ali:Account_Number,1,Len(sub_ali:Main_Account_Number)) = sub_Ali:Main_Account_Number
          Else! If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key.) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:SUBTRACC_ALIAS.Tryfetch(sub_ali:Account_Number_Key.) = Level:Benign
      
      
          Loop x# = 1 To Records(glo:Queue)
              Get(glo:Queue,x#)
              Access:TRADEACC_ALIAS.Clearkey(tra_ali:RecordNumberKey)
              tra_ali:RecordNumber    = glo:Pointer
              If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
                  !Found
                  If Access:SUBTRACC.PrimeRecord() = Level:Benign
                      RecordNumber$   = sub:RecordNumber
                      sub:Record  :=: sub_ali:Record
                      sub:RecordNumber = RecordNumber$
                      sub:Main_Account_Number = tra_Ali:Account_Number
                      sub:Account_Number = Clip(sub:Main_Account_Number) & '-' & tmp:NewAccount
      
                      If Access:SUBTRACC.TryInsert() = Level:Benign
                          !Insert Successful
                          !Now do childrean
                          ReplicateSubChildren()
                      Else !If Access:SUBTRACC.TryInsert() = Level:Benign
                          !Insert Failed
                          Access:SUBTRACC.CancelAutoInc()
                      End !If Access:SUBTRACC.TryInsert() = Level:Benign
                  End !If Access:SUBTRACC.PrimeRecord() = Level:Benign
              Else! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:TRADEACC_ALIAS.Tryfetch(tra_ali:RecordNumberKey) = Level:Benign
          End !x# = 1 To Records(glo:Queue)
          Case Missive('Accounts Updated.','ServiceBase 3g',|
                         'midea.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          Post(Event:CloseWindow)
      End !Records(glo:Queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra_ali:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra_ali:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue

ReplicateSubChildren PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_sua_ali_id      USHORT,AUTO
save_suc_ali_id      USHORT,AUTO
save_sue_ali_id      USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Save_sua_ali_ID = Access:SUBACCAD_ALIAS.SaveFile()
    Access:SUBACCAD_ALIAS.ClearKey(sua_ali:AccountNumberKey)
    sua_ali:RefNumber     = sub_ali:RecordNumber
    Set(sua_ali:AccountNumberKey,sua_ali:AccountNumberKey)
    Loop
        If Access:SUBACCAD_ALIAS.NEXT()
           Break
        End !If
        If sua_ali:RefNumber     <> sub_ali:RecordNumber      |
            Then Break.  ! End If


        Access:SUBACCAD.ClearKey(sua:AccountNumberKey)
        sua:RefNumber     = sub:RecordNumber
        sua:AccountNumber = sua_ali:AccountNumber
        If Access:SUBACCAD.TryFetch(sua:AccountNumberKey)
           !failed
            If Access:SUBACCAD.PrimeRecord() = Level:Benign
                RecordNumber$   = sua:RecordNumber
                sua:Record :=: sua_ali:Record
                sua:RecordNumber = RecordNumber$
                sua:RefNumber       = sub:RecordNumber
                If Access:SUBACCAD.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:SUBACCAD.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:SUBACCAD.Cancelautoinc()
                End !If Access:SUBACCAD.TryInsert() = Level:Benign
            End !If Access:SUBACCAD.PrimeRecord() = Level:Benign
        End !If Access:SUBACCAD.TryFetch(sua:AccountNumberKey)
    End !Loop
    Access:SUBACCAD_ALIAS.RestoreFile(Save_sua_ali_ID)

    Save_suc_ali_ID = Access:SUBCHRGE_ALIAS.SaveFile()
    Access:SUBCHRGE_ALIAS.ClearKey(suc_ali:Account_Charge_Key)
    suc_ali:Account_Number = sub_ali:Account_Number
    Set(suc_ali:Account_Charge_Key,suc_ali:Account_Charge_Key)
    Loop
        If Access:SUBCHRGE_ALIAS.NEXT()
           Break
        End !If
        If suc_ali:Account_Number <> sub_ali:Account_Number      |
            Then Break.  ! End If
        Access:SUBCHRGE.ClearKey(suc:Repair_Type_Key)
        suc:Account_Number = sub:Account_Number
        suc:Model_Number   = suc_ali:Model_Number
        suc:Repair_Type    = suc_ali:Repair_Type
        If Access:SUBCHRGE.TryFetch(suc:Repair_Type_Key)
            !failed
            If Access:SUBCHRGE.PrimeRecord() = Level:Benign
                suc:Record :=: suc_ali:Record
                suc:Account_Number  = sub:Account_Number
                If Access:SUBCHRGE.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:SUBCHRGE.TryInsert() = Level:Benign
                    !Insert Failed
                End !If Access:SUBCHRGE.TryInsert() = Level:Benign
            End !If Access:SUBCHRGE.PrimeRecord() = Level:Benign
        End!If Access:SUBCHRGE.TryFetch(suc:Repair_Type_Key)
    End !Loop
    Access:SUBCHRGE_ALIAS.RestoreFile(Save_suc_ali_ID)

    Save_sue_ali_ID = Access:SUBEMAIL_ALIAS.SaveFile()
    Access:SUBEMAIL_ALIAS.ClearKey(sue_ali:RecipientTypeKey)
    sue_ali:RefNumber     = sub_ali:RecordNumber
    Set(sue_ali:RecipientTypeKey,sue_ali:RecipientTypeKey)
    Loop
        If Access:SUBEMAIL_ALIAS.NEXT()
           Break
        End !If
        If sue_ali:RefNumber     <> sub_ali:RecordNumber      |
            Then Break.  ! End If
        Access:SUBEMAIL.ClearKey(sue:RecipientTypeKey)
        sue:RefNumber     = sub:RecordNumber
        sue:RecipientType = sue_ali:RecipientType
        If Access:SUBEMAIL.TryFetch(sue:RecipientTypeKey)
            !failed
            If Access:SUBEMAIL.PrimeRecord() = Level:Benign
                RecordNumber$   = sue:RecordNumber
                sue:Record :=: sue_ali:Record
                sue:RecordNumber    = RecordNumber$
                sue:RefNumber   = sub:RecordNumber
                If Access:SUBEMAIL.TryInsert() = Level:Benign
                    !Insert Successful
                Else !If Access:SUBEMAIL.TryInsert() = Level:Benign
                    !Insert Failed
                    Access:SUBEMAIL.Cancelautoinc()
                End !If Access:SUBEMAIL.TryInsert() = Level:Benign
            End !If Access:SUBEMAIL.PrimeRecord() = Level:Benign
        End!If Access:SUBEMAIL.TryFetch(sue:RecipientTypeKey)
    End !Loop
    Access:SUBEMAIL_ALIAS.RestoreFile(Save_sue_ali_ID)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ExchangeLoanOrderWindow PROCEDURE (STRING strLocation,STRING fType) !Generated from procedure template - Window

tmp:Manufacturer     STRING(30)
tmp:Model_Number     STRING(30)
tmp:Qty_Required     LONG
tmp:Notes            STRING(255)
tmp:CurrentCount     LONG
locOrderNumber       LONG
ModelQueue           QUEUE,PRE(modque)
ModelNumber          STRING(30)
Quantity             LONG
Count                LONG
Manufacturer         STRING(30)
Notes                STRING(255)
                     END
window               WINDOW('Order Loan/Exchange Units'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,128),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Order Exchange Units'),USE(?Tab1)
                           PROMPT('Manufacturer'),AT(184,92),USE(?tmp:Manufacturer:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(260,92,124,10),USE(tmp:Manufacturer),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(388,87),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model'),AT(184,110),USE(?tmp:Model_Number:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(260,111,124,10),USE(tmp:Model_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(388,106),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Quantity Required'),AT(184,130),USE(?tmp:Qty_Required:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@s8),AT(260,130,,10),USE(tmp:Qty_Required),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,RANGE(1,9999999),STEP(1)
                           LIST,AT(260,215,164,111),USE(?List1),VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),FORMAT('120L(2)|M~Model Number~@s30@32L(2)|M~Quantity~@s8@'),FROM(ModelQueue)
                           PROMPT('Models To Order'),AT(184,218),USE(?Prompt7),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Notes'),AT(184,150),USE(?Notes:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           TEXT,AT(260,150,164,54),USE(tmp:Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,180),USE(?ButtonOrderUnit),TRN,FLAT,LEFT,ICON('addordp.jpg'),DEFAULT
                         END
                       END
                       BUTTON,AT(448,334),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PANEL,AT(164,212,352,118),USE(?Panel5),FILL(09A6A7CH)
                       BUTTON,AT(444,300),USE(?ButtonDelete),TRN,FLAT,ICON('remmdlp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Order Exchange Units'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(380,334),USE(?ButtonFinish),TRN,FLAT,LEFT,ICON('creordp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
PassModelQueue  Queue(DefModelQueue)
                End
!Save Entry Fields Incase Of Lookup
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:Model_Number                Like(tmp:Model_Number)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020270'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExchangeLoanOrderWindow')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHORDR.Open
  Relate:LOANORNO.Open
  Relate:MANUFACT.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:EXCHORNO.UseFile
  Access:USERS.UseFile
  Access:MODEXCHA.UseFile
  Access:LOAORDR.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  IF (fType = 'LOA')
      ?WindowTitle{prop:Text} = 'Order Loan Units'
  END
  ! Save Window Name
   AddToLog('Window','Open','ExchangeLoanOrderWindow')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:Model_Number{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Model_Number{Prop:Tip}
  END
  IF ?tmp:Model_Number{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Model_Number{Prop:Msg}
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHORDR.Close
    Relate:LOANORNO.Close
    Relate:MANUFACT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','ExchangeLoanOrderWindow')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Manufacturers
      SelectModelKnownMake
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
      glo:Select2 = tmp:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
    OF ?CallLookup:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
      glo:Select2 = tmp:Manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:2, Accepted)
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Post(event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
      glo:Select2 = ''
      IF tmp:Model_Number OR ?tmp:Model_Number{Prop:Req}
        mod:Model_Number = tmp:Model_Number
        mod:Manufacturer = tmp:Manufacturer
        GLO:Select2 = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Model_Number        = tmp:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Model_Number = mod:Model_Number
          ELSE
            CLEAR(mod:Manufacturer)
            CLEAR(GLO:Select2)
            !Restore Lookup On Error
            tmp:Model_Number = look:tmp:Model_Number
            SELECT(?tmp:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Model_Number, Accepted)
    OF ?CallLookup:2
      ThisWindow.Update
      mod:Model_Number = tmp:Model_Number
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Model_Number)
    OF ?ButtonOrderUnit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOrderUnit, Accepted)
      if tmp:Manufacturer = ''
          select(?tmp:Manufacturer)
          cycle
      end
      
      if tmp:Model_Number = ''
          select(?tmp:Model_Number)
          cycle
      end
      
      if tmp:Qty_Required = 0
          select(?tmp:Qty_Required)
          cycle
      end
      
      modque:ModelNumber = tmp:Model_Number
      modque:Manufacturer = tmp:Manufacturer
      GET(ModelQueue,modque:ModelNumber,modque:Manufacturer)
      IF (NOT ERROR())
          Beep(Beep:SystemHand)  ;  Yield()
          Case Missive('Error. You have already requested this Model.','ServiceBase',|
                         'mstop.jpg','/&OK') 
          Of 1 ! &OK Button
          End!Case Message
          CYCLE
      END
      
      Case fType
      OF 'LOA'
          ! Check the stock
          Access:STOCK.Clearkey(sto:LoanModelKey)
          sto:Location = MainStoreLocation()
          sto:Manufacturer = tmp:Manufacturer
          sto:LoanModelNumber = tmp:Model_Number
          IF (Access:STOCK.Tryfetch(sto:LoanModelKey))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error. The selected Model is not available to be ordered.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END ! IF (Access:STOCK.Tryfetch(sto:LoanModelKey))
      
          ! Check the order cap
          Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
          mod:Manufacturer = tmp:Manufacturer
          mod:Model_Number = tmp:Model_Number
          IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected Model Number cannot be found.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          IF (mod:RRCOrderCap > 0 And tmp:Qty_Required > mod:RRCOrderCap)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error. The maximum quantity you can order for this model is ' & Clip(mod:RRCOrderCap) & '.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              Select(?tmp:Qty_Required)
              CYCLE
          END
      
      OF 'EXC'
      
          ! Check the stock cap
          Access:STOCK.Clearkey(sto:ExchangeModelKey)
          sto:Location = MainStoreLocation()
          sto:Manufacturer = tmp:Manufacturer
          sto:ExchangeModelNumber = tmp:Model_Number
          IF (Access:STOCK.TryFetch(sto:ExchangeModelKey))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error. The selected Model is not available to be ordered.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          ! #12398 Check if the model is available for this franchise (DBH: 07/12/2011)
          Access:MODEXCHA.Clearkey(moa:AccountNumberKey)
          moa:AccountNumber = glo:UserAccountNumber
          moa:Manufacturer = tmp:Manufacturer
          moa:ModelNumber = tmp:Model_Number
          IF (Access:MODEXCHA.TryFetch(moa:AccountNumberKey))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error! You have not been authorised to order this model number.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLe
          END
      
      
          IF (sto:ExchangeOrderCap > 0 And tmp:Qty_Required > sto:ExchangeOrderCap)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Error. The maximum quantity you can order for this model is ' & Clip(sto:ExchangeOrderCap) & '.','ServiceBase',|
                             'mstop.jpg','/&OK')
              Of 1 ! &OK Button
              End!Case Message
              Select(?tmp:Qty_Required)
              CYCLE
          END
      
      
      
      END ! CASE fType
      
      ! Add To Temp Queue
      modque:ModelNumber = tmp:Model_Number
      modque:Manufacturer = tmp:Manufacturer
      modque:Quantity = tmp:Qty_Required
      modque:Count = tmp:CurrentCount
      modque:Notes = Upper(tmp:Notes)
      Add(ModelQueue)
      tmp:CurrentCount += 1
      Sort(ModelQueue,-modque:Count)
      
      !Add to queue, and start again
      tmp:Manufacturer = ''
      tmp:Model_Number = ''
      tmp:Qty_Required = 0
      tmp:Notes = ''
      Display()
      Select(?tmp:Manufacturer)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonOrderUnit, Accepted)
    OF ?ButtonDelete
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonDelete, Accepted)
      Get(ModelQueue,Choice(?List1))
      IF (NOT ERROR())
          Beep(Beep:SystemQuestion)  ;  Yield()
          Case Missive('Are you sure you want to delete the selected Model?','ServiceBase',|
                         'mquest.jpg','\&No|/&Yes') 
          Of 2 ! &Yes Button
              Delete(ModelQueue)
      
          Of 1 ! &No Button
          End!Case Message
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonDelete, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020270'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020270'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020270'&'0')
      ***
    OF ?ButtonFinish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonFinish, Accepted)
      IF (Records(ModelQueue))
          Beep(Beep:SystemQuestion)  ;  Yield()
          Case Missive('Are you sure you want to create the order?','ServiceBase',|
                         'mquest.jpg','\&No|/&Yes') 
          Of 2 ! &Yes Button
              FREE(PassModelQueue)
      
              CASE fType
              OF 'LOA'
                  IF (Access:LOANORNO.PrimeRecord() = Level:Benign)
                      lno:Location = strLocation
                      lno:UserCode = glo:Usercode
                      IF (Access:LOANORNO.TryInsert())
                          Access:LOANORNO.CancelAutoInc()
                      ELSE
                          LOOP ll# = 1 TO RECORDS(ModelQueue)
                              GET(ModelQueue,ll#)
                              IF (Access:LOAORDR.PrimeRecord() = Level:Benign)
                                  lor:Location = strLocation
                                  lor:Manufacturer = modque:Manufacturer
                                  lor:Model_Number = modque:ModelNumber
                                  lor:Qty_Required = modque:Quantity
                                  lor:Qty_Received = 0
                                  lor:Notes = UPPER(modque:Notes)
                                  lor:OrderNumber = lno:RecordNumber
                                  IF (Access:LOAORDR.TryInsert())
                                      Access:LOAORDR.CancelAutoInc()
                                  ELSE
                                      PassModelQueue.Manufacturer = modque:Manufacturer
                                      PassModelQueue.ModelNumber = modque:ModelNumber
                                      PassModelQueue.Quantity = modque:Quantity
                                      ADD(PassModelQueue)
                                  END
                              END ! IF (Access:LOAODRD.PrimeRecord() = Level:Benign)
                          END ! LOOP ll# = 1 TO RECORDS(ModelQueue)
                          ! Print paperwork with Order Number
                          IF (Records(PassModelQueue))
                              LoanOrders(PassModelQueue,lno:RecordNumber)
                          END
                      END ! IF (Access:LOANORNO.TryInsert())
                  END ! IF (Access:LOANORNO.PrimeRecord() = Level:Benign)
              OF 'EXC'
                  IF (Access:EXCHORNO.PrimeRecord() = Level:Benign)
                      eno:Location = strLocation
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password = glo:Password
                      IF (Access:USERS.TryFetch(use:Password_Key))
                      END
                      eno:UserCode = use:User_Code
                      IF (Access:EXCHORNO.TryInsert())
                          Access:EXCHORNO.CancelAutoInc()
                      ELSE
                          Loop ll# = 1 To Records(ModelQueue)
                              GET(ModelQueue,ll#)
      
                              IF (Access:EXCHORDR.PrimeRecord() = Level:Benign)
                                  exo:Location = strLocation
                                  exo:Manufacturer = modque:Manufacturer
                                  exo:Model_Number = modque:ModelNumber
                                  exo:Qty_Required = modque:Quantity
                                  exo:Qty_Received = 0
                                  exo:Notes = Upper(modque:Notes)
                                  exo:OrderNumber = eno:RecordNumber
                                  IF (Access:EXCHORDR.TryInsert())
                                      Access:EXCHORDR.CancelAutoInc()
                                  ELSE
                                      PassModelQueue.Manufacturer = modque:Manufacturer
                                      PassModelQueue.ModelNumber = modque:ModelNumber
                                      PassModelQueue.Quantity = modque:Quantity
                                      ADD(PassModelQueue)
                                  END
                              END
                          END
                          ! Print paperwork with Order Number
                          IF (Records(PassModelQueue))
                              ExchangeOrders(PassModelQueue,0,0,eno:RecordNumber)
                          END
      
                      END
                  END
              END
                  
          Of 1 ! &No Button
          End!Case Message
      END
      
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonFinish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Rapid_Exchange_Transfer PROCEDURE                     !Generated from procedure template - Window

Requests_Q           QUEUE,PRE()
Requested_Location   STRING(30)
                     END
qFulFilled           QUEUE,PRE()
ExchangeRefNumber    LONG
ExchangeOrderRefNumber LONG
                     END
tmp:Scanned_IMEI     STRING(30)
tmp:courier          STRING(30)
tmp:From_Site_Location STRING(30)
tmp:From_Stock_Type  STRING(30)
tmp:To_Site_Location STRING(30)
tmp:To_Stock_Type    STRING(30)
tmp:Response         BYTE
tmp:Original_Location STRING(30)
tmp:ListPos          LONG
tmp:Notes            STRING(1)
tmp:SiteLocation     STRING(30)
locPartNumber        STRING(30)
BRW7::View:Browse    VIEW(EXCHORDR)
                       PROJECT(exo:Manufacturer)
                       PROJECT(exo:Model_Number)
                       PROJECT(exo:OrderNumber)
                       PROJECT(exo:DateCreated)
                       PROJECT(exo:TimeCreated)
                       PROJECT(exo:Qty_Required)
                       PROJECT(exo:Qty_Received)
                       PROJECT(exo:Notes)
                       PROJECT(exo:Ref_Number)
                       PROJECT(exo:Received)
                       PROJECT(exo:Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
exo:Manufacturer       LIKE(exo:Manufacturer)         !List box control field - type derived from field
exo:Manufacturer_NormalFG LONG                        !Normal forground color
exo:Manufacturer_NormalBG LONG                        !Normal background color
exo:Manufacturer_SelectedFG LONG                      !Selected forground color
exo:Manufacturer_SelectedBG LONG                      !Selected background color
exo:Model_Number       LIKE(exo:Model_Number)         !List box control field - type derived from field
exo:Model_Number_NormalFG LONG                        !Normal forground color
exo:Model_Number_NormalBG LONG                        !Normal background color
exo:Model_Number_SelectedFG LONG                      !Selected forground color
exo:Model_Number_SelectedBG LONG                      !Selected background color
exo:OrderNumber        LIKE(exo:OrderNumber)          !List box control field - type derived from field
exo:OrderNumber_NormalFG LONG                         !Normal forground color
exo:OrderNumber_NormalBG LONG                         !Normal background color
exo:OrderNumber_SelectedFG LONG                       !Selected forground color
exo:OrderNumber_SelectedBG LONG                       !Selected background color
locPartNumber          LIKE(locPartNumber)            !List box control field - type derived from local data
locPartNumber_NormalFG LONG                           !Normal forground color
locPartNumber_NormalBG LONG                           !Normal background color
locPartNumber_SelectedFG LONG                         !Selected forground color
locPartNumber_SelectedBG LONG                         !Selected background color
tmp:Notes              LIKE(tmp:Notes)                !List box control field - type derived from local data
tmp:Notes_NormalFG     LONG                           !Normal forground color
tmp:Notes_NormalBG     LONG                           !Normal background color
tmp:Notes_SelectedFG   LONG                           !Selected forground color
tmp:Notes_SelectedBG   LONG                           !Selected background color
tmp:Notes_Icon         LONG                           !Entry's icon ID
exo:DateCreated        LIKE(exo:DateCreated)          !List box control field - type derived from field
exo:DateCreated_NormalFG LONG                         !Normal forground color
exo:DateCreated_NormalBG LONG                         !Normal background color
exo:DateCreated_SelectedFG LONG                       !Selected forground color
exo:DateCreated_SelectedBG LONG                       !Selected background color
exo:TimeCreated        LIKE(exo:TimeCreated)          !List box control field - type derived from field
exo:TimeCreated_NormalFG LONG                         !Normal forground color
exo:TimeCreated_NormalBG LONG                         !Normal background color
exo:TimeCreated_SelectedFG LONG                       !Selected forground color
exo:TimeCreated_SelectedBG LONG                       !Selected background color
exo:Qty_Required       LIKE(exo:Qty_Required)         !List box control field - type derived from field
exo:Qty_Required_NormalFG LONG                        !Normal forground color
exo:Qty_Required_NormalBG LONG                        !Normal background color
exo:Qty_Required_SelectedFG LONG                      !Selected forground color
exo:Qty_Required_SelectedBG LONG                      !Selected background color
exo:Qty_Received       LIKE(exo:Qty_Received)         !List box control field - type derived from field
exo:Qty_Received_NormalFG LONG                        !Normal forground color
exo:Qty_Received_NormalBG LONG                        !Normal background color
exo:Qty_Received_SelectedFG LONG                      !Selected forground color
exo:Qty_Received_SelectedBG LONG                      !Selected background color
exo:Notes              LIKE(exo:Notes)                !List box control field - type derived from field
exo:Notes_NormalFG     LONG                           !Normal forground color
exo:Notes_NormalBG     LONG                           !Normal background color
exo:Notes_SelectedFG   LONG                           !Selected forground color
exo:Notes_SelectedBG   LONG                           !Selected background color
exo:Ref_Number         LIKE(exo:Ref_Number)           !Primary key field - type derived from field
exo:Received           LIKE(exo:Received)             !Browse key field - type derived from field
exo:Location           LIKE(exo:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK5::exo:Location         LIKE(exo:Location)
HK5::exo:Received         LIKE(exo:Received)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Rapid Exchange Transfer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       MENUBAR
                         MENU('Options'),USE(?Options)
                           ITEM('Set Courier'),USE(?OptionsSetCourier)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(592,9),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Exchange Transfer'),AT(8,9),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,384,672,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(4,7,644,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(4,25,332,56),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('FROM'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(68,43),USE(?tmp:From_Site_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(120,43,124,10),USE(tmp:From_Site_Location),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(248,38),USE(?From_Location_Button),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Stock Type'),AT(68,62),USE(?tmp:From_Stock_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(120,62,124,10),USE(tmp:From_Stock_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(248,59),USE(?From_Stock_Type_Button),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       SHEET,AT(340,25,336,56),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('TO'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           STRING('TO'),AT(344,30),USE(?String3),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           PROMPT('Location'),AT(392,43),USE(?tmp:To_Site_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(444,43,124,10),USE(tmp:To_Site_Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Stock Type'),AT(392,62),USE(?tmp:To_Stock_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(444,62,124,10),USE(tmp:To_Stock_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(572,59),USE(?To_Stock_Type_Button),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       SHEET,AT(4,89,672,292),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Requests'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('N - Notes'),AT(608,209),USE(?Prompt6),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Site Location'),AT(20,119),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(20,135,556,202),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('110L(2)|M*~Manufacturer~@s30@110L(2)|M*~Model~@s30@40R(2)|M*~Order No~@n_8@100L(' &|
   '2)|M*~Part Number~@s30@11C|*J~N~@s1@[52L(2)|*~Date~C(0)@d6@28L(2)|*~Time~C(0)@t7' &|
   '@]|~Created~[39R(2)|M*~Ordered~L@s8@34L(2)|M*~Fulfilled~@s8@]|M~Quantity~0L(2)|M' &|
   '*~Notes~@s255@'),FROM(Queue:Browse)
                           BUTTON,AT(604,151),USE(?ViewNotes),TRN,FLAT,LEFT,ICON('viewnotp.jpg')
                           BUTTON,AT(604,252),USE(?ReprintWaybillButton),TRN,FLAT,LEFT,ICON('prnwayp.jpg')
                           PROMPT('I.M.E.I. Number To Allocate'),AT(20,353),USE(?Prompt9),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(256,345),USE(?buttonAllocate),TRN,FLAT,LEFT,ICON('allocatp.jpg')
                           ENTRY(@s30),AT(124,353,124,10),USE(tmp:Scanned_IMEI),DISABLE,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           COMBO(@s30),AT(72,119,179,10),USE(tmp:SiteLocation),VSCROLL,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Requests_Q)
                         END
                       END
                       BUTTON,AT(608,385),USE(?buttonFinish),TRN,FLAT,LEFT,ICON('finishp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
FinishProcess   PROCEDURE(),BYTE
MarkLocationReceived        PROCEDURE(STRING fSiteLocation)
MoreToProcess       PROCEDURE(STRING fSiteLocation),BYTE
    END
!Save Entry Fields Incase Of Lookup
look:tmp:From_Site_Location                Like(tmp:From_Site_Location)
look:tmp:From_Stock_Type                Like(tmp:From_Stock_Type)
look:tmp:To_Stock_Type                Like(tmp:To_Stock_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Create_Requests_Queue Routine
    ! Fill queue with location requests
    data
tmp:Current_Location string(30)
    code

    free(Requests_Q)

    Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
    exo:Received = 0
    SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
    LOOP UNTIL Access:EXCHORDR.Next()
        IF (exo:Received <> 0)
            BREAK
        END

        IF (tmp:Current_Location = '')
            tmp:Current_Location = exo:Location
        ELSE
            IF (tmp:Current_Location = exo:Location)
                CYCLE
            END
        END

        !TB12448 second part hide franchises - JC - 12/07/12
        access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
        tra_ali:SiteLocation = exo:Location
        if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
            !eh?? - let it show
        ELSE
            if tra_ali:Stop_Account = 'YES' then cycle.
        END



        Requests_Q.Requested_Location = exo:Location
        GET(Requests_Q,Requests_Q.Requested_Location)
        IF (ERROR())
            Add(Requests_Q,Requests_Q.Requested_Location)
        END
    END
!
!    if records(EXCHORDR) = 0 then exit.
!
!    set(exo:By_Location_Key)
!    loop until Access:EXCHORDR.Next()
!
!        if tmp:Current_Location = ''
!            tmp:Current_Location = exo:Location
!        else
!            if tmp:Current_Location = exo:Location then cycle.
!        end
!        Requests_Q.Requested_Location = exo:Location
!        !Only add one occurance of each location
!        Get(Requests_Q,Requested_Location)
!        If error()
!            add(Requests_Q,Requests_Q.Requested_Location)
!        End !If error()
!
!    end

Enable_IMEI_Scan Routine
    ! Disable / Enable IMEI field
    Disable(?tmp:Scanned_IMEI)
    ?tmp:Scanned_IMEI{prop:FillColor} = COLOR:Silver

    if tmp:From_Site_Location = '' then exit.
    if tmp:From_Stock_Type = '' then exit.
    if tmp:To_Site_Location = '' then exit.
    if tmp:To_Stock_Type = '' then exit.

    Enable(?tmp:Scanned_IMEI)
    ?tmp:Scanned_IMEI{prop:FillColor} = COLOR:White
Print_Waybill       Routine ! Finish Off / Create invoices
    data
tmp:Type        string(3)
tmp:WayBillNo   string(30)
tmp:AccountNumber       string(15)
    code

        loop a# = 1 to len(clip(tmp:To_Site_Location))
            if tmp:To_Site_Location[a#] = '<32>' then break.
            tmp:AccountNumber[a#] = tmp:To_Site_Location[a#]
        end

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = tmp:AccountNumber
        if not Access:TRADEACC.Fetch(tra:Account_Number_Key)
            tmp:Type = 'TRA'
        else
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = tmp:AccountNumber
            Access:SUBTRACC.Fetch(sub:Account_Number_Key)
            tmp:Type = 'SUB'
        end

    !In case the tmp:courier was not set up
        if tmp:courier = ''
            Case tmp:Type
            Of 'TRA'
                tmp:Courier = tra:Courier_Incoming
            Of 'SUB'
                tmp:Courier = sub:Courier_Incoming
            End !Case func:Type
        END

        access:courier.clearkey(cou:Courier_Key)
        cou:Courier = tmp:Courier
        if access:courier.fetch(cou:Courier_Key)
        !tmp:WayBillNo = Clip(tmp:AccountNumber) & |
            tmp:WayBillNo = Format(Month(Today()),@n02) & Format(Day(Today()),@n02) & Format(Year(Today()),@n04) & |
                Format(Clock(),@t2)
        ELSE
        !Get the next waybill number - 3384 (DBH: 10-10-2003)
            tmp:WaybillNo = NextWayBillNumber()
        !Do not continue if waybill error - 3432 (DBH: 27-10-2003)
            If tmp:WayBillNo = 0
                Exit
            End !If tmp:WayBillNo = 0
        END !If could find a courier

        ARCAccount" = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))

        Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
        way:WayBillNumber = tmp:WayBillNo
        If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !Found
            way:AccountNumber = ARCAccount"
            way:WaybillID = 100 ! Exchange waybill
            way:FromAccount = ARCAccount"
            way:ToAccount = tmp:AccountNumber
            if Access:WAYBILLS.TryUpdate()
            !Access:WAYBILLS.CancelAutoInc()
            else
            ! Add items onto waybill
            ! Create Retail Sale
                Access:TRADEACC.Clearkey(tra:SiteLocationKey)
                tra:SiteLocation = tmp:To_Site_Location
                IF (Access:TRADEACC.TryFetch(tra:SiteLocationKey))
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('An error occurred retreiving the Trade Account Details.','ServiceBase',|
                        'mstop.jpg','/&OK') 
                    Of 1 ! &OK Button
                    End!Case Message
                    EXIT
                END
                
                IF (Access:RETSALES.PrimeRecord() = Level:Benign)
                    Access:USERS.Clearkey(use:Password_Key)
                    use:Password = glo:Password
                    IF (Access:USERS.TryFetch(use:Password_Key))
                    END
                    ret:Who_Booked = use:User_Code
                    ret:Account_Number = tra:StoresAccount
                    ret:Contact_Name = ''
                    ret:Purchase_Order_Number = ''
                    ret:WebOrderNumber = ''
                    ret:WebDateCreated = ''
                    ret:WebCreatedUser = ''
                    ret:PostCode = tra:Postcode
                    ret:Company_Name = tra:Company_Name
                    ret:Address_Line1 = tra:Address_Line1
                    ret:Address_Line2 = tra:Address_Line2
                    ret:Address_Line3 = tra:Address_Line3
                    ret:Telephone_Number = tra:Telephone_Number
                    ret:Fax_Number = tra:Fax_Number
                    ret:Postcode_Delivery = tra:Postcode
                    ret:Address_Line1_Delivery = tra:Address_Line1
                    ret:Address_LIne2_Delivery = tra:Address_Line2
                    ret:Address_Line3_Delivery = tra:Address_Line3
                    ret:Telephone_Delivery = tra:Telephone_Number
                    ret:Fax_Number_Delivery = tra:Fax_Number
                    ret:ExchangeOrder = 1

                    IF (tra:Invoice_Sub_Accounts = 'YES')
                        If sub:Retail_Payment_Type = 'ACC'
                            ret:payment_method  = 'TRA'
                        End!If SUB:Retail_Payment_Type = 'ACC'
                        IF sub:retail_payment_type  = 'CAS'
                            ret:payment_method = 'CAS'
                        End!IF sub:retail_payment_type  = 'CAS'

                        ret:Courier_Cost = sub:Courier_Cost
                        ret:Courier = sub:Courier_Outgoing
                    ELSE ! IF (tra:Invoice_Sub_Accounts = 'YES')
                        If tra:Retail_Payment_Type = 'ACC'
                            ret:payment_method  = 'TRA'
                        End!If SUB:Retail_Payment_Type = 'ACC'
                        IF tra:retail_payment_type  = 'CAS'
                            ret:payment_method = 'CAS'
                        End!IF sub:retail_payment_type  = 'CAS'

                        ret:Courier_Cost = tra:Courier_Cost
                        ret:Courier = tra:Courier_Outgoing
                    END ! IF (tra:Invoice_Sub_Accounts = 'YES')

                    IF (Access:RETSALES.TryInsert())
                        Access:RETSALES.CancelAutoInc()
                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Missive('An error occurred creating the Retail Sale.','ServiceBase',|
                            'mstop.jpg','/&OK') 
                        Of 1 ! &OK Button
                        End!Case Message
                        EXIT
                            
                    END

                    LOOP i# = 1 TO RECORDS(qFulFilled)
                        GET(qFulFilled,i#)

                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = qFulFilled.ExchangeRefNumber
                        if Access:EXCHANGE.Fetch(xch:Ref_Number_Key)
                            Delete(qFulFilled)
                            CYCLE
                        END

                        if not Access:WAYITEMS.PrimeRecord()
                            wai:WayBillNumber = way:WayBillNumber
                            wai:IsExchange = true
                            wai:Ref_Number = xch:Ref_Number
                            if Access:WAYITEMS.TryUpdate()
                                Access:WAYITEMS.CancelAutoInc()
                                CYCLE
                            end
                        end

                        Access:EXCHORDR.Clearkey(exo:Ref_Number_Key)
                        exo:Ref_Number = qFulFilled.ExchangeOrderRefNumber
                        IF (Access:EXCHORDR.TryFetch(exo:Ref_Number_Key))
                            CYCLE
                        END

                        ! Add Parts. 1 per IMEI
                        Access:STOCK.Clearkey(sto:ExchangeModelKey)
                        sto:Location = MainStoreLocation()
                        sto:Manufacturer = exo:Manufacturer
                        sto:ExchangeModelNumber = exo:Model_Number
                        IF (Access:STOCK.TryFetch(sto:ExchangeModelKey))
                            CYCLE
                        END
                                

                        IF (Access:RETSTOCK.PrimeRecord() = Level:Benign)
                            res:Ref_Number = ret:Ref_Number
                            res:Part_Number = sto:Part_Number
                            res:Description = sto:Description
                            res:Supplier = sto:Supplier
                            res:Purchase_Cost = sto:Purchase_Cost
                            res:Sale_Cost = sto:Sale_Cost
                            res:Retail_Cost = sto:Retail_Cost
                            res:Item_Cost = sto:Sale_Cost
                            res:AccessoryCost = sto:AccessoryCost
                            res:Part_Ref_Number = sto:Ref_Number
                            res:Despatched = 'YES'
                            res:Quantity = 1
                            res:QuantityReceived = 1
                            res:ScannedQty = 0
                            res:Purchase_Order_Number = exo:OrderNumber
                            res:ExchangeRefNumber = xch:Ref_Number
                            IF (Access:RETSTOCK.TryInsert())
                                Access:RETSTOCK.CancelAutoInc()
                                            
                            END
 
                        END ! IF (Access:RETSTOCK.PrimeRecord() = Level:Benign)

                        exo:Received = 1
                        exo:DateReceived = TODAY()
                        exo:ExchangeRefNumber = xch:Ref_Number
                        Access:EXCHORDR.TryUpdate()

                    END ! LOOP i# = 1 TO RECORDS(qFulFilled)

                    ! Create Despatch Note
                    IF (Access:RETDESNO.PrimeRecord() = Level:Benign)
                        rdn:Consignment_Number = ''            
                        rdn:Courier     = ret:Courier
                        rdn:Sale_Number = ret:Ref_Number
                        IF (Access:RETDESNO.TryInsert() = Level:Benign)
                            ret:Despatched = 'PRO'
                            ret:Despatch_Number = rdn:Despatch_Number
                            Access:RETSALES.TryUpdate()
                            glo:ReportCopies = 2
                            Retail_Despatch_Note(ret:Despatch_Number)
                        END ! IF (Access:RETDESNO.TryInsert() = Level:Benign)
                    END ! IF (Access:RETDESNO.PrimeRecord() = Level:Benign)

                    ! Create Invoice
                    SET(DEFAULTS,0)
                    Access:DEFAULTS.Next()

                    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                    sub:Account_Number = ret:Account_Number
                    IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key))
                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Missive('An error occurred retrieving the Trade Account details.','ServiceBase',|
                            'mstop.jpg','/&OK') 
                        Of 1 ! &OK Button
                        End!Case Message        
                        EXIT
                    END ! IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)

                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number = sub:Main_Account_Number
                    IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Missive('An error occurred retrieving the Trade Account details.','ServiceBase',|
                            'mstop.jpg','/&OK') 
                        Of 1 ! &OK Button
                        End!Case Message    
                        EXIT
                    END ! IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
    
                    Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                    IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                        vat:Vat_Code = sub:Retail_Vat_Code
                    ELSE ! IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                        vat:Vat_Code = tra:Retail_Vat_Code
                    END ! IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                    IF (Access:VATCODE.TryFetch(vat:Vat_Code_Key))
                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Missive('An error occurred retrieving the VAT details.','ServiceBase',|
                            'mstop.jpg','/&OK') 
                        Of 1 ! &OK Button
                        End!Case Message
                        EXIT
                    END
        
                    ! Get Parts Cost
                    Access:RETSTOCK.Clearkey(res:Despatched_Only_Key)
                    res:Ref_Number = ret:Ref_Number
                    res:Despatched = 'YES'
                    SET(res:Despatched_Only_Key,res:Despatched_Only_Key)
                    LOOP UNTIL Access:RETSTOCK.Next()
                        IF (res:Ref_Number <> ret:Ref_Number OR |
                            res:Despatched <> 'YES')
                            BREAK
                        END
        
                        ret:Parts_Cost += ROUND(res:Item_Cost * res:Quantity,.01)
                    END

                    ret:Sub_Total = ROUND(ret:Parts_Cost,.01) + ret:Courier_Cost
                    Access:RETSALES.TryUpdate()

                    ! Create Invoice
                    IF (Access:INVOICE.PrimeRecord() = Level:Benign)
                        inv:Invoice_Type = 'RET'
                        inv:Job_Number = ret:Ref_Number
                        inv:Date_Created = TODAY()
                        IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                            inv:AccountType = 'SUB'
                            inv:Invoice_Vat_Number = sub:Vat_Number
                        ELSE
                            inv:AccountType = 'MAI'
                            inv:Invoice_Vat_Number = tra:Vat_Number
                        END
                        inv:Account_Number = ret:Account_Number
                        inv:Total = ret:Sub_Total
                        inv:Vat_Rate_Retail = ROUND(vat:Vat_Rate,.01)
                        inv:Vat_Number = def:Vat_Number
                        inv:Courier_Paid = ret:Courier_Cost
                        inv:Parts_Paid = ret:Parts_Cost
        
                        IF (Access:INVOICE.TryInsert())
                            Access:INVOICE.CancelAutoInc()
                        ELSE
                            ret:Invoice_Number = inv:Invoice_Number
                            ret:Invoice_Date = TODAY()
                            ret:Invoice_Courier_Cost = ret:Courier_Cost
                            ret:Invoice_Parts_Cost = ret:Parts_Cost
                            ret:Invoice_Sub_Total = ret:Sub_Total
                            Access:RETSALES.TryUpdate()

                            ! #12223 Invoice created. Add exchange unit audit history. (Bryan: 29/07/2011)
                            LOOP i# = 1 TO RECORDS(qFulFilled)
                                GET(qFulFilled,i#)

                                ! Lookup the relevate Retail Stock Item
                                Access:RETSTOCK.Clearkey(res:ExchangeRefNumberKey)
                                res:Ref_Number = ret:Ref_Number
                                res:ExchangeRefNumber = qFulFilled.ExchangeRefNumber
                                IF (Access:RETSTOCK.Tryfetch(res:ExchangeRefNumberKey))
                                    CYCLE
                                END

                                IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                                    exh:Ref_Number     = qFulFilled.ExchangeRefNumber
                                    exh:Date        = TODAY()
                                    exh:Time        = CLOCK()
                                    Access:USERS.Clearkey(use:Password_Key)
                                    use:Password = glo:Password
                                    IF (Access:USERS.TryFetch(use:Password_Key))
                                    END
                                    exh:User        = use:User_Code
                                    exh:Status        = 'IN TRANSIT'
                                    exh:Notes        = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                                                        '<13,10>RETAIL INVOICE NUMBER: ' & ret:Invoice_Number & |
                                                        '<13,10>PRICE: ' & Format(res:Item_Cost,@n14.2)

                                    IF (Access:EXCHHIST.TryInsert())
                                        Access:EXCHHIST.CancelAutoInc()
                                    END
                                END ! IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                            END
            
                            Line500_XML('STF')

                            ! #13608 Put the logging in the correct place. (DBH: 28/09/2015)
                            MakeInvoiceLog('STF')

                            Retail_Single_Invoice(0,inv:Invoice_Number)
                        END ! IF (Access:INVOICE.TryInsert())
                    END ! IF (Access:INVOICE.PrimeRecord() = Level:Benign)
                    

                END ! IF (Access:RETSALES.PrimeRecord() = Level:Benign)
            END
        END ! If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        

        WayBillExchange(ARCAccount",'TRA',|
            tmp:AccountNumber,tmp:Type,|
            tmp:WayBillNo,tmp:Courier,0)

    FREE(qFulFilled)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020279'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Rapid_Exchange_Transfer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonHelp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:EXCHORDR.Open
  Relate:RETDESNO.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WAYBILLS.Open
  Relate:WAYITEMS.Open
  Access:LOCATION.UseFile
  Access:STOCKTYP.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:RETSALES.UseFile
  Access:RETSTOCK.UseFile
  Access:EXCHHIST.UseFile
  Access:STOCK.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:EXCHORDR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !fetch default courier
  tmp:courier = getini('DESPATCH','RETcourier','',clip(path())&'\SB2KDEF.INI')
  if tmp:courier = '' then
      Case Missive('A Default Courier has not been setup for Exchange Transfers.'&|
        '<13,10>'&|
        '<13,10>Use the menu option to setup one up.','ServiceBase 3g',|
                     'mstop.jpg','/OK')
          Of 1 ! OK Button
      End ! Case Missive
  END
  do Create_Requests_Queue    ! Fill queue with location requests
  
  Access:LOCATION.ClearKey(loc:Main_Store_Key)
  loc:Main_Store = 'YES'
  set(loc:Main_Store_Key,loc:Main_Store_Key)
  loop until Access:LOCATION.Next()
      if loc:Main_Store <> 'YES' then break.
      tmp:From_Site_Location = loc:Location
      break
  end
  ! Save Window Name
   AddToLog('Window','Open','Rapid_Exchange_Transfer')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?List{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,exo:LocationReceivedKey)
  BRW7.AddRange(exo:Location)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,exo:Manufacturer,1,BRW7)
  BIND('locPartNumber',locPartNumber)
  BIND('tmp:Notes',tmp:Notes)
  BIND('tmp:To_Site_Location',tmp:To_Site_Location)
  ?List{PROP:IconList,1} = '~bluetick.ico'
  BRW7.AddField(exo:Manufacturer,BRW7.Q.exo:Manufacturer)
  BRW7.AddField(exo:Model_Number,BRW7.Q.exo:Model_Number)
  BRW7.AddField(exo:OrderNumber,BRW7.Q.exo:OrderNumber)
  BRW7.AddField(locPartNumber,BRW7.Q.locPartNumber)
  BRW7.AddField(tmp:Notes,BRW7.Q.tmp:Notes)
  BRW7.AddField(exo:DateCreated,BRW7.Q.exo:DateCreated)
  BRW7.AddField(exo:TimeCreated,BRW7.Q.exo:TimeCreated)
  BRW7.AddField(exo:Qty_Required,BRW7.Q.exo:Qty_Required)
  BRW7.AddField(exo:Qty_Received,BRW7.Q.exo:Qty_Received)
  BRW7.AddField(exo:Notes,BRW7.Q.exo:Notes)
  BRW7.AddField(exo:Ref_Number,BRW7.Q.exo:Ref_Number)
  BRW7.AddField(exo:Received,BRW7.Q.exo:Received)
  BRW7.AddField(exo:Location,BRW7.Q.exo:Location)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:EXCHORDR.Close
    Relate:RETDESNO.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WAYBILLS.Close
    Relate:WAYITEMS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Rapid_Exchange_Transfer')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      Select_Stock_Type
      Select_Stock_Type
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:From_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
    OF ?From_Stock_Type_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
    OF ?tmp:To_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
    OF ?To_Stock_Type_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
      glo:select1    = 'EXC'
      stp:use_exchange = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
    OF ?buttonAllocate
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonAllocate, Accepted)
      if ?tmp:Scanned_IMEI{prop:Disable} = False
          select(?tmp:Scanned_IMEI)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonAllocate, Accepted)
    OF ?buttonFinish
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
      IF (Records(qFulFilled))
          IF (FinishProcess() = 1)
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OptionsSetCourier
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OptionsSetCourier, Accepted)
      globalrequest = selectrecord
      browse_Courier
      tmp:courier = cou:Courier
      putiniEXT('DESPATCH','RETcourier',tmp:courier,clip(path())&'/SB2KDEF.INI')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OptionsSetCourier, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020279'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020279'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020279'&'0')
      ***
    OF ?tmp:From_Site_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Site_Location, Accepted)
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:From_Site_Location OR ?tmp:From_Site_Location{Prop:Req}
        loc:Location = tmp:From_Site_Location
        !Save Lookup Field Incase Of error
        look:tmp:From_Site_Location        = tmp:From_Site_Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:From_Site_Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:From_Site_Location = look:tmp:From_Site_Location
            SELECT(?tmp:From_Site_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Site_Location, Accepted)
    OF ?From_Location_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSiteLocations
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Location_Button, Accepted)
      case GlobalResponse
          of RequestCompleted
              tmp:From_Site_Location = loc:Location
              display(?tmp:From_Site_Location)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Location_Button, Accepted)
    OF ?tmp:From_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
      glo:select1    = ''
      
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:From_Stock_Type OR ?tmp:From_Stock_Type{Prop:Req}
        stp:Stock_Type = tmp:From_Stock_Type
        !Save Lookup Field Incase Of error
        look:tmp:From_Stock_Type        = tmp:From_Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:From_Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:From_Stock_Type = look:tmp:From_Stock_Type
            SELECT(?tmp:From_Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
    OF ?From_Stock_Type_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Stock_Type
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
      glo:select1    = ''
      
      case GlobalResponse
          of RequestCompleted
              tmp:From_Stock_Type = stp:Stock_Type
              display(?tmp:From_Stock_Type)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
    OF ?tmp:To_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
      glo:select1    = ''
      
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:To_Stock_Type OR ?tmp:To_Stock_Type{Prop:Req}
        stp:Stock_Type = tmp:To_Stock_Type
        !Save Lookup Field Incase Of error
        look:tmp:To_Stock_Type        = tmp:To_Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:To_Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:To_Stock_Type = look:tmp:To_Stock_Type
            SELECT(?tmp:To_Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
    OF ?To_Stock_Type_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Stock_Type
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
      glo:select1    = ''
      
      case GlobalResponse
          of RequestCompleted
              tmp:To_Stock_Type = stp:Stock_Type
              display(?tmp:To_Stock_Type)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
    OF ?ViewNotes
      ThisWindow.Update
      ExchangeOrderNotes
      ThisWindow.Reset
    OF ?ReprintWaybillButton
      ThisWindow.Update
      AskWaybillNumber
      ThisWindow.Reset
    OF ?tmp:Scanned_IMEI
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Scanned_IMEI, Accepted)
          ! Process IMEI Number
          BRW7.UpdateViewRecord()
      
          IF (exo:Qty_Required = exo:Qty_Received)
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Missive('The reqested quantity of units has been fulfilled for this order.','ServiceBase',|
                  'midea.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message     
              tmp:Scanned_IMEI = ''
              DISPLAY()
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
              
          END
      
          Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
          xch:ESN = tmp:Scanned_IMEI
          IF (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('I.M.E.I. Number not found.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END
      
          IF (xch:Available <> 'AVL') OR (xch:Location <> tmp:From_Site_Location)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected I.M.E.I. number is not available.','ServiceBase',|
                  'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END
      
          IF (exo:Model_Number <> xch:Model_Number) OR (tmp:From_Stock_Type <> xch:Stock_Type)
              Case Missive('The I.M.E.I. Number scanned is a ' & Clip(xch:Model_Number) & ' from the ' & Clip(xch:Stock_Type) & ' Stock Type.'&|
                  '<13,10>The model requested  is a ' & Clip(exo:Model_Number) & '. Do you wish to IGNORE the mismatch, or SCAN a different unit?','ServiceBase 3g',|
                  'mquest.jpg','\Ignore|Scan Again')
              Of 2 ! Scan Again Button
                  SELECT(?tmp:Scanned_IMEI)
                  CYCLE
              Of 1 ! Ignore Button
       
              End ! Case Missive
          END
      
      ! Move exchange to new location
          xch:Location = tmp:To_Site_Location
          xch:Available = 'ITM'
          xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
      
          qFulfilled.ExchangeRefNumber = xch:Ref_Number
          qFulFilled.ExchangeOrderRefNumber = exo:Ref_Number
          ADD(qFulFilled)
      
          IF (Access:EXCHANGE.TryUpdate() = Level:Benign)
              ! Add Exchange History
              IF (exo:OrderNumber = 0)
                  ! Old exchange method, write the exchange audit here. If new, do it when waybill created
                  IF (Access:EXCHHIST.PrimeRecord() = Level:Benign)
                      exh:Ref_Number  = xch:Ref_Number
                      exh:Date        = TODAY()
                      exh:Time        = CLOCK()
                      Access:USERS.Clearkey(use:Password_Key)
                      use:Password = glo:Password
                      IF (Access:USERS.TryFetch(use:Password_Key))
                      END
                      exh:User        = use:User_Code
                      exh:Status  = 'TRANSFERRED FROM ' & CLIP(tmp:From_Site_Location)
                      IF (Access:EXCHHIST.TryInsert())
                          Access:EXCHHIST.CancelAutoInc()
                      END
                  END
              END
              moveDown# = 0
              exo:Qty_Received += 1
              IF (Access:EXCHORDR.TryUpdate() = Level:Benign)
                  BRW7.ResetSort(1)
      
                  IF (exo:Qty_Required = exo:Qty_Received)
                      moveDown# = 1
                  END
                  
                  IF (MoreToProcess(tmp:To_Site_Location))
                      IF (moveDown# = 1)
                          SELECT(?List,CHOICE(?List) + 1)
                      END 
                  ELSE
                      Beep(Beep:SystemAsterisk)  ;  Yield()
                      Case Missive('The requested quantity of unit(s) has been fulfilled.','ServiceBase',|
                          'midea.jpg','/&OK') 
                      Of 1 ! &OK Button
                      End!Case Message
                      
                      ! Print Waybill
                      DO Print_Waybill
                      
                      FREE(qFulFilled)
                      
                      MarkLocationReceived(tmp:To_Site_Location)
                      
                      BRW7.ResetSort(1)
                      
                      DO Create_Requests_Queue
                  END
                  
                      
                      
                          
                  
              END
              
          END
          tmp:Scanned_IMEI = ''
          DISPLAY()
          SELECT(?tmp:Scanned_IMEI)
      
      
              
              
          
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Scanned_IMEI, Accepted)
    OF ?tmp:SiteLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
      if records(qFulFilled)
          tmp:Original_Location = tmp:To_Site_Location

!          GET(Requests_Q,CHOICE(?List1))
          if tmp:Original_Location <> tmp:SiteLocation
              IF (FinishProcess() = 1)
                  tmp:SiteLocation = tmp:Original_Location
      !            GET(Requests_Q,tmp:ListPos)
      !            Select(?List1,tmp:ListPos)
                  Select(tmp:SiteLocation)
                  cycle
              end
          end

      end

      !tmp:ListPos = CHOICE(?List1)
      !GET(Requests_Q,CHOICE(?List1))
      tmp:To_Site_Location = tmp:SiteLocation
      display(?tmp:To_Site_Location)
      do Enable_IMEI_Scan
      !BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
    OF ?buttonFinish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?buttonFinish, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:SiteLocation
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      if keycode() = EscKey
          cycle
      end
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
FinishProcess       PROCEDURE()
rtnValue                BYTE(0)
locMoreToProcess        Byte(0)
gLater                  GROUP,PRE()
locLaterQty                 LIKE(exo:Qty_Required)
locLaterManufacturer        LIKE(exo:Manufacturer)
locLaterModelNumber         LIKE(exo:Model_Number)
locLaterNotes               LIKE(exo:Notes)
locLaterOrderNumber         LIKE(exo:OrderNumber)
                        END
    CODE
        IF (MoreToProcess(tmp:To_Site_Location))
            Case Missive('Not all requests from ' & Clip(tmp:To_Site_Location) & ' have been fulfilled.'&|
                '<13,10>'&|
                '<13,10>Do you wish to go back and ALLOCATE the shortage now, allocate this shortage LATER, or DELETE the shortage?','ServiceBase 3g',|
                'mquest.jpg','\Delete|Later|Allocate')
            Of 3 ! Allocate Button
                rtnValue = 1
            OF 2 ! Later Button
                rtnValue = 2

                locLaterQty = 0
                Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
                exo:Received = 0
                exo:Location = tmp:To_Site_Location
                SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
                LOOP UNTIL Access:EXCHORDR.Next()
                    IF (exo:Received <> 0 OR |
                        exo:Location <> tmp:To_Site_Location)
                        BREAK
                    END

                    IF (exo:OrderNumber > 0)
                        IF (exo:Qty_Received > 0 AND (exo:Qty_Received < exo:Qty_Required))
                            ! Make the "received" as Received. ADd new line for "later"
                            CLEAR(gLater)
                            locLaterQty = exo:Qty_Required - exo:Qty_Received
                            locLaterManufacturer = exo:Manufacturer
                            locLaterModelNumber = exo:Model_Number
                            locLaterNotes = exo:Notes
                            locLaterOrderNumber = exo:OrderNumber

                            exo:Qty_Required = exo:Qty_Received
                            exo:Received = 1
                            exo:DateReceived = TODAY()
                            Access:EXCHORDR.TryUpdate()

                            Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
                            exo:Received = 0
                            exo:Location = tmp:To_Site_Location
                            SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
                        END
                    ELSE
                        IF (exo:Qty_Received < exo:Qty_Required)
                            IF (exo:Qty_Received > 0)
                                exo:Qty_Required = exo:Qty_Required - exo:Qty_Received
                                exo:Qty_Received = 0
                                Access:EXCHORDR.TryUpdate()

                                Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
                                exo:Received = 0
                                exo:Location = tmp:To_Site_Location
                                SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
                            END
                        ELSE
                            Relate:EXCHORDR.Delete(0)

                            Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
                            exo:Received = 0
                            exo:Location = tmp:To_Site_Location
                            SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
                        END
                    END

                    IF (locLaterQty > 0)
                        ! Add a new entry for the "later"
                        IF (Access:EXCHORDR.PrimeRecord() = Level:Benign)
                            exo:Location        = tmp:To_Site_Location
                            exo:Manufacturer    = locLaterManufacturer
                            exo:Model_Number    = locLaterModelNumber
                            exo:Qty_Required    = locLaterQty
                            exo:Notes           = locLaterNotes
                            exo:OrderNumber     = locLaterOrderNumber
                            IF (Access:EXCHORDR.TryInsert())
                                Access:EXCHORDR.CancelAutoInc()
                            END
                        END
                        Clear(gLater)
                    END
                    


                END
                
                ! Print Waybill
                DO Print_WayBill
                FREE(glo:Q_JobNumber)
                
                BRW7.ResetSort(1)
            OF 1 ! Delete Button
                rtnValue = 3
                
                ! Remove Location Request
                Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
                exo:Received = 0
                exo:Location = tmp:To_Site_Location
                SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
                LOOP UNTIL Access:EXCHORDR.Next()
                    IF (exo:Received <> 0 OR |
                        exo:Location <> tmp:To_Site_Location)
                        BREAK
                    END
                    
                    Relate:EXCHORDR.Delete(0)
                            
                    Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
                    exo:Received = 0
                    exo:Location = tmp:To_Site_Location
                    SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
                END
                
                ! Print Waybill
                DO Print_Waybill
                FREE(glo:Q_JobNumber)
                
                DO Create_Requests_Queue
                
                BRW7.ResetSort(1)
                    
            END
        END

    RETURN rtnValue
            
                    
MarkLocationReceived        PROCEDURE(STRING fSiteLocation)
    CODE
        Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
        exo:Received = 0
        exo:Location = fSiteLocation
        SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
        LOOP UNTIL Access:EXCHORDR.Next()
            IF (exo:Received <> 0 OR |
                exo:Location <> fSiteLocation)
                BREAK
            END
            
            IF (exo:OrderNumber > 0)
                exo:Received = 1
                exo:DateReceived = TODAY()
                Access:EXCHORDR.TryUpdate()
            ELSE
                Relate:EXCHORDR.Delete(0)
            END
            Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
            exo:Received = 0
            exo:Location = fSiteLocation
        END
MoreToProcess       PROCEDURE(STRING fSiteLocation)
rtnValue                BYTE(0)

    CODE
        Access:EXCHORDR.Clearkey(exo:LocationReceivedKey)
        exo:Received = 0
        exo:Location = fSiteLocation
        SET(exo:LocationReceivedKey,exo:LocationReceivedKey)
        LOOP UNTIL Access:EXCHORDR.Next()
            IF (exo:Received <> 0 OR |
                exo:Location <> fSiteLocation)
                BREAK
            END
            IF (exo:Qty_Received < exo:Qty_Required)
                rtnValue = 1
                BREAK
            END
        END
            
        RETURN rtnValue
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW7.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 0
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:SiteLocation
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(7, SetQueueRecord, ())
  ! #12127 Lookup stock "exchange" part number (Bryan: 02/07/2011)
  locPartNumber = ''
  IF (exo:OrderNumber > 0 AND tmp:From_Site_Location = MainStoreLocation())
      Access:STOCK.Clearkey(sto:ExchangeModelKey )
      sto:Location = tmp:From_Site_Location
      sto:Manufacturer = exo:Manufacturer
      sto:ExchangeModelNumber = exo:Model_Number
      IF (Access:STOCK.Tryfetch(sto:ExchangeMOdelKey) = Level:Benign)
          locPartNumber = sto:Part_Number
      END
  END
  PARENT.SetQueueRecord
  IF (exo:Qty_Required = exo:Qty_Received)
    SELF.Q.exo:Manufacturer_NormalFG = 255
    SELF.Q.exo:Manufacturer_NormalBG = -1
    SELF.Q.exo:Manufacturer_SelectedFG = 16777215
    SELF.Q.exo:Manufacturer_SelectedBG = -1
  ELSE
    SELF.Q.exo:Manufacturer_NormalFG = -1
    SELF.Q.exo:Manufacturer_NormalBG = -1
    SELF.Q.exo:Manufacturer_SelectedFG = -1
    SELF.Q.exo:Manufacturer_SelectedBG = -1
  END
  IF (exo:Qty_Required = exo:Qty_Received)
    SELF.Q.exo:Model_Number_NormalFG = 255
    SELF.Q.exo:Model_Number_NormalBG = -1
    SELF.Q.exo:Model_Number_SelectedFG = 16777215
    SELF.Q.exo:Model_Number_SelectedBG = -1
  ELSE
    SELF.Q.exo:Model_Number_NormalFG = -1
    SELF.Q.exo:Model_Number_NormalBG = -1
    SELF.Q.exo:Model_Number_SelectedFG = -1
    SELF.Q.exo:Model_Number_SelectedBG = -1
  END
  SELF.Q.exo:OrderNumber_NormalFG = -1
  SELF.Q.exo:OrderNumber_NormalBG = -1
  SELF.Q.exo:OrderNumber_SelectedFG = -1
  SELF.Q.exo:OrderNumber_SelectedBG = -1
  SELF.Q.locPartNumber_NormalFG = -1
  SELF.Q.locPartNumber_NormalBG = -1
  SELF.Q.locPartNumber_SelectedFG = -1
  SELF.Q.locPartNumber_SelectedBG = -1
  SELF.Q.tmp:Notes_NormalFG = -1
  SELF.Q.tmp:Notes_NormalBG = -1
  SELF.Q.tmp:Notes_SelectedFG = -1
  SELF.Q.tmp:Notes_SelectedBG = -1
  IF (exo:Notes <> '')
    SELF.Q.tmp:Notes_Icon = 1
  ELSE
    SELF.Q.tmp:Notes_Icon = 0
  END
  SELF.Q.exo:DateCreated_NormalFG = -1
  SELF.Q.exo:DateCreated_NormalBG = -1
  SELF.Q.exo:DateCreated_SelectedFG = -1
  SELF.Q.exo:DateCreated_SelectedBG = -1
  SELF.Q.exo:TimeCreated_NormalFG = -1
  SELF.Q.exo:TimeCreated_NormalBG = -1
  SELF.Q.exo:TimeCreated_SelectedFG = -1
  SELF.Q.exo:TimeCreated_SelectedBG = -1
  IF (exo:Qty_Required = exo:Qty_Received)
    SELF.Q.exo:Qty_Required_NormalFG = 255
    SELF.Q.exo:Qty_Required_NormalBG = -1
    SELF.Q.exo:Qty_Required_SelectedFG = 16777215
    SELF.Q.exo:Qty_Required_SelectedBG = -1
  ELSE
    SELF.Q.exo:Qty_Required_NormalFG = -1
    SELF.Q.exo:Qty_Required_NormalBG = -1
    SELF.Q.exo:Qty_Required_SelectedFG = -1
    SELF.Q.exo:Qty_Required_SelectedBG = -1
  END
  IF (exo:Qty_Required = exo:Qty_Received)
    SELF.Q.exo:Qty_Received_NormalFG = 255
    SELF.Q.exo:Qty_Received_NormalBG = -1
    SELF.Q.exo:Qty_Received_SelectedFG = 16777215
    SELF.Q.exo:Qty_Received_SelectedBG = -1
  ELSE
    SELF.Q.exo:Qty_Received_NormalFG = -1
    SELF.Q.exo:Qty_Received_NormalBG = -1
    SELF.Q.exo:Qty_Received_SelectedFG = -1
    SELF.Q.exo:Qty_Received_SelectedBG = -1
  END
  SELF.Q.exo:Notes_NormalFG = -1
  SELF.Q.exo:Notes_NormalBG = -1
  SELF.Q.exo:Notes_SelectedFG = -1
  SELF.Q.exo:Notes_SelectedBG = -1
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(7, SetQueueRecord, ())


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
Rapid_Loan_Transfer PROCEDURE                         !Generated from procedure template - Window

Requests_Q           QUEUE,PRE()
Requested_Location   STRING(30)
                     END
qFulFilled           QUEUE,PRE()
LoanRefNumber        LONG
LoanOrderRefNumber   LONG
                     END
tmp:Scanned_IMEI     STRING(30)
tmp:From_Site_Location STRING(30)
tmp:From_Stock_Type  STRING(30)
tmp:To_Site_Location STRING(30)
tmp:To_Stock_Type    STRING(30)
tmp:Response         BYTE
tmp:Original_Location STRING(30)
tmp:ListPos          LONG
tmp:Notes            STRING(1)
tmp:SiteLocation     STRING(30)
BRW7::View:Browse    VIEW(LOAORDR)
                       PROJECT(lor:Manufacturer)
                       PROJECT(lor:Model_Number)
                       PROJECT(lor:Qty_Required)
                       PROJECT(lor:OrderNumber)
                       PROJECT(lor:Qty_Received)
                       PROJECT(lor:Notes)
                       PROJECT(lor:Ref_Number)
                       PROJECT(lor:Received)
                       PROJECT(lor:Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
lor:Manufacturer       LIKE(lor:Manufacturer)         !List box control field - type derived from field
lor:Manufacturer_NormalFG LONG                        !Normal forground color
lor:Manufacturer_NormalBG LONG                        !Normal background color
lor:Manufacturer_SelectedFG LONG                      !Selected forground color
lor:Manufacturer_SelectedBG LONG                      !Selected background color
lor:Model_Number       LIKE(lor:Model_Number)         !List box control field - type derived from field
lor:Model_Number_NormalFG LONG                        !Normal forground color
lor:Model_Number_NormalBG LONG                        !Normal background color
lor:Model_Number_SelectedFG LONG                      !Selected forground color
lor:Model_Number_SelectedBG LONG                      !Selected background color
tmp:Notes              LIKE(tmp:Notes)                !List box control field - type derived from local data
tmp:Notes_NormalFG     LONG                           !Normal forground color
tmp:Notes_NormalBG     LONG                           !Normal background color
tmp:Notes_SelectedFG   LONG                           !Selected forground color
tmp:Notes_SelectedBG   LONG                           !Selected background color
tmp:Notes_Icon         LONG                           !Entry's icon ID
lor:Qty_Required       LIKE(lor:Qty_Required)         !List box control field - type derived from field
lor:Qty_Required_NormalFG LONG                        !Normal forground color
lor:Qty_Required_NormalBG LONG                        !Normal background color
lor:Qty_Required_SelectedFG LONG                      !Selected forground color
lor:Qty_Required_SelectedBG LONG                      !Selected background color
lor:OrderNumber        LIKE(lor:OrderNumber)          !List box control field - type derived from field
lor:OrderNumber_NormalFG LONG                         !Normal forground color
lor:OrderNumber_NormalBG LONG                         !Normal background color
lor:OrderNumber_SelectedFG LONG                       !Selected forground color
lor:OrderNumber_SelectedBG LONG                       !Selected background color
lor:Qty_Received       LIKE(lor:Qty_Received)         !List box control field - type derived from field
lor:Qty_Received_NormalFG LONG                        !Normal forground color
lor:Qty_Received_NormalBG LONG                        !Normal background color
lor:Qty_Received_SelectedFG LONG                      !Selected forground color
lor:Qty_Received_SelectedBG LONG                      !Selected background color
lor:Notes              LIKE(lor:Notes)                !List box control field - type derived from field
lor:Notes_NormalFG     LONG                           !Normal forground color
lor:Notes_NormalBG     LONG                           !Normal background color
lor:Notes_SelectedFG   LONG                           !Selected forground color
lor:Notes_SelectedBG   LONG                           !Selected background color
lor:Ref_Number         LIKE(lor:Ref_Number)           !Primary key field - type derived from field
lor:Received           LIKE(lor:Received)             !Browse key field - type derived from field
lor:Location           LIKE(lor:Location)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::lor:Location        LIKE(lor:Location)
HK10::lor:Received        LIKE(lor:Received)
! ---------------------------------------- Higher Keys --------------------------------------- !
window               WINDOW('Rapid Loan Transfer'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,114,552,250),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Requests'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('N - Notes'),AT(472,130),USE(?Prompt5),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(548,252),USE(?ReprintWaybillButton),TRN,FLAT,LEFT,ICON('prnwayp.jpg')
                           LIST,AT(72,144,468,182),USE(?List),IMM,VSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('114L(2)|M*~Manufacturer~@s30@107L(2)|M*~Model Number~@s30@11C|*J~N~@s1@34L(2)|M*' &|
   '~Quantity~@s8@56R(2)|*~Order Number~@n_8@34L(2)|M*~Fulfilled~@s8@0L(2)|M*~Notes~' &|
   '@s255@'),FROM(Queue:Browse)
                           BUTTON,AT(352,332),USE(?Allocate_Button),TRN,FLAT,LEFT,ICON('allocatp.jpg')
                           ENTRY(@s30),AT(220,340,124,10),USE(tmp:Scanned_IMEI),DISABLE,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('I.M.E.I. Number To Allocate'),AT(104,340),USE(?Prompt9),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Site Location'),AT(72,130),USE(?Prompt7),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(132,130,160,10),USE(tmp:SiteLocation),VSCROLL,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Requests_Q)
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Process Loan Sales'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(64,54,272,56),USE(?Sheet4),COLOR(0D6E7EFH),SPREAD
                         TAB('FROM'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(68,72),USE(?tmp:From_Site_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(120,72,124,10),USE(tmp:From_Site_Location),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(248,68),USE(?From_Location_Button),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Stock Type'),AT(68,92),USE(?tmp:From_Stock_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(120,92,124,10),USE(tmp:From_Stock_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(248,88),USE(?From_Stock_Type_Button),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       SHEET,AT(340,54,276,56),USE(?Sheet3),COLOR(0D6E7EFH),SPREAD
                         TAB('TO'),USE(?Tab5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(344,72),USE(?tmp:To_Site_Location:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,72,124,10),USE(tmp:To_Site_Location),SKIP,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Stock Type'),AT(344,92),USE(?tmp:To_Stock_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(396,92,124,10),USE(tmp:To_Stock_Type),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(524,88),USE(?To_Stock_Type_Button),TRN,FLAT,ICON('lookupp.jpg')
                         END
                       END
                       BUTTON,AT(544,194),USE(?ViewNotes),TRN,FLAT,LEFT,ICON('viewnotp.jpg')
                       BUTTON,AT(548,368),USE(?OkButton),TRN,FLAT,LEFT,ICON('finishp.jpg'),DEFAULT
                       BUTTON,AT(4,152),USE(?CancelButton),TRN,FLAT,HIDE,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
    MAP
FinishProcess   PROCEDURE(),BYTE
MarkLocationReceived        PROCEDURE(STRING fSiteLocation)
MoreToProcess       PROCEDURE(STRING fSiteLocation),BYTE
    END
!Save Entry Fields Incase Of Lookup
look:tmp:From_Site_Location                Like(tmp:From_Site_Location)
look:tmp:From_Stock_Type                Like(tmp:From_Stock_Type)
look:tmp:To_Stock_Type                Like(tmp:To_Stock_Type)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
Create_Requests_Queue Routine
    ! Fill queue with location requests
    data
tmp:Current_Location string(30)
    code

    free(Requests_Q)

    if records(LOAORDR) = 0 then exit.

    set(lor:By_Location_Key)
    loop until Access:LOAORDR.Next()
        if tmp:Current_Location = ''
            tmp:Current_Location = lor:Location
        else
            if tmp:Current_Location = lor:Location then cycle.
        end

        !TB12448 second part hide franchises - JC - 12/07/12
        access:Tradeacc_Alias.clearkey(tra_ali:SiteLocationKey)
        tra_ali:SiteLocation = lor:Location
        if access:Tradeacc_Alias.fetch(tra_ali:SiteLocationKey)
            !eh?? - let it show
        ELSE
            if tra_ali:Stop_Account = 'YES' then cycle.
        END
        !TB12448 second part hide franchises - JC - 12/07/12


        Requests_Q.Requested_Location = lor:Location
        Get(Requests_Q,Requested_Location)
        If Error()
            add(Requests_Q,Requests_Q.Requested_Location)
        End !If Error()
    end

Enable_IMEI_Scan Routine
    ! Disable / Enable IMEI field
    Disable(?tmp:Scanned_IMEI)
    ?tmp:Scanned_IMEI{prop:FillColor} = COLOR:Silver

    if tmp:From_Site_Location = '' then exit.
    if tmp:From_Stock_Type = '' then exit.
    if tmp:To_Site_Location = '' then exit.
    if tmp:To_Stock_Type = '' then exit.

    Enable(?tmp:Scanned_IMEI)
    ?tmp:Scanned_IMEI{prop:FillColor} = COLOR:White
End_Of_Process Routine

    tmp:Response = 0

    ! Check if all exchange units have been processed for this location
    More_To_Process# = 0

    Access:LOAORDR.ClearKey(lor:By_Location_Key)
    lor:Location = tmp:To_Site_Location
    set(lor:By_Location_Key,lor:By_Location_Key)
    loop until Access:LOAORDR.Next()
        if lor:Location <> tmp:To_Site_Location then break.
        if lor:Qty_Received < lor:Qty_Required
            More_To_Process# = 1
            break
        end
    end

    if More_To_Process# = 1
        Case Missive('All requests from ' & Clip(tmp:To_Site_Location) & ' have not been fulfilled.'&|
          '<13,10>'&|
          '<13,10>Do you wish to go back and ALLOCATE the shortage now, allocate the shortage LATER, or DELETE this shortage?','ServiceBase 3g',|
                       'mquest.jpg','\Delete|Later|Allocate')
            Of 3 ! Allocate Button
                tmp:Response = 1
            Of 2 ! Later Button
                tmp:Response = 2

                ! Remove location requests
                Access:LOAORDR.ClearKey(lor:By_Location_Key)
                lor:Location = tmp:To_Site_Location
                set(lor:By_Location_Key,lor:By_Location_Key)
                loop until Access:LOAORDR.Next()
                    if lor:Location <> tmp:To_Site_Location then break.
                    if lor:Qty_Received < lor:Qty_Required then
                        if lor:Qty_Received <> 0
                            lor:Qty_Required = lor:Qty_Required - lor:Qty_Received
                            lor:Qty_Received = 0
                            Access:LOAORDR.TryUpdate()
                        end
                    else
                        Relate:LOAORDR.Delete(0)
                        Access:LOAORDR.ClearKey(lor:By_Location_Key)               ! Reset the key
                        lor:Location = tmp:To_Site_Location
                        set(lor:By_Location_Key,lor:By_Location_Key)
                    end
                end

                ! Print WayBill
                do Print_Waybill
                free(glo:q_JobNumber)

                BRW7.ResetFromView()
                BRW7.ResetSort(1)

            Of 1 ! Delete Button
                tmp:Response = 3

                ! Remove location requests
                Access:LOAORDR.ClearKey(lor:By_Location_Key)
                lor:Location = tmp:To_Site_Location
                set(lor:By_Location_Key,lor:By_Location_Key)
                loop until Access:LOAORDR.Next()
                    if lor:Location <> tmp:To_Site_Location then break.
                    Relate:LOAORDR.Delete(0)
                    Access:LOAORDR.ClearKey(lor:By_Location_Key)               ! Reset the key
                    lor:Location = tmp:To_Site_Location
                    set(lor:By_Location_Key,lor:By_Location_Key)
                end

                ! Print WayBill
                do Print_Waybill
                free(glo:q_JobNumber)

                do Create_Requests_Queue                                        ! Reload requests queue

                BRW7.ResetFromView()
                BRW7.ResetSort(1)
        End ! Case Missive
    end
Print_Waybill Routine
    data
tmp:Type            string(3)
tmp:WayBillNo       string(30)
tmp:Courier         string(30)
tmp:AccountNumber   string(15)
    code

    loop a# = 1 to len(clip(tmp:To_Site_Location))
        if tmp:To_Site_Location[a#] = '<32>' then break.
        tmp:AccountNumber[a#] = tmp:To_Site_Location[a#]
    end

    Access:TRADEACC.ClearKey(tra:Account_Number_Key)
    tra:Account_Number = tmp:AccountNumber
    if not Access:TRADEACC.Fetch(tra:Account_Number_Key)
        tmp:Type = 'TRA'
    else
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = tmp:AccountNumber
        Access:SUBTRACC.Fetch(sub:Account_Number_Key)
        tmp:Type = 'SUB'
    end

    Case tmp:Type
        Of 'TRA'
            tmp:Courier = tra:Courier_Incoming
        Of 'SUB'
            tmp:Courier = sub:Courier_Incoming
    End !Case func:Type

    access:courier.clearkey(cou:Courier_Key)
    cou:Courier = tmp:Courier
    if access:courier.fetch(cou:Courier_Key)
        !tmp:WayBillNo = Clip(tmp:AccountNumber) & |
        tmp:WayBillNo = Format(Month(Today()),@n02) & Format(Day(Today()),@n02) & Format(Year(Today()),@n04) & |
                        Format(Clock(),@t2)
    ELSE
        !Get next waybill number - 3384 (DBH: 10-10-2003)
        tmp:WayBillNo = NextWayBillNumber()
        !Do not continue if waybill error - 3432 (DBH: 27-10-2003)
        If tmp:WayBillNo = 0
            Exit
        End !If tmp:WayBillNo = 0
    END !If could find a courier

    ARCAccount" = Clip(GETINI('BOOKING','HeadAccount',,CLIP(PATH())&'\SB2KDEF.INI'))

    Access:WAYBILLS.ClearKey(way:WayBillNumberKey)
    way:WayBillNumber = tmp:WaybillNo
    If Access:WAYBILLS.TryFetch(way:WayBillNumberKey) = Level:Benign
        !Found
        way:AccountNumber = ARCAccount"
        way:WaybillID = 101 ! Loan waybill
        way:FromAccount = ARCAccount"
        way:ToAccount = tmp:AccountNumber
        If Access:WAYBILLS.Update() = Level:Benign
            ! Add items onto waybill

            !TB13370 - J - 15/04/15 - adding a waybillconf record
            Access:WaybConf.primerecord()
            WAC:AccountNumber    = way:AccountNumber
            WAC:WaybillNo        = way:WayBillNumber
            WAC:GenerateDate     = today()
            WAC:GenerateTime     = clock()
            WAC:ConfirmationSent = 0
            Access:Waybconf.update()


            ! Create Retail Sale
            Access:TRADEACC.Clearkey(tra:SiteLocationKey)
            tra:SiteLocation = tmp:To_Site_Location
            IF (Access:TRADEACC.Tryfetch(tra:SiteLocationKey))
                Beep(Beep:SystemHand)  ;  Yield()
                Case Missive('An error occurred retreiving the Trade Account Details.','ServiceBase',|
                    'mstop.jpg','/&OK')
                Of 1 ! &OK Button
                End!Case Message
            END

            IF (Access:RETSALES.PrimeRecord() = Level:Benign)
                ret:Who_Booked = glo:Usercode
                ret:Account_Number = tra:StoresAccount
                ret:Contact_Name = ''
                ret:Purchase_Order_Number = ''
                ret:WebOrderNumber = ''
                ret:WebDateCreated = ''
                ret:WebCreatedUser = ''
                ret:PostCode = tra:Postcode
                ret:Company_Name = tra:Company_Name
                ret:Address_Line1 = tra:Address_Line1
                ret:Address_Line2 = tra:Address_Line2
                ret:Address_Line3 = tra:Address_Line3
                ret:Telephone_Number = tra:Telephone_Number
                ret:Fax_Number = tra:Fax_Number
                ret:Postcode_Delivery = tra:Postcode
                ret:Address_Line1_Delivery = tra:Address_Line1
                ret:Address_LIne2_Delivery = tra:Address_Line2
                ret:Address_Line3_Delivery = tra:Address_Line3
                ret:Telephone_Delivery = tra:Telephone_Number
                ret:Fax_Number_Delivery = tra:Fax_Number
                ret:ExchangeOrder = 0
                ret:LoanOrder = 1

                IF (tra:Invoice_Sub_Accounts = 'YES')
                    If sub:Retail_Payment_Type = 'ACC'
                        ret:payment_method  = 'TRA'
                    End!If SUB:Retail_Payment_Type = 'ACC'
                    IF sub:retail_payment_type  = 'CAS'
                        ret:payment_method = 'CAS'
                    End!IF sub:retail_payment_type  = 'CAS'

                    ret:Courier_Cost = sub:Courier_Cost
                    ret:Courier = sub:Courier_Outgoing
                ELSE ! IF (tra:Invoice_Sub_Accounts = 'YES')
                    If tra:Retail_Payment_Type = 'ACC'
                        ret:payment_method  = 'TRA'
                    End!If SUB:Retail_Payment_Type = 'ACC'
                    IF tra:retail_payment_type  = 'CAS'
                        ret:payment_method = 'CAS'
                    End!IF sub:retail_payment_type  = 'CAS'

                    ret:Courier_Cost = tra:Courier_Cost
                    ret:Courier = tra:Courier_Outgoing
                END ! IF (tra:Invoice_Sub_Accounts = 'YES')

                IF (Access:RETSALES.TryInsert())
                    Access:RETSALES.CancelAutoInc()
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('An error occurred creating the Retail Sale.','ServiceBase',|
                        'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    EXIT

                END

                LOOP i# = 1 TO RECORDS(qFulFilled)
                    GET(qFulFilled,i#)

                    Access:LOAN.Clearkey(loa:Ref_Number_Key)
                    loa:Ref_Number = qFulfilled.LoanRefNumber
                    IF (Access:LOAN.Tryfetch(loa:Ref_Number_Key))
                        DELETE(qFulfilled)
                        CYCLE
                    END

                    if not Access:WAYITEMS.PrimeRecord()
                        wai:WayBillNumber = way:WayBillNumber
                        wai:IsExchange = FALSE
                        wai:Ref_Number = loa:Ref_Number
                        if Access:WAYITEMS.TryUpdate()
                            Access:WAYITEMS.CancelAutoInc()
                            CYCLE
                        end
                    end

                    Access:LOAORDR.Clearkey(lor:Ref_Number_Key)
                    lor:Ref_Number = qFulFilled.LoanOrderRefNumber
                    IF (Access:LOAORDR.Tryfetch(lor:Ref_Number_Key))
                        CYCLE
                    END

                    ! Add Parts. 1 per IMEI
                    Access:STOCK.Clearkey(sto:LoanModelKey)
                    sto:Location = MainStoreLocation()
                    sto:Manufacturer = lor:Manufacturer
                    sto:LoanModelNumber = lor:Model_Number
                    IF (Access:STOCK.TryFetch(sto:LoanModelKey))
                        CYCLE
                    END


                    IF (Access:RETSTOCK.PrimeRecord() = Level:Benign)
                        res:Ref_Number = ret:Ref_Number
                        res:Part_Number = sto:Part_Number
                        res:Description = sto:Description
                        res:Supplier = sto:Supplier
                        res:Purchase_Cost = sto:Purchase_Cost
                        res:Sale_Cost = sto:Sale_Cost
                        res:Retail_Cost = sto:Retail_Cost
                        res:Item_Cost = sto:Sale_Cost
                        res:AccessoryCost = sto:AccessoryCost
                        res:Part_Ref_Number = sto:Ref_Number
                        res:Despatched = 'YES'
                        res:Quantity = 1
                        res:QuantityReceived = 1
                        res:ScannedQty = 0
                        res:Purchase_Order_Number = lor:OrderNumber
                        res:LoanRefNumber = loa:Ref_Number
                        IF (Access:RETSTOCK.TryInsert())
                            Access:RETSTOCK.CancelAutoInc()

                        END

                    END ! IF (Access:RETSTOCK.PrimeRecord() = Level:Benign)

                    lor:Received = 1
                    lor:DateReceived = TODAY()
                    lor:LoanRefNumber = lor:Ref_Number
                    Access:LOAORDR.TryUpdate()

                END ! LOOP i# = 1 TO RECORDS(qFulFilled)

                ! Create Despatch Note
                IF (Access:RETDESNO.PrimeRecord() = Level:Benign)
                    rdn:Consignment_Number = ''
                    rdn:Courier     = ret:Courier
                    rdn:Sale_Number = ret:Ref_Number
                    IF (Access:RETDESNO.TryInsert() = Level:Benign)
                        ret:Despatched = 'PRO'
                        ret:Despatch_Number = rdn:Despatch_Number
                        Access:RETSALES.TryUpdate()
                        glo:ReportCopies = 2
                        Retail_Despatch_Note(ret:Despatch_Number)
                    END ! IF (Access:RETDESNO.TryInsert() = Level:Benign)
                END ! IF (Access:RETDESNO.PrimeRecord() = Level:Benign)

                ! Create Invoice
                SET(DEFAULTS,0)
                Access:DEFAULTS.Next()

                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number = ret:Account_Number
                IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key))
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('An error occurred retrieving the Trade Account details.','ServiceBase',|
                        'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    EXIT
                END ! IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign)

                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('An error occurred retrieving the Trade Account details.','ServiceBase',|
                        'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    EXIT
                END ! IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)

                Access:VATCODE.Clearkey(vat:Vat_Code_Key)
                IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                    vat:Vat_Code = sub:Retail_Vat_Code
                ELSE ! IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                    vat:Vat_Code = tra:Retail_Vat_Code
                END ! IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                IF (Access:VATCODE.TryFetch(vat:Vat_Code_Key))
                    Beep(Beep:SystemHand)  ;  Yield()
                    Case Missive('An error occurred retrieving the VAT details.','ServiceBase',|
                        'mstop.jpg','/&OK')
                    Of 1 ! &OK Button
                    End!Case Message
                    EXIT
                END

                ! Get Parts Cost
                Access:RETSTOCK.Clearkey(res:Despatched_Only_Key)
                res:Ref_Number = ret:Ref_Number
                res:Despatched = 'YES'
                SET(res:Despatched_Only_Key,res:Despatched_Only_Key)
                LOOP UNTIL Access:RETSTOCK.Next()
                    IF (res:Ref_Number <> ret:Ref_Number OR |
                        res:Despatched <> 'YES')
                        BREAK
                    END

                    ret:Parts_Cost += ROUND(res:Item_Cost * res:Quantity,.01)
                END

                ret:Sub_Total = ROUND(ret:Parts_Cost,.01) + ret:Courier_Cost
                Access:RETSALES.TryUpdate()

                ! Create Invoice
                IF (Access:INVOICE.PrimeRecord() = Level:Benign)
                    inv:Invoice_Type = 'RET'
                    inv:Job_Number = ret:Ref_Number
                    inv:Date_Created = TODAY()
                    IF (tra:Invoice_Sub_Accounts = 'YES' AND tra:Use_Sub_Accounts = 'YES')
                        inv:AccountType = 'SUB'
                        inv:Invoice_Vat_Number = sub:Vat_Number
                    ELSE
                        inv:AccountType = 'MAI'
                        inv:Invoice_Vat_Number = tra:Vat_Number
                    END
                    inv:Account_Number = ret:Account_Number
                    inv:Total = ret:Sub_Total
                    inv:Vat_Rate_Retail = ROUND(vat:Vat_Rate,.01)
                    inv:Vat_Number = def:Vat_Number
                    inv:Courier_Paid = ret:Courier_Cost
                    inv:Parts_Paid = ret:Parts_Cost

                    IF (Access:INVOICE.TryInsert())
                        Access:INVOICE.CancelAutoInc()
                    ELSE
                        ret:Invoice_Number = inv:Invoice_Number
                        ret:Invoice_Date = TODAY()
                        ret:Invoice_Courier_Cost = ret:Courier_Cost
                        ret:Invoice_Parts_Cost = ret:Parts_Cost
                        ret:Invoice_Sub_Total = ret:Sub_Total
                        Access:RETSALES.TryUpdate()

                        ! #12223 Invoice created. Add exchange unit audit history. (Bryan: 29/07/2011)
                        LOOP i# = 1 TO RECORDS(qFulFilled)
                            GET(qFulFilled,i#)

                            ! Lookup the relevate Retail Stock Item
                            Access:RETSTOCK.Clearkey(res:LoanRefNumberKey)
                            res:Ref_Number = ret:Ref_Number
                            res:LoanRefNumber = qFulFilled.LoanRefNumber
                            IF (Access:RETSTOCK.Tryfetch(res:LoanRefNumberKey))
                                CYCLE
                            END

                            IF (Access:LOANHIST.PrimeRecord() = Level:Benign)
                                loh:Ref_Number = qFulfilled.LoanRefNumber
                                loh:Date = TODAY()
                                loh:Time = CLOCK()
                                loh:User = glo:Usercode
                                loh:Status = 'IN TRANSIT'
                                loh:Notes = 'ORDER NUMBER: ' & CLIP(res:Purchase_Order_Number) & |
                                                    '<13,10>RETAIL INVOICE NUMBER: ' & ret:Invoice_Number & |
                                                    '<13,10>PRICE: ' & Format(res:Item_Cost,@n14.2)
                                IF (Access:LOANHIST.Tryinsert())
                                    Access:LOANHIST.CancelAutoInc()
                                END
                            END! IF (Access:LOANHIST.PrimeRecord() = Level:Benign)

                        END

                        Line500_XML('STF')

                        ! #13608 Put the logging in the correct place. (DBH: 28/09/2015)
                        MakeInvoiceLog('STF')

                        Retail_Single_Invoice(0,inv:Invoice_Number)
                    END ! IF (Access:INVOICE.TryInsert())
                END ! IF (Access:INVOICE.PrimeRecord() = Level:Benign)


            END ! IF (Access:RETSALES.PrimeRecord() = Level:Benign)

        end
    end

    WayBillLoan(ARCAccount",'TRA',|
                tmp:AccountNumber,tmp:Type,|
                tmp:WayBillNo,tmp:Courier)

    FREE(qFulfilled)
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020288'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Rapid_Loan_Transfer')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt5
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:LOAN.Open
  Relate:LOANORNO.Open
  Relate:RETDESNO.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WAYBCONF.Open
  Relate:WAYBILLS.Open
  Relate:WAYITEMS.Open
  Access:LOCATION.UseFile
  Access:STOCKTYP.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:RETSTOCK.UseFile
  Access:RETSALES.UseFile
  Access:STOCK.UseFile
  Access:LOANHIST.UseFile
  SELF.FilesOpened = True
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:LOAORDR,SELF)
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  do Create_Requests_Queue    ! Fill queue with location requests
  
  Access:LOCATION.ClearKey(loc:Main_Store_Key)
  loc:Main_Store = 'YES'
  set(loc:Main_Store_Key,loc:Main_Store_Key)
  loop until Access:LOCATION.Next()
      if loc:Main_Store <> 'YES' then break.
      tmp:From_Site_Location = loc:Location
      break
  end
  ! Save Window Name
   AddToLog('Window','Open','Rapid_Loan_Transfer')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?List{prop:vcr} = False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW7.Q &= Queue:Browse
  BRW7.AddSortOrder(,lor:LocationReceivedKey)
  BRW7.AddRange(lor:Location)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,lor:Manufacturer,1,BRW7)
  BIND('tmp:Notes',tmp:Notes)
  BIND('tmp:To_Site_Location',tmp:To_Site_Location)
  ?List{PROP:IconList,1} = '~bluetick.ico'
  BRW7.AddField(lor:Manufacturer,BRW7.Q.lor:Manufacturer)
  BRW7.AddField(lor:Model_Number,BRW7.Q.lor:Model_Number)
  BRW7.AddField(tmp:Notes,BRW7.Q.tmp:Notes)
  BRW7.AddField(lor:Qty_Required,BRW7.Q.lor:Qty_Required)
  BRW7.AddField(lor:OrderNumber,BRW7.Q.lor:OrderNumber)
  BRW7.AddField(lor:Qty_Received,BRW7.Q.lor:Qty_Received)
  BRW7.AddField(lor:Notes,BRW7.Q.lor:Notes)
  BRW7.AddField(lor:Ref_Number,BRW7.Q.lor:Ref_Number)
  BRW7.AddField(lor:Received,BRW7.Q.lor:Received)
  BRW7.AddField(lor:Location,BRW7.Q.lor:Location)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:LOAN.Close
    Relate:LOANORNO.Close
    Relate:RETDESNO.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WAYBCONF.Close
    Relate:WAYBILLS.Close
    Relate:WAYITEMS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Rapid_Loan_Transfer')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      Select_Stock_Type
      Select_Stock_Type
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Allocate_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate_Button, Accepted)
      if ?tmp:Scanned_IMEI{prop:Disable} = False
          select(?tmp:Scanned_IMEI)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate_Button, Accepted)
    OF ?tmp:From_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
      glo:select1    = 'LOA'
      stp:use_exchange = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
    OF ?From_Stock_Type_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
      glo:select1    = 'LOA'
      stp:use_exchange = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
    OF ?tmp:To_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
      glo:select1    = 'LOA'
      stp:use_exchange = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
    OF ?To_Stock_Type_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
      glo:select1    = 'LOA'
      stp:use_exchange = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      IF (Records(qFulFilled))
          IF (FinishProcess() = 1)
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END
      END
      
      !if records(glo:q_JobNumber)
      !    do End_Of_Process
      !
      !    if tmp:Response = 1
      !        select(?tmp:Scanned_IMEI)
      !        cycle
      !    end
      !
      !end
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      if records(glo:q_JobNumber)
          do End_Of_Process
      
          if tmp:Response = 1
              select(?tmp:Scanned_IMEI)
              cycle
          end
      
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ReprintWaybillButton
      ThisWindow.Update
      AskWaybillNumber
      ThisWindow.Reset
    OF ?tmp:Scanned_IMEI
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Scanned_IMEI, Accepted)
      ! #12347 Allocate IMEI (Rewritten) (DBH: 19/01/2012)
          BRW7.UpdateViewRecord()
      
          IF (lor:Qty_Required = lor:Qty_Received)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The requested quantity of units has been fulfilled for this order.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              tmp:Scanned_IMEI = ''
              DISPLAY()
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END
      
          Access:LOAN.Clearkey(loa:ESN_Only_Key)
          loa:ESN = tmp:Scanned_IMEI
          IF (Access:LOAN.TryFetch(loa:ESN_Only_Key))
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('I.M.E.I. Number not found.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              tmp:Scanned_IMEI = ''
              DISPLAY()
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END
      
          IF (loa:Available <> 'AVL' OR loa:Location <> tmp:From_Site_Location)
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('The selected I.M.E.I. Number is not available.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              tmp:Scanned_IMEI = ''
              DISPLAY()
              SELECT(?tmp:Scanned_IMEI)
              CYCLE
          END ! IF (loa:Available <> 'AVL' OR loa:Location <> tmp:From_Site_Location)
      
          IF (lor:Model_Number <> loa:Model_Number) OR (tmp:From_Stock_Type <> loa:Stock_Type)
              Case Missive('The I.M.E.I. Number scanned is a ' & Clip(loa:Model_Number) & ' from the ' & Clip(loa:Stock_Type) & ' Stock Type.'&|
                  '<13,10>The model requested  is a ' & Clip(lor:Model_Number) & '. Do you wish to IGNORE the mismatch, or SCAN a different unit?','ServiceBase 3g',|
                  'mquest.jpg','\Ignore|Scan Again')
              Of 2 ! Scan Again Button
                  SELECT(?tmp:Scanned_IMEI)
                  CYCLE
              Of 1 ! Ignore Button
      
              End ! Case Missive
          END
      
          ! Move Loan To New Location
          loa:Location = tmp:To_Site_Location
          loa:Available = 'ITF' ! In Transit To Site
          loa:StatusChangeDate = TODAY()
      
          qFulfilled.LoanRefNumber = loa:Ref_Number
          qFulfilled.LoanOrderRefNumber = lor:Ref_Number
          ADD(qFulfilled)
      
          IF (Access:LOAN.TryUpdate())
              Beep(Beep:SystemHand)  ;  Yield()
              Case Missive('Unable to update Loan Unit.','ServiceBase',|
                             'mstop.jpg','/&OK') 
              Of 1 ! &OK Button
              End!Case Message
              CYCLE
          END
      
          moveDown# = 0
          lor:Qty_Received += 1
          IF (Access:LOAORDR.TryUpdate() = Level:Benign)
              BRW7.ResetSort(1)
      
              IF (lor:Qty_Required = lor:Qty_Received)
                  moveDown# = 1
              END ! IF (lor:Qty_Required = lor:Qty_Received)
      
              IF (MoreToProcess(tmp:To_Site_Location))
                  IF (moveDown# = 1)
                      SELECT(?List,CHOICE(?List) + 1)
                  END
              ELSE ! IF (MoreToProcess(tmp:To_Site_Location))
                  Beep(Beep:SystemAsterisk)  ;  Yield()
                  Case Missive('The requested quantity of unit(s) has been fulfilled.','ServiceBase',|
                      'midea.jpg','/&OK')
                  Of 1 ! &OK Button
                  End!Case Message
      
                  ! Print Waybill
                  DO Print_Waybill
      
                  FREE(qFulFilled)
      
                  MarkLocationReceived(tmp:To_Site_Location)
      
                  BRW7.ResetSort(1)
      
                  DO Create_Requests_Queue
              END ! IF (MoreToProcess(tmp:To_Site_Location))
          END ! IF (Access:LOAORDR.TryUpdate() = Level:Benign)
      
          tmp:Scanned_IMEI = ''
          DISPLAY()
          SELECT(?tmp:Scanned_IMEI)
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Scanned_IMEI, Accepted)
    OF ?tmp:SiteLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
      if records(glo:q_JobNumber)
          tmp:Original_Location = tmp:To_Site_Location

!          GET(Requests_Q,CHOICE(?List1))
          if tmp:Original_Location <> tmp:SiteLocation
              do End_Of_Process

              if tmp:Response = 1
                  tmp:SiteLocation = tmp:Original_Location
      !            GET(Requests_Q,tmp:ListPos)
      !            Select(?List1,tmp:ListPos)
                  Select(tmp:SiteLocation)
                  cycle
              end
          end

      end

      !tmp:ListPos = CHOICE(?List1)
      !GET(Requests_Q,CHOICE(?List1))
      tmp:To_Site_Location = tmp:SiteLocation
      display(?tmp:To_Site_Location)
      do Enable_IMEI_Scan
      !BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SiteLocation, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020288'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020288'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020288'&'0')
      ***
    OF ?tmp:From_Site_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Site_Location, Accepted)
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:From_Site_Location OR ?tmp:From_Site_Location{Prop:Req}
        loc:Location = tmp:From_Site_Location
        !Save Lookup Field Incase Of error
        look:tmp:From_Site_Location        = tmp:From_Site_Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:From_Site_Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:From_Site_Location = look:tmp:From_Site_Location
            SELECT(?tmp:From_Site_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Site_Location, Accepted)
    OF ?From_Location_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickSiteLocations
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Location_Button, Accepted)
      case GlobalResponse
          of RequestCompleted
              tmp:From_Site_Location = loc:Location
              display(?tmp:From_Site_Location)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Location_Button, Accepted)
    OF ?tmp:From_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
      glo:select1    = ''
      
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:From_Stock_Type OR ?tmp:From_Stock_Type{Prop:Req}
        stp:Stock_Type = tmp:From_Stock_Type
        !Save Lookup Field Incase Of error
        look:tmp:From_Stock_Type        = tmp:From_Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:From_Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:From_Stock_Type = look:tmp:From_Stock_Type
            SELECT(?tmp:From_Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:From_Stock_Type, Accepted)
    OF ?From_Stock_Type_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Stock_Type
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
      glo:select1    = ''
      
      case GlobalResponse
          of RequestCompleted
              tmp:From_Stock_Type = stp:Stock_Type
              display(?tmp:From_Stock_Type)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?From_Stock_Type_Button, Accepted)
    OF ?tmp:To_Stock_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
      glo:select1    = ''
      
      if ~0{prop:AcceptAll}
          do Enable_IMEI_Scan
      end
      IF tmp:To_Stock_Type OR ?tmp:To_Stock_Type{Prop:Req}
        stp:Stock_Type = tmp:To_Stock_Type
        !Save Lookup Field Incase Of error
        look:tmp:To_Stock_Type        = tmp:To_Stock_Type
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:To_Stock_Type = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:To_Stock_Type = look:tmp:To_Stock_Type
            SELECT(?tmp:To_Stock_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:To_Stock_Type, Accepted)
    OF ?To_Stock_Type_Button
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Select_Stock_Type
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
      glo:select1    = ''
      
      case GlobalResponse
          of RequestCompleted
              tmp:To_Stock_Type = stp:Stock_Type
              display(?tmp:To_Stock_Type)
      end
      
      do Enable_IMEI_Scan
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?To_Stock_Type_Button, Accepted)
    OF ?ViewNotes
      ThisWindow.Update
      LoanOrderNotes
      ThisWindow.Reset
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:SiteLocation
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW7.ApplyRange
      BRW7.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
      if keycode() = EscKey
          cycle
      end
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
MarkLocationReceived        PROCEDURE(STRING fSiteLocation)
    CODE
        Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
        lor:Received = 0
        lor:Location = fSiteLocation
        SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
        LOOP UNTIL Access:LOAORDR.Next()
            IF (lor:Received <> 0 OR |
                lor:Location <> fSiteLocation)
                BREAK
            END

            IF (lor:OrderNumber > 0)
                lor:Received = 1
                lor:DateReceived = TODAY()
                Access:LOAORDR.TryUpdate()
            ELSE
                Relate:LOAORDR.Delete(0)
            END
            Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
            lor:Received = 0
            lor:Location = fSiteLocation
        END
MoreToProcess       PROCEDURE(STRING fSiteLocation)
rtnValue                BYTE(0)

    CODE
        Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
        lor:Received = 0
        lor:Location = fSiteLocation
        SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
        LOOP UNTIL Access:LOAORDR.Next()
            IF (lor:Received <> 0 OR |
                lor:Location <> fSiteLocation)
                BREAK
            END
            IF (lor:Qty_Received < lor:Qty_Required)
                rtnValue = 1
                BREAK
            END
        END

        RETURN rtnValue
FinishProcess       PROCEDURE()
rtnValue                BYTE(0)
locMoreToProcess        Byte(0)
gLater                  GROUP,PRE()
locLaterQty                 LIKE(lor:Qty_Required)
locLaterManufacturer        LIKE(lor:Manufacturer)
locLaterModelNumber         LIKE(lor:Model_Number)
locLaterNotes               LIKE(lor:Notes)
locLaterOrderNumber         LIKE(lor:OrderNumber)
                        END
    CODE
        IF (MoreToProcess(tmp:To_Site_Location))
            Case Missive('Not all requests from ' & Clip(tmp:To_Site_Location) & ' have been fulfilled.'&|
                '<13,10>'&|
                '<13,10>Do you wish to go back and ALLOCATE the shortage now, allocate this shortage LATER, or DELETE the shortage?','ServiceBase 3g',|
                'mquest.jpg','\Delete|Later|Allocate')
            Of 3 ! Allocate Button
                rtnValue = 1
            OF 2 ! Later Button
                rtnValue = 2

                locLaterQty = 0
                Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
                lor:Received = 0
                lor:Location = tmp:To_Site_Location
                SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
                LOOP UNTIL Access:LOAORDR.Next()
                    IF (lor:Received <> 0 OR |
                        lor:Location <> tmp:To_Site_Location)
                        BREAK
                    END

                    IF (lor:OrderNumber > 0)
                        IF (lor:Qty_Received > 0 AND (lor:Qty_Received < lor:Qty_Required))
                            ! Make the "received" as Received. ADd new line for "later"
                            CLEAR(gLater)
                            locLaterQty = lor:Qty_Required - lor:Qty_Received
                            locLaterManufacturer = lor:Manufacturer
                            locLaterModelNumber = lor:Model_Number
                            locLaterNotes = lor:Notes
                            locLaterOrderNumber = lor:OrderNumber

                            lor:Qty_Required = lor:Qty_Received
                            lor:Received = 1
                            lor:DateReceived = TODAY()
                            Access:LOAORDR.TryUpdate()

                            Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
                            lor:Received = 0
                            lor:Location = tmp:To_Site_Location
                            SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
                        END
                    ELSE
                        IF (lor:Qty_Received < lor:Qty_Required)
                            IF (lor:Qty_Received > 0)
                                lor:Qty_Required = lor:Qty_Required - lor:Qty_Received
                                lor:Qty_Received = 0
                                Access:LOAORDR.TryUpdate()

                                Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
                                lor:Received = 0
                                lor:Location = tmp:To_Site_Location
                                SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
                            END
                        ELSE
                            Relate:LOAORDR.Delete(0)

                            Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
                            lor:Received = 0
                            lor:Location = tmp:To_Site_Location
                            SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
                        END
                    END

                    IF (locLaterQty > 0)
                        ! Add a new entry for the "later"
                        IF (Access:LOAORDR.PrimeRecord() = Level:Benign)
                            lor:Location        = tmp:To_Site_Location
                            lor:Manufacturer    = locLaterManufacturer
                            lor:Model_Number    = locLaterModelNumber
                            lor:Qty_Required    = locLaterQty
                            lor:Notes           = locLaterNotes
                            lor:OrderNumber     = locLaterOrderNumber
                            IF (Access:LOAORDR.TryInsert())
                                Access:LOAORDR.CancelAutoInc()
                            END
                        END
                        Clear(gLater)
                    END



                END

                ! Print Waybill
                DO Print_WayBill
                FREE(glo:Q_JobNumber)

                BRW7.ResetSort(1)
            OF 1 ! Delete Button
                rtnValue = 3

                ! Remove Location Request
                Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
                lor:Received = 0
                lor:Location = tmp:To_Site_Location
                SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
                LOOP UNTIL Access:LOAORDR.Next()
                    IF (lor:Received <> 0 OR |
                        lor:Location <> tmp:To_Site_Location)
                        BREAK
                    END

                    Relate:LOAORDR.Delete(0)

                    Access:LOAORDR.Clearkey(lor:LocationReceivedKey)
                    lor:Received = 0
                    lor:Location = tmp:To_Site_Location
                    SET(lor:LocationReceivedKey,lor:LocationReceivedKey)
                END

                ! Print Waybill
                DO Print_Waybill
                FREE(glo:Q_JobNumber)

                DO Create_Requests_Queue

                BRW7.ResetSort(1)

            END
        END

    RETURN rtnValue


Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW7.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 0
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:SiteLocation
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (lor:Qty_Required = lor:Qty_Received)
    SELF.Q.lor:Manufacturer_NormalFG = 255
    SELF.Q.lor:Manufacturer_NormalBG = -1
    SELF.Q.lor:Manufacturer_SelectedFG = 16777215
    SELF.Q.lor:Manufacturer_SelectedBG = -1
  ELSE
    SELF.Q.lor:Manufacturer_NormalFG = -1
    SELF.Q.lor:Manufacturer_NormalBG = -1
    SELF.Q.lor:Manufacturer_SelectedFG = -1
    SELF.Q.lor:Manufacturer_SelectedBG = -1
  END
  IF (lor:Qty_Required = lor:Qty_Received)
    SELF.Q.lor:Model_Number_NormalFG = 255
    SELF.Q.lor:Model_Number_NormalBG = -1
    SELF.Q.lor:Model_Number_SelectedFG = 16777215
    SELF.Q.lor:Model_Number_SelectedBG = -1
  ELSE
    SELF.Q.lor:Model_Number_NormalFG = -1
    SELF.Q.lor:Model_Number_NormalBG = -1
    SELF.Q.lor:Model_Number_SelectedFG = -1
    SELF.Q.lor:Model_Number_SelectedBG = -1
  END
  SELF.Q.tmp:Notes_NormalFG = -1
  SELF.Q.tmp:Notes_NormalBG = -1
  SELF.Q.tmp:Notes_SelectedFG = -1
  SELF.Q.tmp:Notes_SelectedBG = -1
  IF (lor:Notes <> '')
    SELF.Q.tmp:Notes_Icon = 1
  ELSE
    SELF.Q.tmp:Notes_Icon = 0
  END
  IF (lor:Qty_Required = lor:Qty_Received)
    SELF.Q.lor:Qty_Required_NormalFG = 255
    SELF.Q.lor:Qty_Required_NormalBG = -1
    SELF.Q.lor:Qty_Required_SelectedFG = 16777215
    SELF.Q.lor:Qty_Required_SelectedBG = -1
  ELSE
    SELF.Q.lor:Qty_Required_NormalFG = -1
    SELF.Q.lor:Qty_Required_NormalBG = -1
    SELF.Q.lor:Qty_Required_SelectedFG = -1
    SELF.Q.lor:Qty_Required_SelectedBG = -1
  END
  SELF.Q.lor:OrderNumber_NormalFG = -1
  SELF.Q.lor:OrderNumber_NormalBG = -1
  SELF.Q.lor:OrderNumber_SelectedFG = -1
  SELF.Q.lor:OrderNumber_SelectedBG = -1
  IF (lor:Qty_Required = lor:Qty_Received)
    SELF.Q.lor:Qty_Received_NormalFG = 255
    SELF.Q.lor:Qty_Received_NormalBG = -1
    SELF.Q.lor:Qty_Received_SelectedFG = 16777215
    SELF.Q.lor:Qty_Received_SelectedBG = -1
  ELSE
    SELF.Q.lor:Qty_Received_NormalFG = -1
    SELF.Q.lor:Qty_Received_NormalBG = -1
    SELF.Q.lor:Qty_Received_SelectedFG = -1
    SELF.Q.lor:Qty_Received_SelectedBG = -1
  END
  SELF.Q.lor:Notes_NormalFG = -1
  SELF.Q.lor:Notes_NormalBG = -1
  SELF.Q.lor:Notes_SelectedFG = -1
  SELF.Q.lor:Notes_SelectedBG = -1


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
