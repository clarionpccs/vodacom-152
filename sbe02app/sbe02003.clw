

   MEMBER('sbe02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBE02003.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTradeAccFaultCodes PROCEDURE                    !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
BRW2::View:Browse    VIEW(TRAFAULO)
                       PROJECT(tfo:Field)
                       PROJECT(tfo:Description)
                       PROJECT(tfo:AccountNumber)
                       PROJECT(tfo:Field_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
tfo:Field              LIKE(tfo:Field)                !List box control field - type derived from field
tfo:Description        LIKE(tfo:Description)          !List box control field - type derived from field
tfo:AccountNumber      LIKE(tfo:AccountNumber)        !Primary key field - type derived from field
tfo:Field_Number       LIKE(tfo:Field_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::tfo:AccountNumber   LIKE(tfo:AccountNumber)
HK12::tfo:Field_Number    LIKE(tfo:Field_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
History::taf:Record  LIKE(taf:RECORD),STATIC
QuickWindow          WINDOW('Update Trade Account Fault Codes'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(64,54,244,310),USE(?CurrentTab),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('General'),USE(?Tab:1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Account Number'),AT(68,70),USE(?TAF:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,956,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,70,124,10),USE(taf:AccountNumber),DISABLE,LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Account Number'),TIP('Account Number'),REQ,UPR,READONLY
                           PROMPT('Field Number'),AT(68,86),USE(?TAF:Field_Number:Prompt),FONT(,,,FONT:bold,CHARSET:ANSI)
                           SPIN(@n2),AT(144,86,64,10),USE(taf:Field_Number),RIGHT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR,RANGE(1,12),MSG('Fault Code Field Number')
                           PROMPT('Field Name'),AT(68,102),USE(?TAF:Field_Name:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(144,102,124,10),USE(taf:Field_Name),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,CAP
                           PROMPT('Field Type'),AT(68,118),USE(?TAF:Field_Type:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           LIST,AT(144,118,64,10),USE(taf:Field_Type),LEFT(2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),DROP(5),FROM('STRING|NUMBER|DATE')
                           CHECK('Lookup'),AT(144,134,70,8),USE(taf:Lookup),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('YES','NO')
                           CHECK('Force Lookup'),AT(216,134,58,8),USE(taf:Force_Lookup),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Compulsory At Completion'),AT(144,158),USE(taf:Compulsory),FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Compulsory At Point Of Booking'),AT(144,146,132,8),USE(taf:Compulsory_At_Booking),FONT(,,,FONT:bold,CHARSET:ANSI),VALUE('YES','NO')
                           CHECK('Replicate To Fault Description'),AT(144,174,132,8),USE(taf:ReplicateFault),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Replicate To Invoice Text'),AT(144,186,112,8),USE(taf:ReplicateInvoice),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Restrict Length'),AT(144,202,72,8),USE(taf:RestrictLength),FONT(,,,FONT:bold,CHARSET:ANSI),MSG('Restrict Length'),TIP('Restrict Length'),VALUE('1','0')
                           GROUP,AT(64,210,208,40),USE(?Group2),HIDE
                             PROMPT('Length From'),AT(68,214),USE(?TAF:LengthFrom:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(144,214,64,10),USE(taf:LengthFrom),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Length From'),TIP('Length From'),UPR
                             PROMPT('Length To'),AT(68,230),USE(?TAF:LengthTo:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                             ENTRY(@s8),AT(144,230,64,10),USE(taf:LengthTo),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Length To'),TIP('Length To'),UPR
                           END
                         END
                       END
                       SHEET,AT(312,54,304,310),USE(?Sheet2),COLOR(0D6E7EFH),SPREAD
                         TAB('Lookup'),USE(?Tab4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(316,70,124,10),USE(tfo:Field),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           LIST,AT(316,86,296,242),USE(?Browse:2),IMM,HVSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('120L(2)|M~Field~@s30@80L(2)|M~Description~@s60@'),FROM(Queue:Browse:2)
                           BUTTON,AT(412,332),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(480,332),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg')
                           BUTTON,AT(548,332),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend A Trade Account Fault Code'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW2::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020298'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTradeAccFaultCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TAF:AccountNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(taf:Record,History::taf:Record)
  SELF.AddHistoryField(?taf:AccountNumber,2)
  SELF.AddHistoryField(?taf:Field_Number,3)
  SELF.AddHistoryField(?taf:Field_Name,4)
  SELF.AddHistoryField(?taf:Field_Type,5)
  SELF.AddHistoryField(?taf:Lookup,6)
  SELF.AddHistoryField(?taf:Force_Lookup,7)
  SELF.AddHistoryField(?taf:Compulsory,8)
  SELF.AddHistoryField(?taf:Compulsory_At_Booking,9)
  SELF.AddHistoryField(?taf:ReplicateFault,10)
  SELF.AddHistoryField(?taf:ReplicateInvoice,11)
  SELF.AddHistoryField(?taf:RestrictLength,12)
  SELF.AddHistoryField(?taf:LengthFrom,13)
  SELF.AddHistoryField(?taf:LengthTo,14)
  SELF.AddUpdateFile(Access:TRAFAULT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRAFAULO.Open
  Access:TRAFAULT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRAFAULT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:TRAFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','UpdateTradeAccFaultCodes')
  ?taf:Field_Type{prop:vcr} = TRUE
  ?Browse:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW2.Q &= Queue:Browse:2
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,tfo:Field_Key)
  BRW2.AddRange(tfo:Field_Number)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,tfo:Field,1,BRW2)
  BRW2.AddField(tfo:Field,BRW2.Q.tfo:Field)
  BRW2.AddField(tfo:Description,BRW2.Q.tfo:Description)
  BRW2.AddField(tfo:AccountNumber,BRW2.Q.tfo:AccountNumber)
  BRW2.AddField(tfo:Field_Number,BRW2.Q.tfo:Field_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?taf:Lookup{Prop:Checked} = True
    UNHIDE(?TAF:Force_Lookup)
  END
  IF ?taf:Lookup{Prop:Checked} = False
    HIDE(?TAF:Force_Lookup)
  END
  IF ?taf:Compulsory{Prop:Checked} = True
    DISABLE(?taf:Compulsory_At_Booking)
  END
  IF ?taf:Compulsory{Prop:Checked} = False
    ENABLE(?taf:Compulsory_At_Booking)
  END
  IF ?taf:Compulsory_At_Booking{Prop:Checked} = True
    DISABLE(?taf:Compulsory)
  END
  IF ?taf:Compulsory_At_Booking{Prop:Checked} = False
    ENABLE(?taf:Compulsory)
  END
  IF ?taf:RestrictLength{Prop:Checked} = True
    UNHIDE(?Group2)
  END
  IF ?taf:RestrictLength{Prop:Checked} = False
    HIDE(?Group2)
  END
  BRW2.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRAFAULO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateTradeAccFaultCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request = Insertrecord
      If taf:field_number = ''
          Case Missive('You have not entered a Field Number.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
          do_update# = 0
      End!If maf:field_number = ''
  End!If request = Insertrecord
  If do_update# = 1 and taf:lookup = 'YES'
      glo:select1   = taf:AccountNumber
      glo:select2   = taf:Field_Number
      glo:Select3   = taf:Field_Name
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTradeFaultCodesLookup
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1 and maf:lookup = 'YES'
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?taf:Field_Number
      IF Access:TRAFAULT.TryValidateField(3)
        SELECT(?taf:Field_Number)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?taf:Field_Number
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?taf:Field_Number{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:2)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?taf:Lookup
      IF ?taf:Lookup{Prop:Checked} = True
        UNHIDE(?TAF:Force_Lookup)
      END
      IF ?taf:Lookup{Prop:Checked} = False
        HIDE(?TAF:Force_Lookup)
      END
      ThisWindow.Reset
    OF ?taf:Compulsory
      IF ?taf:Compulsory{Prop:Checked} = True
        DISABLE(?taf:Compulsory_At_Booking)
      END
      IF ?taf:Compulsory{Prop:Checked} = False
        ENABLE(?taf:Compulsory_At_Booking)
      END
      ThisWindow.Reset
    OF ?taf:Compulsory_At_Booking
      IF ?taf:Compulsory_At_Booking{Prop:Checked} = True
        DISABLE(?taf:Compulsory)
      END
      IF ?taf:Compulsory_At_Booking{Prop:Checked} = False
        ENABLE(?taf:Compulsory)
      END
      ThisWindow.Reset
    OF ?taf:RestrictLength
      IF ?taf:RestrictLength{Prop:Checked} = True
        UNHIDE(?Group2)
      END
      IF ?taf:RestrictLength{Prop:Checked} = False
        HIDE(?Group2)
      END
      ThisWindow.Reset
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020298'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020298'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020298'&'0')
      ***
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?taf:Field_Number
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:2)
      BRW2.ApplyRange
      BRW2.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW2.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = taf:AccountNumber
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = taf:Field_Number
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseTradeFaultCodes PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(TRAFAULT)
                       PROJECT(taf:Field_Number)
                       PROJECT(taf:Field_Name)
                       PROJECT(taf:Field_Type)
                       PROJECT(taf:Lookup)
                       PROJECT(taf:RecordNumber)
                       PROJECT(taf:AccountNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
taf:Field_Number       LIKE(taf:Field_Number)         !List box control field - type derived from field
taf:Field_Name         LIKE(taf:Field_Name)           !List box control field - type derived from field
taf:Field_Type         LIKE(taf:Field_Type)           !List box control field - type derived from field
taf:Lookup             LIKE(taf:Lookup)               !List box control field - type derived from field
GLO:Select1            STRING(1)                      !Browse hot field - unable to determine correct data type
taf:RecordNumber       LIKE(taf:RecordNumber)         !Primary key field - type derived from field
taf:AccountNumber      LIKE(taf:AccountNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Fault Code File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,114,344,180),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('49R(2)|M~Field Number~L@n2@125L(2)|M~Field Name~@s30@95L(2)|M~Field Type~@s30@12' &|
   'L(2)|M~Lookup~@s3@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),COLOR(0D6E7EFH),SPREAD
                         TAB('By Field Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n2),AT(168,98,59,12),USE(taf:Field_Number),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Fault Code Field Number')
                           BUTTON,AT(168,298),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(236,298),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(304,298),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(448,298),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Trade Fault Code File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020293'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseTradeFaultCodes')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:TRADEACC.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRAFAULT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseTradeFaultCodes')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,taf:Field_Number_Key)
  BRW1.AddRange(taf:AccountNumber,Relate:TRAFAULT,Relate:TRADEACC)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?taf:Field_Number,taf:Field_Number,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(taf:Field_Number,BRW1.Q.taf:Field_Number)
  BRW1.AddField(taf:Field_Name,BRW1.Q.taf:Field_Name)
  BRW1.AddField(taf:Field_Type,BRW1.Q.taf:Field_Type)
  BRW1.AddField(taf:Lookup,BRW1.Q.taf:Lookup)
  BRW1.AddField(GLO:Select1,BRW1.Q.GLO:Select1)
  BRW1.AddField(taf:RecordNumber,BRW1.Q.taf:RecordNumber)
  BRW1.AddField(taf:AccountNumber,BRW1.Q.taf:AccountNumber)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:TRADEACC.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseTradeFaultCodes')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            If SecurityCheck('TRADE FAULT CODES - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                access:trafault.cancelautoinc()
                do_update# = false
            end
        of changerecord
            If SecurityCheck('TRADE FAULT CODES - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
        of deleterecord
            If SecurityCheck('TRADE FAULT CODES - DELETE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTradeAccFaultCodes
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020293'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020293'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020293'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)




BrowseTRADEACC PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BrLocator1           STRING(50)
BrFilter1            STRING(300)
BrFilter2            STRING(300)
BrLocator2           STRING(50)
ViewAccounts         STRING('A')
BRW1::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:Telephone_Number)
                       PROJECT(tra:Fax_Number)
                       PROJECT(tra:Labour_VAT_Code)
                       PROJECT(tra:Parts_VAT_Code)
                       PROJECT(tra:Retail_VAT_Code)
                       PROJECT(tra:Use_Sub_Accounts)
                       PROJECT(tra:Invoice_Sub_Accounts)
                       PROJECT(tra:Stop_Account)
                       PROJECT(tra:Price_Despatch)
                       PROJECT(tra:Price_Retail_Despatch)
                       PROJECT(tra:Print_Despatch_Complete)
                       PROJECT(tra:Print_Despatch_Despatch)
                       PROJECT(tra:Standard_Repair_Type)
                       PROJECT(tra:Courier_Incoming)
                       PROJECT(tra:Courier_Outgoing)
                       PROJECT(tra:Use_Contact_Name)
                       PROJECT(tra:VAT_Number)
                       PROJECT(tra:Use_Delivery_Address)
                       PROJECT(tra:Use_Collection_Address)
                       PROJECT(tra:UseCustDespAdd)
                       PROJECT(tra:Allow_Loan)
                       PROJECT(tra:Allow_Exchange)
                       PROJECT(tra:Force_Fault_Codes)
                       PROJECT(tra:Despatch_Invoiced_Jobs)
                       PROJECT(tra:Despatch_Paid_Jobs)
                       PROJECT(tra:Print_Despatch_Notes)
                       PROJECT(tra:Print_Retail_Despatch_Note)
                       PROJECT(tra:Print_Retail_Picking_Note)
                       PROJECT(tra:Despatch_Note_Per_Item)
                       PROJECT(tra:Skip_Despatch)
                       PROJECT(tra:Summary_Despatch_Notes)
                       PROJECT(tra:Exchange_Stock_Type)
                       PROJECT(tra:Loan_Stock_Type)
                       PROJECT(tra:Courier_Cost)
                       PROJECT(tra:Force_Estimate)
                       PROJECT(tra:Estimate_If_Over)
                       PROJECT(tra:Turnaround_Time)
                       PROJECT(tra:Retail_Payment_Type)
                       PROJECT(tra:Retail_Price_Structure)
                       PROJECT(tra:Use_Customer_Address)
                       PROJECT(tra:Invoice_Customer_Address)
                       PROJECT(tra:ZeroChargeable)
                       PROJECT(tra:RefurbCharge)
                       PROJECT(tra:ChargeType)
                       PROJECT(tra:WarChargeType)
                       PROJECT(tra:ExchangeAcc)
                       PROJECT(tra:ExcludeBouncer)
                       PROJECT(tra:RetailZeroParts)
                       PROJECT(tra:HideDespAdd)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:Telephone_Number   LIKE(tra:Telephone_Number)     !List box control field - type derived from field
tra:Fax_Number         LIKE(tra:Fax_Number)           !List box control field - type derived from field
tra:Labour_VAT_Code    LIKE(tra:Labour_VAT_Code)      !List box control field - type derived from field
tra:Parts_VAT_Code     LIKE(tra:Parts_VAT_Code)       !List box control field - type derived from field
tra:Retail_VAT_Code    LIKE(tra:Retail_VAT_Code)      !List box control field - type derived from field
tra:Use_Sub_Accounts   LIKE(tra:Use_Sub_Accounts)     !List box control field - type derived from field
tra:Invoice_Sub_Accounts LIKE(tra:Invoice_Sub_Accounts) !List box control field - type derived from field
tra:Stop_Account       LIKE(tra:Stop_Account)         !List box control field - type derived from field
tra:Price_Despatch     LIKE(tra:Price_Despatch)       !List box control field - type derived from field
tra:Price_Retail_Despatch LIKE(tra:Price_Retail_Despatch) !List box control field - type derived from field
tra:Print_Despatch_Complete LIKE(tra:Print_Despatch_Complete) !List box control field - type derived from field
tra:Print_Despatch_Despatch LIKE(tra:Print_Despatch_Despatch) !List box control field - type derived from field
tra:Standard_Repair_Type LIKE(tra:Standard_Repair_Type) !List box control field - type derived from field
tra:Courier_Incoming   LIKE(tra:Courier_Incoming)     !List box control field - type derived from field
tra:Courier_Outgoing   LIKE(tra:Courier_Outgoing)     !List box control field - type derived from field
tra:Use_Contact_Name   LIKE(tra:Use_Contact_Name)     !List box control field - type derived from field
tra:VAT_Number         LIKE(tra:VAT_Number)           !List box control field - type derived from field
tra:Use_Delivery_Address LIKE(tra:Use_Delivery_Address) !List box control field - type derived from field
tra:Use_Collection_Address LIKE(tra:Use_Collection_Address) !List box control field - type derived from field
tra:UseCustDespAdd     LIKE(tra:UseCustDespAdd)       !List box control field - type derived from field
tra:Allow_Loan         LIKE(tra:Allow_Loan)           !List box control field - type derived from field
tra:Allow_Exchange     LIKE(tra:Allow_Exchange)       !List box control field - type derived from field
tra:Force_Fault_Codes  LIKE(tra:Force_Fault_Codes)    !List box control field - type derived from field
tra:Despatch_Invoiced_Jobs LIKE(tra:Despatch_Invoiced_Jobs) !List box control field - type derived from field
tra:Despatch_Paid_Jobs LIKE(tra:Despatch_Paid_Jobs)   !List box control field - type derived from field
tra:Print_Despatch_Notes LIKE(tra:Print_Despatch_Notes) !List box control field - type derived from field
tra:Print_Retail_Despatch_Note LIKE(tra:Print_Retail_Despatch_Note) !List box control field - type derived from field
tra:Print_Retail_Picking_Note LIKE(tra:Print_Retail_Picking_Note) !List box control field - type derived from field
tra:Despatch_Note_Per_Item LIKE(tra:Despatch_Note_Per_Item) !List box control field - type derived from field
tra:Skip_Despatch      LIKE(tra:Skip_Despatch)        !List box control field - type derived from field
tra:Summary_Despatch_Notes LIKE(tra:Summary_Despatch_Notes) !List box control field - type derived from field
tra:Exchange_Stock_Type LIKE(tra:Exchange_Stock_Type) !List box control field - type derived from field
tra:Loan_Stock_Type    LIKE(tra:Loan_Stock_Type)      !List box control field - type derived from field
tra:Courier_Cost       LIKE(tra:Courier_Cost)         !List box control field - type derived from field
tra:Force_Estimate     LIKE(tra:Force_Estimate)       !List box control field - type derived from field
tra:Estimate_If_Over   LIKE(tra:Estimate_If_Over)     !List box control field - type derived from field
tra:Turnaround_Time    LIKE(tra:Turnaround_Time)      !List box control field - type derived from field
tra:Retail_Payment_Type LIKE(tra:Retail_Payment_Type) !List box control field - type derived from field
tra:Retail_Price_Structure LIKE(tra:Retail_Price_Structure) !List box control field - type derived from field
tra:Use_Customer_Address LIKE(tra:Use_Customer_Address) !List box control field - type derived from field
tra:Invoice_Customer_Address LIKE(tra:Invoice_Customer_Address) !List box control field - type derived from field
tra:ZeroChargeable     LIKE(tra:ZeroChargeable)       !List box control field - type derived from field
tra:RefurbCharge       LIKE(tra:RefurbCharge)         !List box control field - type derived from field
tra:ChargeType         LIKE(tra:ChargeType)           !List box control field - type derived from field
tra:WarChargeType      LIKE(tra:WarChargeType)        !List box control field - type derived from field
tra:ExchangeAcc        LIKE(tra:ExchangeAcc)          !List box control field - type derived from field
tra:ExcludeBouncer     LIKE(tra:ExcludeBouncer)       !List box control field - type derived from field
tra:RetailZeroParts    LIKE(tra:RetailZeroParts)      !List box control field - type derived from field
tra:HideDespAdd        LIKE(tra:HideDespAdd)          !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Trade Account File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(168,114,344,184),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('65L(2)|M~Account Number~@s15@119L(2)|M~Company Name~@s30@70L(2)|M~Telephone Numb' &|
   'er~@s15@60L(2)|M~Fax Number~@s15@68L(2)|M~Labour VAT Code~@s2@68L(2)|M~Labour VA' &|
   'T Code~@s2@68L(2)|M~Labour VAT Code~@s2@68L(2)|M~Use Sub Accounts~@s3@68L(2)|M~I' &|
   'nvoice Sub Accounts~@s3@68L(2)|M~Stop Account~@s3@68L(2)|M~Price Despatch Notes~' &|
   '@s3@68L(2)|M~Price Retail Despatch Notes~@s3@68L(2)|M~Print Despatch Complete~@s' &|
   '3@68L(2)|M~Print Despatch Despatch~@s3@68L(2)|M~Standard Repair Type~@s15@120L(2' &|
   ')|M~Courier Incoming~@s30@120L(2)|M~Courier Outgoing~@s30@68L(2)|M~Use Contact N' &|
   'ame~@s3@68L(2)|M~V.A.T. Number~@s30@68L(2)|M~Use Delivery Address~@s3@68L(2)|M~U' &|
   'se Delivery Address~@s3@68R(2)|M~Use Cust Desp Add~L@s3@68R(2)|M~Allow Loan~L@s3' &|
   '@68R(2)|M~Allow Exchange~L@s3@68R(2)|M~Force Fault Codes~L@s3@68R(2)|M~Despatch ' &|
   'Invoiced Jobs~L@s3@68R(2)|M~Despatch Paid Jobs~L@s3@68R(2)|M~Print Despatch Note' &|
   's~L@s3@68R(2)|M~Print Retail Despatch Note~L@s3@68R(2)|M~Print Retail Picking No' &|
   'te~L@s3@68R(2)|M~Despatch Note Per Item~L@s3@68R(2)|M~Skip Despatch~L@s3@68R(2)|' &|
   'M~Summary Despatch Notes~L@s3@120R(2)|M~Exchange Stock Type~L@s30@120R(2)|M~Loan' &|
   ' Stock Type~L@s30@68R(2)|M~Courier Cost~L@n14.2@68R(2)|M~Force Estimate~L@s3@68R' &|
   '(2)|M~Estimate If Over~L@n14.2@120R(2)|M~Turnaround Time~L@s30@68R(2)|M~Retail S' &|
   'ales Payment Type~L@s3@68R(2)|M~Retail Price Structure~L@s3@68R(2)|M~Use Custome' &|
   'r Address~L@s3@68R(2)|M~Use Customer Address~L@s3@68R(2)|M~Zero Chargeable~L@s3@' &|
   '68L(2)|M~Refurb Charge~@s3@120L(2)|M~Charge Type~@s30@120L(2)|M~War Charge Type~' &|
   '@s30@68L(2)|M~Exchange Acc~@s3@68L(2)|M~Exclude From Bouncer~@n1@68L(2)|M~Zero V' &|
   'alue Parts~@n1@68L(2)|M~Hide Address On Despatch Note~@n1@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Trade Account File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,84,352,246),USE(?CurrentTab),FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),SPREAD
                         TAB('By Account Number'),USE(?Tab:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           OPTION,AT(307,97,177,17),USE(ViewAccounts)
                             RADIO('Active'),AT(315,100),USE(?ViewAccounts:Radio1),VALUE('A')
                             RADIO('Inactive'),AT(371,100),USE(?ViewAccounts:Radio2),VALUE('I')
                             RADIO('All'),AT(436,100),USE(?ViewAccounts:Radio3),VALUE('L')
                           END
                           ENTRY(@s15),AT(168,100,124,10),USE(tra:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(168,300),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(236,300),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(304,300),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                           BUTTON,AT(448,300),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepStringClass !STRING          !Xplore: Column displaying tra:Account_Number
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying tra:Account_Number
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying tra:Company_Name
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying tra:Company_Name
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying tra:Labour_VAT_Code
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying tra:Labour_VAT_Code
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying tra:Parts_VAT_Code
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying tra:Parts_VAT_Code
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying tra:Retail_VAT_Code
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying tra:Retail_VAT_Code
Xplore1Step8         StepStringClass !STRING          !Xplore: Column displaying tra:Use_Sub_Accounts
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Sub_Accounts
Xplore1Step9         StepStringClass !STRING          !Xplore: Column displaying tra:Invoice_Sub_Accounts
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying tra:Invoice_Sub_Accounts
Xplore1Step10        StepStringClass !STRING          !Xplore: Column displaying tra:Stop_Account
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying tra:Stop_Account
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying tra:Price_Despatch
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying tra:Price_Despatch
Xplore1Step12        StepStringClass !STRING          !Xplore: Column displaying tra:Price_Retail_Despatch
Xplore1Locator12     IncrementalLocatorClass          !Xplore: Column displaying tra:Price_Retail_Despatch
Xplore1Step13        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Despatch_Complete
Xplore1Locator13     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Despatch_Complete
Xplore1Step14        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Despatch_Despatch
Xplore1Locator14     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Despatch_Despatch
Xplore1Step15        StepStringClass !STRING          !Xplore: Column displaying tra:Standard_Repair_Type
Xplore1Locator15     IncrementalLocatorClass          !Xplore: Column displaying tra:Standard_Repair_Type
Xplore1Step16        StepStringClass !STRING          !Xplore: Column displaying tra:Courier_Incoming
Xplore1Locator16     IncrementalLocatorClass          !Xplore: Column displaying tra:Courier_Incoming
Xplore1Step17        StepStringClass !STRING          !Xplore: Column displaying tra:Courier_Outgoing
Xplore1Locator17     IncrementalLocatorClass          !Xplore: Column displaying tra:Courier_Outgoing
Xplore1Step18        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Contact_Name
Xplore1Locator18     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Contact_Name
Xplore1Step19        StepStringClass !STRING          !Xplore: Column displaying tra:VAT_Number
Xplore1Locator19     IncrementalLocatorClass          !Xplore: Column displaying tra:VAT_Number
Xplore1Step20        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Delivery_Address
Xplore1Locator20     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Delivery_Address
Xplore1Step21        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Collection_Address
Xplore1Locator21     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Collection_Address
Xplore1Step22        StepStringClass !STRING          !Xplore: Column displaying tra:UseCustDespAdd
Xplore1Locator22     IncrementalLocatorClass          !Xplore: Column displaying tra:UseCustDespAdd
Xplore1Step23        StepStringClass !STRING          !Xplore: Column displaying tra:Allow_Loan
Xplore1Locator23     IncrementalLocatorClass          !Xplore: Column displaying tra:Allow_Loan
Xplore1Step24        StepStringClass !STRING          !Xplore: Column displaying tra:Allow_Exchange
Xplore1Locator24     IncrementalLocatorClass          !Xplore: Column displaying tra:Allow_Exchange
Xplore1Step25        StepStringClass !STRING          !Xplore: Column displaying tra:Force_Fault_Codes
Xplore1Locator25     IncrementalLocatorClass          !Xplore: Column displaying tra:Force_Fault_Codes
Xplore1Step26        StepStringClass !STRING          !Xplore: Column displaying tra:Despatch_Invoiced_Jobs
Xplore1Locator26     IncrementalLocatorClass          !Xplore: Column displaying tra:Despatch_Invoiced_Jobs
Xplore1Step27        StepStringClass !STRING          !Xplore: Column displaying tra:Despatch_Paid_Jobs
Xplore1Locator27     IncrementalLocatorClass          !Xplore: Column displaying tra:Despatch_Paid_Jobs
Xplore1Step28        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Despatch_Notes
Xplore1Locator28     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Despatch_Notes
Xplore1Step29        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Retail_Despatch_Note
Xplore1Locator29     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Retail_Despatch_Note
Xplore1Step30        StepStringClass !STRING          !Xplore: Column displaying tra:Print_Retail_Picking_Note
Xplore1Locator30     IncrementalLocatorClass          !Xplore: Column displaying tra:Print_Retail_Picking_Note
Xplore1Step31        StepStringClass !STRING          !Xplore: Column displaying tra:Despatch_Note_Per_Item
Xplore1Locator31     IncrementalLocatorClass          !Xplore: Column displaying tra:Despatch_Note_Per_Item
Xplore1Step32        StepStringClass !STRING          !Xplore: Column displaying tra:Skip_Despatch
Xplore1Locator32     IncrementalLocatorClass          !Xplore: Column displaying tra:Skip_Despatch
Xplore1Step33        StepStringClass !STRING          !Xplore: Column displaying tra:Summary_Despatch_Notes
Xplore1Locator33     IncrementalLocatorClass          !Xplore: Column displaying tra:Summary_Despatch_Notes
Xplore1Step34        StepStringClass !STRING          !Xplore: Column displaying tra:Exchange_Stock_Type
Xplore1Locator34     IncrementalLocatorClass          !Xplore: Column displaying tra:Exchange_Stock_Type
Xplore1Step35        StepStringClass !STRING          !Xplore: Column displaying tra:Loan_Stock_Type
Xplore1Locator35     IncrementalLocatorClass          !Xplore: Column displaying tra:Loan_Stock_Type
Xplore1Step36        StepRealClass   !REAL            !Xplore: Column displaying tra:Courier_Cost
Xplore1Locator36     IncrementalLocatorClass          !Xplore: Column displaying tra:Courier_Cost
Xplore1Step37        StepStringClass !STRING          !Xplore: Column displaying tra:Force_Estimate
Xplore1Locator37     IncrementalLocatorClass          !Xplore: Column displaying tra:Force_Estimate
Xplore1Step38        StepRealClass   !REAL            !Xplore: Column displaying tra:Estimate_If_Over
Xplore1Locator38     IncrementalLocatorClass          !Xplore: Column displaying tra:Estimate_If_Over
Xplore1Step39        StepStringClass !STRING          !Xplore: Column displaying tra:Turnaround_Time
Xplore1Locator39     IncrementalLocatorClass          !Xplore: Column displaying tra:Turnaround_Time
Xplore1Step40        StepStringClass !STRING          !Xplore: Column displaying tra:Retail_Payment_Type
Xplore1Locator40     IncrementalLocatorClass          !Xplore: Column displaying tra:Retail_Payment_Type
Xplore1Step41        StepStringClass !STRING          !Xplore: Column displaying tra:Retail_Price_Structure
Xplore1Locator41     IncrementalLocatorClass          !Xplore: Column displaying tra:Retail_Price_Structure
Xplore1Step42        StepStringClass !STRING          !Xplore: Column displaying tra:Use_Customer_Address
Xplore1Locator42     IncrementalLocatorClass          !Xplore: Column displaying tra:Use_Customer_Address
Xplore1Step43        StepStringClass !STRING          !Xplore: Column displaying tra:Invoice_Customer_Address
Xplore1Locator43     IncrementalLocatorClass          !Xplore: Column displaying tra:Invoice_Customer_Address
Xplore1Step44        StepStringClass !STRING          !Xplore: Column displaying tra:ZeroChargeable
Xplore1Locator44     IncrementalLocatorClass          !Xplore: Column displaying tra:ZeroChargeable
Xplore1Step45        StepStringClass !STRING          !Xplore: Column displaying tra:RefurbCharge
Xplore1Locator45     IncrementalLocatorClass          !Xplore: Column displaying tra:RefurbCharge
Xplore1Step46        StepStringClass !STRING          !Xplore: Column displaying tra:ChargeType
Xplore1Locator46     IncrementalLocatorClass          !Xplore: Column displaying tra:ChargeType
Xplore1Step47        StepStringClass !STRING          !Xplore: Column displaying tra:WarChargeType
Xplore1Locator47     IncrementalLocatorClass          !Xplore: Column displaying tra:WarChargeType
Xplore1Step48        StepStringClass !STRING          !Xplore: Column displaying tra:ExchangeAcc
Xplore1Locator48     IncrementalLocatorClass          !Xplore: Column displaying tra:ExchangeAcc
Xplore1Step49        StepCustomClass !BYTE            !Xplore: Column displaying tra:ExcludeBouncer
Xplore1Locator49     IncrementalLocatorClass          !Xplore: Column displaying tra:ExcludeBouncer
Xplore1Step50        StepCustomClass !BYTE            !Xplore: Column displaying tra:RetailZeroParts
Xplore1Locator50     IncrementalLocatorClass          !Xplore: Column displaying tra:RetailZeroParts
Xplore1Step51        StepCustomClass !BYTE            !Xplore: Column displaying tra:HideDespAdd
Xplore1Locator51     IncrementalLocatorClass          !Xplore: Column displaying tra:HideDespAdd
Xplore1Step52        StepLongClass   !LONG            !Xplore: Column displaying tra:RecordNumber
Xplore1Locator52     IncrementalLocatorClass          !Xplore: Column displaying tra:RecordNumber

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('BrowseTRADEACC','?Browse:1')     !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020292'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseTRADEACC')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:TRADEACC.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRADEACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseTRADEACC')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbe02app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,tra:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,tra:Company_Name,1,BRW1)
  BRW1.AddSortOrder(,tra:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?tra:Account_Number,tra:Account_Number,1,BRW1)
  BRW1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  BRW1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)
  BRW1.AddField(tra:Telephone_Number,BRW1.Q.tra:Telephone_Number)
  BRW1.AddField(tra:Fax_Number,BRW1.Q.tra:Fax_Number)
  BRW1.AddField(tra:Labour_VAT_Code,BRW1.Q.tra:Labour_VAT_Code)
  BRW1.AddField(tra:Parts_VAT_Code,BRW1.Q.tra:Parts_VAT_Code)
  BRW1.AddField(tra:Retail_VAT_Code,BRW1.Q.tra:Retail_VAT_Code)
  BRW1.AddField(tra:Use_Sub_Accounts,BRW1.Q.tra:Use_Sub_Accounts)
  BRW1.AddField(tra:Invoice_Sub_Accounts,BRW1.Q.tra:Invoice_Sub_Accounts)
  BRW1.AddField(tra:Stop_Account,BRW1.Q.tra:Stop_Account)
  BRW1.AddField(tra:Price_Despatch,BRW1.Q.tra:Price_Despatch)
  BRW1.AddField(tra:Price_Retail_Despatch,BRW1.Q.tra:Price_Retail_Despatch)
  BRW1.AddField(tra:Print_Despatch_Complete,BRW1.Q.tra:Print_Despatch_Complete)
  BRW1.AddField(tra:Print_Despatch_Despatch,BRW1.Q.tra:Print_Despatch_Despatch)
  BRW1.AddField(tra:Standard_Repair_Type,BRW1.Q.tra:Standard_Repair_Type)
  BRW1.AddField(tra:Courier_Incoming,BRW1.Q.tra:Courier_Incoming)
  BRW1.AddField(tra:Courier_Outgoing,BRW1.Q.tra:Courier_Outgoing)
  BRW1.AddField(tra:Use_Contact_Name,BRW1.Q.tra:Use_Contact_Name)
  BRW1.AddField(tra:VAT_Number,BRW1.Q.tra:VAT_Number)
  BRW1.AddField(tra:Use_Delivery_Address,BRW1.Q.tra:Use_Delivery_Address)
  BRW1.AddField(tra:Use_Collection_Address,BRW1.Q.tra:Use_Collection_Address)
  BRW1.AddField(tra:UseCustDespAdd,BRW1.Q.tra:UseCustDespAdd)
  BRW1.AddField(tra:Allow_Loan,BRW1.Q.tra:Allow_Loan)
  BRW1.AddField(tra:Allow_Exchange,BRW1.Q.tra:Allow_Exchange)
  BRW1.AddField(tra:Force_Fault_Codes,BRW1.Q.tra:Force_Fault_Codes)
  BRW1.AddField(tra:Despatch_Invoiced_Jobs,BRW1.Q.tra:Despatch_Invoiced_Jobs)
  BRW1.AddField(tra:Despatch_Paid_Jobs,BRW1.Q.tra:Despatch_Paid_Jobs)
  BRW1.AddField(tra:Print_Despatch_Notes,BRW1.Q.tra:Print_Despatch_Notes)
  BRW1.AddField(tra:Print_Retail_Despatch_Note,BRW1.Q.tra:Print_Retail_Despatch_Note)
  BRW1.AddField(tra:Print_Retail_Picking_Note,BRW1.Q.tra:Print_Retail_Picking_Note)
  BRW1.AddField(tra:Despatch_Note_Per_Item,BRW1.Q.tra:Despatch_Note_Per_Item)
  BRW1.AddField(tra:Skip_Despatch,BRW1.Q.tra:Skip_Despatch)
  BRW1.AddField(tra:Summary_Despatch_Notes,BRW1.Q.tra:Summary_Despatch_Notes)
  BRW1.AddField(tra:Exchange_Stock_Type,BRW1.Q.tra:Exchange_Stock_Type)
  BRW1.AddField(tra:Loan_Stock_Type,BRW1.Q.tra:Loan_Stock_Type)
  BRW1.AddField(tra:Courier_Cost,BRW1.Q.tra:Courier_Cost)
  BRW1.AddField(tra:Force_Estimate,BRW1.Q.tra:Force_Estimate)
  BRW1.AddField(tra:Estimate_If_Over,BRW1.Q.tra:Estimate_If_Over)
  BRW1.AddField(tra:Turnaround_Time,BRW1.Q.tra:Turnaround_Time)
  BRW1.AddField(tra:Retail_Payment_Type,BRW1.Q.tra:Retail_Payment_Type)
  BRW1.AddField(tra:Retail_Price_Structure,BRW1.Q.tra:Retail_Price_Structure)
  BRW1.AddField(tra:Use_Customer_Address,BRW1.Q.tra:Use_Customer_Address)
  BRW1.AddField(tra:Invoice_Customer_Address,BRW1.Q.tra:Invoice_Customer_Address)
  BRW1.AddField(tra:ZeroChargeable,BRW1.Q.tra:ZeroChargeable)
  BRW1.AddField(tra:RefurbCharge,BRW1.Q.tra:RefurbCharge)
  BRW1.AddField(tra:ChargeType,BRW1.Q.tra:ChargeType)
  BRW1.AddField(tra:WarChargeType,BRW1.Q.tra:WarChargeType)
  BRW1.AddField(tra:ExchangeAcc,BRW1.Q.tra:ExchangeAcc)
  BRW1.AddField(tra:ExcludeBouncer,BRW1.Q.tra:ExcludeBouncer)
  BRW1.AddField(tra:RetailZeroParts,BRW1.Q.tra:RetailZeroParts)
  BRW1.AddField(tra:HideDespAdd,BRW1.Q.tra:HideDespAdd)
  BRW1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:TRADEACC.Close
    Relate:USERS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('BrowseTRADEACC','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  ! Save Window Name
   AddToLog('Window','Close','BrowseTRADEACC')
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            If SecurityCheck('TRADE ACCOUNTS - INSERT')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
                do_update# = false
            end
        of changerecord
            If SecurityCheck('TRADE ACCOUNTS - CHANGE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
        of deleterecord
            If SecurityCheck('TRADE ACCOUNTS - DELETE')
                Case Missive('You do not have access to this option.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
  
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTRADEACC
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  Xplore1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)
  Xplore1.AddField(tra:Telephone_Number,BRW1.Q.tra:Telephone_Number)
  Xplore1.AddField(tra:Fax_Number,BRW1.Q.tra:Fax_Number)
  Xplore1.AddField(tra:Labour_VAT_Code,BRW1.Q.tra:Labour_VAT_Code)
  Xplore1.AddField(tra:Parts_VAT_Code,BRW1.Q.tra:Parts_VAT_Code)
  Xplore1.AddField(tra:Retail_VAT_Code,BRW1.Q.tra:Retail_VAT_Code)
  Xplore1.AddField(tra:Use_Sub_Accounts,BRW1.Q.tra:Use_Sub_Accounts)
  Xplore1.AddField(tra:Invoice_Sub_Accounts,BRW1.Q.tra:Invoice_Sub_Accounts)
  Xplore1.AddField(tra:Stop_Account,BRW1.Q.tra:Stop_Account)
  Xplore1.AddField(tra:Price_Despatch,BRW1.Q.tra:Price_Despatch)
  Xplore1.AddField(tra:Price_Retail_Despatch,BRW1.Q.tra:Price_Retail_Despatch)
  Xplore1.AddField(tra:Print_Despatch_Complete,BRW1.Q.tra:Print_Despatch_Complete)
  Xplore1.AddField(tra:Print_Despatch_Despatch,BRW1.Q.tra:Print_Despatch_Despatch)
  Xplore1.AddField(tra:Standard_Repair_Type,BRW1.Q.tra:Standard_Repair_Type)
  Xplore1.AddField(tra:Courier_Incoming,BRW1.Q.tra:Courier_Incoming)
  Xplore1.AddField(tra:Courier_Outgoing,BRW1.Q.tra:Courier_Outgoing)
  Xplore1.AddField(tra:Use_Contact_Name,BRW1.Q.tra:Use_Contact_Name)
  Xplore1.AddField(tra:VAT_Number,BRW1.Q.tra:VAT_Number)
  Xplore1.AddField(tra:Use_Delivery_Address,BRW1.Q.tra:Use_Delivery_Address)
  Xplore1.AddField(tra:Use_Collection_Address,BRW1.Q.tra:Use_Collection_Address)
  Xplore1.AddField(tra:UseCustDespAdd,BRW1.Q.tra:UseCustDespAdd)
  Xplore1.AddField(tra:Allow_Loan,BRW1.Q.tra:Allow_Loan)
  Xplore1.AddField(tra:Allow_Exchange,BRW1.Q.tra:Allow_Exchange)
  Xplore1.AddField(tra:Force_Fault_Codes,BRW1.Q.tra:Force_Fault_Codes)
  Xplore1.AddField(tra:Despatch_Invoiced_Jobs,BRW1.Q.tra:Despatch_Invoiced_Jobs)
  Xplore1.AddField(tra:Despatch_Paid_Jobs,BRW1.Q.tra:Despatch_Paid_Jobs)
  Xplore1.AddField(tra:Print_Despatch_Notes,BRW1.Q.tra:Print_Despatch_Notes)
  Xplore1.AddField(tra:Print_Retail_Despatch_Note,BRW1.Q.tra:Print_Retail_Despatch_Note)
  Xplore1.AddField(tra:Print_Retail_Picking_Note,BRW1.Q.tra:Print_Retail_Picking_Note)
  Xplore1.AddField(tra:Despatch_Note_Per_Item,BRW1.Q.tra:Despatch_Note_Per_Item)
  Xplore1.AddField(tra:Skip_Despatch,BRW1.Q.tra:Skip_Despatch)
  Xplore1.AddField(tra:Summary_Despatch_Notes,BRW1.Q.tra:Summary_Despatch_Notes)
  Xplore1.AddField(tra:Exchange_Stock_Type,BRW1.Q.tra:Exchange_Stock_Type)
  Xplore1.AddField(tra:Loan_Stock_Type,BRW1.Q.tra:Loan_Stock_Type)
  Xplore1.AddField(tra:Courier_Cost,BRW1.Q.tra:Courier_Cost)
  Xplore1.AddField(tra:Force_Estimate,BRW1.Q.tra:Force_Estimate)
  Xplore1.AddField(tra:Estimate_If_Over,BRW1.Q.tra:Estimate_If_Over)
  Xplore1.AddField(tra:Turnaround_Time,BRW1.Q.tra:Turnaround_Time)
  Xplore1.AddField(tra:Retail_Payment_Type,BRW1.Q.tra:Retail_Payment_Type)
  Xplore1.AddField(tra:Retail_Price_Structure,BRW1.Q.tra:Retail_Price_Structure)
  Xplore1.AddField(tra:Use_Customer_Address,BRW1.Q.tra:Use_Customer_Address)
  Xplore1.AddField(tra:Invoice_Customer_Address,BRW1.Q.tra:Invoice_Customer_Address)
  Xplore1.AddField(tra:ZeroChargeable,BRW1.Q.tra:ZeroChargeable)
  Xplore1.AddField(tra:RefurbCharge,BRW1.Q.tra:RefurbCharge)
  Xplore1.AddField(tra:ChargeType,BRW1.Q.tra:ChargeType)
  Xplore1.AddField(tra:WarChargeType,BRW1.Q.tra:WarChargeType)
  Xplore1.AddField(tra:ExchangeAcc,BRW1.Q.tra:ExchangeAcc)
  Xplore1.AddField(tra:ExcludeBouncer,BRW1.Q.tra:ExcludeBouncer)
  Xplore1.AddField(tra:RetailZeroParts,BRW1.Q.tra:RetailZeroParts)
  Xplore1.AddField(tra:HideDespAdd,BRW1.Q.tra:HideDespAdd)
  Xplore1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?ViewAccounts
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewAccounts, Accepted)
      brw1.resetsort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewAccounts, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020292'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020292'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020292'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  !TB12488 Part 2 : Hide inactive trade accounts   J 26/06/2012
  Case ViewAccounts
      of 'A'  !Active
          if tra:Stop_Account ='YES' then return(record:filtered).
      of 'I'  !inactive
          if tra:Stop_Account <> 'YES' then return(record:filtered).
      !of 'L'  !all not needed
  END
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !tra:Account_Number
  OF 2 !tra:Company_Name
  OF 3 !tra:Telephone_Number
        ColumnString     = SUB(LEFT(BRW1.Q.tra:Telephone_Number),1,20)
  OF 4 !tra:Fax_Number
        ColumnString     = SUB(LEFT(BRW1.Q.tra:Fax_Number),1,20)
  OF 5 !tra:Labour_VAT_Code
  OF 6 !tra:Parts_VAT_Code
  OF 7 !tra:Retail_VAT_Code
  OF 8 !tra:Use_Sub_Accounts
  OF 9 !tra:Invoice_Sub_Accounts
  OF 10 !tra:Stop_Account
  OF 11 !tra:Price_Despatch
  OF 12 !tra:Price_Retail_Despatch
  OF 13 !tra:Print_Despatch_Complete
  OF 14 !tra:Print_Despatch_Despatch
  OF 15 !tra:Standard_Repair_Type
  OF 16 !tra:Courier_Incoming
  OF 17 !tra:Courier_Outgoing
  OF 18 !tra:Use_Contact_Name
  OF 19 !tra:VAT_Number
  OF 20 !tra:Use_Delivery_Address
  OF 21 !tra:Use_Collection_Address
  OF 22 !tra:UseCustDespAdd
  OF 23 !tra:Allow_Loan
  OF 24 !tra:Allow_Exchange
  OF 25 !tra:Force_Fault_Codes
  OF 26 !tra:Despatch_Invoiced_Jobs
  OF 27 !tra:Despatch_Paid_Jobs
  OF 28 !tra:Print_Despatch_Notes
  OF 29 !tra:Print_Retail_Despatch_Note
  OF 30 !tra:Print_Retail_Picking_Note
  OF 31 !tra:Despatch_Note_Per_Item
  OF 32 !tra:Skip_Despatch
  OF 33 !tra:Summary_Despatch_Notes
  OF 34 !tra:Exchange_Stock_Type
  OF 35 !tra:Loan_Stock_Type
  OF 36 !tra:Courier_Cost
  OF 37 !tra:Force_Estimate
  OF 38 !tra:Estimate_If_Over
  OF 39 !tra:Turnaround_Time
  OF 40 !tra:Retail_Payment_Type
  OF 41 !tra:Retail_Price_Structure
  OF 42 !tra:Use_Customer_Address
  OF 43 !tra:Invoice_Customer_Address
  OF 44 !tra:ZeroChargeable
  OF 45 !tra:RefurbCharge
  OF 46 !tra:ChargeType
  OF 47 !tra:WarChargeType
  OF 48 !tra:ExchangeAcc
  OF 49 !tra:ExcludeBouncer
  OF 50 !tra:RetailZeroParts
  OF 51 !tra:HideDespAdd
  OF 52 !tra:RecordNumber
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step52)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?tra:Account_Number,tra:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,tra:Company_Name,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,tra:Labour_VAT_Code,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,tra:Parts_VAT_Code,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,tra:Retail_VAT_Code,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,tra:Use_Sub_Accounts,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,tra:Invoice_Sub_Accounts,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,tra:Stop_Account,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,tra:Price_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,tra:Price_Retail_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,tra:Print_Despatch_Complete,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,tra:Print_Despatch_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step15.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator15)
      Xplore1Locator15.Init(,tra:Standard_Repair_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step16.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator16)
      Xplore1Locator16.Init(,tra:Courier_Incoming,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step17.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator17)
      Xplore1Locator17.Init(,tra:Courier_Outgoing,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step18.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator18)
      Xplore1Locator18.Init(,tra:Use_Contact_Name,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step19.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator19)
      Xplore1Locator19.Init(,tra:VAT_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step20.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator20)
      Xplore1Locator20.Init(,tra:Use_Delivery_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step21.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator21)
      Xplore1Locator21.Init(,tra:Use_Collection_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step22.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator22)
      Xplore1Locator22.Init(,tra:UseCustDespAdd,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step23.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator23)
      Xplore1Locator23.Init(,tra:Allow_Loan,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step24.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator24)
      Xplore1Locator24.Init(,tra:Allow_Exchange,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step25.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator25)
      Xplore1Locator25.Init(,tra:Force_Fault_Codes,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step26.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator26)
      Xplore1Locator26.Init(,tra:Despatch_Invoiced_Jobs,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step27.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator27)
      Xplore1Locator27.Init(,tra:Despatch_Paid_Jobs,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step28.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator28)
      Xplore1Locator28.Init(,tra:Print_Despatch_Notes,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step29.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator29)
      Xplore1Locator29.Init(,tra:Print_Retail_Despatch_Note,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step30.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator30)
      Xplore1Locator30.Init(,tra:Print_Retail_Picking_Note,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step31.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator31)
      Xplore1Locator31.Init(,tra:Despatch_Note_Per_Item,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step32.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator32)
      Xplore1Locator32.Init(,tra:Skip_Despatch,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step33.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator33)
      Xplore1Locator33.Init(,tra:Summary_Despatch_Notes,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step34.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator34)
      Xplore1Locator34.Init(,tra:Exchange_Stock_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step35.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator35)
      Xplore1Locator35.Init(,tra:Loan_Stock_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step36.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator36)
      Xplore1Locator36.Init(,tra:Courier_Cost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step37.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator37)
      Xplore1Locator37.Init(,tra:Force_Estimate,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step38.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator38)
      Xplore1Locator38.Init(,tra:Estimate_If_Over,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step39.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator39)
      Xplore1Locator39.Init(,tra:Turnaround_Time,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step40.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator40)
      Xplore1Locator40.Init(,tra:Retail_Payment_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step41.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator41)
      Xplore1Locator41.Init(,tra:Retail_Price_Structure,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step42.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator42)
      Xplore1Locator42.Init(,tra:Use_Customer_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step43.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator43)
      Xplore1Locator43.Init(,tra:Invoice_Customer_Address,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step44.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator44)
      Xplore1Locator44.Init(,tra:ZeroChargeable,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step45.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator45)
      Xplore1Locator45.Init(,tra:RefurbCharge,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step46.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator46)
      Xplore1Locator46.Init(,tra:ChargeType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step47.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator47)
      Xplore1Locator47.Init(,tra:WarChargeType,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step48.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator48)
      Xplore1Locator48.Init(,tra:ExchangeAcc,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step49.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator49)
      Xplore1Locator49.Init(,tra:ExcludeBouncer,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step50.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator50)
      Xplore1Locator50.Init(,tra:RetailZeroParts,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step51.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator51)
      Xplore1Locator51.Init(,tra:HideDespAdd,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step52.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator52)
      Xplore1Locator52.Init(,tra:RecordNumber,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(TRADEACC)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('tra:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Company_Name')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Company Name')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Company Name')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Telephone_Number')       !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Telephone Number')           !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Telephone Number')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tra:Fax_Number')             !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Fax Number')                 !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Fax Number')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tra:Labour_VAT_Code')        !Field Name
                SHORT(8)                              !Default Column Width
                PSTRING('Labour VAT Code')            !Header
                PSTRING('@s2')                        !Picture
                PSTRING('Labour VAT Code')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Parts_VAT_Code')         !Field Name
                SHORT(8)                              !Default Column Width
                PSTRING('Labour VAT Code')            !Header
                PSTRING('@s2')                        !Picture
                PSTRING('Labour VAT Code')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Retail_VAT_Code')        !Field Name
                SHORT(8)                              !Default Column Width
                PSTRING('Labour VAT Code')            !Header
                PSTRING('@s2')                        !Picture
                PSTRING('Labour VAT Code')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Sub_Accounts')       !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Sub Accounts')           !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Sub Accounts')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Invoice_Sub_Accounts')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Invoice Sub Accounts')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Invoice Sub Accounts')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Stop_Account')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Stop Account')               !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Stop Account')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Price_Despatch')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Price Despatch Notes')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Price Despatch Notes')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Price_Retail_Despatch')  !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Price Retail Despatch Notes') !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Price Retail Despatch Notes') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Despatch_Complete') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Despatch Complete')    !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Despatch Note At Completion') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Despatch_Despatch') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Despatch Despatch')    !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Despatch Note At Despatch') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Standard_Repair_Type')   !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Standard Repair Type')       !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Standard Repair Type')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Courier_Incoming')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier Incoming')           !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier Incoming')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Courier_Outgoing')       !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier Outgoing')           !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier Outgoing')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Contact_Name')       !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Contact Name')           !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Contact Name')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:VAT_Number')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('V.A.T. Number')              !Header
                PSTRING('@s30')                       !Picture
                PSTRING('V.A.T. Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Delivery_Address')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Delivery Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Delivery Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Collection_Address') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Delivery Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Delivery Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:UseCustDespAdd')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Cust Desp Add')          !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Customer Address As Despatch Address') !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Allow_Loan')             !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Allow Loan')                 !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Allow Loan')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Allow_Exchange')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Allow Exchange')             !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Allow Exchange')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Force_Fault_Codes')      !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Force Fault Codes')          !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Force Fault Codes')          !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Despatch_Invoiced_Jobs') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatch Invoiced Jobs')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatch Invoiced Jobs')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Despatch_Paid_Jobs')     !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatch Paid Jobs')         !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatch Paid Jobs')         !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Despatch_Notes')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Despatch Notes')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Despatch Notes')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Retail_Despatch_Note') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Retail Despatch Note') !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Retail Despatch Note') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Print_Retail_Picking_Note') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Print Retail Picking Note')  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Print Retail Picking Note')  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Despatch_Note_Per_Item') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatch Note Per Item')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatch Note Per Item')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Skip_Despatch')          !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Skip Despatch')              !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Batch Despatch')         !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Summary_Despatch_Notes') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Summary Despatch Notes')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Summary Despatch Notes')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Exchange_Stock_Type')    !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Exchange Stock Type')        !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Exchange Stock Type')        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Loan_Stock_Type')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Loan Stock Type')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Loan Stock Type')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Courier_Cost')           !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Courier Cost')               !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Courier Cost')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Force_Estimate')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Force Estimate')             !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Force Estimate')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Estimate_If_Over')       !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Estimate If Over')           !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Estimate If Over')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Turnaround_Time')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Turnaround Time')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Turnaround Time')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Retail_Payment_Type')    !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Retail Sales Payment Type')  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Retail Sales Payment Type')  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Retail_Price_Structure') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Retail Price Structure')     !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Retail Price Structure')     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Use_Customer_Address')   !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Customer Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Customer Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:Invoice_Customer_Address') !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Use Customer Address')       !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Use Customer Address')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ZeroChargeable')         !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Zero Chargeable')            !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Don''t show Chargeable Costs') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:RefurbCharge')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Refurb Charge')              !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Refurb Charge')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ChargeType')             !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Charge Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Charge Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:WarChargeType')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('War Charge Type')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('War Charge Type')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ExchangeAcc')            !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Exchange Acc')               !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Account used for repairing exchange units') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:ExcludeBouncer')         !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Exclude From Bouncer')       !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Exclude From Bouncer')       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:RetailZeroParts')        !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Zero Value Parts')           !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Zero Value Parts')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:HideDespAdd')            !Field Name
                SHORT(4)                              !Default Column Width
                PSTRING('Hide Address On Despatch Note') !Header
                PSTRING('@n1')                        !Picture
                PSTRING('Hide Address On Despatch Note') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('tra:RecordNumber')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Entry Long')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(52)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('tra:Telephone_Number')       !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                PSTRING('tra:Fax_Number')             !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                END
XpSortDim       SHORT(2)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)

Rapid_Exchange_Unit PROCEDURE (ordPartsID)            !Generated from procedure template - Window

tmp:StockType        STRING(30)
save_xch_ali_id      USHORT,AUTO
tmp:count            LONG
tmp:UnitQueue        QUEUE,PRE(untque)
esn                  STRING(16)
number               LONG
                     END
tmp:ModelNumber      STRING(30)
tmp:Location         STRING(30)
tmp:ShelfLocation    STRING(30)
tmp:Manufacturer     STRING(30)
tmp:esn              STRING(16)
tmp:MSN              STRING(30)
tmp:AuditNumber      LONG
tmp:AssignAuditNumbers BYTE(0)
tmp:stockRecordFound BYTE(0)
tmp:Information      STRING(255)
ReturningHandset     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ImportFileName       CSTRING(255),STATIC
ExceptionFileName    CSTRING(255),STATIC
Importing            BYTE
NotImporting         BYTE
ImportError          STRING(100)
window               WINDOW('Rapid Exchange Item Inserting'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       SHEET,AT(164,82,352,248),USE(?Sheet1),BELOW,COLOR(0D6E7EFH),SPREAD
                         TAB,USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Common Fields'),AT(236,102),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Insert the fields that will be common to all the inserted Exchange Units.'),AT(236,116,210,24),USE(?Prompt3),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Manufacturer'),AT(235,180),USE(?Prompt4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Stock Type'),AT(235,160),USE(?Prompt4:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(303,160,112,10),USE(tmp:StockType),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(428,156),USE(?CallLookup),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(303,180,112,10),USE(tmp:Manufacturer),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),UPR
                           BUTTON,AT(428,176),USE(?CallLookup:2),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(303,200,112,10),USE(tmp:ModelNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(428,196),USE(?CallLookup:3),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Model Number'),AT(235,200),USE(?Prompt4:3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Location'),AT(235,222),USE(?Prompt4:4),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(303,222,112,10),USE(tmp:Location),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(428,218),USE(?CallLookup:4),TRN,FLAT,ICON('lookupp.jpg')
                           ENTRY(@s30),AT(303,244,112,10),USE(tmp:ShelfLocation),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(428,240),USE(?CallLookup:5),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Shelf Location'),AT(235,244),USE(?Prompt4:5),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         END
                         TAB('Tab 2'),USE(?Tab2),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('Create New Units'),AT(232,102),USE(?Prompt10),FONT(,,080FFFFH,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('Insert an I.M.E.I. to insert the unit into the Exchange Stock.'),AT(232,114),USE(?Prompt9),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Assign Audit Numbers'),AT(300,126),USE(tmp:AssignAuditNumbers),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Assign Audit Numbers'),TIP('Assign Audit Numbers'),VALUE('1','0')
                           PROMPT('Units Successfully inserted.'),AT(276,190),USE(?Prompt9:2),FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                           PROMPT('I.M.E.I. Number'),AT(232,158),USE(?tmp:esn:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s16),AT(328,158,124,10),USE(tmp:esn),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(232,174),USE(?tmp:MSN:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s30),AT(328,174,124,10),USE(tmp:MSN),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('M.'),TIP('M.'),UPR
                           PROMPT('Audit Number'),AT(232,138),USE(?tmp:AuditNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@s8),AT(300,138,36,10),USE(tmp:AuditNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(340,134),USE(?btnPickAuditNo),TRN,FLAT,ICON('lookupp.jpg')
                           BUTTON,AT(448,128),USE(?NewAuditNo),TRN,FLAT,LEFT,ICON('newaudp.jpg')
                           LIST,AT(276,200,128,112),USE(?List1),VSCROLL,COLOR(COLOR:White),FORMAT('64L(2)|M~I.M.E.I. Number~@s16@'),FROM(tmp:UnitQueue)
                         END
                         TAB('Tab 3'),USE(?Tab3),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           BUTTON,AT(400,170),USE(?LookupFile),FLAT,ICON('lookupp.jpg')
                           PROMPT('Filename'),AT(244,174),USE(?Prompt16),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s255),AT(284,174,112,10),USE(ImportFileName),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                           BUTTON,AT(307,207),USE(?ButtonImport),TRN,FLAT,ICON('impordp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                       BUTTON,AT(232,332),USE(?Next),TRN,FLAT,LEFT,ICON('nextp.jpg')
                       BUTTON,AT(168,332),USE(?back),DISABLE,TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(460,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Rapid Exchange Units Insert Wizard'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup9          SelectFileClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
ImportFile          File,DRIVER('BASIC'),PRE(impfil),Name(ImportFileName),CREATE,BINDABLE,THREAD
RECORD                  RECORD
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
IMEINumber                  STRING(30)
MSNNumber                   STRING(30)
                        END !record
                    END  !definiton



ExceptionFile          File,DRIVER('BASIC'),PRE(excfil),Name(ExceptionFileName),CREATE,BINDABLE,THREAD
RECORD                  RECORD
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
IMEINumber                  STRING(30)
MSNNumber                   STRING(30)
Reason                      STRING(255)
                        END
                    END
!Save Entry Fields Incase Of Lookup
look:tmp:StockType                Like(tmp:StockType)
look:tmp:Manufacturer                Like(tmp:Manufacturer)
look:tmp:ModelNumber                Like(tmp:ModelNumber)
look:tmp:Location                Like(tmp:Location)
look:tmp:ShelfLocation                Like(tmp:ShelfLocation)
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
TheImport       Routine

    if clip(GETINI('EXCEPTION','EXCHANGE','',CLIP(Path()) & '\SB2KDEF.INI')) = '' or |
       clip(GETINI('EXCEPTION','EXCHANGESAVE','',CLIP(Path()) & '\SB2KDEF.INI')) = '' then

        miss# = missive('Folders required for this sytem have not been set up. Please check the defaults',|
                        'ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END

    Open(ImportFile)
    if error() then
        miss# = missive('Error on opening the import file '&clip(ImportFileName)&' Please check and start again',|
                        'ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END

    !first pass check all manufacturers and models are identical and error if not
    Error# = 0
    Set(ImportFile)
    next(ImportFile) !to get past the header
    if error() then
        miss# = missive('Error on reading the import file '&clip(ImportFileName)&' Please check and start again',|
                        'ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END
    !now for the rest
    Loop

        next(ImportFile)
        if Error() then break.
        !message('Matching '& clip(impfil:Manufacturer) &':'& clip(tmp:Manufacturer))
        !message('Matching '& clip(impfil:ModelNumber)  &':'& clip(tmp:ModelNumber))

        if upper(impfil:Manufacturer) <> upper(tmp:Manufacturer) then
            Error# = 1
            !message('Manufacturer mismatch')
            break
        END
        if upper(impfil:ModelNumber)  <> upper(tmp:ModelNumber)
            !message('Model mismatch')
            Error# = 1
            break
        END

    END !loop
    if error# = 1 then

        Miss# = missive('The CSV file contains different manufacturers or models. The CSV file will not be imported.',|
                        'ServiceBase 3g','mexclam.jpg','OK')

       close(importFile)
        EXIT

    END

    !Second pass - import the handsets
    Set(ImportFile)
    next(ImportFile) !to get past the header
    if error() then
        miss# = missive('Error on reading the import file '&clip(ImportFileName)&' Please check and start again',|
                        'ServiceBase 3g','mexclam.jpg','OK')
        EXIT
    END
    !now for the rest

    Loop

        next(ImportFile)
        if Error() then break.

        tmp:esn          = impfil:IMEINumber
        tmp:MSN          = impfil:MSNNumber

        do AddUnit

        if error# = 1 then
            Do ImportException
        END
        
    END !loop

    if clip(ExceptionFileName) <> '' then
        !something was added to the exceptioins
        CLOSE(ExceptionFile)
        miss# = missive('Errors were detected during this import please check the exceptions file',|
                        'ServiceBase 3g','mexclam.jpg','OK')
    ELSE
        miss# = missive('IMEI CSV file imported sucessfully',|
                        'ServiceBase 3g','mexclam.jpg','OK')
    END
    close(ImportFile)

    !backup the import file
    copy(ImportFileName, clip(GETINI('EXCEPTION','EXCHANGESAVE','',CLIP(Path()) & '\SB2KDEF.INI')))

    EXIT


ImportException         Routine

    if clip(ExceptionFileName) = '' then
        !this has not been set up yet
        ExceptionFileName = getini('EXCEPTION','EXCHANGE','C:',CLIP(Path()) & '\SB2KDEF.INI')&  |
                                   '\Bulk_Receipt_Exch_'&format(today(),@D10-)&'_'&format(clock(),@t5-)&'.csv'

        if exists(ExceptionFileName) then remove(ExceptionFileName).
      
        CREATE(ExceptionFile)
        Open(ExceptionFile)
        
        excfil:Manufacturer  = 'Manufacturer'
        excfil:ModelNumber   = 'Model Number'
        excfil:IMEInumber    = 'IMEI Number'
        excfil:MSNNumber     = 'MSN Number'
        excfil:Reason        = 'Reason'
        Add(ExceptionFile)
    END

    excfil:Manufacturer  = impfil:Manufacturer
    excfil:ModelNumber   = impfil:ModelNumber
    excfil:IMEInumber    = impfil:IMEINumber
    excfil:MSNNumber     = impfil:MSNNumber
    excfil:Reason        = ImportError
    Add(ExceptionFile)

    exit
AddUnit     Routine                 

    error# = 0
    ReturningHandset = 0

    setcursor(cursor:wait)

    !TB13554 - J - 14/08/15 will need manufacturer record
    if clip(tmp:Manufacturer) = '' then
        !try to look it up from model number
        if clip(tmp:ModelNumber) = '' then
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = xch:Model_Number
            If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                tmp:Manufacturer = mod:Manufacturer
            END
        END   !no model number = no manufacturer
    END !if clip(tmp:Manufacturer) = ''
    if clip(tmp:Manufacturer) <> '' then
        Access:Manufact.clearkey(man:Manufacturer_Key)
        man:Manufacturer = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
            man:ApplyMSNFormat = 0
        END !if manufact did not fetch
    ELSE  !if clip(tmp:Manufacturer) <> ''
        man:ApplyMSNFormat = 0
    END !if clip(tmp:Manufacturer) <> ''
    !TB13554 - J - 14/08/15 end lookup manufacturer record


    save_xch_ali_id = access:exchange_alias.savefile()
    access:exchange_alias.clearkey(xch_ali:esn_only_key)
    xch_ali:esn = tmp:esn
    set(xch_ali:esn_only_key,xch_ali:esn_only_key)
    loop
        if access:exchange_alias.next()
           break
        end !if
        if xch_ali:esn <> tmp:esn      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        !tb 12461 - if previously not avialable and in 'RETURN TO MANUFACTURER' then this can go through
        If xch_ali:Location = 'RETURN TO MANUFACTURER' and xch_ali:Available = 'NOA'
            ReturningHandset = xch_ali:Ref_Number       !remembers where in the file and acts as a flag
        ELSE
            if importing then
                ImportError = 'This I.M.E.I. Number already exists in Exchange Stock.'
            ELSE
                Case Missive('This I.M.E.I. Number already exists in Exchange Stock.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            END !if importing
            error# = 1
            ReturningHandset = 0

        END !RTM and NVL
    end !loop

    !whilst this is available check for the MSN as well
    if error# = 0  and clip(tmp:msn) <> '' then
        Access:Exchange_Alias.clearkey(xch_ali:MSN_Only_Key)
        xch_ali:MSN = tmp:msn
        set(xch_ali:MSN_Only_Key,xch_ali:MSN_Only_Key)
        !only need to find one
        if access:Exchange_Alias.next() = level:benign then
            if xch_ali:MSN = tmp:msn then
                If importing then
                    importError = 'This MSN already exists in Exchange Stock'
                ELSE
                    miss# =  Missive('This MSN Number already exists in Exchange Stock.','ServiceBase 3g','mstop.jpg','/OK')
                END !if importin
                Error# = 1

            END !if matching MSN
        END !if next()
    END !if this is not a returning handset

    access:exchange_alias.restorefile(save_xch_ali_id)
    setcursor()

    If error# = 0
        access:loan.clearkey(loa:esn_only_key)
        loa:esn = tmp:esn
        if access:loan.fetch(loa:esn_only_key) = Level:Benign
            if importing then
                ImportError = 'This I.M.E.I. Number has already been entered as a Loan Stock Item.'
            ELSE
                Case Missive('This I.M.E.I. Number has already been entered as a Loan Stock Item.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                    Of 1 ! OK Button
                End ! Case Missive
            END !if imporitng
            error# = 1
        End!if access:loan.fetch(loa:esn_key) = Level:Benign
    End!If error# = 0

    If error# = 0
        access:jobs_alias.clearkey(job_ali:esn_key)
        job_ali:esn = tmp:esn
        if access:jobs_alias.fetch(job_ali:esn_key) = Level:Benign
           If job_ali:date_completed = ''
                error# = 1
                if importing then
                    ImportError = 'You are attempting to insert an Exchange Unit with the same I.M.E.I. Number as job number ' & Clip(job_ali:Ref_Number) & '. You must use Rapid Job Update to return this unit to Exchange Stock.'
                ELSE
                    Case Missive('You are attempting to insert an Exchange Unit with the same I.M.E.I. Number as job number ' & Clip(job_ali:Ref_Number) & '. You must use Rapid Job Update if you wish to return this unit to Exchange Stock.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                        Of 1 ! OK Button
                    End ! Case Missive
                END !if importing
           End!If job:date_completed <> ''
        end!if access:jobs.fetch(job:esn_key) = Level:Benign
    end!IF error# = 0
    If error# = 0
        If CheckLength('IMEI',tmp:ModelNumber,tmp:ESN,Importing)    !TBN12433 - adds a silent mode to the checklength
            ImportError = glo:ErrorText
            Error# = 1
        End !If CheckLength('IMEI',tmp:ModelNumber,tmp:ESN)

        If ?tmp:MSN{prop:Hide} = 0 And Error# = 0
            If CheckLength('MSN',tmp:ModelNumber,tmp:MSN,Importing)    !TBN12433 - adds a silent mode to the checklength)
                importError = glo:ErrorText     !'This MSN fails the length checks for '&clip(tmp:ModelNumber)
                Error# = 1
            End !If CheckLength('MSN',tmp:ModelNumber,tmp:MSN)

            !TB13554 - J - 14/08/15 check format of MSN if it is here to check
            If Error# = 0
                If man:ApplyMSNFormat
                    If CheckFaultFormat(tmp:MSN,man:MSNFormat)
                        If Importing then
                            ImportError = 'The M.S.N. has failed format validation.'
                        ELSE
                            Case Missive('The M.S.N. has failed format validation.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive
                        END !if importing
                        tmp:MSN = ''
                        Error# = 1
                    End !If CheckFieldFormat(xch:MSN,man:MSNFormat)
                End !If man:ApplyMSNFormat
            END !if error# = 0
            !!TB13554 - J - 14/08/15 END check format of MSN

        End !If ?tmp:MSN{prop:Hide} = 0
    End!IF error# = 0


    ! Inserting (DBH 01/06/2006) #6972 - Check the validity of the imei number
    If error# = 0
        If IsIMEIValid(tmp:ESN,tmp:ModelNumber,notimporting) = False          !if importing want no messages
            ImportError = 'This IMEI is not valid for '&clip(tmp:ModelNumber)
            error# = 1
        End ! If IsIMEIValid(tmp:ESN,tmp:ModelNumber,1) = False
    End ! If error# = 0
    ! End (DBH 01/06/2006) #6972
    If error# = 1
        if not importing then
            Select(?tmp:esn)
        END
    Else
        !everything checks out fine
        if ReturningHandset > 0 then
            !update existing handset
            Access:Exchange.clearkey(xch:Ref_Number_Key)
            xch:Ref_Number = ReturningHandset
            if Access:Exchange.fetch(xch:Ref_Number_Key)
                !error
            END
        ELSE
            !crete a new one
            get(exchange,0)
            if access:exchange.primerecord() = Level:Benign
                !error
            END
        END !if returning
        
        xch:model_number   = tmp:modelnumber
        xch:manufacturer   = tmp:manufacturer
        xch:esn            = tmp:esn
        If ?tmp:MSN{prop:Hide} = 0
            xch:msn            = tmp:MSN
        End !If tmp:MSN{prop:Hide} = 0
        xch:location       = tmp:location
        xch:shelf_location = tmp:shelflocation
        xch:date_booked    = Today()
        xch:available      = 'AVL'
        xch:StatusChangeDate = Today() ! #12127 Record status change. (Bryan: 13/07/2011)
        xch:stock_type     = tmp:stocktype
        If tmp:AssignAuditNumbers
            xch:Audit_Number    = tmp:AuditNumber
        Else !If tmp:AssignAuditNumbers
            xch:Audit_Number    = 0
        End !If tmp:AssignAuditNumbers

        IF (ordPartsID > 0)
            IF (orp:FreeExchangeStock)
                ! #12127 Save if the unit is Free STock or Purchased (Bryan: 01/07/2011)
                xch:FreeStockPurchased = 'F'
            ELSE
                xch:FreeStockPurchased = 'P'
            END
        END

        If ReturningHandset >0  then
            if Access:Exchange.update()
                !error
            END
        ELSE
            if access:exchange.tryinsert()
                !error
            END
        END !if returning handset

        !TB12461 - if returning then previous history has to be hidden from franchise
        If ReturningHandset then
            Access:Exchhist.clearkey(exh:Ref_Number_Key)
            exh:Ref_Number = xch:ref_number
            set(exh:Ref_Number_Key,exh:Ref_Number_Key)
            Loop
                if access:Exchhist.next() then break.
                if exh:Ref_Number <> xch:ref_number then break.
                exh:Notes = 'HIDDEN FROM FRANCHISE: '&clip(exh:notes)
                Access:Exchhist.update()
            END
        END
        !Add entry
        if access:exchhist.primerecord() = Level:Benign
            exh:ref_number      = xch:ref_number
            exh:date            = Today()
            exh:time            = Clock()
            access:users.clearkey(use:password_key)
            use:password        = glo:password
            access:users.fetch(use:password_key)
            exh:user            = use:user_code
            exh:status          = 'INITIAL ENTRY'
            access:exchhist.insert()
        end!if access:exchhist.primerecord() = Level:Benign

        If tmp:AssignAuditNumbers
            access:excaudit.clearkey(exa:audit_number_key)
            exa:stock_type   = xch:stock_type
            exa:audit_number = xch:audit_number
            if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                If exa:stock_unit_number = 0
                    exa:stock_unit_number = xch:ref_number
                    access:excaudit.update()
                    get(exchhist,0)
                    if access:exchhist.primerecord() = level:benign
                        exh:ref_number   = xch:ref_number
                        exh:date          = today()
                        exh:time          = clock()
                        access:users.clearkey(use:password_key)
                        use:password =glo:password
                        access:users.fetch(use:password_key)
                        exh:user = use:user_code
                        exh:status        = 'ASSIGNED AUDIT NUMBER: ' & Clip(xch:audit_number)
                        access:exchhist.insert()
                    end!if access:exchhist.primerecord() = level:benign
                End!If exa:stock_unit_number = ''
            end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign

        End !If tmp:AssignAuditNumbers

        ! Update order parts record
        if ordPartsID <> 0
            orp:Date_Received = today()
            ! #12351 Record the captured date, as this isn't used for exchange orders (Bryan: 27/10/2011)
            orp:TimeReceived = CLOCK()
            orp:DatePriceCaptured = TODAY()
            orp:TimePriceCaptured = CLOCK()
            orp:Number_Received += 1
            if orp:Number_Received >= orp:Quantity
                orp:All_Received = 'YES'
            end
            Access:ORDPARTS.TryUpdate()

            if access:exchhist.primerecord() = level:benign
                exh:ref_number   = xch:ref_number
                exh:date          = today()
                exh:time          = clock()
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                exh:user = use:user_code
                exh:status        = 'RECEIVED ON ORDER NO: ' & Clip(orp:Order_Number)
                if orp:GRN_Number <> 0
                    exh:status = clip(exh:status) & ' GRN NUMBER: ' & clip(orp:GRN_Number)
                end

                Access:ORDERS.Clearkey(ord:Order_Number_Key)
                ord:Order_Number = orp:Order_Number
                IF (Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign)
                    exh:Notes = 'SUPPLIER: ' & Clip(ord:Supplier) 
                ELSE
                    exh:Notes = 'SUPPLIER: ' & Clip(sto:Supplier) ! If can't get order for some reason, should have the stock records
                END

                access:exchhist.insert()
            end!if access:exchhist.primerecord() = level:benign

            if orp:All_Received = 'YES'
                post(Event:Accepted, ?Close)
            end
        end

        IF Access:Exchange.tryupdate() then
            if ReturningHandset = 0 then
                Access:Exchange.cancelautoinc()
            END
        END

!            Else !if access:exchange.tryinsert() = Level:Benign
!                access:exchange.cancelautoinc()
!            end

        untque:esn  = tmp:esn
        tmp:count   += 1
        untque:number = tmp:count
        Add(tmp:unitqueue)
        Sort(tmp:UnitQueue,-untque:number)
        tmp:esn = ''
        tmp:MSN = ''
        tmp:AuditNumber = 0
        Select(?tmp:esn)
        Display()
        !End!if access:exchange.primerecord() = Level:Benign
    End !if error# = 0

    EXIT


AveragePrice        Routine
DATA
savePurchaseCost        REAL()
CODE
    glo:ErrorText = ''
    savePurchaseCost = sto:Purchase_Cost
    sto:Purchase_Cost = Deformat(AveragePurchaseCost(sto:Ref_Number,sto:Quantity_Stock,sto:Purchase_Cost),@n14.2)
    If glo:ErrorText <> ''
        If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                             'ADD', | ! Transaction_Type
                             '', | ! Depatch_Note_Number
                             0, | ! Job_Number
                             0, | ! Sales_Number
                             0, | ! Quantity
                             sto:Purchase_Cost, | ! Purchase_Cost
                             sto:Sale_Cost, | ! Sale_Cost
                             sto:Retail_Cost, | ! Retail_Cost
                             'AVERAGE COST CALCULATION', | ! Notes
                             Clip(glo:ErrorText),|
                             sto:AveragePUrchaseCost,|
                             savePurchaseCost,|
                             sto:Sale_Cost,|
                             sto:Retail_Cost)
            ! Added OK
        Else ! AddToStockHistory
            ! Error
        End ! AddToStockHistory
    End !If glo:ErrorText <> ''
    glo:ErrorText = ''
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020272'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Rapid_Exchange_Unit')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:EXCAUDIT.Open
  Relate:JOBS_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:EXCHANGE_ALIAS.UseFile
  Access:EXCHHIST.UseFile
  Access:STOCKTYP.UseFile
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:LOCATION.UseFile
  Access:LOCSHELF.UseFile
  Access:ORDPARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  !defaults
  ?Sheet1{prop:wizard} = 1
  
  Access:USERS.Clearkey(use:Password_Key)
  use:Password    = glo:Password
  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      !Found
      If use:StockFromLocationOnly or glo:WebJob
          ?tmp:Location{prop:Disable} = 1
          ?CallLookup:4{prop:Disable} = 1
      End !If use:StockFromLocationOnly
      tmp:Location    = use:Location
  
  End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  
  Importing = false
  Notimporting = true
  ExceptionFileName = ''
      if ordPartsID <> 0
          ! #12127 If this from an order, why would the location be anything but Main Store (Bryan: 01/07/2011)
          tmp:Location = MainStoreLocation()
          ?tmp:Location{prop:Disable} = 1
          ?CallLookup:4{prop:Disable} = 1
          
          Access:ORDPARTS.ClearKey(orp:record_number_key)
          orp:Record_Number = ordPartsID
          if Access:ORDPARTS.Fetch(orp:record_number_key)
              Case Missive('Cannot access parts order record.','ServiceBase 3g',|
                  'mstop.jpg','/OK')
              Of 1 ! OK Button
              End ! Case Missive
              post(Event:Accepted, ?Close)
          else
              Access:STOCK.ClearKey(sto:Ref_Number_Key)
              sto:Ref_Number = orp:Part_Ref_Number
              if not Access:STOCK.Fetch(sto:Ref_Number_Key)
                  tmp:Manufacturer = sto:Manufacturer
                  glo:Select2 = tmp:Manufacturer      ! Used for model number filter
                  tmp:stockRecordFound = true
              else
                  Access:STOCK.ClearKey(sto:Location_Key)
                  sto:Location = MainStoreLocation()
                  sto:Part_Number = orp:Part_Number
                  if not Access:STOCK.TryFetch(sto:Location_Key)
                      tmp:Manufacturer = sto:Manufacturer
                      glo:Select2 = tmp:Manufacturer  ! Used for model number filter
                      tmp:stockRecordFound = true
                  end
              end
  
          end
      end
  ! Save Window Name
   AddToLog('Window','Open','Rapid_Exchange_Unit')
  ?List1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  IF ?tmp:StockType{Prop:Tip} AND ~?CallLookup{Prop:Tip}
     ?CallLookup{Prop:Tip} = 'Select ' & ?tmp:StockType{Prop:Tip}
  END
  IF ?tmp:StockType{Prop:Msg} AND ~?CallLookup{Prop:Msg}
     ?CallLookup{Prop:Msg} = 'Select ' & ?tmp:StockType{Prop:Msg}
  END
  IF ?tmp:Manufacturer{Prop:Tip} AND ~?CallLookup:2{Prop:Tip}
     ?CallLookup:2{Prop:Tip} = 'Select ' & ?tmp:Manufacturer{Prop:Tip}
  END
  IF ?tmp:Manufacturer{Prop:Msg} AND ~?CallLookup:2{Prop:Msg}
     ?CallLookup:2{Prop:Msg} = 'Select ' & ?tmp:Manufacturer{Prop:Msg}
  END
  IF ?tmp:ModelNumber{Prop:Tip} AND ~?CallLookup:3{Prop:Tip}
     ?CallLookup:3{Prop:Tip} = 'Select ' & ?tmp:ModelNumber{Prop:Tip}
  END
  IF ?tmp:ModelNumber{Prop:Msg} AND ~?CallLookup:3{Prop:Msg}
     ?CallLookup:3{Prop:Msg} = 'Select ' & ?tmp:ModelNumber{Prop:Msg}
  END
  IF ?tmp:Location{Prop:Tip} AND ~?CallLookup:4{Prop:Tip}
     ?CallLookup:4{Prop:Tip} = 'Select ' & ?tmp:Location{Prop:Tip}
  END
  IF ?tmp:Location{Prop:Msg} AND ~?CallLookup:4{Prop:Msg}
     ?CallLookup:4{Prop:Msg} = 'Select ' & ?tmp:Location{Prop:Msg}
  END
  IF ?tmp:ShelfLocation{Prop:Tip} AND ~?CallLookup:5{Prop:Tip}
     ?CallLookup:5{Prop:Tip} = 'Select ' & ?tmp:ShelfLocation{Prop:Tip}
  END
  IF ?tmp:ShelfLocation{Prop:Msg} AND ~?CallLookup:5{Prop:Msg}
     ?CallLookup:5{Prop:Msg} = 'Select ' & ?tmp:ShelfLocation{Prop:Msg}
  END
  IF ?tmp:AssignAuditNumbers{Prop:Checked} = True
    UNHIDE(?NewAuditNo)
    UNHIDE(?tmp:AuditNumber)
    UNHIDE(?tmp:AuditNumber:Prompt)
    UNHIDE(?btnPickAuditNo)
  END
  IF ?tmp:AssignAuditNumbers{Prop:Checked} = False
    HIDE(?NewAuditNo)
    HIDE(?tmp:AuditNumber)
    HIDE(?tmp:AuditNumber:Prompt)
    HIDE(?btnPickAuditNo)
  END
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('CSV files','*.csv')
  FileLookup9.WindowTitle='Stock Import File'
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:EXCAUDIT.Close
    Relate:JOBS_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Rapid_Exchange_Unit')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Stock_Type
      Browse_Manufacturers
      SelectModelKnownMake
      PickSiteLocations
      BrowseShelfLocations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?tmp:StockType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StockType, Accepted)
      glo:Select1 = 'XCH'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StockType, Accepted)
    OF ?CallLookup
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup, Accepted)
      glo:Select1 = 'XCH'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup, Accepted)
    OF ?CallLookup:5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:5, Accepted)
      glo:Select1 = tmp:Location
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CallLookup:5, Accepted)
    OF ?ButtonImport
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonImport, Accepted)
      if clip(ImportFileName) = '' then
          miss# = missive('Please select a file to import','ServiceBase 3g','mexclam.jpg','Oops')
      ELSE
          ExceptionFileName = ''  !to trigger a new one
          do TheImport
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ButtonImport, Accepted)
    OF ?Close
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      if ordPartsID <> 0
          if orp:All_Received <> 'YES'
              Case Missive('Not all received exchange units have been entered.'&|
                '<13,10>'&|
                '<13,10>Do you wish to enter the remaining units now or later?','ServiceBase 3g',|
                             'mquest.jpg','Later|Now')
                  Of 2 ! Now Button
                      !TB12423 - select manual or file import
                      If missive('Do you want to import a CSV file containing the IMEI numbers to be received or manually scan the IMEI numbers in?',|
                                 'ServiceBase 3g','mquest.jpg','Import CSV|Scan') = 2 then
                          !manual scan
                          Importing = false
                          Notimporting = true
                          Hide(?Tab3)
                          Unhide(?tab2)
                          Select(?Sheet1,2)
                          select(?tmp:esn)
                      ELSE
                          !missive was to import the CSV
                          Importing = true
                          Notimporting = false
                          Hide(?Tab2)
                          Unhide(?Tab3)
                          Select(?Sheet1,3)
                          select(?ImportFileName)
                      END
                          
                      cycle
                  Of 1 ! Later Button
                      if orp:Number_Received <> 0     ! If user has scanned some items, then create a new order line
                          glo:select3  = 'NEW ORDER'
                          glo:select4  = orp:record_number
                          glo:select5  = orp:Number_Received
                          orp:quantity = orp:quantity - orp:Number_Received
                          glo:select6  = orp:date_received
                          orp:date_received = ''
                          orp:Number_Received = 0
                          Access:ORDPARTS.TryUpdate()
                      end
              End ! Case Missive
          end
      
          if (tmp:stockRecordFound = true) and (orp:Number_Received <> 0)
              tmp:Information = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier)
              If orp:Reason <> ''
                  tmp:Information = Clip(tmp:Information) & '<13,10>ORDER COMMENTS: <13,10>' & Clip(orp:Reason)
              End ! If orp:Reason <> ''
              If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                 'ADD', | ! Transaction_Type
                                 '', | ! Depatch_Note_Number
                                 0, | ! Job_Number
                                 0, | ! Sales_Number
                                 orp:Number_Received, | ! Quantity
                                 orp:Purchase_Cost, | ! Purchase_Cost
                                 orp:Sale_Cost, | ! Sale_Cost
                                 orp:Retail_Cost, | ! Retail_Cost
                                 'STOCK ADDED FROM ORDER', | ! Notes
                                 Clip(tmp:Information)) ! Information
                ! Added OK
              Else ! AddToStockHistory
                ! Error
              End ! AddToStockHistory
      
              sto:quantity_on_order -= orp:Number_Received
      
              If sto:quantity_on_order < 0
                  sto:quantity_on_order = 0
              End
      
              Do AveragePrice
      
              If sto:Percentage_Mark_Up <> 0
                  sto:Sale_Cost   = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:Percentage_Mark_Up/100))
              End !If sto:Percentage_Mark_Up <> 0
              If sto:RetailMarkUp <> 0
                  sto:Retail_Cost = sto:Purchase_Cost + (sto:Purchase_Cost * (sto:RetailMarkUp/100))
              End !If sto:RetailMarkUp <> 0
      
              Access:STOCK.Update()
          end
      
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:StockType
      IF tmp:StockType OR ?tmp:StockType{Prop:Req}
        stp:Stock_Type = tmp:StockType
        !Save Lookup Field Incase Of error
        look:tmp:StockType        = tmp:StockType
        IF Access:STOCKTYP.TryFetch(stp:Use_Exchange_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:StockType = stp:Stock_Type
          ELSE
            !Restore Lookup On Error
            tmp:StockType = look:tmp:StockType
            SELECT(?tmp:StockType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update
      stp:Stock_Type = tmp:StockType
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:StockType = stp:Stock_Type
          Select(?+1)
      ELSE
          Select(?tmp:StockType)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:StockType)
    OF ?tmp:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
      glo:Select2 = tmp:Manufacturer
      IF tmp:Manufacturer OR ?tmp:Manufacturer{Prop:Req}
        man:Manufacturer = tmp:Manufacturer
        !Save Lookup Field Incase Of error
        look:tmp:Manufacturer        = tmp:Manufacturer
        IF Access:MANUFACT.TryFetch(man:Manufacturer_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:Manufacturer = man:Manufacturer
          ELSE
            !Restore Lookup On Error
            tmp:Manufacturer = look:tmp:Manufacturer
            SELECT(?tmp:Manufacturer)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Manufacturer, Accepted)
    OF ?CallLookup:2
      ThisWindow.Update
      man:Manufacturer = tmp:Manufacturer
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:Manufacturer = man:Manufacturer
          Select(?+1)
      ELSE
          Select(?tmp:Manufacturer)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Manufacturer)
    OF ?tmp:ModelNumber
      IF tmp:ModelNumber OR ?tmp:ModelNumber{Prop:Req}
        mod:Model_Number = tmp:ModelNumber
        !Save Lookup Field Incase Of error
        look:tmp:ModelNumber        = tmp:ModelNumber
        IF Access:MODELNUM.TryFetch(mod:Manufacturer_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:ModelNumber = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            tmp:ModelNumber = look:tmp:ModelNumber
            SELECT(?tmp:ModelNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:3
      ThisWindow.Update
      mod:Model_Number = tmp:ModelNumber
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:ModelNumber = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?tmp:ModelNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ModelNumber)
    OF ?tmp:Location
      IF tmp:Location OR ?tmp:Location{Prop:Req}
        loc:Location = tmp:Location
        !Save Lookup Field Incase Of error
        look:tmp:Location        = tmp:Location
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            tmp:Location = loc:Location
          ELSE
            !Restore Lookup On Error
            tmp:Location = look:tmp:Location
            SELECT(?tmp:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update
      loc:Location = tmp:Location
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          tmp:Location = loc:Location
          Select(?+1)
      ELSE
          Select(?tmp:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Location)
    OF ?tmp:ShelfLocation
      IF tmp:ShelfLocation OR ?tmp:ShelfLocation{Prop:Req}
        los:Shelf_Location = tmp:ShelfLocation
        !Save Lookup Field Incase Of error
        look:tmp:ShelfLocation        = tmp:ShelfLocation
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tmp:ShelfLocation = los:Shelf_Location
          ELSE
            !Restore Lookup On Error
            tmp:ShelfLocation = look:tmp:ShelfLocation
            SELECT(?tmp:ShelfLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update
      los:Shelf_Location = tmp:ShelfLocation
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tmp:ShelfLocation = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?tmp:ShelfLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ShelfLocation)
    OF ?tmp:AssignAuditNumbers
      IF ?tmp:AssignAuditNumbers{Prop:Checked} = True
        UNHIDE(?NewAuditNo)
        UNHIDE(?tmp:AuditNumber)
        UNHIDE(?tmp:AuditNumber:Prompt)
        UNHIDE(?btnPickAuditNo)
      END
      IF ?tmp:AssignAuditNumbers{Prop:Checked} = False
        HIDE(?NewAuditNo)
        HIDE(?tmp:AuditNumber)
        HIDE(?tmp:AuditNumber:Prompt)
        HIDE(?btnPickAuditNo)
      END
      ThisWindow.Reset
    OF ?tmp:esn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
      ! Inserting (DBH 01/06/2006) #6972 - Double check the IMEI Number
      If Clip(tmp:ESN) <> ''
          If ValidateIMEI(tmp:ESN) = True
              Case Missive('An IMEI Number mismatch has occurred. The IMEI Number entered does not match the original one.'&|
                '|Please try again.','ServiceBase 3g',|
                             'mstop.jpg','/OK')
                  Of 1 ! OK Button
              End ! Case Missive
              tmp:ESN = ''
              Select(?tmp:ESN)
              Cycle
          Else ! If ValidateIMEI(tmp:ESN) = True
              If ?tmp:MSN{prop:hide} = 1
                  Do AddUnit
              End !?tmp:MSN{prop:hide} = 1
          End ! If ValidateIMEI(tmp:ESN) = True
      End ! If Clip(tmp:ESN) <> ''
      ! End (DBH 01/06/2006) #6972
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:esn, Accepted)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
      Do AddUnit
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
    OF ?btnPickAuditNo
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseAuditNumbers(tmp:StockType)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnPickAuditNo, Accepted)
      IF GlobalResponse = RequestCompleted THEN
         tmp:AuditNumber = exa:Audit_Number
         ThisWIndow.UPDATE()
         DISPLAY(?tmp:AuditNumber)
      END !IF
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?btnPickAuditNo, Accepted)
    OF ?NewAuditNo
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NewAuditNo, Accepted)
      If SecurityCheck('EXCHANGE - NEW AUDIT NOS')
          Case Missive('You do not have access to this option.','ServiceBase 3g',|
                         'mstop.jpg','/OK')
              Of 1 ! OK Button
          End ! Case Missive
        do_update# = false
      Else!if x" = false
          Add_Exchange_Audit_Numbers(tmp:StockType)
      end!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NewAuditNo, Accepted)
    OF ?LookupFile
      ThisWindow.Update
      ImportFileName = FileLookup9.Ask(1)
      DISPLAY
    OF ?Next
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next, Accepted)
      Case Choice(?Sheet1)
          Of 1
              If tmp:stocktype = '' Or |
                  tmp:manufacturer = '' Or |
                  tmp:modelnumber = '' Or |
                  tmp:location    = '' Or |
                  tmp:shelflocation = ''
                  Case Missive('You must complete all the fields before you can continue.','ServiceBase 3g',|
                                 'mstop.jpg','/OK')
                      Of 1 ! OK Button
                  End ! Case Missive
              Else!If tmp:stocktype = '' Or |
                  If missive('Do you want to import a CSV file containing the IMEI numbers to be received or manually scan the IMEI numbers in?',|
                             'ServiceBase 3g','mquest.jpg','Import CSV|Scan') = 2 then
                      !manual scan
                      Importing = false
                      Notimporting = true
      
                      ?tmp:MSN{prop:Hide} = 1
                      ?tmp:MSN:Prompt{prop:Hide} = 1
                      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                      man:Manufacturer    = tmp:Manufacturer
                      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                          !Found
                          If man:Use_MSN = 'YES'
                              ?tmp:MSN{prop:Hide} = 0
                              ?tmp:MSN:Prompt{prop:Hide} = 0
                          End !If man:Use_MSN = 'YES'
      
                      End! If Access:MANUFACTURER.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                      Unhide(?tab2)
                      Select(?Sheet1,2)
                  ELSE
                      !missive was to import the CSV
                      Importing = true
                      Notimporting = false
                      Unhide(?Tab3)
                      Select(?Sheet1,3)
                  END
                  Disable(?Next)
                  Enable(?Back)
      
              End!If tmp:stocktype = '' Or |
      
      End!Case Choice(?Sheet1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Next, Accepted)
    OF ?back
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?back, Accepted)
      Case Choice(?Sheet1)
          Of 1
      
          Of 2 orof 3
              hide(?tab2)
              hide(?Tab3)
              Select(?Sheet1,1)
              Disable(?back)
              Enable(?Next)
      End!Case Choice(?Sheet1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?back, Accepted)
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020272'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020272'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020272'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Add_Exchange_Audit_Numbers PROCEDURE (f_stock_type)   !Generated from procedure template - Window

FilesOpened          BYTE
new_audit_numbers_temp LONG(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Add Exchange Audit Numbers'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Add Exchange Audit Numbers'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('How many new Audit Nos do you want to add?'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           PROMPT('New Audit Numbers'),AT(264,208),USE(?new_audit_numbers_temp:Prompt),TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n8),AT(344,208,64,10),USE(new_audit_numbers_temp),FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,STEP(1)
                         END
                       END
                       BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,ICON('cancelp.jpg'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020306'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Add_Exchange_Audit_Numbers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCAUDIT.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Add_Exchange_Audit_Numbers')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Add_Exchange_Audit_Numbers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020306'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020306'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020306'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:excaudit.clearkey(exa:audit_number_key)
      start# = 0
      exa:stock_type = f_stock_type
      set(exa:audit_number_key,exa:audit_number_key)
      Loop
          If access:excaudit.next()
              Break
          End!If access:excaudit.next()
          If exa:stock_type   <> f_stock_type
              Break
          End!If exa:stock_type   <> f_stock_type
          start# = exa:audit_number
      End!Loop
      
      Loop x# = 1 To new_audit_numbers_temp
          number# = start# + x#
          get(excaudit,0)
          if access:excaudit.primerecord() = Level:Benign
              exa:stock_type              = f_stock_type
              exa:audit_number            = number#
              exa:stock_unit_number       = ''
              exa:replacement_unit_number = ''
      
              If access:excaudit.tryinsert()
                  access:excaudit.cancelautoinc()
              End!If access:excaudit.tryinsert()
          end!if access:excaudit.primerecord() = Level:Benign
      End!Loop x# = 1 To new_audit_numbers_temp
      Post(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
BrowseTradeFaultCodesLookup PROCEDURE                 !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(TRAFAULO)
                       PROJECT(tfo:Description)
                       PROJECT(tfo:AccountNumber)
                       PROJECT(tfo:Field_Number)
                       PROJECT(tfo:Field)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tfo:Description        LIKE(tfo:Description)          !List box control field - type derived from field
tfo:AccountNumber      LIKE(tfo:AccountNumber)        !Primary key field - type derived from field
tfo:Field_Number       LIKE(tfo:Field_Number)         !Primary key field - type derived from field
tfo:Field              LIKE(tfo:Field)                !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::tfo:AccountNumber    LIKE(tfo:AccountNumber)
HK9::tfo:Field_Number     LIKE(tfo:Field_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse Fault Codes File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Fault Code Lookup File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,HSCROLL,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('80L(2)|M~Description~@s60@'),FROM(Queue:Browse:1)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Field'),USE(?Tab:2)
                           ENTRY(@s60),AT(264,98,124,10),USE(tfo:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,114),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                           BUTTON,AT(448,191),USE(?Insert:3),TRN,FLAT,LEFT,ICON('insertp.jpg')
                           BUTTON,AT(448,222),USE(?Change:3),TRN,FLAT,LEFT,ICON('editp.jpg'),DEFAULT
                           BUTTON,AT(448,250),USE(?Delete:3),TRN,FLAT,LEFT,ICON('deletep.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('closep.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020294'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseTradeFaultCodesLookup')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TRAFAULO.Open
  Access:TRAFAULT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRAFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseTradeFaultCodesLookup')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,tfo:Field_Key)
  BRW1.AddRange(tfo:Field_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,tfo:Field,1,BRW1)
  BRW1.AddField(tfo:Description,BRW1.Q.tfo:Description)
  BRW1.AddField(tfo:AccountNumber,BRW1.Q.tfo:AccountNumber)
  BRW1.AddField(tfo:Field_Number,BRW1.Q.tfo:Field_Number)
  BRW1.AddField(tfo:Field,BRW1.Q.tfo:Field)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRAFAULO.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseTradeFaultCodesLookup')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTradeFaultCodesLookup
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020294'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020294'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020294'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tfo:Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfo:Description, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tfo:Description, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = GLO:Select1
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = GLO:Select2
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

BrowseAuditNumbers PROCEDURE (f_stocktype)            !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:zero             BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:stocktype        STRING(30)
BRW1::View:Browse    VIEW(EXCAUDIT)
                       PROJECT(exa:Audit_Number)
                       PROJECT(exa:Record_Number)
                       PROJECT(exa:Stock_Type)
                       PROJECT(exa:Stock_Unit_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
exa:Audit_Number       LIKE(exa:Audit_Number)         !List box control field - type derived from field
exa:Record_Number      LIKE(exa:Record_Number)        !Primary key field - type derived from field
exa:Stock_Type         LIKE(exa:Stock_Type)           !Browse key field - type derived from field
exa:Stock_Unit_Number  LIKE(exa:Stock_Unit_Number)    !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK8::exa:Stock_Type       LIKE(exa:Stock_Type)
HK8::exa:Stock_Unit_Number LIKE(exa:Stock_Unit_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse Audit Number File'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       LIST,AT(264,112,148,212),USE(?Browse:1),IMM,FONT(,,,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('52L(2)|M~Audit Number~@s8@'),FROM(Queue:Browse:1)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Browse The Audit Number File'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?CurrentTab),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),SPREAD
                         TAB('By Audit Number'),USE(?Tab:2)
                           ENTRY(@s8),AT(264,98,64,10),USE(exa:Audit_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(448,111),USE(?Select:2),TRN,FLAT,LEFT,ICON('selectp.jpg')
                         END
                       END
                       BUTTON,AT(448,332),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020262'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BrowseAuditNumbers')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:EXCAUDIT.Open
  SELF.FilesOpened = True
  tmp:stocktype = f_stocktype
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:EXCAUDIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','BrowseAuditNumbers')
  ?Browse:1{prop:vcr} = TRUE
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,exa:TypeStockNumber)
  BRW1.AddRange(exa:Stock_Unit_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?exa:Audit_Number,exa:Audit_Number,1,BRW1)
  BRW1.AddField(exa:Audit_Number,BRW1.Q.exa:Audit_Number)
  BRW1.AddField(exa:Record_Number,BRW1.Q.exa:Record_Number)
  BRW1.AddField(exa:Stock_Type,BRW1.Q.exa:Stock_Type)
  BRW1.AddField(exa:Stock_Unit_Number,BRW1.Q.exa:Stock_Unit_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCAUDIT.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','BrowseAuditNumbers')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020262'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020262'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020262'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = tmp:stocktype
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = tmp:zero
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

Confirm_Import PROCEDURE (Stock_Type_Local,Location_Local) !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Stock_Type           STRING(30)
Location_Temp        STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Stock_Type
stp:Stock_Type         LIKE(stp:Stock_Type)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(STOCKTYP)
                       PROJECT(stp:Stock_Type)
                     END
FDCB5::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
window               WINDOW('Confirm Import'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Confirm Import'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(244,162,192,94),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Confirm Import'),USE(?Tab1)
                           STRING('Stock Type'),AT(248,194),USE(?String1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(308,194,124,10),USE(Stock_Type),IMM,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~Stock Type~@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           STRING('Location'),AT(248,212),USE(?String2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           COMBO(@s30),AT(308,212,124,10),USE(Location_Temp),IMM,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M~Location~@s30@'),DROP(5),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       BUTTON,AT(368,258),USE(?OkButton),TRN,FLAT,LEFT,ICON('okp.jpg'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020307'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Confirm_Import')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Relate:STOCKTYP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','Confirm_Import')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FDCB4.Init(Stock_Type,?Stock_Type,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:STOCKTYP,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder()
  FDCB4.AddField(stp:Stock_Type,FDCB4.Q.stp:Stock_Type)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:1,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:1
  FDCB5.AddSortOrder()
  FDCB5.AddField(loc:Location,FDCB5.Q.loc:Location)
  FDCB5.AddField(loc:RecordNumber,FDCB5.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:STOCKTYP.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','Confirm_Import')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      IF Stock_Type = ''
        SELECT(?Stock_Type)
        CYCLE
      END
      
      IF Location_Temp = ''
        SELECT(?Location_Temp)
        CYCLE
      END
      
      Stock_Type_Local = Stock_Type
      Location_Local = Location_Temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020307'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020307'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020307'&'0')
      ***
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
MinimumExchangeProcess PROCEDURE                      !Generated from procedure template - Process

Progress:Thermometer BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_xch_id          USHORT,AUTO
tmp:ExchangeUnitCount LONG
Process:View         VIEW(MODELNUM)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Tahoma',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepStringClass                  !Progress Manager
LastPctValue        LONG(0)                           !---ClarioNET 20
ClarioNET:PW:UserString   STRING(40)
ClarioNET:PW:PctText      STRING(40)
ClarioNET:PW WINDOW('Progress...'),AT(,,142,59),CENTER,TIMER(1),GRAY,DOUBLE
               PROGRESS,USE(Progress:Thermometer,,?ClarioNET:Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
               STRING(''),AT(0,3,141,10),USE(ClarioNET:PW:UserString),CENTER
               STRING(''),AT(0,30,141,10),USE(ClarioNET:PW:PctText),CENTER
             END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  LastPctValue = 0                                    !---ClarioNET 37
  ClarioNET:PW:PctText = ?Progress:PctText{Prop:Text}
  ClarioNET:PW:UserString{Prop:Text} = ?Progress:UserString{Prop:Text}
  ClarioNET:OpenPushWindow(ClarioNET:PW)
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MinimumExchangeProcess')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHANGE.Open
  Relate:EXMINLEV.Open
  Relate:MODELNUM.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','MinimumExchangeProcess')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ProgressMgr.Init(ScrollSort:AllowAlpha+ScrollSort:AllowNumeric,ScrollBy:RunTime)
  ThisProcess.Init(Process:View, Relate:MODELNUM, ?Progress:PctText, Progress:Thermometer, ProgressMgr, mod:Model_Number)
  ThisProcess.CaseSensitiveValue = FALSE
  ThisProcess.AddSortOrder(mod:Model_Number_Key)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(MODELNUM,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Save Window Name
   AddToLog('Window','Open','MinimumExchangeProcess')
  Bryan.CompFieldColour()


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:EXMINLEV.Close
    Relate:MODELNUM.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','MinimumExchangeProcess')
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  If mod:ExchangeUnitMinLevel <> 0
      tmp:ExchangeUnitCount = 0
      Setcursor(Cursor:Wait)
      Save_xch_ID = Access:EXCHANGE.SaveFile()
      Access:EXCHANGE.ClearKey(xch:AvailModOnlyKey)
      xch:Available    = 'AVL'
      xch:Model_Number = mod:Model_Number
      Set(xch:AvailModOnlyKey,xch:AvailModOnlyKey)
      Loop
          If Access:EXCHANGE.NEXT()
             Break
          End !If
          If xch:Available    <> 'AVL'      |
          Or xch:Model_Number <> mod:Model_Number      |
              Then Break.  ! End If
          tmp:ExchangeUnitCount += 1
      End !Loop
      Access:EXCHANGE.RestoreFile(Save_xch_ID)
      Setcursor()
  
      !If tmp:ExchangeUnitCount < mod:ExchangeUnitMinLevel
          If Access:EXMINLEV.PrimeRecord() = Level:Benign
              exm:Manufacturer = mod:Manufacturer
              exm:ModelNumber = mod:Model_Number
              exm:MinimumLevel    = mod:ExchangeUnitMinLevel
              exm:Available   = tmp:ExchangeUnitCount
              If Access:EXMINLEV.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:EXMINLEV.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:EXMINLEV.TryInsert() = Level:Benign
          End !If Access:EXMINLEV.PrimeRecord() = Level:Benign
      !End !tmp:ExchageUnitCount < mod:ExchangeUnitMinLevel
  End !mod:ExchangeUnitMinLevel <> 0
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:ClosePushWindow(ClarioNET:PW)         !---ClarioNET 88
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF LastPctValue <> Progress:Thermometer             !---ClarioNET 99
    IF INLIST(Progress:Thermometer,'5','10','15','20','25','30','35','40','45','50','55','60','65','70','75','80','85','90','95')
      LastPctValue = Progress:Thermometer
      ClarioNET:UpdatePushWindow(ClarioNET:PW)
    END
  END
  ReturnValue = PARENT.TakeRecord()
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
UpdateSubAddresses PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::sua:Record  LIKE(sua:RECORD),STATIC
QuickWindow          WINDOW('Update Sub Addresses'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Insert / Amend Sub Account Address'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(236,136),USE(?sua:AccountNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(320,136,124,10),USE(sua:AccountNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Company Name'),AT(236,152),USE(?sua:CompanyName:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,152,123,11),USE(sua:CompanyName),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('Postcode'),AT(236,206),USE(?sua:Postcode:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(320,206,64,10),USE(sua:Postcode),SKIP,LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Address'),AT(236,166),USE(?sua:AddressLine1:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,166,124,10),USE(sua:AddressLine1),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(320,178,124,10),USE(sua:AddressLine2),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Suburb'),AT(236,192),USE(?sua:AddressLine1:Prompt:2),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,192,124,10),USE(sua:AddressLine3),FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           BUTTON,AT(448,188),USE(?LookupSuburb),TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('Telephone Number'),AT(236,220),USE(?sua:TelephoneNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(320,220,64,10),USE(sua:TelephoneNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(236,236),USE(?sua:FaxNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s15),AT(320,236,64,10),USE(sua:FaxNumber),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Email Address'),AT(236,252),USE(?sua:EmailAddress:Prompt),TRN,FONT('Tahoma',8,COLOR:White,956,CHARSET:ANSI)
                           ENTRY(@s255),AT(320,252,124,10),USE(sua:EmailAddress),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Email Address'),TIP('Email Address')
                           PROMPT('Contact Name'),AT(236,268),USE(?sua:ContactName:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(320,268,124,10),USE(sua:ContactName),LEFT,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON,AT(380,332),USE(?OK),TRN,FLAT,LEFT,ICON('okp.jpg')
                       BUTTON,AT(448,332),USE(?Cancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Sub Account Address'
  OF ChangeRecord
    ActionMessage = 'Changing A Sub Account Address'
  END
  QuickWindow{Prop:Text} = ActionMessage
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020303'&'0'
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSubAddresses')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sua:Record,History::sua:Record)
  SELF.AddHistoryField(?sua:AccountNumber,3)
  SELF.AddHistoryField(?sua:CompanyName,4)
  SELF.AddHistoryField(?sua:Postcode,5)
  SELF.AddHistoryField(?sua:AddressLine1,6)
  SELF.AddHistoryField(?sua:AddressLine2,7)
  SELF.AddHistoryField(?sua:AddressLine3,8)
  SELF.AddHistoryField(?sua:TelephoneNumber,9)
  SELF.AddHistoryField(?sua:FaxNumber,10)
  SELF.AddHistoryField(?sua:EmailAddress,11)
  SELF.AddHistoryField(?sua:ContactName,12)
  SELF.AddUpdateFile(Access:SUBACCAD)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SUBACCAD.Open
  Relate:SUBURB.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:SUBACCAD
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  If Instring('/NOIMAGE',Command(),1,1)
      0{prop:Wallpaper} = ''
  End ! If Instring('/NOIMAGE',Command(),1,1)
  ! Inserting (DBH 04/08/2006) # 6643 - Find suburb? Was the postcode manually entered
  If ThisWindow.Request = ChangeRecord
      ! If edited the record and cannot find the suburb, assume that the postcode was manually entered. (DBH: 04/08/2006)
      Access:SUBURB.ClearKey(sur:SuburbKey)
      sur:Suburb = sua:AddressLine3
      If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Found
          ?sua:Postcode{prop:Skip} = True
          ?sua:Postcode{prop:ReadOnly} = True
      Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
          !Error
          ?sua:Postcode{prop:Skip} = False
          ?sua:Postcode{prop:ReadOnly} = False
      End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
  End ! If ThisWindow.Request = ChangeRecord
  ! End (DBH 04/08/2006) #6643
  ! Save Window Name
   AddToLog('Window','Open','UpdateSubAddresses')
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SUBACCAD.Close
    Relate:SUBURB.Close
  END
  ! Save Window Name
   AddToLog('Window','Close','UpdateSubAddresses')
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020303'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020303'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020303'&'0')
      ***
    OF ?sua:AddressLine3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sua:AddressLine3, Accepted)
      If ~0{prop:AcceptAll}
          Access:SUBURB.ClearKey(sur:SuburbKey)
          sur:Suburb = sua:AddressLine3
          If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Found
              sua:Postcode = sur:Postcode
              ?sua:Postcode{prop:Skip} = True
              ?sua:Postcode{prop:ReadOnly} = True
              Bryan.CompFieldColour()
              Display()
          Else ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
              !Error
              Case Missive('Cannot find the select Suburb. '&|
                '|Do you wish to RE-ENTER the Suburb, PICK from all available Suburbs, or CONTINUE and manually enter the postcode?','ServiceBase 3g',|
                             'mexclam.jpg','Continue|Pick|/Re-Enter')
                  Of 3 ! Re-Enter Button
                      sua:AddressLine3 = ''
                      sua:Postcode = ''
                      ?sua:Postcode{prop:Skip} = True
                      ?sua:Postcode{prop:ReadOnly} = True
                      Bryan.CompFieldColour()
                      Display()
                      Select(?sua:AddressLine3)
                  Of 2 ! Pick Button
                      sua:AddressLine3 = ''
                      sua:Postcode = ''
                      Post(Event:Accepted,?LookupSuburb)
                  Of 1 ! Continue Button
                      ?sua:Postcode{prop:Skip} = False
                      ?sua:Postcode{prop:ReadOnly} = False
                      Bryan.CompFieldColour()
                      Select(?sua:Postcode)
              End ! Case Missive
          End ! If Access:SUBURB.TryFetch(sur:SuburbKey) = Level:Benign
      End ! If ~0{prop:AcceptAll}
      
      
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sua:AddressLine3, Accepted)
    OF ?LookupSuburb
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseSuburbs
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSuburb, Accepted)
      Case GlobalResponse
          Of Requestcompleted
              sua:AddressLine3 = sur:Suburb
              sua:Postcode = sur:Postcode
              Select(?+2)
          Of Requestcancelled
              Select(?-1)
      End!Case Globalreponse
      ?sua:Postcode{prop:Skip} = True
      ?sua:Postcode{prop:ReadOnly} = True
      Bryan.CompFieldColour()
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupSuburb, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

